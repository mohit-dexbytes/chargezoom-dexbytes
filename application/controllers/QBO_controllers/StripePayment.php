<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;

class StripePayment extends CI_Controller
{
    
    private $resellerID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
	    
		
	    include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
	  
	
		$this->load->model('general_model');
	   $this->load->model('customer_model');
	   $this->load->model('quickbooks');
			$this->load->model('QBO_models/qbo_company_model');
        $this->load->model('QBO_models/qbo_customer_model');
			$this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', TRUE);
	  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   	
			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->merchantID = $logged_in_data['merchID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 	$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
            $this->merchantID = $merchID;
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	  
	   	redirect('QBO_controllers/home','refresh'); 
	}
	
	
		 
    public function pay_invoice()
    {
        
        $this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
            $token=array();
	        if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
	     $resellerID = $this->resellerID; 
	     $custom_data_fields = [];
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		$checkPlan = check_free_plan_transactions();
		 
		  $cardID = $this->czsecurity->xssCleanPostInput('CardID');
		  if (!$cardID || empty($cardID)) {
		  $cardID = $this->czsecurity->xssCleanPostInput('schCardID');
		  }
  
		  $gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
		  if (!$gatlistval || empty($gatlistval)) {
		  $gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
		  }
		  $gateway = $gatlistval;	
	   $cusproID=array();  $error=array();
       $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
		 $inv_amount           = $this->czsecurity->xssCleanPostInput('inv_amount'); 
		 if($this->czsecurity->xssCleanPostInput('qbo_check') == 'qbo_pay')
		 {
		  
		        if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 	
		 	
      
                 $token = $this->czsecurity->xssCleanPostInput('stripeToken');
                 if($this->czsecurity->xssCleanPostInput('tc')) 
    			 $tc = 1;
    			 else
    			 $tc = 0; 
    			 
    			   $ref_number= array();
       if($checkPlan && !empty($cardID) && !empty($gateway))
       {  
            $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
		 	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));

       
		if(!empty($in_data))
		{ 
		  
            $Customer_ListID = $in_data['CustomerListID'];
            
         
    		 $con_cust = array('Customer_ListID'=>$Customer_ListID,'merchantID'=>$merchID) ;
			 $c_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
		   	$companyID  = $c_data['companyID'];  
           if($cardID=='new1')
           {
                     $cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
						$custom_data_fields['payment_type'] = $friendlyname;
           }
        else{
            $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						$cardType = $card_data['CardType'];
        }
        $cardType = $this->general_model->getType($card_no);
        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
        $custom_data_fields['payment_type'] = $friendlyname;
        
		     
			 
		 if(!empty($cardID))
		 {
		     
		     	     $val = array(
				      	'merchantID' => $merchID,
			      	   );
    				$data = $this->general_model->get_row_data('QBO_token',$val);
    				
    				
    
    				$accessToken = $data['accessToken'];
    				$refreshToken = $data['refreshToken'];
    				 $realmID      = $data['realmID']; 
    				$dataService = DataService::Configure(array(
            				 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
    				));
    		
    			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
    	            
    	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
					if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                	{
				
				if( $in_data['BalanceRemaining'] > 0)
				{
				$cr_amount = 0;
				$amount    =	 $in_data['BalanceRemaining']; 
			     	$amount1    =  $inv_amount;
				     
				 	 $amount    = $inv_amount;
					        
		        	 $amount =  (int)($amount*100);
                    $order_id =mt_rand('300000','800000');

					$plugin = new ChargezoomStripe();
					$plugin->setApiKey($gt_result['gatewayPassword']);
        		 	$res = \Stripe\Token::create([
        									'card' => [
        									'number' =>$card_no,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv
        								   ]
        						]);
					$tcharge= json_encode($res);  
        			$rest = json_decode($tcharge);
        			
        			if($rest->id){
        				$token = $rest->id; 
        			}else{
        				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
        			}

        			$charge =	\Stripe\Charge::create(array(
        				  "amount" => $amount,
				        "currency" => "usd",
				          "source" => $token, // obtained with Stripe.js
				     "description" => "Charge Using Stripe Gateway",
				));	

			 $charge= json_encode($charge);
			 $result = json_decode($charge);
			 
             $crtxnID=''; $inID='';
             $resultID = isset($result->id)?$result->id:'';
			  if($result->paid=='1' && $result->failure_code=="")
              {
			  	 $code		 =  '200';
				
				$val = array(
					'merchantID' => $merchID,
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
            			 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
	            
	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                {
					$theInvoice = current($targetInvoiceArray);
				}
				

				$createPaymentObject = [
				    "TotalAmt" => $amount1,
				    "SyncToken" => 1,
				    "CustomerRef" => $Customer_ListID,
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $invoiceID,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount1
			        ]
				];

				$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
				if($paymentMethod){
					$createPaymentObject['PaymentMethodRef'] = [
						'value' => $paymentMethod['payment_id']
					];
				}
				if(isset($resultID) && $resultID != '')
	            {
	                $createPaymentObject['PaymentRefNum'] = substr($resultID, 0, 21);
	            }
				$newPaymentObj = Payment::create($createPaymentObject);
				$savedPayment = $dataService->Add($newPaymentObj);
				
				
				
				$error = $dataService->getLastError();
			if ($error != null) {
			  $ermsg=array();
		    	$ermsg.= "The Status code is: " . $error->getHttpStatusCode() . "\n";
				 $ermsg.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
				 $ermsg."The Response message is: " . $error->getResponseBody() . "\n";
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> '.$ermsg.'</div>');
				$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); $qbID=$resultID;	
				
			}
			else {
			    $crtxnID =	$savedPayment->Id;
			    $inID    = $resultID;
			    	      
				$this->session->set_flashdata('success','Successfully Processed Invoice'); 
					$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$resultID;
			}
                	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);

						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					    
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						  $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchID,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
									
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'],
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchID,
										  'is_default'			   => $is_default,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				    
				 $customerID =  $in_data['CustomerListID'];
				 $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchID); 
					$ref_number = $in_data['refNumber'];
						$tr_date   =date('Y-m-d H:i:s');
					 $toEmail = $c_data['userEmail']; $company=$c_data['companyName']; $customer = $c_data['fullName'];  
				        
						
    			
				}
				else{
					 $code =  $result->failure_code;
			       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->status.'</div>'); 
			       	$st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
						}
					  $qbID=$invoiceID;
				 	  
                      $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' =>$result->id,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$merchID,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				       $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
				        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;
                            

							$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }
				 	  $transaction['transactionID']      = $result->id;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d H:i:s'); 
					    $transaction['transactionModified']     = date('Y-m-d H:i:s'); 
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']  = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  $transaction['merchantID']   = $merchID;
					  $transaction['gateway']      = "Stripe";
					  $transaction['resellerID']   = $this->resellerID;
					  $transaction['invoiceID']    = $inID;
					 $transaction['qbListTxnID']   =  $crtxnID;
					 $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);

					if(!empty($this->transactionByUser)){
					    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
					 if($custom_data_fields){
		                  $transaction['custom_data_fields']  = json_encode($custom_data_fields);
		              }
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);

					   	if($st == 1 && $chh_mail =='1')
						{
							$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount/100, $tr_date, $result->id);
						} 
					}
				 else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
				
		     }else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');  
				}	
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	   	}
	   	else
	   	{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
		}
			 
    }
    else
    {
			  $ermsg=array();
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }
			
			 
		   	 
		   	  if($cusproID=="2"){
			 	 redirect('QBO_controllers/home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" ){
			 	redirect('QBO_controllers/Create_invoice/invoice_details_page/'.$in_data['invoiceID'],'refresh');
			 }
			 $trans_id = $result->id;
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'QBO_controllers/Create_invoice/Invoice_details',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					 'checkPlan'  => $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		  
		} 
		
    }     

	
	
	
	
	
	public function create_customer_sale()
    {
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");  	
	   	if(!empty($this->input->post(null, true))){

	   		$custom_data_fields = [];
	   		$applySurcharge = false;
			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
				$applySurcharge = true;
				$custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
			}

			if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
				$custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
			}
			$inv_array=array();    $inv_invoice=array();
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0; 
			 $ref_number= array();
			if($gatlistval !="" && !empty($gt_result) )
			{
			   
    		  
				
				 if($this->session->userdata('logged_in')){
				$user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				$custID =$this->czsecurity->xssCleanPostInput('customerID');
		
			 
				 $con_cust = array('Customer_ListID'=>$custID,'merchantID'=>$merchantID) ;
			     $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			
				$companyID  = $comp_data['companyID'];
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
               {	
				     	$card_no = $card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no = $card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}	
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;

					$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					// update amount with surcharge 
	                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
	                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
	                    $amount += round($surchargeAmount, 2);
	                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
	                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
	                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
	                    
	                }
	                $totalamount  = $amount;
					$cardType = $this->general_model->getType($card);
					$finalAmount =  (int)($totalamount*100);
					/*Added card type in transaction table*/
	                $cardType = $this->general_model->getType($card);
	                $friendlyname = $cardType . ' - ' . substr($card, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
	                
			
            		$metadata = [];
            		if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
            			$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 1);
						$metadata['invoice_number'] = $new_invoice_number;
					}

					if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
						$metadata['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
					}
					
					$plugin = new ChargezoomStripe();
					$plugin->setApiKey($gt_result['gatewayPassword']);
            		$res = \Stripe\Token::create([
        									'card' => [
        									'number' =>$card,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv
        								   ]
        						]);
					$tcharge= json_encode($res);  
        			$rest = json_decode($tcharge);
        			
        			if($rest->id){
        				$token = $rest->id; 
        			}else{
        				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
        			}
        			
					
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $finalAmount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						  'metadata' => $metadata
						 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
				 $trID='';
				 if($result->paid=='1' && $result->failure_code=="")
				 {
				  $code ='200';
				  $trID = $result->id;

				  
				  $this->session->set_flashdata('success','Transaction Successful');
				  
					$invoiceIDs='';
					$invoicePayAmounts = array();
					
				 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				 {
				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
				 }
				    
				             	$val = array('merchantID' => $merchantID);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
        							 'auth_mode' => $this->config->item('AuthMode'),
            						 'ClientID'  => $this->config->item('client_id'),
            						 'ClientSecret' =>$this->config->item('client_secret'),
            						 'accessTokenKey' =>  $accessToken,
            						 'refreshTokenKey' => $refreshToken,
            						 'QBORealmID' => $realmID,
            						 'baseUrl' => $this->config->item('QBOURL'),
								));
						$refNum=array();
			            	$qblist=array();
				           if(!empty($invoiceIDs))
				           	{
				           		$saleAmountRemaining = $amount;
				           		$payIndex = 0;
		                        foreach ($invoiceIDs as $inID) {
		                            $theInvoice = array();
		                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

		                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
		                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

		                            
		                            $refNumber[]   =  $res1['refNumber'];
		                            $txnID      = $inID;
		                            $pay_amounts = 0;


		                            $amount_data = $res1['BalanceRemaining'];
		                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
		                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

		                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
		                                $actualInvoicePayAmount += $surchargeAmount;
		                                $amount_data += $surchargeAmount;
		                                $updatedInvoiceData = [
		                                    'inID' => $inID,
		                                    'targetInvoiceArray' => $targetInvoiceArray,
		                                    'merchantID' => $user_id,
		                                    'dataService' => $dataService,
		                                    'realmID' => $realmID,
		                                    'accessToken' => $accessToken,
		                                    'refreshToken' => $refreshToken,
		                                    'amount' => $surchargeAmount,
		                                ];
		                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
		                            }

		                            $ispaid      = 0;
		                            $isRun = 0;
		                            if($saleAmountRemaining > 0){
		                                $BalanceRemaining = 0.00;
		                                if($amount_data == $actualInvoicePayAmount){
		                                    $actualInvoicePayAmount = $amount_data;
		                                    $isPaid      = 1;

		                                }else{

		                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
		                                    $isPaid      = 0;
		                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
		                                    
		                                }
		                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

		                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

		                                
		                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

		                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
		                                    $theInvoice = current($targetInvoiceArray);

											$createPaymentObject = [
		                                        "TotalAmt" => $actualInvoicePayAmount,
		                                        "SyncToken" => 1, 
		                                        "CustomerRef" => $custID,
		                                        "Line" => [
		                                            "LinkedTxn" => [
		                                                "TxnId" => $inID,
		                                                "TxnType" => "Invoice",
		                                            ],
		                                            "Amount" => $actualInvoicePayAmount
		                                        ]
		                                    ];
							
											$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
											if($paymentMethod){
												$createPaymentObject['PaymentMethodRef'] = [
													'value' => $paymentMethod['payment_id']
												];
											}
											if(isset($trID) && $trID != '')
								            {
								                $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
								            }
											$newPaymentObj = Payment::create($createPaymentObject);

		                                    $savedPayment = $dataService->Add($newPaymentObj);

		                                    $error = $dataService->getLastError();
		                                    if ($error != null) {
		                                        $err = '';
		                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
		                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
		                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
		                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

		                                        $st = '0';
		                                        $action = 'Pay Invoice';
		                                        $msg = "Payment Success but " . $error->getResponseBody();
		                                        $qbID = $trID;
		                                        $pinv_id = '';
		                                    } else {
		                                        $pinv_id = '';
		                                        $crtxnID = $pinv_id = $savedPayment->Id;
		                                        $st = '1';
		                                        $action = 'Pay Invoice';
		                                        $msg = "Payment Success";
		                                        $qbID = $trID;
		                                    }
		                                    $qbID = $inID;
		                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

		                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
		                                    if($syncid){
		                                        $qbSyncID = 'CQ-'.$syncid;

		                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

		                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
		                                    }
		                                    $transactiondata= array();
											$transactiondata['transactionID']       = $trID;
											$transactiondata['transactionStatus']    = $result->status;
											$transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
											$transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
											$transactiondata['transactionCode']     = $code;  
											$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
											$transactiondata['transactionType']    = 'stripe_sale';	
											$transactiondata['gatewayID']          = $gatlistval;
											$transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;					
											$transactiondata['customerListID']      = $custID;
											$transactiondata['transactionAmount']   = $actualInvoicePayAmount;
											$transactiondata['merchantID']             = $merchantID;
											$transactiondata['resellerID']            = $this->resellerID ;
											$transactiondata['gateway']      = "Stripe";
											$transactiondata['invoiceID']            = $inID;

											$transactiondata['qbListTxnID']          = $pinv_id; 

	                				        if($custom_data_fields){
				                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
				                            }
                					   		$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
	                					    if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											} 
											if($custom_data_fields){
								                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
								             }
                				       		$id = $this->general_model->insert_row('customer_transaction',   $transactiondata); 
		                                }
		                            }
		                            $payIndex++;

		                            
		                        }

						 
				         }else{
				             
				             
        						        $transactiondata= array();
                				       $transactiondata['transactionID']       = $trID;
                					   $transactiondata['transactionStatus']    = $result->status;
                					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
                					    $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
                					   $transactiondata['transactionCode']     = $code;  
									   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                						$transactiondata['transactionType']    = 'stripe_sale';	
                						$transactiondata['gatewayID']          = $gatlistval;
                                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;					
                					   $transactiondata['customerListID']      = $custID;
                					   $transactiondata['transactionAmount']   = ($result->amount/100);
                					    $transactiondata['merchantID']             = $merchantID;
                					     $transactiondata['resellerID']            = $this->resellerID ;
                							      $transactiondata['gateway']      = "Stripe";
                					  
                				        if($custom_data_fields){
			                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			                            }
                					    if(!empty($this->transactionByUser)){
										    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
										    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
										}
										if($custom_data_fields){
							                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
							            }
                				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				         }
						
				  
				  
				 /* This block is created for saving Card info in encrypted form  */
				 
		       
				 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                 {
				     
				        $this->load->library('encrypt');
			
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');		
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
		              	$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and merchantID='".$merchantID."'  and    customerCardfriendlyName ='".$friendlyname."' ")	;			   
					     
					  $crdata =   $query->row_array()['numrow'];
				
						if($crdata > 0)
						{
							 $card_type = $this->general_model->getType($card_no);
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'CardType'  =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$customerListID = $this->czsecurity->xssCleanPostInput('customerID');
						    $card_type = $this->general_model->getType($card_no);
						    $is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										    'CardType'  =>$card_type,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										 'customerListID' =>$customerListID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
						
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
                
						}
				
				 	}
				 	
					if($chh_mail =='1')
					{
						$customerID=  $custID;
				  		$condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
				  		$ref_number = implode(',',$refNum); 
				  		$tr_date   =date('Y-m-d H:i:s');
					  	$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName'];
						
						$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount/100, $tr_date, $result->id);
					} 
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }
                 else
                 {
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'.</div>'); 
					
					                     $transactiondata= array();
                				       $transactiondata['transactionID']       = $trID;
                					   $transactiondata['transactionStatus']    = $result->status;
                					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
                					    $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
                					   $transactiondata['transactionCode']     = $code;  
									   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                						$transactiondata['transactionType']    = 'stripe_sale';	
                						$transactiondata['gatewayID']          = $gatlistval;
                                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;					
                					   $transactiondata['customerListID']      = $custID;
                					   $transactiondata['transactionAmount']   = ($result->amount/100);
                					    $transactiondata['merchantID']             = $merchantID;
                					     $transactiondata['resellerID']            = $this->resellerID ;
                							      $transactiondata['gateway']      = "Stripe";
                						if($custom_data_fields){
			                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			                            }
                						if(!empty($this->transactionByUser)){
										    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
										    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
										}
										if($custom_data_fields){
							                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
							             }
                					    $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);		      
				     }
				 
				      
          
				}
				else
				{
			    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
				}	
             
           }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Payment token not created for stripe</div>'); 
				}
				$invoice_IDs = array();
		   if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			   $invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
		   }
	   
		   $receipt_data = array(
			   'transaction_id' => $result->id,
			   'IP_address' => getClientIpAddr(),
			   'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
			   'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
			   'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
			   'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
			   'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
			   'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
			   'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
			   'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
			   'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
			   'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
			   'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
			   'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
			   'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
			   'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
			   'Contact' => $this->czsecurity->xssCleanPostInput('email'),
			   'proccess_url' => 'QBO_controllers/Payments/create_customer_sale',
			   'proccess_btn_text' => 'Process New Sale',
			   'sub_header' => 'Sale',
		   );
		   
		   $this->session->set_userdata("receipt_data",$receipt_data);
		   $this->session->set_userdata("invoice_IDs",$invoice_IDs);
		   
		   
		   redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');
		 
	   }
	 
	 
	 
	 
	 
	
	public function create_customer_auth()
    {
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			$custom_data_fields = [];

			if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
			}

			$po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }

               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			   
    		  
				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
		
			$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID'=>$merchantID ));
				$companyID  = $comp_data['companyID'];
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
               {	
				     	$card_no = $card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no = $card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}	
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;

					$amount =  (int)($this->czsecurity->xssCleanPostInput('totalamount')*100);
			
					            		
					$plugin = new ChargezoomStripe();
					$plugin->setApiKey($gt_result['gatewayPassword']);
            
					$res = \Stripe\Token::create([
        									'card' => [
        									'number' =>$card,
        									'exp_month' => $expmonth,
        									'exp_year' =>  $exyear,
        									'cvc' => $cvv
        								   ]
        						]);
					$tcharge= json_encode($res);  
        			$rest = json_decode($tcharge);
        			
        			if($rest->id){
        				$token = $rest->id; 
        			}else{
        				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
        			}
					$metadata = [];
            		if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
						$metadata['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_number');
					}

					if (!empty($po_number)) {
						$metadata['po_number'] = $po_number;
					}
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $amount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						  'metadata' => $metadata,
						  'capture'     => 'false' 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
				 $trID='';
				 if($result->paid=='1' && $result->failure_code==""){
				  $code ='200';
				  $trID = $result->id;
				 /* This block is created for saving Card info in encrypted form  */
				 
		       
				 if($this->czsecurity->xssCleanPostInput('card_number')!="")
                 {
				     
				        $this->load->library('encrypt');
			
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');		
						$card_type       = $this->general_model->getType($card_no);
						$custom_data_fields['card_type'] = $cardType;
						$friendlyname   =  $card_type.' - '.substr($card_no,-4);

						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
			$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			   
					     
					  $crdata =   $query->row_array()['numrow'];
				
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										'CardType'     =>$card_type,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$customerListID = $this->czsecurity->xssCleanPostInput('customerID');
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										'CardType'     =>$card_type,
										'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										 'customerListID' =>$customerListID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
						
				            $id1 = $this->db1->insert('customer_card_data', $card_data);
				            $custom_data_fields['card_type'] = $cardType;	
                
				
						}
				
				 }
				    $this->session->set_flashdata('success','Transaction Successful');
				    
				 }
                 else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'.</div>'); 
				 }
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $trID;
					   $transactiondata['transactionStatus']    = $result->status;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $code;  
					   $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
						$transactiondata['transactionType']    = 'stripe_auth';
						$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');	
							$transactiondata['transaction_user_status']    = '5';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = ($result->amount/100);
					    $transactiondata['merchantID']   = $merchantID;
					     $transactiondata['resellerID']   = $this->resellerID ;
							      $transactiondata['gateway']   = "Stripe";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
          
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
				}	
				$invoice_IDs = array();
		  
	   
				$receipt_data = array(
					'transaction_id' => $result->id,
					'IP_address' => getClientIpAddr(),
					'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
					'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
					'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
					'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
					'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
					'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
					'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
					'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
					'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
					'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
					'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
					'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
					'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
					'Contact' => $this->czsecurity->xssCleanPostInput('email'),
					'proccess_url' => 'QBO_controllers/Payments/create_customer_auth',
					'proccess_btn_text' => 'Process New Transaction',
					'sub_header' => 'Authorize',
				);
				
				$this->session->set_userdata("receipt_data",$receipt_data);
				$this->session->set_userdata("invoice_IDs",$invoice_IDs);
				
				
				redirect('QBO_controllers/home/transation_sale_receipt',  'refresh');

		 }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Payment token not created for stripe</div>'); 
				}
		     redirect('QBO_controllers/Payments/payment_capture','refresh');
		 
		 
	   }
	 
	 
	
	 
	/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				} 
				
				
		if(!empty($this->input->post(null, true))){
				 $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			       
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			    if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
				
				$plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);

				$ch = \Stripe\Charge::retrieve($tID);
				$charge = $ch->capture();
								 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $con_cust = array('Customer_ListID'=>$customerID,'merchantID'=>$merchantID) ;
			    	 $comp_data = $this->general_model->get_select_data('QBO_custom_customer',array('companyID','companyName','fullName','userEmail'), $con_cust);
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
				
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded')){  
				     
				$amount = ($result->amount/100) ;
				 $code  ='200';
			    $trID   = $result->id;
				$condition = array('transactionID'=>$tID);
				$update_data =   array('transaction_user_status'=>"4");
				$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				$condition = array('transactionID'=>$tID);
				$customerID = $paydata['customerListID'];
				$tr_date   =date('Y-m-d H:i:s');
				$ref_number =  $tID;
				$toEmail = $comp_data['userEmail']; $company=$comp_data['companyName']; $customer = $comp_data['fullName']; 
				if($chh_mail =='1')
                            {
                               
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
							}
				$this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 $code =  $result->failure_code; 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				     
					 }
					 
					   $transactiondata= array();
				       $transactiondata['transactionID']      =  $trID;
					   $transactiondata['transactionStatus']  = $result->status;;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					    $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionType']    = 'stripe_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					    $transactiondata['merchantID']  =  $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID ;
				       $transactiondata['gateway']   = "Stripe"; 
				       $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

				        if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			            }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 $invoice_IDs = array();
					 
				 
					 $receipt_data = array(
						 'proccess_url' => 'QBO_controllers/Payments/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
						 }
						 if($paydata['customerListID'] == ''){
							 $paydata['customerListID'] ='null';
						 }
						 if($result->id == ''){
							 $result->id = 'null';
						 }
					 
					 redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->id,  'refresh');

        }
              
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	 
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
		if(!empty($this->input->post(null, true))){
		    $txnred ='';
	
			  $trID  = $this->czsecurity->xssCleanPostInput('trID');
	        
				 $con     = array('id'=>$trID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
		     if(!empty($paydata))
		     {
				  $tID     = $paydata['transactionID'];
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
    		    	 $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				  $amount  =  $total;
    	    	 $amount=   (int)($amount*100);
    		  	
				$plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);
				$charge = \Stripe\Refund::create(array(
				  "charge" => $tID,
				  "amount"=>$amount,
				));
			  
				 
				 $customerID = $paydata['customerListID'];
			  
			
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
			  
				   
				  
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded'))
				 {  
				     
				$amount = ($result->amount/100) ;
				 $code ='200';
			    $trID = $result->id;
			  
			   $c_data               =  $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
              
			    $merchantID = $this->session->userdata('logged_in')['merchID'];
			    
			    
			  
				$val = array(
					'merchantID' => $paydata['merchantID'],
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
        			 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			   
			   
			      $refund =$amount;
			
				  if(!empty($paydata['invoiceID']))
				  {
			   
			   
			   
					$item_data = $this->qbo_customer_model->get_merc_invoice_item_data($paydata['invoiceID'],$paydata['merchantID']);
                            

                    if (!empty($item_data)) {
                        $lineArray = array();
                               
						$i = 0;
						for ($i = 0; $i < count($item_data); $i++) {
							$LineObj = Line::create([
								"Amount"              => $item_data[$i]['itemQty']*$item_data[$i]['itemPrice'],
								"DetailType"          => "SalesItemLineDetail",
								"Description" => $item_data[$i]['itemDescription'],
								"SalesItemLineDetail" => [
									"ItemRef" => $item_data[$i]['itemID'],
									"Qty" => $item_data[$i]['itemQty'],
									"UnitPrice" => $item_data[$i]['itemPrice'],
									"TaxCodeRef" => [
										"value" => (isset($item_data[$i]['itemTax']) && $item_data[$i]['itemTax']) ? "TAX" : "NON",
									]
								],
		
							]);
							$lineArray[] = $LineObj;
						}
						$acc_id = '';
						$acc_name = '';
            			
            		     $acc_data = $this->general_model->get_select_data('QBO_accounts_list',array('accountID'),array('accountName'=>'Undeposited Funds', 'merchantID' => $paydata['merchantID']));
            		     if(!empty($acc_data))
            		     {
            		          $ac_value =$acc_data['accountID'];
            		     }else{
            		        $ac_value ='4'; 
						 }
						 $invoice_tax = $this->qbo_customer_model->get_merc_invoice_tax_data($paydata['invoiceID'],$paydata['merchantID']);
            		   	$theResourceObj = RefundReceipt::create([
                		    "CustomerRef" => $paydata['customerListID'], 
        		            "Line" => $lineArray,
        		          	"DepositToAccountRef" =>  [
            			        "value" =>$ac_value ,
            			        "name"=> "Undeposited Funds",
							  ],
							  "TxnTaxDetail" => [
								"TxnTaxCodeRef" => [
									"value" => (isset($invoice_tax['taxID']) && $invoice_tax['taxID']) ? $invoice_tax['taxID'] : "",
								],
							],
        		      ]);	  
            		$resultingObj = $dataService->Add($theResourceObj);
        			$error = $dataService->getLastError();	  
        			 
			 
					
					if ($error != null)
				 {
					
                      	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund '.$error.'</strong></div>'); 
                  }
                   $ins_id = $resultingObj->Id;
                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
				  	          
				  	           );	
				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
			   }	
                 }else{
                     
                       $ins_id = '';
                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
				  	          
				  	           );	
				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
                 }
			
				  $this->qbo_customer_model->update_refund_payment($tID, 'STRIPE');    
					 
		
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }      
				    
				    if($txnred == 'void'){
				            $ttype = 'stripe_void';
				        }
				        else{
				            $ttype = 'stripe_refund';
				        }
				        
				       $transactiondata= array();
				       $transactiondata['transactionID']      = $result->id;
					   $transactiondata['transactionStatus']  =  $result->status;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');  
					    $transactiondata['transactionmodified']     = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionType']    = $ttype;
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					   $transactiondata['merchantID']  = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID ;
					     if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
				       $transactiondata['gateway']   = "Stripe";
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                 $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			            }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				if($txnred == 'void'){
				    	redirect('QBO_controllers/Payments/refund_transaction','refresh');
				}
				
		     }	
		    	if(!empty($this->czsecurity->xssCleanPostInput('payrefund')))
		        {
					$invoice_IDs = array();
					
					$receipt_data = array(
						'proccess_url' => 'QBO_controllers/Payments/payment_refund',
						'proccess_btn_text' => 'Process New Refund',
						'sub_header' => 'Refund',
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					
					if($paydata['invoiceTxnID'] == ''){
					$paydata['invoiceTxnID'] ='null';
					}
					if($paydata['customerListID'] == ''){
						$paydata['customerListID'] ='null';
					}
					if($result->id == ''){
						$result->id = 'null';
					}
					redirect('QBO_controllers/home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$result->id,  'refresh');

	        	} else {		
				 
				redirect('QBO_controllers/Payments/payment_transaction','refresh');
				
	        	} 
        }
              
				
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_refund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	    }
	
	 
	 
	 
	
  public function get_single_card_data($cardID)
  {  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	    	
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				}		
					
					return  $card;

       }
 

	 
	
		 
    public function pay_multi_invoice()
    {
        
        
            $token='';
	        if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
	 	 $resellerID = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=> $merchID))['resellerID']; 
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');	
		 
		  $cusproID='';
		  $custom_data_fields = [];
       $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
		 
		 if($this->czsecurity->xssCleanPostInput('qbo_check') == 'qbo_pay'){
		 	
		 

         $multo_token = $this->czsecurity->xssCleanPostInput('stripeToken');
       if($cardID!=""&& $gateway&& !empty($multo_token) )
       {  
           	 
		 	$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));

         $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
         if(!empty($invoices)) 
         { 
            foreach($invoices  as $k=>$invoiceID)
            {    
                    $pay_amounts =$this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID); 
                	$amount    =	$pay_amounts;
                
                $token  =$multo_token[$k];
         
       $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
		if(!empty($in_data))
		{ 
		  
            $Customer_ListID = $in_data['CustomerListID'];
        	$c_data   = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID, 'merchantID'=>$merchID));
			$companyID = $c_data['companyID'];		 
    		   
           if($cardID=='new1')
           {
                     $cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
           }
        else{
            $card_data    =   $this->card_model->get_single_card_data($cardID); 
            $card_no  = $card_data['CardNo'];
           	$cvv      =  $card_data['CardCVV'];
			$expmonth =  $card_data['cardMonth'];
			$exyear   = $card_data['cardYear'];
			$cardType = $card_data['CardType'];
        }
        $cardType = $this->general_model->getType($card_no);
    	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
    	$custom_data_fields['payment_type'] = $friendlyname;

        
        
		     
			 
		 if(!empty($cardID))
		 {
				
				if( $in_data['BalanceRemaining'] > 0)
				{
				$cr_amount = 0;
				$amount    =	 $in_data['BalanceRemaining']; 
				
				     	$amount    =$pay_amounts;
				 	 $amount    = $amount-$cr_amount;
					        
		        	 $amount =  (int)($amount*100);

			$plugin = new ChargezoomStripe();
			$plugin->setApiKey($gt_result['gatewayPassword']); 
			$res = \Stripe\Token::create([
									'card' => [
									'number' =>$card_no,
									'exp_month' => $expmonth,
									'exp_year' =>  $exyear,
									'cvc' => $cvv
								   ]
						]);
			$tcharge= json_encode($res);  
			$rest = json_decode($tcharge);
			
			if($rest->id){
				$token = $rest->id; 
			}else{
				$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
			}   	 
			$charge =	\Stripe\Charge::create(array(
				  "amount" => $amount,
				  "currency" => "usd",
				  "source" => $token, // obtained with Stripe.js
				  "description" => "Charge Using Stripe Gateway",
				 
				));	

			 $charge= json_encode($charge);
			 $result = json_decode($charge);

			  if($result->paid=='1' && $result->failure_code=="")
              {
			  	 $code		 =  '200';
			
				$val = array(
					'merchantID' => $merchID,
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
        				 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
	            
	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                {
					$theInvoice = current($targetInvoiceArray);
				}
				
				$createPaymentObject = [
				    "TotalAmt" => $amount,
				    "SyncToken" => 1, 
				    "CustomerRef" => $Customer_ListID,
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $invoiceID,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				];

				$paymentMethod = $this->general_model->qbo_payment_method($cardType, $this->merchantID, false);
				if($paymentMethod){
					$createPaymentObject['PaymentMethodRef'] = [
						'value' => $paymentMethod['payment_id']
					];
				}
				if(isset($result->id) && $result->id != '')
	            {
	                $createPaymentObject['PaymentRefNum'] = substr($result->id, 0, 21);
	            }
				$newPaymentObj = Payment::create($createPaymentObject);
				$savedPayment = $dataService->Add($newPaymentObj);
				 
				
				
				$error = $dataService->getLastError();
			if ($error != null) 
			{
			  $ermsg='';
		    	$ermsg.= "The Status code is: " . $error->getHttpStatusCode() . "\n";
				 $ermsg.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
				 $ermsg."The Response message is: " . $error->getResponseBody() . "\n";
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> '.$ermsg.'</div>');
					$st='0'; $action='Pay Invoice'; $msg ="Payment Success but ". $error->getResponseBody(); $qbID=$result->id;	
			}
			else {
			    
				$this->session->set_flashdata('success','Successfully Processed Invoice'); 
					$st='1'; $action='Pay Invoice'; $msg ="Payment Success"; $qbID=$result->id;	
			}
                	 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
				 		
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					    
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						  $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchID,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
									
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					      	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'],
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchID,
										  'is_default'			   => $is_default,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				    
    			
    			
				}
				else{
						$st='0'; $action='Pay Invoice'; $msg ="Payment Failed"; $qbID=$invoiceID;
		 $code =  $result->failure_code;
       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->status.'</div>'); 
			}
				  $qbID=$invoiceID;
				  $qbo_log =array('syncType' => 'CQ','type' => 'transaction','transactionID' => $result->id,'qbStatus'=>$st, 'qbAction'=>$action,'qbText'=>$msg, 'merchantID'=>$merchID,'qbActionID'=>$qbID,'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'));
				       
				    $syncid = $this->general_model->insert_row('tbl_qbo_log',$qbo_log);
				    if($syncid){
                        $qbSyncID = 'CQ-'.$syncid;

						$qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                        
                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                    }
				 	  $transaction['transactionID']      = $result->id;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d H:i:s');  
					    $transaction['transactionmodified']     = date('Y-m-d H:i:s'); 
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['CustomerListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  $transaction['merchantID']   = $merchID;
					        if ($error == null) {
					            $transaction['qbListTxnID']   = $savedPayment->Id;
					        }
					  $transaction['gateway']   = "Stripe";
					  $transaction['resellerID']   = $resellerID;
					  $transaction['invoiceID']   = $invoiceID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
						if($custom_data_fields){
			                $transaction['custom_data_fields']  = json_encode($custom_data_fields);
			            }
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);

					}
				 else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
			 }
			 
            }
         }
         else
         {
            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');    
         }	 	 
			 
          }else{
			  $ermsg='';
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }
			
			 
		   	if($cusproID!=""){
			 	 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
		   	 }
		}
    }     

	
	
	
	}  