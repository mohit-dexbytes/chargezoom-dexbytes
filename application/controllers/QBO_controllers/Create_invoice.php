<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;

require APPPATH .'libraries/qbo/QBO_data.php';

class Create_invoice extends CI_Controller
{
    protected $loginDetails;
	function __construct()
	{
		parent::__construct();

		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('QBO_models/qbo_company_model');
		$this->load->model('QBO_models/qbo_customer_model');

		$this->load->model('QBO_models/qbo_invoices_model');

		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);

		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '1') {
			$merchID = $this->session->userdata('logged_in')['merchID'];
			$this->resellerID = $this->session->userdata('logged_in')['resellerID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}
		$this->loginDetails = get_names();
		$this->qboOBJ = new QBO_data($merchID); 
	}

	public function index()
	{

		redirect(base_url('QBO_controllers/home'));
	}



	public function ajax_invoices_list()
	{

		error_reporting(0);
		$customerID = '';
		$filter = $this->czsecurity->xssCleanPostInput('status_filter');
		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
			$merchID = $user_id;
		}
		$condition =  array('merchantID' => $merchID);

		$val = array(
			'merchantID' => $merchID,
		);

		$data = $this->general_model->get_row_data('QBO_token', $val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID, 'qb.companyID' => $realmID);
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');

		$list = $this->qbo_invoices_model->get_datatables_invoices($condition1, $filter);
		$list_data = $list['result'];
		$count = $this->qbo_invoices_model->count_all_invoices($condition1,$filter);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($merchID);
		$plantype_vs = $this->general_model->chk_merch_plantype_vt($merchID);
		$data = array();
		$no = $_POST['start'];
		
		foreach ($list_data as $person) {
			$no++;
			$row = array();

			$balance = ($person->BalanceRemaining) ? number_format($person->BalanceRemaining, 2) : "0.00";
			$base_url1 = base_url() . "QBO_controllers/home/view_customer/" . $person->CustomerListID;
			$base_url2 = base_url() . "QBO_controllers/Create_invoice/invoice_details_page/" . $person->invoiceID;
			if(empty($customerID)){

				$row[] = "<div class='text-center visible-lg'><input class='singleCheck' type='checkbox' name='selectRow[]'' value='" . $person->invoiceID . "' ></div>";
			
				if ($plantype) {
					$row[] = "<div class='hidden-xs text-left'>$person->custname";
				} else {
					$row[] = "<div class='hidden-xs text-left cust_view'><a href='" . $base_url1 . "' >$person->custname </a>";
				}

				if($plantype_vs){
					$row[] = "<div class='text-right cust_view'>".$person->refNumber;
				}else{
					$row[] = "<div class='text-right cust_view'><a href='" . $base_url2 . "' >$person->refNumber </a>";
				}
			}else{
				if($plantype_vs){
					$row[] = "<div class='text-left cust_view'>".$person->refNumber;
				}else{
					$row[] = "<div class='text-left cust_view'><a href='" . $base_url2 . "' >$person->refNumber </a>";
				}
			}
			
			$row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($person->DueDate)) . " </div>";

			if ($person->Total_payment != "0.00") {

				
			} else {

				
			}

			$row[] = '<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="set_qbo_payment_data(\'' . $person->invoiceID . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' .  number_format(($person->BalanceRemaining), 2) . ' </a> </div>';

			
			$row[] =	"<div class='text-right'>$person->status</div>";

			$link = '';
			$disabled_class = "";
			if(strtolower($filter) == 'paid' AND strtolower($person->payment_gateway) == 'heartland echeck'){
				$disabled_class = "disabled";
			}
			if (($person->BalanceRemaining == 0 ||  $person->IsPaid == '1') && $person->UserStatus == '0') {


				$link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li>
						           <a href="javascript:void(0);" id="txnRefund'. $person->invoiceID.'"  invoice-id="'. $person->invoiceID.'" integration-type="1" data-url="'.base_url().'ajaxRequest/getTotalRefundOfInvoice" class="refunTotalInvoiceAmountCustom">Refund</a>
						           </li>
                               
						          </ul>
						          </div> </div>';
			} else  if ($person->UserStatus == '1') {
				$link .= '<div class="text-center">
             	             <div class="btn-group dropbtn">
                                 <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
						           <li><a href="javascript:void(0);"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>
                               
						          </ul>
						          </div> </div>';
			} else {
				
				$link .= '<div class="text-center">
             	            	
                     	            	<div class="btn-group dropbtn">
                                         <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle '.$disabled_class.'">Select <span class="caret"></span></a>
                                         <ul class="dropdown-menu text-left">
                                            <li> <a href="#qbo_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                                     data-toggle="modal" onclick="set_qbo_invoice_process_id(\'' . $person->invoiceID . '\',\'' . $person->CustomerListID . '\',\'' . $person->BalanceRemaining . '\',1);">Process</a></li>
                                       <li><a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id(\'' . $person->invoiceID . '\',\'' . $person->CustomerListID . '\',\'' . $person->BalanceRemaining . '\',\'' . date('M d Y', strtotime($person->DueDate)) . '\');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                                            
                                        <li> <a href="#set_subs" class=""  onclick=
        							"set_sub_status_id(\'' . $person->invoiceID . '\',\'' . $person->BalanceRemaining . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>';
				if ($person->BalanceRemaining == $person->Total_payment) {

					$link .= '<li> <a href="#qbo_invoice_delete" onclick="get_invoice_id(\'' . $person->invoiceID . '\');" data-keyboard="false" data-toggle="modal" class="'.$disabled_class.'">Void</a></li>';
				}

				$link .= '</ul>
                                     </div> </div>  ';
			}

			$row[] = $link;

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $count,
			"recordsFiltered" => $count,
			"data" => $data,
			
		);
		//output to json format
		echo json_encode($output);
		die;
	}


	public function Invoice_details()
	{

		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_ids = $this->session->userdata('user_logged_in');
			$merchID = $user_id = $user_ids['merchantID'];
		}
		$condition =  array('merchantID' => $merchID);

		$val = array(
			'merchantID' => $merchID,
		);

		$data = $this->general_model->get_row_data('QBO_token', $val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID, 'qb.companyID' => $realmID);
		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' => $this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config', array('lastUpdated','lastTimeStamp'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery'));

        if (!empty($qbo_data)) {
            $last_date   = $qbo_data['lastUpdated'];
            $lastTimeStamp   = $qbo_data['lastTimeStamp'];

            if ($lastTimeStamp == 'UTC') {
                $my_timezone = date_default_timezone_get();
                $from = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
                $to   = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
                $last_date =  $this->general_model->datetimeconvqbo($last_date, $from, $to);
                $last_date1 = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

                $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));
            } else {
                $latedata = $last_date;
            }

			$sqlQuery = "SELECT * FROM Invoice where Metadata.LastUpdatedTime > '$latedata'";
        } else {
			$latedata = date('Y-m-d') . 'T' . date('H:i:s');
			$sqlQuery = "SELECT * FROM Invoice where Balance >'0' ";
		}

		$keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}

		$err = '';
		$error = $dataService->getLastError();
		if ($error != null) {
			$err .= $error->getHttpStatusCode() . "\n";
			$err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err .= "The Response message is: " . $error->getResponseBody() . "\n";
			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . QBO_DISCONNECT_MSG . '</div>');
			 redirect('QBO_controllers/home/index', 'refresh');
		}

		// #Sync Customer & tax data
		$this->qbo = new QBO_data($merchID); 
		$this->qbo->get_customer_data();
		$this->qbo->get_payment_method(); 
		
        $prevUpdateTime = strtotime($latedata);
		if (!empty($entities)) {
			foreach ($entities as $oneInvoice) {

				#Update Last Sync
				$mostUpdatedTime = strtotime($oneInvoice->MetaData->LastUpdatedTime);
                if($mostUpdatedTime > $prevUpdateTime){
                    $prevUpdateTime = $mostUpdatedTime;
                    $latedata= $oneInvoice->MetaData->LastUpdatedTime;
                }

				$QBO_invoice_details['invoiceID'] = $oneInvoice->Id;
				$QBO_invoice_details['refNumber'] = $oneInvoice->DocNumber;

				if ($oneInvoice->ShipAddr != "") {
					$QBO_invoice_details['ShipAddress_Addr1'] = ($oneInvoice->ShipAddr->Line1) ? $oneInvoice->ShipAddr->Line1 : '';
					$QBO_invoice_details['ShipAddress_Addr2'] = ($oneInvoice->ShipAddr->Line2) ? $oneInvoice->ShipAddr->Line2 : '';
					$QBO_invoice_details['ShipAddress_City'] = ($oneInvoice->ShipAddr->City) ? $oneInvoice->ShipAddr->City : '';
					$QBO_invoice_details['ShipAddress_State'] = ($oneInvoice->ShipAddr->CountrySubDivisionCode) ? $oneInvoice->ShipAddr->CountrySubDivisionCode : '';
					$QBO_invoice_details['ShipAddress_Country'] = ($oneInvoice->ShipAddr->Country) ? $oneInvoice->ShipAddr->Country : '';
					$QBO_invoice_details['ShipAddress_PostalCode'] = ($oneInvoice->ShipAddr->PostalCode) ? $oneInvoice->ShipAddr->PostalCode : '';
				}
				if ($oneInvoice->BillAddr != "") {
					$QBO_invoice_details['BillAddress_Addr1'] = ($oneInvoice->BillAddr->Line1) ? $oneInvoice->BillAddr->Line1 : '';
					$QBO_invoice_details['BillAddress_Addr2'] = ($oneInvoice->BillAddr->Line2) ? $oneInvoice->BillAddr->Line2 : '';
					$QBO_invoice_details['BillAddress_City'] = ($oneInvoice->BillAddr->City) ? $oneInvoice->BillAddr->City : '';
					$QBO_invoice_details['BillAddress_State'] = ($oneInvoice->BillAddr->CountrySubDivisionCode) ? $oneInvoice->BillAddr->CountrySubDivisionCode : '';
					$QBO_invoice_details['BillAddress_Country'] = ($oneInvoice->BillAddr->Country) ? $oneInvoice->BillAddr->Country : '';

					$QBO_invoice_details['BillAddress_PostalCode'] = ($oneInvoice->BillAddr->PostalCode) ? $oneInvoice->BillAddr->PostalCode : '';
				}

				$QBO_invoice_details['DueDate'] = $oneInvoice->DueDate;
				$QBO_invoice_details['BalanceRemaining'] = $oneInvoice->Balance;
				$QBO_invoice_details['Total_payment'] = $oneInvoice->TotalAmt;
				$QBO_invoice_details['CustomerListID'] = $oneInvoice->CustomerRef;

				if ($oneInvoice->PrivateNote != 'Voided'){
					$QBO_invoice_details['userStatus'] = 0;
					if ($oneInvoice->Balance == 0)
						$QBO_invoice_details['IsPaid'] = 1;
					else
						$QBO_invoice_details['IsPaid'] = 0;
				} else {
					$QBO_invoice_details['userStatus'] = 1;
				}

				$QBO_invoice_details['merchantID'] = $merchID;
				$QBO_invoice_details['companyID'] = $realmID;
				$QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
				$QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));

				if ($oneInvoice->TxnTaxDetail) {
					$taxref = $oneInvoice->TxnTaxDetail->TxnTaxCodeRef;
					if ($taxref) {
						$tax_data =    $this->general_model->get_row_data('tbl_taxe_code_qbo', array('taxID' => $taxref, 'merchantID' => $merchID));
						if(isset($tax_data['total_tax_rate']) && $tax_data['total_tax_rate'] != ''){
                            $QBO_invoice_details['taxRate'] = $tax_data['total_tax_rate'];
                        }else{
                            $QBO_invoice_details['taxRate'] = 0;
                        }
						
						$QBO_invoice_details['taxID'] = $taxref;
					} else {
						$QBO_invoice_details['taxRate'] = 0;
					}

					$QBO_invoice_details['totalTax'] = $oneInvoice->TxnTaxDetail->TotalTax;
				} else {
					$QBO_invoice_details['totalTax'] = 0.00;
				}

				if(!isset($QBO_invoice_details['taxRate']) || $QBO_invoice_details['taxRate'] === NULL) {
					$QBO_invoice_details['taxRate'] = 0;
				}

				if ($this->general_model->get_num_rows('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID)) > 0) {

					$this->general_model->update_row_data('QBO_test_invoice', array('companyID' => $realmID, 'invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID), $QBO_invoice_details);

					$line_items = $oneInvoice->Line;

					$l_data = array();
					$k = 0;

					if (!empty($line_items)) {
						
						$this->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID, 'releamID' => $realmID));
						foreach ($line_items as $line) {
							$l_data = array();

							if (isset($line->Id)) {


								$l_data['itemID'] = $line->LineNum;

								if ($line->Description)
									$l_data['itemDescription'] = $line->Description;
								$l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';
								$l_data['itemPrice'] = 0.00;
								$l_data['itemQty'] = 1;
								$l_data['itemTax'] = 0;
								if(!empty($line->SalesItemLineDetail)){
									$l_data['itemRefID'] = $line->SalesItemLineDetail->ItemRef;
									if ($line->SalesItemLineDetail->UnitPrice){
										$l_data['itemPrice'] = $line->SalesItemLineDetail->UnitPrice;
									}

									if ($line->SalesItemLineDetail->Qty){
										$l_data['itemQty'] = $line->SalesItemLineDetail->Qty;
									}
									if ($line->SalesItemLineDetail->TaxCodeRef){
										$l_data['itemTax'] = ($line->SalesItemLineDetail->TaxCodeRef == "TAX") ? 1 : 0;
									}
									
								}
								

								$l_data['invoiceID'] = $oneInvoice->Id;
								$l_data['merchantID'] = $merchID;
								$l_data['releamID'] = $realmID;
								$l_data['createdAt'] = date('Y-m-d H:i:s');

								
								$this->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
							}
						}
					}
				} else {
					$line_items = $oneInvoice->Line;

					$l_data = array();
					$k = 0;

					if (!empty($line_items)) {
						
						$this->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $oneInvoice->Id, 'merchantID' => $merchID, 'releamID' => $realmID));
						foreach ($line_items as $line) {
							$l_data = array();
							if (isset($line->Id)) {

								$l_data['itemID'] = $line->LineNum;
								if ($line->Description)
									$l_data['itemDescription'] = $line->Description;
								$l_data['totalAmount'] = ($line->Amount) ? $line->Amount : '0';

								$l_data['itemRefID'] = $line->SalesItemLineDetail->ItemRef;
								if ($line->SalesItemLineDetail->UnitPrice)
									$l_data['itemPrice'] = $line->SalesItemLineDetail->UnitPrice;
								else
									$l_data['itemPrice'] = 0.00;
								if ($line->SalesItemLineDetail->Qty)
									$l_data['itemQty'] = $line->SalesItemLineDetail->Qty;
								else
									$l_data['itemQty'] = 1;

								$l_data['invoiceID'] = $oneInvoice->Id;
								$l_data['merchantID'] = $merchID;
								$l_data['releamID'] = $realmID;
								$l_data['createdAt'] = date('Y-m-d H:i:s');

								if ($line->SalesItemLineDetail->TaxCodeRef)
									$l_data['itemTax'] = ($line->SalesItemLineDetail->TaxCodeRef == "TAX") ? 1 : 0;
								else
									$l_data['itemTax'] = 0;

								$this->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
							}
						}
					}

					$this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
				}
			}

			#Create Logs for SYNC New Entry
			$qbo_log = array('syncType' => 'CQ','type' => 'invoiceImport','qbStatus' => 1, 'qbAction' => 'InvoiceDataImport', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
			$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
			if($syncid){
	            $qbSyncID = 'CQ-'.$syncid;
	            $qbSyncStatus = 'Invoice Data Imported';
	            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus));
	        }
		}

		if (!empty($qbo_data)) {
			$updatedata['lastUpdated'] = $latedata;
			$updatedata['lastTimeStamp'] = 'America/Los_Angeles';
			$updatedata['updatedAt'] = date('Y-m-d H:i:s');
			if (!empty($entities))
				$this->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery'), $updatedata);
		} else {
			$updateda = date('Y-m-d') . 'T' . date('H:i:s');
			$upteda = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'InvoiceQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
			if (!empty($entities))
				$this->general_model->insert_row('tbl_qbo_config', $upteda);
		}

		if(!empty($this->input->post(null, true))) {
			$search = $this->input->post(null, true);
			$data['serachString'] = $search['search_data'];
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$data['gateway_datas']		  = $this->general_model->get_gateway_data($merchID);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];
		
		$merchant_condition = [
			'merchID' => $merchID,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}

		$data['page_num']      = 'customer_qbo';
		$data['in_details']           = $this->qbo_customer_model->get_invoice_details($merchID);
		$data['failed_invoice']           = $this->qbo_customer_model->get_failed_invoice($merchID);
		$data['paid_invoice']           = $this->qbo_customer_model->get_paid_invoice($merchID);
		$data['shedule_invoice']           = $this->qbo_customer_model->get_shedule_invoice($merchID);
		$data['page'] = 'page_invoices';

		$filter = $this->czsecurity->xssCleanPostInput('status_filter');
		if($filter == ''){
			$data['filter'] = 'open';
		}else{
			$data['filter'] = $filter;
		}
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$data['plantype_as'] = $this->general_model->chk_merch_plantype_as($merchID);
		$data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($merchID);
		
		
		$data['overdue_invoice']           = $this->qbo_customer_model->get_overdue_invoice($merchID);

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];
		$data['merchantEmail'] = $this->session->userdata('logged_in')['merchantEmail']; 
		$templateList = $this->general_model->getTemplateForInvoice($user_id);
		$data['templateList'] = $templateList;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_invoices', $data);
		$this->load->view('QBO_views/page_qbo_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function invoice_details_page()
	{

		$oneInvoice = array();
		if ($this->session->userdata('logged_in')) {
			$data['login_info']	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info']		= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchID = $user_id;

		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID);

		$condition =  array('merchantID' => $merchID);

		$inv_id = $this->uri->segment('4');

		#Invoice existence check at QBO
		$this->qbo = new QBO_data($merchID); 
		$this->qbo->get_tax_data();
		$inv_data = $this->qbo->check_qbo_invoice([
			'invoiceID' => $inv_id
		]);
		$this->qbo->get_items_data();

		$val = array(
			'merchantID' => $merchID,
		);

		$data1 = $this->general_model->get_row_data('QBO_token', $val);
		$accessToken = $data1['accessToken'];
		$refreshToken = $data1['refreshToken'];
		$realmID      = $data1['realmID'];

		$oneInvoice = $this->general_model->get_row_data('QBO_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID, 'companyID' => $realmID));


		if (!empty($oneInvoice)) {
			$oneInvoice['l_items'] = $this->qbo_invoices_model->get_item_data_new(array('invoiceID' => $inv_id, 'merchantID' => $merchID, 'releamID' => $realmID));
			$oneInvoice['cust_data'] = $this->general_model->get_row_data('QBO_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $oneInvoice['CustomerListID']));

			

		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error Invalid Invoice </strong></div>');
			redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$data['gateway_datas']		  = $this->general_model->get_gateway_data($merchID);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];

		$merchant_condition = [
			'merchID' => $merchID,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
			$data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}
		$data['page_num']      = 'invoice_details';

		$invoiceID = $inv_id;
		$data['invoice_data']     = $oneInvoice;

		$con = array('invoiceID' => $oneInvoice['invoiceID'], 'merchantID' => $merchID);
		$data['notes']   		  = $this->qbo_customer_model->get_customer_note_data($oneInvoice['cust_data']['Customer_ListID'], $merchID);
		$transactionData    =  $this->general_model->get_row_data('customer_transaction', $con);

		if(!empty($transactionData)){
			if (strpos($transactionData['gateway'], 'ECheck') !== false) {
	            $transactionData['paymentType'] = 2;
	        }else if (strpos($transactionData['gateway'], 'Offline') !== false) {
	            $transactionData['paymentType'] = 'Offline Payment';
	        }else{
	            $transactionData['paymentType'] = 1;
	        }
		}
        
        $data['transaction'] = $transactionData;

		$conditionPlan 			= array('merchantID' => $merchID);
		$qboItems = $this->general_model->get_table_data('QBO_test_item', $conditionPlan);

		// Traverse Items
		foreach($qboItems as $k1=>$qbItem){
			if($qbItem['Type'] == 'Group') {
				$childItems = $this->general_model->get_table_data('qbo_group_lineitem', array('groupListID' => $qbItem['ListID']));
				$price2 = 0;
				foreach($childItems as $ci){
					$price2 += (float)$ci['Price'];
				}

				$qboItems[$k1]['saleCost'] = (float)$price2;
			}
		}
		
		$data['plans'] = $qboItems;

		$code = '';
		$link_data = $this->general_model->get_row_data('tbl_template_data', array('merchantID' => $merchID, 'invoiceID' => $invoiceID));
		$user_id = $merchID;
		$coditionp = array('merchantID' => $user_id, 'customerPortal' => '1');

		$ur_data = $this->general_model->get_select_data('tbl_config_setting', array('customerPortalURL'), $coditionp);
		$data['ur_data'] = $ur_data;
		$position1 = $position = 0;
		$purl = '';
		if(isset($ur_data['customerPortalURL']) && !empty($ur_data['customerPortalURL'])){
			$ttt = explode(PLINK, $ur_data['customerPortalURL']);
			$purl = $ttt[0] . PLINK . '/customer/';
		}
		
		$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
		$shuffled = str_shuffle($str);
		$shuffled = substr($shuffled, 1, 12) . '=' . $user_id;
		$code     =    $this->safe_encode($shuffled);
		$invcode    =  $this->safe_encode($invoiceID);
		$data['paylink']  = $purl . 'update_payment/' . $code . '/' . $invcode;

		$today =date('Y-m-d');
        
        $due_date = date('Y-m-d', strtotime($data['invoice_data']['DueDate'])); 
	
	    $type =0;
	
	    $date1=strtotime($due_date);  
	    $date2=strtotime($today); 
		if($date1 >= $date2)
		{
		    $diff= $date2 - $date1;

		    $diff =floor($diff / (60*60*24) );
		    $type = 3;
		    
		}
		else{
		      
		    $diff= $date2 - $date1;
		
		    $diff= floor($diff / (60*60*24) ); 
		    if($diff > 7){
		       $type = 2;
		    }else{
		       $type = 1;
		    }
   
	    }

		$temp_data = $this->general_model->get_row_data('tbl_email_template',array('merchantID'=>$merchID,'templateType'=>$type)) ;

		$data['templatedata'] = $temp_data;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];

		$taxes = $this->qbo_company_model->get_qbo_tax_data($user_id);
		$data['taxes'] = $taxes;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_invoice_details', $data);
		$this->load->view('QBO_views/page_qbo_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function invoice_details_print()
	{
		$invoiceID             =  $this->uri->segment(4);
		if ($invoiceID != "") {
			$invoice = $this->qbo_company_model->get_invoice_details_data($invoiceID);
			if (!empty($invoice))
				$this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'D');
		} else {
			redirect('QBO_controllers/home/index');
		}
	}
	public function invoice_details_printOLD()
	{


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$invoiceID             =  $this->uri->segment(4);

		$data['template'] 		= template_variable();
		$merchID = $user_id;


		$condition2 			= array('invoiceID' => $invoiceID, 'merchantID' => $merchID);
		$invoice_data           = $this->general_model->get_row_data('QBO_test_invoice', $condition2);
		$condition3 			= array('Customer_ListID' => $invoice_data['CustomerListID']);
		$customer_data			= $this->general_model->get_row_data('QBO_custom_customer', $condition3);


		$condition4 			= array('merchID' => $user_id);

		$company_data			= $this->general_model->get_select_data('tbl_merchant_data', array('firstName', 'lastName', 'companyName', 'merchantAddress1', 'merchantEmail', 'merchantContact', 'merchantAddress2', 'merchantCity', 'merchantState', 'merchantCountry', 'merchantZipCode'), $condition4);
		$config_data			= $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID' => $merchID));
		if ($config_data['ProfileImage'] != "")
			$logo = base_url() . LOGOURL . $config_data['ProfileImage'];
		else
			$logo = CZLOGO;
		$data['customer_data']  = $customer_data;
		$data['company_data']    = $company_data;

		$invoice_items  = $this->qbo_company_model->get_invoice_item_data($invoiceID);

		$no   = $invoiceID;
		$payamount = $invoice_data['Total_payment'] - $invoice_data['BalanceRemaining'];


		$pdfFilePath = "$no.pdf";

		ini_set('memory_limit', '320M');
		$this->load->library("TPdf");

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		define(PDF_HEADER_TITLE, 'Invoice');
		define(PDF_HEADER_STRING, '');
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Chargezoom 1.0');
		$pdf->SetTitle($data['template']['title']);

		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


		$pdf->SetFont('dejavusans', '', 10, '', true);
		// set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
		// Add a page

		$pdf->AddPage();


		$y = 20;
		$logo_div = '<div style="text-align:left; float:left ">
		<img src="' . $logo . '"  border="0" />
		</div>';



		// set color for background
		$pdf->SetFillColor(255, 255, 255);
		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);


		$tt = "\n" . $company_data['firstName'] . " " . $company_data['lastName'] . "<br/>" . $company_data['companyName'] . "<br/>" . ($company_data['merchantAddress1']) . "<br/> " . ($company_data['merchantAddress2']) . "<br/>" . ($company_data['merchantCity']) . ", " . ($company_data['merchantState']) . " " . ($company_data['merchantZipCode']) . '<br/>' . ($company_data['merchantCountry']);


		$y = 50;
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		// write the first column
		$pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>" . $tt, 0, 0, 1, true, 'J', true);

		// set color for text
		$pdf->SetTextColor(51, 51, 51);

		$pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice ID: </b>' . $invoice_data['refNumber'] . "<br/><br/>" . '<b>Due Date: </b>' . date("m/d/Y", strtotime($invoice_data['DueDate'])), 0, 1, 1, true, 'J', true);

		$lll = '';

		if ($customer_data['firstName'] != '') {
			$lll .= $customer_data['firstName'] . ' ' . $customer_data['lastName'] . "<br>";
		} else {
			$lll .= $invoice_data['CustomerFullName'] . "<br>";
		}

		$lll .= $customer_data['companyName'] . '<br/>';
		if ($invoice_data['BillAddress_Addr1'] != '') {
			$lll .= $invoice_data['BillAddress_Addr1'] . '<br>' . $invoice_data['BillAddress_Addr2'];
		}

		$lll .= '<br>';
		$lll .= ($invoice_data['BillAddress_City']) ? $invoice_data['BillAddress_City'] . ',' : '--';
		$lll .= ($invoice_data['BillAddress_State']) ? $invoice_data['BillAddress_State '] . ' ' : '--';
		$lll .= ($invoice_data['BillAddress_PostalCode']) ? $invoice_data['BillAddress_PostalCode'] : '--';
		$lll .= '<br>';
		$lll .= ($customer_data['phoneNumber']) ? $customer_data['phoneNumber'] : '--';
		$lll .= '<br>';
		$email = ($customer_data['userEmail']) ? $customer_data['userEmail'] : '#';

		$lll .= '<a href="' . $email . '">' . $email . '</a>';

		$lll_ship = '';
		if ($customer_data['firstName'] != '') {
			$lll_ship .= $customer_data['firstName'] . ' ' . $customer_data['lastName'] . "<br>";
		} else {
			$lll_ship .= $invoice_data['CustomerFullName'] . "<br>";
		}
		$lll_ship .= $customer_data['companyName'] . '<br>';
		if ($invoice_data['ShipAddress_Addr1'] != '') {
			$lll_ship .= $invoice_data['ShipAddress_Addr1'] . '<br>' . $invoice_data['ShipAddress_Addr2'];
		}
		$lll_ship .= '<br>';
		$lll_ship .= ($invoice_data['ShipAddress_City']) ? $invoice_data['ShipAddress_City'] . ',' : '--' . ',';
		$lll_ship .= ($invoice_data['ShipAddress_State']) ? $invoice_data['ShipAddress_State'] . ' ' : '--';
		$lll_ship .= ($invoice_data['ShipAddress_PostalCode']) ? $invoice_data['ShipAddress_PostalCode'] : '--';
		$lll_ship .= '<br>';
		$lll_ship .= ($customer_data['phoneNumber']) ? $customer_data['phoneNumber'] : '--';
		$lll_ship .= '<br>';

		$email = ($customer_data['userEmail']) ? $customer_data['userEmail'] : '#';

		$lll_ship .= '<a href="' . $email . '">' . $email . '</a>';


		$y = $pdf->getY();
		//$y = 20;
		// write the first column
		$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>" . $lll, 0, 0, 1, true, 'J', true);
		$pdf->SetTextColor(51, 51, 51);
		$pdf->writeHTMLCell(80, 0, '', '', '<b>Shipping/Customer Address</b><br/>' . $lll_ship . "<br/>", 0, 1, 1, true, 'J', true);
		$pdf->setCellPaddings(1, 1, 1, 1);

		// set cell margins
		$pdf->setCellMargins(0, 1, 0, 0);
		$html = "\n\n\n" . '<h3>Items:</h3><table style="border: 1px solid #333333;"  cellpadding="4" >
    <tr style="border: 1px solid #333333;background-color:#3f3f3f;color:#FFFFFF;">
       
        <th style="border: 1px solid black;" colspan="2" align="center"><b>Product/Services</b></th>
        <th style="border: 1px solid black;" align="center"><b>Qty</b></th>
		  <th style="border: 1px solid black;" align="center"><b>Unit Rate</b></th>
		    <th style="border: 1px solid black;" align="center"><b>Amount</b></th>
        
    </tr>';
		$html1 = '';
		$total_val = 0;


		$totaltax = 0;
		$total = 0;
		$tax = 0;
		foreach ($invoice_items as $key => $item) {

			$total +=  $item['itemQty'] * $item['itemPrice'];

			$html .= '<tr>
                        
						<td style="border: 1px solid black;" colspan="2">' . $item['Name'] . '</td>
                        <td style="border: 1px solid black;"  align="center" >' . $item['itemQty '] . '</td>
                        <td style="border: 1px solid black;"  align="right">' . number_format($item['itemPrice'], 2) . '</td>
						<td style="border: 1px solid black;"  align="right">' . number_format($item['itemQty'] * $item['itemPrice'], 2) . '</td>
                    </tr>';
		}
		$total_val = $total + $invoice_data['totalTax'];
		$total_tax = ($invoice_data['totalTax']) ? $invoice_data['totalTax'] : 0;
		$trate = ($invoice_data['taxRate']) ? $invoice_data['taxRate'] : '0.00';
		$html .= '<tr><td colspan="4" style="border: 1px solid black;"   align="right">Tax ' . $trate . '%</td><td style="border: 1px solid black;" align="right">' . number_format($total_tax, 2) . '</td></tr>';
		$html .= '<tr><td colspan="4" style="border: 1px solid black;" align="right" >Subtotal</td><td style="border: 1px solid black;" align="right">' . number_format($total, 2) . '</td></tr>';
		$html .= '<tr><td colspan="4" style="border: 1px solid black;" align="right">Total</td><td style="border: 1px solid black;" align="right">' . number_format($total_val, 2) . '</td></tr>';
		$html .= '<tr><td colspan="4" style="border: 1px solid black;" align="right" >Payment</td><td style="border: 1px solid black;" align="right">' . number_format($payamount, 2) . '</td></tr>';
		$html .= '<tr><td colspan="4"  align="right" >Balance</td><td align="right">' . number_format(($invoice_data['BalanceRemaining']), 2) . '</td></tr>';

		$email1 = ($company_data['merchantEmail']) ? $company_data['merchantEmail'] : '#';


		$html .= '</table>';
		$html .= '</table>';


		$pdf->writeHTML($html, true, false, true, false, '');



		// ---------------------------------------------------------    

		// Close and output PDF document
		// This method has several options, check the source code documentation for more information.
		$pdf->Output($pdfFilePath, 'D');
	}

	public function add_invoice()
	{
		if ($this->session->userdata('logged_in') != "") {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$this->qbo = new QBO_data($user_id); 
		$this->qbo->get_items_data(); 
		$this->qbo->get_customer_data();
		$this->qbo->get_tax_data();

		$condition1 = array('merchantID' => $user_id);
		$condition2 = array('merchantID' => $user_id, 'IsActive' => 'true');
		$data['customers'] = $this->qbo_customer_model->get_customers_data($user_id);
		$data['plans'] = $this->general_model->get_table_data('QBO_test_item', $condition2);

		$conditiontax = array('tax.merchantID' => $user_id, 'IsActive' => 'true');

		$taxes = $this->qbo_company_model->get_qbo_tax_data($user_id);
		$data['taxes'] = $taxes;
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

		$country       =   $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;

		if ($this->uri->segment(4) != "") {
			$customerID = $this->uri->segment(4);
			$data['Invcustomer']  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $user_id));
			$data['cards']        = $this->card_model->get_card_data($customerID);
		}
		
		$condition1 = array('merchantID' => $user_id);
		$condition2 = array('merchantID' => $user_id, 'IsActive' => 'true');
		$data['customers'] = $this->qbo_customer_model->get_customers_data($user_id);
		$data['plans'] = $this->general_model->get_table_data('QBO_test_item', $condition2);

		$conditiontax = array('tax.merchantID' => $user_id, 'IsActive' => 'true');

		$taxes = $this->qbo_company_model->get_qbo_tax_data($user_id);
		$data['taxes'] = $taxes;
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

		$country       =   $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;

		$data['netterms']  = $this->qbo_company_model->get_terms_data($user_id);
		$data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/create_new_invoice_online', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	
	public function edit_custom_invoice()
	{
		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			} else if ($this->session->userdata('user_logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchantID'];
				$merchID = $user_id;
			}
			$total = 0;
			
			$invoiceID = $this->czsecurity->xssCleanPostInput('invNo');
			
			$val = array(
				'merchantID' => $merchID,
			);
			$taxID = $this->czsecurity->xssCleanPostInput('taxes');

			$tx_rate = $totalTax = 0;
			$tx_val = 	$this->general_model->get_select_data('tbl_taxe_code_qbo', array('total_tax_rate'), array('merchantID' => $merchID, 'taxID' => $taxID));
			if (!empty($tx_val))
				$tx_rate =  $tx_val['total_tax_rate'];
			
			$taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {

				$proItemID = $this->czsecurity->xssCleanPostInput('productID')[$key];
					
				$itemFetchdata  = $this->general_model->get_select_data('QBO_test_item', array('Type','ListID'), array('productID' => $proItemID, 'merchantID' => $merchID));

				$insert_row['itemListID'] = $proItemID;
				$insert_row['itemListType'] = $itemFetchdata['Type'];
				$insert_row['itemRowID'] = $itemFetchdata['ListID'];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTax'] = $taxList[$key]; 
				$itemDes = $this->czsecurity->xssCleanPostInput('description')[$key];

				$itemDes = str_replace('<','< ',$itemDes);
				$itemDes = str_replace('>','> ',$itemDes);
				$insert_row['itemDescription'] = $itemDes;
				$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];
				if($insert_row['itemTax']){
					$totalTax += ($insert_row['itemTotal'] * $tx_rate)/100;
				}
				
				
				$item_val[$key] = $insert_row;
			}

			$totalTax = round($totalTax, 2);
			$total = round($total + $totalTax, 2);

			
			$inv_date  = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invDate')));
			$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('dueDate')));


			$data = $this->general_model->get_row_data('QBO_token', $val);

			$accessToken = $data['accessToken'];
			$refreshToken = $data['refreshToken'];
			$realmID      = $data['realmID'];

			$dataService = DataService::Configure(array(
				'auth_mode' => $this->config->item('AuthMode'),
				'ClientID'  => $this->config->item('client_id'),
				'ClientSecret' => $this->config->item('client_secret'),
				'accessTokenKey' =>  $accessToken,
				'refreshTokenKey' => $refreshToken,
				'QBORealmID' => $realmID,
				'baseUrl' => $this->config->item('QBOURL'),
			));



			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			$lineArray = array();



			$targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

			$taxval = $this->czsecurity->xssCleanPostInput('taxes');

			if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
				$i = 0;
				
				for ($i = 0; $i < count($item_val); $i++) {

					if($item_val[$i]['itemListType'] == 'Group'){

						$childItems = $this->general_model->get_table_data('qbo_group_lineitem', array('groupListID' => $item_val[$i]['itemRowID']));
						$objLine = [];
						$groupItemTotal = 0;
						if(count($childItems) > 0){
							foreach($childItems as $ci){
								$itemSaleFetchdata  = $this->general_model->get_select_data('QBO_test_item', array('productID'), array('ListID' => $ci['itemListID'], 'merchantID' => $merchID));

								$LineObj = Line::create([
									"Amount" => $ci['Price'],
									"DetailType" => "SalesItemLineDetail",
									"Description" => $ci['FullName'],
									"SalesItemLineDetail" => [
										"ItemRef" => [
											"value" => $itemSaleFetchdata['productID'],
											"name" => $ci['FullName'],
										],
										"Qty" => $ci['Quantity'],
										"UnitPrice" => $ci['Price'],
									],

								]);
								$groupItemTotal += $ci['Price'];
								$objLine[] = $LineObj;
							}
						}
						
							
						$LineObj = Line::create([
							"Amount" => $item_val[$i]['itemTotal'],
							"DetailType" => "GroupLineDetail",
							"Description" => $item_val[$i]['itemDescription'],
							"GroupLineDetail" => [
								"GroupItemRef" => $item_val[$i]['itemListID'],
								"Quantity" => $item_val[$i]['itemQuantity'],
								"Line" => $objLine,
							],


						]);
						
					}else{
						$LineObj = Line::create([
							"Amount" => $item_val[$i]['itemTotal'],
							"DetailType" => "SalesItemLineDetail",
							"Description" => $item_val[$i]['itemDescription'],
							"SalesItemLineDetail" => [
								"ItemRef" => $item_val[$i]['itemListID'],
								"Qty" => $item_val[$i]['itemQuantity'],
								"UnitPrice" => $item_val[$i]['itemRate'],
							],

						]);
					}
					
					$lineArray[] = $LineObj;
				}

				
				if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
					$theInvoice = current($targetInvoiceArray);

					$theResourceObj = Invoice::update($theInvoice, [

						"Balance" => $total,
						"TotalAmt" => $total,

						"Line" => $lineArray,

						"DueDate" =>  $duedate,

						"MetaData" => [
							"CreateTime" => date('Y-m-d', strtotime($inv_date)) . 'T' . date('H:i:s'),
							"LastUpdatedTime" => date('Y-m-d') . 'T' . date('H:i:s'),
						]
					]);


					
				}

				
			} else {
				$theInvoice = current($targetInvoiceArray);
				
				$inv_amnt = 0;

				for ($i = 0; $i < count($item_val); $i++) {
					$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];

					$inv_amnt += $amount;

					if($item_val[$i]['itemListType'] == 'Group'){
							
						$LineObj = Line::create([
							"Amount" => $amount,
							"DetailType" => "GroupLineDetail",
							"Description" => $item_val[$i]['itemDescription'],
							"GroupLineDetail" => [
								"GroupItemRef" => $item_val[$i]['itemListID'],
								"Quantity" => $item_val[$i]['itemQuantity'],
								
							],


						]);
					}else{
						$LineObj = Line::create([
							"Amount" => $amount,
							"DetailType" => "SalesItemLineDetail",
							"Description" => $item_val[$i]['itemDescription'],
							"SalesItemLineDetail" => [
								"ItemRef" => $item_val[$i]['itemListID'],
								"Qty" => $item_val[$i]['itemQuantity'],
								"UnitPrice" => $item_val[$i]['itemRate'],
								"TaxCodeRef" => [
									"value" => (isset($item_val[$i]['itemTax']) && $item_val[$i]['itemTax']) ? "TAX" : "NON",
								]

							],

						]);
					}
					
					$lineArray[] = $LineObj;
				}

				$inv_amnt = 0;
				$theResourceObj = Invoice::update($theInvoice, [

					"TxnTaxDetail" => [
						"TxnTaxCodeRef" => [
							"value" => $taxval
						],
					],
					"Balance" => $total,
					"TotalAmt" => $total,

					"Line" => $lineArray,
					"sparse" => true,
					"DueDate" =>  $duedate,
					"MetaData" => [
						"CreateTime" => date('Y-m-d', strtotime($inv_date)) . 'T' . date('H:i:s'),
						"LastUpdatedTime" => date('Y-m-d') . 'T' . date('H:i:s'),
					]

				]);
			}

			

			

			$resultingObj = $dataService->Update($theResourceObj);

			$error = $dataService->getLastError();
			
			$error1 = '';
			if ($error != null) {


				$error1 .=  $error->getResponseBody() . "\n";

				$st = '0';
				$action = 'Add Invoice';
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: ' . $error1 . '  </strong></div>');


			} else {

				$st = '1';
				$error1 = 'success';
				$action = 'Update Invoice';
			}

			$QBO_invoice_details['DueDate'] = $duedate;
			$QBO_invoice_details['BalanceRemaining'] = $total;
			$QBO_invoice_details['Total_payment']  = $total;
			$QBO_invoice_details['IsPaid']         = 0;
			$QBO_invoice_details['totalTax'] = $totalTax;

			$QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s', strtotime($this->czsecurity->xssCleanPostInput('invDate')));
			$QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s');

			$this->general_model->update_row_data('QBO_test_invoice', array('invoiceID' => $invoiceID, 'merchantID' => $merchID), $QBO_invoice_details);




			$this->general_model->delete_row_data('tbl_qbo_invoice_item', array('invoiceID' => $invoiceID, 'merchantID' => $merchID, 'releamID' => $realmID));
			if (!empty($item_val)) {
				foreach ($item_val as $key => $line) {




					$l_data['itemID'] = $key + 1;

					$l_data['itemDescription'] = $line['itemDescription'];
					$l_data['totalAmount'] = $total;

					$l_data['itemRefID'] = $line['itemListID'];

					$l_data['itemPrice'] = $line['itemRate'];

					$l_data['itemQty'] = $line['itemQuantity'];
					$l_data['itemTax'] = $line['itemTax'];

					$l_data['invoiceID'] = $invoiceID;
					$l_data['merchantID'] = $merchID;
					$l_data['releamID'] = $realmID;
					$l_data['createdAt'] = date('Y-m-d H:i:s');


					$this->general_model->insert_row('tbl_qbo_invoice_item', $l_data);
				}
			}





			$qblog_data = array(
				'syncType' => 'CQ','type' => 'invoice',
				'merchantID' => $merchID, 'qbAction' => $action, 'qbStatus' => $st,
				'updatedAt' => date('Y-m-d H:i:s'),
				'createdAt' => date('Y-m-d H:i:s'), 'qbText' => $error1, 'qbActionID' => $invoiceID,
			);

			$syncid = $this->general_model->insert_row('tbl_qbo_log', $qblog_data);
			if($syncid){
                $qbSyncID = 'CQ-'.$syncid;

                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invoiceID.'">'.$invoiceID.'</a></span> ';
                
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
            }
			if ($this->czsecurity->xssCleanPostInput('index') == 'self')
				redirect(base_url('QBO_controllers/Create_invoice/invoice_details_page/' . $invoiceID));
			else
				redirect(base_url('QBO_controllers/Create_invoice/invoice_details'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Invalid Request</div>');

			redirect(base_url('QBO_controllers/Create_invoice/invoice_details'));
		}
	}

	public function invoice_create_old()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			} else if ($this->session->userdata('user_logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchantID'];
				$merchID = $user_id;
			}
			$total = 0;

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$item['itemRefID'] =  $insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$item['itemQty']   =  $insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$item['itemPrice'] =   $insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$item['itemTax']   =   $insert_row['itemTax'] = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
				$item['itemDescription'] =   $insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$item['totalAmount'] =   $insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$item['itemID']    =   $key + 1;
				$item['merchantID'] = $merchID;
				$item['itemID']    =   $key + 1;

				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
				$items[$key] = $item;
			}


			$inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
			if (!empty($inv_data)) {
				$inv_pre   = $inv_data['prefix'];
				$inv_po    = $inv_data['postfix'] + 1;
				$new_inv_no = $inv_pre . $inv_po;
				$Number = $new_inv_no;
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('/QBO_controllers/Create_invoice/add_invoice', 'refresh');
			}



			$val = array(
				'merchantID' => $merchID,
			);

			$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
			$address1 = $this->czsecurity->xssCleanPostInput('address1');
			$address2 = $this->czsecurity->xssCleanPostInput('address2');
			$country = $this->czsecurity->xssCleanPostInput('country');
			$state = $this->czsecurity->xssCleanPostInput('state');
			$city = $this->czsecurity->xssCleanPostInput('city');
			$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
			$phone = $this->czsecurity->xssCleanPostInput('phone');
			$baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
			$baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
			$bcountry = $this->czsecurity->xssCleanPostInput('bcountry');
			$bstate = $this->czsecurity->xssCleanPostInput('bstate');
			$bcity = $this->czsecurity->xssCleanPostInput('bcity');
			$bzipcode = $this->czsecurity->xssCleanPostInput('bzipcode');
			$bphone = $this->czsecurity->xssCleanPostInput('bphone');

			$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
			$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
			if ($pterm != "" && $pterm != 0) {
				$duedate =  date("Y-m-d",  strtotime($inv_date . "+ $pterm day"));
				
			} else {
				$duedate    = date('Y-m-d', strtotime($inv_date));
			}

			
			if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
				$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			} else {
				$paygateway  = 0;
			}
			if ($this->czsecurity->xssCleanPostInput('card_list') != '') {

				$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			} else {

				$cardID = 0;
			}
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			$cs_data  = $this->general_model->get_select_data('QBO_custom_customer', array('fullName', 'companyID', 'companyName', 'userEmail'), array('Customer_ListID' => $customerID));

			if ($this->czsecurity->xssCleanPostInput('autopay')) {

				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$card = array();
					$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
					$card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
					$card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
					$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
					$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
					$card['friendlyname'] = $friendlyName;
					$card['customerID']  = $customerID;
					$card['merchantID']  = $merchID;
					$card['Billing_Addr1']     = $this->czsecurity->xssCleanPostInput('baddress1');
					$card['Billing_Addr2']     = $this->czsecurity->xssCleanPostInput('baddress2');
					$card['Billing_City']     = $this->czsecurity->xssCleanPostInput('bcity');
					$card['Billing_State']     = $this->czsecurity->xssCleanPostInput('bstate');
					$card['Billing_Contact']     = $this->czsecurity->xssCleanPostInput('bphone');
					$card['Billing_Country']     = $this->czsecurity->xssCleanPostInput('bcountry');
					$card['Billing_Zipcode']     = $this->czsecurity->xssCleanPostInput('bzipcode');

					$card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
					if (!empty($card_data)) {
						$this->card_model->update_card($card, $customerID, $friendlyName);
						$cardID  = $card_data['CardID'];
					} else {
						$cardID = $this->card_model->insert_new_card($card);
					}
				}
			}

			$data = $this->general_model->get_row_data('QBO_token', $val);

			$accessToken = $data['accessToken'];
			$refreshToken = $data['refreshToken'];
			$realmID      = $data['realmID'];

			$dataService = DataService::Configure(array(
				'auth_mode' => $this->config->item('AuthMode'),
				'ClientID'  => $this->config->item('client_id'),
				'ClientSecret' => $this->config->item('client_secret'),
				'accessTokenKey' =>  $accessToken,
				'refreshTokenKey' => $refreshToken,
				'QBORealmID' => $realmID,
				'baseUrl' => $this->config->item('QBOURL'),
			));

			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			$lineArray = array();

			if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
				$i = 0;
				for ($i = 0; $i < count($item_val); $i++) {
					$LineObj = Line::create([
						"Amount"      => $item_val[$i]['itemTotal'],
						"DetailType" => "SalesItemLineDetail",
						"Description" => $item_val[$i]['itemDescription'],
						"SalesItemLineDetail" => [
							"ItemRef"  => $item_val[$i]['itemListID'],
							"Qty"      => $item_val[$i]['itemQuantity'],
							"UnitPrice" => $item_val[$i]['itemRate'],
						],

					]);
					$lineArray[] = $LineObj;
				}


				//Add a new Invoice
				$theResourceObj = Invoice::create([
					"CustomerRef" => $customer_ref,
					"DocNumber" => $Number,
					"BillAddr" => [
						"City" =>  $bcity,
						"Line1" =>  $baddress1,
						"Line2" =>  $baddress2,
						"CountrySubDivisionCode" =>  $bstate,
						"Country" =>  $bcountry,
						"PostalCode" =>  $bzipcode
					],
					"ShipAddr" => [
						"Line1" =>  $address1,
						"Line2" =>  $address2,
						"City" =>  $city,
						"Country" =>  $country,
						"PostalCode" =>  $zipcode
					],


					"TotalAmt" => $total,

					"Line" => $lineArray,

					"DueDate" =>  $duedate,



				]);
			} else {
				$taxval = $this->czsecurity->xssCleanPostInput('taxes');

				$inv_amnt = 0;
				for ($i = 0; $i < count($item_val); $i++) {
					$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];

					$inv_amnt += $amount;


					$LineObj = Line::create([
						"Amount" => $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'],
						"DetailType" => "SalesItemLineDetail",
						"Description" => $item_val[$i]['itemDescription'],
						"SalesItemLineDetail" => [
							"ItemRef" => $item_val[$i]['itemListID'],
							"Qty" => $item_val[$i]['itemQuantity'],
							"UnitPrice" => $item_val[$i]['itemRate'],
							"TaxCodeRef" => [
								"value" => "TAX"
							]
						],

					]);
					$lineArray[] = $LineObj;
				}




				$theResourceObj = Invoice::create([
					"CustomerRef" => $customer_ref,
					"DocNumber" => $Number,
					"BillAddr" => [
						"City" =>  $bcity,
						"Line1" =>  $baddress1,
						"Line2" =>  $baddress2,
						"CountrySubDivisionCode" =>  $bstate,
						"Country" =>  $bcountry,
						"PostalCode" =>  $bzipcode
					],
					"ShipAddr" => [
						"Line1" =>  $address1,
						"Line2" =>  $address2,
						"City" =>  $city,

						"Country" =>  $country,
						"PostalCode" =>  $zipcode
					],


					"TxnTaxDetail" => [
						"TxnTaxCodeRef" => [
							"value" => $taxval
						],
					],



					"TotalAmt" => $total,

					"Line" => $lineArray,

					"DueDate" =>  $duedate,

				]);
			}


			$resultingObj = $dataService->Add($theResourceObj);

			$error = $dataService->getLastError();



			$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $inv_po));

			$QBO_invoice_details['refNumber'] = $Number;
			$QBO_invoice_details['ShipAddress_Addr1'] = $address1;
			$QBO_invoice_details['ShipAddress_Addr2'] = $address2;
			$QBO_invoice_details['ShipAddress_City']  = $city;
			$QBO_invoice_details['ShipAddress_Country']    = $country;
			$QBO_invoice_details['ShipAddress_PostalCode'] = $zipcode;
			$QBO_invoice_details['ShipAddress_State']      = $state;

			$QBO_invoice_details['BillAddress_Addr1'] = $baddress1;
			$QBO_invoice_details['BillAddress_Addr2'] = $baddress2;
			$QBO_invoice_details['BillAddress_City']  = $bcity;
			$QBO_invoice_details['BillAddress_Country']    = $bcountry;
			$QBO_invoice_details['BillAddress_PostalCode'] = $bzipcode;
			$QBO_invoice_details['BillAddress_State']      = $bstate;


			$QBO_invoice_details['DueDate'] = $duedate;
			$QBO_invoice_details['BalanceRemaining'] = $total;
			$QBO_invoice_details['Total_payment']  = $total;
			$QBO_invoice_details['CustomerListID'] = $customer_ref;

			$QBO_invoice_details['IsPaid']         = 0;


			$QBO_invoice_details['merchantID']  = $merchID;
			$QBO_invoice_details['companyID']   = $realmID;
			$QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s');
			$QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s');


			$error1 = '';
			if ($error != null) {

				$error1 .=  $error->getResponseBody() . "\n";

				$st = '0';
				$error1 = 'success';
				$action = 'Add Invoice';
				$QBO_invoice_details['UserStatus'] = 1;
				$invID = mt_rand(6000000, 9000000);
				$QBO_invoice_details['invoiceID'] = $invID;
				$this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
				foreach ($items as $itm) {
					$itm['releamID'] = $realmID;
					$itm['invoiceID'] = $invID;
					$itm['createdAt'] = date('Y-m-d H:i:s');
					$this->general_model->insert_row('tbl_qbo_invoice_item', $itm);
				}

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $error1 . '  </strong></div>');
			} else {
				$invID =  $resultingObj->Id;
				$QBO_invoice_details['invoiceID'] = $invID;
				$this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);

				foreach ($items as $itm) {
					$itm['releamID'] = $realmID;
					$itm['invoiceID'] = $invID;
					$itm['createdAt'] = date('Y-m-d H:i:s');
					$this->general_model->insert_row('tbl_qbo_invoice_item', $itm);
				}

				$st = '1';
				$error1 = 'success';
				$action = 'Add Invoice';
			}
			$inv_no = $Number;
			$invoiceID = $invID;
			$invoice  = $this->qbo_company_model->get_invoice_details_data($invoiceID);
			$this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');

			if ($invID) {

				$schedule['invoiceID'] = $invID;
				$schedule['gatewayID'] =  $paygateway;
				$schedule['cardID'] =  $cardID;
				$schedule['customerID'] =  $customerID;

				if ($this->czsecurity->xssCleanPostInput('autopay')) {
					$schedule['autoPay'] =  1;
					$schedule['paymentMethod'] =   1;
				} else {
					$schedule['autoPay'] =  1;
					$schedule['paymentMethod']  =   2;
				}
				$schedule['merchantID']     =  $merchID;
				$schedule['createdAt']    =  date('Y-m-d H:i:s');
				$schedule['updatedAt']  =  date('Y-m-d H:i:s');
				$this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
				if ($chh_mail == '1') {
					$condition_mail         = array('templateType' => '1', 'merchantID' => $user_id);

					$toEmail = $cs_data['userEmail'];
					$company = $cs_data['companyName'];
					$customer = $cs_data['fullName'];
					$invoice_due_date = $duedate;
					$invoicebalance = $total;
					$invoiceID = $invID;
					$tr_date   = date('Y-m-d H:i:s');
					$this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $Number);
				}


				$this->session->set_flashdata('success', 'Success');
			}

			$qblog_data = array(
				'syncType' => 'CQ','type' => 'invoice',
				'merchantID' => $merchID, 'qbAction' => $action, 'qbStatus' => $st,
				'updatedAt' => date('Y-m-d H:i:s'),
				'createdAt' => date('Y-m-d H:i:s'), 'qbText' => $error1, 'qbActionID' => $invID,
			);
			$syncid = $this->general_model->insert_row('tbl_qbo_log', $qblog_data);
			if($syncid){
                $qbSyncID = 'CQ-'.$syncid;
                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invID.'">'.$invID.'</a></span> ';
                
                
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
            }
			redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		}
	}

	public function invoice_create()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in') != "") {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			} else if ($this->session->userdata('user_logged_in') != "") {
				$user_id = $this->session->userdata('user_logged_in')['merchantID'];
				$merchID = $user_id;
			}

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('invdate', 'Invoice Date', 'required|xss_clean');


			if ($this->form_validation->run() == true) {



				$total = 0;
				$taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');

				foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
					$proItemID = $this->czsecurity->xssCleanPostInput('productID')[$key];
					
					$itemFetchdata  = $this->general_model->get_select_data('QBO_test_item', array('Type','ListID'), array('productID' => $proItemID, 'merchantID' => $merchID));

					$insert_row['itemListID'] = $proItemID;
					$insert_row['itemListType'] = $itemFetchdata['Type'];
					$insert_row['itemRowID'] = $itemFetchdata['ListID'];
					$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
					$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
					$insert_row['itemTax'] = $taxList[$key]; 
					$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
					$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
					$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
					$item_val[$key] = $insert_row;
					
				}

				if ($this->czsecurity->xssCleanPostInput('setMail'))
					$chh_mail = 1;
				else
					$chh_mail = 0;

				$inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
				if (!empty($inv_data)) {
					$inv_pre   = $inv_data['prefix'];
					$inv_po    = $inv_data['postfix'] + 1;
					$new_inv_no = $inv_pre . $inv_po;
					$Number = $new_inv_no;
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

					redirect('/QBO_controllers/Create_invoice/add_invoice', 'refresh');
				}



				$val = array(
					'merchantID' => $merchID,
				);

				$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
				$address1 = $this->czsecurity->xssCleanPostInput('address1');
				$address2 = $this->czsecurity->xssCleanPostInput('address2');
				$country = $this->czsecurity->xssCleanPostInput('country');
				$state = $this->czsecurity->xssCleanPostInput('state');
				$city = $this->czsecurity->xssCleanPostInput('city');
				$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
				$phone = $this->czsecurity->xssCleanPostInput('phone');
				$baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
				$baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
				$bcountry = $this->czsecurity->xssCleanPostInput('bcountry');
				$bstate = $this->czsecurity->xssCleanPostInput('bstate');
				$bcity = $this->czsecurity->xssCleanPostInput('bcity');
				$bzipcode = $this->czsecurity->xssCleanPostInput('bzipcode');
				$bphone = $this->czsecurity->xssCleanPostInput('bphone');

				$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
				$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
				if ($pterm != "" && $pterm != 0) {
					$duedate =  date("Y-m-d",  strtotime($inv_date . "+ $pterm day"));
					
				} else {
					$duedate    = date('Y-m-d', strtotime($inv_date));
				}

				
				//card-details
				if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
					$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
				} else {
					$paygateway  = 0;
				}
				if ($this->czsecurity->xssCleanPostInput('card_list') != '') {
					$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
				} else {
					$cardID = 0;
				}
				
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$cs_data  = $this->general_model->get_select_data('QBO_custom_customer', array('fullName', 'companyID', 'companyName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
				$fullname = $cs_data['fullName'];
				$companyID = $cs_data['companyID'];

				

				$data = $this->general_model->get_row_data('QBO_token', $val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				$realmID      = $data['realmID'];

				$dataService = DataService::Configure(array(
					'auth_mode' => $this->config->item('AuthMode'),
					'ClientID'  => $this->config->item('client_id'),
					'ClientSecret' => $this->config->item('client_secret'),
					'accessTokenKey' =>  $accessToken,
					'refreshTokenKey' => $refreshToken,
					'QBORealmID' => $realmID,
					'baseUrl' => $this->config->item('QBOURL'),
				));

				$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
				$lineArray = array();

				if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
					$i = 0;

					for ($i = 0; $i < count($item_val); $i++) {
						
						/* Check product type is group or not */
						if($item_val[$i]['itemListType'] == 'Group'){
							
							$LineObj = Line::create([
								"Amount" => $item_val[$i]['itemTotal'],
								"DetailType" => "GroupLineDetail",
								"Description" => $item_val[$i]['itemDescription'],
								"GroupLineDetail" => [
									"GroupItemRef" => $item_val[$i]['itemListID'],
									"Quantity" => $item_val[$i]['itemQuantity'],
									
								],


							]);
						}else{
							$LineObj = Line::create([
								"Amount" => $item_val[$i]['itemTotal'],
								"DetailType" => "SalesItemLineDetail",
								"Description" => $item_val[$i]['itemDescription'],
								"SalesItemLineDetail" => [
									"ItemRef" => $item_val[$i]['itemListID'],
									"Qty" => $item_val[$i]['itemQuantity'],
									"UnitPrice" => $item_val[$i]['itemRate'],
								],

							]);
						}
						
						$lineArray[] = $LineObj;
					}


					//Add a new Invoice
					$theResourceObj = Invoice::create([
						"CustomerRef" => $customer_ref,
						"DocNumber" => $Number,
						"BillAddr" => [
							"City" =>  $bcity,
							"Line1" =>  $baddress1,
							"Line2" =>  $baddress2,
							"CountrySubDivisionCode" =>  $bstate,
							"Country" =>  $bcountry,
							"PostalCode" =>  $bzipcode
						],
						"ShipAddr" => [
							"Line1" =>  $address1,
							"Line2" =>  $address2,
							"City" =>  $city,
							"Country" =>  $country,
							"PostalCode" =>  $zipcode
						],


						"TotalAmt" => $total,

						"Line" => $lineArray,

						"DueDate" =>  $duedate,



					]);
					
				} else {



					$taxval = $this->czsecurity->xssCleanPostInput('taxes');

					$inv_amnt = 0;
					for ($i = 0; $i < count($item_val); $i++) {
						$amount = $item_val[$i]['itemRate'] * $item_val[$i]['itemQuantity'];

						$inv_amnt += $amount;

						/* Check product type is group or not */
						if($item_val[$i]['itemListType'] == 'Group'){
							
							$LineObj = Line::create([
								"Amount" => $amount,
								"DetailType" => "GroupLineDetail",
								"Description" => $item_val[$i]['itemDescription'],
								"GroupLineDetail" => [
									"GroupItemRef" => $item_val[$i]['itemListID'],
									"Quantity" => $item_val[$i]['itemQuantity'],
									
								],
							]);
						}else{
							$LineObj = Line::create([
								"Amount" => $amount,
								"DetailType" => "SalesItemLineDetail",
								"Description" => $item_val[$i]['itemDescription'],
								"SalesItemLineDetail" => [
									"ItemRef" => $item_val[$i]['itemListID'],
									"Qty" => $item_val[$i]['itemQuantity'],
									"UnitPrice" => $item_val[$i]['itemRate'],
									"TaxCodeRef" => [
										"value" => (isset($item_val[$i]['itemTax']) && $item_val[$i]['itemTax']) ? "TAX" : "NON",
									]

								],

							]);
						}

						$lineArray[] = $LineObj;
					}




					$theResourceObj = Invoice::create([
						"CustomerRef" => $customer_ref,
						"DocNumber" => $Number,
						"BillAddr" => [
							"City" =>  $bcity,
							"Line1" =>  $baddress1,
							"Line2" =>  $baddress2,
							"CountrySubDivisionCode" =>  $bstate,
							"Country" =>  $bcountry,
							"PostalCode" =>  $bzipcode
						],
						"ShipAddr" => [
							"Line1" =>  $address1,
							"Line2" =>  $address2,
							"City" =>  $city,
							"Country" =>  $country,
							"PostalCode" =>  $zipcode
						],




						"TxnTaxDetail" => [
							"TxnTaxCodeRef" => [
								"value" => $taxval
							],
						],

						"TotalAmt" => $total,

						"Line" => $lineArray,

						"DueDate" =>  $duedate,

					]);
				}


				$resultingObj = $dataService->Add($theResourceObj);
				
				$error = $dataService->getLastError();

				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $inv_po));

				$QBO_invoice_details['refNumber'] = $Number;
				$QBO_invoice_details['ShipAddress_Addr1'] = $address1;
				$QBO_invoice_details['ShipAddress_Addr2'] = $address2;
				$QBO_invoice_details['ShipAddress_City']  = $city;
				$QBO_invoice_details['ShipAddress_Country']    = $country;
				$QBO_invoice_details['ShipAddress_PostalCode'] = $zipcode;
				$QBO_invoice_details['ShipAddress_State']      = $state;

				$QBO_invoice_details['BillAddress_Addr1'] = $baddress1;
				$QBO_invoice_details['BillAddress_Addr2'] = $baddress2;
				$QBO_invoice_details['BillAddress_City']  = $bcity;
				$QBO_invoice_details['BillAddress_Country']    = $bcountry;
				$QBO_invoice_details['BillAddress_PostalCode'] = $bzipcode;
				$QBO_invoice_details['BillAddress_State']      = $bstate;


				$QBO_invoice_details['DueDate'] = $duedate;
				$QBO_invoice_details['BalanceRemaining'] = $total;
				$QBO_invoice_details['Total_payment']  = $total;
				$QBO_invoice_details['CustomerListID'] = $customer_ref;
				if($total > 0){
					$IsPaid = 0;
				}else{
					$IsPaid = 1;
				}
				$QBO_invoice_details['IsPaid']         = $IsPaid;

				$QBO_invoice_details['merchantID']  = $merchID;
				$QBO_invoice_details['companyID']   = $realmID;
				$QBO_invoice_details['TimeCreated'] = date('Y-m-d H:i:s');
				$QBO_invoice_details['TimeModified'] = date('Y-m-d H:i:s');

				$error1 = '';
				if ($error != null) {

					$error1 = 'Error: Can not connect to QuickBooks';
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error1 . '  </strong></div>');

					$st = '0';
					$action = 'Add Invoice';
					$QBO_invoice_details['UserStatus'] = 1;
					$invID = mt_rand(6000000, 9000000);
					$QBO_invoice_details['invoiceID'] = $invID;
					
					$error1 =  $error->getResponseBody();
				} else {
					$invID =  $resultingObj->Id;
					$st = '1';
					$QBO_invoice_details['invoiceID'] = $invID;
					$error1 = 'success';
					$action = 'Add Invoice';
				}

				$this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);

				if ($invID) {
					
					$schedule['invoiceID'] = $invID;
					$schedule['gatewayID'] =  $paygateway;
					$schedule['cardID'] =  $cardID;
					$schedule['customerID'] =  $customerID;

					if ($this->czsecurity->xssCleanPostInput('autopay')) {
						$schedule['autoPay'] =  1;
						$schedule['paymentMethod'] =   1;
					} else {
						$schedule['autoPay'] =  0;
						$schedule['paymentMethod']  =   0;
					}
					$schedule['merchantID']     =  $merchID;
					$schedule['createdAt']    =  date('Y-m-d H:i:s');
					$schedule['updatedAt']  =  date('Y-m-d H:i:s');
					$this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);

					if ($chh_mail == '1') {
						$inv_no = $QBO_invoice_details['refNumber'];
						$invoiceID = $QBO_invoice_details['invoiceID'];
						$invoice  = $this->qbo_company_model->get_invoice_details_data($invoiceID);
						$item_arr = [];
						foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
							$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
							$insert_row['itemName'] = $this->czsecurity->xssCleanPostInput('plan_name')[$key];
							$insert_row['Quantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
							$insert_row['itemPrice']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
							$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
							$insert_row['itemTax'] = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
							$insert_row['Description'] = $this->czsecurity->xssCleanPostInput('description')[$key];
							$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
							$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
							$item_arr[$key] = $insert_row;
						}
						$this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $item_arr, 'F');
						$condition_mail         = array('templateType' => '1', 'merchantID' => $user_id);

						$toEmail = $cs_data['userEmail'];
						$company = $cs_data['companyName'];
						$customer = $cs_data['fullName'];
						$invoice_due_date = $duedate;
						$invoicebalance = $QBO_invoice_details['BalanceRemaining'];
						$tr_date   = date('Y-m-d H:i:s');
						$this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
					}
					

					$this->session->set_flashdata('success', 'Success');
				}

				$qblog_data = array(
					'syncType' => 'CQ','type' => 'invoice',
					'merchantID' => $merchID, 'qbAction' => $action, 'qbStatus' => $st,
					'updatedAt' => date('Y-m-d H:i:s'),
					'createdAt' => date('Y-m-d H:i:s'), 'qbText' => $error1, 'qbActionID' => $invID,
				);
				$syncid = $this->general_model->insert_row('tbl_qbo_log', $qblog_data);

				if($syncid){
	                $qbSyncID = 'CQ-'.$syncid;

	                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$invID.'">'.$Number.'</a></span> ';
	                
	                
	                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
	            }

				if($st == 1){
					redirect(base_url("QBO_controllers/Create_invoice/invoice_details_page/$invID"));
				} else {
					redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
				}
				

			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error</strong></div>');
			redirect(base_url('QBO_controllers/Create_invoice/add_invoice'));
		}
	}
	public function delete_invoice()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		}

		$val = array(
			'merchantID' => $merchID,
		);

		$inv_id = $this->czsecurity->xssCleanPostInput('invID');

		$condition = array("invoiceID" => $inv_id);

		$data = $this->general_model->get_row_data('QBO_token', $val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];

		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' => $this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));

		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

		$getInvoices = $dataService->Query("SELECT * FROM Invoice WHERE id = '$inv_id'");
		
		$error = $dataService->getLastError();
		if ($error != null) {
			
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>.</div>');

			redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		} else {
			 

			$this->session->set_flashdata('success', 'Successfully Deleted Invoice');

			redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
		}
	}

	public function get_item_data()
	{

		if ($this->session->userdata('logged_in') != "") {
			$user_id = $this->session->userdata('logged_in')['merchID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
		}


		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('QBO_test_item', array('productID' => $itemID, 'merchantID' => $user_id));

			// Traverse Items
			
			if($itemdata['Type'] == 'Group') {
				$childItems = $this->general_model->get_table_data('qbo_group_lineitem', array('groupListID' => $itemdata['ListID']));
				$price2 = 0;
				foreach($childItems as $ci){
					$price2 += (float)$ci['Price'];
				}

				$itemdata['saleCost'] = (float)$price2;
			}
			
			
			echo json_encode($itemdata);
			die;
		}
		return false;
	}




	function safe_encode($string)
	{
		return strtr(base64_encode($string), '+/=', '-_-');
	}
}
