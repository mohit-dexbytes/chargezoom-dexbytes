<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
require APPPATH .'libraries/qbo/QBO_data.php';
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
class Home extends CI_Controller
{
    protected $loginDetails;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('QBO_models/qbo_company_model');
        $this->load->model('card_model');
        $this->load->model('Queue_Model', 'queue_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $merchID = $this->session->userdata('logged_in')['merchID'];
			$this->resellerID = $this->session->userdata('logged_in')['resellerID'];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->loginDetails = get_names();
        $this->merchantID = $merchID;
        $this->qboOBJ = new QBO_data($merchID); 
    }

    public function index()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        if (isset($user_id)) {
            $where = ['merchID' => $user_id, 'merchantPasswordNew' => null];

            $newPasswordNotFound = $this->general_model->get_row_data('tbl_merchant_data', $where);

            if ($newPasswordNotFound) {
                $dateDiff = time() - strtotime(getenv('PASSWORD_EXP_DATE'));

                $data['passwordExpDays'] = abs(round($dateDiff / (60 * 60 * 24)));
            }
        }
        
        $today      = date('Y-m-d');
        $condition2 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "inv.merchantID" => $user_id);
        $condition  = array("inv.merchantID" => $user_id, 'cust.merchantID' => $user_id, 'trans.merchantID' => $user_id);
        $month = date("M-Y");
        $data['recent_pay']      = $this->qbo_company_model->get_recent_volume_dashboard($user_id,$month);
        $data['card_payment'] = $this->qbo_company_model->get_creditcard_payment($user_id,$month);
      
        $data['eCheck_payment'] = $this->qbo_company_model->get_eCheck_payment($user_id,$month);
        $data['outstanding_total'] = $this->qbo_company_model->get_outstanding_payment($user_id);


        $data['recent_paid']    = $this->qbo_company_model->get_recent_transaction_data($user_id);
        $data['oldest_invs']   = $this->qbo_company_model->get_oldest_due($user_id);
        $plantype              = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']      = $plantype;
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
		$data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype_vt'] = 0;
        $merchData = $this->general_model->get_select_data('tbl_merchant_data',array('rhgraphOption','rhgraphFromDate','rhgraphToDate'), array('merchID' => $user_id));
        
        $data['rhgraphOption'] = isset($merchData['rhgraphOption'])?$merchData['rhgraphOption']:0;
        $data['rhgraphFromDate'] = isset($merchData['rhgraphFromDate'])?$merchData['rhgraphFromDate']:'';
        $data['rhgraphToDate'] = isset($merchData['rhgraphToDate'])?$merchData['rhgraphToDate']:'';
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/index', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

// Dummy index start here

    public function index_new()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
       
        $today      = date('Y-m-d');
        $condition2 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "inv.merchantID" => $user_id);
        $condition  = array("inv.merchantID" => $user_id, 'cust.merchantID' => $user_id, 'trans.merchantID' => $user_id);

        $data['recent_pay']    = $this->qbo_company_model->get_company_invoice_data_payment($user_id);
        $data['today_invoice'] = $this->qbo_company_model->get_process_trans_count($user_id);
        $data['upcoming_inv']  = $this->qbo_customer_model->get_invoice_details($user_id)->schedule;
        $data['failed_inv']    = $this->qbo_company_model->get_invoice_data_count_failed($user_id);
        $data['recent_paid']   = $this->qbo_company_model->get_paid_invoice_recent($condition);
        $data['oldest_invs']   = $this->qbo_company_model->get_oldest_due($user_id);
        $plantype              = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']      = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/index_new', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function invoice_schedule()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $val = array(
            'merchantID' => $merchID,
        );

        $duedate     = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));
        $sch_gateway = $this->czsecurity->xssCleanPostInput('sch_gateway');

        $Inv_id     = $this->czsecurity->xssCleanPostInput('scheduleID');
        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');
        $sch_amount = $this->czsecurity->xssCleanPostInput('schAmount');
        $schCardID  = $this->czsecurity->xssCleanPostInput('schCardID');
        $inv_data   = $this->general_model->get_row_data('QBO_test_invoice', array('invoiceID' => $Inv_id, 'merchantID' => $merchID, 'isPaid' => '0'));

        $update_data = array('scheduleDate' => $duedate, 'invoiceID' => $Inv_id, 'scheduleAmount' => $sch_amount, 'isProcessed' => 0 , 'cardID' => $schCardID, 'paymentMethod' => $sch_method, 'gatewayID' => $sch_gateway, 'updatedAt' => date('Y-m-d H:i:s'));
        if (!empty($inv_data)) {

            $customerID = $inv_data['CustomerListID'];
            $c_data     = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'userEmail', 'fullName'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
            $companyID  = $c_data['companyID'];
            if ($schCardID == "new1") {

                $address1 = $this->czsecurity->xssCleanPostInput('address1');
                $address2 = $this->czsecurity->xssCleanPostInput('address2');
                $city     = $this->czsecurity->xssCleanPostInput('city');
                $country  = $this->czsecurity->xssCleanPostInput('country');
                $phone    = $this->czsecurity->xssCleanPostInput('contact');
                $state    = $this->czsecurity->xssCleanPostInput('state');
                $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');
                if ($sch_method == 1) {
                    $card_no   = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth  = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear    = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv       = $this->czsecurity->xssCleanPostInput('cvv');
                    $card_data = array('cardMonth' => $expmonth,
                        'cardYear'                     => $exyear,
                        'CardType'                     => $card_type,
                        'CustomerCard'                 => $card_no,
                        'CardCVV'                      => $cvv,
                        'customerListID'               => $customerID,
                        'companyID'                    => $companyID,
                        'merchantID'                   => $merchID,

                        'createdAt'                    => date("Y-m-d H:i:s"),
                        'Billing_Addr1'                => $address1,
                        'Billing_Addr2'                => $address2,
                        'Billing_City'                 => $city,
                        'Billing_State'                => $state,
                        'Billing_Country'              => $country,
                        'Billing_Contact'              => $phone,
                        'Billing_Zipcode'              => $zipcode,
                    );
                    $schCardID             = $this->card_model->process_card($card_data);
                    $update_data['cardID'] = $schCardID;
                }
                if ($sch_method == 2) {
                    $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode          = $this->czsecurity->xssCleanPostInput('secCode');
                    $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    $type         = 'Echeck';
                    $friendlyname = $type . ' - ' . substr($acc_number, -4);
                    $card_data    = array('CardType' => $card_type,
                        'accountNumber'                  => $acc_number,
                        'routeNumber'                    => $route_number,
                        'accountName'                    => $acc_name,
                        'accountType'                    => $acct_type,
                        'accountHolderType'              => $acct_holder_type,
                        'secCodeEntryMethod'             => $secCode,
                        'customerListID'                 => $customerID,
                        'companyID'                      => $companyID,
                        'merchantID'                     => $merchID,
                        'customerCardfriendlyName'       => $friendlyname,

                        'createdAt'                      => date("Y-m-d H:i:s"),
                        'Billing_Addr1'                  => $address1,
                        'Billing_Addr2'                  => $address2,
                        'Billing_City'                   => $city,
                        'Billing_State'                  => $state,
                        'Billing_Country'                => $country,
                        'Billing_Contact'                => $phone,
                        'Billing_Zipcode'                => $zipcode,
                    );
                    $schCardID             = $this->card_model->insert_card_data($card_data);
                    $update_data['cardID'] = $schCardID;

                }

            } else {
                $update_data['cardID'] = $schCardID;

            }

            $sch_data = $this->general_model->get_row_data('tbl_scheduled_invoice_payment', array('invoiceID' => $Inv_id, 'merchantID' => $merchID));
            if (!empty($sch_data)) {
                $update_data['autoPay']    = 1;
                $this->general_model->update_row_data('tbl_scheduled_invoice_payment', array('scheduleID' => $sch_data['scheduleID']), $update_data);
            } else {
                $update_data['merchantID'] = $merchID;
                $update_data['customerID'] = $inv_data['CustomerListID'];
                $update_data['updatedAt']  = date('Y-m-d H:i:s');
                $update_data['createdAt']  = date('Y-m-d H:i:s');
                $update_data['autoPay']    = 1;
                $this->general_model->insert_row('tbl_scheduled_invoice_payment', $update_data);

            }

            

            $this->session->set_flashdata('success', 'Successfully Scheduled Payment');

            $res = array('status' => "success");
            echo json_encode($res);
            die;

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

        }
        return false;

    }

    public function delete_invoice()
    {
        if ($this->session->userdata('logged_in')) {
            $da      = $this->session->userdata('logged_in');
            $merchID = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da      = $this->session->userdata('user_logged_in');
            $merchID = $da['merchantID'];
        }

        $val = array(
            'merchantID' => $merchID,
        );

        $inv_id    = $this->czsecurity->xssCleanPostInput('invID');
        $condition = array("invoiceID" => $inv_id);

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),
        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$inv_id'");


        if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
            $theInvoice = current($targetInvoiceArray);

            $this->general_model->update_row_data('QBO_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID), array('userStatus' => '1'));
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
        }

        $currentResultObj = $dataService->Void($theInvoice);
        $st               = "1";
        $action           = 'Void Invoice';
        $msg              = "Success";
        $error            = $dataService->getLastError();
        if ($error != null) {
           
            $st     = "1";
            $action = 'Void Invoice';
            $msg    = $error->getResponseBody();
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
        } else {
           
            $this->session->set_flashdata('success', 'Successfully Void');

            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));

        }
        $qbo_log = array('syncType' => 'CQ','type' => 'invoice','qbStatus' => $st, 'qbAction' => $action, 'qbActionID' => $inv_id, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
        if($syncid){
            $qbSyncID = 'CQ-'.$syncid;

            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$inv_id.'">'.$inv_id.'</a></span> ';

            
            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
        }
        redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));

    }

    public function delete_customer()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('qbocustID', 'Customer', 'trim|required|xss-clean');
        $this->form_validation->set_rules('st_act', 'Status', 'trim|required|xss-clean');

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $val = array(
            'merchantID' => $merchID,
        );
        if ($this->form_validation->run() == true) {

            $msg = '';
            $cust_id = $this->czsecurity->xssCleanPostInput('qbocustID');

            $condition    = array('Customer_ListID' => $cust_id, 'merchantID' => $merchID);
          
            $customerdata = $this->general_model->get_select_data('tbl_qbo_config', array('fullName'), $condition);

            $fullName = $customerdata['fullName'];

            $st_role = $this->czsecurity->xssCleanPostInput('st_act');
            $data    = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];

            $dataService = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
            if ($st_role == '1') {
                $entities = $dataService->Query(" select * from Customer Where ID='" . $cust_id . "' and Active=FALSE ");
                $error    = $dataService->getLastError();

                if (!empty($entities)) {

                    $theCustomer = reset($entities);

                    $updateCustomer = Customer::update($theCustomer, [
                        'Active' => 'true',

                    ]);

                    $currentResultObj = $dataService->Update($updateCustomer);
                    $st               = "1";
                    $action           = 'Activate Customer';
                    $msg              = "Success";
                    $error            = $dataService->getLastError();

                    $error_msg = '';
                    if ($error != null) {
                        $error_msg .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                        $error_msg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                        $error_msg .= "The Response message is: " . $error->getResponseBody() . "\n";
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong>' . $error_msg . '</div>');
                       

                        redirect(base_url('QBO_controllers/Customer_Details/customer_details'));
                    } else {
                        $up_data = array('customerStatus' => 'false', 'updatedAt' => date('Y-m-d H:i:s'));
                        $this->general_model->update_row_data('QBO_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $cust_id), $up_data);

                        $this->session->set_flashdata('success', 'Successfully Activated');

                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Error: Customer not found in QuickBooks</strong></div>');

                }

            }

            if ($st_role == '0') {
                $entities = $dataService->Query(" select * from Customer Where Id='" . $cust_id . "' ");
                $error    = $dataService->getLastError();
              
                if (!empty($entities)) {

                    $theCustomer = reset($entities);

                    $updateCustomer = Customer::update($theCustomer, [
                        'Active' => 'false',

                    ]);

                    $currentResultObj = $dataService->Update($updateCustomer);
                    $st               = "0";
                    $action           = 'Deactivate Customer';
                    $msg              = "Success";
                    $error            = $dataService->getLastError();

                    $error_msg = '';
                    if ($error != null) {
                        $error_msg .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                        $error_msg .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                        $error_msg .= "The Response message is: " . $error->getResponseBody() . "\n";
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong>' . $error_msg . '</div>');
                      
                        $st     = "0";
                        $action = 'Activate Customer';
                        $msg    = $error->getResponseBody();
                        redirect(base_url('QBO_controllers/Customer_Details/customer_details'));
                    } else {
                        $up_data = array('customerStatus' => 'false', 'updatedAt' => date('Y-m-d H:i:s'));
                        $this->general_model->update_row_data('QBO_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $cust_id), $up_data);

                        $this->session->set_flashdata('success', 'Successfully Deactivated');

                        $st     = "1";
                        $action = 'Deactivate Customer';
                        $msg    = "Success";

                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Error: Customer not found in QuickBooks</strong></div>');

                }

            }

            $qbo_log = array('syncType' => 'CQ','type' => 'customer','qbStatus' => $st, 'qbAction' => $action, 'qbActionID' => $cust_id, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
            if($syncid){
                $qbSyncID = 'CQ-'.$syncid;

                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/home/view_customer/'.$cust_id.'">'.$fullName.'</a></span> ';

                
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
            }
        } else {

            if (form_error('st_act')) {
                $error .= form_error('st_act');
            }

            if (form_error('qbocustID')) {
                $error .= form_error('qbocustID');
            }

            $this->session->set_flashdata('message', '<div class="alert alert-danger">Validatio Error</strong> ' . $error . '</div>');
        }

        redirect(base_url('QBO_controllers/Customer_Details/customer_details/InActive'));

    }

    public function view_customer($cusID = '')
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $data['merchantEmailID'] = $this->session->userdata('logged_in')['merchantEmail'];
            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $data['merchantEmailID'] = $this->session->userdata('user_logged_in')['merchant_data']['merchantEmail'];
            $user_id = $data['login_info']['merchantID'];
        }
        $merchant_condition = [
			'merchID' => $user_id,
        ];
        
        $this->qboOBJ->get_customer_data([
            'customerId' => $cusID
        ]);

        $this->qboOBJ->get_invoice_data([
            'customerId' => $cusID
        ]);

        $data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		$data['isEcheckGatewayExists'] = $data['gateway_datas']['isEcheckExists'];
		$data['isCardGatewayExists'] = $data['gateway_datas']['isCardExists'];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
            $data['isEcheckGatewayExists'] = $data['defaultGateway']['echeckStatus'];
			$data['isCardGatewayExists'] = $data['defaultGateway']['creditCard'];
		}

        $data['customer']      = $this->qbo_customer_model->customer_by_id($cusID, $user_id);
        if (!empty($data['customer'])) {
            // Convert added date in timezone 
            if(isset($data['customer']->createdAt) && isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone']) ){
              $timezone = ['time' => $data['customer']->createdAt, 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
              $data['customer']->createdAt = getTimeBySelectedTimezone($timezone);
            }
            
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();
            $data['page_num']    = 'customer_qbo';
            $data['customerID'] = $cusID;

           
            $data['invoices']       = $this->qbo_customer_model->get_invoice_upcomming_data($cusID, $user_id);
          
            $paydata = $this->qbo_customer_model->get_customer_invoice_data_payment($cusID, $user_id);
           
            $data['notes'] = $this->qbo_customer_model->get_customer_note_data($cusID, $user_id);

            $data['sum_invoice'] = ($paydata->applied_amount) ? $paydata->applied_amount : '0.00';
            $condition1          = array('merchantID' => $user_id, 'systemMail' => 0);

            $data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);

            $data['pay_invoice']    = ($paydata->applied_amount1) ? $paydata->applied_amount1 : '0.00';
            $data['pay_upcoming']   = ($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00';
            $data['pay_remaining']  = ($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00';
            $data['pay_due_amount'] = ($paydata->applied_due) ? $paydata->applied_due : '0.00';
            $mail_con               = array('merchantID' => $user_id, 'customerID' => $cusID);

            $sub                      = array('sbs.customerID' => $cusID, 'cust.merchantID' => $user_id, 'sbs.merchantDataID' => $user_id);
            $data['getsubscriptions'] = $this->qbo_customer_model->get_cust_subscriptions_data($sub);

            $data['editdatas']       = $this->qbo_customer_model->get_email_history($mail_con);
            $data['card_data_array'] = $this->card_model->get_customer_card_data($cusID);

            $data['transaction_history_data']   = $this->qbo_customer_model->get_transaction_history_data($user_id, $cusID);
            $condition = array('merchantID' => $user_id);
          
            $data['gateway_datas'] = $this->general_model->get_gateway_data($user_id);

           
            $plantype              = $this->general_model->chk_merch_plantype_status($user_id);
            $data['plantype']      = $plantype;
            $data['from_mail'] = DEFAULT_FROM_EMAIL;
			$data['mailDisplayName'] = $this->loginDetails['companyName'];

            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('QBO_views/page_customer_details', $data);
            $this->load->view('QBO_views/page_qbo_model', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);

        } else {
            redirect('login', 'refresh');
        }
    }

    public function change_profile_picture()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $customerListID = $this->czsecurity->xssCleanPostInput('customerID');

        if (!empty($_FILES['profile_picture']['name'])) {

            $config['upload_path']   = 'uploads/customer_pic/';
            $config['allowed_types'] = 'jpg|jpeg|png|gif';
            $config['overwrite']     = false;
            $config['remove_spaces'] = true;
            $config['file_name']     = time() . $_FILES['profile_picture']['name'];

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('profile_picture')) {
                $uploadData = $this->upload->data();

                $profle_picture = $uploadData['file_name'];

                $this->thumb($uploadData, 172, false);
                $this->thumb($uploadData, 172, true);
                unlink($uploadData['full_path']);

            } else {

                $error = $this->upload->display_errors();

                $profle_picture = '';
            }
            $input_data['profile_picture'] = $profle_picture;

            $con    = array('Customer_ListID' => $customerListID, 'merchantID' => $user_id);
            $insert = $this->general_model->update_row_data('QBO_custom_customer', $con, $input_data);

        }
        if ($insert) {
            $data['status'] = 'success';
            $data['msg']    = "Profile picture uploaded successfully";
        } else {
            $data['status'] = 'failed';
            $data['msg']    = $error;
        }

        echo json_encode($data);
        die;
    }

    public function thumb($data, $thumb_size, $create_thumb)
    {
        $config['image_library']  = 'gd2';
        $config['source_image']   = $data['full_path'];
        $config['create_thumb']   = $create_thumb;
        $config['maintain_ratio'] = true;
        $config['width']          = $thumb;
        $config['height']         = $thumb;
        $this->load->library('image_lib', $config);
        $this->image_lib->resize();
    }

    public function get_gateway_data()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');
        $condition = array('gatewayID' => $gatewayID);

        $res = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

        if (!empty($res)) {

            $res['status'] = 'true';
            echo json_encode($res);
        }

        die;

    }

    //------------- Merchant gateway START ------------//

    public function gateway()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $condition = array('merchantID' => $user_id);

        $data['all_gateway'] = $this->general_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function Qbo_sync_data(){
        $sync_filter = $this->czsecurity->xssCleanPostInput('sync_filter');
        $this->merchantID;

        // Add Queue
        $operation = '';
        switch($sync_filter){
            case 1:
                $operation = 'QBO/sync_customers';
                // $this->qboOBJ->get_customer_data();
            break;
            case 2:
                $operation = 'QBO/sync_invoices';
                // $this->qboOBJ->get_invoice_data();
            break;
            case 3:
                $operation = 'QBO/sync_products';
                // $this->qboOBJ->get_items_data();
            break;
            case 4:
                $operation = 'QBO/sync_payment_methods';
                // $this->qboOBJ->get_payment_method();
            break;
            default:
                $operation = 'QBO/sync_all';
                //$this->qboOBJ->syncAll();
            break;
        }

        if($operation) {
            $this->queue_model->addQueue( $this->merchantID, $operation);
            $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success: Sync is added in queue and will be starting.</strong></div>');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Selection</strong></div>');
        }
        
        redirect(base_url('QBO_controllers/home/qbo_log'), 'refresh');
    }
    

    //---------------- To add gateway  ---------------//

    public function create_gateway1()
    {
        $signature  = '';
        $cr_status  = 1;
        $ach_status = 0;
        if (!empty($this->input->post(null, true))) {

            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
            $condition          = array('merchantID' => $user_id);
            $gmID     = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
            if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                if ($this->czsecurity->xssCleanPostInput('mni_cr_status')) {
                    $cr_status = 1;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $nmipassword = $this->czsecurity->xssCleanPostInput('secretKey');

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '12') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword');
                $gmID     = $this->czsecurity->xssCleanPostInput('tsysMerchID');

                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '14') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
            }
            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');

            $gatetype = $this->czsecurity->xssCleanPostInput('gateway_opt');
            $frname   = $this->czsecurity->xssCleanPostInput('frname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,

            );

            if ($this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {

                $this->session->set_flashdata('success', 'Successfully Created Gateway');

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('QBO_controllers/home/gateway'));

        }

    }

    public function create_gateway_old()
    {
        $this->load->library('Gateway');
        $cr_status  = 1;
        $ach_status = 0;

        $signature = '';

        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $user_id = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $user_id = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $condition = array('merchantID' => $user_id);

            $gt_status = 0;
            $gt_obj    = new Gateway();

            if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
                if ($this->czsecurity->xssCleanPostInput('mni_cr_status')) {
                    $cr_status = 1;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_auth_gateway_auth($auth_data);

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_paytrace_gateway_auth($auth_data);

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword, 'signature' => $signature);

                $gt_status = $gt_obj->chk_paypal_gateway_auth($auth_data);

            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_stripe_gateway_auth($auth_data);

            }

            if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_usaePay_gateway_auth($auth_data);

            }
            if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_heartland_gateway_auth($auth_data);

            }

            if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');

            }

            if ($gt_status == 0) {
                $this->session->set_flashdata('message', '<strong>Error: Gateway Authentication Failed</strong>');
                redirect(base_url('QBO_controllers/home/gateway'));
            }

            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');

            $gatetype = $this->czsecurity->xssCleanPostInput('gateway_opt');
            $frname   = $this->czsecurity->xssCleanPostInput('frname');
            $gmID     = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,

            );

            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
                if ($chk_gateway == '1') {
                    $val = array(
                        'gatewayID' => $gid,
                    );

                    $val1 = array(
                        'merchantID' => $user_id,
                    );
                    $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                    $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                    $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                    $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
                }
                $this->session->set_flashdata('success', 'Successfully Inserted');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('QBO_controllers/home/gateway'));

        }

    }

    public function create_gateway()
    {
        $signature  = '';
        $extra1 = '';
        $cr_status  = 1;
        $ach_status = 0;
        $isSurcharge = $surchargePercentage = 0;
        if (!empty($this->input->post(null, true))) {

            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
            $condition          = array('merchantID' => $user_id);
            $gatetype           = $this->czsecurity->xssCleanPostInput('gateway_opt');
            $gmID     = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '3') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');

            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');

            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');

            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status')) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
    			if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
					$isSurcharge = 1;
				}
				else{
                    $isSurcharge = 0;
				}
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			} else if ($gatetype == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == '12') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword');
                $gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if ($gatetype == '14') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '15') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
				$nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
			} else if ($gatetype  == '17') {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			}  else if ($gatetype == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            }
            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');
            $frname   = $this->czsecurity->xssCleanPostInput('frname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'extra_field_1'                        => $extra1,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
            );

            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
                
                $val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

                $this->session->set_flashdata('success', 'Successfully Created Gateway');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('QBO_controllers/home/gateway'));
        }

    }

    //----------- TO update the gateway  --------------//

    public function update_gateway()
    {
        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
            $ach_status = 0;
            $signature  = '';
            $extra1 = '';
            $cr_status  = 1;
            $isSurcharge = $surchargePercentage = 0;

            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype      = $this->czsecurity->xssCleanPostInput('gateway');
            $gmID   = $this->czsecurity->xssCleanPostInput('mid');
            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if (strtolower($gatetype) == 'authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Paytrace') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID;

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');

            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');

            } else if ($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecret1');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');

            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == iTransactGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
			} else if ($gatetype == FluidGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype  == TSYSGatewayName) {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('tsysUserID1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('tsysPassword1');
                $gmID                  = $this->czsecurity->xssCleanPostInput('tsysMerchID1');
                if ($this->czsecurity->xssCleanPostInput('tsys_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('tsys_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }  else if ($gatetype  == BASYSGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'CardPointe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName1');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

			} else if($gatetype == PayArcGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser1');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            } else if ($gatetype  == MaverickGatewayName) {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId1');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
            } else if ($gatetype == EPXGatewayName) {
                $nmiuser         = $this->input->post('EPXCustNBR1');
                $nmipassword     = $this->input->post('EPXMerchNBR1');
                $signature     = $this->input->post('EPXDBANBR1');
                $extra1   = $this->input->post('EPXterminal1');
                if ($this->input->post('EPX_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            } 

            $frname = $this->czsecurity->xssCleanPostInput('fname');
            

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewayFriendlyName'                  => $frname,
                'gatewaySignature'                     => $signature,
                'extra_field_1'                        => $extra1,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
                'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
            );

            if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {

                $this->session->set_flashdata('success', 'Successfully Updated');

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');

            }

            redirect(base_url('QBO_controllers/home/gateway'));

        }

    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
        $data['gateway'] = getGatewayNames($data['gatewayType']);

        echo json_encode($data);die;
    }

    public function set_gateway_default()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('QBO_controllers/home/gateway'));
    }

    /**************Delete credit********************/

    public function delete_gateway()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
        $condition = array('gatewayID' => $gatewayID);
        $del       = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
        if ($del) {
            
            $this->session->set_flashdata('success', 'Successfully Deleted Gateway');

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect(base_url('QBO_controllers/home/gateway'));

    }

    public function delele_note()
    {

        if ($this->czsecurity->xssCleanPostInput('noteID') != "") {

            $noteID = $this->czsecurity->xssCleanPostInput('noteID');

            if ($this->db->query("Delete from tbl_private_note where noteID =  '" . $noteID . "' ")) {

                array('status' => "success");
                echo json_encode(array('status' => "success"));
                die;
            }
            return false;
        }

    }

    public function add_note()
    {
        if ($this->session->userdata('logged_in')) {
            $da['login_info'] = $this->session->userdata('logged_in');

            $user_id = $da['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $da['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $da['login_info']['merchantID'];
        }
        if (!empty($this->czsecurity->xssCleanPostInput('customerID'))) {
            $cusID = $this->czsecurity->xssCleanPostInput('customerID');
            if ($this->czsecurity->xssCleanPostInput('private_note') != "") {

                $private_note = $this->czsecurity->xssCleanPostInput('private_note');
                $data_ar      = array('privateNote' => $private_note, 'privateNoteDate' => date('Y-m-d H:i:s'), 'customerID' => $cusID, 'merchantID' => $user_id);
                $id = $this->general_model->insert_row('tbl_private_note', $data_ar);
                if ($id > 0) {
                    array('status' => "success");
                    echo json_encode(array('status' => "success"));
                    die;

                } else {
                    array('status' => "error");
                    echo json_encode(array('status' => "success"));
                    die;
                }
            }

        }

    }

    public function general_volume()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $get_result = $this->qbo_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
           
            $result_set    = array();
            $result_value1 = array();
            $result_value2 = array();

            $result_online_value = array();
            $result_online_month = array();

            $result_eCheck_value = array();
            $result_eCheck_month = array();

            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);

                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);

                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);

            }
            
            $opt_array['revenu_month'] =   $result_value1;
			$opt_array['revenu_volume'] =   $result_value2;
            
			$opt_array['online_month'] =   $result_online_month;
			$opt_array['online_volume'] =   $result_online_value;
			
			
            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];

            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $result_eCheck_value;
            echo json_encode($opt_array);

        }

    }

    public function get_invoice_due_company()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $condition     = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id, 'inv.isDeleted' => 0);
        $result_value1 = array();
        $result_value2 = array();
        $get_result    = array();
        $get_result1   = $this->qbo_company_model->get_invoice_due_by_company($condition, 0);
        if ($get_result1) {

            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];

            }
            $get_result = $res;

            echo json_encode($get_result);
            die;
        } else {

            echo json_encode($get_result);
            die;
        }

    }
    public function get_invoice_Past_due_company()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $condition   = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $get_result  = array();
        $get_result1 = $this->qbo_company_model->get_invoice_due_by_company($condition, 1);
        if ($get_result1) {

            foreach ($get_result1 as $k => $count_merch) {
                $res[$k][] = $count_merch['label'];
                $res[$k][] = (float) $count_merch['balance'];

            }
            $get_result = $res;

            echo json_encode($get_result);

        } else {
            echo json_encode($get_result);

        }
        die;
    }

    public function get_qbo_sale_invoices()
    {
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $invoices = $this->qbo_customer_model->get_invoice_upcomming_data($customerID, $merchantID);
            $new_inv  = '<div class="form-group alignTableInvoiceList" >
		       <div class="col-md-2 text-center"><b></b></div>
		        <div class="col-md-2  text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2  text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
               </div>';
            $inv_data = [];
            foreach ($invoices as $inv) {
                $new_inv .= '<div class="form-group alignTableInvoiceList" >

		        <div class="col-md-2 text-center"><input type="checkbox" class="chk_pay check_'.$inv['refNumber'].'"  id="' . 'multiinv' . $inv['invoiceID'] . '"   onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['refNumber'] . '</div>
		        <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
                <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '"  value="' . $inv['BalanceRemaining'] . '" data-value="' . $inv['BalanceRemaining'] . '" /></div>
               </div>';
               $inv_data[] = [
                    'refNumber' => $inv['refNumber'],
                    'invoiceID' => $inv['invoiceID'],
                    'BalanceRemaining' => $inv['BalanceRemaining'],
                ];
            }

            $card         = '';
            $card_name    = '';
            $customerdata = array();

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);
            if (!empty($customerdata)) {

            
                $customerdata['status'] = 'success';

              
                $customerdata['invoices'] = $new_inv;
                $customerdata['invoice_record'] = $invoices;
				$customerdata['inv_data']   = $inv_data;

                echo json_encode($customerdata);
                die;
            }

        }

    }
    public function get_qbo_customer_invoices()
    {
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $invoices = $this->qbo_customer_model->get_invoice_upcomming_data($customerID, $merchantID);
            $new_inv  = '<div class="form-group" >
		       <div class="col-md-2 text-center"><b>Select</b></div>
		        <div class="col-md-2  text-left"><b>Number</b></div>
		        <div class="col-md-3 text-right"><b>Due Date</b></div>
		        <div class="col-md-2  text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
               </div>';
           
            #Invoice existence check at QBO
            $this->qbo = $this->qboOBJ; 

            foreach ($invoices as $inv) {
                $inv_data = false; 

                if(!$inv_data){
                    $new_inv .= '<div class="form-group" >
    
                    <div class="col-md-2 text-center"><input type="checkbox" class="chk_pay"  id="' . 'multiinv' . $inv['invoiceID'] . '"   onclick="chk_inv_position(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
                    <div class="col-md-2 text-left">' . $inv['refNumber'] . '</div>
                    <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
                    <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
                   <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
                   </div>';
                }
            }

            $card         = '';
            $card_name    = '';
            $customerdata = array();

       
            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('QBO_custom_customer', $condition);
            if (!empty($customerdata)) {

              
                $customerdata['status'] = 'success';

                $card_data                = $this->card_model->getCustomerCardDataByID($customerID);
                $customerdata['card']     = $card_data;
                $customerdata['invoices'] = $new_inv;

                echo json_encode($customerdata);
                die;
            }

        }

    }

    public function get_product_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $data      = $this->qbo_customer_model->get_qbo_item_data($merchantID);
        $data      = json_encode($data);
        echo $data = str_replace("'", "", $data);

        die;

    }
    public function get_subs_item_count_data()
    {
        $sbID           = $this->czsecurity->xssCleanPostInput('subID');
        $data1['items'] = $this->general_model->get_table_data('tbl_subscription_invoice_item_qbo', array('subscriptionID' => $sbID));
        $data1['rows']  = $this->general_model->get_num_rows('tbl_subscription_invoice_item_qbo', array('subscriptionID' => $sbID));
        echo json_encode($data1);
        die;
    }

    public function get_invoice_item_count_data()
    {

        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $sbID           = $this->czsecurity->xssCleanPostInput('invID');
        $con            = array('invoiceID' => $sbID, 'merchantID' => $merchantID);
        $data1['items'] = $this->general_model->get_table_data('tbl_qbo_invoice_item', $con);
        $data1['rows']  = $this->general_model->get_num_rows('tbl_qbo_invoice_item', $con);
        echo json_encode($data1);die;

    }

    public function company()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $merchantID         = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $merchantID         = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $val = array(
            'merchantID' => $merchantID,
        );

        $data1 = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data1['accessToken'];
        $refreshToken = $data1['refreshToken'];
        $realmID      = $data1['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),
        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $qbo_data = $this->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchantID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery'));

        if (!empty($qbo_data)) {
            $last_date = $qbo_data['lastUpdated'];
           
            $my_timezone = date_default_timezone_get();
            $from        = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to          = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date   = $this->general_model->datetimeconvqbo($last_date, $from, $to);
            $last_date1  = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

            $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));
			$sqlQuery = "SELECT * FROM Item where Metadata.LastUpdatedTime > '$latedata'";
          
        } else {
			$sqlQuery = "SELECT * FROM Item";
		}
        $keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}
        $error = $dataService->getLastError();
       
        if ($error != null) {
            $data['connect'] = 'no';
		}else{
            $data['connect'] = 'yes';
        }
        $data['gt_result'] = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 0]);
        $in = $this->session->flashdata('disconnect_redircect');
        $data['rediect_dis']  = 0;
        if($in==1)
        {
            $data['rediect_dis'] = 1;
        }
        $condition         = array('merchantID' => $merchantID);
        $data['companies'] = $this->general_model->get_table_data('QBO_token', $condition);
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);

        $this->load->view('QBO_views/page_company', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function setup_crm()
    {


        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $merchantID         = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $merchantID         = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $condition         = array('merchantID' => $merchantID);
        $data['companies'] = $this->general_model->get_table_data('QBO_token', $condition);
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);

        $this->load->view('QBO_views/page_crm', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function qbo_log()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['alls'] = $this->general_model->get_table_data('tbl_qbo_log', array('merchantID' => $user_id));

        // CHeck Queue is in process
        $data['queueInProgress'] = $this->queue_model->getMerchantQueues($user_id, [Queue_Model :: QUEUE_PENDING, Queue_Model :: QUEUE_INPROGRESS]);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/page_log', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /**
     * Check QBO Queue
     *
     * @return void
     */
    public function checkQboQueue()
	{
		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}else if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		} else {
            $merchantID = false; 
        }
		
		if($merchantID){
			// Check Merchant Queues
			$queueUpdated = $this->queue_model->checkRecentCompletedQueue($merchantID);

			echo json_encode(array('status' => 'success', 'queue_updated' => $queueUpdated)); die;
		}
		else {
            echo json_encode(array('status' => 'failed'));
		}

         die;
	}

    // dummy function

    public function view_customer_new($cusID = '')
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $merchant_condition = [
			'merchID' => $user_id,
		];

		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
        }
        
        $data['gateway_datas'] = $this->general_model->get_gateway_data($user_id);
        $data['customer']      = $this->qbo_customer_model->customer_by_id($cusID, $user_id);

        if (!empty($data['customer'])) {
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();
            $data['page_num']    = 'customer_qbo';

            $data_invoice           = $this->qbo_customer_model->get_customer_invoice_data_sum($cusID, $user_id);
            $data['invoices_count'] = ($data_invoice->incount) ? $data_invoice->incount : '0';
            $data['invoices']       = $this->qbo_customer_model->get_invoice_upcomming_data($cusID, $user_id);
            $data['latest_invoice'] = $this->qbo_customer_model->get_invoice_latest_data($cusID, $user_id);

            $paydata = $this->qbo_customer_model->get_customer_invoice_data_payment($cusID, $user_id);
            $data['notes'] = $this->qbo_customer_model->get_customer_note_data($cusID, $user_id);

            $data['sum_invoice'] = ($paydata->applied_amount) ? $paydata->applied_amount : '0.00';
            $condition1          = array('merchantID' => $user_id, 'systemMail' => 0);

            $data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);

            $data['pay_invoice']    = ($paydata->applied_amount1) ? $paydata->applied_amount1 : '0.00';
            $data['pay_upcoming']   = ($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00';
            $data['pay_remaining']  = ($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00';
            $data['pay_due_amount'] = ($paydata->applied_due) ? $paydata->applied_due : '0.00';
            $mail_con               = array('merchantID' => $user_id, 'customerID' => $cusID);

            $sub                      = array('sbs.customerID' => $cusID, 'cust.merchantID' => $user_id, 'sbs.merchantDataID' => $user_id);
            $data['getsubscriptions'] = $this->qbo_customer_model->get_cust_subscriptions_data($sub);

            $data['editdatas']       = $this->qbo_customer_model->get_email_history($mail_con);
            $data['card_data_array'] = $this->card_model->get_card_expiry_data($cusID);

            $condition = array('merchantID' => $user_id);
            $data['gateway_datas'] = $this->general_model->get_gateway_data($user_id);


            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('QBO_views/page_customer_details_new', $data);
            $this->load->view('QBO_views/page_qbo_model', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);

        } else {
            redirect('login', 'refresh');
        }
    }
    public function transation_receipt($txt_id = '', $invoiceId = '', $trans_id = '')
	{

		$page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);
        foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['transaction_id'] = empty($trans_id) ? null : $trans_id;
            $data['Ip'] = getClientIpAddr();
            $con = array('transactionID' => $data['transaction_id'], 'merchantID' => $user_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            $data['transactionDetail'] = $pay_amount;

            $data['transactionAmount'] = ($pay_amount && !empty($trans_id)) ? $pay_amount['transactionAmount'] : '0.00';

            $surCharge = 0;
            $totalAmount = 0;
            $isSurcharge = 0;
            $transactionType = isset($pay_amount['transactionGateway']) ? $pay_amount['transactionGateway'] : '0';

            if(isset($pay_amount['transactionGateway']) && $pay_amount['transactionID'] != '' ){
                if($pay_amount['transactionGateway'] == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$trans_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }   
                }
            }
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
            $data['transactionCode'] = ($pay_amount) ? $pay_amount['transactionCode'] : '0';
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$in_data  =  $this->session->userdata('in_data');
            $data['invoice'] = $invoiceId;
            $val = array(
                'merchantID' => $user_id,
            );
    
            $data1 = $this->general_model->get_row_data('QBO_token', $val);
    
            $accessToken = $data1['accessToken'];
            $refreshToken = $data1['refreshToken'];
            $realmID      = $data1['realmID'];
			$data['invoice_number'] = $in_data['refNumber'];
			$condition2 			= array('invoiceID' => $invoiceId, 'merchantID' => $user_id, 'companyID' => $realmID);
            $invoice_data = [];
            $invoice_data[]  = $this->general_model->get_row_data('QBO_test_invoice', $condition2);
          
            $condition3 			= array('Customer_ListID' => $in_data['CustomerListID'],'merchantID' => $user_id, 'companyID' => $realmID);
            
            $customer_data			= $this->general_model->get_row_data('QBO_custom_customer', $condition3);
            $customer_data = convertCustomerDataFieldName($customer_data,1);
			$data['customer_data'] = $customer_data;
			$data['invoice_data'] = $invoice_data;
		


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);

			$this->load->view('comman-pages/transaction_proccess_receipt', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	public function transation_credit_receipt($invoiceId = "null", $customer_id = "null", $trans_id = "null")
	{

		$page_data = $this->session->userdata('receipt_data');
        $page_data['transaction_id'] = $trans_id;
        $this->session->set_userdata("receipt_data", $page_data);
        foreach($page_data as $key => $rcData){
			$page_data[$key] = strip_tags($rcData);
		}
         
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page_data'] = $page_data;
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}
			
			$data['transaction_id'] = $trans_id;
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
			$data['invoice'] = '';
			$data['invoice_number'] = '';
			
			$condition3 			= array('Customer_ListID' => $customer_id, 'merchantID' => $user_id);
			$customer_data			= $this->general_model->get_row_data('QBO_custom_customer', $condition3);
            $customer_data = convertCustomerDataFieldName($customer_data,1);
			$data['customer_data'] = $customer_data;
			$data['invoice_data'] = $customer_data;
            if(isset($invoiceId) && !empty($invoiceId) && $invoiceId != 'transaction'){
                $in_data   =    $this->qbo_company_model->get_invoice_data_pay($invoiceId);
                $data['invoice'] = $invoiceId;
                $data['invoice_number'] = $in_data['refNumber'];
            }

            $surCharge = 0;
            $isSurcharge = 0;
            $totalAmount = 0;
            $transactionType = 0;

            $invoice_IDs = [];
            $invoice_data = [];
            if($trans_id != null){
                $condition4             = array('transactionID' => $trans_id, 'merchantID' => $user_id,'customerListID' => $customer_id);
                $transactionData          = $this->general_model->get_row_data('customer_transaction', $condition4);
                $data['transactionAmount'] = $transactionData['transactionAmount'];
                $data['transactionCode'] = $transactionData['transactionCode'];
                $transactionType = $transactionData['transactionGateway'];
                $data['transactionDetail'] = $transactionData;
                /*Invoice Set*/
                $invoiceArray = json_decode($transactionData['custom_data_fields']);
                $invoiceStr = '';
                
                if(!empty($invoiceArray) && isset($invoiceArray->invoice_number)){

                    $invoiceStr = $invoiceArray->invoice_number;
                    $invoice_IDs = explode(',', $invoiceStr);
                }
                
                if (!empty($invoice_IDs)) {
                    foreach ($invoice_IDs as $inID) {
                        $condition2 = array('invoiceID' => $inID);
                        $invoice_data[]  = $this->general_model->get_row_data('QBO_test_invoice', $condition2);
                    }
                }
                $data['invoice_IDs'] = $invoice_IDs;
                $data['invoice_data'] = $invoice_data;
            }else{
                $data['transactionAmount'] = 0;
                $data['transactionCode'] = 0;
                $data['invoice_IDs'] = $invoice_IDs;
                $data['invoice_data'] = $invoice_data;
            }
            
		    if(isset($transactionType)){
                if($transactionType == 10){

                    $resultAmount = getiTransactTransactionDetails($user_id,$trans_id);
                    $totalAmount = $resultAmount['totalAmount'];
                    $surCharge = $resultAmount['surCharge'];
                    $isSurcharge = $resultAmount['isSurcharge'];
                    if($resultAmount['payAmount'] != 0){
                        $data['transactionAmount'] = $resultAmount['payAmount'];
                    }   
                }
            }
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/transaction_proccess_receipt', $data);
		    $this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
	
	public function transation_sale_receipt()
	{

		$invoice_IDs = $this->session->userdata('invoice_IDs');
		$receipt_data = $this->session->userdata('receipt_data');
        if(!empty($receipt_data)){
            foreach($receipt_data as $key => $rcData){
                $receipt_data[$key] = strip_tags($rcData);
            }
        }
        

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$transactionCode = 0;
        $surCharge = 0;
        $isSurcharge = 0;
        $totalAmount = 0;
        $transactionType = 0;
        $pay_amount = [];
			
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
            }
            $data['transactionAmount'] = $referenceMemo = '';
            $data['transactionCode'] = 0;
            
			if(isset($receipt_data['transaction_id']) && !empty($receipt_data['transaction_id'])){
                $transaction_id = $receipt_data['transaction_id'];
                $transactionRowID = isset($receipt_data['transactionRowID'])?$receipt_data['transactionRowID']:'';
                $con = array('transactionID' => $transaction_id);
                $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount');
                $this->db->from('customer_transaction tr');
                $this->db->where($con);
                if(isset($transactionRowID) && !empty($transactionRowID)){
                    $this->db->where("tr.id",$transactionRowID);
                }
                $this->db->group_by("tr.transactionID");
                $pay_amount = $this->db->get()->row_array();
                if($pay_amount){
                    $payAmountSet = $pay_amount['transactionAmount'];

                    $transactionType = $pay_amount['transactionGateway'];

                    if($pay_amount['transactionGateway'] == 10){

                        $resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
                        $totalAmount = $resultAmount['totalAmount'];
                        $surCharge = $resultAmount['surCharge'];
                        $isSurcharge = $resultAmount['isSurcharge'];
                        if($resultAmount['payAmount'] != 0){
                            $payAmountSet = $resultAmount['payAmount'];
                        }   
                    }

                    $data['transactionCode'] = $pay_amount['transactionCode'];
                    
                    if(isset($data['customer_details']) && $data['customer_details']){
                        $data['customer_details'] = $this->qbo_customer_model->customer_by_id($pay_amount['customerListID'], $user_id);
                        $receipt_data['FullName'] = $data['customer_details']->fullName;
                    }
                    $data['transactionAmount'] = $payAmountSet;
                    $data['transactionDetail'] = $pay_amount;
                    $referenceMemo = $pay_amount['referenceMemo'];
                }
                
            }
             
			$data['Ip'] = getClientIpAddr();
			$data['email'] = $this->session->userdata('logged_in')['merchantEmail'];
			$data['name'] = $this->session->userdata('logged_in')['firstName']. ' ' .$this->session->userdata('logged_in')['lastName'];
		
			
			$data['customer_data'] = $receipt_data;
			$invoice_data = [];
			if (!empty($invoice_IDs)) {

				foreach ($invoice_IDs as $inID) {
					$condition2 = array('invoiceID' => $inID, 'merchantID' => $user_id);
					$invoiceData  = $this->general_model->get_row_data('QBO_test_invoice', $condition2);
                    if(!empty($invoiceData)){
                        $invoice_data[]  = $invoiceData;
                    }
				}
			}

            if(isset($receipt_data['refundAmount']) && !empty($receipt_data['refundAmount'])){
                $data['transactionAmount'] = $receipt_data['refundAmount'];
            }
            $data['referenceMemo'] = $referenceMemo;
            $data['surchargeAmount'] = $surCharge;
            $data['totalAmount'] = $totalAmount;
            $data['transactionType'] = $transactionType;
            $data['isSurcharge'] = $isSurcharge;
			$data['invoice_IDs'] = $invoice_IDs;
			$data['invoice_data'] = $invoice_data;
            $data['transactionDetail'] = $pay_amount; 
			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
            $this->load->view('comman-pages/transaction_receipt', $data);
			$this->load->view('template/page_footer', $data);

			$this->load->view('template/template_end', $data);
		
	}
    public function insert_new_temp_data()
    {
        $reseller = $this->general_model->get_reseller_table_data('tbl_merchant_data');
      
        foreach($reseller as $value){
          $arr = array(
              'templateName' => 'Payment Successful (Merchant)',
              'fromEmail' => 'donotreply@payportal.com',
              'replyTo' => 'donotreply@payportal.com',
              'templateType' => '15',
              'merchantID' => $value['merchID'],
              'message' => '<p><span style="font-size:12px"><span style="font-family:Arial,Helvetica,sans-serif">Dear {{merchant_name}},</span></span></p>


              <p><span style="font-size:12px"><span style="font-family:Arial,Helvetica,sans-serif">A successful payment of {{transaction.amount}} was made by {{customer.name}} on {{transaction.transaction_date}}</span></span></p>
              
              <p><span style="font-size:12px"><span style="font-family:Arial,Helvetica,sans-serif">Please login to review the transaction.<br />
              
              
              <p><strong><span style="font-family:Arial,Helvetica,sans-serif">
              <span style="font-size:12px"><br>{{logo}}</span></span></strong></p>
              </div>
              ',
              'emailSubject' => 'Payment notification',
          );
          $this->general_model->insert_row_data('tbl_email_template', $arr);
        }

        die;
    }

    public function my_account()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');

            $user_id                = $data['login_info']['merchID'];
            $data['loginType'] = 1;
        }else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
            $data['loginType'] = 2;
        }   

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        
        $condition  = array('merchantID' => $user_id);
        $data['invoices'] = $this->general_model->get_table_data('tbl_merchant_billing_invoice', $condition);

        $plandata = $this->general_model->chk_merch_plantype_data($user_id);
        $resellerID = $data['login_info']['resellerID'];
        $planID = $plandata->plan_id;
        $planname = $this->general_model->chk_merch_planFriendlyName($resellerID,$planID);

        if(isset($planname) && !empty($planname)){
            $data['planname'] = $planname;
        }else{
            $data['planname'] = $plandata->plan_name;
        }


        if($plandata->cardID > 0 && $plandata->payOption > 0){
            $carddata = $this->card_model->get_merch_card_data($plandata->cardID);
        }else{
            $carddata = [];
        }
        
        $data['plan'] = $plandata;

        $data['carddata'] = $carddata;

        $conditionMerch = array('merchID'=>$user_id);
        $data['merchantData'] = $this->general_model->get_row_data('tbl_merchant_data', $conditionMerch);

        $data['interface'] = 1;
        $data['merchantID'] = $user_id;
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_my_account', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_card_data()
    {
        
        $success_msg = null;
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }   
            
            
            $resellerID  = $this->czsecurity->xssCleanPostInput('resellerID');
                
            $merchantcardID = $this->czsecurity->xssCleanPostInput('cardID'); 

            $merchantID = $user_id;

            $condition = array('merchantListID'=>$merchantID);

            $conditionMerch = array('merchID'=>$merchantID);
            
            $billing_first_name = ($this->czsecurity->xssCleanPostInput('billing_first_name') != null)?$this->czsecurity->xssCleanPostInput('billing_first_name'):null;

            $billing_last_name = ($this->czsecurity->xssCleanPostInput('billing_last_name')!= null)?$this->czsecurity->xssCleanPostInput('billing_last_name'):null;

            $billing_phone_number = ($this->czsecurity->xssCleanPostInput('billing_phone_number')!= null)?$this->czsecurity->xssCleanPostInput('billing_phone_number'):null;

            $billing_email = ($this->czsecurity->xssCleanPostInput('billing_email')!= null)?$this->czsecurity->xssCleanPostInput('billing_email'):null;

            $billing_address = ($this->czsecurity->xssCleanPostInput('billing_address')!= null)?$this->czsecurity->xssCleanPostInput('billing_address'):null;

            $billing_state = ($this->czsecurity->xssCleanPostInput('billing_state')!= null)?$this->czsecurity->xssCleanPostInput('billing_state'):null;

            $billing_city = ($this->czsecurity->xssCleanPostInput('billing_city')!= null)?$this->czsecurity->xssCleanPostInput('billing_city'):null;

            $billing_zipcode = ($this->czsecurity->xssCleanPostInput('billing_zipcode')!= null)?$this->czsecurity->xssCleanPostInput('billing_zipcode'):null;
            $statusInsert = 0;
            /* check is_address_update condition 1 than only address update and 2 for all */
            if($this->czsecurity->xssCleanPostInput('is_address_update') == 1){
                $insert_array =  array( 
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                if($merchantcardID!="")
                {
                    
                    $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                    
                    
                    $success_msg = 'Address Updated Successfully';  

                }
            }else{
                /* Save credit card data */
                if($this->czsecurity->xssCleanPostInput('payOption') == 1){

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
                    $card_type = $this->general_model->getcardType($card_no);
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $type = 'Credit';
                    $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                    $insert_array =  array( 'CardMonth'  =>$expmonth,
                                    'CardYear'   =>$exyear, 
                                    'resellerID'  =>$resellerID,
                                    'merchantListID'=>$merchantID,
                                    'accountNumber'   => null,
                                    'routeNumber'     => null,
                                    'accountName'   => null,
                                    'accountType'   => null,
                                    'accountHolderType'   => null,
                                    'secCodeEntryMethod'   => null,
                                    'merchantFriendlyName' => $friendlyname,
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                    if($merchantcardID!="")
                    {
                        
                           
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = '';
                        $insert_array['CardType']        = $card_type;
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                        $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                        
                        
                        $success_msg = 'Credit Card Updated Successfully';  

                    }else{
                        
                        $insert_array['CardType']    = $card_type;
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = ''; 
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                              
                            
                        $id = $this->card_model->insert_merchant_card_data($insert_array);
                        $statusInsert = 1;
                        $merchantcardID = $id;
                        $success_msg = 'Credit Card Inserted Successfully';  
                        
                    }

                }else if($this->czsecurity->xssCleanPostInput('payOption') == 2){
                    /* Save checking card data */
                    $acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode      = $this->czsecurity->xssCleanPostInput('secCode');
                    $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    $card_type = 'Checking';
                    $type = 'Checking';
                    $friendlyname = $type . ' - ' . substr($acc_number, -4);
                    $card_data = array(
                        'CardType'     => $card_type,
                        'CardMonth'  => null,
                        'CardYear'   => null, 
                        'MerchantCard' => null, 
                        'CardCVV' => null,
                        'accountNumber'   => $acc_number,
                        'routeNumber'     => $route_number,
                        'accountName'   => $acc_name,
                        'accountType'   => $acct_type,
                        'accountHolderType'   => $acct_holder_type,
                        'secCodeEntryMethod'   => $secCode,
                        'merchantListID'=>$merchantID,
                        'resellerID'  =>$resellerID,
                        'merchantFriendlyName' => $friendlyname,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        "billing_first_name" => $billing_first_name,
                        "billing_last_name" => $billing_last_name,
                        "billing_phone_number" => $billing_phone_number,
                        "billing_email" => $billing_email,
                        "Billing_Addr1" => $billing_address,
                        "Billing_Country" =>null,
                        "Billing_State" => $billing_state,
                        "Billing_City" =>$billing_city,
                        "Billing_Zipcode" => $billing_zipcode
                                 
                    );
                    if($merchantcardID!="")
                    {
                        
                        $id = $this->card_model->update_merchant_card_data($condition, $card_data);  
                        
                        

                        $success_msg = 'Checking Card Updated Successfully';  
                    }else{
                        
                        $id = $this->card_model->insert_merchant_card_data($card_data);
                        $merchantcardID = $id;
                        $statusInsert = 1;
                        $success_msg = 'Checking Card Inserted Successfully';  
                    }


                }else{
                    /* do nothing*/
                    $id = false;
                }
            }
            
            if($statusInsert == 1){
                $merchant_condition = ['merchID' => $merchantID];
                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$merchant_condition);
                if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
                    /* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    
                    $merchantData['merchant_type'] = 'Quickbox Online (QBO)';        
                    
                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                    if($status['statusCode'] == 400){
                        $resource = $this->hatchbuckapi->createContact($merchantData);
                        if($resource['contactID'] != '0'){
                            $contact_id = $resource['contactID'];
                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                        }
                    }
                    /* End campaign in hatchbuck CRM*/
                } 
            }   
            if( $id ){
                /* Update Pay option type in merchant table */
                $update_array =  array( 'payOption'  => $this->czsecurity->xssCleanPostInput('payOption'), 'cardID' => $merchantcardID );

                $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

                $this->session->set_flashdata('success', $success_msg);
            }else{
             
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
            }

            redirect('QBO_controllers/home/my_account/');
    }
    public function marchant_invoice()
    {


        if ($this->uri->segment(4) != "") {
            $data['primary_nav']    = primary_nav();
            $data['template']       = template_variable();
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }
            $invoiceID             =  $this->uri->segment(4);
            
            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);


            $this->load->view('QBO_views/marchant_invoice', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect(base_url('QBO_controllers/home/invoices'));
        }
    }
    public function disconnet_account()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $val = array(
            'merchantID' => $merchID,
        );
       

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        
        $client = new Client($this->config->item('client_id'), $this->config->item('client_secret'));
        $grant_type = 'revoke token';
        $tokenEndPointUrl = 'https://developer.api.intuit.com/v2/oauth2/tokens/revoke';
        $revokeResult = $client->revokeToken($tokenEndPointUrl, $grant_type, $accessToken);
        if($revokeResult == '1'){
            #Sync tax data
            
            $updateData = [
                'accessToken' => 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..g06ceuiEds-sNWQIf13OEw.7yX87FX8JrlLODzAprXviLm7QmgOeyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..g06ceuiEds-sNWQIf13OEw.7yX87FX8JrlLODzAprXviLm7Qmg',
                'refreshToken' => 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..g06ceuiEds',
                'realmID' => 'disconnected',
            ];
            $this->general_model->update_row_data('QBO_token', $val, $updateData);

            $this->session->set_userdata("disconnect",'1');
            $this->session->set_flashdata('disconnect_redircect',1);
            $this->session->set_flashdata('success', 'Successfully Disconnected');
            $res = array('status' => "success");
            echo json_encode($res);
            die;
        }else{
            $this->session->set_flashdata('message', 'Error while Disconnecting');
            $res = array('status' => "failed");
            echo json_encode($res);
            die;
        }
    }
    
    public function level_three()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }

        $data['level_three_master_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master']);
        $data['level_three_visa_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa']);
        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/level_three', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
    
    // add or update visa details
    public function level_three_visa()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');
            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['customer_tax_id'] = $this->czsecurity->xssCleanPostInput('customer_tax_id');
            $insert_data['commodity_code'] = $this->czsecurity->xssCleanPostInput('commodity_code');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['addtnl_tax_freight'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_freight');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['line_item_commodity_code'] = $this->czsecurity->xssCleanPostInput('line_item_commodity_code');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['card_type'] = 'visa';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'visa']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('QBO_controllers/home/level_three');
    }

    // add or update master details
    public function level_three_master_card()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');

            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['addtnl_tax_indicator'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_indicator');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['debit_credit_indicator'] = $this->czsecurity->xssCleanPostInput('debit_credit_indicator');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['addtnl_tax_type'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_type');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['net_gross_indicator'] = $this->czsecurity->xssCleanPostInput('net_gross_indicator');
            
            $insert_data['card_type'] = 'master';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'master']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('QBO_controllers/home/level_three');
    }
    public function dashboardReport()
    {
        
        
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $filterType  = $this->czsecurity->xssCleanPostInput('revenue_filter');
        /*Update filter*/
        $endDate  = $startDate = date('Y-m-d');
        if($this->czsecurity->xssCleanPostInput('endDate',true) != null){
            $endDate  = $this->czsecurity->xssCleanPostInput('endDate',true);
        }
        if($this->czsecurity->xssCleanPostInput('startDate',true) != null){
            $startDate  = $this->czsecurity->xssCleanPostInput('startDate',true);
        }
        $update_array =  array( 'rhgraphOption'  => $filterType, 'rhgraphToDate' => $endDate, 'rhgraphFromDate' => $startDate );
        $conditionMerch = array('merchID'=>$user_id);

        $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array);

        if($filterType == 0){
            $opt_array = $this->getAnnualRevenue($user_id);
            echo json_encode($opt_array);
        }else if($filterType == 1){
            $startDate = date('Y-m-d', strtotime('today - 30 days'));
            $endDate = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,1);
            echo json_encode($opt_array);
        }else if($filterType == 2){
            $startDate = date('Y-m-01');
            $endDate = date('Y-m-d');
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
            echo json_encode($opt_array);
        }else if($filterType == 3){
            $opt_array = $this->general_model->getHourlyRevenue($user_id);
            echo json_encode($opt_array);
        }else if($filterType == 4){
            $opt_array = $this->general_model->getDayRevenue($user_id,$startDate,$endDate,2);
            echo json_encode($opt_array);
        }
        
    }

    public function sync_event()
    {
        $args = [];
        $logID = $this->czsecurity->xssCleanPostInput('logID');
        $returnObj = array('status'=> 0,'message' => 'Not sync');
        
        $logData = $this->general_model->get_row_data('tbl_qbo_log', array('id' => $logID));
        if(!empty($logData)){
            

            if ($logData['type'] == 'invoice') {
                
                if($logData['syncType'] == 'QC'){
                    $args['invoiceID'] = $logData['qbActionID'];
                    $this->qboOBJ->get_invoice_data($args);
                }elseif($logData['syncType'] == 'CQ'){
                    
                    $logData = $this->general_model->get_row_data('QBO_test_invoice', array('invoiceID' => $logData['qbActionID'],'merchantID' => $logData['merchantID']));

                    $returnObj = $this->qboOBJ->sync_invoice_data($logData);
                    
                } 

            }else if ($logData['type'] == 'customer'){

                if($logData['syncType'] == 'QC'){
                    $this->qboOBJ->get_customer_data();
                }elseif($logData['syncType'] == 'CQ'){
                    
                    $customerData = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $logData['qbActionID'],'merchantID' => $logData['merchantID']));
                    $returnObj = $this->qboOBJ->sync_customer_data($customerData);
                }
               
            }else if ($logData['type'] == 'product') {

                if($logData['syncType'] == 'QC'){
                    $this->qboOBJ->get_items_data();
                }elseif($logData['syncType'] == 'CQ'){
                    $productData = $this->general_model->get_row_data('QBO_test_item', array('productID' => $logData['qbActionID'],'merchantID' => $logData['merchantID']));
                    $returnObj = $this->qboOBJ->sync_product_data($productData);
                }
               
            }else if ($logData['type'] == 'transaction') {

                if($logData['syncType'] == 'QC'){
                    
                }elseif($logData['syncType'] == 'CQ'){
                    $txnData = $this->general_model->get_row_data('customer_transaction', array('transactionID' => $logData['transactionID'],'merchantID' => $logData['merchantID']));

                    $invData = $this->general_model->get_row_data('QBO_test_invoice', array('invoiceID' => $logData['qbActionID'],'merchantID' => $logData['merchantID']));

                    $returnObj = $this->qboOBJ->sync_transaction_data($invData,$txnData);
                }
               
            }else{
                 $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Selection</strong></div>');
            }
            
            if($returnObj['status'] = 1){
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $logID),array('qbStatus' => 1,'qbText' => 'success'));
            }if($returnObj['status'] = 0){
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $logID),array('qbStatus' => 0,'qbText' => $returnObj['message']));
            }else{

            }

            
        }

        
        
        
        redirect(base_url('QBO_controllers/home/qbo_log'), 'refresh');
    }
    public function batchReciept()
    {
        

        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');
            $user_id                = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
        }

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();

        $page_data = $this->session->userdata('batch_process_receipt_data'); 
        
        $invoices = $page_data['data'];
        $invoiceObj = [];
        if(count($invoices) > 0){
            foreach ($invoices as $value) {
                
                $invoiceObj[] = $this->general_model->getInvoiceBatchTransactionList($value['invoice_id'],$value['customerID'],$value['transactionRowID'],1);

                # code...
            }
        }
        
        $data['allTransaction'] = $invoiceObj;
        $data['statusCode'] = array('200','100','111','1','102','120');
        $data['integrationType'] = 1;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/batch_invoice_reciept', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
    public function getAnnualRevenue($user_id){
        $get_result = $this->qbo_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            $result_set = array();
            $result_value1 = array();
            $result_value2 = array();
            $result_online_value = array();
            $result_online_month = array();
            $result_eCheck_value = array();
            $result_eCheck_month = array();
            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);
                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);
                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
            }
            $ob[] = [];
            $obRevenu = [];
            $obOnline = [];
            $obeCheck = [];
            $in = 0;

            foreach ($result_value1 as $value) {
                $custmonths = date("M", strtotime($value));
                
                $ob1 = [];
                $obR = [];
                $obON = [];
                $obEC = [];
                $inc = strtotime($value);
                $ob1[0] = $inc;
                $obR[] = $inc;
                $obR[] = $custmonths;
                $ob1[1] = $result_value2[$in];
                $ob[] = $ob1;
                $obON[0] = $inc;
                $obON[1] = $result_online_value[$in];
                $obOnline[] = $obON;
                $obEC[0] = $inc;
                $obEC[1] = $result_eCheck_value[$in];
                $obeCheck[] = $obEC;
                $obRevenu[] = $obR; 
                $in++;
            }
            $opt_array['revenu_month'] =   $obRevenu;
            $opt_array['revenu_volume'] =   $ob;
            $opt_array['online_month'] =   $result_online_month;
            $opt_array['online_volume'] =   $obOnline; 
            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $obeCheck;
            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];
            $opt_array['totalCCA'] =   $get_result['totalCCA'];
            $opt_array['totalECLA'] =   $get_result['totalECLA'];
            return ($opt_array);
        }
        return [];
    }
    
}
class Client
{
    private $client_id;
    private $client_secret;

    /**
     * HTTP Methods
     */
    const HTTP_METHOD_GET    = 'GET';
    const HTTP_METHOD_POST   = 'POST';
    const HTTP_METHOD_PUT    = 'PUT';
    const HTTP_METHOD_DELETE = 'DELETE';
    const HTTP_METHOD_HEAD   = 'HEAD';
    const HTTP_METHOD_PATCH  = 'PATCH';

    public function __construct($client_id, $client_secret)
    {
        if (!extension_loaded('curl')) {
            throw new Exception('The PHP exention curl must be installed to use this library.', Exception::CURL_NOT_FOUND);
        }
        if (!isset($client_id) || !isset($client_secret)) {
            throw new Exception('The App key must be set.', Exception::InvalidArgumentException);
        }

        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;

    }

    private function generateAccessTokenHeader($access_token)
    {
        $authorizationheader = 'Bearer ' . $access_token;
        return $authorizationheader;
    }

    public function getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state)
    {
        $parameters = array(
            'client_id'     => $this->client_id,
            'scope'         => $scope,
            'redirect_uri'  => $redirect_uri,
            'response_type' => $response_type,
            'state'         => $state,
            //The include_granted_scope is always set to false. No need to pass.
            //'include_granted_scope' => $include_granted_scope
        );
        $authorizationRequestUrl .= '?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
        return $authorizationRequestUrl;
    }

    public function getAccessToken($tokenEndPointUrl, $code, $redirectUrl, $grant_type)
    {
        if (!isset($grant_type)) {
            throw new InvalidArgumentException('The grant_type is mandatory.', InvalidArgumentException::INVALID_GRANT_TYPE);
        }

        $parameters = array(
            'grant_type'   => $grant_type,
            'code'         => $code,
            'redirect_uri' => $redirectUrl,
        );
        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );

        //Try catch???
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    public function revokeToken($tokenEndPointUrl, $grant_type, $access_token)
    {
        $parameters = array(
            'token' => $access_token,
        );

        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }



    private function generateAuthorizationHeader()
    {
        $encodedClientIDClientSecrets = base64_encode($this->client_id . ':' . $this->client_secret);
        $authorizationheader          = 'Basic ' . $encodedClientIDClientSecrets;
        return $authorizationheader;
    }

    private function executeRequest($url, $parameters = array(), $http_header, $http_method)
    {

        $curl_options = array();

        switch ($http_method) {
            case self::HTTP_METHOD_GET:
                $curl_options[CURLOPT_HTTPGET] = 'true';
                if (is_array($parameters) && count($parameters) > 0) {
                    $url .= '?' . http_build_query($parameters);
                } elseif ($parameters) {
                    $url .= '?' . $parameters;
                }
                break;
            case self::HTTP_METHOD_POST:
                $curl_options[CURLOPT_POST] = '1';
                if (is_array($parameters) && count($parameters) > 0) {
                    $body                             = http_build_query($parameters);
                    $curl_options[CURLOPT_POSTFIELDS] = $body;
                }
                break;
            default:
                break;
        }

        if (is_array($http_header)) {
            $header = array();
            foreach ($http_header as $key => $value) {
                $header[] = "$key: $value";
            }
            $curl_options[CURLOPT_HTTPHEADER] = $header;
        }

        $curl_options[CURLOPT_URL] = $url;
        $ch                        = curl_init();

        curl_setopt_array($ch, $curl_options);

        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        //Don't display, save it on result
        //curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Execute the Curl Request
        $result = curl_exec($ch);

        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        if ($curl_error = curl_error($ch)) {
            throw new Exception($curl_error);
        } else {
            $json_decode = json_decode($result, true);
        }
        
        curl_close($ch);
        return $json_decode;
    }
    
    
}