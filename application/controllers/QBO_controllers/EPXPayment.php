<?php

/**
 * This Controller has iTransact Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\RefundReceipt;

include APPPATH . 'third_party/EPX.class.php';

class EPXPayment extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    private $merchantID;
    public function __construct()
    {
        parent::__construct();

        $this->load->model('quickbooks');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('QBO_models/qbo_customer_model');
        $this->load->model('customer_model');
        $this->load->config('EPX');
        $this->db1 = $this->load->database('otherdb', true);
        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
            $merchID          = $logged_in_data['merchID'];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
        $this->merchantID         = $merchID;
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('QBO_controllers/home', 'refresh');
    }

    public function pay_invoice()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        $this->session->unset_userdata("in_data");
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $invoiceID = $this->input->post('invoiceProcessID');
        $user_id   = $this->merchantID;
		$checkPlan = check_free_plan_transactions();

        $cardID = $this->input->post('CardID');
        if (!$cardID || empty($cardID)) {
            $cardID = $this->input->post('schCardID');
        }
        $responseId = $transactionID = 'TXNFAILED'.time();
        $gatlistval = $this->input->post('sch_gateway');
        if (!$gatlistval || empty($gatlistval)) {
            $gatlistval = $this->input->post('gateway');
        }

        $gateway = $gatlistval;
        $custom_data_fields = [];
        $cusproID   = array();
        $error      = array();
        $cusproID   = $this->input->post('customerProcessID');
        $sch_method = $this->input->post('sch_method');

        $resellerID = $this->resellerID;
        if ($this->input->post('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        if ($checkPlan && $cardID != "" && $gateway != "") {
            $in_data = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
            $merchantID = $merchID;

            $val = array(
                'merchantID' => $merchID,
            );
            $data = $this->general_model->get_row_data('QBO_token', $val);

            $accessToken  = $data['accessToken'];
            $refreshToken = $data['refreshToken'];
            $realmID      = $data['realmID'];
            $dataService  = DataService::Configure(array(
                'auth_mode'       => $this->config->item('AuthMode'),
                'ClientID'        => $this->config->item('client_id'),
                'ClientSecret'    => $this->config->item('client_secret'),
                'accessTokenKey'  => $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID'      => $realmID,
                'baseUrl'         => $this->config->item('QBOURL'),
            ));

            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

            if (!empty($in_data)) {
                $customerID = $in_data['CustomerListID'];

                $comp_data       = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchID));

                $c_data    = $this->general_model->get_select_data('QBO_custom_customer', array('companyID','firstName','lastName', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchID));
                $companyID = $c_data['companyID'];

                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                    if (!empty($cardID)) {

                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];

                        $amount     = $this->input->post('inv_amount');
                        $theInvoice = current($targetInvoiceArray);
                        

                        $error = $dataService->getLastError();
                        if ($error != null) {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error in invoice payment process</strong></div>');
                            redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                        }

                        $amount = $amount - $cr_amount;

                        $crtxnID = '';
                        $inID    = array();

                        $name    = $in_data['fullName'];
                        $address = $in_data['ShipAddress_Addr1'];
                        $city    = $in_data['ShipAddress_City'];
                        $state   = $in_data['ShipAddress_State'];
                        $zipcode = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';

                        $achValue = false;

                        $CUST_NBR = $gt_result['gatewayUsername'];
                        $MERCH_NBR = $gt_result['gatewayPassword'];
                        $DBA_NBR = $gt_result['gatewaySignature'];
                        $TERMINAL_NBR = $gt_result['extra_field_1'];
                        $orderId = time();
                        
                        $amount = number_format($amount,2,'.','');
                        $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',
                        );
                        if($c_data['firstName'] != ''){
                            $transaction['firstName'] = $c_data['firstName'];
                        }
                        if($c_data['lastName'] != ''){
                            $transaction['lastName'] = $c_data['lastName'];
                        }
                        if ($sch_method == "1") {
                            $achValue = false;

                            if ($cardID != "new1") {

                                $card_data = $this->card_model->get_single_card_data($cardID);
                                $card_no   = $card_data['CardNo'];
                                $expmonth  = $card_data['cardMonth'];
                                $exyear    = $card_data['cardYear'];
                                $cvv       = $card_data['CardCVV'];
                                $cardType  = $card_data['CardType'];
                                $address1  = $card_data['Billing_Addr1'];
                                $address2  = $card_data['Billing_Addr2'];
                                $city      = $card_data['Billing_City'];
                                $zipcode   = $card_data['Billing_Zipcode'];
                                $state     = $card_data['Billing_State'];
                                $country   = $card_data['Billing_Country'];
                                $phone    = $card_data['Billing_Contact'];
                            } else {

                                $card_no  = $this->input->post('card_number');
                                $expmonth = $this->input->post('expiry');
                                $exyear   = $this->input->post('expiry_year');
                                $cardType = $this->general_model->getType($card_no);
                                $cvv      = '';
                                if ($this->input->post('cvv') != "") {
                                    $cvv = $this->input->post('cvv');
                                }

                                $address1 = $this->input->post('address1');
                                $address2 = $this->input->post('address2');
                                $city     = $this->input->post('city');
                                $country  = $this->input->post('country');
                                $phone    = $this->input->post('contact');
                                $state    = $this->input->post('state');
                                $zipcode  = $this->input->post('zipcode');
                            }
                            $cardType = $this->general_model->getType($card_no);
                            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                    

                            
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $exyear1  = substr($exyear, 2);
                            $transaction['EXP_DATE'] = $exyear1.$expmonth;
                            $transaction['ACCOUNT_NBR'] = $card_no;
                            $transaction['TRAN_TYPE'] = 'CCE1';
                            $transaction['CARD_ENT_METH'] = 'E';
                            $transaction['INDUSTRY_TYPE'] = 'E';

                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                            

                        } else if ($sch_method == "2") {
                            $achValue = true;
                            $cardType = 'Check';
                            if ($cardID == 'new1') {
                                $accountDetails = [
                                    'accountName'        => $this->input->post('acc_name'),
                                    'accountNumber'      => $this->input->post('acc_number'),
                                    'routeNumber'        => $this->input->post('route_number'),
                                    'accountType'        => $this->input->post('acct_type'),
                                    'accountHolderType'  => $this->input->post('acct_holder_type'),
                                    'Billing_Addr1'      => $this->input->post('address1'),
                                    'Billing_Addr2'      => $this->input->post('address2'),
                                    'Billing_City'       => $this->input->post('city'),
                                    'Billing_Country'    => $this->input->post('country'),
                                    'Billing_Contact'    => $this->input->post('contact'),
                                    'Billing_State'      => $this->input->post('state'),
                                    'Billing_Zipcode'    => $this->input->post('zipcode'),
                                    'customerListID'     => $customerID,
                                    'companyID'          => $companyID,
                                    'merchantID'         => $user_id,
                                    'createdAt'          => date("Y-m-d H:i:s"),
                                    'secCodeEntryMethod' => $this->input->post('secCode'),
                                ];
                            } else {
                                $accountDetails = $this->card_model->get_single_card_data($cardID);
                            }
                            $accountNumber = $accountDetails['accountNumber'];
                            $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                            $custom_data_fields['payment_type'] = $friendlyname;
                            $transaction['RECV_NAME'] = $accountDetails['accountName'];
                            $transaction['ACCOUNT_NBR'] = $accountDetails['accountNumber'];
                            $transaction['ROUTING_NBR'] = $accountDetails['routeNumber'];

                            if($accountDetails['accountType'] == 'savings'){
                                $transaction['TRAN_TYPE'] = 'CKS2';
                            }else{
                                $transaction['TRAN_TYPE'] = 'CKC2';
                            }
                            if($accountDetails['Billing_Addr1'] != ''){
                                $transaction['ADDRESS'] = $accountDetails['Billing_Addr1'];
                            }
                            if($accountDetails['Billing_City'] != ''){
                                $transaction['CITY'] = $accountDetails['Billing_City'];
                            }
                            if( $accountDetails['Billing_Zipcode'] != ''){
                                $transaction['ZIP_CODE'] = $accountDetails['Billing_Zipcode'];
                            }
                            
                        }

                        

                        $gatewayTransaction              = new EPX();
                        $result = $gatewayTransaction->processTransaction($transaction);

                        if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                        {
                            $responseId = $transactionID = $result['AUTH_GUID'];

                            $createPaymentObject = [
                                "TotalAmt"    => $amount,
                                "SyncToken"   => 1,
                                "CustomerRef" => $customerID,
                                "Line"        => [
                                    "LinkedTxn" => [
                                        "TxnId"   => $invoiceID,
                                        "TxnType" => "Invoice",
                                    ],
                                    "Amount"    => $amount,
                                ],
                            ];

                            $paymentMethod = $this->general_model->qbo_payment_method($cardType, $user_id, $achValue);
                            if($paymentMethod){
                                $createPaymentObject['PaymentMethodRef'] = [
                                    'value' => $paymentMethod['payment_id']
                                ];
                            }

                            if($responseId != '')
                            {
                                $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                            }

                            $newPaymentObj = Payment::create($createPaymentObject);
                            $savedPayment = $dataService->Add($newPaymentObj);

                            $error = $dataService->getLastError();
                            if ($error != null) {
                                $er = '';
                                $er .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                $er .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                $er .= "The Response message is: " . $error->getResponseBody() . "\n";
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error' . $er . ' </strong>.</div>');

                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $responseId;
                            } else {
                               
                                $crtxnID        = $savedPayment->Id;
                                $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                $ref_number     = $in_data['refNumber'];
                                $tr_date        = date('Y-m-d H:i:s');
                                $toEmail        = $c_data['userEmail'];
                                $company        = $c_data['companyName'];
                                $customer       = $c_data['fullName'];


                               
                                $st     = '1';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success";
                                $qbID   = $responseId;

                                
                                $this->session->set_flashdata('success', 'Success </strong></div>');
                                
                            }

                            if ($cardID == "new1") {
                                if ($sch_method == "1") {

                                    $card_no  = $this->input->post('card_number');
                                    $cardType = $this->general_model->getType($card_no);

                                    $expmonth = $this->input->post('expiry');
                                    $exyear   = $this->input->post('expiry_year');
                                    $cvv      = $this->input->post('cvv');

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $cardType,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'Billing_Addr1'   => $this->input->post('address1'),
                                        'Billing_Addr2'   => $this->input->post('address2'),
                                        'Billing_City'    => $this->input->post('city'),
                                        'Billing_Country' => $this->input->post('country'),
                                        'Billing_Contact' => $this->input->post('contact'),
                                        'Billing_State'   => $this->input->post('state'),
                                        'Billing_Zipcode' => $this->input->post('zipcode'),
                                        'customerListID'  => $customerID,

                                        'companyID'       => $companyID,
                                        'merchantID'      => $user_id,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                } else if ($sch_method == "2") {
                                    $id1 = $this->card_model->process_ack_account($accountDetails);
                                }
                            }
                        } else {
                            $err_msg      = $result['AUTH_RESP_TEXT'];

                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                        }
                        $qbID   = $invoiceID;
                        
                        $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' =>$responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        if($syncid){
                            $qbSyncID = 'CQ-'.$syncid;

                            $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                            
                            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                        }

                        $result['transactionid'] = $responseId;
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $invoiceID, $achValue, $this->transactionByUser, $custom_data_fields);

                        
                        if (isset($st) && $st == 1 && $chh_mail == '1') {
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $responseId);
                        }
                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                    }
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice payment process try again.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if ($cusproID == "2") {
            redirect('QBO_controllers/home/view_customer/' . $customerID, 'refresh');
        }

        if ($cusproID == "3") {
            redirect('QBO_controllers/Create_invoice/invoice_details_page/' . $in_data['invoiceID'], 'refresh');
        }

        $invoice_IDs  = array();
        $receipt_data = array(
            'proccess_url'      => 'QBO_controllers/Create_invoice/Invoice_details',
            'proccess_btn_text' => 'Process New Invoice',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);
        $this->session->set_userdata("in_data", $in_data);
        if ($cusproID == "1") {
            redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
        }

        redirect('QBO_controllers/home/transation_receipt/' . $in_data['invoiceID'] . '/' . $invoiceID . '/' . $responseId, 'refresh');
    }

    public function create_customer_sale()
    {

        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }
            $responseId = $transactionID = 'TXNFAILED'.time();
            $inputData = $this->input->post();
            $inv_array   = array();
            $inv_invoice = array();
            $gatlistval  = $this->input->post('gateway_list');
            $gt_result   = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();

            $custom_data_fields = [];
            $applySurcharge = false;
            if (!empty($this->input->post('invoice_id'))) {
                $applySurcharge = true;
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_id');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                if ($this->session->userdata('logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
                }
                if ($this->session->userdata('user_logged_in')) {
                    $user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
                }
                $resellerID = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchantID))['resellerID'];
                $customerID = $this->input->post('customerID');
                $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID','firstName','lastName', 'companyName', 'fullName', 'userEmail'), array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $orderId = time();

                $cardID = $this->input->post('card_list');
                if ($this->input->post('card_number') != "") {

                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');

                    $exyear = $this->input->post('expiry_year');
                    $expry  = $expmonth . $exyear;
                    $cvv    = $this->input->post('cvv');

                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];
                    $cvv       = $card_data['CardCVV'];
                }
                /*Added card type in transaction table*/
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $phone = $this->input->post('phone');

                $name     = $this->input->post('firstName') . ' ' . $this->input->post('lastName');
                $address1 = $this->input->post('baddress1');
                $address2 = $this->input->post('baddress2');
                $country  = $this->input->post('bcountry');
                $city     = $this->input->post('bcity');
                $state    = $this->input->post('bstate');
                $amount   = $this->input->post('totalamount');
                // update amount with surcharge 
                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
                    $amount += round($surchargeAmount, 2);
                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
                    
                }
                $totalamount  = $amount;
                $zipcode  = ($this->input->post('bzipcode')) ? $this->input->post('bzipcode') : '74035';
                $amount = number_format($amount,2,'.','');

                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE1',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                if($inputData['firstName'] != ''){
                    $transaction['FIRST_NAME'] = $inputData['firstName'];
                }
                if($inputData['lastName'] != ''){
                    $transaction['LAST_NAME'] = $inputData['lastName'];
                }
                
                if($cvv && !empty($cvv)){
                    $transaction['CVV2'] = $cvv;
                }
                if($inputData['baddress1'] != ''){
                    $transaction['ADDRESS'] = $inputData['baddress1'];
                }
                if($inputData['bcity'] != ''){
                    $transaction['CITY'] = $inputData['bcity'];
                }
                
                if($inputData['bzipcode'] != ''){
                    $transaction['ZIP_CODE'] = $inputData['bzipcode'];
                }
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                $crtxnID = '';
                $invID   = '';
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);


                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {

                    $message = $result['AUTH_RESP_TEXT'];
                    $trID = $responseId = $transactionID = $result['AUTH_GUID'];

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }

                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));
                    $refNum = array();
                    $qblist = array();
                    
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();
                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con = array('invoiceID' => $inID, 'merchantID' => $merchantID);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            
                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;


                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

                                $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
                                $actualInvoicePayAmount += $surchargeAmount;
                                $amount_data += $surchargeAmount;
                                $updatedInvoiceData = [
                                    'inID' => $inID,
                                    'targetInvoiceArray' => $targetInvoiceArray,
                                    'merchantID' => $user_id,
                                    'dataService' => $dataService,
                                    'realmID' => $realmID,
                                    'accessToken' => $accessToken,
                                    'refreshToken' => $refreshToken,
                                    'amount' => $surchargeAmount,
                                ];
                                $this->general_model->updateSurchargeInvoice($updatedInvoiceData,1);
                            }
                            $ispaid      = 0;
                            $isRun = 0;
                            if($saleAmountRemaining > 0){
                                $BalanceRemaining = 0.00;
                                if($amount_data == $actualInvoicePayAmount){
                                    $actualInvoicePayAmount = $amount_data;
                                    $isPaid      = 1;

                                }else{

                                    $actualInvoicePayAmount = $actualInvoicePayAmount;
                                    $isPaid      = 0;
                                    $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                    
                                }
                                $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;

                                $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);

                                
                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);

                                    $createPaymentObject = [
                                        "TotalAmt" => $actualInvoicePayAmount,
                                        "SyncToken" => 1, 
                                        "CustomerRef" => $customerID,
                                        "Line" => [
                                            "LinkedTxn" => [
                                                "TxnId" => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount" => $actualInvoicePayAmount
                                        ]
                                    ];

                                    $paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchantID, false);
                                    if($paymentMethod){
                                        $createPaymentObject['PaymentMethodRef'] = [
                                            'value' => $paymentMethod['payment_id']
                                        ];
                                    }

                                    if($trID != '')
                                    {
                                        $createPaymentObject['PaymentRefNum'] = substr($trID, 0, 21);
                                    }
                                    $newPaymentObj = Payment::create($createPaymentObject);

                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' .   $err . '</div>');

                                        $st = '0';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success but " . $error->getResponseBody();
                                        $qbID = $trID;
                                        $pinv_id = '';
                                    } else {
                                        $pinv_id = '';
                                        $crtxnID = $pinv_id = $savedPayment->Id;
                                        $st = '1';
                                        $action = 'Pay Invoice';
                                        $msg = "Payment Success";
                                        $qbID = $trID;
                                    }
                                    $qbID = $inID;
                                    $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $trID,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    if($syncid){
                                        $qbSyncID = 'CQ-'.$syncid;

                                        $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';

                                        $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                    }
                                    $result['transactionid'] = $responseId;
                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $merchantID, $pinv_id, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
                                }
                            }
                            $payIndex++;

                        }
                        
                       
                    } else {
                        $crtxnID = '';
                        $inID    = '';
                        $result['transactionid'] = $responseId;
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                    }

                    /* This block is created for saving Card info in encrypted form  */

                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no = $this->input->post('card_number');

                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv       = $this->input->post('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $address1,
                            'Billing_Addr2'   => $address2,
                            'Billing_City'    => $city,
                            'Billing_State'   => $state,
                            'Billing_Country' => $country,
                            'Billing_Contact' => $phone,
                            'Billing_Zipcode' => $zipcode,
                        );

                        $this->card_model->process_card($card_data);
                    }

                    if ($chh_mail == '1') {
                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                        if (!empty($refNum)) {
                            $ref_number = implode(',', $refNum);
                        } else {
                            $ref_number = '';
                        }

                        $tr_date  = date('Y-m-d H:i:s');
                        $toEmail  = $comp_data['userEmail'];
                        $company  = $comp_data['companyName'];
                        $customer = $comp_data['fullName'];

                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, $trID);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg      = $result['AUTH_RESP_TEXT'];
                    if(!isset($result['data'])){
						$result['data'] = [
							'id' => '',
							'response_code' => 400
						]; 
					}
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');

                    $crtxnID = '';
                    $inID    = '';
                    $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();
        if (!empty($this->input->post('invoice_id'))) {
            $invoice_IDs = explode(',', $this->input->post('invoice_id'));
        }

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => $_SERVER['REMOTE_ADDR'],
            'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'billing_address1'  => $this->input->post('baddress1'),
            'billing_address2'  => $this->input->post('baddress2'),
            'billing_city'      => $this->input->post('bcity'),
            'billing_zip'       => $this->input->post('bzipcode'),
            'billing_state'     => $this->input->post('bstate'),
            'billing_country'   => $this->input->post('bcountry'),
            'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'shipping_address1' => $this->input->post('address1'),
            'shipping_address2' => $this->input->post('address2'),
            'shipping_city'     => $this->input->post('city'),
            'shipping_zip'      => $this->input->post('zipcode'),
            'shipping_state'    => $this->input->post('state'),
            'shiping_counry'    => $this->input->post('country'),
            'Phone'             => '',
            'Contact'           => $this->input->post('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_sale',
            'proccess_btn_text' => 'Process New Sale',
            'sub_header'        => 'Sale',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_esale()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $custom_data_fields = [];
            // get custom field data
            if (!empty($this->input->post('invoice_id'))) {
                $custom_data_fields['invoice_number'] = $this->input->post('invoice_id');
            }

            if (!empty($this->input->post('po_number'))) {
                $custom_data_fields['po_number'] = $this->input->post('po_number');
            }

            $user_id = $merchantID;
            $responseId = 'TXNFAILED-'.time();
            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $amount = $this->input->post('totalamount');
            $name   = $this->input->post('firstName') . " " . $this->input->post('lastName');
            $firstName = ($this->input->post('firstName') != '')?$this->input->post('firstName'):'None';
            $lastName = ($this->input->post('lastName') != '')?$this->input->post('lastName'):'None';
            $address1 = ($this->input->post('baddress1') != '')?$this->input->post('baddress1'):'';
            $zipcode = ($this->input->post('bzipcode') != '')?$this->input->post('bzipcode'):'';
            $city = ($this->input->post('bcity') != '')?$this->input->post('bcity'):'';
            
            $checkPlan = check_free_plan_transactions();

            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $customerID   = $this->input->post('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->input->post('customerID'), 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $payableAccount = $this->input->post('payable_ach_account');
                $sec_code       = 'WEB';

                if ($payableAccount == '' || $payableAccount == 'new1') {
                    $accountDetails = [
                        'accountName'        => $this->input->post('account_name'),
                        'accountNumber'      => $this->input->post('account_number'),
                        'routeNumber'        => $this->input->post('route_number'),
                        'accountType'        => $this->input->post('acct_type'),
                        'accountHolderType'  => $this->input->post('acct_holder_type'),
                        'Billing_Addr1'      => $this->input->post('baddress1'),
                        'Billing_Addr2'      => $this->input->post('baddress2'),
                        'Billing_City'       => $this->input->post('bcity'),
                        'Billing_Country'    => $this->input->post('bcountry'),
                        'Billing_Contact'    => $this->input->post('phone'),
                        'Billing_State'      => $this->input->post('bstate'),
                        'Billing_Zipcode'    => $this->input->post('bzipcode'),
                        'customerListID'     => $customerID,
                        'companyID'          => $companyID,
                        'merchantID'         => $merchantID,
                        'createdAt'          => date("Y-m-d H:i:s"),
                        'secCodeEntryMethod' => $sec_code,
                    ];
                } else {
                    $accountDetails = $this->card_model->get_single_card_data($payableAccount);
                }
                $accountNumber = $accountDetails['accountNumber'];
                $friendlyname = 'Echeck' . ' - ' . substr($accountNumber, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $amount = number_format($amount,2,'.','');
                if($accountDetails['accountType'] == 'savings'){
                    $txnType = 'CKS2'; 
                }else{
                    $txnType = 'CKC2';
                }
                $orderId = time();


                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => $txnType,
                    'ACCOUNT_NBR' => $accountDetails['accountNumber'],
                    'ROUTING_NBR' => $accountDetails['routeNumber'],
                    'RECV_NAME' => $accountDetails['accountName'],
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'ORDER_NBR' => ($this->input->post('po_number') != null)?$this->input->post('po_number'):time(),
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',


                );
                if($firstName != ''){
                    $transaction['FIRST_NAME'] = $firstName;
                }
                if($lastName != ''){
                    $transaction['LAST_NAME'] = $lastName;
                }
                if(!empty($this->input->post('invoice_id'))){
                    $new_invoice_number = getInvoiceOriginalID($this->input->post('invoice_id'), $merchantID, 2);
                    $transaction['ORDER_NBR'] = $new_invoice_number;
                }
                if($address1 != ''){
                    $transaction['ADDRESS'] = $address1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
                $gatewayTransaction = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);
                $invoiceIDs = [];
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];

                    $invoiceIDs = array();
                    $invoicePayAmounts = array();
                    if (!empty($this->input->post('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->input->post('invoice_id'));
                        $invoicePayAmounts = explode(',', $this->input->post('invoice_pay_amount'));
                    }

                    $val       = array('merchantID' => $user_id);
                    $tokenData = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $tokenData['accessToken'];
                    $refreshToken = $tokenData['refreshToken'];
                    $realmID      = $tokenData['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));

                    $refNumber = array();
                    $merchantID = $user_id;
                    if (!empty($invoiceIDs)) {
                        $payIndex = 0;
                        $saleAmountRemaining = $amount;
                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                            $con         = array('invoiceID' => $inID, 'merchantID' => $user_id);
                            $res1 = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment','AppliedAmount'), $con);

                            $refNumber[]   =  $res1['refNumber'];
                            $txnID      = $inID;
                            $pay_amounts = 0;
                            $amount_data = $res1['BalanceRemaining'];
                            $actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
                            $ispaid      = 0;
                            $isRun = 0;
                            $BalanceRemaining = 0.00;
                            if($amount_data == $actualInvoicePayAmount){
                                $actualInvoicePayAmount = $amount_data;
                                $isPaid      = 1;

                            }else{

                                $actualInvoicePayAmount = $actualInvoicePayAmount;
                                $isPaid      = 0;
                                $BalanceRemaining = $amount_data - $actualInvoicePayAmount;
                                
                            }
                            $AppliedAmount = $res1['AppliedAmount'] + $actualInvoicePayAmount;
                            $data        = array('IsPaid' => $ispaid, 'BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount);
                            $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                $theInvoice = current($targetInvoiceArray);

                             

                                $createPaymentObject = [
                                    "TotalAmt"    => $actualInvoicePayAmount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $customerID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $inID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $actualInvoicePayAmount,
                                    ],
                                ];

                                $paymentMethod = $this->general_model->qbo_payment_method('Check', $user_id, true);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }

                                if($responseId != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);

                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = '';
                                    $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' . $err . '</div>');

                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();

                                    $qbID    = $responseId;
                                    $pinv_id = '';
                                } else {
                                    $pinv_id = '';
                                    $pinv_id = $savedPayment->Id;
                                    $st      = '1';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success";
                                    $qbID    = $responseId;
                                }
                                $qbID = $inID;


                                $qbo_log = array('syncType' => 'CQ','type' => 'transaction','transactionID' => $responseId,'qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                if($syncid){
                                    $qbSyncID = 'CQ-'.$syncid;

                                    

                                    $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Create_invoice/invoice_details_page/'.$qbID.'">'.$qbID.'</a></span> ';
                                    
                                    $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
                                }
                                $result['transactionid'] = $responseId;
                                $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $actualInvoicePayAmount, $user_id, $pinv_id, $this->resellerID, $inID, true, $this->transactionByUser, $custom_data_fields);
                            }
                            $payIndex++;
                        }
                    } else {

                        $crtxnID = '';
                        $inID    = '';
                        $result['transactionid'] = $responseId;
                        $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);
                    }

                    if ($payableAccount == '' || $payableAccount == 'new1') {
                        $id1 = $this->card_model->process_ack_account($accountDetails);
                    }

                    $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                    $ref_number     = '';
                    $tr_date        = date('Y-m-d H:i:s');
                    $toEmail        = $comp_data['userEmail'];
                    $company        = $comp_data['companyName'];
                    $customer       = $comp_data['fullName'];

                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {
                    $err_msg      = $result['AUTH_RESP_TEXT'];
                    $result['transactionid'] = $responseId;
                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $user_id, '', $this->resellerID, $inID = '', true, $this->transactionByUser, $custom_data_fields);

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
            
            $invoice_IDs = array();
            if (!empty($this->input->post('invoice_id'))) {
                $invoice_IDs = explode(',', $this->input->post('invoice_id'));
            }

            $receipt_data = array(
                'transaction_id'    => $responseId,
                'IP_address'        => $_SERVER['REMOTE_ADDR'],
                'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'billing_address1'  => $this->input->post('baddress1'),
                'billing_address2'  => $this->input->post('baddress2'),
                'billing_city'      => $this->input->post('bcity'),
                'billing_zip'       => $this->input->post('bzipcode'),
                'billing_state'     => $this->input->post('bstate'),
                'billing_country'   => $this->input->post('bcountry'),
                'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
                'shipping_address1' => $this->input->post('address1'),
                'shipping_address2' => $this->input->post('address2'),
                'shipping_city'     => $this->input->post('city'),
                'shipping_zip'      => $this->input->post('zipcode'),
                'shipping_state'    => $this->input->post('state'),
                'shiping_counry'    => $this->input->post('country'),
                'Phone'             => $this->input->post('phone'),
                'Contact'           => $this->input->post('email'),
                'proccess_url'      => 'QBO_controllers/Payments/create_customer_esale',
                'proccess_btn_text' => 'Process New Sale',
                'sub_header'        => 'Sale',
                'checkPlan'         => $checkPlan
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);
            redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid request.</div>');
        }
        redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');
    }

    public function create_customer_auth()
    {
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {
            
            $inputData = $this->input->post();
            $responseId = 'TXNFAILED-'.time();
            $gatlistval = $this->input->post('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            $checkPlan = check_free_plan_transactions();
            $custom_data_fields = [];
            $po_number = $this->czsecurity->xssCleanPostInput('po_number');
            if (!empty($po_number)) {
                $custom_data_fields['po_number'] = $po_number;
            }
            if ($checkPlan && $gatlistval != "" && !empty($gt_result)) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $customerID = $this->input->post('customerID');

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customerID, 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];
                $cardID    = $this->input->post('card_list');

                if ($this->input->post('card_number') != "") {
                    $card_no  = $this->input->post('card_number');
                    $expmonth = $this->input->post('expiry');
                    $cvv      = $this->input->post('cvv');

                    $exyear = $this->input->post('expiry_year');
                    $expry  = $expmonth . $exyear;
                } else {

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $expmonth  = $card_data['cardMonth'];
                    $cvv       = $card_data['CardCVV'];

                    $exyear = $card_data['cardYear'];
                    
                }
                $cardType = $this->general_model->getType($card_no);
                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                $custom_data_fields['payment_type'] = $friendlyname;

                $exyear1  = substr($exyear, 2);
                if (strlen($expmonth) == 1) {
                    $expmonth = '0' . $expmonth;
                }
                $expry = $exyear1.$expmonth;

                $name      = $this->input->post('firstName') . ' ' . $this->input->post('lastName');
                $address   = $this->input->post('address');
                $baddress1 = $this->input->post('baddress1');
                $baddress2 = $this->input->post('baddress2');
                $country   = $this->input->post('bcountry');
                $city      = $this->input->post('bcity');
                $state     = $this->input->post('bstate');
                $amount    = $this->input->post('totalamount');
                $zipcode   = ($this->input->post('zipcode')) ? $this->input->post('zipcode') : '74035';
                $calamount = $amount * 100;

                $orderId = time();
                $amount = number_format($amount,2,'.','');
                $CUST_NBR = $gt_result['gatewayUsername'];
                $MERCH_NBR = $gt_result['gatewayPassword'];
                $DBA_NBR = $gt_result['gatewaySignature'];
                $TERMINAL_NBR = $gt_result['extra_field_1'];
                $transaction = array(
                    'CUST_NBR' => $CUST_NBR,
                    'MERCH_NBR' => $MERCH_NBR,
                    'DBA_NBR' => $DBA_NBR,
                    'TERMINAL_NBR' => $TERMINAL_NBR,
                    'TRAN_TYPE' => 'CCE2',
                    'ACCOUNT_NBR' => $card_no,
                    'EXP_DATE' => $expry,
                    'CARD_ENT_METH' => 'E',
                    'INDUSTRY_TYPE' => 'E',
                    'AMOUNT' => $amount,
                    'TRAN_NBR' => rand(1,10),
                    'INVOICE_NBR' => $orderId,
                    'BATCH_ID' => time(),
                    'VERBOSE_RESPONSE' => 'Y',
                );
                if (!empty($po_number)) {
                    $transaction['ORDER_NBR'] = $po_number;
                }
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                    $transaction['INVOICE_NBR'] = $this->czsecurity->xssCleanPostInput('invoice_number');
                }
                if($cvv != ''){
                    $transaction['CVV2'] = $cvv;
                }
                if($baddress1 != ''){
                    $transaction['ADDRESS'] = $baddress1;
                }
                if($city != ''){
                    $transaction['CITY'] = $city;
                }
                
                if($zipcode != ''){
                    $transaction['ZIP_CODE'] = $zipcode;
                }
               
                $gatewayTransaction              = new EPX();
                $result = $gatewayTransaction->processTransaction($transaction);

                $crtxnID = '';
                $invID = $inID = '';
                
                if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                {
                    $responseId = $result['AUTH_GUID'];

                    if ($cardID == "new1" && !($this->input->post('tc'))) {
                        $card_no   = $this->input->post('card_number');
                        $card_type = $this->general_model->getType($card_no);
                        $expmonth  = $this->input->post('expiry');

                        $exyear = $this->input->post('expiry_year');

                        $cvv       = $this->input->post('cvv');
                        $card_data = array(
                            'cardMonth'       => $expmonth,
                            'cardYear'        => $exyear,
                            'CardType'        => $card_type,
                            'CustomerCard'    => $card_no,
                            'CardCVV'         => $cvv,
                            'customerListID'  => $customerID,
                            'companyID'       => $companyID,
                            'merchantID'      => $merchantID,

                            'createdAt'       => date("Y-m-d H:i:s"),
                            'Billing_Addr1'   => $inputData['baddress1'],
                            'Billing_Addr2'   => $inputData['baddress2'],
                            'Billing_City'    => $inputData['bcity'],
                            'Billing_State'   => $inputData['bstate'],
                            'Billing_Country' => $inputData['bcountry'],
                            'Billing_Contact' => $inputData['phone'],
                            'Billing_Zipcode' => $inputData['zipcode'],
                        );
                        $custom_data_fields['card_type'] = $cardType;
                        $this->card_model->process_card($card_data);
                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');

                } else {
                    $err_msg = $result['AUTH_RESP_TEXT'];
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                }
                $result['transactionid'] = $responseId;
                $id = $this->general_model->insert_gateway_transaction_data($result, 'auth', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID, $this->resellerID, $inID, false, $this->transactionByUser, $custom_data_fields);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }
        }
        $invoice_IDs = array();

        $receipt_data = array(
            'transaction_id'    => $responseId,
            'IP_address'        => $_SERVER['REMOTE_ADDR'],
            'billing_name'      => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'billing_address1'  => $this->input->post('baddress1'),
            'billing_address2'  => $this->input->post('baddress2'),
            'billing_city'      => $this->input->post('bcity'),
            'billing_zip'       => $this->input->post('bzipcode'),
            'billing_state'     => $this->input->post('bstate'),
            'billing_country'   => $this->input->post('bcountry'),
            'shipping_name'     => $this->input->post('firstName') . ' ' . $this->input->post('lastName'),
            'shipping_address1' => $this->input->post('address1'),
            'shipping_address2' => $this->input->post('address2'),
            'shipping_city'     => $this->input->post('city'),
            'shipping_zip'      => $this->input->post('zipcode'),
            'shipping_state'    => $this->input->post('state'),
            'shiping_counry'    => $this->input->post('country'),
            'Phone'             => '',
            'Contact'           => $this->input->post('email'),
            'proccess_url'      => 'QBO_controllers/Payments/create_customer_auth',
            'proccess_btn_text' => 'Process New Transaction',
            'sub_header'        => 'Authorize',
            'checkPlan'         => $checkPlan
        );

        $this->session->set_userdata("receipt_data", $receipt_data);
        $this->session->set_userdata("invoice_IDs", $invoice_IDs);

        redirect('QBO_controllers/home/transation_sale_receipt', 'refresh');
    }

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address
        $this->session->unset_userdata("receipt_data");
        $this->session->unset_userdata("invoice_IDs");
        if (!empty($this->input->post())) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->input->post('txnID');
            $con     = array('transactionID' => $tID);
            $transactionid = $transactionID = 'TXNFAILED'.time();
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->input->post('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $customerID = $paydata['customerListID'];
            $amount     = $paydata['transactionAmount'];
            $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

            $CUST_NBR = $gt_result['gatewayUsername'];
            $MERCH_NBR = $gt_result['gatewayPassword'];
            $DBA_NBR = $gt_result['gatewaySignature'];
            $TERMINAL_NBR = $gt_result['extra_field_1'];
            $orderId = time();
            $amount = number_format($amount,2,'.','');

            $transaction = array(
                'CUST_NBR' => $CUST_NBR,
                'MERCH_NBR' => $MERCH_NBR,
                'DBA_NBR' => $DBA_NBR,
                'TERMINAL_NBR' => $TERMINAL_NBR,
                'ORIG_AUTH_GUID' => $tID,
                'TRAN_TYPE' => 'CCE4',
                'CARD_ENT_METH' => 'Z',
                'INDUSTRY_TYPE' => 'E',
                'AMOUNT' => $amount,
                'TRAN_NBR' => rand(1,10),
                'INVOICE_NBR' => $orderId,
                'BATCH_ID' => time(),
                'VERBOSE_RESPONSE' => 'Y',
            );
            $gatewayTransaction              = new EPX();
            $result = $gatewayTransaction->processTransaction($transaction);

            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
            {
                $transactionid = $transactionID = $result['AUTH_GUID'];
                /* This block is created for saving Card info in encrypted form  */

                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "4");

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);
                $condition  = array('transactionID' => $tID);
                $customerID = $paydata['customerListID'];
                $tr_date    = date('Y-m-d H:i:s');
                $ref_number = $tID;
                $toEmail    = $comp_data['userEmail'];
                $company    = $comp_data['companyName'];
                $customer   = $comp_data['fullName'];
                if ($chh_mail == '1') {

                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                }
                
                $this->session->set_flashdata('success', 'Successfully Captured Authorization');

            } else {
                $err_msg      = $result['AUTH_RESP_TEXT'];
                
                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
            }
            $result['transactionid'] = $transactionid;
            $id = $this->general_model->insert_gateway_transaction_data($result, 'capture', $gatlistval, $gt_result['gatewayType'], $customerID, $amount, $merchantID, $crtxnID = '', $this->resellerID, $inID = '', false, $this->transactionByUser, $custom_data_fields);

            $invoice_IDs = array();

            $receipt_data = array(
                'proccess_url'      => 'QBO_controllers/Payments/payment_capture',
                'proccess_btn_text' => 'Process New Transaction',
                'sub_header'        => 'Capture',
            );

            $this->session->set_userdata("receipt_data", $receipt_data);
            $this->session->set_userdata("invoice_IDs", $invoice_IDs);

            if ($paydata['invoiceTxnID'] == '') {
                $paydata['invoiceTxnID'] = 'null';
            }
            if ($paydata['customerListID'] == '') {
                $paydata['customerListID'] = 'null';
            }
            
            redirect('QBO_controllers/home/transation_credit_receipt/transaction/' . $paydata['customerListID'] . '/' . $transactionid, 'refresh');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    
    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function pay_multi_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID     = $this->input->post('CardID1');
        $gateway    = $this->input->post('gateway1');
        $merchantID = $merchID;

		$checkPlan = check_free_plan_transactions();
        $cusproID   = '';
        $custom_data_fields = [];
        $cusproID   = $this->input->post('customermultiProcessID');
        $error      = '';
        $resellerID = $this->resellerID;
        $responseId = $transactionid = 'TXNFAILED-'.time();

        if ($cardID != "" && $gateway != "") {
            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $invoices = $this->input->post('multi_inv');
            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {
                    $pay_amounts = $this->input->post('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'firstName', 'lastName', 'companyName'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];

                        if ($cardID == 'new1') {
                            $cardID_upd = $cardID;
                            $card_no    = $this->input->post('card_number');
                            $expmonth   = $this->input->post('expiry');
                            $exyear     = $this->input->post('expiry_year');
                            $cvv        = $this->input->post('cvv');
    
                            $address1 = $this->input->post('address1');
                            $address2 = $this->input->post('address2');
                            $city     = $this->input->post('city');
                            $country  = $this->input->post('country');
                            $phone    = $this->input->post('contact');
                            $state    = $this->input->post('state');
                            $zipcode  = $this->input->post('zipcode');
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];
    
                            $address1 = $card_data['Billing_Addr1'];
                            $address2 = $card_data['Billing_Addr2'];
                            $city     = $card_data['Billing_City'];
                            $zipcode  = $card_data['Billing_Zipcode'];
                            $state    = $card_data['Billing_State'];
                            $country  = $card_data['Billing_Country'];
                            $phone    = $card_data['Billing_Contact'];
                        }
                        $cardType = $this->general_model->getType($card_no);
                        $friendlyname = $cardType . ' - ' . substr($card_no, -4);
                        $custom_data_fields['payment_type'] = $friendlyname;

                        $cardType =  $this->general_model->getType($card_no);

                        if (!empty($cardID)) {

                            $cr_amount = 0;
                            $amount    = $pay_amounts;
                            $amount    = $amount - $cr_amount;

                            $name     = $in_data['fullName'];
                            $address  = $in_data['ShipAddress_Addr1'];
                            $address2 = $in_data['ShipAddress_Addr2'];
                            $city     = $in_data['ShipAddress_City'];
                            $state    = $in_data['ShipAddress_State'];
                            $zipcode  = ($in_data['ShipAddress_PostalCode']) ? $in_data['ShipAddress_PostalCode'] : '85284';
                            $exyear1 = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $exyear1.$expmonth;
                            $amount = number_format($amount,2,'.','');
                            $CUST_NBR = $gt_result['gatewayUsername'];
                            $MERCH_NBR = $gt_result['gatewayPassword'];
                            $DBA_NBR = $gt_result['gatewaySignature'];
                            $TERMINAL_NBR = $gt_result['extra_field_1'];
                            $orderId = time();
                            $transaction = array(
                                'CUST_NBR' => $CUST_NBR,
                                'MERCH_NBR' => $MERCH_NBR,
                                'DBA_NBR' => $DBA_NBR,
                                'TERMINAL_NBR' => $TERMINAL_NBR,
                                'TRAN_TYPE' => 'CCE1',
                                'ACCOUNT_NBR' => $card_no,
                                'EXP_DATE' => $expry,
                                'CARD_ENT_METH' => 'E',
                                'INDUSTRY_TYPE' => 'E',
                                'AMOUNT' => $amount,
                                'TRAN_NBR' => rand(1,10),
                                'INVOICE_NBR' => $invoiceID,
                                'ORDER_NBR' => $orderId,
                                'BATCH_ID' => time(),
                                'VERBOSE_RESPONSE' => 'Y',


                            );
                            if($c_data['firstName'] != ''){
                                $transaction['FIRST_NAME'] = $c_data['firstName'];
                            }
                            if($c_data['lastName'] != ''){
                                $transaction['LAST_NAME'] = $c_data['lastName'];
                            }
                            if($cvv != ''){
                                $transaction['CVV2'] = $cvv;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            
                            if($zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                           
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);

                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                $responseId = $transactionid = $result['AUTH_GUID'];

                                $val                   = array(
                                    'merchantID' => $merchID,
                                );
                                $data = $this->general_model->get_row_data('QBO_token', $val);

                                $accessToken  = $data['accessToken'];
                                $refreshToken = $data['refreshToken'];
                                $realmID      = $data['realmID'];
                                $dataService  = DataService::Configure(array(
                                    'auth_mode'       => $this->config->item('AuthMode'),
                                    'ClientID'        => $this->config->item('client_id'),
                                    'ClientSecret'    => $this->config->item('client_secret'),
                                    'accessTokenKey'  => $accessToken,
                                    'refreshTokenKey' => $refreshToken,
                                    'QBORealmID'      => $realmID,
                                    'baseUrl'         => $this->config->item('QBOURL'),
                                ));

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);
                                }
                               
                                $createPaymentObject = [
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => 1,
                                    "CustomerRef" => $Customer_ListID,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $invoiceID,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ];

                                $paymentMethod = $this->general_model->qbo_payment_method($cardType, $merchID, false);
                                if($paymentMethod){
                                    $createPaymentObject['PaymentMethodRef'] = [
                                        'value' => $paymentMethod['payment_id']
                                    ];
                                }
                                if($responseId != '')
                                {
                                    $createPaymentObject['PaymentRefNum'] = substr($responseId, 0, 21);
                                }

                                $newPaymentObj = Payment::create($createPaymentObject);
                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $responseId;
                                    $crtxnID        = $savedPayment->Id;

                                } else {
                                   
                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $responseId;
                                }

                                if ($cardID == "new1" && !($this->input->post('tc'))) {
                                    $card_no   = $this->input->post('card_number');
                                    $expmonth  = $this->input->post('expiry');
                                    $exyear    = $this->input->post('expiry_year');
                                    $cvv       = $this->input->post('cvv');
                                    $card_type = $this->general_model->getType($card_no);

                                    $card_data = array(
                                        'cardMonth'       => $expmonth,
                                        'cardYear'        => $exyear,
                                        'CardType'        => $card_type,
                                        'CustomerCard'    => $card_no,
                                        'CardCVV'         => $cvv,
                                        'customerListID'  => $Customer_ListID,
                                        'companyID'       => $companyID,
                                        'merchantID'      => $merchID,

                                        'createdAt'       => date("Y-m-d H:i:s"),
                                        'Billing_Addr1'   => $address1,
                                        'Billing_Addr2'   => $address2,
                                        'Billing_City'    => $city,
                                        'Billing_State'   => $state,
                                        'Billing_Country' => $country,
                                        'Billing_Contact' => $phone,
                                        'Billing_Zipcode' => $zipcode,
                                    );

                                    $id1 = $this->card_model->process_card($card_data);
                                }

                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Failed";
                                $qbID   = $invoiceID;

                                $err_msg      = $result['AUTH_RESP_TEXT'];

                                $this->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
                            }
                            $result['transactionid'] = $transactionid;
                            $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $Customer_ListID, $amount, $merchID, $crtxnID, $resellerID, $invoiceID, false, $this->transactionByUser, $custom_data_fields);

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }

                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Select Gateway and Card.</div>');
        }

        if(!$checkPlan){
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'QBO_controllers/home/my_account">Click here</a> to upgrade.</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

}
