<?php
class Client
{
    private $client_id;
    private $client_secret;

    /**
     * HTTP Methods
     */
    const HTTP_METHOD_GET    = 'GET';
    const HTTP_METHOD_POST   = 'POST';
    const HTTP_METHOD_PUT    = 'PUT';
    const HTTP_METHOD_DELETE = 'DELETE';
    const HTTP_METHOD_HEAD   = 'HEAD';
    const HTTP_METHOD_PATCH  = 'PATCH';

    public function __construct($client_id, $client_secret)
    {
        if (!extension_loaded('curl')) {
            throw new Exception('The PHP exention curl must be installed to use this library.', Exception::CURL_NOT_FOUND);
        }
        if (!isset($client_id) || !isset($client_secret)) {
            throw new Exception('The App key must be set.', Exception::InvalidArgumentException);
        }

        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;

    }

    private function generateAccessTokenHeader($access_token)
    {
        $authorizationheader = 'Bearer ' . $access_token;
        return $authorizationheader;
    }

    public function getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state)
    {
        $parameters = array(
            'client_id'     => $this->client_id,
            'scope'         => $scope,
            'redirect_uri'  => $redirect_uri,
            'response_type' => $response_type,
            'state'         => $state,
            //The include_granted_scope is always set to false. No need to pass.
        );
        $authorizationRequestUrl .= '?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
        return $authorizationRequestUrl;
    }

    public function getAccessToken($tokenEndPointUrl, $code, $redirectUrl, $grant_type)
    {
        if (!isset($grant_type)) {
            throw new InvalidArgumentException('The grant_type is mandatory.', InvalidArgumentException::INVALID_GRANT_TYPE);
        }

        $parameters = array(
            'grant_type'   => $grant_type,
            'code'         => $code,
            'redirect_uri' => $redirectUrl,
        );
        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );

        //Try catch???
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    public function refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token)
    {
        $parameters = array(
            'grant_type'    => $grant_type,
            'refresh_token' => $refresh_token,
        );

        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    private function generateAuthorizationHeader()
    {
        $encodedClientIDClientSecrets = base64_encode($this->client_id . ':' . $this->client_secret);
        $authorizationheader          = 'Basic ' . $encodedClientIDClientSecrets;
        return $authorizationheader;
    }

    private function executeRequest($url, $parameters = array(), $http_header, $http_method)
    {

        $curl_options = array();

        switch ($http_method) {
            case self::HTTP_METHOD_GET:
                $curl_options[CURLOPT_HTTPGET] = 'true';
                if (is_array($parameters) && count($parameters) > 0) {
                    $url .= '?' . http_build_query($parameters);
                } elseif ($parameters) {
                    $url .= '?' . $parameters;
                }
                break;
            case self::HTTP_METHOD_POST:
                $curl_options[CURLOPT_POST] = '1';
                if (is_array($parameters) && count($parameters) > 0) {
                    $body                             = http_build_query($parameters);
                    $curl_options[CURLOPT_POSTFIELDS] = $body;
                }
                break;
            default:
                break;
        }

        if (is_array($http_header)) {
            $header = array();
            foreach ($http_header as $key => $value) {
                $header[] = "$key: $value";
            }
            $curl_options[CURLOPT_HTTPHEADER] = $header;
        }

        $curl_options[CURLOPT_URL] = $url;
        $curl_options[CURLOPT_SSL_VERIFYPEER] = 0;
        $ch                        = curl_init();

        curl_setopt_array($ch, $curl_options);

        //Don't display, save it on result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Execute the Curl Request
        $result = curl_exec($ch);

        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        if ($curl_error = curl_error($ch)) {
            throw new Exception($curl_error);
        } else {
            $json_decode = json_decode($result, true);
        }
        curl_close($ch);

        return $json_decode;
    }
}
class QuickBooks_online extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
        if ($this->session->userdata('logged_in') != "") {

        }

    }

    public function app_integration()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $merchID               = $this->session->userdata('logged_in')['merchID'];
        $in_data['merchantID'] = $merchID;

        $con = array('merchantID' => $merchID);

        $data['gt_result'] = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 0]);

        $data['get_data'] = $this->general_model->get_row_data('tbl_freshbooks', $con);

        $data['get_dt'] = $this->general_model->get_row_data('QBO_token', $con);

        $data['get_xero_data'] = $this->general_model->get_row_data('tbl_xero_token', $con);

        $data['get_app_type'] = $this->general_model->get_row_data('app_integration_setting', $con);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/app_integration', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function qb_online()
    {

        $config_data = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 0]);

        if (!empty($config_data)) {
            foreach ($config_data as $configs) {

                $authorizationRequestUrl = $configs['authorizationRequestUrl'];
                $tokenEndPointUrl        = $configs['tokenEndPointUrl'];
                $client_id               = $configs['client_id'];
                $client_secret           = $configs['client_secret'];
                $scope                   = $configs['oauth_scope'];
                $redirect_uri            = $configs['oauth_redirect_uri'];

            }

        }

        $response_type         = 'code';
        $state                 = 'RandomState';
        $include_granted_scope = 'false';
        $grant_type            = 'authorization_code';

        $client = new Client($client_id, $client_secret);

        if (!isset($_GET["code"])) {

            $authUrl = $client->getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state);
            header("Location: " . $authUrl);
            exit();
        } else {
            $code          = $_GET["code"];
            $responseState = $_GET['state'];
            if (strcmp($state, $responseState) != 0) {
                throw new Exception("The state is not correct from Intuit Server. Consider your app is hacked.");
            }
            $result = $client->getAccessToken($tokenEndPointUrl, $code, $redirect_uri, $grant_type);

            //record them in the session variable
            $in_data['realmID']      = $_GET['realmId'];
            $in_data['accessToken']  = $result['access_token'];
            $in_data['refreshToken'] = $result['refresh_token'];

            $merchID               = $this->session->userdata('logged_in')['merchID'];
            $merchantID            = $merchID;
            $in_data['merchantID'] = $merchantID;
            $in_data['createdAt']  = date('Y-m-d H:i:s');
            $in_data['Validity']   = "";

            $rs_id         = $this->session->userdata('logged_in')['resellerID'];
            $chk_condition = array('merchantID' => $merchID);
            $tok_data      = $this->general_model->get_row_data('QBO_token', $chk_condition);
            $url_data      = $this->general_model->get_select_data('Config_merchant_portal', array('merchantPortalURL'), array('resellerID' => $rs_id));
            $r_url         = $url_data['merchantPortalURL'];
            if (!empty($tok_data)) {

                $this->general_model->update_row_data('QBO_token', $chk_condition, $in_data);
            } else {
                $this->general_model->insert_row('QBO_token', $in_data);

            }

            $data['appIntegration'] = "1";
            $data['merchantID']     = $merchantID;

            $app_integration = $this->general_model->get_row_data('app_integration_setting', $chk_condition);
            if (!empty($app_integration)) {

                $this->general_model->update_row_data('app_integration_setting', $chk_condition, $data);

                $user               = $this->session->userdata('logged_in');
                $user['active_app'] = '1';
                $this->session->set_userdata('logged_in', $user);

            } else {
                $this->general_model->insert_row('app_integration_setting', $data);
                $user               = $this->session->userdata('logged_in');
                $user['active_app'] = '1';
                $this->session->set_userdata('logged_in', $user);

            }

            redirect($r_url . 'QBO_controllers/QuickBooks_online/auth_success');

        }

    }

    public function auth_success()
	{
	         $data['primary_nav']  = primary_nav();
			 $data['template']   = template_variable();
		
			  $merchID  = $this->session->userdata('logged_in')['merchID'];
			  $con = array('merchantID'=> $merchID);  
			  $condition = array('merchID'=> $merchID); 
		   	$data['gt_result'] 	  = $this->general_model->get_row_data('QBO_token', $con);
		   	$this->general_model->update_row_data('tbl_merchant_data',$condition,array('is_integrate'=>1));
		   	$user = $this->session->userdata('logged_in');
		   	   $user['active_app'] = 1;
              $user['is_integrate'] = 1;
              $this->session->unset_userdata('disconnect');
              $this->clean_qbo_data($merchID);
              
			 $this->get_qbo_data($merchID);
				
			  $this->session->set_userdata('logged_in',$user);
			  $gatew                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID));
			if (!empty($gatew)) {
				$merchID  = $this->session->userdata('logged_in')['merchID'];
				$con = array('merchantID'=> $merchID);  
				$condition = array('merchID'=> $merchID); 
				 $data['gt_result'] 	  = $this->general_model->get_row_data('QBO_token', $con);
				 $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
				 $user = $this->session->userdata('logged_in');
					$user['active_app'] = 1;
				$user['firstLogin'] = 1;
				   if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID)) == '0')
					   {  
						   
						   $fromEmail       = $user['merchantEmail'];
						  
							 $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						  
							foreach($templatedatas as $templatedata){
							 $insert_data = array('templateName'  =>$templatedata['templateName'],
									  'templateType'    => $templatedata['templateType'],
									   'merchantID'      => $merchID,
									   'fromEmail'      => DEFAULT_FROM_EMAIL,
									   'message'		  => $templatedata['message'],
									   'emailSubject'   => $templatedata['emailSubject'],
									   'createdAt'      => date('Y-m-d H:i:s') 	
								   );
						   $this->general_model->insert_row('tbl_email_template', $insert_data);
						  
						  }
					   }
				$this->session->set_userdata('logged_in',$user);
				
			   
		  
				$this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/qb_online_success_msg',$data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
			} else {
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('QBO_views/qb_online_success_msg_temp',$data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
			}
 
	}
	
	
	public function get_qbo_data($merchantID)
	{
	         require APPPATH .'libraries/qbo/QBO_data.php';
			  $this->qbo = new QBO_data($merchantID); 
			   $this->qbo->get_tax_data();
			  $this->qbo->get_account_data();
			  $this->qbo->get_customer_data();
			  $this->qbo->get_items_data();
			  $this->qbo->get_credit_data();
			   $this->qbo->get_invoice_data();
			   $this->qbo->get_payment_method();
			 
	}
	
	

    public function auth_error()
	{
	         $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('QBO_views/qb_Auth_error_msg',$data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	}
	
	
	public function final_auth_success()
	{
	         $data['primary_nav']  = primary_nav();
			 $data['template']   = template_variable();
		
			  $merchID  = $this->session->userdata('logged_in')['merchID'];
			  $con = array('merchantID'=> $merchID);  
			  $condition = array('merchID'=> $merchID); 
		   	$data['gt_result'] 	  = $this->general_model->get_row_data('QBO_token', $con);
		   	$this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
		   	$user = $this->session->userdata('logged_in');
		   	   $user['active_app'] = 1;
			  $user['firstLogin'] = 1;
            $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
            if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
            {  
                /* Start campaign in hatchbuck CRM*/  
                $this->load->library('hatchBuckAPI');
              
                
                $merchantData['merchant_type'] = 'QuickBooks Online (QBO)';        
                
                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
                if($status['statusCode'] == 400){
                    $resource = $this->hatchbuckapi->createContact($merchantData);
                    if($resource['contactID'] != '0'){
                        $contact_id = $resource['contactID'];
                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
                    }
                }
                /* End campaign in hatchbuck CRM*/ 
            }
			$this->get_qbo_data($merchID);
				 if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID)) == '0')
					 {  
					     
						 $fromEmail       = $user['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata){
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchID,
									 'fromEmail'      => DEFAULT_FROM_EMAIL,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
						 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                 }
			  $this->session->set_userdata('logged_in',$user);
			  
			 
                redirect('QBO_controllers/home/index');
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('QBO_views/qb_online_success_msg',$data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	}		
    public function clean_qbo_data($merchID)
	{
        $this->delete_tax_data($merchID);
        $this->delete_customer_data($merchID);
        $this->delete_credit_data($merchID);
        $this->delete_invoice_data($merchID);
        $this->delete_items_data($merchID);
        $this->delete_account_data($merchID);
        $condition = array('merchantID' => $merchID);

        $this->general_model->delete_row_data('tbl_qbo_config', $condition);

      
    }
    
    public function delete_tax_data($merchID)
	{
        $condition = array('merchantID' => $merchID);
        $this->general_model->delete_row_data('tbl_taxagency_qbo', $condition);
        $this->general_model->delete_row_data('tbl_taxes_qbo', $condition);
        $this->general_model->delete_row_data('tbl_taxe_code_qbo', $condition);
        
    }
    public function delete_customer_data($merchID)
	{
       
        $condition = array('merchantID' => $merchID);
        
        $this->general_model->delete_row_data('QBO_custom_customer', $condition);
        
    }
	public function delete_credit_data($merchID)
	{
        $condition = array('merchantID' => $merchID);
        $this->general_model->delete_row_data('qbo_customer_credit', $condition);
        
    }
    public function delete_invoice_data($merchID)
	{
        $condition = array('merchantID' => $merchID);
        $this->general_model->delete_row_data('QBO_test_invoice', $condition);
        
    }
    public function delete_items_data($merchID)
	{
        $condition = array('merchantID' => $merchID);
        $this->general_model->delete_row_data('QBO_test_item', $condition);
        
    }
    public function delete_account_data($merchID)
	{
        $condition = array('merchantID' => $merchID);
        $this->general_model->delete_row_data('QBO_accounts_list', $condition);
        
    }

	
}
