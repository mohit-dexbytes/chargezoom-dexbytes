<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;

require APPPATH .'libraries/qbo/QBO_data.php';

class Customer_Details extends CI_Controller 
{
	
	function __construct()
	{
		parent::__construct();
		
	 $this->load->model('card_model');
     	$this->load->model('general_model');
     	$this->load->model('QBO_models/qbo_customer_model');
     	$this->load->model('QBO_models/data_table_model');
     	
            if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
		  $this->loginDetails = get_names();
	}
	
		public function index(){
		    
		    redirect(base_url('QBO_controllers/home'));
		}
		
		
	public function customer_details()
	{
    
	    if($this->session->userdata('logged_in')){
            $data['login_info'] = $this->session->userdata('logged_in');
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
			$merchID = $user_id = $data['login_info']['merchantID'];
		}

		$entities=array();    
		$condition =  array('merchantID'=>$merchID);
     	 

	    $val = array(
			'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);

		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
			'auth_mode' => $this->config->item('AuthMode'),
			'ClientID'  => $this->config->item('client_id'),
			'ClientSecret' =>$this->config->item('client_secret'),
			'accessTokenKey' =>  $accessToken,
			'refreshTokenKey' => $refreshToken,
			'QBORealmID' => $realmID,
			'baseUrl' => $this->config->item('QBOURL'),
		));
		
		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

		$qbo_data = $this->general_model->get_select_data('tbl_qbo_config',array('lastUpdated'),array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'));
 
		if(!empty($qbo_data))
		{
			$last_date   = $qbo_data['lastUpdated'];
            $timezone1 = ['time' => $last_date, 'current_format' => date_default_timezone_get(), 'new_format' => 'America/Los_Angeles'];
            $nft = explode(' ', getTimeBySelectedTimezone($timezone1));
            $latedata = $nft[0]."T".$nft[1]."-07:00";
			  
			$sqlQuery = "SELECT * FROM Customer where   Metadata.LastUpdatedTime > '$latedata'";
		}else{
			$sqlQuery = "SELECT * FROM Customer";
		}

		$keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}
		
		$error = $dataService->getLastError();
     	$err='';
		if ($error != null) {
			$err.="The Status code is: " . $error->getHttpStatusCode() . "\n";
			$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
			$err.="The Response message is: " . $error->getResponseBody() . "\n";

			$this->session->set_flashdata('message', '<div class="alert alert-danger">' . QBO_DISCONNECT_MSG . '</div>');
        	redirect(base_url('QBO_controllers/home/index'));
		}
        
		if(!empty($qbo_data))
		{ 
			$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
			$updatedata['updatedAt'] =date('Y-m-d H:i:s');
			if(!empty($entities))
				$this->general_model->update_row_data('tbl_qbo_config',array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery'),$updatedata);
		}
        else{
			$updateda= date('Y-m-d') . 'T' . date('H:i:s');
			$upteda = array('merchantID'=>$merchID, 'realmID'=>$realmID,'qbo_action'=>'CustomerQuery','lastUpdated'=>$updateda, 'createdAt'=>date('Y-m-d H:i:s'), 'updatedAt'=>date('Y-m-d H:i:s'));
			if(!empty($entities))
				$this->general_model->insert_row('tbl_qbo_config', $upteda);
		} 
        
   
		if(!empty($entities))
		{
			foreach ($entities as $oneCustomer) {
				
				$QBO_customer_details = [];
				
				$QBO_customer_details['Customer_ListID'] = $oneCustomer->Id;
				$QBO_customer_details['firstname'] = $oneCustomer->GivenName;
				$QBO_customer_details['lastname'] = $oneCustomer->FamilyName;
				$QBO_customer_details['fullname'] = $oneCustomer->DisplayName;
				$QBO_customer_details['userEmail'] = ($oneCustomer->PrimaryEmailAddr)?$oneCustomer->PrimaryEmailAddr->Address:'';
				$QBO_customer_details['phoneNumber'] = ($oneCustomer->PrimaryPhone)?$oneCustomer->PrimaryPhone->FreeFormNumber:'';
				
				$QBO_customer_details['address1'] = (isset($oneCustomer->BillAddr->Line1) && $oneCustomer->BillAddr->Line1)?$oneCustomer->BillAddr->Line1:'';
				$QBO_customer_details['address2'] = (isset($oneCustomer->BillAddr->Line2) && $oneCustomer->BillAddr->Line2)?$oneCustomer->BillAddr->Line2:'';
				$QBO_customer_details['zipCode'] = (isset($oneCustomer->BillAddr->PostalCode) && $oneCustomer->BillAddr->PostalCode)?$oneCustomer->BillAddr->PostalCode:'';
				$QBO_customer_details['Country'] = (isset($oneCustomer->BillAddr->Country) && $oneCustomer->BillAddr->Country)?$oneCustomer->BillAddr->Country:'';
				$QBO_customer_details['State'] = (isset($oneCustomer->BillAddr->CountrySubDivisionCode) && $oneCustomer->BillAddr->CountrySubDivisionCode)?$oneCustomer->BillAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['City'] = (isset($oneCustomer->BillAddr->City) && $oneCustomer->BillAddr->City)?$oneCustomer->BillAddr->City:'';
				
				$QBO_customer_details['ship_address1'] = (isset($oneCustomer->ShipAddr->Line1) && $oneCustomer->ShipAddr->Line1)?$oneCustomer->ShipAddr->Line1:'';
				$QBO_customer_details['ship_address2'] = (isset($oneCustomer->ShipAddr->Line2) && $oneCustomer->ShipAddr->Line2)?$oneCustomer->ShipAddr->Line2:'';
				$QBO_customer_details['ship_zipcode'] = (isset($oneCustomer->ShipAddr->PostalCode) && $oneCustomer->ShipAddr->PostalCode)?$oneCustomer->ShipAddr->PostalCode:'';
				$QBO_customer_details['ship_country'] = (isset($oneCustomer->ShipAddr->Country) && $oneCustomer->ShipAddr->Country)?$oneCustomer->ShipAddr->Country:'';
				$QBO_customer_details['ship_state'] = (isset($oneCustomer->ShipAddr->CountrySubDivisionCode) && $oneCustomer->ShipAddr->CountrySubDivisionCode)?$oneCustomer->ShipAddr->CountrySubDivisionCode:'';
				$QBO_customer_details['ship_city'] = (isset($oneCustomer->ShipAddr->City) && $oneCustomer->ShipAddr->City)?$oneCustomer->ShipAddr->City:'';
				
				
				$QBO_customer_details['companyName'] = str_replace("'", "\'",$oneCustomer->CompanyName);
				$QBO_customer_details['companyID'] = $realmID;
				$QBO_customer_details['createdAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->CreateTime));
				$QBO_customer_details['updatedAt'] = date('Y-m-d H:i:s',strtotime($oneCustomer->MetaData->LastUpdatedTime));
				$QBO_customer_details['listID'] = '1';
				$QBO_customer_details['customerStatus'] = $oneCustomer->Active;;
				$QBO_customer_details['merchantID'] = $merchID;
				if($this->general_model->get_num_rows('QBO_custom_customer',array('companyID'=>$realmID,'Customer_ListID'=>$oneCustomer->Id , 'merchantID'=>$merchID)) > 0){  
					$this->general_model->update_row_data('QBO_custom_customer',array('companyID'=>$realmID, 'Customer_ListID'=>$oneCustomer->Id, 'merchantID'=>$merchID), $QBO_customer_details);
			
				}else{
					$this->general_model->insert_row('QBO_custom_customer', $QBO_customer_details);
				}
			
				
			}

			#Create Logs for SYNC New Entry
			$qbo_log = array('syncType' => 'CQ','type' => 'customerImport','qbStatus' => 1, 'qbAction' => 'CustomerDataImport', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
			$syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
			if($syncid){
	            $qbSyncID = 'CQ-'.$syncid;
	            $qbSyncStatus = 'Customer Data Imported';
	            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
	        }
		}	

		if($this->session->userdata('logged_in')){
            $data['login_info'] = $this->session->userdata('logged_in');
		} else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
		}

        $qboOBJ = new QBO_data($merchID); 
        $qboOBJ->get_payment_method();
 
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		
		$in_data['merchantID'] =  $merchID;
		
		
		if($this->uri->segment('4') == "InActive"){

			$data['type'] = '0';
			$data['activeval'] = "Active";
		}
		else{
			
			$data['type'] = '1';
			$data['activeval'] = "InActive";
		} 
		
		$condition1          = array('merchantID' => $user_id, 'systemMail' => 0);
		$data['template_data'] = $this->general_model->get_table_select_data('tbl_email_template', array('templateName', 'templateID', 'message'), $condition1);
		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];
		$data['page_num'] = '';
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('QBO_views/page_customers',$data);
		$this->load->view('QBO_views/page_qbo_model', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
 
	}
	
	
	public function customer_details_ajax()
	 {
		$showEmail = false;
		if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchantEmailID = $this->session->userdata('logged_in')['merchantEmail'];

			$merchID = $user_id;
			$showEmail = true;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in')['merchantID'];
			$merchantEmailID = $this->session->userdata('user_logged_in')['merchant_data']['merchantEmail'];

			$merchID = $user_id;
			if(in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])){
				$showEmail = true;
			}
		}
		$type = $this->czsecurity->xssCleanPostInput('type');
		if($type == 0){
			$customer_data = $this->qbo_customer_model->get_all_customer_details($merchID,'0');
		}else{
			$customer_data = $this->qbo_customer_model->get_all_customer_details($merchID,'1');
		}
		
		$plantype = $this->general_model->chk_merch_plantype_status($merchID);
		$data['plantype'] = $plantype;
		$data = array();
		$no = $_POST['start'];
		
		$customers = $customer_data['result'];
				if(isset($customers) && $customers)
				{
					foreach($customers as $customer)
					{
						$customerEmailId = $customer['Customer_ListID'];
						$customerEmailName = $customer['firstName'].' '.$customer['lastName'];
						$customerEmail = $customer['userEmail'];
						$customerEmailCompanyID = $customer['companyID'];

						$emailAnchor = "<div class='text-left'>$customerEmail";
						if($showEmail){
							$emailAnchor = "<div class='text-left cust_view'><a href='#set_tempemail_QBO' onclick=\"set_template_data_qbo('$customerEmailId','$customerEmailName', '$customerEmailCompanyID','$customerEmail','$merchantEmailID')\" title='' data-backdrop='static' data-keyboard='false' data-toggle='modal' data-original-title=''>$customerEmail</a>";
						}
						$base_url1 = base_url() . "QBO_controllers/home/view_customer/".$customer['Customer_ListID'];
						$no++;
						$row = array();
						$row[] = "<div class='text-left cust_view'><a href='" . $base_url1 . "' >".$customer['fullName']."</a>";
						$row[] = "<div class='text-left'>". $customer['firstName'].' '.$customer['lastName']."";
						$row[] = $emailAnchor;
						
						$balance = ($customer['Balance']) ? number_format($customer['Balance'],2) : '0.00';
					    $row[] = "<div class='text-right'>$".$balance."</div>";
						$data[] = $row;
					}
				}
				if($type == 0){
					$count = 0;
				}else{
					$count = $this->qbo_customer_model->get_all_customer_count($merchID,'1');

				}
			
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
	}
/* ---------------------- Server Side Data table For All Customers List    -----------------------*/	
	 public function ajax_all_customers()
	 {
	     if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in');
			$merchID = $user_id['merchantID'];
		}
	     error_reporting(0);
	     
	     $st = $this->czsecurity->xssCleanPostInput('type');
	 
	     $list = $this->data_table_model->get_datatables_customers_list($merchID, $st);
	     $data = array();
		 $no = $_POST['start'];
		
	
		 
		 foreach ($list as $person) {
				$no++;
				$row = array();
				 
			 $action_column = ''; 
			
		 if($person->customerStatus=="true"){
								
				$action_column.= '<a href="'.base_url().'QBO_controllers/Customer_Details/create_customer/'.$person->Customer_ListID.'" data-toggle="tooltip" title="Edit" class="btn btn-default"> <i class="fa fa-edit"> </i> </a>	
								
									<a href="#qbo_del_cust" data-backdrop="static" onclick="delete_qbo_customer("'.$person->Customer_ListID.'","0")"  data-keyboard="false" data-toggle="modal"  title="Deactivate" class="btn btn-danger"> <i class="fa fa-ban"> </i> </a>';
									
								 } else {
									
							$action_column.= '<a href="#qbo_del_cust" data-backdrop="static" onclick="delete_qbo_customer("'.$person->Customer_ListID.'","1")"  data-keyboard="false" data-toggle="modal"  title="Activate" class="btn btn-danger"> <i class="fa fa-check"> </i> </a>' ; 
									
									}
				   

				$row[] ="<div class='text-left'> <a href='".base_url().'QBO_controllers/home/view_customer/'.$person->Customer_ListID."' title='Edit Talent'> $person->fullName </a> </div>";
				$row[] = $person->firstName.' '.$person->lastName;
				$row[] = $person->userEmail;
				$row[] = $person->phoneNumber;
				$row[] =  number_format($person->Balance)?number_format($person->Balance,2):'0.00';
			   
               $row[] = 	 '<div class="text-center">
								<div class="btn-group btn-group-xs ">
								   
								  '.$action_column.'
								   
								 </div>
							</div>';
							
							
							$data[] = $row;
			
			}

			 $output = array(
								"draw" => $_POST['draw'],
								"recordsTotal" => $this->data_table_model->count_all_customers($merchID),
								"recordsFiltered" => $this->data_table_model->count_filtered_all_customers($merchID, $st),
								"data" => $data,
						);
				//output to json format
				echo json_encode($output); die;
		 
	     
	     
	 }

	
	
	
	public function create_customer()
	{
		
		
		if($this->session->userdata('logged_in')){
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
	    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		
		 $val = array(
		'merchantID' => $user_id,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
    	$realmID      = $data['realmID']; 
		$dataService = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
		
	 	if(!empty($this->input->post(null, true)))
	 	{
	 		$fullName = $this->czsecurity->xssCleanPostInput('fullName');
	 		$con  = array('fullName'=> $fullName, 'merchantID'=>$user_id); 
	 		$originalValue = $this->general_model->get_select_data('QBO_custom_customer',array('fullName'),$con); 
			
		    if(isset($originalValue['fullName']) && ($fullName != $originalValue['fullName']) ) {
		       $isUnique =  '|is_unique[QBO_custom_customer.fullName]';
		    } else {
		       $isUnique =  '';
		    }
	    	$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');	
            if($this->czsecurity->xssCleanPostInput('customerListID')=="" )
            {
				$this->form_validation->set_rules(
					'fullName', 'fullName',
					'required'.$isUnique,
					array(
							'required'      => 'Please enter %s.',
							'is_unique'     => 'This %s already exists.'
					)
				);
            
            }
       
	    if ($this->form_validation->run() == true)
		{
		   
		 	$insert['firstName'] = $firstName      = $this->czsecurity->xssCleanPostInput('firstName');
	     	$insert['lastName'] = 	$lastName	    = $this->czsecurity->xssCleanPostInput('lastName');
			$insert['fullName'] = 	$fullName	    = $this->czsecurity->xssCleanPostInput('fullName');
			$insert['userEmail'] = 	$userEmail      = $this->czsecurity->xssCleanPostInput('userEmail');
			
			$insert['companyName'] = 	$companyName    = $this->czsecurity->xssCleanPostInput('companyName');
			$insert['phoneNumber'] = $phoneNumber    = $this->czsecurity->xssCleanPostInput('phone');
			$insert['address1'] = $address1	    = $this->czsecurity->xssCleanPostInput('address1');
			$insert['address2'] = $address2	    = $this->czsecurity->xssCleanPostInput('address2');
			$insert['State   '] = $state	        = $this->czsecurity->xssCleanPostInput('companyState');
			$insert['City']     = $city	        = $this->czsecurity->xssCleanPostInput('companyCity');
			$insert['zipCode'] = $zipCode        = $this->czsecurity->xssCleanPostInput('zipCode');
       	    $insert['Country'] = $country        = $this->czsecurity->xssCleanPostInput('companyCountry');
       	    
       	    $insert['ship_country'] = $ship_country	= $this->czsecurity->xssCleanPostInput('sCountry');
       	    $insert['ship_state'] = $ship_state	   = $this->czsecurity->xssCleanPostInput('sState');
       	    $insert['ship_city'] = $ship_city     = $this->czsecurity->xssCleanPostInput('sCity');
       	   	$insert['ship_address1'] = $ship_address1	   = $this->czsecurity->xssCleanPostInput('saddress1');
			$insert['ship_address2'] = $ship_address2   = $this->czsecurity->xssCleanPostInput('saddress2');
		   	$insert['ship_zipcode'] = $ship_zipcode  =$this->czsecurity->xssCleanPostInput('szipCode');   
		   	if($this->czsecurity->xssCleanPostInput('overdue'))
		   	 	$enable  =$this->czsecurity->xssCleanPostInput('overdue');  
       	    else
       	       $enable  ='0';
       	      
        
			
			
	       $insert['merchantID'] =  $merchantID     = $user_id;
		   $insert['createdAt'] =  $createdat      = date('Y-m-d H:i:s');
		  $lsID='';
		    
    		if($this->czsecurity->xssCleanPostInput('customerListID')!="" )
    		{
    				      
    				         $id = $this->czsecurity->xssCleanPostInput('customerListID');
    						 $chk_condition = array('Customer_ListID'=>$id);
    						 
    						 $entities = $dataService->Query("SELECT * FROM Customer where Id='".$id."'");
    						 $lsID = $id;
    						 if(!empty($entities)) 
                            {    
    							//Get the first element
    							$theCustomer = reset($entities);
    							$updateCustomer = Customer::update($theCustomer, [
    								 
    								 "GivenName"=>  $firstName,
    								 "FamilyName"=>  $lastName,
    								  "DisplayName"=>  $fullName,
    								 "CompanyName"=>  $companyName,
    								 "PrimaryPhone"=>  [
    									 "FreeFormNumber"=>  $phoneNumber,
    									 "CountryCode"=> $zipCode
    								 ],
    								 "PrimaryEmailAddr"=>  [
    									 "Address" => $userEmail
    								 ],
    								 "BillAddr"=>  [
    									 "Line1" => $address1,
    									 "Line2" => $address2,
    									 "PostalCode" => $zipCode,
    									 "Country" => $country,
    									 "CountrySubDivisionCode" => $state,
    									 "City" => $city
    								 ],
    								"ShipAddr"=>  [
    									 "Line1" => $ship_address1,
    									 "Line2" => $ship_address2,
    									 "PostalCode" => $ship_zipcode,
    									 "Country" => $ship_country,
    									 "CountrySubDivisionCode" => $ship_state,
    									 "City" => $ship_city
    								 ]
    								]);
    							  	
    							$resultingCustomerUpdatedObj = $dataService->Update($updateCustomer);
                            }	
                            
                            
                            
                          
                          
    							if($dataService->getLastError()!=null)
    							{
    							    $st ='1';
    							    $insert['updatedAt'] =date('Y-m-d H:i:s');
    							    
    							    $error1='';
                				   
                			        $error1.=  $dataService->getLastError() . "\n";
    							    $action="Edit Customer"; 
    							}else{
    							    $st ='1';
								  	$error1='success'; $action = 'Edit Customer';  
									$con =array('Customer_ListID' => $id,'merchantID'=>$user_id); 
                			        $id = $this->general_model->update_row_data('QBO_custom_customer',$con,$insert);
    							}
    							
    							 $login['overDue']= $enable;
    							 $this->general_model->update_row_data('tbl_customer_login',array('merchantID'=>$user_id,'customerID'=>$id),$login);
    							
    						
    						
    						 $this->session->set_flashdata('success', 'Successfully Updated Customer');
 
    						 
    			    	}
    		else
    		{
    		           $lsID =mt_rand(7000000,8000000);          
    							$customerObj = Customer::create([
    							     
    								 "GivenName"=>  $firstName,
    								 "FamilyName"=>  $lastName,
    								 "FullyQualifiedName"=>  $fullName,
    								 "DisplayName"=>  $fullName,
    								 "CompanyName"=>  $companyName,
    								 "PrimaryPhone"=>  [
    									 "FreeFormNumber"=>  $phoneNumber,
    									 "CountryCode"=> $zipCode
    								 ],
    								 "PrimaryEmailAddr"=>  [
    									 "Address" => $userEmail
    								 ],
    								 "BillAddr"=>  [
    									 "Line1" => $address1,
    									 "Line2" => $address2,
    									 "PostalCode" => $zipCode,
    									 "Country" => $country,
    									 "CountrySubDivisionCode" => $state,
    									 "City" => $city
    								 ],
    						    	"ShipAddr"=>  [
    									 "Line1" => $ship_address1,
    									 "Line2" => $ship_address2,
    									 "PostalCode" => $ship_zipcode,
    									 "Country" => $ship_country,
    									 "CountrySubDivisionCode" => $ship_state,
    									 "City" => $ship_city
    								 ]
    								]);
    								
    								
    							
    							
    				           $resultingCustomerObj = $dataService->Add($customerObj);
    				          
    				           $error = $dataService->getLastError();
    				        
    				          
                			if ($error != null) 
                			{
                			    $error1='';
                			
                			      $error1.=  $error->getResponseBody() . "\n";
                			     
                			      
                			        $insert['updatedAt'] =date('Y-m-d H:i:s');
                			        $insert['Customer_ListID'] = $lsID; 
                			         $insert['customerStatus'] = 'false'; 
                			        $id = $this->general_model->insert_row('QBO_custom_customer',$insert);
                			     $st ='0';  $action = 'Add Customer';  
                			        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> "'.$error1.'"</strong></div>'); 
                			}else{ 
                			     $st ='1';
                			     $action = 'Add Customer'; 
                			     $error1='success';
                			     $lsID= $resultingCustomerObj->Id;
                			}
                			
    				           
    				           if(is_numeric($lsID))
    				           {
    				               
    				               $login['isEnable']=1;
    				               $login['customerEmail']=$userEmail;
    				               $login['merchantID']=$user_id;
    				               $login['customerID']=$lsID;
    				               $login['customerPassword']=md5($userEmail);
    				               $login['customerUsername']=$userEmail;
    				                $login['overDue']= $enable;
    				               $login['createdAt']   = date('Y-m-d H:i:s');
    				               $this->general_model->insert_row('tbl_customer_login',$login);
    				               
    				               
    				               
    				           }   				         
    				         	
    						 $this->session->set_flashdata('success', 'Successfully Inserted Customer');
 
    				
    			
    			}
    		    $qblog_data = array('syncType' => 'CQ','type' => 'customer','merchantID'=>$user_id,'qbAction'=>$action,'qbStatus'=>$st, 
		         	'updatedAt'=>date('Y-m-d H:i:s'), 
		         	'createdAt'=>date('Y-m-d H:i:s'), 'qbText'=>$error1, 'qbActionID'=>$lsID, 
		        );
		        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qblog_data); 
		        if($syncid){
	                $qbSyncID = 'CQ-'.$syncid;

	                $qbSyncStatus = $action.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/home/view_customer/'.$lsID.'">'.$fullName.'</a></span> ';

	                
	                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
	            }					   
				redirect(base_url('QBO_controllers/Customer_Details/customer_details'));
					
			}

		 
 }
 
			if($this->uri->segment('4')){
				$customerListID  			  = $this->uri->segment('4');	
				$this->qboOBJ = new QBO_data($user_id); 
				$this->qboOBJ->get_customer_data([
					'customerId' => $customerListID
				]);
				
				$con                = array('Customer_ListID'=> $customerListID, 'merchantID'=>$user_id);  
				$data['customer'] = $this->general_model->get_row_data('QBO_custom_customer',$con); 
			} 
 
 
		    $data['primary_nav']  = primary_nav();
			$data['template']   = template_variable();
		    
		     $country = $this->general_model->get_table_data('country','');
					 $data['country_datas'] = $country;
			
					
     		$state = $this->general_model->get_table_data('state','');
					 $data['state_datas'] = $state;
					
    		$city = $this->general_model->get_table_data('city','');
					$data['city_datas'] = $city;		 
		  
		    $this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('QBO_views/create_customer',$data);
			$this->load->view('template/page_footer',$data);
			$this->load->view('template/template_end', $data);
}
	
	
	
	public function invoice_create(){
		
		if($this->session->userdata('logged_in')){
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
	    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		
		 $val = array(
		'merchantID' => $user_id,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
		$realmID      = $data['realmID'];
		$dataService = DataService::Configure(array(
						 'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
						));
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

			//Add a new Invoice
			$theResourceObj = Invoice::create([
			"CustomerRef" => "62", 
			 "ShipAddr" => [
			 "Line1"=>  "123 Main Street",
			 "City"=>  "Taxas",
			 "Country"=>  "US",
			 "PostalCode"=>  "94067"
		 ],
		  "Line" => [
             [
               "Amount" => 100.00,
               "DetailType" => "SalesItemLineDetail",
				 "SalesItemLineDetail" => [
				   "ItemRef" => "4"
				  ]
    
             ]
            ],
		 "DueDate" =>  "12/2/2018",
		 "Balance"=>  "200",
		 
		
		]);
			$resultingObj = $dataService->Add($theResourceObj);


			$error = $dataService->getLastError();
			if ($error != null) {
				echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
				echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
				echo "The Response message is: " . $error->getResponseBody() . "\n";
			}
			else {
				echo "Created Id={$resultingObj->Id}. Reconstructed response body:\n\n";
				$xmlBody = XmlObjectSerializer::getPostXmlFromArbitraryEntity($resultingObj, $urlResource);
				echo $xmlBody . "\n";
			}
		
		
	}
	
	public function get_Invoice(){
	 	if($this->session->userdata('logged_in')){
		$user_id = $this->session->userdata('logged_in')['merchID'];
		$merchID = $user_id;
		}
	
	     $val = array(
		'merchantID' => $merchID,
		);
		
		$data = $this->general_model->get_row_data('QBO_token',$val);
		
		$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	$realmID      = $data['realmID'];
	
		$dataService = DataService::Configure(array(
        	            'auth_mode' => $this->config->item('AuthMode'),
						 'ClientID'  => $this->config->item('client_id'),
						 'ClientSecret' =>$this->config->item('client_secret'),
						 'accessTokenKey' =>  $accessToken,
						 'refreshTokenKey' => $refreshToken,
						 'QBORealmID' => $realmID,
						 'baseUrl' => $this->config->item('QBOURL'),
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

			//Add a new Invoice
			$entities = $dataService->Query("SELECT * FROM Invoice");
			print_r($entities);die;
			$error = $dataService->getLastError();
			if ($error != null) {
			echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
			echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
			echo "The Response message is: " . $error->getResponseBody() . "\n";
			exit();
		}
		
		foreach ($entities as $oneInvoice) {
			
		     $QBO_invoice_details['invoiceID'] = $oneInvoice->Id;
			 $QBO_invoice_details['refNumber'] = $oneInvoice->CustomerRef;
			 $QBO_invoice_details['ShipAddress_Addr1'] = $oneInvoice->BillAddr->Line1;
			 $QBO_invoice_details['ShipAddress_Addr2'] = $oneInvoice->BillAddr->Line2;
			 $QBO_invoice_details['ShipAddress_City'] = $oneInvoice->BillAddr->City;
			 $QBO_invoice_details['ShipAddress_Country'] = $oneInvoice->BillAddr->Country;
			 $QBO_invoice_details['ShipAddress_PostalCode'] = $oneInvoice->BillAddr->PostalCode;
			 $QBO_invoice_details['DueDate'] = $oneInvoice->DueDate;
			 $QBO_invoice_details['BalanceRemaining'] = $oneInvoice->Balance;
			 $this->general_model->insert_row('QBO_test_invoice', $QBO_invoice_details);
			
		}
		
		
	}
	

	
}