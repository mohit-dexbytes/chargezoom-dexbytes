<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Item;

class Plan_Product extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('general_model');

        $this->db->query("SET SESSION sql_mode = ''");

        $this->load->model('QBO_models/Qbo_company_model','qbo_company_model');

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '1') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {

        
        redirect('QBO_controllers/home', 'refresh');
    }

    public function delete_exsiting_records_Products()
    {
        $user_id = $this->session->userdata('logged_in')['merchID'];
        $merchID = $user_id;

        $condition = array('merchantID' => $merchID);

        $del = $this->general_model->delete_row_data('QBO_test_item', $condition);
        if ($del) {
            
            $this->session->set_flashdata('success', 'Successfully Deleted Product');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Something Is Wrong...</div>');
        }
        redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));
    }

    public function get_plan_data_item()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
        
        $plan =    $this->qbo_company_model->getQboNonGroupProducts($user_id);

		echo json_encode($plan);
		die;
	}

    public function get_item_data()
	{

        $user_id = null;
		if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $itemdata = [];
		if ($user_id && $this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');

			$itemdata = $this->general_model->get_row_data('QBO_test_item', array('ListID' => $itemID));

			$itemdata['saleCost'] =  number_format($itemdata['saleCost'], 2, '.', '');

            
            $itemdata['SalesPrice'] =  $itemdata['saleCost'];

            $itemdata['QuantityOnHand'] = (int)$itemdata['QuantityOnHand'];
		} 
        if(!empty($itemdata)){
            echo json_encode(['status' => 'success', 'data' => $itemdata]);
        } else {
            echo json_encode(['status' => 'error', 'data' => [] ]);
        }
        
        die;
	}

    public function create_product_services()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchID = $user_id;
        $val     = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        // Fetch Item Details
        $p_List = null;
        $productListItem = null;
        if ($this->uri->segment('4')) {
            $productID        = $this->uri->segment('4');
            $con              = array('productID' => $productID, 'merchantID' => $merchID);
            $productListItem = $this->general_model->get_row_data('QBO_test_item', $con);

            $p_List = $productListItem['ListID'];
        }

        

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),

        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $err = '';
        if (!empty($this->input->post(null, true))) {

           
          
            $item_details['Name'] = $productName = $this->czsecurity->xssCleanPostInput('productName');
            $item_details['SKU']  = $sku  = $this->czsecurity->xssCleanPostInput('sku');
            if ($this->czsecurity->xssCleanPostInput('productParent') != "") {
                $item_details['parent_ListID'] = $productParent = $this->czsecurity->xssCleanPostInput('productParent');
            } else {
                $item_details['parent_ListID'] = 0;
                $productParent                 = '';
            }
            $item_details['Type'] = $type = $this->czsecurity->xssCleanPostInput('ser_type');

            $item_details['IsActive']   = 'true';
            $qbaction                   = '';
            $prodid                     = 0;
            $item_details['CompanyID']  = $realmID;
            $item_details['merchantID'] = $merchID;
            if ($type == 'Inventory') {

                $item_details['QuantityOnHand'] = $quantityonhand = $this->czsecurity->xssCleanPostInput('quantityonhand');

                $item_details['IncomeAccountRef']  = $incomeAccount  = $this->czsecurity->xssCleanPostInput('incomeAccount');
                $item_details['ExpenseAccountRef'] = $expenseAccount = $this->czsecurity->xssCleanPostInput('expenseAccount');
                $item_details['InvStartDate']      = $Date      = $this->czsecurity->xssCleanPostInput('datePick');
                $item_details['InvStartDate']      = date('Y-m-d', strtotime($Date));
                $item_details['AssetAccountRef']   = $AssetAccountRef   = $this->czsecurity->xssCleanPostInput('AssetAccountRef');
                $item_details['saleCost']         = $salePrice         = $this->czsecurity->xssCleanPostInput('salePrice');
                $item_details['purchaseCost']     = $purchasePrice     = $this->czsecurity->xssCleanPostInput('purchasePrice');
                $item_details['SalesDescription'] = $saleProDescription = $this->czsecurity->xssCleanPostInput('saleProDescription');
                $item_details['purchaseDesc']     = $purProDescription     = $this->czsecurity->xssCleanPostInput('purProDescription');
                $createdat                        = date('Y-m-d H:i:s');

                if ($this->czsecurity->xssCleanPostInput('productID') != "") {
                    $item_details['TimeModified'] = date('Y-m-d H:i:s');
                    $id                           = $this->czsecurity->xssCleanPostInput('productID');
                    $prodid                       = $id;
                    $qbaction                     = 'Edit Item';
                    $item_details['productID']    = $id;
                    $chk_condition                = array('productID' => $id);

                    $entities = $dataService->Query("SELECT * FROM Item where Id='" . $id . "'");
                    if (!empty($entities)) {
                        $proServices = reset($entities);
                        $dateTime    = new \DateTime('NOW');
                        $updateItem  = Item::update($proServices, [
                            "Name"              => $productName,
                            "Sku"               => $sku,
                            "Type"              => $type,
                            "QtyOnHand"         => $quantityonhand,
                            "TrackQtyOnHand"    => true,
                            "AssetAccountRef"   => $AssetAccountRef,
                            "ParentRef"         => $productParent,
                            "SubItem"           => true,
                            "Active"            => true,
                            "IncomeAccountRef"  => [
                                "value" => $incomeAccount,
                            ],
                            "ExpenseAccountRef" => [
                                "value" => $expenseAccount,
                            ],
                            "PurchaseDesc"      => $purProDescription,
                            "Description"       => $saleProDescription,
                            "PurchaseCost"      => $purchasePrice,
                            "UnitPrice"         => $salePrice,
                            "InvStartDate"      => $dateTime,
                        ]);
                        $resultingItemsUpdatedObj = $dataService->Update($updateItem);
                    }
                    if ($dataService->getLastError() != null) {
                        $this->general_model->update_row_data('QBO_test_item', array('merchantID' => $merchID, 'productID' => $id), $item_details);
                    }
                } else {

                    $qbaction = 'Add Item';

                    $dateTime = new \DateTime('NOW');
                    $Item     = Item::create([
                        "Name"              => $productName,
                        "Sku"               => $sku,
                        "Type"              => $type,
                        "QtyOnHand"         => $quantityonhand,
                        "ParentRef"         => $productParent,
                        "InvStartDate"      => $Date,
                        "TrackQtyOnHand"    => true,
                        "AssetAccountRef"   => $AssetAccountRef,

                        "SubItem"           => true,
                        "Active"            => true,
                        "IncomeAccountRef"  => [
                            "value" => $incomeAccount,
                        ],
                        "ExpenseAccountRef" => [
                            "value" => $expenseAccount,
                        ],
                        "PurchaseDesc"      => $purProDescription,
                        "Description"       => $saleProDescription,
                        "PurchaseCost"      => $purchasePrice,
                        "UnitPrice"         => $salePrice,
                        "InvStartDate"      => $dateTime,
                    ]);

                    if ($resultingObj = $dataService->Add($Item)) {
                        $prodid = $resultingObj->Id;
                    }

                    if ($dataService->getLastError() != null) {
                        $prodid                       = mt_rand(200000, 600000);
                        $item_details['productID']    = $prodid;
                        $item_details['TimeCreated']  = date('Y-m-d H:i:s');
                        $item_details['TimeModified'] = date('Y-m-d H:i:s');
                        $item_details['IsActive']     = 'false';
                        $p_List = $this->general_model->insert_row('QBO_test_item', $item_details);
                    }

                }

            }

            if ($type == 'Service') {

                $item_details['IncomeAccountRef']  = $incomeAccount  = $this->czsecurity->xssCleanPostInput('incomeAccount_serv');
                $item_details['ExpenseAccountRef'] = $expenseAccount = $this->czsecurity->xssCleanPostInput('expenseAccount_serv');
                $Date                              = $this->czsecurity->xssCleanPostInput('datePick');
                $item_details['InvStartDate']      = date('Y-m-d', strtotime($Date));
                $item_details['saleCost']          = $salePrice          = $this->czsecurity->xssCleanPostInput('salePrice_serv');
                $item_details['purchaseCost']      = $purchasePrice      = $this->czsecurity->xssCleanPostInput('purchasePrice_serv');
                $item_details['SalesDescription']  = $saleProDescription  = $this->czsecurity->xssCleanPostInput('saleProDescription_serv');
                $item_details['purchaseDesc']      = $purProDescription      = $this->czsecurity->xssCleanPostInput('purProDescription_serv');
                $createdat                         = date('Y-m-d H:i:s');

                if ($this->czsecurity->xssCleanPostInput('productID') != "") {

                    $id            = $this->czsecurity->xssCleanPostInput('productID');
                    $qbaction      = 'Edit Item';
                    $chk_condition = array('productID' => $id);
                    $prodid        = $id;
                    $entities      = $dataService->Query("SELECT * FROM Item where Id='" . $id . "'");

                    if (!empty($entities)) {
                        $proServices = reset($entities);
                        $dateTime    = new \DateTime('NOW');
                        $updateItem  = Item::update($proServices, [
                            "Name"              => $productName,
                            "Sku"               => $sku,
                            "Type"              => $type,
                            "ParentRef"         => $productParent,
                            "SubItem"           => true,
                            "Active"            => true,
                            "IncomeAccountRef"  => [
                                "value" => $incomeAccount,
                            ],
                            "ExpenseAccountRef" => [
                                "value" => $expenseAccount,
                            ],
                            "PurchaseDesc"      => $purProDescription,
                            "Description"       => $saleProDescription,
                            "PurchaseCost"      => $purchasePrice,
                            "UnitPrice"         => $salePrice,
                            "InvStartDate"      => $dateTime,
                        ]);
                        $resultingItemsUpdatedObj = $dataService->Update($updateItem);
                    }

                    if ($dataService->getLastError() != null) {
                        $this->general_model->update_row_data('QBO_test_item', array('merchantID' => $merchID, 'productID' => $id), $item_details);
                    }
                } else {
                    $qbaction = 'Add Item';
                    $dateTime = new \DateTime('NOW');
                    $Item     = Item::create([
                        "Name"              => $productName,
                        "Sku"               => $sku,
                        "Type"              => $type,
                        "ParentRef"         => $productParent,

                        "SubItem"           => true,
                        "Active"            => true,
                        "IncomeAccountRef"  => [
                            "value" => $incomeAccount,
                        ],
                        "ExpenseAccountRef" => [
                            "value" => $expenseAccount,
                        ],
                        "PurchaseDesc"      => $purProDescription,
                        "Description"       => $saleProDescription,
                        "PurchaseCost"      => $purchasePrice,
                        "UnitPrice"         => $salePrice,
                        "InvStartDate"      => $dateTime,
                    ]);

                    if ($resultingObj = $dataService->Add($Item)) {
                        $prodid = $resultingObj->Id;
                    }

                    if ($dataService->getLastError() != null) {
                        $prodid                       = mt_rand(200000, 600000);
                        $item_details['productID']    = $prodid;
                        $item_details['TimeCreated']  = date('Y-m-d H:i:s');
                        $item_details['TimeModified'] = date('Y-m-d H:i:s');
                        $item_details['IsActive']     = 'false';
                        $p_List = $this->general_model->insert_row('QBO_test_item', $item_details);
                    }

                }

            }

            

            // Group Item
            if ($type == 'Group') 
            {
                $priceTotal = 0;
                // Get total sale price using the group child items
                if (!empty($this->czsecurity->xssCleanPostInput('prodID'))) {
                    $prod_datas = $this->czsecurity->xssCleanPostInput('prodID');
                    $rate = $this->czsecurity->xssCleanPostInput('unit_rate');
                    foreach ($prod_datas as $k => $prod_data) {
                        // Add price
                        $priceTotal += (float)$rate[$k];
                    }

                    $salePrice = $priceTotal;
                }
                
                $item_details['IncomeAccountRef']  = '';
                $item_details['ExpenseAccountRef'] = '';
                $item_details['saleCost']          = $salePrice;
                $item_details['purchaseCost']      = 0;
                $item_details['SalesDescription']  = '';
                $item_details['purchaseDesc']      = '';

                $createdat                         = date('Y-m-d H:i:s');

                // Update Product
                if ($this->czsecurity->xssCleanPostInput('productID') != "") {

                    $id            = $this->czsecurity->xssCleanPostInput('productID');
                    $qbaction      = 'Edit Item';
                    $prodid        = $id;
                    $entities      = $dataService->Query("SELECT * FROM Item where Id='" . $id . "'");

                    if (!empty($entities)) {
                        $proServices = reset($entities);
                        $dateTime    = new \DateTime('NOW');
                        $updateItem  = Item::update($proServices, [
                            "Name"              => $productName,
                            "Sku"               => $sku,
                            "Type"              => $type,
                            "ParentRef"         => $productParent,
                            "SubItem"           => true,
                            "Active"            => true,
                            "UnitPrice"         => $salePrice,
                        ]);
                        
                        // Group Items is supported in QBO online
                        $resultingItemsUpdatedObj = $dataService->Update($updateItem);
                    }

                    if ($dataService->getLastError() != null) {
                        $this->general_model->update_row_data('QBO_test_item', array('merchantID' => $merchID, 'productID' => $id), $item_details);
                    }
                } else {

                // Add Product

                    $qbaction = 'Add Item';
                    $dateTime = new \DateTime('NOW');
                    $Item     = Item::create([
                        "Name"              => $productName,
                        "Sku"               => $sku,
                        "Type"              => $type,
                        "ParentRef"         => $productParent,
                        "SubItem"           => true,
                        "Active"            => true,
                        "UnitPrice"         => $salePrice,
                    ]);

                    $prodid                       = mt_rand(200000, 600000);
                    if ($resultingObj = $dataService->Add($Item)) {
                        $prodid = $resultingObj->Id;
                    }

                 
                    $item_details['productID']    = $prodid;
                    $item_details['TimeCreated']  = date('Y-m-d H:i:s');
                    $item_details['TimeModified'] = date('Y-m-d H:i:s');
                    $item_details['IsActive']     = 'true';

                    $p_List = $this->general_model->insert_row('QBO_test_item', $item_details);

                }

                if (!empty($p_List)) {
                    // Delete Previous child products of Group
                    $this->general_model->delete_row_data('qbo_group_lineitem', array('groupListID' => $p_List));

                    $priceTotal = 0;

                    // Check the Group product child items
                    if (!empty($this->czsecurity->xssCleanPostInput('prodID'))) {

                        $prod_datas = $this->czsecurity->xssCleanPostInput('prodID');
                        $description = $this->czsecurity->xssCleanPostInput('description');
                        $rate = $this->czsecurity->xssCleanPostInput('unit_rate');
                        $quantity = $this->czsecurity->xssCleanPostInput('quantity');

                        
                        foreach ($prod_datas as $k => $prod_data) {
                            $input_data1 = array();
                            $input_data1['itemListID'] = $prod_data;
                            $input_data1['FullName']   = $description[$k];
                            $input_data1['Quantity']   = $quantity[$k];
                            $input_data1['Price']	  = $rate[$k];
                            $input_data1['groupListID'] = $p_List;

                            $this->general_model->insert_row('qbo_group_lineitem', $input_data1);

                            // Add price
                            $priceTotal += $input_data1['Price'];
                        }
                    }

                    // Update Product Price
                    $this->general_model->update_row_data('QBO_test_item', array('ListID' => $p_List), ['saleCost' => $priceTotal ]);
                }

                // Group Product
                $this->session->set_flashdata('success', 'Success');
                redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));
            }

            $error = $dataService->getLastError();

            if ($error != null) {

             
                $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                $qbstatus = 0;

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error ' . $err . ' </strong></div>');
            } else {
                $err      = "Success";
                $qbstatus = 1;
            
                $this->session->set_flashdata('success', 'Success');

            }
            $qbolog = array('syncType' => 'CQ','type' => 'product','merchantID' => $merchID, 'qbStatus'             => $qbstatus, 'qbAction'             => $qbaction, 'qbText' => $err,
                'createdAt'                  => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'qbActionID' => $prodid);
            $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbolog);
            if($syncid){
                $qbSyncID = 'CQ-'.$syncid;
               

                $qbSyncStatus = $qbaction.' <span class="cust_view"><a href="'.base_url().'QBO_controllers/Plan_Product/create_product_services/'.$prodid.'">'.$productName.'</a></span> '.$err;

                
                $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
            }
            redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));

        }
        
        

        // Assign item details to view variable
        $data['item_pro'] = $productListItem;

        // Fetch Group Items
        $data['item_data'] = [];
        if($productListItem && $productListItem['Type'] == 'Group'){
            $data['item_data'] = $this->general_model->get_table_data('qbo_group_lineitem', array('groupListID' => $productListItem['ListID']));
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $parent_list         = $this->general_model->get_table_data('QBO_test_item', array('merchantID' => $user_id, 'IsActive' => 'true'));
        $data['parent_list'] = $parent_list;

        $account_list         = $this->general_model->get_table_data('QBO_accounts_list', array('merchantID' => $user_id));
        $data['account_list'] = $account_list;

        $data['inventory_account'] = $this->general_model->get_table_data('QBO_accounts_list', array('accountType' => 'Other Current Asset', 'merchantID' => $user_id, 'accountTypeDetails' => 'Inventory'));

        $data['inventory_income'] = $this->general_model->get_table_data('QBO_accounts_list', array('accountType' => 'Income', 'merchantID' => $user_id, 'accountTypeDetails' => 'SalesOfProductIncome'));

        $data['inventory_expense'] = $this->general_model->get_table_data('QBO_accounts_list', array('accountType' => 'Cost of Goods Sold', 'merchantID' => $user_id, 'accountTypeDetails' => 'SuppliesMaterialsCogs'));

        $data['items']   = $this->qbo_company_model->getQboNonGroupProducts($user_id);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/create_product', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_plan_product()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
            $merchID = $user_id;
        }

        $val = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),

        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        //Add a new Invoice
        
        $error = $dataService->getLastError();

        if ($error != null) {
            echo "The Status code is: " . $error->getHttpStatusCode() . "\n";
            echo "The Helper message is: " . $error->getOAuthHelperError() . "\n";
            echo "The Response message is: " . $error->getResponseBody() . "\n";
            exit();
        }

        foreach ($entities as $oneInvoice) {

            $QBO_item_details['productID']         = $oneInvoice->Id;
            $QBO_item_details['IsActive']          = $oneInvoice->Active;
            $QBO_item_details['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
            $QBO_item_details['TimeModified']      = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));
            $QBO_item_details['Name']              = $oneInvoice->Name;
            $QBO_item_details['SKU']               = $oneInvoice->Sku;
            $QBO_item_details['SalesDescription']  = $oneInvoice->Description;
            $QBO_item_details['saleCost']          = $oneInvoice->UnitPrice;
            $QBO_item_details['Type']              = $oneInvoice->Type;
            $QBO_item_details['parent_ListID']     = $oneInvoice->ParentRef;
            $QBO_item_details['QuantityOnHand']    = $oneInvoice->QtyOnHand;
            $QBO_item_details['InvStartDate']      = $oneInvoice->InvStartDate;
            $QBO_item_details['AssetAccountRef']   = $oneInvoice->AssetAccountRef;
            $QBO_item_details['purchaseCost']      = $oneInvoice->PurchaseCost;
            $QBO_item_details['purchaseDesc']      = $oneInvoice->PurchaseDesc;
            $QBO_item_details['IncomeAccountRef']  = $oneInvoice->IncomeAccountRef;
            $QBO_item_details['ExpenseAccountRef'] = $oneInvoice->ExpenseAccountRef;
            $QBO_item_details['CompanyID']         = '';
            $QBO_item_details['merchantID']        = $merchID;

            $this->general_model->insert_row('QBO_test_item', $QBO_item_details);

        }
        #Create Logs for SYNC New Entry
        $qbo_log = array('syncType' => 'CQ','type' => 'productImport','qbStatus' => 1, 'qbAction' => 'ProductDataImport', 'qbActionID' => 0, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'), 'merchantID' => $merchID);
        $syncid = $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
        if($syncid){
            $qbSyncID = 'CQ-'.$syncid;
            $qbSyncStatus = 'Product Data Imported';
            $this->general_model->update_row_data('tbl_qbo_log', array('id' => $syncid),array('qbSyncID' => $qbSyncID,'qbSyncStatus' => $qbSyncStatus),array('qbSyncStatus'));
        }

        redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));

    }

    public function Item_detailes()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $merchID = $user_id;

        $val = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),
        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $qbo_data = $this->general_model->get_select_data('tbl_qbo_config', array('lastUpdated'), array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery'));

        if (!empty($qbo_data)) {
            $last_date = $qbo_data['lastUpdated'];
          
            $my_timezone = date_default_timezone_get();
            $from        = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to          = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date   = $this->general_model->datetimeconvqbo($last_date, $from, $to);
            $last_date1  = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

            $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));
			$sqlQuery = "SELECT * FROM Item where Active IN (false, true) AND Metadata.LastUpdatedTime > '$latedata'";
            
            
        } else {
			$sqlQuery = "SELECT * FROM Item where Active IN (false, true)";
		}
		
		$keepRunning = true;
		$entities = [];
		$st    = 0;
		$limit = 0;
		$total = 0;
		while($keepRunning === true){
            $mres   = MAXRESULT;
			$st_pos = MAXRESULT * $st + 1;
			$s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
			if ($s_data && !empty($s_data)) {
				$entities = array_merge($entities, $s_data);
				$total += count($s_data);
				$st = $st + 1;

				$limit = ($st) * MAXRESULT;
			} else {
				$keepRunning = false;
				break;
			}
		}
        
        $error = $dataService->getLastError();
        if ($error != null) {
			
            $this->session->set_flashdata('message', '<div class="alert alert-danger">' . QBO_DISCONNECT_MSG . '</div>');
			 redirect('QBO_controllers/home/index', 'refresh');
		}
        if (!empty($qbo_data)) {$updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
            $updatedata['updatedAt']                              = date('Y-m-d H:i:s');
            if (!empty($entities)) {
                $this->general_model->update_row_data('tbl_qbo_config', array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery'), $updatedata);
            }

        } else {
            $updateda = date('Y-m-d') . 'T' . date('H:i:s');
            $upteda   = array('merchantID' => $merchID, 'realmID' => $realmID, 'qbo_action' => 'ItemQuery', 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            if (!empty($entities)) {
                $this->general_model->insert_row('tbl_qbo_config', $upteda);
            }

        }

        if (!empty($entities)) {
            
            foreach ($entities as $oneInvoice) {

                $QBO_item_details['productID']         = $oneInvoice->Id;
                $QBO_item_details['IsActive']          = $oneInvoice->Active;
                $QBO_item_details['TimeCreated']       = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->CreateTime));
                $QBO_item_details['TimeModified']      = date('Y-m-d H:i:s', strtotime($oneInvoice->MetaData->LastUpdatedTime));
                $QBO_item_details['Name']              = $oneInvoice->Name;
                $QBO_item_details['SKU']               = $oneInvoice->Sku;
                $QBO_item_details['SalesDescription']  = $oneInvoice->Description;
                $QBO_item_details['saleCost']          = $oneInvoice->UnitPrice;
                $QBO_item_details['Type']              = $oneInvoice->Type;
                $QBO_item_details['parent_ListID']     = $oneInvoice->ParentRef;
                $QBO_item_details['QuantityOnHand']    = $oneInvoice->QtyOnHand;
                $QBO_item_details['InvStartDate']      = $oneInvoice->InvStartDate;
                $QBO_item_details['AssetAccountRef']   = $oneInvoice->AssetAccountRef;
                $QBO_item_details['purchaseCost']      = $oneInvoice->PurchaseCost;
                $QBO_item_details['purchaseDesc']      = $oneInvoice->PurchaseDesc;
                $QBO_item_details['IncomeAccountRef']  = $oneInvoice->IncomeAccountRef;
                $QBO_item_details['ExpenseAccountRef'] = $oneInvoice->ExpenseAccountRef;
                $QBO_item_details['CompanyID']         = $realmID;
                $QBO_item_details['merchantID']        = $merchID;
                if ($this->general_model->get_num_rows('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID)) > 0) {

                    $this->general_model->update_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID), $QBO_item_details);

                } else {

                    $this->general_model->insert_row('QBO_test_item', $QBO_item_details);

                }
                
                // IF items is group item then ItemGroupDetail
                if($oneInvoice->Type == 'Group')
                {
                    $qboTestItem = $this->general_model->get_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $oneInvoice->Id, 'merchantID' => $merchID));

                    // CHeck Group Child Items
                    if ($this->general_model->get_num_rows('qbo_group_lineitem', array('groupListID' => $qboTestItem['ListID'])) > 0) {
                        // Delete Previous 
                        $this->general_model->delete_row_data('qbo_group_lineitem', array('groupListID' => $qboTestItem['ListID']));
                    }


                    // Add Group Child Items
                    $priceTotal = 0;
                    if(!empty($oneInvoice->ItemGroupDetail)){
                        foreach($oneInvoice->ItemGroupDetail->ItemGroupLine as $groupLineItem)
                        {
                            $itemData =    $this->general_model->get_row_data('QBO_test_item', array('CompanyID' => $realmID, 'productID' => $groupLineItem->ItemRef, 'merchantID' => $merchID));

                            if ($itemData) {
                                $input_data1 = array();
                                $input_data1['itemListID'] = $itemData['ListID'];
                                $input_data1['FullName']   = $itemData['Name'];
                                $input_data1['Quantity']   = $groupLineItem->Qty;
                                $input_data1['Price']     = $itemData['saleCost'];
                                $input_data1['groupListID'] = $qboTestItem['ListID'];

                                $priceTotal += (float)$itemData['saleCost'];
                            }
                            $this->general_model->insert_row('qbo_group_lineitem', $input_data1);
                            
                        }
                    }
                    

                    // Update Product Price
                    $this->general_model->update_row_data('QBO_test_item', array('ListID' => $qboTestItem['ListID']), ['saleCost' => $priceTotal ]);
                }
            }
        }
        
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $in_data['merchantID'] = $merchID;
        $cond                  = array("merchantID" => $merchID);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/plan_products', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }
    public function product_ajax(){
        if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		} else if ($this->session->userdata('user_logged_in')) {
			$user_id = $this->session->userdata('user_logged_in');
			$merchID = $user_id['merchantID'];
		}
        $cond                  = array("merchantID" => $merchID);
        $plans = $this->general_model->get_product_table_data('QBO_test_item', $cond);
       $count = $this->general_model->get_product_table_count('QBO_test_item', $cond);
        $data = array();
		$no = $_POST['start'];
        if(isset($plans) && $plans)
        {
            foreach($plans as $plan)
            {
                $no++;
                $row = array();
                $url = base_url('QBO_controllers/Plan_Product/create_product_services/'.$plan['productID']);
                $row[] = "<div class='text-left cust_view'><a href='" . $url . "' data-toggle='tooltip' title='Edit' >".$plan['Name']."</a>";
                $row[] = "<div class='text-left hidden-xs'>".$plan['SalesDescription']."";
                $row[] = "<div class='text-left'>".$plan['Type']."";
                $row[] = "<div class='hidden-xs text-right'>$".number_format((float)$plan['purchaseCost'],2)."";
                $row[] = "<div class='hidden-xs text-right'>$".number_format((float)$plan['saleCost'],2)."";
                $data[] = $row;
            }
        }
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
		die;
    }
    public function det_plan_product()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchID = $user_id;

        $val = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),

        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $id  = $this->czsecurity->xssCleanPostInput('productID');
        $act = $this->czsecurity->xssCleanPostInput('st_act');
        if ($act == 0) {
            $entities = $dataService->Query("SELECT * FROM Item where Id='" . $id . "'  ");
            if (empty($entities)) {
                exit();
            }
//No Record for the

            $proServices = reset($entities);
            $dateTime    = new \DateTime('NOW');
            $updateItem  = Item::update($proServices, [

                "Active" => false,

            ]);
            $resultingItemsUpdatedObj = $dataService->Update($updateItem);
            if ($resultingItemsUpdatedObj) {
                $this->general_model->update_row_data('QBO_test_item', array('productID' => $id, 'merchantID' => $merchID), array('IsActive' => 'false'));

            }
        } else {

            $entities = $dataService->Query("SELECT * FROM Item where ID='" . $id . "' and Active=FALSE");

            if (empty($entities)) {
                exit();
            }
//No Record for the

            $proServices = reset($entities);
            $dateTime    = new \DateTime('NOW');
            $updateItem  = Item::update($proServices, [

                "Active" => true,

            ]);
            $resultingItemsUpdatedObj = $dataService->Update($updateItem);

            $error = $dataService->getLastError();

            if ($resultingItemsUpdatedObj) {
                $this->general_model->update_row_data('QBO_test_item', array('productID' => $id, 'merchantID' => $merchID), array('IsActive' => 'true'));

            }

        }

        redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));

    }

    public function activate_product()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        $merchID = $user_id;

        $val = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),

        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

        $id = $this->czsecurity->xssCleanPostInput('productID');

        $entities = $dataService->Query("SELECT * FROM Item where Id='" . $id . "'   ");
        if (empty($entities)) {
            exit();
        }
//No Record for the

        $proServices = reset($entities);
        $dateTime    = new \DateTime('NOW');
        $updateItem  = Item::update($proServices, [

            "Active" => true,

        ]);
        $resultingItemsUpdatedObj = $dataService->Update($updateItem);

        $error = $dataService->getLastError();

        if ($resultingItemsUpdatedObj) {
            $this->general_model->update_row_data('QBO_test_item', array('productID' => $id, 'merchantID' => $merchID), array('IsActive' => 'true'));

        }

        redirect(base_url('QBO_controllers/Plan_Product/Item_detailes'));

    }

    public function get_accounting_details()
    {

        if ($this->session->userdata('logged_in')) {
            $user_id = $this->session->userdata('logged_in')['merchID'];
            $merchID = $user_id;
        }

        $val = array(
            'merchantID' => $merchID,
        );

        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),

        ));

        $allAccounts = $dataService->FindAll('Account');

        foreach ($allAccounts as $oneAccount) {
            $QBO_item_details['accountID']          = $oneAccount->Id;
            $QBO_item_details['accountName']        = $oneAccount->Name;
            $QBO_item_details['accountType']        = $oneAccount->AccountType;
            $QBO_item_details['accountTypeDetails'] = $oneAccount->AccountSubType;

            $this->general_model->insert_row('QBO_accounts_list', $QBO_item_details);

        }

    }

    public function plan_product_details()
    {

        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $productID = $this->czsecurity->xssCleanPostInput('productID');
        $condition = array('productID' => $productID, 'merchantID' => $user_id);
        $plandata  = $this->general_model->get_row_data('QBO_test_item', $condition);
        if (!empty($plandata)) {
            ?>
		 <table class="table table-bordered table-striped table-vcenter"   >
            <thead>
                <tr>
                    <th class="text-left">Attribute</th>
                    <th class="visible-lg text-left">Details</th>
                </tr>
            </thead>
            <tbody>

				<tr>
					<th class="text-left"><strong>Item ID</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['productID']; ?></a></td>
			  </tr>
			<tr>
					<th class="text-left"><strong>Name</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['Name']; ?></a></td>
			</tr>
            <tr>
					<th class="text-left"><strong>SKU</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['SKU']; ?></a></td>
			</tr>
			<tr>
					<th class="text-left"><strong>Description</strong></th>
					<td class="text-left visible-lg"><?php echo ($plandata['SalesDescription']) ? $plandata['SalesDescription'] : '---'; ?></a></td>
			</tr>
           <tr>
					<th class="text-left"><strong>Type</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['Type']; ?></a></td>
			</tr>
            <tr>
					<th class="text-left"><strong> Parent ListID</strong></th>
					<td class="text-left visible-lg"><?php echo ($plandata['parent_ListID']) ? $plandata['parent_ListID'] : '--'; ?></a></td>
			  </tr>
              <tr>
					<th class="text-left"><strong>  Quantity On Hand</strong></th>
					<td class="text-left visible-lg"><?php echo ($plandata['QuantityOnHand']) ? $plandata['QuantityOnHand'] : '--'; ?></a></td>
			</tr>
            	<tr>
					<th class="text-left"><strong>  Sales Price</strong></th>
					<td class="text-left visible-lg">$<?php echo number_format($plandata['saleCost'], 2); ?></a></td>
			</tr>

			<tr>
					<th class="text-left"><strong> Purchase Cost</strong></th>
					<td class="text-left visible-lg">$<?php echo number_format($plandata['purchaseCost'], 2); ?></a></td>
			</tr>
			<tr>
					<th class="text-left"><strong>  Purchase Description</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['purchaseDesc']; ?></a></td>
			</tr>

			<tr>
					<th class="text-left"><strong>  Created Time</strong></th>
					<td class="text-left visible-lg"><?php if (!empty($plandata['TimeCreated'])) {
                echo date('M d, Y', strtotime($plandata['TimeCreated']));
            } else {
                echo "";
            }
            ?></a></td>
			</tr>

			<tr>
					<th class="text-left"><strong>  Modified Time</strong></th>
					<td class="text-left visible-lg"><?php if (!empty($plandata['TimeModified'])) {
                echo date('M d, Y', strtotime($plandata['TimeModified']));
            } else {
                echo "";
            }
            ?></a></td>
			</tr>

			</tbody>
        </table>

	<?php }

        die;

    }

    public function fetchQboData(){

        require APPPATH .'libraries/qbo/QBO_data.php';

        $qboOBJ = new QBO_data(56); 

        echo 'Yes';die;
    }

}