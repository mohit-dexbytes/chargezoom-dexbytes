<?php

/**
   * This Controller has Paytrace Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process

 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;
class PaytracePayment extends CI_Controller
{
    private $resellerID;
	private $transactionByUser;
    
	public function __construct()
	{
		parent::__construct();
		
	
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
		include APPPATH . 'third_party/PayTraceAPINEW.php';

		$this->load->config('paytrace');
		$this->load->model('quickbooks');
     	$this->load->model('general_model');
     	$this->load->model('card_model');
		$this->load->model('QBO_models/qbo_customer_model');
		$this->load->model('customer_model');
        $this->db1 = $this->load->database('otherdb', TRUE);
		  if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='1')
		  {
			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 	$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];

		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	    
	   	redirect('QBO_controllers/home','refresh'); 
	}
	

	 
public function pay_invoice()
{
   
	             if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}


	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID'); 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
		 $cusproID=''; $error=array();
          $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');		 
    		$resellerID = $this->resellerID;	    
    		  
			
	 if($cardID!="" && $gateway!="")
	 {  
               $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
		 
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   $merchantID=$merchID;
		 
             	$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
				$integratorId  = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
		       $crtxnID='';$inID='';		  
		 if(!empty($in_data)){ 
			
				$Customer_ListID = $in_data['CustomerListID'];
         
              
           if($cardID=='new1')
           {
                         	
			        	$cardID_upd  =$cardID;
			        	$c_data   = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID, 'merchantID'=>$merchantID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				        $merchantID = $merchID;
           
                          $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID'   => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                         $this->db1->select('*')->from('customer_card_data');
                          $qq = $this->db1->get();
         
                         if($qq->num_rows()>0 ){
                           $card_type = $this->general_model->getType($card_no);
                           
                           $cardID    = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
                                             'CardType'  =>$card_type,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                        
                         }else{
                         
                           	$card_type = $this->general_model->getType($card_no);
                           	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}

                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
                                            'CardType'  =>$card_type,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
        
        
         
         
				 $card_data      =   $this->card_model->get_single_card_data($cardID);
				 
			 if(!empty($card_data))
			 {
                 $val = array(
                    'merchantID' => $merchID,
                );
                $data = $this->general_model->get_row_data('QBO_token',$val);

                $accessToken = $data['accessToken'];
                $refreshToken = $data['refreshToken'];
                 $realmID      = $data['realmID']; 
                $dataService = DataService::Configure(array(
                'auth_mode' => 'oauth2',
                'ClientID'  => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
                'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
                'accessTokenKey' =>  $accessToken,
                'refreshTokenKey' => $refreshToken,
                'QBORealmID' => $realmID,
                'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
                ));

               $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                {
             
             
						
						      $cr_amount = 0;
							 $amount  =	 $in_data['BalanceRemaining']; 
						
						   $amount    =	  $this->czsecurity->xssCleanPostInput('inv_amount');  
							    
					       $amount    = $amount-$cr_amount;
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$cvv      = $card_data['CardCVV'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
				
						$payAPI  = new PayTraceAPINEW();	
					 
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    
						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                    

		 if(!$oauth_moveforward){ 
		
		
		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				$name = $in_data['fullName'];
				$address = $in_data['ShipAddress_Addr1'];
				$city = $in_data['ShipAddress_City'];
				$state = $in_data['ShipAddress_State'];
				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
				
			
				$invoice_number = rand('100000','200000');
				$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "invoice_id"=>$invoice_number,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);  
                 
				      $request_data = json_encode($request_data); 
				      
			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
			if ( $result['http_status_code']=='200' ){
				
				// add level three data in transaction
                if($response['success']){
                    $level_three_data = [
                        'card_no' => $card_no,
                        'merchID' => $merchID,
                        'amount' => $amount,
                        'token' => $oauth_token,
                        'integrator_id' => $integratorId,
                        'transaction_id' => $response['transaction_id'],
                        'invoice_id' => $invoice_number,
                        'gateway' => 3,
                    ];
                    addlevelThreeDataInTransaction($level_three_data);
                }
				
				$val = array(
					'merchantID' => $merchID,
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID']; 
				$dataService = DataService::Configure(array(
				'auth_mode' => 'oauth2',
				'ClientID'  => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
				'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
				'accessTokenKey' =>  $accessToken,
				'refreshTokenKey' => $refreshToken,
				'QBORealmID' => $realmID,
				'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
	            
	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
				
				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
					$theInvoice = current($targetInvoiceArray);
				}
				$updatedInvoice = Invoice::update($theInvoice, [
				    "sparse " => 'true'
				]);
				
				$updatedResult = $dataService->Update($updatedInvoice);
				
				$newPaymentObj = Payment::create([
				    "TotalAmt" => $amount,
				    "SyncToken" => $updatedResult->SyncToken,
				    "CustomerRef" => $updatedResult->CustomerRef,
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $updatedResult->Id,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				    ]);
				$savedPayment = $dataService->Add($newPaymentObj);
				
				
				
				$error = $dataService->getLastError();
			if ($error != null) {
            $err='';
			$err= "The Status code is: " . $error->getHttpStatusCode() . "\n";
				$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
				$err.="The Response message is: " . $error->getResponseBody() . "\n";
				 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error! </strong> '.$err.'</div>');

				
			}
			else {
			     $inID = $updatedResult->Id;
			    	$crtxnID = $savedPayment->Id;

			  
			    $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
				
			} 
					   } else{
					   
							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
					
				  }	   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					 
					    
					     if ($error == null) {
					    $transactiondata['qbListTxnID']   = $crtxnID;
					    }
						$transactiondata['transactionType']    = 'pay_sale';
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      =$in_data['CustomerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']   = $merchantID;
					    
					    $transactiondata['resellerID']   =$this->resellerID;
					    $transactiondata['gateway']      = "Paytrace";
					    $transactiondata['invoiceID']    = $invoiceID;     
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}


				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication not valid.</div>'); 
				}	   
			 
			
          
		     }
                else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid Invoice</div>'); 
			     }
             
             }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
	        }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }		 
			 if($cusproID!=""){
			 	 redirect('QBO_controllers/home/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');
		   	 }

    }     




	public function create_new_sale()
    {



		$username ="demo123";
		$password ="demo123";
		$grant_type ="password";
		
		$payAPI  = new PayTraceAPINEW();
		
		$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $username, $password);

		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			// Build the transaction 
			    $request_data = array(
                    "amount" => "2.50",
                    "credit_card"=> array (
                         "number"=> "4111111111111122",
                         "expiration_month"=> "12",
                         "expiration_year"=> "2020"),
                    "csc"=> "999",
                    "billing_address"=> array(
                        "name"=> "Mark Smith",
                        "street_address"=> "8320 E. West St.",
                        "city"=> "Spokane",
                        "state"=> "WA",
                        "zip"=> "85284")
                    );
    
            $request_data = json_encode($request_data);
   
			
			$result =$payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
		
					
			  $json = $payAPI->jsonDecode($result['temp_json_response']);  
			  	
			  foreach($json['errors'] as $error =>$no_of_errors )
				{
					//Do you code here as an action based on the particular error number 
					//you can access the error key with $error in the loop as shown below.
					echo "<br>". $error;
					// to access the error message in array assosicated with each key.
					foreach($no_of_errors as $item)
					{
					   //Optional - error message with each individual error key.
						echo "  " . $item ; 
					} 
				}
	die;
			
			
		}

	}	







	
	
	
		 
	public function create_customer_sale()
    {
           
		if(!empty($this->input->post(null, true))){
			
			    $inv_array=array();
			    $inv_invoice=array();
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  if($gatlistval !="" && !empty($gt_result) )
			{
			    if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
					$resellerID = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'),array('merchID'=> $merchantID))['resellerID'];
    		$payusername   = $gt_result['gatewayUsername'];
   	        $paypassword   = $gt_result['gatewayPassword'];
			$integratorId  = $gt_result['gatewaySignature'];

     	    $grant_type    = "password";
    		
		$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
				
				
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 
		
		if(!$oauth_moveforward)
		{
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					}
					
					$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				      $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
					$invoice_number = rand('100000','200000');
					$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "invoice_id"=>$invoice_number,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);
					    $request_data = json_encode($request_data);
						$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
						
				       $response = $payAPI->jsonDecode($result['temp_json_response']); 
			    
	
				 if ( $result['http_status_code']=='200' )
				 {
				 	// add level three data in transaction
                    if($response['success']){
                        $level_three_data = [
                            'card_no' => $card_no,
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $response['transaction_id'],
                            'invoice_id' => $invoice_number,
                            'gateway' => 3,
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }
				      $invoiceIDs=array();
				 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id'))){
				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				 }
				    
				             	$val = array('merchantID' => $merchantID);
								$data = $this->general_model->get_row_data('QBO_token',$val);
                                
								$accessToken = $data['accessToken'];
								$refreshToken = $data['refreshToken'];
								 $realmID      = $data['realmID'];
						
								$dataService = DataService::Configure(array(
								'auth_mode' => 'oauth2',
								'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
								'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
								'accessTokenKey' =>  $accessToken,
								'refreshTokenKey' => $refreshToken,
								'QBORealmID' =>  $realmID,
								'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
								));
						
			            	$qblist=array();
				           if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				                $theInvoice = '';
        							  
        						       
        							   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        							  $invse = $inID;
        								$targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '".$invse."'");
        							
        					
        								
        								if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
        								{
        									$theInvoice = current($targetInvoiceArray);
        								
            								$updatedInvoice = Invoice::update($theInvoice, [
            									"sparse " => 'true'
            								]);
            									$con = array('invoiceID'=>$invse,'merchantID'=>$merchantID);
            									$res = $this->general_model->get_select_data('QBO_test_invoice',array('BalanceRemaining'), $con);
            									$amount_data = $res['BalanceRemaining'];
            								$updatedResult = $dataService->Update($updatedInvoice);
            								
            								$newPaymentObj = Payment::create([
            									"TotalAmt" => $amount,
            									"SyncToken" => $updatedResult->SyncToken,
            									"CustomerRef" => $updatedResult->CustomerRef,
            									"Line" => [
            									 "LinkedTxn" =>[
            											"TxnId" => $updatedResult->Id,
            											"TxnType" => "Invoice",
            										],    
            									   "Amount" => $amount_data
            									]
            									]);
            							 
            								$savedPayment = $dataService->Add($newPaymentObj);
        								
        								
        								
            								$error = $dataService->getLastError();
            							if ($error != null)
            							{
            						
            								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
            
            								redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
            								
            							}
            							else 
            							{
            							    
            							    $pinv_id='';
            							      $inv_array['inv_no'][]= $invse;
									    $inv_array['inv_amount'][]= $amount_data;
									    $inv_invoice[]  =   $invse;
										$pinv_id   =  $savedPayment->Id;
										$qblist[]  = $pinv_id;
            							  
            							
            							$this->session->set_flashdata('success', 'Transaction Successful');
                
            							}
        						}
				              }
						 
				    }
				     
				     
				     
				     
				 /* This block is created for saving Card info in encrypted form  */
				 
				 if($this->czsecurity->xssCleanPostInput('card_number')!=""){
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');		
				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					     
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							  $card_type = $this->general_model->getType($card_no);
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
                                            'CardType'   =>$card_type,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
                        	$customerListID = $this->czsecurity->xssCleanPostInput('customerID');
                          	$card_type = $this->general_model->getType($card_no);
                          	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
                                           'CardType'    =>$card_type,    
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										 'customerListID' =>$customerListID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				   
				
				$this->session->set_flashdata('success', 'Successfully Inserted Card');
                
				 }else{
					 
	               if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_sale';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = 	$resellerID;
                        $transactiondata['gateway']   = "Paytrace";
                           if(!empty($invoiceIDs) && !empty($inv_array))
				        {
					      $transactiondata['invoiceID']            = implode(',',$inv_invoice);
					      $transactiondata['invoiceRefID']         = json_encode($inv_array);
					      if(!empty($qblist))
					      $transactiondata['qbListTxnID']          = implode(',',$qblist); 
				        }   
					         
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}


				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication failed.</div>'); 
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select gateway.</div>'); 
		}			
					   
			  // die;
                       
				       redirect('QBO_controllers/Payments/create_customer_sale','refresh');
        }
              
				
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
			 	$user_id 				= $data['login_info']['merchID'];
			     $condition				= array('merchantID'=>$user_id );
			    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
				$compdata				= $this->customer_model->get_customers_data($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_sale', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	

	
		 
	public function create_customer_auth()
    {
         
		if(!empty($this->input->post(null, true))){
			
			    
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  if($gatlistval !="" && !empty($gt_result) )
			{
			
    		$payusername   = $gt_result['gatewayUsername'];
   	        $paypassword   = $gt_result['gatewayPassword'];
     	    $grant_type    = "password";
    		
				
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				
		$comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
				
				
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					}
					
					$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				      $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
					
					$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);
					    $request_data = json_encode($request_data);
						$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_AUTHORIZATION );
						
				       $response = $payAPI->jsonDecode($result['temp_json_response']); 
			
	
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
				 if($this->czsecurity->xssCleanPostInput('card_number')!=""){
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');		
				        $friendlyname   =  $this->czsecurity->xssCleanPostInput('friendlyname');
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					     
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',  
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$customerListID = $this->czsecurity->xssCleanPostInput('customerID');
							$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										 'customerListID' =>$customerListID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				  
				 
				     	
			        	$this->session->set_flashdata('success', 'Transaction Successful');
				     
				 }else{
					 
	               if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					   
						$transactiondata['transactionType']    = 'pay_auth';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $this->resellerID;
                        $transactiondata['gateway']   = "Paytrace";
					         
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);  

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}

				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication failed.</div>'); 
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select gateway.</div>'); 
		}			
                       
				       redirect('QBO_controllers/Payments/payment_capture','refresh');
        }
              
				
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
			 	$user_id 				= $data['login_info']['merchID'];
			     $condition				= array('merchantID'=>$user_id );
			    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
				$compdata				= $this->customer_model->get_customers_data($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_auth', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}		 

	
	

	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
			
			    $tID         = $this->czsecurity->xssCleanPostInput('txnID2');
			      $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    $gatlistval  = $paydata['gatewayID'];
				$merchantID = $this->session->userdata('logged_in')['merchID']; 
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
				$integratorId = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
			
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			    
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_CAPTURE);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
				   
			
				   
				   
			    	
				}
				
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 	
				 	$card_last_number = $paydata['transactionCard'];
				 	$card_detail = $this->db1->select('CardType')->from('customer_card_data')->where('merchantID = '.$merchantID.' AND customerCardfriendlyName like "%'.$card_last_number.'%"')->get()->row_array();

				 	$cardType = isset($card_detail['CardType']) ? strtolower($card_detail['CardType']) : '';

				 	// add level three data in transaction
                    if($response['success']){
                        $level_three_data = [
                            'card_no' => '',
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $response['transaction_id'],
                            'invoice_id' => '',
                            'gateway' => 3,
                            'card_type' => $cardType
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }

					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"success");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
									    
				    	
				    $this->session->set_flashdata('success', 'Successfully Captured Authorization');
                
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']   = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_capture';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']  =  $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paytrace";					
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}    
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication failed.</div>'); 
		}		
			 		   
			   
                       
				   redirect('QBO_controllers/Payments/payment_capture','refresh');
        }
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
			     $tID       = $this->czsecurity->xssCleanPostInput('txnvoidID2');
				   $con     = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
	            $gatlistval  = $paydata['gatewayID'];
				$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type   = "password";
			   $merchantID = $this->session->userdata('logged_in')['merchID'];
			  
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			 
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_VOID_TRANSACTION);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    	
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"success");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
								   
				     $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.'.$response['status_message']);
				 
				     
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']           = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_void';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']  = $merchantID;
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paytrace";
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication failed.</div>'); 
		}		
					   
			   
                       
				        redirect('QBO_controllers/Payments/payment_capture','refresh');
        }
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	



	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
	             $tID       = $this->czsecurity->xssCleanPostInput('paytxnID');
				  $con      = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
			     $gatlistval  = $paydata['gatewayID'];
				  
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			  
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];
				 $merchantID = $paydata['merchantID'];
				 $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
                 $amount     = $total ;
				if($paydata['transactionCode']=='200'){
					 $request_data = array( "transaction_id" => $tID,'amount'=>$amount );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    		
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' )
				 {
				 /* This block is created for saving Card info in encrypted form  */
				
			
			   
			   $c_data               =  $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID'=>$customerID, 'merchantID'=>$merchantID));
			   
			       
				 if(!empty($paydata['invoiceID']))
				{
			  
			  
				$val = array(
					'merchantID' => $paydata['merchantID'],
				);
				$data = $this->general_model->get_row_data('QBO_token',$val);

				$accessToken = $data['accessToken'];
				$refreshToken = $data['refreshToken'];
				 $realmID      = $data['realmID'];
				$dataService = DataService::Configure(array(
				'auth_mode' => 'oauth2',
				'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
				'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
				'accessTokenKey' =>  $accessToken,
				'refreshTokenKey' => $refreshToken,
				'QBORealmID' =>  $realmID,
				'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
				));
		
			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
			   
			   
			    $refund =$amount;
				    $ref_inv =explode(',',$paydata['invoiceID']);
				    if(count($ref_inv)>1)
				    {
				      $ref_inv_amount = json_decode($paydata['invoiceRefID']);
				      $imn_amount =  $ref_inv_amount->inv_amount; 
				      $p_ids     =   explode(',',$paydata['qbListTxnID']);
				     
				      $inv_array = array();
				      foreach($ref_inv as $k=> $r_inv)
				      {
				          $re_amount = $imn_amount[$k];
				          if($refund > 0 && $imn_amount[$k] >0)
				          {
				              
				              
				            $p_refund1=0;  
				              
				            $p_id  =  $p_ids[$k];
				              				           
				            if($refund <= $imn_amount[$k] )
				            {
				                $p_refund1 =  $refund;
				                  
				                  
				                   $p_refund =   $imn_amount[$k] -$refund;
				                 
				                  $re_amount =$imn_amount[$k] -$refund;
				                  $refund =0;
				               
				            }
				            else
				            {
				                 $p_refund =0;
				                 $p_refund1 =  $imn_amount[$k];
				                 $refund=$refund-$imn_amount[$k];
				                 
				                 $re_amount =0;
				            }
				            
        				       
        				       $item_data = $this->qbo_customer_model->get_invoice_item_data($r_inv);
                			   if(!empty($item_data))
                			   {
                			   $LineObj = Line::create([
                                   "Amount" => $p_refund1,
                                    "DetailType" => "SalesItemLineDetail",
                    		            "SalesItemLineDetail" => [
                    			        "ItemRef" =>$item_data['itemID'] ,
                    			       ]
                    			      
                    			  ]);
                    			  
                    			  if(!empty($item_data['AssetAccountRef']))
                    			  {
                    			     $acc_id = $item_data['AssetAccountRef'];
                    			     $acc_name = $item_data['AssetAccountName'];
                    			   }
                    			   if(!empty($item_data['IncomeAccountRef']))
                    			   {
                    			        $acc_id = $item_data['IncomeAccountRef'];
                    			     $acc_name = $item_data['IncomeAccountName'];
                    			   }
                    			      if(!empty($item_data['ExpenseAccountRef']))
                    			      {
                    			         $acc_id = $item_data['ExpenseAccountRef'];
                    			     $acc_name = $item_data['ExpenseAccountName'];  
                    			      }
                    		     $acc_data = $this->general_model->get_select_data('QBO_accounts_list',array('accountID'),array('accountName'=>'Undeposited Funds'));
                    		     if(!empty($acc_data))
                    		     {
                    		          $ac_value =$acc_data['accountID'];
                    		     }else{
                    		        $ac_value ='4'; 
                    		     }
                    		   	$theResourceObj = RefundReceipt::create([
                        		    "CustomerRef" => $paydata['customerListID'], 
                		            "Line" => $LineObj,
                		          	"DepositToAccountRef" =>  [
                    			        "value" =>$ac_value ,
                    			        "name"=> "Undeposited Funds",
                    			       ]
                		      ]);	  
                    		$resultingObj = $dataService->Add($theResourceObj);
                			$error = $dataService->getLastError();
                					if ($error != null)
                				 {
                				
                                      	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error:In Creating QuickBooks Refund '.$error.'</strong></div>'); 
                                      		redirect('QBO_controllers/Payments/payment_transaction','refresh');
                                  }else{
                                   $ins_id = $resultingObj->Id;
                                   
                                            $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$p_refund1,
            				  	           'creditInvoiceID'=>$r_inv,'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           );	
            				  	        $ppid=  $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr); 
                                   
                                    
                			   }
        				       
                			   }   
        				   
                            $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount;
        				           
				          }else{
				               
				                $inv_array['inv_no'][]=$r_inv;
                            $inv_array['inv_amount'][]=$re_amount; 
				           }  
				            
				          
				      }
				       $this->general_model->update_row_data('customer_transaction',$con,array('invoiceRefID'=>json_encode($inv_array)));
				     }
				     else
				     {
				         
				     
			   
			   
			   
			   $item_data = $this->qbo_customer_model->get_invoice_item_data($paydata['invoiceID']);
			   if(!empty($item_data))
			   {
			   $LineObj = Line::create([
                   "Amount" => $total,
                    "DetailType" => "SalesItemLineDetail",
    		            "SalesItemLineDetail" => [
    			        "ItemRef" =>$item_data['itemID'] ,
    			       ]
    			      
    			  ]);
    			  
    			  if(!empty($item_data['AssetAccountRef']))
    			  {
    			     $acc_id = $item_data['AssetAccountRef'];
    			     $acc_name = $item_data['AssetAccountName'];
    			   }
    			   if(!empty($item_data['IncomeAccountRef']))
    			   {
    			        $acc_id = $item_data['IncomeAccountRef'];
    			     $acc_name = $item_data['IncomeAccountName'];
    			   }
    			      if(!empty($item_data['ExpenseAccountRef']))
    			      {
    			         $acc_id = $item_data['ExpenseAccountRef'];
    			     $acc_name = $item_data['ExpenseAccountName'];  
    			      }
    		     $acc_data = $this->general_model->get_select_data('QBO_accounts_list',array('accountID'),array('accountName'=>'Undeposited Funds'));
    		     if(!empty($acc_data))
    		     {
    		          $ac_value =$acc_data['accountID'];
    		     }else{
    		        $ac_value ='4'; 
    		     }
    		   	$theResourceObj = RefundReceipt::create([
        		    "CustomerRef" => $paydata['customerListID'], 
		            "Line" => $LineObj,
		          	"DepositToAccountRef" =>  [
    			        "value" =>$ac_value ,
    			        "name"=> "Undeposited Funds",
    			       ]
		      ]);	  
    		$resultingObj = $dataService->Add($theResourceObj);
			$error = $dataService->getLastError();	  
			 
			 
					
					if ($error != null)
				 {
				
                      	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error:In Creating QuickBooks Refund '.$error.'</strong></div>'); 
                      		redirect('QBO_controllers/Payments/payment_transaction','refresh');
                  }
                   $ins_id = $resultingObj->Id;
                    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
				  	           'creditInvoiceID'=>$paydata['invoiceID'],'creditTransactionID'=>$tID,
				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
				  	          
				  	           );	
				  	            $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);  
			   }	
             }
				
				}	
               
				$this->customer_model->update_refund_payment($tID, 'PAYTRACE');
				 
				
					
					
				

		        
		 $this->session->set_flashdata('success', 'Successfully Refunded Payment </strong> '.$response['status_message']);
 	
			
			 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error:</strong> '.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_refund';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']   = $merchantID;
					     if(!empty($paydata['invoiceID']))
        			     	{
        				      $transactiondata['invoiceID']  = $paydata['invoiceID'];
        			     	}
					   $transactiondata['resellerID']   = $this->resellerID;
					   $transactiondata['gateway']   = "Paytrace";
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication failed.</div>'); 
		}		
					   
		
                       
				   redirect('QBO_controllers/Payments/refund_transaction','refresh');
      }
              
				
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('QBO_views/payment_refund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }

public function pay_multi_invoice()
{
   
	             if($this->session->userdata('logged_in') ){
			           $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') ){
			           $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}

    
    		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
    		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
             $merchantID           =      $merchID ;
    		 	$error='';
    		 $resellerID = $this->resellerID; 	 
        		
    			
             if($cardID!="" && $gateway!="")
    	     {  
    	       $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			   
		 
             	$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
				$integratorId  = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
	    
            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');
         if(!empty($invoices)) 
         { 
            foreach($invoices as $key=> $invoiceID)
            { 
                	$pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
	            $in_data   =    $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
            
          
			if(!empty($in_data))
    		{ 
    		
				$Customer_ListID = $in_data['CustomerListID'];
         
              
           if($cardID=='new1')
           {
			        	$cardID_upd  =$cardID;
			        	$c_data   = $this->general_model->get_select_data('QBO_custom_customer',array('companyID'), array('Customer_ListID'=>$Customer_ListID, 'merchantID'=>$merchantID));
			        	$companyID = $c_data['companyID'];
           
           
                           $this->load->library('encrypt');
                           $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				        $merchantID = $merchID;
           
                          $this->db1->where(array('customerListID' =>$in_data['Customer_ListID'],'merchantID'   => $merchantID,'customerCardfriendlyName'=>$friendlyname));
                         $this->db1->select('*')->from('customer_card_data');
                          $qq = $this->db1->get();
         
                         if($qq->num_rows()>0 ){
                         
                           $cardID = $qq->row_array()['CardID'];
                          $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										 'customerCardfriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                          $this->db1->where(array('CardID' =>$cardID));
                          $this->db1->update('customer_card_data', $card_data);	
                         }else{
                         	$is_default = 0;
					     	$checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchantID);
				        	if($checkCustomerCard == 0){
				        		$is_default = 1;
				        	}
                         	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
										 
								
				           $this->db1->insert('customer_card_data', $card_data);	
                           $cardID =$this->db1->insert_id();
                         }
           
           }
        
        
         
         
				 $card_data      =   $this->card_model->get_single_card_data($cardID);
			
			 if(!empty($card_data))
			 {
						
						   $cr_amount = 0;
						
						
						   $amount    =$pay_amounts;
					       $amount    = $amount-$cr_amount;
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$cvv      = $card_data['CardCVV'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						  
						$payAPI  = new PayTraceAPINEW();	
					 
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                    
						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                    

		          if(!$oauth_moveforward)
		          { 
		
		
                		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
                			
                				//set Authentication value based on the successful oAuth response.
                				//Add a space between 'Bearer' and access _token 
                				$oauth_token = sprintf("Bearer %s",$json['access_token']);
                			
                				$name = $in_data['fullName'];
                				$address = $in_data['ShipAddress_Addr1'];
                				$city = $in_data['ShipAddress_City'];
                				$state = $in_data['ShipAddress_State'];
                				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
                				
                			
                				$invoice_number = rand('100000','200000');
                				$request_data = array(
                                    "amount" => $amount,
                                    "credit_card"=> array (
                                         "number"=> $card_no,
                                         "expiration_month"=>$expmonth,
                                         "expiration_year"=>$exyear ),
                                    "csc"=> $cvv,
                                     "invoice_id"=>$invoice_number,
                                    "billing_address"=> array(
                                        "name"=>$name,
                                        "street_address"=> $address,
                                        "city"=> $city,
                                        "state"=> $state,
                                        "zip"=> $zipcode
                						)
                					);  
                                 
                				      $request_data = json_encode($request_data); 
                				   
                			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
                				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
                			
                	                  
                				   if ( $result['http_status_code']=='200' )
                				   {
                					// add level three data in transaction
				                    if($response['success']){
				                        $level_three_data = [
				                            'card_no' => $card_no,
				                            'merchID' => $merchantID,
				                            'amount' => $amount,
				                            'token' => $oauth_token,
				                            'integrator_id' => $integratorId,
				                            'transaction_id' => $response['transaction_id'],
				                            'invoice_id' => $invoice_number,
				                            'gateway' => 3,
				                        ];
				                        addlevelThreeDataInTransaction($level_three_data);
				                    }
				                    
                				$val = array(
                					'merchantID' => $merchID,
                				);
                				$data = $this->general_model->get_row_data('QBO_token',$val);
                
                				$accessToken = $data['accessToken'];
                				$refreshToken = $data['refreshToken'];
                				 $realmID      = $data['realmID']; 
                				$dataService = DataService::Configure(array(
                				'auth_mode' => 'oauth2',
                				'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
                				'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
                				'accessTokenKey' =>  $accessToken,
                				'refreshTokenKey' => $refreshToken,
                				'QBORealmID' => $realmID,
                				'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
                				));
                		
                			   $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                	            
                	            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                				
                				if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
                					$theInvoice = current($targetInvoiceArray);
                				}
                				$updatedInvoice = Invoice::update($theInvoice, [
                				    "sparse " => 'true'
                				]);
                				
                				$updatedResult = $dataService->Update($updatedInvoice);
                				
                				$newPaymentObj = Payment::create([
                				    "TotalAmt" => $amount,
                				    "SyncToken" => $updatedResult->SyncToken,
                				    "CustomerRef" => $updatedResult->CustomerRef,
                				    "Line" => [
                				     "LinkedTxn" =>[
                				            "TxnId" => $updatedResult->Id,
                				            "TxnType" => "Invoice",
                				        ],    
                				       "Amount" => $amount
                			        ]
                				    ]);
                				$savedPayment = $dataService->Add($newPaymentObj);
                				
			
				
				$error = $dataService->getLastError();
			if ($error != null) 
			{
		      	$err= "The Status code is: " . $error->getHttpStatusCode() . "\n";
					$err.="The Helper message is: " . $error->getOAuthHelperError() . "\n";
					$err.="The Response message is: " . $error->getResponseBody() . "\n";
			
				
			}
			else 
			{
			    $invoiceID =$updatedResult->Id;
			    
				
					 $this->session->set_flashdata('success', 'Successfully Processed Invoice');
 
			} 
		  } 
                        		  else
                        		  {

                                                    if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}

                                                    $this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                        		 }	   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					    if ($error == null) {
					    $transactiondata['qbListTxnID']          = $savedPayment->Id;
					    }
						$transactiondata['transactionType']    = 'pay_sale';
						$transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      =$in_data['CustomerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['merchantID']   = $merchantID;
					    $transactiondata['resellerID']   = $resellerID;
					    $transactiondata['gateway']   = "Paytrace";
					    $transactiondata['invoiceID']   = $invoiceID;     
					    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}
				else
				{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication not valid.</div>'); 
				}	   
			
			
          
          
          
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Customer has no card.</div>'); 
			 }
		
		 
	       	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
			 
         }
         }
         else
         {
            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Please select invoices.</div>');    
         }	 	 
			 
			 
			 
	        }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }		 
			 
		   	 redirect('QBO_controllers/Create_invoice/Invoice_details','refresh');

    }     



       
       
       
 
	
}