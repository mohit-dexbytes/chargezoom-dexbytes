<?php
class OAuthSimpleException extends Exception {}


class OAuthSimple {
    var $_secrets;
    var $_default_signature_method;
    var $_action;
    var $_nonce_chars;

   
    public function __construct ($APIKey = "",$sharedSecret=""){
        if (!empty($APIKey))
            $this->_secrets{'consumer_key'}=$APIKey;
        if (!empty($sharedSecret))
            $this->_secrets{'shared_secret'}=$sharedSecret;
        $this->_default_signature_method="HMAC-SHA1";
        $this->_action="GET";
        $this->_nonce_chars="0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return $this;
    }

    /** reset the parameters and url
    *
    */
    function reset() {
        $this->_parameters=null;
        $this->path=null;
        $this->sbs=null;
        return $this;
    }

    /** set the parameters either from a hash or a string
    *
    * @param {string,object} List of parameters for the call, this can either be a URI string (e.g. "foo=bar&gorp=banana" or an object/hash)
    */
    function setParameters ($parameters=Array()) {

        if (is_string($parameters))
            $parameters = $this->_parseParameterString($parameters);
        if (empty($this->_parameters))
            $this->_parameters = $parameters;
        elseif (!empty($parameters))
            $this->_parameters = array_merge($this->_parameters,$parameters);
        if (empty($this->_parameters['oauth_nonce']))
            $this->_getNonce();
        if (empty($this->_parameters['oauth_timestamp']))
            $this->_getTimeStamp();
        if (empty($this->_parameters['oauth_consumer_key']))
            $this->_getApiKey();
        if (empty($this->_parameters['oauth_token']))
            $this->_getAccessToken();
        if (empty($this->_parameters['oauth_signature_method']))
            $this->setSignatureMethod();
        if (empty($this->_parameters['oauth_version']))
            $this->_parameters['oauth_version']="1.0";
        return $this;
    }

    // convienence method for setParameters
    function setQueryString ($parameters) {
        return $this->setParameters($parameters);
    }

    /** Set the target URL (does not include the parameters)
    *
    * @param path {string} the fully qualified URI (excluding query arguments) (e.g "http://example.org/foo")
    */
    function setURL ($path) {
        if (empty($path))
            throw new OAuthSimpleException('No path specified for OAuthSimple.setURL');
        $this->_path=$path;
        return $this;
    }

    /** convienence method for setURL
    *
    * @param path {string} see .setURL
    */
    function setPath ($path) {
        return $this->_path=$path;
    }

    /** set the "action" for the url, (e.g. GET,POST, DELETE, etc.)
    *
    * @param action {string} HTTP Action word.
    */
    function setAction ($action) {
        if (empty($action))
            $action = 'GET';
        $action = strtoupper($action);
        if (preg_match('/[^A-Z]/',$action))
            throw new OAuthSimpleException('Invalid action specified for OAuthSimple.setAction');
        $this->_action = $action;
        return $this;
    }

    /** set the signatures (as well as validate the ones you have)
    *
    * @param signatures {object} object/hash of the token/signature pairs {api_key:, shared_secret:, oauth_token: oauth_secret:}
    */
    function signatures ($signatures) {
        if (!empty($signatures) && !is_array($signatures))
            throw new OAuthSimpleException('Must pass dictionary array to OAuthSimple.signatures');
        if (!empty($signatures)){
            if (empty($this->_secrets)) {
                $this->_secrets=Array();
            }
            $this->_secrets=array_merge($this->_secrets,$signatures);
        }
        // Aliases
        if (isset($this->_secrets['api_key']))
            $this->_secrets['consumer_key'] = $this->_secrets['api_key'];
        if (isset($this->_secrets['access_token']))
            $this->_secrets['oauth_token'] = $this->_secrets['access_token'];
        if (isset($this->_secrets['access_secret']))
            $this->_secrets['oauth_secret'] = $this->_secrets['access_secret'];
        if (isset($this->_secrets['access_token_secret']))
            $this->_secrets['oauth_secret'] = $this->_secrets['access_token_secret'];
        if (isset($this->_secrets['rsa_private_key']))
            $this->_secrets['private_key'] = $this->_secrets['rsa_private_key'];
         if (isset($this->_secrets['rsa_public_key']))
            $this->_secrets['public_key'] = $this->_secrets['rsa_public_key'];
        // Gauntlet
        if (empty($this->_secrets['consumer_key']))
            throw new OAuthSimpleException('Missing required consumer_key in OAuthSimple.signatures');
        if (empty($this->_secrets['shared_secret']))
            throw new OAuthSimpleException('Missing requires shared_secret in OAuthSimple.signatures');
        if (!empty($this->_secrets['oauth_token']) && empty($this->_secrets['oauth_secret']))
            throw new OAuthSimpleException('Missing oauth_secret for supplied oauth_token in OAuthSimple.signatures');
        return $this;
    }

    function setTokensAndSecrets($signatures) {
        return $this->signatures($signatures);
    }

    /** set the signature method (currently only Plaintext or SHA-MAC1)
    *
    * @param method {string} Method of signing the transaction (only PLAINTEXT and SHA-MAC1 allowed for now)
    */
    function setSignatureMethod ($method="") {
        if (empty($method))
            $method = $this->_default_signature_method;
        $method = strtoupper($method);
        switch($method)
        {
            case 'RSA-SHA1':
                $this->_parameters['oauth_signature_method']=$method;
                break;
            case 'PLAINTEXT':
            case 'HMAC-SHA1':
                $this->_parameters['oauth_signature_method']=$method;
                break;
            default:
                throw new OAuthSimpleException ("Unknown signing method $method specified for OAuthSimple.setSignatureMethod");
        }
        return $this;
    }

    function sign($args=array()) {
        if (!empty($args['action']))
            $this->setAction($args['action']);
        if (!empty($args['path']))
            $this->setPath($args['path']);
        if (!empty($args['method']))
            $this->setSignatureMethod($args['method']);
        if (!empty($args['signatures']))
            $this->signatures($args['signatures']);
        if (empty($args['parameters']))
            $args['parameters']=array();        // squelch the warning.
        $this->setParameters($args['parameters']);
        $normParams = $this->_normalizedParameters();
        $this->_parameters['oauth_signature'] = $this->_generateSignature($normParams);
        return Array(
            'parameters' => $this->_parameters,
            'signature' => $this->_oauthEscape($this->_parameters['oauth_signature']),
            'signed_url' => $this->_path . '?' . $this->_normalizedParameters('true'),
            'header' => $this->getHeaderString(),
            'sbs'=> $this->sbs
            );
    }

   
    function getHeaderString ($args=array()) {
        if (empty($this->_parameters['oauth_signature']))
            $this->sign($args);

        $result = 'OAuth ';

        foreach ($this->_parameters as $pName=>$pValue)
        {
            if (strpos($pName,'oauth_') !== 0)
                continue;
            if (is_array($pValue))
            {
                foreach ($pValue as $val)
                {
                    $result .= $pName .'="' . $this->_oauthEscape($val) . '", ';
                }
            }
            else
            {
                $result .= $pName . '="' . $this->_oauthEscape($pValue) . '", ';
            }
        }
        return preg_replace('/, $/','',$result);
    }

   
    function _parseParameterString ($paramString) {
        $elements = explode('&',$paramString);
        $result = array();
        foreach ($elements as $element)
        {
            list ($key,$token) = explode('=',$element);
            if ($token)
                $token = urldecode($token);
            if (!empty($result[$key]))
            {
                if (!is_array($result[$key]))
                    $result[$key] = array($result[$key],$token);
                else
                    array_push($result[$key],$token);
            }
            else
                $result[$key]=$token;
        }
        return $result;
    }

    function _oauthEscape($string) {
        if ($string === 0)
            return 0;
        if (empty($string))
            return '';
        if (is_array($string))
            throw new OAuthSimpleException('Array passed to _oauthEscape');
        $string = rawurlencode($string);
        $string = str_replace('+','%20',$string);
        $string = str_replace('!','%21',$string);
        $string = str_replace('*','%2A',$string);
        $string = str_replace('\'','%27',$string);
        $string = str_replace('(','%28',$string);
        $string = str_replace(')','%29',$string);
        return $string;
    }

    function _getNonce($length=5) {
        $result = '';
        $cLength = strlen($this->_nonce_chars);
        for ($i=0; $i < $length; $i++)
        {
            $rnum = rand(0,$cLength);
            $result .= substr($this->_nonce_chars,$rnum,1);
        }
        $this->_parameters['oauth_nonce'] = $result;
        return $result;
    }

    function _getApiKey() {
        if (empty($this->_secrets['consumer_key']))
        {
            throw new OAuthSimpleException('No consumer_key set for OAuthSimple');
        }
        $this->_parameters['oauth_consumer_key']=$this->_secrets['consumer_key'];
        return $this->_parameters['oauth_consumer_key'];
    }

    function _getAccessToken() {
        if (!isset($this->_secrets['oauth_secret']))
            return '';
        if (!isset($this->_secrets['oauth_token']))
            throw new OAuthSimpleException('No access token (oauth_token) set for OAuthSimple.');
        $this->_parameters['oauth_token'] = $this->_secrets['oauth_token'];
        return $this->_parameters['oauth_token'];
    }

    function _getTimeStamp() {
        return $this->_parameters['oauth_timestamp'] = time();
    }

    function _normalizedParameters($filter='false') {
        $elements = array();
        $ra = 0;
        ksort($this->_parameters);
        foreach ( $this->_parameters as $paramName=>$paramValue) {
           if($paramName=='xml'){
               if($filter=="true")
                   continue;
               }
            if (preg_match('/\w+_secret/',$paramName))
                continue;
            if (is_array($paramValue))
            {
                sort($paramValue);
                foreach($paramValue as $element)
                    array_push($elements,$this->_oauthEscape($paramName).'='.$this->_oauthEscape($element));
                continue;
            }
            array_push($elements,$this->_oauthEscape($paramName).'='.$this->_oauthEscape($paramValue));

        }
        return join('&',$elements);
    }

    function _readFile($filePath) {

           $fp = fopen($filePath,"r");

        $file_contents = fread($fp,8192);

        fclose($fp);

        return $file_contents;
    }

    function _generateSignature () {
        $secretKey = '';
    if(isset($this->_secrets['shared_secret']))
        $secretKey = $this->_oauthEscape($this->_secrets['shared_secret']);
    $secretKey .= '&';
    if(isset($this->_secrets['oauth_secret']))
            $secretKey .= $this->_oauthEscape($this->_secrets['oauth_secret']);
        switch($this->_parameters['oauth_signature_method'])
        {
            case 'RSA-SHA1':

                $publickey = "";
                // Fetch the public key
                if($publickey = openssl_get_publickey($this->_readFile($this->_secrets['public_key']))){

                }else{
                    throw new OAuthSimpleException('Cannot access public key for signing');
                }
                
                $privatekeyid = "";
                // Fetch the private key
                if($privatekeyid = openssl_pkey_get_private($this->_readFile($this->_secrets['private_key'])))
                {
                    // Sign using the key
                     $this->sbs = $this->_oauthEscape($this->_action).'&'.$this->_oauthEscape($this->_path).'&'.$this->_oauthEscape($this->_normalizedParameters());

                       $ok = openssl_sign($this->sbs, $signature, $privatekeyid);

                      // Release the key resource
                    openssl_free_key($privatekeyid);

                       return base64_encode($signature);

                }else{
                    throw new OAuthSimpleException('Cannot access private key for signing');
                }


            case 'PLAINTEXT':
                return urlencode($secretKey);

            case 'HMAC-SHA1':
                $this->sbs = $this->_oauthEscape($this->_action).'&'.$this->_oauthEscape($this->_path).'&'.$this->_oauthEscape($this->_normalizedParameters());
                return base64_encode(hash_hmac('sha1',$this->sbs,$secretKey,true));

            default:
                throw new OAuthSimpleException('Unknown signature method for OAuthSimple');
        }
    }
}



class XeroOAuthException extends Exception {
}
class XeroOAuth {
	var $_xero_defaults;
	var $_xero_consumer_options;
	var $_action;
	var $_nonce_chars;
	
	/**
	 * Creates a new XeroOAuth object
	 *
	 * @param string $config,
	 *        	the configuration settings
	 */
	function __construct($config) {
		$this->params = array ();
		$this->headers = array ();
		$this->auto_fixed_time = false;
		$this->buffer = null;
		$this->request_params = array();
		
		if (! empty ( $config ['application_type'] )) {
			switch ($config ['application_type']) {
				case "Public" :
					$this->_xero_defaults = array (
							'xero_url' => 'https://api.xero.com/',
							'site' => 'https://api.xero.com',
							'authorize_url' => 'https://api.xero.com/oauth/Authorize',
							'signature_method' => 'HMAC-SHA1' 
					);
					break;
				case "Private" :
					$this->_xero_defaults = array (
							'xero_url' => 'https://api.xero.com/',
							'site' => 'https://api.xero.com',
							'authorize_url' => 'https://api.xero.com/oauth/Authorize',
							'signature_method' => 'RSA-SHA1' 
					);
					break;
				case "Partner" :
					$this->_xero_defaults = array (
							'xero_url' => 'https://api.xero.com/',
							'site' => 'https://api.xero.com',
							'authorize_url' => 'https://api.xero.com/oauth/Authorize',
							'signature_method' => 'RSA-SHA1' 
					);
					break;
			}
		}
		
		$this->_xero_consumer_options = array (
				'request_token_path' => 'oauth/RequestToken',
				'access_token_path' => 'oauth/AccessToken',
				'authorize_path' => 'oauth/Authorize' 
		);
		
		// Remove forced dependency on BASE_PATH constant.
		// Note that __DIR__ is PHP 5.3 and above only.
		$base_path = defined ( 'BASE_PATH' ) ? BASE_PATH : dirname ( __DIR__ );
		
		$this->_xero_curl_options = array ( // you probably don't want to change any of these curl values
				'curl_connecttimeout' => 30,
				'curl_timeout' => 20,
				// for security you may want to set this to TRUE. If you do you need
				// to install the servers certificate in your local certificate store.
				'curl_ssl_verifypeer' => 2,
				// include ca-bundle.crt from http://curl.haxx.se/ca/cacert.pem
				
				'curl_followlocation' => false, // whether to follow redirects or not
				                                // TRUE/1 is not a valid ssl verifyhost value with curl >= 7.28.1 and 2 is more secure as well.
				                                // More details here: http://php.net/manual/en/function.curl-setopt.php
			
				// support for proxy servers
				'curl_proxy' => false, // really you don't want to use this if you are using streaming
				'curl_proxyuserpwd' => false, // format username:password for proxy, if required
				'curl_encoding' => '', // leave blank for all supported formats, else use gzip, deflate, identity
				'curl_verbose' => true 
		);
		
		$this->config = array_merge ( $this->_xero_defaults, $this->_xero_consumer_options, $this->_xero_curl_options, $config );
	}
	

	private function curlHeader($ch, $header) {
		$i = strpos ( $header, ':' );
		if (! empty ( $i )) {
			$key = str_replace ( '-', '_', strtolower ( substr ( $header, 0, $i ) ) );
			$value = trim ( substr ( $header, $i + 2 ) );
			$this->response ['headers'] [$key] = $value;
		}
		return strlen ( $header );
	}
	

	private function curlWrite($ch, $data) {
		$l = strlen ( $data );
		if (strpos ( $data, $this->config ['streaming_eol'] ) === false) {
			$this->buffer .= $data;
			return $l;
		}
		
		$buffered = explode ( $this->config ['streaming_eol'], $data );
		$content = $this->buffer . $buffered [0];
		
		$this->metrics ['tweets'] ++;
		$this->metrics ['bytes'] += strlen ( $content );
		
		if (! function_exists ( $this->config ['streaming_callback'] ))
			return 0;
		
		$metrics = $this->update_metrics ();
		$stop = call_user_func ( $this->config ['streaming_callback'], $content, strlen ( $content ), $metrics );
		$this->buffer = $buffered [1];
		if ($stop)
			return 0;
		
		return $l;
	}
	

	function extract_params($body) {
		$kvs = explode ( '&', $body );
		$decoded = array ();
		foreach ( $kvs as $kv ) {
			$kv = explode ( '=', $kv, 2 );
			$kv [0] = $this->safe_decode ( $kv [0] );
			$kv [1] = $this->safe_decode ( $kv [1] );
			$decoded [$kv [0]] = $kv [1];
		}
		return $decoded;
	}
	

	private function safe_encode($data) {
		if (is_array ( $data )) {
			return array_map ( array (
					$this,
					'safe_encode' 
			), $data );
		} else if (is_scalar ( $data )) {
			return str_ireplace ( array (
					'+',
					'%7E' 
			), array (
					' ',
					'~' 
			), rawurlencode ( $data ) );
		} else {
			return '';
		}
	}
	

	private function safe_decode($data) {
		if (is_array ( $data )) {
			return array_map ( array (
					$this,
					'safe_decode' 
			), $data );
		} else if (is_scalar ( $data )) {
			return rawurldecode ( $data );
		} else {
			return '';
		}
	}
	

	private function prepare_method($method) {
		$this->method = strtoupper ( $method );
	}

	private function curlit() {
		$this->request_params = array();
	
		
		// configure curl
		$c = curl_init ();
		$useragent = (isset ( $this->config ['user_agent'] )) ? (empty ( $this->config ['user_agent'] ) ? 'XeroOAuth-PHP' : $this->config ['user_agent']) : 'XeroOAuth-PHP';
		curl_setopt_array ( $c, array (
				CURLOPT_USERAGENT => $useragent,
				CURLOPT_CONNECTTIMEOUT => $this->config ['curl_connecttimeout'],
				CURLOPT_TIMEOUT => $this->config ['curl_timeout'],
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_SSL_VERIFYPEER => $this->config ['curl_ssl_verifypeer'],
				
				CURLOPT_FOLLOWLOCATION => $this->config ['curl_followlocation'],
				CURLOPT_PROXY => $this->config ['curl_proxy'],
				CURLOPT_ENCODING => $this->config ['curl_encoding'],
				CURLOPT_URL => $this->sign ['signed_url'],
				CURLOPT_VERBOSE => $this->config ['curl_verbose'],
				// process the headers
				CURLOPT_HEADERFUNCTION => array (
						$this,
						'curlHeader' 
				),
				CURLOPT_HEADER => FALSE,
				CURLINFO_HEADER_OUT => TRUE 
		) );
		
	
		
		if ($this->config ['curl_proxyuserpwd'] !== false)
			curl_setopt ( $c, CURLOPT_PROXYUSERPWD, $this->config ['curl_proxyuserpwd'] );
		
		if (isset ( $this->config ['is_streaming'] )) {
			// process the body
			$this->response ['content-length'] = 0;
			curl_setopt ( $c, CURLOPT_TIMEOUT, 0 );
			curl_setopt ( $c, CURLOPT_WRITEFUNCTION, array (
					$this,
					'curlWrite' 
			) );
		}
		
		switch ($this->method) {
			case 'GET' :
				$contentLength = 0;
				break;
			case 'POST' :
				curl_setopt ( $c, CURLOPT_POST, TRUE );
				$post_body = $this->safe_encode ( $this->xml );
				curl_setopt ( $c, CURLOPT_POSTFIELDS, $post_body );
				$this->request_params ['xml'] = $post_body;
				$contentLength = strlen ( $post_body );
				$this->headers ['Content-Type'] = 'application/x-www-form-urlencoded';
				
				break;
			case 'PUT' :
				$fh = tmpfile();
				if ($this->format == "file") {
					$put_body = $this->xml;
				} else {
					$put_body = $this->safe_encode ( $this->xml );
					$this->headers ['Content-Type'] = 'application/x-www-form-urlencoded';
				}
				fwrite ( $fh, $put_body );
				rewind ( $fh );
				curl_setopt ( $c, CURLOPT_PUT, true );
				curl_setopt ( $c, CURLOPT_INFILE, $fh );
				curl_setopt ( $c, CURLOPT_INFILESIZE, strlen ( $put_body ) );
				$contentLength = strlen ( $put_body );
				
				break;
			default :
				curl_setopt ( $c, CURLOPT_CUSTOMREQUEST, $this->method );
		}
		
		if (! empty ( $this->request_params )) {
			// if not doing multipart we need to implode the parameters
			if (! $this->config ['multipart']) {
				foreach ( $this->request_params as $k => $v ) {
					$ps [] = "{$k}={$v}";
				}
				$this->request_payload = implode ( '&', $ps );
			}
			curl_setopt ( $c, CURLOPT_POSTFIELDS, $this->request_payload);
		} else {
			// CURL will set length to -1 when there is no data
			$this->headers ['Content-Length'] = $contentLength;
		}
		
		$this->headers ['Expect'] = '';
		
		if (! empty ( $this->headers )) {
			foreach ( $this->headers as $k => $v ) {
				$headers [] = trim ( $k . ': ' . $v );
			}
			curl_setopt ( $c, CURLOPT_HTTPHEADER, $headers );
		}
		
		if (isset ( $this->config ['prevent_request'] ) && false == $this->config ['prevent_request'])
			return;
			
			// do it!
		$response = curl_exec ( $c );
		if ($response === false) {
			$response = 'Curl error: ' . curl_error ( $c );
			$code = 1;
		} else {
			$code = curl_getinfo ( $c, CURLINFO_HTTP_CODE );
		}
		
		$info = curl_getinfo ( $c );
		
		curl_close ( $c );
		if (isset ( $fh )) {
			fclose( $fh );
		}
		
		// store the response
		$this->response ['code'] = $code;
		$this->response ['response'] = $response;
		$this->response ['info'] = $info;
		$this->response ['format'] = $this->format;
		return $code;
	}
	

	function request($method, $url, $params = array(), $xml = "", $format = 'xml') {
		// removed these as function parameters for now
		$useauth = true;
		$multipart = false;
		$this->headers = array ();
		
		if (isset ( $format )) {
			switch ($format) {
				case "pdf" :
					$this->headers ['Accept'] = 'application/pdf';
					break;
				case "json" :
					$this->headers ['Accept'] = 'application/json';
					break;
				case "xml" :
				default :
					$this->headers ['Accept'] = 'application/xml';
					break;
			}
		}
		
		if (isset ( $params ['If-Modified-Since'] )) {
			$modDate = "If-Modified-Since: " . $params ['If-Modified-Since'];
			$this->headers ['If-Modified-Since'] = $params ['If-Modified-Since'];
		}
		
		if ($xml !== "") {
			$xml = trim($xml);
			$this->xml = $xml;
		}
		
		if ($method == "POST")
			$params ['xml'] = $xml;
		
		$this->prepare_method ( $method );
		$this->config ['multipart'] = $multipart;
		$this->url = $url;
		$oauthObject = new OAuthSimple ();
		try {
			$this->sign = $oauthObject->sign ( array (
					'path' => $url,
					'action' => $method,
					'parameters' => array_merge ( $params, array (
							'oauth_signature_method' => $this->config ['signature_method'] 
					) ),
					'signatures' => $this->config 
			) );
		} 

		catch ( Exception $e ) {
			$errorMessage = 'XeroOAuth::request() ' . $e->getMessage ();
			$this->response['response'] = $errorMessage;
			$this->response['helper'] = $url;
			return $this->response;
		}
		$this->format = $format;
		
		$curlRequest = $this->curlit ();
		
		if ($this->response ['code'] == 401 && isset ( $this->config ['session_handle'] )) {
			if ((strpos ( $this->response ['response'], "oauth_problem=token_expired" ) !== false)) {
				$this->response ['helper'] = "TokenExpired";
			} else {
				$this->response ['helper'] = "TokenFatal";
			}
		}
		if ($this->response ['code'] == 403) {
			$errorMessage = "It looks like your Xero Entrust cert issued by Xero is either invalid or has expired. See http://developer.xero.com/api-overview/http-response-codes/#403 for more";
			// default IIS page isn't informative, a little swap
			$this->response ['response'] = $errorMessage;
			$this->response ['helper'] = "SetupIssue";
		}
		if ($this->response ['code'] == 0) {
			$errorMessage = "It looks like your Xero Entrust cert issued by Xero is either invalid or has expired. See http://developer.xero.com/api-overview/http-response-codes/#403 for more";
			$this->response ['response'] = $errorMessage;
			$this->response ['helper'] = "SetupIssue";
		}
		
		return $this->response;
	}
	

	function parseResponse($response, $format) {
		if (isset ( $format )) {
			switch ($format) {
				case "pdf" :
					$theResponse = $response;
					break;
				case "json" :
					$theResponse = json_decode ( $response );
					break;
				default :
					$theResponse = simplexml_load_string ( $response );
					break;
			}
		}
		return $theResponse;
	}
	

	function url($request, $api = "core") {
		if ($request == "RequestToken") {
			$this->config ['host'] = $this->config ['site'] . '/oauth/';
		} elseif ($request == "Authorize") {
			$this->config ['host'] = $this->config ['authorize_url'];
			$request = "";
		} elseif ($request == "AccessToken") {
			$this->config ['host'] = $this->config ['site'] . '/oauth/';
		} else {
			if (isset ( $api )) {
				if ($api == "core") {
					$api_stem = "api.xro";
					$api_version = $this->config ['core_version'];
				}
				if ($api == "payroll") {
					$api_stem = "payroll.xro";
					$api_version = $this->config ['payroll_version'];
				}
				if ($api == "file") {
					$api_stem = "files.xro";
					$api_version = $this->config ['file_version'];
				}
			}
			$this->config ['host'] = $this->config ['xero_url'] . $api_stem . '/' . $api_version . '/';
		}
		
		return implode ( array (
				$this->config ['host'],
				$request 
		) );
	}

	function refreshToken($accessToken, $sessionHandle) {
		$code = $this->request ( 'GET', $this->url ( 'AccessToken', '' ), array (
				'oauth_token' => $accessToken,
				'oauth_session_handle' => $sessionHandle 
		) );
		if ($this->response ['code'] == 200) {
			
			$response = $this->extract_params ( $this->response ['response'] );
			
			return $response;
		} else {
			$this->response ['helper'] = "TokenFatal";
			return $this->response;
		}
	}

	public static function php_self($dropqs = true)
	{
	$protocol = 'http';
	if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on')
		{
		$protocol = 'https';
		}
	elseif (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443'))
		{
		$protocol = 'https';
		}

	$url = sprintf('%s://%s%s', $protocol, $_SERVER['SERVER_NAME'], $_SERVER['REQUEST_URI']);
	$parts = parse_url($url);
	$port = $_SERVER['SERVER_PORT'];
	$scheme = $parts['scheme'];
	$host = $parts['host'];
	$path = @$parts['path'];
	$qs = @$parts['query'];
	$port or $port = ($scheme == 'https') ? '443' : '80';
	if (($scheme == 'https' && $port != '443') || ($scheme == 'http' && $port != '80'))
		{
		$host = "$host:$port";
		}

	$url = "$scheme://$host$path";
	if (!$dropqs) return "{$url}?{$qs}";
	  else return $url;
	}
	
	/*
	 * Run some basic checks on our config options etc to make sure all is ok
	 */
	function diagnostics() {
		$testOutput = array ();

		
		if ($this->config ['application_type'] == 'Partner' || $this->config ['application_type'] == 'Private') {
			
			if (! file_exists ( $this->config ['rsa_public_key'] ))
				$testOutput ['rsa_cert_error'] = "Can't read the self-signed SSL cert. Private and Partner API applications require a self-signed X509 cert http://developer.xero.com/documentation/advanced-docs/public-private-keypair/ \n";
			if (file_exists ( $this->config ['rsa_public_key'] )) {
				$data = openssl_x509_parse ( file_get_contents ( $this->config ['rsa_public_key'] ) );
				$validFrom = date ( 'Y-m-d H:i:s', $data ['validFrom_time_t'] );
				if (time () < $data ['validFrom_time_t']) {
					$testOutput ['ssl_cert_error'] = "Application cert not yet valid - cert valid from " . $validFrom . "\n";
				}
				$validTo = date ( 'Y-m-d H:i:s', $data ['validTo_time_t'] );
				if (time () > $data ['validTo_time_t']) {
					$testOutput ['ssl_cert_error'] = "Application cert cert expired - cert valid to " . $validFrom . "\n";
				}
			}
			if (! file_exists ( $this->config ['rsa_private_key'] ))
				$testOutput ['rsa_cert_error'] = "Can't read the self-signed cert key. Check your rsa_private_key config variable. Private and Partner API applications require a self-signed X509 cert http://developer.xero.com/documentation/advanced-docs/public-private-keypair/ \n";
			if (file_exists ( $this->config ['rsa_private_key'] )) {
				$cert_content = file_get_contents ( $this->config ['rsa_public_key'] );
				$priv_key_content = file_get_contents ( $this->config ['rsa_private_key'] );
				if (! openssl_x509_check_private_key ( $cert_content, $priv_key_content ))
					$testOutput ['rsa_cert_error'] = "Application certificate and key do not match \n";
				;
			}
		}
		
		return $testOutput;
	}
}





class Xero extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	    $this->load->model('general_model');
	   $this->load->library('session');
		
	
	}

	
		
			
		function testLinks()
        {
              $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
            
            if (isset($_SESSION['access_token']) || XRO_APP_TYPE == 'Private')
            {
                
                $in_data['accessToken'] = $_SESSION['access_token'];
                $merchID  = $this->session->userdata('logged_in')['merchID'];
                $in_data['merchantID'] =  $merchID;
                
                	$chk_condition = array('merchantID'=>$merchID);
                	$tok_data = $this->general_model->get_row_data('tbl_xero_token',$chk_condition);
                
               if(!empty($tok_data)){
			
			    $this->general_model->update_row_data('tbl_xero_token',$chk_condition, $in_data);
			   }
			   else
				{
					 $this->general_model->insert_row('tbl_xero_token', $in_data);
				  
				}
				
				    $data1['appIntegration']    = "4";
					$data1['merchantID']    = $merchID;
				
				$app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
				 if(!empty($app_integration)){
			
			    $this->general_model->update_row_data('app_integration_setting',$chk_condition, $data1);
				$user = $this->session->userdata('logged_in');
				$user['active_app'] = '4';
				$this->session->set_userdata('logged_in',$user);
			   }
			   else
				{
					 $this->general_model->insert_row('app_integration_setting', $data1);
					  
				     $user = $this->session->userdata('logged_in');
				     $user ['active_app'] = '4';
				     $this->session->set_userdata('logged_in',$user);
				  
				}
				
					redirect(base_url('Xero_controllers/Xero/success_auth'));
   
                      
				}
                
                
        
               
                if (XRO_APP_TYPE !== 'Private' && isset($_SESSION['access_token'])) {
                    echo '<li><a href="?wipe=1">Start Over and delete stored tokens</a></li>';
                } elseif(XRO_APP_TYPE !== 'Private') {
                    
                    echo "fjehj";die;
                    
                    $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/xeroAuth_process');
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
                   
                }
      
            
        }
        
        
     
        function persistSession($response)
        {
            if (isset($response)) {
                $_SESSION['access_token']       = $response['oauth_token'];
                $_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];
              	if(isset($response['oauth_session_handle']))  $_SESSION['session_handle']     = $response['oauth_session_handle'];
            } else {
                return false;
            }
        
        }
        
        
        function retrieveSession()
        {
            if (isset($_SESSION['access_token'])) {
                $response['oauth_token']            =    $_SESSION['access_token'];
                $response['oauth_token_secret']     =    $_SESSION['oauth_token_secret'];
                return $response;
            } else {
                return false;
            }
        
        }
        
        function outputError($XeroOAuth)
        {
            echo 'Error: ' . $XeroOAuth->response['response'] . PHP_EOL;
            pr($XeroOAuth);
        }
        
        /**
         * Debug function for printing the content of an object
         *
         * @param mixes $obj
         */
        function pr($obj)
        {
        
            if (!is_cli())
                echo '<pre style="word-wrap: break-word">';
            if (is_object($obj))
                print_r($obj);
            elseif (is_array($obj))
                print_r($obj);
            else
                echo $obj;
            if (!is_cli())
                echo '</pre>';
        }
        
        function is_cli()
        {
            return (PHP_SAPI == 'cli' && empty(getClientIpAddr()));
        }
		


   public function xero_int()
	{
	    $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  
	    define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
		}
		}
		define ( "OAUTH_CALLBACK", $url );



		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);

		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {
			
			$here = XeroOAuth::php_self ();
			session_start ();
			$oauthSession = $this->retrieveSession ();
			
		
			
			if (isset ( $_REQUEST ['oauth_verifier'] )) {
				$XeroOAuth->config ['access_token'] = $_SESSION ['oauth'] ['oauth_token'];
				$XeroOAuth->config ['access_token_secret'] = $_SESSION ['oauth'] ['oauth_token_secret'];
				
				$code = $XeroOAuth->request ( 'GET', $XeroOAuth->url ( 'AccessToken', '' ), array (
						'oauth_verifier' => $_REQUEST ['oauth_verifier'],
						'oauth_token' => $_REQUEST ['oauth_token'] 
				) );
				
				if ($XeroOAuth->response ['code'] == 200) {
					
					$response = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
					$session = $this->persistSession ( $response );
					
					unset ( $_SESSION ['oauth'] );
					header ( "Location: {$here}" );
				} else {
					$this->outputError ( $XeroOAuth );
				}
				// start the OAuth dance
			} elseif (isset ( $_REQUEST ['authenticate'] ) || isset ( $_REQUEST ['authorize'] )) {
				$params = array (
						'oauth_callback' => OAUTH_CALLBACK 
				);
				
				$response = $XeroOAuth->request ( 'GET', $XeroOAuth->url ( 'RequestToken', '' ), $params );
				
				if ($XeroOAuth->response ['code'] == 200) {
					
					$scope = "";
					if ($_REQUEST ['authenticate'] > 1)
						$scope = 'payroll.employees,payroll.payruns,payroll.timesheets';
					
					 $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] ); 
					$_SESSION ['oauth'] = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
					
					$authurl = $XeroOAuth->url ( "Authorize", '' ) . "?oauth_token={$_SESSION['oauth']['oauth_token']}&scope=" . $scope;
					redirect($authurl,'refresh');
					echo '<p>To complete the OAuth flow follow this URL: <a href="' . $authurl . '">' . $authurl . '</a></p>';
				} else {
					$this->outputError ( $XeroOAuth );
				}
			}
			
		$this->testLinks();
		}
		
	 
		
			  
	}
	
	public function success_auth()
	{
	          $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  
			   $merchID  = $this->session->userdata('logged_in')['merchID'];
			
			  $condition = array('merchID'=> $merchID); 
		   	 
		   	  $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/xero_Auth_success');
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	
	
}
	