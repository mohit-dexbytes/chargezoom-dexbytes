<?php

/**
 * This Controller has Paypal Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */
class PaypalPayment extends CI_Controller
{
    private $resellerID;
	private $transactionByUser;
    
	public function __construct()
	{
		parent::__construct();
	   
	    include APPPATH . 'third_party/PayPalAPINEW.php';
		$this->load->config('paypal');
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->model('company_model');
		$this->load->model('card_model');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
		  {
		    $logged_in_data = $this->session->userdata('logged_in');
			$this->resellerID = $logged_in_data['resellerID'];
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 	$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];
		  }else{
			redirect('login','refresh');
		  }
		  
	}
	
	
	public function index(){
	    
	
	    
	}
	
	  
	  
	   public function create_customer_sale()
       {
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
			   
			$invoiceIDs=array();	
		   if(!empty($this->input->post(null, true))){
				
				$country  = "US";
				 $currencyID = 'USD';
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   if($this->czsecurity->xssCleanPostInput('tc'))
			   $tc = 1;
			   else
			   $tc  =0;
			    if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			  	$custom_data_fields = [];
			  	$applySurcharge = false;
	            // get custom field data
	            if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
	            	$applySurcharge = true;
	                $custom_data_fields['invoice_number'] = $this->czsecurity->xssCleanPostInput('invoice_id');
	            }

	            if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {
	                $custom_data_fields['po_number'] = $this->czsecurity->xssCleanPostInput('po_number');
	            }
				$checkPlan = check_free_plan_transactions();
				if($checkPlan && $gatlistval !="" && !empty($gt_result) )
			   {
					
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);			
					
					
					
					 if($this->session->userdata('logged_in')){
					$user_id = $merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$user_id = $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					$customerID = $this->czsecurity->xssCleanPostInput('customerID');
					if(!$customerID || empty($customerID)){
						$customerID = create_card_customer($this->input->post(null, true), '1');
					}
					
		            $comp_data  =$this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
				    $companyID  = $comp_data['companyID'];
				    $comp  =$this->general_model->get_select_data('tbl_company',array('qbwc_username'), array('id'=>$companyID, 'merchantID'=>$merchantID));
				    $user  = $comp['qbwc_username'];
				    
				      $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
				      
                   	  if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
					    $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
				  }else {
					  
						  
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];
					}	
					/*Added card type in transaction table*/
	                $cardType = $this->general_model->getType($card_no);
	                $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	                $custom_data_fields['payment_type'] = $friendlyname;
                			
							 
					$paymentType 		=	'Sale';
					$companyName        = $this->czsecurity->xssCleanPostInput('companyName');
					$firstName 			= $this->czsecurity->xssCleanPostInput('fistName');
					$lastName 			= $this->czsecurity->xssCleanPostInput('lastName');
					$creditCardType 	= 'Visa';
					$creditCardNumber 	= $card_no;
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                    
					$expDateYear=  	$exyear;
					$cvv2Number =   $cvv;
					$address1	=   $this->czsecurity->xssCleanPostInput('address');
					$address2 	= '';
					$city 		=   $this->czsecurity->xssCleanPostInput('city');
					$state 		= $this->czsecurity->xssCleanPostInput('state');
					$zip 		= $this->czsecurity->xssCleanPostInput('zipcode');
					$email      = $this->czsecurity->xssCleanPostInput('email');
					$phone      = $this->czsecurity->xssCleanPostInput('phone');
				
					if($this->czsecurity->xssCleanPostInput('country')=="United States" ||$this->czsecurity->xssCleanPostInput('country')=="United State"){
					  $country 	= 'US';	// US or other valid country code
					  $currencyID = 'USD';
					 
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	  = 'CAD';	// US or other valid country code
					  $currencyID = 'CAD';
					}
			
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
					  
                    // update amount with surcharge 
	                if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){
	                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $amount;
	                    $amount += round($surchargeAmount, 2);
	                    $custom_data_fields['invoice_surcharge'] = $this->czsecurity->xssCleanPostInput('invoice_surcharge');
	                    $custom_data_fields['amount_with_out_sucharge'] = $this->czsecurity->xssCleanPostInput('totalamount');
	                    $custom_data_fields['surcharge_amount_value'] = $this->czsecurity->xssCleanPostInput('surchargeAmountOnly');
	                    
	                }
	                $totalamount  = $amount;
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);

						if(!empty($cvv2Number)){
							$CCDetails['cvv2'] = $cvv2Number;
						}
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID, 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

		                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
    						$new_invoice_number = getInvoiceOriginalID($this->czsecurity->xssCleanPostInput('invoice_id'), $merchantID, 2);

		                	$new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_id');
                    		
		                	$PaymentDetails['invnum'] = $new_invoice_number;
		                }

		                if (!empty($this->czsecurity->xssCleanPostInput('po_number'))) {

		                	$PaymentDetails['custom'] = 'PO Number: '.$this->czsecurity->xssCleanPostInput('po_number');
		                }

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
								
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
				
					
					if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
						$code = '111'; 
						
						if ($cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {
							$card_no = $this->czsecurity->xssCleanPostInput('card_number');
							$card_type      = $this->general_model->getType($card_no);
							$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
	
							$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
	
							$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'     => $card_type,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'customerListID' => $customerID,
								'companyID'     => $companyID,
								'merchantID'   => $merchantID,
	
								'createdAt' 	=> date("Y-m-d H:i:s"),
								'Billing_Addr1'	 => $address1,
								'Billing_Addr2'	 => $address2,
								'Billing_City'	 => $city,
								'Billing_State'	 => $state,
								'Billing_Country'	 => $country,
								'Billing_Contact'	 => $phone,
								'Billing_Zipcode'	 => $zip
							);
	
	
	
							$this->card_model->process_card($card_data);
						} 
                  $tranID ='' ;$amt='0.00';
				     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
                    					  
					$invoicePayAmounts = array();
				    if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
					{
				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
						$invoicePayAmounts = explode(',', $this->czsecurity->xssCleanPostInput('invoice_pay_amount'));
					}
				        $refnum=array();
				         if(!empty($invoiceIDs))
				           {
				           		$saleAmountRemaining = $amount;
				           		$payIndex = 0;
								foreach ($invoiceIDs as $inID) {
									$theInvoice = array();

									$theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));

									if (!empty($theInvoice))
									{
										
										$amount_data = $theInvoice['BalanceRemaining'];
										$actualInvoicePayAmount = $invoicePayAmounts[$payIndex];
										if($this->czsecurity->xssCleanPostInput('invoice_surcharge_type') == 'percentage' && $this->czsecurity->xssCleanPostInput('invoice_surcharge') != 'Ineligible' && $applySurcharge){

		                                    $surchargeAmount = ($this->czsecurity->xssCleanPostInput('invoice_surcharge') / 100) * $actualInvoicePayAmount;
		                                    $actualInvoicePayAmount += $surchargeAmount;
		                                    $updatedInvoiceData = [
		                                        'inID' => $inID,
		                                        'merchantID' => $user_id,
		                                        'amount' => $surchargeAmount,
		                                    ];
		                                    $this->general_model->updateSurchargeInvoice($updatedInvoiceData,2);
		                                    $theInvoice = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'));
		                                    $amount_data = $theInvoice['BalanceRemaining'];

		                                }
										$isPaid 	 = 'false';
										$BalanceRemaining = 0.00;
										if($saleAmountRemaining > 0){
											$refnum[] = $theInvoice['RefNumber'];
											if($amount_data == $actualInvoicePayAmount){
												$actualInvoicePayAmount = $amount_data;
												$isPaid 	 = 'true';

											}else{

												$actualInvoicePayAmount = $actualInvoicePayAmount;
												$isPaid 	 = 'false';
												$BalanceRemaining = $amount_data - $actualInvoicePayAmount;
												
												
											}
											$AppliedAmount = $theInvoice['AppliedAmount'] + $actualInvoicePayAmount;
											$tes = $this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $inID, 'IsPaid' => 'false'), array('BalanceRemaining' => $BalanceRemaining, 'AppliedAmount' => $AppliedAmount, 'IsPaid' => $isPaid));

											$transactiondata= array();
											$transactiondata['transactionID']       = $tranID;
											$transactiondata['transactionStatus']    = $PayPalResult["ACK"];
											$transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
											$transactiondata['transactionCode']     = $code;  
											$transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
											$transactiondata['transactionType']    = "Paypal_sale";	
											$transactiondata['gatewayID']          = $gatlistval;
											$transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
											$transactiondata['customerListID']      = $customerID;
											$transactiondata['transactionAmount']   = $actualInvoicePayAmount;
											$transactiondata['merchantID']         = $merchantID;
											$transactiondata['invoiceTxnID']      = $inID;
											$transactiondata['gateway']              = "Paypal";
											$transactiondata['resellerID']            =  $this->resellerID;
											$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                    					    if($custom_data_fields){
				                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
				                            }
                    					  	$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);		
                    				        if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											}
											if($custom_data_fields){
								                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
								             }
                    				       	$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                    				       
                    				        $comp_data1 = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$merchantID));
                    				        $user      = $comp_data1['qbwc_username'] ;
											
											if(!is_numeric($inID)){
	                    				       $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
											}
											
										}
										
									}
									$payIndex++;
									
								}
							
								
				              
				              
						 
				          }else{
				              
				                        	 $tranID ='' ;$amt='0.00';
                    					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
                    					   $transactiondata= array();
                    				       $transactiondata['transactionID']       = $tranID;
                    					   $transactiondata['transactionStatus']    = $PayPalResult["ACK"];
                    					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
                    					   $transactiondata['transactionCode']     = $code;  
                    					   $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
											$transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
                    						$transactiondata['transactionType']    = "Paypal_sale";	
                    						$transactiondata['gatewayID']          = $gatlistval;
                                           $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
                    					   $transactiondata['customerListID']      = $customerID;
                    					   $transactiondata['transactionAmount']   = $amt;
                    					    $transactiondata['merchantID']         = $merchantID;
                    					   $transactiondata['gateway']              = "Paypal";
                    					  $transactiondata['resellerID']            =  $this->resellerID;
					 					if($custom_data_fields){
			                                $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			                            }
					   					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					   					if(!empty($this->transactionByUser)){
										    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
										    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
										}
										if($custom_data_fields){
							                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
							             }
				                         $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                    			
				              
				          }
				     		$condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							
							if (!empty($refnum)) {
	                            $ref_number = implode(',', $refnum);
	                        } else {
	                            $ref_number = '';
	                        } 
							$tr_date   =date('Y-m-d H:i:s');
							$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];
				              
				            if($chh_mail =='1')
							{
							    
							 
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $tranID);
							}
				 
				    
				     if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zip,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
				    
				    
					 $this->session->set_flashdata('success','Transaction Successful'); 	
				
				} else{
					$code='401';
					 $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$responsetext.'"</strong>.</div>'); 
						 $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt =$PayPalResult["AMT"];  }
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $tranID;
					   $transactiondata['transactionStatus']    = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					   $transactiondata['transactionCode']     = $code;  
					   $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
					   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
						$transactiondata['transactionType']    = "Paypal_sale";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $amt;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Paypal";
					  $transactiondata['resellerID']   =  $this->resellerID;
					 	if($custom_data_fields){
                            $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                        }
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					} 
				
				
			   }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}	
				
				if(!$checkPlan){
					$PayPalResult['TRANSACTIONID'] = '';
				}

				$invoice_IDs = array();
				if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
					$invoice_IDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				}
			
				$receipt_data = array(
					'transaction_id' => (isset($PayPalResult['TRANSACTIONID'])) ? $PayPalResult['TRANSACTIONID'] : '',
					'IP_address' => getClientIpAddr(),
					'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
					'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
					'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
					'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
					'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
					'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
					'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
					'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
					'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
					'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
					'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
					'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
					'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
					'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
					'Contact' => $this->czsecurity->xssCleanPostInput('email'),
					'proccess_url' => 'Payments/create_customer_sale',
					'proccess_btn_text' => 'Process New Sale',
					'sub_header' => 'Sale',
					'checkPlan'	=> $checkPlan
				);
				
				$this->session->set_userdata("receipt_data",$receipt_data);
				$this->session->set_userdata("invoice_IDs",$invoice_IDs);
				
				
				redirect('home/transation_sale_receipt',  'refresh');
		   }
		   
		   
		   
		   
		   
		   
		   
	   }
	
	
	
	  
	   public function create_customer_auth()
       {
		   
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
				 
				
		   if(!empty($this->input->post(null, true))){
				
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			   $checkPlan = check_free_plan_transactions();
			   $custom_data_fields = []; 
			   $po_number = $this->czsecurity->xssCleanPostInput('po_number');
	           if (!empty($po_number)) {
	               $custom_data_fields['po_number'] = $po_number;
	           }
			   if($checkPlan && $gatlistval !="" && !empty($gt_result) )
			   {
					$username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					$option = array('API_UserName' => $username,
						'API_Password'       => $password,
						'API_Signature'      => $signature,
						'API_Endpoint'       => "https://api-3t.paypal.com/nvp",
						'envoironment'       => 'sandbox',
						'version'            => '123');
					
					 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}	
					
					 $customerID = $this->czsecurity->xssCleanPostInput('customerID');
					 if(!$customerID || empty($customerID)){
						$customerID = create_card_customer($this->input->post(null, true), '1');
					}
					
					$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID));
					$companyID  = $comp_data['companyID'];	
                   
                   
           	   if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
           	   {	
					    $card_no 	= $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth  	= $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear     = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv        = $this->czsecurity->xssCleanPostInput('cvv');
				  }
				  else 
				  {
					  
						    $cardID   = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card_no  = $card_data['CardNo'];
					    	$expmonth =  $card_data['cardMonth']; 
							$exyear   = $card_data['cardYear'];
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$cvv     = $card_data['CardCVV'];
					}			
					$cardType = $this->general_model->getType($card_no);
                	$friendlyname = $cardType . ' - ' . substr($card_no, -4);
                	$custom_data_fields['payment_type'] = $friendlyname;
							 
					$paymentType 		=	'Authorization';
					$companyName        = urlencode($this->czsecurity->xssCleanPostInput('companyName'));
					$firstName 			= urlencode($this->czsecurity->xssCleanPostInput('firstName'));
					$lastName 			= urlencode($this->czsecurity->xssCleanPostInput('lastName'));
					$creditCardType 	= urlencode('Visa');
					$creditCardNumber 	= urlencode($card_no);
					$expDateMonth 		= $expmonth;
					// Month must be padded with leading zero
					$padDateMonth 		= urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

					$expDateYear= urlencode($exyear);

					if(!empty($cvv)){
						$cvv2Number = '&CVV2='.urlencode($cvv);
					}
					
					$address1	= urlencode($this->czsecurity->xssCleanPostInput('address'));
					$address2 	= '';
					$city 		= urlencode($this->czsecurity->xssCleanPostInput('city'));
					$state 		= urlencode($this->czsecurity->xssCleanPostInput('state'));
					$zip 		= urlencode($this->czsecurity->xssCleanPostInput('zipcode'));
				 $country 	= 'US';	
					if(strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATES" || strtoupper($this->czsecurity->xssCleanPostInput('country'))=="UNITED STATE"){
					  $country 	= 'US';	// US or other valid country code
					}
					
					if($this->czsecurity->xssCleanPostInput('country')=="Canada" ||$this->czsecurity->xssCleanPostInput('country')=="canada"){
					  $country 	= 'CAD';	// US or other valid country code
					}
					
					$amount 	= $this->czsecurity->xssCleanPostInput('totalamount');	//actual amount should be substituted here
					$currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')
					  
					$invnum = '';
                    $custom = '';
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_number'))) {
                        $new_invoice_number = $this->czsecurity->xssCleanPostInput('invoice_number');
                        
                        $invnum = $new_invoice_number;
                    }

                    if (!empty($po_number)) {
                        $custom = 'PO Number: '.$po_number;
                    }			   
					// Add request-specific fields to the request string.
					$nvpStr =	"$cvv2Number&PAYMENTACTION=$paymentType&AMT=$amount&invnum=$invnum&custom=$custom&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
								"&EXPDATE=$padDateMonth$expDateYear&FIRSTNAME=$firstName&LASTNAME=$lastName&COMPANYNAME=$companyName".
								"&STREET=$address1&CITY=$city&STATE=$state&ZIP=$zip&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

					$paypal = new PayPalAPINEW($option);
					
					
					$httpParsedResponseAr = $paypal ->PPHttpPost('DoDirectPayment', $nvpStr, $option);
				
               
					if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
						$code='111';

						if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

							$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
							$cvv          =  $this->czsecurity->xssCleanPostInput('cvv');
							$cardType         = $this->general_model->getType($card_no);
	
							$card_data = array(
								'cardMonth'   => $expmonth,
								'cardYear'	 => $exyear,
								'CardType'    => $cardType,
								'CustomerCard' => $card_no,
								'CardCVV'      => $cvv,
								'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('baddress1'),
								'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('baddress2'),
								'Billing_City' => $this->czsecurity->xssCleanPostInput('bcity'),
								'Billing_Country' => $this->czsecurity->xssCleanPostInput('bcountry'),
								'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
								'Billing_State' => $this->czsecurity->xssCleanPostInput('bstate'),
								'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('bzipcode'),
								'customerListID' => $this->czsecurity->xssCleanPostInput('customerID'),
								'companyID'     => $companyID,
								'merchantID'   => $merchantID,
	
								'createdAt' 	=> date("Y-m-d H:i:s")
							);
	
							$id1 = $this->card_model->process_card($card_data);
						}
						 
					 $this->session->set_flashdata('success','Transaction Successful'); 	
				} else{
					 $code='401';
					 $responsetext= urldecode($httpParsedResponseAr['L_LONGMESSAGE0']);
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$responsetext.'"</strong>.</div>'); 
					}
					  $transactiondata= array();
					  	 $tranID ='' ;$amt='0.00';
					     if(isset($httpParsedResponseAr['TRANSACTIONID'])) { $tranID = $httpParsedResponseAr['TRANSACTIONID'];   $amt = urldecode($httpParsedResponseAr["AMT"]);  }
				       $transactiondata['transactionID']       =  $tranID;
					   $transactiondata['transactionStatus']    = $httpParsedResponseAr["ACK"];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s',strtotime(urldecode($httpParsedResponseAr["TIMESTAMP"]))); 
					   $transactiondata['referenceMemo']   = $this->czsecurity->xssCleanPostInput('reference');
					   $transactiondata['transactionCode']     = $code; 
					   $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($httpParsedResponseAr["TIMESTAMP"])); 
						$transactiondata['transactionType']    = "Paypal_auth";	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   =  $amt;
					    $transactiondata['transaction_user_status']= '5';
					  $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Paypal";
					  $transactiondata['resellerID']   =  $this->resellerID;
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
		                    $transactiondata['custom_data_fields'] = json_encode($custom_data_fields);
		                }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					
			   }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong>.</div>'); 
				}		
				
				if(!$checkPlan){
					$httpParsedResponseAr['TRANSACTIONID'] = '';
				}
            
		   }
		   $invoice_IDs = array();
			
			$receipt_data = array(
				'transaction_id' => $httpParsedResponseAr['TRANSACTIONID'],
				'IP_address' => getClientIpAddr(),
				'billing_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'billing_address1' => $this->czsecurity->xssCleanPostInput('baddress1'),
				'billing_address2' => $this->czsecurity->xssCleanPostInput('baddress2'),
				'billing_city' => $this->czsecurity->xssCleanPostInput('bcity'),
				'billing_zip' => $this->czsecurity->xssCleanPostInput('bzipcode'),
				'billing_state' => $this->czsecurity->xssCleanPostInput('bstate'),
				'billing_country' => $this->czsecurity->xssCleanPostInput('bcountry'),
				'shipping_name' => $this->czsecurity->xssCleanPostInput('firstName'). ' '.$this->czsecurity->xssCleanPostInput('lastName'),
				'shipping_address1' => $this->czsecurity->xssCleanPostInput('address1'),
				'shipping_address2' => $this->czsecurity->xssCleanPostInput('address2'),
				'shipping_city' => $this->czsecurity->xssCleanPostInput('city'),
				'shipping_zip' => $this->czsecurity->xssCleanPostInput('zipcode'),
				'shipping_state' => $this->czsecurity->xssCleanPostInput('state'),
				'shiping_counry' => $this->czsecurity->xssCleanPostInput('country'),
				'Phone' => $this->czsecurity->xssCleanPostInput('phone'),
				'Contact' => $this->czsecurity->xssCleanPostInput('email'),
				'proccess_url' => 'Payments/create_customer_auth',
				'proccess_btn_text' => 'Process New Transaction',
				'sub_header' => 'Authorize',
				'checkPlan'	=> $checkPlan
			);
			
			$this->session->set_userdata("receipt_data",$receipt_data);
			$this->session->set_userdata("invoice_IDs",$invoice_IDs);
			
			
			redirect('home/transation_sale_receipt',  'refresh');
		   
	   }
	
	
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
			     
				   
				 	 if($this->session->userdata('logged_in')){
					$merchantID = $this->session->userdata('logged_in')['merchID'];
					}
					if($this->session->userdata('user_logged_in')){
					$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
					}
			     $tID     = $this->czsecurity->xssCleanPostInput('txnpaypalID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				if($tID!='' && !empty($gt_result))
	         	{  
			       
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					$total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					$this->load->library('paypal/Paypal_pro', $config);	
						
    		
				 $customerID = $paydata['customerListID'];
				 
				
				 $amount     =  $paydata['transactionAmount']; 
				
				 if($amount==$total)
				 $refundtype='Full';
				 else if( $total < $amount )
				 $refundtype='Partial';
				  
				   $amount  = $total;
			  
                   	$RTFields = array(
					'transactionid' => $tID, 							// Required.  PayPal transaction ID for the order you're refunding.
					'payerid' => '', 								// Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
					'invoiceid' => '', 								// Your own invoice tracking number.
					'refundtype' => $refundtype, 							// Required.  Type of refund.  Must be Full, Partial, or Other.
					'amt' =>  $amount, 									// Refund Amt.  Required if refund type is Partial.  
					'currencycode' => '', 							// Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
					'note' => '',  									// Custom memo about the refund.  255 char max.
					'retryuntil' => '', 							// Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
					'refundsource' => '', 							// Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
					'merchantstoredetail' => '', 					// Information about the merchant store.
					'refundadvice' => '', 							// Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
					'refunditemdetails' => '', 						// Details about the individual items to be returned.
					'msgsubid' => '', 								// A message ID used for idempotence to uniquely identify a message.
					'storeid' => '', 								// ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
					'terminalid' => ''								// ID of the terminal.  50 char max.
				);	
				
				
				if(!empty($paydata['invoiceTxnID']))
				{
			  	$cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$paydata['merchantID']) );
				$user_id  = $paydata['merchantID'];
				 $user    =  $cusdata['qbwc_username'];
		        $comp_id  =  $cusdata['id']; 
		        
		        $ittem = $this->general_model->get_row_data('qb_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
				$ins_data['customerID']     = $paydata['customerListID'];
			 if(empty($ittem))
		        {
		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                
		            
		        }
		        		        
		        
			    
			    
			      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
        			   if(!empty($in_data))
        			   {
            			$inv_pre   = $in_data['prefix'];
            			$inv_po    = $in_data['postfix']+1;
            			$new_inv_no = $inv_pre.$inv_po;
                        
                       
                       }
		    	$ins_data['merchantDataID'] = $paydata['merchantID'];	
			  $ins_data['creditDescription']     ="Credit as Refund" ;
               $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
				$ins_data['creditDate']   = date('Y-m-d H:i:s');
        	  $ins_data['creditAmount']   = $total;
              $ins_data['creditNumber']   = $new_inv_no;
               $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                $ins_data['Type']         = "Payment";
        	   $ins_id  = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
					   
					   $item['itemListID']      =    $ittem['ListID']; 
				       $item['itemDescription'] =    $ittem['Name']; 
				       $item['itemPrice']       =$total; 
				       $item['itemQuantity']    = 0; 
				      	$item['crlineID']       = $ins_id;
						$acc_name  = $ittem['DepositToAccountName']; 
				      	$acc_ID    = $ittem['DepositToAccountRef']; 
				      	$method_ID = $ittem['PaymentMethodRef']; 
				      	$method_name  = $ittem['PaymentMethodName']; 
						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
				  	           );	
					
				
					
				 if($ins_id && $ins)
				 {
					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
					 
                  }else{
                      	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>'); 
                
                  }
					
             }
					
		$PayPalRequestData = array('RTFields' => $RTFields);
		
		$PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);
		
				
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
				     
				$code = '111';
			  
	
				     $this->customer_model->update_refund_payment($tID, 'PAYPAL');	
				     
				     	if(!empty($paydata['invoiceTxnID']))
            			{	
            			    
            			    
            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);
            			 $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
            			}
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					  $code = '401';
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$responsetext.'"</strong>.</div>'); 
				 }
				       $transactiondata= array();
				       	 $tranID ='' ;$amt='0.00';
					      if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult['ACK'];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP'])); 
					    $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
					   $transactiondata['transactionType']    = 'Paypal_refund' ;
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amt ;
					   $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Paypal";
					  $transactiondata['resellerID']   =  $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				      
		}else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>.</div>'); 
				     
					 }		
					 $invoice_IDs = array();
				
				
					$receipt_data = array(
						'proccess_url' => 'Payments/payment_refund',
						'proccess_btn_text' => 'Process New Refund',
						'sub_header' => 'Refund',
					);
					
					$this->session->set_userdata("receipt_data",$receipt_data);
					$this->session->set_userdata("invoice_IDs",$invoice_IDs);
					if($paydata['invoiceTxnID'] == ''){
						$paydata['invoiceTxnID'] ='null';
						}
						if($paydata['customerListID'] == ''){
							$paydata['customerListID'] ='null';
						}
						if($PayPalResult['REFUNDTRANSACTIONID']== ''){
							$PayPalResult['REFUNDTRANSACTIONID'] ='null';
						}
					
					redirect('home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['REFUNDTRANSACTIONID'],  'refresh');	
			
				
          }
              
	}
	
	
	/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		if(!empty($this->input->post(null, true))){
		        if($this->session->userdata('logged_in'))
                {
                
                
                $merchantID 				= $this->session->userdata('logged_in')['merchID'];
                }
                if($this->session->userdata('user_logged_in'))
                {
                
                $merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
                }
		    
		          
		       
				 $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			       
			      
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
				if($tID!='' && !empty($gt_result))
		{  
			  
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	

			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				$DCFields = array(
						'authorizationid' => $tID, 				// Required. The authorization identification number of the payment you want to capture. This is the transaction ID returned from DoExpressCheckoutPayment or DoDirectPayment.
						'amt' =>  $amount , 							// Required. Must have two decimal places.  Decimal separator must be a period (.) and optional thousands separator must be a comma (,)
						'completetype' => 'Complete', 					// Required.  The value Complete indiciates that this is the last capture you intend to make.  The value NotComplete indicates that you intend to make additional captures.
						'currencycode' => '', 					// Three-character currency code
						'invnum' => '', 						// Your invoice number
						'note' => '', 							// Informational note about this setlement that is displayed to the buyer in an email and in his transaction history.  255 character max.
						'softdescriptor' => '', 				// Per transaction description of the payment that is passed to the customer's credit card statement.
						'storeid' => '', 						// ID of the merchant store.  This field is required for point-of-sale transactions.  Max: 50 char
						'terminalid' => ''						// ID of the terminal.  50 char max.  
					);
					
		$PayPalRequestData = array('DCFields' => $DCFields);
		$PayPalResult = $this->paypal_pro->DoCapture($PayPalRequestData);
				 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					$condition = array('transactionID'=>$tID);
					$customerID = $paydata['customerListID'];
					
					$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
					$tr_date   =date('Y-m-d H:i:s');
					$ref_number =  $tID;
					$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];
					if($chh_mail =='1')
                            {
                                 
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
			        $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 $code = '401';
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$responsetext.'"</strong>.</div>'); 
				     
					 }
					 
					   $transactiondata= array();
					    $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt = $PayPalResult['AMT'];  }
					   
				       $transactiondata['transactionID']      =$tranID ;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult["TIMESTAMP"]));  
					    $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
					   $transactiondata['transactionType']    = 'Paypal_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  =$amt;
					   $transactiondata['merchantID']   = $paydata['merchantID'];
					   $transactiondata['gateway']   = "Paypal";
					  $transactiondata['resellerID']   =  $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
	     	}else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Gateway not available.</div>'); 
				     
					 }		
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				     
					 }	
					 
			  			 
					 $invoice_IDs = array();
					
				 
					 $receipt_data = array(
						 'proccess_url' => 'Payments/payment_capture',
						 'proccess_btn_text' => 'Process New Transaction',
						 'sub_header' => 'Capture',
					 );
					 
					 $this->session->set_userdata("receipt_data",$receipt_data);
					 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
					 if($paydata['invoiceTxnID'] == ''){
						 $paydata['invoiceTxnID'] ='null';
						 }
						 if($paydata['customerListID'] == ''){
							 $paydata['customerListID'] ='null';
						 }
						 if($PayPalResult['TRANSACTIONID']== ''){
							 $PayPalResult['TRANSACTIONID'] ='null';
						 }
					 
					 redirect('home/transation_credit_receipt/transaction/'.$paydata['customerListID'].'/'.$PayPalResult['TRANSACTIONID'],  'refresh');
				
        }
              
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		$custom_data_fields = [];
		if(!empty($this->input->post(null, true)))
        {       if($this->session->userdata('logged_in'))
                {
                
                
                $merchantID 				= $this->session->userdata('logged_in')['merchID'];
                }
                if($this->session->userdata('user_logged_in'))
                {
                
                $merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
                }
				
			
    			  $tID     = $this->czsecurity->xssCleanPostInput('paypaltxnvoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				  $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
	        	 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			  	 if( $tID!='' && !empty($gt_result)){ 
			   
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
					
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				$DVFields = array(
						'authorizationid' => $tID, 				// Required.  The value of the original authorization ID returned by PayPal.  NOTE:  If voiding a transaction that has been reauthorized, use the ID from the original authorization, not the reauth.
						'note' => 'This test void',  							// An information note about this void that is displayed to the payer in an email and in his transaction history.  255 char max.
						'msgsubid' => ''						// A message ID used for idempotence to uniquely identify a message.
					);	
								
					$PayPalRequestData = array('DVFields' => $DVFields);
					$PayPalResult = $this->paypal_pro->DoVoid($PayPalRequestData);
		
		
			 
				if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					 $code = '111';
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
					
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
			    	 $code = '401';
					  $responsetext= $PayPalResult['L_LONGMESSAGE0'];
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$responsetext.'"</strong>.</div>');  
				
				 }
				       $transactiondata= array();
				         $tranID ='' ;$amt='0.00';
					     if(isset($PayPalResult['AUTHORIZATIONID'])) { $tranID = $PayPalResult['AUTHORIZATIONID'];    }
				       
				       $transactiondata['transactionID']      = $tranID;
					   $transactiondata['transactionStatus']  = $PayPalResult["ACK"];
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s', strtotime($PayPalResult['TIMESTAMP']));  
					    $transactiondata['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
					   $transactiondata['transactionType']    = 'Paypal_void';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $paydata['transactionAmount'];
					   $transactiondata['merchantID']         = $paydata['merchantID'];
					   $transactiondata['gateway']            = "Paypal";
					  $transactiondata['resellerID']          =  $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
			             }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			
			  	 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Gateway not available.</div>'); 
				     
					 }	
				
					redirect('Payments/payment_capture','refresh');
		}     
			
	}
	
	
	
	
	
	
	 	 
public function pay_invoice()
{
		$this->session->unset_userdata("receipt_data");
		$this->session->unset_userdata("invoice_IDs");
		$this->session->unset_userdata("in_data");
      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
	
		$checkPlan = check_free_plan_transactions();
	     
	    $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 
		$cardID = $this->czsecurity->xssCleanPostInput('CardID');
		if (!$cardID || empty($cardID)) {
		$cardID = $this->czsecurity->xssCleanPostInput('schCardID');
		}

		$gatlistval = $this->czsecurity->xssCleanPostInput('sch_gateway');
		if (!$gatlistval || empty($gatlistval)) {
		$gatlistval = $this->czsecurity->xssCleanPostInput('gateway');
		}
		$gateway = $gatlistval;	
	      $cusproID=''; $error='';
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
         $custom_data_fields = [];
        if(!empty($invoiceID) && !empty($cardID) && !empty($gateway) )
        {
                      $cardudt='';
      
             $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    		$Customer_ListID = $in_data['Customer_ListID'];
        
           if($cardID=='new1')
           {
               
                        	
			        	$cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
					
           }
           else
           {
               
             
                        $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
           }
           	$cardType = $this->general_model->getType($card_no);
            $friendlyname = $cardType . ' - ' . substr($card_no, -4);
            $custom_data_fields['payment_type'] = $friendlyname;
        
                 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($checkPlan && $cardID!="" && $gateway!="")
				   {  
					 
				   
					if(!empty($in_data))
					{ 
					  
						$Customer_ListID = $in_data['Customer_ListID'];
						
						$customerID =  $in_data['Customer_ListID'];
                      	$c_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
                     	$companyID = $c_data['companyID'];
						  
					 if(!empty($cardID))
					 {
							
        				if( $in_data['BalanceRemaining'] > 0)
        				{
				           $cr_amount=0;
					        $amount 	    =	 $in_data['BalanceRemaining'];
							$amount           =$this->czsecurity->xssCleanPostInput('inv_amount');;	
							$amount           = $amount-$cr_amount;
							$expDateMonth 		= $expmonth;
								$creditCardNumber 	= $card_no;
									$expDateYear=  	$exyear;
									$cvv2Number  = $cvv;
						 	$creditCardType 	= 'Visa';
					  		
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								
							$currencyID     = "USD";
						  

                           $firstName = $in_data['FirstName'];
                            $lastName =  $in_data['LastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['ShipAddress_Addr1']; 
                            $address2 = $in_data['ShipAddress_Addr2']; 
							$country  = $in_data['ShipAddress_Country']; 
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];		
								$zip  = $in_data['ShipAddress_PostalCode']; 
								$phone = $in_data['Phone']; 
								$email = $in_data['Contact']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);

						if(!empty($cvv2Number)){
							$CCDetails['cvv2'] = $cvv2Number;
						}
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
					 if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])))
			     	{
					
					           $code = '111';
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $bamount    = $in_data['BalanceRemaining']-$amount;
						 if($bamount > 0)
						  $ispaid 	 = 'false';
						  
						  	 $app_amount    = $in_data['AppliedAmount']-$amount;
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
						 
						 $condition  = array('TxnID'=>$in_data['TxnID']);	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['qbwc_username'];
						 $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['RefNumber']; 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
			            	 
							
							if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

								$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
								$cardType        = $this->general_model->getType($card_no);


								$expmonth     =    $this->czsecurity->xssCleanPostInput('expiry');
								$exyear       =    $this->czsecurity->xssCleanPostInput('expiry_year');
								$cvv          =    $this->czsecurity->xssCleanPostInput('cvv');

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'    => $cardType,
									'CustomerCard' => $card_no,
									'CardCVV'      => $cvv,
									'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
									'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
									'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
									'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
									'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
									'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
									'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
									'customerListID' => $customerID,

									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'createdAt' 	=> date("Y-m-d H:i:s")
								);



								$id1 = $this->card_model->process_card($card_data);
							}
				    
                        
            					
						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
					   } 
					   else
					   {
                         
					    $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>'); 
				
					   }  
					   
					
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					    $transaction['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])); 
					   $transaction['transactionCode']     = $code;  
					    $transaction['gateway']             = "Paypal";
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']  = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['Customer_ListID'];
					   $transaction['transactionAmount']   =$amt;
					    $transaction['invoiceTxnID']       =$invoiceID;
					   $transaction['merchantID']          = $user_id;
					 
					  $transaction['resellerID']           =  $this->resellerID;
						$CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
						if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
						if($custom_data_fields){
			                  $transaction['custom_data_fields']  = json_encode($custom_data_fields);
			             }
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
				       
				        redirect('home/invoices','refresh');
				        
				        if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]) && !is_numeric($invoiceID)) 
                     { 
					   $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1','', $user); 
                     }

					if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]) && $chh_mail =='1')
					{
						$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $tranID);
					} 
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }
		 }else{
            
       

			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway & Card   are required</strong>.</div>'); 
		  }	
		  
		   if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
			 $trans_id = $PayPalResult['transactionid'];
			 $invoice_IDs = array();
				 $receipt_data = array(
					 'proccess_url' => 'home/invoices',
					 'proccess_btn_text' => 'Process New Invoice',
					 'sub_header' => 'Sale',
					'checkPlan'	=> $checkPlan
				 );
				 
				 $this->session->set_userdata("receipt_data",$receipt_data);
				 $this->session->set_userdata("invoice_IDs",$invoice_IDs);
				 $this->session->set_userdata("in_data",$in_data);
			 if ($cusproID == "1") {
				 redirect('home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
			 }
			 redirect('home/transation_receipt/' . $in_data['TxnID'].'/'.$invoiceID.'/'.$trans_id, 'refresh');
		


    }     






  public function get_card_edit_data(){
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
	 
	 public function check_vault(){
		 
 	
		  $card=''; $card_name=''; $customerdata=array();
		  
		        if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$merchantID 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $merchantID 				= $da['merchantID'];
		}
		  
		 if($this->czsecurity->xssCleanPostInput('customerID')!=""){
			 
				
			 	$customerID 	= $this->czsecurity->xssCleanPostInput('customerID'); 
			
			 	$condition     =  array('ListID'=>$customerID); 
			    $customerdata = $this->general_model->get_row_data('qb_test_customer',$condition);
				if(!empty($customerdata)){
	                 				
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_card_expiry_data($customerID);
					$customerdata['card']  = $card_data;
				
					echo json_encode($customerdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

	  
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
				
			      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}
		        $transactions           = $this->customer_model->get_invoice_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-center"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No record available</td></tr>';
				}
              die;				

      }	

	   
	   
	   
	 	 
public function pay_multi_invoice()
{
     
      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
	     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv'); 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
	     $customerID = $this->czsecurity->xssCleanPostInput('customerID');
	     	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
		$companyID  = $comp_data['companyID'];
		$checkPlan = check_free_plan_transactions();
	
		 $cusproID=''; $error='';
		 $custom_data_fields = [];
         $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
          
          if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
             	
            $cardudt='';
          
          
            
		$resellerID = $this->resellerID;
         
        if(!empty($invoices ) && $checkPlan)
        {
	 	foreach($invoices as $invoiceID)
	 	{
	 	      $cardID_upd='';
	 	       $pay_amounts=$this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
               $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
	    	$Customer_ListID = $in_data['Customer_ListID'];
        
           
                     if($cardID=='new1')
           {
               
                        	
			        	$cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
					
           }
           else
           {
               
             
                        $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
           }
           $cardType = $this->general_model->getType($card_no);
	       $friendlyname = $cardType . ' - ' . substr($card_no, -4);
	       $custom_data_fields['payment_type'] = $friendlyname;
        
            		
        
                    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($cardID!="" || $gateway!="")
				   {  
					 
				   
					if(!empty($in_data))
					{ 
					  
						$Customer_ListID = $in_data['Customer_ListID'];
						 
					 if(!empty($cardID))
					 {
							
        			     	if( $in_data['BalanceRemaining'] > 0)
        			     	{
        				    $cr_amount=0;
                            
                            
                            $card_data    =   $this->card_model->get_single_card_data($cardID); 
                            
                            $amount 	    =	 $in_data['BalanceRemaining'];
                            $amount           =$pay_amounts;	
                            $amount           = $amount-$cr_amount;
                            $expDateMonth 		= $expmonth;
                            $creditCardNumber 	= $card_no;
                            $expDateYear=  	$exyear;
                            $cvv2Number  = $cvv;
                            $creditCardType 	= 'Visa';
                           
                            
                            // Month must be padded with leading zero
                            $padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                            
                            $currencyID     = "USD";
        					     
        
                                   $firstName = $in_data['FirstName'];
                                    $lastName =  $in_data['LastName']; 
                                    $companyName =  $in_data['companyName']; 
        							$address1 = $in_data['ShipAddress_Addr1']; 
                                    $address2 = $in_data['ShipAddress_Addr2']; 
        							$country  = $in_data['ShipAddress_Country']; 
        							$city     = $in_data['ShipAddress_City'];
        							$state    = $in_data['ShipAddress_State'];		
        								$zip  = $in_data['ShipAddress_PostalCode']; 
        								$phone = $in_data['Phone']; 
        								$email = $in_data['Contact']; 
        										
        		$DPFields = array(
        							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
        																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
        							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
        							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
        						);
        						
        		$CCDetails = array(
        							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
        							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
        							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
        							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
        							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
								);
								
								if(!empty($cvv2Number)){
									$CCDetails['cvv2'] = $cvv2Number;
								}
        						
        		$PayerInfo = array(
        							'email' => $email, 								// Email address of payer.
        							'payerid' => '', 							// Unique PayPal customer ID for payer.
        							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
        							'business' => '' 							// Payer's business name.
        						);  
        						
        		$PayerName = array(
        							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
        							'firstname' => $firstName, 							// Payer's first name.  25 char max.
        							'middlename' => '', 						// Payer's middle name.  25 char max.
        							'lastname' => $lastName, 							// Payer's last name.  25 char max.
        							'suffix' => ''								// Payer's suffix.  12 char max.
        						);
        					
        		$BillingAddress = array(
        								'street' => $address1, 						// Required.  First street address.
        								'street2' => $address2, 						// Second street address.
        								'city' => $city, 							// Required.  Name of City.
        								'state' => $state, 							// Required. Name of State or Province.
        								'countrycode' => $country, 					// Required.  Country code.
        								'zip' => $zip, 							// Required.  Postal code of payer.
        								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
        							);
        	
        							
        		                       $PaymentDetails = array(
        								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
        								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
        								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
        								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
        								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
        								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
        								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
        								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
        								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
        								'custom' => '', 						// Free-form field for your own use.  256 char max.
        								'invnum' => '', 						// Your own invoice or tracking number
        								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
        								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
        								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
        							);					
        
        						$PayPalRequestData = array(
        										'DPFields' => $DPFields, 
        										'CCDetails' => $CCDetails, 
        										'PayerInfo' => $PayerInfo, 
        										'PayerName' => $PayerName, 
        										'BillingAddress' => $BillingAddress, 
        										
        										'PaymentDetails' => $PaymentDetails, 
        										
        									);
        							
        				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
        					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) 
                             {  
        					
        					           $code = '111';
        						 $txnID      = $in_data['TxnID'];  
        						 $ispaid 	 = 'true';
        						 $bamount    = $in_data['BalanceRemaining']-$amount;
        						 if($bamount > 0)
        						  $ispaid 	 = 'false';
        						  $app_ampount = $in_data['AppliedAmount'] +(-$amount);
        						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_ampount , 'BalanceRemaining'=>$bamount );
        						 
        						 $condition  = array('TxnID'=>$in_data['TxnID']);	
        						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
        						 
        						 $user = $in_data['qbwc_username'];
        					
							if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1"  &&  !($this->czsecurity->xssCleanPostInput('tc'))) {

								$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
								$cardType        = $this->general_model->getType($card_no);


								$expmonth     =    $this->czsecurity->xssCleanPostInput('expiry');
								$exyear       =    $this->czsecurity->xssCleanPostInput('expiry_year');
								$cvv          =    $this->czsecurity->xssCleanPostInput('cvv');

								$card_data = array(
									'cardMonth'   => $expmonth,
									'cardYear'	 => $exyear,
									'CardType'    => $cardType,
									'CustomerCard' => $card_no,
									'CardCVV'      => $cvv,
									'Billing_Addr1' => $this->czsecurity->xssCleanPostInput('address1'),
									'Billing_Addr2' => $this->czsecurity->xssCleanPostInput('address2'),
									'Billing_City' => $this->czsecurity->xssCleanPostInput('city'),
									'Billing_Country' => $this->czsecurity->xssCleanPostInput('country'),
									'Billing_Contact' => $this->czsecurity->xssCleanPostInput('phone'),
									'Billing_State' => $this->czsecurity->xssCleanPostInput('state'),
									'Billing_Zipcode' => $this->czsecurity->xssCleanPostInput('zipcode'),
									'customerListID' => $in_data['Customer_ListID'],

									'companyID'     => $companyID,
									'merchantID'   => $user_id,
									'createdAt' 	=> date("Y-m-d H:i:s")
								);



								$id1 = $this->card_model->process_card($card_data);
							}
				    
             
        					
        						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
        					   } 
        					   else
        					   {
                                 if($cardudt=='new1')
                                 {
                                      $this->db1->where(array('CardID'=>$cardID));
                                       $this->db1->delete('customer_card_data');
                                 }
            					    $code = '401';
            				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>'); 
            				
            					   }  
            					   
        					
                               $transaction= array();
                                 $tranID ='' ;$amt='0.00';
        					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                               
        				       $transaction['transactionID']       = $tranID;
        					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
        					   $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
        					    $transaction['transactionModified'] =  date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));
        					   $transaction['transactionCode']     = $code;  
        					    $transaction['invoiceTxnID']       =$invoiceID;
        						$transaction['transactionType']    = "Paypal_sale";	
        						$transaction['gatewayID']          = $gateway;
                               $transaction['transactionGateway']  = $gt_result['gatewayType'] ;					
        					   $transaction['customerListID']      = $in_data['Customer_ListID'];
        					   $transaction['transactionAmount']   =$amt ;
        					   $transaction['merchantID']          = $user_id;
        					   $transaction['gateway']             = "Paypal";
        					  $transaction['resellerID']           =  $this->resellerID;
        						$CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
        						if(!empty($this->transactionByUser)){
								    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
								    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
								}
								if($custom_data_fields){
					                  $transaction['custom_data_fields']  = json_encode($custom_data_fields);
					             }
        				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
        				        if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]) && !is_numeric($invoiceID)) 
                             { 
        					   $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1','', $user); 
                             }
        			     	}else{
        					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
        			    	}
          
    		            }else{
    	                  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
    			        }
    		
    		 
        	         	}else{
        	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
        			   }
    			 
                     }
                 else
                 {
    			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
    		     }
                
                if($chh_mail =='1')
                {
                $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
                $ref_number =  $in_data['RefNumber']; 
                $tr_date   =date('Y-m-d H:i:s');
                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
            
                $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date, $tranID);
                } 
		  
	 	     }
		  
	     	 }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway & Card   are required</strong>.</div>'); 
		  }	
		  
			if(!$checkPlan){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Transaction limit has been reached. <a href= "'.base_url().'home/my_account">Click here</a> to upgrade.</strong>.</div>');
			}
		   	 if($cusproID!=""){
			 	 redirect('home/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('home/invoices','refresh');
		   	 }


    }     
	
	 
	 
	 
	 
}

