<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tax extends CI_Controller 
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
        $this->load->model('company_model');
		
			if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='2')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
  
	}
	
	

	public function index()
	{   
		redirect('tax_list','refresh');
	}
	
    public function tax_list()
	{   
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
        if($this->session->userdata('logged_in'))
        {
			
		$data['login_info'] 	= $this->session->userdata('logged_in');
		
		$user_id 				=$data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in'))
        {
		   $data['login_info'] 	    = $this->session->userdata('user_logged_in');
		   $user_id 				= $data['login_info']['merchantID'];
		}
	
		$con    =array();
        $taxname=array();
        $con = array('merchantID'=>$user_id);
		$vendors =array();	 
             $ven_cont = $this->general_model->get_num_rows('qb_merchant_vendor', $con);
             if($ven_cont <=0)
             {
				$comp_data   = $this->general_model->get_select_data('tbl_company',array('qbwc_username'),array('merchantID'=>$user_id));
					$user = $comp_data['qbwc_username'];
					$this->quickbooks->enqueue(QUICKBOOKS_QUERY_VENDOR, '1', '1','', $user); 
             }else{
              $vendors =  $this->general_model->get_table_data('qb_merchant_vendor',array('merchantID'=>$user_id));
            $data['vendors'] = $vendors;
            
             }
               
             
               
			  $taxname = $this->company_model->get_tax_data($user_id);
			  $data['taxes'] = $taxname;
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('pages/taxes', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	

	 public function create_taxes()
 
	{
     
       if($this->session->userdata('logged_in')){
			
		
		$m_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		$m_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
     
     
	  if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('friendlyName', 'Friendly Name', 'required|xss_clean');
		$this->form_validation->set_rules('taxRate', 'Tax Rate', 'required|xss_clean');
      	$this->form_validation->set_rules('taxDescription', 'Tax Description', 'required|xss_clean');
	   
      
      
		if ($this->form_validation->run() == true)
		{
			
			$input_data['taxName']	   = $this->czsecurity->xssCleanPostInput('friendlyName');
			$input_data['taxRate']	   = $this->czsecurity->xssCleanPostInput('taxRate');
			$input_data['taxDescription'] = $this->czsecurity->xssCleanPostInput('taxDescription');
			

					if($this->czsecurity->xssCleanPostInput('taxID')!="" ){
				      
				         $id = $this->czsecurity->xssCleanPostInput('taxID');
						 $chk_condition = array('taxID'=>$id);
						 $this->general_model->update_row_data('tbl_taxes',$chk_condition, $input_data);
						 $this->session->set_flashdata('success', 'Successfully Updated');
			    	}
					else
					{
                    $input_data['merchantID']	   = $m_id;
					$input_data['taxStatus']	   = 'true';
                    $input_data['createdAt']	   = date('Y-m-d H:i:s');
					$input_data['updatedAt']	   = date('Y-m-d H:i:s');
                    $input_data['vendorID']	       = $this->czsecurity->xssCleanPostInput('vendor');
					$input_data['qb_status']       =    0;
                              $lsID = mt_rand('10000','50000');    
                     $input_data['taxListID']   =  $lsID;                
						$id = $this->general_model->insert_row('tbl_custom_tax', $input_data);
                    if($id)
                    {         $comp_data   = $this->general_model->get_select_data('tbl_company',array('id','qbwc_username'),array('merchantID'=>$m_id));
                               $user = $comp_data['qbwc_username'];
							   $this->quickbooks->enqueue(QUICKBOOKS_ADD_SALESTAXITEM, $id, '1','', $user); 
							 
							  $inst_data =array('friendlyName'=>$input_data['taxName'],'companyID'=>$comp_data['id'],'taxListID'=>$lsID,'taxRate'=>$input_data['taxRate'],
							  'taxDescription'=>$input_data['taxDescription'],'vendorID'=>$input_data['vendorID'],'taxStatus'=>'Active',
							  'CreatedTime'=>date('Y-m-d H:i:s'),'TimeModified'=>date('Y-m-d H:i:s'));	 
							  
							  	$tid = $this->general_model->insert_row('tbl_taxes', $inst_data);
								 
                    }
                         $this->session->set_flashdata('success', 'Your Information Stored Successfully.');						 
						 
					}
					   redirect(base_url('Tax/tax_list'));
		
		}
		
		
	}
				
							 
					$data['primary_nav'] 	= primary_nav();
					$data['template'] 		= template_variable();
					$data['login_info']     = $this->session->userdata('logged_in');
					
					
					 $taxname = $this->general_model->get_table_data('tbl_taxes','');
					 $data['taxes'] = $taxname;
					 
					
		 					 
					$this->load->view('template/template_start', $data);
					$this->load->view('template/page_head', $data);
					$this->load->view('pages/taxes', $data);
					$this->load->view('template/page_footer',$data);
					$this->load->view('template/template_end', $data);
 }
 
 
	public function get_tax_id()
    {
		
        $id = $this->czsecurity->xssCleanPostInput('taxID');
		$val = array(
		'taxListID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_taxes',$val);
        echo json_encode($data);
    }
	
	
	
public function delete_tax(){
	
	
		  $id = $this->czsecurity->xssCleanPostInput('tax_id');
		 $condition =  array('taxListID'=>$id); 
		 $del      = $this->general_model->update_row_data('tbl_taxes', $condition,array(taxStatus=>'false'));
      if($del){
	            $this->session->set_flashdata('success', 'Successfully Deleted');  
	  }else{
                $this->session->set_flashdata('message', '<strong>Error:</strong> Somthing Is Wrong.');        
	  }	  
	  
	  	redirect(base_url('Tax/tax_list'));
    	 
	
	}
	
	
	
}