<?php

/**
 *Subscription Process
 * 
 * Subscriptions=>List of customer subscripiton
 * create_subscription
 * 
 * status is the process for Offline Invoice payment
 * 
 *Create_invoice
 * edit_custome_invoice
 */

include_once APPPATH .'libraries/Manage_payments.php';
include_once APPPATH .'libraries/QBD_Sync.php';

class SettingSubscription extends CI_Controller
{
    private $transactionByUser;
	private $resellerID;
	private $merchantID;
	public function __construct()
	{
		parent::__construct();

		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('common_model');
		$this->load->model('card_model');

		$this->db1 = $this->load->database('otherdb', TRUE);
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
			$logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $merchID = $logged_in_data['merchID'];
            $this->transactionByUser = ['id' => $merchID, 'type' => 1];
		} else if ($this->session->userdata('user_logged_in') != "") {
			$logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		} else {
			redirect('login', 'refresh');
		}
        $this->merchantID = $merchID;
	}


	public function index()
	{
		redirect('home', 'refresh');
		
	}


	public function status()
	{
		if ($this->session->userdata('logged_in')) {
			$data 	= $this->session->userdata('logged_in');

			$user_id 				= $data['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['merchantID'];
		}
		$amount =  $this->czsecurity->xssCleanPostInput('inv_amount');

		if ($this->czsecurity->xssCleanPostInput('pay_type') == '0') {

			if (!empty($this->czsecurity->xssCleanPostInput('payment_date'))  && !empty($this->czsecurity->xssCleanPostInput('check_number'))) {
				$invoiceID = $this->czsecurity->xssCleanPostInput('subscID');
				$in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
				$amount =  $this->czsecurity->xssCleanPostInput('inv_amount');

				$Customer_ListID = $in_data['Customer_ListID'];
				$balance = $in_data['BalanceRemaining'] - $amount;
				if ($balance > 0)
					$ispaid 	 = 'false';
				else
					$ispaid 	 = 'true';
				$transaction = array();

				$appliedAmount = $in_data['AppliedAmount'] - $amount;

				$txnID      = $in_data['TxnID'];
				$status = $this->czsecurity->xssCleanPostInput('status');
				$date = $this->czsecurity->xssCleanPostInput('payment_date');
				$Check_Date = date('Y-m-d', strtotime($date));
				$check  = $this->czsecurity->xssCleanPostInput('check_number');
				$transaction['transactionID']       = $check;
				$transaction['transactionStatus']    = 'Offline Payment';
				$transaction['transactionDate']     = date('Y-m-d H:i:s');
				$transaction['transactionCode']     = '100';
				$transaction['transactionType']    = "Offline Payment";
				$transaction['customerListID']       = $in_data['Customer_ListID'];
				$transaction['transactionAmount']   = $amount;
				$transaction['invoiceTxnID']        = $in_data['TxnID'];
				$transaction['merchantID']       = $user_id;
				$transaction['resellerID']       = $data['resellerID'];
				if(!empty($this->transactionByUser)){
					$transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
					$transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
				}

				$user = $in_data['qbwc_username'];



				$data   	 = array('IsPaid' => $ispaid,'AppliedAmount' => $appliedAmount, 'userStatus' => $status, 'checkNumber' => $check, 'paymentDate' => $Check_Date, 'BalanceRemaining' => $balance);

				$condition  = array('TxnID' => $in_data['TxnID']);
				$this->general_model->update_row_data('qb_test_invoice', $condition, $data);
				$CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
				$id = $this->general_model->insert_row('customer_transaction',   $transaction);
				$invoice_pay = array('invoiceID' => $in_data['TxnID'], 'qb_action' => 'Add Payment', 'qb_status' => '0', 'paymentID' => $id, 'merchantID' => $user_id, 'TimeModified' => date('Y-m-d H:i:s'));
				if(!is_numeric($in_data['TxnID']))
					$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Fill the all required fields.</div>');
			}
		} else if ($this->czsecurity->xssCleanPostInput('pay_type') == '1') {

			$invoiceID = $this->czsecurity->xssCleanPostInput('subscID');
			$in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
			$remaining = $amount;
			
			$Customer_ListID = $in_data['Customer_ListID'];
			$cr_amnt = 0;
			$balace = $in_data['BalanceRemaining'] - $amount;

			if($remaining > $in_data['BalanceRemaining']){
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Amount can not be greater than invoice balance.</div>');
				redirect('home/invoices', 'refresh');
			}

			if (!empty($this->czsecurity->xssCleanPostInput('pay_check_credit'))) {
				$amountArray = [];
				foreach ($this->czsecurity->xssCleanPostInput('pay_check_credit') as $cr_data) {
					if($remaining == 0){
						break;
					}

					$amountToCharge = 0;
					
					$cr_current_balance = $cr_new_balance = $this->general_model->get_select_data('qb_customer_credit', array('CreditRemaining'), array('TxnID' => $cr_data))['CreditRemaining'];
					if($remaining >= $cr_current_balance){
						$cr_amnt = $cr_amnt + $cr_current_balance;
						$remaining = $remaining - $cr_current_balance;
						$cr_new_balance = 0;
						$amountToCharge = $cr_current_balance;
					} else {
						$cr_amnt = $cr_amnt + $remaining;
						$cr_new_balance = ($cr_current_balance - $remaining);
						$amountToCharge = $remaining;
						$remaining = 0;
					}

					$amountArray[] = $amountToCharge;
					$conditionCR  = array('TxnID' => $cr_data);
					$crUpdateData = array(
						'CreditRemaining' => $cr_new_balance,
						'IsPending' => ($cr_new_balance == 0) ? 'true' : 'false'
					);
					$this->general_model->update_row_data('qb_customer_credit', $conditionCR, $crUpdateData);
				}

				$amount = $cr_amnt; //If selected credit does not have same amount as entered 

				$balace = $in_data['BalanceRemaining'] - $cr_amnt;

				if ($balace > 0) {
					$ispaid = 'false';
				} else {
					$ispaid = 'true';
				}

				$appliedAmount = $in_data['AppliedAmount'] - $amount;


				$crtxnID   = implode(',', $this->czsecurity->xssCleanPostInput('pay_check_credit'));
				$crAmmounts   = implode(',', $amountArray);

				$transaction = array();
				$transaction['transactionID']       = '---';
				$transaction['transactionStatus']    = 'Credit Payment';
				$transaction['transactionDate']     = date('Y-m-d H:i:s');
				$transaction['transactionCode']     = '100';
				$transaction['transactionType']    = 'Credit Payment';
				$transaction['customerListID']       = $in_data['Customer_ListID'];
				$transaction['transactionAmount']   = $amount;
				$transaction['invoiceTxnID']        = $in_data['TxnID'];
				$transaction['creditTxnID']       =  $crtxnID;
				$transaction['cr_amounts']       =  $crAmmounts;

				if(!empty($this->transactionByUser)){
					$transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
					$transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
				}

				$txnID      = $in_data['TxnID'];
				$transaction['merchantID']      = $user_id;
				$transaction['resellerID']       = $data['resellerID'];
				$status = 'Credit';
				$date = $this->czsecurity->xssCleanPostInput('payment_date');
				$Check_Date = date('Y-m-d');

				$user = $in_data['qbwc_username'];


				$data   	 = array('IsPaid' => $ispaid, 'AppliedAmount' => $appliedAmount, 'userStatus' => $status, 'paymentDate' => $Check_Date, 'BalanceRemaining' => $balace);

				$condition  = array('TxnID' => $in_data['TxnID']);
				$this->general_model->update_row_data('qb_test_invoice', $condition, $data);
				$CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
				$id = $this->general_model->insert_row('customer_transaction',   $transaction);
				$invoice_pay = array('invoiceID' => $in_data['TxnID'], 'qb_action' => 'Add Payment', 'qb_status' => '0', 'paymentID' => $id, 'merchantID' => $user_id, 'TimeModified' => date('Y-m-d H:i:s'));
				$this->general_model->insert_row('tbl_qbd_invoice_payment_log',   $invoice_pay);

				if(!is_numeric($in_data['TxnID']))
					$this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1', '', $user);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Fill the all required fields.</div>');
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Invalid Request</div>');
			redirect('home/index', 'refresh');
		}

		redirect('home/invoices', 'refresh');
	}



	public function subscriptions()
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$data['merchantID']     =  $user_id;

		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['subscriptions']   = $this->customer_model->get_subscription_plan_data($user_id);
		$data['subs_data']      = $this->customer_model->get_total_subscription($user_id);
		$this->load->view('template/template_start', $data);

		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_subscriptions', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_subscription()
	{
		//Show a form here which collects someone's name and e-mail address
		if (!empty($this->input->post(null, true))) {

			if ($this->session->userdata('logged_in')) {
				$da 	= $this->session->userdata('logged_in');

				$user_id 				= $da['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$da 	= $this->session->userdata('user_logged_in');

				$user_id 				= $da['merchantID'];
			}

			$subscriptionID = false;

			$in_prefix = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (empty($in_prefix)) {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('SettingSubscription/create_subscription', 'refresh');
			}

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemTax'] = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				if ($this->czsecurity->xssCleanPostInput('onetime_charge')[$key] && $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] != 'Recurring') {

					$insert_row['oneTimeCharge'] = '1';
				} else {
					$insert_row['oneTimeCharge'] = '0';
				}

				if(empty($insert_row['itemTax'])){
					$insert_row['itemTax'] = 0;
				} else {
					$insert_row['itemTax'] = 1;
				}

				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$sname      = $this->czsecurity->xssCleanPostInput('sub_name');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$plan        = $this->czsecurity->xssCleanPostInput('duration_list');
			if ($plan > 0) {
				$subsamount  = $total / $plan;
			} else {
				$subsamount  = $total;
			}
			$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
			$invoice_date = $first_date;
			$st_date      = $first_date;
			$current_date = date('Y-m-d');

			$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');

			if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
				$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			} else {
				$paygateway  = 0;
			}

			$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');

			$phone        = $this->czsecurity->xssCleanPostInput('phone');
			$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
			$tax     = ($this->czsecurity->xssCleanPostInput('taxes') != '') ? $this->czsecurity->xssCleanPostInput('taxes') : 0;
			$planid     = $this->czsecurity->xssCleanPostInput('sub_plan');
			$end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));

			$totalInv = ($plan > 0) ? $plan + $freetrial - 1 : 1;
			$nextGeneratingDate = $st_date;
			$fromdateCus =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($st_date)) . " -1 day"));
			$conditionGt				= array('planID' => $planid);

			$subplansData		= $this->general_model->get_row_data('tbl_subscriptions_plan_qb', $conditionGt);
			if($subplansData['proRate']){
				$proRate = $subplansData['proRate']; 
			}else{
				$proRate = 0;
			}
			if($subplansData['proRateBillingDay']){
				$proRateday = $subplansData['proRateBillingDay'];  
			}
			else{
				$proRateday = 1;			 
			}
			
			if ($paycycle == 'dly') {
				$totalInv   = ($totalInv) ? $totalInv : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv day"));

				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 day"));

			} else if ($paycycle == '1wk') {
				$totalInv   = ($totalInv) ? $totalInv : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 week"));
			} else if ($paycycle == '2wk') {
				$totalInv   = ($totalInv) ? $totalInv + 1 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv week"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 week"));
			} else if ($paycycle == 'mon') {
				
				$totalInv   = ($totalInv) ? $totalInv + 1 : '1';
				if($proRate==1)
				{
					$end_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  

					$nextGeneratingDate =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
				} else
				{

					$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));  
					$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +1 month"));
				}


			} else if ($paycycle == '2mn') {
				$totalInv   = ($totalInv) ? $totalInv + 2 * 1 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +2 month"));


			} else if ($paycycle == 'qtr') {
				$totalInv   = ($totalInv) ? $totalInv + 3 * 1 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +3 month"));

			} else if ($paycycle == 'six') {
				$totalInv   = ($totalInv) ? $totalInv + 6 * 1 : '1';
				$end_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +6 month"));
			} else if ($paycycle == 'yrl') {
				$totalInv   = ($totalInv) ? $totalInv + 12 * 1 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +12 month"));
			} else if ($paycycle == '2yr') {
				$totalInv   = ($totalInv) ? $totalInv + 2 * 12 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));

				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +24 month"));
			} else if ($paycycle == '3yr') {
				$totalInv   = ($totalInv) ? $totalInv + 3 * 12 : '1';
				$end_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +$totalInv month"));
				$nextGeneratingDate =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($fromdateCus)) . " +36 month"));
			}


			if ($this->czsecurity->xssCleanPostInput('autopay')) {

				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$card = array();
					$card_no = $this->czsecurity->xssCleanPostInput('card_number');
					$card_type  = $this->general_model->getType($this->czsecurity->xssCleanPostInput('card_number'));
					$friendlyName = $card_type . ' - ' . substr($this->czsecurity->xssCleanPostInput('card_number'), -4);

					$insert_array =  array(
						'cardMonth'  => $this->czsecurity->xssCleanPostInput('expiry'),
						'cardYear'	 => $this->czsecurity->xssCleanPostInput('expiry_year'),
						'CustomerCard' => $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('card_number')),
						'CardCVV'      => '', 
						'CardType'      => $card_type,
						'Billing_Addr1'	 => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2'	 => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City'	 => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_State'	 => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Country'	 => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact'	 => $this->czsecurity->xssCleanPostInput('bphone'),
						'Billing_Zipcode'	 => $this->czsecurity->xssCleanPostInput('bzipcode'),

						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						
					);
					$con  = array(
						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName
					);

					$card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);


					if ($card_data > 0) {
						$cardID = $this->card_model->update_card_data($con, $insert_array);
						$cardID  = $card_data['CardID'];
					} else {

						$cardID = $this->card_model->insert_card_data($insert_array);
					}
				}else if ($this->czsecurity->xssCleanPostInput('acc_number') != "") {
					$card = array();
					$acc_number   		= $this->czsecurity->xssCleanPostInput('acc_number');
					$route_number 		= $this->czsecurity->xssCleanPostInput('route_number');
					$acc_name     		= $this->czsecurity->xssCleanPostInput('acc_name');
					$secCode      		= 'WEB';
					$acct_type        	= $this->czsecurity->xssCleanPostInput('acct_type');
					$acct_holder_type 	= $this->czsecurity->xssCleanPostInput('acct_holder_type');
					
					$friendlyName = 'Checking - ' . substr($acc_number, -4);

					$insert_array =  array(
						'accountNumber'  => $acc_number,
						'routeNumber'	 => $route_number,
						'accountName' => $acc_name,
						'secCodeEntryMethod'      =>$secCode,
						'accountType'      => $acct_type,
						'accountHolderType'      => $acct_holder_type,
						'CardType' => 'Echeck',
						'Billing_Addr1'	 => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2'	 => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City'	 => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_State'	 => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Country'	 => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact'	 => $this->czsecurity->xssCleanPostInput('bphone'),
						'Billing_Zipcode'	 => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						
					);
					$con  = array(
						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName
					);

					$card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);


					if ($card_data > 0) {
						$cardID = $this->card_model->update_card_data($con, $insert_array);
						$cardID  = $card_data['CardID'];
					} else {

						$cardID = $this->card_model->insert_card_data($insert_array);
					}
				}
			}

			$subdata = array(
				'subscriptionName'    => $sname,
				'customerID'         => $customerID,
				'subscriptionPlan'   => $plan,
				'subscriptionAmount' => $total,
				'generatingDate'    => $invoice_date,
				'firstDate'			=> $first_date,
				'startDate'         => $st_date,
				'endDate'           => $end_date,
				'paymentGateway'    => $paygateway,
				'address1'          => $address1,
				'address2'          => $address2,
				'country'           => $country,
				'state'             => $state,
				'city'              => $city,
				'zipcode'           => $zipcode,
				'contactNumber'     => $phone,
				'totalInvoice'      => $plan,
				'invoicefrequency'  => $paycycle,
				'freeTrial'			=> $freetrial,
				'taxID'			=> $tax,
				'planID'		=> $planid,
				'nextGeneratingDate' => $nextGeneratingDate,
				'baddress1'	=> $this->czsecurity->xssCleanPostInput('baddress1'),
				'baddress2'	=> $this->czsecurity->xssCleanPostInput('baddress2'),
				'bcity'	=> $this->czsecurity->xssCleanPostInput('bcity'),
				'bzipcode'	=> $this->czsecurity->xssCleanPostInput('bzipcode'),
				'bcountry'	=> $this->czsecurity->xssCleanPostInput('bcountry'),
				'bstate'	=> $this->czsecurity->xssCleanPostInput('bstate'),
			);

			if ($this->czsecurity->xssCleanPostInput('autopay')) {
				$subdata['cardID']	= $cardID;
				$subdata['automaticPayment'] = '1';
			} else {
				$subdata['automaticPayment'] = '0';
				$subdata['cardID']	= 0;
			}

			if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

				$subdata['emailRecurring'] = '1';
			} else {
				$subdata['emailRecurring'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('billinfo')) {

				$subdata['usingExistingAddress'] = '1';
			} else {
				$subdata['usingExistingAddress'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('subID') != "") {
				$rowID = $this->czsecurity->xssCleanPostInput('subID');
				$subdata['updatedAt']  = date('Y-m-d H:i:s');

				$subs =  $this->general_model->get_row_data('tbl_subscriptions', array('subscriptionID' => $rowID));
				$date = $first_date;

				$in_num  = $subs['generatedInvoice'];

				$subdata['generatedInvoice'] = $in_num;
				$subscriptionID = $rowID;
				$subdata['merchantDataID'] = $this->merchantID;

				if(strtotime($current_date) <= strtotime($st_date) && $subs['generatedInvoice'] == 0){
					$subdata['nextGeneratingDate'] = $st_date;
				}

				$ins_data = $this->general_model->update_row_data('tbl_subscriptions', array('subscriptionID' => $rowID), $subdata);

				$this->general_model->delete_row_data('tbl_subscription_invoice_item', array('subscriptionID' => $rowID));

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $rowID;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
			} else {
				$subdata['merchantDataID'] = $this->merchantID;
				$subdata['createdAt']  = date('Y-m-d H:i:s');
				$subdata['generatedInvoice'] = 0;
				if(strtotime($current_date) <= strtotime($st_date)){
					$subdata['nextGeneratingDate'] = $st_date;
				}

				$ins_data = $this->general_model->insert_row('tbl_subscriptions', $subdata);
				$subscriptionID = $ins_data;

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $ins_data;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
			}

			if ($ins_data && $ins) {
				$first_date = strtotime($first_date);
				$current_time = time();
				if($subdata['generatedInvoice'] == 0){
					$subdata['subscriptionID'] = $subscriptionID;
					$subdata['subplansData'] = $subplansData;
					$subdata['subscriptionItems'] = $item_val;
					$this->_createSubscriptionFirstInvoice($subdata);
				}

				$this->session->set_flashdata('success', 'Successfully Created');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}
			redirect('SettingSubscription/subscriptions', 'refresh');
		}


		if ($this->uri->segment('3') != "") {
			$sbID = $this->uri->segment('3');
			$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions', array('subscriptionID' => $sbID));
			$data['c_cards']    = $this->get_card_expiry_data($data['subs']['customerID']);
			$data['items']      = $this->general_model->get_table_data('tbl_subscription_invoice_item', array('subscriptionID' => $sbID));
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$condition11				= array('merchantDataID' => $user_id);

		$merchant_condition = [
			'merchID' => $user_id,
		];
		$data['defaultGateway'] = false;
		if(!merchant_gateway_allowed($merchant_condition)){
			$defaultGateway = $this->general_model->merchant_default_gateway($merchant_condition);
			$data['defaultGateway'] = $defaultGateway[0];
		}

		$data['subplans']		= $this->general_model->get_table_data('tbl_subscriptions_plan_qb', $condition11);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
		$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan', '');
		$data['plans']          = $this->company_model->get_plan_data_invoice($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$taxname = $this->company_model->get_tax_data($user_id);
		$data['taxes'] = $taxname;
	
		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_subscription', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_tax_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('tax_id');
		$val = array(
			'taxID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_taxes', $val);
		echo json_encode($data);
	}

	public function update_customer_invoice_cancel()
	{

		if ($this->session->userdata('logged_in')) {
			$data 	= $this->session->userdata('logged_in');

			$user_id 				= $data['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['merchantID'];
		}
		$today 				    = date('Y-m-d');
		$txnID           =  $this->czsecurity->xssCleanPostInput('invoiceID');
		$condition		 = array('TxnID' => $txnID);
		$invData         = $this->general_model->get_row_data('qb_test_invoice', $condition);
		$input_array = array();
		$input_array['qb_status']  = 0;
		$input_array['qb_action']  = "Delete Invoice";
		$input_array['createdAt']  = date('Y-m-d H:i:s');
		$input_array['updatedAt']  = date('Y-m-d H:i:s');;
		$input_array['merchantID'] = $user_id;
		$input_array['invoiceID']  = $txnID;

		$input_array1 = array();
		$input_array1['qb_status']  = 0;
		$input_array1['txnType']  = "Invoice";
		$input_array1['createdAt']  = date('Y-m-d H:i:s');
		$input_array1['updatedAt']  = date('Y-m-d H:i:s');;
		$input_array1['merchantID'] = $user_id;
		$input_array1['delTxnID']  = $txnID;



		if (!empty($invData)) {
			$updateData  = array();
			$updateData['userStatus'] = 'cancel';

			if ($this->general_model->update_row_data('qb_test_invoice', $condition, $updateData)) {
				$qbc = $this->customer_model->customer_qbc_by_invoice($txnID);
				$qb_user = $qbc->qbwc_username;
				if ($qb_user) {
					$insID =  $this->general_model->insert_row('tbl_del_transactions', $input_array1);
					$this->quickbooks->enqueue(QUICKBOOKS_DELETE_TXN, $insID, '1', '', $qb_user);
				}
				$this->session->set_flashdata('success', ' Data added to sync queue. Please refresh for status update');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Error in process.</div>');
			}
		}

		redirect('home/invoices', 'refresh');
	}



	public function delete_tnx()
	{

		if ($this->session->userdata('logged_in')) {
			$data 	= $this->session->userdata('logged_in');

			$user_id 				= $data['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['merchantID'];
		}
		$today 				    = date('Y-m-d');
		$ID              =  $this->czsecurity->xssCleanPostInput('tID');
		$txnID           =  $this->czsecurity->xssCleanPostInput('txnID');
		$txnID1           = $this->db->query("Select invoiceTxnID from customer_transaction where id='" . $tID . "'")->row_array()['invoiceTxnID'];
		$input_array = array();
		$input_array['qb_status']  = 0;
		$input_array['qb_action']  = "Refund";
		$input_array['createdAt']  = date('Y-m-d H:i:s');
		$input_array['updatedAt']  = date('Y-m-d H:i:s');;
		$input_array['merchantID'] = $user_id;
		$input_array['invoiceID']  = $txnID;

		$input_array1 = array();
		$input_array1['qb_status']  = 0;
		$input_array1['txnType']  = "ReceivePayment";
		$input_array1['createdAt']  = date('Y-m-d H:i:s');
		$input_array1['updatedAt']  = date('Y-m-d H:i:s');;
		$input_array1['merchantID'] = $user_id;
		$input_array1['txnID']  = $txnID;



		if (!empty($invData)) {
			$updateData  = array();
			$qbc = $this->customer_model->customer_qbc_by_invoice($txnID1);
			if (!empty($qbc)) {

				$qb_user = $qbc->qbwc_username;
				if ($qb_user) {
					$this->general_model->insert_row('qb_custom_log', $input_array);
					$insID =  $this->general_model->insert_row('tbl_del_transactions', $input_array1);
					$this->quickbooks->enqueue(QUICKBOOKS_DELETE_TXN, $insID, '1', '', $qb_user);
				}
				$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>Error in process.</div>');
			}
		}

		redirect('home/invoices', 'refresh');
	}


	public function sync_invoice()
	{

		if ($this->czsecurity->xssCleanPostInput('inv_id') != '') {
			$tid   =    $this->czsecurity->xssCleanPostInput('inv_id');
			$logID   = $this->czsecurity->xssCleanPostInput('logID');
			$updateRec = 0;

			$condition		 = array('insertInvID' => $tid);
			$invData         = $this->general_model->get_row_data('tbl_custom_invoice', $condition);

			$exist_row = $this->general_model->get_row_data('tbl_custom_invoice', $condition);
			if (!empty($exist_row)) {


				if ($exist_row['invoicelsID'] != '') {
					$ldata =   $this->general_model->get_select_data('qb_test_invoice', array('EditSequence'), array('TxnID' => $exist_row['invoicelsID']));
					$edtID = $ldata['EditSequence'];
					$updateRec    =   $this->general_model->update_row_data('tbl_custom_invoice', array('insertInvID' => $tid), array('qb_status' => 1, 'TimeModified' => date('Y-m-d H:i:s'), 'EditSequence' => $edtID));
					$qbc1 = $this->general_model->get_select_data('qb_test_customer', array('companyID'), array('ListID' => $exist_row['Customer_ListID']));
					$qbc = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $qbc1['companyID']));

					$user    =  $qbc['qbwc_username'];

				} else {


					$updateRec    =   $this->general_model->update_row_data('tbl_custom_invoice', array('insertInvID' => $tid), array('qb_status' => 0, 'TimeModified' => date('Y-m-d H:i:s')));
					$qbc1 = $this->general_model->get_select_data('qb_test_customer', array('companyID'), array('ListID' => $exist_row['Customer_ListID']));
					$qbc = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $qbc1['companyID']));

					$user    =  $qbc['qbwc_username'];

				}
			}

			$log_row = $this->general_model->get_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID));

			if (!empty($log_row)) {
				
				$logupdate    =   $this->general_model->update_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID), array('qb_status' =>'q', 'enqueue_datetime' => date('Y-m-d H:i:s')));
			}
			$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
		}
		redirect('home/qbd_log', 'refresh');
	}

	function create_invoice()
	{


		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;
		if (!empty($this->input->post(null, true))) {

			$total = 0;
			$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (!empty($in_data)) {
				$inv_pre   = $in_data['prefix'];
				$inv_po    = $in_data['postfix'] + 1;
				$new_inv_no = $inv_pre . $inv_po;
				$randomNum =   rand(100000, 900000);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('home/invoices', 'refresh');
			}
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$qbc = $this->customer_model->customer_qbc_by_id($customerID);
			$qb_user = $qbc->qbwc_username;

			if ($this->czsecurity->xssCleanPostInput('setMail'))
				$chh_mail = 1;
			else
				$chh_mail = 0;

			$tax          = $this->czsecurity->xssCleanPostInput('taxes');

			if ($tax) {
				$tax_data =    $this->general_model->get_row_data('tbl_taxes', array('taxID' => $tax));
				$taxRate = $tax_data['taxRate'];
				$taxlsID = $tax_data['taxListID'];
				$tx_rate =  $tax_data['taxRate'];
			} else {
				$taxRate = 0;
				$taxlsID = '';
				$tx_rate = 0;
			}
			$taxval = 0;
			$tot = 0;
			$totalTax = 0;
			$taxList = $this->czsecurity->xssCleanPostInput('tax_check');
			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				//fixed calculation
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTax'] = $taxList[$key]; 

				$itemDes = $this->czsecurity->xssCleanPostInput('description')[$key];

				$itemDes = str_replace('<','< ',$itemDes);
				$itemDes = str_replace('>','> ',$itemDes);
				$insert_row['itemDescription'] = $itemDes;

				$tot  = $this->czsecurity->xssCleanPostInput('quantity')[$key] * $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$total =  $total+$this->czsecurity->xssCleanPostInput('total')[$key];
				if($insert_row['itemTax']){
					$totalTax += ($tot * $tx_rate)/100;
				}
				$item_val[$key] = $insert_row;
				
			}
			$totalTax = round($totalTax, 2);
			$total = round($total, 2);
			
			$check_today = date('Y-m-d');
			$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
			$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');
			if ($pterm != "" && $pterm != 0) {
				$invoicedate = date("Y-m-d", strtotime("$inv_date +$pterm day"));
			} else {
				$invoicedate = date('Y-m-d', strtotime($inv_date));
			}

			if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
				$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			} else {
				$paygateway  = 0;
			}

			if ($this->czsecurity->xssCleanPostInput('card_list') != '') {
				$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			} else {
				$cardID  = 0;
			}
			
			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
			$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
			
			$cs_data  = $this->general_model->get_select_data('qb_test_customer', array('FullName', 'companyID', 'companyName', 'Contact'), array('ListID' => $customerID,'qbmerchantID' => $user_id));
			$fullname = $cs_data['FullName'];
			$companyID = $cs_data['companyID'];
			

			if ($this->czsecurity->xssCleanPostInput('autopay')) {
				$auto = '1';
			} else {
				$auto = '0';
				$paygateway = 0;
			}
			if($total > 0){
				$isPaid = 'false';
			}else{
				$isPaid = 'true';
			}
			$inv_data = array(
				'Customer_FullName' => $fullname,
				'Customer_ListID' => $customerID,
				'TxnID' => $randomNum,
				'RefNumber' => $new_inv_no,
				'TimeCreated' => date('Y-m-d H:i:s'),
				'TimeModified' => date('Y-m-d H:i:s'),
				'DueDate'   => $invoicedate,
				'ShipAddress_Addr1' => $address1,
				'ShipAddress_Addr2' => $address2,
				'ShipAddress_City' => $city,
				'ShipAddress_Country' => $country,
				'ShipAddress_State' => $state,
				'ShipAddress_PostalCode' => $zipcode,

				'IsPaid'				=> $isPaid,
				'insertInvID' => $randomNum,
				'invoiceRefNumber' => $inv_po,
				'BalanceRemaining' => $total,
				'freeTrial' => 0,
				'gatewayID' => $paygateway,
				'autoPayment' => $auto,
				'cardID' => $cardID,
				'merchantID' => $user_id,

			);
			$inv_data['Billing_Addr1']     = $this->czsecurity->xssCleanPostInput('baddress1');
			$inv_data['Billing_Addr2']     = $this->czsecurity->xssCleanPostInput('baddress2');
			$inv_data['Billing_City']     = $this->czsecurity->xssCleanPostInput('bcity');
			$inv_data['Billing_State']     = $this->czsecurity->xssCleanPostInput('bstate');
			$inv_data['Billing_Country']     = $this->czsecurity->xssCleanPostInput('bcountry');
			$inv_data['Billing_PostalCode']     = $this->czsecurity->xssCleanPostInput('bzipcode');
			$inv_data['qb_action'] = 'Add Invoice';
			$inv_data['TaxRate'] 				= $taxRate;
			$inv_data['TotalTax'] 				= $totalTax;
			$inv_data['TaxListID'] 				= $taxlsID;
			$this->general_model->insert_row('tbl_custom_invoice', $inv_data);
			foreach ($item_val as $k => $item) {
				$item['subscriptionID'] = 0;
				$item['invoiceDataID'] = $randomNum;
				$item_tax = 0;
				if($item['itemTax']){
					$item_tax = 1;
				}

				$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				$itemline = mt_rand(5000500, 9000900);
				$inv_item =	array('TxnID' => $randomNum, 'TxnLineID' => $itemline, 'Item_ListID' => $item['itemListID'], 'Item_FullName' => $item['itemFullName'], 'Descrip' => $item['itemDescription'], 'Quantity' => $item['itemQuantity'], 'Rate' => $item['itemRate'], 'taxListID' => $taxlsID, 'item_tax' => $item_tax,'inv_itm_merchant_id' => $user_id);
				$this->general_model->insert_row('qb_test_invoice_lineitem', $inv_item);
			}

			if ($ins) {

				$isCustomerSynced = $this->general_model->get_row_data('tbl_custom_customer', array('customListID' => $inv_data['Customer_ListID'], 'merchantID' => $user_id));

				$comp_data   = $this->general_model->get_select_data('tbl_company',array('id','qbwc_username'),array('merchantID'=>$user_id));
				$user = $comp_data['qbwc_username'];
				
				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));

				$invoice_data['TxnID'] 			= $randomNum;
				$invoice_data['TimeCreated'] 	= date('Y-m-d H:i:s');
				$invoice_data['TimeModified'] 	= date('Y-m-d H:i:s');
				$invoice_data['RefNumber'] 		= $inv_data['RefNumber'];
				$invoice_data['Customer_ListID']        = $inv_data['Customer_ListID'];
				$invoice_data['Customer_FullName'] 		= $inv_data['Customer_FullName'];
				$invoice_data['ShipAddress_Addr1'] 		= $inv_data['ShipAddress_Addr1'];
				$invoice_data['ShipAddress_Addr2'] 		= $inv_data['ShipAddress_Addr2'];
				$invoice_data['ShipAddress_City']      	= $inv_data['ShipAddress_City'];
				$invoice_data['ShipAddress_State']    	= $inv_data['ShipAddress_State'];
				$invoice_data['ShipAddress_Country']    = $inv_data['ShipAddress_Country'];
				$invoice_data['ShipAddress_PostalCode'] = $inv_data['ShipAddress_PostalCode'];
				$invoice_data['Billing_Addr1'] 		= $inv_data['Billing_Addr1'];
				$invoice_data['Billing_Addr2'] 		= $inv_data['Billing_Addr2'];
				$invoice_data['Billing_City']      	= $inv_data['Billing_City'];
				$invoice_data['Billing_State']    	= $inv_data['Billing_State'];
				$invoice_data['Billing_Country']    = $inv_data['Billing_Country'];
				$invoice_data['Billing_PostalCode'] = $inv_data['Billing_PostalCode'];
				$invoice_data['BalanceRemaining'] 		= $inv_data['BalanceRemaining'];
				$invoice_data['DueDate'] 				= $inv_data['DueDate'];
				$invoice_data['IsPaid'] 				= $inv_data['IsPaid'];
				$invoice_data['userStatus'] 		    	= 'Active';
				$invoice_data['insertInvID'] 			= $inv_data['insertInvID'];
				$invoice_data['invoiceRefNumber'] 		= $inv_data['invoiceRefNumber'];
				$invoice_data['TaxRate'] 				= $taxRate;
				$invoice_data['TotalTax'] 				= $totalTax;
				$invoice_data['TaxListID'] 				= $taxlsID;
				$invoice_data['qb_inv_companyID']  = $comp_data['id'];
				$invoice_data['qb_inv_merchantID'] = $user_id;
				$this->general_model->insert_row('qb_test_invoice', $invoice_data);

				$insert_data = array('invoiceID' => $inv_data['TxnID'], 'cardID' => $inv_data['cardID'], 'gatewayID' => $inv_data['gatewayID'], 'merchantID' => $inv_data['merchantID'], 'updatedAt' => date('Y-m-d H:i:s'), 'CreatedAt' => date('Y-m-d H:i:s'));
				$this->db->insert('tbl_scheduled_invoice_payment', $insert_data);

				$QBD_Sync = new QBD_Sync($user_id);
				$QBD_Sync->invoiceSync([
					'invID' => $randomNum
				]);

				if ($chh_mail == '1') {

					$inv_no = $inv_data['RefNumber'];
					$invoiceID = $inv_data['insertInvID'];
					$invoice  = $this->company_model->get_invoice_details_data($invoiceID);
					$this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');


					$condition_mail         = array('templateType' => '1', 'merchantID' => $user_id);

					$toEmail = $cs_data['Contact'];
					$company = $cs_data['companyName'];
					$customer = $cs_data['FullName'];
					$invoice_due_date = $inv_data['DueDate'];
					$invoicebalance = $inv_data['BalanceRemaining'];
					$tr_date   = date('Y-m-d H:i:s');
					$this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $customerID, $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
				}

				$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
				redirect('home/invoice_details/' . $inv_data['TxnID'], 'refresh');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
				redirect('home/invoices', 'refresh');
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
		$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan', '');
		$data['plans']          = $this->company_model->get_plan_data_invoice($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		if ($this->uri->segment(3) != "") {
			$customerID = $this->uri->segment(3);
			$data['Invcustomer']  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
			$data['cards']        = $this->card_model->get_card_data($customerID);
		}

		$data['netterms']  = $this->general_model->get_table_data('tbl_payment_terms', array('merchantID' => $user_id), array('name' => 'pt_netTerm', 'type' => 'asc'));
		$taxname = $this->company_model->get_tax_data($user_id);
		$data['taxes'] = $taxname;

		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;

		$data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_new_invoice', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	function edit_custom_invoice()
	{
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$due_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('dueDate')));
		$txnID = $randomNum = $this->czsecurity->xssCleanPostInput('invNo');

		$qb_data = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('merchantID' => $user_id));
		$user   = 	$qb_data['qbwc_username'];
		$index = $this->czsecurity->xssCleanPostInput('index');

		$invdata = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $txnID, 'qb_inv_merchantID' => $user_id));

		$total = $invdata['Customer_FullName'];
		$tax          = $this->czsecurity->xssCleanPostInput('taxes');

		if ($tax) {
			$tax_data =    $this->general_model->get_row_data('tbl_taxes', array('taxID' => $tax));
			$taxRate = $tax_data['taxRate'];
			$taxlsID = $tax_data['taxListID'];
			$tx_rate =  $tax_data['taxRate'];
		} else {
			$taxRate = 0;
			$taxlsID = '';
			$tx_rate = 0;
		}
		$taxval = 0;
		$totalTax = 0;
		$taxList = $this->czsecurity->xssCleanPostInput('is_tax_check');

		$product_list = $this->czsecurity->xssCleanPostInput('productID');
		if (isset($product_list) && !empty($product_list)) {
			$total = 0;
			foreach ($product_list as $key => $prod) {
				$tot = 0;
				$insert_row['TxnLineID'] = $this->czsecurity->xssCleanPostInput('txn_line_id')[$key];
				$insert_row['itemListID'] = $product_list[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTax'] = $taxList[$key];
				$itemDes = $this->czsecurity->xssCleanPostInput('description')[$key];

				$itemDes = str_replace('<','< ',$itemDes);
				$itemDes = str_replace('>','> ',$itemDes);
				$insert_row['itemDescription'] = $itemDes;

				$tot  = $this->czsecurity->xssCleanPostInput('quantity')[$key] * $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];
				if($insert_row['itemTax']){
					$totalTax += ($insert_row['itemTotal'] * $tx_rate)/100;
				}
				$item_val[$key] = $insert_row;
			}
			$totalTax = round($totalTax, 2);
			$total = round($total + $totalTax, 2);
			$this->general_model->delete_row_data('tbl_subscription_invoice_item', array('invoiceDataID' => $randomNum));
			$this->general_model->delete_row_data('qb_test_invoice_lineitem', array('TxnID' => $randomNum,'inv_itm_merchant_id' => $user_id));
			foreach ($item_val as $k => $item) {
				$item['subscriptionID'] = 0;
				$item['invoiceDataID'] = $randomNum;
				$item_tax = 0;
				if($item['itemTax']){
					$item_tax = 1;
				}

				$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				$itemline = mt_rand(5000500, 9000900);
				$inv_item =	array('TxnID' => $randomNum, 'TxnLineID' => $item['TxnLineID'], 'Item_ListID' => $item['itemListID'], 'Item_FullName' => $item['itemFullName'], 'Descrip' => $item['itemDescription'], 'Quantity' => $item['itemQuantity'], 'Rate' => $item['itemRate'], 'item_tax' => $item_tax, 'inv_itm_merchant_id' => $user_id);
				$this->general_model->insert_row('qb_test_invoice_lineitem', $inv_item);
			}
		}

		$inv_data = array(
			'Customer_FullName' => $invdata['Customer_FullName'],
			'Customer_ListID' => $invdata['Customer_ListID'],
			'TxnID' => $randomNum,
			'RefNumber' => $invdata['RefNumber'],
			'TimeCreated' => date('Y-m-d H:i:s'),
			'TimeModified' => date('Y-m-d H:i:s'),
			'DueDate'   => $due_date,
			'ShipAddress_Addr1' => $invdata['ShipAddress_Addr1'],
			'ShipAddress_Addr2' => $invdata['ShipAddress_Addr2'],
			'ShipAddress_City' => $invdata['ShipAddress_City'],
			'ShipAddress_Country' => $invdata['ShipAddress_Country'],
			'ShipAddress_State' => $invdata['ShipAddress_State'],
			'ShipAddress_PostalCode' => $invdata['ShipAddress_PostalCode'],

			'IsPaid'				=> 'false',
			'insertInvID' => $randomNum,
			'invoiceRefNumber' => $invdata['RefNumber'],
			'BalanceRemaining' => $total,
			'merchantID' => $user_id,
			'EditSequence' => $invdata['EditSequence']
		);

		$inv_data['Billing_Addr1']     = $invdata['Billing_Addr1'];
		$inv_data['Billing_Addr2']     = $invdata['Billing_Addr2'];
		$inv_data['Billing_City']      = $invdata['Billing_City'];
		$inv_data['Billing_State']     = $invdata['Billing_State'];

		$inv_data['Billing_Country']     = $invdata['Billing_Country'];
		$inv_data['Billing_PostalCode']     = $invdata['Billing_PostalCode'];

		$num_inv =    $this->general_model->get_num_rows('tbl_custom_invoice', array('insertInvID' => $randomNum));
		if ($num_inv > 0) {
			$inv_data['qb_action'] = 'Mod Invoice';
			$this->general_model->update_row_data('tbl_custom_invoice', array('insertInvID' => $randomNum), $inv_data);
		} else {
			$inv_data['qb_action'] = 'Add Invoice';
			$this->general_model->insert_row('tbl_custom_invoice', $inv_data);
		}

		$inv_update_data = array('DueDate' => $due_date, 'BalanceRemaining' => $total, 'TaxRate' 				=> $taxRate, 'TotalTax' => $totalTax, 'TaxListID' => $taxlsID);
		$this->general_model->update_row_data('qb_test_invoice', array('TxnID' => $randomNum, 'qb_inv_merchantID' => $user_id), $inv_update_data);

		$QBD_Sync = new QBD_Sync($user_id);
		$QBD_Sync->invoiceSync([
			'invID' => $txnID
		]);

		$this->session->set_flashdata('success', 'Invoice Updated Successfully');

		if (strtoupper($index) == 'SELF')
			redirect('home/invoice_details/' . $txnID, 'refresh');
		else
			redirect('home/invoices', 'refresh');
	}
	function edit_invoice()
	{


		if (!empty($this->input->post(null, true))) {

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {

				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$editSeq      = $this->czsecurity->xssCleanPostInput('edit_sequence');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			$customer   = $this->customer_model->customer_by_id($customerID);
			$txnID      = $this->czsecurity->xssCleanPostInput('txnID');
			$refNumber  = $this->czsecurity->xssCleanPostInput('refNumber');
			$fullname   = $customer->FullName;
			$user       = $customer->qbwc_username;
			$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('duedate')));

			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');


			$inv_data = array(
				'Customer_FullName' => $fullname,
				'Customer_ListID' => $customerID,
				'TxnID' => $txnID,
				'RefNumber' => $refNumber,
				'TimeCreated' => date('Y-m-d H:i:S'),
				'TimeModified' => date('Y-m-d H:i:S'),
				'DueDate'   => $duedate,
				'ShipAddress_Addr1' => $address1,
				'ShipAddress_Addr2' => $address2,
				'ShipAddress_City' => $city,
				'ShipAddress_Country' => $country,
				'ShipAddress_State' => $state,
				'ShipAddress_PostalCode' => $zipcode,
				'IsPaid'				=> 'false',
				'invoiceRefNumber' => $refNumber,
				'EditSequence' => $editSeq
			);

			if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
				$card = array();
				$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
				$card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
				$card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
				$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
				$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
				$card['friendlyname'] = $friendlyName;
				$card['customerID']  = $customerID;

				$card_data = $this->check_friendly_name($customerID, $friendlyName);
				if (!empty($card_data)) {
					$this->update_card($card, $customerID, $friendlyName);
					$cardID  = $card_data['CardID'];
				} else {


					$cardID = $this->insert_new_card($card);
				}
			}



			$invoice_data =  $this->general_model->get_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber));

			if (!empty($invoice_data)) {
				
				$rowID    = $invoice_data['insertInvID'];
				$ins_data = $this->general_model->update_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber), $inv_data);

				$this->general_model->delete_row_data('tbl_subscription_invoice_item', array('invoiceDataID' => $rowID));
				$randomNum = $rowID;
				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $rowID;
					$item['invoiceDataID']  = $rowID;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
				

			} else {
				$randomNum = substr(str_shuffle("0123456789"), 0, 5);
				$inv_data['insertInvID'] = $randomNum;
				$ins_data = $this->general_model->insert_row('tbl_custom_invoice', $inv_data);

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $randomNum;
					$item['invoiceDataID']  = $randomNum;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
			}

			if ($ins_data && $ins) {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_INVOICE, $randomNum, '', '', $user);
				$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}

			redirect('home/invoices', 'refresh');
		}



		if ($this->uri->segment('3') != "") {
			$invoiceID = $this->uri->segment('3');
			$invoice   = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $invoiceID));
			$products  = $this->general_model->get_table_data('qb_test_invoice_lineitem', array('TxnID' => $invoiceID));
			$custom    = $this->general_model->get_row_data('tbl_custom_invoice', array('RefNumber' => $invoice['RefNumber']));
			if (!empty($custom)) {
				$data['custom'] =  $custom;
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['plans']          = $this->company_model->get_plan_data($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['invoice']		= $invoice;
		$data['items']		    = $products;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/edit_invoice', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function update_gateway()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}


			$sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));

			$old_gateway =  $this->czsecurity->xssCleanPostInput('gateway_old');
			$new_gateway =  $this->czsecurity->xssCleanPostInput('gateway_new');
			if ($old_gateway != '' && $new_gateway != '' && !empty($sub_ID)) {
				$condition	  = array('merchantDataID' => $user_id, 'paymentGateway' => $old_gateway);
				$num_rows    = $this->general_model->get_num_rows('tbl_subscriptions', $condition);

				if ($num_rows > 0) {
					$update_data = array('paymentGateway' => $new_gateway);

					$sss = $this->db->query("update tbl_subscriptions set paymentGateway='" . $new_gateway . "' where subscriptionID IN ($sub_ID) ");

					if ($sss) {

						$this->session->set_flashdata('success', 'Subscription gateway have changed');
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process.</div>');
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> No data selected. </div>');
				}
			} else {
				$rror_msg = '';
				if ($old_gateway == '')
					$rror_msg = 'Old Gateway is required';

				if ($new_gateway == '')
					$rror_msg = 'New Gateway is required';
				if (empty($sub_ID))
					$rror_msg = 'Subscription is required to update';
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $rror_msg . '</div>');
			}
		}
		redirect('SettingSubscription/subscriptions', 'refresh');
	}



	//------------- To edit gateway ------------------//

	public function get_gateway_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gateway_id');
		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$val = array(
			'paymentGateway' => $id,
			'merchantDataID' => $merchantID
		);

		$datas = $this->customer_model->get_subscriptiongateway_data($id, $merchantID);

?>


		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Customer Name</strong></th>
					<th class="text-left col-md-6"> <strong>Plan</strong></th>
					<th class="text-center col-md-6"><strong>Select</strong></th>
				</tr>
				<?php
				if (!empty($datas)) {
					foreach ($datas as $c_data) {
				?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['FullName']; ?> </td>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['sub_planName']; ?> </td>
							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['subscriptionID']; ?>" /> </td>
						</tr>


					<?php     }
				} else { ?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php } ?>

			</tbody>
		</table>


<?php die;
	}









	public function delete_subscription()
	{


		$subscID = $this->czsecurity->xssCleanPostInput('subscID');
		$condition =  array('subscriptionID' => $subscID);
		$this->general_model->delete_row_data('tbl_subscription_invoice_item', $condition);

		$del  = $this->general_model->delete_row_data('tbl_subscriptions', $condition);
		if ($del) {

			$this->session->set_flashdata('success', 'Successfully Deleted');
			array('status' => "success");
			echo json_encode(array('status' => "success"));
			die;
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
		}

		return false;
	}

	public function get_subplan()
	{

		$planid =   $this->czsecurity->xssCleanPostInput('planID');

		$val = $this->company_model->get_subplan_data($planid);
		echo json_encode($val);
	}





	public function check_friendly_name($cid, $cfrname)
	{
		$card = array();
		$this->load->library('encrypt');







		$query1 = $this->db1->query("Select * from customer_card_data where customerListID = '" . $cid . "' and customerCardfriendlyName ='" . $cfrname . "' ");

		$card = $query1->row_array();
		return $card;
	}


	public function insert_new_card($card)
	{

		$this->load->library('encrypt');




		$card_no  = $card['card_number'];
		$expmonth = $card['expiry'];
		$exyear   = $card['expiry_year'];
		$cvv      = $card['cvv'];

		$b_add1   = $card['Billing_Addr1'];
		$b_add2   = $card['Billing_Addr2'];
		$b_city   = $card['Billing_City'];
		$b_state  = $card['Billing_State'];
		$b_country = $card['Billing_Country'];
		$b_contact = $card['Billing_Contact'];
		$b_zip   = $card['Billing_Zipcode'];

		$friendlyname = $card['friendlyname'];
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		$customer   = $card['customerID'];

		$is_default = 0;
     	$checkCustomerCard = checkCustomerCard($customer,$merchantID);
    	if($checkCustomerCard == 0){
    		$is_default = 1;
    	}
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $this->encrypt->encode($card_no),
			'CardCVV'      => $this->encrypt->encode($cvv),
			'Billing_Addr1'	 => $b_add1,
			'Billing_Addr2'	 => $b_add2,
			'Billing_City'	 => $b_city,
			'Billing_State'	 => $b_state,
			'Billing_Country'	 => $b_country,
			'Billing_Contact'	 => $b_contact,
			'Billing_Zipcode'	 => $b_zip,
			'customerListID' => $customer,
			'merchantID'     => $merchantID,
			'is_default'			   => $is_default,
			'customerCardfriendlyName' => $friendlyname,
			'createdAt' 	=> date("Y-m-d H:i:s")
		);

		$this->db1->insert('customer_card_data', $insert_array);
		return $this->db1->insert_id();;
	}



	public function update_card($card, $cid, $cfrname)
	{
		$this->load->library('encrypt');


		$card_no  = $card['card_number'];
		$expmonth = $card['expiry'];
		$exyear   = $card['expiry_year'];
		$cvv      = $card['cvv'];
		$b_add1   = $card['Billing_Addr1'];
		$b_add2   = $card['Billing_Addr2'];
		$b_city   = $card['Billing_City'];
		$b_state   = $card['Billing_State'];
		$b_country   = $card['Billing_Country'];
		$b_contact   = $card['Billing_Contact'];
		$b_zip   = $card['Billing_Zipcode'];


		$update_data =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $this->encrypt->encode($card_no),
			'CardCVV'      => $this->encrypt->encode($cvv),
			'Billing_Addr1'	 => $b_add1,
			'Billing_Addr2'	 => $b_add2,
			'Billing_City'	 => $b_city,
			'Billing_State'	 => $b_state,
			'Billing_Country'	 => $b_country,
			'Billing_Contact'	 => $b_contact,
			'Billing_Zipcode'	 => $b_zip,
			'updatedAt' 	=> date("Y-m-d H:i:s")
		);
		$this->db1->where('customerListID', $cid);
		$this->db1->where('customerCardfriendlyName', $cfrname);
		$this->db1->update('customer_card_data', $update_data);
		return true;
	}


	public function get_card_expiry_data($customerID)
	{

		$card = array();
		$this->load->library('encrypt');


		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$customer_card['CardNo']  = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
				$customer_card['CardID'] = $card_data['CardID'];
				$customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				$card[$key] = $customer_card;
			}
		}

		return  $card;
	}

	public function get_item_data()
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		} else
				if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		} else {
			return false;
		}

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('qb_test_item', array('ListID' => $itemID));
			$itemdata['SalesPrice'] =  number_format($itemdata['SalesPrice'], 2, '.', '');
			echo json_encode($itemdata);
			die;
		}
		return false;
	}

	public function invoice_schedule()
	{



		if ($this->session->userdata('logged_in')) {
			$merchID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchID = $this->session->userdata('user_logged_in')['merchantID'];
		}


		if (!empty($merchID) && !empty($this->input->post(null, true))) {

			$val = array(
				'merchantID' => $merchID,
			);

			$duedate      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));

			$sch_gateway  = $this->czsecurity->xssCleanPostInput('sch_gateway');

			$Inv_id = $this->czsecurity->xssCleanPostInput('scheduleID');
			$sch_method = $this->czsecurity->xssCleanPostInput('sch_method');
			$sch_amount = $this->czsecurity->xssCleanPostInput('schAmount');
			$schCardID = $this->czsecurity->xssCleanPostInput('schCardID');
			$inv_data = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $Inv_id, 'isPaid' => 'false'));

			$update_data = array('scheduleDate' => $duedate, 'invoiceID' => $Inv_id, 'cardID' => $schCardID, 'scheduleAmount' => $sch_amount,  'paymentMethod' => $sch_method, 'gatewayID' => $sch_gateway, 'updatedAt' => date('Y-m-d H:i:s'));
			if (!empty($inv_data)) {
				$sch_data = $this->general_model->get_row_data('tbl_scheduled_invoice_payment', array('invoiceID' => $Inv_id, 'merchantID' => $merchID));
				$customerID  = $inv_data['Customer_ListID'];
				$c_data     = $this->general_model->get_select_data('qb_test_customer', array('companyID', 'companyName', 'Contact', 'FullName'), array('ListID' => $customerID, 'qbmerchantID' => $merchID));

				$companyID = $c_data['companyID'];
				if ($schCardID == "new1") {


					$address1 =  $this->czsecurity->xssCleanPostInput('address1');
					$address2 =  $this->czsecurity->xssCleanPostInput('address2');
					$city     =  $this->czsecurity->xssCleanPostInput('city');
					$country     =  $this->czsecurity->xssCleanPostInput('country');
					$phone       =  $this->czsecurity->xssCleanPostInput('contact');
					$state       = $this->czsecurity->xssCleanPostInput('state');
					$zipcode     =  $this->czsecurity->xssCleanPostInput('zipcode');



					if ($sch_method == 1) {
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$card_type      = $this->general_model->getType($card_no);
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$card_data = array(
							'cardMonth' => $expmonth,
							'cardYear'	 => $exyear,
							'CardType'     => $card_type,
							'CustomerCard' => $card_no,
							'CardCVV'      => $cvv,
							'customerListID' => $customerID,
							'companyID'     => $companyID,
							'merchantID'   => $merchID,

							'createdAt' 	=> date("Y-m-d H:i:s"),
							'Billing_Addr1'	 => $address1,
							'Billing_Addr2'	 => $address2,
							'Billing_City'	 => $city,
							'Billing_State'	 => $state,
							'Billing_Country'	 => $country,
							'Billing_Contact'	 => $phone,
							'Billing_Zipcode'	 => $zipcode,
						);
						$schCardID =    $this->card_model->process_card($card_data);
						$update_data['cardID'] =  $schCardID;
					}
					if ($sch_method == 2) {
						$acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
						$route_number = $this->czsecurity->xssCleanPostInput('route_number');
						$acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
						$secCode      = $this->czsecurity->xssCleanPostInput('secCode');
						$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
						$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
						$card_type = 'Echeck';
						$friendlyname = $card_type . ' - ' . substr($acc_number, -4);
						$card_data = array(
							'CardType'     => $card_type,
							'accountNumber'   => $acc_number,
							'routeNumber'     => $route_number,
							'accountName'   => $acc_name,
							'accountType'   => $acct_type,
							'accountHolderType'   => $acct_holder_type,
							'secCodeEntryMethod'   => $secCode,
							'customerListID' => $customerID,
							'companyID'     => $companyID,
							'merchantID'   => $merchID,
							'customerCardfriendlyName' => $friendlyname,

							'createdAt' 	=> date("Y-m-d H:i:s"),
							'Billing_Addr1'	 => $address1,
							'Billing_Addr2'	 => $address2,
							'Billing_City'	 => $city,
							'Billing_State'	 => $state,
							'Billing_Country'	 => $country,
							'Billing_Contact'	 => $phone,
							'Billing_Zipcode'	 => $zipcode,
						);
						$schCardID =    $this->card_model->insert_card_data($card_data);
						$update_data['cardID'] =  $schCardID;
					}
				} else {
					$update_data['cardID'] =  $schCardID;
				}

				$update_data['merchantID'] = $merchID;
				$update_data['scheduleAmount'] = $sch_amount;
				$update_data['customerID'] = $inv_data['Customer_ListID'];
				$update_data['updatedAt'] = date('Y-m-d H:i:s');
				$update_data['createdAt'] = date('Y-m-d H:i:s');
				$update_data['autoPay']   = 1;
				$update_data['autoPay']   = 1;
				if (!empty($sch_data)) {
					$this->general_model->update_row_data('tbl_scheduled_invoice_payment', array('scheduleID' => $sch_data['scheduleID']), $update_data);
				} else {
					$this->general_model->insert_row('tbl_scheduled_invoice_payment', $update_data);
				}




				$this->session->set_flashdata('success', ' Successfully Scheduled Payment');
				array('status' => "success");
				echo json_encode(array('status' => "success"));
				die;
			} else {


				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
				array('status' => "success");
				echo json_encode(array('status' => "success"));
				die;
			}
		} else {


			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error</strong></div>');
		}
		return false;
	}

	public function subscription_invoices()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$sbID = $this->uri->segment(3);
		$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions', array('subscriptionID' => $sbID));

		$conditionGt				= array('planID' => $data['subs']['planID']);
		$conditionCust				= array('ListID' => $data['subs']['customerID'],'qbmerchantID' =>$user_id);

		$subplansData		= $this->general_model->get_row_data('tbl_subscriptions_plan_qb', $conditionGt);
		$subplansCustomer		= $this->general_model->get_row_data('qb_test_customer', $conditionCust);


		$data['page_num']      = 'customer_qbd';
		$condition				  = array('merchantID' => $user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		$invoices    			 = $this->company_model->get_subscription_invoice_data($user_id, $sbID);
		$data['totalInvoice'] = count($invoices);

		$data['revenue']   			 = $this->company_model->get_subscription_revenue($user_id, $sbID);

		$data['customerName'] = $subplansCustomer['FullName'];
		$data['planName'] = $subplansData['planName'];
		
		$condition  = array('comp.merchantID' => $user_id);

		$data['credits'] = $this->general_model->get_credit_user_data($condition);

		$data['invoices']    = $invoices;

		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_subscription_details', $data);
		$this->load->view('pages/page_qbd_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	protected function _createSubscriptionFirstInvoice($subs){
		$subs['merchantDataID'] = $this->merchantID;
		$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']));

		$inv_pre    = $in_data['prefix'];
		$inv_po     = $in_data['postfix'] + 1;
		$new_inv_no = $inv_pre . $inv_po;
		$duedate    = date("Y-m-d", strtotime($subs['nextGeneratingDate']));
		$randomNum  = substr(str_shuffle("0123456789"), 0, 5);
		$date     = $subs['firstDate'];

		$customerData = $this->general_model->get_row_data('qb_test_customer', array('qbmerchantID' => $subs['merchantDataID'], 'ListID' => $subs['customerID']));

		if ($subs['generatedInvoice'] < $subs['totalInvoice'] || $subs['totalInvoice'] == 0) {

			if ($subs['generatedInvoice'] < $subs['freeTrial']) {
				$free_trial = '1';
			} else {
				$free_trial = '0';
			}

			$inv_data = array(
				'Customer_FullName'      => $customerData['FullName'],
				'Customer_ListID'        => $subs['customerID'],
				'TxnID'                  => $randomNum,
				'RefNumber'              => $new_inv_no,
				'TimeCreated'            => date('Y-m-d H:i:S'),
				'TimeModified'           => date('Y-m-d H:i:S'),
				'DueDate'                => $duedate,
				'ShipAddress_Addr1'      => $subs['address1'],
				'ShipAddress_Addr2'      => $subs['address2'],
				'ShipAddress_City'       => $subs['city'],
				'ShipAddress_Country'    => $subs['country'],
				'ShipAddress_State'      => $subs['state'],
				'ShipAddress_PostalCode' => $subs['zipcode'],
				'IsPaid'                 => 'false',
				'insertInvID'            => $randomNum,
				'invoiceRefNumber'       => $inv_po,
				'freeTrial'              => $free_trial,
				'gatewayID'              => $subs['paymentGateway'],
				'autoPayment'            => $subs['automaticPayment'],
				'cardID'                 => $subs['cardID'],
				'paymentMethod'          => '1',
				'merchantID'             => $subs['merchantDataID'],
			);

			if (isset($subs['taxID']) && $subs['taxID']) {
				$tax_data =    $this->general_model->get_row_data('tbl_taxes', array('taxID' => $subs['taxID']));
				$taxRate = $tax_data['taxRate'];
				$taxlsID = $tax_data['taxListID'];
			} else {
				$taxRate = 0;
				$taxlsID = '';
			}
			$taxval = 0;

			$inv_data['Billing_Addr1']     = $subs['baddress1'];
			$inv_data['Billing_Addr2']     = $subs['baddress2'];
			$inv_data['Billing_City']     = $subs['bcity'];
			$inv_data['Billing_State']     = $subs['bstate'];
			$inv_data['Billing_Country']     = $subs['bcountry'];
			$inv_data['Billing_PostalCode']     = $subs['bzipcode'];

			$total = $onetime = $recurring = 0;

			$subscriptionListItem = $subs['subscriptionItems'];
			$item_val = [];
			foreach ($subscriptionListItem as $key => $prod) {
				if($subs['generatedInvoice'] > 1 && $prod['oneTimeCharge']){
					continue;
				}

				$itemTotal = 0;
				$insert_row['itemListID'] = $prod['itemListID'];
				$insert_row['itemQuantity'] = $prod['itemQuantity'];
				$insert_row['itemRate'] = $prod['itemRate'];
				$insert_row['itemFullName'] = $prod['itemFullName'];
				$insert_row['itemDescription'] = $prod['itemDescription'];
				$insert_row['itemTax'] = $prod['itemTax'];
				$itemTotal = ($prod['itemRate'] * $prod['itemQuantity']); 
				$total += $itemTotal;

				if($prod['oneTimeCharge']){
					$onetime += $itemTotal;
				} else {
					$recurring += $itemTotal;
				}

				$item_val[] = $insert_row;
			}
			
			if ($subs['subplansData']['proRate'] && $subs['generatedInvoice'] == 0) {
				$proRate = $subs['subplansData']['proRate'];
			} else {
				$proRate = 0;
			}

			if ($subs['subplansData']['proRateBillingDay']) {
				$proRateday = $subs['subplansData']['proRateBillingDay'];
				$nextInvoiceDate = $subs['subplansData']['nextMonthInvoiceDate'];
			} else {
				$proRateday = 1;
				$nextInvoiceDate = date('d', strtotime($date));
			}

			$in_num   = $subs['generatedInvoice'];

			$prorataCalculation = prorataCalculation([
				'paycycle' => $subs['invoicefrequency'],
				'proRateday' => $proRateday,
				'nextInvoiceDate' => $nextInvoiceDate,
				'total_amount' => $total,
				'onetime' => $onetime,
				'recurring' => $recurring,
				'date' => $date,
				'in_num' => $in_num,
				'proRate' => $proRate,
			]);

			$next_date = $prorataCalculation['next_date'];
			$total = $prorataCalculation['total_amount'];

			if ($this->general_model->update_row_data('tbl_subscription_invoice_item',
				array('subscriptionID' => $subs['subscriptionID']), array('invoiceDataID' => $randomNum))) {

				$gen_inv = $subs['generatedInvoice'] + 1;
				$this->general_model->update_row_data('tbl_subscriptions',
					array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));
				
				$subscription_auto_invoices_data = [
					'subscriptionID' => $subs['subscriptionID'],
					'invoiceID' => $randomNum,
					'app_type' => 2 //QBD
				];
		
				$this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));

				$inv_data['qb_status'] = 0;
				$inv_data['qb_action'] = "Add Invoice";
				$inv_data['BalanceRemaining'] = $total;

				$this->general_model->insert_row('tbl_custom_invoice', $inv_data);
				foreach ($item_val as $k => $item) {
					$itemline = mt_rand(5000500, 9000900);
					$inv_item =	array('TxnID' => $randomNum, 'TxnLineID' => $itemline, 'Item_ListID' => $item['itemListID'], 'Item_FullName' => $item['itemFullName'], 'Descrip' => $item['itemDescription'], 'Quantity' => $item['itemQuantity'], 'Rate' => $item['itemRate'],'inv_itm_merchant_id'=> $subs['merchantDataID']);
					$this->general_model->insert_row('qb_test_invoice_lineitem', $inv_item);
				}

				$comp_data   = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$subs['merchantDataID']));
				$user = $comp_data['qbwc_username'];

				$TimeCreated = date('Y-m-d H:i:s', strtotime($date));
				$invoice_data['TxnID'] 			= $randomNum;
				$invoice_data['TimeCreated'] 	= $TimeCreated;
				$invoice_data['TimeModified'] 	= $TimeCreated;
				$invoice_data['RefNumber'] 		= $inv_data['RefNumber'];
				$invoice_data['Customer_ListID']        = $customerData['ListID'];
				$invoice_data['Customer_FullName'] 		= $customerData['FullName'];
				$invoice_data['ShipAddress_Addr1'] 		= $subs['address1'];
				$invoice_data['ShipAddress_Addr2'] 		= $subs['address2'];
				$invoice_data['ShipAddress_City']      	= $subs['city'];
				$invoice_data['ShipAddress_State']    	= $subs['state'];
				$invoice_data['ShipAddress_Country']    = $subs['country'];
				$invoice_data['ShipAddress_PostalCode'] = $subs['zipcode'];
				$invoice_data['Billing_Addr1'] 		= $subs['baddress1'];
				$invoice_data['Billing_Addr2'] 		= $subs['baddress2'];
				$invoice_data['Billing_City']      	= $subs['bcity'];
				$invoice_data['Billing_State']    	= $subs['bstate'];
				$invoice_data['Billing_Country']    = $subs['bcountry'];
				$invoice_data['Billing_PostalCode'] = $subs['bzipcode'];
				$invoice_data['BalanceRemaining'] 		= $inv_data['BalanceRemaining'];
				$invoice_data['DueDate'] 				= $inv_data['DueDate'];
				$invoice_data['IsPaid'] 				= $inv_data['IsPaid'];
				$invoice_data['userStatus'] 		    	= 'Active';
				$invoice_data['insertInvID'] 			= $inv_data['insertInvID'];
				$invoice_data['invoiceRefNumber'] 		= $inv_data['invoiceRefNumber'];
				$invoice_data['TaxRate'] 				= $taxRate;
				$invoice_data['TotalTax'] 				= $taxval;
				$invoice_data['TaxListID'] 				= $taxlsID;
				$invoice_data['qb_inv_companyID']  = $comp_data['id'];
				$invoice_data['qb_inv_merchantID'] = $subs['merchantDataID'];
				$this->general_model->insert_row('qb_test_invoice', $invoice_data);

				$insert_data = array('invoiceID' => $inv_data['TxnID'], 'cardID' => $inv_data['cardID'], 'gatewayID' => $inv_data['gatewayID'], 'merchantID' => $inv_data['merchantID'], 'updatedAt' => date('Y-m-d H:i:s'), 'CreatedAt' => date('Y-m-d H:i:s'));
				$this->db->insert('tbl_scheduled_invoice_payment', $insert_data);

				$QBD_Sync = new QBD_Sync($subs['merchantDataID']);
				$QBD_Sync->invoiceSync([
					'invID' => $randomNum
				]);

				if ($subs['emailRecurring'] == '1') {

					$inv_no = $inv_data['RefNumber'];
					$invoiceID = $inv_data['insertInvID'];
					$invoice  = $this->company_model->get_invoice_details_data($invoiceID);
					$this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');


					$condition_mail         = array('templateType' => '3', 'merchantID' => $subs['merchantDataID']);

					$toEmail = $customerData['Contact'];
					$company = $customerData['companyName'];
					$customer = $customerData['FullName'];
					$invoice_due_date = $inv_data['DueDate'];
					$invoicebalance = $inv_data['BalanceRemaining'];
					$tr_date   = date('Y-m-d H:i:s');
					$this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $subs['customerID'], $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
				}

				$first_date = strtotime($date);
				$current_time = time();
				if($subs['automaticPayment'] == 1 && $first_date <= $current_time){
					$paymentObj = new Manage_payments($subs['merchantDataID']);
					$paymentDetails =   $this->card_model->get_single_card_data($subs['cardID']);
					$saleData = [
						'paymentDetails' => $paymentDetails,
						'transactionByUser' => $this->transactionByUser,
						'ach_type' => (!empty($paymentDetails['CardNo'])) ? 1 : 2,
						'invoiceID'	=> $inv_data['TxnID'],
						'crtxnID'	=> '',
						'companyName'	=> $customerData['companyName'],
						'fullName'	=> $customerData['FullName'],
						'firstName'	=> $customerData['FirstName'],
						'lastName'	=> $customerData['LastName'],
						'contact'	=> $customerData['Phone'],
						'email'	=> $customerData['Contact'],
						'amount'	=> $inv_data['BalanceRemaining'],
						'gatewayID' => $subs['paymentGateway'],
						'storeResult' => true,
						'customerListID'		=> $customerData['ListID'],
					];

					$saleData = array_merge($saleData, $subs);
					$paidResult = $paymentObj->_processSaleTransaction($saleData);
					if(isset($paidResult['response']) && $paidResult['response']){
						$invUpdate       = array('IsPaid' => 'true', 'AppliedAmount' => -$inv_data['BalanceRemaining'], 'BalanceRemaining' => 0);

                        $conditionInv = array('TxnID' => $inv_data['TxnID'], 'qb_inv_merchantID' => $subs['merchantDataID']);
                        $this->general_model->update_row_data('qb_test_invoice', $conditionInv, $invUpdate);
					}
				}

			}
		}
	} 
}
