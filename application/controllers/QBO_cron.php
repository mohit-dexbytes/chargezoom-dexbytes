<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;

class QBO_cron extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
		$this->load->model('QBO_models/qbo_subscription_model');
	}
	
	

	public function index(){
	    
	 
	   }

	
	public function create_invoice(){
        $subsdata  = $this->qbo_subscription_model->get_subcription_data();
        
        if(!empty($subsdata)){
        
        
        foreach($subsdata as $subs)
        {
        
        $item_val =  $this->qbo_subscription_model->get_sub_invoice('tbl_subscription_invoice_item_qbo',array("subscriptionID" => $subs['subscriptionID']));
                
        $duedate   = date("Y-m-d", strtotime($subs['nextGeneratingDate']));
		$randomNum =substr(str_shuffle("0123456789"), 0, 5);
        
        $in_num  =$subs['generatedInvoice']; 
				$paycycle = $subs['invoiceFrequency'];
				$date     = $subs['firstDate'];
				
				  if($paycycle=='dly'){
							 $in_num   = ($in_num)? $in_num+1:'1';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day")); 
						 }
						 if($paycycle=='1wk'){
							 $in_num   = ($in_num)? $in_num+1:'1';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }		 
						
						if($paycycle=='2wk'){
							 $in_num   = ($in_num)?$in_num +2 :'1' ;
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }
						if($paycycle=='mon'){
						   $in_num   = ($in_num)? $in_num+1:'1'; 
							$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='2mn'){
							 $in_num   = ($in_num)?$in_num + 2*1:'1';
							$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
						 }		 
						 if($paycycle=='qtr'){
							 $in_num   = ($in_num)?$in_num + 3*1:'1';
							$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
						 }
						if($paycycle=='six'){
							 $in_num   = ($in_num)?$in_num + 6*1:'1';
							$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='yrl'){
							 $in_num   = ($in_num)?$in_num + 12*1:'0';
							$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }	

						if($paycycle=='2yr'){
							 $in_num   = ($in_num)?$in_num + 2*12:'1';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='3yr'){
							 $in_num   = ($in_num)?$in_num + 3*12:'1';
							$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }	
        if($this->general_model->update_row_data('tbl_subscription_invoice_item_qbo', 
			array('subscriptionID'=>$subs['subscriptionID']),array('invoiceDataID'=>$randomNum)))
			{
        
        $gen_inv = 	$subs['generatedInvoice']+1;
					$this->general_model->update_row_data('tbl_subscriptions_qbo', 
			array('subscriptionID'=>$subs['subscriptionID']), array('nextGeneratingDate'=>$next_date,'generatedInvoice'=>$gen_inv ));
			
        $val = array(
			'merchantID' => $subs['merchantDataID'],
		);
		
        $data = $this->general_model->get_row_data('QBO_token',$val);
		
		
			$accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	
	
		$dataService = DataService::Configure(array(
         'auth_mode' => 'oauth2',
         'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
         'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
         'accessTokenKey' =>  $accessToken,
         'refreshTokenKey' => $refreshToken,
         'QBORealmID' => "123145932193969",
         'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		 
		));
		
			$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        
          if($subs['taxID'] == 0){ 
              
          
           $lineArray = array();
        
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemQuantity'] * $item_val[$i]['itemRate'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			  "Qty" => $item_val[$i]['itemQuantity'],
    			  "UnitPrice" => $item_val[$i]['itemRate'],
    			  ]
    			  ]);
               $lineArray[] = $LineObj;
            }
			//Add a new Invoice
			$theResourceObj = Invoice::create([
			"CustomerRef" => $subs['customerID'], 
			 "ShipAddr" => [
			 "Line1"=>  $subs['address1'],
			 "City"=>  $subs['city'],
			 "Country"=>  $subs['country'],
			 "PostalCode"=>  $subs['zipcode']
		 ],
		 
		  "Line" => $lineArray,
       
		 "DueDate" =>  $duedate,
		 
		
		]);
			$resultingObj = $dataService->Add($theResourceObj);


			$error = $dataService->getLastError();  $lineArray = array();
        

          }else{
            
             $lineArray = array();
        
             $i = 0;
             for($i=0; $i<count($item_val);$i++){
               $LineObj = Line::create([
                   "Amount" => $item_val[$i]['itemQuantity'] * $item_val[$i]['itemRate'],
                    "DetailType" => "SalesItemLineDetail",
    		        "SalesItemLineDetail" => [
    			  "ItemRef" => $item_val[$i]['itemListID'],
    			  "Qty" => $item_val[$i]['itemQuantity'],
    			  "UnitPrice" => $item_val[$i]['itemRate'],
    			  "TaxCodeRef" => [
                                  "value" => "TAX"
                                ]
    			  ]
    			  ]);
               $lineArray[] = $LineObj;
            }
			//Add a new Invoice
			$theResourceObj = Invoice::create([
			"CustomerRef" => $subs['customerID'], 
			 "ShipAddr" => [
			 "Line1"=>  $subs['address1'],
			 "City"=>  $subs['city'],
			 "Country"=>  $subs['country'],
			 "PostalCode"=>  $subs['zipcode']
		 ],
		  "TxnTaxDetail" => [
                        "TxnTaxCodeRef" => [
                            "value" => $subs['taxID']
                        ],
                    ],
                    
		  "Line" => $lineArray,
       
		 "DueDate" =>  $duedate,
		 
		
		]);
			$resultingObj = $dataService->Add($theResourceObj);


			$error = $dataService->getLastError();
			
			
			
			if($resultingObj->Id!='')
			{
			       
			   $schedule['invoiceID']     = $resultingObj->Id;
			    $schedule['gatewayID']    = $subs['paymentGateway'];  
			     $schedule['cardID']      = $subs['cardID'];
			      $schedule['customerID'] = $subs['customerID'];
			        $schedule['autoPay']  =  $subs['automaticPayment'];
			         $schedule['paymentMethod']  =   '1';
			         $schedule['merchantID']     =  $subs['merchantDataID'];
			           $schedule['createdAt']    =  date('Y-m-d H:i:s');
			             $schedule['updatedAt']  =  date('Y-m-d H:i:s');
			     $this->general_model->insert_row('tbl_scheduled_invoice_payment', $schedule);
			}
          }
          
			
            }	
        } 
	}
	}
}