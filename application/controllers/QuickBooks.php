<?php

/**
 * QuickBooks Web Connector integration
 * 
 * This file servers as a controller which servers up .QWC configuration files, 
 * also also acts as the Web Connector SOAP endpoint. Download your .QWC file 
 * by visiting:
 * 	/config
 * 
 * The Web Connector will get pointed to this endpoint:
 * 	/quickbooks/qbwc
 * 
 * This particular example adds dummy customers to QuickBooks, but you could 
 * easily extend it to perform other operations on QuickBooks too. The final 
 * piece of this is just throwing things into the queue to be processed - for 
 * an example of that, see: _quickbooks_get_last_run
 * 	docs/example_web_connector_queueing.php
 * 

 *Omni Merchant controller for QuickBooks Web Connector integrations
 * 
 * 
 */
 
include_once APPPATH .'libraries/QBD_Sync.php';

class QuickBooks extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		// QuickBooks config
		$this->load->config('quickbooks');
			
		
    	$dsn ='mysqli://' . $this->db->username . ':' . $this->db->password . '@'.$this->db->hostname.  '/' . $this->db->database;
		define('QB_QUICKBOOKS_DSN', $dsn);
		define('QB_QUICKBOOKS_MAILTO', 'dilip.prajapati@chargezoom.com');

		$this->db1= $this->load->database('otherdb', TRUE);	
		$this->db->query("SET SESSION sql_mode = ''");
        $this->db1->query("SET SESSION sql_mode = ''");
	}
	
	function index(){
      redirect(base_url('home/company'));
    
    }
	
	
	
       function your_function(){
        $this->load->helper('download');
        $data = file_get_contents(FCPATH.'/uploads/'.$this->uri->segment(3)); // Read the file's contents
        $name = $this->uri->segment(3);
        force_download($name, $data);
       }
       
       
       	
	function populate_state()
    {
		$this->load->model('general_model');
        $id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->general_model->get_table_data('state', array('country_id'=>$id));
        echo json_encode($data);
    }

    function populate_city()
    {
		$this->load->model('general_model');
        $id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->general_model->get_table_data('city', array('state_id'=>$id));
        echo json_encode($data);
    }
    
	public function p($data){
		echo '<pre>';
		print_R($data);
		echo '</pre>';

	}

	/**
	 * Get Attribute from Particular Node
	 */
	public function getAttribute($element, $attr=null)
	{
		if($element->attributeExists($attr)){

			return $element->getAttribute($attr);
		}

		return false;
	}

	/**
	 * Get statusCode and statusMessage
	 */
	public function getStatusData($List, $tag=null)
	{
		// Fetch Attributes from the Node
		$statusCode = $this->getAttribute($List, 'statusCode');
		$statusMessage = $this->getAttribute($List, 'statusMessage');
		$statusSeverity = $this->getAttribute($List, 'statusSeverity');

		$requestID = $this->getAttribute($List, 'requestID');
		
		$return = [];
		if($statusCode !== false) {
			
			$return = ['tag' => $tag, 'requestID' => $requestID, 'statusCode' => $statusCode, 'statusSeverity' => $statusSeverity, 'statusMessage' => $statusMessage];
			
			
			// Save data
			
			$msg = $statusSeverity ? $statusSeverity.": ".$statusMessage : $statusMessage;

        	$this->db->where('quickbooks_queue_id', $requestID);
			$this->db->update('quickbooks_queue', ['qbdStatusCode' => $statusCode, 'qbdStatusMessage' => $msg ]);

		}	

		return json_encode($return);
	}

	/**
	 * Log input string
	 */
	public function logInputStream(){
		$log_data = file_get_contents('php://input');

		if($log_data != null){
			
			$fp = fopen('./uploads/qbd_log_'.date('Y_m_d').'.txt', 'a+');
			fwrite($fp, "\r\n".'-----'.date('c').'-----'."\r\n");
			fwrite($fp, $log_data);
			fclose($fp);

		}
	}

	/**
	 * Log Response code and message
	 */
	public function logResponseMessageCode($log_data){
		$fp = fopen('./uploads/qbd_response_log.txt', 'a+');
		
		fwrite($fp, "\r\n".'----- '.date('c').'-----'."\r\n");
		fwrite($fp, $log_data);

		fclose($fp);
	}
	
	/**********Create New Company and Edit **********************/
	
	
	public function create_new_company()
	{ 
	    
	    
	  
	 $da['login_info'] = $this->session->userdata('logged_in');
	  $this->load->model('general_model');
    if(!empty($this->input->post(null, true)))
    {
	 
	   $this->load->library('form_validation');
		// Displaying Errors In Div
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		// Validation For Name Field
		$this->form_validation->set_rules('companyName', 'company Name', 'required|min_length[3]');
		if($this->czsecurity->xssCleanPostInput('companyID')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!='' )
		{ 
		    	$this->form_validation->set_rules('qwbc_password', 'QBWC Password', 'required|min_length[5]');
		}
		
		if ($this->form_validation->run() == true)
		{
		    
			    $input_data['companyName']	   = $this->czsecurity->xssCleanPostInput('companyName');
			  
				
				
				if($this->czsecurity->xssCleanPostInput('companyID')!="" )
				{
				      
				         $id          = $this->czsecurity->xssCleanPostInput('companyID');
					   if( $this->czsecurity->xssCleanPostInput('qwbc_password')!="")
					   {
					 
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					 
					 }
						 $chk_condition = array('id'=>$id, 'companyName'=>$this->czsecurity->xssCleanPostInput('companyName'));
			         	$cmp_num  = $this->general_model->get_num_rows('tbl_company', $chk_condition);
						 if($cmp_num ==0)
						 {
						 $con         = array('id'=>$id);
						 $row_data    =  $this->general_model->get_row_data('tbl_company',$con);
						 $qb_username = $row_data['qbwc_username'];
						 
						 $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
						 $condition   = array('id'=>$id);
					     $this->general_model->update_row_data('tbl_company',$condition, $input_data);
					    $data['file_name'] =	 $this->config_file($c_name, $qb_username, $id);
					      }else{
					         
							  $condition   = array('id'=>$id);
					          $this->general_model->update_row_data('tbl_company',$condition, $input_data); 
							  $data['file_name'] =''; 
						  }
					 $data['status'] ='Success';
					 
			
					 $this->session->set_flashdata('success','Successfully Updated Company'); 
			    	
				}else{
				     $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
				    
				     $input_data['merchantID'] = $da['login_info']['merchID'];
				   
				     
					 if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					   $input_data['qbwc_username']   = $this->czsecurity->xssCleanPostInput('qwbc_username');
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					   $qb_username                   =  $this->czsecurity->xssCleanPostInput('qwbc_username'); 
					 }
					 
					  $companyID = $this->general_model->insert_row('tbl_company', $input_data);
					   if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					    $data['file_name'] =	$this->config_file($c_name, $qb_username, $companyID);
					  }
			             $this->session->set_flashdata('success','Successfully Created Company'); 
			     }
			     $data['status'] ='Success';
 			   
		      }else{
		          
		       
		            $this->session->set_flashdata('message','<div class="alert alert-danger">Error</div>'); 
		           $data['file_name'] =	'';
		            $data['status'] ='Error';	
		      }
		   }else{
		            $this->session->set_flashdata('message','<div class="alert alert-danger">Error</div>'); 
		            $data['file_name'] =	'';
		            $data['status']    ='Error';  
		   }
		   
		   
		  
		   echo json_encode($data); die;
		   

	}
	

	public function create_company()
	{ 
	 $data['login_info'] = $this->session->userdata('logged_in');
	  $this->load->model('general_model');
    if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		// Displaying Errors In Div
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		// Validation For Name Field
		$this->form_validation->set_rules('companyName', 'company Name', 'required|min_length[3]');
		if($this->czsecurity->xssCleanPostInput('companyID')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!='' )
		{ 
		    	$this->form_validation->set_rules('qwbc_password', 'QBWC Password', 'required|min_length[5]');
		}
		
		if ($this->form_validation->run() == true)
		{
			    $input_data['companyName']	   = $this->czsecurity->xssCleanPostInput('companyName');
			    if($this->czsecurity->xssCleanPostInput('companyAddress1')!="")
				$input_data['companyAddress1'] = $this->czsecurity->xssCleanPostInput('companyAddress1');
				if($this->czsecurity->xssCleanPostInput('companyEmail')!="")
				$input_data['companyEmail'] 	= $this->czsecurity->xssCleanPostInput('companyEmail');
				if($this->czsecurity->xssCleanPostInput('companyContact')!="")
				 $input_data['companyContact'] 	= $this->czsecurity->xssCleanPostInput('companyContact');
				 if($this->czsecurity->xssCleanPostInput('zipCode')!="")
				$input_data['zipCode']          = $this->czsecurity->xssCleanPostInput('zipCode');
				if($this->czsecurity->xssCleanPostInput('companyCountry')!="")
				$input_data['companyCountry']   = $this->czsecurity->xssCleanPostInput('companyCountry');
				if($this->czsecurity->xssCleanPostInput('companyState')!="")
				$input_data['companyState'] 	= $this->czsecurity->xssCleanPostInput('companyState');
				if($this->czsecurity->xssCleanPostInput('companyCity')!="")
				$input_data['companyCity'] 		= $this->czsecurity->xssCleanPostInput('companyCity');
				if( $this->czsecurity->xssCleanPostInput('companyAddress2')!="")
				$input_data['companyAddress2']  = $this->czsecurity->xssCleanPostInput('companyAddress2');
				if( $this->czsecurity->xssCleanPostInput('alternateContact')!="")
				$input_data['alternateContact'] = $this->czsecurity->xssCleanPostInput('alternateContact');
				if( $this->czsecurity->xssCleanPostInput('tagline')!="")
				$input_data['companyTagline'] 	= $this->czsecurity->xssCleanPostInput('tagline');
				if( $this->czsecurity->xssCleanPostInput('companyFullAddress')!="")
		     	$input_data['companyFullAddress']= $this->czsecurity->xssCleanPostInput('companyFullAddress');
				
				
				if($this->czsecurity->xssCleanPostInput('companyID')!="" ){
				      
				         $id          = $this->czsecurity->xssCleanPostInput('companyID');
					   if( $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					 
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					 
					 }
						 $chk_condition = array('id'=>$id, 'companyName'=>$this->czsecurity->xssCleanPostInput('companyName'));
						 if($this->general_model->get_num_rows('tbl_company', $chk_condition) ==0){
						 $con         = array('id'=>$id);
						 $row_data    =  $this->general_model->get_row_data('tbl_company',$con);
						 $qb_username = $row_data['qbwc_username'];
						 $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
						 $condition   = array('id'=>$id);
					     $this->general_model->update_row_data('tbl_company',$condition, $input_data);
						 $this->config_file($c_name, $qb_username, $id);
					      }else{
							  $condition   = array('id'=>$id);
					          $this->general_model->update_row_data('tbl_company',$condition, $input_data); 
							  
						  }
					 
			    	
				}else{
				     $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
				    
				     $input_data['merchantID'] = $data['login_info']['merchID'];
				   
				     
					 if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					   $input_data['qbwc_username']   = $this->czsecurity->xssCleanPostInput('qwbc_username');
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					   $qb_username                   =  $this->czsecurity->xssCleanPostInput('qwbc_username'); 
					 }
					 
					  $companyID = $this->general_model->insert_row('tbl_company', $input_data);
					   if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					   $this->config_file($c_name, $qb_username, $companyID);
					  }
			
			     }
			
 			     redirect(base_url('home/company'));
		      }
		   }
		   
		  if($this->uri->segment('3')){
		      $companyID  			  = $this->uri->segment('3');	
    	      $condition              = array('id'=>$companyID);  
			 $data['company']	      = $this->general_model->get_row_data('tbl_company', $condition);	
	      }
		  
		   
	
				$data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] = $this->session->userdata('logged_in');
				$user_id = $data['login_info']['merchID'];
				
				  $username  = $this->config->item('quickbooks_user');		
      		      $randomNum =substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 5);
	              $data['qb_username']  = $username.$randomNum;                         
		        
		        $country = $this->general_model->get_table_data('country','');
				$data['country_datas'] = $country;
				
				$state = $this->general_model->get_table_data('state','');
				$data['state_datas'] = $state;
				
				$city = $this->general_model->get_table_data('city','');
				$data['city_datas'] = $city;
				
		        
		        
				$this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/create_new_company', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	}
	
	/*******************************Create .QWC FILE ***********************/
		public function create_crm()
	{ 
	 $data['login_info'] = $this->session->userdata('logged_in');
	  $this->load->model('general_model');
    if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		// Displaying Errors In Div
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		// Validation For Name Field
		$this->form_validation->set_rules('companyName', 'company Name', 'required|min_length[3]');
		if($this->czsecurity->xssCleanPostInput('companyID')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!='' )
		{ 
		    	$this->form_validation->set_rules('qwbc_password', 'QBWC Password', 'required|min_length[5]');
		}
		
		if ($this->form_validation->run() == true)
		{
			    $input_data['companyName']	   = $this->czsecurity->xssCleanPostInput('companyName');
			    if($this->czsecurity->xssCleanPostInput('companyAddress1')!="")
				$input_data['companyAddress1'] = $this->czsecurity->xssCleanPostInput('companyAddress1');
				if($this->czsecurity->xssCleanPostInput('companyEmail')!="")
				$input_data['companyEmail'] 	= $this->czsecurity->xssCleanPostInput('companyEmail');
				if($this->czsecurity->xssCleanPostInput('companyContact')!="")
				 $input_data['companyContact'] 	= $this->czsecurity->xssCleanPostInput('companyContact');
				 if($this->czsecurity->xssCleanPostInput('zipCode')!="")
				$input_data['zipCode']          = $this->czsecurity->xssCleanPostInput('zipCode');
				if($this->czsecurity->xssCleanPostInput('companyCountry')!="")
				$input_data['companyCountry']   = $this->czsecurity->xssCleanPostInput('companyCountry');
				if($this->czsecurity->xssCleanPostInput('companyState')!="")
				$input_data['companyState'] 	= $this->czsecurity->xssCleanPostInput('companyState');
				if($this->czsecurity->xssCleanPostInput('companyCity')!="")
				$input_data['companyCity'] 		= $this->czsecurity->xssCleanPostInput('companyCity');
				if( $this->czsecurity->xssCleanPostInput('companyAddress2')!="")
				$input_data['companyAddress2']  = $this->czsecurity->xssCleanPostInput('companyAddress2');
				if( $this->czsecurity->xssCleanPostInput('alternateContact')!="")
				$input_data['alternateContact'] = $this->czsecurity->xssCleanPostInput('alternateContact');
				if( $this->czsecurity->xssCleanPostInput('tagline')!="")
				$input_data['companyTagline'] 	= $this->czsecurity->xssCleanPostInput('tagline');
				if( $this->czsecurity->xssCleanPostInput('companyFullAddress')!="")
		     	$input_data['companyFullAddress']= $this->czsecurity->xssCleanPostInput('companyFullAddress');
				
				
				if($this->czsecurity->xssCleanPostInput('companyID')!="" ){
				      
				         $id          = $this->czsecurity->xssCleanPostInput('companyID');
					   if( $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					 
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					 
					 }
						 $chk_condition = array('id'=>$id, 'companyName'=>$this->czsecurity->xssCleanPostInput('companyName'));
						 if($this->general_model->get_num_rows('tbl_company', $chk_condition) ==0){
						 $con         = array('id'=>$id);
						 $row_data    =  $this->general_model->get_row_data('tbl_company',$con);
						 $qb_username = $row_data['qbwc_username'];
						 $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
						 $condition   = array('id'=>$id);
					     $this->general_model->update_row_data('tbl_company',$condition, $input_data);
						 $this->config_file($c_name, $qb_username, $id);
					      }else{
							  $condition   = array('id'=>$id);
					          $this->general_model->update_row_data('tbl_company',$condition, $input_data); 
							  
						  }
					 
			    	
				}else{
				     $c_name      = $this->czsecurity->xssCleanPostInput('companyName');
				    
				     $input_data['merchantID'] = $data['login_info']['merchID'];
				   
				     
					 if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					   $input_data['qbwc_username']   = $this->czsecurity->xssCleanPostInput('qwbc_username');
					   $input_data['qbwc_password']   = $this->czsecurity->xssCleanPostInput('qwbc_password');
					   $qb_username                   =  $this->czsecurity->xssCleanPostInput('qwbc_username'); 
					 }
					 
					  $companyID = $this->general_model->insert_row('tbl_company', $input_data);
					   if($this->czsecurity->xssCleanPostInput('qwbc_username')!="" && $this->czsecurity->xssCleanPostInput('qwbc_password')!=""){
					   $this->config_file($c_name, $qb_username, $companyID);
					  }
			
			     }
			
 			     redirect(base_url('home/company'));
		      }
		   }
		   
		  if($this->uri->segment('3')){
		      $companyID  			  = $this->uri->segment('3');	
    	      $condition              = array('id'=>$companyID);  
			 $data['company']	      = $this->general_model->get_row_data('tbl_company', $condition);	
	      }
		  
		   
	
				$data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] = $this->session->userdata('logged_in');
				$user_id = $data['login_info']['merchID'];
				
				  $username  = $this->config->item('quickbooks_user');		
      		      $randomNum =substr(str_shuffle("0123456789ABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 5);
	              $data['qb_username']  = $username.$randomNum;                         
		        
		        $country = $this->general_model->get_table_data('country','');
				$data['country_datas'] = $country;
				
				$state = $this->general_model->get_table_data('state','');
				$data['state_datas'] = $state;
				
				$city = $this->general_model->get_table_data('city','');
				$data['city_datas'] = $city;
				
		        
		        
				$this->load->view('template/template_start', $data);
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/create_new_company', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
	}
	
	public function QB_firstlogin()
	{
	        
	          if($this->session->userdata('logged_in')['merchID']!="")
	          {
    	         $data['primary_nav']  = primary_nav();
    			 $data['template']   = template_variable();
    			 $this->load->model('general_model');
    			  $merchantID  = $this->session->userdata('logged_in')['merchID'];
    			  $condition = array('merchID'=> $merchantID); 
    		   	 
    		   	  $this->general_model->update_row_data('tbl_merchant_data',$condition,array('is_integrate'=>1));
    		   	  $sess_array = $this->session->userdata('logged_in');
    		   	   $sess_array['is_integrate'] = '1';
				   $this->session->set_userdata('logged_in', $sess_array);
				   
				   $gatew                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchantID));
					   if (!empty($gatew)) {
						$merchantID  = $this->session->userdata('logged_in')['merchID'];
						$condition = array('merchID'=> $merchantID); 
						  
						   $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
						   $sess_array = $this->session->userdata('logged_in');
							$sess_array['firstLogin'] = '1';
						 $this->session->set_userdata('logged_in', $sess_array);  
						 
						 if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchantID)) == '0')
						 {  
							   
							   $fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];
							  
								 $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
							  
								foreach($templatedatas as $templatedata)
								{
								 $insert_data = array('templateName'  =>$templatedata['templateName'],
										  'templateType'    => $templatedata['templateType'],
										   'merchantID'      => $merchantID,
										   'fromEmail'      => DEFAULT_FROM_EMAIL,//$fromEmail,
										   'message'		  => $templatedata['message'],
										   'emailSubject'   => $templatedata['emailSubject'],
										   'createdAt'      => date('Y-m-d H:i:s') 	
									   );
								   $this->general_model->insert_row('tbl_email_template', $insert_data);
							  
							  }
						  }
						 
						 
						 
						   redirect(base_url('home/index'));
					   }else{
						redirect(base_url('firstlogin/dashboard_first_login'));
					   }

    		
    		
    			  
	          }
	          else
	          {
	              redirect('login','refresh');
	          }
 
	}
	 
	public function config_file($name,$username,$companyId)
	{
	    
	 
	    @ob_start();
	    
	    
	    $name   = $name; 
		$descrip = 'PayPortal';		// A description of your server 

    	$appurl = 'https://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/qbwc';		// This *must* be httpS:// (path to your QuickBooks SOAP server)
		$appsupport = $appurl; 		// This *must* be httpS:// and the domain name must match the domain name above
		$fileid = QuickBooks_WebConnector_QWC::fileID();		// Just make this up, but make sure it keeps that format
		$ownerid = QuickBooks_WebConnector_QWC::ownerID();		// Just make this up, but make sure it keeps that format
		$this->db->query("update tbl_company set fileID = '".$fileid."' where  id='".$companyId."' ");
        $qbtype = QUICKBOOKS_TYPE_QBFS;	
		$readonly = false; 
        // Run every 600 seconds (10 minutes)
		$run_every_n_seconds = 600; 
        $username  = $username;
	     // Generate the XML file
		$QWC = new QuickBooks_WebConnector_QWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);
		$xml = $QWC->generate();

		// Send as a file download
		     $this->load->helper('file');
             
	     	 $file = "my-quickbooks-wc-file$username.qwc";  

		if ( ! write_file(FCPATH."uploads/$file", $xml,'w+'))
			{
					echo 'Error';  die;
			}
				else
			{
			     return $file; 
			}  
	
		 
		  return exit;
	}
	
		
	
	
	 
	public function config()
	{
		$this->load->model('general_model');	
		$descrip = 'PayPortal';		// A description of your server 

		$appurl = 'https://' . $_SERVER['HTTP_HOST'] . dirname($_SERVER['REQUEST_URI']) . '/qbwc';		// This *must* be httpS:// (path to your QuickBooks SOAP server)
		$appsupport = $appurl; 		// This *must* be httpS:// and the domain name must match the domain name above

		
		
	    $username  = $this->czsecurity->xssCleanPostInput('qbusername'); 	// This is the username you stored in the 'quickbooks_user' table by using QuickBooks_Utilities::createUser()
	    $name      = $this->czsecurity->xssCleanPostInput('qbcompany_name'); 	// A name for your server (make it whatever you want)
		$password  = $this->czsecurity->xssCleanPostInput('qbpassword');
		
		$fileid = QuickBooks_WebConnector_QWC::fileID();		// Just make this up, but make sure it keeps that format
		$ownerid = QuickBooks_WebConnector_QWC::ownerID();		// Just make this up, but make sure it keeps that format
		$merchId = $this->session->userdata('logged_in')['merchID'];
	
	$this->db->query("insert into tbl_company set fileID = '".$fileid."', qbwc_username='".$username."', companyName='".$name."', qbwc_password='".$password."', merchantID='".$merchId."', date_added='".date("Y-m-d H:i:s")."' ");
	      
	        
	    $qb_data['appIntegration']    = "2";
		$qb_data['merchantID']    = $merchId;
		$chk_condition = array('merchantID'=>$merchId);
		$app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);	  
		 if(!empty($app_integration)){
			
			    $this->general_model->update_row_data('app_integration_setting',$chk_condition, $qb_data);
			    $user = $this->session->userdata('logged_in');
				$user['active_app'] = '2';
				$this->session->set_userdata('logged_in',$user);
			   }
			   else
				{
					 $this->general_model->insert_row('app_integration_setting', $qb_data);
					 $user = $this->session->userdata('logged_in');
				$user['active_app'] = '2';
				$this->session->set_userdata('logged_in',$user);
				  
				}
	        
	        

		$qbtype = QUICKBOOKS_TYPE_QBFS;	

		$readonly = false; 
        // Run every 600 seconds (10 minutes)
		$run_every_n_seconds = 600; 

		// Generate the XML file
		$QWC = new QuickBooks_WebConnector_QWC($name, $descrip, $appurl, $appsupport, $username, $fileid, $ownerid, $qbtype, $readonly, $run_every_n_seconds);
		$xml = $QWC->generate();

		// Send as a file download
		     $this->load->helper('file');
             
	     	 $file = "my-quickbooks-wc-file$username.qwc";  

			if ( ! write_file(FCPATH."uploads/$file", $xml,'w+'))
			{
					echo 'Error ';  die;
			}
			else
			{
					$condition = array('merchID'=>$merchId);

					$this->general_model->update_row_data('tbl_merchant_data', $condition, array('is_integrate'=>1));
					
					header('Content-type: text/xml');
                	header('Content-Disposition: attachment; filename="'.$file.'"');
			}
	
		  print($xml);
	      exit;
	}
	
	
	
	
	
	/**
	 * SOAP endpoint for the Web Connector to connect to
	 */
	public function qbwc()
	{
		$this->logInputStream();
		
	
		  $today = date('Y-m-d');
		  $this->db->query("delete from quickbooks_user where qb_username ='' ");
    		$this->db->query("delete from quickbooks_log where DATE_FORMAT(log_datetime, '%Y-%m-%d') < $today ");
		// Memory limit
		ini_set('memory_limit', $this->config->item('quickbooks_memorylimit'));
		
		// We need to make sure the correct timezone is set, or some PHP installations will complain
		if (function_exists('date_default_timezone_set'))
		{
			// * MAKE SURE YOU SET THIS TO THE CORRECT TIMEZONE! *
			// List of valid timezones is here: http://us3.php.net/manual/en/timezones.php
			date_default_timezone_set($this->config->item('quickbooks_tz'));
		}
				
		// Map QuickBooks actions to handler functions
		$map = array( 
		   
		   
		   
		 	QUICKBOOKS_ADD_ACCOUNT => array( array($this, '_quickbooks_account_create_request'), array($this, '_quickbooks_account_create_response') ) ,
			QUICKBOOKS_QUERY_ACCOUNT => array( array($this, '_quickbooks_account_import_request'), array($this, '_quickbooks_account_import_response') ) ,
			QUICKBOOKS_QUERY_VENDOR=>array(array( $this, '_quickbooks_vendor_query_request' ), array( $this, '_quickbooks_vendor_query_response' )),
				QUICKBOOKS_ADD_SALESTAXITEM=>array(array( $this, '_add_sales_tax_item_request' ), array( $this, '_add_sales_tax_item_response' )),
				QUICKBOOKS_MOD_CUSTOMER => array( array( $this, '_updateCustomerRequest' ), array( $this, '_updateCustomerResponse' ) ),
				QUICKBOOKS_MOD_INVOICE=>array(array( $this, '_update_invoice_request' ), array( $this, '_update_invoice_response' )),
             QUICKBOOKS_QUERY_INVOICE => array(  array($this,'_quickbooks_invoice_query_request'),  array($this, '_quickbooks_invoice_query_response' )),  
             QUICKBOOKS_ADD_CREDITCARDREFUND => array( array( $this, '_add_credit_card_refund_request' ), array( $this, '_add_credit_card_refund_response' ) ),
		      QUICKBOOKS_ADD_CREDITMEMO => array( array( $this, '_addCustomerCredit_request' ), array( $this, '_addCustomerCredit_response' ) ),
		      QUICKBOOKS_ADD_NONINVENTORYITEM=>array(array( $this, '_add_noninventory_item_request' ), array( $this, '_add_noninventory_item_response' )),
		      QUICKBOOKS_MOD_NONINVENTORYITEM=>array(array( $this, '_update_noninventory_item_request' ), array( $this, '_update_noninventory_item_response' )),
		      QUICKBOOKS_ADD_SERVICEITEM=>array(array( $this, '_add_service_item_request' ), array( $this, '_add_service_item_response' )),
              QUICKBOOKS_MOD_SERVICEITEM=>array(array( $this, '_update_service_item_request' ), array( $this, '_update_service_item_response' )),
              QUICKBOOKS_ADD_DISCOUNTITEM=>array(array( $this, '_add_item_request_discount' ), array( $this, '_add_item_response_discount' )),
              QUICKBOOKS_MOD_DISCOUNTITEM=>array(array( $this, '_update_item_request_discount' ), array( $this, '_update_item_response_discount' )),
              QUICKBOOKS_ADD_OTHERCHARGEITEM=>array(array( $this, '_add_item_othercharge_request' ), array( $this, '_add_item_othercharge_response' )),
              QUICKBOOKS_MOD_OTHERCHARGEITEM=>array(array( $this, '_update_item_othercharge_request' ), array( $this, '_update_item_othercharge_response' )),
              QUICKBOOKS_ADD_SUBTOTALITEM=>array(array( $this, '_add_item_subtotal_request' ), array( $this, '_add_item_subtotal_response' )),
              QUICKBOOKS_MOD_SUBTOTALITEM=>array(array( $this, '_update_item_subtotal_request' ), array( $this, '_update_item_subtotal_response' )),
              QUICKBOOKS_ADD_PAYMENTITEM=>array(array( $this, '_add_item_payment_request' ), array( $this, '_add_item_payment_response' )),
              QUICKBOOKS_MOD_PAYMENTITEM=>array(array( $this, '_update_item_payment_request' ), array( $this, '_update_item_payment_response' )),
              QUICKBOOKS_ADD_GROUPITEM=>array(array( $this, '_add_item_group_request' ), array( $this, '_add_item_group_response' )),
              QUICKBOOKS_MOD_GROUPITEM=>array(array( $this, '_update_item_group_request' ), array( $this, '_update_item_group_response' )),
        
		     
		      QUICKBOOKS_ADD_INVOICE=>array(array( $this, '_addInvoiceRequest' ), array( $this, '_addInvoiceResponse' )),
              QUICKBOOKS_DELETE_TXN => array( array($this, '_deleteInvoiceRequest'), array($this, '_deleteInvoiceResponse') ) ,
              
	          QUICKBOOKS_ADD_CUSTOMER => array( array( $this, '_addCustomerRequest' ), array( $this, '_addCustomerResponse' ) ),
			 
	          QUICKBOOKS_ADD_RECEIVEPAYMENT => array( array( $this, '_addPaymentRequest' ), array( $this, '_addPaymentResponse' ) ),     
		   
	          
			  QUICKBOOKS_IMPORT_CUSTOMER => array( array($this,'_quickbooks_customer_import_request'), array($this, '_quickbooks_customer_import_response') ), 
             QUICKBOOKS_IMPORT_INVOICE => array(  array($this,'_quickbooks_invoice_import_request'),  array($this, '_quickbooks_invoice_import_response' )),  
		     QUICKBOOKS_IMPORT_ITEM => array( array($this, '_quickbooks_item_import_request'), array($this, '_quickbooks_item_import_response') ) ,
             QUICKBOOKS_IMPORT_ACCOUNT => array( array($this, '_quickbooks_account_import_request'), array($this, '_quickbooks_account_import_response') ) ,
			 QUICKBOOKS_IMPORT_CREDITMEMO => array( array( $this, '_quickbooks_credit_import_request' ), array( $this, '_quickbooks_credit_import_response' ) ),
			 QUICKBOOKS_IMPORT_SALESTAXITEM => array( array( $this, '_quickbooks_sales_tax_item_import_request' ), array( $this, '_quickbooks_sales_tax_item_import_response' ) ),
			
			QUICKBOOKS_IMPORT_VENDOR=>array(array( $this, '_quickbooks_vendor_import_request' ), array( $this, '_quickbooks_vendor_import_response' )),
       
	
	     
	);
			
		
		
		
		
		// Catch all errors that QuickBooks throws with this function 
		$errmap = array(
		500 => array($this,'_quickbooks_error_e500_notfound'), 			// Catch errors caused by searching for things not present in QuickBooks
		1 => array($this, '_quickbooks_error_e500_notfound'), 
		'*' => array($this,'_quickbooks_error_catchall'), 				// Catch any other errors that might occur
		);
		
		// Call this method whenever the Web Connector connects
		$hooks = array(
        	QuickBooks_WebConnector_Handlers::HOOK_LOGINSUCCESS => array( array( $this, '_loginSuccess' ) ) , 	// call this whenever a successful login occurs
        	
        	);

		// An array of callback options
		$callback_options = array();
		
		// Logging level
		$log_level = $this->config->item('quickbooks_loglevel');
		
		// What SOAP server you're using 
		
		$soapserver = QUICKBOOKS_SOAPSERVER_BUILTIN;		// A pure-PHP SOAP server (no PHP ext/soap extension required, also makes debugging easier)
		
		$soap_options = array(	
			);
		
		$handler_options = array(
		     'authenticate' => array($this,'_quickbooks_custom_auth'), 
			'deny_concurrent_logins' => false, 
		   'deny_reallyfast_logins' => false, 
			);	
			
			
			// See the comments in the QuickBooks/Server/Handlers.php file
		
		$driver_options = array(		// See the comments in the QuickBooks/Driver/<YOUR DRIVER HERE>.php file ( i.e. 'Mysql.php', etc. )
			'max_log_history' => 320000,	// Limit the number of quickbooks_log entries to 1024
			'max_queue_history' => 10240, 	// Limit the number of *successfully processed* quickbooks_queue entries to 64
		);
		
		// Build the database connection string
		
		
	    	$dsn = QB_QUICKBOOKS_DSN;
		
		// Check to make sure our database is set up 
		if (!QuickBooks_Utilities::initialized($dsn))
		{
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			
			// This creates a username and password which is used by the Web Connector to authenticate
			
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}
		
		// Set up our queue singleton
		QuickBooks_WebConnector_Queue_Singleton::initialize($dsn);
		
		// Create a new server and tell it to handle the requests
		// __construct($dsn_or_conn, $map, $errmap = array(), $hooks = array(), $log_level = QUICKBOOKS_LOG_NORMAL, $soap = QUICKBOOKS_SOAPSERVER_PHP, $wsdl = QUICKBOOKS_WSDL, $soap_options = array(), $handler_options = array(), $driver_options = array(), $callback_options = array()
	
		$Server = new QuickBooks_WebConnector_Server($dsn, $map, $errmap, $hooks, $log_level, $soapserver, QUICKBOOKS_WSDL, $soap_options, $handler_options, $driver_options, $callback_options);
	  
	 
		$response = $Server->handle(true, true);		
		
		
		
	}
	
	
     
    function _quickbooks_custom_auth($username, $password, &$qb_company_file)
    {
    	
    	        $con = array('qbwc_username'=>$username);
    	  
    	 	    $query = $this->db->select('*')->from('tbl_company')->where($con)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query->num_rows()>0 ) {
    				$user = $query->row_array();
    			    $user1 = $user['qbwc_username'];
    			    $company = $user['companyName'];
    			 $qb_company_file_name = $company.'.qbw';
    			} else {
    				return false;
    			}				
    	  
    	
   
    	
    	
    		if ($username == $user1 and $password ==  $user['qbwc_password'])
    	{
    	// Use this company file and auth successfully
    	
    		return true;
    	}
    	
    	// Login failure
    	return false;
    }
   


  public function _add_sales_tax_item_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
		$xml='';	 
			 
		  $query  = 	$this->db->query("SELECT * from tbl_custom_tax  where txID='".$ID."'  ");
        if($query->num_rows > 0)
        {
		  $data   = 	$query->row_array();
			
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemSalesTaxAddRq requestID="'.$requestID.'">
					<ItemSalesTaxAdd> 
				    <Name >'.$data['taxName'].'</Name> 
                   <ItemDesc >'.$data['taxDescription'].'</ItemDesc>
                   <TaxRate >'.$data['taxRate'].'</TaxRate> 
					
                    
                    <TaxVendorRef>

                      <ListID >'.$data['vendorID'].'</ListID>
                  </TaxVendorRef>
					</ItemSalesTaxAdd>
					</ItemSalesTaxAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
        }
				return $this->_formatXMLRequest($xml);  
   }
   public function _quickbooks_sales_tax_item_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
{
	
	// Iterator support (break the result set into small chunks)
	$attr_iteratorID = '';
	$attr_iterator = ' iterator="Start" ';
	if (empty($extra['iteratorID']))
	{
		// This is the first request in a new batch
		$last = $this->_quickbooks_get_last_run($user, $action);
		$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
		
		// Set the current run to $last
		$this->_quickbooks_set_current_run($user, $action, $last);
	}
	else
	{
		// This is a continuation of a batch
		$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
		$attr_iterator = ' iterator="Continue" ';
		
		$last = $this->_quickbooks_get_current_run($user, $action);
	}
	$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="' . $version . '"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemSalesTaxQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
					<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					<FromModifiedDate>' . $last . '</FromModifiedDate>
					<OwnerID>0</OwnerID>
				</ItemSalesTaxQueryRq>	
			</QBXMLMsgsRq>
		</QBXML>';
		
	return $xml; 
}		
public function _quickbooks_sales_tax_item_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
{	
	
	if (!empty($idents['iteratorRemainingCount']))
	{
		// Queue up another request
		
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		$Queue->enqueue(QUICKBOOKS_IMPORT_SALESTAXITEM, null, 2, array( 'iteratorID' => $idents['iteratorID'] ),$user);
	}

	
	// Import all of the records
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	
	if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemSalesTaxQueryRs');
			
			$this->getStatusData($List, 'ItemSalesTaxQuery');
	 
           	foreach ($List->children() as $Item)
			{
            
              $listID  = $Item->getChildDataAt('ItemSalesTaxRet ListID');
             $compID   = $this->db->select('id')->from('tbl_company')->where(array('qbwc_username'=>$user))->get()->row_array()['id'];
           
            $tax_data =array( 
            'friendlyName'=>$Item->getChildDataAt('ItemSalesTaxRet Name'),
            'taxStatus'=>$Item->getChildDataAt('ItemSalesTaxRet IsActive'),
            'taxRate'=>$Item->getChildDataAt('ItemSalesTaxRet TaxRate'),
            'companyID'=>$compID,
            'taxDescription'=>$Item->getChildDataAt('ItemSalesTaxRet ItemDesc'),
           
            'taxListID'=>$listID,
            'CreatedTime'=>date('Y-m-d H:i:s',strtotime($Item->getChildDataAt('ItemSalesTaxRet TimeCreated'))),
            'TimeModified'=>date('Y-m-d H:i:s',strtotime($Item->getChildDataAt('ItemSalesTaxRet TimeModified'))),
             'EditSequence'=>$Item->getChildDataAt('ItemSalesTaxRet EditSequence')
			);
			
			$qr = $this->db->query('Select * from tbl_custom_tax where 	txID="'.$ID.'" ');
			if($qr->num_rows() >0)
			{  
				$tx_data=$qr->row_array();
				 $qr1 = $this->db->query('Select * from tbl_taxes where taxListID="'.$tx_data['taxListID'].'" and companyID="'.$compID.'" ');
				 if($qr1->num_rows() >0)
				{
					$taxListWhere = [
						'taxListID' => $tx_data['taxListID'],
						'companyID' => $compID
					];  
					 $this->db->where($taxListWhere);
					$this->db->update('tbl_taxes',$tax_data);
					$this->db->where(array('txID'=>$ID));
					$this->db->update('tbl_custom_tax', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s'))); 
				}else{
					$qr2 = $this->db->query('Select * from tbl_taxes where taxListID="'.$listID.'" and companyID="'.$compID.'" ');
					if($qr2->num_rows() >0)
					{  
						$taxListWhere = [
							'taxListID' => $listID,
							'companyID' => $compID
						];  
						$this->db->where($taxListWhere);
						$this->db->update('tbl_taxes',$tax_data);
					}else{
						$this->db->insert('tbl_taxes',$tax_data);
					}
					   
				}
			}else{
				$qr2 = $this->db->query('Select * from tbl_taxes where taxListID="'.$listID.'" and companyID="'.$compID.'" ');
				if($qr2->num_rows() > 0){
					$taxListWhere = [
						'taxListID' => $listID,
						'companyID' => $compID
					];  
					$this->db->where($taxListWhere);
						$this->db->update('tbl_taxes',$tax_data);
				} else {
					$this->db->insert('tbl_taxes',$tax_data);
				}
			}
		 
		  
		   
				 
    	    
			}  
		
		}
		return true;
}

	public function _add_sales_tax_item_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemSalesTaxAddRs');

			$this->getStatusData($List, 'ItemSalesTaxAdd');
        
           	foreach ($List->children() as $Item)
			{
            
              $listID  = $Item->getChildDataAt('ItemSalesTaxRet ListID');
             $compID   = $this->db->select('id')->from('tbl_company')->where(array('qbwc_username'=>$user))->get()->row_array()['id'];
            
            $tax_data =array( 
            'friendlyName'=>$Item->getChildDataAt('ItemSalesTaxRet Name'),
            'taxStatus'=>$Item->getChildDataAt('ItemSalesTaxRet IsActive'),
            'taxRate'=>$Item->getChildDataAt('ItemSalesTaxRet TaxRate'),
            'companyID'=>$compID,
            'taxDescription'=>$Item->getChildDataAt('ItemSalesTaxRet ItemDesc'),
           
            'taxListID'=>$listID,
            'CreatedTime'=>date('Y-m-d H:i:s',strtotime($Item->getChildDataAt('ItemSalesTaxRet TimeCreated'))),
            'TimeModified'=>date('Y-m-d H:i:s',strtotime($Item->getChildDataAt('ItemSalesTaxRet TimeModified'))),
             'EditSequence'=>$Item->getChildDataAt('ItemSalesTaxRet EditSequence')
            );
             $qr = $this->db->query('Select * from tbl_custom_tax where 	txID="'.$ID.'" ');
             if($qr->num_rows() >0)
             {  
                 $tx_data=$qr->row_array();
				 $qr1 = $this->db->query('Select * from tbl_taxes where taxListID="'.$tx_data['taxListID'].'" and companyID="'.$compID.'" ');
                  if($qr1->num_rows() >0)
                 {  
					 $taxListWhere = [
						'taxListID' => $tx_data['taxListID'],
						'companyID' => $compID
					];  
					$this->db->where($taxListWhere);
					 $this->db->update('tbl_taxes',$tax_data);  
                 }else{
                     
                      $this->db->insert('tbl_taxes',$tax_data);  
                 }
             }
          
    	    $this->db->where(array('txID'=>$ID));
            $this->db->update('tbl_custom_tax', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}  
		
		}
    	
    
    
		return true; 
	}


 
  public function _add_credit_card_refund_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
		$xml='';	 
			 
		  $query  = 	$this->db->query("SELECT rf.*,qb.FullName from tbl_customer_refund_transaction rf inner join qb_test_customer qb on qb.ListID=rf.refundCustomerID  where rf.refundID='".$ID."'  ");
        if($query->num_rows > 0)
        {
		  $data   = 	$query->row_array();
		  
			
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ARRefundCreditCardAddRq requestID="'.$requestID.'">
					<ARRefundCreditCardAdd> 
					<CustomerRef>
                          <ListID >'.$data['refundCustomerID'].'</ListID>
                         <FullName >'.$data['FullName'].'</FullName> 
                     </CustomerRef>
					<RefundFromAccountRef> 
                    <ListID >'.$data['AccountRef'].'</ListID>
                    <FullName >'.$data['AccountName'].'</FullName> 
                    </RefundFromAccountRef>
				
					<TxnDate >'.date('Y-m-d').'</TxnDate>
					<PaymentMethodRef>';
            if($data['paymentMethod']!="")
                      $xml.='<ListID >'.$data['paymentMethod'].'</ListID>';
        
                      $xml.='<FullName >'.$data['paymentMethodName'].'</FullName> 
                    </PaymentMethodRef>
                    <RefundAppliedToTxnAdd>
                    <TxnID useMacro="MACROTYPE">'.$data['creditTxnID'].'</TxnID> 
                    <RefundAmount>'.$data['refundAmount'].'</RefundAmount> 
                    </RefundAppliedToTxnAdd>
					</ARRefundCreditCardAdd>
					
					</ARRefundCreditCardAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
		
        }
		return $this->_formatXMLRequest($xml); 
   }				
	
	
	public function _add_credit_card_refund_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ARRefundCreditCardAddRs');

			$this->getStatusData($List, 'ARRefundCreditCardAdd');
		
		}
    	
    
    
		return true; 
	}




 
  public function _addCustomerCredit_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
		  $query  = 	$this->db->query("Select cr.*, qb.FullName as Name from tbl_custom_credit cr inner join qb_test_customer qb  on qb.ListID=cr.customerID   where cr.crID='".$ID."'  ");
		  $data   = 	$query->row_array();
		  
		   $query1  = 	$this->db->query("Select cr1.*, qb1.Name, qb1.Type, qb1.SalesDesc  from tbl_credit_item cr1 inner join qb_test_item qb1  on cr1.itemListID=qb1.ListID  where cr1.crlineID='".$ID."'  ");
		   $data1   = 	$query1->result_array();
		  
			
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<CreditMemoAddRq requestID="'.$requestID.'">
					<CreditMemoAdd> 
					<CustomerRef>
                     <ListID >'.$data['customerID'].'</ListID>
                        <FullName >'.$data['Name'].'</FullName> 
                     </CustomerRef>
					<TxnDate >'.date('Y-m-d', strtotime($data['creditDate'])).'</TxnDate>
                    <RefNumber >'.$data['creditNumber'].'</RefNumber> 
					
                    <Memo >'.$data['creditMemo'].'</Memo>'; 
				
					foreach($data1 as $dat)
					{
					   
					    if($dat['Type']=='Payment')
					    {
					        	$amount = $amount= sprintf('%0.2f', $dat['itemPrice']);  
					        	$xml.='<CreditMemoLineAdd>
					<ItemRef>
					<ListID >'.$dat['itemListID'].'</ListID> 
					<FullName >'.$dat['Name'].'</FullName>
					</ItemRef>
                    <Desc >'.$dat['SalesDesc'].'</Desc> 
					
                   <Rate >'.$dat['itemPrice'].'</Rate>

                   <Amount >'.$amount.'</Amount>
					
					</CreditMemoLineAdd>';
					
					        
					    }else{
					      	$amount = $amount= sprintf('%0.2f', $dat['itemPrice']*$dat['itemQuantity']);  
					   
				
					$xml.='<CreditMemoLineAdd>
					<ItemRef>
					<ListID >'.$dat['itemListID'].'</ListID> 
					<FullName >'.$dat['Name'].'</FullName>
					</ItemRef>
                    <Desc >'.$dat['SalesDesc'].'</Desc> 
					<Quantity >'.$dat['itemQuantity'].'</Quantity> 
                   <Rate >'.$dat['itemPrice'].'</Rate>

                   <Amount >'.$amount.'</Amount>
					
					</CreditMemoLineAdd>';
					
					}
					}
					$xml.='</CreditMemoAdd>
					
					</CreditMemoAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
			
				
		return $this->_formatXMLRequest($xml); 
   }				
	
	
	public function _addCustomerCredit_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CreditMemoAddRs');

			$this->getStatusData($List, 'CreditMemoAdd');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			foreach ($List->children() as $Item)
			{
            
              $txnID = $Item->getChildDataAt('CreditMemoRet TxnID');
              
              
              $cr_data = $this->general_model->get_select_data('tbl_custom_credit',array('Type'),array('crID'=>$ID));
              
              if(!empty($cr_data['Type']=='Payment'))
              {
                 $ref_data = $this->general_model->get_row_data('tbl_customer_refund_transaction',array('creditTxnID'=>$ID));
                 
                 	$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
	             	$Queue->enqueue(QUICKBOOKS_ADD_CREDITCARDREFUND,$ref_data['refundID'] , '1' ,'', $user);
                
                  
              }
					
			 $this->db->where(array('creditTxnID'=>$ID));
			 $this->db->update('tbl_customer_refund_transaction', array('creditTxnID'=>$txnID, 'updatedAt'=>date('Y-m-d H:i:s')));
			 
            
          
    	    $this->db->where(array('crID'=>$ID));
            $this->db->update('tbl_custom_credit', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}  
		}
    	
    
    
		return true; 
	}


    
    
    
    
   	
public function _updateCustomerRequest($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Do something here to load data using your model
		
	$ss='';
	$con = array('qbwc_username'=>$user);
	$companyData = $this->db->select('*')->from('tbl_company')->where($con)->get();
  
   if($companyData->num_rows()>0 ) {
	   
	   $user_data    = $companyData->row_array();
	   $username     = $user_data['qbwc_username'];
	   $companyID    = $user_data['id'] ;
	   $merchantID    = $user_data['merchantID'] ;
   }
		  $query = $this->db->query("Select c.*,comp.qbwc_username as user  from qb_test_customer c inner join tbl_company comp on c.companyID=comp.id where ListID ='".$ID."' and companyID ='".$companyID."'");
		
				
	     $data  =   $query->row_array();
        		
	  
      
	  
	  		$xml ='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<CustomerModRq requestID="' . $requestID . '">
				  <CustomerMod>
					<ListID >'.$data['ListID'].'</ListID> 
					<EditSequence >'.$data['EditSequence'].'</EditSequence> 
					<Name>'.$data['FullName'].'</Name>
					<IsActive >'.$data['IsActive'].'</IsActive> 
					<CompanyName>'.$data['companyName'].'</CompanyName> 
					<FirstName>'.$data['FirstName'].'</FirstName>
					<LastName>'.$data['LastName'].'</LastName>
					<BillAddress> 
    					<Addr1>'.$data['BillingAddress_Addr1'].'</Addr1>
    					<Addr2 >'.$data['BillingAddress_Addr2'].'</Addr2> 				
    					<City>'.$data['BillingAddress_City'].'</City>
    					<State>'.$data['BillingAddress_State'].'</State>
    					<PostalCode>'.$data['BillingAddress_PostalCode'].'</PostalCode>
    					<Country>'.$data['BillingAddress_Country'].'</Country>
					</BillAddress>
					<ShipAddress>
    					<Addr1>'.$data['ShipAddress_Addr1'].'</Addr1>
    					<Addr2 >'.$data['ShipAddress_Addr2'].'</Addr2> 				
    					<City>'.$data['ShipAddress_City'].'</City>
    					<State>'.$data['ShipAddress_State'].'</State>
    					<PostalCode>'.$data['ShipAddress_PostalCode'].'</PostalCode>
    					<Country>'.$data['ShipAddress_Country'].'</Country>
					</ShipAddress>';
                   
					$xml.='<Phone>'.$data['Phone'].'</Phone>
					<Email>'.$data['Contact'].'</Email>
					
					</CustomerMod>
				</CustomerModRq>
			</QBXMLMsgsRq>
		</QBXML>';  
      
	 
		return $this->_formatXMLRequest($xml);
	  
	
	}


	/**
	 * Handle a response from QuickBooks indicating a new customer has been added
	 */	
	public function _updateCustomerResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		$errnum = 0;
	    $errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
      
      
		$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
	   $lastquery = $this->db->last_query();
   
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   }
    	if ($Doc = $Parser->parse($errnum, $errmsg))
    	{
    		$Root = $Doc->getRoot();
    		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CustomerModRs');

			$this->getStatusData($List, 'CustomerMod');

    	    foreach ($List->children() as $Customer)
	    	{  
    	      if($Customer->getChildDataAt('CustomerRet IsActive')=='true'){ $st='1'; }else{ $st='0'; }
		    	$arr = array(
				'ListID'                 => $Customer->getChildDataAt('CustomerRet ListID'),
				'EditSequence'           => $Customer->getChildDataAt('CustomerRet EditSequence'),
				'TimeCreated'  			 => date("Y-m-d H:i:s",strtotime($Customer->getChildDataAt('CustomerRet TimeCreated'))),
				'TimeModified' 			 => date("Y-m-d H:i:s",strtotime($Customer->getChildDataAt('CustomerRet TimeModified'))),
				'companyName' 			 => $Customer->getChildDataAt('CustomerRet CompanyName'),
				'FullName'				 => ($Customer->getChildDataAt('CustomerRet FullName') != '0')?$Customer->getChildDataAt('CustomerRet FullName'):'',
				'FirstName'   			 => ($Customer->getChildDataAt('CustomerRet FirstName') != '0')?$Customer->getChildDataAt('CustomerRet FirstName'):'',
				'MiddleName' 			 => ($Customer->getChildDataAt('CustomerRet MiddleName') != '0')?$Customer->getChildDataAt('CustomerRet MiddleName'):'',
				'LastName' 				 => ($Customer->getChildDataAt('CustomerRet LastName') != '0')?$Customer->getChildDataAt('CustomerRet LastName'):'',
				'Contact' 				 => ($Customer->getChildDataAt('CustomerRet Email') != '0')?$Customer->getChildDataAt('CustomerRet Email'):'',
				'Phone'					 => ($Customer->getChildDataAt('CustomerRet Phone') != '0')?$Customer->getChildDataAt('CustomerRet Phone'):'',
				'BillingAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet BillAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr1'):'',
				'BillingAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet BillAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr2'):'',
				'BillingAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet BillAddress City') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress City'):'',
				'BillingAddress_State'		 => ($Customer->getChildDataAt('CustomerRet BillAddress State') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress State'):'',
				'BillingAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet BillAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Country'):'',
				'BillingAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet BillAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress PostalCode'):'',
                'ShipAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr1'):'',
				'ShipAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr2'):'',
				'ShipAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet ShipAddress City') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress City'):'',
				'ShipAddress_State'		 => ($Customer->getChildDataAt('CustomerRet ShipAddress State') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress State'):'',
				'ShipAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Country'):'',
				'ShipAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet ShipAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress PostalCode'):'',
				'IsActive'               =>$Customer->getChildDataAt('CustomerRet IsActive'),
				'customerStatus'         => $st,
				'companyID' 			 => $companyID,
				'qbmerchantID'           => $merchantID,
				);
			
               $lsID     =    	$arr['ListID'];


               $this->db->where(array('ListID'=>$lsID));
               $this->db->update('qb_test_customer', $arr);
    	       $this->db->where(array('customerID'=>$ID));
               $this->db->update('tbl_custom_customer', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
               
            
    	   
    	}
    	    
    	}	
    
    	
    		
		// Do something here to record that the data was added to QuickBooks successfully 
		return true; 
		
	}
	
	
	
    
   public function _update_service_item_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
		
	 
		  if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemServiceModRq requestID="'.$requestID.'">
					  <ItemServiceMod>
					<ListID >'.$data['productListID'].'</ListID> <!-- required -->
					<EditSequence >'.$data['EditSequence'].'</EditSequence> <!-- required -->
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['isActive'].'</IsActive> 
				  </ItemServiceMod>
				</ItemServiceModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{	
           
           
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemServiceModRq requestID="'.$requestID.'">
					  <ItemServiceMod>
					  <ListID >'.$data['productListID'].'</ListID> <!-- required -->
					  <EditSequence >'.$data['EditSequence'].'</EditSequence>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<SalesOrPurchaseMod>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
                          <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
						</SalesOrPurchaseMod>
						
					  </ItemServiceMod>
					</ItemServiceModRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
	   }		
		
	
				
	   return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _update_service_item_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemServiceModRs');

			$this->getStatusData($List, 'ItemServiceMod');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');
				$arr = array(
					'ListID' => $Item->getChildDataAt($ret . ' ListID'),
					'EditSequence'=>$Item->getChildDataAt($ret . ' EditSequence'),
					'IsActive'=>$Item->getChildDataAt($ret . ' IsActive'),
				     'companyListID' =>$companyID,
					'TimeCreated' => $Item->getChildDataAt($ret . ' TimeCreated'),
					'TimeModified' => $Item->getChildDataAt($ret . ' TimeModified'),
					'Name' => $Item->getChildDataAt($ret . ' Name'),
					'FullName' => $Item->getChildDataAt($ret . ' FullName'),
					'Type' => $type, 
					'Parent_ListID' => $Item->getChildDataAt($ret . ' ParentRef ListID'),
					'Parent_FullName' => $Item->getChildDataAt($ret . ' ParentRef FullName'),
					'ManufacturerPartNumber' => $Item->getChildDataAt($ret . ' ManufacturerPartNumber'), 
					'SalesTaxCode_ListID' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef ListID'), 
					'SalesTaxCode_FullName' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef FullName'), 
					'BuildPoint' => $Item->getChildDataAt($ret . ' BuildPoint'), 
					'ReorderPoint' => $Item->getChildDataAt($ret . ' ReorderPoint'), 
					'QuantityOnHand' => $Item->getChildDataAt($ret . ' QuantityOnHand'), 
					'SalesPrice' => $Item->getChildDataAt($ret . ' SalesOrPurchase Price'),
					'SalesDesc' => $Item->getChildDataAt($ret . ' SalesOrPurchase Desc'),
					'AverageCost' => $Item->getChildDataAt($ret . ' AverageCost'), 
					'QuantityOnOrder' => $Item->getChildDataAt($ret . ' QuantityOnOrder'), 
					'QuantityOnSalesOrder' => $Item->getChildDataAt($ret . ' QuantityOnSalesOrder'),  
					'TaxRate' => $Item->getChildDataAt($ret . ' TaxRate'), 
					
					
					);
				
				$look_for = array(
					'SalesPrice' => array( 'SalesOrPurchase Price', 'SalesAndPurchase SalesPrice', 'SalesPrice' ),
					'SalesDesc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase SalesDesc', 'SalesDesc' ),
					'PurchaseCost' => array( 'SalesOrPurchase Price', 'SalesAndPurchase PurchaseCost', 'PurchaseCost' ),
					'PurchaseDesc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase PurchaseDesc', 'PurchaseDesc' ),
					'PrefVendor_ListID' => array( 'SalesAndPurchase PrefVendorRef ListID', 'PrefVendorRef ListID' ), 
					'PrefVendor_FullName' => array( 'SalesAndPurchase PrefVendorRef FullName', 'PrefVendorRef FullName' ),
					); 
				
				foreach ($look_for as $field => $look_here)
				{
					if (!empty($arr[$field]))
					{
						break;
					}
					
					foreach ($look_here as $look)
					{
						$arr[$field] = $Item->getChildDataAt($ret . ' ' . $look);
					}
				}
				
				$this->db->select('*');
				$this->db->from('qb_test_item');
				$this->db->where('ListID', $Item->getChildDataAt($ret . ' ListID'));
				$this->db->where('companyListID', $companyID);
				$qr1 = $this->db->get();
				
					
				if($qr1->num_rows() >0)
				{  
					$this->db->where(array('ListID' => $Item->getChildDataAt($ret . ' ListID'), 'companyListID' => $companyID));
					$this->db->update('qb_test_item',$arr);
					
				}else{
					
					  $this->db->insert('qb_test_item',$arr);
				}
				
			
			
    	       $this->db->where(array('productID'=>$ID));
               $this->db->update('tbl_custom_product',array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		  
		return true; 
	}
	
    
	
	
	
	
    
   public function _update_noninventory_item_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();

      if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemNonInventoryModRq requestID="'.$requestID.'">
				  <ItemNonInventoryMod>
					<ListID >'.$data['productListID'].'</ListID> 
					<EditSequence >'.$data['EditSequence'].'</EditSequence> 
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['IsActive'].'</IsActive> 

				  </ItemNonInventoryMod>
				</ItemNonInventoryModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{
		   
	$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemNonInventoryModRq requestID="'.$requestID.'">
						<ItemNonInventoryMod> <!-- required -->
						   <ListID >'.$data['productListID'].'</ListID> 
					       <EditSequence >'.$data['EditSequence'].'</EditSequence> 
							<Name>'.$data['productName'].'</Name>
							<IsActive >true</IsActive>';
							if($data['parentID']!=""){
							$xml.='<ParentRef> 
							<ListID >'.$data['parentID'].'</ListID> 
							<FullName >'.$data['parentFullName'].'</FullName> 
							</ParentRef>';
							}
                           $xml.='<SalesOrPurchaseMod>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
                          <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
						</SalesOrPurchaseMod>
							
                            
						  </ItemNonInventoryMod>
						</ItemNonInventoryModRq>
				  </QBXMLMsgsRq>
				</QBXML>'; 
	   }
		return $this->_formatXMLRequest($xml); 
	}				
	
	
    
	public function _update_noninventory_item_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemNonInventoryModRs');

			$this->getStatusData($List, 'ItemNonInventoryMod');

	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
					$merchantID   = $user_data['merchantID'];
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
           
					$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			       $data   = 	$query->row_array();

				   	if(!empty($data)){
						$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
						$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");

						$QBD_Sync = new QBD_Sync($merchantID);
						$QBD_Sync->invoiceSync([
							'itemId' => $lsid
						]);
					}
    	       $this->db->where(array('productID'=>$ID));
             $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		return true; 
	}
    
    
    
    
    
    
   public function _add_noninventory_item_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
		
			 
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
				<ItemNonInventoryAddRq requestID="'.$requestID.'">
						<ItemNonInventoryAdd>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<SalesOrPurchase>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
                          <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
						</SalesOrPurchase>
						
					  </ItemNonInventoryAdd>
					</ItemNonInventoryAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
		
		return $this->_formatXMLRequest($xml); 
   }				
	
	
	public function _add_noninventory_item_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	    $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemNonInventoryAddRs');

			$this->getStatusData($List, 'ItemNonInventoryAdd');

	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    			$merchantID = '';
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
					$merchantID   = $user_data['merchantID'];
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');
            
				
				   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			       $data   = 	$query->row_array();

				   	if(!empty($data)){
						$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
						$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");
					}
			
    	        $this->db->where(array('productID'=>$ID));
              $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
		return true; 
	}
	
	
    
   public function _add_service_item_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
		
	 
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemServiceAddRq requestID="'.$requestID.'">
					  <ItemServiceAdd>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<SalesOrPurchase>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
   						   <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
                         </SalesOrPurchase>
						
					  </ItemServiceAdd>
					</ItemServiceAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
		
				
		return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _add_service_item_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemServiceAddRs');

			$this->getStatusData($List, 'ItemServiceAdd');
			
			$con1 = array('qbwc_username'=>$user);
			$query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
			$lastquery = $this->db->last_query();
		
			$merchantID = '';
			if($query1->num_rows()>0 ) {
				
				$user_data    = $query1->row_array();
				$merchantID = $user_data['merchantID'];
				$companyID    = $user_data['id'] ;
			}
		
			
			foreach ($List->children() as $Item)
			{
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');
			
					
				$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
				$data   = 	$query->row_array();
						
				if(!empty($data)){
					$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
					$query1  = 	$this->db->query("Update  qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");
					// update product id for items in sync settings table
					if($data['productName'] == 'Surcharge Fees'){
						// update in settings
						$this->db->where('merchantID', $data['merchantID']);
						$this->db->update('tbl_merchant_surcharge', ['defaultItem' => $lsid]);
					}
					$QBD_Sync = new QBD_Sync($merchantID);
					$QBD_Sync->invoiceSync([
						'itemId' => $lsid
					]);
				}
				
				$this->db->where(array('productID'=>$ID));
				$this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));	
			}
		}
		else{
			$this->db->where(array('productID'=>$ID));
			$this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
		
		}
	
	
		return true; 
	}
	
  
	public function _add_item_request_discount($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
	 	$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		<QBXMLMsgsRq onError="stopOnError">
			<ItemDiscountAddRq>
				<ItemDiscountAdd> 
				<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
				$xml.='<ItemDesc >'.$data['productDescription'].'</ItemDesc>
				

				<DiscountRate >'.$data['Discount'].'</DiscountRate>
				

				<AccountRef>
				<ListID >'.$data['AccountRef'].'</ListID>
				<FullName >'.$data['accountFullName'].'</FullName>
				</AccountRef>

				</ItemDiscountAdd>
	</ItemDiscountAddRq> 
    </QBXMLMsgsRq>
</QBXML>';  
		return $this->_formatXMLRequest($xml); 
	}	

	public function _add_item_response_discount($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);

		$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
   
		if($query->num_rows()>0 ) {
			
			$user_data    = $query->row_array();
			$username     = $user_data['qbwc_username'];
			$companyID    = $user_data['id'] ;
			$merchantID    = $user_data['merchantID'] ;
		}
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			
		       foreach ($List->children() as $Item)
			{
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');	
			
			       $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			       $data   = 	$query->row_array();
			       			       
			       	if(!empty($data)){
						$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
						$query1  = 	$this->db->query("Update  qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");

						$QBD_Sync = new QBD_Sync($merchantID);
						$QBD_Sync->invoiceSync([
							'itemId' => $lsid
						]);
					}
			
    	        $this->db->where(array('productID'=>$ID));
              $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));	
              
              
			}
			
		}
    	
    
    
		return true; 
	}

 
   public function _update_item_request_discount($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
		
	 
		  if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemDiscountModRq requestID="'.$requestID.'">
					  <ItemDiscountMod>
					<ListID >'.$data['productListID'].'</ListID> <!-- required -->
					<EditSequence >'.$data['EditSequence'].'</EditSequence> <!-- required -->
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['isActive'].'</IsActive> 
				  </ItemDiscountMod>
				</ItemDiscountModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{	
           
           
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemDiscountModRq requestID="'.$requestID.'">
					  <ItemDiscountMod>
					  <ListID >'.$data['productListID'].'</ListID> <!-- required -->
					  <EditSequence >'.$data['EditSequence'].'</EditSequence>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<ItemDesc >'.$data['productDescription'].'</ItemDesc>
				

				<DiscountRate >'.$data['Discount'].'</DiscountRate>
				

				<AccountRef>
				<ListID >'.$data['AccountRef'].'</ListID>
				<FullName >'.$data['accountFullName'].'</FullName>
				</AccountRef>
						
					  </ItemDiscountMod>
					</ItemDiscountModRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
	   }		
		
	
	   return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _update_item_response_discount($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemDiscountModRs');

			$this->getStatusData($List, 'ItemDiscountMod');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');
			
    	       $this->db->where(array('productID'=>$ID));
               $this->db->update('tbl_custom_product',array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		  
		return true; 
	}
	
     
   public function _add_item_othercharge_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
		
	 
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
          

					<ItemOtherChargeAddRq requestID="'.$requestID.'">
					  <ItemOtherChargeAdd>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<SalesOrPurchase>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
   						   <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
                         </SalesOrPurchase>
						
					  </ItemOtherChargeAdd>
					</ItemOtherChargeAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
				
	
				
				
		return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _add_item_othercharge_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemOtherChargeAddRs');

			$this->getStatusData($List, 'ItemOtherChargeAdd');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
					$merchantID    = $user_data['merchantID'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
    	      	$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');	
			
				$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
				$data   = 	$query->row_array();
								
				if(!empty($data)){
					$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' inv_itm_merchant_id = AND '$merchantID'  ");
					$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");
				}
			
    	        $this->db->where(array('productID'=>$ID));
              $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));	
			}
		}
    	
    
    
		return true; 
	}
	
    

   
 
   public function _update_item_othercharge_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
			
		  if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemOtherChargeModRq requestID="'.$requestID.'">
					  <ItemOtherChargeMod>
					<ListID >'.$data['productListID'].'</ListID> <!-- required -->
					<EditSequence >'.$data['EditSequence'].'</EditSequence> <!-- required -->
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['isActive'].'</IsActive> 
				  </ItemOtherChargeMod>
				</ItemOtherChargeModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{	
           
           
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemOtherChargeModRq requestID="'.$requestID.'">
					  <ItemOtherChargeMod>
					  <ListID >'.$data['productListID'].'</ListID> <!-- required -->
					  <EditSequence >'.$data['EditSequence'].'</EditSequence>
						<Name>'.$data['productName'].'</Name>';
						if($data['parentID']!=""){
						$xml.='<ParentRef> 
						<ListID >'.$data['parentID'].'</ListID> 
						<FullName >'.$data['parentFullName'].'</FullName> 
						</ParentRef>';
						}
						$xml.='<SalesOrPurchaseMod>
						  <Desc>'.$data['productDescription'].'</Desc>
						  <Price>'.$data['productPrice'].'</Price>
   						   <AccountRef>
                            <ListID >'.$data['AccountRef'].'</ListID> 
							<FullName>'.$data['accountFullName'].'</FullName>
						   </AccountRef>
                         </SalesOrPurchaseMod>
					  </ItemOtherChargeMod>
					</ItemOtherChargeModRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
	   }		
		
	
				
	   return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _update_item_othercharge_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemOtherChargeModRs');

			$this->getStatusData($List, 'ItemOtherChargeMod');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				
			
    	       $this->db->where(array('productID'=>$ID));
               $this->db->update('tbl_custom_product',array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		  
		return true; 
	}
	

 
   public function _add_item_subtotal_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
			
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
          

					<ItemSubtotalAddRq requestID="'.$requestID.'">
					  <ItemSubtotalAdd>
						<Name>'.$data['productName'].'</Name>
						<ItemDesc >'.$data['productDescription'].'</ItemDesc>
					
						
					  </ItemSubtotalAdd>
					</ItemSubtotalAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
				
	
		return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _add_item_subtotal_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemSubtotalAddRs');

			$this->getStatusData($List, 'ItemSubtotalAdd');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
				$merchantID = '';
    			if($query1->num_rows()>0 ) {
    				$user_data    = $query1->row_array();
					$merchantID   = $user_data['merchantID'];
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
             
    	      	$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');	
			
				$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
				$data   = 	$query->row_array();
								
				if(!empty($data)){
					$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' inv_itm_merchant_id = AND '$merchantID'  ");
					$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");
				}
			
    	        $this->db->where(array('productID'=>$ID));
              $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));	
			}
		}
    	
    
    
		return true; 
	}
	
  

   public function _update_item_subtotal_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
			
      if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemSubtotalModRq requestID="'.$requestID.'">
				  <ItemSubtotalMod>
					<ListID >'.$data['productListID'].'</ListID> 
					<EditSequence >'.$data['EditSequence'].'</EditSequence> 
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['IsActive'].'</IsActive> 

				  </ItemSubtotalMod>
				</ItemSubtotalModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{
		   
	$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemSubtotalModRq requestID="'.$requestID.'">
						<ItemSubtotalMod> <!-- required -->
						   <ListID >'.$data['productListID'].'</ListID> 
					       <EditSequence >'.$data['EditSequence'].'</EditSequence> 
							<Name>'.$data['productName'].'</Name>
							<IsActive >true</IsActive>
							<ItemDesc >'.$data['productDescription'].'</ItemDesc>
                          
							
                            
						  </ItemSubtotalMod>
						</ItemSubtotalModRq>
				  </QBXMLMsgsRq>
				</QBXML>'; 
	   }
	 	return $this->_formatXMLRequest($xml); 
 
	}				
	
	
    
	public function _update_item_subtotal_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemSubtotalModRs');

			$this->getStatusData($List, 'ItemSubtotalMod');

	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
          
    	       $this->db->where(array('productID'=>$ID));
             $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		return true; 
	}
    

	
   
 
  public function _add_item_payment_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
			
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
          

					<ItemPaymentAddRq requestID="'.$requestID.'">
					  <ItemPaymentAdd>
						<Name>'.$data['productName'].'</Name>
						<ItemDesc >'.$data['productDescription'].'</ItemDesc>
						<PaymentMethodRef> 
						<FullName >Cash</FullName> 
						</PaymentMethodRef>
					  </ItemPaymentAdd>
					</ItemPaymentAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
				
				
	
				
				
		return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _add_item_payment_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemPaymentAddRs');

			$this->getStatusData($List, 'ItemPaymentAdd');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
				$merchantID = '';
    			if($query1->num_rows()>0 ) {
    				$user_data    = $query1->row_array();
					$merchantID   = $user_data['merchantID'];
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
           
    	    	$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');	
			
				$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
				$data   = 	$query->row_array();
				if(!empty($data)){
					$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
					$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");

					$QBD_Sync = new QBD_Sync($merchantID);
					$QBD_Sync->invoiceSync([
						'itemId' => $lsid
					]);
				}
			
    	        $this->db->where(array('productID'=>$ID));
              $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));	
			}
		}
    	
    
    
		return true; 
	}
	




   public function _update_item_payment_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();

      if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemPaymentModRq requestID="'.$requestID.'">
				  <ItemPaymentMod>
					<ListID >'.$data['productListID'].'</ListID> 
					<EditSequence >'.$data['EditSequence'].'</EditSequence> 
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['IsActive'].'</IsActive> 

				  </ItemPaymentMod>
				</ItemPaymentModRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{
		   
	$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemPaymentModRq requestID="'.$requestID.'">
						<ItemPaymentMod> <!-- required -->
						   <ListID >'.$data['productListID'].'</ListID> 
					       <EditSequence >'.$data['EditSequence'].'</EditSequence> 
							<Name>'.$data['productName'].'</Name>
							<IsActive >true</IsActive>
							<ItemDesc >'.$data['productDescription'].'</ItemDesc>
                             <PaymentMethodRef> 
						    <FullName >Cash</FullName> 
						   </PaymentMethodRef>
							
                            
						  </ItemPaymentMod>
						</ItemPaymentModRq>
				  </QBXMLMsgsRq>
				</QBXML>'; 
	   }
		return $this->_formatXMLRequest($xml); 
 
	}				
	
	
    
	public function _update_item_payment_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemPaymentModRs');

			$this->getStatusData($List, 'ItemPaymentMod');

	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
            
    	       $this->db->where(array('productID'=>$ID));
             $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		return true; 
	}
    

	

 
  public function _add_item_group_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		  $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
		  $data   = 	$query->row_array();
		  
		   $query1  = 	$this->db->query("Select * from qb_qroup_lineitem where groupListID='".$ID."'  ");
		   $data1   = 	$query1->result_array();
		  
		  
			
				
	   $xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemGroupAddRq requestID="'.$requestID.'">
					<ItemGroupAdd> 
					<Name >'.$data['productName'].'</Name> 
					
					<IsActive >true</IsActive> 
					<ItemDesc >'.$data['productDescription'].'</ItemDesc>
					
					<IsPrintItemsInGroup >true</IsPrintItemsInGroup>';
					foreach($data1 as $dat)
					{
					
					$xml.='<ItemGroupLine>
					<ItemRef>
					<ListID >'.$dat['itemListID'].'</ListID> 
					<FullName >'.$dat['FullName'].'</FullName>
					</ItemRef>
					<Quantity >'.$dat['Quantity'].'</Quantity> 
					
					</ItemGroupLine>';
					}
					$xml.='</ItemGroupAdd>
					
					</ItemGroupAddRq>
				  </QBXMLMsgsRq>
				</QBXML>';  
				
			
				
	
				
		return $this->_formatXMLRequest($xml); 
   	}				
	
	
	public function _add_item_group_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
	 
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemGroupAddRs');

			$this->getStatusData($List, 'ItemGroupAdd');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
				$merchantID = '';
    			if($query1->num_rows()>0 ) {
    				$user_data    = $query1->row_array();
					$merchantID   = $user_data['merchantID'];
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
              
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');
			
            	$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$lsid= $Item->getChildDataAt($ret . ' ListID');	
			
				$query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
				$data   = 	$query->row_array();
				if(!empty($data)){
					$this->db->query("Update qb_test_invoice_lineitem set Item_ListID='".$lsid."' where Item_ListID='".$data['productListID']."' AND inv_itm_merchant_id = '$merchantID'  ");
					$query1  = 	$this->db->query("Update qb_test_item set ListID='".$lsid."' where ListID='".$data['productListID']."'  ");

					$QBD_Sync = new QBD_Sync($merchantID);
					$QBD_Sync->invoiceSync([
						'itemId' => $lsid
					]);
				}
			
    	     
          
            $this->db->where(array('groupListID'=>$ID));
            $this->db->update('qb_qroup_lineitem', array('groupListID'=>$lsid));
    	    $this->db->where(array('productID'=>$ID));
            $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
    
    
		return true; 
	}






    

   public function _update_item_group_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
   {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_product where productID='".$ID."'  ");
			$data   = 	$query->row_array();
			 $query1  = 	$this->db->query("Select * from qb_qroup_lineitem where groupListID='".$data['productListID']."'  ");
		   $data1   = 	$query1->result_array();

      if($data['IsActive']=='false'){
           
        $xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<ItemGroupModRq requestID="'.$requestID.'">
				  <ItemGroupMod>
					<ListID >'.$data['productListID'].'</ListID> 
					<EditSequence >'.$data['EditSequence'].'</EditSequence> 
					<Name>'.$data['productName'].'</Name>
					<IsActive >'.$data['IsActive'].'</IsActive> 

				  </ItemGroupMod>
				</ItemGroupRq>
			</QBXMLMsgsRq>
		</QBXML>'; 
       }else{
		   
	$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
					<ItemGroupModRq requestID="'.$requestID.'">
						<ItemGroupMod> <!-- required -->
						   <ListID >'.$data['productListID'].'</ListID> 
					       <EditSequence >'.$data['EditSequence'].'</EditSequence> 
							<Name>'.$data['productName'].'</Name>
							<IsActive >true</IsActive>
							<ItemDesc >'.$data['productDescription'].'</ItemDesc>
							
					<IsPrintItemsInGroup >true</IsPrintItemsInGroup>';
					foreach($data1 as $dat)
					{
					
					$xml.='<ItemGroupLine>
					<ItemRef>
					<ListID >'.$dat['itemListID'].'</ListID> 
					<FullName >'.$dat['FullName'].'</FullName>
					</ItemRef>
					<Quantity >'.$dat['Quantity'].'</Quantity> 
					
					</ItemGroupLine>';
					}
					$xml.='
						  </ItemGroupMod>
						</ItemGroupModRq>
				  </QBXMLMsgsRq>
				</QBXML>'; 
	   }
	 	return $this->_formatXMLRequest($xml); 
	}				
	
	
    
	public function _update_item_group_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	 $errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
	
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemGroupModRs');

			$this->getStatusData($List, 'ItemGroupMod');

	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
            
           
    	     $this->db->where(array('productID'=>$ID));
             $this->db->update('tbl_custom_product', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));
			}
		}
    	
		return true; 
	}
    

	

	
	public function _update_invoice_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
		
		$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
   
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   } 
			 
	  	 
		   $query  = 	$this->db->query("Select * from qb_test_invoice where TxnID='".$ID."' and qb_inv_merchantID='".$merchantID."' ");
			$data   = 	$query->row_array();
		
		 $query1 = $this->db->query("Select * from  qb_test_invoice_lineitem where TxnID='".$ID."' and inv_itm_merchant_id='".$merchantID."' ");
			$data_items   = 	$query1->result_array();
			$querytax = $this->db->query("Select * from tbl_taxes where taxListID ='".$data['TaxListID']."'  ");
			$datatax   = 	$querytax->row_array();
			
				
		$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
			  <InvoiceModRq requestID="'.$requestID.'">
			  <InvoiceMod>
			    <TxnID>'.$data['TxnID'].'</TxnID>
				<EditSequence>'.$data['EditSequence'].'</EditSequence>
				<CustomerRef>
			    	<ListID>'.$data['Customer_ListID'].'</ListID>
				  	<FullName>'.$data['Customer_FullName'].'</FullName>
				</CustomerRef>
				<TxnDate>'.date('Y-m-d', strtotime($data['TimeModified'])).'</TxnDate>
				<RefNumber>'.$data['RefNumber'].'</RefNumber>';
			
			
				$xml.='<DueDate>'.date('Y-m-d', strtotime($data['DueDate'])).'</DueDate>
			   <ShipDate>'.date('Y-m-d', strtotime($data['DueDate'])).'</ShipDate>';
			   if(!empty($datatax)){
				$xml.='<ItemSalesTaxRef>
				<ListID>'.$datatax['taxListID'].'</ListID>
				<FullName>'.$datatax['friendlyName'].'</FullName>
				</ItemSalesTaxRef>';
			}
				  foreach($data_items as $item){	
					
					$amount= sprintf('%0.2f', $item['Rate']*$item['Quantity']);
					if($item['item_tax'] == 1){
						$tax = 'Tax';
					}else{
						$tax = 'Non';
					}
						$xml.='<InvoiceLineMod>
						<TxnLineID>'.$item['TxnLineID'].'</TxnLineID>';
						if($item['TxnLineID'] == '-1'){
							$xml.='<ItemRef>
							<ListID>'.$item['Item_ListID'].'</ListID>
						</ItemRef>';
						}
						$xml.='<Desc>'.$item['Descrip'].'</Desc>
					   	<Quantity>'.$item['Quantity'].'</Quantity>
						<Rate>'.$item['Rate'].'</Rate>
						<Amount>'.$amount.'</Amount>
					   	<SalesTaxCodeRef>
					   		<FullName>'.$tax.'</FullName>
						   </SalesTaxCodeRef>
						   </InvoiceLineMod>';
					
					
				 }
					
				  $xml.='</InvoiceMod>
				  </InvoiceModRq>
		
		  </QBXMLMsgsRq>
		</QBXML>';	 
	
		return $this->_formatXMLRequest($xml);  
	}		
	
    
	public function _update_invoice_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
    if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/InvoiceModRs');

		$this->getStatusData($List, 'InvoiceMod');

		$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
   
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   }
		foreach ($List->children() as $Invoice)
		{
			$arr = array(
				'TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'),
				'TimeCreated' => $Invoice->getChildDataAt('InvoiceRet TimeCreated'),
				'TimeModified' => $Invoice->getChildDataAt('InvoiceRet TimeModified'),
				'RefNumber' => $Invoice->getChildDataAt('InvoiceRet RefNumber'),
				'Customer_ListID' => $Invoice->getChildDataAt('InvoiceRet CustomerRef ListID'),
				'Customer_FullName' => $Invoice->getChildDataAt('InvoiceRet CustomerRef FullName'),
				'ShipAddress_Addr1' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr1'),
				'ShipAddress_Addr2' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr2'),
				'ShipAddress_City' => $Invoice->getChildDataAt('InvoiceRet BillAddress City'),
				'ShipAddress_State' => $Invoice->getChildDataAt('InvoiceRet BillAddress State'),
				'ShipAddress_Country' => $Invoice->getChildDataAt('InvoiceRet BillAddress Country'),
				'ShipAddress_PostalCode' => $Invoice->getChildDataAt('InvoiceRet BillAddress PostalCode'),
				'BalanceRemaining'  => $Invoice->getChildDataAt('InvoiceRet BalanceRemaining'),
				'DueDate'           => $Invoice->getChildDataAt('InvoiceRet DueDate'),
				'IsPaid'            => $Invoice->getChildDataAt('InvoiceRet IsPaid'),
				'EditSequence'      => $Invoice->getChildDataAt('InvoiceRet EditSequence'),
				'AppliedAmount'      => $Invoice->getChildDataAt('InvoiceRet AppliedAmount'),
				'TaxRate'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxPercentage'),
				'TotalTax'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxTotal'),
				'TaxListID'      => $Invoice->getChildDataAt('InvoiceRet ItemSalesTaxRef ListID'),
				'qb_inv_companyID' => $companyID,
				'qb_inv_merchantID' => $merchantID,
				);
        
				$invoiceStatus =$Invoice->getChildDataAt('InvoiceRet Memo');
				if ($invoiceStatus && strpos($invoiceStatus, 'VOID') !== false) {
					$invoiceStatus = 'cancel';
					$arr['IsPaid']= 'false';
				} else {
					$invoiceStatus = '';
				}
		 $this->db->select('*');
		 $this->db->from('qb_test_invoice');
		 $this->db->where('TxnID', $Invoice->getChildDataAt('InvoiceRet TxnID'));
		 $this->db->where('qb_inv_companyID', $companyID);
		 $this->db->where('qb_inv_merchantID', $merchantID);
		 $qr1 = $this->db->get();
		 
		 if($qr1->num_rows() >0)
		 {  
			if(empty($invoiceStatus)){
				$invoiceStatus = $qr1->row_array()['userStatus'];
			} 
			$arr['userStatus']=$invoiceStatus;
			
			 $this->db->where(array('TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'), 'qb_inv_companyID' => $companyID, 'qb_inv_merchantID' => $merchantID));
			 $this->db->update('qb_test_invoice',$arr);
		 }else{
			if(empty($invoiceStatus)){
				$invoiceStatus = 'Active';
			} 
			$arr['userStatus']=$invoiceStatus;

			 $this->db->insert('qb_test_invoice',$arr);
		}

			
			// Remove any old line items
			$this->db->query("DELETE FROM qb_test_invoice_lineitem WHERE TxnID = '" . $this->db->escape_str($arr['TxnID']) . "' and inv_itm_merchant_id = '" . $merchantID . "' ") ;
			
			// Process the line items
			foreach ($Invoice->children() as $Child)
			{
				if ($Child->name() == 'InvoiceLineRet')
				{
					$InvoiceLine = $Child;
					if($InvoiceLine->getChildDataAt('InvoiceLineRet SalesTaxCodeRef FullName') == 'Tax'){
						$tax = 1;
					}
					else{
						$tax = null;
					}
					if($InvoiceLine->getChildDataAt('InvoiceLineRet Quantity')){
						$qunt = $InvoiceLine->getChildDataAt('InvoiceLineRet Quantity');
					}
					else{
						$qunt = 1;
					}
					$lineitem = array( 
						'TxnID'          => $arr['TxnID'], 
						'TxnLineID'      => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 
						'Item_ListID'    => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef ListID'), 
						'Item_FullName'  => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef FullName'), 
						'Descrip'        => $InvoiceLine->getChildDataAt('InvoiceLineRet Desc'), 
						'Quantity'       => $qunt,
						'Rate'           => $InvoiceLine->getChildDataAt('InvoiceLineRet Rate'),
						'item_tax' => $tax,
						'inv_itm_merchant_id' => $merchantID,
						);
						$this->db->select('*');
						$this->db->from('qb_test_invoice_lineitem');
						$this->db->where('TxnID', $arr['TxnID']);
						$this->db->where('TxnLineID', $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'));
						$this->db->where('inv_itm_merchant_id', $merchantID);
						$qr1 = $this->db->get();
						
						if($qr1->num_rows() >0)
						{  
							$this->db->where(array('TxnID' => $arr['TxnID'], 'TxnLineID' => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 'inv_itm_merchant_id' => $merchantID));
							$this->db->update('qb_test_invoice_lineitem',$lineitem);
						}else{
							$this->db->insert('qb_test_invoice_lineitem',$lineitem);
					   }
					
				}
			}
        
           $this->db->where(array('insertInvID'=>$ID));
            $this->db->update('tbl_custom_invoice', array('qb_status'=>5, 'TimeModified'=>date('Y-m-d H:i:s')));
           
        
		}
    }
		return true;
	}
	


    public function _deleteInvoiceRequest($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
		
			
      $data = $this->db->query("Select * from tbl_del_transactions where delID='".$ID."' ")->row_array();
    
       
		$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		 <QBXMLMsgsRq onError="stopOnError">
		<TxnDelRq requestID="'.$requestID.'">
			<TxnDelType>'.$data['txnType'].'</TxnDelType>
			<TxnID>'.$data['delTxnID'].'</TxnID>
		</TxnDelRq>
      </QBXMLMsgsRq>
      </QBXML>';	 
		
		

	return $xml;  
	}		
	
    
	public function _deleteInvoiceResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	if ($Doc = $Parser->parse($errnum, $errmsg))
	{
            $this->db->where(array('delID'=>$ID));
           $this->db->update('tbl_del_transactions', array('qb_status'=>5, 'TimeModified'=>date('Y-m-d H:i:s')));
    
    
           
    }
		return true; 
	}
	
	
    
	
	public function _addInvoiceRequest($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
    {
		
			 
			 
	  	 
		   $query  = 	$this->db->query("Select * from tbl_custom_invoice where insertInvID='".$ID."'  ");
			$data   = 	$query->row_array();
			 $query1 = $this->db->query("Select * from  qb_test_invoice_lineitem where TxnID='".$ID."'  ");
			$data_items   = 	$query1->result_array();
		 
		$xml='<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="'.$version.'"?>
		<QBXML>
		  <QBXMLMsgsRq onError="stopOnError">
			  <InvoiceAddRq requestID="'.$requestID.'">
			  <InvoiceAdd >
				<CustomerRef>
			    	<ListID>'.$data['Customer_ListID'].'</ListID>
				  <FullName>'.$data['Customer_FullName'].'</FullName>
				</CustomerRef>
				<TxnDate>'.date('Y-m-d', strtotime($data['TimeModified'])).'</TxnDate>
				<RefNumber>'.$data['RefNumber'].'</RefNumber>
			
			
				<BillAddress>
				  <Addr1>'.$data['Billing_Addr1'].'</Addr1>
				  <City>'.$data['Billing_City'].'</City>
				  <State>'.$data['Billing_State'].'</State>
				  <PostalCode>'.$data['Billing_PostalCode'].'</PostalCode>
				  <Country>'.$data['Billing_Country'].'</Country>
				</BillAddress>
			  <DueDate>'.date('Y-m-d', strtotime($data['DueDate'])).'</DueDate>
			   <ShipDate>'.date('Y-m-d', strtotime($data['DueDate'])).'</ShipDate>';

				 $querytax = $this->db->query("Select * from tbl_taxes where taxListID ='".$data['TaxListID']."'  ");
			   $datatax   = 	$querytax->row_array();
				if(!empty($datatax)){
					$xml.='<ItemSalesTaxRef>
					<ListID>'.$datatax['taxListID'].'</ListID>
					<FullName>'.$datatax['friendlyName'].'</FullName>
					</ItemSalesTaxRef>';
				}
			   		
				 
			  foreach($data_items as $item){	
			      if($data['freeTrial']=='1'){
			          $amount='0.00';
			      }else{
                   $amount= sprintf('%0.2f', $item['Rate']*$item['Quantity']);
			      }
			      if($item['item_tax'] == 1){
				
					$tax = 'Tax';
				}else{
					
					$tax = 'Non';
				}
			$xml.='<InvoiceLineAdd>
				  <ItemRef>
				   <ListID>'.$item['Item_ListID'].'</ListID>
					<FullName>'.$item['Item_FullName'].'</FullName>
				  </ItemRef>
				  <Desc>'.$item['Descrip'].'</Desc>
				  <Quantity>'.$item['Quantity'].'</Quantity>
				  <Rate>'.$item['Rate'].'</Rate>
				   <Amount>'.$amount.'</Amount>
				   
				   <SalesTaxCodeRef>
						<FullName>'.$tax.'</FullName>
					</SalesTaxCodeRef>
				</InvoiceLineAdd>';
			  }	
				$xml.='</InvoiceAdd>
			</InvoiceAddRq>
		
		  </QBXMLMsgsRq>
		</QBXML>';	 
		
		
				  

  		return $this->_formatXMLRequest($xml); 
 
	}		
	
    
	public function _addInvoiceResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
    if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/InvoiceAddRs');

		$this->getStatusData($List, 'InvoiceAdd');

		$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
   
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   }
		foreach ($List->children() as $Invoice)
		{
		
		
			$arr = array(
				'TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'),
				'TimeCreated' => date("Y-m-d H:i:s",strtotime($Invoice->getChildDataAt('InvoiceRet TimeCreated'))),
				'TimeModified' => date("Y-m-d H:i:s",strtotime($Invoice->getChildDataAt('InvoiceRet TimeModified'))),
				'RefNumber' => $Invoice->getChildDataAt('InvoiceRet RefNumber'),
				'Customer_ListID' => $Invoice->getChildDataAt('InvoiceRet CustomerRef ListID'),
				'Customer_FullName' => $Invoice->getChildDataAt('InvoiceRet CustomerRef FullName'),
				'ShipAddress_Addr1' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr1'),
				'ShipAddress_Addr2' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr2'),
				'ShipAddress_City' => $Invoice->getChildDataAt('InvoiceRet BillAddress City'),
				'ShipAddress_State' => $Invoice->getChildDataAt('InvoiceRet BillAddress State'),
				'ShipAddress_Country' => $Invoice->getChildDataAt('InvoiceRet BillAddress Country'),
				'ShipAddress_PostalCode' => $Invoice->getChildDataAt('InvoiceRet BillAddress PostalCode'),
				'BalanceRemaining'  => $Invoice->getChildDataAt('InvoiceRet BalanceRemaining'),
				'DueDate'           => $Invoice->getChildDataAt('InvoiceRet DueDate'),
				'IsPaid'            => $Invoice->getChildDataAt('InvoiceRet IsPaid'),
				'EditSequence'      => $Invoice->getChildDataAt('InvoiceRet EditSequence'),
				'AppliedAmount'      => $Invoice->getChildDataAt('InvoiceRet AppliedAmount'),
				'TaxRate'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxPercentage'),
				'TotalTax'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxTotal'),
				'TaxListID'      => $Invoice->getChildDataAt('InvoiceRet ItemSalesTaxRef ListID'),
				'qb_inv_companyID' => $companyID,
				'qb_inv_merchantID' => $merchantID,
				);
        
        $q = $this->db->query("Select * from qb_test_invoice where TxnID='".$ID."' ");
         if($q->num_rows()== 0){
         $arr['userStatus']='Active';
         }else{
         $arr['userStatus']=$q->row_array()['userStatus'];
         
         $this->db->where(array('TxnID'=>$ID));
         $this->db->update('qb_test_invoice',$arr);
         $this->db->query("DELETE FROM qb_test_invoice_lineitem WHERE TxnID = '".$ID."' and inv_itm_merchant_id = '".$merchantID."' ") ;
         
         }
			
	$this->db->select('*');
	$this->db->from('qb_test_invoice');
	$this->db->where('TxnID', $Invoice->getChildDataAt('InvoiceRet TxnID'));
	$this->db->where('qb_inv_companyID', $companyID);
	$this->db->where('qb_inv_merchantID', $merchantID);
	$qr1 = $this->db->get();
	
	if($qr1->num_rows() >0)
	{  
		$this->db->where(array('TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'), 'qb_inv_companyID' => $companyID, 'qb_inv_merchantID' => $merchantID));
		$this->db->update('qb_test_invoice',$arr);
	}else{
		$this->db->insert('qb_test_invoice',$arr);
   }
		
			// Remove any old line items
			$this->db->query("DELETE FROM qb_test_invoice_lineitem WHERE TxnID = '" . $this->db->escape_str($arr['TxnID']) . "' and inv_itm_merchant_id = '".$merchantID."' ") ;
			
			// Process the line items
			foreach ($Invoice->children() as $Child)
			{
				if ($Child->name() == 'InvoiceLineRet')
				{
					$InvoiceLine = $Child;
					if($InvoiceLine->getChildDataAt('InvoiceLineRet SalesTaxCodeRef FullName') == 'Tax'){
						$tax = 1;
					}
					else{
						$tax = null;
					}
					if($InvoiceLine->getChildDataAt('InvoiceLineRet Quantity')){
						$qunt = $InvoiceLine->getChildDataAt('InvoiceLineRet Quantity');
					}
					else{
						$qunt = 1;
					}
					$lineitem = array( 
						'TxnID'          => $arr['TxnID'], 
						'TxnLineID'      => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 
						'Item_ListID'    => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef ListID'), 
						'Item_FullName'  => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef FullName'), 
						'Descrip'        => $InvoiceLine->getChildDataAt('InvoiceLineRet Desc'), 
						'Quantity'       => $qunt,
						'Rate'           => $InvoiceLine->getChildDataAt('InvoiceLineRet Rate'),
						'item_tax' => $tax,
						'inv_itm_merchant_id' => $merchantID,
						);
						$this->db->select('*');
						$this->db->from('qb_test_invoice_lineitem');
						$this->db->where('TxnID', $arr['TxnID']);
						$this->db->where('TxnLineID', $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'));
						$this->db->where('inv_itm_merchant_id', $merchantID);
						$qr1 = $this->db->get();
						
						if($qr1->num_rows() >0)
						{  
							$this->db->where(array('TxnID' => $arr['TxnID'], 'TxnLineID' => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 'inv_itm_merchant_id' => $merchantID));
							$this->db->update('qb_test_invoice_lineitem',$lineitem);
						}else{
							$this->db->insert('qb_test_invoice_lineitem',$lineitem);
					   }
					
				}
			}
        
           $this->db->where(array('insertInvID'=>$ID));
           $this->db->update('tbl_custom_invoice', array('qb_status'=>5, 'TimeModified'=>date('Y-m-d H:i:s')));
           $this->db->where(array('invoiceDataID'=>$ID));
           $this->db->delete('tbl_subscription_invoice_item');
        
           $qinv = $this->db->select('*')->from('tbl_custom_invoice')->where(array('insertInvID'=>$ID))->get();
           if($qinv->num_rows>0)
           {
               $in_row =$qinv->row_array();
           
               $insert_data=array('invoiceID'=>$arr['TxnID'], 'cardID'=>$in_row['cardID'], 'paymentMethod'=>$in_row['paymentMethod'], 'gatewayID'=>$in_row['gatewayID'], 'merchantID'=>$in_row['merchantID'], 'updatedAt'=>date('Y-m-d H:i:s'), 'CreatedAt'=>date('Y-m-d H:i:s'));
             $this->db->insert('tbl_scheduled_invoice_payment',$insert_data);
           }
		   
		   	$transactionWhere = [
			   'merchantID' => $merchantID,
			   'customerListID'=>$arr['Customer_ListID'],
			   'invoiceTxnID'=>$ID
		   	];
             $query = $this->db->select('*')->from('customer_transaction')->where($transactionWhere)->get();
             if($query->num_rows()>0)
             {
                 $tr_data = $query->row_array();
                 if($tr_data['transactionCode']=='100'||$tr_data['transactionCode']=='200'||$tr_data['transactionCode']=='111'||$tr_data['transactionCode']=='1')
                 {
                      $this->db->where($transactionWhere);
                      $this->db->update('customer_transaction', array('invoiceTxnID'=>$arr['TxnID'])); 
                       $Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
        			   $Queue->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $tr_data['id'], '1','', $user);
                 }
                 
                 
             }
        
		}
	}
		return true; 
	}
	
    
    
    
    
    
	
	// Adding a payment 
	
	
	public function _addPaymentRequest($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		$query = $this->db->query("Select * from	customer_transaction where id='$ID' ");
		if ($query->num_rows() > 0) {
			$data    = $query->row_array();
			$query12 = $this->db->last_query();
			$txID    = $data['invoiceTxnID'];
			$query1  = $this->db->query("Select * from qb_test_invoice where TxnID='" . $txID . "'  ");
			$indata  = $query1->row_array();

			$crtxnID = '';
			if ($data['transactionType'] == 'Offline Payment' || strpos($data['transactionType'] , 'ECheck') !== false) {
				$tID     = $data['transactionID'];
				$tdte    = $data['transactionDate'];
				$pmethod = "Check";
			} else if (!empty(explode(',', $data['creditTxnID'])) && $data['creditTxnID'] != null) {

				$tID  = $data['transactionID'];
				$tdte = $data['transactionDate'];

				$crtxnID = explode(',', $data['creditTxnID']);
				$pmethod = "Credit";
			} else {
				if ($data['gateway'] == 'Stripe') {
					$tID = $data['id'];
				} else {
					$tID = $data['transactionID'];
				}

				$tdte    = $data['transactionDate'];
				$pmethod = "VISA";

			}


			$balance = sprintf('%0.2f', ($data['transactionAmount']));
		

			if (!empty(explode(',', $data['creditTxnID'])) && $data['creditTxnID'] != null) {
				$crAmounts = explode(',', $data['cr_amounts']);

				$xml = '<?xml version="1.0" encoding="utf-8"?>
					<?qbxml version="' . $version . '"?>
					<QBXML>
					<QBXMLMsgsRq onError="stopOnError">
					<ReceivePaymentAddRq requestID="' . $requestID . '">
					<ReceivePaymentAdd>
						<CustomerRef>
							<ListID>' . $indata['Customer_ListID'] . '</ListID>
							<FullName >' . $indata['Customer_FullName'] . '</FullName>
						</CustomerRef>
						<TxnDate>' . date('Y-m-d', strtotime($tdte)) . '</TxnDate>
						<TotalAmount>' . ($balance) . '</TotalAmount>
						<AppliedToTxnAdd>
								<TxnID >' . $indata['TxnID'] . '</TxnID>
								<SetCredit>';
				$i = 0;
				foreach ($crtxnID as $crd) {

					$xml .= '<CreditTxnID >' . $crd . '</CreditTxnID>
									<AppliedAmount >' . sprintf('%0.2f', $crAmounts[$i]) . '</AppliedAmount>';
					++$i;
				}
				$xml .= '</SetCredit>

								</AppliedToTxnAdd>';

				$xml .= '</ReceivePaymentAdd>
								</ReceivePaymentAddRq>
										</QBXMLMsgsRq>
										</QBXML>';
			} else {

				$xml = '<?xml version="1.0" encoding="utf-8"?>
					<?qbxml version="' . $version . '"?>
					<QBXML>
					<QBXMLMsgsRq onError="stopOnError">
					<ReceivePaymentAddRq requestID="' . $requestID . '">
					<ReceivePaymentAdd>
						<CustomerRef>
							<ListID>' . $indata['Customer_ListID'] . '</ListID>
							<FullName >' . $indata['Customer_FullName'] . '</FullName>
						</CustomerRef>
						<TxnDate>' . date('Y-m-d', strtotime($tdte)) . '</TxnDate>';
				$xml .= '<RefNumber>' . $tID . '</RefNumber>
						<TotalAmount>' . ($balance) . '</TotalAmount>
					<PaymentMethodRef>
						<FullName>' . $pmethod . '</FullName>
					</PaymentMethodRef>
					<AppliedToTxnAdd>
					<TxnID>' . $indata['TxnID'] . '</TxnID>
						<PaymentAmount>' . $balance . '</PaymentAmount>
						</AppliedToTxnAdd>

						</ReceivePaymentAdd>
					</ReceivePaymentAddRq>
				</QBXMLMsgsRq>
				</QBXML>';
			}
			
			return $this->_formatXMLRequest($xml); 
		}
	}			
	
	public function _addPaymentResponse($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
		// Do something here to record that the data was added to QuickBooks successfully 
		$Parser = new QuickBooks_XML_Parser($xml);
   
    	if ($Doc = $Parser->parse($errnum, $errmsg))
    	{
    	
		  $mID =  $this->db->query("Select merchantID from tbl_company where qbwc_username='$user' ")->row_array()['merchantID'];
		   
		  
            
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ReceivePaymentAddRs');

			$this->getStatusData($List, 'ReceivePaymentAdd');

		foreach ($List->children() as $Invoice)
			{
		
		      $txnListID  = $Invoice->getChildDataAt('ReceivePaymentRet TxnID');
		      
		      $sql= "update customer_transaction set qbListTxnID='".$txnListID."' where merchantID='".$mID."' and  id='$ID' and  (transactionCode IN('100','1','200','111')  
     or transactionType='Offline Payment' 
     or ((transactionType='AUTH ECheck'or transactionType='NMI ECheck')and transactionCode IN('100','1')))";
  	 $this->db->query($sql);  
            
               $this->db->where(array('paymentID'=>$ID));
               $this->db->update('tbl_qbd_invoice_payment_log', array('qb_status'=>5, 'TimeModified'=>date('Y-m-d H:i:s')));      
             
			}
        
        }else{
        
           $this->db->where(array('paymentID'=>$ID));
             $this->db->update('tbl_qbd_invoice_payment_log', array('qb_status'=>3, 'TimeModified'=>date('Y-m-d H:i:s')));        
        }
        
        }
    
		return true; 
	}
	

    
    
    
	
	/**
	 * Issue a request to QuickBooks to add a customer
	 */
	 
	 
    public function _addCustomerRequest($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Do something here to load data using your model
	
	      $query = $this->db->query("Select  c.*,cr.country_name as country_name, cr1.country_name as scountry_name,   comp.qbwc_username as user  from tbl_custom_customer c inner join tbl_company comp on c.companyID=comp.id  left join country cr on c.country= cr.country_id  left join country cr1 on c.ship_country= cr1.country_id  where 	customerID =$ID ");
	   
	     $data  =   $query->row_array();
	    
		
		// Build the qbXML request from data
     
		$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="' . $version . '"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<CustomerAddRq requestID="' . $requestID . '">
					<CustomerAdd>
					<Name>'.$data['fullName'].'</Name>
    				<CompanyName>'.$data['companyName'].'</CompanyName>
    				<FirstName>'.$data['firstName'].'</FirstName>
    				<LastName>'.$data['lastName'].'</LastName>
    				<BillAddress>
    				        	<Addr1>'.$data['address1'].'</Addr1>
    							<Addr2>'.$data['address2'].'</Addr2>
    							<City>'.$data['city'].'</City>
    							<State>'.$data['state'].'</State>
    							<PostalCode>'.$data['zipcode'].'</PostalCode>
    							<Country>'.$data['country'].'</Country>
    				</BillAddress>
                    <ShipAddress>
    				        	<Addr1>'.$data['ship_address1'].'</Addr1>
    							<Addr2>'.$data['ship_address2'].'</Addr2>
    							<City>'.$data['ship_city'].'</City>
    							<State>'.$data['ship_state'].'</State>
    							<PostalCode>'.$data['ship_zipcode'].'</PostalCode>
    							<Country>'.$data['ship_country'].'</Country>
    				</ShipAddress>
    				<Phone>'.$data['phoneNumber'].'</Phone>
    				<Email>'.$data['userEmail'].'</Email>
    		     	<Contact>'.$data['userEmail'].'</Contact>
					</CustomerAdd>
				</CustomerAddRq>
			</QBXMLMsgsRq>
		</QBXML>';

		
		return $this->_formatXMLRequest($xml); 
	}

	/**
	 * Handle a response from QuickBooks indicating a new customer has been added
	 */	
	public function _addCustomerResponse($requestID, $user, $action, $ID, $extr, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CustomerAddRs');

		$this->getStatusData($List, 'CustomerAdd');
	
		        $con = array('qbwc_username'=>$user);
    	 	    $query = $this->db->select('*')->from('tbl_company')->where($con)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query->num_rows()>0 ) {
					
    				$user_data    = $query->row_array();
    			    $username     = $user_data['qbwc_username'];
					$companyID    = $user_data['id'] ;
					$merchantID    = $user_data['merchantID'] ;
    			}
		
		foreach ($List->children() as $Customer)
		{   
		       $listID = $Customer->getChildDataAt('CustomerRet ListID');
		       		
			
		       $this->db->where(array('customerID'=>$ID));
		       
               $this->db->update('tbl_custom_customer', array('qb_status'=>5, 'updatedAt'=>date('Y-m-d H:i:s')));      
             
             
                  $query = $this->db->select('customListID')->from('tbl_custom_customer')->where(array('customerID'=>$ID))->get();
					
					if($query->num_rows()>0 ) 
					{
						$data = $query->row_array();
						
						if($data['customListID']!='' && $data['customListID']!=NULL)
						{
						       $this->db->where(array('ListID'=>$data['customListID'],'qbmerchantID'=>$merchantID));
		       
                              $this->db->update('qb_test_customer', array('ListID'=>$listID,'qb_status'=>'1', 'IsActive'=>'true'));    
						      
						      
                               //Update Customer ID for Customer Subscriptions
                              $this->db->where(array('customerID'=>$data['customListID'],'merchantDataID'=>$merchantID));
		       
                              $this->db->update('tbl_subscriptions', array('customerID'=>$listID));
                               //Update Customer ID for Customer Transactions
                               $this->db->where(array('customerListID'=>$data['customListID'],'merchantID'=>$merchantID));
		       
                              $this->db->update('customer_transaction', array('customerListID'=>$listID));  
                               //Update Customer Login for Customer  ID
                               $this->db->where(array('customerID'=>$data['customListID'],'merchantID'=>$merchantID));
		       
                              $this->db->update('tbl_customer_login', array('customerID'=>$listID)); 
                              //Update Customer Card Customer ID
                               $this->db1->where(array('customerListID'=>$data['customListID'],'merchantID'=>$merchantID));
                               $this->db1->update('customer_card_data', array('customerListID'=>$listID));  

							   $this->db->where(array('Customer_ListID'=>$data['customListID'],'qb_inv_merchantID'=>$merchantID));
                               $this->db->update('qb_test_invoice', array('Customer_ListID'=>$listID));  
                               
							   	$customInvWhere = [
									'merchantID' => $merchantID,
									'Customer_ListID' => $data['customListID'],
								];
								$this->db->where($customInvWhere);
                               	$this->db->update('tbl_custom_invoice', array('Customer_ListID'=>$listID));  

                          

							$QBD_Sync = new QBD_Sync($merchantID);
							$QBD_Sync->invoiceSync([
								'customerListId' => $listID
							]);
                               //Update Customer ID for Customer Invoices
						      
                               
						    
						}
					  
					} 		
        
        
        
        
		if($Customer->getChildDataAt('CustomerRet IsActive')=='true'){ $st='1'; }else{ $st='0'; }
		$fullName = $Customer->getChildDataAt('CustomerRet FullName');
			  if($fullName == '0'){
				$fullName = '';
			  }
			  $FirstName = $Customer->getChildDataAt('CustomerRet FirstName');
			  if($FirstName == '0'){
				$FirstName = '';
			  }
			  $LastName = $Customer->getChildDataAt('CustomerRet LastName');
			  if($LastName == '0'){
				$LastName = '';
			  }
			  $MiddleName = $Customer->getChildDataAt('CustomerRet MiddleName');
			  if($MiddleName == '0'){
				$MiddleName = '';
			  }
			  $Phone = $Customer->getChildDataAt('CustomerRet Phone');
			  if($Phone == '0'){
				$Phone = '';
			  }
			  $Email = $Customer->getChildDataAt('CustomerRet Email');
			  if($Email == '0'){
				$Email = '';
			  }
			$arr = array(
				'ListID' => $Customer->getChildDataAt('CustomerRet ListID'),
				'TimeCreated'  			 => date("Y-m-d H:i:s",strtotime($Customer->getChildDataAt('CustomerRet TimeCreated'))),
				'TimeModified' 			 => date("Y-m-d H:i:s",strtotime($Customer->getChildDataAt('CustomerRet TimeModified'))), 
				'CompanyName' 			 => $Customer->getChildDataAt('CustomerRet CompanyName'),
				'FullName'				 => ($Customer->getChildDataAt('CustomerRet FullName') != '0')?$Customer->getChildDataAt('CustomerRet FullName'):'',
				'FirstName'   			 => ($Customer->getChildDataAt('CustomerRet FirstName') != '0')?$Customer->getChildDataAt('CustomerRet FirstName'):'',
				'MiddleName' 			 => ($Customer->getChildDataAt('CustomerRet MiddleName') != '0')?$Customer->getChildDataAt('CustomerRet MiddleName'):'',
				'LastName' 				 => ($Customer->getChildDataAt('CustomerRet LastName') != '0')?$Customer->getChildDataAt('CustomerRet LastName'):'',
				'Contact' 				 => ($Customer->getChildDataAt('CustomerRet Email') != '0')?$Customer->getChildDataAt('CustomerRet Email'):'',
				'Phone'					 => ($Customer->getChildDataAt('CustomerRet Phone') != '0')?$Customer->getChildDataAt('CustomerRet Phone'):'',
				'BillingAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet BillAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr1'):'',
				'BillingAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet BillAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr2'):'',
				'BillingAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet BillAddress City') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress City'):'',
				'BillingAddress_State'		 => ($Customer->getChildDataAt('CustomerRet BillAddress State') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress State'):'',
				'BillingAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet BillAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Country'):'',
				'BillingAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet BillAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress PostalCode'):'',
                'ShipAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr1'):'',
				'ShipAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr2'):'',
				'ShipAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet ShipAddress City') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress City'):'',
				'ShipAddress_State'		 => ($Customer->getChildDataAt('CustomerRet ShipAddress State') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress State'):'',
				'ShipAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Country'):'',
				'ShipAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet ShipAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress PostalCode'):'',
				'EditSequence'            => $Customer->getChildDataAt('CustomerRet EditSequence'),
				'IsActive'                =>$Customer->getChildDataAt('CustomerRet IsActive'),
				'customerStatus'          => $st,
				'company_qb_username'     => $username,
				'companyID' 			  => $companyID,
				'qbmerchantID'           =>$merchantID, 
				);
				$this->db->select('*');
				$this->db->from('qb_test_customer');
				$this->db->where('ListID', $Customer->getChildDataAt('CustomerRet ListID'));
				$this->db->where('companyID', $companyID);
				$this->db->where('qbmerchantID', $merchantID);
				$qr1 = $this->db->get();
				
				if($qr1->num_rows() >0)
				{  
					$this->db->where(array('ListID' => $Customer->getChildDataAt('CustomerRet ListID'), 'companyID' => $companyID, 'qbmerchantID' => $merchantID));
					$this->db->update('qb_test_customer',$arr);
				}else{
					$this->db->insert('qb_test_customer',$arr);
				}
	
		}
              
              
	}
	      
    
	
		return true; 
	}
	
	
	
	
/********************First Time Import***********/


/**
 * Build a request to import invoices already in QuickBooks into our application
 */
 
public function _quickbooks_invoice_query_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
{
	// Iterator support (break the result set into small chunks)
	$attr_iteratorID = '';
	$attr_iterator = ' iterator="Start" ';
	if (empty($extra['iteratorID']))
	{
		// This is the first request in a new batch
		$last = $this->_quickbooks_get_last_run($user, $action);
		$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
		
		// Set the current run to $last
		$this->_quickbooks_set_current_run($user, $action, $last);
	}
	else
	{
		// This is a continuation of a batch
		$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
		$attr_iterator = ' iterator="Continue" ';
		
		$last = $this->_quickbooks_get_current_run($user, $action);
	}


	// Build the request
	$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="' . $version . '"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<InvoiceQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
					<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					<ModifiedDateRangeFilter>
						<FromModifiedDate>' . $last . '</FromModifiedDate>
					</ModifiedDateRangeFilter>
					
                      <PaidStatus >NotPaidOnly</PaidStatus>
					<IncludeLineItems>true</IncludeLineItems>
					<OwnerID>0</OwnerID>
				</InvoiceQueryRq>	
			</QBXMLMsgsRq>
		</QBXML>';
		
	return $xml;
}

/** 
 * Handle a response from QuickBooks 
 */
public function _quickbooks_invoice_query_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
{	
	if (!empty($idents['iteratorRemainingCount']))
	{
		// Queue up another request
		
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		$Queue->enqueue(QUICKBOOKS_IMPORT_INVOICE, null, QB_PRIORITY_INVOICE, array( 'iteratorID' => $idents['iteratorID'] ),$user);
	}
	
	// This piece of the response from QuickBooks is now stored in $xml. You 
	//	can process the qbXML response in $xml in any way you like. Save it to 
	//	a file, stuff it in a database, parse it and stuff the records in a 
	//	database, etc. etc. etc. 
	//	
	// The following example shows how to use the built-in XML parser to parse 
	//	the response and stuff it into a database. 
	
	// Import all of the records
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
	  
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   }
	if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/InvoiceQueryRs');

		$this->getStatusData($List, 'InvoiceQuery');
		
		foreach ($List->children() as $Invoice)
		{
			$arr = array(
				'TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'),
				'TimeCreated' => date("Y-m-d H:i:s", strtotime($Invoice->getChildDataAt('InvoiceRet TimeCreated'))),
				'TimeModified' =>date("Y-m-d H:i:s", strtotime( $Invoice->getChildDataAt('InvoiceRet TimeModified'))),
				'RefNumber' => $Invoice->getChildDataAt('InvoiceRet RefNumber'),
				'Customer_ListID' => $Invoice->getChildDataAt('InvoiceRet CustomerRef ListID'),
				'Customer_FullName' => $Invoice->getChildDataAt('InvoiceRet CustomerRef FullName'),
				'ShipAddress_Addr1' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr1'),
				'ShipAddress_Addr2' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr2'),
				'ShipAddress_City' => $Invoice->getChildDataAt('InvoiceRet BillAddress City'),
				'ShipAddress_State' => $Invoice->getChildDataAt('InvoiceRet BillAddress State'),
				'ShipAddress_Country' => $Invoice->getChildDataAt('InvoiceRet BillAddress Country'),
				'ShipAddress_PostalCode' => $Invoice->getChildDataAt('InvoiceRet BillAddress PostalCode'),
				'BalanceRemaining'  => $Invoice->getChildDataAt('InvoiceRet BalanceRemaining'),
				'DueDate'           => $Invoice->getChildDataAt('InvoiceRet DueDate'),
				'IsPaid'            => $Invoice->getChildDataAt('InvoiceRet IsPaid'),
				'EditSequence'      => $Invoice->getChildDataAt('InvoiceRet EditSequence'),
				'AppliedAmount'      => $Invoice->getChildDataAt('InvoiceRet AppliedAmount'),
               'TaxRate'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxPercentage'),
				'TotalTax'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxTotal'),
				'TaxListID'      => $Invoice->getChildDataAt('InvoiceRet ItemSalesTaxRef ListID'),
				'qb_inv_companyID' => $companyID,
				'qb_inv_merchantID' => $merchantID,
				);
        
				$invoiceStatus =$Invoice->getChildDataAt('InvoiceRet Memo');
				if ($invoiceStatus && strpos($invoiceStatus, 'VOID') !== false) {
					$invoiceStatus = 'cancel';
					$arr['IsPaid']= 'false';
				} else {
					$invoiceStatus = '';
				}
			
	$this->db->select('*');
	$this->db->from('qb_test_invoice');
	$this->db->where('TxnID', $Invoice->getChildDataAt('InvoiceRet TxnID'));
	$this->db->where('qb_inv_companyID', $companyID);
	$this->db->where('qb_inv_merchantID', $merchantID);
	$qr1 = $this->db->get();
	
	if($qr1->num_rows() >0)
	{  
		if(empty($invoiceStatus)){
			$invoiceStatus = $qr1->row_array()['userStatus'];
		} 
		$arr['userStatus']=$invoiceStatus;
		
		$this->db->where(array('TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'), 'qb_inv_companyID' => $companyID, 'qb_inv_merchantID' => $merchantID));
		$this->db->update('qb_test_invoice',$arr);
	}else{
		if(empty($invoiceStatus)){
			$invoiceStatus = 'Active';
		} 
		$arr['userStatus']=$invoiceStatus;
		
		$this->db->insert('qb_test_invoice',$arr);
   }
		
			
			// Remove any old line items
			$this->db->query("DELETE FROM qb_test_invoice_lineitem WHERE TxnID = '" . $this->db->escape_str($arr['TxnID']) . "' and inv_itm_merchant_id = '" . $merchantID . "' ") ;
			
			
			// Process the line items
			foreach ($Invoice->children() as $Child)
			{
				if ($Child->name() == 'InvoiceLineRet')
				{
					$InvoiceLine = $Child;
					if($InvoiceLine->getChildDataAt('InvoiceLineRet SalesTaxCodeRef FullName') == 'Tax'){
						$tax = 1;
					}
					else{
						$tax = null;
					}
					if($InvoiceLine->getChildDataAt('InvoiceLineRet Quantity')){
						$qunt = $InvoiceLine->getChildDataAt('InvoiceLineRet Quantity');
					}
					else{
						$qunt = 1;
					}
					$lineitem = array( 
						'TxnID'          => $arr['TxnID'], 
						'TxnLineID'      => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 
						'Item_ListID'    => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef ListID'), 
						'Item_FullName'  => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef FullName'), 
						'Descrip'        => $InvoiceLine->getChildDataAt('InvoiceLineRet Desc'), 
						'Quantity'       => $qunt,
						'Rate'           => $InvoiceLine->getChildDataAt('InvoiceLineRet Rate'),
						'item_tax' => $tax,
						'inv_itm_merchant_id' => $merchantID
						);
						$this->db->select('*');
						$this->db->from('qb_test_invoice_lineitem');
						$this->db->where('TxnID', $arr['TxnID']);
						$this->db->where('TxnLineID', $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'));
						$this->db->where('inv_itm_merchant_id', $merchantID);
						$qr1 = $this->db->get();
						
						if($qr1->num_rows() >0)
						{  
							$this->db->where(array('TxnID' => $arr['TxnID'], 'TxnLineID' => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 'inv_itm_merchant_id' => $merchantID));
							$this->db->update('qb_test_invoice_lineitem',$lineitem);
						}else{
							$this->db->insert('qb_test_invoice_lineitem',$lineitem);
					   }
					
					
				}
			}
		}
	}
	
	return true;
}	
	
	




/****************END******************/	
		
	


/**
 * Build a request to import invoices already in QuickBooks into our application
 */
 
public function _quickbooks_invoice_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
{
	// Iterator support (break the result set into small chunks)
	$attr_iteratorID = '';
	$attr_iterator = ' iterator="Start" ';
	if (empty($extra['iteratorID']))
	{
		// This is the first request in a new batch
		$last = $this->_quickbooks_get_last_run($user, $action);
		$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
		
		// Set the current run to $last
		$this->_quickbooks_set_current_run($user, $action, $last);
	}
	else
	{
		// This is a continuation of a batch
		$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
		$attr_iterator = ' iterator="Continue" ';
		
		$last = $this->_quickbooks_get_current_run($user, $action);
	}


	// Build the request
	$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="' . $version . '"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
				<InvoiceQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
					<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					<ModifiedDateRangeFilter>
						<FromModifiedDate>' . $last . '</FromModifiedDate>
					</ModifiedDateRangeFilter>
					<IncludeLineItems>true</IncludeLineItems>
					<OwnerID>0</OwnerID>
				</InvoiceQueryRq>	
			</QBXMLMsgsRq>
		</QBXML>';

	return $xml;
}

/** 
 * Handle a response from QuickBooks 
 */
public function _quickbooks_invoice_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
{	
	if (!empty($idents['iteratorRemainingCount']))
	{
		// Queue up another request
		
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		$Queue->enqueue(QUICKBOOKS_IMPORT_INVOICE, null, QB_PRIORITY_INVOICE, array( 'iteratorID' => $idents['iteratorID'] ),$user);
	}
	
	
	
	// Import all of the records
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	$con = array('qbwc_username'=>$user);
		$query = $this->db->select('*')->from('tbl_company')->where($con)->get();
   
	   if($query->num_rows()>0 ) {
		   
		   $user_data    = $query->row_array();
		   $username     = $user_data['qbwc_username'];
		   $companyID    = $user_data['id'] ;
		   $merchantID    = $user_data['merchantID'] ;
	   }
	if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/InvoiceQueryRs');

		$this->getStatusData($List, 'InvoiceQuery');
		
		foreach ($List->children() as $Invoice)
		{
        
       
			$arr = array(
				'TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'),
				'TimeCreated' => date("Y-m-d H:i:s",strtotime($Invoice->getChildDataAt('InvoiceRet TimeCreated'))),
				'TimeModified' => date("Y-m-d H:i:s",strtotime($Invoice->getChildDataAt('InvoiceRet TimeModified'))),
				'RefNumber' => $Invoice->getChildDataAt('InvoiceRet RefNumber'),
				'Customer_ListID' => $Invoice->getChildDataAt('InvoiceRet CustomerRef ListID'),
				'Customer_FullName' => $Invoice->getChildDataAt('InvoiceRet CustomerRef FullName'),
				'ShipAddress_Addr1' => $Invoice->getChildDataAt('InvoiceRet ShipAddress Addr1'),
				'ShipAddress_Addr2' => $Invoice->getChildDataAt('InvoiceRet ShipAddress Addr2'),
				'ShipAddress_City' => $Invoice->getChildDataAt('InvoiceRet ShipAddress City'),
				'ShipAddress_State' => $Invoice->getChildDataAt('InvoiceRet ShipAddress State'),
				'ShipAddress_Country' => $Invoice->getChildDataAt('InvoiceRet ShipAddress Country'),
				'ShipAddress_PostalCode' => $Invoice->getChildDataAt('InvoiceRet ShipAddress PostalCode'),
				'Billing_Addr1' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr1'),
				'Billing_Addr2' => $Invoice->getChildDataAt('InvoiceRet BillAddress Addr2'),
				'Billing_City' => $Invoice->getChildDataAt('InvoiceRet BillAddress City'),
				'Billing_State' => $Invoice->getChildDataAt('InvoiceRet BillAddress State'),
				'Billing_Country' => $Invoice->getChildDataAt('InvoiceRet BillAddress Country'),
				'Billing_PostalCode' => $Invoice->getChildDataAt('InvoiceRet BillAddress PostalCode'),
				'BalanceRemaining'  => $Invoice->getChildDataAt('InvoiceRet BalanceRemaining'),
				'DueDate'           => $Invoice->getChildDataAt('InvoiceRet DueDate'),
				'IsPaid'            => $Invoice->getChildDataAt('InvoiceRet IsPaid'),
				'EditSequence'      => $Invoice->getChildDataAt('InvoiceRet EditSequence'),
				'AppliedAmount'      => $Invoice->getChildDataAt('InvoiceRet AppliedAmount'),
               'TaxRate'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxPercentage'),
				'TotalTax'      => $Invoice->getChildDataAt('InvoiceRet SalesTaxTotal'),
				'TaxListID'      => $Invoice->getChildDataAt('InvoiceRet ItemSalesTaxRef ListID'),   
				'qb_inv_companyID' => $companyID,
				'qb_inv_merchantID' => $merchantID,
			);

			$invoiceStatus =$Invoice->getChildDataAt('InvoiceRet Memo');
			if ($invoiceStatus && strpos($invoiceStatus, 'VOID') !== false) {
				$invoiceStatus = 'cancel';
				$arr['IsPaid']= 'false';
			} else {
				$invoiceStatus = '';
			}
        
			$this->db->select('*');
			$this->db->from('qb_test_invoice');
			$this->db->where('TxnID', $Invoice->getChildDataAt('InvoiceRet TxnID'));
			$this->db->where('qb_inv_companyID', $companyID);
			$this->db->where('qb_inv_merchantID', $merchantID);
			$qr1 = $this->db->get();
			
			if($qr1->num_rows() >0)
			{
				if(empty($invoiceStatus)){
					$invoiceStatus = $qr1->row_array()['userStatus'];
				} 
				$arr['userStatus']=$invoiceStatus;

				$this->db->where(array('TxnID' => $Invoice->getChildDataAt('InvoiceRet TxnID'), 'qb_inv_companyID' => $companyID, 'qb_inv_merchantID' => $merchantID));
				$this->db->update('qb_test_invoice',$arr);
			}else{
				if(empty($invoiceStatus)){
					$invoiceStatus = 'Active';
				} 
				$arr['userStatus']=$invoiceStatus;
				$this->db->insert('qb_test_invoice',$arr);
			}
	
			
				$this->db->db_debug = false;
			   
			   $eeoer = $this->db->_error_message();
			   
			   	QuickBooks_Utilities::log(QB_QUICKBOOKS_DSN, 'Importing INVOICE ERROR>>>>>>>>>>>>>>>' .  ': ' . print_r($eeoer, true));
			// Remove any old line items
			$this->db->query("DELETE FROM qb_test_invoice_lineitem WHERE TxnID = '" . $this->db->escape_str($arr['TxnID']) . "' and inv_itm_merchant_id = '" . $merchantID . "' ") ;
			
			// Process the line items
			foreach ($Invoice->children() as $Child)
			{
				if ($Child->name() == 'InvoiceLineRet')
				{
					$InvoiceLine = $Child;
					
					if($InvoiceLine->getChildDataAt('InvoiceLineRet SalesTaxCodeRef FullName') == 'Tax'){
						$tax = 1;
					}
					else{
						$tax = null;
					}
					if($InvoiceLine->getChildDataAt('InvoiceLineRet Quantity')){
						$qunt = $InvoiceLine->getChildDataAt('InvoiceLineRet Quantity');
					}
					else{
						$qunt = 1;
					}
					$lineitem = array( 
						'TxnID'          => $arr['TxnID'], 
						'TxnLineID'      => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 
						'Item_ListID'    => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef ListID'), 
						'Item_FullName'  => $InvoiceLine->getChildDataAt('InvoiceLineRet ItemRef FullName'), 
						'Descrip'        => $InvoiceLine->getChildDataAt('InvoiceLineRet Desc'), 
						'Quantity'       => $qunt,
						'Rate'           => $InvoiceLine->getChildDataAt('InvoiceLineRet Rate'),
						'item_tax' => $tax,
						'inv_itm_merchant_id' => $merchantID
						);
						$this->db->select('*');
						$this->db->from('qb_test_invoice_lineitem');
						$this->db->where('TxnID', $arr['TxnID']);
						$this->db->where('TxnLineID', $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'));
						$this->db->where('inv_itm_merchant_id', $merchantID);
						$qr1 = $this->db->get();
						
						if($qr1->num_rows() >0)
						{  
							$this->db->where(array('TxnID' => $arr['TxnID'], 'TxnLineID' => $InvoiceLine->getChildDataAt('InvoiceLineRet TxnLineID'), 'inv_itm_merchant_id' => $merchantID));
							$this->db->update('qb_test_invoice_lineitem',$lineitem);
						}else{
							$this->db->insert('qb_test_invoice_lineitem',$lineitem);
					   }
					
						
				}
			}
		}
	}
	
	return true;
}
				
	
	
	
	
/**
 * Build a request to import customers already in QuickBooks into our application
 */
public function _quickbooks_customer_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
{
	// Iterator support (break the result set into small chunks)
	$attr_iteratorID = '';
	$attr_iterator = ' iterator="Start" ';
	if (empty($extra['iteratorID']))
	{
		// This is the first request in a new batch
		$last = $this->_quickbooks_get_last_run($user, $action);
	
		$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
		
		// Set the current run to $last
		$this->_quickbooks_set_current_run($user, $action, $last);
	}
	else
	{
		// This is a continuation of a batch
		$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
		$attr_iterator   = ' iterator="Continue" ';
		
		$last = $this->_quickbooks_get_current_run($user, $action);
	}
	
	// Build the request
	$xml = '<?xml version="1.0" encoding="utf-8"?>
		<?qbxml version="' . $version . '"?>
		<QBXML>
			<QBXMLMsgsRq onError="stopOnError">
			<CustomerQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
					<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					<FromModifiedDate>' . $last . '</FromModifiedDate>
					<OwnerID>0</OwnerID>
				</CustomerQueryRq>	
			</QBXMLMsgsRq>
		</QBXML>';
		
	return $xml;   
}

/** 
 * Handle a response from QuickBooks 
 */
public function _quickbooks_customer_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
{	
	if (!empty($idents['iteratorRemainingCount']))
	{
		// Queue up another request
		
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		$Queue->enqueue(QUICKBOOKS_IMPORT_CUSTOMER, null, '', array( 'iteratorID' => $idents['iteratorID'] ),$user);
	}

	
	// Import all of the records
	$errnum = 0;
	$errmsg = '';
	$Parser = new QuickBooks_XML_Parser($xml);
	
	if ($Doc = $Parser->parse($errnum, $errmsg))
	{
		$Root = $Doc->getRoot();
		$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CustomerQueryRs');

		$this->getStatusData($List, 'CustomerQuery');
	
		        $con = array('qbwc_username'=>$user);
    	 	    $query = $this->db->select('*')->from('tbl_company')->where($con)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query->num_rows()>0 ) {
					
    				$user_data    = $query->row_array();
    			    $username     = $user_data['qbwc_username'];
					$companyID    = $user_data['id'] ;
					$merchantID    = $user_data['merchantID'] ;
    			}
		
		foreach ($List->children() as $Customer)
		{   
			  if($Customer->getChildDataAt('CustomerRet IsActive')=='true'){ $st='1'; }else{ $st='0'; }
			  
			$arr = array(
				'ListID' => $Customer->getChildDataAt('CustomerRet ListID'),
				'TimeCreated'  			 => date("Y-m-d H:i:s", strtotime($Customer->getChildDataAt('CustomerRet TimeCreated'))),
				'TimeModified' 			 => date("Y-m-d H:i:s", strtotime($Customer->getChildDataAt('CustomerRet TimeModified'))), 
				'CompanyName' 			 => $Customer->getChildDataAt('CustomerRet CompanyName'),
				'FullName'				 => ($Customer->getChildDataAt('CustomerRet FullName') != '0')?$Customer->getChildDataAt('CustomerRet FullName'):'',
				'FirstName'   			 => ($Customer->getChildDataAt('CustomerRet FirstName') != '0')?$Customer->getChildDataAt('CustomerRet FirstName'):'',
				'MiddleName' 			 => ($Customer->getChildDataAt('CustomerRet MiddleName') != '0')?$Customer->getChildDataAt('CustomerRet MiddleName'):'',
				'LastName' 				 => ($Customer->getChildDataAt('CustomerRet LastName') != '0')?$Customer->getChildDataAt('CustomerRet LastName'):'',
				'Contact' 				 => ($Customer->getChildDataAt('CustomerRet Email') != '0')?$Customer->getChildDataAt('CustomerRet Email'):'',
				'Phone'					 => ($Customer->getChildDataAt('CustomerRet Phone') != '0')?$Customer->getChildDataAt('CustomerRet Phone'):'',
				'BillingAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet BillAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr1'):'',
				'BillingAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet BillAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Addr2'):'',
				'BillingAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet BillAddress City') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress City'):'',
				'BillingAddress_State'		 => ($Customer->getChildDataAt('CustomerRet BillAddress State') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress State'):'',
				'BillingAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet BillAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress Country'):'',
				'BillingAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet BillAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet BillAddress PostalCode'):'',
                'ShipAddress_Addr1'      => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr1') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr1'):'',
				'ShipAddress_Addr2' 	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Addr2') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Addr2'):'',
				'ShipAddress_City' 	     => ($Customer->getChildDataAt('CustomerRet ShipAddress City') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress City'):'',
				'ShipAddress_State'		 => ($Customer->getChildDataAt('CustomerRet ShipAddress State') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress State'):'',
				'ShipAddress_Country'	 => ($Customer->getChildDataAt('CustomerRet ShipAddress Country') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress Country'):'',
				'ShipAddress_PostalCode' => ($Customer->getChildDataAt('CustomerRet ShipAddress PostalCode') != '0')?$Customer->getChildDataAt('CustomerRet ShipAddress PostalCode'):'',
					'EditSequence'      => $Customer->getChildDataAt('CustomerRet EditSequence'),
				'IsActive'              =>$Customer->getChildDataAt('CustomerRet IsActive'),
				'customerStatus'          => $st,
				'company_qb_username'    => $username,
				'companyID' 			 => $companyID,
				'qbmerchantID'           => $merchantID,
				);
				$this->db->select('*');
				$this->db->from('qb_test_customer');
				$this->db->where('ListID', $Customer->getChildDataAt('CustomerRet ListID'));
				$this->db->where('companyID', $companyID);
				$this->db->where('qbmerchantID', $merchantID);
				$qr1 = $this->db->get();
				
				if($qr1->num_rows() >0)
				{  
					$this->db->where(array('ListID' => $Customer->getChildDataAt('CustomerRet ListID'), 'companyID' => $companyID, 'qbmerchantID' => $merchantID));
					$this->db->update('qb_test_customer',$arr);
				}else{
					$this->db->insert('qb_test_customer',$arr);
					
				
				}
	   
		}
	}
	
	return true;
}

	
	
	



		
	/**
	 * Build a request to import customers already in QuickBooks into our application
	 */
	public function _quickbooks_received_payment_query_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
	$attr_iteratorID = '';
	$attr_iterator = ' iterator="Start" ';
	if (empty($extra['iteratorID']))
	{
		// This is the first request in a new batch
		$last = $this->_quickbooks_get_last_run($user, $action);
		$this->_quickbooks_set_last_run($user, $action);		
		// Update the last run time to NOW()
		
		// Set the current run to $last
		$this->_quickbooks_set_current_run($user, $action, $last);
	}
	else
	{
		// This is a continuation of a batch
		$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
		$attr_iterator = ' iterator="Continue" ';
		
		$last = $this->_quickbooks_get_current_run($user, $action);
	}
		
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
            <QBXMLMsgsRq onError="stopOnError">
           
					
                     <ReceivePaymentQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . '  requestID="' . $requestID . '">
                    <MaxReturned>100</MaxReturned>
					<ModifiedDateRangeFilter>
						<FromModifiedDate>'.$last.'</FromModifiedDate>
					</ModifiedDateRangeFilter>
                     <IncludeLineItems>true</IncludeLineItems>
                    
				
				</ReceivePaymentQueryRq>
				</QBXMLMsgsRq>
			</QBXML>';
			
		return $xml;
	}

	/** 
	 * Handle a response from QuickBooks 
	 */
	public function _quickbooks_received_payment_query_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
	
		if (!empty($idents['iteratorRemainingCount']))
	{
		// Queue up another request
		
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		$Queue->enqueue(QUICKBOOKS_IMPORT_RECEIVEPAYMENT, null, QB_PRIORITY_RECEIVEPAYMENT, array( 'iteratorID' => $idents['iteratorID'] ),$user);
	}
	
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ReceivePaymentQueryRs');

			$this->getStatusData($List, 'ReceivePaymentQuery');
		
			foreach ($List->children() as $Invoice)
			{
				
			
			
				
			  $txnID = $Invoice->getChildDataAt('ReceivePaymentRet AppliedToTxnRet TxnID');
		      $listID  = $Invoice->getChildDataAt('ReceivePaymentRet CustomerRef ListID');
			
		 $mr_data =  $this->db->query("Select m.merchID,m.resellerID   from qb_test_customer  c inner join tbl_company cm on cm.id=c.companyID inner join tbl_merchant_data m on m.merchID=cm.merchantID where c.ListID='".$listID."' ")->row_array();
            
            
                      $qe ="Select * from  qb_test_invoice where  TxnID='".$txnID."' ";

			  $qr  =  $this->db->query($qe);
            if($qr->num_rows()>0)
            {

                         $tr_data =$qr->row_array();
                   $q1 =$this->db->query("Select * from customer_transaction where InvoiceTxnID='".$txnID."'  and ((transactionCode in('100','1','200','111') and transactionType in ('sale','capture', 'pay_sale','pay_capture','stripe_sale','stripe_capture','auth_capture','Paypal_sale','Paypal_capture','prior_auth_capture') and  transaction_user_status!='refund') or (transactionType='Offline Payment' or transactionType='Credit Payment') ) "); 
          
                 if($q1->num_rows() >0 )
                 {
                 
                    $inv_data =$q1->row_array();
                    $id = $inv_data['id'];
                    $arr = array( 
                    
                     'transactionModified' => date('Y-m-d H:i:s', strtotime($Invoice->getChildDataAt('ReceivePaymentRet TimeModified'))),
                    'qbListTxnID'=>$Invoice->getChildDataAt('ReceivePaymentRet TxnID')
                   );		
                       $this->db->where(array('id'=>$id));
                    $this->db->update('customer_transaction', $arr);
              
            
                  
                 }
                 else 
                 {
                     $arr = array(
                        'transactionDate' => date('Y-m-d H:i:s', strtotime($Invoice->getChildDataAt('ReceivePaymentRet TimeCreated'))),

                        'transactionID' => $Invoice->getChildDataAt('ReceivePaymentRet RefNumber'),
                        'transactionStatus'=> 'SUCCESS',
                        'transactionModified' => date('Y-m-d H:i:s', strtotime($Invoice->getChildDataAt('ReceivePaymentRet TimeModified'))),
                        'transactionAmount'=>$Invoice->getChildDataAt('ReceivePaymentRet TotalAmount'),
                        'transactionCode' =>'100',
                        'transactionType' => $Invoice->getChildDataAt('ReceivePaymentRet PaymentMethodRef FullName'),
                        'customerListID'     =>$listID,
                         'gatewayID'        =>0,
                         'invoiceTxnID'    =>$txnID,				
                         'transactionCard' =>0,
                      
                         'transaction_user_status'=>'',
                         'transactionGateway'=>0,
                         'merchantID'=>$mr_data['merchID'],
                         'resellerID'=>$mr_data['resellerID'],
                         'qbListTxnID'=>$Invoice->getChildDataAt('ReceivePaymentRet TxnID'),
                         'transaction_by_user_type' => 1,
                         'transaction_by_user_id' => $mr_data['merchID']   
                        );
              
                        $this->db->insert('customer_transaction', $arr);	
            
               
                 }			 
      
            }
           
        
			
			
			}	
        
        }
    
     
		return true;
			
		
	}
		

	
	
	
	/**
	 * Catch and handle errors from QuickBooks
	 */		
	public function _catchallErrors($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
	{
		return false;
	}
	
	/**
	 * Whenever the Web Connector connects, do something (e.g. queue some stuff up if you want to)
	 */
	public function _loginSuccess($requestID, $user, $hook, &$err, $hook_data, $callback_config)
	{

	
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
	    $date = '1983-06-15 12:01:01';
	


  
	if (! ($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_SALESTAXITEM)))
	{
		$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_SALESTAXITEM, $date);
	}
	if (! ($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_CUSTOMER)))
	{
		$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_CUSTOMER, $date);
	}

	
   if (! ($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_ITEM)))
	{
	$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_ITEM, $date);
	}
 	// Comment
	
	// Set up the invoice imports
	$qbqry =$this->db->query('Select count(1) as inv_count from qb_test_invoice inv inner join tbl_company cmp on cmp.merchantID= qb_inv_merchantID where qbwc_username="'.$user.'" ');
	    if($qbqry){
			$queryData = $qbqry->row_array();
			if($queryData && $queryData['inv_count'] > 0){
				if (!	($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_INVOICE)))
				{
					$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_INVOICE, $date);
				}
				$Queue->enqueue(QUICKBOOKS_IMPORT_INVOICE, 1, QB_PRIORITY_INVOICE,'', $user);
			}else{
				$date = date('Y-m-d') . 'T' . date('H:i:s');
				$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, 1, QB_PRIORITY_INVOICE,'', $user);
				$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_INVOICE, $date);
			}
		}else{
			$date = date('Y-m-d') . 'T' . date('H:i:s');
			$Queue->enqueue(QUICKBOOKS_QUERY_INVOICE, 1, QB_PRIORITY_INVOICE,'', $user);
			$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_INVOICE, $date);
		}

	
    
      if (! ($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_ACCOUNT)))
	{
	$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_ACCOUNT, $date);
	}
    
    
      if (! ($this->_quickbooks_get_last_run($user, QUICKBOOKS_IMPORT_CREDITMEMO)))
	{
	$this->_quickbooks_set_last_run($user, QUICKBOOKS_IMPORT_CREDITMEMO, $date);
	}
	
    
 
	$Queue->enqueue(QUICKBOOKS_IMPORT_CUSTOMER, 1, '','', $user);
	$Queue->enqueue(QUICKBOOKS_IMPORT_SALESTAXITEM, 1, 5,'', $user);
	$Queue->enqueue(QUICKBOOKS_IMPORT_ITEM, 1, QB_PRIORITY_ITEM,'', $user);
    $Queue->enqueue(QUICKBOOKS_IMPORT_ACCOUNT, 1, 10,'', $user);
   	$Queue->enqueue(QUICKBOOKS_IMPORT_CREDITMEMO, 1, 1,'', $user);
  
	}
	
	
		
		
	/**
	 * Get the last date/time the QuickBooks sync ran
	 * 
	 * @param string $user		The web connector username 
	 * @return string			A date/time in this format: "yyyy-mm-dd hh:ii:ss"
	 */
	public function _quickbooks_get_last_run($user, $action)
	{
		$type = null;
		$opts = null;
		return QuickBooks_Utilities::configRead(QB_QUICKBOOKS_DSN, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_LAST . '-' . $action, $type, $opts);
	}

	/**
	 * Set the last date/time the QuickBooks sync ran to NOW
	 * 
	 * @param string $user
	 * @return boolean
	 */
	 
public	 function _quickbooks_set_last_run($user, $action, $force = null)
{
	$value = date('Y-m-d') . 'T' . date('H:i:s');
	if ($force)
	{
		$value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
	}

	
	
	return QuickBooks_Utilities::configWrite(QB_QUICKBOOKS_DSN, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_LAST . '-' . $action, $value);
}
	 
	
	/**
	 * 
	 * 
	 */
	public function _quickbooks_get_current_run($user, $action)
	{
		$type = null;
		$opts = null;
		return QuickBooks_Utilities::configRead(QB_QUICKBOOKS_DSN, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_CURR . '-' . $action, $type, $opts);	
	}

	/**
	 * 
	 * 
	 */
	public function _quickbooks_set_current_run($user, $action, $force = null)
	{
		$value = date('Y-m-d') . 'T' . date('H:i:s');
		
		if ($force)
		{
			$value = date('Y-m-d', strtotime($force)) . 'T' . date('H:i:s', strtotime($force));
		}
		
		return QuickBooks_Utilities::configWrite(QB_QUICKBOOKS_DSN, $user, md5(__FILE__), QB_QUICKBOOKS_CONFIG_CURR . '-' . $action, $value);	
	}

		
	/**
	* Build a request to import Account already in QuickBooks into our application
	*/
	public function _quickbooks_account_create_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		$account_data =  $this->db->query("Select * from qb_item_account where acc_id='$ID'")->row_array();

		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
					<?qbxml version="' . $version . '"?>
					<QBXML>
					        <QBXMLMsgsRq onError="stopOnError">
					                <AccountAddRq>
					                        <AccountAdd>
					                                <Name>'.$account_data['Name'].'</Name>
					                                <AccountType>'.$account_data['AccountType'].'</AccountType>
					                        </AccountAdd>
					                </AccountAddRq>
					        </QBXMLMsgsRq>
					</QBXML>';
		return $xml;
	}


	public function _quickbooks_account_create_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		echo 'asd';die;
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		if (!empty($idents['iteratorRemainingCount']))
		{
			// Queue up another request
			$Queue->enqueue(QUICKBOOKS_IMPORT_ACCOUNT, null, QB_PRIORITY_ITEM, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		// get account
		$account_data = $this->db->select('*')->from('qb_item_account')->where('acc_id', $ID)->get()->row_array();
		// delete demo account create for Sync plan
		$this->db->query("DELETE FROM qb_item_account WHERE acc_id = '" . $ID . "'");

		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/AccountAddRs');
			
	        $con1 = array('qbwc_username'=>$user);
			$query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
			$lastquery = $this->db->last_query();
			$mID  =0;
			if($query1->num_rows()>0 ) {
				
				$user_data    = $query1->row_array();
				
				$companyID    = $user_data['id'];
					$mID    = $user_data['merchantID'];
			}
		
			
			foreach ($List->children() as $Item)
			{
				$listID = $Item->getChildDataAt('AccountRet ListID');
				$arr = array(
					'ListID' => $listID,
					'EditSequence'=>$Item->getChildDataAt('AccountRet EditSequence'),
					'IsActive'=>$Item->getChildDataAt('AccountRet IsActive'),
				    'company' =>$companyID,
					'TimeCreated' => date('Y-m-d H:i:s', strtotime($Item->getChildDataAt('AccountRet TimeCreated'))),
					'TimeModified' =>  date('Y-m-d H:i:s', strtotime($Item->getChildDataAt('AccountRet TimeModified'))),
					'Name' => $Item->getChildDataAt('AccountRet Name'),
					'FullName' => $Item->getChildDataAt('AccountRet FullName'),
					'AccountType' => $Item->getChildDataAt('AccountRet AccountType'), 
					'ParentListID' => $Item->getChildDataAt('AccountRet ParentRef ListID'),
					'ParentFullName' => $Item->getChildDataAt('AccountRet ParentRef FullName'),
					'Sublevel' => $Item->getChildDataAt('AccountRet Sublevel'),
					'SpecialAccountType' => $Item->getChildDataAt('AccountRet SpecialAccountType'),
					'AccountNumber' => $Item->getChildDataAt('AccountRet AccountNumber'),
					'BankNumber' => $Item->getChildDataAt('AccountRet BankNumber'),
					'Description' => $Item->getChildDataAt('AccountRet Desc'),
					'Balance' => $Item->getChildDataAt('AccountRet Balance'),
					'TotalBalance' => $Item->getChildDataAt('AccountRet TotalBalance'),
					'TaxLineID' => $Item->getChildDataAt('AccountRet TaxLineInfoRet TaxLineID'),
					'TaxLineName' => $Item->getChildDataAt('AccountRet TaxLineInfoRet TaxLineName'),
					'qbAccountmerchantID'=>$mID,
					
				);
				
				//	QuickBooks_Utilities::log(QB_QUICKBOOKS_DSN, 'Importing  Account Data : ' . print_r($arr, true));
				$this->db->select('*');
				$this->db->from('qb_item_account');
				$this->db->where('ListID', $listID);
				$this->db->where('company', $companyID);
				$this->db->where('qbAccountmerchantID', $mID);
				$qr1 = $this->db->get();
				
					
				if($qr1->num_rows() >0)
				{  
					$this->db->where(array('ListID' => $listID, 'company' => $companyID, 'qbAccountmerchantID' => $mID));
					$this->db->update('qb_item_account',$arr);
				}else{
					$this->db->insert('qb_item_account',$arr);
				}

				if($account_data){
					$tip_where = [
						'AccountRef' => $account_data['ListID'],
						'merchantID' => $mID,
						'companyID' => $companyID,
					];
					$tip_products = $this->db->select('*')->from('tbl_custom_product')->where($tip_where)->get()->result_array();
					// update in tbl_custom_product
					$this->db->where($tip_where);
					$this->db->update('tbl_custom_product', ['AccountRef' => $listID]);
	
					// update in qb_test_item
					$this->db->where(['AccountRef' => $account_data['ListID'], 'companyListID' => $companyID]);
					$this->db->update('qb_test_item', ['AccountRef' => $listID]);
	
					// update in sync_settings
					if($account_data['AccountType'] == 'Income' && $account_data['Name'] == 'Surcharge Income'){
						// update in tbl_merchant_surcharge
						$this->db->where('merchantID', $mID);
						$this->db->update('tbl_merchant_surcharge', ['defaultItemAccount' => $listID]);
					}elseif($account_data['AccountType'] == 'Income'){
						$this->db->where('merchant_id', $mID);
						$this->db->update('tbl_pos_sync_settings', ['default_account_id' => $listID]);
					} else if($account_data['AccountType'] == 'OtherCurrentLiability'){
						$this->db->where('merchant_id', $mID);
						$this->db->update('tbl_pos_sync_settings', ['default_tip_account_id' => $listID]);
					}


					if(!empty($tip_products)){
						foreach($tip_products as $tip_product){
							$Queue->enqueue(QUICKBOOKS_ADD_SERVICEITEM,  $tip_product['productID'], '1', '', $user);
						}
					}
					
				}
			}
		}
		
		
		return true;
	}
   
	/**
	 * Build a request to import Account already in QuickBooks into our application
	 */
	public function _quickbooks_account_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		
		
		$last = $this->_quickbooks_get_current_run($user, $action);
		$value = date('Y-m-d') . 'T' . date('H:i:s');
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<AccountQueryRq  requestID="' . $requestID . '">
					
						<FromModifiedDate>' . $last . '</FromModifiedDate>
                        
                        <ToModifiedDate >'.$value.'</ToModifiedDate>
				
					</AccountQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
		return $xml;
	}


	public function _quickbooks_account_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			// Queue up another request
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_IMPORT_ACCOUNT, null, QB_PRIORITY_ITEM, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/AccountQueryRs');

			$this->getStatusData($List, 'AccountQuery');
			
	        $con1 = array('qbwc_username'=>$user);
			$query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
			$lastquery = $this->db->last_query();
			$mID  =0;
			if($query1->num_rows()>0 ) {
				
				$user_data    = $query1->row_array();
				
				$companyID    = $user_data['id'];
					$mID    = $user_data['merchantID'];
			}
		
			
			foreach ($List->children() as $Item)
			{
				$arr = array(
					'ListID' => $Item->getChildDataAt('AccountRet ListID'),
					'EditSequence'=>$Item->getChildDataAt('AccountRet EditSequence'),
					'IsActive'=>$Item->getChildDataAt('AccountRet IsActive'),
				    'company' =>$companyID,
					'TimeCreated' => date('Y-m-d H:i:s', strtotime($Item->getChildDataAt('AccountRet TimeCreated'))),
					'TimeModified' =>  date('Y-m-d H:i:s', strtotime($Item->getChildDataAt('AccountRet TimeModified'))),
					'Name' => $Item->getChildDataAt('AccountRet Name'),
					'FullName' => $Item->getChildDataAt('AccountRet FullName'),
					'AccountType' => $Item->getChildDataAt('AccountRet AccountType'), 
					'ParentListID' => $Item->getChildDataAt('AccountRet ParentRef ListID'),
					'ParentFullName' => $Item->getChildDataAt('AccountRet ParentRef FullName'),
					'Sublevel' => $Item->getChildDataAt('AccountRet Sublevel'),
					'SpecialAccountType' => $Item->getChildDataAt('AccountRet SpecialAccountType'),
					'AccountNumber' => $Item->getChildDataAt('AccountRet AccountNumber'),
					'BankNumber' => $Item->getChildDataAt('AccountRet BankNumber'),
					'Description' => $Item->getChildDataAt('AccountRet Desc'),
					'Balance' => $Item->getChildDataAt('AccountRet Balance'),
					'TotalBalance' => $Item->getChildDataAt('AccountRet TotalBalance'),
					'TaxLineID' => $Item->getChildDataAt('AccountRet TaxLineInfoRet TaxLineID'),
					'TaxLineName' => $Item->getChildDataAt('AccountRet TaxLineInfoRet TaxLineName'),
					'qbAccountmerchantID'=>$mID,
					
				);
				
			$this->db->select('*');
			$this->db->from('qb_item_account');
			$this->db->where('ListID', $Item->getChildDataAt('AccountRet ListID'));
			$this->db->where('company', $companyID);
			$this->db->where('qbAccountmerchantID', $mID);
			$qr1 = $this->db->get();
			
				
			if($qr1->num_rows() >0)
			{  
				$this->db->where(array('ListID' => $Item->getChildDataAt('AccountRet ListID'), 'company' => $companyID, 'qbAccountmerchantID' => $mID));
				$this->db->update('qb_item_account',$arr);
				
			}else{
				
				  $this->db->insert('qb_item_account',$arr);
			}

			
				
			
				
				
			}
		}
		
		return true;
	}	



	
	/**
	 * Build a request to import Credit already in QuickBooks into our application
	 */
	public function _quickbooks_credit_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		
		
		
		$attr_iteratorID = '';
		$attr_iterator = ' iterator="Start" ';
		if (empty($extra['iteratorID']))
		{
			// This is the first request in a new batch
			$last = $this->_quickbooks_get_last_run($user, $action);
			$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
			
			// Set the current run to $last
			$this->_quickbooks_set_current_run($user, $action, $last);
		}
		else
		{
			// This is a continuation of a batch
			$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
			$attr_iterator = ' iterator="Continue" ';
			
			$last = $this->_quickbooks_get_current_run($user, $action);
		}
		
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<CreditMemoQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
						<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					
                      <ModifiedDateRangeFilter>
                       <FromModifiedDate >'. $last .'</FromModifiedDate>
                      </ModifiedDateRangeFilter>  
                    <IncludeLineItems >true</IncludeLineItems> 
                  
						<OwnerID>0</OwnerID>
					</CreditMemoQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
	
		return $xml;
	}		

   /** 
	 * Handle a response from QuickBooks 
	 */
	public function _quickbooks_credit_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			// Queue up another request
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_IMPORT_CREDITMEMO, null, QB_PRIORITY_ITEM, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/CreditMemoQueryRs');

			$this->getStatusData($List, 'CreditMemoQuery');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
	
			foreach ($List->children() as $Item)
			{
				
				$arr = array(
					'TxnID' => $Item->getChildDataAt('CreditMemoRet TxnID'),
					'EditSequence'=>$Item->getChildDataAt('CreditMemoRet EditSequence'),
					'TxnNumber'=>$Item->getChildDataAt('CreditMemoRet TxnNumber'),
				     'companyListID' =>$companyID,
					'TimeCreated' => date("Y-m-d H:i:s", strtotime($Item->getChildDataAt('CreditMemoRet TimeCreated'))),
					'TimeModified' => date("Y-m-d H:i:s", strtotime($Item->getChildDataAt('CreditMemoRet TimeModified'))),
					'CustomerListID' => $Item->getChildDataAt('CreditMemoRet CustomerRef ListID'),
					'CustomerFullName' => $Item->getChildDataAt('CreditMemoRet CustomerRef FullName'),
					
					'AccountListID' => $Item->getChildDataAt('CreditMemoRet ARAccountRef ListID'),
					'AccountFullName' => $Item->getChildDataAt('CreditMemoRet ARAccountRef FullName'),
					'TxnDate' => $Item->getChildDataAt('CreditMemoRet TxnDate'), 
					'RefNumber' => $Item->getChildDataAt('CreditMemoRet RefNumber'), 
					'IsPending' => $Item->getChildDataAt('CreditMemoRet IsPending'), 
					'DueDate' => $Item->getChildDataAt('CreditMemoRet DueDate'), 
					'Subtotal' => $Item->getChildDataAt('CreditMemoRet Subtotal'), 
					'TotalAmount' => $Item->getChildDataAt('CreditMemoRet TotalAmount'), 
					'CreditRemaining' => $Item->getChildDataAt('CreditMemoRet CreditRemaining'),
					'Memo'=>$Item->getChildDataAt('CreditMemoRet Memo'),
					);
                
				
			
				
			
				
				foreach ($arr as $key => $value)
				{
					$arr[$key] = $this->db->escape_str($value);
				}
				
				
				
				// Store the customers in MySQL
				
			$this->db->query("
					REPLACE INTO
						qb_customer_credit
					(
						" . implode(", ", array_keys($arr)) . "
					) VALUES (
						'" . implode("', '", array_values($arr)) . "'
					)");
		     	$this->db->query("DELETE FROM qb_customer_credit_item WHERE CreditTxnID = '" . $this->db->escape_str($arr['TxnID']) . "' ") ;
			
           
			// Process the line items
			foreach ($Item->children() as $Child)
			{
				if ($Child->name() == 'CreditMemoLineRet')
				{
					$InvoiceLine = $Child;
					
					$lineitem = array( 
						'CreditTxnID'          => $arr['TxnID'], 
						'TxnLineID'      => $InvoiceLine->getChildDataAt('CreditMemoLineRet TxnLineID'), 
						'ItemListID'    => $InvoiceLine->getChildDataAt('CreditMemoLineRet ItemRef ListID'), 
						'ItemName'  => $InvoiceLine->getChildDataAt('CreditMemoLineRet ItemRef FullName'), 
						'ItemDesc'        => $InvoiceLine->getChildDataAt('CreditMemoLineRet Desc'), 
						'ItemQuantity'       => $InvoiceLine->getChildDataAt('CreditMemoLineRet Quantity'),
						'ItemRate'           => $InvoiceLine->getChildDataAt('CreditMemoLineRet Rate'), 
                    	'Amount'           => $InvoiceLine->getChildDataAt('CreditMemoLineRet Amount'), 
						);
					
					foreach ($lineitem as $key => $value)
					{
						$lineitem[$key] = $this->db->escape_str($value);
					}
				
					$this->db->query("
						INSERT INTO
							qb_customer_credit_item
						(
							" . implode(", ", array_keys($lineitem)) . "
						) VALUES (
							'" . implode("', '", array_values($lineitem)) . "'
						) ") ;
				}
			}
			
			}
		}
		
		return true;
	}



	/**
	 * Build a request to import items already in QuickBooks into our application
	 */
	public function _quickbooks_item_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		
		
		
		$attr_iteratorID = '';
		$attr_iterator = ' iterator="Start" ';
		if (empty($extra['iteratorID']))
		{
			// This is the first request in a new batch
			$last = $this->_quickbooks_get_last_run($user, $action);
			$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
			
			// Set the current run to $last
			$this->_quickbooks_set_current_run($user, $action, $last);
		}
		else
		{
			// This is a continuation of a batch
			$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
			$attr_iterator = ' iterator="Continue" ';
			
			$last = $this->_quickbooks_get_current_run($user, $action);
		}
		
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<ItemQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
						<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
						<FromModifiedDate>' . $last . '</FromModifiedDate>
						<OwnerID>0</OwnerID>
					</ItemQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
		return $xml;
	}
  



	/** 
	 * Handle a response from QuickBooks 
	 */
	public function _quickbooks_item_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			// Queue up another request
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_IMPORT_ITEM, null, QB_PRIORITY_ITEM, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		
				
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/ItemQueryRs');

			$this->getStatusData($List, 'ItemQuery');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$companyID    = $user_data['id'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
			    
				$type = substr(substr($Item->name(), 0, -3), 4);
				$ret = $Item->name();
				$priceItem = $Item->getChildDataAt($ret . ' SalesOrPurchase Price');
				if(empty($priceItem)){
					$salePrice = '0.00';
				}else{
					if($priceItem == ''){
						$salePrice = '0.00';
						
						
					} else {
						$salePrice = $priceItem;
					}
					
					
				}
				
				$lsid = $Item->getChildDataAt($ret . ' ListID');
				$arr = array(
					'ListID' => $Item->getChildDataAt($ret . ' ListID'),
					'EditSequence'=>$Item->getChildDataAt($ret . ' EditSequence'),
					'IsActive'=>$Item->getChildDataAt($ret . ' IsActive'),
				     'companyListID' =>$companyID,
					'TimeCreated' => date('Y-m-d H:i:s',strtotime($Item->getChildDataAt($ret . ' TimeCreated'))),
					'TimeModified' => date('Y-m-d H:i:s',strtotime($Item->getChildDataAt($ret . ' TimeModified'))),
					'Name' => $Item->getChildDataAt($ret . ' Name'),
					'FullName' => $Item->getChildDataAt($ret . ' FullName'),
					'Type' => $type, 
					'Parent_ListID' => $Item->getChildDataAt($ret . ' ParentRef ListID'),
					'Parent_FullName' => $Item->getChildDataAt($ret . ' ParentRef FullName'),
					'ManufacturerPartNumber' => $Item->getChildDataAt($ret . ' ManufacturerPartNumber'), 
					'SalesTaxCode_ListID' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef ListID'), 
					'SalesTaxCode_FullName' => $Item->getChildDataAt($ret . ' SalesTaxCodeRef FullName'), 
					'BuildPoint' => $Item->getChildDataAt($ret . ' BuildPoint'), 
					'ReorderPoint' => $Item->getChildDataAt($ret . ' ReorderPoint'), 
					'QuantityOnHand' => ($Item->getChildDataAt($ret . ' QuantityOnHand'))?$Item->getChildDataAt($ret . ' QuantityOnHand'):'0', 
					'SalesPrice' => $salePrice,
                     'AccountRef' => $Item->getChildDataAt($ret . ' SalesOrPurchase AccountRef ListID'),
					'SalesDesc' => $Item->getChildDataAt($ret . ' SalesOrPurchase Desc'),
					'AverageCost' => ($Item->getChildDataAt($ret . ' AverageCost'))?$Item->getChildDataAt($ret . ' AverageCost'):'0', 
					'QuantityOnOrder' => ($Item->getChildDataAt($ret . ' QuantityOnOrder'))?$Item->getChildDataAt($ret . ' QuantityOnOrder'):'0', 
					'QuantityOnSalesOrder' => ($Item->getChildDataAt($ret . ' QuantityOnSalesOrder'))?$Item->getChildDataAt($ret . ' QuantityOnSalesOrder'):'0',  
					'TaxRate' => $Item->getChildDataAt($ret . ' TaxRate'), 
                    'Discount' => ($Item->getChildDataAt($ret . ' DiscountRate'))?$Item->getChildDataAt($ret . ' DiscountRate'):'0', 
					 'DepositToAccountRef' => $Item->getChildDataAt($ret . ' DepositToAccountRef ListID'), 
					 'DepositToAccountName' => $Item->getChildDataAt($ret . ' DepositToAccountRef FullName'),
					  'PaymentMethodRef' => $Item->getChildDataAt($ret . ' PaymentMethodRef ListID'), 
					 'PaymentMethodName' => $Item->getChildDataAt($ret . ' PaymentMethodRef FullName'),
					
					);
                  if($type=='Payment'|| $type=='Group' || $type=='Subtotal')    
                 $arr['SalesDesc']  =$Item->getChildDataAt($ret . ' ItemDesc');
				 
				$look_for = array(
					'SalesPrice' => array( 'SalesOrPurchase Price', 'SalesAndPurchase SalesPrice', 'SalesPrice' ),
					'SalesDesc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase SalesDesc', 'SalesDesc' ),
					'PurchaseCost' => array( 'SalesOrPurchase Price', 'SalesAndPurchase PurchaseCost', 'PurchaseCost' ),
					'PurchaseDesc' => array( 'SalesOrPurchase Desc', 'SalesAndPurchase PurchaseDesc', 'PurchaseDesc' ),
					'PrefVendor_ListID' => array( 'SalesAndPurchase PrefVendorRef ListID', 'PrefVendorRef ListID' ), 
					'PrefVendor_FullName' => array( 'SalesAndPurchase PrefVendorRef FullName', 'PrefVendorRef FullName' ),
					); 
					
				foreach ($look_for as $field => $look_here)
				{
					if (!empty($arr[$field]))
					{
						break;
					}
					
					foreach ($look_here as $look)
					{
						$arr[$field] = $Item->getChildDataAt($ret . ' ' . $look);
					}
				}
				$this->db->select('*');
				$this->db->from('qb_test_item');
				$this->db->where('ListID', $Item->getChildDataAt($ret . ' ListID'));
				$this->db->where('companyListID', $companyID);
				$qr1 = $this->db->get();
				
					
				if($qr1->num_rows() >0)
				{  
					$this->db->where(array('ListID' => $Item->getChildDataAt($ret . ' ListID'), 'companyListID' => $companyID));
					$this->db->update('qb_test_item',$arr);
					
				}else{
					
					  $this->db->insert('qb_test_item',$arr);
				}
			
			
					$this->db->db_debug = false;
				$err = $this->db->_error_message();
				
			   
							
			if($type=='Group')
			{		
				if($this->db->query("Select * from qb_qroup_lineitem where groupListID='".$lsid."' ")->num_rows() >0)
				{	
					foreach ($Item->children() as $Child)
			        {
						if ($Child->name() == 'ItemGroupLine')
						{
							$ItemGroupLine = $Child;
							
					$lineitem = array( 
						'groupListID'      =>$lsid, 
						'itemListID'       => $ItemGroupLine->getChildDataAt('ItemGroupLine ItemRef ListID'), 
						'FullName'         => $ItemGroupLine->getChildDataAt('ItemGroupLine ItemRef FullName'), 
						'Quantity'         => $ItemGroupLine->getChildDataAt('ItemGroupLine Quantity')
						);
					
					
					$this->db->where(array('groupListID'=>$lsid,'itemListID'=>$lineitem['itemListID'] ));
					$this->db->update('qb_qroup_lineitem', $lineitem);
					
				  }
			    }
			}
			else{
	                 foreach ($Item->children() as $Child)
			        {
						if ($Child->name() == 'ItemGroupLine')
						{
							$ItemGroupLine = $Child;
							
					$lineitem = array( 
						'groupListID'      =>$lsid, 
						'itemListID'       => $ItemGroupLine->getChildDataAt('ItemGroupLine ItemRef ListID'), 
						'FullName'         => $ItemGroupLine->getChildDataAt('ItemGroupLine ItemRef FullName'), 
						'Quantity'         => $ItemGroupLine->getChildDataAt('ItemGroupLine Quantity')
						);
					
					
					foreach ($lineitem as $key => $value)
				{
					$lineitem[$key] = $this->db->escape_str($value);
				}
				
			
				// Store the customers in MySQL
				
				$this->db->query("
					Insert INTO
						qb_qroup_lineitem
					(
						" . implode(", ", array_keys($lineitem)) . "
					) VALUES (
						'" . implode("', '", array_values($lineitem)) . "'
					)");
					
					
				  }
			    }

			}			
					
					
					
			}	
					
				
			}
		}
		
		return true;
	}

	/**
	 * Build a request to import invoices already in QuickBooks into our application
	 */
	 

	 
	public function _quickbooks_purchaseorder_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		$attr_iteratorID = '';
		$attr_iterator = ' iterator="Start" ';
		if (empty($extra['iteratorID']))
		{
			// This is the first request in a new batch
			$last = $this->_quickbooks_get_last_run($user, $action);
			$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
			
			// Set the current run to $last
			$this->_quickbooks_set_current_run($user, $action, $last);
		}
		else
		{
			// This is a continuation of a batch
			$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
			$attr_iterator = ' iterator="Continue" ';
			
			$last = $this->_quickbooks_get_current_run($user, $action);
		}
		
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<PurchaseOrderQueryRq ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
						<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
						<!--<ModifiedDateRangeFilter>
							<FromModifiedDate>' . $last . '</FromModifiedDate>
						</ModifiedDateRangeFilter>-->
						<IncludeLineItems>true</IncludeLineItems>
						<OwnerID>0</OwnerID>
					</PurchaseOrderQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
			
		return $xml;
	}
	
	

	/** 
	 * Handle a response from QuickBooks 
	 */
	 
 
	 
	public function _quickbooks_purchaseorder_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			// Queue up another request
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_IMPORT_PURCHASEORDER, null, QB_PRIORITY_PURCHASEORDER, array( 'iteratorID' => $idents['iteratorID'] ));
		}
		
		// This piece of the response from QuickBooks is now stored in $xml. You 
		//	can process the qbXML response in $xml in any way you like. Save it to 
		//	a file, stuff it in a database, parse it and stuff the records in a 
		//	database, etc. etc. etc. 
		//	
		// The following example shows how to use the built-in XML parser to parse 
		//	the response and stuff it into a database. 
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/PurchaseOrderQueryRs');

			$this->getStatusData($List, 'PurchaseOrderQuery');
			
			foreach ($List->children() as $PurchaseOrder)
			{
				$arr = array(
					'TxnID' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet TxnID'),
					'TimeCreated' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet TimeCreated'),
					'TimeModified' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet TimeModified'),
					'RefNumber' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet RefNumber'),
					'Customer_ListID' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet CustomerRef ListID'),
					'Customer_FullName' => $PurchaseOrder->getChildDataAt('PurchaseOrderRet CustomerRef FullName'),
					);
				
				QuickBooks_Utilities::log(QB_QUICKBOOKS_DSN, 'Importing purchase order #' . $arr['RefNumber'] . ': ' . print_r($arr, true));
				
				foreach ($arr as $key => $value)
				{
					$arr[$key] = $this->db->escape_str($value);
				}
				
				// Process all child elements of the Purchase Order
				foreach ($PurchaseOrder->children() as $Child)
				{
					if ($Child->name() == 'PurchaseOrderLineRet')
					{
						// Loop through line items
						
						$PurchaseOrderLine = $Child;
						
						$lineitem = array( 
							'TxnID' => $arr['TxnID'], 
							'TxnLineID' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet TxnLineID'), 
							'Item_ListID' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet ItemRef ListID'), 
							'Item_FullName' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet ItemRef FullName'), 
							'Descrip' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet Desc'), 
							'Quantity' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet Quantity'),
							'Rate' => $PurchaseOrderLine->getChildDataAt('PurchaseOrderLineRet Rate'), 
							);
						
						QuickBooks_Utilities::log(QB_QUICKBOOKS_DSN, ' - line item #' . $lineitem['TxnLineID'] . ': ' . print_r($lineitem, true));
					}
					else if ($Child->name() == 'DataExtRet')
					{
						// Loop through custom fields
						
						$DataExt = $Child;
						
						$dataext = array(
							'DataExtName' => $Child->getChildDataAt('DataExtRet DataExtName'), 
							'DataExtValue' => $Child->getChildDataAt('DataExtRet DataExtValue'), 
							);
						
						QuickBooks_Utilities::log(QB_QUICKBOOKS_DSN, ' - custom field "' . $dataext['DataExtName'] . '": ' . $dataext['DataExtValue']);
					}
				}
			}
		}
		
		return true;
	}

   
     /**
	 * Build a request to import Vendor already in QuickBooks into our application
	 */
	public function _quickbooks_vendor_query_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		
		
		$last = $this->_quickbooks_get_current_run($user, $action);
		$value = date('Y-m-d') . 'T' . date('H:i:s');
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<VendorQueryRq  requestID="' . $requestID . '">
						<FromModifiedDate>'. $last.'</FromModifiedDate>
                        <ToModifiedDate >'.$value.'</ToModifiedDate>
					</VendorQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
		
	  
   

		return $xml;
	}


	public function _quickbooks_vendor_query_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_QUERY_VENDOR, null, 1, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/VendorQueryRs');

			$this->getStatusData($List, 'VendorQuery');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$mrID    = $user_data['merchantID'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				 
				
				
				$arr = array(
					'vListID' => $Item->getChildDataAt('VendorRet ListID'),
					'EditSequence'=>$Item->getChildDataAt('VendorRet EditSequence'),
					'IsActive'=>$Item->getChildDataAt('VendorRet IsActive'),
				     'merchantID' =>$mrID,
					'TimeCreated' => date('Y-m-d',strtotime($Item->getChildDataAt('VendorRet TimeCreated'))),
					'TimeModified' => date('Y-m-d',strtotime($Item->getChildDataAt('VendorRet TimeModified'))),
					'FullName' => $Item->getChildDataAt('VendorRet Name'),
					'companyName' => $Item->getChildDataAt('VendorRet Name'),
					
					);
				
				
				foreach ($arr as $key => $value)
				{
					$arr[$key] = $this->db->escape_str($value);
				}
				
				$whereVendor = [
					'vListID' => $Item->getChildDataAt('VendorRet ListID'),
					'merchantID' => $mrID
				];

				$this->db->select('*');
				$this->db->from('qb_merchant_vendor');
				$this->db->where($whereVendor);
				$qr1 = $this->db->get();

				if($qr1->num_rows() >0)
				{  
					$this->db->where($whereVendor);
					$this->db->update('qb_merchant_vendor',$arr);
				}else{
					$this->db->insert('qb_merchant_vendor',$arr);
				}
					
				
				
			}
		}
		
		return true;
	}	


	public function _quickbooks_vendor_import_request($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $version, $locale)
	{
		// Iterator support (break the result set into small chunks)
		
		
		$attr_iteratorID = '';
		$attr_iterator = ' iterator="Start" ';
		if (empty($extra['iteratorID']))
		{
			// This is the first request in a new batch
			$last = $this->_quickbooks_get_last_run($user, $action);
			$this->_quickbooks_set_last_run($user, $action);			// Update the last run time to NOW()
			
			// Set the current run to $last
			$this->_quickbooks_set_current_run($user, $action, $last);
		}
		else
		{
			// This is a continuation of a batch
			$attr_iteratorID = ' iteratorID="' . $extra['iteratorID'] . '" ';
			$attr_iterator = ' iterator="Continue" ';
			
			$last = $this->_quickbooks_get_current_run($user, $action);
		}
		$value = date('Y-m-d') . 'T' . date('H:i:s');
		// Build the request
		$xml = '<?xml version="1.0" encoding="utf-8"?>
			<?qbxml version="' . $version . '"?>
			<QBXML>
				<QBXMLMsgsRq onError="stopOnError">
					<VendorQueryRq  ' . $attr_iterator . ' ' . $attr_iteratorID . ' requestID="' . $requestID . '">
					<MaxReturned>' . QB_QUICKBOOKS_MAX_RETURNED . '</MaxReturned>
					<FromModifiedDate>' . $last . '</FromModifiedDate>
                        
					</VendorQueryRq>	
				</QBXMLMsgsRq>
			</QBXML>';
		
				
	  
   

		return $xml;
	}


	public function _quickbooks_vendor_import_response($requestID, $user, $action, $ID, $extra, &$err, $last_action_time, $last_actionident_time, $xml, $idents)
	{	
		if (!empty($idents['iteratorRemainingCount']))
		{
			
			$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
			$Queue->enqueue(QUICKBOOKS_IMPORT_VENDOR, null, 1, array( 'iteratorID' => $idents['iteratorID'] ),$user);
		}
		
		// Import all of the records
		$errnum = 0;
		$errmsg = '';
		$Parser = new QuickBooks_XML_Parser($xml);
		
		if ($Doc = $Parser->parse($errnum, $errmsg))
		{
			$Root = $Doc->getRoot();
			$List = $Root->getChildAt('QBXML/QBXMLMsgsRs/VendorQueryRs');

			$this->getStatusData($List, 'VendorQuery');
			
	        $con1 = array('qbwc_username'=>$user);
    	 	    $query1 = $this->db->select('*')->from('tbl_company')->where($con1)->get();
    			$lastquery = $this->db->last_query();
    		
    			if($query1->num_rows()>0 ) {
					
    				$user_data    = $query1->row_array();
    			   
					$mrID    = $user_data['merchantID'] ;
    			}
		
			
			foreach ($List->children() as $Item)
			{
				 
				
				
				$arr = array(
					'vListID' => $Item->getChildDataAt('VendorRet ListID'),
					'EditSequence'=>$Item->getChildDataAt('VendorRet EditSequence'),
					'IsActive'=>$Item->getChildDataAt('VendorRet IsActive'),
				     'merchantID' =>$mrID,
					'TimeCreated' => date('Y-m-d',strtotime($Item->getChildDataAt('VendorRet TimeCreated'))),
					'TimeModified' => date('Y-m-d',strtotime($Item->getChildDataAt('VendorRet TimeModified'))),
					'FullName' => $Item->getChildDataAt('VendorRet Name'),
					'companyName' => $Item->getChildDataAt('VendorRet Name'),
					
					);
				
				
				foreach ($arr as $key => $value)
				{
					$arr[$key] = $this->db->escape_str($value);
				}

				$whereVendor = [
					'vListID' => $Item->getChildDataAt('VendorRet ListID'),
					'merchantID' => $mrID
				];
				
				$this->db->select('*');
				$this->db->from('qb_merchant_vendor');
				$this->db->where($whereVendor);
				$qr1 = $this->db->get();

				if($qr1->num_rows() >0)
				{  
					$this->db->where($whereVendor);
					$this->db->update('qb_merchant_vendor',$arr);
				}else{
					$this->db->insert('qb_merchant_vendor',$arr);
				}
				
				
				
			}
		}
		
		return true;
	}
	/**
	 * Handle a 500 not found error from QuickBooks
	 * 
	 * Instead of returning empty result sets for queries that don't find any 
	 * records, QuickBooks returns an error message. This handles those error 
	 * messages, and acts on them by adding the missing item to QuickBooks. 
	 */
public	function _quickbooks_error_e500_notfound($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
	{
		$Queue = QuickBooks_WebConnector_Queue_Singleton::getInstance();
		
			if ($action == QUICKBOOKS_QUERY_INVOICE)
		{
			return true;
		}
		
		if ($action == QUICKBOOKS_IMPORT_INVOICE)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_CUSTOMER)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_SALESORDER)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_ITEM)
		{
			return true;
		}
        else if ($action == QUICKBOOKS_IMPORT_ACCOUNT)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_PURCHASEORDER)
		{
			return true;
		}
       else if ($action == QUICKBOOKS_IMPORT_CREDITMEMO)
		{
         
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_RECEIVEPAYMENT)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_IMPORT_SALESTAXITEM)
		{
			return true;
		}
		else if ($action == QUICKBOOKS_QUERY_VENDOR)
		{
			return true;
		}
		
		return false;
	}
	/**
	 * Catch any errors that occur
	 * 
	 * @param string $requestID			
	 * @param string $action
	 * @param mixed $ID
	 * @param mixed $extra
	 * @param string $err
	 * @param string $xml
	 * @param mixed $errnum
	 * @param string $errmsg
	 * @return void
	 */
public	function _quickbooks_error_catchall($requestID, $user, $action, $ID, $extra, &$err, $xml, $errnum, $errmsg)
	{
		$message = '';
		$message .= 'Request ID: ' . $requestID . "\r\n";
		$message .= 'User: ' . $user . "\r\n";
		$message .= 'Action: ' . $action . "\r\n";
		$message .= 'ID: ' . $ID . "\r\n";
		$message .= 'Extra: ' . print_r($extra, true) . "\r\n";
		$message .= 'Error number: ' . $errnum . "\r\n";
		$message .= 'Error message: ' . $errmsg . "\r\n";


        if($action=='CustomerAdd' ||  $action=='CustomerMod')
        {
         $this->db->where(array('customerID'=>$ID));
          $this->db->update('tbl_custom_customer',array('qb_status'=>3));
       
        }
      if($action=='ItemGroupMod' ||  $action=='ItemGroupAdd')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
      if($action=='ItemPaymentAdd' ||  $action=='ItemPaymentMod')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
     if($action=='ItemDiscountAdd' ||  $action=='ItemDiscountMod')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
 if($action=='ItemNonInventoryMod' ||  $action=='ItemNonInventoryAdd')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
 if($action=='ItemOtherChargeMod' ||  $action=='ItemOtherChargeAdd')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
 if($action=='ItemSubtotalMod' ||  $action=='ItemSubtotalAdd')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }
     
     if($action=='ItemServiceMod' ||  $action=='ItemServiceAdd')
        {
         $this->db->where(array('productID'=>$ID));
          $this->db->update('tbl_custom_product',array('qb_status'=>3));
       
        }


     if($action=='InvoiceMod' ||  $action=='InvoiceAdd')
        {
         $this->db->where(array('insertInvID'=>$ID));
          $this->db->update('tbl_custom_invoice',array('qb_status'=>3));
       
        }
		  if($action=='TxnDel')
        {
         $this->db->where(array('delID'=>$ID));
          $this->db->update('tbl_del_transactions',array('qb_status'=>3));
       
		}
		
		if($action == QUICKBOOKS_ADD_RECEIVEPAYMENT){
			return true;
		}
		
	}

	public function final_auth_success()
	{
	        
	          if($this->session->userdata('logged_in')['merchID']!="")
	          {
    	        $data['primary_nav']  = primary_nav();
    			$data['template']   = template_variable();
    			$this->load->model('general_model');
    			$merchantID  = $this->session->userdata('logged_in')['merchID'];
    			$condition = array('merchID'=> $merchantID); 
    		   	 
    		   	$this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
    		   	$merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
    		   	if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
	    		   	/* Start campaign in hatchbuck CRM*/  
	                $this->load->library('hatchBuckAPI');
	               
	                
	                $merchantData['merchant_type'] = 'QuickBooks Desktop (QBD)';     
	                
	                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                if($status['statusCode'] == 400){
	                    $resource = $this->hatchbuckapi->createContact($merchantData);
	                    if($resource['contactID'] != '0'){
	                        $contact_id = $resource['contactID'];
	                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                    }
	                }
	                /* End campaign in hatchbuck CRM*/ 
		        }
    		   	  

    		   	  $sess_array = $this->session->userdata('logged_in');
    		   	   $sess_array['firstLogin'] = '1';
    			   $this->session->set_userdata('logged_in', $sess_array);  
    			   
    			   if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchantID)) == '0')
    			   {  
					     
						 $fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata)
						  {
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchantID,
									 'fromEmail'      => DEFAULT_FROM_EMAIL,//$fromEmail,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
					    	 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                }
    			   
    			   
    			   
    		   	  redirect(base_url('home/index'));
    		
    		
    			  
	          }
	          else
	          {
	              redirect('login','refresh');
	          }
 
	}
	
	public function _formatXMLRequest($xml){
		$xml = str_replace('&', '&amp;', $xml);
		return $xml;
	}
	
	
	

	
	
	
	
}
	