<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
use \Chargezoom\Core\Http\SessionRequest;
class Firstlogin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		if ($this->session->userdata('logged_in') != "") {
		} else {

			redirect('login', 'refresh');
		}
	}


	public function index()
	{

		redirect('login', 'refresh');
	}



	public function dashboard_first_login_ajaxdata()
	{
		$user_id  = $this->session->userdata('logged_in')['merchID'];
		$input_data = $nmi_data = array();
		$resID    = $this->session->userdata('logged_in')['resellerID'];

		$rs       =  $this->general_model->get_select_data('tbl_reseller', array('ProfileURL', 'Chat'), array('resellerID' => $resID));

		$hepltext = '';

		if ($this->czsecurity->xssCleanPostInput('action') == '1') {

			$input_data['companyName']	        = $this->czsecurity->xssCleanPostInput('companyName');
			$input_data['merchantAddress1']     = $this->czsecurity->xssCleanPostInput('companyAddress1');
			$input_data['merchantContact'] 	    = $this->czsecurity->xssCleanPostInput('companyContact');
			$input_data['merchantZipCode']      = $this->czsecurity->xssCleanPostInput('zipCode');
			$input_data['merchantCountry']      = $this->czsecurity->xssCleanPostInput('companyCountry');
			$input_data['merchantState']     	= $this->czsecurity->xssCleanPostInput('companyState');
			$input_data['merchantCity'] 		= $this->czsecurity->xssCleanPostInput('companyCity');
			if ($this->czsecurity->xssCleanPostInput('companyAddress2') != "")
				$input_data['merchantAddress2']  = $this->czsecurity->xssCleanPostInput('companyAddress2');
			if ($this->czsecurity->xssCleanPostInput('alternateContact') != "")
				$input_data['merchantAlternateContact'] = $this->czsecurity->xssCleanPostInput('alternateContact');
		}
		if ($this->czsecurity->xssCleanPostInput('action') == '3') {
			$condition = array('merchID'=>$user_id);

			$this->general_model->update_row_data('tbl_merchant_data', $condition, array('is_integrate'=>1));
		}
		

		if ($this->czsecurity->xssCleanPostInput('action') == '2') {




			$hepltext = '';
			$rootDomain             = CUS_PORTAL;
			if ($this->czsecurity->xssCleanPostInput('tagline') != "") {
				$input_data['merchantTagline'] 	= $this->czsecurity->xssCleanPostInput('tagline');
				$hepltext                          = $this->czsecurity->xssCleanPostInput('tagline');
			}

			$picture  = '';
			$extra1 = '';



			if (!empty($_FILES['picture']['name'])) {
				$config['upload_path'] = 'uploads/merchant_logo/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = time() . $_FILES['picture']['name'];
				
				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('picture')) {
					$uploadData = $this->upload->data();
					$picture = $uploadData['file_name'];
				} else {
					$res = array('status' => 'error');
					echo json_encode($res);
					die;
				}
			}






			if ($this->czsecurity->xssCleanPostInput('portal_url') != "") {
				$portal_url = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));
				$url = "https://" . $portal_url . $rootDomain . "/";


				if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $user_id))) {
					$update_data = array('customerPortalURL' => $url, 'customerHelpText' => $hepltext, 'portalprefix' => $portal_url);

					if ($picture != "")
						$update_data['ProfileImage'] = $picture;

					$this->general_model->update_row_data('tbl_config_setting', array('merchantID' => $user_id), $update_data);
				} else {
					$insert_data = array(
						'customerPortalURL' => $url, 'customerHelpText' => $hepltext,
						'merchantID' => $user_id, 'portalprefix' => $portal_url,
						'customerPortal' => 1
					);
					if ($picture != "")
						$insert_data['ProfileImage'] = $picture;
					$this->general_model->insert_row('tbl_config_setting', $insert_data);
				}
			}
			$condition_m = array('merchID' => $this->session->userdata('logged_in')['merchID']);
			$merchant_data =  $this->general_model->get_row_data('tbl_merchant_data', $condition_m);

			$condition1 = array('merchantID' => $this->session->userdata('logged_in')['merchID']);

			$row =   $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));
			if (!empty($row)) {
				$merchant_data['poral_url'] = $row['customerPortalURL'];
				$merchant_data['portalprefix'] = $row['portalprefix'];
				$merchant_data['ProfileImage'] = $picture;
			}
			$sess_array = $merchant_data;


			if (!empty($rs)) {
				$sess_array['logo_img'] = $rs['ProfileURL'];
				$sess_array['script']   =  $rs['Chat'];
			}

			$this->session->set_userdata('logged_in', $sess_array);
			$res = array('status' => 'success');
			echo json_encode($res);
			die;
		}

		if ($this->czsecurity->xssCleanPostInput('action') == '4' && $this->czsecurity->xssCleanPostInput('gateway_opt') != "") {


			$signature = '';
			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
			$frname          = '';

			$cr_status = 1;
			$isSurcharge = $surchargePercentage = $ach_status = 0;
			$gmID = 0;
			if ($gatetype == '3') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}
			if ($gatetype == '2') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');

				if ($this->czsecurity->xssCleanPostInput('auth_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

			}
			if ($gatetype == '1' || $gatetype == '9') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmi_user');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmi_password');

				if ($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			if ($gatetype == '9') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');

				if ($this->czsecurity->xssCleanPostInput('cz_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			if ($gatetype == '4') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
			}

			if ($gatetype == '5') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
			}
			if ($gatetype == '6') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
			}
			if ($gatetype == '7') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
				if ($this->czsecurity->xssCleanPostInput('heart_cr_status')){
					$cr_status       =  1;
				}
				else{
					$cr_status       =  0;
				}

				if ($this->czsecurity->xssCleanPostInput('heart_ach_status')){
					$ach_status       = 1;
				}
				else{
					$ach_status       =  0;
				}

			}

			if ($gatetype == '8') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
			}

			if ($gatetype == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
					$isSurcharge = 1;
				}
				else{
					$isSurcharge = 0;
				}
				$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			}

			if ($gatetype == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}
			if ($gatetype == '12') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
				$gmID                  = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			if ($gatetype == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			if ($gatetype == '15') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
				$nmipassword     = '';
				$cr_status       =  1;
				$ach_status       =  0;
			}

			if ($gatetype  == '17') {

				$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');

				if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			if ($gatetype == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            } 
			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
			$frname          = getGatewayNames($gatetype);

			$nurm = 0;
			$nurm =	$this->general_model->get_num_rows('tbl_merchant_gateway', array('merchantID' => $user_id));
			if ($nurm > 0) {
				$update_dataa     = array(
					'gatewayUsername' => $nmiuser, 'gatewayPassword' => $nmipassword,  'gatewaySignature' => $signature,'extra_field_1' =>$extra1,
					'gatewayType' => $gatetype, 'gatewayFriendlyName' => $frname, 'gatewayMerchantID' => $gmID,'isSurcharge' => $isSurcharge,
					'surchargePercentage' => $surchargePercentage, 'echeckStatus' => $ach_status, 'creditCard' => $cr_status
				);
				$nurm_1 =	$this->general_model->get_num_rows('tbl_merchant_gateway', array('merchantID' => $user_id, 'gatewayFriendlyName' => $frname));
				if ($nurm_1 > 0)
					$this->general_model->update_row_data('tbl_merchant_gateway', array('merchantID' => $user_id), $update_dataa);
				else {
					$nmi_data     = array(
						'gatewayUsername' => $nmiuser, 'gatewayPassword' => $nmipassword,
						'gatewayType' => $gatetype, 'gatewaySignature' => $signature, 'gatewayFriendlyName' => $frname, 'gatewayMerchantID' => $gmID, 'merchantID' => $user_id,'isSurcharge' => $isSurcharge,
						'surchargePercentage' => $surchargePercentage, 'echeckStatus' => $ach_status, 'creditCard' => $cr_status
					);
					$this->general_model->insert_row('tbl_merchant_gateway', $nmi_data);
				}
			} else {
				$nmi_data     = array(
					'gatewayUsername' => $nmiuser, 'gatewayPassword' => $nmipassword,'isSurcharge' => $isSurcharge,
					'surchargePercentage' => $surchargePercentage, 'echeckStatus' => $ach_status, 'creditCard' => $cr_status,
					'gatewayType' => $gatetype, 'gatewaySignature' => $signature,'extra_field_1' =>$extra1, 'gatewayFriendlyName' => $frname, 'gatewayMerchantID' => $gmID, 'merchantID' => $user_id, 'set_as_default' => '1'
				);

				$this->general_model->insert_row('tbl_merchant_gateway', $nmi_data);
			}

			$res = array('status' => 'success');

			echo json_encode($res);
			die;
		}
		if ($this->czsecurity->xssCleanPostInput('action') == '4') {

			$res = array('status' => 'success');
			echo json_encode($res);
			die;
		}

		if (!empty($input_data)) {

			$condition   = array('merchID' => $this->session->userdata('logged_in')['merchID']);

			if ($this->general_model->update_row_data('tbl_merchant_data', $condition, $input_data)) {

				$merchant_data =  $this->general_model->get_row_data('tbl_merchant_data', $condition);
				$condition1 = array('merchantID' => $this->session->userdata('logged_in')['merchID']);

				$row =   $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));
				if (!empty($row)) {
					$merchant_data['poral_url'] = $row['customerPortalURL'];
					$merchant_data['portalprefix'] = $row['portalprefix'];
				}
				$sess_array = $merchant_data;


				if (!empty($rs)) {
					$sess_array['logo_img'] = $rs['ProfileURL'];
					$sess_array['script']   =  $rs['Chat'];
				}
				$this->session->set_userdata('logged_in', $sess_array);
			}
		}


		$res = array('status' => 'success');
		echo json_encode($res);
		die;
	}



	public function dashboard_first_login()
	{


		/****************First Login Data Update**************/

		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			// Displaying Errors In Div
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			// Validation For Name Field
			$this->form_validation->set_rules('companyName', 'company Name', 'required|min_length[3]');
			$this->form_validation->set_rules('firstName', 'first Name', 'required|min_length[3]');
			$this->form_validation->set_rules('lastName', 'last Name', 'required|min_length[3]');
			// Validation For Email Field
			$this->form_validation->set_rules('companyAddress1', 'companyAddress1', 'required|min_length[5]');
			$this->form_validation->set_rules('companyAddress2', 'companyAddress2', 'required|min_length[5]');
			$this->form_validation->set_rules('companyCity', 'comapnyCity', 'required');
			
			$this->form_validation->set_rules('zipCode', 'zipCode', 'required|numeric');



			if ($this->form_validation->run() == true) {
				$extra1 = '';
				

				$input_data['companyName']	    = $this->czsecurity->xssCleanPostInput('companyName');
				$input_data['firstName']        = $this->czsecurity->xssCleanPostInput('firstName');
				$input_data['lastName']         = $this->czsecurity->xssCleanPostInput('lastName');
				$input_data['merchantAddress1'] = $this->czsecurity->xssCleanPostInput('companyAddress1');
				$input_data['merchantAddress2'] = $this->czsecurity->xssCleanPostInput('companyAddress2');
				$input_data['merchantContact'] 	= $this->czsecurity->xssCleanPostInput('companyContact');
				$input_data['merchantZipCode']   = $this->czsecurity->xssCleanPostInput('zipCode');
				$input_data['merchantCountry']  = $this->czsecurity->xssCleanPostInput('companyCountry');
				$input_data['merchantState'] 	= $this->czsecurity->xssCleanPostInput('companyState');
				$input_data['merchantCity'] 	= $this->czsecurity->xssCleanPostInput('companyCity');
				if ($this->czsecurity->xssCleanPostInput('companyAddress2') != "")
					$input_data['merchantAddress2']  = $this->czsecurity->xssCleanPostInput('companyAddress2');
				if ($this->czsecurity->xssCleanPostInput('alternateContact') != "")
					$input_data['merchantAlternateContact'] = $this->czsecurity->xssCleanPostInput('alternateContact');


				$condition   = array('merchID' => $this->session->userdata('logged_in')['merchID']);
				$gmID                  = $this->czsecurity->xssCleanPostInput('mid');

				if ($this->general_model->update_row_data('tbl_merchant_gateway', $condition, $input_data)) {


					$isSurcharge = $surchargePercentage = 0;
					$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
					if ($gatetype == '1' || $gatetype == '9') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
					}

					else if ($gatetype == '2') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
					}

					else if ($gatetype == '3') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
					}
					else if ($gatetype == '4') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
						$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
					}

					else if ($gatetype == '5') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
					}

					else if ($gatetype == '6') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
					}
					else if ($gatetype == '7') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
					}

					else if ($gatetype == '8') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
						$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
					}

					else if ($gatetype == '10') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
						if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
							$cr_status       =  1;
						else
							$cr_status       =  0;

						if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
							$ach_status       = 1;
						else
							$ach_status       =  0;

						if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
							$isSurcharge = 1;
						}
						else{
							$isSurcharge = 0;
						}
						$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
					}

					else if ($gatetype == '11') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
						$nmipassword     = '';
						if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
							$cr_status       =  1;
						else
							$cr_status       =  0;
		
						if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
							$ach_status       = 1;
						else
							$ach_status       =  0;
					}
					else if ($gatetype == '13') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
						$nmipassword     = '';
						if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
							$cr_status       =  1;
						else
							$cr_status       =  0;
		
						if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
							$ach_status       = 1;
						else
							$ach_status       =  0;
					}
					else if($gatetype == '14') {
						$nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointe_user');
		                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointe_password');
		                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
		                $signature     = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
		                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
		                    $cr_status = 1;
		                } else {
		                    $cr_status       =  0;
		                }

		                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
		                    $ach_status = 1;
		                } else {
		                    $ach_status = 0;
		                }

		            }
					else if ($gatetype == '12') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
						$gmID                  = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
						if ($this->czsecurity->xssCleanPostInput('TSYS_cr_status'))
							$cr_status       =  1;
						else
							$cr_status       =  0;
		
						if ($this->czsecurity->xssCleanPostInput('TSYS_ach_status'))
							$ach_status       = 1;
						else
							$ach_status       =  0;
					}else if ($gatetype == '15') {
						$nmiuser         = $this->czsecurity->xssCleanPostInput('payarcUser');
						$nmipassword     = '';
						$cr_status       =  1;
						$ach_status       =  0;
					} else if ($gatetype  == '17') {

						$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
						$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');
		
						if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
							$cr_status       =  1;
						else
							$cr_status       =  0;
		
						if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
							$ach_status       = 1;
						else
							$ach_status       =  0;
					
					}else if ($gatetype == '16') {
		                $nmiuser         = $this->input->post('EPXCustNBR');
		                $nmipassword     = $this->input->post('EPXMerchNBR');
		                $signature     = $this->input->post('EPXDBANBR');
		                $extra1   = $this->input->post('EPXterminal');
		                if ($this->input->post('EPX_cr_status'))
		                    $cr_status       =  1;
		                else
		                    $cr_status       =  0;

		                if ($this->input->post('EPX_ach_status'))
		                    $ach_status       = 1;
		                else
		                    $ach_status       =  0;
		                    
		            } 

					$frname           = '';


					$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
					$frname          = getGatewayNames($gatetype);


					$g_condition = array('gatewayFriendlyName' => $frname, 'merchantID' => $this->session->userdata('logged_in')['merchID']);
					$num   = $this->general_model->get_num_rows('tbl_merchant_gateway', $g_condition);
					if ($num) {
						
						$nmi_data     = array(
							'gatewayUsername' => $nmiuser, 'gatewayPassword' => $nmipassword,'extra_field_1' =>$extra1,
							'gatewayType' => $gatetype, 'gatewayFriendlyName' => $frname, 'gatewayMerchantID' => $gmID,
							'isSurcharge' => $isSurcharge,
							'surchargePercentage' => $surchargePercentage,
						);
						$nmi_data['merchantID']	 =  $this->session->userdata('logged_in')['merchID'];
						$gid =  $this->general_model->insert_row('tbl_merchant_gateway', $nmi_data);
					} else {
					}



					$company_data =  $this->general_model->get_row_data('tbl_merchant_data', $condition);
					$condition1 = array('merchantID' => $this->session->userdata('logged_in')['merchID']);
					$sess_array = $company_data;
					$sess_array['merchant_gateway'] = '1';
					$filenum                  = $this->general_model->get_num_rows('tbl_company', $condition1);
					$sess_array['fileID']     = $filenum;

					$this->session->set_userdata('logged_in', $sess_array);
					
				}


				$merchantdata = $this->session->userdata('logged_in');


				if (
					$merchantdata['companyName'] != "" && $merchantdata['merchantAddress1'] != "" &&
					$merchantdata['merchantFullAddress'] != "" &&  $merchantdata['merchant_gateway'] != "0"
				) {

					$condition1 = array('merchID' => $this->session->userdata('logged_in')['merchID']);
					$comp_data  = array('firstLogin' => '1');
					$this->general_model->update_row_data('tbl_merchant_data', $condition1, $comp_data);
					$sess_array['firstLogin'] = '1';

					$appset     = $this->general_model->get_row_data('app_integration_setting',  array('merchantID' => $this->session->userdata('logged_in')['merchID']));
					if (!empty($appset)) {
						$sess_array['active_app']	= $appset['appIntegration'];
						$sess_array['gatewayMode']	= $appset['transactionMode'];
					} else {
						$sess_array['active_app']	= '2';
						$sess_array['gatewayMode']	= 0;
					}

					$this->session->set_userdata('logged_in', $sess_array);

					$merchantID       = $this->session->userdata('logged_in')['merchID'];

					if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

						$fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];

						$templatedatas =  $this->general_model->get_table_data('tbl_email_template_data', '');

						foreach ($templatedatas as $templatedata) {
							$insert_data = array(
								'templateName'  => $templatedata['templateName'],
								'templateType'    => $templatedata['templateType'],
								'merchantID'      => $merchantID,
								'fromEmail'      => DEFAULT_FROM_EMAIL,
								'message'		  => $templatedata['message'],
								'emailSubject'   => $templatedata['emailSubject'],
								'createdAt'      => date('Y-m-d H:i:s')
							);
							$this->general_model->insert_row('tbl_email_template', $insert_data);
						}

						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->general_model->insert_inv_number($merchantID);
						}





						if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->general_model->insert_payterm($merchantID);
						}
					}

					redirect(base_url('home/index'));
				}
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$data['login_info']     = $this->session->userdata('logged_in');
		$user_id = $data['login_info']['merchID'];

		$data['login_info']        = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));

		$filenum                   = $this->general_model->get_row_data('tbl_company', array('merchantID' => $user_id));
		$qm                        =  $this->db->query('select  *  from tbl_company where merchantID="' . $user_id . '" and  fileID!=""  ');
		$mode =  $this->general_model->get_select_data('plan_friendlyname', array('gatewayAccess'), array('reseller_id' => $data['login_info']['resellerID'], 'plan_id' => $data['login_info']['plan_id']));

		if (!empty($mode))
			$data['gmode'] = $mode['gatewayAccess'];
		else
			$data['gmode'] = 0;

		$filenum1 = $qm->num_rows();
		$resID    = $this->session->userdata('logged_in')['resellerID'];
		$rs  =  $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName,lastName,ProfileURL', 'Chat'), array('resellerID' => $resID));


		if (!empty($rs)) {
			$data['login_info']['logo_img'] = $rs['ProfileURL'];
			$data['login_info']['script']   =  $rs['Chat'];
		}

		$config     = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $user_id));
		if (!empty($config)) {

			$data['login_info']['portalprefix']      = $config['portalprefix'];
			$data['login_info']['ProfileImage']      = $config['ProfileImage'];
		} else {
			$data['login_info']['portalprefix']      = '';
			$data['login_info']['ProfileImage']      = '';
		}
		// Set active app
		$appset     = $this->general_model->get_row_data('app_integration_setting',  array('merchantID' => $this->session->userdata('logged_in')['merchID']));
		if (!empty($appset)) {
			$data['login_info']['active_app']	= $appset['appIntegration'];
		} else {
			$data['login_info']['active_app']	= '5';
		}
		if ($filenum1 > 0) {
			$count     = count($filenum);
			$data['login_info']['fileID']      = '1';
			$data['file_user']                 = $filenum['qbwc_username'];
			$this->session->set_userdata('logged_in',  $data['login_info']);
		} else {
			$data['login_info']['fileID']      = '';
			$this->session->set_userdata('logged_in',  $data['login_info']);
		}
		$data['qbusername']     = $this->config->item('quickbooks_user');
		$gatew                  = $this->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $user_id, 'set_as_default' => '1'));
		if (!empty($gatew)) {
			$data['gateway_data']   = $gatew;
			$data['login_info']['merchant_gateway']      = '1';
			$this->session->set_userdata('logged_in',  $data['login_info']);
		} else {
			$data['login_info']['merchant_gateway']      = '0';
			$this->session->set_userdata('logged_in',  $data['login_info']);
		}


		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');
		$data['gt_result'] = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 0]);

		$merchID  = $this->session->userdata('logged_in')['merchID'];
		$con   = array('merchantID' => $merchID);
		$data['get_data'] = $this->general_model->get_row_data('tbl_freshbooks', $con);
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/dashboard_first_login', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/*********************Check Existing Email***************************/
	public function get_gateway_data()
	{


		$gatewayID  = $this->czsecurity->xssCleanPostInput('gatewayID');
		$condition     = array('gatewayID' => $gatewayID);

		$res  = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

		if (!empty($res)) {

			$res['status'] = 'true';
			echo json_encode($res);
		}

		die;
	}




	//------------- Merchant gateway START ------------//

	public function gateway()
	{


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];

		$condition	= array('merchantID' => $user_id);

		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');
		$data['gateways'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		if ($data['gatewayType'] == '1') {
			$data['gateway'] = "NMI";
		}
		if ($data['gatewayType'] == '2') {
			$data['gateway'] = "Authorize.net";
		}
		if ($data['gatewayType'] == '3') {
			$data['gateway'] = "Paytrace";
		}
		if ($data['gatewayType'] == '4') {
			$data['gateway'] = "Paypal";
		}
		if ($data['gatewayType'] == '5') {
			$data['gateway'] = "Stripe";
		}
		if ($data['gatewayType'] == '6') {
			$data['gateway'] = "USAePay";
		}
		if ($data['gatewayType'] == '9') {
			$data['gateway'] = "Chargezoom";
		}

		echo json_encode($data);
	}



	public function set_gateway_default()
	{


		if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

			if ($this->session->userdata('logged_in')) {
				$da['login_info'] 	= $this->session->userdata('logged_in');

				$merchID 				= $da['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$da['login_info'] 	= $this->session->userdata('user_logged_in');

				$merchID 				= $da['login_info']['merchantID'];
			}

			$id = $this->czsecurity->xssCleanPostInput('gatewayid');
			$val = array(
				'gatewayID' => $id,
			);
			$val1 = array(
				'merchantID' => $merchID,
			);
			$update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
			$this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
			$update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

			$this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
		}
		redirect(base_url('home/gateway'));
	}

	public function check_url()
	{
		$res = array();


		if ($this->session->userdata('logged_in') != "") {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in') != "") {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}



		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');

		if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
			if (strpos($portal_url, ' ') > 0) {

				$res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
				echo json_encode($res);
				die;
			}

			$res = array('portal_url' => 'Only letters and numbers allowed', 'status' => 'false');
			echo json_encode($res);
			die;
		} else {
			$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url));
			if ($num == 0) {
				$res = array('status' => 'success');
			} else if ($num == 1) {
				$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url, 'merchantID' => $merchantID));
				if ($num == 1)
					$res = array('status' => 'success');
				else
					$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			} else {
				$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			}

			echo json_encode($res);
			die;
		}
	}




	public function check_url_old()
	{
		$res = array();
		$portal_url  = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));

		if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
			if (strpos($portal_url, ' ') > 0) {

				$res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
				echo json_encode($res);
				die;
			}

			$res = array('portal_url' => 'Special character not allowed in url', 'status' => 'false');
			echo json_encode($res);
			die;
		} else {
			$res = array('status' => 'true');
			echo json_encode($res);
			die;
		}
	}






	public function chk_friendly_name()
	{
		if ($this->session->userdata('logged_in')) {
			$da['login_info'] 	= $this->session->userdata('logged_in');

			$merchID 				= $da['login_info']['merchID'];
		}
		$res = array();
		$frname  = $this->czsecurity->xssCleanPostInput('frname');
		$condition = array('gatewayFriendlyName' => $frname, 'merchantID' => $merchID);
		$num   = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
		if ($num) {
			$res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');
		} else {
			$res = array('status' => 'true');
		}
		echo json_encode($res);
		die;
	}


	public function freshbooks_user()

	{


		if (!empty($this->input->post(null, true))) {

			$merchID  = $this->session->userdata('logged_in')['merchID'];
			$resellerID  = $this->session->userdata('logged_in')['resellerID'];

			$condition = array('resellerID' => $resellerID);
			$merchantID = $merchID;
			$sub1 = $this->general_model->get_select_data('Config_merchant_portal', array('portalprefix'), $condition);
			$portal = $sub1['portalprefix'];
			$typr = $this->czsecurity->xssCleanPostInput('typetest');
			$input_data['accountType']  = $typr;
			$input_data['merchantID']	   = $merchantID;
			$input_data['createdAt']  = date('Y-m-d H:i:s');
			if ($typr == 1) {
				$fb_data  =  $this->general_model->get_row_data('tbl_freshbooks_config', array('id' => 1));
				$clientID  =  $fb_data['ClientID'];
				$key       =  $fb_data['ClientSecret'];
				$redirectURL =  $fb_data['FreshBooksURL'];
				$input_data['secretKey'] = $key;
				$input_data['sub_domain'] = $portal;
				$num    = $this->general_model->get_num_rows('tbl_freshbooks', array('merchantID' => $merchantID));
				if ($num > 0)
					$this->general_model->update_row_data('tbl_freshbooks', array('merchantID' => $merchantID), $input_data);
				else

					$nn =  $this->general_model->insert_row('tbl_freshbooks', $input_data);
				$fb_url = "https://my.freshbooks.com/service/auth/oauth/authorize?client_id=$clientID&response_type=code&redirect_uri=$redirectURL";

				redirect($fb_url);
			}
			$input_data['sub_domain']  = $this->czsecurity->xssCleanPostInput('subdomain');
			$input_data['secretKey']	   = $this->czsecurity->xssCleanPostInput('secretKey');


			$num = $this->general_model->get_num_rows(' tbl_freshbooks', array('merchantID' => $merchantID));
			if ($num > 0)
				$this->general_model->update_row_data('tbl_freshbooks', array('merchantID' => $merchantID), $input_data);
			else

				$nn =  $this->general_model->insert_row('tbl_freshbooks', $input_data);


			redirect(base_url('FreshBooks_controllers/Freshbooks_integration/freshbooks_auth_process'));
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/dashboard_first_login');
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	public function change_password()
	{
		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}

			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}
			
			// Check Password Strength
			$strength_check = $this->czsecurity->checkPasswordStrength($this->czsecurity->xssCleanPostInput('user_settings_password'));

			if ($strength_check['status'] != 'OK') {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>'.$strength_check['message'].'</div>');

				redirect(base_url('firstlogin/dashboard_first_login'));
			}

            $oldPassword    = $this->czsecurity->xssCleanPostInput('user_settings_currentpassword');
            $merch_data     = $this->general_model->get_row_data(
                'tbl_merchant_data',
                array('merchID' => $merchantID)
            );

            $validPassword = true;

            if (!empty($merch_data)) {
                if ($merch_data['merchantPasswordNew'] !== null) {
                    if (!password_verify($oldPassword, $merch_data['merchantPasswordNew'])) {
                        $validPassword = false;
                    }
                } else {
                    if (md5($oldPassword) !== $merch_data['merchantPassword']) {
                        $validPassword = false;
                    }
                }
            }

			if (!empty($merch_data) && $validPassword === true) {
				$password_update_date   = $merch_data['password_update_date'];
				$current                = strtotime(date('Y-m-d H:i:s'));
				$password_update_date   = strtotime($password_update_date);
				$diff                   = $current - $password_update_date;
				$hours                  = round($diff / ( 60 * 60));

				if ($hours == 0) {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Your password cannot be changed for 1 hour.</div>');
				} else {
					$this->general_model->update_row_data(
					    'tbl_merchant_data',
                        array('merchID' => $merchantID),
                        array(
                            'merchantPassword' => null,
                            'merchantPasswordNew' => password_hash(
                                $this->czsecurity->xssCleanPostInput('user_settings_password'),
                                PASSWORD_BCRYPT
                            ),
                            'password_update_date' => date('Y-m-d H:i:s')
                        )
                    );

					// Create log on password update
					$log_session_data = [
						'session' => $this->session->userdata,
						'http_data' => $_SERVER
					];

					$logData = [
						'request_data' => json_encode($this->input->post(null, true), true),
						'session_data' => json_encode( $log_session_data, true),
						'executed_sql' => $this->db->last_query(),
						'log_date'	   => date('Y-m-d H:i:s'),
						'action_interface' => 'Merchant - Change Password',
						'header_data' => json_encode(apache_request_headers(), true),
					];

					$this->general_model->insert_row('merchant_password_log', $logData);

					
					$this->session->set_flashdata('success', 'Password has been changed successfully');

					/* Destroy all login session */
					$sessionRequest 	= new SessionRequest($this->db);
					$updateSession = $sessionRequest->destroySessionUser(SessionRequest::USER_TYPE_MERCHANT,$merchantID);
					/*End destroy session*/
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Please enter correct current password</div>');
			}

			$ap_data =  $this->general_model->get_row_order_data('app_integration_setting', array('merchantID' => $merchantID));

			if (!empty($ap_data)) {
				if ($ap_data['appIntegration'] == 1) {
					redirect(base_url('QBO_controllers/home/index'));
				}

				if ($ap_data['appIntegration'] == 2) {
					redirect(base_url('home/index'));
				}

				if ($ap_data['appIntegration'] == 3) {
					redirect(base_url('FreshBooks_controllers/home/index'));
				}

				if ($ap_data['appIntegration'] == 4) {
					redirect(base_url('Integration/home/index'));
				}

				if ($ap_data['appIntegration'] == 5) {
					redirect(base_url('company/home/index'));
				}
			} else {
				redirect(base_url('firstlogin/dashboard_first_login'));
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Invalid Request</div>');
		}
	}
}
