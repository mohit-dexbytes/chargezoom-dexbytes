<?php

/**
 * Set cron to create invoice for
 * Set cron to create invoice payment
 */
ob_start();
require_once dirname(__FILE__) . '/../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;

use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include_once APPPATH .'libraries/QBD_Sync.php';

class NMICron extends CI_Controller
{
    private $gatewayEnvironment;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/nmiCustomerVault.class.php';
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        include APPPATH . 'third_party/TSYS.class.php';
        include APPPATH . 'third_party/Fluidpay.class.php';
        include APPPATH . 'third_party/EPX.class.php';
        include APPPATH . 'third_party/Cardpointe.class.php';
		$this->load->config('fluidpay');
        $this->load->config('paytrace');
        $this->load->config('auth_pay');
        $this->load->config('TSYS');
        
        $this->load->config('payarc');
        $this->load->library('PayarcGateway');

        $this->load->config('maverick');
        $this->load->library('MaverickGateway');

        $this->load->config('quickbooks');
        $this->load->model('quickbooks');
        $this->load->model('card_model');
        $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        $this->load->model('general_model');
        $this->load->model('company_model');
        $this->load->model('customer_model');
        $this->db1 = $this->load->database('otherdb', true);
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
      
    }

    public function send_mail_data_test()
    {

     
    }

    public function pay_invoice()
    {

		$invoice_data =    $this->quickbooks->get_invoice_data_auto_pay();

        if (!empty($invoice_data)) {

            foreach ($invoice_data as $in_data) {
                $custom_data_fields = [];
                if ($in_data['BalanceRemaining'] != '0.00') {

                    if ($in_data['DueDate'] !== '') {
                        $card_data = $this->card_model->get_single_card_data($in_data['cardID']);
                        $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
                        
						$amount = $in_data['BalanceRemaining'];
						$pay_sts = '';
                        if ($card_data['CardType'] != 'Echeck') {
                            $payOption = 1;
                        }else{
                            $payOption = 2;
                        }
                        $type = 'sale';

                        $merchantCompnay = $in_data['mrCompanyName'];

						$c_data = $cust_data    = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $in_data['Customer_ListID']));
                        $name = $c_data['FirstName'].' '.$c_data['LastName'];
                        $transactionByUser = ['id' => null, 'type' => 4];

						$firstName   = $cust_data['FirstName'];
						$lastName    = $cust_data['LastName'];
						$address1    = $in_data['ShipAddress_Addr1'];
						$address2    = $in_data['ShipAddress_Addr2'];
						$country     = $in_data['ShipAddress_Country'];
						$city        = $in_data['ShipAddress_City'];
						$state       = $in_data['ShipAddress_State'];
						$zip         = $in_data['ShipAddress_PostalCode'];
						$phone       = $cust_data['Phone'];
						$email       = $in_data['Contact'] = $cust_data['Contact'];
						$companyName = $cust_data['companyName'];
						$zipcode = $zip;

						$cvv = $card_data['CardCVV'];
						$expmonth = $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
						$card_no = $card_data['CardNo'];
                        $cardType = $card_data['CardType'];

                        $user = $in_data['qbwc_username'];
                        if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL))
                        {
                           $email = 'devteam@chargezoom.com';
                        }
                        
                        if ($in_data['gatewayType'] == '1' || $in_data['gatewayType'] == '9') {

                            $nmiuser         = $in_data['gatewayUsername'];
                            $nmipass         = $in_data['gatewayPassword'];
                            $nmi_data        = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                            $Customer_ListID = $in_data['Customer_ListID'];

                            $transaction1 = new nmiDirectPost($nmi_data);
                            
                            if($payOption == 2){
                                $transaction1->setAccountName($card_data['accountName']);
                                $transaction1->setAccount($card_data['accountNumber']);
                                $transaction1->setRouting($card_data['routeNumber']);
                                
                                $transaction1->setAccountType($card_data['accountType']);
                                $transaction1->setAccountHolderType($card_data['accountHolderType']);
                                $transaction1->setSecCode($card_data['secCodeEntryMethod']);
                                $transaction1->setPayment('check');
                            } else {

                                $transaction1->setCcNumber($card_data['CardNo']);
                                $expmonth = $card_data['cardMonth'];
                                $exyear   = $card_data['cardYear'];
                                
                                if (strlen($exyear) > 2) {
                                    $minLength = 4 - strlen($exyear);
                                    $exyear   = substr($exyear, $minLength);
                                }

                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }

                                $expry = $expmonth . $exyear;
                                $transaction1->setCcExp($expry);
                                $transaction1->setCvv($card_data['CardCVV']);

                                // add level III data
                                $level_request_data = [
                                    'transaction' => $transaction1,
                                    'card_no' => $card_data['CardNo'],
                                    'merchID' => $in_data['merchantID'],
                                    'amount' => $amount,
                                    'invoice_id' => $in_data['TxnID'],
                                    'gateway' => 1
                                ];
                                $transaction1 = addlevelThreeDataInTransaction($level_request_data);
                            }
                            
                            $transaction1->setAmount($amount);

                            $transaction1->sale();
                            $res = $transaction1->execute();
                            
                            if ($res['response_code'] == "100") {

                                $pay_sts = "SUCCESS";
                            
                            } else {

                            }

                        } else if ($in_data['gatewayType'] == '2') {

                            $apiloginID     = $in_data['gatewayUsername'];
                            $transactionKey = $in_data['gatewayPassword'];

                            $transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
                            $transaction1->setSandbox($this->config->item('auth_test_mode'));
                            if($payOption == 2){
                                $transaction1->setECheck($card_data['routeNumber'], $card_data['accountNumber'], $card_data['accountType'], $bank_name='Wells Fargo Bank NA', $card_data['accountName'], $card_data['secCodeEntryMethod']);
                                
                                $res = $transaction1->authorizeAndCapture($amount);
                            }else{
                                $card_no      = $card_data['CardNo'];
                                $expmonth     = $card_data['cardMonth'];
                                $exyear       = $card_data['cardYear'];
                                $exyear       = substr($exyear, 2);
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $expry = $expmonth . $exyear;

                                $res = $transaction1->authorizeAndCapture($amount, $card_no, $expry);
                            }
                            
                            if ($res->response_code == "1" && $res->transaction_id != 0 && $res->transaction_id != '') {
                                $pay_sts = "SUCCESS";

                            } else {

                            }

                        } else if ($in_data['gatewayType'] == '3') {
                            $this->load->config('paytrace');

                            $payusername = $in_data['gatewayUsername'];
                            $paypassword = $in_data['gatewayPassword'];
                            $integratorId = $in_data['gatewaySignature'];

                            $grant_type  = "password";
                          
                            $payAPI = new PayTraceAPINEW();

                            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

                            //call a function of Utilities.php to verify if there is any error with OAuth token.
                            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                            if (!$oauth_moveforward) {
                                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

                                //set Authentication value based on the successful oAuth response.
                                //Add a space between 'Bearer' and access _token
                                $oauth_token = sprintf("Bearer %s", $json['access_token']);

                                $name     = $in_data['Customer_FullName'];
                                $address  = $in_data['ShipAddress_Addr1'];
                                $city     = $in_data['ShipAddress_City'];
                                $state    = $in_data['ShipAddress_State'];
                                $zipcode  = $in_data['ShipAddress_PostalCode'];
                                
                                if($payOption == 2){
                                    $request_data = array(
                                        "amount"          => $amount,
                                        "check"     => array(
                                            "account_number"=> $card_data['accountNumber'],
                                            "routing_number"=> $card_data['routeNumber'],
                                        ),
                                        "integrator_id" => $integratorId,
                                        "billing_address" => array(
                                            "name" => $card_data['accountName'],
                                            "street_address" => $card_data['Billing_Addr1']. ', '.$card_data['Billing_Addr2'],
                                            "city" => $card_data['Billing_City'],
                                            "state" => $card_data['Billing_State'],
                                            "zip" => $card_data['Billing_Zipcode'],
                                        ),
                                    );
    
                                    $reqURL = URL_ACH_SALE;
                                }else{
                                    $card_no  = $card_data['CardNo'];
                                    $expmonth = $card_data['cardMonth'];

                                    $exyear = $card_data['cardYear'];
                                    $cvv = $card_data['CardCVV'];
                                    if (strlen($expmonth) == 1) {
                                        $expmonth = '0' . $expmonth;
                                    }

                                    $request_data = array(
                                        "amount"          => $amount,
                                        "credit_card"     => array(
                                            "number"           => $card_no,
                                            "expiration_month" => $expmonth,
                                            "expiration_year"  => $exyear),
                                        "invoice_id"      => rand('500000', '200000'),
                                        "billing_address" => array(
                                            "name"           => $name,
                                            "street_address" => $address,
                                            "city"           => $city,
                                            "state"          => $state,
                                            "zip"            => $zipcode,
                                        ),
                                    );

                                    if(!empty($cvv)){
                                        $request_data['csc'] = $cvv;
                                    }
                                    $reqURL = URL_KEYED_SALE;
                                }
                                
                                $type         = 'auth_capture';
                                $request_data = json_encode($request_data);
                                $result       = $payAPI->processTransaction($oauth_token, $request_data, $reqURL);
                                $res     = $payAPI->jsonDecode($result['temp_json_response']);

                                if ($result['http_status_code'] == '200') {
                                    $res['http_status_code']=200;
                                    $pay_sts = "SUCCESS";

                                    // add level three data in transaction
                                    if($res['success']){
                                        if($payOption == 1){
                                            $level_three_data = [
                                                'card_no' => $card_no,
                                                'merchID' => $in_data['merchantID'],
                                                'amount' => $amount,
                                                'token' => $oauth_token,
                                                'integrator_id' => $integratorId,
                                                'transaction_id' => $res['transaction_id'],
                                                'invoice_id' => $in_data['TxnID'],
                                                'gateway' => 3,
                                            ];
                                            addlevelThreeDataInTransaction($level_three_data);
                                        }
                                        
                                    }
                                    
                                } else {

                                }

                            }

                        } else if ($in_data['gatewayType'] == '4') {

                            $username  = $in_data['gatewayUsername'];
                            $password  = $in_data['gatewayPassword'];
                            $signature = $in_data['gatewaySignature'];

                            $config = array(
                                'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                                'APIUsername'  => $username, // PayPal API username of the API caller
                                'APIPassword'  => $password, // PayPal API password of the API caller
                                'APISignature' => $signature, // PayPal API signature of the API caller
                                'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                                'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                            );

                            // Show Errors
                            if ($config['Sandbox']) {
                                error_reporting(E_ALL);
                                ini_set('display_errors', '1');
                            }

                            $this->load->library('paypal/Paypal_pro', $config);

                            $creditCardType = 'Visa';
                            $card_no = $card_data['CardNo'];
                            $expDateMonth     = $card_data['cardMonth'];
                            $expDateYear      = $card_data['cardYear'];
                            $creditCardNumber = $card_no;

                            // Month must be padded with leading zero
                            $padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                            $cvv2Number   = $card_data['CardCVV'];
                            $currencyID   = "USD";

                            $DPFields = array(
                                'paymentaction'    => 'Sale', // How you want to obtain payment.
                                //Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                                'ipaddress'        => '', // Required.  IP address of the payer's browser.
                                'returnfmfdetails' => '0', // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                            );

                            $CCDetails = array(
                                'creditcardtype' => $creditCardType, // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                                'acct'           => $creditCardNumber, // Required.  Credit card number.  No spaces or punctuation.
                                'expdate'        => $padDateMonth . $expDateYear, // Required.  Credit card expiration date.  Format is MMYYYY
                                'startdate'      => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                                'issuenumber'    => '', // Issue number of Maestro or Solo card.  Two numeric digits max.
                            );

                            if(!empty($cvv2Number)){
                                $CCDetails['cvv2'] = $cvv2Number;
                            }

                            $PayerInfo = array(
                                'email'       => $email, // Email address of payer.
                                'payerid'     => '', // Unique PayPal customer ID for payer.
                                'payerstatus' => 'verified', // Status of payer.  Values are verified or unverified
                                'business'    => '', // Payer's business name.
                            );

                            $PayerName = array(
                                'salutation' => $companyName, // Payer's salutation.  20 char max.
                                'firstname'  => $firstName, // Payer's first name.  25 char max.
                                'middlename' => '', // Payer's middle name.  25 char max.
                                'lastname'   => $lastName, // Payer's last name.  25 char max.
                                'suffix'     => '', // Payer's suffix.  12 char max.
                            );

                            $BillingAddress = array(
                                'street'      => $address1, // Required.  First street address.
                                'street2'     => $address2, // Second street address.
                                'city'        => $city, // Required.  Name of City.
                                'state'       => $state, // Required. Name of State or Province.
                                'countrycode' => $country, // Required.  Country code.
                                'zip'         => $zip, // Required.  Postal code of payer.
                                'phonenum'    => $phone, // Phone Number of payer.  20 char max.
                            );

                            $PaymentDetails = array(
                                'amt'          => $amount, // Required.  Total amount of order, including shipping, handling, and tax.
                                'currencycode' => $currencyID, // Required.  Three-letter currency code.  Default is USD.
                                'itemamt'      => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                'shippingamt'  => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                'insuranceamt' => '', // Total shipping insurance costs for this order.
                                'shipdiscamt'  => '', // Shipping discount for the order, specified as a negative number.
                                'handlingamt'  => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                'taxamt'       => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                                'desc'         => '', // Description of the order the customer is purchasing.  127 char max.
                                'custom'       => '', // Free-form field for your own use.  256 char max.
                                'invnum'       => '', // Your own invoice or tracking number
                                'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                                'notifyurl'    => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                                'recurring'    => '', // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                            );

                            $PayPalRequestData = array(
                                'DPFields'       => $DPFields,
                                'CCDetails'      => $CCDetails,
                                'PayerInfo'      => $PayerInfo,
                                'PayerName'      => $PayerName,
                                'BillingAddress' => $BillingAddress,

                                'PaymentDetails' => $PaymentDetails,

                            );

                            $PayPalResult = $res = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

                            if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {

                                $code = '111';
                                $pay_sts = "SUCCESS";


                            } else {
                                $code   = '401';
                                $tranID = '';
                                $amt    = '0.00';

                            }

                        } else if ($in_data['gatewayType'] == '5') {

                            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
                            $stripeKey = $in_data['gatewayUsername'];
                            $strPass   = $in_data['gatewayPassword'];
                            $amount    = $in_data['BalanceRemaining'];

                            $real_amt = (int) ($amount * 100);

                            $plugin = new ChargezoomStripe();
                            $plugin->setApiKey($stripeKey);
                            $cardData = [
                                'number'    => $card_data['CardNo'],
                                'exp_month' => $card_data['cardMonth'],
                                'exp_year'  => $card_data['cardYear'],
                                'name'      => $c_data['FullName'],
                            ];

                            if(!empty($cvv)){
                                $cardData['cvc'] = $cvv;
                            }
                            $res = \Stripe\Token::create([
                                'card' => $cardData,
                            ]);

                            $tcharge = json_encode($res);

                            $rest = json_decode($tcharge);
                            if ($rest->id) {

                                $plugin = new ChargezoomStripe();
                                $plugin->setApiKey($strPass);
                                $charge = \Stripe\Charge::create(array(
                                    "amount"      => $real_amt,
                                    "currency"    => "usd",
                                    "source"      => $rest->id,
                                    "description" => "Charge Using Stripe Gateway",

                                ));

                                $charge = json_encode($charge);

                                $result  = json_decode($charge);
                                $type    = 'stripe_sale';
                                $crtxnID = '';
                                $res = $result;
                                if ($result->paid == '1' && $result->failure_code == "") {
                                    $pay_sts = "SUCCESS";
                                } else {
                                }

                                
                            }

                        } else if ($in_data['gatewayType'] == '6') {

                            require_once APPPATH . "third_party/usaepay/usaepay.php";
                            $this->load->config('usaePay');

                            $crtxnID          = '';
                            $invNo            = mt_rand(1000000, 2000000);
                            $transaction      = new umTransaction;
                            $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                            $transaction->key = $in_data['gatewayUsername'];

                            $transaction->pin         = $in_data['gatewayPassword'];
                            $transaction->usesandbox  = $this->config->item('Sandbox');
                            $transaction->invoice     = $invNo; // invoice number.  must be unique.
                            $transaction->description = "Chargezoom Invoice Payment"; // description of charge

                            $transaction->testmode = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
                            $transaction->command  = "sale";

                            $card_no      = $card_data['CardNo'];
                            $expmonth     = $card_data['cardMonth'];
                            $exyear       = $card_data['cardYear'];
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
								$expmonth = '0' . $expmonth;
                            }
							$expry = $expmonth . $exyear;
							
                            if ($cvv != "") {
								$transaction->cvv2 = $cvv;
                            }
							
							$transaction->exp = $expry;
							$transaction->card = $card_no;

                            $transaction->billstreet  = $address1;
                            $transaction->billstreet2 = $address2;
                            $transaction->billcountry = $country;
                            $transaction->billcity    = $city;
                            $transaction->billstate   = $state;
                            $transaction->billzip     = $zipcode;

                            $transaction->shipstreet  = $address1;
                            $transaction->shipstreet2 = $address2;
                            $transaction->shipcountry = $country;
                            $transaction->shipcity    = $city;
                            $transaction->shipstate   = $state;
                            $transaction->shipzip     = $zipcode;
                            $transaction->amount      = $amount;

                            $transaction->Process();
                            $type = 'sale';
                            if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {

                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;

                                $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                $pay_sts = "SUCCESS";

                            } else {
                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;
                                $res  = array('transactionCode' => 300, 'status' => $msg, 'transactionId' => $trID);

                            }

                        } else if ($in_data['gatewayType'] == '7') {
							require_once dirname(__FILE__) . '/../../vendor/autoload.php';
                            $this->load->config('globalpayments');

							$payusername   = $in_data['gatewayUsername'];
							$secretApiKey   = $in_data['gatewayPassword'];

                            $crtxnID = '';

                            $config = new PorticoConfig();
               
							$config->secretApiKey = $secretApiKey;
							$config->serviceUrl =  $this->config->item('GLOBAL_URL');
							$customerID = $in_data['Customer_ListID'] ;
							ServicesContainer::configureService($config);

                            if($payOption == 2){
                                $check = new ECheck();
                                $check->accountNumber = $card_data['accountNumber'];
                                $check->routingNumber = $card_data['routeNumber'];
                                if(strtolower($card_data['accountType']) == 'checking'){
                                    $check->accountType = 0;
                                }else{
                                    $check->accountType = 1;
                                }

                                if(strtoupper($card_data['accountHolderType']) == 'PERSONAL'){
                                    $check->checkType = 0;
                                }else{
                                    $check->checkType = 1;
                                }
                                $check->checkHolderName = $card_data['accountName'];
                                $check->secCode = "WEB";

                                
                            }else{
                                $card = new CreditCardData();
                                $card->number = $card_no;
                                $card->expMonth = $expmonth;
                                $card->expYear = $exyear;
                                if($cvv!="")
                                    $card->cvn = $cvv;
                                $card->cardType = $cardType;
                            }

                            $address                 = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city           = $city;
                            $address->state          = $state;
                            $address->postalCode     = $zipcode;
                            $address->country        = $country;

                            $invNo = mt_rand(1000000, 2000000);
                            try
                            {
                                if($payOption == 2){
                                    $response = $check->charge($amount)
                                            ->withCurrency('USD')
                                            ->withAddress($address)
                                            ->withInvoiceNumber($invNo)
                                            ->withAllowDuplicates(true)
                                            ->execute();
                                }else{
                                    $response = $card->charge($amount)
                                    ->withCurrency('USD')
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();
                                }

                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                                    if($payOption == 1){
                                        // add level three data
                                        $transaction = new Transaction();
                                        $transaction->transactionReference = new TransactionReference();
                                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                                        $level_three_request = [
                                            'card_no' => $card_no,
                                            'amount' => $amount,
                                            'invoice_id' => $invNo,
                                            'merchID' => $in_data['merchantID'],
                                            'transaction_id' => $response->transactionId,
                                            'transaction' => $transaction,
                                            'levelCommercialData' => $levelCommercialData,
                                            'gateway' => 7
                                        ];
                                        addlevelThreeDataInTransaction($level_three_request);

                                    }
                                    
                                    $msg     = $response->responseMessage;
                                    $trID    = $response->transactionId;
                                    $st      = '0';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success ";
                                    $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                    $pay_sts = "SUCCESS";

                                } else {
                                    $res = array('transactionCode' => $response->responseCode, 'status' => $response->responseMessage, 'transactionId' => $response->transactionId);

                                }

                            } catch (BuilderException $e) {
                                $error = 'Build Exception Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ConfigurationException $e) {
                                $error = 'ConfigurationException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (GatewayException $e) {
                                $error = 'GatewayException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (UnsupportedTransactionException $e) {
                                $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ApiException $e) {
                                $error = ' ApiException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            }

                        } else if ($in_data['gatewayType'] == '8') {
                            $this->load->config('cyber_pay');

                            $option =array();
                            $option['merchantID']     = trim($in_data['gatewayUsername']);
                            $option['apiKey']         = trim($in_data['gatewayPassword']);
                            $option['secretKey']      = trim($in_data['gatewaySignature']);
                            
                            if($this->config->item('Sandbox')){
                                $env   = $this->config->item('SandboxENV');
                            } else{
                                $env   = $this->config->item('ProductionENV');
                            }
                            $option['runENV']      = $env;
                            $Customer_ListID = $in_data['Customer_ListID'];

                            $commonElement = new CyberSource\ExternalConfiguration($option);
                            $config = $commonElement->ConnectionHost();
                        
                            $merchantConfig = $commonElement->merchantConfigObject();
                            $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                            $api_instance = new CyberSource\Api\PaymentsApi($apiclient);

                            $cliRefInfoArr = [
                                "code" => $merchantCompnay
                            ];
                            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

                            if($flag == "true") {
                                $processingInformationArr = [
                                    "capture" => true, "commerceIndicator" => "internet"
                                ];
                            } else {
                                $processingInformationArr = [
                                    "commerceIndicator" => "internet"
                                ];
                            }
                            $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

                            $amountDetailsArr = [
                                "totalAmount" => $amount,
                                "currency" => CURRENCY
                            ];
                            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                            $billtoArr = [
                                "firstName" => $firstName,
                                "lastName" => $lastName,
                                "address1" => $address1,
                                "postalCode" => $zipcode,
                                "locality" => $city,
                                "administrativeArea" => $state,
                                "country" => $country,
                                "phoneNumber" => $phone,
                                "company" => $companyName,
                                "email" => $email
                            ];
                            $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

                            $orderInfoArr = [
                                "amountDetails" => $amountDetInfo, 
                                "billTo" => $billto
                            ];
                            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

                            $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"expirationMonth" => $expmonth
                            ];
                            
                            if(!empty($cvv)){
                                $paymentCardInfo['securityCode'] = $cvv;
                            }

                            $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                            
                            $paymentInfoArr = [
                                "card" => $card
                            ];
                            $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                        
                            $paymentRequestArr = [
                                "clientReferenceInformation" => $client_reference_information, 
                                "orderInformation" => $order_information, 
                                "paymentInformation" => $payment_information, 
                                "processingInformation" => $processingInformation
                            ];
                            $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                            
                            $api_response = list($response, $statusCode, $httpHeader) = null;

                            try
                            {
                                //Calling the Api
                                $api_response = $api_instance->createPayment($paymentRequest);
                                
                                if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                                    $trID =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                   
                                    $pay_sts = "SUCCESS";
                                    
                                    $code =   '200';
                                    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                } else {
                                    $trID  =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                    $code =   $api_response[1];
                                    $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
                                    
                                    $error = $api_response[0]['status'];
                                }
                            }
                            catch(Cybersource\ApiException $e)
                            {
                                $error = $e->getMessage();
                                $res =array('transactionCode'=>500, 'status'=>$error, 'transactionId'=> '' ); 
                            }

                        } else if ($in_data['gatewayType'] == '10') {

                            $apiUsername     = $in_data['gatewayUsername'];
                            $apiKey = $in_data['gatewayPassword'];
                            if($payOption == 2){
                                $payload = [
                                    "amount" => ($amount * 100),
                                    "ach" => [
                                        "name" => $name, 
                                        "account_number" => $card_data['accountNumber'],
                                        "routing_number" => $card_data['routeNumber'], 
                                        "phone_number" => $card_data['Billing_Contact'], 
                                        "sec_code" => $card_data['secCodeEntryMethod'],
                                        "savings_account" => (strtolower($card_data['accountType']) == 'savings') ? true : false, 
                                    ],
                                    "address" => [
                                        "line1" => $card_data['Billing_Addr1'],
                                        "line2" => $card_data['Billing_Addr2'],
                                        "city" => $card_data['Billing_City'],
                                        "state" => $card_data['Billing_State'],
                                        "postal_code" => $card_data['Billing_Zipcode'],
                                        "country" => $card_data['Billing_Country']
                                    ],
                                ];
                            }else{
                                if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                    $expmonth = substr($expmonth, 1);
                                }
                                $payload = array(
                                    "amount"          => ($amount * 100),
                                    "card"     => array(
                                        "name" => $name,
                                        "number" => $card_no,
                                        "exp_month" => $expmonth,
                                        "exp_year"  => $exyear,
                                    ),
                                    "billing_address" => array(
                                        "line1"        => $address1,
                                        "line2" => $address2,
                                        "city"         => $city,
                                        "state"        => $state,
                                        "postal_code"  => $zip,
                                    ),
                                );
                                if($cvv && !empty($cvv)){
                                    $payload['card']['cvv'] = $cvv;
                                }
                            }
                            
                            $sdk = new iTTransaction();
                            $res = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
                            if ($res['status_code'] == "200" || $res['status_code'] == "201") {
                                $res['status_code'] = "200";
                                $pay_sts = "SUCCESS";
                            } else {
                            }
                        } else if ($in_data['gatewayType'] == '11' || $in_data['gatewayType'] == '13') {

                            $apiUsername     = $in_data['gatewayUsername'];
							$calamount = $amount * 100;
							$payload = [
								"amount" => round($calamount,2),
								"type"                => "sale",
								"address" => [
									"line1" => $card_data['Billing_Addr1'],
									"line2" => $card_data['Billing_Addr2'],
									"city" => $card_data['Billing_City'],
									"state" => $card_data['Billing_State'],
									"postal_code" => $card_data['Billing_Zipcode'],
									"country" => $card_data['Billing_Country']
								],
								"name" => $name, 
							];
							if($payOption == 2){
								$payload["payment_method"] = [
									'ach' => [
										"routing_number" => $card_data['routeNumber'],
                                        "account_number" => $card_data['accountNumber'],
                                        "sec_code"       => $card_data['secCodeEntryMethod'],
                                        "account_type"   => $card_data['accountType'], 
									]
								];
							} else {
								$exyear1  = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
								}
								$expry = $expmonth . $exyear1;

								$payload["payment_method"] = [
									"card" => array(
                                        "entry_type"      => "keyed",
                                        "number"          => $card_no,
                                        "expiration_date" => $expry,
                                    ),
                                ];
                                if($cvv && !empty($cvv)){
                                    $payload['payment_method']['card']['cvc'] = $cvv;
                                }
							}
							
                            $gatewayTransaction              = new Fluidpay();
							$gatewayTransaction->environment = $this->gatewayEnvironment;
							$gatewayTransaction->apiKey      = $apiUsername;
							$result = $gatewayTransaction->processTransaction($payload);
                            $res = $result;
							if ($result['status'] == 'success') {
								$responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            } else {
                            }
						} else if ($in_data['gatewayType'] == '12') {

                            $card_no      = $card_data['CardNo'];
                            $expmonth     = $card_data['cardMonth'];
                            $exyear       = $card_data['cardYear'];
                            $exyear1       = substr($exyear, 2);
                            if(empty($exyear1)){
                                $exyear1  = $exyear;
                            }
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth.'/'.$exyear1;

                            
                            $deviceID = $in_data['gatewayMerchantID'].'01';
                            $gatewayTransaction              = new TSYS();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->deviceID = $deviceID;
                            $res = $result = $gatewayTransaction->generateToken($in_data['gatewayUsername'],$in_data['gatewayPassword'],$in_data['gatewayMerchantID']);
                            $generateToken = '';
                            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                                
                            }
                            $gatewayTransaction->transactionKey = $generateToken;
                            $responseType = 'SaleResponse';
                            $address1 = ($address1 != '')?$address1:'None';
                            $address2 = ($address2 != '')?$address2:'None';
                            $city     =  ($city != '')?$city:'None';
                            $zipcode = ($zipcode != '')?$zipcode:'None';
                            $state    = ($state != '')?$state:'AZ';
                            $country  = ($country != '')?$country:'USA';
                            $phone = ($phone != '')?$phone:'None';

                            $amount = round($amount,2);

                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => (int)($amount * 100),
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => $address1,
                                "zip"                               => $zipcode,
                                "orderNumber"                       => $invoiceID,
                                "notifyEmailID"                     => (isset($c_data['Contact']))?$c_data['Contact']:'chargezoom@chargezoom.com',
                                "firstName"                         => (isset($c_data['FirstName']))?$c_data['FirstName']:'None',
                                "lastName"                          => (isset($c_data['LastName']))?$c_data['LastName']:'None',
                              
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                            "contactDetails" => array(
                                            
                                                "addressLine1"=> $address1,
                                                 "addressLine2"  => $address2,
                                                "city"=>$city,
                                                "zip"=>$zipcode,
                                            ),
                                            "shippingDetails" => array( 
                                                "firstName"=>(isset($c_data['FirstName']))?$c_data['FirstName']:'None',
                                                "lastName"=>(isset($c_data['LastName']))?$c_data['LastName']:'None',
                                                "addressLine1"=>$address1,
                                                 "addressLine2" => $address2,
                                                "city"=>$city,
                                                "zip"=>$zipcode,
                                                "emailID"=>(isset($c_data['Contact']))?$c_data['Contact']:'chargezoom@chargezoom.com'
                                             )
                                        )
                            );
                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }
                            if($generateToken != ''){ 
                                $res = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
                            
                            if (isset($res[$responseType]['status']) && $res[$responseType]['status'] == 'PASS') {
                                $pay_sts = "SUCCESS";

                            }
                            $res['responseType'] = $responseType;
                        } else if ($in_data['gatewayType'] == '14') {
                            $cardpointeuser = $apiUsername     = $in_data['gatewayUsername'];
							$cardpointepass = $apiPassword     = $in_data['gatewayPassword'];
							$cardpointeMerchID = $apiMerchantId   = $in_data['gatewayMerchantID'];
                            $cardpointeSiteName   = $in_data['gatewaySignature'];
							$client = new CardPointe();
							if($payOption == 2){
                                $res = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_data['accountNumber'], $card_data['routeNumber'], $amount, $card_data['accountType'], $name, $card_data['Billing_Addr1'], $card_data['Billing_City'],$card_data['Billing_State'],$card_data['Billing_Zipcode']);
								
							} else {
								$card_no      = $card_data['CardNo'];
								$expmonth     = $card_data['cardMonth'];
								$exyear       = $card_data['cardYear'];
								$exyear       = substr($exyear, 2);
								if (strlen($expmonth) == 1) {
									$expmonth = '0' . $expmonth;
							    }
								$expry = $expmonth . $exyear;

                                $res = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount, $cvv, $name, $card_data['Billing_Addr1'], $card_data['Billing_City'],$card_data['Billing_State'],$card_data['Billing_Zipcode']);
							}
                            if ($res['respcode'] == '00') {
                                $pay_sts = "SUCCESS";
						    }
                        } else if ($in_data['gatewayType'] == '15') {

                            // Payarc
							$apiUsername     = $in_data['gatewayUsername'];
                                                       
                            $exyear  = $exyear;
							if (strlen($expmonth) == 1) {
								$expmonth = '0' . $expmonth;
                            }
                            
                            $this->load->config('payarc');
                            $this->load->library('PayarcGateway');
                            
							$this->payarcgateway->setApiMode($this->config->item('environment'));
                        	$this->payarcgateway->setSecretKey($apiUsername);
							
							// Create Credit Card Token
							$address_info = [
                                        'address_line1' => $card_data['Billing_Addr1'],
                                        'address_line2' => $card_data['Billing_Addr2'],
                                        'state' => $card_data['Billing_State'],
                                        'country' => '',
                                    ];


							$token_response = $this->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);
	
							$token_data = json_decode($token_response['response_body'], 1);

							if(isset($token_data['status']) && $token_data['status'] == 'error'){
                                $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->gatewayEnvironment,'accessKey'=>$apiUsername, 'card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
								// Error while creating the credit card token
								$err_msg      = $token_data['message'];
								$pay_sts = "ERROR";
						
							} else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {
							
								// If token created
								$token_id = $token_data['data']['id'];
						
								$charge_payload = [];
						
								$charge_payload['token_id'] = $token_id;
								
								$charge_payload['email'] = (isset($email))?$email:'chargezoom@chargezoom.com';
								
								if(isset($phone) && $phone){
									$charge_payload['phone_number'] = $phone; // Customer's contact phone number..
								}
						
								$charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD
						
								$charge_payload['currency'] = 'usd'; 
						
								$charge_payload['capture'] = '1';
						
								$charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.
						
								if(isset($card_data['Billing_Zipcode']) && $card_data['Billing_Zipcode']) {
									$charge_payload['ship_to_zip'] = $card_data['Billing_Zipcode']; 
								};
	
								$charge_payload['statement_description'] = '';
						
								$charge_response = $this->payarcgateway->createCharge($charge_payload);
						
								$result = json_decode($charge_response['response_body'], 1);
								
								// Handle Card Decline Error
                                if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
                                {
                                    $result['message'] = $result['data']['failure_message'];
                                }

                                $res = $result;

								if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
									$responseId = $result['data']['id'];
                                	$pay_sts = "SUCCESS";
								} else {
                                    $pay_sts = "Error! ".$result['message'];
                                }
							}
                        }  else if ($in_data['gatewayType'] == '17') {
                           
                            // Maverick Payment Gateway
                            $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                            $this->maverickgateway->setTerminalId($in_data['gatewayPassword']);
                            $this->maverickgateway->setAccessToken($in_data['gatewayUsername']);

							if($payOption == 2){
								
                                // ACH
                                // get DBA id
                                $dbaId = $this->maverickgateway->getDba(true);

                                // electronic Sale
                                $request_payload = [
                                        'amount'          => $amount,
                                        'routingNumber'   => $card_data['routeNumber'],
                                        'accountName'     => $card_data['accountName'],
                                        'accountNumber'   => $card_data['accountNumber'],
                                        'accountType'     => ucwords($card_data['accountType']), // Checking or Savings
                                        'transactionType' => 'Debit', // Debit or Credit
                                        'customer' => [
                                            'email'     => $email,
                                            'firstName' => $firstName,
                                            'lastName'  => $lastName,
                                            "address1"  => $card_data['Billing_Addr1'],
                                            "address_2" => $card_data['Billing_Addr2'],
                                            "city"      => $card_data['Billing_City'],
                                            "state"     => $card_data['Billing_State'],
                                            "zipCode"   => $card_data['Billing_Zipcode'],
                                            "country"   => $card_data['Billing_Country'],
                                            "phone"     => $card_data['Billing_Contact'],
                                        ],
                                        'dba' => [
                                            'id' => $dbaId,
                                        ],
                                    ];
                                // Process ACH
                                $r = $this->maverickgateway->processAch($request_payload);
							} else {
                                $card_no      = $card_data['CardNo'];
                                $expmonth     = $card_data['cardMonth'];
                                $exyear       = $card_data['cardYear'];
								$exyear       = substr($exyear, -2);
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $expry = $expmonth.'/'.$exyear;

								// Sale Payload
                                $request_payload = [
                                    'level' => 1,
                                    'threeds' => [
                                        'id' => null,
                                    ],
                                    'amount' => $amount,
                                    'card' => [
                                        'number' => $card_no,
                                        'cvv'    => $cvv,
                                        'exp'    => $expry,
                                        'save'   => 'No',
                                        'address' => [
                                            'street' => $address1,
                                            'city' => $city,
                                            'state' => $state,
                                            'country' => $country,
                                            'zip' => $zipcode,
                                        ]
                                    ],
                                    'contact' => [
                                        'name'   => $in_data['FirstName'].' '.$in_data['LastName'],
                                        'email'  => '',
                                        'phone' => $phone,
                                    ]
                                ];
                
                                // Process Sale
                                $r = $this->maverickgateway->processSale($request_payload);
							}

                            $result = [];

                            $rbody = json_decode($r['response_body'], true);

                            $result['response_code'] = $r['response_code'];

                            $result['data'] = $rbody;
                            
                            // Response                
                            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                                if(isset($rbody['id']) && $rbody['id']){
                                    $result['status'] = 'success';
                                    $result['msg'] = $result['message'] = 'Payment success.';
                                    $result['data']['id'] =  $rbody['id'];
                                } else {
                                    $result['status'] = 'failed';
                                    $result['msg'] = $result['message'] = 'Payment failed.';    
                                }

                            } else {
                                $result['status'] = 'failed';
                                $result['msg'] = $result['message'] = $rbody['message'];
                            }


                            $res = $result;
							if ($result['status'] == 'success') {
								$responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            } else {
                            }
                            // Maverick Payment Ends
                        }
                         else if ($in_data['gatewayType'] == '16') {
                            /*EPX Credit card*/
                            $this->load->config('EPX');
                            $CUST_NBR = $in_data['gatewayUsername'];
                            $MERCH_NBR = $in_data['gatewayPassword'];
                            $DBA_NBR = $in_data['gatewaySignature'];
                            $TERMINAL_NBR = $in_data['extra_field_1'];
                            $orderId = time();
                            $amount = number_format($amount,2,'.','');
                            $transaction = array(
                                    'CUST_NBR' => $CUST_NBR,
                                    'MERCH_NBR' => $MERCH_NBR,
                                    'DBA_NBR' => $DBA_NBR,
                                    'TERMINAL_NBR' => $TERMINAL_NBR,
                                    'AMOUNT' => $amount,
                                    'TRAN_NBR' => rand(1,10),
                                    'BATCH_ID' => time(),
                                    'VERBOSE_RESPONSE' => 'Y',
                            );
                            if($firstName != ''){
                                $transaction['FIRST_NAME'] = $firstName;
                            }
                            if($lastName != ''){
                                $transaction['LAST_NAME'] = $lastName;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            if( $zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                            
                            if($payOption == 2){
                                $transaction['RECV_NAME'] = $card_data['accountName'];
                                $transaction['ACCOUNT_NBR'] = $card_data['accountNumber'];
                                $transaction['ROUTING_NBR'] = $card_data['routeNumber'];

                                if($card_data['accountType'] == 'savings'){
                                    $transaction['TRAN_TYPE'] = 'CKS2';
                                }else{
                                    $transaction['TRAN_TYPE'] = 'CKC2';
                                }
                                

                            } else {
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $exyear1  = substr($exyear, 2);

                                $transaction['EXP_DATE'] = $exyear1.$expmonth;
                                $transaction['ACCOUNT_NBR'] = $card_no;
                                $transaction['TRAN_TYPE'] = 'CCE1';
                                $transaction['CARD_ENT_METH'] = 'E';
                                $transaction['INDUSTRY_TYPE'] = 'E';
                            }
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                            
                            $res = $result;
                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                $responseId = $transactionID = $result['AUTH_GUID'];
                                $pay_sts = "SUCCESS";
                                
                            } else {
                                $responseId = 'TXNFAILED-'.time();
                                $pay_sts = $result['AUTH_RESP_TEXT'];
                            }
                        } 

                           
						$crtxnID = '';
						if ($pay_sts == "SUCCESS") {

                            $txnID  = $in_data['TxnID'];
                            $ispaid = 'true';

                            

                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            if ($bamount > 0) {
                                $ispaid = 'false';
                            }

                            $app_amount = $in_data['AppliedAmount'] - $amount;
                            $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                            $condition  = array('TxnID' => $in_data['TxnID']);

                            $this->general_model->update_row_data('qb_test_invoice', $condition, $data);
                        } else if(isset($in_data['recurring_send_mail']) && $in_data['recurring_send_mail']){
                        }
                        if(isset($res)){
                            $id = $this->general_model->insert_gateway_transaction_data($res, $type, $in_data['gatewayID'], $in_data['gatewayType'], $in_data['Customer_ListID'], $amount, $in_data['merchantID'], $crtxnID, $in_data['resellerID'], $in_data['TxnID'], false, $transactionByUser,$custom_data_fields);
                        }
                        
                        if ($pay_sts == "SUCCESS") {
                            if(isset($in_data['recurring_send_mail']) && $in_data['recurring_send_mail']){
                                $this->send_mail_data($in_data, '5');
                            }
                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                        }

                    }
                }
            }
        }

    }

    public function get_vault_data($customerID)
    {
        $this->load->library('encrypt');
        $card = array();

        $sql       = "SELECT * from customer_card_data  where  customerListID='$customerID'  order by CardID desc limit 1  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']                   = $this->encrypt->decode($card_data['CustomerCard']);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function get_card_by_ID($carID)
    {
        $this->load->library('encrypt');
        $card = array();

        $sql       = "SELECT * from customer_card_data  where  CardID='$carID'   ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']                   = $this->encrypt->decode($card_data['CustomerCard']);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function send_mail_data($indata = array(), $type)
    {


        $customerID = $indata['Customer_ListID'];
        $companyID  = $indata['companyID'];
        $typeID     = $type;
        $invoiceID  = $indata['TxnID'];
        

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));

        $merchantID = $comp_data['merchantID'];

        $condition = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data = $this->company_model->template_data($condition);

        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $comp_data['merchantID']));

        $currency = "$";

        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = $merchant_data['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $tr_amount           = '0.00';
        $update_link         = $config_data['customerPortalURL'];
        $gateway_msg         = '';
        $companyName         = '';

        $condition1 = " and Customer_ListID='" . $customerID . "' AND inv.qb_inv_merchantID = '$merchantID' ";
        $condition1 .= " and TxnID='" . $invoiceID . "' ";

        if ($typeID == '5') {
            $data = $this->customer_model->get_invoice_data_template($condition1);
        }
        if ($typeID == '4') {
            $data      = $this->customer_model->get_invoice_data_template($condition1);
            $card_data = $this->card_model->get_single_card_data($indata['cardID']);
            if (!empty($card_data)) {
                $cardno        = $card_data['CustomerCard'];
                $friendly_name = $card_data['customerCardfriendlyName'];
            }

        }

        if (!empty($data)) {
            $companyName = $data['comapanyName'];
            $customer = $data['FullName'];
            $amount   = $data['AppliedAmount'];

            $balance    = $data['BalanceRemaining'];
            $paymethods = $data['paymentType'];

            $duedate    = date('F d, Y', strtotime($data['DueDate']));
            $ref_number = $data['RefNumber'];
            $tr_date    = date('F d, Y', strtotime($data['tr_date']));
            $tr_data    = $data['tr_data'];
            $tr_amount  = $data['tr_amount'];
        }

        if ($typeID == '5' && isset($merchant_data['merchant_default_timezone'])  && !empty($merchant_data['merchant_default_timezone'])) {
            // Convert added date in timezone 
            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $merchant_data['merchant_default_timezone']];
            $tr_date = getTimeBySelectedTimezone($timezone);
            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
        }
        $subject = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $view_data['emailSubject']));
        if ($view_data['emailSubject'] == "Welcome to { company_name }") {
            $subject = 'Welcome to ' . $company;
        }

        $message = $view_data['message'];
        /* Check merchant logo is available if yes than display otherwise display default chargezoom */
        $logo_url = CZLOGO;
        if($config_data['ProfileImage']!=""){
            $logo_url = LOGOURL1.$config_data['ProfileImage']; 
        }

        $logo_url = "<img src='" . $logo_url . "' />";
        $message  = stripslashes(str_ireplace('{{logo}}', $logo_url, $message));

        $message = stripslashes(str_ireplace('{{creditcard.type_name}}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{creditcard.type_name}}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{creditcard.url_updatelink}}', $update_link, $message));
        $message = stripslashes(str_ireplace('{{merchant_company_name}}',$merchant_name ,$message));
        $message = stripslashes(str_ireplace('{{customer.company}}', $companyName, $message));
        $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{transaction.amount}}', ($tr_amount) ? ($tr_amount) : '0.00', $message));

        $message = stripslashes(str_ireplace('{{transaction.transaction_method}}', $paymethods, $message));
        $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message));
        $message = stripslashes(str_ireplace('{{transaction.transaction_detail}}', $tr_data, $message));

        $message = stripslashes(str_ireplace('{% transaction.gateway_method %}', $paymethods, $message));
        $message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message));
        $message = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}', $duedate, $message));
        $message = stripslashes(str_ireplace('{{transaction.gateway_msg}}', $transaction_details, $message));

        $message = stripslashes(str_ireplace('{{invoice.url_permalink}}', $in_link, $message));
        $message = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{merchant_phone}}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{current.date}}', $cur_date, $message));
        $message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}','' ,$message ));
        $fromEmail    = isset($view_data['fromEmail'])? $view_data['fromEmail']:$merchant_data['merchantEmail'];
        $toEmail   = isset($indata['Contact'])? $indata['Contact']:$indata['userEmail'];
        $addCC     = $view_data['addCC'];
        $addBCC    = $view_data['addBCC'];
        $replyTo    = isset($view_data['replyTo'])?$view_data['replyTo']:$config_email;

        $isValid = is_a_valid_customer([
			'email' => $toEmail
		]);
		if(!$isValid){
			return false;
		}

      

        $mailData = [
            'to' => $toEmail,
            'from' => $fromEmail,
            'fromname' => $view_data['mailDisplayName'],
            'subject' => $subject,
            'replyto' => $replyTo,
            'html' => $message,
            'addBCC' => $addBCC,
            'addCC' => $addCC,
        ];
        sendEmailUsingSendGrid($mailData);

    }

    public function set_deu_invoice()
    {
        #Stopped email functionality for CZ-921
        die;
        $datas = $this->quickbooks->get_invoice_mail_data('1');

        foreach ($datas as $k => $data) {
            if ($data['overDue']) {
                $this->set_template_datainvoice('1', $data);
            }

        }

    }

    public function set_deu_invoice_past()
    {
        #Stopped email functionality for CZ-921
        die;
        $datas = $this->quickbooks->get_invoice_mail_data('0');

        foreach ($datas as $k => $data) {
            if ($data['overDue']) {
                $this->set_template_datainvoice('2', $data);
            }

        }
    }

    public function set_credit_card_expired_soon()
    {
        #Stopped email functionality for CZ-921
        die;
        $datas = $this->get_expiry_card_data('0');

        if (!empty($datas)) {
            foreach ($datas as $k => $data) {
                $row_data = $this->customer_model->get_customer_details_id($data['customerListID'], $data['merchantID']);

                if (!empty($row_data)) {
                    $this->set_template_datainvoice('12', $row_data);
                }
            }
        }
    }

    public function set_credit_card_expired()
    {
        #Stopped email functionality for CZ-921
        die;
        $datas = $this->get_expiry_card_data('1');

        if (!empty($datas)) {
            foreach ($datas as $k => $data) {
                $row_data = $this->customer_model->get_customer_details_id($data['customerListID'], $data['merchantID']);

                if (!empty($row_data)) {
                    $this->set_template_datainvoice('11', $row_data);
                }
            }
        }

    }

    public function set_template_datainvoice($typeID, $invoce_data)
    {

        if ($typeID == '1' || $typeID == '2') {
            $customerID = $invoce_data['Customer_ListID'];
        }

        if ($typeID == '11' || $typeID == '12') {
            $customerID = $invoce_data['customerID'];
        }

        if(isset($invoce_data['merchantID'])){
            $merchantID = $invoce_data['merchantID'];
        } elseif(isset($invoce_data['companyID'])){
            $companyID = $invoce_data['companyID'];
            $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));
            $merchantID = $comp_data['merchantID'];
        } else {
            return false;
        }

        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));

        $condition = array('templateType' => $typeID, 'merchantID' => $merchantID);
        $view_data = $this->company_model->template_data($condition);

        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));
        $currency      = "$";

        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = $merchant_data['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $update_link         = $config_data['customerPortalURL'];
        $inv_date            = '';
        $customer            = '';

        $customer = $invoce_data['Customer_FullName'];

        $company = $merchant_data['companyName'];
        if (isset($invoce_data['RefNumber'])) {
            $ref_number = $invoce_data['RefNumber'];
        }
        if (isset($invoce_data['TimeModified'])) {
            $tr_data = date('Y-m-d', strtotime($invoce_data['TimeModified']));
        }
        if (isset($invoce_data['BalanceRemaining'])) {
            $balance = ($invoce_data['BalanceRemaining']) ? $invoce_data['BalanceRemaining'] : '0.00';
        }
        if (isset($invoce_data['CardNo'])) {

            $cardno        = $invoce_data['CardNo'];
            $expiry        = $invoce_data['CardNo'];
            $friendly_name = $invoce_data['customerCardfriendlyName'];

        }

        $subject = replace_email_tags([
            'merchantID' => $merchantID,
            'customerID' => $customerID,
            'mailText' => $view_data['emailSubject'],
        ]);

        $subject = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $subject));
        if ($subject == "Welcome to { company_name }") {
            $subject = 'Welcome to ' . $company;
        }

        $message = replace_email_tags([
            'merchantID' => $merchantID,
            'customerID' => $customerID,
            'mailText' =>  $view_data['message'],
        ]);

        $message = stripslashes(str_ireplace('{{ merchant_name }}', $merchant_name, $message));
        $message = stripslashes(str_ireplace('{{ logo }}', "<img height='150' width='150'  src='$logo_url'>", $message));
        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.mask_number }}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.url_updatelink }}', $update_link, $message));

        $message = stripslashes(str_ireplace('{{ customer.company }}', $customer, $message));
        $message = stripslashes(str_ireplace('{{ transaction.currency_symbol }}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));

        $message = stripslashes(str_ireplace(' {{ transaction.transaction_method }}', $transaction_details, $message));
        $message = stripslashes(str_ireplace('{{ transaction.transaction_date}}', $tr_data, $message));

        $message = stripslashes(str_ireplace('{{ invoice.days_overdue }}', $overday, $message));
        $message = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_ireplace('{{ invoice.due_date|date("F j, Y") }}', $duedate, $message));

        $message = stripslashes(str_ireplace('{{ invoice.url_permalink }}', $in_link, $message));
        $message = stripslashes(str_ireplace('{{ merchant_email }}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{ merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{ current.date }}', $cur_date, $message));


        $fromEmail = $merchant_data['merchantEmail'];
        $toEmail   = $invoce_data['Contact'];
        $addCC     = $view_data['addCC'];
        $addBCC    = $view_data['addBCC'];
        $replyTo   = $view_data['replyTo'];
        $email_data = array('customerID' => $customerID,
            'merchantID'                     => $merchantID,
            'emailSubject'                   => $subject,
            'emailfrom'                      => $fromEmail,
            'emailto'                        => $toEmail,
            'emailcc'                        => $addCC,
            'emailbcc'                       => $addBCC,
            'emailreplyto'                   => $replyTo,
            'emailMessage'                   => $message,
            'emailsendAt'                    => date("Y-m-d H:i:s"),

        );

 
        $request_array = [
            "personalizations" => [
                [
                "to" => [
                    [
                    "email" => $toEmail
                    ]
                ],
                "subject" => $subject
                ]
            ],
            "from" => [
                "email" => $fromEmail,
                "name" => $merchant_name
            ],
            "reply_to" => [
                "email" => $fromEmail
            ],
            "content" => [
                [
                "type" => "text/html",
                "value" => $message
                ]
                ],
            
                    
        ];

        if(!empty($addCC)){
            $addCC = explode(';', $addCC);
            foreach ($addCC as $key => $value) {
                $addCC[$key]= trim($value);
            }
            $request_array['personalizations']['cc'] = $addCC;
        }

        if(!empty($addBCC)){
            $addBCC = explode(',', $addBCC);
            foreach ($addBCC as $key => $value) {
                $addBCC[$key]= trim($value);
            }
            $request_array['personalizations']['bcc'] = $addBCC;
        }
        
        // get api key for send grid
        $api_key = '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';
        $url = 'https://api.sendgrid.com/v3/mail/send';

        // set authorization header
        $headerArray = [
            'Authorization: Bearer '.$api_key,
            'Content-Type: application/json',
            'Accept: application/json'
        ];

        $ch = curl_init($url);
        curl_setopt ($ch, CURLOPT_POST, true);
        curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);

        // add authorization header
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        // parse header data from response
        $header_text = substr($response, 0, $header_size);
        // parse response body from curl response
        $body = substr($response, $header_size);

        $headers = createSendGridHeaders($header_text);
        

        // if mail sent success
        if($httpcode == '202' || $httpcode == '200'){
            $email_data['send_grid_email_id'] = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : '';
            $email_data['send_grid_email_status'] = 'Sent';
            $this->general_model->insert_row('tbl_template_data', $email_data);

        } else {
            $email_data['send_grid_email_status'] = 'Failed';
            $this->general_model->insert_row('tbl_template_data', $email_data);

        }

    }

    public function create_invoice()
    {

        $subsdata = $this->company_model->get_subcription_data();
   
        if (!empty($subsdata)) {

            foreach ($subsdata as $subs) {

                $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']));

                $inv_pre    = $in_data['prefix'];
                $inv_po     = $in_data['postfix'] + 1;
                $new_inv_no = $inv_pre . $inv_po;
                $duedate    = date("Y-m-d", strtotime($subs['nextGeneratingDate']));
                $randomNum  = substr(str_shuffle("0123456789"), 0, 5);

               
                if ($subs['generatedInvoice'] < $subs['totalInvoice'] || $subs['totalInvoice'] == 0) {

                    if ($subs['generatedInvoice'] < $subs['freeTrial']) {

                        $free_trial = '1';
                    } else {

                        $free_trial = '0';
                    }

                    $inv_data = array(
                        'Customer_FullName'      => $subs['FullName'],
                        'Customer_ListID'        => $subs['customerID'],
                        'TxnID'                  => $randomNum,
                        'RefNumber'              => $new_inv_no,
                        'TimeCreated'            => date('Y-m-d H:i:S'),
                        'TimeModified'           => date('Y-m-d H:i:S'),
                        'DueDate'                => $duedate,
                        'ShipAddress_Addr1'      => $subs['address1'],
                        'ShipAddress_Addr2'      => $subs['address2'],
                        'ShipAddress_City'       => $subs['city'],
                        'ShipAddress_Country'    => $subs['country'],
                        'ShipAddress_State'      => $subs['state'],
                        'ShipAddress_PostalCode' => $subs['zipcode'],
                        'IsPaid'                 => 'false',
                        'insertInvID'            => $randomNum,
                        'invoiceRefNumber'       => $inv_po,
                        'freeTrial'              => $free_trial,
                        'gatewayID'              => $subs['paymentGateway'],
                        'autoPayment'            => $subs['automaticPayment'],
                        'cardID'                 => $subs['cardID'],
                        'paymentMethod'          => '1',
                        'merchantID'             => $subs['merchantDataID'],
                    );

                    if (isset($subs['taxID']) && $subs['taxID']) {
                        $tax_data =    $this->general_model->get_row_data('tbl_taxes', array('taxID' => $subs['taxID']));
                        $taxRate = $tax_data['taxRate'];
                        $taxlsID = $tax_data['taxListID'];
                    } else {
                        $taxRate = 0;
                        $taxlsID = '';
                    }
                    $taxval = 0;

                    $inv_data['Billing_Addr1']     = $subs['baddress1'];
                    $inv_data['Billing_Addr2']     = $subs['baddress2'];
                    $inv_data['Billing_City']     = $subs['bcity'];
                    $inv_data['Billing_State']     = $subs['bstate'];
                    $inv_data['Billing_Country']     = $subs['bcountry'];
                    $inv_data['Billing_PostalCode']     = $subs['bzipcode'];
                    
                    if ($subs['proRate']) {
                        $proRate = $subs['proRate'];
                    } else {
                        $proRate = 0;
                    }

                    $in_num   = $subs['generatedInvoice'];
                    $paycycle = $subs['invoiceFrequency'];
                    $date     = $subs['firstDate'];

                    if ($subs['proRateBillingDay']) {
                        $proRateday = $subs['proRateBillingDay'];
                        $nextInvoiceDate = $subs['nextMonthInvoiceDate'];
                    } else {
                        $proRateday = 1;
                        $nextInvoiceDate = date('d', strtotime($date));
                    }

                    $total = $onetime = $recurring = 0;

                    $subscriptionListItem = $this->general_model->get_table_data('tbl_subscription_invoice_item', array('subscriptionID' => $subs['subscriptionID']));

					$item_val = [];
                    foreach ($subscriptionListItem as $key => $prod) {
                        if($subs['generatedInvoice'] > 1 && $prod['oneTimeCharge']){
							continue;
						}
                        $insert_row['itemListID'] = $prod['itemListID'];
                        $insert_row['itemQuantity'] = $prod['itemQuantity'];
                        $insert_row['itemRate'] = $prod['itemRate'];
                        $insert_row['itemFullName'] = $prod['itemFullName'];
                        $insert_row['itemDescription'] = $prod['itemDescription'];
                        $insert_row['itemTax'] = $prod['itemTax'];
                        $itemTotal = ($prod['itemRate'] * $prod['itemQuantity']); 
				        $total += $itemTotal;

                        if($prod['oneTimeCharge']){
                            $onetime += $itemTotal;
                        } else {
                            $recurring += $itemTotal;
                        }

                        $item_val[] = $insert_row;
                    }

                    $prorataCalculation = prorataCalculation([
                        'paycycle' => $paycycle,
                        'proRateday' => $proRateday,
                        'nextInvoiceDate' => $nextInvoiceDate,
                        'total_amount' => $total,
                        'onetime' => $onetime,
                        'recurring' => $recurring,
                        'date' => $date,
                        'in_num' => $in_num,
                        'proRate' => $proRate,
                    ]);
        
                    $next_date = $prorataCalculation['next_date'];
                    $total = $prorataCalculation['total_amount'];

                    if ($this->general_model->update_row_data('tbl_subscription_invoice_item',
                        array('subscriptionID' => $subs['subscriptionID']), array('invoiceDataID' => $randomNum))) {

                        $gen_inv = $subs['generatedInvoice'] + 1;
                        $this->general_model->update_row_data('tbl_subscriptions',
                            array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));
                        
                        $subscription_auto_invoices_data = [
                            'subscriptionID' => $subs['subscriptionID'],
                            'invoiceID' => $randomNum,
                            'app_type' => 2 //QBD
                        ];
                
                        $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

                        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));

                        $inv_data['qb_status'] = 0;
                        $inv_data['qb_action'] = "Add Invoice";
                        $inv_data['BalanceRemaining'] = $total;

                        $this->general_model->insert_row('tbl_custom_invoice', $inv_data);
                        foreach ($item_val as $k => $item) {
                            $itemline = mt_rand(5000500, 9000900);
                            $inv_item =	array('TxnID' => $randomNum, 'TxnLineID' => $itemline, 'Item_ListID' => $item['itemListID'], 'Item_FullName' => $item['itemFullName'], 'Descrip' => $item['itemDescription'], 'Quantity' => $item['itemQuantity'], 'Rate' => $item['itemRate'],'inv_itm_merchant_id'=> $subs['merchantDataID']);
                 
                            $this->general_model->insert_row('qb_test_invoice_lineitem', $inv_item);
                        }

                        $user = $subs['qbwc_username'];
                        $QBD_Sync = new QBD_Sync($subs['merchantDataID']);
                        $QBD_Sync->invoiceSync([
                            'invID' => $randomNum
                        ]);

                        $comp_data   = $this->general_model->get_select_data('tbl_company',array('id','qbwc_username'),array('merchantID'=>$subs['merchantDataID']));
                        $invoice_data['TxnID'] 			= $randomNum;
                        $invoice_data['TimeCreated'] 	= date('Y-m-d H:i:s');
                        $invoice_data['TimeModified'] 	= date('Y-m-d H:i:s');
                        $invoice_data['RefNumber'] 		= $inv_data['RefNumber'];
                        $invoice_data['Customer_ListID']        = $inv_data['Customer_ListID'];
                        $invoice_data['Customer_FullName'] 		= $inv_data['Customer_FullName'];
                        $invoice_data['ShipAddress_Addr1'] 		= $inv_data['ShipAddress_Addr1'];
                        $invoice_data['ShipAddress_Addr2'] 		= $inv_data['ShipAddress_Addr2'];
                        $invoice_data['ShipAddress_City']      	= $inv_data['ShipAddress_City'];
                        $invoice_data['ShipAddress_State']    	= $inv_data['ShipAddress_State'];
                        $invoice_data['ShipAddress_Country']    = $inv_data['ShipAddress_Country'];
                        $invoice_data['ShipAddress_PostalCode'] = $inv_data['ShipAddress_PostalCode'];
                        $invoice_data['Billing_Addr1'] 		= $inv_data['Billing_Addr1'];
                        $invoice_data['Billing_Addr2'] 		= $inv_data['Billing_Addr2'];
                        $invoice_data['Billing_City']      	= $inv_data['Billing_City'];
                        $invoice_data['Billing_State']    	= $inv_data['Billing_State'];
                        $invoice_data['Billing_Country']    = $inv_data['Billing_Country'];
                        $invoice_data['Billing_PostalCode'] = $inv_data['Billing_PostalCode'];
                        $invoice_data['BalanceRemaining'] 		= $inv_data['BalanceRemaining'];
                        $invoice_data['DueDate'] 				= $inv_data['DueDate'];
                        $invoice_data['IsPaid'] 				= $inv_data['IsPaid'];
                        $invoice_data['userStatus'] 		    	= 'Active';
                        $invoice_data['insertInvID'] 			= $inv_data['insertInvID'];
                        $invoice_data['invoiceRefNumber'] 		= $inv_data['invoiceRefNumber'];
                        $invoice_data['TaxRate'] 				= $taxRate;
                        $invoice_data['TotalTax'] 				= $taxval;
                        $invoice_data['TaxListID'] 				= $taxlsID;
                        $invoice_data['qb_inv_companyID']  = $comp_data['id'];
                        $invoice_data['qb_inv_merchantID'] = $subs['merchantDataID'];
                        $this->general_model->insert_row('qb_test_invoice', $invoice_data);

                        $insert_data = array('invoiceID' => $inv_data['TxnID'], 'cardID' => $inv_data['cardID'], 'gatewayID' => $inv_data['gatewayID'], 'merchantID' => $inv_data['merchantID'], 'updatedAt' => date('Y-m-d H:i:s'), 'CreatedAt' => date('Y-m-d H:i:s'));
                        $this->db->insert('tbl_scheduled_invoice_payment', $insert_data);

                        if ($subs['emailRecurring'] == '1') {

                            $inv_no = $inv_data['RefNumber'];
                            $invoiceID = $inv_data['insertInvID'];
                            $invoice  = $this->company_model->get_invoice_details_data($invoiceID);
                            $this->general_model->generate_invoice_pdf($invoice['customer_data'], $invoice['invoice_data'], $invoice['item_data'], 'F');


                            $condition_mail         = array('templateType' => '3', 'merchantID' => $subs['merchantDataID']);

                            $toEmail = $subs['Contact'];
                            $company = $subs['companyName'];
                            $customer = $subs['FullName'];
                            $invoice_due_date = $inv_data['DueDate'];
                            $invoicebalance = $inv_data['BalanceRemaining'];
                            $tr_date   = date('Y-m-d H:i:s');
                            $this->general_model->send_mail_invoice_data($condition_mail, $company, $customer, $toEmail, $subs['customerID'], $invoice_due_date, $invoicebalance, $tr_date, $invoiceID, $inv_no);
                        }

                    }
                }
            }

        }

    }

    public function get_card_expiry_data($customerID, $companyID)
    {

        $card = array();
        $this->load->library('encrypt');

        $sql       = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where `companyID` = '$companyID' and customerListID='$customerID' limit 1  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {
            $card['CustomerCard']             = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function get_expiry_card_data($type)
    {
        $new_card = array();
        $card     = array();
        $this->load->library('encrypt');

        if ($type == '1') {
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day ) ";
        }

        if ($type == '0') {
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  ";

        }
        $query1     = $this->db1->query($sql);
        $card_datas = $query1->result_array();
        if (!empty($card_datas)) {
            foreach ($card_datas as $k => $card_data) {
                $card['CardNo']                   = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
                $card['cardMonth']                = $card_data['cardMonth'];
                $card['cardYear']                 = $card_data['cardYear'];
                $card['expiry']                   = $card_data['expired_date'];
                $card['CardID']                   = $card_data['CardID'];
                $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
                $card['customerListID']           = $card_data['customerListID'];
                $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
                $card['merchantID'] = $card_data['merchantID'];

                $new_card[$k] = $card;
            }
        }

        return $new_card;

    }

    public function pay_invzzzzoice()
    {

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        if ($invoiceID != '') {

            $in_data = $this->quickbooks->get_invoice_data();

            if (!empty($in_data)) {

                if ($in_data['BalanceRemaining'] > 0) {

                    $nmiuser = $in_data['nmi_username'];
                    $nmipass = $in_data['nmi_password'];

                    $nmi_data          = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                    $customer_vault_id = $in_data['vaultID'];
                    $amount            = $in_data['BalanceRemaining'];
                    $vault             = new nmiCustomerVault($nmi_data);
                    $vault->setCustomerVaultId($customer_vault_id);
                    $vault->charge($amount);
                    $result = $vault->execute();

                    if ($result['response_code'] == "100") {
                        $txnID     = $in_data['TxnID'];
                        $ispaid    = 'true';
                        $data      = array('IsPaid' => $ispaid);
                        $condition = array('TxnID' => $in_data['TxnID'], 'EditSequence' => $in_data['EditSequence']);
                        $this->general_model->update_row_data('qb_test_invoice', $condition, $data);
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID);
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');
                    } else {

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: "' . $result['responsetext'] . '"</strong>.</div>');
                    }

                    $transaction['transactionID']     = $result['transactionid'];
                    $transaction['transactionStatus'] = $result['responsetext'];
                    $transaction['transactionCode']   = $result['response_code'];
                    $transaction['transactionType']   = ($result['type']) ? $result['type'] : 'auto-nmi';
                    $transaction['transactionDate']   = date('Y-m-d h:i:s');
                    $transaction['invoiceTxnID']      = $in_data['TxnID'];
                    $transaction['customerListID']    = $in_data['Customer_ListID'];
                    $transaction['transactionAmount'] = $in_data['BalanceRemaining'];
                    $id                               = $this->general_model->insert_row('tbl_customer_tansaction', $transaction);

                    redirect('home/index', 'refresh');

                }

            }

        }

    }

    public function schedule_invoice()
    {
        $rec_data = $this->general_model->get_table_data('tbl_recurring_payment', '');

        if (!empty($rec_data)) {
            foreach ($rec_data as $rec_d) {

                $invoices = $this->company_model->get_ivoice_data_sch($rec_d);

                if (!empty($invoices)) {
                    foreach ($invoices as $invoice) {
                        $num      = $this->general_model->get_num_rows('tbl_scheduled_invoice_payment', array('merchantID' => $rec_d['merchantID'], 'invoiceID' => $invoice['inv']));
                        $sch_data = array('gatewayID' => $rec_d['gateway'], 'customerID'                => $rec_d['customerID'],
                            'cardID'                      => $rec_d['cardID'], 'autoPay'                    => 1, 'paymentMethod' => 1, 'ScheduleDate' => date('Y-m-d H:i:s'),
                            'scheduleAmount'              => $invoice['BalanceRemaining'], 'scheduleAmount' => $invoice['BalanceRemaining'],
                        );

                        if ($num > 0) {
                            $sch_data['updatedAt'] = date('Y-m-d H:i:s');
                            $this->general_model->update_row_data('tbl_scheduled_invoice_payment',
                                array('merchantID' => $rec_d['merchantID'], 'invoiceID' => $invoice['inv']), $sch_data);

                        } else {
                            $sch_data['updatedAt'] = date('Y-m-d H:i:s');
                            $sch_data['createdAt'] = date('Y-m-d H:i:s');
                            $sch_data['invoiceID'] = $invoice['inv'];
                            $this->db->insert('tbl_scheduled_invoice_payment', $sch_data);
                        }

                    }

                }

            }
        }
    }

    public function process_recurring_invoice()
    {


        $invoice_data = $this->company_model->getAutoRecurringInvoices();
        
        if (!empty($invoice_data)) {
            /*Log Request Payload*/
            $jsonData = json_encode($invoice_data);
            logData($jsonData, 'Recurring Invoice','qbd_recurring');
            foreach ($invoice_data as $in_data) {
                $invoiceID  = $in_data['TxnID'];

                if ($in_data['BalanceRemaining'] != '0.00') {
                    if ($in_data['DueDate'] != '') {

                        $card_data  = $this->card_model->get_single_card_data($in_data['cardID']);

                        if(empty($card_data)){
                            continue;
                        }

                        $user_id    = $in_data['qb_inv_merchantID'];

                        $invWhere  = array( 'BalanceRemaining >' => 0, 'TxnID' => $in_data['TxnID'], 'qb_inv_merchantID' => $user_id);
						$invData = $this->general_model->get_row_data('qb_test_invoice', $invWhere);
						if(!$invData || empty($invData)){
							continue;
						}

                        $res = [];
                        $card_no    = $card_data['CardNo'];
                        $cvv        = $card_data['CardCVV'];
                        $expmonth   = $card_data['cardMonth'];
                        $exyear     = $card_data['cardYear'];
                        $cardType   = $card_data['CardType'];
                        
                        $address1   = $card_data['Billing_Addr1'];
                        $address2   = $card_data['Billing_Addr2'];
                        $city       = $card_data['Billing_City'];
                        $zipcode = $zip    = $card_data['Billing_Zipcode'];
                        $state      = $card_data['Billing_State'];
                        $country    = $card_data['Billing_Country'];



                        $phone      = $in_data['Phone'];
                        $customerID = $in_data['Customer_ListID'];

                        $firstName = $in_data['FirstName'];
                        $lastName  = $in_data['LastName'];

                        
                        $name =  $in_data['FirstName'] . " " .  $in_data['LastName'];
                        $fullName =  $in_data['FullName'];

                        $email = $in_data['userEmail'];

                        $companyName =  $in_data['companyName'];
                        
                        

                        $amount = $in_data['BalanceRemaining'];

                        $pay_sts = "";
                        $type = 'sale';
                        
                        if ($in_data['gatewayType'] == '1' || $in_data['gatewayType'] == '9') {

                            $nmiuser         = $in_data['gatewayUsername'];
                            $nmipass         = $in_data['gatewayPassword'];
                            $nmi_data        = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                            $Customer_ListID = $customerID;

                            $transaction1 = new nmiDirectPost($nmi_data);
                            $transaction1->setCcNumber($card_no);
                          
                            if (strlen($exyear) > 2) {
                                $minLength = 4 - strlen($exyear);
                                $exyear   = substr($exyear, $minLength);
                            }

                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }

                            $expry = $expmonth . $exyear;
                            $transaction1->setCcExp($expry);
                            if($cvv != ''){
                                $transaction1->setCvv($cvv);
                            }
                            
                            $transaction1->setAmount($amount);
                            
                            $transaction1->sale();
                            $result = $transaction1->execute();
                            $type   = 'sale';
                            if ($result['response_code'] == "100") {
                                $pay_sts = "SUCCESS";
                            }
                            $res = $result;

                        } else if ($in_data['gatewayType'] == '2') {

                            $this->load->config('auth_pay');

                            $apiloginID     = $in_data['gatewayUsername'];
                            $transactionKey = $in_data['gatewayPassword'];

                            $transaction1 = new AuthorizeNetAIM($apiloginID, $transactionKey);
                            $transaction1->setSandbox($this->config->item('auth_test_mode'));
                            $card_no      = $card_no;
                            $expmonth     = $expmonth;
                            $exyear       = $exyear;
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;

                            $res = $transaction1->authorizeAndCapture($amount, $card_no, $expry);
                            $logResult = json_encode($res);
                            /* Response log data */
                            logData($logResult, 'Recurring Auth Response','qbd_auth_response');
                            if ($res->response_code == "1" && $res->transaction_id != 0 && $res->transaction_id != '') {
                                $pay_sts = "SUCCESS";
                            }

                        } else if ($in_data['gatewayType'] == '3') {
                            $this->load->config('paytrace');

                            $payusername = $in_data['gatewayUsername'];
                            $paypassword = $in_data['gatewayPassword'];
                            $grant_type  = "password";

                            $payAPI = new PayTraceAPINEW();

                            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

                            //call a function of Utilities.php to verify if there is any error with OAuth token.
                            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                            if (!$oauth_moveforward) {
                                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

                                //set Authentication value based on the successful oAuth response.
                                //Add a space between 'Bearer' and access _token
                                $oauth_token = sprintf("Bearer %s", $json['access_token']);

                                
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }

                                $request_data = array(
                                    "amount"          => $amount,
                                    "credit_card"     => array(
                                        "number"           => $card_no,
                                        "expiration_month" => $expmonth,
                                        "expiration_year"  => $exyear),
                                    "csc"             => $cvv,
                                    "invoice_id"      => rand('500000', '200000'),
                                    "billing_address" => array(
                                        "name"           => $name,
                                        "street_address" => $address1,
                                        "city"           => $city,
                                        "state"          => $state,
                                        "zip"            => $zipcode,
                                    ),
                                );
                                $type         = 'auth_capture';
                                $request_data = json_encode($request_data);
                                $result       = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);
                                $res     = $payAPI->jsonDecode($result['temp_json_response']);

                                if ($result['http_status_code'] == '200') {
                                    $res['http_status_code']=200;
                                    $pay_sts = "SUCCESS";
                                }
                            }

                        } else if ($in_data['gatewayType'] == '4') {

                            $username  = $in_data['gatewayUsername'];
                            $password  = $in_data['gatewayPassword'];
                            $signature = $in_data['gatewaySignature'];

                            $config = array(
                                'Sandbox'      => $this->config->item('Sandbox'), 
                                'APIUsername'  => $username,
                                'APIPassword'  => $password, 
                                'APISignature' => $signature, 
                                'APISubject'   => '',
                                'APIVersion'   => $this->config->item('APIVersion'),
                            );
                            
                            if ($config['Sandbox']) {
                                error_reporting(E_ALL);
                                ini_set('display_errors', '1');
                            }

                            $this->load->library('paypal/Paypal_pro', $config);

                            $creditCardType = 'Visa';
                            
                            $expDateMonth     = $expmonth;
                            
                            $padDateMonth = str_pad($expmonth, 2, '0', STR_PAD_LEFT);
                            
                            $currencyID   = "USD";
                           
                            $DPFields = array(
                                'paymentaction'    => 'Sale', 
                                'ipaddress'        => '', 
                                'returnfmfdetails' => '0',
                            );

                            $CCDetails = array(
                                'creditcardtype' => $creditCardType, 
                                'acct'           => $card_no, 
                                'expdate'        => $padDateMonth . $exyear, 
                                'cvv2'           => $cvv, 
                                'startdate'      => '', 
                                'issuenumber'    => '', 
                            );

                            $PayerInfo = array(
                                'email'       => $email, 
                                'payerid'     => '', 
                                'payerstatus' => 'verified', 
                                'business'    => '', 
                            );

                            $PayerName = array(
                                'salutation' => $companyName,
                                'firstname'  => $firstName, 
                                'middlename' => '', 
                                'lastname'   => $lastName, 
                                'suffix'     => '',
                            );

                            $BillingAddress = array(
                                'street'      => $address1,
                                'street2'     => $address2,
                                'city'        => $city, 
                                'state'       => $state,
                                'countrycode' => $country, 
                                'zip'         => $zip, 
                                'phonenum'    => $phone,
                            );

                            $PaymentDetails = array(
                                'amt'          => $amount, 
                                'currencycode' => $currencyID, 
                                'itemamt'      => '',
                                'shippingamt'  => '', 
                                'insuranceamt' => '', 
                                'shipdiscamt'  => '', 
                                'handlingamt'  => '', 
                                'taxamt'       => '', 
                                'desc'         => '', 
                                'custom'       => '', 
                                'invnum'       => '', 
                                'buttonsource' => '', 
                                'notifyurl'    => '', 
                                'recurring'    => '', 
                            );

                            $PayPalRequestData = array(
                                'DPFields'       => $DPFields,
                                'CCDetails'      => $CCDetails,
                                'PayerInfo'      => $PayerInfo,
                                'PayerName'      => $PayerName,
                                'BillingAddress' => $BillingAddress,

                                'PaymentDetails' => $PaymentDetails,

                            );
                            $type         = 'auth_capture';
                            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

                            if (!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) {
                                $pay_sts = "SUCCESS";
                            }
                            
                        } else if ($in_data['gatewayType'] == '5') {
                            include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

                            $stripeKey = $in_data['gatewayUsername'];
                            $strPass   = $in_data['gatewayPassword'];
                            
                            $real_amt = (int) ($amount * 100);
                            $plugin = new ChargezoomStripe();
                            $plugin->setApiKey($stripeKey);
                            $res = \Stripe\Token::create([
                                'card' => [
                                    'number'    => $card_no,
                                    'exp_month' => $expmonth,
                                    'exp_year'  => $exyear,
                                    'cvc'       => $cvv,
                                    'name'      => $fullName,
                                ],
                            ]);

                            $tcharge = json_encode($res);

                            $rest = json_decode($tcharge);

                            if ($rest->id) {
                                $charge = \Stripe\Charge::create(array(
                                    "amount"      => $real_amt,
                                    "currency"    => "usd",
                                    "source"      => $rest->id,
                                    "description" => "Charge Using Stripe Gateway",
                                ));
                                $charge = json_encode($charge);

                                $result  = json_decode($charge);

                                $type    = 'stripe_sale';
                                $crtxnID = '';
                                if ($result->paid == '1' && $result->failure_code == "") {
                                    $pay_sts = "SUCCESS";
                                }
                                $res = $result;
                            }

                        } else if ($in_data['gatewayType'] == '6') {

                            require_once APPPATH . "third_party/usaepay/usaepay.php";
                            $this->load->config('usaePay');

                            $crtxnID          = '';
                            $invNo            = mt_rand(1000000, 2000000);
                            $transaction      = new umTransaction;
                            $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                            
                            $transaction->key = $in_data['gatewayUsername'];
                            $transaction->pin = $in_data['gatewayPassword'];

                            $transaction->usesandbox  = $this->config->item('Sandbox');
                            $transaction->invoice     = $invNo; // invoice number.  must be unique.
                            $transaction->description = "Chargezoom Invoice Payment"; // description of charge

                            $transaction->testmode = $this->config->item('TESTMODE'); // Change this to 0 for the transaction to process
                            
                            $transaction->command  = "sale";

                            
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                            
                            if ($cvv != "") {
                                $transaction->cvv2 = $cvv;
                            }
                            
                            $transaction->exp = $expry;
                            $transaction->card = $card_no;

                            $transaction->billstreet  = $address1;
                            $transaction->billstreet2 = $address2;
                            $transaction->billcountry = $country;
                            $transaction->billcity    = $city;
                            $transaction->billstate   = $state;
                            $transaction->billzip     = $zipcode;

                            $transaction->shipstreet  = $address1;
                            $transaction->shipstreet2 = $address2;
                            $transaction->shipcountry = $country;
                            $transaction->shipcity    = $city;
                            $transaction->shipstate   = $state;
                            $transaction->shipzip     = $zipcode;
                            $transaction->amount      = $amount;

                            $transaction->Process();
                            $type = 'sale';
                            if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {

                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;

                                $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                $pay_sts = "SUCCESS";
                            } else {
                                $msg  = $transaction->result;
                                $trID = $transaction->refnum;
                                $res  = array('transactionCode' => 300, 'status' => $msg, 'transactionId' => $trID);
                            }

                        } else if ($in_data['gatewayType'] == '7') {
                            $this->load->config('globalpayments');

                            $payusername   = $in_data['gatewayUsername'];
                            $secretApiKey   = $in_data['gatewayPassword'];

                            $crtxnID = '';

                            $config = new PorticoConfig();
                            
                            $config->secretApiKey = $secretApiKey;
                            $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                            
                            ServicesContainer::configureService($config);
                            
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;
                            if($cvv != ''){
                                $card->cvn = $cvv;
                            }
                            $card->cardType = $cardType;

                            $address                 = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city           = $city;
                            $address->state          = $state;
                            $address->postalCode     = $zipcode;
                            $address->country        = $country;

                            $invNo = mt_rand(1000000, 2000000);
                            try
                            {
                                $response = $card->charge($amount)
                                    ->withCurrency('USD')
                                    ->withAddress($address)
                                    ->withInvoiceNumber($invNo)
                                    ->withAllowDuplicates(true)
                                    ->execute();

                                if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {

                                    $msg     = $response->responseMessage;
                                    $trID    = $response->transactionId;
                                    $st      = '0';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success ";
                                    $res     = array('transactionCode' => '200', 'status' => $msg, 'transactionId' => $trID);
                                    $pay_sts = "SUCCESS";

                                } else {
                                    $res = array('transactionCode' => $response->responseCode, 'status' => $response->responseMessage, 'transactionId' => $response->transactionId);
                                }

                            } catch (BuilderException $e) {
                                $error = 'Build Exception Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ConfigurationException $e) {
                                $error = 'ConfigurationException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (GatewayException $e) {
                                $error = 'GatewayException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (UnsupportedTransactionException $e) {
                                $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            } catch (ApiException $e) {
                                $error = ' ApiException Failure: ' . $e->getMessage();
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
                            }

                        } else if ($in_data['gatewayType'] == '8') {
                            $this->load->config('cyber_pay');

                            $option =array();
                            $option['merchantID']     = trim($in_data['gatewayUsername']);
                            $option['apiKey']         = trim($in_data['gatewayPassword']);
                            $option['secretKey']      = trim($in_data['gatewaySignature']);
                            
                            if($this->config->item('Sandbox')){
                                $env   = $this->config->item('SandboxENV');
                            } else{
                                $env   = $this->config->item('ProductionENV');
                            }
                            $option['runENV']      = $env;
                            $Customer_ListID = $customerID;

                            $commonElement = new CyberSource\ExternalConfiguration($option);
                            $config = $commonElement->ConnectionHost();
                            
                            $merchantConfig = $commonElement->merchantConfigObject();
                            $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                            $api_instance = new CyberSource\Api\PaymentsApi($apiclient);

                            $cliRefInfoArr = [
                                "code" => $companyName
                            ];
                            $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

                            if($flag == "true") {
                                $processingInformationArr = [
                                    "capture" => true, "commerceIndicator" => "internet"
                                ];
                            } else {
                                $processingInformationArr = [
                                    "commerceIndicator" => "internet"
                                ];
                            }
                            $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

                            $amountDetailsArr = [
                                "totalAmount" => $amount,
                                "currency" => CURRENCY
                            ];
                            $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                            $billtoArr = [
                                "firstName" => $firstName,
                                "lastName" => $lastName,
                                "address1" => $address1,
                                "postalCode" => $zipcode,
                                "locality" => $city,
                                "administrativeArea" => $state,
                                "country" => $country,
                                "phoneNumber" => $phone,
                                "company" => $companyName,
                                "email" => $email,
                            ];
                            $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

                            $orderInfoArr = [
                                "amountDetails" => $amountDetInfo, 
                                "billTo" => $billto
                            ];
                            $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

                            $paymentCardInfo = [
                                "expirationYear" => $exyear,
                                "number" => $card_no,
                                "securityCode" => $cvv,
                                "expirationMonth" => $expmonth
                            ];
                            $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                            
                            $paymentInfoArr = [
                                "card" => $card
                            ];
                            $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                        
                            $paymentRequestArr = [
                                "clientReferenceInformation" => $client_reference_information, 
                                "orderInformation" => $order_information, 
                                "paymentInformation" => $payment_information, 
                                "processingInformation" => $processingInformation
                            ];
                            $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                            
                            $api_response = list($response, $statusCode, $httpHeader) = null;

                            try
                            {
                                //Calling the Api
                                $api_response = $api_instance->createPayment($paymentRequest);
                                
                                if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                                    $trID =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                   
                                    $pay_sts = "SUCCESS";
                                    
                                    $code =   '200';
                                    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                } else {
                                    $trID  =   $api_response[0]['id'];
                                    $msg  =   $api_response[0]['status'];
                                    $code =   $api_response[1];
                                    $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
                                    
                                    $error = $api_response[0]['status'];
                                }
                            }
                            catch(Cybersource\ApiException $e)
                            {
                                $error = $e->getMessage();
                                $res =array('transactionCode'=>500, 'status'=>$error, 'transactionId'=> '' ); 
                            }

                        } else if ($in_data['gatewayType'] == '10') {

                            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                                $expmonth = substr($expmonth, 1);
                            }

                            $apiUsername     = $in_data['gatewayUsername'];
                            $apiKey = $in_data['gatewayPassword'];

                            $payload = array(
                                "amount"          => ($amount * 100),
                                "card"     => array(
                                    "name" => $name,
                                    "number" => $card_no,
                                    "exp_month" => $expmonth,
                                    "exp_year"  => $exyear,
                                    "cvv"             => $cvv,
                                ),
                                "billing_address" => array(
                                    "line1"        => $address1,
                                    "line2" => $address2,
                                    "city"         => $city,
                                    "state"        => $state,
                                    "postal_code"  => $zip,
                                ),
                            );
                            if($cvv == ''){
                                unset($payload['card']['cvv']);
                            }
                            $sdk = new iTTransaction();
                            $res = $sdk->postCardTransaction($apiUsername, $apiKey, $payload);
                            if ($res['status_code'] == "200" || $res['status_code'] == "201") {
                                $res['status_code'] = "200";
                                $pay_sts = "SUCCESS";
                            }
                        }else if ($in_data['gatewayType'] == '11' || $in_data['gatewayType'] == '13') {
                            $apiUsername     = $in_data['gatewayUsername'];
                            $calamount = $amount * 100;
                            $payload = [
                                "amount" => round($calamount,2),
                                "type"                => "sale",
                                "address" => [
                                    "line1" => $address1,
                                    "line2" => $address2,
                                    "city" => $city,
                                    "state" => $state,
                                    "postal_code" => $zipcode,
                                    
                                ],
                                "name" => $name,
                            ];
                            
                            $exyear1  = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear1;

                            $payload["payment_method"] = [
                                "card" => array(
                                    "entry_type"      => "keyed",
                                    "number"          => $card_no,
                                    "expiration_date" => $expry,
                                    "cvc"             => $cvv,
                                ),
                            ];
                            
                            $gatewayTransaction              = new Fluidpay();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->apiKey      = $apiUsername;
                            $result = $gatewayTransaction->processTransaction($payload);
                            $res = $result;
                            if ($result['status'] == 'success') {
                                $responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            }
                        } else if ($in_data['gatewayType'] == '12') {
                            

                            $exyear1       = substr($exyear, 2);
                            if(empty($exyear1)){
                                $exyear1  = $exyear;
                            }
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth.'/'.$exyear1;

                            $deviceID = $in_data['gatewayMerchantID'].'01';
                            $gatewayTransaction              = new TSYS();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->deviceID = $deviceID;
                            $result = $gatewayTransaction->generateToken($in_data['gatewayUsername'],$in_data['gatewayPassword'],$in_data['gatewayMerchantID']);
                            $generateToken = '';
                            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                                
                            }

                            $gatewayTransaction->transactionKey = $generateToken;
                            $responseType = 'SaleResponse';
                            $address1 = ($address1 != '')?$address1:'None';
                            $address2 = ($address2 != '')?$address2:'None';
                            $city     =  ($city != '')?$city:'None';
                            $zipcode = ($zipcode != '')?$zipcode:'None';
                            $state    = ($state != '')?$state:'AZ';
                            $country  = ($country != '')?$country:'USA';
                            $phone = ($phone != '')?$phone:'None';

                            $transaction['Sale'] = array(
                                "deviceID"                          => $deviceID,
                                "transactionKey"                    => $generateToken,
                                "cardDataSource"                    => "MANUAL",  
                                "transactionAmount"                 => $amount * 100,
                                "currencyCode"                      => "USD",
                                "cardNumber"                        => $card_no,
                                "expirationDate"                    => $expry,
                                "cvv2"                              => $cvv,
                                "addressLine1"                      => $address1,
                                "zip"                               => $zipcode,
                                "orderNumber"                       => $invoiceID,
                                "notifyEmailID"                     => (isset($email))?$email:'chargezoom@chargezoom.com',
                                "firstName"                         => (isset($firstName))?$firstName:'None',
                                "lastName"                          => (isset($lastname))?$lastname:'None',
                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                "terminalOutputCapability"          => "DISPLAY_ONLY",
                                "maxPinLength"                      => "UNKNOWN",
                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                "cardPresentDetail"                 => "CARD_PRESENT",
                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                "cardholderAuthenticationEntity"    => "OTHER",
                                "cardDataOutputCapability"          => "NONE",

                                "customerDetails"   => array( 
                                            "contactDetails" => array(
                                         
                                                "addressLine1"=> $address1,
                                                 "addressLine2"  => $address2,
                                                "city"=>$city,
                                                "zip"=>$zipcode
                                            ),
                                            "shippingDetails" => array( 
                                                "firstName"=>(isset($firstName))?$firstName:'None',
                                                "lastName"=>(isset($lastname))?$lastname:'None',
                                                "addressLine1"=>$address1,
                                                 "addressLine2" => $address2,
                                                "city"=>$city,
                                                "zip"=>$zipcode,
                                                "emailID"=>(isset($email))?$email:'chargezoom@chargezoom.com'
                                             )
                                        )
                            );
                            if($cvv == ''){
                                unset($transaction['Sale']['cvv2']);
                            }
                            if($generateToken != ''){ 
                                $result = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
                            
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                                $responseId = $result[$responseType]['transactionID'];
                                $pay_sts = "SUCCESS";

                            }
                            $result['responseType'] = $responseType;
                            $res = $result;

                        } else if($in_data['gatewayType'] == '14') {

                            $apiUsername     = $in_data['gatewayUsername'];
							$apiPassword     = $in_data['gatewayPassword'];
							$apiMerchantId   = $in_data['gatewayMerchantID'];
                            $cardpointeSiteName   = $in_data['gatewaySignature'];
                            $address1 = ($address1 != '')?$address1:'';
                            $city     =  ($city != '')?$city:'';
                            $zipcode = ($zipcode != '')?$zipcode:'';
                            $state    = ($state != '')?$state:'AZ';
                            $country  = ($country != '')?$country:'USA';
                            $firstName = isset($firstName)?$firstName:'';
                            $lastname = isset($lastname)?$lastname:'';
                            $name = $firstName.' '.$lastname;

                            $transaction1 = new CardPointe();
                            $card_no      = $card_data['CardNo'];
                            $expmonth     = $card_data['cardMonth'];
                            $exyear       = $card_data['cardYear'];
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                            
                            $res = $transaction1->authorize_capture($cardpointeSiteName, $apiMerchantId, $apiUsername, $apiPassword, $card_no, $expry, $amount,'',$name,$address1,$city,$state,$zipcode);
                            if ($res['respcode'] == '00') {
                                $pay_sts = "SUCCESS";
                            }
                        } else if ($in_data['gatewayType'] == '15') {

                            $apiUsername     = $in_data['gatewayUsername'];
                            
                            $exyear  = $exyear;
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }

                            $this->load->config('payarc');
                            $this->load->library('PayarcGateway');
                            
							$this->payarcgateway->setApiMode($this->config->item('environment'));
                            $this->payarcgateway->setSecretKey($apiUsername);

                            // Create Credit Card Token
                            $address_info = [
                                                'address_line1' => $address1,
                                                'address_line2' => $address2,
                                                'state' => $state,
                                                'country' => ''
                                            ];


                            $token_response = $this->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);

                            $token_data = json_decode($token_response['response_body'], 1);

                            if(isset($token_data['status']) && $token_data['status'] == 'error'){
                                $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->gatewayEnvironment,'accessKey'=>$apiUsername, 'card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
                                // Error while creating the credit card token
                                $pay_sts = "ERROR";
                                $err_msg      = $token_data['message'];

                            } else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {

                                // If token created
                                $token_id = $token_data['data']['id'];
                        
                                $charge_payload = [];
                        
                                $charge_payload['token_id'] = $token_id;
                                
                                if(isset($email) && $email){
                                    $charge_payload['email'] = $email; // Customer's email address.
                                }
                                if(isset($phone) && $phone){
                                    $charge_payload['phone_number'] = $phone; // Customer's contact phone number..
                                }
                        
                                $charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD
                        
                                $charge_payload['currency'] = 'usd'; 
                        
                                $charge_payload['capture'] = '1';
                        
                                $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.
                        
                                if(isset($zipcode) && $zipcode) {
                                    $charge_payload['ship_to_zip'] = $zipcode; 
                                };

                                $charge_payload['statement_description'] = '';
                        
                                $charge_response = $this->payarcgateway->createCharge($charge_payload);
                        
                                $result = json_decode($charge_response['response_body'], 1);
                                
                                // Handle Card Decline Error
                                if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
                                {
                                    $result['message'] = $result['data']['failure_message'];
                                }
                                
                                $res = $result;

                                if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
                                    $responseId = $result['data']['id'];
                                    $pay_sts = "SUCCESS";
                                } else {
                                    $pay_sts = "ERROR";
                                    $err_msg = $result['message'];
                                }
                            }
                        } else if ($in_data['gatewayType'] == '17') {
                            // Maverick Payment Begins
                            
                            // Maverick Payment Gateway
                            $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                            $this->maverickgateway->setTerminalId($in_data['gatewayPassword']);
                            $this->maverickgateway->setAccessToken($in_data['gatewayUsername']);

							

                            $exyear       = substr($exyear, -2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth.'/'.$exyear;

                            // Sale Payload
                            $request_payload = [
                                'level' => 1,
                                'threeds' => [
                                    'id' => null,
                                ],
                                'amount' => $amount,
                                'card' => [
                                    'number' => $card_no,
                                    'cvv'    => $cvv,
                                    'exp'    => $expry,
                                    'save'   => 'No',
                                    'address' => [
                                        'street' => $address1,
                                        'city' => $city,
                                        'state' => $state,
                                        'country' => $country,
                                        'zip' => $zipcode,
                                    ]
                                ],
                                'contact' => [
                                    'name'   => $fullName,
                                    'email'  => $email,
                                    'phone' => $phone,
                                ]
                            ];
            
                            // Process Sale
                            $r = $this->maverickgateway->processSale($request_payload);
							

                            $result = [];

                            $rbody = json_decode($r['response_body'], true);

                            $result['response_code'] = $r['response_code'];

                            $result['data'] = $rbody;
                            
                            // Response                
                            if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                                if(isset($rbody['id']) && $rbody['id']){
                                    $result['status'] = 'success';
                                    $result['msg'] = $result['message'] = 'Payment success.';
                                } else {
                                    $result['status'] = 'failed';
                                    $result['msg'] = $result['message'] = 'Payment failed.';    
                                }

                            } else {
                                $result['status'] = 'failed';
                                $result['msg'] = $result['message'] = $rbody['message'];
                            }


                            $res = $result;
							if ($result['status'] == 'success') {
								$responseId = $result['data']['id'];
                                $pay_sts = "SUCCESS";
                            } else {
                                $pay_sts = "ERROR";
                            }
                            // Maverick Payment Ends
                        } else if ($in_data['gatewayType'] == '16') {
                            
                            $this->load->config('EPX');
                            $CUST_NBR = $in_data['gatewayUsername'];
                            $MERCH_NBR = $in_data['gatewayPassword'];
                            $DBA_NBR = $in_data['gatewaySignature'];
                            $TERMINAL_NBR = $in_data['extra_field_1'];
                            $orderId = time();
                            $amount = number_format($amount,2,'.','');
                            $address1 = ($address1 != '')?$address1:'None';
                            $address2 = ($address2 != '')?$address2:'None';
                            $city     =  ($city != '')?$city:'None';
                            $zipcode = ($zipcode != '')?$zipcode:'None';
                            $state    = ($state != '')?$state:'AZ';
                            $country  = ($country != '')?$country:'USA';
                            $phone = ($phone != '')?$phone:'None';
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $exyear.$expmonth;
                            
                            $transaction = array(
                                    'CUST_NBR' => $CUST_NBR,
                                    'MERCH_NBR' => $MERCH_NBR,
                                    'DBA_NBR' => $DBA_NBR,
                                    'TERMINAL_NBR' => $TERMINAL_NBR,
                                    'CARD_ENT_METH' => 'E',
                                    'INDUSTRY_TYPE' => 'E',
                                    'AMOUNT' => $amount,
                                    'TRAN_NBR' => rand(1,10),
                                    'BATCH_ID' => time(),
                                    'VERBOSE_RESPONSE' => 'Y',
                            );
                            if($firstName != ''){
                                $transaction['FIRST_NAME'] = $firstName;
                            }
                            if($lastname != ''){
                                $transaction['LAST_NAME'] = $lastname;
                            }
                            if($address1 != ''){
                                $transaction['ADDRESS'] = $address1;
                            }
                            if($city != ''){
                                $transaction['CITY'] = $city;
                            }
                            if( $zipcode != ''){
                                $transaction['ZIP_CODE'] = $zipcode;
                            }
                            $transaction['EXP_DATE'] = $expry;
                            $transaction['ACCOUNT_NBR'] = $card_no;
                            $transaction['TRAN_TYPE'] = 'CCE1';
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                            $res = $result;

                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                $responseId = $transactionID = $result['AUTH_GUID'];
                                $pay_sts = "SUCCESS";
                            }
                           
                        }

                        $action = 'Pay Invoice';
                        $st = 0;
                        $msg = 'Payment Failed';
                        $crtxnID = $qbID = '';
                        if ($pay_sts == "SUCCESS") {
                            
                            $ispaid    = 'true';
                            
                            $bamount = $in_data['BalanceRemaining'] - $amount;
                            $totamt  = $in_data['BalanceRemaining'] + $in_data['AppliedAmount'];
                            if ($bamount > 0) {
                                $ispaid = 'false';
                            }

                            if($in_data['recurring_send_mail']){
                                $this->send_mail_data($in_data, '5');
                            }

                            $app_amount = $in_data['AppliedAmount'] + $amount;
                            $data       = array('IsPaid' => $ispaid, 'AppliedAmount' => ($app_amount), 'BalanceRemaining' => $bamount);
                            $condition  = array('TxnID' => $in_data['TxnID']);
                            $this->general_model->update_row_data('qb_test_invoice', $condition, $data);

                            $nf = $this->addNotificationForMerchant($amount,$in_data['FullName'],$customerID,$in_data['merchID'],$in_data['TxnID']);

                        }else{

                            $nf = $this->failedNotificationForMerchant($amount,$in_data['FullName'],$customerID,$in_data['merchID'],$in_data['TxnID']);
                        }
                        
                        $id = $this->general_model->insert_gateway_transaction_data($res, $type, $in_data['gatewayID'], $in_data['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $in_data['resellerID'], $invoiceID);
                        if ($pay_sts == "SUCCESS") {
                            $user = $in_data['qbwc_username'];
                            $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1', '', $user);
                        }


                    }
                }
            }
        }
    }

    public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
            $title = 'Sale Payments';
            $nf_desc = 'A payment for <b>'.$customerName.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
            $type = 1;
        }else{
            $title = 'Invoice Recurring Payments';
            $in_data =    $this->general_model->get_row_data('qb_test_invoice', array('TxnID'=>$invoiceNumber));
            if(isset($in_data['RefNumber'])){
                $invoiceRefNumber = $in_data['RefNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }
            $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
            $type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
    public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('qb_test_invoice', array('TxnID'=>$invoiceNumber));
        if(isset($in_data['RefNumber'])){
            $invoiceRefNumber = $in_data['RefNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
}
