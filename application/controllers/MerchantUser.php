<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class MerchantUser extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');

		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);

		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
	}


	public function index()
	{

		redirect(base_url('MerchantUser/admin_role'));
	}


	/********** Get Admin Details ********/

	public function admin_role()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];

		$roles        = $this->general_model->get_table_data('tbl_role', array('merchantID' => $user_id));

		$rolenew = array();
		$authnew = array();
		if (!empty($roles)) {
			foreach ($roles as $key => $role) {
				$auth = '';
				if (!empty($role['authID']))
					$auth = $this->general_model->get_auth_data($role['authID']);

				$role['authName'] = $auth;
				$authnew[$key] = $role;
			}
		}
		$data['roles_data']      = $authnew;
		$rolename = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_userrole', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Roles ********/

	public function create_role()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('roleName', 'roleName', 'required');


		if ($this->form_validation->run() == true && !empty($this->czsecurity->xssCleanPostInput('role'))) {
			$input_data['roleName']	   = $this->czsecurity->xssCleanPostInput('roleName');

			$qq = implode(',', $this->czsecurity->xssCleanPostInput('role'));
			$input_data['roleName']  = $this->czsecurity->xssCleanPostInput('roleName');
			$input_data['authID'] = $qq;
			$merchantID  = $this->session->userdata('logged_in')['merchID'];
			$input_data['merchantID'] = $merchantID;

			if ($this->czsecurity->xssCleanPostInput('roleID') != "") {

				$id = $this->czsecurity->xssCleanPostInput('roleID');

				$condition = array('roleID' => $id);
				$updt = $this->general_model->update_row_data('tbl_role', $condition, $input_data);
				if ($updt) {
					$this->session->set_flashdata('success', 'Successfully Updated Role');
				} else {


					$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
				}
			} else {
				$insert =  $this->general_model->insert_row('tbl_role', $input_data);
				if ($insert) {
					$this->session->set_flashdata('success', 'Successfully Inserted Role');
				} else {


					$this->session->set_flashdata('message', '<Strong>Error:</strong> Something Is Wrong.');
				}
			}


			redirect(base_url('MerchantUser/admin_role'));
		} else {
			$this->session->set_flashdata('message', 'Error: Validation Error.');
		}
		if ($this->uri->segment('3') != "") {

			$roleId = $this->uri->segment('3');
			$con = array('roleID' => $roleId);
			$data['role'] = $this->general_model->get_row_data('tbl_role', $con);
		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id = $data['login_info']['merchID'];

		$rolename = $this->general_model->get_table_data('tbl_auth', '');

		$data['auths'] = $rolename;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/page_userrole', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/********** Get Role ID ********/

	public function get_role_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('role_id');
		$val = array(
			'roleID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_role', $val);
		echo json_encode($data);
	}


	/********** Delete Admin Role ********/



	public function delete_role()
	{

		$roleID = $this->czsecurity->xssCleanPostInput('merchantroleID');
		
		$condition =  array('roleID' => $roleID);

		$del      = $this->general_model->delete_row_data('tbl_role', $condition);
		if ($del) {
			$this->session->set_flashdata('success', 'Successfully Deleted Role');
		} else {


			$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
		}

		redirect(base_url('MerchantUser/admin_role'));
	}






	/********** Get Admin Users Records ********/




	public function admin_user()
	{


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];

		$users = $this->general_model->get_merchant_user_data($user_id);
		$usernew = array();
		foreach ($users as $key => $user) {
			$auth = $this->general_model->get_auth_data($user['authID']);
			$user['authName'] = $auth;
			$usernew[$key] = $user;
		}

		$data['user_data']  = $usernew;
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/admin_user', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/********** Add, Update and Edit Admin Users ********/

	public function create_user()
	{
		if ($this->session->userdata('logged_in')) {
            $data['login_info']      = $this->session->userdata('logged_in');
            $merchantID = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']      = $this->session->userdata('user_logged_in');
            $merchantID = $data['login_info']['merchantID'];
        }

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('userFname', 'userFname', 'required');
			$this->form_validation->set_rules('userLname', 'userLname', 'required');


			if ($this->form_validation->run() == true) {

				$input_data['userFname']	   = $this->czsecurity->xssCleanPostInput('userFname');
				$input_data['userLname']	   = $this->czsecurity->xssCleanPostInput('userLname');
				$input_data['userEmail']	   = $this->czsecurity->xssCleanPostInput('userEmail');

				$is_exist_data = [
					'email' => $input_data['userEmail']
				];

				if ($this->czsecurity->xssCleanPostInput('userID') == "") {
					$input_data['userPasswordNew'] = password_hash(
                        $this->czsecurity->xssCleanPostInput('userPassword'),
                        PASSWORD_BCRYPT
                    );
				} else {
					$is_exist_data['merchantUserID'] = $this->czsecurity->xssCleanPostInput('userID');
				}

				$input_data['userAddress']	   = $this->czsecurity->xssCleanPostInput('userAddress');
				$input_data['roleId']          = $this->czsecurity->xssCleanPostInput('roleID');
				$input_data['merchantID']	   = $merchantID;

				$is_exist = is_email_exists($is_exist_data);
				if($is_exist) {
					$this->session->set_flashdata('message', '<strong>Error:</strong> '.$is_exist);
				} else{
					if ($this->czsecurity->xssCleanPostInput('userID') != "") {

						$id = $this->czsecurity->xssCleanPostInput('userID');
						$chk_condition = array('merchantUserID' => $id, 'merchantID' => $merchantID);
						$updt = $this->general_model->update_row_data('tbl_merchant_user', $chk_condition, $input_data);
						if ($updt) {
							$this->session->set_flashdata('success', 'Successfully Updated User');
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
						}
					} else {
						$insert = $this->general_model->insert_row('tbl_merchant_user', $input_data);
						if ($insert) {
							$this->session->set_flashdata('success', 'Successfully Created New User');
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
						}
					}
				}			
				redirect(base_url('MerchantUser/admin_user'));
			}
		}

		if ($this->uri->segment('3')) {
			$userID  			  = $this->uri->segment('3');
			$con                = array('merchantUserID' => $userID, 'merchantID' => $merchantID);
			$data['user'] 	  = $this->general_model->get_row_data('tbl_merchant_user', $con);
			if(!$data['user'] || empty($data['user'])){
				$this->session->set_flashdata('error', 'Invalid Request');
				redirect(base_url('MerchantUser/admin_user'));
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		$username = $this->general_model->get_table_data('tbl_auth', '');
		$data['auths'] = $username;

		$con1                = array('merchantID' => $merchantID);
		$role = $this->general_model->get_table_data('tbl_role', $con1);

		$data['role_name'] = $role;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/adduser', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/**************Delete Admin Users********************/

	public function delete_user()
	{

		$userID = $this->czsecurity->xssCleanPostInput('merchantID');
		$condition =  array('merchantUserID' => $userID);
		$del      = $this->general_model->delete_row_data('tbl_merchant_user', $condition);
		if ($del) {
			$this->session->set_flashdata('success', 'Successfully Deleted User');
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> MerchantID not found.</div>');
		}

		redirect(base_url('MerchantUser/admin_user'));
	}




	public function edit_customer()
	{
		if ($this->session->userdata('logged_in')) {

			
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'firstName', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'lastName', 'required|xss_clean');
			$this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');
			$this->form_validation->set_rules('companyName', 'comapanyName', 'required|xss_clean');
			$this->form_validation->set_rules('userEmail', 'userEmail', 'required|xss_clean');
		
			$this->form_validation->set_rules('phone', 'Contact No.', 'required');
			
			if ($this->form_validation->run() == true) {

				$input_data['firstName']   = $this->czsecurity->xssCleanPostInput('firstName');
				$input_data['lastName']	   = $this->czsecurity->xssCleanPostInput('lastName');
				$input_data['fullName']	   = $this->czsecurity->xssCleanPostInput('fullName');
				$input_data['userEmail']   = $this->czsecurity->xssCleanPostInput('userEmail');
				$input_data['country']	   = $this->czsecurity->xssCleanPostInput('companyCountry');
				$input_data['state']	   = $this->czsecurity->xssCleanPostInput('companyState');
				$input_data['city']	       = $this->czsecurity->xssCleanPostInput('companyCity');
				$input_data['companyName']	       = $this->czsecurity->xssCleanPostInput('companyName');
				$input_data['isActive']   = 'true';
				
				$input_data['zipcode']	  = $this->czsecurity->xssCleanPostInput('zipCode');
				
				$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
				$user = 	$comp_data['qbwc_username'];
				$input_data['companyID']   = $comp_data['id'];
				$input_data['phoneNumber'] = $this->czsecurity->xssCleanPostInput('phone');

				$input_data['address1']	   = $this->czsecurity->xssCleanPostInput('address1');
				$input_data['address2']    = $this->czsecurity->xssCleanPostInput('address2');

				$input_data['ship_country']	   = $this->czsecurity->xssCleanPostInput('sCountry');
				$input_data['ship_state']	   = $this->czsecurity->xssCleanPostInput('sState');
				$input_data['ship_city']	       = $this->czsecurity->xssCleanPostInput('sCity');
				$input_data['ship_address1']	   = $this->czsecurity->xssCleanPostInput('saddress1');
				$input_data['ship_address2']    = $this->czsecurity->xssCleanPostInput('saddress2');
				$input_data['ship_zipcode']	  = $this->czsecurity->xssCleanPostInput('szipCode');

				$input_data['merchantID']   = $user_id;
				$input_data['createdat']    = date('Y-m-d H:i:s');
				$c_List   				   = $this->czsecurity->xssCleanPostInput('customerID');
				if ($this->czsecurity->xssCleanPostInput('overdue_checked'))
					$enable  = '1';
				else
					$enable  = '0';
				if ($c_List != "") {
					$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('customerID' => $c_List));
					if (!empty($exist_row)) {
						$input_data['qb_status'] = 0;
						$input_data['qb_action'] = "Add Customer";
						$cusID =   $this->general_model->update_row_data('tbl_custom_customer', array('customerID' => $c_List), $input_data);
						$cusID = $exist_row['customerID'];
						$this->quickbooks->enqueue(QUICKBOOKS_ADD_CUSTOMER, $cusID, '1', '', $user);
					} else {
						$input_data['qb_status'] = 0;
						$input_data['qb_action'] = "Add Customer";
						$cusID =  $this->general_model->insert_row('tbl_custom_customer', $input_data);
						$login = array('overDue' => $enable);
						$this->general_model->update_row_data('tbl_custom_login', array('customerID' => $cID), $login);
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '1', '', $user);
					}
				}
				if ($cusID) {

					$this->session->set_flashdata('success', 'Success');
				} else {

					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
				}
				redirect(base_url('home/customer'));
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Validation errors.</div>');
				redirect(base_url('home/index'));
			}
		}

		if ($this->uri->segment('3')) {
			$userID  		  = $this->uri->segment('3');
			$con                = array('customerID' => $userID);
			$data['customer']   = $this->general_model->get_row_data('tbl_custom_customer', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('logged_in');

		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$state = $this->general_model->get_table_data('state', '');
		$data['state_datas'] = $state;

		$city = $this->general_model->get_table_data('city', '');
		$data['city_datas'] = $city;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_customer_edit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_customer()
	{
		if ($this->session->userdata('logged_in')) {
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			
			$this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');

			if ($this->form_validation->run() == true) {

				$input_data['firstName']   = $this->czsecurity->xssCleanPostInput('firstName');
				$input_data['lastName']	   = $this->czsecurity->xssCleanPostInput('lastName');
				$input_data['fullName']	   = $this->czsecurity->xssCleanPostInput('fullName');
				$input_data['userEmail']   = $this->czsecurity->xssCleanPostInput('userEmail');
				$input_data['country']	   = $this->czsecurity->xssCleanPostInput('companyCountry');
				$input_data['state']	   = $this->czsecurity->xssCleanPostInput('companyState');
				$input_data['city']	       = $this->czsecurity->xssCleanPostInput('companyCity');
				$input_data['ship_country']	   = $this->czsecurity->xssCleanPostInput('scountry');
				$input_data['ship_state']	   = $this->czsecurity->xssCleanPostInput('sState');
				$input_data['ship_city']	           = $this->czsecurity->xssCleanPostInput('sCity');
				$input_data['ship_address1']	       = $this->czsecurity->xssCleanPostInput('saddress1');
				$input_data['ship_address2']           = $this->czsecurity->xssCleanPostInput('saddress2');
				$input_data['ship_zipcode']	           = $this->czsecurity->xssCleanPostInput('szipCode');
				$input_data['companyName']	           = $this->czsecurity->xssCleanPostInput('companyName');
				$input_data['isActive']                = 'true';
			
				$input_data['zipcode']	  = $this->czsecurity->xssCleanPostInput('zipCode');
				
				$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
				$user = 	$comp_data['qbwc_username'];
				$input_data['companyID']        = 	$comp_data['id'];
				$input_data['phoneNumber']      = $this->czsecurity->xssCleanPostInput('phone');

				$input_data['address1']	        = $this->czsecurity->xssCleanPostInput('address1');
				$input_data['address2']         = $this->czsecurity->xssCleanPostInput('address2');
				$input_data['merchantID']        = $user_id;
				$input_data['createdat']         = date('Y-m-d H:i:s');
				$c_List   				        = $this->czsecurity->xssCleanPostInput('customerListID');
				$input_data['ListID']            = $c_List;
				
				$input_data['fullName'] = getSanitizedName($input_data['fullName']);
				$input_data['fullName'] = str_replace("'","\\'", $input_data['fullName']);

				if ($this->czsecurity->xssCleanPostInput('overdue_checked'))
					$enable  = '1';
				else
					$enable  = '0';

				if ($c_List != "") {
					$url = base_url('home/view_customer/'.$c_List);
					$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('customListID' => $c_List, 'merchantID' => $user_id));
					if (!empty($exist_row)) {

						$cID = $exist_row['customerID'];
						$input_data['EditSequence'] =  $this->czsecurity->xssCleanPostInput('EditSequence');
						
						$cusID =   $this->general_model->update_row_data('tbl_custom_customer', array('customerID' => $cID), $input_data);
						$login = array('overDue' => $enable);
						$this->general_model->update_row_data('tbl_customer_login', array('customerID' => $cID), $login);
						$editListID = $this->czsecurity->xssCleanPostInput('customerListID');

						if($exist_row['customListID'] != $input_data['ListID'])
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $editListID, '1', '', $user);
					} else {
						$input_data['qb_status']    = 1;
						$input_data['qb_action'] = "Edit Customer";
						$input_data['EditSequence'] =  $this->czsecurity->xssCleanPostInput('EditSequence');
						
						$editListID = $this->czsecurity->xssCleanPostInput('customerListID');
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $editListID, '1', '', $user);
					}

					$cusom_data = array(
						'ListID' => $input_data['ListID'], 'FirstName' => $input_data['firstName'], 'LastName' => $input_data['lastName'], 'FullName' => $input_data['fullName'],
						'Contact' => $input_data['userEmail'], 'companyName' => $input_data['companyName'], 'companyID' => $input_data['companyID'],
						'qbmerchantID' => $user_id, 'qb_status' => 0, 'customerStatus' => 1, 'qbAction' => $input_data['qb_action'],

						'ShipAddress_Addr1' => $input_data['ship_address1'], 'ShipAddress_Addr2' => $input_data['ship_address2'], 'ShipAddress_City' => $input_data['ship_city'], 'ShipAddress_State' => $input_data['ship_state'],
						'ShipAddress_PostalCode' => $input_data['ship_zipcode'], 'ShipAddress_Country' => $input_data['ship_country'],
						'BillingAddress_Addr1' => $input_data['address1'], 'BillingAddress_Addr2' => $input_data['address2'], 'BillingAddress_State' => $input_data['state'],
						'BillingAddress_City' => $input_data['city'], 'BillingAddress_Country' => $input_data['country'], 'BillingAddress_PostalCode' => $input_data['zipcode'],

						'Phone' => $input_data['phoneNumber'], 'TimeCreated' => date('Y-m-d H:i:s'), 'TimeModified' => date('Y-m-d H:i:s')
					);

					if (!isset($exist_row) || empty($exist_row)) {
						$cusom_data['IsActive'] = 'true';
					}

					$this->general_model->update_row_data('qb_test_customer', array('ListID' => $input_data['ListID'],'qbmerchantID' => $user_id,'companyID' => $input_data['companyID']), $cusom_data);
					$this->session->set_flashdata('success', 'Successfully Updated Customer');
				} else {
					$url = base_url('home/customer/');

					$input_data['qb_action'] = "Add Customer";
					$c_List = mt_rand(4000000, 9000000);
					$input_data['customListID'] = $c_List;

					
					$cusID = 	 $this->general_model->insert_row('tbl_custom_customer', $input_data);
					$login = array(
						'customerID' => $cusID, 'merchantID' => $user_id, 'createdAt' => date('Y-m-d H:i:s'),
						'customerEmail' => $input_data['userEmail'], 'overDue' => $enable,
						'customerPassword' => md5($input_data['userEmail']), 'isEnable' => '1', 'customerUsername' => $input_data['userEmail']
					);
					$cus111ID = 	 $this->general_model->insert_row('tbl_customer_login', $login);


					$this->quickbooks->enqueue(QUICKBOOKS_ADD_CUSTOMER, $cusID, '1', '', $user);

					$cusom_data = array(
						'ListID' => $c_List, 'FirstName' => $input_data['firstName'], 'LastName' => $input_data['lastName'], 'FullName' => $input_data['fullName'],
						'Contact' => $input_data['userEmail'], 'companyName' => $input_data['companyName'], 'companyID' => $input_data['companyID'],

						'ShipAddress_Addr1' => $input_data['ship_address1'], 'ShipAddress_Addr2' => $input_data['ship_address2'], 'ShipAddress_City' => $input_data['ship_city'], 'ShipAddress_State' => $input_data['ship_state'],
						'ShipAddress_PostalCode' => $input_data['ship_zipcode'], 'ShipAddress_Country' => $input_data['ship_country'],
						'BillingAddress_Addr1' => $input_data['address1'], 'BillingAddress_Addr2' => $input_data['address2'], 'BillingAddress_State' => $input_data['state'],
						'BillingAddress_City' => $input_data['city'], 'BillingAddress_Country' => $input_data['country'], 'BillingAddress_PostalCode' => $input_data['zipcode'],



						'qbmerchantID' => $user_id, 'qb_status' => 0, 'customerStatus' => 1, 'IsActive' => 'true', 'qb_status' => 0, 'qbAction' => $input_data['qb_action'],
						'Phone' => $input_data['phoneNumber'], 'TimeCreated' => date('Y-m-d H:i:s'), 'TimeModified' => date('Y-m-d H:i:s')
					);

					$this->general_model->insert_row('qb_test_customer', $cusom_data);
					$this->session->set_flashdata('success', 'Successfully Inserted Customer');
				}
			

				
				redirect($url);
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Validation errors.</div>');
				redirect(base_url('home/index'));
			}
		}

		if ($this->uri->segment('3')) {
			$userID  		  = $this->uri->segment('3');
			$con                = array('ListID' => $userID, 'qbmerchantID' => $user_id);
			$data['customer']   = $this->general_model->get_row_data('qb_test_customer', $con);

			
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('logged_in');
		
		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$state = $this->general_model->get_table_data('state', '');
		$data['state_datas'] = $state;

		$city = $this->general_model->get_table_data('city', '');
		$data['city_datas'] = $city;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_customer', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function delete_customer()
	{
		if ($this->session->userdata('logged_in')) {

			
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if ($this->czsecurity->xssCleanPostInput('status') != "") {
			$customerID  = $this->czsecurity->xssCleanPostInput('custactiveID');
			$status      = 'true';
		} else {

			$customerID  = $this->czsecurity->xssCleanPostInput('custID');
			$status      = 'false';
		}

		$customer    = $this->customer_model->customer_by_id($customerID);
		$user       = $customer->qbwc_username;
		$input_data['firstName']   = $customer->FirstName;
		$input_data['lastName']	   = $customer->LastName;
		$input_data['fullName']	   = $customer->FullName;
		$input_data['userEmail']   = $customer->Contact;
		$input_data['ListID']      = $customerID;
		$input_data['IsActive']    = $status;
		$input_data['companyName'] =	$customer->companyName;
		$input_data['companyID']   =   $customer->companyID;
		$input_data['merchantID']   = $user_id;
		$input_data['phoneNumber'] = $customer->Phone;
		$input_data['EditSequence'] =  $customer->EditSequence;
		$input_data['qb_status']    = '2';
		if ($status == 'false')
			$input_data['qb_action']    = 'Deactivate Customer';
		else
			$input_data['qb_action']    = 'Activate Customer';
		$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('ListID' => $customerID, 'merchantID' => $user_id));
		if (!empty($exist_row)) {
			$custID = $exist_row['customerID'];
			$cusID =   $this->general_model->update_row_data('tbl_custom_customer', array('customerID' => $custID), $input_data);

			$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $custID, '1', '', $user);
		} else {
			$cusID =  $this->general_model->insert_row('tbl_custom_customer', $input_data);
			$this->quickbooks->enqueue(QUICKBOOKS_MOD_CUSTOMER, $cusID, '1', '', $user);
		}






		$condition   = array('ListID' => $customerID);
		if ($status == 'false') {
			$update_data = array('customerStatus' => '0', 'IsActive' => 'false');
			$this->session->set_flashdata('success', 'Successfully Deactivated Customer');
		} else {
			$update_data = array('customerStatus' => '1', 'IsActive' => 'true');
			$this->session->set_flashdata('success', 'Successfully Activated Customer');
		}
		if ($this->general_model->update_row_data('qb_test_customer', $condition, $update_data)) {

		
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In delete process.</div>');
		}

		redirect('home/view_customer/'.$customerID, 'refresh');
	}



	public function sync_customer()
	{
		if ($this->session->userdata('logged_in')) {

		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->input->post(null, true))) {
			$customerID = $this->czsecurity->xssCleanPostInput('cust_id');
			$logID   = $this->czsecurity->xssCleanPostInput('logID');
			$updateRec = 1;

			if ($customerID != '') {
				
				$exist_row = $this->general_model->get_row_data('tbl_custom_customer', array('customerID' => $customerID));
				if (!empty($exist_row)) {

					if ($exist_row['ListID'] != '') {
						$ldata =   $this->general_model->get_select_data('qb_test_customer', array('EditSequence'), array('ListID' => $exist_row['ListID'], 'companyID' => $exist_row['companyID']));
						$edtID = $ldata['EditSequence'];
						$updateRec    =   $this->general_model->update_row_data('tbl_custom_customer', array('customerID' => $customerID), array('qb_status' => 1, 'qb_action' => 'Edit Customer', 'updatedAt' => date('Y-m-d H:i:s'), 'EditSequence' => $edtID));
						$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
						$user    =  $cusdata['qbwc_username'];
					
					} else {

						$$updateRec    =   $this->general_model->update_row_data('tbl_custom_customer', array('customerID' => $customerID), array('qb_status' => 0,  'qb_action' => 'Add Customer', 'updatedAt' => date('Y-m-d H:i:s')));
						$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
						$user =  $cusdata['qbwc_username'];
						
					}
				}
			}


			$log_row = $this->general_model->get_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID));

			if (!empty($log_row)) {
				$logupdate    =   $this->general_model->update_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID), array('qb_status' =>'q', 'enqueue_datetime' => date('Y-m-d H:i:s')));
			}

		
			$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Error in process.</div>');
		}
		redirect('home/qbd_log', 'refresh');
	}




	public function create_product()
	{
		if ($this->session->userdata('logged_in')) {

			
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('productName', 'Product Name', 'required|xss_clean');

			
			$this->form_validation->set_rules('type', 'Type', 'required|xss_clean');
			
			$this->form_validation->set_rules('proDescription', 'Pro Description', 'required|xss_clean');


			if ($this->form_validation->run() == true) {

				$input_data['productName']         = $this->czsecurity->xssCleanPostInput('productName');
			
				$input_data['productType']	   	   = $this->czsecurity->xssCleanPostInput('type');

				if ($input_data['productType'] != 'Group') {



					if ($this->czsecurity->xssCleanPostInput('pquantity')) {

						$input_data['productQty']	   	   = $this->czsecurity->xssCleanPostInput('pquantity');
					} else {

						$input_data['productQty']	   	   = '1';
					}
					$input_data['IsActive']	   	   = 'true';
					$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
					$user = 	$comp_data['qbwc_username'];
					$input_data['companyID']	       = 	$comp_data['id'];
					if ($this->czsecurity->xssCleanPostInput('AccountRef') != '')
						$input_data['AccountRef']   		   = $this->czsecurity->xssCleanPostInput('AccountRef');
					if ($this->czsecurity->xssCleanPostInput('discount') != '')
						$input_data['Discount']   		   = $this->czsecurity->xssCleanPostInput('discount');
					else
						$input_data['Discount']   		   = 0;

					$input_data['parentID']	  		   = $this->czsecurity->xssCleanPostInput('productParent');

					if ($input_data['parentID'] != '') {
						$pare = $this->general_model->get_select_data('qb_test_item', array('Name'), array('ListID' => $input_data['parentID']));
						if ($pare['Name'] != '') {
							$input_data['parentFullName'] = $pare['Name'];
						}
					} else {
						$input_data['parentFullName'] = '';
					}


					$input_data['AccountRef']	  		   = $this->czsecurity->xssCleanPostInput('productAccount');

					if ($input_data['AccountRef'] != '') {
						$pare1 = $this->general_model->get_select_data('qb_item_account', array('Name'), array('ListID' => $input_data['AccountRef']));
						if ($pare1['Name'] != '') {
							$input_data['accountFullName'] = $pare1['Name'];
						}
					} else {
						$input_data['accountFullName'] = '';
					}


					if ($this->czsecurity->xssCleanPostInput('averageCost') != "")
						$input_data['productPrice']	       = $this->czsecurity->xssCleanPostInput('averageCost');
					else
						$input_data['productPrice']	       = 0;

					$input_data['productDescription']         = $this->czsecurity->xssCleanPostInput('proDescription');
					$itemType = trim($this->czsecurity->xssCleanPostInput('type'));

					$input_data['merchantID']   = $user_id;


					$p_List   				   = $this->czsecurity->xssCleanPostInput('productID');
					$input_data['productListID']       = $p_List;

					if ($p_List != "") {
						$input_data['updatedat']    = date('Y-m-d H:i:s');



						$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productListID' => $p_List, 'merchantID' => $user_id));
						$exist = $this->general_model->get_row_data('qb_test_item', array('ListID' => $p_List));

						$input_data['EditSequence'] = $exist['EditSequence'];
						$input_data['qb_status']    = 1;
						if (!empty($exist_row)) {
							$input_data['qb_action']    = "Edit Item";
							$productID =  $exist_row['productID'];
							$proID =  $this->general_model->update_row_data('tbl_custom_product', array('productID' => $productID), $input_data);
						} else {
							$productID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
						}

						$itemType = $input_data['productType'];
						if ($itemType == "NonInventory") {
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_NONINVENTORYITEM,  $productID, '1', '', $user);
						}

					
						if ($itemType == "Service") {
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_SERVICEITEM,  $productID, '1', '', $user);
						}

						if ($itemType == "OtherCharge") {
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_OTHERCHARGEITEM,  $productID, '1', '', $user);
						}

						 
						if ($itemType == "Discount") {
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_DISCOUNTITEM,  $productID, '1', '', $user);
						}

						if ($itemType == "SubTotal") {
							$this->quickbooks->enqueue(QUICKBOOKS_MOD_SUBTOTALITEM,  $productID, '1', '', $user);
						}


						$product_data = array(
							'ListID' => $p_List, 'IsActive' => 'true', 'TimeCreated' => date('Y-m-d H:i:s'),
							'Name' => $input_data['productName'], 'FullName' => $input_data['productName'], 'Type' => $itemType,

							'SalesPrice' => $input_data['productPrice'], 'SalesDesc' => $input_data['productDescription'], 'AccountRef' => $input_data['AccountRef'],
							'Name' => $input_data['productName'], 'Discount' => $input_data['Discount'], 'Type' => $itemType,
							'Parent_ListID' => $input_data['parentID'], 'Parent_FullName' => $input_data['parentFullName'], 'companyListID' => $input_data['companyID']

						);
						$this->general_model->update_row_data('qb_test_item', array('ListID' => $p_List), $product_data);



						$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
					} else {

						$input_data['createdat']    = date('Y-m-d H:i:s');
						$input_data['updatedat']    = date('Y-m-d H:i:s');
						$input_data['qb_action']    = "Add Item";
						$lsID = mt_rand('4500000', '14458000');
						$input_data['productListID']  = $lsID;
						$proID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
						if ($proID) {

							if ($itemType == "NonInventory") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM,  $proID, '1', '', $user);
							}

							 
							if ($itemType == "Service") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_SERVICEITEM,  $proID, '1', '', $user);
							}
							if ($itemType == "OtherCharge") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_OTHERCHARGEITEM,  $proID, '1', '', $user);
							}

							if ($itemType == "Discount") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_DISCOUNTITEM,  $proID, '1', '', $user);
							}
							if ($itemType == "SubTotal") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_SUBTOTALITEM,  $proID, '1', '', $user);
							}
						


							if ($itemType == "Payment") {
								$this->quickbooks->enqueue(QUICKBOOKS_ADD_PAYMENTITEM,  $proID, '1', '', $user);
							}

							
							$product_data = array(
								'ListID' => $lsID, 'IsActive' => 'true', 'TimeCreated' => date('Y-m-d H:i:s'),
								'Name' => $input_data['productName'], 'FullName' => $input_data['productName'], 'Type' => $itemType,

								'SalesPrice' => $input_data['productPrice'], 'SalesDesc' => $input_data['productDescription'], 'AccountRef' => $input_data['AccountRef'],
								'Name' => $input_data['productName'], 'Discount' => $input_data['Discount'], 'Type' => $itemType,
								'Parent_ListID' => $input_data['parentID'], 'Parent_FullName' => $input_data['parentFullName'], 'companyListID' => $input_data['companyID']

							);
							$this->general_model->insert_row('qb_test_item', $product_data);
							$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
						} else {

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
						}
					}
				} else {
					$input_data['productDescription']         = $this->czsecurity->xssCleanPostInput('proDescription');
					$input_data['IsActive']	   	           = 'true';
					$input_data['merchantID']  				   = $user_id;
					$p_List   				               = $this->czsecurity->xssCleanPostInput('productID');
					$input_data['productListID']                = $p_List;
					$comp_data = $this->general_model->get_row_data('tbl_company', array('merchantID' =>	$user_id));
					$user = 	$comp_data['qbwc_username'];
					$input_data['companyID']	       = 	$comp_data['id'];

					if ($p_List != "") {
						$input_data['updatedat']    = date('Y-m-d H:i:s');
						$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productListID' => $p_List, 'merchantID' => $user_id));
						$exist = $this->general_model->get_row_data('qb_test_item', array('ListID' => $p_List));

						$input_data['EditSequence'] = $exist['EditSequence'];
						$input_data['qb_status']    = 1;
						if (!empty($exist_row)) {
							$input_data['qb_action']    = "Edit Item";
							$productID =  $exist_row['productID'];
							$proID =  $this->general_model->update_row_data('tbl_custom_product', array('productID' => $productID), $input_data);
						} else {
							$productID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
						}
						$this->general_model->delete_row_data('qb_qroup_lineitem', array('groupListID' => $p_List));
						if (!empty($this->czsecurity->xssCleanPostInput('prodID'))) {
							$prod_datas = $this->czsecurity->xssCleanPostInput('prodID');
							$description = $this->czsecurity->xssCleanPostInput('description');
							$rate = $this->czsecurity->xssCleanPostInput('unit_rate');
							$quantity = $this->czsecurity->xssCleanPostInput('quantity');
							foreach ($prod_datas as $k => $prod_data) {
								$input_data1 = array();
								$input_data1['itemListID'] = $prod_data;
								$input_data1['FullName']   = $description[$k];
								$input_data1['Quantity']   = $quantity[$k];
								$input_data1['Price']	  = $rate[$k];
								$input_data1['groupListID'] = $p_List;

								$this->general_model->insert_row('qb_qroup_lineitem', $input_data1);
							}
						}


						$itemType = $input_data['productType'];
						$this->quickbooks->enqueue(QUICKBOOKS_MOD_GROUPITEM,  $productID, '1', '', $user);
						 
						$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
					} else {
						$input_data['createdat']    = date('Y-m-d H:i:s');
						$input_data['updatedat']    = date('Y-m-d H:i:s');
						$input_data['qb_action']    = "Add Item";
						$proID =  $this->general_model->insert_row('tbl_custom_product', $input_data);


						if (!empty($this->czsecurity->xssCleanPostInput('prodID'))) {
							$prod_datas = $this->czsecurity->xssCleanPostInput('prodID');
							$description = $this->czsecurity->xssCleanPostInput('description');
							$rate = $this->czsecurity->xssCleanPostInput('unit_rate');
							$quantity = $this->czsecurity->xssCleanPostInput('quantity');
							foreach ($prod_datas as $k => $prod_data) {
								$input_data1 = array();
								$input_data1['itemListID'] = $prod_data;
								$input_data1['FullName']   = $description[$k];
								$input_data1['Quantity']   = $quantity[$k];
								$input_data1['Price']	  = $rate[$k];
								$input_data1['groupListID'] = $proID;

								$this->general_model->insert_row('qb_qroup_lineitem', $input_data1);
							}
						}



						if ($proID) {

							$this->quickbooks->enqueue(QUICKBOOKS_ADD_GROUPITEM,  $proID, '1', '', $user);




							$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
						} else {

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
						}
					}
				}
				redirect(base_url('home/plan_product'));
			}
		}

		if ($this->uri->segment('3')) {
			$userID  			  = $this->uri->segment('3');
			$con                = array('ListID' => $userID);
			$data['item_pro'] 	  = $this->general_model->get_row_data('qb_test_item', $con);

			if ($data['item_pro']['Type'] == 'Group') {
				$data['item_data'] = $this->general_model->get_table_data('qb_qroup_lineitem', array('groupListID' => $userID));
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
	
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$parent = $this->company_model->get_product_data(array('merchantID' => $user_id));
		$data['products'] = $parent;
		$account = $this->company_model->get_product_account_data(array('merchantID' => $user_id));
		$data['accounts'] = $account;
		$data['items']   = $this->company_model->get_plan_data_item($user_id);


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_product', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function edit_product()
	{
		if ($this->session->userdata('logged_in')) {

		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('productName', 'Product Name', 'required|xss_clean');

			$this->form_validation->set_rules('fullName', 'Full Name', 'required|xss_clean');

			$this->form_validation->set_rules('type', 'Type', 'required|xss_clean');
			$this->form_validation->set_rules('company', 'Company', 'required|xss_clean');

			$this->form_validation->set_rules('averageCost', 'Average Cost', 'required|xss_clean');
			$this->form_validation->set_rules('proDescription', 'Pro Description', 'required|xss_clean');


			if ($this->form_validation->run() == true) {

				$input_data['productName']         = $this->czsecurity->xssCleanPostInput('productName');
				$input_data['productFullName']	   		   = $this->czsecurity->xssCleanPostInput('fullName');
				$input_data['productType']	   	   = $this->czsecurity->xssCleanPostInput('type');
				$input_data['IsActive']	   	   = 'true';
				$input_data['companyID']   		   = $this->czsecurity->xssCleanPostInput('company');
				$comp_data = $this->general_model->get_row_data('tbl_company', array('id' =>	$input_data['companyID']));
				$user = 	$comp_data['qbwc_username'];

				$input_data['parentID']	  		   = $this->czsecurity->xssCleanPostInput('productParent');

				if ($input_data['parentID'] != '') {
					$pare = $this->general_model->get_select_data('qb_test_item', array('Name'), array('ListID' => $input_data['parentID']));
					if ($pare['Name'] != '') {
						$input_data['parentFullName'] = $pare['Name'];
					}
				} else {
					$input_data['parentFullName'] = '';
				}

				$input_data['productPrice']	       = $this->czsecurity->xssCleanPostInput('averageCost');

				$input_data['productDescription']         = $this->czsecurity->xssCleanPostInput('proDescription');
				$itemType = trim($this->czsecurity->xssCleanPostInput('type'));

				$input_data['merchantID']   = $user_id;
				$input_data['createdat']    = date('Y-m-d H:i:s');

				$p_List   				   = $this->czsecurity->xssCleanPostInput('productID');
				

				if ($p_List != "") {
					$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productID' => $p_List));

					$input_data['qb_status']    = 0;
					$input_data['qb_action']    = "Add Item";
					if (!empty($exist_row)) {
						$productID =  $exist_row['productID'];
						$proID =  $this->general_model->update_row_data('tbl_custom_product', array('productID' => $productID), $input_data);
					}

					$itemType = $input_data['productType'];
					if ($itemType == "NonInventory") {
						$this->quickbooks->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM,  $productID, '1', '', $user);
					}

					 
					if ($itemType == "Service") {
						$this->quickbooks->enqueue(QUICKBOOKS_ADD_SERVICEITEM,  $productID, '1', '', $user);
					}
					$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
				} else {
					$proID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
					if ($proID) {
						if ($itemType == "NonInventory") {
							$this->quickbooks->enqueue(QUICKBOOKS_ADD_NONINVENTORYITEM,  $proID, '1', '', $user);
						}

					  
						if ($itemType == "Service") {
							$this->quickbooks->enqueue(QUICKBOOKS_ADD_SERVICEITEM,  $proID, '1', '', $user);
						}

						$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
					} else {

						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
					}
				}
				redirect(base_url('home/plan_product'));
			}
		}

		if ($this->uri->segment('3')) {
			$proID  			  = $this->uri->segment('3');
			$con                = array('productID' => $proID);
			$data['item_pro'] 	  = $this->general_model->get_row_data('tbl_custom_product', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;

		$parent = $this->company_model->get_product_data(array('merchantID' => $user_id));
		$data['products'] = $parent;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_product_edit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}







	public function change_product_status()
	{


		if ($this->czsecurity->xssCleanPostInput('status') == "") {
			$productID    = $this->czsecurity->xssCleanPostInput('prolistID');
			$status       = 'false';
		}
		if ($this->czsecurity->xssCleanPostInput('status') == "1") {

			$productID   = $this->czsecurity->xssCleanPostInput('proactiveID');
			$status      = 'true';
		}

		$product_data    = $this->general_model->get_row_data('qb_test_item', array('ListID' => $productID));


		$input_data['productName']         = $product_data['Name'];
		$input_data['productFullName']	   = $product_data['FullName'];
		$input_data['productType']	   	   = $product_data['Type'];
		$input_data['IsActive']	   	       = $status;
		$input_data['companyID']   		   = $product_data['companyListID'];
		$comp_data    = $this->general_model->get_row_data('tbl_company', array('id' =>	$input_data['companyID']));
		$user = 	$comp_data['qbwc_username'];

		$input_data['parentID']	  		   = $product_data['Parent_ListID'];
		$itemType 						   = trim($product_data['Type']);

		$input_data['merchantID']       = $comp_data['merchantID'];
		$input_data['createdat']        = date('Y-m-d H:i:s');
		$input_data['updatedat']        = date('Y-m-d H:i:s');

		$input_data['productListID']        = $productID;
		$input_data['EditSequence']       =  $product_data['EditSequence']; 
		$input_data['qb_status']    = '2';
		$condition   = array('ListID' => $productID);
		$update_data = array('IsActive' => $status);
		if ($status == 'false') {
			$input_data['qb_action']    = "Deactive Item";
			$msg 			= " Item Deactivated Successfully";
			$this->session->set_flashdata('success', $msg);
		} else {
			$input_data['qb_action']    = "Active Item";
			$msg 			= " Item Activated Successfully";
			$this->session->set_flashdata('success', $msg);
		}
		$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productListID' => $productID));
		if (!empty($exist_row)) {
			$prodID =   $this->general_model->update_row_data('tbl_custom_product', array('productID' => $exist_row['productID']), $input_data);
			$prodID = $exist_row['productID'];
		} else {
			$prodID =  $this->general_model->insert_row('tbl_custom_product', $input_data);
		}

		if ($prodID) {

			$itemType = $input_data['productType'];
			if ($itemType == "NonInventory") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_NONINVENTORYITEM,  $prodID, '1', '', $user);
			}

			
			if ($itemType == "Service") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_SERVICEITEM,  $prodID, '1', '', $user);
			}

			if ($itemType == "OtherCharge") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_OTHERCHARGEITEM,  $prodID, '1', '', $user);
			}

		
			if ($itemType == "Discount") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_DISCOUNTITEM,  $prodID, '1', '', $user);
			}
			if ($itemType == "Payment") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_PAYMENTITEM,  $prodID, '1', '', $user);
			}
			if ($itemType == "SubTotal") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_SUBTOTALITEM,  $prodID, '1', '', $user);
			}
			if ($itemType == "Group") {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_GROUPITEM,  $prodID, '1', '', $user);
			}



			if ($this->general_model->update_row_data('qb_test_item', $condition, $update_data)) {

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> In delete process.</div>');
			}
		}

		redirect('home/plan_product', 'refresh');
	}





	public function sync_product()
	{

		if ($this->session->userdata('logged_in')) {

		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->input->post(null, true))) {

			$proID   = $this->czsecurity->xssCleanPostInput('prod_id');
			$logID   = $this->czsecurity->xssCleanPostInput('logID');
			$updateRec = 0;



			if ($proID != '') {
				
				$exist_row = $this->general_model->get_row_data('tbl_custom_product', array('productID' => $proID));
				if (!empty($exist_row)) {



					if ($exist_row['productListID'] != '') {
						$ldata =   $this->general_model->get_select_data('qb_test_item', array('EditSequence'), array('ListID' => $exist_row['productListID'], 'companyListID' => $exist_row['companyID']));
						$edtID = $ldata['EditSequence'];
						$updateRec    =   $this->general_model->update_row_data('tbl_custom_product', array('productID' => $proID), array('qb_status' => 1, 'qb_action' => 'Edit Item', 'updatedAt' => date('Y-m-d H:i:s'), 'EditSequence' => $edtID));
						$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
						$user    =  $cusdata['qbwc_username'];
						$itemType = $exist_row['productType'];
						
						if ($itemType == "NonInventory") {
							
						}


						if ($itemType == "Service") {
							
						}
					} else {


						$updateRec    =   $this->general_model->update_row_data('tbl_custom_product', array('productID' => $proID), array('qb_status' => 0, 'qb_action' => 'Add Item', 'updatedAt' => date('Y-m-d H:i:s')));
						$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
						$user =  $cusdata['qbwc_username'];
						
						$itemType = $exist_row['productType'];
						
						if ($itemType == "NonInventory") {
							
						}


						if ($itemType == "Service") {
							
						}
					}
				}
			}
			$log_row = $this->general_model->get_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID));

			if (!empty($log_row)) {
				$logupdate    =   $this->general_model->update_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID), array('qb_status' =>'q', 'enqueue_datetime' => date('Y-m-d H:i:s')));
			}
			
			$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
		}

		redirect('home/qbd_log', 'refresh');
	}




	public function get_plan_data_item()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		$plan =    $this->company_model->get_plan_data_item($user_id);
		echo json_encode($plan);
		die;
	}


	public function create_credit()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		if (!empty($this->input->post(null, true))) {

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemPrice']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];

				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['updatedAt'] = date('Y-m-d H:i:s');


				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $user_id));
			if (!empty($in_data)) {
				$inv_pre   = $in_data['prefix'];
				$inv_po    = $in_data['postfix'] + 1;
				$new_inv_no = $inv_pre . $inv_po;
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('home/invoices', 'refresh');
			}

			$ins_data['customerID'] = $this->czsecurity->xssCleanPostInput('customerID');
			$ins_data['creditDescription'] = $this->czsecurity->xssCleanPostInput('cr_description');
			$ins_data['creditMemo']  = $this->czsecurity->xssCleanPostInput('cr_note');
			$ins_data['creditDate'] = date('Y-m-d H:i:s');
			$ins_data['creditAmount'] = $total;
			$ins_data['creditNumber'] = $new_inv_no;
			$ins_data['updatedAt']     = date('Y-m-d H:i:s');
			$crlsID = mt_rand('4464646464', '55545454544');
			$ins_data['creditListID'] = $crlsID;
			$cname = '';
			$exist_row =   $this->general_model->get_select_data('qb_test_customer', array('companyID', 'FullName'), array('ListID' => $ins_data['customerID']));
			$cname    = $exist_row['FullName'];
			$cusdata = $this->general_model->get_select_data('tbl_company', array('qbwc_username'), array('id' => $exist_row['companyID']));
			$user    =  $cusdata['qbwc_username'];

			$ins_data['merchantDataID'] = $user_id;
			$ins_id = $this->general_model->insert_row('tbl_custom_credit', $ins_data);

			foreach ($item_val as $k => $item) {
				$item['crlineID'] = $ins_id;
				$ins = $this->general_model->insert_row('tbl_credit_item', $item);
				$linitem = mt_rand('300000000', 800000000);
				$cr_itm = array(
					'itemListID' => $item['itemListID'], 'TxnLineID' => $linitem,
					'ItemDesc' => $item['itemDescription'], 'itemQuantity' => $item['itemQuantity'],
					'ItemRate' => $item['itemPrice'], 'Amount' => ($item['itemPrice'] * $item['itemQuantity']),
					'CreditTxnID' => $crlsID
				);

				$this->general_model->insert_row('qb_customer_credit_item', $cr_itm);
			}


			if ($ins_id) {
				$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $user_id), array('postfix' => $inv_po));
				$this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1', '', $user);

				$credit_ins = array(
					'TxnID' => $crlsID,
					'companyListID' => $exist_row['companyID'],
					'DueDate' => date('Y-m-d H:i:s'),
					'TotalAmount' => $total,
					'SubTotal' => $total,
					'CreditRemaining' => $total,
					'IsPending' => 'false', 'Memo' => $ins_data['creditMemo'],
					'TimeCreated' => date('Y-m-d H:i:s'), 'TimeModified' => date('Y-m-d H:i:s'), 'CustomerListID' => $ins_data['customerID'], 'CustomerFullName' => $cname
				);


				$ins = $this->general_model->insert_row('qb_customer_credit', $credit_ins);

				$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
			}

			redirect('Payments/credit', 'refresh');
		}


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);

		$data['plans']          = $this->company_model->get_plan_data($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_credit', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function recover_pwd()
	{
		if ($this->session->userdata('logged_in')) {

			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {

			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}

		$id = $this->uri->segment('3');
		$results = $this->general_model->get_select_data('tbl_merchant_user', array('userFname', 'userLname', 'userEmail', 'merchantID'), array('merchantUserID' => $id));

		$merchant_data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'firstName', 'lastName','merchantEmail','merchantContact'), array('merchID' => $results['merchantID']));

		$res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $merchant_data['resellerID']));

		$Merchant_url = $this->general_model->get_select_data('Config_merchant_portal', array('merchantPortalURL'), array('resellerID' => $merchant_data['resellerID']));

		if (!empty($results)) {
			$name = $results['userFname'];
			$login_url = $Merchant_url['merchantPortalURL'];
			$toEmail = $results['userEmail'];
			$this->load->helper('string');
			$password = random_string('alnum', 16);
			$marchant_name = $merchant_data['firstName'] . ' ' . $merchant_data['lastName'];
			$condition = array('merchantUserID' => $id);
			$data = array(
				'userPasswordNew' => password_hash(
                    $password,
                    PASSWORD_BCRYPT
                )
			);
			$update = $this->general_model->update_row_data('tbl_merchant_user', $condition, $data);
			if ($update) {
				$temp_data = $this->general_model->get_row_data('tbl_email_template', array('merchantID' => $results['merchantID'], 'templateType' => '14'));
				$message = $temp_data['message'];
				$subject = $temp_data['emailSubject'];
				if ($temp_data['fromEmail'] != '') {
					$fromEmail = $temp_data['fromEmail'];
				} else {

					$fromEmail = $res_data['resellerEmail'];
				}

				$mailDisplayName = $marchant_name;
				if ($temp_data['mailDisplayName'] != '') {
					$mailDisplayName = $temp_data['mailDisplayName'];
				}
				$logo_url ='';
				$config_data = $this->general_model->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
				if(!empty($config_data)){
			   		$logo_url  = $config_data['ProfileImage']; 
				}
				if( !empty( $config_data['ProfileImage'])){
				   	$logo_url = base_url().LOGOURL.$config_data['ProfileImage'];  
				}else{
				  	$logo_url = CZLOGO; 
				}
    
    			$logo_url=	"<img src='".$logo_url."' />";

				$login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';
				$message = stripslashes(str_replace('{{userFname}}', $name, $message));
				$message = stripslashes(str_replace('{{login_url}}', $login_url, $message));
				$message = stripslashes(str_replace('{{userEmail}}', $toEmail, $message));
				$message = stripslashes(str_replace('{{userPassword}}', $password, $message));
				$message = stripslashes(str_replace('{{merchant_name}}', $marchant_name, $message));
				$message = stripslashes(str_replace('{{merchant_email}}', $merchant_data['merchantEmail'], $message));
				$message = stripslashes(str_replace('{{merchant_phone}}', $merchant_data['merchantContact'], $message));
				$message = stripslashes(str_replace('{{logo}}',$logo_url ,$message ));



				if($temp_data['replyTo'] != ''){
					$replyTo = $temp_data['replyTo'];
				}else{
					$replyTo = $results['userEmail'];
				}
				$addCC      = $temp_data['addCC'];
				$addBCC		= $temp_data['addBCC'];
				$email_data          = array(
					'customerID'=>'',
					'merchantID'=>$user_id, 
					'emailSubject'=>$subject,
					'emailfrom'=>$fromEmail,
					'emailto'=>$toEmail,
					'emailcc'=>$addCC,
					'emailbcc'=>$addBCC,
					'emailreplyto'=>$replyTo,
					'emailMessage'=>$message,
					'emailsendAt'=> date('Y-m-d H:i:s'),
					
				);

				$mail_sent = $this->general_model->sendMailBySendgrid($toEmail,$subject,$fromEmail,$mailDisplayName,$message,$replyTo, $addCC, $addBCC);

				if($mail_sent){
					$email_data['send_grid_email_id'] = $mail_sent;
					$email_data['send_grid_email_status'] = 'Sent';
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-success"><button data-dismiss="alert" class="close" type="button">×</button>Password Reset request has been sent. If your email is listed with an account, an email will be sent to you.</div>');
				} else {
					$email_data['send_grid_email_status'] = 'Failed';
					$email_data['mailStatus']=0;
					$this->general_model->insert_row('tbl_template_data', $email_data);
					$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
				}
			}
			redirect(base_url() . 'MerchantUser/admin_user');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">Email was not sent, please contact your administrator.</div>');
			redirect(base_url() . 'MerchantUser/admin_user');
		}
	}
	public function sync_event()
	{
		if ($this->session->userdata('logged_in')) {


			$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
		if (!empty($this->input->post(null, true))) {
			$event_id = $this->czsecurity->xssCleanPostInput('event_id');
			$logID   = $this->czsecurity->xssCleanPostInput('logID');
			$cusID = 0;

			if ($event_id != '') {
				$cusID = 1;
			}


			if ($cusID) {
				$log_row = $this->general_model->get_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID));

				if (!empty($log_row)) {
					$logupdate    =   $this->general_model->update_row_data('quickbooks_queue', array('quickbooks_queue_id' => $logID), array('qb_status' =>'q', 'enqueue_datetime' => date('Y-m-d H:i:s')));
				}

				$this->session->set_flashdata('success', 'Data added to sync queue. Please refresh for status update');
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Error in process.</div>');
			}
		}
		redirect('home/qbd_log', 'refresh');
	}
}
