<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class SettingPlan extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
	   
	    include APPPATH . 'third_party/nmiCustomerVault.class.php';
	
	    $this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');

		
	//	$this->load->model('common');
		if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='2')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	    
	 //	 $data['users']   = $this->common->get_list_data('tbl_customer');
	//	 $this->load->view('quickbook/user', $data);
	    
	}
	
		public function plans()
		{
		

		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$data['merchantID']     =  $user_id ;
				
				$condition				= array('merchantID'=>$user_id );
				$condition1				= array('merchantDataID'=>$user_id );
			    
			 //   $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
			 
		        $data['plandata']   = $this->general_model->get_table_data('tbl_subscriptions_plan_qb', $condition1);
		      //  print_r($data['plans']);die;
				// $data['subs_data']      = $this->customer_model->get_total_subscription($user_id );
				$this->load->view('template/template_start', $data);
				
			//	print_r($data['subscriptions']); die;
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/page_plans', $data);
			//	$this->load->view('template/template_scripts',$data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
		
	}	
	
	 public function create_plan()
	 {
	     
	     
	 }
	

	 public function create_plan()
	{
		//Show a form here which collects someone's name and e-mail address
		    if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}
				$base_id = base64_encode($user_id);
	             $coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1');
		        
		        	
	        	$urp_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortal'),$coditionp); 	
			       
			    if(empty($urp_data))
        		 {
        		     	$this->session->set_flashdata('error','<strong>Errr: Please enable your customer portal</strong>'); 
        		    	redirect('SettingConfig/setting_customer_portal','refresh');   
        		 } 
			    
		        
		        $ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 
        		$data['ur_data'] = $ur_data;
        		 $purl = $ur_data['customerPortalURL'].'qbd_check_out/'.$base_id.'/';
        		 $inv_pre_data =$this->general_model->get_row_data('tbl_merchant_invoices',array('merchantID'=>$user_id));
        		 if(empty($inv_pre_data))
        		 {
        		     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Errr: Please set your invoice prefix to create invoice</strong></div>'); 
        		    	redirect('SettingConfig/profile_setting','refresh');   
        		 }
        		 
        //	print_r($this->input->post(null, true));die;	 
		if(!empty($this->input->post(null, true)))
		{
			 
				$total=0;	
			    foreach($this->czsecurity->xssCleanPostInput('productID') as $key=> $prod){
                      //print_r($prod);
                       $insert_row['itemListID'] =$this->czsecurity->xssCleanPostInput('productID')[$key];
					   $insert_row['itemQuantity'] =$this->czsecurity->xssCleanPostInput('quantity')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemRate']      =$this->czsecurity->xssCleanPostInput('unit_rate')[$key];
					   $insert_row['itemTax'] =$this->czsecurity->xssCleanPostInput('tax_check')[$key];
					   $insert_row['itemFullName'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   $insert_row['itemDescription'] =$this->czsecurity->xssCleanPostInput('description')[$key];
					   if($this->czsecurity->xssCleanPostInput('onetime_charge')[$key]){
						   
						  $insert_row['oneTimeCharge'] ='1';  
					   }else{
						 $insert_row['oneTimeCharge'] ='0';  	
						  $total=$total+ $this->czsecurity->xssCleanPostInput('total')[$key];
					   } 
					   
					   
					  	
					   $item_val[$key] =$insert_row;
                }				
				
			    $pname      = $this->czsecurity->xssCleanPostInput('plan_name');
    		//	$customerID = $this->czsecurity->xssCleanPostInput('customerID');		
				$plan        = $this->czsecurity->xssCleanPostInput('duration_list');		
				if($plan > 0){
		        	$subsamount  = $total/$plan ;	
				}else{ 
				   	$subsamount  = $total ;
				}    
				$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));		
				$st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));		
				//$tot_invoice  = $this->czsecurity->xssCleanPostInput('total_invoice');		
				$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');
				$address1     = $this->czsecurity->xssCleanPostInput('address1');
				$paygateway    = $this->czsecurity->xssCleanPostInput('gateway_list');
				$address2     = $this->czsecurity->xssCleanPostInput('address2');				
				$country      = $this->czsecurity->xssCleanPostInput('country');
				$state        = $this->czsecurity->xssCleanPostInput('state');
				$city	      = $this->czsecurity->xssCleanPostInput('city');
				$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
				$phone      = $this->czsecurity->xssCleanPostInput('phone');
				$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
				
			
				
				
				
				
				$shoturl     = $this->czsecurity->xssCleanPostInput('short_url');
				$confirm_page_url = $this->czsecurity->xssCleanPostInput('confirm_page_url');
                $subdata =array(
				  'planName'    => $pname,
				   'subscriptionPlan'   =>$plan,
				   'subscriptionAmount' =>$total,
				   'paymentGateway'    => $paygateway,
			
					'totalInvoice'      => $plan,
					'invoicefrequency'  => $paycycle,
					'freeTrial'			=> $freetrial,
					 'confirm_page_url' => $confirm_page_url, 
					'postPlanURL' => $shoturl,
					'merchantPlanURL' => $purl
					
				
				);  
				
				
				if($paycycle=='mon')
				{
				     if($this->czsecurity->xssCleanPostInput('pro_rate'))
				     {
				         	$subdata['proRate'] ='1';   
				         	
				         	$subdata['proRateBillingDay']    =$this->czsecurity->xssCleanPostInput('pro_billing_date');   
				         	$subdata['nextMonthInvoiceDate'] =$this->czsecurity->xssCleanPostInput('pro_next_billing_date');   
				         
				     }
				     else
				     {
				                         $subdata['proRate'] ='0';   
				         	
				         	$subdata['proRateBillingDay']    =$this->czsecurity->xssCleanPostInput('pro_billing_date');   
				         	$subdata['nextMonthInvoiceDate'] =$this->czsecurity->xssCleanPostInput('pro_next_billing_date');     
				         
				     }
				    
				}
				
				
				if($this->czsecurity->xssCleanPostInput('autopay')){
					//$subdata['cardID']	= $cardID;
					$subdata['automaticPayment'] ='1';
				}else{
					$subdata['automaticPayment'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('email_recurring')){
					
					$subdata['emailRecurring'] ='1';
				}else{
					$subdata['emailRecurring'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('billinfo')){
					
					$subdata['usingExistingAddress'] ='1';
				}else{
					$subdata['usingExistingAddress'] ='0';	
				}
				
				if($this->czsecurity->xssCleanPostInput('require_shipping')){
				    $subdata['isShipping'] ='1';
				}
				else
				{
				    $subdata['isShipping'] ='0';
				}
				if($this->czsecurity->xssCleanPostInput('require_service')){
				    $subdata['isService'] ='1';
				}
				else
				{
				    $subdata['isService'] ='0';
				}	
				
				if($this->czsecurity->xssCleanPostInput('plan_url')!='')
				{
			    	$plan_url                   = $this->czsecurity->xssCleanPostInput('plan_url');
			    	$coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1');
			    	$base_id = base64_encode($user_id);
			    	
			    	$ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 	
			    
			    	if(!empty($ur_data))
			    	{
			    //	$url = "http://".$portal_url.".".$rootDomain."/customer";
			        
					$subdata['merchantPlanURL'] = $ur_data['customerPortalURL'].'qbd_check_out/'.$base_id.'/'.$plan_url;
						$subdata['postPlanURL'] = $plan_url;
			    	}else{
			    	   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portal URL</div>');  
			    	   		redirect('SettingPlan/plans','refresh');
			    	}		
				}
				
				
				
				if($this->czsecurity->xssCleanPostInput('planID') !="") {
				 $rowID = $this->czsecurity->xssCleanPostInput('planID');
				 $subdata['updatedAt']  = date('Y-m-d H:i:s');
				 
				 //$date = date("Y-m-d", strtotime($subs['firstDate']));
				// print_r($subdata); die;
				     $subs =  $this->general_model->get_row_data('tbl_subscriptions_plan_qb',array('planID'=>$rowID));
				 
				 $ins_data = $this->general_model->update_row_data('tbl_subscriptions_plan_qb',array('planID'=>$rowID) ,$subdata);	
				
   				$this->general_model->delete_row_data('tbl_subscription_plan_item_qb', array('planID'=>$rowID));
				   
				    foreach($item_val as $k=>$item){
						$item['planID'] = $rowID;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qb',$item);	
				    } 	
					
				}else{
				    $subdata['merchantDataID'] = $this->session->userdata('logged_in')['merchID'];
				    
				    
				    	if($this->czsecurity->xssCleanPostInput('short_url')!='')
				{
			    	$plan_url                   = $this->czsecurity->xssCleanPostInput('short_url');
			    	$coditionp=array('merchantID'=>$user_id,'customerPortal'=>'1');
			    	$base_id = base64_encode($user_id);
			    	
			    	$ur_data = $this->general_model->get_select_data('tbl_config_setting',array('customerPortalURL'),$coditionp); 	
			    
			    	if(!empty($ur_data))
			    	{
			    //	$url = "http://".$portal_url.".".$rootDomain."/customer";
			        
					$subdata['merchantPlanURL'] = $ur_data['customerPortalURL'].'qbd_check_out/'.$base_id.'/'.$plan_url;
						$subdata['postPlanURL'] = $plan_url;
			    	}else{
			    	   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Please create merchant portak URL</div>');  
			    	   		redirect('Freshbook_controllers/SettingPlan/plans','refresh');
			    	}		
				}
				    
				    
				    
                    $subdata['createdAt']  = date('Y-m-d H:i:s');
				   
					$ins_data = $this->general_model->insert_row('tbl_subscriptions_plan_qb',$subdata);	
					
					foreach($item_val as $k=>$item){
						$item['planID'] = $ins_data;
						$ins = $this->general_model->insert_row('tbl_subscription_plan_item_qb',$item);	
				    } 		
					
				}
					
				 if($ins_data && $ins){
					
			        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Created Plan</strong></div>'); 
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }
				
					redirect('SettingPlan/plans','refresh');
				
        }
              
			//  echo $this->uri->segment('3');  die;
			  
			  if($this->uri->segment('3')!=""){
				  $sbID = $this->uri->segment('3');
				$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions_plan_qb',array('planID'=>$sbID));
				//echo $data['subs']['customerID'];
				//$data['c_cards']    = $this->get_card_expiry_data($data['subs']['customerID']);
				$data['items']      = $this->general_model->get_table_data('tbl_subscription_plan_item_qb',array('planID'=>$sbID)); 

			  }
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				  if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				
				$data['base_id']  = base64_encode($user_id);
				$condition				= array('merchantID'=>$user_id );
			    $data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
				$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy','' );
				$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan','' );
				$data['plans']          = $this->company_model->get_plan_data($user_id);
				$compdata				= $this->customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
				$taxes = $this->general_model->get_table_data('tbl_taxes','');
				$data['taxes'] = $taxes;
				  /*  if(!empty($taxes)){
			
						 foreach($taxes as $tax){ 
						 
						  $tax_id = $tax['taxID'];
				
				         
		           $data['tax']		= $this->general_model->get_row_data('tbl_taxes',array('taxID'=>$tax_id)); 
		       //print_r($data['tax']);die;
						 }
				 } */
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/create_plan', $data);
			//	$this->load->view('template/template_scripts',$data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}   
	
	
	

     public function delete_plan(){
	
	 $plancID = $this->czsecurity->xssCleanPostInput('plancID');
	 $condition =  array('planID'=>$plancID); 
	 $this->general_model->delete_row_data('tbl_subscription_plan_item_qb', $condition);
	
	 $del  = $this->general_model->delete_row_data('tbl_subscriptions_plan_qb', $condition);
            if($del){
					
			        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Deleted Plan</strong></div>'); 
				 }else{
					 
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				 }
	   
						redirect('SettingPlan/plans','refresh');
	}
 
 
 
 
     public function recurring_payment()
     {
         
         	if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}
         
         
          //  print_r($this->input->post(null, true));  die;
         if(!empty($this->input->post(null, true)))
         {
             if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
              $editrecID = ''; $recpayTerm='';
             $editrecID  = $this->czsecurity->xssCleanPostInput('editrecID');
             
             $customerID = $this->czsecurity->xssCleanPostInput('recCustomer');
              $cardID  = $this->czsecurity->xssCleanPostInput('rec_cardID');
              $payterm ='0';
              	$gt_data = $this->general_model->get_select_data('tbl_merchant_gateway',array('gatewayID'),array('merchantID'=>$user_id,'set_as_Default'=>1)); 	
             $gateway    = $gt_data['gatewayID'];
             $recurAuto   = $this->czsecurity->xssCleanPostInput('recurAuto');
             if(!empty($this->czsecurity->xssCleanPostInput('rec_payTem')))
              $payterm = implode(',',$this->czsecurity->xssCleanPostInput('rec_payTem'));
               $recurAuto = $this->czsecurity->xssCleanPostInput('recurAuto');
               
               $amount =  sprintf('%0.2f', $this->czsecurity->xssCleanPostInput('recAmount'));
               $card = array('cardID'=>$cardID, 'amount'=>$amount, 'gateway'=>$gateway, 'customerID'=>$customerID,'merchantID'=>$user_id, 'optionData'=>$recurAuto, 'paymentTerm'=>$payterm );

               			$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID,'merchantID'=>$user_id));
				        $companyID  = $comp_data['companyID'];
                if($chh_mail =='1')
			         {
             
						   $condition_mail  = array('templateType'=>'5', 'merchantID'=>$user_id); 
        				   $ref_number =''; 
        				   $tr_date   =date('Y-m-d H:i:s');
        				   $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
        				   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date );
		        	}               

               if($editrecID!="")
               {
                  $card['updatedAt']  = date('Y-m-d H:i:s');
                  $con =array('recurrID'=>$editrecID);
                  $this->general_model->update_row_data('tbl_recurring_payment',$con, $card);
               }
               else
               {
                   $card['createdAt']  = date('Y-m-d H:i:s');
                   $card['updatedAt']  = date('Y-m-d H:i:s');
                   $this->general_model->insert_row('tbl_recurring_payment', $card);
                   
               }
               
               
               redirect(base_url().'home/view_customer/'.$customerID, 'refresh');
             
         }
         
     }
    
    
     public function delete_recurring()
    {
		                if($this->session->userdata('logged_in') )
				    	{
			               $merchID = $this->session->userdata('logged_in')['merchID'];
			        	}
			        	if($this->session->userdata('user_logged_in') )
			        	{
			               $merchID = $this->session->userdata('user_logged_in')['merchantID'];
			        	}
		        
		
		
		
			              $recurringID =  $this->uri->segment('3'); 
			              
			            
			        	$qq= $this->general_model->get_select_data('tbl_recurring_payment', array('customerID'), array('recurrID'=>$recurringID,'merchantID'=>$merchID));
					 	if(!empty($qq))
					 	{
					 	    $customer=$qq['customerID'];
					 	}
					
		       $sts =  $this->general_model->delete_row_data('tbl_recurring_payment',array('recurrID'=>$recurringID,'merchantID'=>$merchID));
				 if($sts){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Deleted</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
		 }
		 
	    	redirect('home/view_customer/'.$customer,'refresh');
	}	
 
 
	public function get_subs_item_count_data()
    	{
    	     $sbID   =$this->czsecurity->xssCleanPostInput('subID');
  			  $data1['items']      = $this->general_model->get_table_data('tbl_subscription_plan_item_qb',array('planID'=>$sbID)); 
      	      //$data1['items']      = $this->general_model->get_table_data('tbl_subscription_invoice_item',array('subscriptionID'=>$sbID,'invoiceDataID'=>0));
      	     $data1['rows']      = $this->general_model->get_num_rows('tbl_subscription_plan_item_qb',array('planID'=>$sbID));
      	     echo json_encode($data1);
      	     die;
    	}
	 
	
	 
	 
}

