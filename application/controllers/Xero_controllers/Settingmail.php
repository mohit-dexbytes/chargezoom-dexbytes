<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Settingmail extends CI_Controller
{

    protected $loginDetails;
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('general');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->model('general_model');
        $this->load->model('customer_model');
        $this->load->model('company_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '4') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
        $this->loginDetails = get_names();
    }

    public function index()
    {
        redirect('Integration/home', 'refresh');
    }

    public function reset_email_template(){
        if ($this->session->userdata('logged_in')) {
            $merchant_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchant_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');

        foreach ($templatedatas as $templatedata) {

            $insert_data = array(
                'templateName' => $templatedata['templateName'],
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
                'fromEmail'    => DEFAULT_FROM_EMAIL,
                'message'      => $templatedata['message'],
                'emailSubject' => $templatedata['emailSubject'],
                'createdAt'    => date('Y-m-d H:i:s'),
            );

            $emailCondition = [
                'templateType' => $templatedata['templateType'],
                'merchantID'   => $merchant_id,
            ];

            $emailData = $this->general_model->get_row_data('tbl_email_template', $emailCondition);
            if($emailData && !empty($emailData)){
                $this->general_model->update_row_data('tbl_email_template', $emailCondition, $insert_data);
            } else {
                $this->general_model->insert_row('tbl_email_template', $insert_data);
            }
        }

        $this->session->set_flashdata('success', 'Successfully Reset Templates');
        redirect('Xero_controllers/Settingmail/email_temlate');
    }

    public function create_template_data()
    {

        $login_info = $this->session->userdata('logged_in');
        $fromEmail  = $login_info['companyEmail'];
        $merchantID = $login_info['merchID'];

        $templatedatas = $this->general_model->get_table_data('tbl_email_template_data', '');

        foreach ($templatedatas as $templatedata) {

            $insert_data = array('templateName' => $templatedata['templateName'],
                'templateType'                      => $templatedata['templateType'],
                'merchantID'                        => '6',
                'fromEmail'                         => 'falkner076@gmail.com',
                'message'                           => $templatedata['message'],
                'emailSubject'                      => $templatedata['emailSubject'],
                'createdAt'                         => date('Y-m-d H:i:s'),
            );
            $this->general_model->insert_row('tbl_email_template', $insert_data);

        }

    }

    public function email_temlate()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $codition  = array('merchantID' => $user_id);
        $templates = $this->company_model->template_data_list($codition);

        $data['templates'] = prepareMailList($templates, $user_id);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/page_email', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function create_template()
    {

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id = $data['login_info']['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('templateName') != "") {

            $templateName = $this->czsecurity->xssCleanPostInput('templateName');

            $type      = $this->czsecurity->xssCleanPostInput('type');
            $fromEmail = $this->czsecurity->xssCleanPostInput('fromEmail');
            $toEmail   = $this->czsecurity->xssCleanPostInput('toEmail');
            $addCC     = $this->czsecurity->xssCleanPostInput('ccEmail');
            $addBCC    = $this->czsecurity->xssCleanPostInput('bccEmail');
            $replyTo   = $this->czsecurity->xssCleanPostInput('replyEmail');
            $message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
            $subject   = $this->czsecurity->xssCleanPostInput('emailSubject');
            $createdAt = date('Y-m-d H:i:s');
            $mailDisplayName = $this->czsecurity->xssCleanPostInput('mailDisplayName');

            if ($this->czsecurity->xssCleanPostInput('add_attachment')) {
                $add_attachment = '1';
            } else {
                $add_attachment = '0';
            }
            $message  = htmlspecialchars_decode($message);
            $insert_data = array('templateName' => $templateName,
                'templateType'                      => $type,
                'merchantID'                        => $user_id,
                'fromEmail'                         => $fromEmail,
                'toEmail'                           => $toEmail,
                'addCC'                             => $addCC,
                'addBCC'                            => $addBCC,
                'replyTo'                           => $replyTo,
                'message'                           => $message,
                'emailSubject'                      => $subject,
                'attachedTo'                        => $add_attachment,
                'mailDisplayName'                   => $mailDisplayName,
            );

            if ($this->czsecurity->xssCleanPostInput('tempID') != "") {
                $insert_data['updatedAt'] = date('Y-m-d H:i:s');
                $condition                = array('templateID' => $this->czsecurity->xssCleanPostInput('tempID'));

                $data['templatedata'] = $this->general_model->update_row_data('tbl_email_template', $condition, $insert_data, ['message']);

            } else {
                $insert_data['createdAt'] = date('Y-m-d H:i:s');
                $id                       = $this->general_model->insert_row('tbl_email_template', $insert_data, ['message']);

            }
            redirect('Xero_controllers/Settingmail/email_temlate', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['from_mail'] = DEFAULT_FROM_EMAIL;
        $data['mailDisplayName'] = $this->loginDetails['companyName'];

        if ($this->uri->segment('4')) {
            $temID = $this->uri->segment('4');
            $condition            = array('templateID' => $temID);
            $templatedata         = $this->general_model->get_row_data('tbl_email_template', $condition);
            $data['templatedata'] = $templatedata;
            if ($templatedata && !empty($templatedata['fromEmail'])) {
                $data['from_mail'] = $templatedata['fromEmail'];
			}
			
			if ($templatedata && !empty($templatedata['mailDisplayName'])) {
                $data['mailDisplayName'] = $templatedata['mailDisplayName'];
            }
        }

        $data['types'] = $this->general_model->get_table_data('tbl_teplate_type', '');

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/page_email_template', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function copy_template()
    {

        $temID        = $this->uri->segment('4');
        $condition    = array('templateID' => $temID);
        $templatedata = $this->general_model->get_row_data('tbl_email_template', $condition);

        $insert_data = array('templateName' => $templatedata['templateName'],
            'templateType'                      => $templatedata['templateType'],
            'merchantID'                        => $templatedata['merchantID'],
            'fromEmail'                         => $templatedata['fromEmail'],
            'toEmail'                           => $templatedata['toEmail'],
            'addCC'                             => $templatedata['addCC'],
            'addBCC'                            => $templatedata['addBCC'],
            'replyTo'                           => $templatedata['replyTo'],
            'message'                           => $templatedata['message'],
            'emailSubject'                      => $templatedata['emailSubject'],
            'attachedTo'                        => $templatedata['attachedTo'],
            'createdAt'                         => date('Y-m-d H:i:s'),

        );

        if ($this->general_model->insert_row('tbl_email_template', $insert_data)) {
            $this->session->set_flashdata('success', 'Successfully Copied');

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong></div>');
        }

        redirect('Xero_controllers/Settingmail/email_temlate', 'refresh');

    }

    public function delete_template()
    {

        $temID = $this->czsecurity->xssCleanPostInput('tempateDelID');
        if ($this->db->query("Delete from tbl_email_template where templateID = $temID")) {
            $this->session->set_flashdata('success', 'Success');

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }
        redirect('Xero_controllers/Settingmail/email_temlate', 'refresh');

    }

    public function view_template()
    {

        $temID     = $this->czsecurity->xssCleanPostInput('tempateViewID');
        $condition = array('templateID' => $temID);

        $view_data = $this->company_model->template_data($condition);

        ?>
		 <table class="table table-bordered table-striped table-vcenter"   >

            <tbody>

			<tr>
					<th class="text-left"><strong> Template Name</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['templateName']; ?></a></td>
			</tr>
		<tr>
					<th class="text-left"><strong> From Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['fromEmail']; ?></a></td>
			</tr>
          <tr>
					<th class="text-left"><strong>Cc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addCC']; ?></a></td>
			</tr>
           <tr>
					<th class="text-left"><strong>Bcc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addBCC']; ?></a></td>
			</tr>

            	<tr>
					<th class="text-left"><strong> Set to Reply</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['replyTo']; ?></a></td>
			</tr>
             <tr>
					<th class="text-left"><strong> Email Subject</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['emailSubject']; ?></a></td>
			</tr>
			<tr>
					<th class="text-left"><strong> Template Type</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['typeText']; ?></td>
			</tr>
			<tr>
					<th colspan="2" class="text-left"><strong> Message</strong></th>

			</tr>
            <tr>
					<td colspan="2" class="text-left msg_view"><?php echo $view_data['message']; ?></td>

			</tr>

			</tbody>
        </table>

      <?php
die;

    }

    public function set_template()
    {

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $companyID  = $this->czsecurity->xssCleanPostInput('companyID');
        $typeID     = $this->czsecurity->xssCleanPostInput('typeID');
        $customer   = $this->czsecurity->xssCleanPostInput('customer');

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));

        $condition = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data = $this->company_model->template_data($condition);

        $merchant_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchantEmail,companyName,merchantContact'), array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $comp_data['merchantID']));

        $currency = "$";

        $config_email  = $merchant_data['merchantEmail'];
        $merchant_name = ($merchant_data['companyName'] != null)?$merchant_data['companyName']:$this->loginDetails['companyName'];
        $logo_url      = $merchant_data['merchantProfileURL'];
        $mphone        = $merchant_data['merchantContact'];

        if(!$view_data['replyTo'] || empty($view_data['replyTo'])){
            $view_data['replyTo'] = $config_email;
        }

        if (!empty($config_data['ProfileImage'])) {
            $logo_url = $config_data['ProfileImage'];
        } else {
            $logo_url = CZLOGO;
        }

        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';

        $cardno        = '';
        $expired       = '';
        $expiring      = '';
        $friendly_name = '';
        $update_link   = $config_data['customerPortalURL'];
        $in_link       = $config_data['customerPortalURL'];
        $custCompany = '';
        $company = '';

        $condition1 = " and Customer_ListID='" . $customerID . "' and  companyID='" . $companyID . "' ";
        if ($this->czsecurity->xssCleanPostInput('invoiceID') != "") {

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
            $condition1 .= " and TxnID='" . $invoiceID . "' ";

            if ($typeID == '1' || $typeID == '3') {
              
                $condition1 .= " and   `IsPaid` = 'false' and userStatus=''  ";
            }
            if ($typeID == '2') {
             
                $condition1 .= " and   `IsPaid` = 'false' and userStatus=''  ";
            }
            if ($typeID == '5') {

                $condition1 .= " and   `IsPaid` = 'true' and userStatus=''  ";
            }
            if ($typeID == '4') {
              
                $condition1 .= " and   `IsPaid` = 'false' and userStatus=''  ";
            }

        } else {

            if ($typeID == '1' || $typeID == '3') {
                $condition1 .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus=''  ";
            }
            if ($typeID == '2') {
               
                $condition1 .= " and   `IsPaid` = 'false' and userStatus=''  ";
            }
            if ($typeID == '4') {
               
                $condition1 .= " and   `IsPaid` = 'false' and userStatus=''  ";
            }
            if ($typeID == '5') {
                $condition1 .= " and   `IsPaid` = 'true' and userStatus=''  ";
            }

            if ($typeID == '12' || $typeID == '11') {

                $card_data = $this->get_expiry_card_data($customerID, '1');

                if (!empty($card_data)) {
                    $cardno        = $card_data['CustomerCard'];
                    $friendly_name = $card_data['customerCardfriendlyName'];
                }
            }
        }

        $data = $this->customer_model->get_invoice_data_template($condition1);

        if (!empty($data)) {
            $customer   = $data['FullName'];
            $amount     = $data['AppliedAmount'];
            $balance    = $data['BalanceRemaining'];
            $paymethods = $data['paymentType'];

            $duedate    = date('F d, Y', strtotime($data['DueDate']));
            $ref_number = $data['RefNumber'];
            $tr_date    = date('F d, Y', strtotime($data['TimeCreated']));
            $custCompany = $c_data['companyName'];
        }

        $subject = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $view_data['emailSubject']));
        if ($view_data['emailSubject'] == "Welcome to {{company_name}}") {
            $subject = 'Welcome to ' . $company;
        }

        if ($typeID == '11') {

            $subject = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $view_data['emailSubject']));
        }

        $message = $view_data['message'];
        $message = stripslashes(str_ireplace('{{merchant_name}}', $merchant_name, $message));
       
        $message = stripslashes(str_ireplace('{{creditcard.mask_number}}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{creditcard.type_name}}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{creditcard.url_updatelink}}', $update_link, $message));

        $message = stripslashes(str_ireplace('{{customer.company}}', $custCompany, $message));
        $message = stripslashes(str_ireplace('{{transaction.currency_symbol}}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{transaction.amount}}', ($balance) ? ('$' . number_format($balance, 2)) : '0.00', $message));
        $message = stripslashes(str_ireplace('{{transaction.transaction_date}}', $tr_date, $message));
        $message = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $message));

        $message = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_ireplace('{{invoice.due_date|date("F j, Y")}}', $duedate, $message));
        $message = stripslashes(str_ireplace('{{invoice.days_overdue}}', $overday, $message));
        $message = stripslashes(str_ireplace('{{invoice.url_permalink}}', $in_link, $message));
        $message = stripslashes(str_ireplace('{{invoice_payment_pagelink}}', $in_link, $message));

        $message = stripslashes(str_ireplace('{{merchant_email}}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{current.date}}', $cur_date, $message));
        $message = stripslashes(str_ireplace('{{logo}}', "<img src='" . $logo_url . "'>", $message));

       
        $new_data_array                 = array();
        $new_data_array['message']      = $message;
        $new_data_array['emailSubject'] = $subject;
        $new_data_array['message']      = $message;
        $new_data_array['addCC']        = $view_data['addCC'];
        $new_data_array['addBCC']       = $view_data['addBCC'];
        $new_data_array['replyTo']      = $view_data['replyTo'];
        $new_data_array['fromEmail']      = ($view_data['fromEmail'] != null)?$view_data['fromEmail']:DEFAULT_FROM_EMAIL;
        $new_data_array['mailDisplayName']      = ( $view_data['mailDisplayName'] != null)? $view_data['mailDisplayName']:$this->loginDetails['companyName'];

        echo json_encode($new_data_array);
        die;
    }

    public function set_due_template()
    {

        $customerID = $this->czsecurity->xssCleanPostInput('customerID');
        $companyID  = $this->czsecurity->xssCleanPostInput('companyID');
        $typeID     = $this->czsecurity->xssCleanPostInput('typeID');
        $customer   = $this->czsecurity->xssCleanPostInput('customer');

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));

        $condition     = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data     = $this->company_model->template_data($condition);
        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchID' => $comp_data['merchantID']));

        $currency = "$";

        $config_email  = $merchant_data['merchantEmail'];
        $merchant_name = $merchant_data['companyName'];
        $logo_url      = $merchant_data['merchantProfileURL'];
        $mphone        = $merchant_data['merchantContact'];
        $cur_date      = date('Y-m-d');

        $currency = "$";

        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $update_link         = $config_data['customerPortalURL'];

        $data['login_info'] = $merchant_data;
        $company            = $merchant_data['companyName'];

        $condtion = " and Customer_ListID='" . $customerID . "' and  companyID='" . $companyID . "' ";
        if ($typeID == '1' || $typeID == '3') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus=''  ";
        }
        if ($typeID == '2') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus=''  ";
        }
        if ($typeID == '5') {
            $condtion .= " and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '" . date('Y-m-d') . "' AND `IsPaid` = 'false' and userStatus='' and  (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300' ";
        }
        if ($typeID == '12') {

            $card_data = $this->get_card_expiry_data($customerID, $companyID);
            if (!empty($card_data)) {
                $cardno        = $card_data['CustomerCard'];
                $friendly_name = $card_data['customerCardfriendlyName'];
            }
        }
        $data = $this->customer_model->get_invoice_data_template($condtion);

        if (!empty($data)) {
            $customer   = $data['FullName'];
            $amount     = $data['AppliedAmount'];
            $balance    = $data['BalanceRemaining'];
            $paymethods = $data['paymentType'];

            $duedate    = date('F d, Y', strtotime($data['DueDate']));
            $ref_number = $data['RefNumber'];
            
        }

        $subject = $view_data['emailSubject'];
        $subject = stripslashes(str_ireplace('{{invoice.refnumber}}', $ref_number, $subject));
        $subject = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $subject));
        $subject = stripslashes(str_ireplace('{{transaction.transaction_date}}', $duedate, $subject));
        $subject = stripslashes(str_ireplace('{{company_name}}', $cust_company, $subject));
        $message = $view_data['message'];

        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.mask_number }}', $cardno, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_ireplace('{{ creditcard.url_updatelink }}', $update_link, $message));
        $message = stripslashes(str_ireplace('{{ customer.company }}', $customer, $message));
        $message = stripslashes(str_ireplace('{{ transaction.currency_symbol }}', $currency, $message));
        $message = stripslashes(str_ireplace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_ireplace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));

        $message = stripslashes(str_ireplace(' {{ transaction.transaction_method }}', $transaction_details, $message));
        $message = stripslashes(str_ireplace('{{ transaction.transaction_date}}', $tr_data, $message));
        $message = stripslashes(str_ireplace('{{invoice_due_date}}', $duedate, $message));
        $message = stripslashes(str_ireplace('{{ transaction.transaction_detail }}', $transaction_details, $message));
        $message = stripslashes(str_ireplace('{{ invoice.days_overdue }}', $overday, $message));
        $message = stripslashes(str_ireplace('{{ invoice.refnumber }}', $ref_number, $message));
        $message = stripslashes(str_ireplace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_ireplace('{{ invoice.due_date|date("F j, Y") }}', $duedate, $message));

        $message = stripslashes(str_ireplace('{{ invoice.url_permalink }}', $update_link, $message));
        $message = stripslashes(str_ireplace('{{ merchant_email }}', $config_email, $message));
        $message = stripslashes(str_ireplace('{{ merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_ireplace('{{ current.date }}', $cur_date, $message));
        $message = stripslashes(str_ireplace('{{logo}}', "<div id='lg_div' ></div>", $message));

        $new_data_array                 = array();
        $new_data_array['message']      = $message;
        $new_data_array['emailSubject'] = $subject;
        $new_data_array['message']      = $message;
        $new_data_array['addCC']        = $view_data['addCC'];
        $new_data_array['addBCC']       = $view_data['addBCC'];

        $new_data_array['replyTo'] = $view_data['replyTo'];

        echo json_encode($new_data_array);
        die;
    }

    //-------------------   START -------------------------------//

    //---------- View email history ----------------------//

    public function get_history_id()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
        }

        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
        }

        $historyID    = $this->czsecurity->xssCleanPostInput('customertempID');
        $condition    = array('mailID' => $historyID);
        $historydatas = $this->general_model->get_row_data('tbl_template_data', $condition);

        if (!empty($historydatas)) {
            if(isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone'])){
                $timezone = ['time' => $historydatas['emailsendAt'], 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
                $historydatas['emailsendAt'] = getTimeBySelectedTimezone($timezone);
            }
            ?>


		 <table class="table table-bordered table-striped table-vcenter">

            <tbody>


		<tr>
					<th class="text-right col-md-1 control-label"><strong> To </strong> </th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailto']; ?></a> </td>
			</tr>
          <tr>
					<th class="text-right col-md-1 control-label"><strong>Cc </strong></th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailcc']; ?> </a> </td>
			</tr>
           <tr>
					<th class="text-right col-md-1 control-label"><strong>Bcc </strong></th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailbcc']; ?></td>
			</tr>

				 <tr>
					<th class="text-right col-md-1 control-label"><strong> Date</strong></th>
					<td class="text-left visible-lg col-md-11"> <?php echo date('M d, Y - h:i A', strtotime($historydatas['emailsendAt'])); ?> </a></td>
			</tr>

             <tr>
					<th class="text-right col-md-1 control-label"><strong>Subject</strong></th>
					<td class="text-left visible-lg col-md-11"> <?php echo $historydatas['emailSubject']; ?> </a></td>
			</tr>




            <tr>
					<td colspan="2" class="text-left"><?php echo $historydatas['emailMessage']; ?></td>

			</tr>

			</tbody>
        </table>

      <?php }die;

    }

    public function send_mail()
    {
        if ($this->session->userdata('logged_in')) {

            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $user_id));
        $logo_url      = $merchant_data['merchantProfileURL'];
        
        $this->load->library('email');

        $type      = $this->czsecurity->xssCleanPostInput('type');
        $fromEmail = $this->czsecurity->xssCleanPostInput('fromEmail');
        $toEmail   = $this->czsecurity->xssCleanPostInput('toEmail');
        $addCC     = $this->czsecurity->xssCleanPostInput('ccEmail');
        $addBCC    = $this->czsecurity->xssCleanPostInput('bccEmail');
        $replyTo   = $this->czsecurity->xssCleanPostInput('replyEmail');
        $message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
        $subject   = $this->czsecurity->xssCleanPostInput('emailSubject');
        $message   = stripslashes(str_replace('{{logo}}', "<img src='$logo_url'>", $message));
        $message   = stripslashes(str_replace('<div id="lg_div">&nbsp;</div>', "<img src='$logo_url'>", $message));

        $date       = date('Y-m-d H:i:s');
        $customerID = $this->czsecurity->xssCleanPostInput('customertempID');

        $email_data = array('customerID' => $customerID,
            'merchantID'                     => $user_id,
            'emailSubject'                   => $subject,
            'emailfrom'                      => $fromEmail,
            'emailto'                        => $toEmail,
            'emailcc'                        => $addCC,
            'emailbcc'                       => $addBCC,
            'emailreplyto'                   => $replyTo,
            'emailMessage'                   => $message,
            'emailsendAt'                    => $date,

        );

        $this->email->clear();
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($fromEmail, $merchant_data['companyName']);
        $this->email->to($toEmail);
        $this->email->subject($subject);
        $this->email->message($message);

        if (isset($_FILES['mail_attachment']) && !empty($_FILES['mail_attachment']['name'])) {
            $config['upload_path']   = 'uploads/mail_attachment/';
            $config['allowed_types'] = '*';
            $config['file_name']     = time() . $_FILES['mail_attachment']['name'];
            $config['new_image']     = 'uploads/mail_attachment/';

            //Load upload library and initialize configuration
            $this->load->library('upload', $config);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('mail_attachment')) {
                $uploadData = $this->upload->data();
                $file       = "uploads/mail_attachment/" . $uploadData['file_name'];
                $this->email->attach($file);
                $email_data['mail_attachment'] = FILEURL . $file;
            }
        }

      
        if ($this->email->send()) {

            $this->general_model->insert_row('tbl_template_data', $email_data);
            $this->session->set_flashdata('success', 'Success');

        } else {

            $this->general_model->insert_row('tbl_template_data', $email_data);
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }
        redirect('Xero_controllers/Customer/view_customer/' . $customerID, 'refresh');
    }

//-------------------   END -------------------------------//

    public function get_expiry_card_data($customerID, $type)
    {

        $card = array();
        $this->load->library('encrypt');
        $db_user = "devcz_quicktest";
        $db_pass = "ecrubit@123";
        $db_name = "devcz_chargezoom_card_db";

        $dsn = 'mysqli://' . $db_user . ':' . $db_pass . '@' . 'localhost' . '/' . $db_name;
        $this->db1 = $this->load->database($dsn, true);
        if ($type == '1') {
            /**************Expired Card***********/

            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  ";
        }

        if ($type == '0') {

            /**************Expiring SOON Card***********/
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";

        }
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();

        if (!empty($card_data)) {

            $card['CustomerCard']             = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['expiry']                   = $card_data['expired_date'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
            $card['customerListID']           = $card_data['customerListID'];
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];

        }

        return $card;

    }

}
