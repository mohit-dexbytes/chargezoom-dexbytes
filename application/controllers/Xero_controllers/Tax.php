<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tax extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		
		 if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='4')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	   	redirect('Integration/home','refresh'); 
	}
	
  public function tax_list()
	{   
	    define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
	    $user_id    = $data['login_info']['merchID'];
		$merchID = $user_id;	 
		
		$con = array(
			'merchantID' => $merchID,
		);
		
        if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {
		    
		     
	    $xero_data = $this->general_model->get_row_data('tbl_xero_token',$con);
		
    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
        
        $response = $XeroOAuth->request('GET', $XeroOAuth->url('TaxRates', 'core'), array());
        
        
        if ($XeroOAuth->response['code'] == 200) {
         	 $taxes = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
         	 
         	  $taxs_arr = json_decode( json_encode($taxes) , 1);
         	  
         	    
         	    
         	   foreach ($taxs_arr['TaxRates'] as $tax) {
         	       
         	       foreach ($tax as $taxdata) {
         	           
         	           
         	      
         	           
 	           $taxID = 	$xero_tax_details['taxID'] = $this->db->escape_str($taxdata['TaxType']);
		        $xero_tax_details['friendlyName'] = $this->db->escape_str($taxdata['Name']);
		        $xero_tax_details['componentName'] = $this->db->escape_str($taxdata['TaxComponents']['TaxComponent']['Name']);
		        $xero_tax_details['taxRate'] = $this->db->escape_str($taxdata['TaxComponents']['TaxComponent']['Rate']);
		        $xero_tax_details['merchantID'] = $merchID;
		        $xero_tax_details['Status'] = $this->db->escape_str($taxdata['Status']);
		        
		         $condition = array('merchantID' => $merchID,
		                             'taxID' => $taxID
		            );
		         $res = $this->general_model->get_row_data('tbl_taxes_xero', $condition); 
		          
		          if(!empty($res))
		            $this->general_model->update_row_data('tbl_taxes_xero',$condition, $xero_tax_details);
		          else
		            $this->general_model->insert_row('tbl_taxes_xero', $xero_tax_details);
         	       }
         	       
         	   }
         	 
         	
         }
		    
		}
		    $condition =  array('merchantID'=>$merchID);
		    
	    
			$taxname = $this->general_model->get_table_data('tbl_taxes_xero',$condition);
			
			$data['taxes'] = $taxname;
			
	     
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/taxes', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	


	 public function create_taxes()
 
	{
	    
	  
	  if(!empty($this->input->post(null, true))){
	 	
	 	define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		
		
		
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
	    	
	    
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );


	   $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('friendlyName', 'Friendly Name', 'required|xss_clean');
		$this->form_validation->set_rules('taxRate', 'Tax Rate', 'required|xss_clean');
		
		if ($this->form_validation->run() == true)
		{
			$data['login_info'] = $this->session->userdata('logged_in');
		    $user_id    = $data['login_info']['merchID'];
			$merchID = $user_id;
			$val = array(
				'merchantID' => $merchID,
			);
			$condition =  array('merchantID'=>$merchID);

			

			$input_data['friendlyName']	   = $this->czsecurity->xssCleanPostInput('friendlyName');
			$input_data['taxRate']	   = $this->czsecurity->xssCleanPostInput('taxRate');
			$input_data['taxComponent']	   = $this->czsecurity->xssCleanPostInput('taxComponent');
			$input_data['date'] = date('Y-m-d');
			

				if($this->czsecurity->xssCleanPostInput('taxID')!="" )
				{
					    
				      
			        $id = $this->czsecurity->xssCleanPostInput('taxID');

					if ($checkErrors > 0) {
					    
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
					foreach ( $initialCheck as $check ) {
						echo 'Error: ' . $check . PHP_EOL;
						}
					} 
                else {

					$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
					
			    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			         $tax = '<Response>
						  <TaxRate>
						  <TaxType>'.$id.'</TaxType>
						     <Name>'.$input_data['friendlyName'].'</Name>
						    <TaxComponents>
						      <TaxComponent>
						        <Name>'.$input_data['taxComponent'].'</Name>
						        <Rate>'.$input_data['taxRate'].'</Rate>
						        <IsCompound>false</IsCompound>
						        <IsNonRecoverable>false</IsNonRecoverable>
						      </TaxComponent>
						    </TaxComponents>
						  </TaxRate>
						</Response>';
						
            $response = $XeroOAuth->request('POST', $XeroOAuth->url('TaxRates', 'core'), array(), $tax);
	         if ($XeroOAuth->response['code'] == 200) 
             {
	         		$this->session->set_flashdata('success', 'Successfully Updated');	
	         }else{
	         	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Friendlyname already Exists.</div>');	
					}
	         }
            
				}
					else
					{
					
				if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
					foreach ( $initialCheck as $check ) {
						echo 'Error: ' . $check . PHP_EOL;
						}
					} else {

					
				  	$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
				  	
			 	    $XeroOAuth->config['access_token']  =   $xero_data['accessToken'];
			        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
		           
		         
			        $tax = '<Response>
						  <TaxRate>
						    <Name>'.$input_data['friendlyName'].'</Name>
						    <TaxComponents>
						      <TaxComponent>
						        <Name>'.$input_data['taxComponent'].'</Name>
						        <Rate>'.$input_data['taxRate'].'</Rate>
						        <IsCompound>false</IsCompound>
						        <IsNonRecoverable>false</IsNonRecoverable>
						      </TaxComponent>
						    </TaxComponents>
						  </TaxRate>
						</Response>';
						
              $response = $XeroOAuth->request('POST', $XeroOAuth->url('TaxRates', 'core'), array(), $tax);
            
          
	         if ($XeroOAuth->response['code'] == 200) {
	         	  $this->session->set_flashdata('success', 'Your Information Stored successfully');	
	         }else{
	         	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Friendlyname already Exists.</div>');	
				print_r($response);die;
	             
	         }		
		
          	
            }					 
					 
				}
				
				redirect(base_url('Xero_controllers/Tax/tax_list'));
		
		}
		
	}
			
 }
 
 
	public function get_tax_id()
    {
		
        $id = $this->czsecurity->xssCleanPostInput('taxID');
		$val = array(
		'taxID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_taxes_xero',$val);
        echo json_encode($data);
    }
	
	
	
public function delete_tax(){
	
	
	 	$id = $this->czsecurity->xssCleanPostInput('tax_id');
	 
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];
		$merchID = $user_id;
		$val = array(
			'merchantID' => $merchID,
		);

		define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
					foreach ( $initialCheck as $check ) {
						echo 'Error: ' . $check . PHP_EOL;
						}
					} else {

					$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
			    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			         $tax = '<Response>
						  <TaxRate>
						  <TaxType>'.$id.'</TaxType>
						     <Status>DELETED</Status>
						  </TaxRate>
						</Response>';
						
            $response = $XeroOAuth->request('POST', $XeroOAuth->url('TaxRates', 'core'), array(), $tax);
	         if ($XeroOAuth->response['code'] == 200) {
	         	 	$this->session->set_flashdata('success', 'Successfully Deleted');             	
	         }else{
	         	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Somthing Is Wrong.</div>');
				}		

	  	redirect(base_url('Xero_controllers/Tax/tax_list'));
    	 
		}
	
}
	
	
	
}