<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('Xero_models/xero_company_model');
		 $this->db1 = $this->load->database('otherdb', TRUE);
		
		 if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='4')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	   	redirect('Integration/home','refresh'); 
	}
	
	public function reports()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
		$data['startdate'] ='';
		$data['report_type'] ='1';
	
	
    	$condition 			 	= array("inv.merchantID"=>$user_id);
		$invoices    			= $this->xero_company_model->get_invoice_data_by_due($condition);  
 		$data['report1'] 	    = $invoices; 					
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_reports', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	

 
	public function transaction_reports()
	{
	   
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
			$today 			    = date('Y-m-d');
		$data['report_type']	= $this->czsecurity->xssCleanPostInput('report_type');	
	   	$data['startdate'] ='';
	   
	    if($this->czsecurity->xssCleanPostInput('report_type')=='1' ){
		
     	$condition 			 	= array("inv.merchantID"=>$user_id);
		$invoices    			= $this->xero_company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	
	  }		
			
	else if($this->czsecurity->xssCleanPostInput('report_type')=='2'){
		
     	$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,  "inv.merchantID"=>$user_id);   
	
	    $invoices    			 = $this->xero_company_model->get_invoice_data_by_due($condition1);  		
		 $data['report2']	   	 = 	$invoices;
		 
		
   	
	}
	
	
			
		else if($this->czsecurity->xssCleanPostInput('report_type')=='3'){
		
		$invoices    			 = $this->xero_company_model->get_invoice_data_by_past_due($user_id); 
		$data['report3']		 = 	$invoices;
	 
	
	}
	
	
		
		else if($this->czsecurity->xssCleanPostInput('report_type')=='4'){
		$invoices    			 = $this->xero_company_model->get_invoice_data_by_past_time_due($user_id); 
		 $data['report4']		 = 	$invoices;
	
	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='5'){
		
		$invoices    			 = $this->xero_company_model->get_transaction_failure_report_data($user_id); 
	
	
	
		$data['report5']		 = 	$invoices;

	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='6'){
		 $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->get_credit_card_info($user_id);
		   foreach($card_data as $key=> $card){
		       
		         $condition      =  array('Customer_ListID'=>$card['customerListID'],'merchantID'=>$user_id);
			     $customer_data  = $this->general_model->get_row_data('Xero_custom_customer',$condition);
			     $customer_card['Customer_ListID']  = $customer_data['Customer_ListID'] ;
                 $customer_card['CardNo']  = substr($this->encrypt->decode($card['CustomerCard']),12) ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['Contact']  = $customer_data['userEmail'] ;
				 $customer_card['fullName']  = $customer_data['fullName'] ;
				 $customer_card['companyName']  = $customer_data['companyName'] ;
			     $report[$key] = $customer_card;
            		   
		   }
		
    	
			$data['report6']		 = 	$report;
			
	}
	
	else if($this->czsecurity->xssCleanPostInput('report_type')=='8' ||  $this->czsecurity->xssCleanPostInput('report_type')=='9' || $this->czsecurity->xssCleanPostInput('report_type')=='10'  ){
	
    	$type					 =	$this->czsecurity->xssCleanPostInput('report_type');
		
	   if($type=='10'){
	   $invoices   				 = 	 $this->xero_company_model->get_invoice_upcomming_data($user_id);
	   $data['report10']		 = 	$invoices;
	   }else{
		$invoices    			 = $this->xero_company_model->get_invoice_data_open_due($user_id, $type); 
		  $data['report89']		 = 	$invoices;
		}
		
	}
	
	
else	if($this->czsecurity->xssCleanPostInput('report_type')=='7'){
	
	 $startdate = date('Y-m-d',strtotime($this->czsecurity->xssCleanPostInput('startDate')));
     $enddate   = date('Y-m-d',strtotime($this->czsecurity->xssCleanPostInput('endDate')));
	 $data['startdate']   = date('m/d/Y', strtotime($startdate));
	 $data['enddate']     =  date('m/d/Y', strtotime($enddate)); ;
	    $invoices   				 = 	 $this->xero_company_model->get_transaction_report_data($user_id,$startdate,  $enddate);
	  $data['report7']				 = 	$invoices;
	
	}else{
		$condition 			 	= array("inv.merchantID"=>$user_id);
		$invoices    			= $this->xero_company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	
	
	}
	    $this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_reports', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	
	
 }	
	
 
 	public function export_csv()
	{
	$delimiter = ","; 
	$newline = "\r\n";
	$enclosure = '"';
	
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}

		$today 			        = date('Y-m-d');
     $condition 			 	= array("inv.merchantID"=>$user_id);
	$this->load->dbutil(); // call db utility library
	$this->load->helper('download'); // call download helper
	
      $type = $this->uri->segment(4);  
	 
	
    if($type=='1' || $type=='2' ){
	
        $condition 		    =  array("inv.merchantID"=>$user_id);
		$condition1 		= array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,  "inv.merchantID"=>$user_id); 
		
		$this->db->select(" `cust`.companyName, `cust`.fullName, sum(inv.BalanceRemaining) as balance, `cust`.userEmail, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status ,  ");
		$this->db->from('Xero_test_invoice inv ');
		$this->db->join('Xero_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');

        $this->db->group_by('inv.CustomerListID');
		
		$this->db->order_by('balance','desc');
	    if($type=='1'){
	    $this->db->where($condition);
		}else if($type=='2'){
		$this->db->where($condition1);
		}  
		
		$query = $this->db->get();
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
		 
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
		$a2=array("Company", "Customer Name", "Amount", "Email", "Status", );
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
				  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	}	
   if($type=='3' ){	
	  
		$query  =  $this->db->query("SELECT `inv`.RefNumber, cust.fullName, cust.userEmail, inv.DueDate, `inv`.BalanceRemaining, `inv`.TimeCreated,
		(case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
		when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` != 0 then 'Success'
		else 'Canceled' end ) as status 
		FROM Xero_test_invoice inv 
		INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
		WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != 0 and 
	  
		`inv`.`merchantID` = '$user_id' order by  BalanceRemaining   desc limit 10 ");
		
		
		$result_set = $query->result_array(); 
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
		 
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
		$a2=array("TxnID", "Customer Name", "Email",  "Due Date", "Amount", "Time Created", "Status");
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
				  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	  
	}
	 if($type=='4' ){
	 

	  $query  =  $this->db->query("SELECT `inv`.RefNumber, cust.fullName, cust.userEmail, inv.DueDate,  `inv`.BalanceRemaining, `inv`.TimeCreated,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0  then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0   and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0   then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Xero_test_invoice inv 
	  	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  AND `BalanceRemaining` != 0 and 
	  
	  `inv`.`merchantID` = '$user_id' order by  DueDate asc limit 10 "); 
	  
	  $result_set = $query->result_array(); 
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
		foreach($result_set as $value1)
  
		$keys = array_keys($value1);
		$a2=array("Txn_ID", "Customer", "Email", "Due Date", "Amount", "Time Created", "Status");
		$final_keys = array_replace($keys, $a2); 
		unset($final_keys[0]);
		
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			unset($values[0]);	  
			 
			 
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		}
	  
	
	}
	if($type=='5'){
	  
		 $sql ="SELECT `tr`.`id`, `tr`.`transactionID`, `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`, (select RefNumber from Xero_test_invoice where invoiceID = tr.invoiceID ) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `Xero_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`  WHERE `cust`.`merchantID` = '$user_id' AND transactionCode NOT IN ('200','1','100')";
		$query = $this->db->query($sql); 
		
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
        	 
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
	$a2=array("ID","Transaction ID", "Amount", "Transaction Date", "Type", "Invoice ID", "Customer");
		$final_keys = array_replace($keys, $a2); 
		unset($final_keys[0]);
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			unset($values[0]);	  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	
	}

   if($type=='6'){
   
	   	 
		 $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->get_credit_card_info_data($user_id);
		   
		  
		    foreach($card_data as $key=> $card){
		       
		         $condition      =  array('Customer_ListID'=>$card['customerListID'],'merchantID'=>$user_id);
			     $customer_data  = $this->general_model->get_row_data('Xero_custom_customer',$condition);
			     $customer_card['FullName']  = $customer_data['fullName'] ;
			      $customer_card['companyName']  = $customer_data['companyName'] ;
			     $customer_card['Contact']  = $customer_data['userEmail'] ;
                 $customer_card['CardNo']  = substr($this->encrypt->decode($card['CustomerCard']),12) ;
                  $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
          
			     $report[$key] = $customer_card;
            		   
		   }
		
		  $result_set =  $report;
		  
		  
        header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w');
		
		
             
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
		$a2=array( "Customer", "Company", "Email", "Card ", "Card Frindly Name", "Expiry");
		$final_keys = array_replace($keys, $a2); 
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 

		 
	      
	
	}
	
	 if($type=='7'){
   
	   	  $minvalue = $this->uri->segment(5);  
	      $maxvalue = $this->uri->segment(6);  
	
	      $trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.transactionID, (select RefNumber from Xero_test_invoice where  invoiceID= tr.invoiceID ) as RefNumber, cust.fullName,  tr.transactionAmount, tr.transactionDate, tr.transactionStatus 
			');
		$this->db->from('customer_transaction tr');
		$this->db->join('Xero_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
	    $this->db->where('cust.merchantID ', $user_id);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue); 
		
		$query = $this->db->get();
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	    
	     
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
		$a2=array("TxnID ", "Invoice ID ","FullName",  "Amount", "Date", "Remarks");
		
		$final_keys = array_replace($keys, $a2); 
		
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	
	
	}
	
	
	
  	
	 if($type=='10'){
	  $query = $this->db->query("SELECT inv.RefNumber,`cust`.fullName,  inv.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0  and userStatus ='' then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	   FROM Xero_test_invoice inv 
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and  
	  
	  `inv`.`merchantID` = '$user_id'  order by   DueDate  asc limit 10 ");
		
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
    		 
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
        
		$a2=array("Invoice","Full Name", "Paid Amount", "Balance", "Due Date",);
		$final_keys = array_replace($keys, $a2); 
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
	   }
	   
	   if($type=='8' || $type=='9'){
	   $con = '';
		if($type=='8')
		{
			  $con.="and `inv`.BalanceRemaining !='0.00' and `inv`.`IsPaid` = 'false' ";
		}
	  $query  =  $this->db->query("SELECT  inv.RefNumber, cust.fullName,  (`inv`.Total_payment - `inv`.BalanceRemaining)  as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining , `inv`.TimeCreated ,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0   then 'Success'
	  else 'Canceled' end ) as status 


	  FROM Xero_test_invoice inv 
	  INNER JOIN `Xero_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   	  
	  `inv`.`merchantID` = '$user_id'  $con  order by  DueDate   asc");
		
		$result_set = $query->result_array();
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=reportexport.csv');
		$output = fopen('php://output', 'w'); 
	
    		 
	    $keys = array();
        if(!empty($result_set)){	
		foreach($result_set as $value1)
		$keys = array_keys($value1);
        }
		$a2=array("Invoice ID", "Full Name", "Paid", "Due Date", "Balance",   "Added At","Status");
		$final_keys = array_replace($keys, $a2); 
		fputcsv($output, $final_keys);
  
		foreach($result_set as $value1){
			$values = array_values($value1);  
			$final_array = array_combine($final_keys, $values);
			fputcsv($output, $final_array);
		} 
  } 
	

 
		 
	}
 
 
	public function report_details_pdf()
	{
	    $data['template'] 		= template_variable();
		if($this->session->userdata('logged_in')){
		
			$data['login_info']	    = $this->session->userdata('logged_in');
			$user_id			    = $data['login_info']['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
		
		$data['login_info']	    = $this->session->userdata('user_logged_in');
		$user_id			    = $data['login_info']['merchantID'];
		}
	    $type = $this->uri->segment(4);  
		 
			$today 			    = date('Y-m-d');
		
	   
			if($type=='1' ){
			
			$condition 			 	= array("inv.merchantID"=>$user_id);
			$invoices    			= $this->xero_company_model->get_invoice_data_by_due($condition);   	
			//print_r($invoices); die;
			$data['report1']		= 	$invoices;
		     $data['type']		= 	$type;
		}		
			
		else if($type=='2'){
			$condition1 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,  "inv.merchantID"=>$user_id);   
		
			$invoices    			 = $this->xero_company_model->get_invoice_data_by_due($condition1);  		
			$data['report2']	   	 = 	$invoices;
			  $data['type']		= 	$type;
		}		
		else if($type=='3'){
		
		$invoices    			 = $this->xero_company_model->get_invoice_data_by_past_due($user_id); 
		$data['report3']		 = 	$invoices;
	  $data['type']		= 	$type;
	
	}
	
	
		
		else if($type=='4'){
		$invoices    			 = $this->xero_company_model->get_invoice_data_by_past_time_due($user_id); 
		 $data['report4']		 = 	$invoices;
	     $data['type']		= 	$type;
	}
	
	else if($type=='5'){
		
		$invoices    			 = $this->xero_company_model->get_transaction_failure_report_data($user_id); 
		$data['report5']		 = 	$invoices;
         $data['type']		= 	$type;
	}
	
	else if($type=='6'){
		   $this->load->library('encrypt');
		       $report=array();
		   $card_data =  $this->get_credit_card_info($user_id);
	      //$this->encrypt->decode();
		   foreach($card_data as $key=> $card){
		       
		       
            		   
		         $condition      =  array('Customer_ListID'=>$card['customerListID']);
			     $customer_data  = $this->general_model->get_row_data('Xero_custom_customer',$condition);
						     $customer_card['ListID']  = $customer_data['Customer_ListID'] ;
                 $customer_card['CardNo']  = substr($this->encrypt->decode($card['CustomerCard']),12) ;
				 $customer_card['expired_date'] = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['Contact']  = $customer_data['userEmail'] ;
				 $customer_card['FullName']  = $customer_data['fullName'] ;
				 $customer_card['companyName']  = $customer_data['companyName'] ;
			     $report[$key] = $customer_card;
            	 
	
            	
		   }
		   
		  
			$data['report6']		 = 	$report;
		     $data['type']		= 	$type;
	}
	
	else if($type=='8' ||  $type=='9' || $type=='10'  ){
	
    	
		
	   if($type=='10'){
	   $invoices   				 = 	 $this->xero_company_model->get_invoice_upcomming_data($user_id);
	   $data['report10']		 = 	$invoices;
	    $data['type']		= 	$type;
	   }else{
		$invoices    			 = $this->xero_company_model->get_invoice_data_open_due($user_id, $type); 
		  $data['report89']		 = 	$invoices;
		   $data['type']		= 	$type;
		}
		
	}
   else	if($type=='7'){
	     $startdate = $this->uri->segment(5);  
	     $enddate   = $this->uri->segment(6); 
	
	  $invoices   				 = 	 $this->xero_company_model->get_transaction_report_data($user_id,$startdate,  $enddate);
	  $data['report7']				 = 	$invoices;
	   $data['type']		= 	$type;
	
	}else{
		$condition 			 	= array("inv.merchantID"=>$user_id);
		$invoices    			= $this->xero_company_model->get_invoice_data_by_due($condition);   	
		
		$data['report1']		= 	$invoices;
	     $data['type']		= 	$type;
	
	}
	
		
	     	$no = 'Report'.$type;
			$pdfFilePath = "$no.pdf"; 
			
			 ini_set('memory_limit','32M'); 
			
			$html= $this->load->view('Xero_views/page_report_pdf', $data,true);
			
			 $this->load->library('pdf');
			 $pdf = $this->pdf->load();
			 $pdf->WriteHTML($html); // write the HTML into the PDF
			 $pdf->Output($pdfFilePath, 'D'); // save to file because we can
  
      
	}
	
	
	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
	      
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID){
	
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
		
	
}



