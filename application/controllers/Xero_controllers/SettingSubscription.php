<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class SettingSubscription extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		include APPPATH . 'third_party/Xero.php';
		include APPPATH . 'third_party/nmiDirectPost.class.php';

		include APPPATH . 'third_party/nmiCustomerVault.class.php';

	
		$this->load->model('customer_model');


		$this->load->model('Xero_models/xero_subscription_model');
		$this->load->model('Xero_models/xero_model');
		$this->load->model('Xero_models/xero_customer_model');
		$this->load->model('Xero_models/xero_company_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');

		$this->db1 = $this->load->database('otherdb', TRUE);

		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '4') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
	}


	public function index()
	{
		redirect('Integration/home', 'refresh');
	}


	public function status()
	{

		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		}

		$val = array(
			'merchantID' => $merchID,
		);

		$invoiceID = $this->czsecurity->xssCleanPostInput('subscID');

		$in_data   =    $this->xero_model->get_fb_invoice_data_pay($invoiceID);
		
		$amount    =	 $in_data['BalanceRemaining'];
		$Customer_ListID = $in_data['Customer_ListID'];

		$transaction = array();
		$transaction['transactionID']       = 'N/A';
		$transaction['transactionStatus']    = 'Offline Payment';
		$transaction['transactionDate']     = date('Y-m-d h:i:s');
		$transaction['transactionCode']     = '';
		$transaction['transactionType']    = "Offline Payment";
		$transaction['customerListID']       = $in_data['Customer_ListID'];
		$transaction['transactionAmount']   = $amount;
		$transaction['merchantID']   = $merchID;



		$ispaid 	 = 'true';
		$status = $this->czsecurity->xssCleanPostInput('status');

		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			$PayDate = date('Y-m-d');

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];



			$payment = '<Payment>
							  <Invoice>
							    <InvoiceID>' . $invoiceID . '</InvoiceID>
							  </Invoice>
							  <Account>
							    <Code>2500</Code>
							  </Account>
							  <Date>' . $PayDate . '</Date>
							  <Amount>' . $amount . '</Amount>
							</Payment>';


			$response = $XeroOAuth->request('POST', $XeroOAuth->url('Payments', 'core'), array(), $payment);
			if ($XeroOAuth->response['code'] == 200) {
				$this->session->set_flashdata('success', 'Success');
				$CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
				$id = $this->general_model->insert_row('customer_transaction',   $transaction);
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
			}
		}
		redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'), 'refresh');
	}



	public function subscriptions()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$data['merchantID']     =  $user_id;

		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['subscriptions']   = $this->xero_subscription_model->get_subscriptions_plan_data($user_id);
		$data['subs_data']        = $this->xero_customer_model->get_total_subscription($user_id);


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_subscriptions', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function create_subscription()
	{
		//Show a form here which collects someone's name and e-mail address

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$da 	= $this->session->userdata('logged_in');

				$user_id 				= $da['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$da 	= $this->session->userdata('user_logged_in');

				$user_id 				= $da['merchantID'];
			}
			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];

				if ($this->czsecurity->xssCleanPostInput('tax_check') != "") {
					$insert_row['itemTax']      =  $this->czsecurity->xssCleanPostInput('tax_check')[$key];
				} else {
					$insert_row['itemTax'] = '0';
				}

				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				if ($this->czsecurity->xssCleanPostInput('onetime_charge')[$key] && $this->czsecurity->xssCleanPostInput('onetime_charge')[$key] != 'Recurring') {

					$insert_row['oneTimeCharge'] = '1';
				} else {
					$insert_row['oneTimeCharge'] = '0';
				}


				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$sname      = $this->czsecurity->xssCleanPostInput('sub_name');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$plan        = $this->czsecurity->xssCleanPostInput('duration_list');
			if ($plan > 0) {
				$subsamount  = $total / $plan;
			} else {
				$subsamount  = $total;
			}
			$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
			$invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
			$st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
			$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');
			$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
			$phone      = $this->czsecurity->xssCleanPostInput('phone');
			$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
			$planid     = $this->czsecurity->xssCleanPostInput('sub_plan');

			$baddress1    = $this->czsecurity->xssCleanPostInput('baddress1');
			$baddress2    = $this->czsecurity->xssCleanPostInput('baddress2');
			$bcity        = $this->czsecurity->xssCleanPostInput('bcity');
			$bstate       = $this->czsecurity->xssCleanPostInput('bstate');
			$bphone       = $this->czsecurity->xssCleanPostInput('bphone');
			$bcountry    = $this->czsecurity->xssCleanPostInput('bcountry');
			$bzipcode    = $this->czsecurity->xssCleanPostInput('bzipcode');


			if ($this->czsecurity->xssCleanPostInput('gateway_list') != '') {
				$paygateway = $this->czsecurity->xssCleanPostInput('gateway_list');
			} else {
				$paygateway = 0;
			}

			if ($this->czsecurity->xssCleanPostInput('taxes') != '') {
				$taxval = $this->czsecurity->xssCleanPostInput('taxes');
			} else {
				$taxval = 0;
			}
			$end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));
			if ($this->czsecurity->xssCleanPostInput('autopay')) {

				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$card = array();
					$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
					$card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
					$card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
					$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
					$card['cvv']         = ''; 
					$card['friendlyname'] = $friendlyName;
					$card['customerID']  = $customerID;
					$card['Billing_Addr1']     = $this->czsecurity->xssCleanPostInput('baddress1');
					$card['Billing_Addr2']     = $this->czsecurity->xssCleanPostInput('baddress2');
					$card['Billing_City']     = $this->czsecurity->xssCleanPostInput('bcity');
					$card['Billing_State']     = $this->czsecurity->xssCleanPostInput('bstate');
					$card['Billing_Contact']     = $this->czsecurity->xssCleanPostInput('bphone');
					$card['Billing_Country']     = $this->czsecurity->xssCleanPostInput('bcountry');
					$card['Billing_Zipcode']     = $this->czsecurity->xssCleanPostInput('bzipcode');
					
					$card_data = $this->card_model->check_friendly_name($customerID, $friendlyName);
					if (!empty($card_data)) {
						$this->card_model->update_card($card, $customerID, $friendlyName);
						$cardID  = $card_data['CardID'];
					} else {


						$cardID = $this->card_model->insert_new_card($card);
					}
				}else if ($this->czsecurity->xssCleanPostInput('acc_number') != "") {
					$card = array();
					$acc_number   		= $this->czsecurity->xssCleanPostInput('acc_number');
					$route_number 		= $this->czsecurity->xssCleanPostInput('route_number');
					$acc_name     		= $this->czsecurity->xssCleanPostInput('acc_name');
					$secCode      		= 'WEB';
					$acct_type        	= $this->czsecurity->xssCleanPostInput('acct_type');
					$acct_holder_type 	= $this->czsecurity->xssCleanPostInput('acct_holder_type');
					
					$friendlyName = 'Checking - ' . substr($acc_number, -4);

					$insert_array =  array(
						'accountNumber'  => $acc_number,
						'routeNumber'	 => $route_number,
						'accountName' => $acc_name,
						'secCodeEntryMethod'      =>$secCode,
						'accountType'      => $acct_type,
						'accountHolderType'      => $acct_holder_type,
						'CardType' => 'Echeck',
						'Billing_Addr1'	 => $this->czsecurity->xssCleanPostInput('baddress1'),
						'Billing_Addr2'	 => $this->czsecurity->xssCleanPostInput('baddress2'),
						'Billing_City'	 => $this->czsecurity->xssCleanPostInput('bcity'),
						'Billing_State'	 => $this->czsecurity->xssCleanPostInput('bstate'),
						'Billing_Country'	 => $this->czsecurity->xssCleanPostInput('bcountry'),
						'Billing_Contact'	 => $this->czsecurity->xssCleanPostInput('bphone'),
						'Billing_Zipcode'	 => $this->czsecurity->xssCleanPostInput('bzipcode'),
						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName,
						'createdAt' 	=> date("Y-m-d H:i:s"),
						
					);
					$con  = array(
						'customerListID' => $customerID,
						'merchantID'     => $user_id,
						'customerCardfriendlyName' => $friendlyName
					);

					$card_data = $this->card_model->chk_card_firendly_name($customerID, $friendlyName);


					if ($card_data > 0) {
						$cardID = $this->card_model->update_card_data($con, $insert_array);
						$cardID  = $card_data['CardID'];
					} else {

						$cardID = $this->card_model->insert_card_data($insert_array);
					}
				}
			}
			$subdata = array(
				'baddress1' => $baddress1,
				'baddress2' => $baddress2,
				'bcity'	  => $bcity,
				'bstate'	  => $bstate,
				'bcountry'  => $bcountry,
				'bphone'	  => $bphone,
				'bzipcode'  => $bzipcode,
				'subscriptionName'    => $sname,
				'customerID'         => $customerID,
				'subscriptionPlan'   => $plan,
				'subscriptionAmount' => $total,
				'generatingDate'    => $invoice_date,
				'firstDate'			=> $first_date,
				'startDate'         => $st_date,
				'endDate'           => $end_date,
				'paymentGateway'    => $paygateway,
				'address1'          => $address1,
				'address2'          => $address2,
				'country'           => $country,
				'state'             => $state,
				'city'              => $city,
				'zipcode'           => $zipcode,
				'contactNumber'     => $phone,
				'totalInvoice'      => $plan,
				'invoicefrequency'  => $paycycle,
				'freeTrial'			=> $freetrial,
				'taxID'			=> $taxval,
				'planID'		=> $planid,

			);


			if ($this->czsecurity->xssCleanPostInput('autopay')) {
				$subdata['cardID']	= $cardID;
				$subdata['automaticPayment'] = '1';
			} else {
				$subdata['automaticPayment'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('email_recurring')) {

				$subdata['emailRecurring'] = '1';
			} else {
				$subdata['emailRecurring'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('billinfo')) {

				$subdata['usingExistingAddress'] = '1';
			} else {
				$subdata['usingExistingAddress'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('subID') != "") {
				$rowID = $this->czsecurity->xssCleanPostInput('subID');
				$subdata['updatedAt']  = date('Y-m-d H:i:s');


				$subs =  $this->general_model->get_row_data('tbl_subscriptions_xero', array('subscriptionID' => $rowID));
				$date = $first_date;

				$in_num  = $subs['generatedInvoice'];

				if ($paycycle == 'dly') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
				}
				if ($paycycle == '1wk') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				}

				if ($paycycle == '2wk') {
					$in_num   = ($in_num) ? $in_num + 1 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				}
				if ($paycycle == 'mon') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == '2mn') {
					$in_num   = ($in_num) ? $in_num + 2 * 1 : '0';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
				}
				if ($paycycle == 'qtr') {
					$in_num   = ($in_num) ? $in_num + 3 * 1 : '0';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
				}
				if ($paycycle == 'six') {
					$in_num   = ($in_num) ? $in_num + 6 * 1 : '0';
					$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == 'yrl') {
					$in_num   = ($in_num) ? $in_num + 12 * 1 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}

				if ($paycycle == '2yr') {
					$in_num   = ($in_num) ? $in_num + 2 * 12 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == '3yr') {
					$in_num   = ($in_num) ? $in_num + 3 * 12 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}

				$subdata['nextGeneratingDate'] = $next_date;

				$ins_data = $this->general_model->update_row_data('tbl_subscriptions_xero', array('subscriptionID' => $rowID), $subdata);

				$this->general_model->delete_row_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $rowID));

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $rowID;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
				}
			} else {
				$subdata['merchantDataID'] = $this->session->userdata('logged_in')['merchID'];
				$subdata['createdAt']  = date('Y-m-d H:i:s');
				$subdata['nextGeneratingDate'] = $first_date;
				$ins_data = $this->general_model->insert_row('tbl_subscriptions_xero', $subdata);

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $ins_data;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
				}
			}
			if ($ins_data && $ins) {

				$this->session->set_flashdata('success', 'Successfully Created Subscription');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
			}

			redirect('Xero_controllers/SettingSubscription/subscriptions', 'refresh');
		}


		if ($this->uri->segment('4') != "") {
			$sbID = $this->uri->segment('4');

			$data['subs']		= $this->general_model->get_row_data('tbl_subscriptions_xero', array('subscriptionID' => $sbID));


			$data['c_cards']    = $this->card_model->get_card_expiry_data($data['subs']['customerID']);
			$data['items']      = $this->general_model->get_table_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));


		}
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();

		if ($this->session->userdata('logged_in')) {

			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}

		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$condition				= array('merchantID' => $user_id);
		$conditionPlan				= array('merchantID' => $user_id, 'IsActive' => 'active');
		$condition11				= array('merchantDataID' => $user_id);

		$data['subplans']		= $this->general_model->get_table_data('tbl_subscriptions_plan_xero', $condition11);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
		$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan', '');
		$data['plans']          = $this->general_model->get_table_data('Xero_test_item', $conditionPlan);
		$compdata				= $this->xero_customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;


		$conditiontax = array('tax.merchantID' => $user_id);

		$taxes = $this->xero_company_model->get_xero_tax_data($user_id);
		$data['taxes'] = $taxes;



		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/create_subscription', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	

	public function get_tax_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('tax_id');
		$val = array(
			'taxID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_taxes_xero', $val);
		echo json_encode($data);
	}





	function create_invoice()
	{
		if (!empty($this->input->post(null, true))) {

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTax'] = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
				$itemDes = $this->czsecurity->xssCleanPostInput('description')[$key];

				$itemDes = str_replace('<','< ',$itemDes);
				$itemDes = str_replace('>','> ',$itemDes);
				$insert_row['itemDescription'] = $itemDes;
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$sname      = $this->czsecurity->xssCleanPostInput('sub_name');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			$plan        = $this->czsecurity->xssCleanPostInput('duration_list');
			if ($plan > 0) {
				$subsamount  = $total / $plan;
			} else {
				$subsamount  = $total;
			}
			$first_date = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
			$invoice_date = date('d', strtotime($this->czsecurity->xssCleanPostInput('invoice_date')));
			$st_date      = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('sub_start_date')));
			$freetrial    = $this->czsecurity->xssCleanPostInput('freetrial');
			$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');
			$paycycle     = $this->czsecurity->xssCleanPostInput('paycycle');
			$tax     = $this->czsecurity->xssCleanPostInput('texes');
			$end_date = date('Y-m-d', strtotime($st_date . "+$plan months"));
			if ($this->czsecurity->xssCleanPostInput('autopay')) {

				if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
					$card = array();
					$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
					$card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
					$card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
					$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
					$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
					$card['friendlyname'] = $friendlyName;
					$card['customerID']  = $customerID;

					$card_data = $this->check_friendly_name($customerID, $friendlyName);
					if (!empty($card_data)) {
						$this->update_card($card, $customerID, $friendlyName);
						$cardID  = $card_data['CardID'];
					} else {
						$cardID = $this->insert_new_card($card);
					}
				}
			}

			$subdata = array(
				'subscriptionName'    => $sname,
				'customerID'         => $customerID,
				'subscriptionPlan'   => $plan,
				'subscriptionAmount' => $total,
				'generatingDate'    => $invoice_date,
				'firstDate'			=> $first_date,
				'startDate'         => $st_date,
				'endDate'           => $end_date,
				'paymentGateway'    => $paygateway,
				'address1'          => $address1,
				'address2'          => $address2,
				'country'           => $country,
				'state'             => $state,
				'city'              => $city,
				'zipcode'           => $zipcode,
				'totalInvoice'      => $plan,
				'invoicefrequency'  => $paycycle,
				'freeTrial'			=> $freetrial,
				'taxID'			=> $tax,
			);

			if ($this->czsecurity->xssCleanPostInput('autopay')) {
				$subdata['cardID']	= $cardID;
				$subdata['automaticPayment'] = '1';
			} else {
				$subdata['automaticPayment'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('billinfo')) {

				$subdata['usingExistingAddress'] = '1';
			} else {
				$subdata['usingExistingAddress'] = '0';
			}

			if ($this->czsecurity->xssCleanPostInput('subID') != "") {
				$rowID = $this->czsecurity->xssCleanPostInput('subID');
				$subdata['updatedAt']  = date('Y-m-d H:i:s');


				$subs =  $this->general_model->get_row_data('tbl_subscriptions', array('subscriptionID' => $rowID));
				$date = $first_date;

				$in_num  = $subs['generatedInvoice'];


				if ($paycycle == 'dly') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
				}
				if ($paycycle == '1wk') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				}

				if ($paycycle == '2wk') {
					$in_num   = ($in_num) ? $in_num + 1 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
				}
				if ($paycycle == 'mon') {
					$in_num   = ($in_num) ? $in_num : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == '2mn') {
					$in_num   = ($in_num) ? $in_num + 2 * 1 : '0';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
				}
				if ($paycycle == 'qtr') {
					$in_num   = ($in_num) ? $in_num + 3 * 1 : '0';
					$next_date =  strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
				}
				if ($paycycle == 'six') {
					$in_num   = ($in_num) ? $in_num + 6 * 1 : '0';
					$next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == 'yrl') {
					$in_num   = ($in_num) ? $in_num + 12 * 1 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}

				if ($paycycle == '2yr') {
					$in_num   = ($in_num) ? $in_num + 2 * 12 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}
				if ($paycycle == '3yr') {
					$in_num   = ($in_num) ? $in_num + 3 * 12 : '0';
					$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
				}

				$subdata['nextGeneratingDate'] = $next_date;

				$ins_data = $this->general_model->update_row_data('tbl_subscriptions', array('subscriptionID' => $rowID), $subdata);

				$this->general_model->delete_row_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $rowID));

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $rowID;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
				}
			} else {
				$subdata['merchantDataID'] = $this->session->userdata('logged_in')['merchID'];
				$subdata['createdAt']  = date('Y-m-d H:i:s');
				$subdata['nextGeneratingDate'] = $first_date;
				$ins_data = $this->general_model->insert_row('tbl_subscriptions', $subdata);

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $ins_data;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item_xero', $item);
				}
			}

			if ($ins_data && $ins) {

				$this->session->set_flashdata('success', 'Success');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');
			}

			redirect('Integration/home/invoices', 'refresh');
		}



		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['frequencies']      = $this->general_model->get_table_data('tbl_invoice_frequecy', '');
		$data['durations']      = $this->general_model->get_table_data('tbl_subscription_plan', '');
		$data['plans']          = $this->company_model->get_plan_data($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;

		$taxes = $this->general_model->get_table_data('tbl_taxes', '');
		$data['taxes'] = $taxes;
		$data['selectedID'] = $this->general_model->getTermsSelectedID($user_id);
		
		$this->load->view('template/template_start', $data);


		$this->load->view('template/page_head', $data);
		$this->load->view('pages/create_new_invoice', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	function edit_invoice()
	{


		if (!empty($this->input->post(null, true))) {

			$total = 0;
			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {

				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$editSeq      = $this->czsecurity->xssCleanPostInput('edit_sequence');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

			$customer   = $this->customer_model->customer_by_id($customerID);
			$txnID      = $this->czsecurity->xssCleanPostInput('txnID');
			$refNumber  = $this->czsecurity->xssCleanPostInput('refNumber');
			$fullname   = $customer->FullName;
			$user       = $customer->qbwc_username;
			$duedate    = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('duedate')));

			$address1     = $this->czsecurity->xssCleanPostInput('address1');
			$address2     = $this->czsecurity->xssCleanPostInput('address2');
			$country      = $this->czsecurity->xssCleanPostInput('country');
			$state        = $this->czsecurity->xssCleanPostInput('state');
			$city	      = $this->czsecurity->xssCleanPostInput('city');
			$zipcode      = $this->czsecurity->xssCleanPostInput('zipcode');


			$inv_data = array(
				'Customer_FullName' => $fullname,
				'Customer_ListID' => $customerID,
				'TxnID' => $txnID,
				'RefNumber' => $refNumber,
				'TimeCreated' => date('Y-m-d H:i:S'),
				'TimeModified' => date('Y-m-d H:i:S'),
				'DueDate'   => $duedate,
				'ShipAddress_Addr1' => $address1,
				'ShipAddress_Addr2' => $address2,
				'ShipAddress_City' => $city,
				'ShipAddress_Country' => $country,
				'ShipAddress_State' => $state,
				'ShipAddress_PostalCode' => $zipcode,
				'IsPaid'				=> 'false',
				'invoiceRefNumber' => $refNumber,
				'EditSequence' => $editSeq
			);

			if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
				$card = array();
				$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
				$card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number');
				$card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
				$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
				$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
				$card['friendlyname'] = $friendlyName;
				$card['customerID']  = $customerID;

				$card_data = $this->check_friendly_name($customerID, $friendlyName);
				if (!empty($card_data)) {
					$this->update_card($card, $customerID, $friendlyName);
					$cardID  = $card_data['CardID'];
				} else {


					$cardID = $this->insert_new_card($card);
				}
			}



			$invoice_data =  $this->general_model->get_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber));

			if (!empty($invoice_data)) {
				

				$rowID    = $invoice_data['insertInvID'];
				$ins_data = $this->general_model->update_row_data('tbl_custom_invoice', array('Customer_ListID' => $customerID, 'invoiceRefNumber' => $refNumber), $inv_data);

				$this->general_model->delete_row_data('tbl_subscription_invoice_item', array('invoiceDataID' => $rowID));
				$randomNum = $rowID;
				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $rowID;
					$item['invoiceDataID']  = $rowID;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
				

			} else {
				$randomNum = substr(str_shuffle("0123456789"), 0, 5);
				$inv_data['insertInvID'] = $randomNum;
				$ins_data = $this->general_model->insert_row('tbl_custom_invoice', $inv_data);

				foreach ($item_val as $k => $item) {
					$item['subscriptionID'] = $randomNum;
					$item['invoiceDataID']  = $randomNum;
					$ins = $this->general_model->insert_row('tbl_subscription_invoice_item', $item);
				}
			}

			if ($ins_data && $ins) {
				$this->quickbooks->enqueue(QUICKBOOKS_MOD_INVOICE, $randomNum, '', '', $user);
				$this->session->set_flashdata('success', 'Success');
			} else {

				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong></div>');
			}

			redirect('Integration/home/invoices', 'refresh');
		}



		if ($this->uri->segment('3') != "") {
			$invoiceID = $this->uri->segment('3');
			$invoice   = $this->general_model->get_row_data('qb_test_invoice', array('TxnID' => $invoiceID));
			$products  = $this->general_model->get_table_data('qb_test_invoice_lineitem', array('TxnID' => $invoiceID));
			$custom    = $this->general_model->get_row_data('tbl_custom_invoice', array('RefNumber' => $invoice['RefNumber']));
			if (!empty($custom)) {
				$data['custom'] =  $custom;
			}
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');

			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}
		$condition				= array('merchantID' => $user_id);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		
		$data['plans']          = $this->company_model->get_plan_data($user_id);
		$compdata				= $this->customer_model->get_customers_data($user_id);
		$data['customers']		= $compdata;
		$data['invoice']		= $invoice;
		$data['items']		    = $products;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('pages/edit_invoice', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function get_subplan()
	{

		$planid =   $this->czsecurity->xssCleanPostInput('planID');

		$val = $this->xero_subscription_model->get_subplan_data($planid);
		echo json_encode($val);
	}

	public function update_gateway()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('user_logged_in');

				$user_id 				= $data['login_info']['merchantID'];
			}

			$sub_ID = implode(',', $this->czsecurity->xssCleanPostInput('gate'));

			$old_gateway =  $this->czsecurity->xssCleanPostInput('gateway_old');
			$new_gateway =  $this->czsecurity->xssCleanPostInput('gateway_new');

			$condition	  = array('merchantDataID' => $user_id, 'paymentGateway' => $old_gateway);
			$num_rows    = $this->general_model->get_num_rows('tbl_subscriptions_xero', $condition);

			if ($num_rows > 0) {
				$update_data = array('paymentGateway' => $new_gateway);

				$sss = $this->db->query("update tbl_subscriptions_xero set paymentGateway='" . $new_gateway . "' where subscriptionID IN ($sub_ID) ");

				if ($sss) {

					$this->session->set_flashdata('success', 'Subscription gateway have changed');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process. </strong></div>');
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> No data selected. </strong></div>');
			}
		}
		redirect('Xero_controllers/SettingSubscription/subscriptions', 'refresh');
	}



	//------------- To edit gateway ------------------//

	public function get_gateway_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gateway_id');
		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$val = array(
			'paymentGateway' => $id,
			'merchantDataID' => $merchantID
		);

		$datas = $this->xero_subscription_model->get_subscriptions_gatway_data($val);

?>


		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Customer Name</strong></th>

					<th class="text-center col-md-6"><strong>Select List </strong></th>

				</tr>
				<?php
				if (!empty($datas)) {
					foreach ($datas as $c_data) {
				?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['fullName']; ?> </td>

							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['subscriptionID']; ?>" /> </td>


						</tr>


					<?php     }
				} else { ?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found..!"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php } ?>

			</tbody>
		</table>


<?php die;
	}









	public function delete_subscription()
	{

		$subscID = $this->czsecurity->xssCleanPostInput('xerosubscID');
		$condition =  array('subscriptionID' => $subscID);
		$this->general_model->delete_row_data('tbl_subscription_invoice_item_xero', $condition);

		$del  = $this->general_model->delete_row_data('tbl_subscriptions_xero', $condition);
		if ($del) {

			$this->session->set_flashdata('success', 'Success');
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
		}

		redirect('Xero_controllers/SettingSubscription/subscriptions', 'refresh');
	}



	public function get_item_data()
	{

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemID = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('qb_test_item', array('ListID' => $itemID));
			echo json_encode($itemdata);
			die;
		}
		return false;
	}



	public function check_friendly_name($cid, $cfrname)
	{
		$card = array();
		$this->load->library('encrypt');



		$query1 = $this->db1->query("Select * from customer_card_data where customerListID = '" . $cid . "' and customerCardfriendlyName ='" . $cfrname . "' ");
		$card = $query1->row_array();

		return $card;
	}


	public function insert_new_card($card)
	{

		$this->load->library('encrypt');

		$card_no  = $card['card_number'];
		$expmonth = $card['expiry'];
		$exyear   = $card['expiry_year'];
		$cvv      = $card['cvv'];
		$b_add1   = $card['Billing_Addr1'];
		$b_add2   = $card['Billing_Addr2'];
		$b_city   = $card['Billing_City'];
		$b_state  = $card['Billing_State'];
		$b_country = $card['Billing_Country'];
		$b_contact = $card['Billing_Contact'];
		$b_zip   = $card['Billing_Zipcode'];
		$friendlyname = $card['friendlyname'];
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		$customer   = $card['customerID'];

		$is_default = 0;
        $checkCustomerCard = checkCustomerCard($customer,$merchantID);
        if($checkCustomerCard == 0){
            $is_default = 1;
        }
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $this->encrypt->encode($card_no),
			'CardCVV'      => $this->encrypt->encode($cvv),
			'Billing_Addr1'	 => $b_add1,
			'Billing_Addr2'	 => $b_add2,
			'Billing_City'	 => $b_city,
			'Billing_State'	 => $b_state,
			'Billing_Country'	 => $b_country,
			'Billing_Contact'	 => $b_contact,
			'Billing_Zipcode'	 => $b_zip,
			'customerListID' => $customer,
			'merchantID'     => $merchantID,
			'is_default'			   => $is_default,
			'customerCardfriendlyName' => $friendlyname,
			'createdAt' 	=> date("Y-m-d H:i:s")
		);

		$this->db1->insert('customer_card_data', $insert_array);
		return $this->db1->insert_id();;
	}



	public function update_card($card, $cid, $cfrname)
	{
		$this->load->library('encrypt');

		$card_no  = $card['card_number'];
		$expmonth = $card['expiry'];
		$exyear   = $card['expiry_year'];
		$cvv      = $card['cvv'];
		$b_add1   = $card['Billing_Addr1'];
		$b_add2   = $card['Billing_Addr2'];
		$b_city   = $card['Billing_City'];
		$b_state   = $card['Billing_State'];
		$b_country   = $card['Billing_Country'];
		$b_contact   = $card['Billing_Contact'];
		$b_zip   = $card['Billing_Zipcode'];

		$update_data =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $this->encrypt->encode($card_no),
			'CardCVV'      => $this->encrypt->encode($cvv),
			'updatedAt' 	=> date("Y-m-d H:i:s"),
			'Billing_Addr1'	 => $b_add1,
			'Billing_Addr2'	 => $b_add2,
			'Billing_City'	 => $b_city,
			'Billing_State'	 => $b_state,
			'Billing_Country'	 => $b_country,
			'Billing_Contact'	 => $b_contact,
			'Billing_Zipcode'	 => $b_zip
		);
		$this->db1->where('customerListID', $cid);
		$this->db1->where('customerCardfriendlyName', $cfrname);
		$this->db1->update('customer_card_data', $update_data);
		return true;
	}


	public function get_card_expiry_data($customerID)
	{

		$card = array();
		$this->load->library('encrypt');


		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'    ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$customer_card['CardNo']  = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
				$customer_card['CardID'] = $card_data['CardID'];
				$customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				$card[$key] = $customer_card;
			}
		}

		return  $card;
	}
}
