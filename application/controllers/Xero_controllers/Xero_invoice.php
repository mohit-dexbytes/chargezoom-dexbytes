<?php
class Xero_invoice extends CI_Controller
{
    protected $loginDetails;
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
		$this->load->model('general_model');
		$this->load->model('card_model');
		$this->load->model('Xero_models/xero_customer_model');
		$this->load->library('session');
		$this->db1 = $this->load->database('otherdb', TRUE);

		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '4') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
		$this->loginDetails = get_names();
	}


	public function index()
	{
		redirect('Integration/home', 'refresh');
	}

	public function Invoice_detailes()
	{



		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			} else if ($this->session->userdata('user_logged_in')) {
				$user_id = $this->session->userdata('user_logged_in')['merchantID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);


			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Invoices', 'core'), array());
			
			if ($XeroOAuth->response['code'] == 200) {
				$invoices = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

				$invoices_arr = json_decode(json_encode($invoices), 1);
				if (isset($invoices_arr['Invoices'])) {

					
					foreach ($invoices_arr['Invoices'] as $Invoice) {

						foreach ($Invoice as $Invoicedata) {
							if (!empty($Invoicedata['InvoiceID'])) {
								$FB_invoice_details['invoiceID'] = $Invoicedata['InvoiceID'];
								$FB_invoice_details['refNumber'] = $Invoicedata['InvoiceNumber'];
								$FB_invoice_details['CustomerFullName'] = $this->db->escape_str($Invoicedata['Contact']['Name']);
								$FB_invoice_details['BalanceRemaining'] = $Invoicedata['AmountDue'];
								$FB_invoice_details['Total_payment'] = $Invoicedata['Total'];
								$FB_invoice_details['UserStatus'] = $this->db->escape_str($Invoicedata['Status']);
								$FB_invoice_details['CustomerListID'] = $Invoicedata['Contact']['ContactID'];
								$FB_invoice_details['TimeModified'] = $this->db->escape_str($Invoicedata['UpdatedDateUTC']);
								$FB_invoice_details['TimeCreated'] = $this->db->escape_str($Invoicedata['Date']);
								$FB_invoice_details['AppliedAmount'] = $this->db->escape_str($Invoicedata['AmountPaid']);
								$FB_invoice_details['DueDate'] = $this->db->escape_str($Invoicedata['DueDate']);
								$FB_invoice_details['merchantID'] = $merchID;

								$val1 = array('invoiceID' => $FB_invoice_details['invoiceID'], 'merchantID' => $merchID);
								if (!empty($this->general_model->get_row_data('Xero_test_invoice', $val1)))
									$this->general_model->update_row_data('Xero_test_invoice', $val1, $FB_invoice_details);
								else
									$this->general_model->insert_row('Xero_test_invoice', $FB_invoice_details);
							}
						}
					}
				}
			}
		}

		if(!empty($this->input->post(null, true))) {
			$search = $this->input->post(null, true);
			$data['serachString'] = $search['search_data'];
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['page_num'] = 'customer_xero';
		$cond = array(
			'merchantID' => $merchID
		);

		$merchant_condition = [
            'merchID' => $merchID,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }
		$data['invoices'] = $this->general_model->get_xero_invoice_data('Xero_test_invoice', $cond);
		$plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;

		$data['gateway_datas']		  = $this->general_model->get_table_data('tbl_merchant_gateway', $val);
		$data['in_details']           = $this->xero_customer_model->get_invoice_details($merchID);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_invoices', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function add_invoice()
	{


		$data['login_info'] 	= $this->session->userdata('logged_in');
		$user_id 				= $data['login_info']['merchID'];
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$condition1 = array('merchantID' => $user_id);

		$customerList = $this->general_model->get_table_data('Xero_custom_customer', $condition1);
		if(!empty($customerList)) {
			$i = 0;
			foreach($customerList as $customer) {
				if(empty($customer['companyName'])){
					$customerList[$i]['companyName'] = $customer['fullName'];
				}
				else if(empty($customer['fullName'])){
					$customerList[$i]['companyName'] = $customer['firstName'] . ' '.$customer['lastName'];
				} 
				++$i;
			}
		}
		$data['customers'] = $customerList;
		$data['plans'] = $this->general_model->get_table_data('Xero_test_item', $condition1);
		$taxes = $this->general_model->get_table_data('tbl_taxes_xero', $condition1);
		$data['taxes'] = $taxes;
		$condition				= array('merchantID' => $user_id);
		$data['accounts']     = $this->general_model->get_table_data('tbl_xero_accounts', $condition);
		$data['gateways']		= $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		$data['netterms']  = $this->xero_customer_model->get_terms_data($user_id);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/create_new_invoice_online', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function invoice_create()
	{



		if ($this->session->userdata('logged_in')) {
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
		}
		if (!empty($this->input->post(null, true))) {
			$total = 0;

			foreach ($this->czsecurity->xssCleanPostInput('productID') as $key => $prod) {
				$insert_row['itemListID'] = $this->czsecurity->xssCleanPostInput('productID')[$key];
				$insert_row['itemQuantity'] = $this->czsecurity->xssCleanPostInput('quantity')[$key];
				$insert_row['itemRate']      = $this->czsecurity->xssCleanPostInput('unit_rate')[$key];
				$insert_row['itemFullName'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTax'] = $this->czsecurity->xssCleanPostInput('tax_check')[$key];
				$insert_row['itemDescription'] = $this->czsecurity->xssCleanPostInput('description')[$key];
				$insert_row['itemTotal'] = $this->czsecurity->xssCleanPostInput('total')[$key];
				$total = $total + $this->czsecurity->xssCleanPostInput('total')[$key];
				$item_val[$key] = $insert_row;
			}

			$inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));

			if (!empty($inv_data)) {
				$inv_pre   = $inv_data['prefix'];
				$inv_po    = $inv_data['postfix'] + 1;
				$new_inv_no = $inv_pre . $inv_po;
				$Number = $new_inv_no;
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please create invoice prefix to generate invoice number.</div>');

				redirect('/Xero_controllers/Create_invoice/add_invoice', 'refresh');
			}


			$val = array(
				'merchantID' => $merchID,
			);

			$customer_ref = $this->czsecurity->xssCleanPostInput('customerID');
			$address1 = $this->czsecurity->xssCleanPostInput('address1');
			$address2 = $this->czsecurity->xssCleanPostInput('address2');
			$country = $this->czsecurity->xssCleanPostInput('country');
			$state = $this->czsecurity->xssCleanPostInput('state');
			$city = $this->czsecurity->xssCleanPostInput('city');
			$zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
			$phone = $this->czsecurity->xssCleanPostInput('phone');
			$baddress1 = $this->czsecurity->xssCleanPostInput('baddress1');
			$baddress2 = $this->czsecurity->xssCleanPostInput('baddress2');
			$bcountry = $this->czsecurity->xssCleanPostInput('bcountry');
			$bstate = $this->czsecurity->xssCleanPostInput('bstate');
			$bcity = $this->czsecurity->xssCleanPostInput('bcity');
			$bzipcode = $this->czsecurity->xssCleanPostInput('bzipcode');
			$bphone = $this->czsecurity->xssCleanPostInput('bphone');

			$inv_date  = date($this->czsecurity->xssCleanPostInput('invdate'));
			$pterm  = $this->czsecurity->xssCleanPostInput('pay_terms');

			if ($pterm != "" && $pterm != 0) {
				$duedate =  date("Y-m-d",  strtotime($inv_date . "+ $pterm day"));
				
			} else {
				$duedate    = date('Y-m-d', strtotime($inv_date));
			}

			
			//card-details
			$paygateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$cardID       = $this->czsecurity->xssCleanPostInput('card_list');
			$customerID = $this->czsecurity->xssCleanPostInput('customerID');

		
			define('BASE_PATH', dirname(__FILE__));

			define("XRO_APP_TYPE", "Public");

			$useragent = "Chargezoom";

			$data = $this->general_model->get_table_data('xero_config', '');

			if (!empty($data)) {

				foreach ($data as $dt) {
					$url = $dt['oauth_callback'];
					$key = $dt['consumer_key'];
					$secret = $dt['shared_secret'];
				}
			}
			define("OAUTH_CALLBACK", $url);

			$signatures = array(
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0'
			);

			$XeroOAuth = new XeroOAuth(array_merge(array(
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent
			), $signatures));

			$initialCheck = $XeroOAuth->diagnostics();
			$checkErrors = count($initialCheck);
			if ($checkErrors > 0) {
				// you could handle any config errors here, or keep on truckin if you like to live dangerously
				foreach ($initialCheck as $check) {
					echo 'Error: ' . $check . PHP_EOL;
				}
			} else {

				if ($this->session->userdata('logged_in')) {
					$user_id = $this->session->userdata('logged_in')['merchID'];
					$merchID = $user_id;
				}

				$val = array(
					'merchantID' => $merchID,
				);
			}

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
			$inv_po = '';
			if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

				if ($this->czsecurity->xssCleanPostInput('taxes') == "") {
					$lineArray = "";




					for ($i = 0; $i < count($item_val); $i++) {

						$lineArray .= '<LineItem><Description>' . $item_val[$i]['itemDescription'] . '</Description>';
						$lineArray .= '<UnitAmount>' . $item_val[$i]['itemRate'] . '</UnitAmount>';
						$lineArray .= '<Quantity>' . $item_val[$i]['itemQuantity'] . '</Quantity>';
						$lineArray .= '<AccountCode>4000</AccountCode>';
					
						$lineArray .= '<ItemCode>' . $item_val[$i]['itemListName'] . '</ItemCode></LineItem>';
					}
					

					$invoice = '<Invoice>
						  <Type>ACCREC</Type>
						  <Contact>
						  <ContactID>' . $customerID . '</ContactID>
						   <Address>
							  <AddressType>STREET</AddressType>
							  <AddressLine1>' . $address1 . '</AddressLine1>
							  <AddressLine2>' . $address2 . '</AddressLine2>
							  <City>' . $city . '</City>
							  <Region>' . $state . '</Region>
							  <PostalCode>' . $zipcode . '</PostalCode>
							  <Country>' . $country . '</Country>
						 </Address>
						 <Phones>
						 	<Phone>
							<PhoneType>MOBILE</PhoneType>
							  <PhoneNumber>' . $phone . '</PhoneNumber>
							</Phone>
                          </Phones>
                          </Contact>
						  <DueDate>' . $duedate . '</DueDate>
                         
						  <LineItems>' . $lineArray . '</LineItems>
						  <Status>AUTHORISED</Status>
						</Invoice>';


					$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);

					if ($XeroOAuth->response['code'] == 200) {
						$this->session->set_flashdata('success', 'Success');

						redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

						redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
					}
				} else {

					$taxtype = $this->czsecurity->xssCleanPostInput('taxes');

					$lineArray = "";

					for ($i = 0; $i < count($item_val); $i++) {

						$lineArray .= '<LineItem><Description>' . $item_val[$i]['itemDescription'] . '</Description>';
						$lineArray .= '<UnitAmount>' . $item_val[$i]['itemRate'] . '</UnitAmount>';
						$lineArray .= '<Quantity>' . $item_val[$i]['itemQuantity'] . '</Quantity>';
						$lineArray .= '<AccountCode>2500</AccountCode>';
						
						$lineArray .= '<ItemCode>' . $item_val[$i]['itemListName'] . '</ItemCode><TaxType>' . $taxtype . '</TaxType></LineItem>';
					}
				
					$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchID));
					if (!empty($in_data)) {
						$inv_pre   = $in_data['prefix'];
						$nexnum    =   $in_data['postfix'] + 1;
						$inv_po    = $inv_pre . $nexnum;
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error </strong> Create your invoice prefix</div>');

						redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
					}
					$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchID), array('postfix' => $nexnum));

					$invoice = '<Invoice>
						  <Type>ACCREC</Type>
						  <Contact>
						  <ContactID>' . $customerID . '</ContactID>
						   <Address>
							  <AddressType>STREET</AddressType>
							  <AddressLine1>' . $address1 . '</AddressLine1>
							  <AddressLine2>' . $address2 . '</AddressLine2>
							  <City>' . $city . '</City>
							  <Region>' . $state . '</Region>
							  <PostalCode>' . $zipcode . '</PostalCode>
							  <Country>' . $country . '</Country>
						 </Address>
						 <Phones>
						 	<Phone>
							<PhoneType>MOBILE</PhoneType>
							  <PhoneNumber>' . $phone . '</PhoneNumber>
							</Phone>
                          </Phones>
                          </Contact>
						  <DueDate>' . $duedate . '</DueDate>';
					if ($inv_po != "")
						$invoice .= '<InvoiceNumber>' . $inv_po . '</InvoiceNumber>';

					$invoice .= '<LineItems>' . $lineArray . '</LineItems>
						  <Status>AUTHORISED</Status>
						</Invoice>';


					$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);



					if ($XeroOAuth->response['code'] == 200) {

						$invoices = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

						$invoices_arr = json_decode(json_encode($invoices), 1);

						if (isset($invoices_arr['Invoices'])) {


							$Invoicedata = $invoices_arr['Invoices']['Invoice'];
							if (!empty($Invoicedata['InvoiceID'])) {
								$FB_invoice_details['invoiceID'] = $Invoicedata['InvoiceID'];
								$FB_invoice_details['refNumber'] = $Invoicedata['InvoiceNumber'];
								$FB_invoice_details['CustomerFullName'] = $this->db->escape_str($Invoicedata['Contact']['Name']);
								$FB_invoice_details['BalanceRemaining'] = $Invoicedata['AmountDue'];
								$FB_invoice_details['Total_payment'] = $Invoicedata['Total'];
								$FB_invoice_details['UserStatus'] = $this->db->escape_str($Invoicedata['Status']);
								$FB_invoice_details['CustomerListID'] = $Invoicedata['Contact']['ContactID'];
								$FB_invoice_details['TimeModified'] = $this->db->escape_str($Invoicedata['UpdatedDateUTC']);
								$FB_invoice_details['TimeCreated'] = $this->db->escape_str($Invoicedata['Date']);
								$FB_invoice_details['AppliedAmount'] = $this->db->escape_str($Invoicedata['AmountPaid']);
								$FB_invoice_details['DueDate'] = $this->db->escape_str($Invoicedata['DueDate']);
								$FB_invoice_details['merchantID'] = $merchID;
								$FB_invoice_details['DueDate'] = $this->db->escape_str($Invoicedata['DueDate']);
								$FB_invoice_details['totalTax'] = $Invoicedata['TotalTax'];


								if (isset($Invoicedata['LineItems'])) {

									$line_datas  = $Invoicedata['LineItems']['LineItem'];

									foreach ($line_datas as $lines) {

										$l_array['invoiceID'] = $Invoicedata['InvoiceID'];
										$l_array['itemID'] = $lines['ItemCode'];
										$l_array['itemDescription'] = $lines['Description'];
										$l_array['itemPrice'] = $lines['UnitAmount'];
										$l_array['totalAmount'] = $lines['LineAmount'];
										$l_array['tax'] = $lines['TaxType'];
										$l_array['itemQty'] = $lines['Quantity'];
										$l_array['merchantID'] = $merchID;

										$this->general_model->insert_row('tbl_xero_invoice_item', $l_array);
									}
								}


								$cardID = $this->general_model->insert_row('Xero_test_invoice', $FB_invoice_details);
							}
						}



						$this->session->set_flashdata('success', 'Success');

						redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
					} else {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

						redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
					}
				}
			}
		}
	}


	

	public function invoice_details_page()
	{

		$oneInvoice = array();
		if ($this->session->userdata('logged_in')) {
			$data['login_info']	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info']		= $this->session->userdata('user_logged_in');
			$user_id 				= $data['login_info']['merchantID'];
		}

		$merchID = $user_id;

		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID);

		$condition =  array('merchantID' => $merchID);

		$inv_id = $this->uri->segment('4');
		$val = array(
			'merchantID' => $merchID,
		);

		
		$oneInvoice = $this->general_model->get_row_data('Xero_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID));


		if (!empty($oneInvoice)) {
			$oneInvoice['l_items'] = $this->general_model->get_table_data('tbl_xero_invoice_item', array('invoiceID' => $inv_id, 'merchantID' => $merchID));
			$oneInvoice['cust_data'] = $this->general_model->get_row_data('Xero_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $oneInvoice['CustomerListID']));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error Invalid Invoice </strong></div>');
			redirect('Xero_controllers/Xero_invoice/Invoice_detailes', 'refresh');
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

	
		$data['page_num']      = 'customer_xero';
		$data['invoice_data']     = $oneInvoice;
		
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
		 
		$con = array('invoiceID' => $oneInvoice['invoiceID']);
		$data['transaction'] = $this->general_model->get_row_data('customer_transaction', $con);

		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_invoice_details', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function invoice_details_print()
	{

		
		if ($this->session->userdata('logged_in')) {
			$data['login_info'] 	= $this->session->userdata('logged_in');
			
			$user_id 				= $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('user_logged_in');

			$user_id 				= $data['login_info']['merchantID'];
		}

		$invoiceID             =  $this->uri->segment(4);


		$merchID = $user_id;

		$condition1 =  array('qb.merchantID' => $merchID, 'cs.merchantID' => $merchID);

		$condition =  array('merchantID' => $merchID);

		$inv_id = $this->uri->segment('4');
		$val = array(
			'merchantID' => $merchID,
		);

		$data['template'] 		= template_variable();


		$oneInvoice = $this->general_model->get_row_data('Xero_test_invoice', array('invoiceID' => $inv_id, 'merchantID' => $merchID));


		if (!empty($oneInvoice)) {
			$oneInvoice['invoice_items'] = $this->general_model->get_table_data('tbl_xero_invoice_item', array('invoiceID' => $inv_id, 'merchantID' => $merchID));
			$oneInvoice['customer_data'] = $this->general_model->get_row_data('Xero_custom_customer', array('merchantID' => $merchID, 'Customer_ListID' => $oneInvoice['CustomerListID']));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error Invalid Invoice </strong></div>');
			redirect('Xero_controllers/Xero_invoice/Invoice_detailes', 'refresh');
		}


		$data['invoice_data']   = $oneInvoice;
		

		$no = $invoiceID;
		$pdfFilePath = "$no.pdf";
		
		ini_set('memory_limit', '32M');

		$html = $this->load->view('Xero_views/page_invoice_details_print', $data, true);

		$this->load->library('pdf');
		$pdf = $this->pdf->load();
		$pdf->WriteHTML($html); // write the HTML into the PDF
		$pdf->Output($pdfFilePath, 'D'); // save to file because we can
	

	}





	public function get_item_data()
	{

		if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

			$itemName = $this->czsecurity->xssCleanPostInput('itemID');
			$itemdata = $this->general_model->get_row_data('Xero_test_item', array('Code' => $itemName));
			echo json_encode($itemdata);
			die;
		}
		return false;
	}

	public function delete_invoice()
	{

		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);

			$inv_id = $this->czsecurity->xssCleanPostInput('invID');
			$inv_status = $this->czsecurity->xssCleanPostInput('invSTATUS');

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
			if ($inv_id != "") {

				if ($inv_status == 'AUTHORISED') {
					$invoice = '<Invoice>
						  <InvoiceNumber>' . $inv_id . '</InvoiceNumber>
						  <Status>VOIDED</Status>
						</Invoice>';
				} else {
					$invoice = '<Invoice>
						  <InvoiceNumber>' . $inv_id . '</InvoiceNumber>
						  <Status>DELETED</Status>
						</Invoice>';
				}

				$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);

				if ($XeroOAuth->response['code'] == 200) {
					$this->session->set_flashdata('success', 'Success');

					redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

					redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
				}
			}
		}
	}

	public function invoice_schedule()
	{

		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);

			$Inv_id = $this->czsecurity->xssCleanPostInput('scheduleID');
			$DueDate = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date')));

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];



			$invoice = '<Invoice>
						  <InvoiceNumber>' . $Inv_id . '</InvoiceNumber>
						   <DueDate>' . $DueDate . '</DueDate>
						</Invoice>';


			$response = $XeroOAuth->request('POST', $XeroOAuth->url('Invoices', 'core'), array(), $invoice);
			
			if ($XeroOAuth->response['code'] == 200) {
				$this->session->set_flashdata('success', 'Success');

				redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

				redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'));
			}
		}
	}

	public function check_friendly_name($cid, $cfrname)
	{
		$card = array();
		$this->load->library('encrypt');







		$query1 = $this->db1->query("Select * from customer_card_data where customerListID = '" . $cid . "' and customerCardfriendlyName ='" . $cfrname . "' ");
		
		$card = $query1->row_array();

		return $card;
	}

	public function insert_new_card($card)
	{




		$card_no  = $card['card_number'];
		$expmonth = $card['expiry'];
		$exyear   = $card['expiry_year'];
		$cvv      = $card['cvv'];

		$friendlyname = $card['friendlyname'];
		$merchantID = $this->session->userdata('logged_in')['merchID'];
		$customer   = $card['customerID'];

		$is_default = 0;
        $checkCustomerCard = checkCustomerCard($customer,$merchantID);
        if($checkCustomerCard == 0){
            $is_default = 1;
        }
		$insert_array =  array(
			'cardMonth'  => $expmonth,
			'cardYear'	 => $exyear,
			'CustomerCard' => $this->encrypt->encode($card_no),
			'CardCVV'      => $this->encrypt->encode($cvv),
			'customerListID' => $customer,
			'merchantID'     => $merchantID,
			'customerCardfriendlyName' => $friendlyname,
			'is_default'			   => $is_default,
			'createdAt' 	=> date("Y-m-d H:i:s")
		);

		$this->db1->insert('customer_card_data', $insert_array);
		return $this->db1->insert_id();;
	}


	function getSequence($num)
	{
		return sprintf("%'.04d", $num);
	}


}// end of class
