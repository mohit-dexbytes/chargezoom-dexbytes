<?php
require APPPATH . 'libraries/REST_Controller.php';

class Auth extends REST_Controller{ 
 
	function __construct()
    {
        parent::__construct();
	
	  $this->load->model('Api_model','api_model');
	  $this->load->helper('api');
	  $this->load->helper('jwt');
	  $this->load->helper('auth');
	  $this->load->helper('error_code');
	}
	
	  /**
   * Index method for completeness. Returns a JSON response with an
   * error message.
   */
  public function index(){
    $response=array(
      "code" => BAD_DATA,
      "message"=>"No resource specified."
    );
      $this->response($response);
  }
	
	public function auth_login_post()
	{
	   
        $this->load->library('form_validation');
		$this->form_validation->set_rules('auth_id', 'User ', 'trim|required|xss-clean');
			$this->form_validation->set_rules('auth_pass', 'Password ', 'trim|required|xss-clean');
		if ($this->form_validation->run() == true)
		{
			  $secret = $this->input->request_headers()['Secret-Key'];
		     $chk_data = chk_api_key_services($secret);
          
			 if(!empty($chk_data))
			 {	 
		      $service  =   $chk_data['APIPackage'];
			   $domain  =   $chk_data['Domain'];
			  $auth_id  = 	 $this->czsecurity->xssCleanPostInput('auth_id');
	          $password = 	 $this->czsecurity->xssCleanPostInput('auth_pass');
			  $response =     auth_api( $auth_id, $password,$service,$domain, $secret, $chk_data['merchantID']);
			 }
			 else
			 {
				$response['message'] = 'Invalid Key';
				$response['data']    = "" ;
				$response['code']    = UNAUTHORIZED;	 
			 }
			$this->response($response);
		}
		else
		{
			  if(form_error('auth_id')!="")
			  {
				$error['auth_id'] = form_error('auth_id');
			  }	
			   if(form_error('auth_pass')!="")
			  {
				$error['auth_pass'] = form_error('auth_pass');
			  }	
			   $response['data']  = $error;	
	           $response['message'] = 'Validation Error';			   
        	   $response['code']  = PROHIBITED;
			   $this->response($response);
		}	   
	}	
      public function get_profile_data_post()
	  {
		    $this->load->library('form_validation');
		    $this->form_validation->set_rules('token', 'Auth Token', 'trim|required|xss-clean');
			
		if ($this->form_validation->run() == true)
		{
			$secret = $this->input->request_headers()['Secret-Key'];
			 $chk_data = chk_api_key_services($secret);
			
		 if(!empty($chk_data))
			 {	
		        $token  = 	 $this->czsecurity->xssCleanPostInput('token');
		      
				 $res_data =   get_jwt_data_api($token, $secret);
				 if($res_data['code']==SUCCESS)
				 {	 
					 $res_data      = $this->api_model->get_profile_data($res_data['role'],$res_data['id'],$res_data['mid']);
				     $response['message'] = 'Data';
						$response['data']    = $res_data ;
						$response['code']    = SUCCESS;
				 }else{
				  $response = $res_data;
				 }
				
			 }else
			 {
				$response['message'] = 'Invalid Key';
				$response['data']    = "" ;
				$response['code']    = UNAUTHORIZED;	 
			 }
			
		}else{
				  if(form_error('token')!="")
			  {
				$error['token'] = form_error('token');
			  }	
			   
			   $response['data']  = $error;	
	           $response['message'] = 'Validation Error';			   
        	   $response['code']  = PROHIBITED;
			  
		}
		 $this->response($response);
		
	  }
	  
	     public function refresh_token_post()
	  {
		    $this->load->library('form_validation');
		    $this->form_validation->set_rules('token', 'Auth Token', 'trim|required|xss-clean');
			
		if ($this->form_validation->run() == true)
		{
			$secret = $this->input->request_headers()['Secret-Key'];
			 $chk_data = chk_api_key_services($secret);
			
		 if(!empty($chk_data))
			 {	
		        $token  = 	 $this->czsecurity->xssCleanPostInput('token');
		      
				 $response =   regenerate_jwt_token($token, $secret);
				
				
			 }else
			 {
				$response['message'] = 'Invalid Key';
				$response['data']    = "" ;
				$response['code']    = UNAUTHORIZED;	 
			 }
			
		}else{
				  if(form_error('token')!="")
			  {
				$error['token'] = form_error('token');
			  }	
			   
			   $response['data']  = $error;	
	           $response['message'] = 'Validation Error';			   
        	   $response['code']  = PROHIBITED;
			  
		}
		 $this->response($response);
		
	  }

	
	function random_number()
	{
		$number 	= mt_rand(100000, 999999);
		$result 	= $this->auth->generic_select('users',array('transporter_id'=>$number));
		if($result){ 
		return random_number();
		}
		else{ 
		return $number;
		} 
	}
	function send_email($from, $name, $to, $subject, $message)
	{
		$config = Array(
			'charset' => 'utf-8',
			'mailtype' => 'html',
			'wordwrap' => TRUE
		);
		
		if($from == '')
		$from = "unknown@unknown.com";
		$data['data'] = $message;
		if($name == '')
		$name = "";
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from($from,$name);
		$this->email->to($to);
		$this->email->subject($subject);
		$html = $this->load->view('template/mail', array('title'=>$subject , 'message'=>$message), true);
		$this->email->message($html);
		$ack = $this->email->send();
		if ($ack) 
		return "success";
		else 
		return false;		
	}
	
	
	
	public function get_profile_post()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_id', 'User ', 'trim|required|xss-clean');
		if ($this->form_validation->run() == true)
		{
			
			$user_id		    = $this->czsecurity->xssCleanPostInput('user_id');
			$data = $this->auth->get_profile_data($user_id);
			$response['message'] = 'User Details';
			$response['data'] = $data ;
			$response['code']  = '200';
			$this->response($response);
		}
		else
		{
			  if(form_error('user_id')!="")
			  {
				$error['user_id'] = form_error('user_id');
			  }	
			   $response['data']  = $error;	
	           $response['message'] = 'Validation Error';			   
        	   $response['code']  = 401;
			   $this->response($response);
		}	   
	}	
	
	
	
	public function reset_password()
	{
		   
	}
	
	
}