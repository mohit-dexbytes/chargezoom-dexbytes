<?php
ini_set('display_errors', 'On');
require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
class XeroNew extends CI_Controller
{
    private $resellerID;
    private $merchantID;
    private $transactionByUser;
    private $gatewayEnvironment;
    public function __construct()
    {
        parent::__construct();
        // $this->load->config('quickbooks');
        // $this->load->model('quickbooks');
        // $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
        // $this->load->model('general_model');
        // $this->load->model('company_model');
        // $this->db1 = $this->load->database('otherdb', true);
        // $this->load->model('customer_model');
        // $this->load->model('card_model');
        // $this->load->library('form_validation');
        // $this->load->config('fluidpay');
        // if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '2') {
        //     $logged_in_data = $this->session->userdata('logged_in');
        //     $this->resellerID = $logged_in_data['resellerID'];
        //     $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        //     $merchID = $logged_in_data['merchID'];
        // } else if ($this->session->userdata('user_logged_in') != "") {
        //     $logged_in_data = $this->session->userdata('user_logged_in');
        //     $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
        //     $merchID = $logged_in_data['merchantID'];
        //     $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
        //     $this->resellerID = $rs_Data['resellerID'];
        // } else {
        //     // redirect('login', 'refresh');
        // }
        // $this->merchantID = $merchID;
        // $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => 'A9D9758A9EF54E9189436AF0B72791D0',
            'clientSecret'            => 'Iaer01vhxSKrpDBQBc2y4wvz2Ky7Bg85kqJ02A1shtnjokLo',
            'redirectUri'             => 'http://localhost/Chargezoom/Xero_controllers/XeroNew/xero_int',
            'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken'          => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation',
        ]);

        // Scope defines the data your app has permission to access.
        // Learn more about scopes at https://developer.xero.com/documentation/oauth2/scopes
        $options = [
            'scope' => ['openid email profile offline_access accounting.settings accounting.transactions accounting.contacts accounting.journals.read accounting.reports.read accounting.attachments'],
        ];

        // This returns the authorizeUrl with necessary parameters applied (e.g. state).
        $authorizationUrl = $provider->getAuthorizationUrl($options);

        // Save the state generated for you and store it to the session.
        // For security, on callback we compare the saved state with the one returned to ensure they match.
        $_SESSION['oauth2state'] = $provider->getState();

        // Redirect the user to the authorization URL.
        header('Location: ' . $authorizationUrl);
    }

    public function xero_int()
    {
        echo '<pre>';
        print_r($_REQUEST);

        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => 'A9D9758A9EF54E9189436AF0B72791D0',
            'clientSecret'            => 'Iaer01vhxSKrpDBQBc2y4wvz2Ky7Bg85kqJ02A1shtnjokLo',
            'redirectUri'             => 'http://localhost/Chargezoom/Xero_controllers/XeroNew/xero_int',
            'urlAuthorize'            => 'https://login.xero.com/identity/connect/authorize',
            'urlAccessToken'          => 'https://identity.xero.com/connect/token',
            'urlResourceOwnerDetails' => 'https://api.xero.com/api.xro/2.0/Organisation',
        ]);

        // If we don't have an authorization code then get one
        if (!isset($_GET['code'])) {
            echo "Something went wrong, no authorization code found";
            exit("Something went wrong, no authorization code found");

            // Check given state against previously stored one to mitigate CSRF attack
        } elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {
            echo "Invalid State";
            unset($_SESSION['oauth2state']);
            exit('Invalid state');
        } else {

            try {
                // Try to get an access token using the authorization code grant.
                $accessToken = $provider->getAccessToken('authorization_code', [
                    'code' => $_GET['code'],
                ]);

                $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken((string) $accessToken->getToken());
                $identityApi = new XeroAPI\XeroPHP\Api\IdentityApi(
                    new GuzzleHttp\Client(),
                    $config
                );

                $result = $identityApi->getConnections();

                // Save my tokens, expiration tenant_id
				echo '<pre>';
        		print_r([
					$accessToken->getToken(),
                    $accessToken->getExpires(),
                    $result[0]->getTenantId(),
                    $accessToken->getRefreshToken(),
                    $accessToken->getValues()["id_token"]
				]);die;

                header('Location: ' . './authorizedResource.php');
                exit();

            } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
                echo "Callback failed";
                exit();
            }
        }
    }

}
