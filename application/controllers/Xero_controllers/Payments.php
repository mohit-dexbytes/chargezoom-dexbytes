<?php

/**
 * This Controller has NMI Payment Gateway Process
 *
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment.
 *
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 * Perform Customer Card oprtations using this controller
 * Create, Delete, Modify
 */

class Payments extends CI_Controller
{
    private $resellerID;
    private $transactionByUser;
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/Xero.php';
        include APPPATH . 'third_party/nmiDirectPost.class.php';

        include APPPATH . 'third_party/nmiCustomerVault.class.php';

        $this->load->model('customer_model');
        $this->load->model('Xero_models/xero_customer_model');
        $this->load->model('Xero_models/xero_model');
        $this->load->model('general_model');

        $this->load->model('card_model');

        $this->load->library('form_validation');
        $this->load->model('Xero_models/xero_invoices_model');

        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '4') {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
        } else if ($this->session->userdata('user_logged_in') != "") {
            $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
        redirect('Xero_controllers/Payments/payment_transaction', 'refresh');

    }

    public function pay_invoice()
    {

        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {

            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $merchID = $user_id;

        define('BASE_PATH', dirname(__FILE__));

        define("XRO_APP_TYPE", "Public");

        $useragent = "Chargezoom";

        $data = $this->general_model->get_table_data('xero_config', '');

        if (!empty($data)) {

            foreach ($data as $dt) {
                $url    = $dt['oauth_callback'];
                $key    = $dt['consumer_key'];
                $secret = $dt['shared_secret'];
            }
        }

        define("OAUTH_CALLBACK", $url);

        $signatures = array(
            'consumer_key'    => $key,
            'shared_secret'   => $secret,
            // API versions
            'core_version'    => '2.0',
            'payroll_version' => '1.0',
            'file_version'    => '1.0',
        );

        $XeroOAuth = new XeroOAuth(array_merge(array(
            'application_type' => XRO_APP_TYPE,
            'oauth_callback'   => OAUTH_CALLBACK,
            'user_agent'       => $useragent,
        ), $signatures));

        $initialCheck = $XeroOAuth->diagnostics();
        $checkErrors  = count($initialCheck);

        $this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');
        $this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        $this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');

        $this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');

        if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

            
            $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
         
            $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
            $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');

        }

        if ($this->form_validation->run() == true) {

            $cusproID   = '';
            $error      = '';
            $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
            $cardID_upd = '';

            $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');

            $cardID = $this->czsecurity->xssCleanPostInput('CardID');

            $gateway = $this->czsecurity->xssCleanPostInput('gateway');

            $amount = $this->czsecurity->xssCleanPostInput('inv_amount');

            $in_data = $this->xero_model->get_invoice_data_pay($invoiceID, $user_id);

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $ref_number = array();

            if ($cardID != "" || $gateway != "") {

                if (!empty($in_data)) {
                    $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

                    $customerID = $in_data['CustomerListID'];

                    $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                    $c_data     = $this->general_model->get_select_data('Xero_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                    $resellerID = $this->resellerID;

                    $nmiuser     = $gt_result['gatewayUsername'];
                    $nmipass     = $gt_result['gatewayPassword'];
                    $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                    $gatewayName = getGatewayName($gt_result['gatewayType']);
                    $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

                    $error = '';

                    if ($cardID != "new1") {
                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no   = $card_data['CardNo'];
                        $expmonth  = $card_data['cardMonth'];
                        $exyear    = $card_data['cardYear'];
                        $cvv       = $card_data['CardCVV'];
                        $cardType  = $card_data['CardType'];
                        $address1  = $card_data['Billing_Addr1'];
                        $city      = $card_data['Billing_City'];
                        $zipcode   = $card_data['Billing_Zipcode'];
                        $state     = $card_data['Billing_State'];
                        $country   = $card_data['Billing_Country'];

                    } else {
                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cardType = $this->general_model->getType($card_no);
                        $cvv      = '';
                        if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                            $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                        }

                        $address1 = $this->czsecurity->xssCleanPostInput('address1');
                        $address2 = $this->czsecurity->xssCleanPostInput('address2');
                        $city     = $this->czsecurity->xssCleanPostInput('city');
                        $country  = $this->czsecurity->xssCleanPostInput('country');
                        $phone    = $this->czsecurity->xssCleanPostInput('phone');
                        $state    = $this->czsecurity->xssCleanPostInput('state');
                        $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');

                    }
                    $crtxnID = '';

                    $transaction1 = new nmiDirectPost($nmi_data);
                    $transaction1->setCcNumber($card_no);

                    $exyear = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear;
                    $transaction1->setCcExp($expry);
                    $transaction1->setCvv($cvv);
                    $transaction1->setCompany($c_data['companyName']);
                    $transaction1->setFirstName($c_data['fullName']);
                   
                    $transaction1->setAddress1($address1);
                    $transaction1->setCountry($country);
                    $transaction1->setCity($city);
                    $transaction1->setState($state);
                    $transaction1->setZip($zipcode);
                    
                    $transaction1->setEmail($c_data['userEmail']);
                    $transaction1->setAmount($amount);
                    // add level III data
                    $level_request_data = [
                        'transaction' => $transaction1,
                        'card_no' => $card_no,
                        'merchID' => $user_id,
                        'amount' => $amount,
                        'invoice_id' => $invoiceID,
                        'gateway' => 1
                    ];
                    $transaction1 = addlevelThreeDataInTransaction($level_request_data);
                    $transaction1->sale();
                    $result = $transaction1->execute();
                    $inID    = '';
                    $crtxnID = '';

                    if ($result['response_code'] == "100") {
                        $scode = "success";

                        $trID   = $result['transactionid'];
                        $st     = '0';
                        $action = 'Pay Invoice';
                        $msg    = "Payment Success ";

                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                        $txnID  = $in_data['invoiceID'];
                        $ispaid = 1;
                        $bamount = $in_data['BalanceRemaining'] - $amount;
                        if ($bamount > 0) {
                            $ispaid = 0;
                        }

                        $app_amount = $in_data['Total_payment'] + $amount;
                        $data       = array('IsPaid' => $ispaid, 'Total_payment' => ($app_amount), 'BalanceRemaining' => $bamount);
                        $condition  = array('invoiceID' => $in_data['invoiceID'], 'merchantID' => $user_id);

                        $this->general_model->update_row_data('Xero_test_invoice', $condition, $data);

                        if ($checkErrors > 0) {
                            // you could handle any config errors here, or keep on truckin if you like to live dangerously
                            foreach ($initialCheck as $check) {
                                echo 'Transaction Failed -  ' . $check . PHP_EOL;
                            }
                        }

                        $val = array(
                            'merchantID' => $user_id,
                        );

                        $PayDate = date('Y-m-d');

                        $data = $this->general_model->get_row_data('tbl_xero_token', $val);

                        $XeroOAuth->config['access_token']        = $xero_data['accessToken'];
                        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

                        $payment = '<Payment>
							  <Invoice>
							    <InvoiceID>' . $invoiceID . '</InvoiceID>
							  </Invoice>
							  <Account>
							    <Code>2500</Code>
							  </Account>
							  <Date>' . $PayDate . '</Date>
							  <Amount>' . $in_data['BalanceRemaining'] . '</Amount>
							</Payment>';

                        $response = $XeroOAuth->request('POST', $XeroOAuth->url('Payments', 'core'), array(), $payment);

                        if ($XeroOAuth->response['code'] == 200) {

                            $this->session->set_flashdata('success', 'Success');

                        } else {
                            $this->session->set_flashdata('success', 'Success');

                           
                        }
                    }
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
                }
            }
            $transaction['transactionID']     = $result['transactionid'];
            $transaction['transactionStatus'] = $result['responsetext'];
            $transaction['transactionCode']   = $result['response_code'];
            $transaction['transactionType']   = ($result['type']) ? $result['type'] : 'auto-nmi';
            $transaction['transactionDate']   = date('Y-m-d H:i:s');
            $transaction['gatewayID']          = $gateway;
            $transaction['transactionGateway'] = $gt_result['gatewayType'];
            $transaction['customerListID']     = $in_data['CustomerListID'];
            $transaction['transactionAmount']  = $in_data['BalanceRemaining'];
            $transaction['merchantID']         = $user_id;
            $transaction['invoiceID']          = $invoiceID;
            $transaction['resellerID']         = $this->resellerID;
            $transaction['gateway']            = $gatewayName;

            $transaction = alterTransactionCode($transaction);
            $CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transaction);

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>');
        }

        redirect(base_url('Xero_controllers/Xero_invoice/Invoice_detailes'), 'refresh');
    }

    public function pay_old_invoice()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $invoiceID  = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway');
        $cusproID   = '';
        $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        $m_data     = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
        $resellerID = $m_data['resellerID'];

        $in_data   = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);
        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

        $nmiuser     = $gt_result['gatewayUsername'];
        $nmipass     = $gt_result['gatewayPassword'];
        $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
        $gatewayName = getGatewayName($gt_result['gatewayType']);
        $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

        $error = '';
        if ($cardID != "" || $gateway != "") {

            if (!empty($in_data)) {

                $customerID = $in_data['CustomerListID'];
                $con_cust   = array('Customer_ListID' => $customerID, 'merchantID' => $merchID);
                $comp_data  = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                $companyID  = $comp_data['companyID'];
                if ($cardID == 'new1') {
                    $cardID_upd   = $cardID;
                    $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
                    $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv          = $this->czsecurity->xssCleanPostInput('cvv');
                    $cardType       = $this->general_model->getType($card_no);
                    $friendlyname   =  $cardType.' - '.substr($card_no,-4);
                } else {
                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no   = $card_data['CardNo'];
                    $cvv       = $card_data['CardCVV'];
                    $expmonth  = $card_data['cardMonth'];
                    $exyear    = $card_data['cardYear'];

                }

                $ref_number = array();
                if (!empty($cardID)) {

                    if ($in_data['BalanceRemaining'] > 0) {
                        $cr_amount = 0;
                        $amount    = $in_data['BalanceRemaining'];
                        $amount    = $this->czsecurity->xssCleanPostInput('inv_amount');
                        $amount    = $amount - $cr_amount;

                        $transaction1 = new nmiDirectPost($nmi_data);
                        $transaction1->setCcNumber($card_no);

                        $exyear = substr($exyear, 2);
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $expry = $expmonth . $exyear;
                        $transaction1->setCcExp($expry);
                        $transaction1->setCvv($cvv);
                        $transaction1->setAmount($amount);
                        // add level III data
                        $level_request_data = [
                            'transaction' => $transaction1,
                            'card_no' => $card_no,
                            'merchID' => $merchID,
                            'amount' => $amount,
                            'invoice_id' => $invoiceID,
                            'gateway' => 1
                        ];
                        $transaction1 = addlevelThreeDataInTransaction($level_request_data);
                        $transaction1->sale();
                        $result = $transaction1->execute();
                        //     print_r( $result);
                        $inID    = '';
                        $crtxnID = '';

                        if ($result['response_code'] == "100") {

                            $val = array(
                                'merchantID' => $merchID,
                            );
                            $data = $this->general_model->get_row_data('QBO_token', $val);

                            $accessToken  = $data['accessToken'];
                            $refreshToken = $data['refreshToken'];
                            $realmID      = $data['realmID'];
                            $dataService  = DataService::Configure(array(
                                'auth_mode'       => $this->config->item('AuthMode'),
                                'ClientID'        => $this->config->item('client_id'),
                                'ClientSecret'    => $this->config->item('client_secret'),
                                'accessTokenKey'  => $accessToken,
                                'refreshTokenKey' => $refreshToken,
                                'QBORealmID'      => $realmID,
                                'baseUrl'         => $this->config->item('QBOURL'),
                            ));

                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                            $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");


                            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                $theInvoice = current($targetInvoiceArray);

                                $updatedInvoice = Invoice::update($theInvoice, [
                                    "sparse" => 'true',
                                ]);

                                $updatedResult = $dataService->Update($updatedInvoice);

                                $newPaymentObj = Payment::create([
                                    "TotalAmt"    => $amount,
                                    "SyncToken"   => $updatedResult->SyncToken,
                                    "CustomerRef" => $updatedResult->CustomerRef,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $updatedResult->Id,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount,
                                    ],
                                ]);

                                $savedPayment = $dataService->Add($newPaymentObj);

                                $crtxnID = $savedPayment->Id;

                                $error = $dataService->getLastError();

                                if ($error != null) {

                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $result['transactionid'];

                                } else {
                                    if ($chh_mail == '1') {

                                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchID);
                                        $ref_number     = $in_data['refNumber'];
                                        $tr_date        = date('Y-m-d H:i:s');
                                        $toEmail        = $comp_data['userEmail'];
                                        $company        = $comp_data['companyName'];
                                        $customer       = $comp_data['fullName'];
                                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                                    }

                                    $st     = '1';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success";
                                    $qbID   = $result['transactionid'];

                                    $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                }

                                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                                    $cardType       = $this->general_model->getType($card_no);
                                    $friendlyname   =  $cardType.' - '.substr($card_no,-4);

                                    $cardType       = $this->general_model->getType($card_no);
                                    $card_condition = array(
                                        'customerListID'           => $this->czsecurity->xssCleanPostInput('customerID'),
                                        'customerCardfriendlyName' => $friendlyname,
                                    );
                                    $cid      = $this->czsecurity->xssCleanPostInput('customerID');
                                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                    $query = $this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

                                    $crdata = $query->row_array()['numrow'];

                                    if ($crdata > 0) {

                                        $card_data = array('cardMonth' => $expmonth,
                                            'cardYear'                     => $exyear,
                                            'CardType'                     => $cardType,
                                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                            'CardCVV'                      => '', 
                                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'               => $in_data['Customer_ListID'],
                                            'customerCardfriendlyName'     => $friendlyname,
                                            'companyID'                    => $companyID,
                                            'merchantID'                   => $merchID,
                                            'updatedAt'                    => date("Y-m-d H:i:s"));

                                        $this->db1->update('customer_card_data', $card_condition, $card_data);
                                    } else {
                                        $is_default = 0;
                                        $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
                                        if($checkCustomerCard == 0){
                                            $is_default = 1;
                                        }
                                        $card_data = array('cardMonth' => $expmonth,
                                            'cardYear'                     => $exyear,
                                            'CardType'                     => $cardType,
                                            'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                            'CardCVV'                      => '',
                                            'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                            'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                            'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                            'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                            'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                            'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                            'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                            'customerListID'               => $in_data['Customer_ListID'],
                                            'customerCardfriendlyName'     => $friendlyname,
                                            'companyID'                    => $companyID,
                                            'merchantID'                   => $merchID,
                                            'is_default'               => $is_default,
                                            'createdAt'                    => date("Y-m-d H:i:s"));


                                        $id1 = $this->db1->insert('customer_card_data', $card_data);

                                    }

                                }

                                $invoiceID = $updatedResult->Id;
                            } else {
                                $st     = '0';
                                $action = 'Pay Invoice';
                                $msg    = "Payment Success but " . $error->getResponseBody();
                                $qbID   = $result['transactionid'];
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Invalid invoice.</div>');

                            }
                        } else {
                            $st     = '0';
                            $action = 'Pay Invoice';
                            $msg    = "Payment Failed";
                            $qbID   = $invoiceID;

                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
                        }
                        $qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                        $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                        $transaction['transactionID']       = $result['transactionid'];
                        $transaction['transactionStatus']   = $result['responsetext'];
                        $transaction['transactionCode']     = $result['response_code'];
                        $transaction['transactionType']     = ($result['type']) ? $result['type'] : 'auto-nmi';
                        $transaction['transactionDate']     = date('Y-m-d H:i:s');
                        $transaction['transactionModified'] = date('Y-m-d H:i:s');
                        $transaction['gatewayID']          = $gateway;
                        $transaction['transactionGateway'] = $gt_result['gatewayType'];
                        $transaction['customerListID']     = $in_data['CustomerListID'];
                        $transaction['transactionAmount']  = $amount;
                        $transaction['merchantID']         = $merchID;

                        $transaction['qbListTxnID'] = $crtxnID;

                        $transaction['resellerID'] = $this->resellerID;
                        $transaction['gateway']    = $gatewayName;
                        $transaction['invoiceID']  = $invoiceID;

                        $transaction = alterTransactionCode($transaction);
                        $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
                        $id = $this->general_model->insert_row('customer_transaction', $transaction);

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
            }

        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

    public function get_vault_data($customerID)
    {
        $card = array();

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $sql       = "SELECT * from customer_card_data  where  customerListID='$customerID'  and  merchantID ='$merchID' order by CardID desc limit 1  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']                   = $this->card_model->decrypt($card_data['CustomerCard']);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->card_model->decrypt($card_data['CardCVV']);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function delete_card_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }
        if (!empty($this->czsecurity->xssCleanPostInput('delCardID'))) {

            $cardID   = $this->czsecurity->xssCleanPostInput('delCardID');
            $customer = $this->czsecurity->xssCleanPostInput('delCustodID');

            $num = $this->general_model->get_num_rows('tbl_subscriptions_qbo', array('CardID' => $cardID, 'merchantDataID' => $merchID));
            if ($num == 0) {
                $sts = $this->card_model->delete_card_data(array('CardID' => $cardID));
                if ($sts) {
                    $this->session->set_flashdata('success', 'Successfully Deleted');} else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error Invalid Card ID</strong></div>');}
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Subscription Transaction Failed -  This card cannot be deleted. Please change Customer card on Subscription</strong></div>');
            }
            redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');

        }
    }

    

    public function update_card_data()
    {

       
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');
        $CardType = 'Visa';
        $qq = $this->db1->select('customerListID','CardType')->from('customer_card_data')->where(array('CardID' => $cardID, 'merchantID' => $merchID))->get();
        if ($qq->num_rows > 0) {
            $customer = $qq->row_array()['customerListID'];
            $CardType = $qq->row_array()['CardType'];
        }
        if ($CardType != 'Echeck')  {

            $expmonth     = $this->czsecurity->xssCleanPostInput('edit_expiry');
            $exyear       = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
            if ($this->czsecurity->xssCleanPostInput('edit_cvv') != '') {
                $cvv      = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('edit_cvv'));
            }else{
                $cvv      = null;
            }
            
            $friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';

        }

        if ($this->czsecurity->xssCleanPostInput('edit_acc_number') !== '') {
            $acc_number       = $this->czsecurity->xssCleanPostInput('edit_acc_number');
            $route_number     = $this->czsecurity->xssCleanPostInput('edit_route_number');
            $acc_name         = $this->czsecurity->xssCleanPostInput('edit_acc_name');
            $secCode          = $this->czsecurity->xssCleanPostInput('edit_secCode');
            $acct_type        = $this->czsecurity->xssCleanPostInput('edit_acct_type');
            $acct_holder_type = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
            $expmonth         = $exyear         = $cvv         = 0;
            $card_no          = $friendlyname          = '';

        }

        $cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');

        $expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
        $exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
        $friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
        $b_addr1      = $this->czsecurity->xssCleanPostInput('baddress1');
        $b_addr2      = $this->czsecurity->xssCleanPostInput('baddress2');
        $b_city       = $this->czsecurity->xssCleanPostInput('bcity');
        $b_state      = $this->czsecurity->xssCleanPostInput('bstate');
        $b_country    = $this->czsecurity->xssCleanPostInput('bcountry');
        $b_contact    = $this->czsecurity->xssCleanPostInput('bcontact');
        $b_zip        = $this->czsecurity->xssCleanPostInput('bzipcode');
        $merchantID   = $merchID;
        $is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;

        $condition    = array('CardID' => $cardID);
        $insert_array = array('cardMonth' => $expmonth,
            'cardYear'                        => $exyear,

            'CardCVV'                         => $cvv,
            'Billing_Addr1'                   => $b_addr1,
            'Billing_Addr2'                   => $b_addr2,
            'Billing_City'                    => $b_city,
            'Billing_State'                   => $b_state,
            'Billing_Zipcode'                 => $b_zip,
            'Billing_Country'                 => $b_country,
            'Billing_Contact'                 => $b_contact,
            'accountNumber'                   => $acc_number,
            'routeNumber'                     => $route_number,
            'accountName'                     => $acc_name,
            'accountType'                     => $acct_type,
            'accountHolderType'               => $acct_holder_type,
            'secCodeEntryMethod'              => $secCode,
            'is_default'         => $is_default,
            'updatedAt'                       => date("Y-m-d H:i:s"));
        if($friendlyname != ''){
            $insert_array['customerCardfriendlyName'] = $friendlyname;
        }

        if ($this->czsecurity->xssCleanPostInput('edit_card_number') != '') {
            $card_no                      = $this->czsecurity->xssCleanPostInput('edit_card_number');
            $insert_array['CustomerCard'] = $this->card_model->encrypt($card_no);
            $card_type                    = $this->general_model->getType($card_no);
            $insert_array['CardType']     = $card_type;

        }
        if($is_default == 1){
            $conditionDefaultSet = array('customerListID' => $customer, 'merchantID' => $merchID);
            $updateData = $this->card_model->update_customer_card_data($conditionDefaultSet,['is_default' => 0]);
        }
        $id = $this->card_model->update_card_data($condition, $insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Updated Card');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
        }

        redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
    }

    public function insert_new_data()
    {

        $customer = $this->czsecurity->xssCleanPostInput('customerID11');

        $c_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $customer));

        $companyID = $c_data['companyID'];

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        if ($this->czsecurity->xssCleanPostInput('formselector') == '1') {
            $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
            $card_type    = $this->general_model->getType($card_no);
            $card_no      = $this->card_model->encrypt($card_no);
            $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
            $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
            if ($this->czsecurity->xssCleanPostInput('cvv') != '') {
                $cvv      = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
            }else{
                $cvv      = null;
            }
            
            $friendlyname   =  $card_type.' - '.substr($card_no,-4);
            $acc_number   = $route_number   = $acc_name   = $secCode   = $acct_type   = $acct_holder_type   = '';
        }
        if ($this->czsecurity->xssCleanPostInput('formselector') == '2') {
            $acc_number       = $this->czsecurity->xssCleanPostInput('acc_number');
            $route_number     = $this->czsecurity->xssCleanPostInput('route_number');
            $acc_name         = $this->czsecurity->xssCleanPostInput('acc_name');
            $secCode          = $this->czsecurity->xssCleanPostInput('secCode');
            $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
            $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
            $expmonth         = $exyear         = $cvv         = 0;
            $card_no          = $friendlyname          = '';
            $card_type        = 'Echeck';
        }

        $b_addr1   = $this->czsecurity->xssCleanPostInput('address1');
        $b_addr2   = $this->czsecurity->xssCleanPostInput('address2');
        $b_city    = $this->czsecurity->xssCleanPostInput('city');
        $b_state   = $this->czsecurity->xssCleanPostInput('state');
        $b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
        $b_country = $this->czsecurity->xssCleanPostInput('country');
        $b_contact = $this->czsecurity->xssCleanPostInput('contact');

        $is_default = ($this->czsecurity->xssCleanPostInput('defaultMethod') != null )?$this->czsecurity->xssCleanPostInput('defaultMethod'):0;
       
        $insert_array = array('cardMonth' => $expmonth,
            'cardYear'                        => $exyear,
            'CustomerCard'                    => $card_no,
            'CardCVV'                         => $cvv,
            'CardType'                        => $card_type,
            'customerListID'                  => $customer,
            'merchantID'                      => $merchID,
            'companyID'                       => $companyID,

            'Billing_Addr1'                   => $b_addr1,
            'Billing_Addr2'                   => $b_addr2,
            'Billing_City'                    => $b_city,
            'Billing_State'                   => $b_state,
            'Billing_Zipcode'                 => $b_zipcode,
            'Billing_Country'                 => $b_country,
            'Billing_Contact'                 => $b_contact,
            'customerCardfriendlyName'        => $friendlyname,
            'accountNumber'                   => $acc_number,
            'routeNumber'                     => $route_number,
            'accountName'                     => $acc_name,
            'accountType'                     => $acct_type,
            'accountHolderType'               => $acct_holder_type,
            'secCodeEntryMethod'              => $secCode,
            'createdAt'                       => date("Y-m-d H:i:s"));
        if($is_default == 1){
            $condition = array('customerListID' => $customer, 'merchantID' => $merchID);
            
            $updateData = $this->card_model->update_customer_card_data($condition,['is_default' => 0]);
            
        }else{
            $checkCustomerCard = $this->card_model->get_customer_card_data($customer);
            if(count($checkCustomerCard) == 0){
                $is_default == 1;
            }
            
        }
        $insert_array['is_default'] = $is_default;
        $id = $this->card_model->insertBillingdata($insert_array);
        if ($id) {
            $this->session->set_flashdata('success', 'Successfully Inserted Card');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect('QBO_controllers/home/view_customer/' . $customer, 'refresh');
    }

    public function create_customer_old_sale()
    {

        if (!empty($this->input->post(null, true))) {
            $inv_array   = array();
            $inv_invoice = array();
            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $resellerID = $this->resellerID;
            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            if ($gatlistval != "" && !empty($gt_result)) {
                $nmiuser     = $gt_result['gatewayUsername'];
                $nmipass     = $gt_result['gatewayPassword'];
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                $gatewayName = getGatewayName($gt_result['gatewayType']);
                $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

                $invoiceIDs = array();
                if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                    $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                }

                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

               
                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
                $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                $companyID = $comp_data['companyID'];

                $transaction = new nmiDirectPost($nmi_data);

                if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == 'new1') {
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear = substr($exyear, 2);
                    $expry  = $expmonth . $exyear;
                    $transaction->setCcExp($expry);
                    $transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));

                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no = $card_data['CardNo'];

                    $transaction->setCcNumber($card_data['CardNo']);
                    $expmonth = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    $exyear = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear;
                    $transaction->setCcExp($expry);
                    $transaction->setCvv($card_data['CardCVV']);
                }
                $transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->setFirstName($this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
                $transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
                $transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
                $transaction->setState($this->czsecurity->xssCleanPostInput('state'));
                $transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
                $transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

                $transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $transaction->setAmount($amount);

                // add level III data
                $level_request_data = [
                    'transaction' => $transaction,
                    'card_no' => $card_no,
                    'merchID' => $merchantID,
                    'amount' => $amount,
                    'invoice_id' => '',
                    'gateway' => 1
                ];
                $transaction = addlevelThreeDataInTransaction($level_request_data);

                $transaction->setTax('tax');
                $transaction->sale();
                $result = $transaction->execute();

                if ($result['response_code'] == '100') {

                    
                    $val  = array('merchantID' => $merchantID);
                    $data = $this->general_model->get_row_data('QBO_token', $val);

                    $accessToken  = $data['accessToken'];
                    $refreshToken = $data['refreshToken'];
                    $realmID      = $data['realmID'];

                    $dataService = DataService::Configure(array(
                        'auth_mode'       => $this->config->item('AuthMode'),
                        'ClientID'        => $this->config->item('client_id'),
                        'ClientSecret'    => $this->config->item('client_secret'),
                        'accessTokenKey'  => $accessToken,
                        'refreshTokenKey' => $refreshToken,
                        'QBORealmID'      => $realmID,
                        'baseUrl'         => $this->config->item('QBOURL'),
                    ));

                    if (!empty($invoiceIDs)) {

                        foreach ($invoiceIDs as $inID) {
                            $theInvoice = array();

                            $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                            $invse              = $inID;
                            $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $invse . "'");

                           
                            if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                $theInvoice = current($targetInvoiceArray);

                                $updatedInvoice = Invoice::update($theInvoice, [
                                    
                                    "sparse" => 'true',
                                ]);
                                $con           = array('invoiceID' => $invse, 'merchantID' => $merchantID);
                                $res           = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);
                                $amount_data   = $res['BalanceRemaining'];
                                $refNum[]      = $res['refNumber'];
                                $amount1       = $res['BalanceRemaining'] + $res['Total_payment'];
                                $updatedResult = $dataService->Update($updatedInvoice);
                                $newPaymentObj = Payment::create([
                                    "TotalAmt"    => $amount1,
                                    "SyncToken"   => $updatedResult->SyncToken,
                                    "CustomerRef" => $updatedResult->CustomerRef,
                                    "Line"        => [
                                        "LinkedTxn" => [
                                            "TxnId"   => $updatedResult->Id,
                                            "TxnType" => "Invoice",
                                        ],
                                        "Amount"    => $amount_data,
                                    ],
                                ]);

                                $savedPayment = $dataService->Add($newPaymentObj);

                                $error = $dataService->getLastError();
                                if ($error != null) {
                                    $err = '';
                                    $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                    $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                    $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' . $err . '</div>');

                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Success but " . $error->getResponseBody();
                                    $qbID   = $result['transactionid'];

                                } else {
                                    $pinv_id = '';
                                    $pinv_id = $savedPayment->Id;
                                    $st      = '1';
                                    $action  = 'Pay Invoice';
                                    $msg     = "Payment Success";
                                    $qbID    = $result['transactionid'];
                                    if ($chh_mail == '1') {

                                        $condition_mail = array('templateType' => '5', 'merchantID' => $merchantID);
                                        $ref_number     = implode(',', $refNum);
                                        $tr_date        = date('Y-m-d H:i:s');
                                        $toEmail        = $comp_data['userEmail'];
                                        $company        = $comp_data['companyName'];
                                        $customer       = $comp_data['fullName'];
                                        $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                                    }
                                    $this->session->set_flashdata('success', 'Transaction Successful');

                                }
                                $qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchantID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                $transactiondata                        = array();
                                $transactiondata['transactionID']       = $result['transactionid'];
                                $transactiondata['transactionStatus']   = $result['responsetext'];
                                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                                $transactiondata['transactionCode']     = $result['response_code'];

                                $transactiondata['transactionType']    = $result['type'];
                                $transactiondata['gatewayID']          = $gatlistval;
                                $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                                $transactiondata['customerListID']     = $customerID;
                                $transactiondata['transactionAmount']  = $amount_data;
                                $transactiondata['merchantID']         = $merchantID;
                                $transactiondata['invoiceID']          = $invse;
                                if (!empty($pinv_id)) {
                                    $transactiondata['qbListTxnID'] = $pinv_id;
                                }

                                $transactiondata['resellerID'] = $this->resellerID;
                                $transactiondata['gateway']    = $gatewayName;

                                $transactiondata = alterTransactionCode($transactiondata);
                                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                                if(!empty($this->transactionByUser)){
                                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                }
                                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                            }
                        }

                    } else {
                        $amount_data                            = $amount;
                        $transactiondata                        = array();
                        $transactiondata['transactionID']       = $result['transactionid'];
                        $transactiondata['transactionStatus']   = $result['responsetext'];
                        $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                        $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                        $transactiondata['transactionCode']     = $result['response_code'];

                        $transactiondata['transactionType']    = $result['type'];
                        $transactiondata['gatewayID']          = $gatlistval;
                        $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                        $transactiondata['customerListID']     = $customerID;
                        $transactiondata['transactionAmount']  = $amount_data;
                        $transactiondata['merchantID']         = $merchantID;
                        $transactiondata['resellerID']         = $this->resellerID;
                        $transactiondata['gateway']            = $gatewayName;


                        $transactiondata = alterTransactionCode($transactiondata);
                        $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
                        $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

                    }

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == "new1" && $tc == 0) {
                        $this->load->library('encrypt');
                        $card_no        = $this->czsecurity->xssCleanPostInput('card_number');
                        $cardType       = $this->general_model->getType($card_no);
                        $friendlyname   =  $cardType.' - '.substr($card_no,-4);
                        
                        $card_condition = array(
                            'customerListID'           => $this->czsecurity->xssCleanPostInput('customerID'),
                            'customerCardfriendlyName' => $friendlyname,
                        );
                        $cid      = $this->czsecurity->xssCleanPostInput('customerID');
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                        $query = $this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and merchantID='" . $merchantID . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

                        $crdata = $query->row_array()['numrow'];

                        if ($crdata > 0) {
                            $card_type = $this->general_model->getType($card_no);
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CardType'                     => $card_type,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '', 
                                'updatedAt'                    => date("Y-m-d H:i:s"),
                            );

                            $this->db1->update('customer_card_data', $card_condition, $card_data);
                        } else {
                            $card_type = $this->general_model->getType($card_no);
                            $customerListID = $this->czsecurity->xssCleanPostInput('customerID');
                            $is_default = 0;
                            $checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
                            if($checkCustomerCard == 0){
                                $is_default = 1;
                            }
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CardType'                     => $card_type,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'customerListID'               => $customerListID,
                                'companyID'                    => $companyID,
                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                                'merchantID'                   => $merchantID,
                                'is_default'                   => $is_default,
                                'customerCardfriendlyName'     => $friendlyname,
                                'createdAt'                    => date("Y-m-d H:i:s"));

                            $id1 = $this->db1->insert('customer_card_data', $card_data);

                        }

                    }
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
                    $transactiondata                        = array();
                    $transactiondata['transactionID']       = $result['transactionid'];
                    $transactiondata['transactionStatus']   = $result['responsetext'];
                    $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                    $transactiondata['transactionCode']     = $result['response_code'];

                    $transactiondata['transactionType']    = $result['type'];
                    $transactiondata['gatewayID']          = $gatlistval;
                    $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                    $transactiondata['customerListID']     = $customerID;
                    $transactiondata['transactionAmount']  = $amount;
                    $transactiondata['merchantID']         = $merchantID;
                    if (!empty($invoiceIDs)) {
                        $transactiondata['invoiceID'] = implode(',', $invoiceIDs);

                    }
                    $transactiondata['resellerID'] = $this->resellerID;
                    $transactiondata['gateway']    = $gatewayName;


                    $transactiondata = alterTransactionCode($transactiondata);
                    $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                    if(!empty($this->transactionByUser)){
                        $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                        $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                    }
                    $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
                }

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');

            }

            redirect('QBO_controllers/Payments/create_customer_sale', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];

        $compdata          = $this->xero_customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_sale', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_customer_sale()
    {

        if ($this->session->userdata('logged_in')) {

            $user_id = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $user_id = $this->session->userdata('user_logged_in')['merchantID'];
        }
        if (!empty($this->input->post(null, true))) {
            
            $this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
            $this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
            $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');

            if ($this->czsecurity->xssCleanPostInput('cardID') == "new1") {

               
                $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                
                $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');

            }

            if ($this->form_validation->run() == true) {

                $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));
                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                if (!empty($gt_result)) {
                    $nmiuser    = $gt_result['gatewayUsername'];
                    $nmipass    = $gt_result['gatewayPassword'];
                    $nmi_data   = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                    $invoiceIDs = array();
                    if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                        $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                    }

                    $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                    $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $user_id);
                    $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
                    $companyID = $comp_data['companyID'];

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $cvv = '';
                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == 'new1') {
                        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                        if ($this->czsecurity->xssCleanPostInput('cvv') != "") {
                            $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                        }

                    } else {
                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no   = $card_data['CardNo'];
                        $expmonth  = $card_data['cardMonth'];
                        $exyear    = $card_data['cardYear'];
                        if ($card_data['CardCVV']) {
                            $cvv = $card_data['CardCVV'];
                        }

                        $cardType = $card_data['CardType'];
                        $address1 = $card_data['Billing_Addr1'];
                        $city     = $card_data['Billing_City'];
                        $zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
                        $country  = $card_data['Billing_Country'];

                    }
                    $address1 = $this->czsecurity->xssCleanPostInput('address1');
                    $address2 = $this->czsecurity->xssCleanPostInput('address2');
                    $city     = $this->czsecurity->xssCleanPostInput('city');
                    $country  = $this->czsecurity->xssCleanPostInput('country');
                    $phone    = $this->czsecurity->xssCleanPostInput('phone');
                    $state    = $this->czsecurity->xssCleanPostInput('state');
                    $zipcode  = $this->czsecurity->xssCleanPostInput('zipcode');

                    $transaction = new nmiDirectPost($nmi_data);

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $this->czsecurity->xssCleanPostInput('card_list') == 'new1') {
                        $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                        $transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                        $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $exyear = substr($exyear, 2);
                        $expry  = $expmonth . $exyear;
                        $transaction->setCcExp($expry);
                        $transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));

                    } else {

                        $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                        $card_data = $this->card_model->get_single_card_data($cardID);
                        $card_no = $card_data['CardNo'];
                        $transaction->setCcNumber($card_data['CardNo']);
                        $expmonth = $card_data['cardMonth'];

                        $exyear = $card_data['cardYear'];
                        $exyear = substr($exyear, 2);
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $expry = $expmonth . $exyear;
                        $transaction->setCcExp($expry);
                        $transaction->setCvv($card_data['CardCVV']);
                    }
                    $transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
                    $transaction->setFirstName($this->czsecurity->xssCleanPostInput('fistName'));
                    $transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
                    $transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
                    $transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
                    $transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
                    $transaction->setState($this->czsecurity->xssCleanPostInput('state'));
                    $transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
                    $transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

                    $transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
                    $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                    $transaction->setAmount($amount);
                    $transaction->sale();

                    // add level III data
                    $level_request_data = [
                        'transaction' => $transaction,
                        'card_no' => $card_no,
                        'merchID' => $user_id,
                        'amount' => $amount,
                        'invoice_id' => '',
                        'gateway' => 1
                    ];
                    $transaction = addlevelThreeDataInTransaction($level_request_data);
                    $result = $transaction->execute();

                    if ($result['response_code'] == '100') {

                        $trID       = $result['transactionid'];
                        $invoiceIDs = array();
                        if (!empty($this->czsecurity->xssCleanPostInput('invoice_id'))) {
                            $invoiceIDs = explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
                        }

                        $val  = array('merchantID' => $user_id);
                        $data = $this->general_model->get_row_data('QBO_token', $val);

                        $accessToken  = $data['accessToken'];
                        $refreshToken = $data['refreshToken'];
                        $realmID      = $data['realmID'];

                        $dataService = DataService::Configure(array(
                            'auth_mode'       => $this->config->item('AuthMode'),
                            'ClientID'        => $this->config->item('client_id'),
                            'ClientSecret'    => $this->config->item('client_secret'),
                            'accessTokenKey'  => $accessToken,
                            'refreshTokenKey' => $refreshToken,
                            'QBORealmID'      => $realmID,
                            'baseUrl'         => $this->config->item('QBOURL'),
                        ));

                        $refNumber = array();

                        if (!empty($invoiceIDs)) {

                            foreach ($invoiceIDs as $inID) {
                                $theInvoice = array();

                                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
                                $targetInvoiceArray = $dataService->Query("select * from Invoice WHERE Id = '" . $inID . "'");

                                $con         = array('invoiceID' => $inID, 'merchantID' => $user_id);
                                $res1        = $this->general_model->get_select_data('QBO_test_invoice', array('BalanceRemaining', 'refNumber', 'Total_payment'), $con);
                                $amount_data = $res1['BalanceRemaining'];
                                $refNumber[] = $res1['refNumber'];
                                $txnID       = $inID;
                                $ispaid      = 1;

                                $app_amount = $res1['Total_payment'] + $amount;
                                $data       = array('IsPaid' => $ispaid, 'Total_payment' => ($app_amount), 'BalanceRemaining' => '0.00');

                                $this->general_model->update_row_data('QBO_test_invoice', $con, $data);

                                if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                    $theInvoice = current($targetInvoiceArray);

                                    $updatedInvoice = Invoice::update($theInvoice, [
                                        "sparse" => 'true',
                                    ]);

                                    $updatedResult = $dataService->Update($updatedInvoice);

                                    $newPaymentObj = Payment::create([
                                        "TotalAmt"    => $app_amount,
                                        "SyncToken"   => $updatedResult->SyncToken,
                                        "CustomerRef" => $customerID,
                                        "Line"        => [
                                            "LinkedTxn" => [
                                                "TxnId"   => $inID,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount"    => $amount_data,
                                        ],
                                    ]);

                                    $savedPayment = $dataService->Add($newPaymentObj);

                                    $error = $dataService->getLastError();
                                    if ($error != null) {
                                        $err = '';
                                        $err .= "The Status code is: " . $error->getHttpStatusCode() . "\n";
                                        $err .= "The Helper message is: " . $error->getOAuthHelperError() . "\n";
                                        $err .= "The Response message is: " . $error->getResponseBody() . "\n";
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong>' . $err . '</div>');

                                        $st     = '0';
                                        $action = 'Pay Invoice';
                                        $msg    = "Payment Success but " . $error->getResponseBody();

                                        $qbID    = $trID;
                                        $pinv_id = '';
                                    } else {
                                        $pinv_id = '';
                                        $pinv_id = $savedPayment->Id;
                                        $st      = '1';
                                        $action  = 'Pay Invoice';
                                        $msg     = "Payment Success";
                                        $qbID    = $trID;
                                        $this->session->set_flashdata('success', 'Transaction Successful');

                                    }
                                    $qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $user_id, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                    $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                    $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount_data, $user_id, $pinv_id, $this->resellerID, $inID = '', false, $this->transactionByUser);

                                }
                            }

                        } else {

                            $crtxnID = '';
                            $inID    = '';
                            $id      = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);
                        }

                        if ($chh_mail == '1') {

                            $condition_mail = array('templateType' => '5', 'merchantID' => $user_id);
                            if (!empty($refNumber)) {
                                $ref_number = implode(',', $refNumber);
                            } else {
                                $ref_number = '';
                            }

                            $tr_date  = date('Y-m-d H:i:s');
                            $toEmail  = $comp_data['userEmail'];
                            $company  = $comp_data['companyName'];
                            $customer = $comp_data['fullName'];
                            $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date);
                        }

                        if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                            
                            $cardType       = $this->general_model->getType($card_no);
                            $friendlyname   =  $cardType.' - '.substr($card_no,-4);
                            $card_condition = array(
                                'customerListID'           => $customerID,
                                'customerCardfriendlyName' => $friendlyname,
                            );

                            $crdata = $this->card_model->check_friendly_name($customerID, $friendlyname);

                            if (!empty($crdata)) {

                                $card_data = array('cardMonth' => $expmonth,
                                    'cardYear'                     => $exyear,
                                    'companyID'                    => $companyID,
                                    'merchantID'                   => $user_id,
                                    'CardType'                     => $this->general_model->getType($card_no),
                                    'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                    'CardCVV'                      => '', 
                                    'updatedAt'                    => date("Y-m-d H:i:s"),
                                    'Billing_Addr1'                => $address1,

                                    'Billing_City'                 => $city,
                                    'Billing_State'                => $state,
                                    'Billing_Country'              => $country,
                                    'Billing_Contact'              => $contact,
                                    'Billing_Zipcode'              => $zipcode,
                                );

                                $this->card_model->update_card_data($card_condition, $card_data);
                            } else {
                                $card_data = array('cardMonth' => $expmonth,
                                    'cardYear'                     => $exyear,
                                    'CardType'                     => $this->general_model->getType($card_no),
                                    'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                    'CardCVV'                      => '', 
                                    'customerListID'               => $customerID,
                                    'companyID'                    => $companyID,
                                    'merchantID'                   => $user_id,
                                    'customerCardfriendlyName'     => $friendlyname,
                                    'createdAt'                    => date("Y-m-d H:i:s"),
                                    'Billing_Addr1'                => $address1,

                                    'Billing_City'                 => $city,
                                    'Billing_State'                => $state,
                                    'Billing_Country'              => $country,
                                    'Billing_Contact'              => $contact,
                                    'Billing_Zipcode'              => $zipcode,
                                );

                                $id1 = $this->card_model->insert_card_data($card_data);

                            }

                        }
                        $this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');

                    } else {
                        $crtxnID = '';
                        $msg = $result['responsetext'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>');
                        $id = $this->general_model->insert_gateway_transaction_data($result, 'sale', $gateway, $gt_result['gatewayType'], $customerID, $amount, $user_id, $crtxnID, $this->resellerID, $inID = '', false, $this->transactionByUser);

                    }

                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>');
                }

            } else {

                $error = 'Validation Error. Please fill the requred fields';
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>' . $error . '</strong></div>');
            }

        }
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $plantype            = $this->general_model->chk_merch_plantype_status($user_id);
        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];

        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }
        $compdata          = $this->xero_customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;
        $data['plantype']  = $plantype;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_sale', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

/*****************Authorize Transaction***************/

    public function create_customer_auth()
    {
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }

            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                $nmiuser     = $gt_result['gatewayUsername'];
                $nmipass     = $gt_result['gatewayPassword'];
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                $gatewayName = getGatewayName($gt_result['gatewayType']);
                $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

                $comp_data  = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
                $companyID  = $comp_data['companyID'];
                $customerID = $this->czsecurity->xssCleanPostInput('customerID');

                $transaction = new nmiDirectPost($nmi_data);

                if ($this->czsecurity->xssCleanPostInput('card_number') != "") {
                    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                    $transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');

                    $exyear = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $exyear = substr($exyear, 2);
                    $expry  = $expmonth . $exyear;
                    $transaction->setCcExp($expry);
                    $transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));

                } else {

                    $cardID = $this->czsecurity->xssCleanPostInput('card_list');

                    $card_data = $this->card_model->get_single_card_data($cardID);
                    $card_no = $card_data['CardNo'];

                    $transaction->setCcNumber($card_data['CardNo']);
                    $expmonth = $card_data['cardMonth'];

                    $exyear = $card_data['cardYear'];
                    $exyear = substr($exyear, 2);
                    if (strlen($expmonth) == 1) {
                        $expmonth = '0' . $expmonth;
                    }
                    $expry = $expmonth . $exyear;
                    $transaction->setCcExp($expry);
                    $transaction->setCvv($card_data['CardCVV']);
                }
                $transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->setFirstName($this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
                $transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
                $transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
                $transaction->setState($this->czsecurity->xssCleanPostInput('state'));
                $transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
                $transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

                $transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $transaction->setAmount($amount);

                // add level III data
                $level_request_data = [
                    'transaction' => $transaction,
                    'card_no' => $card_no,
                    'merchID' => $merchantID,
                    'amount' => $amount,
                    'invoice_id' => '',
                    'gateway' => 1
                ];
                $transaction = addlevelThreeDataInTransaction($level_request_data);
                $transaction->setTax('tax');
                $transaction->auth();
                $result = $transaction->execute();

                if ($result['response_code'] == '100') {

                    

                    /* This block is created for saving Card info in encrypted form  */

                    if ($this->czsecurity->xssCleanPostInput('card_number') != "") {

                        $this->load->library('encrypt');
                        $card_no        = $this->czsecurity->xssCleanPostInput('card_number');
                        $cardType       = $this->general_model->getType($card_no);
                        $friendlyname   =  $cardType.' - '.substr($card_no,-4);
                       
                        $card_condition = array(
                            'customerListID'           => $this->czsecurity->xssCleanPostInput('customerID'),
                            'customerCardfriendlyName' => $friendlyname,
                        );
                        $cid      = $this->czsecurity->xssCleanPostInput('customerID');
                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                        $query = $this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and merchantID='" . $merchantID . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

                        $crdata = $query->row_array()['numrow'];

                        if ($crdata > 0) {

                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'companyID'                    => $companyID,
                                'merchantID'                   => $merchantID,
                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '', 
                                'updatedAt'                    => date("Y-m-d H:i:s"),
                            );

                            $this->db1->update('customer_card_data', $card_condition, $card_data);
                        } else {
                            $is_default = 0;
                            $checkCustomerCard = checkCustomerCard($customerID,$merchantID);
                            if($checkCustomerCard == 0){
                                $is_default = 1;
                            }
                            $card_data = array('cardMonth' => $expmonth,
                                'cardYear'                     => $exyear,
                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                'CardCVV'                      => '',
                                'customerListID'               => $customerID,
                                'companyID'                    => $companyID,
                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('baddress1'),
                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('baddress2'),
                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('bcity'),
                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('bcountry'),
                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('bphone'),
                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('bstate'),
                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('bzipcode'),
                                'merchantID'                   => $merchantID,
                                'is_default'               => $is_default,
                                'customerCardfriendlyName'     => $friendlyname,
                                'createdAt'                    => date("Y-m-d H:i:s"));

                            $id1 = $this->db1->insert('customer_card_data', $card_data);

                        }

                    }
                    $this->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
                }

                $transactiondata                            = array();
                $transactiondata['transactionID']           = $result['transactionid'];
                $transactiondata['transactionStatus']       = $result['responsetext'];
                $transactiondata['transactionDate']         = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']         = $result['response_code'];
                $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
                $transactiondata['transactionType']         = $result['type'];
                $transactiondata['gatewayID']               = $gatlistval;
                $transactiondata['transaction_user_status'] = '5';
                $transactiondata['transactionGateway']      = $gt_result['gatewayType'];
                $transactiondata['customerListID']          = $customerID;
                $transactiondata['transactionAmount']       = $amount;
                $transactiondata['merchantID']              = $merchantID;
                $transactiondata['resellerID']              = $this->resellerID;
                $transactiondata['gateway']                 = $gatewayName;

                $transactiondata = alterTransactionCode($transactiondata);
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            redirect('QBO_controllers/Payments/payment_capture', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $plantype = $this->general_model->chk_merch_plantype_status($user_id);

        $condition           = array('merchantID' => $user_id);
        $gateway             = $this->general_model->get_gateway_data($user_id);
        $data['gateways']    = $gateway['gateway'];
        $data['gateway_url'] = $gateway['url'];
        $data['stp_user']    = $gateway['stripeUser'];
        $compdata            = $this->xero_customer_model->get_customers_data($user_id);
        $data['customers']   = $compdata;
        $data['plantype']    = $plantype;

        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_auth', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }
    /*****************Refund Transaction***************/
    public function create_customer_refund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $nmiuser     = $gt_result['gatewayUsername'];
            $nmipass     = $gt_result['gatewayPassword'];
            $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
            $gatewayName = getGatewayName($gt_result['gatewayType']);
            $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

            $transaction = new nmiDirectPost($nmi_data);
            $customerID  = $paydata['customerListID'];

           
            $total  = $this->czsecurity->xssCleanPostInput('ref_amount');
            $amount = $total;

            $transaction->setTransactionId($tID);

            $transaction->refund($tID, $amount);

            $result = $transaction->execute();

            if ($result['response_code'] == '100') {

                $val = array(
                    'merchantID' => $paydata['merchantID'],
                );
                $data = $this->general_model->get_row_data('QBO_token', $val);

                $accessToken  = $data['accessToken'];
                $refreshToken = $data['refreshToken'];
                $realmID      = $data['realmID'];
                $dataService  = DataService::Configure(array(
                    'auth_mode'       => $this->config->item('AuthMode'),
                    'ClientID'        => $this->config->item('client_id'),
                    'ClientSecret'    => $this->config->item('client_secret'),
                    'accessTokenKey'  => $accessToken,
                    'refreshTokenKey' => $refreshToken,
                    'QBORealmID'      => $realmID,
                    'baseUrl'         => $this->config->item('QBOURL'),
                ));

                $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                $refund  = $amount;
                $ref_inv = explode(',', $paydata['invoiceID']);
                if (!empty($paydata['invoiceID'])) {

                    $item_data = $this->xero_customer_model->get_invoice_item_data($paydata['invoiceID']);
                    if (!empty($item_data)) {
                        $LineObj = Line::create([
                            "Amount"              => $total,
                            "DetailType"          => "SalesItemLineDetail",
                            "SalesItemLineDetail" => [
                                "ItemRef" => $item_data['itemID'],
                            ],

                        ]);

                        if (!empty($item_data['AssetAccountRef'])) {
                            $acc_id   = $item_data['AssetAccountRef'];
                            $acc_name = $item_data['AssetAccountName'];
                        }
                        if (!empty($item_data['IncomeAccountRef'])) {
                            $acc_id   = $item_data['IncomeAccountRef'];
                            $acc_name = $item_data['IncomeAccountName'];
                        }
                        if (!empty($item_data['ExpenseAccountRef'])) {
                            $acc_id   = $item_data['ExpenseAccountRef'];
                            $acc_name = $item_data['ExpenseAccountName'];
                        }
                        $acc_data = $this->general_model->get_select_data('QBO_accounts_list', array('accountID'), array('accountName' => 'Undeposited Funds'));
                        if (!empty($acc_data)) {
                            $ac_value = $acc_data['accountID'];
                        } else {
                            $ac_value = '4';
                        }
                        $theResourceObj = RefundReceipt::create([
                            "CustomerRef"         => $paydata['customerListID'],
                            "Line"                => $LineObj,
                            "DepositToAccountRef" => [
                                "value" => $ac_value,
                                "name"  => "Undeposited Funds",
                            ],
                        ]);
                        $resultingObj = $dataService->Add($theResourceObj);
                        $error        = $dataService->getLastError();

                        $ins_id = '';
                        if ($error != null) {

                            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund ' . $error->getResponseBody() . '</strong></div>');
                        } else {
                            $ins_id = $resultingObj->Id;
                        }
                        $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                            'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                            'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                            'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                        );

                        $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                    }
                } else {

                    $ins_id    = '';
                    $refnd_trr = array('merchantID' => $paydata['merchantID'], 'refundAmount'       => $total,
                        'creditInvoiceID'               => $paydata['invoiceID'], 'creditTransactionID' => $tID,
                        'creditTxnID'                   => $ins_id, 'refundCustomerID'                  => $paydata['customerListID'],
                        'createdAt'                     => date('Y-m-d H:i:s'), 'updatedAt'             => date('Y-m-d H:i:s'),

                    );

                    $this->general_model->insert_row('tbl_customer_refund_transaction', $refnd_trr);
                }

              
                $this->xero_customer_model->update_refund_payment($tID, 'NMI');

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['transactionid'];
            $transactiondata['transactionStatus']   = $result['responsetext'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $result['type'];
            $transactiondata['transactionCode']     = $result['response_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            if (!empty($paydata['invoiceID'])) {
                $transactiondata['invoiceID'] = $paydata['invoiceID'];
            }
            $transactiondata['transactionAmount'] = $amount;
            $transactiondata['merchantID']        = $merchantID;
            $transactiondata['resellerID']        = $this->resellerID;
            $transactiondata['gateway']           = $gatewayName;
            $transactiondata = alterTransactionCode($transactiondata);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            if (!empty($this->czsecurity->xssCleanPostInput('payrefund'))) {
                redirect('QBO_controllers/Payments/payment_refund', 'refresh');
            } else {

                redirect('QBO_controllers/Payments/payment_transaction', 'refresh');

            }
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['merchID'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    /*****************Capture Transaction***************/

    public function create_customer_capture()
    {
        //Show a form here which collects someone's name and e-mail address

        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }
            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);
            if ($paydata['gatewayID'] > 0) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
                $gatlistval = $paydata['gatewayID'];

                $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

                if ($this->czsecurity->xssCleanPostInput('setMail')) {
                    $chh_mail = 1;
                } else {
                    $chh_mail = 0;
                }

                $nmiuser     = $gt_result['gatewayUsername'];
                $nmipass     = $gt_result['gatewayPassword'];
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                $gatewayName = getGatewayName($gt_result['gatewayType']);
                $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

                $transaction = new nmiDirectPost($nmi_data);

                $customerID = $paydata['customerListID'];
                $amount    = $paydata['transactionAmount'];
                $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
                $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);

                $transaction->setTransactionId($tID);
                $transaction->setAmount($amount);
                if ($paydata['transactionType'] == 'auth') {

                    $transaction->capture($tID, $amount);
                }
                $result = $transaction->execute();

                if ($result['response_code'] == '100') {

                    $condition = array('transactionID' => $tID);

                    $update_data = array('transaction_user_status' => "4");

                    $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                    if ($chh_mail == '1') {
                        $condition  = array('transactionID' => $tID);
                        $customerID = $paydata['customerListID'];

                        $tr_date    = date('Y-m-d H:i:s');
                        $ref_number = $tID;
                        $toEmail    = $comp_data['userEmail'];
                        $company    = $comp_data['companyName'];
                        $customer   = $comp_data['fullName'];
                        $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'capture');

                    }

                    $this->session->set_flashdata('success', 'Successfully Captured Authorization');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');

                }

                $transactiondata                        = array();
                $transactiondata['transactionID']       = $result['transactionid'];
                $transactiondata['transactionStatus']   = $result['responsetext'];
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionType']     = $result['type'];
                $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                $transactiondata['gatewayID']           = $gatlistval;
                $transactiondata['transactionCode']     = $result['response_code'];
                $transactiondata['customerListID']      = $customerID;
                $transactiondata['transactionAmount']   = $amount;
                $transactiondata['merchantID']          = $merchantID;
                $transactiondata['resellerID']          = $this->resellerID;
                $transactiondata['gateway']             = $gatewayName;

                $transactiondata = alterTransactionCode($transactiondata);
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

            }

            redirect('QBO_controllers/Payments/payment_capture', 'refresh');

        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('logged_in');
        $user_id             = $data['login_info']['id'];

        $compdata = $this->customer_model->get_customers($user_id);

        $data['customers'] = $compdata;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    /*****************Void Transaction***************/

    public function create_customer_void()
    {
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {

                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {

                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $merchantID = $this->session->userdata('logged_in')['merchID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
            if ($this->czsecurity->xssCleanPostInput('setMail')) {
                $chh_mail = 1;
            } else {
                $chh_mail = 0;
            }

            $nmiuser = $gt_result['gatewayUsername'];
            $nmipass = $gt_result['gatewayPassword'];

            $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
            $gatewayName = getGatewayName($gt_result['gatewayType']);
            $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

            $transaction = new nmiDirectPost($nmi_data);

            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];

            $con_cust  = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $comp_data = $this->general_model->get_select_data('QBO_custom_customer', array('companyID', 'companyName', 'fullName', 'userEmail'), $con_cust);
            $transaction->setTransactionId($tID);

            $transaction->void($tID);

            $result = $transaction->execute();

            if ($result['response_code'] == '100') {

              
                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                if ($chh_mail == '1') {
                    $condition  = array('transactionID' => $tID);
                    $customerID = $paydata['customerListID'];

                    $tr_date    = date('Y-m-d H:i:s');
                    $ref_number = $tID;
                    $toEmail    = $comp_data['userEmail'];
                    $company    = $comp_data['companyName'];
                    $customer   = $comp_data['fullName'];
                    $this->general_model->send_mail_voidcapture_data($merchantID, $company, $customer, $toEmail, $customerID, $ref_number, $amount, $tr_date, 'void');

                }

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');

            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['transactionid'];
            $transactiondata['transactionStatus']   = $result['responsetext'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $result['type'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionCode']     = $result['response_code'];
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = $gatewayName;

            $transactiondata = alterTransactionCode($transactiondata);
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/payment_capture', 'refresh');
        }

    }

    public function payment_capture()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->xero_customer_model->get_transaction_data_captue($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_capture', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function refund_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }
        $data['transactions'] = $this->xero_customer_model->get_refund_transaction_data($user_id);

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/page_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function payment_refund()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        
        $data['transactions']   = $this->xero_customer_model->get_transaction_datarefund($user_id);

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_refund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function payment_transaction()
    {
        
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $data['transactions'] = $this->xero_customer_model->get_transaction_history_data($user_id);

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function payment_transaction_old()
    {
        
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $data['transactions'] = $this->xero_customer_model->get_transaction_data($user_id);

        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/payment_transaction', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    /* ---------------------- Ajax Payment Transaction Start --------------------- */
    public function ajax_payment_list()
    {
        error_reporting(0);
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $list = $this->qbo_invoices_model->get_datatables_transaction($user_id);
        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $person) {
            $no++;
            $row = array();

            $res_inv = '';
            if (!empty($person->invoiceID) && preg_match('/^[0-9,]+$/', $person->invoiceID)) {
                $inv = $person->invoiceID;

                $qq = $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  QBO_test_invoice where invoiceID IN($inv) and  merchantID=$user_id  GROUP BY merchantID ");

                if ($qq->num_rows > 0) {
                    $res_inv = $qq->row_array()['invoce'];
                }
            }

            $invoice       = ($res_inv) ? $res_inv : '--';
            $balance       = ($person->transactionAmount) ? number_format($person->transactionAmount, 2) : '0.00';
            $transactionID = ($person->transactionID) ? $person->transactionID : '---';

            $amount = ($person->transactionAmount) ? $person->transactionAmount : '0.00';

            $row[] = $person->fullName;
         
            $row[] = '<div class="hidden-xs text-right"> ' . $invoice . ' </div>';
            $row[] = '<div class="hidden-xs text-right"> $' . $balance . ' </div>';
            $row[] = "<div class='hidden-xs text-right'> " . date('M d, Y', strtotime($person->transactionDate)) . " </div>";
            $type  = '';
            if ($person->partial != '0') {

                $type .= "<label class='label label-warning'>Partially Refunded :" . number_format($person->partial, 2) . " </label><br/>";

            } else if ($person->partial == $person->transactionAmount) {
                $type .= "<label class='label label-success'>Fully Refunded : " . number_format($person->partial, 2) . "</label><br/>";

            } else {

                $type .= "";
            }

         
            $row[] = "<div class='hidden-xs text-right'>" . $type . strtoupper($person->transactionType);

            $row[] = '<div class="hidden-xs text-right"> ' . $transactionID . ' </div>';

            $link = '';

            $link .= '<div class="text-center"> <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">';

            if (in_array($person->transactionCode, array('100', '200', '111', '1')) && in_array(strtoupper($person->transactionType), array('SALE', 'AUTH_CAPTURE', 'Offline Payment', 'PAYPAL_SALE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE'))
            ) {

                $link .= '<li> <a href="#payment_refunds" class="" onclick="set_refund_pay(' . $person->id . ', ' . $person->transactionID . ', ' . $person->transactionGateway . ', ' . $amount . ' );"
        					 data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';

            }

            $link .= '<li><a href="#payment_delete" class=""  onclick="set_transaction_pay(' . $person->id . ');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a></li>
					        </ul>
					   </div> </div>';

            $row[] = $link;

            $data[] = $row;

        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->qbo_invoices_model->count_all_transaction($user_id),
            "recordsFiltered" => $this->qbo_invoices_model->count_filtered_transaction($user_id),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);die;

    }

    /* ---------------------- Ajax Payment Transaction End --------------------- */

    /*********ECheck Transactions**********/

    public function evoid_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->xero_customer_model->get_transaction_data_erefund($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_ecapture', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function echeck_transaction()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['transactions'] = $this->xero_customer_model->get_transaction_data_erefund($user_id);
        $plantype             = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype']     = $plantype;

        $this->load->view('template/template_start', $data);

        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_erefund', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function payment_erefund()
    {
        //Show a form here which collects someone's name and e-mail address
        $merchantID = $this->session->userdata('logged_in')['merchID'];
        if (!empty($this->input->post(null, true))) {

            $tID     = $this->czsecurity->xssCleanPostInput('txnID');
            $con     = array('transactionID' => $tID);
            $paydata = $this->general_model->get_row_data('customer_transaction', $con);

            $gatlistval = $paydata['gatewayID'];

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $nmiuser  = $gt_result['gatewayUsername'];
            $nmipass  = $gt_result['gatewayPassword'];
            $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

            $transaction = new nmiDirectPost($nmi_data);
            $customerID  = $paydata['customerListID'];

            $amount = $paydata['transactionAmount'];

            $transaction->setPayment('check');
            $transaction->setTransactionId($tID);

            $transaction->refund($tID, $amount);

            $result = $transaction->execute();

            if ($result['response_code'] == '100') {

               

                $this->xero_customer_model->update_refund_payment($tID, 'NMI');

                $this->session->set_flashdata('success', 'Successfully Refunded Payment');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  ' . $result['responsetext'] . '</strong>.</div>');
            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['transactionid'];
            $transactiondata['transactionStatus']   = $result['responsetext'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $result['type'];
            $transactiondata['transactionCode']     = $result['response_code'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = "NMI ECheck";

            $transactiondata = alterTransactionCode($transactiondata);
            $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/echeck_transaction', 'refresh');

        }

    }
    public function payment_evoid()
    {
        //Show a form here which collects someone's name and e-mail address
        $result = array();
        if (!empty($this->input->post(null, true))) {

            $tID = $this->czsecurity->xssCleanPostInput('txnvoidID');

            $con        = array('transactionID' => $tID);
            $paydata    = $this->general_model->get_row_data('customer_transaction', $con);
            $gatlistval = $paydata['gatewayID'];
            $merchantID = $this->session->userdata('logged_in')['merchID'];
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            $nmiuser = $gt_result['gatewayUsername'];
            $nmipass = $gt_result['gatewayPassword'];

            $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

            $transaction = new nmiDirectPost($nmi_data);

            $customerID = $paydata['customerListID'];
            $amount = $paydata['transactionAmount'];
            $transaction->setPayment('check');
            $transaction->setTransactionId($tID);

            $transaction->void($tID);

            $result = $transaction->execute();

            if ($result['response_code'] == '100') {

               
                $condition = array('transactionID' => $tID);

                $update_data = array('transaction_user_status' => "3", 'transactionModified' => date('Y-m-d H:i:s'));

                $this->general_model->update_row_data('customer_transaction', $condition, $update_data);

                $this->session->set_flashdata('success', 'Transaction Successfully Cancelled.');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');

            }
            $transactiondata                        = array();
            $transactiondata['transactionID']       = $result['transactionid'];
            $transactiondata['transactionStatus']   = $result['responsetext'];
            $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
            $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            $transactiondata['transactionType']     = $result['type'];
            $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
            $transactiondata['gatewayID']           = $gatlistval;
            $transactiondata['transactionCode']     = $result['response_code'];
            $transactiondata['customerListID']      = $customerID;
            $transactiondata['transactionAmount']   = $amount;
            $transactiondata['merchantID']          = $merchantID;
            $transactiondata['resellerID']          = $this->resellerID;
            $transactiondata['gateway']             = "NMI ECheck";
            if(!empty($this->transactionByUser)){
                $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
            }
            $transactiondata = alterTransactionCode($transactiondata);
            $id = $this->general_model->insert_row('customer_transaction', $transactiondata);

            redirect('QBO_controllers/Payments/evoid_transaction', 'refresh');
        }

    }

    public function create_customer_esale()
    {

        if (!empty($this->input->post(null, true))) {

            if ($this->session->userdata('logged_in')) {
                $merchantID = $this->session->userdata('logged_in')['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
            }

            $gatlistval = $this->czsecurity->xssCleanPostInput('gateway_list');
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));

            if ($gatlistval != "" && !empty($gt_result)) {
                $nmiuser  = $gt_result['gatewayUsername'];
                $nmipass  = $gt_result['gatewayPassword'];
                $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

                $comp_data = $this->general_model->get_row_data('QBO_custom_customer', array('Customer_ListID' => $this->czsecurity->xssCleanPostInput('customerID'), 'merchantID' => $merchantID));
                $companyID = $comp_data['companyID'];

                $transaction = new nmiDirectPost($nmi_data);

                if ($this->czsecurity->xssCleanPostInput('account_number') != "") {

                    $transaction->setAccountName($this->czsecurity->xssCleanPostInput('account_name'));
                    $transaction->setAccount($this->czsecurity->xssCleanPostInput('account_number'));
                    $transaction->setRouting($this->czsecurity->xssCleanPostInput('route_number'));

                    $sec_code = "WEB";

                    $transaction->setAccountType($this->czsecurity->xssCleanPostInput('acct_type'));
                    $transaction->setAccountHolderType($this->czsecurity->xssCleanPostInput('acct_holder_type'));
                    $transaction->setSecCode($sec_code);

                    $transaction->setPayment('check');

                }
                $transaction->setCompany($this->czsecurity->xssCleanPostInput('companyName'));
                $transaction->setFirstName($this->czsecurity->xssCleanPostInput('fistName'));
                $transaction->setLastName($this->czsecurity->xssCleanPostInput('lastName'));
                $transaction->setAddress1($this->czsecurity->xssCleanPostInput('address'));
                $transaction->setCountry($this->czsecurity->xssCleanPostInput('country'));
                $transaction->setCity($this->czsecurity->xssCleanPostInput('city'));
                $transaction->setState($this->czsecurity->xssCleanPostInput('state'));
                $transaction->setZip($this->czsecurity->xssCleanPostInput('zipcode'));
                $transaction->setPhone($this->czsecurity->xssCleanPostInput('phone'));

                $transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
                $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                $transaction->setAmount($amount);
                $transaction->setTax('tax');
                $transaction->sale();
                $result = $transaction->execute();

                if ($result['response_code'] == '100') {

                  
                    $this->session->set_flashdata('success', 'Transaction Successful');
                } else {

                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '</div>');
                }

                $transactiondata                        = array();
                $transactiondata['transactionID']       = $result['transactionid'];
                $transactiondata['transactionStatus']   = $result['responsetext'];
                $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
                $transactiondata['transactionCode']     = $result['response_code'];
                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                $transactiondata['transactionType']     = $result['type'];
                $transactiondata['gatewayID']           = $gatlistval;
                $transactiondata['transactionGateway']  = $gt_result['gatewayType'];
                $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
                $transactiondata['transactionAmount']   = $this->czsecurity->xssCleanPostInput('totalamount');
                $transactiondata['merchantID']          = $merchantID;
                $transactiondata['resellerID']          = $this->resellerID;
                $transactiondata['gateway']             = "NMI ECheck";

                $transactiondata = alterTransactionCode($transactiondata);
                $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
                if(!empty($this->transactionByUser)){
                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                }
                $id = $this->general_model->insert_row('customer_transaction', $transactiondata);
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>');
            }

            redirect('QBO_controllers/Payments/create_customer_esale', 'refresh');
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['page_name']   = "NMI ESale";

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }
        $condition        = array('merchantID' => $user_id);
        
        $gateway		= $this->general_model->get_gateway_data($user_id, 'echeck');
		$data['gateways']      = $gateway['gateway'];
        $data['gateway_url']   = $gateway['url'];
        
        $compdata           = $this->xero_customer_model->get_customers_data($user_id);
        $data['customers']  = $compdata;
        $merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('QBO_views/payment_esale', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

/**********END****************/

    public function chk_friendly_name()
    {
        $res       = array();
        $frname    = $this->czsecurity->xssCleanPostInput('frname');
        $condition = array('gatewayFriendlyName' => $frname);
        $num       = $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
        if ($num) {
            $res = array('gfrname' => 'Friendly name already exist', 'status' => 'false');

        } else {
            $res = array('status' => 'true');
        }
        echo json_encode($res);
        die;
    }

    /*****************Test NMI Validity***************/

    public function testNMI()
    {

        $nmiuser = $this->czsecurity->xssCleanPostInput('nmiuser');
        $nmipass = $this->czsecurity->xssCleanPostInput('nmipassword');

        $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

        $transaction = new nmiDirectPost($nmi_data);

        $transaction->setOrderDescription('Some Item');
        $transaction->setAmount('1.00');
        $transaction->setTax('1.00');
        $transaction->setShipping('1.00');

        $transaction->setCcNumber('4111111111111111');
        $transaction->setCcExp('1113');
        $transaction->setCvv('999');

        $transaction->setCompany('Some company');
        $transaction->setFirstName('John');
        $transaction->setLastName('Smith');
        $transaction->setAddress1('888');
        $transaction->setCity('Dallas');
        $transaction->setState('TX');
        $transaction->setZip('77777');
        $transaction->setPhone('5555555555');
        $transaction->setEmail('test@domain.com');

        $transaction->auth();

        $result = $transaction->execute();

        if ($result['responsetext'] == "SUCCESS") {

            $res = array('status' => 'true');
            echo json_encode($res);
        } else {

            $error = array('nmiPassword' => 'User name or password not matched', 'status' => 'false');
            echo json_encode($error);

        }

    }

    //-------------------------  START -----------------------------//

    //------------------ To view the credit -----------------------//

    public function credit()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $compdata          = $this->customer_model->get_customers_data($user_id);
        $data['customers'] = $compdata;
        $condition         = array('comp.merchantID' => $user_id);

        $data['credits']  = $this->general_model->get_credit_user_data($condition);
        $plantype         = $this->general_model->chk_merch_plantype_status($user_id);
        $data['plantype'] = $plantype;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('pages/page_credit', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //----------- TO update the credit  --------------//

    public function update_credit()
    {
        if ($this->czsecurity->xssCleanPostInput('creditEditID') != "") {

            $id            = $this->czsecurity->xssCleanPostInput('creditEditID');
            $chk_condition = array('creditID' => $id);

            $input_data['creditDate']        = date("Y-m-d");
            $input_data['creditAmount']      = $this->czsecurity->xssCleanPostInput('amount');
            $input_data['creditDescription'] = $this->czsecurity->xssCleanPostInput('description');

            if ($this->general_model->update_row_data('tbl_credits', $chk_condition, $input_data)) {
                $this->session->set_flashdata('success', 'Successfully Updated');
            } else {

                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

            }

        }
        redirect(base_url('QBO_controllers/Payments/credit'));

    }

    public function get_creditedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('credit_id');
        $val = array(
            'creditID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_credits', $val);
        echo json_encode($data);
    }

    //-------------for view credit  data--------------//

    public function get_credit_id()
    {

        $creditID    = $this->czsecurity->xssCleanPostInput('customerID');
        $condition   = array('creditName' => $creditID);
        $creditdatas = $this->general_model->get_table_data('tbl_credits', $condition);

        ?>

		   <table class="table table-bordered table-striped table-vcenter">

            <tbody>

				<tr>
					<th class="text-right"> <strong> Credit Date</strong></th>

					<th class="text-right"><strong>Amount ($)</strong></th>
					<th class="text-right"><strong> Processed On</strong></th>
					<th class="text-left"><strong> Edit/Delete</strong></th>
			  </tr>
 	<?php
if (!empty($creditdatas)) {foreach ($creditdatas as $creditdata) {
            ?>

			<tr>
			<td class="text-right visible-lg"><?php echo date('F d, Y', strtotime($creditdata['creditDate'])); ?> </a> </td>
			<td class="text-right visible-lg"> <?php echo number_format($creditdata['creditAmount'], 2); ?> </a> </td>
			<td class="text-right visible-lg">  <?php if ($creditdata['creditStatus'] == "0") {echo "Yet to Processed";} else {echo $creditdata['creditDate'];}?> </a>
			</td>

			<td class="text-left visible-lg"> <a href="javascript:void(0);" class="btn btn-default" onclick="set_edit_credit('<?php echo $creditdata['creditID']; ?>');" title="Edit"> <i class="fa fa-edit"> </i> </a>

			  <a href="#qbo_del_credit" onclick="qbo_del_credit_id('<?php echo $creditdata['creditID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Credit" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>



			</td>


			</tr>


			<?php }}?>

			</tbody>
        </table>


		<?php die;

    }

    public function get_card_data()
    {
        $customerdata = array();
        if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
            $crID      = $this->czsecurity->xssCleanPostInput('cardID');
            $card_data = $this->card_model->get_single_card_data($crID);
            if (!empty($card_data)) {
                $customerdata['status'] = 'success';
                $customerdata['card']   = $card_data;
                echo json_encode($customerdata);
                die;
            }
        }

    }

    /**************Delete credit********************/

    public function delete_credit()
    {

        $creditID  = $this->czsecurity->xssCleanPostInput('qbo_invoicecreditid');
        $condition = array('creditID' => $creditID);
        $del       = $this->general_model->delete_row_data('tbl_credits', $condition);

        if ($del) {
            $this->session->set_flashdata('success', 'Successfully Deleted');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Somthing Is Wrong...</div>');
        }

        redirect(base_url('QBO_controllers/Payments/credit'));

    }

    public function get_card_expiry_data($customerID)
    {

        $card = array();
        $this->load->library('encrypt');

        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
		     	where  customerListID='$customerID'    ";
        $query1     = $this->db1->query($sql);
        $card_datas = $query1->result_array();
        if (!empty($card_datas)) {
            foreach ($card_datas as $key => $card_data) {

                $customer_card['CardNo']                   = substr($this->card_model->decrypt($card_data['CustomerCard']), 12);
                $customer_card['CardID']                   = $card_data['CardID'];
                $customer_card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
                $card[$key]                                = $customer_card;
            }
        }

        return $card;

    }

    public function get_card_edit_data()
    {

        if ($this->czsecurity->xssCleanPostInput('cardID') != "") {
            $cardID = $this->czsecurity->xssCleanPostInput('cardID');
            $data   = $this->card_model->get_single_mask_card_data($cardID);
            echo json_encode(array('status' => 'success', 'card' => $data));
            die;
        }
        echo json_encode(array('status' => 'success'));
        die;
    }

    public function check_xero_vault()
    {

        $card         = '';
        $card_name    = '';
        $customerdata = array();
        $gatewayID      = $this->czsecurity->xssCleanPostInput('gatewayID');
        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $merchantID = $this->session->userdata('logged_in')['merchID'];

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');
           
            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);
            if (!empty($customerdata)) {

               
                $customerdata['status'] = 'success';

                $conditionGW = array('gatewayID' => $gatewayID);
                $gateway    = $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
                $cardTypeOption = 2;
                if(isset($gateway['gatewayID'])){
                    if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
                        $cardTypeOption = 1;
                    }else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
                        $cardTypeOption = 2;
                    }else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
                        $cardTypeOption = 3;
                    }else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
                        $cardTypeOption = 4;
                    }else{
                        $cardTypeOption = 2;
                    }
                    
                }
                $customerdata['cardTypeOption']  = $cardTypeOption;
                $card_data =   $this->card_model->getCardData($customerID,$cardTypeOption);
                
                $customerdata['card'] = $card_data;

                echo json_encode($customerdata);
                die;
            }

        }else{
            $customerdata = [];
            $conditionGW = array('gatewayID' => $gatewayID);
            $gateway    = $this->general_model->get_row_data('tbl_merchant_gateway', $conditionGW);
            $cardTypeOption = 2;
            if(isset($gateway['gatewayID'])){
                if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 1){
                    $cardTypeOption = 1;
                }else if($gateway['creditCard'] == 1 && $gateway['echeckStatus'] == 0){
                    $cardTypeOption = 2;
                }else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 1){
                    $cardTypeOption = 3;
                }else if($gateway['creditCard'] == 0 && $gateway['echeckStatus'] == 0){
                    $cardTypeOption = 4;
                }else{
                    $cardTypeOption = 2;
                }
                
            }
            
            $customerdata['status']  = 'success';
            $customerdata['cardTypeOption']  = $cardTypeOption;
            echo json_encode($customerdata);
                die;
        }

    }

    public function view_transaction()
    {

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];
        $transactions       = $this->xero_customer_model->get_invoice_transaction_data($invoiceID, $user_id);

        if (!empty($transactions)) {
            foreach ($transactions as $transaction) {
                ?>
				<tr>
					<td class="text-left"><?php echo ($transaction['transactionID']) ? $transaction['transactionID'] : '----'; ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($transaction['transactionAmount'], 2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo ucfirst($transaction['transactionType']); ?></td>
                    <td class="text-right visible-lg"><?php if ($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') {?> <span class="">Success</span><?php } else {?> <span class="">Failed</span> <?php }?></td>

				</tr>

		<?php }

        } else {
            echo '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
        }
        die;

    }

    public function pay_multi_invoice()
    {
        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $resellerID = $this->resellerID;
        $cardID     = $this->czsecurity->xssCleanPostInput('CardID1');
        $gateway    = $this->czsecurity->xssCleanPostInput('gateway1');

        $cusproID = '';
        $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

        if ($cardID != "" || $gateway != "") {

            $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gateway));

            $nmiuser     = $gt_result['gatewayUsername'];
            $nmipass     = $gt_result['gatewayPassword'];
            $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
            $gatewayName = getGatewayName($gt_result['gatewayType']);
            $gatewayName = ($gatewayName && isset($gatewayName['gatewayName'])) ? $gatewayName['gatewayName'] : "NMI";

            $invoices = $this->czsecurity->xssCleanPostInput('multi_inv');

            if (!empty($invoices)) {
                foreach ($invoices as $key => $invoiceID) {

                    $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount' . $invoiceID);
                    $in_data     = $this->quickbooks->get_qbo_invoice_data_pay($invoiceID, $merchID);

                    if (!empty($in_data)) {

                        $Customer_ListID = $in_data['CustomerListID'];
                        $c_data          = $this->general_model->get_select_data('QBO_custom_customer', array('companyID'), array('Customer_ListID' => $Customer_ListID, 'merchantID' => $merchID));
                        $companyID       = $c_data['companyID'];
                        if ($cardID == 'new1') {
                            $cardID_upd   = $cardID;
                            $card_no      = $this->czsecurity->xssCleanPostInput('card_number');
                            $expmonth     = $this->czsecurity->xssCleanPostInput('expiry');
                            $exyear       = $this->czsecurity->xssCleanPostInput('expiry_year');
                            $cvv          = $this->czsecurity->xssCleanPostInput('cvv');
                            $cardType       = $this->general_model->getType($card_no);
                            $friendlyname   =  $cardType.' - '.substr($card_no,-4);
                           
                        } else {
                            $card_data = $this->card_model->get_single_card_data($cardID);
                            $card_no   = $card_data['CardNo'];
                            $cvv       = $card_data['CardCVV'];
                            $expmonth  = $card_data['cardMonth'];
                            $exyear    = $card_data['cardYear'];

                        }

                        $error = '';

                        if (!empty($cardID)) {

                            if ($in_data['BalanceRemaining'] > 0) {
                                $cr_amount = 0;
                                $amount1 = $pay_amounts;

                                $amount = $pay_amounts;
                                $amount = $amount - $cr_amount;

                                if ($amount > $in_data['BalanceRemaining']) {
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error Payment Aomont has exceeded to actual amount</strong></div>');

                                    redirect(base_url('QBO_controllers/Create_invoice/Invoice_details'));
                                }

                                $transaction1 = new nmiDirectPost($nmi_data);
                                $transaction1->setCcNumber($card_no);

                                $exyear = substr($exyear, 2);
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $expry = $expmonth . $exyear;
                                $transaction1->setCcExp($expry);
                                $transaction1->setCvv($cvv);
                                $transaction1->setAmount($amount);
                                // add level III data
                                $level_request_data = [
                                    'transaction' => $transaction1,
                                    'card_no' => $card_no,
                                    'merchID' => $merchID,
                                    'amount' => $amount,
                                    'invoice_id' => $invoiceID,
                                    'gateway' => 1
                                ];
                                $transaction1 = addlevelThreeDataInTransaction($level_request_data);
                                
                                $transaction1->sale();
                                $result = $transaction1->execute();

                                if ($result['response_code'] == "100") {

                                    $val = array(
                                        'merchantID' => $merchID,
                                    );
                                    $data = $this->general_model->get_row_data('QBO_token', $val);

                                    $accessToken  = $data['accessToken'];
                                    $refreshToken = $data['refreshToken'];
                                    $realmID      = $data['realmID'];
                                    $dataService  = DataService::Configure(array(
                                        'auth_mode'       => $this->config->item('AuthMode'),
                                        'ClientID'        => $this->config->item('client_id'),
                                        'ClientSecret'    => $this->config->item('client_secret'),
                                        'accessTokenKey'  => $accessToken,
                                        'refreshTokenKey' => $refreshToken,
                                        'QBORealmID'      => $realmID,
                                        'baseUrl'         => $this->config->item('QBOURL'),
                                    ));

                                    $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");

                                    $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");

                                    if (!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1) {
                                        $theInvoice = current($targetInvoiceArray);
                                    }
                                    $updatedInvoice = Invoice::update($theInvoice, [
                                        "sparse " => 'true',
                                    ]);

                                    $updatedResult = $dataService->Update($updatedInvoice);

                                    $error = $dataService->getLastError();

                                    $newPaymentObj = Payment::create([
                                        "TotalAmt"    => $amount,
                                        "SyncToken"   => $updatedResult->SyncToken,
                                        "CustomerRef" => $updatedResult->CustomerRef,
                                        "Line"        => [
                                            "LinkedTxn" => [
                                                "TxnId"   => $updatedResult->Id,
                                                "TxnType" => "Invoice",
                                            ],
                                            "Amount"    => $amount,
                                        ],
                                    ]);

                                    $savedPayment = $dataService->Add($newPaymentObj);
                                    $error        = $dataService->getLastError();

                                    if ($error != null) {

                                       
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');
                                        $st     = '0';
                                        $action = 'Pay Invoice';
                                        $msg    = "Payment Success but " . $error->getResponseBody();
                                        $qbID   = $result['transactionid'];

                                    } else {
                                        $this->session->set_flashdata('success', 'Successfully Processed Invoice');

                                        $st     = '1';
                                        $action = 'Pay Invoice';
                                        $msg    = "Payment Success";
                                        $qbID   = $result['transactionid'];
                                    }
                                    $invoiceID = $updatedResult->Id;

                                    if ($this->czsecurity->xssCleanPostInput('card_number') != "" && $cardID == "new1" && !($this->czsecurity->xssCleanPostInput('tc'))) {

                                        $this->load->library('encrypt');
                                        $card_no = $this->czsecurity->xssCleanPostInput('card_number');
                                        $cardType       = $this->general_model->getType($card_no);
                                        $friendlyname   =  $cardType.' - '.substr($card_no,-4);

                                      
                                        $card_condition = array(
                                            'customerListID'           => $this->czsecurity->xssCleanPostInput('customerID'),
                                            'customerCardfriendlyName' => $friendlyname,
                                        );
                                        $cid      = $this->czsecurity->xssCleanPostInput('customerID');
                                        $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                                        $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                                        $cvv      = $this->czsecurity->xssCleanPostInput('cvv');

                                        $query = $this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='" . $cid . "' and   customerCardfriendlyName ='" . $friendlyname . "' ");

                                        $crdata = $query->row_array()['numrow'];

                                        if ($crdata > 0) {

                                            $card_data = array('cardMonth' => $expmonth,
                                                'cardYear'                     => $exyear,
                                                'CardType'                     => $cardType,
                                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                                'CardCVV'                      => '', 
                                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                                'customerListID'               => $in_data['Customer_ListID'],
                                                'customerCardfriendlyName'     => $friendlyname,
                                                'companyID'                    => $companyID,
                                                'merchantID'                   => $merchID,
                                                'updatedAt'                    => date("Y-m-d H:i:s"));

                                            $this->db1->update('customer_card_data', $card_condition, $card_data);
                                        } else {
                                            $is_default = 0;
                                            $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$merchID);
                                            if($checkCustomerCard == 0){
                                                $is_default = 1;
                                            }
                                            $card_data = array('cardMonth' => $expmonth,
                                                'cardYear'                     => $exyear,
                                                'CardType'                     => $cardType,
                                                'CustomerCard'                 => $this->card_model->encrypt($card_no),
                                                'CardCVV'                      => '',
                                                'Billing_Addr1'                => $this->czsecurity->xssCleanPostInput('address1'),
                                                'Billing_Addr2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                                'Billing_City'                 => $this->czsecurity->xssCleanPostInput('city'),
                                                'Billing_Country'              => $this->czsecurity->xssCleanPostInput('country'),
                                                'Billing_Contact'              => $this->czsecurity->xssCleanPostInput('phone'),
                                                'Billing_State'                => $this->czsecurity->xssCleanPostInput('state'),
                                                'Billing_Zipcode'              => $this->czsecurity->xssCleanPostInput('zipcode'),
                                                'customerListID'               => $in_data['Customer_ListID'],
                                                'customerCardfriendlyName'     => $friendlyname,
                                                'companyID'                    => $companyID,
                                                'merchantID'                   => $merchID,
                                                'is_default'                   => $is_default,
                                                'createdAt'                    => date("Y-m-d H:i:s"));


                                            $id1 = $this->db1->insert('customer_card_data', $card_data);

                                        }

                                    }

                                } else {
                                    $st     = '0';
                                    $action = 'Pay Invoice';
                                    $msg    = "Payment Failed";
                                    $qbID   = $invoiceID;
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $result['responsetext'] . '.</div>');
                                }
                                $qbo_log = array('qbStatus' => $st, 'qbAction' => $action, 'qbText' => $msg, 'merchantID' => $merchID, 'qbActionID' => $qbID, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));

                                $this->general_model->insert_row('tbl_qbo_log', $qbo_log);
                                $transaction['transactionID']           = $result['transactionid'];
                                $transaction['transactionStatus']       = $result['responsetext'];
                                $transaction['transactionCode']         = $result['response_code'];
                                $transaction['transactionType']         = ($result['type']) ? $result['type'] : 'auto-nmi';
                                $transaction['transactionDate']         = date('Y-m-d H:i:s');
                                $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                                $transaction['gatewayID']               = $gateway;
                                $transaction['transactionGateway']      = $gt_result['gatewayType'];
                                $transaction['customerListID']          = $in_data['CustomerListID'];
                                $transaction['transactionAmount']       = $amount;
                                $transaction['merchantID']              = $merchID;
                                if ($error == null) {
                                    $transaction['qbListTxnID'] = $savedPayment->Id;
                                }
                                $transaction['resellerID'] = $resellerID;
                                $transaction['gateway']    = $gatewayName;
                                $transaction['invoiceID']  = $invoiceID;
                                
                                $transaction = alterTransactionCode($transaction);
                                $CallCampaign = $this->general_model->triggerCampaign($merchID,$transaction['transactionCode']);
                                if(!empty($this->transactionByUser)){
                                    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
                                    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
                                }
                                $id                        = $this->general_model->insert_row('customer_transaction', $transaction);

                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>');
                            }

                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>');
                        }

                    } else {
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');
                    }
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Please select invoices.</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card!</strong>.</div>');
        }

        if ($cusproID != "") {
            redirect('QBO_controllers/home/view_customer/' . $cusproID, 'refresh');
        } else {
            redirect('QBO_controllers/Create_invoice/Invoice_details', 'refresh');
        }

    }

    public function check_vault()
    {


        $card         = '';
        $card_name    = '';
        $customerdata = array();

        if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

            $merchantID = $this->session->userdata('logged_in')['merchID'];

            $customerID = $this->czsecurity->xssCleanPostInput('customerID');

            $condition    = array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
            $customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);

            if (!empty($customerdata)) {

                $invoices = $this->xero_customer_model->get_invoice_upcomming_data($customerID, $merchantID);

                $table = '';
                $new_inv = '';
                $totalInvoiceAmount = 0.00;
                if (!empty($invoices)) {
                    $new_inv = '<div class="form-group alignTableInvoiceList" >
                        <div class="col-md-1 text-center"><b></b></div>
                            <div class="col-md-3 text-left"><b>Number</b></div>
                            <div class="col-md-3 text-right"><b>Due Date</b></div>
                            <div class="col-md-2 text-right"><b>Amount</b></div>
                            <div class="col-md-3 text-left"><b>Payment</b></div>
                        </div>';

                    foreach ($invoices as $inv) {
                        if ($inv['status'] != 'Cancel') {
                            $new_inv .= '<div class="form-group alignTableInvoiceList" >
                       
                            <div class="col-md-1 text-center"><input checked type="checkbox" class="chk_pay check_'.$inv['refNumber'].'" id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
                            <div class="col-md-3 text-left">' . $inv['refNumber'] . '</div>
                            <div class="col-md-3 text-right">' . date("M d, Y", strtotime($inv['DueDate'])) . '</div>
                            <div class="col-md-2 text-right">' . '$' . number_format($inv['BalanceRemaining'], 2) . '</div>
                            <div class="col-md-3 text-left"><input type="text" name="pay_amount' . $inv['invoiceID'] . '" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . '  geter" data-id="multiinv' . $inv['invoiceID'] . '" data-inv="' . $inv['invoiceID'] . '" data-ref="' . $inv['refNumber'] . '" data-value="' . $inv['BalanceRemaining'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div>
                            </div>';
                            $inv_data[] = [
                                'RefNumber' => $inv['refNumber'],
                                'TxnID' => $inv['invoiceID'],
                                'BalanceRemaining' => $inv['BalanceRemaining'],
                            ];
                            $invoiceIDs[$inv['invoiceID']] =  $inv['refNumber'];
                            $totalInvoiceAmount = $totalInvoiceAmount + $inv['BalanceRemaining'];
                        }
                    }
                } else {
                    $table .= '';
                }
                $customerdata['invoices'] = $new_inv;
                $customerdata['invoiceIDs'] = $invoiceIDs;
                $customerdata['totalInvoiceAmount'] = $totalInvoiceAmount;
                $customerdata['status']   = 'success';
                $card_data            = $this->card_model->get_card_expiry_data($customerID);
                $customerdata['card'] = $card_data;

                if(empty($customerdata['companyName'])){
                    $customerdata['companyName'] = $customerdata['FullName'];
                }

                echo json_encode($customerdata);
                die;
            }

        }

    }

    //pawan code here

    public function get_invoice()
    {

        if ($this->session->userdata('logged_in')) {
            $merchID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $id = '';
        if (!empty($this->czsecurity->xssCleanPostInput('id'))) {
            $id = $this->czsecurity->xssCleanPostInput('id');
        }
        $invoices = $this->xero_customer_model->get_invoice_upcomming_data($id, $merchID);
        if ($invoices) {
            echo '<table class="mytable" width="100%">';
            echo "<tr><th>Select</th><th>Ref Number</th><th>Total Amount</th></tr>";

            foreach ($invoices as $inv) {
                echo "<tr><td><input type='checkbox' name='inv' value='" . $inv['BalanceRemaining'] . "' class='test' rel='" . $inv['id'] . "'/></td><td>" . $inv['refNumber'] . "</td><td>" . $inv['BalanceRemaining'] . "</td>";
            }

            echo '</table>';
        } else {
            echo 'No pending Invoices Found';
        }
    }

    public function check_transaction_payment()
    {
        if ($this->session->userdata('logged_in')) {
            $da = $this->session->userdata('logged_in');

            $user_id = $da['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $da = $this->session->userdata('user_logged_in');

            $user_id = $da['merchantID'];
        }

        if (!empty($this->czsecurity->xssCleanPostInput('trID'))) {

            $trID      = $this->czsecurity->xssCleanPostInput('trID');
            $av_amount = $this->czsecurity->xssCleanPostInput('ref_amount');

            $tr_data = $this->general_model->get_select_data('customer_transaction', array('transactionID'), array('id' => $trID));
            $tID     = $tr_data['transactionID'];
            $p_data  = $this->xero_customer_model->get_transaction_details_data(array('tr.transactionID' => $tID, 'cust.merchantID' => $user_id, 'tr.merchantID' => $user_id));
            if (!empty($p_data)) {
                if ($p_data['transactionAmount'] >= $av_amount) {
                    $resdata['status'] = 'success';

                } else {
                    $resdata['status']  = 'error';
                    $resdata['message'] = 'Your are not allowed to refund exceeded amount more than actual amount :' . $p_data['transactionAmount'];
                }
            } else {
                $resdata['status']  = 'error';
                $resdata['message'] = 'Invalid transactions ';
            }
        } else {
            $resdata['status']  = 'error';
            $resdata['message'] = 'Invalid request';
        }
        echo json_encode($resdata);
        die;

    }

}
