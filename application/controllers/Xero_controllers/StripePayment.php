<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */


class StripePayment extends CI_Controller
{
     private $resellerID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
	    
		
	    include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

		
	    $this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		$this->load->model('Xero_models/xero_model');
		
		$this->db1 = $this->load->database('otherdb', TRUE);
			$this->load->library('form_validation');
	  if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
		  {
		  	$logged_in_data = $this->session->userdata('logged_in');
			$this->resellerID = $logged_in_data['resellerID'];
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];  
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	 
	    
	}
	
	
		 
    public function pay_invoice()
    {
    
     $token='';
     if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
	
	    
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
	
            $cusproID=''; $error='';
            
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
    		   
         $token = $this->czsecurity->xssCleanPostInput('stripeToken');
          if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
       if(!empty($cardID) && !empty($gateway) && !empty($token))
       {  
         
          $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	$nmiuser   = $gt_result['gatewayUsername'];
    		    $nmipass   = $gt_result['gatewayPassword'];
		if(!empty($in_data)){   
		  
            $Customer_ListID = $in_data['Customer_ListID'];
            
             $customerID = $Customer_ListID;
            
	     	$comp_data  =$this->general_model->get_select_data('Xero_custom_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
	    	$companyID  = $comp_data['companyID'];
        
             if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }     
        
        
          
        		 if(!empty($cardID))
        		 {
				
			        	if( $in_data['BalanceRemaining'] > 0)
			        	{
					     $cr_amount = 0;
					     $amount    =	 $in_data['BalanceRemaining']; 
					
							 $amount  = $this->czsecurity->xssCleanPostInput('inv_amount');	    
					       $amount    = $amount-$cr_amount;
					        $real_amt = $amount;
				     	$amount =  (int)($amount*100);
						$plugin = new ChargezoomStripe();
						$plugin->setApiKey($gt_result['gatewayPassword']);
					
						
						   $token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
							$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge Using Stripe Gateway",
								 
								));	
                      
                        
			  
						   $charge= json_encode($charge);
                      	
						   $result = json_decode($charge);
				  
						    $trID='';
						
						  
				 if($result->paid=='1' && $result->failure_code=="")
				 {
						  $code		 =  '200';
						  $trID 	 = $result->id;
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $crtxnID='';
						 
						$bamount     =  $in_data['BalanceRemaining']-$real_amt;
						if($bamount > 0)
						$ispaid 	 = 'false';
						  $app_amount = $in_data['AppliedAmount']+(-$real_amt);
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
						
					
						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						
                  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
				 		
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				    
						$card_condition = array(
										 'customerListID' =>$customerID, 
										 'customerCardfriendlyName'=>$friendlyname,
										);
										
							$cid      =    $customerID;			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
							$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;
					
						if($crdata > 0)
						{
							
						  $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
									
					
						     $this->card_model->update_card_data($card_condition, $card_data);					 
						}
						else
						{
					      $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'],
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
				          $id1 =    $this->card_model->insert_card_data($card_data);		
						
						}
				
				 }
				    
				        if($chh_mail =='1')
						 {
						  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
						  $ref_number =  $in_data['RefNumber']; 
						  $tr_date   =date('Y-m-d H:i:s');
						  	$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
						   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount/100, $tr_date   );
						 }
			 
						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
					   } else{
                 
                        
                 
					       $code =  $result->failure_code; 
					       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->status.'</div>'); 
					   	
					   }  
				   $id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$real_amt,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                
				    
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
       $ermsg='';
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }
			
			 if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
		   	  if($cusproID=="1") {
		   	 redirect('home/invoices','refresh');
		   	 }
        	 redirect('home/invoices','refresh');
			 
		
	
    }     

	
	
	
	
	
	public function create_customer_sale()
    {
		 			   	
	   	  $invoiceIDs=array();   
		 if(!empty($this->input->post(null, true))){
			
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			   
    		     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
		        $customerID =   $this->czsecurity->xssCleanPostInput('customerID');
		
					$comp_data  =$this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
				    $companyID  = $comp_data['companyID'];
				    $comp  =$this->general_model->get_select_data('tbl_company',array('qbwc_username'), array('id'=>$companyID, 'merchantID'=>$merchantID));
				    $user  = $comp['qbwc_username'];
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1)
						 {
								$expmonth = '0'.$expmonth;
						 }
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}
					$totalamount = $this->czsecurity->xssCleanPostInput('totalamount');
					$amount =  (int)($this->czsecurity->xssCleanPostInput('totalamount')*100);
				
					$plugin = new ChargezoomStripe();
					$plugin->setApiKey($gt_result['gatewayPassword']);					
					$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $amount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
				   
				 $trID='';
				 if($result->paid=='1' && $result->failure_code=="")
				 {
				  $code ='200';
				  $trID = $result->id;
					 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				            $refnum=array();
				           if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
        									$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
            						    	$tes = $this->general_model->update_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
            						
            						         $id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$theInvoice['BalanceRemaining'],$merchantID,$crtxnID='', $this->resellerID,$inID, false, $this->transactionByUser);  
        					                 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
        								    $refnum[]= $theInvoice['RefNumber'];
        								}
				              }
				              
				              if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
							  $ref_number =  implode(',',$refnum); 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$totalamount, $tr_date   );
							 }
						 
				          }
				          else
				          {
				              
				                    $transactiondata= array();
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$totalamount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
				          }
				 
		       
				 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $this->czsecurity->xssCleanPostInput('card_list')=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				     
				        $this->load->library('encrypt');
			
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						 $cardType       = $this->general_model->getType($card_no);
						 $friendlyname   =  $cardType.' - '.substr($card_no,-4);		
						$card_condition = array(
										 'customerListID' =>$customerID, 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					     
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'CardType' =>$this->general_model->getType($card_no),
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',  
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$is_default = 0;
					        $checkCustomerCard = checkCustomerCard($customerID,$merchantID);
					        if($checkCustomerCard == 0){
					            $is_default = 1;
					        }
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										   'CardType' =>$this->general_model->getType($card_no),
										 'customerListID' =>$customerID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
			
				 	
						 if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
    				     {
    				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
    				     }
    						
				         if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        						   	$theInvoice = $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        								if(!empty($theInvoice) )
        								{
        									$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
            						    	$tes = $this->general_model->update_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
            							  
            						    	$transactiondata= array();
            						    	
            						    	  $transactiondata['transactionID']       = $trID;
                        					   $transactiondata['transactionStatus']  = $result->status;
                        					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
                        					   $transactiondata['transactionCode']     = $code;  
                        					   $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
                        						$transactiondata['transactionType']    = 'stripe_sale';	
                        						$transactiondata['gatewayID']          = $gatlistval;
                                               $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;					
                        					   $transactiondata['customerListID']      = $customerID;
                        					   $transactiondata['transactionAmount']   = $theInvoice['BalanceRemaining'];
                        					    $transactiondata['invoiceTxnID']       = $inID;
                        					   $transactiondata['merchantID']          = $merchantID;
                        					   $transactiondata['gateway']             = "Stripe";
                        					  $transactiondata['resellerID']           = $this->resellerID;
                                    						    	
            						    	$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

											if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											}
                    				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                    				       
                    				        $comp_data = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$merchantID));
                    				        $user      = $comp_data['qbwc_username'] ;
                    				       $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
        						}
				              }
						 
				          }
				          else
				          {
				              
				                $transactiondata= array();
            					  $transactiondata['transactionID']        = $trID;
            					   $transactiondata['transactionStatus']   = $result->status;
            					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
            					   $transactiondata['transactionCode']     = $code;  
            					   $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
            						$transactiondata['transactionType']    = 'stripe_sale';	
            						$transactiondata['gatewayID']          = $gatlistval;
                                   $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
            					   $transactiondata['customerListID']      = $customerID;
            					   $transactiondata['transactionAmount']   = ($result->amount/100);
            					   $transactiondata['merchantID']          = $merchantID;
            					   $transactiondata['gateway']             = "Stripe";
            					  $transactiondata['resellerID']           = $this->resellerID;
            					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
            						if(!empty($this->transactionByUser)){
									    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
									    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
									}
            				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				        
				          }
				 
				 }
				 
				        
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'.</div>'); 
				
					      
					  $id = $this->general_model->insert_gateway_transaction_data($result,'stripe_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$totalamount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
				             
					      
				 }
				 
				     
					  
				   
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
				}	
			
				      
           }
		   redirect('Payments/create_customer_sale','refresh');
	   }
	 
	 
	 
	 


	
	
	public function create_customer_auth()
    {
		 			   	
	   	    	   
		 if(!empty($this->input->post(null, true))){
			
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			   
    		  
				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
		
				$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $this->czsecurity->xssCleanPostInput('customerID')));
				$companyID  = $comp_data['companyID'];
					
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card     = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						 if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					
						$cvv    = $this->czsecurity->xssCleanPostInput('cvv');
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                			$card_data= $this->card_model->get_single_card_data($cardID);
							$card = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];							
							$exyear   = $card_data['cardYear'];
							$exyear   = $exyear;
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear;  
							
							$cvv      = $card_data['CardCVV'];
					}	
					$amount =  (int)($this->czsecurity->xssCleanPostInput('totalamount')*100);
			      	$customerID = $this->czsecurity->xssCleanPostInput('customerID');
					$plugin = new ChargezoomStripe();
					$plugin->setApiKey($gt_result['gatewayPassword']);
					
					$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
					$charge =	\Stripe\Charge::create(array(
						  "amount" => $amount,
						  "currency" => "usd",
						  "source" => $token, // obtained with Stripe.js
						  "description" => "Charge for test Account",
						 'capture'     => 'false' 
						));	
               
			       $charge= json_encode($charge);
				   $result = json_decode($charge);
				   
				 $trID='';
				 if($result->paid=='1' && $result->failure_code==""){
				  $code ='200';
				  $trID = $result->id;
				 /* This block is created for saving Card info in encrypted form  */
				 
		       
				 if($this->czsecurity->xssCleanPostInput('card_number')!=""){
				     
				        $this->load->library('encrypt');
			
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);	
				        
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					     
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$is_default = 0;
					        $checkCustomerCard = checkCustomerCard($customerID,$merchantID);
					        if($checkCustomerCard == 0){
					            $is_default = 1;
					        }
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										 'customerListID' =>$customerID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
										  'is_default'			   => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				    $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
				 }
				 else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'.</div>'); 
				 }
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $trID;
					   $transactiondata['transactionStatus']    = $result->status;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $code;  
					   $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
						$transactiondata['transactionType']    = 'stripe_auth';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = ($result->amount/100);
					   $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Stripe";
					    $transactiondata['transaction_user_status']= '5';
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 
				}	
           }
		      redirect('Payments/create_customer_auth','refresh');
	   }
	 
	 
	/*****************Capture Transaction***************/
	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
		       
		    	 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
		    
		    	 $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
    			 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			   if( $paydata['gatewayID'] > 0){ 
			       
			      
				 $gatlistval = $paydata['gatewayID'];  
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				 	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			if( $tID!='' && !empty($gt_result)){   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 

    		    $plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);
				

				$ch = \Stripe\Charge::retrieve($tID);
				$charge = $ch->capture();
								 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
			
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded')){  
				     
				$amount = ($result->amount/100) ;
				 $code  ='200';
			    $trID   = $result->id;
				$condition = array('transactionID'=>$tID);
				$update_data =   array('transaction_user_status'=>"4");
				$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
				$this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 $code =  $result->failure_code; 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result['responsetext'].'</div>'); 
				     
					 }
					 
					   $transactiondata= array();
				       $transactiondata['transactionID']      =  $trID;
					   $transactiondata['transactionStatus']  = $result->status;;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s');
					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionType']    = 'stripe_capture';
					    $transactiondata['transactionGateway']= $gt_result['gatewayType'];
						 $transactiondata['gatewayID']        = $gatlistval;
					    $transactiondata['transactionCode']   = $code;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					     $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Stripe";
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
			}else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Gateway not available.</div>'); 
				     
					 }	
			   }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> </div>'); 
				     
					 }	
					redirect('Payments/payment_capture','refresh');
		
        }
              
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_transaction', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	 
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
		    
		    	 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
			
			     $tID     = $this->czsecurity->xssCleanPostInput('txnstrID');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				if( $tID!='' && !empty($gt_result)){ 
			   
    			$nmiuser  = $gt_result['gatewayUsername'];
    		    $nmipass  =  $gt_result['gatewayPassword']; 
    		     $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
    		    $amount   = $total ;
            		    
            			if(!empty($paydata['invoiceTxnID']))
            			{
            		  
            	        	$cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$paydata['merchantID']) );
            			$user_id  = $paydata['merchantID'];
            			 $user    =  $cusdata['qbwc_username'];
            	        $comp_id  =  $cusdata['id']; 
            	        $ittem = $this->general_model->get_row_data('qb_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
            			$ins_data['customerID']     = $paydata['customerListID'];
            		
            	         if(empty($ittem))
        		        {
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                            	
        		            
        		        }
                    		    
            		      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
                			   if(!empty($in_data))
                			   {
                    			$inv_pre   = $in_data['prefix'];
                    			$inv_po    = $in_data['postfix']+1;
                    			$new_inv_no = $inv_pre.$inv_po;
                                
                               
                               }
            		$ins_data['merchantDataID'] = $paydata['merchantID'];	
            		  $ins_data['creditDescription']     ="Credit as Refund" ;
                       $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
            			$ins_data['creditDate']   = date('Y-m-d H:i:s');
                	  $ins_data['creditAmount']   = $total;
                      $ins_data['creditNumber']   = $new_inv_no;
                       $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                        $ins_data['Type']         = "Payment";
                	   $ins_id  = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
            				   
            				   $item['itemListID']      =    $ittem['ListID']; 
            			       $item['itemDescription'] =    $ittem['Name']; 
            			       $item['itemPrice']       =$total; 
            			       $item['itemQuantity']    = 0; 
            			      	$item['crlineID']       = $ins_id;
            					$acc_name  = $ittem['DepositToAccountName']; 
        				      	$acc_ID    = $ittem['DepositToAccountRef']; 
        				      	$method_ID = $ittem['PaymentMethodRef']; 
        				      	$method_name  = $ittem['PaymentMethodName']; 
        						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
        						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
        				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
        				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
        				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
        				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
        				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
        				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
        				  	           );		
            				
            				
            			 if($ins_id && $ins)
            			 {
            				 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
            				 
                           
                          }else{
                              	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>'); 
                              		
                          }
            				
                     }
            		    
    		     $amount=   (int)($amount*100);
    		    $plugin = new ChargezoomStripe();
				$plugin->setApiKey($gt_result['gatewayPassword']);
				$charge = \Stripe\Refund::create(array(
				  "charge" => $tID,
				  "amount"=>$amount,
				));
			  
				 
				 $customerID = $paydata['customerListID'];
			
			      $charge= json_encode($charge);
				  
				   $result = json_decode($charge);
			
				   
				  
				$trID='';
				 if(strtoupper($result->status) == strtoupper('succeeded')){  
				     
				$amount = ($result->amount/100) ;
				 $code ='200';
			    $trID = $result->id;
			
				  
					 $this->customer_model->update_refund_payment($tID, 'STRIPE');
					 	if(!empty($paydata['invoiceTxnID']))
            			{	
            			    
            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);
            			 $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
            			}
					
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 $code =  $result->failure_code;
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong></div>'); 
				 }
				       $transactiondata= array();
				       $transactiondata['transactionID']      = $result->id;
					   $transactiondata['transactionStatus']  =  $result->status;
					   $transactiondata['transactionDate']    = date('Y-m-d H:i:s'); 
					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionType']    = 'stripe_refund';
					    $transactiondata['transactionCode']   = $code;
						$transactiondata['transactionGateway']= $gt_result['gatewayType'];
						$transactiondata['gatewayID']         = $gatlistval;
					   $transactiondata['customerListID']     = $customerID;
					   $transactiondata['transactionAmount']  = $amount;
					     $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Stripe";
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				}else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong> Gateway not available.</div>'); 
				     
					 }	
				
				
        }
              
					if(!empty($this->czsecurity->xssCleanPostInput('payrefund')))
		        {
					redirect('Payments/payment_refund','refresh');
	        	} else {		
				 
				redirect('Payments/payment_transaction','refresh');
				
	        	}
	        	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_refund', $data);
				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	    }
	
	 
	 
	 
	
  public function get_single_card_data($cardID)
  {  
  
                  $card = array();
               	  $this->load->library('encrypt');

		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				}		
					
					return  $card;

       }
 

	 
		 
    public function pay_multi_invoice()
    {
    
    $token='';
     if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
		
		$customerID = $this->czsecurity->xssCleanPostInput('customerID');
		$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$user_id));
		$companyID  = $comp_data['companyID'];
		
		
			 $resellerID =	$this->resellerID;
	     
	     
	     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
	     
	     
	     
	 	
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
	      $token_array = $this->czsecurity->xssCleanPostInput('stripeToken');
        	$invices = $this->czsecurity->xssCleanPostInput('multi_inv');
    		 
        
       if($cardID!="" && !empty($gateway) && !empty($token_array) )
       {  
         
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	$nmiuser   = $gt_result['gatewayUsername'];
    		    $nmipass   = $gt_result['gatewayPassword'];
    		    
    		    
         if(!empty($invices))
         { 	
         	foreach($invices as $k=> $invoiceID)
         	{
         	     $token = $token_array[$k];
         	     
         	  
         	     $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
         	   $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
           
		if(!empty($in_data))
		{   
		  
            $Customer_ListID = $in_data['Customer_ListID'];
        
           if($cardID=='new1')
           {
				$cardID_upd  =$cardID;
				$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
				$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
				$cardType       = $this->general_model->getType($card_no);
				$friendlyname   =  $cardType.' - '.substr($card_no,-4);
				
					
           }
           else
           {
               
             
                        $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
           }
       
              
		 if(!empty($cardID))
		 {
				
				if( $in_data['BalanceRemaining'] > 0){
					     $cr_amount = 0;
					     $amount    =	 $in_data['BalanceRemaining']; 
					   
						 $amount    = sprintf('%0.2f',$pay_amounts);
					       $amount    = $amount-$cr_amount;
					     
					        $real_amt = $amount;
				     	$amount =  (int)($amount*100);
				
						$plugin = new ChargezoomStripe();
						$plugin->setApiKey($gt_result['gatewayPassword']);
					
							$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge Using Stripe Gateway",
								 
								));	
                      
                        
			  
						   $charge= json_encode($charge);
                      	
						   $result = json_decode($charge);
				  
						    $trID='';
						
						  
				 if($result->paid=='1' && $result->failure_code==""){
						  $code		 =  '200';
						  $trID 	 = $result->id;
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 
						 $bamount = $in_data['BalanceRemaining']-$real_amt;
						 if($bamount > 0)
						  $ispaid 	 = 'false';
						  $app_amount = $in_data['AppliedAmount']+(-$real_amt);
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['qbwc_username'];
					 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
				 		
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);
						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					    
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						  $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
									
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
							$is_default = 0;
					        $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$user_id);
					        if($checkCustomerCard == 0){
					            $is_default = 1;
					        }
					      	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'],
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										  'is_default'			   => $is_default,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
							
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				    
                            if($chh_mail =='1')
                    							 {
                    							  $condition_mail= array('templateType'=>'5', 'merchantID'=>$user_id); 
                    							  $ref_number =  $in_data['RefNumber']; 
                    							  $tr_date   =date('Y-m-d H:i:s');
                    							  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                    							 
                    							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$real_amt, $tr_date);
                    							 }
				    
						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
					   } else{
                 
                         if($cardID_upd=='new1')
                         {
                           $this->db1->where(array('CardID'=>$cardID));
                           $this->db1->delete('customer_card_data');
                         }
                 
					       $code =  $result->failure_code; 
					       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->status.'</div>'); 
					   	
					   }  
					 
					   $transaction['transactionID']      = $trID;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d H:i:s');  
					    $transaction['transactionModified'] = date('Y-m-d H:i:s');
					   $transaction['transactionCode']         = $code;  
					   $transaction['invoiceTxnID']       =$invoiceID;
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['Customer_ListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  $transaction['merchantID']   =$user_id;
					   $transaction['gateway']   = "Stripe";
					  $transaction['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($user_id,$transaction['transactionCode']);
					  	if(!empty($this->transactionByUser)){
						    $transaction['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transaction['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transaction);
				       	 if($result->paid=='1' && $result->failure_code=="")
				       	 {
				          $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1','', $user);
				       	 }
				    
                        				     	  
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>'); 
			 }
			 
         	}
         }else{
             
            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> This is not valid invoice.</div>');   
         }	
			 
			 
			 
          }else{
       $ermsg='';
       if($cardID=="" )
       $ermsg ="Card is required";
       if($gateway=="")
       $ermsg ="Gateway is required";
       if($token=="" )
        $ermsg ="Stripe token is required";
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$ermsg.'</div>'); 
		  }
			
			 
		    redirect('home/invoices','refresh');
	
    }     

	
	
	
	
	
	
	}  