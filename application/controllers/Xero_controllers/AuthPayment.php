<?php

/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 * Also Applied ACH Process for NMI Gateway is given following oprations
 * create_customer_esale for Sale
 * create_customer_evoid for Void
 * create_customer_erefund for Refund
 */
class AuthPayment extends CI_Controller
{
	
	private $resellerID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
		
	
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $this->load->config('auth_pay');
     	$this->load->config('quickbooks');
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
     	$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->model('customer_model');
		$this->load->model('Xero_models/xero_model');
		
		    if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
			  {
				$logged_in_data = $this->session->userdata('logged_in');
				$this->resellerID = $logged_in_data['resellerID'];
				$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
			  }else if($this->session->userdata('user_logged_in')!="")
			  {
			   	$logged_in_data = $this->session->userdata('user_logged_in');
				$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
				$merchID = $logged_in_data['merchantID'];
				$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
				$this->resellerID = $rs_Data['resellerID'];
			  }else{
				redirect('login','refresh');
			  }
			  
					
	
	}
	
	
	public function index(){
		redirect('home/index','refresh');
	    
	}
	

	
	 
public function pay_invoice()
{

   
      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	

   
	
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
         $cusproID=''; $error='';
         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
	     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
    		
	 if($cardID!="" && $gateway!=""){  
	     
	       $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
		  $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
		  $m_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$user_id));
	    	 $resellerID = 	$m_data['resellerID'];  
		 
     	$apiloginID       = $gt_result['gatewayUsername'];
	    $transactionKey   = $gt_result['gatewayPassword'];
		if(!empty($in_data)){ 
		     $customerID  = $in_data['Customer_ListID'];
		
			   $Customer_ListID = $in_data['Customer_ListID'];
            	$comp_data     = $this->general_model->get_select_data('Xero_custom_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    			$companyID = $comp_data['companyID'];
            	   
           if($cardID=='new1')
           {
                        $cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						
           			    $address1 =  $this->czsecurity->xssCleanPostInput('address1');
	                    $address2 =  $this->czsecurity->xssCleanPostInput('address2');
	                    $city     =  $this->czsecurity->xssCleanPostInput('city');
	                    $country     =  $this->czsecurity->xssCleanPostInput('country');
	                    $phone       =  $this->czsecurity->xssCleanPostInput('contact');
	                    $state       = $this->czsecurity->xssCleanPostInput('state');
	                     $zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');
           
           }
        else{
          			  $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
        
        				$address1 =     $card_data['Billing_Addr1'];
        				$address2 =     $card_data['Billing_Addr2'];
	                    $city     =      $card_data['Billing_City'];
        			  $zipcode       =      $card_data['Billing_Zipcode'];
        				$state       =     $card_data['Billing_State'];
	                    $country     =      $card_data['Billing_Country'];
        	            $phone       =     $card_data['Billing_Contact'];
	                                     
        }
        
        
		 if(!empty($cardID))
         {
				
				if( $in_data['BalanceRemaining'] > 0){
					        $cr_amount = 0;
					        $amount  =	 $in_data['BalanceRemaining']; 
					   
					          
							$amount  = $this->czsecurity->xssCleanPostInput('inv_amount');
							$amount           = $amount-$cr_amount;
					  		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
					  		$transaction1->setSandbox($this->config->item('auth_test_mode'));
					  
							
							$exyear1   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
				 	        $expry    = $expmonth.$exyear1;  
					    
						     $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);

					
					   if( $result->response_code=="1"){
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $bamount    = $in_data['BalanceRemaining']-$amount;
						 
						 if($bamount > 0)
						  $ispaid 	 = 'false';
						  $app_amount = $in_data['AppliedAmount']+(-$amount);
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
						 
					
						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
						 $this->general_model->update_row_data('Xero_test_invoice',$condition, $data);
						 
					
				
                    			         if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                       
                       
                       
    			
                       				 if($chh_mail =='1')
        							 {
        							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
        							  $ref_number =  $in_data['RefNumber']; 
        							  $tr_date   =date('Y-m-d H:i:s');
        							  	$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                    
        							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
        							 }
					
					      
						  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
                         
					   } else{
                     
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong>.</div>'); 
					   }  
				  
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']   = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result->response_code;  
					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['gatewayID']           = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
					   $transactiondata['customerListID']      = $in_data['Customer_ListID'];
					   $transactiondata['transactionAmount']   = $amount;
					   $transactiondata['invoiceTxnID']        = $in_data['TxnID'];
					    $transactiondata['merchantID']         = $user_id;
					   $transactiondata['gateway']             = "Auth";
					  $transactiondata['resellerID']           = $this->resellerID;
					  
						$CallCampaign = $this->general_model->triggerCampaign($user_id,$transactiondata['transactionCode']);

						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
					   $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
					
					   
				  
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}
	    	else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
			 }
	             }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
		  }		 
		
		    
		    
               if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
		   	  if($cusproID=="1") {
		   	 redirect('home/invoices','refresh');
		   	 }
        	 redirect('home/invoices','refresh');

    }     




	
	
	
	
		 
	public function create_customer_sale()
	{
           
              
			$invoiceIDs=array();   
		
		if(!empty($this->input->post(null, true))){
			     
			   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   	if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
				
				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$comp_data     = $this->general_model->get_select_data('Xero_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey);
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
		
		$cvv ='';
                  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
		       {	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear1   = substr($exyear,2);
						$expry    = $expmonth.$exyear1;  
						if($this->czsecurity->xssCleanPostInput('cvv')!="")
                        {
                        $cvv = $this->czsecurity->xssCleanPostInput('cvv');
                        }
				  }
				  else 
				  {
					  
					
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear1   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear1;  
                  			$cvv =$card_data['CardCVV'];
						
					}
            
           				 $address1 =  $this->czsecurity->xssCleanPostInput('address1');
	                     $address2 =  '';
	                    $city   =  $this->czsecurity->xssCleanPostInput('city');
	                    $country     =  $this->czsecurity->xssCleanPostInput('country');
	                    $phone       =  $this->czsecurity->xssCleanPostInput('phone');
	                    $state       = $this->czsecurity->xssCleanPostInput('state');
	                     $zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');
					   
						$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
						$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
						$transaction->__set('country',$this->czsecurity->xssCleanPostInput('country'));
						$transaction->__set('city',$this->czsecurity->xssCleanPostInput('city'));
						$transaction->__set('state',$this->czsecurity->xssCleanPostInput('state'));
					
						$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');
						
				        $result = $transaction->authorizeAndCapture($amount,$card_no,$expry);
			
			
				 if($result->response_code == '1')
				 {
				 
				 
				 /* This block is created for saving Card info in encrypted form  */
                 
                 
                   if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    							
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                 
				 
				 
				   if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
				     {
				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
				     }
				          $ref_number=array();
				         if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('Xero_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
        									$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
            						    	$tes = $this->general_model->update_row_data('Xero_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
            							
            						    	$transactiondata= array();
                    				       $transactiondata['transactionID']       = $result->transaction_id;
                    					   $transactiondata['transactionStatus']    = $result->response_reason_text;
                    					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
                    					   $transactiondata['transactionCode']     = $result->response_code;  
                    					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
                    					  $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
                    					 $transactiondata['gatewayID']             = $gatlistval;
                                           $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;	
                    						$transactiondata['transactionType']    = $result->transaction_type;	   
                    					   $transactiondata['customerListID']      = $customerID;
                    					   $transactiondata['transactionAmount']   = $theInvoice['BalanceRemaining'];
                    					    $transactiondata['merchantID']         = $merchantID;
                    					    
                    					   
                    					  $transactiondata['gateway']             = "Auth";
                    					    $transactiondata['invoiceTxnID']      = $inID;
                    					         $transactiondata['resellerID']   = $this->resellerID;
                                        	$ref_number[] = $theInvoice['RefNumber'];
                                        	$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

											if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											}
                    				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                    				       
                    				        $comp_data = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$merchantID));
                    				        $user      = $comp_data['qbwc_username'] ;
                    				       $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
        						}
				              }
						 
				          }else{
				              
				                          $transactiondata= array();
                    				       $transactiondata['transactionID']       = $result->transaction_id;
                    					   $transactiondata['transactionStatus']    = $result->response_reason_text;
                    					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
                    					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
                    					   $transactiondata['transactionCode']     = $result->response_code;  
                    					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
                    					   $transactiondata['gatewayID']            = $gatlistval;
                                           $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;	
                    						$transactiondata['transactionType']      = $result->transaction_type;	   
                    					   $transactiondata['customerListID']        = $customerID;
                    					   $transactiondata['transactionAmount']     = ($result->amount)?$result->amount:$amount;
                    					    $transactiondata['merchantID']           = $merchantID;
                    					   $transactiondata['gateway']   = "Auth";
                    					  $transactiondata['resellerID']   = $this->resellerID;
                     						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

											if(!empty($this->transactionByUser)){
											    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
											    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
											}
                    				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
                    			
				              
				          }
				     
				   if($chh_mail =='1')
        							 {
        							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$merchantID); 
        							  $ref_num =  implode(',',$ref_number); 
        							  $tr_date   =date('Y-m-d H:i:s');
        							  	$toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
        							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_num,$amount, $tr_date   );
        							 } 
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					    $transactiondata= array();
				       $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
					   $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;	
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = ($result->amount)?$result->amount:$amount;
					    $transactiondata['merchantID']         = $merchantID;
					   $transactiondata['gateway']   = "Auth";
					  $transactiondata['resellerID']   = $this->resellerID;
	//	print_r($transactiondata);	
						$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']); 
						if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}  
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				       
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong>.</div>'); 
				 }
				 
				    
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select gateway</strong>.</div>'); 		
			}		
                       
				     
        }  
        redirect('Payments/create_customer_sale','refresh');


	}
	
	
	
		 
	public function create_customer_auth()
	{
          
		if(!empty($this->input->post(null, true))){
			     
			   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
    		  
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID,'qbmerchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
		
		
                  
				  $cardID = $this->czsecurity->xssCleanPostInput('card_list');
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" )
		       {	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear1   = substr($exyear,2);
						$expry    = $expmonth.$exyear1;  
						
				
				  }
				  else 
				  {
					  
					
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear1   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
							$expry    = $expmonth.$exyear1;  
						
					}
            
           				 $address1 =  $this->czsecurity->xssCleanPostInput('address1');
	                     $address2 =  '';
	                    $city   =  $this->czsecurity->xssCleanPostInput('city');
	                    $country     =  $this->czsecurity->xssCleanPostInput('country');
	                    $phone       =  $this->czsecurity->xssCleanPostInput('phone');
	                    $state       = $this->czsecurity->xssCleanPostInput('state');
	                     $zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');
					   
					   
						$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
						$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->__set('address', $this->czsecurity->xssCleanPostInput('address'));
						$transaction->__set('country',$this->czsecurity->xssCleanPostInput('country'));
						$transaction->__set('city',$this->czsecurity->xssCleanPostInput('city'));
						$transaction->__set('state',$this->czsecurity->xssCleanPostInput('state'));
					
						$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');
						
				      $result = $transaction->authorizeOnly($amount,$card_no,$expry);
			
				 if($result->response_code == '1'){
				 
				 
				 /* This block is created for saving Card info in encrypted form  */
				 
                 
                					  if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $merchantID,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    							
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                 
                 $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
			
				 }
				 
		
				  
				 else
				 {
				     
				 
      
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select gateway</strong>.</div>'); 		
			}
				 
				       $transactiondata= array();
				       $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
					   $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;	
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = ($result->amount)?$result->amount:$amount;
					    $transactiondata['merchantID']   = $merchantID;
					     $transactiondata['transaction_user_status']= '5';
					   $transactiondata['gateway']   = "Auth";
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
					     $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				       
				}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
				}		
				 
        }
              
				
		           redirect('Payments/create_customer_auth','refresh');


	}
	
	
   	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
		    	 if($this->session->userdata('logged_in'))
			    {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
			
			     $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			 //print_r($gt_result);die;
		if($tID!='' && !empty($gt_result))
		{
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
				 
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));

			    
				 
				 $customerID = $paydata['customerListID'];
				
				 $amount  =  $paydata['transactionAmount']; 
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				$result     = $transaction->void($tID);
			 
				 if($result->response_code == '1'){
					
			  
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					 if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong></div>'); 
				
				 }
				      $transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']         = $paydata['merchantID'];
					   $transactiondata['gateway']             = "Auth";
					  $transactiondata['resellerID']            = $this->resellerID;
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }
				
					redirect('Payments/payment_capture','refresh');
		}     
			
	}
	
	
		public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
		    	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
			     $tID     = $this->czsecurity->xssCleanPostInput('txnID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gatlistval = $paydata['gatewayID'];
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
		if($tID!='' && !empty($gt_result))
		{
			  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
    		
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey);
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));


			   
				 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				
				
				$result     = $transaction->priorAuthCapture($tID, $amount);
			   
				 if($result->response_code == '1'){
					
					
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					 if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
					
			        $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong></div>'); 
				     
					 }
					 
						$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
                        $transactiondata['transactionCard']     = $result->account_number;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					   $transactiondata['merchantID']         = $paydata['merchantID'];
					   $transactiondata['gateway']   = "Auth";
					  $transactiondata['resellerID']   = $this->resellerID;
					   $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						} 
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }			 
					redirect('Payments/payment_capture','refresh');
        }
              
				
		 return false;   

	}
	
	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
    		
    		
    		     $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			    
    			 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			 	if($tID!='' && !empty($gt_result))
		{  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));


				 $card       = $paydata['transactionCard'];
				 $customerID = $paydata['customerListID'];
				  $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
			      $amount  = $total;
			      $card    = $this->card_model->last_four_digit_card($customerID, $paydata['merchantID']);
            				if(!empty($paydata['invoiceTxnID']))
            				{
            			    $cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$paydata['merchantID']) );
            				$user_id  = $paydata['merchantID'];
            				 $user    =  $cusdata['qbwc_username'];
            		         $comp_id  =  $cusdata['id']; 
            		        
            		        $ittem = $this->general_model->get_row_data('qb_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
            				$ins_data['customerID']     = $paydata['customerListID'];
            			 if(empty($ittem))
            		        {
            		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                                 
            		            
            		        }
            			    
            			    
            			      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
                    			   if(!empty($in_data))
                    			   {
                        			$inv_pre   = $in_data['prefix'];
                        			$inv_po    = $in_data['postfix']+1;
                        			$new_inv_no = $inv_pre.$inv_po;
                                    
                                   
                                   }
            			$ins_data['merchantDataID'] = $paydata['merchantID'];	
            			  $ins_data['creditDescription']     ="Credit as Refund" ;
                           $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
            				$ins_data['creditDate']   = date('Y-m-d H:i:s');
                    	  $ins_data['creditAmount']   = $total;
                          $ins_data['creditNumber']   = $new_inv_no;
                           $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                            $ins_data['Type']         = "Payment";
                    	   $ins_id = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
            					   
            					   $item['itemListID']      =    $ittem['ListID']; 
            				       $item['itemDescription'] =    $ittem['Name']; 
            				       $item['itemPrice'] =$total; 
            				       $item['itemQuantity'] =0; 
            				      	$item['crlineID'] = $ins_id;
            						$acc_name  = $ittem['DepositToAccountName']; 
            				      	$acc_ID    = $ittem['DepositToAccountRef']; 
            				      	$method_ID = $ittem['PaymentMethodRef']; 
            				      	$method_name  = $ittem['PaymentMethodName']; 
            						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
            						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
            				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
            				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
            				  	           );	
            					
            					
            				 if($ins_id && $ins)
            				 {
            					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
            					 
                              }else{
                                  	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>'); 
                              }
            					
                         }
			  
			  
			 
				
				$result     = $transaction->credit($tID, $amount, $card);
				
				   
				
				 if($result->response_code == '1'){  
				     
				     
			
                    $this->customer_model->update_refund_payment($tID, 'AUTH');
                    
                    	if(!empty($paydata['invoiceTxnID']))
            			{	
            			    
            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);
            			 $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
            			}
                
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong></div>'); 
				 }
				     	$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $paydata['merchantID'];
					   $transactiondata['gateway']   = "Auth";
					  $transactiondata['resellerID']   = $this->resellerID;
					   	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  

		        }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }		
              
					if(!empty($this->czsecurity->xssCleanPostInput('payrefund')))
		        {
					redirect('Payments/payment_refund','refresh');
	        	} else {		
				 
				redirect('Payments/payment_transaction','refresh');
				
	        	}
				
        }
              
				
		     

	}
	
	 
  public function get_single_card_data($cardID)
  {  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	         
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->card_model->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->card_model->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
				}		
					
					return  $card;

       }
 

	
	
	public function Auth_Process(){
		
		$tran = new AuthorizeNetAIM('6L7z3mEakZjV','367sbrCsKp878N4j'); 
		$data = $tran->authorizeOnly('20','4111111111111111','1217');
		
		echo "<pre>";
		print_r($data);
		die;
		
	}		
	
	
	public function Void_Process(){
		
		$tran = new AuthorizeNetAIM('6L7z3mEakZjV','367sbrCsKp878N4j'); 
		$data = $tran->void($tran_id);
		echo "<pre>";
		print_r($data);
		die;
		
	}	
	
	
	
	
		 
	public function create_customer_esale()
	{
              
              
              
			 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
				
	   	    	    
		
		if(!empty($this->input->post(null, true))){
			     
			   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  
			if($gatlistval !="" && !empty($gt_result) )
			{
			    $apiloginID  = $gt_result['gatewayUsername'];
				$transactionKey  = $gt_result['gatewayPassword'];
				
				
				
		    	$customerID	= $this->czsecurity->xssCleanPostInput('customerID');
				
				$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$merchantID));
				$companyID  = $comp_data['companyID'];
				
			    $transaction = new AuthorizeNetAIM($apiloginID,$transactionKey);
			    $transaction->setSandbox($this->config->item('auth_test_mode'));
		
		
             
		      		
		       if( $this->czsecurity->xssCleanPostInput('account_number')!="" ){	
				     
                            $acc_name= $this->czsecurity->xssCleanPostInput('account_name');
                            $acc_no  = $this->czsecurity->xssCleanPostInput('account_number');
                            $rout_no = $this->czsecurity->xssCleanPostInput('route_number');
                            
                            
                            
                            $type     =		$this->czsecurity->xssCleanPostInput('acct_type');
                            $hod_name =     $this->czsecurity->xssCleanPostInput('acct_holder_type');
                            $sec_code =     'WEB';
                    }	
				  
				      
				  
				      	$transaction->setECheck($rout_no, $acc_no, $type, $bank_name='Wells Fargo Bank NA', $acc_name, $sec_code);
					   
						$transaction->__set('company',$this->czsecurity->xssCleanPostInput('companyName'));
						$transaction->__set('first_name',$this->czsecurity->xssCleanPostInput('fistName'));
						$transaction->__set('last_name', $this->czsecurity->xssCleanPostInput('lastName'));
						$transaction->__set('address', $this->czsecurity->xssCleanPostInput('baddress'));
						$transaction->__set('country',$this->czsecurity->xssCleanPostInput('bcountry'));
						$transaction->__set('city',$this->czsecurity->xssCleanPostInput('bcity'));
						$transaction->__set('state',$this->czsecurity->xssCleanPostInput('bstate'));
						$transaction->__set('zip',$this->czsecurity->xssCleanPostInput('bzipcode'));
						
						$transaction->__set('ship_to_address', $this->czsecurity->xssCleanPostInput('address'));
						$transaction->__set('ship_to_country',$this->czsecurity->xssCleanPostInput('country'));
						$transaction->__set('ship_to_city',$this->czsecurity->xssCleanPostInput('city'));
						$transaction->__set('ship_to_state',$this->czsecurity->xssCleanPostInput('state'));
						$transaction->__set('ship_to_zip',$this->czsecurity->xssCleanPostInput('zipcode'));
						
						
						$transaction->__set('phone',$this->czsecurity->xssCleanPostInput('phone'));
					
						$transaction->__set('email',$this->czsecurity->xssCleanPostInput('email'));
						$amount = $this->czsecurity->xssCleanPostInput('totalamount');
					
				        $result = $transaction->authorizeAndCapture($amount);
							
			    
				 if($result->response_code == '1'){
				 
				 
				 /* This block is created for saving Card info in encrypted form  */
				 
				
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> '.$result->response_reason_text.'</div>'); 
				 }
				 	$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "AUTH Echeck";
					  $transactiondata['resellerID']   = $this->resellerID;
					   $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

					    if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  
				       
				       
				       
				      
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select gateway.</div>'); 		
			}		
                       
			 redirect('Payments/create_customer_esale','refresh');
        }
              
				


	}
	
	
	
	public function payment_erefund()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			
    		 
			 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}	
    		
    		     $tID     = $this->czsecurity->xssCleanPostInput('txnIDrefund');
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			    
    			 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			
			 	if($tID!='' && !empty($gt_result))
		{  
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
			  
			
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));


				
				
				
				 $customerID = $paydata['customerListID'];
				 $amount     =  $paydata['transactionAmount']; 
			  
			  	$transaction->__set('method','echeck'); 	
				
				
				$result     = $transaction->credit($tID, $amount);
				
				   
				
				 if($result->response_code == '1'){  
				     
				     
				
                    $this->customer_model->update_refund_payment($tID, 'AUTH');
                
			        $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong></div>'); 
				 }
				     	$transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "AUTH Echeck"; 
					  $transactiondata['resellerID']   = $this->resellerID;
					   	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);  

		        }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }		
              
				redirect('Payments/echeck_transaction','refresh');
				
        }
              
				
		     

	}
	
	

	 	
	public function payment_evoid()
	{
		//Show a form here which collects someone's name and e-mail address
		$result = array();
		if(!empty($this->input->post(null, true))){
			
			   if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
			     $tID     = $this->czsecurity->xssCleanPostInput('txnvoidID1');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $gatlistval = $paydata['gatewayID'];
				  
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
		if($tID!='' && !empty($gt_result))
		{
    			$apiloginID  = $gt_result['gatewayUsername'];
    		    $transactionKey  =  $gt_result['gatewayPassword'];
				 
			   	$transaction =  new AuthorizeNetAIM($apiloginID,$transactionKey); 
			   	$transaction->setSandbox($this->config->item('auth_test_mode'));

			    
				 	$transaction->__set('method','echeck'); 
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 
				$result     = $transaction->void($tID);
			 
				 if($result->response_code == '1'){
					
			   
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array( 'transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s') );
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					
					
			        $this->session->set_flashdata('success','Transaction Successfully Cancelled. '); 
				 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong></div>'); 
				
				 }
				      $transactiondata= array();
				        $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result->response_code;  
					     $transactiondata['gatewayID']            = $gatlistval;
                       $transactiondata['transactionGateway']    = $gt_result['gatewayType'] ;
					   
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $this->czsecurity->xssCleanPostInput('customerID');
					   $transactiondata['transactionAmount']   = $result->amount;
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "AUTH Echeck";
					  $transactiondata['resellerID']   = $this->resellerID;
					   	if(!empty($this->transactionByUser)){
						    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);   
				       
				       
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }
				
					redirect('Payments/evoid_transaction','refresh');
		}     
			
	}
	
	
	     
	
   public function delete_pay_transaction()
   {
  	if(!empty($this->czsecurity->xssCleanPostInput('paytxnID')))	
       {  
         if($this->session->userdata('logged_in'))
	       {
				$data 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['merchantID'];
				}
    		$today 				    = date('Y-m-d');
    
    		$txnID           =  $this->czsecurity->xssCleanPostInput('paytxnID'); 
    		$condition		 = array('id'=>$txnID);
    	    $invData         = $this->general_model->get_row_data('customer_transaction',$condition);
    	    
    	 if(!empty($invData))
    	 {    
    	    $input_array =array();
            $input_array['qb_status']  =0;
            $input_array['qb_action']  ="Delete Payment";
            $input_array['createdAt']  =date('Y-m-d H:i:s');
            $input_array['updatedAt']  =date('Y-m-d H:i:s');;
            $input_array['merchantID'] =$user_id;
            $input_array['invoiceID']  =$invData['qbListTxnID'];
     
            $input_array1 =array();
            $input_array1['qb_status']  =0;
            $input_array1['txnType']  ="ReceivePayment";
            $input_array1['createdAt']  =date('Y-m-d H:i:s');
            $input_array1['TimeModified']  =date('Y-m-d H:i:s');;
            $input_array1['merchantID'] =$user_id;
            $input_array1['delTxnID']  =$invData['qbListTxnID'];
     
             
          
    	     
    	        $updateData  =array();
    	      
    	         if($invData)
    	         {
    	           $this->general_model->update_row_data('customer_transaction',$condition,array('transaction_user_status'=>'3'));
			      $in_data = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
                                 $user = $in_data['qbwc_username'];
			          if($user){
                    
						if(!empty($invData['qbListTxnID'])){
							$insID =  $this->general_model->insert_row('tbl_del_transactions',$input_array1);
							$this->quickbooks->enqueue(QUICKBOOKS_DELETE_TXN, $insID, '1','', $user);  
						}
				     	
				    if($invData['invoiceTxnID']!="")
				    {
				       $in_data= $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$invData['invoiceTxnID']));
				       
				       $aap_amount = $in_data['AppliedAmount']+$invData['transactionAmount'];
				       $balance    = $in_data['BalanceRemaining']+$invData['transactionAmount'];
				       
				       $status     = 'false';
				       
				       $this->general_model->update_row_data('qb_test_invoice',array('TxnID'=>$invData['invoiceTxnID']),
				       array('AppliedAmount'=>$aap_amount,'BalanceRemaining'=>$balance,'IsPaid'=>$status));
				        
				        
				    }
				     	
				     	
				     	
				     	
				     	
				     	
				     	
				     	
				     	
				     	
                      }
			        $this->session->set_flashdata('success','Invoice Updated, to know QBO sync status please check: Integration -> Accounting Package -> View Log'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  </strong>Error in process.</div>'); 
				 }
				
    	        	redirect('Payments/evoid_transaction','refresh');
    	        
    	    }
       else
               {
                 
          	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>'); 
               }
       	redirect('Payments/evoid_transaction','refresh');
       
       } else
               {
                   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Invalid request</strong></div>'); 
                    	redirect('Payments/evoid_transaction','refresh');
               }
       
       
   } 
       
       
       
           
          	 
    public function pay_multi_invoice()
    {
    
       
          if($this->session->userdata('logged_in')){
    		$da	= $this->session->userdata('logged_in');
    		
    		$user_id 				= $da['merchID'];
    		}
    		else if($this->session->userdata('user_logged_in')){
    		$da 	= $this->session->userdata('user_logged_in');
    		
    	    $user_id 				= $da['merchantID'];
    		}	
       
    		 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			     $chh_mail =0;
    
    
    
    	
    	 	 $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv');
    		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
    		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');
    		// $merchantID =$user_id;
    		
	
	    	 $cusproID=''; $error='';
           $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
    		   $ref_number=array();
    	if(!empty($invoices))
    	{
    	    foreach($invoices as $invoiceID)
    	    {
    	      
    	   
        		
    	 if($cardID!="" || $gateway!="")
    	 {  
    	       $pay_amounts = $this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
             $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
    		  $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    		 $resellerID = 	$this->resellerID;  
    		 
         	$apiloginID       = $gt_result['gatewayUsername'];
    	    $transactionKey   = $gt_result['gatewayPassword'];
    	     
    		if(!empty($in_data)){ 
    		  $customerID  = $in_data['Customer_ListID'];
		      $Customer_ListID = $in_data['Customer_ListID'];
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    			$companyID = $comp_data['companyID'];
            	   
    	
           if($cardID=='new1')
           {
                        $cardID_upd  =$cardID;
			            $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						
           			  $address1 =  $this->czsecurity->xssCleanPostInput('address1');
	                  $address2 =  $this->czsecurity->xssCleanPostInput('address2');
	                    $city   =  $this->czsecurity->xssCleanPostInput('city');
	                    $country     =  $this->czsecurity->xssCleanPostInput('country');
	                    $phone       =  $this->czsecurity->xssCleanPostInput('contact');
	                    $state       = $this->czsecurity->xssCleanPostInput('state');
	                     $zipcode    =  $this->czsecurity->xssCleanPostInput('zipcode');
           
           }
        else{
          			  $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
        
        				$address1 =     $card_data['Billing_Addr1'];
        				$address2 =     $card_data['Billing_Addr2'];
	                    $city     =      $card_data['Billing_City'];
        			  $zipcode       =      $card_data['Billing_Zipcode'];
        				$state       =     $card_data['Billing_State'];
	                    $country     =      $card_data['Billing_Country'];
        	            $phone       =     $card_data['Billing_Contact'];
	                                     
        }
        
            		      
            		 if(!empty($cardID))
            		 {
    				
    				if( $in_data['BalanceRemaining'] > 0){
    					    $cr_amount = 0;
    					     $amount  =	 $in_data['BalanceRemaining']; 
    					    
    							$amount  =$pay_amounts;
    							$amount           = $amount-$cr_amount;
    					  		$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey);
    					  		$transaction1->setSandbox($this->config->item('auth_test_mode'));
    					  
    							$exyear   = substr($exyear,2);
    							if(strlen($expmonth)==1){
    								$expmonth = '0'.$expmonth;
    							}
    				 	    $expry    = $expmonth.$exyear;  
    							
    
    						     $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
    
    					
    					   if( $result->response_code=="1"){
    						 $txnID      = $in_data['TxnID'];  
    						 $ispaid 	 = 'true';
    						 $bamount =  $in_data['BalanceRemaining']-$result->amount;
    						 if($bamount >0)
    						 	 $ispaid 	 = 'false';
    					      $app_amount = $in_data['AppliedAmount']+(-$result->amount);
    						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
    						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
    						 
    					
    						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
    						 
    						 $user = $in_data['qbwc_username'];
                           
                            if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		    $card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    							
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                           		
                            
    					
    						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
    					   } else{
                        
    					   
    					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  "'.$result->response_reason_text.'"</strong>.</div>'); 
    					   }  
    			
    					   $transactiondata= array();
    				       $transactiondata['transactionID']       = $result->transaction_id;
    					   $transactiondata['transactionStatus']   = $result->response_reason_text;
    					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
    					     $transactiondata['transactionModified'] = date('Y-m-d H:i:s');
    					   $transactiondata['transactionCode']     = $result->response_code;  
    					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
    					   
    						$transactiondata['transactionType']    = $result->transaction_type;	   
    					   $transactiondata['gatewayID']           = $gateway;
                           $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
    					   $transactiondata['customerListID']      = $in_data['Customer_ListID'];
    					   $transactiondata['transactionAmount']   =$pay_amounts;
    					   $transactiondata['invoiceTxnID']       = $in_data['TxnID'];
    					    $transactiondata['merchantID']   = $user_id;
    					   $transactiondata['gateway']   = "Auth";
    					  $transactiondata['resellerID']   = $this->resellerID;
    					  
    					   $CallCampaign = $this->general_model->triggerCampaign($user_id,$transactiondata['transactionCode']);
    					   if(!empty($this->transactionByUser)){
							    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
							    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
							}
    					   $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
    					    if( $result->response_code=="1"){
    					        	 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1','', $user);
    					    }
    				  
    				}else{
    					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Not valid</strong>.</div>'); 
    				}
              
    		     }else{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
    			 }
    		
    		 
    	    	}else{
    	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
    			 }
    	             }else{
    			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
    		  }	
    		  
    	    
    	        	 
    						 if($chh_mail =='1')
        							 {
        							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
        							  $ref_number =''; 
        							  $tr_date   =date('Y-m-d H:i:s');
        							  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                          
        							   $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount,$tr_date );
        							 }
    						 
    						 
    	        
    	        
    	        
    	    }	  
    		  
    		  
    }else{
    
    			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select invoices</strong>.</div>'); 
    		
    }	  
    		   redirect('home/view_customer/'.$cusproID,'refresh');
    		 
    
        }     



	
	
	
	
	
}