<?php

/**
 * This Controller has Paytrace Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */
class PaytracePayment extends CI_Controller
{
    
    private $resellerID;
    private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
		
	
		include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
		include APPPATH . 'third_party/PayTraceAPINEW.php';

     	$this->load->config('quickbooks');
		$this->load->config('paytrace');
		
		$this->load->model('quickbooks');
	    $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
     	$this->load->model('general_model');
		$this->load->model('company_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->model('customer_model');
		$this->load->model('card_model');
		$this->load->model('Xero_models/xero_model');
			$this->load->library('form_validation');
		   if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
		  {
		    
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		  }else{
			redirect('login','refresh');
		  }
  
	
	}
	
	
	public function index(){
	
	}

        	 
        public function pay_invoice()
        {
        
               if($this->session->userdata('logged_in')){
        		$da	= $this->session->userdata('logged_in');
        		
        		$user_id 				= $da['merchID'];
        		}
        		else if($this->session->userdata('user_logged_in')){
        		$da 	= $this->session->userdata('user_logged_in');
        		
        	    $user_id 				= $da['merchantID'];
        		}	
        	   
        	   
        	     	$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
        			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
        			$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
        			
            		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
            
            		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
                    { 
                    
                    
                       $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
                       
                       $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
                       $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
                
                  
                    }
                    
                         $cusproID=''; $error='';
        		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
        		    	
                      
        	    	if ($this->form_validation->run() == true)
        		    {
        	   
        	  	
        		         $cusproID=''; $error='';
                         $cusproID            = $this->czsecurity->xssCleanPostInput('customerProcessID');
               
        	             $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        			     	
        			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
        			     
        			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
        			   
        			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
        			      
        			      $in_data   =    $this->xero_model->get_invoice_data_pay($invoiceID, $user_id);
        			     if($this->czsecurity->xssCleanPostInput('setMail'))
        			     $chh_mail =1;
        			     else
        			      $chh_mail =0;
        			   
        			     
        			    if(!empty( $in_data)) 
        			    {
        			          
            			     $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
            			        $customerID =  $in_data['Customer_ListID'];
            				  	$c_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
            			     	$companyID = $c_data['companyID'];
        			   
        		 
                             	$payusername   = $gt_result['gatewayUsername'];
                    		    $paypassword   = $gt_result['gatewayPassword'];
                                $integratorId = $gt_result['gatewaySignature'];

                    		    $grant_type    = "password";
            		  
        			
        			        	$Customer_ListID = $in_data['Customer_ListID'];
                 
                       
                               $cardID_upd='';
                
                              if($cardID!="new1")
            			      {
            			        
            			        $card_data=   $this->card_model->get_single_card_data($cardID);
            			        $card_no  = $card_data['CardNo'];
        				        $expmonth =  $card_data['cardMonth'];
            					$exyear   = $card_data['cardYear'];
            					$cvv      = $card_data['CardCVV'];
            					$cardType = $card_data['CardType'];
            					$address1 = $card_data['Billing_Addr1'];
            					$city     =  $card_data['Billing_City'];
            					$zipcode  = $card_data['Billing_Zipcode'];
            					$state    = $card_data['Billing_State'];
            					$country  = $card_data['Billing_Country'];
            					
            			      }
            			      else
            			      {
        			          
                			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
                					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
                					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
                					$cardType = $this->general_model->getType($card_no);
                					$cvv ='';
                					if($this->czsecurity->xssCleanPostInput('cvv')!="")
                					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
        					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
        	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
        	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
        	                        $country =$this->czsecurity->xssCleanPostInput('country');
        	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
        	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
        	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
        			         
        			        }    
                  
                      
            		     if(!empty($cardID))
            			 {
            						
            						   $cr_amount = 0;
            						
            						    $amount  = $this->czsecurity->xssCleanPostInput('inv_amount');	
            					     
            						    if(strlen($expmonth)==1)
            						    {
            								$expmonth = '0'.$expmonth;
            							}
            						  
            						$payAPI       = new PayTraceAPINEW();	
            					 
            				    	$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
            
            						//call a function of Utilities.php to verify if there is any error with OAuth token. 
            						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
            
            
            		          if(!$oauth_moveforward)
            		          { 
            		
            		
            		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
            			
                				//set Authentication value based on the successful oAuth response.
                				//Add a space between 'Bearer' and access _token 
                				$oauth_token = sprintf("Bearer %s",$json['access_token']);
                				
                				$name = $in_data['Customer_FullName'];
                				
                				
                			
                			    $invoice_number = rand('500000','200000');
                				$request_data = array(
                                    "amount" => $amount,
                                    "credit_card"=> array (
                                         "number"=> $card_no,
                                         "expiration_month"=>$expmonth,
                                         "expiration_year"=>$exyear ),
                                    "csc"=> $cvv,
                                    "invoice_id"=>$invoice_number,
                                    "billing_address"=> array(
                                        "name"=>$name,
                                        "street_address"=> $address1,
                                        "city"=> $city,
                                        "state"=> $state,
                                        "zip"=> $zipcode
                						)
                					);  
                                 
            				      $request_data = json_encode($request_data); 
            			           $result1     =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
            				       $result      = $payAPI->jsonDecode($result1['temp_json_response']); 
            			
            	
            				   if ( $result1['http_status_code']=='200' )
                               {

                                    // add level three data in transaction
                                    if($result['success']){
                                        $level_three_data = [
                                            'card_no' => $card_no,
                                            'merchID' => $user_id,
                                            'amount' => $amount,
                                            'token' => $oauth_token,
                                            'integrator_id' => $integratorId,
                                            'transaction_id' => $result['transaction_id'],
                                            'invoice_id' => $invoice_number,
                                            'gateway' => 3,
                                        ];
                                        addlevelThreeDataInTransaction($level_three_data);
                                    }

                                     $result['http_status_code'] =$result1['http_status_code'];
            						 $txnID      = $in_data['TxnID'];  
            						 $ispaid 	 = 'true';
            						 
            						 $bamount    = $in_data['BalanceRemaining']-$amount;
            						  if($bamount > 0)
            						  $ispaid 	 = 'false';
            						    $app_amount = $in_data['AppliedAmount']+(-$amount);
            						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
            			
            						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
            						 $this->general_model->update_row_data('Xero_custom_customer',$condition, $data);
            						 
            						
            					
                                  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                				 {
                				 
                				        $this->load->library('encrypt');
                				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
										$cardType       = $this->general_model->getType($card_no);
										$friendlyname   =  $cardType.' - '.substr($card_no,-4);
                				 	
                						$card_condition = array(
                										 'customerListID' =>$customerID, 
                										 'customerCardfriendlyName'=>$friendlyname,
                										);
                										
                							$cid      =    $customerID;			
                							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
                						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
                							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
                					
                							$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;
                					
                						if($crdata > 0)
                						{
                							
                						  $card_data = array('cardMonth'   =>$expmonth,
                										   'cardYear'	 =>$exyear, 
                										   'CardType'    =>$cardType,
                										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                										  'CardCVV'      =>'', 
                										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                										 'customerListID' =>$in_data['Customer_ListID'], 
                										 'customerCardfriendlyName'=>$friendlyname,
                										 'companyID'     =>$companyID,
                										  'merchantID'   => $user_id,
                										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                									
                					
                						     $this->card_model->update_card_data($card_condition, $card_data);					 
                						}
                						else
                						{
                					      $card_data = array('cardMonth'   =>$expmonth,
                										   'cardYear'	 =>$exyear, 
                										   'CardType'    =>$cardType,
                										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                										  'CardCVV'      =>'', 
                										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                										 'customerListID' =>$in_data['Customer_ListID'],
                										 'customerCardfriendlyName'=>$friendlyname,
                										 'companyID'     =>$companyID,
                										  'merchantID'   => $user_id,
                										 'createdAt' 	=> date("Y-m-d H:i:s") );
                										 
                							  
                				          $id1 =    $this->card_model->insert_card_data($card_data);		
                						
                						}
                				
                				 }
                				    
            				 if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['RefNumber']; 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }
            					
            						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
            					 } 
            					 
            					 else
            					 {
                               
                                 
            					   
            							if(!empty($result['errors'])){ $err_msg = $this->getError($result['errors']);}else{ $err_msg = $result['approval_message'];}
            				   
            							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
            					
            				  }	   
            					
            				      $id = $this->general_model->insert_gateway_transaction_data($result,'pay_sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$invoiceID, false, $this->transactionByUser);  
                   
            			    	}
            			    	else
            			    	{
            					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication not valid.</strong></div>'); 
            			    	}	   
            			
            			
                      
            		     }
            		     else
            		     {
            	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
            			 }
            		
            		 
        	    	}
            	    else
            	    {
            	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
            		 }
        	       
        		  }
        		  else
        		  {
        			      $error='Validation Error. Please fill the requred fields';
                		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		  }	
        		  
        		   if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
		   	  if($cusproID=="1") {
		   	 redirect('home/invoices','refresh');
		   	 }
        	 redirect('home/invoices','refresh');
        		    

        
        
            }     



	public function create_customer_sale()
    {
           
        		if(!empty($this->input->post(null, true)))
        		{
        			
			    
                   $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
    			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
    			   if($gatlistval !="" && !empty($gt_result) )
    		       {
			
            		$payusername   = $gt_result['gatewayUsername'];
           	        $paypassword   = $gt_result['gatewayPassword'];
                    $integratorId = $gt_result['gatewaySignature'];

             	    $grant_type    = "password";
             	       if($this->czsecurity->xssCleanPostInput('setMail'))
        			     $chh_mail =1;
        			     else
        			      $chh_mail =0;
            		    
    		  

				
				 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
			
				 $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
    			$companyID  = $comp_data['companyID'];
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
              $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

	
		if(!$oauth_moveforward)
		{
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 $user_id = $merchantID;
			          $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                        $contact    =  $this->czsecurity->xssCleanPostInput('phone');   
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('baddress1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('bcity');
    	                        $country =$this->czsecurity->xssCleanPostInput('bcountry');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('bphone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('bstate');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('bzipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						  
        					
        					}
        					
        					 $exyear   = substr($exyear,2);
						         $expry    = $expmonth.$exyear;
		   	
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				      $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
					
					$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);
					    $request_data = json_encode($request_data);
						$result1     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
						
				       $result = $payAPI->jsonDecode($result1['temp_json_response']); 
			$crtxnID='';
	
				 if ( $result1['http_status_code']=='200' )
				 {
                    // add level three data in transaction
                    if($result['success']){
                        $level_three_data = [
                            'card_no' => $card_no,
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $result['transaction_id'],
                            'invoice_id' => '',
                            'gateway' => 3,
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }
				     $result['http_status_code']=200;
			               $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				          $refnum = array();
				           if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
        									$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
            						    	$tes = $this->general_model->update_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
            						
            						         $id = $this->general_model->insert_gateway_transaction_data( $result,'pay_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$theInvoice['BalanceRemaining'],$user_id,$crtxnID, $this->resellerID,$inID, false, $this->transactionByUser);  
        					        
        								    $refnum[] = $theInvoice['RefNumber'];
        								}
				              }
				              
				              	 if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  implode(',',$refnum); 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  

							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }
						 
				          }
				          else
				          {
				              
				                    $transactiondata= array();
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data( $result,'pay_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
				          }
				     
				     
            				       
            			         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);

            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',

            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',

            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );

            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
				 
				    $this->session->set_flashdata('success','Transaction Successful'); 
				 }else{
					 
	               if(!empty( $result['errors'])){ $err_msg = $this->getError( $result['errors']);}else{ $err_msg =  $result['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
						$result['http_status_code']	=$result1['http_status_code'];
				

				     		   $id = $this->general_model->insert_gateway_transaction_data( $result,'pay_sale',$gatlistval,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);    
				       
              }
				 
				     
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong></div>'); 
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>'); 
		}			
					   

                       
        }
             
				        redirect('Payments/create_customer_sale','refresh'); 
				
		     

	}
	


	 
	public function create_customer_auth()
    {
           

		
		if(!empty($this->input->post(null, true))){
			 if($this->session->userdata('logged_in')){
				$merchantID = $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
				}
			    
               $gatlistval   = $this->czsecurity->xssCleanPostInput('gateway_list');
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
			   
			  if($gatlistval !="" && !empty($gt_result) )
			{
			
    		$payusername   = $gt_result['gatewayUsername'];
   	        $paypassword   = $gt_result['gatewayPassword'];
     	    $grant_type    = "password";
    		    
    		

				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
				$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' => $customerID));
				$companyID  = $comp_data['companyID'];
				
				
			   $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			  
			  $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
       
		//call a function of Utilities.php to verify if there is any error with OAuth token. 
		$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

		//If IsFoundOAuthTokenError results True, means no error 
		//next is to move forward for the actual request 

		if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" ){	
				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
						
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$exyear   = substr($exyear,2);
						$expry    = $expmonth.$exyear;  
				
				  }else {
					  
						    $cardID = $this->czsecurity->xssCleanPostInput('card_list');
						 
                			$card_data= $this->card_model->get_single_card_data($cardID);
                        	$card_no  = $card_data['CardNo'];
							$expmonth =  $card_data['cardMonth'];
							
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
						    if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
					}
					
					$cvv     = $this->czsecurity->xssCleanPostInput('cvv');
					$name    = $this->czsecurity->xssCleanPostInput('fistName').' '.$this->czsecurity->xssCleanPostInput('lastName');
					$address =	$this->czsecurity->xssCleanPostInput('address');
					$country = $this->czsecurity->xssCleanPostInput('country');
					$city    = $this->czsecurity->xssCleanPostInput('city');
					$state   = $this->czsecurity->xssCleanPostInput('state');
				     $amount = $this->czsecurity->xssCleanPostInput('totalamount');		
				      $zipcode  = ($this->czsecurity->xssCleanPostInput('zipcode'))?$this->czsecurity->xssCleanPostInput('zipcode'):'74035';	
					
					$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth ,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "invoice_id"=>rand('500000','200000'),
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);
					    $request_data = json_encode($request_data);
						$result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_AUTHORIZATION );
						
				       $response = $payAPI->jsonDecode($result['temp_json_response']); 
			
	
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
				 if($this->czsecurity->xssCleanPostInput('card_number')!=""){
				 
				        $this->load->library('encrypt');
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);		
				        

						$card_condition = array(
										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
										 'customerCardfriendlyName'=>$friendlyname,
										);
							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
					     
					  $crdata =   $query->row_array()['numrow'];
					
						if($crdata > 0)
						{
							
						   $card_data = array('cardMonth'   =>$expmonth,
										 'cardYear'	     =>$exyear,
										  'companyID'    =>$companyID,
										  'merchantID'   => $merchantID,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',

										  'updatedAt'    => date("Y-m-d H:i:s") 
										  );
					
						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
						}
						else
						{
                            $customerListID = $this->czsecurity->xssCleanPostInput('customerID');
                            $is_default = 0;
                            $checkCustomerCard = checkCustomerCard($customerListID,$merchantID);
                            if($checkCustomerCard == 0){
                                $is_default = 1;
                            }
					     	$card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',

										 'customerListID' =>$customerListID, 
										 'companyID'     =>$companyID,
										  'merchantID'   => $merchantID,
                                          'is_default'             => $is_default,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
								
				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
						
						}
				
				 }
				    $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
			   

				 }
				 
		
				  
				 else
				 {
				 
				    $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>');   
              }
			 $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s'); 
						$transactiondata['transactionType']    = 'pay_auth';	
						$transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
					   $transactiondata['customerListID']      = $customerID;
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['transaction_user_status']= '5';
					    $transactiondata['merchantID']   = $merchantID;
					   $transactiondata['gateway']   = "Paytrace";
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($merchantID,$transactiondata['transactionCode']);

                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        } 
            	          $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);	 
				     
		   }else{
	     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong></div>'); 
		 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Please select gateway</strong></div>'); 
		}			
					   

                       
				        redirect('Payments/create_customer_auth','refresh');
        }
				
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				 if($this->session->userdata('logged_in')){
				$data['login_info'] 	= $this->session->userdata('logged_in');
				
				$user_id 				= $data['login_info']['merchID'];
				}
				if($this->session->userdata('user_logged_in')){
				$data['login_info'] 	= $this->session->userdata('user_logged_in');
				
				$user_id 				= $data['login_info']['merchantID'];
				}	
				$compdata				= $this->customer_model->get_customers_data($user_id);
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_auth', $data);

				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
		 

	
	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			    if($this->session->userdata('logged_in'))
                {
                
                
                $merchantID 				= $this->session->userdata('logged_in')['merchID'];
                }
                if($this->session->userdata('user_logged_in'))
                {
                
                $merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
                }
		    
			
			    $tID         = $this->czsecurity->xssCleanPostInput('txnID2');
			      $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
			    $gatlistval  = $paydata['gatewayID'];
				  
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
	 if( $tID!='' && !empty($gt_result)){	
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
                $integratorId = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
			
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			    
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			    
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];

				 $amount  =  $paydata['transactionAmount']; 
				 $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
			

				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_CAPTURE);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
				   
			
				   
				   
			    	
				}
				
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				    $card_last_number = $paydata['transactionCard'];

                    $card_detail = $this->db1->select('CardType')->from('customer_card_data')->where('merchantID = '.$merchantID.' AND customerCardfriendlyName like "%'.$card_last_number.'%"')->get()->row_array();

                    $cardType = isset($card_detail['CardType']) ? strtolower($card_detail['CardType']) : '';

                    // add level three data in transaction
                    if($response['success']){
                        $level_three_data = [
                            'card_no' => '',
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $response['transaction_id'],
                            'invoice_id' => '',
                            'gateway' => 3,
                            'card_type' => $cardType
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"4");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
				
				    $this->session->set_flashdata('success','Successfully Captured Authorization'); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
              }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']   = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s'); 
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_capture';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   =$paydata['merchantID']; 
					   $transactiondata['gateway']   = "Paytrace";
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        } 
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed</strong>.</div>'); 
		}		
			 		   
	 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }		   
                       
				        redirect('Payments/payment_capture','refresh');
        }
				
		         $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
		
				$this->load->view('template/template_start', $data);
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_transaction', $data);

				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	public function create_customer_void()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true))){
			    if($this->session->userdata('logged_in'))
                {
                
                
                $merchantID 				= $this->session->userdata('logged_in')['merchID'];
                }
                if($this->session->userdata('user_logged_in'))
                {
                
                $merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
                }
		    
			     $tID       = $this->czsecurity->xssCleanPostInput('txnvoidID2');
				   $con     = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
	            $gatlistval  = $paydata['gatewayID'];
				$gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
		 if( $tID!='' && !empty($gt_result)){			
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type   = "password";
			  
			  
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
		 
			
			 
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 
				 $customerID = $paydata['customerListID'];

				 $amount  =  $paydata['transactionAmount']; 
				 $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				

				if($paydata['transactionType']=='pay_auth'){
					 $request_data = array( "transaction_id" => $tID );
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_VOID_TRANSACTION);
					
				   $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    	
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' ){
				 /* This block is created for saving Card info in encrypted form  */
				 
					$condition = array('transactionID'=>$tID);
					
					$update_data =   array('transaction_user_status'=>"3");
					
					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
					
					if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
				
				    $this->session->set_flashdata('success','Transaction Successfully Cancelled.'); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
					    $transactiondata['transactionModified']     = date('Y-m-d H:i:s'); 
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']           = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_void';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   = $paydata['merchantID'];
					   $transactiondata['gateway']   = "Paytrace";
					  $transactiondata['resellerID']   = $this->resellerID;
					   $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Erro: Authentication failed.</strong>.</div>'); 
		}		
					   
		 }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }			   
                       
				        redirect('Payments/payment_capture','refresh');
        }
				
		       $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['id'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_transaction', $data);

				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	



	public function create_customer_refund()
	{
		//Show a form here which collects someone's name and e-mail address
		
      if($this->session->userdata('logged_in')){
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}
		if(!empty($this->input->post(null, true))){
			
	             $tID       = $this->czsecurity->xssCleanPostInput('paytxnID');
				  $con      = array('transactionID'=>$tID);
				 $paydata   = $this->general_model->get_row_data('customer_transaction',$con);
			     $gatlistval  = $paydata['gatewayID'];
				  
				$gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gatlistval));
				
		 if( $tID!='' && !empty($gt_result))
		 {		
				$payusername  = $gt_result['gatewayUsername'];
    		    $paypassword  =  $gt_result['gatewayPassword'];
    		    $grant_type    = "password";
			  
			
			   	  $payAPI = new PayTraceAPINEW();
			  //set the properties for this request in the class
			     $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
				//call a function of Utilities.php to verify if there is any error with OAuth token. 
				$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward)
			{
			//Decode the Raw Json response.
			$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
			//set Authentication value based on the successful oAuth response.
			//Add a space between 'Bearer' and access _token 
			$oauth_token = sprintf("Bearer %s",$json['access_token']);
			
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			if(!empty($paydata))
			{
				 $customerID = $paydata['customerListID'];
			

				 $total   = $this->czsecurity->xssCleanPostInput('ref_amount');
				 $amount  = $total;
			

				if($paydata['transactionCode']=='200')
				{
					 $request_data = array( "transaction_id" => $tID );
					       /******************This is for Invoice Refund Process***********/
					         
            				if(!empty($paydata['invoiceTxnID']))
            				{
            			    $cusdata = $this->general_model->get_select_data('tbl_company',array('qbwc_username','id'), array('merchantID'=>$paydata['merchantID']) );

            				$user_id  = $paydata['merchantID'];
            				 $user    =  $cusdata['qbwc_username'];
            		         $comp_id  =  $cusdata['id']; 
            		        
            		        $ittem = $this->general_model->get_row_data('qb_test_item',array('companyListID'=>$comp_id, 'Type'=>'Payment'));
            				$ins_data['customerID']     = $paydata['customerListID'];
            				 if(empty($ittem))
            		        {
            		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - Create a Payment Type Item to QuickBooks Refund </strong></div>'); 
                               
            		        }
            			    
            			    
            			      $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id));
                    			   if(!empty($in_data))
                    			   {
                        			$inv_pre   = $in_data['prefix'];
                        			$inv_po    = $in_data['postfix']+1;
                        			$new_inv_no = $inv_pre.$inv_po;
                                    
                                   
                                   }
            			$ins_data['merchantDataID'] = $paydata['merchantID'];	
            			  $ins_data['creditDescription']     ="Credit as Refund" ;
                           $ins_data['creditMemo']    = "This credit is given to refund for a invoice ";
            				$ins_data['creditDate']   = date('Y-m-d H:i:s');
                    	  $ins_data['creditAmount']   = $total;
                          $ins_data['creditNumber']   = $new_inv_no;
                           $ins_data['updatedAt']     = date('Y-m-d H:i:s');
                            $ins_data['Type']         = "Payment";
                    	   $ins_id = $this->general_model->insert_row('tbl_custom_credit',$ins_data);	
            					   
            					   $item['itemListID']      =    $ittem['ListID']; 
            				       $item['itemDescription'] =    $ittem['Name']; 
            				       $item['itemPrice'] =$total; 
            				       $item['itemQuantity'] =0; 
            				      	$item['crlineID'] = $ins_id;
            						$acc_name  = $ittem['DepositToAccountName']; 
            				      	$acc_ID    = $ittem['DepositToAccountRef']; 
            				      	$method_ID = $ittem['PaymentMethodRef']; 
            				      	$method_name  = $ittem['PaymentMethodName']; 
            						 $ins_data['updatedAt'] = date('Y-m-d H:i:s');
            						$ins = $this->general_model->insert_row('tbl_credit_item',$item);	
            				  	    $refnd_trr =array('merchantID'=>$paydata['merchantID'], 'refundAmount'=>$total,
            				  	           'creditInvoiceID'=>$paydata['invoiceTxnID'],'creditTransactionID'=>$tID,
            				  	           'creditTxnID'=>$ins_id,'refundCustomerID'=>$paydata['customerListID'],
            				  	           'createdAt'=>date('Y-m-d H:i:s'),'updatedAt'=>date('Y-m-d H:i:s'),
            				  	           'paymentMethod'=>$method_ID,'paymentMethodName'=>$method_name,
            				  	           'AccountRef'=>$acc_ID,'AccountName'=>$acc_name
            				  	           );	
            					
            				

            					
            				 if($ins_id && $ins)
            				 {
            					 $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$user_id), array('postfix'=>$inv_po));
            					 
                               

                              }else{
                                  	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - In Creating QuickBooks Refund </strong></div>'); 
                                  	
                              }
            					
                         }
            			
					 /************End***************/
					 
					 
					 
					 
					 
					// encode Json data by calling a function from json.php
					$request_data = json_encode($request_data);
					 $result = $payAPI->processTransaction($oauth_token, $request_data, URL_TRID_REFUND);
				     $response = $payAPI->jsonDecode($result['temp_json_response']);  
			    		
				}
				
			   
				  
				 if ( $result['http_status_code']=='200' )
				 {
				

				 
				    $this->customer_model->update_refund_payment($tID, 'PAYTRACE');
				
						if(!empty($paydata['invoiceTxnID']))
            			{	
            			    
            			 $this->general_model->insert_row('tbl_customer_refund_transaction',$refnd_trr);
            			 $this->quickbooks->enqueue(QUICKBOOKS_ADD_CREDITMEMO,  $ins_id, '1','', $user);
            			}
					
				
				    $this->session->set_flashdata('success','Successfully Refunded Payment'); 
				 }else{
					 
					  if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $approval_message;}
				   
					$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
                }
				 
				       $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');
					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = $paydata['transactionCard'];
					   $transactiondata['gatewayID']          = $gatlistval;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_refund';	   
					   $transactiondata['customerListID']      = $paydata['customerListID'];
					   $transactiondata['transactionAmount']   = $amount;
					    $transactiondata['merchantID']   =$paydata['merchantID'];
					   $transactiondata['gateway']   = "Paytrace";
					  $transactiondata['resellerID']   = $this->resellerID;
					  $CallCampaign = $this->general_model->triggerCampaign($paydata['merchantID'],$transactiondata['transactionCode']);
                        if(!empty($this->transactionByUser)){
                            $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                            $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                        }
				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
				       
			}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Invalid Transactions</strong>.</div>'); 
		}		       
				       
		}else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication failed.</strong>.</div>'); 
		}		
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }				   
		
                   
      }
              
			    
				       		if(!empty($this->czsecurity->xssCleanPostInput('payrefund')))
		        {
					redirect('Payments/payment_refund','refresh');
	        	} else {		
				 
				redirect('Payments/payment_transaction','refresh');
				
	        	}	
		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('logged_in');
				$user_id 				= $data['login_info']['merchID'];
			
				$compdata				= $this->customer_model->get_customers($user_id);
				
				$data['customers']		= $compdata	;
				
		
				$this->load->view('template/template_start', $data);
				
				
				
				$this->load->view('template/page_head', $data);
				$this->load->view('pages/payment_refund', $data);

				$this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);


	}
	
	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 
	 
	 
	
	public function pay_multi_invoice()
	{
	    
	    


      if($this->session->userdata('logged_in'))
      {
		$da	= $this->session->userdata('logged_in');
		
		$user_id 				= $da['merchID'];
		}
		else if($this->session->userdata('user_logged_in')){
		$da 	= $this->session->userdata('user_logged_in');
		
	    $user_id 				= $da['merchantID'];
		}	
	
	
	
	
        if($this->czsecurity->xssCleanPostInput('setMail'))
        $chh_mail =1;
        else
        $chh_mail =0;
        
        
        

	     $invoices            = $this->czsecurity->xssCleanPostInput('multi_inv'); 
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
	    $customerID = $this->czsecurity->xssCleanPostInput('customerID');
		$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$user_id));
		$companyID  = $comp_data['companyID'];
	
		 $cusproID=''; $error='';
         $cusproID            = $this->czsecurity->xssCleanPostInput('customermultiProcessID');

            
		$resellerID = $this->resellerID;
   
if(!empty($invoices ))
{
	 	foreach($invoices as $invoiceID)
	 	{
	 	       $pay_amounts=$this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
              $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
		 
			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
             	$payusername   = $gt_result['gatewayUsername'];
    		    $paypassword   = $gt_result['gatewayPassword'];
                $integratorId = $gt_result['gatewaySignature'];

    		    $grant_type    = "password";
    		  
			
        	 if($cardID!="" || $gateway!="")
        	 {  
             
        		 
        		 if(!empty($in_data)){ 
        			
        				$Customer_ListID = $in_data['Customer_ListID'];
                 
                               
                     if($cardID=='new1')
           {
               
                        	
				$cardID_upd  =$cardID;
				$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
				$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
				$cardType       = $this->general_model->getType($card_no);
				$friendlyname   =  $cardType.' - '.substr($card_no,-4);

					
           }
           else
           {
               
             
                        $card_data    =   $this->card_model->get_single_card_data($cardID); 
                        $card_no  = $card_data['CardNo'];
                       	$cvv      =  $card_data['CardCVV'];
						$expmonth =  $card_data['cardMonth'];
						$exyear   = $card_data['cardYear'];
           }
        

            		      
            		 if(!empty($cardID))
            		 {
            			
        						
                            $cr_amount = 0;
                            $amount  =	 $in_data['BalanceRemaining']; 
                            
                            $amount  =$pay_amounts;
                            $amount    = $amount-$cr_amount;
                            
                          

                            if(strlen($expmonth)==1){
                            $expmonth = '0'.$expmonth;
                            }
                            
                            $payAPI  = new PayTraceAPINEW();	

        					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
        
        						//call a function of Utilities.php to verify if there is any error with OAuth token. 
        						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
        
        
        		          if(!$oauth_moveforward){ 
        		
        		
        		            $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        			
        				//set Authentication value based on the successful oAuth response.
        				//Add a space between 'Bearer' and access _token 
        				$oauth_token = sprintf("Bearer %s",$json['access_token']);
        				
        				$name = $in_data['Customer_FullName'];
        				$address = $in_data['ShipAddress_Addr1'];
        				$city = $in_data['ShipAddress_City'];
        				$state = $in_data['ShipAddress_State'];
        				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
        				
        			
        			
        				$request_data = array(
                            "amount" => $amount,
                            "credit_card"=> array (
                                 "number"=> $card_no,
                                 "expiration_month"=>$expmonth,
                                 "expiration_year"=>$exyear ),
                            "csc"=> $cvv,
                            "invoice_id"=>rand('700000','800000'),
                            "billing_address"=> array(
                                "name"=>$name,
                                "street_address"=> $address,
                                "city"=> $city,
                                "state"=> $state,
                                "zip"=> $zipcode
        						)
        					);  
                        
        				      $request_data = json_encode($request_data); 
        			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
        				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
        			
        	
        				   if ( $result['http_status_code']=='200' )
                           {
                                // add level three data in transaction
                                if($response['success']){
                                    $level_three_data = [
                                        'card_no' => $card_no,
                                        'merchID' => $user_id,
                                        'amount' => $amount,
                                        'token' => $oauth_token,
                                        'integrator_id' => $integratorId,
                                        'transaction_id' => $response['transaction_id'],
                                        'invoice_id' => $invoiceID,
                                        'gateway' => 3,
                                    ];
                                    addlevelThreeDataInTransaction($level_three_data);
                                }
                                
        						 $txnID      = $in_data['TxnID'];  
        						 $ispaid 	 = 'true';
        						 $bamount  =	 $in_data['BalanceRemaining']-$amount;
        						 if($bamount>0)
        						 $ispaid 	 = 'false';
        					  $app_amount = $in_data['AppliedAmount']+(-$amount);
						        $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
        						 $condition  = array('TxnID'=>$in_data['TxnID'] );	
        						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
        						 
        						 $user = $in_data['qbwc_username'];
        					
        							 if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
                    				 {
                    				 
                    				        $this->load->library('encrypt');
                    				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');	
                    				 		
											$cardType       = $this->general_model->getType($card_no);
											$friendlyname   =  $cardType.' - '.substr($card_no,-4);
                    				       

                    						$card_condition = array(
                    										 'customerListID' =>$this->czsecurity->xssCleanPostInput('customerID'), 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										);
                    							$cid      =    $this->czsecurity->xssCleanPostInput('customerID');			
                    							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
                    						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
                    							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
                    					
                    						$query =	$this->db1->query("select count(*) as numrow  from customer_card_data where customerListID ='".$cid."' and   customerCardfriendlyName ='".$friendlyname."' ")	;			
                    					    
                    					  $crdata =   $query->row_array()['numrow'];
                    					
                    						if($crdata > 0)
                    						{
                    							
                    						  $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 

                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$in_data['Customer_ListID'], 
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                    									
                    					
                    						   $this->db1->update('customer_card_data',$card_condition, $card_data);				 
                    						}
                    						else
                    						{
                    						    
                                                $is_default = 0;
                                                $checkCustomerCard = checkCustomerCard($in_data['Customer_ListID'],$user_id);
                                                if($checkCustomerCard == 0){
                                                    $is_default = 1;
                                                }
                    					        $card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										   'CardType'    =>$cardType,
                    										  'CustomerCard' =>$this->card_model->encrypt($card_no),
                    										  'CardCVV'      =>'', 

                    										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
                    	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
                    	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
                    	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
                    	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
                    	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
                    	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
                    										 'customerListID' =>$in_data['Customer_ListID'],
                    										 'customerCardfriendlyName'=>$friendlyname,
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                                                              'is_default'             => $is_default,
                    										 'createdAt' 	=> date("Y-m-d H:i:s") );
                    										 
                    									 
                    								
                    				            $id1 = $this->db1->insert('customer_card_data', $card_data);	
                    						
                    						}
                    				
                    		 }
				    
                               
                            if($chh_mail =='1')
                            {
                                $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
                                $ref_number =  $in_data['RefNumber']; 
                                $tr_date   =date('Y-m-d H:i:s');
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                              

                                $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
                            }
        						
                                
        						  $this->session->set_flashdata('success','Successfully Processed Invoice');  
        					   } else{
                           
                                if($cardID_upd=='new1')
                                 {
                                   $this->db1->where(array('CardID'=>$cardID));
                                   $this->db1->delete('customer_card_data');
                                 }
        					   
        							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
        				   
        							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
        					
        				  }	   
        					   $transactiondata= array();
        					   if(isset($response['transaction_id'])){
        				       $transactiondata['transactionID']       = $response['transaction_id'];
        					   }else{
        						    $transactiondata['transactionID']  = '';
        					   }
        					   $transactiondata['transactionStatus']    = $response['status_message'];
        					   $transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
        					   $transactiondata['transactionCode']     = $result['http_status_code'];
        					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
        					     $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
        						$transactiondata['transactionType']    = 'pay_sale';
        						$transactiondata['gatewayID']          = $gateway;
                               $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;							
        					   $transactiondata['customerListID']      = $Customer_ListID;
        					   $transactiondata['invoiceTxnID']      = $invoiceID;
        					   
        					   $transactiondata['transactionAmount']   = $amount;
        					    $transactiondata['merchantID']   = $user_id;
        					   $transactiondata['gateway']   = "Paytrace";
        					  $transactiondata['resellerID']   = $this->resellerID;
        					  
        		              $CallCampaign = $this->general_model->triggerCampaign($user_id,$transactiondata['transactionCode']);
        					    if(!empty($this->transactionByUser)){
                                    $transactiondata['transaction_by_user_type'] = $this->transactionByUser['type'];
                                    $transactiondata['transaction_by_user_id'] = $this->transactionByUser['id'];
                                }
        				       $id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
        			 if ( $result['http_status_code']=='200' )
                     {
					   $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $id, '1','', $user); 
                     }
                     
                  
        				}else{
        					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Authentication not valid.</strong></div>'); 
        				}	   
        			
        			
                  
        		     }else{
        	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Customer has no card</strong>.</div>'); 
        			 }
        		
        		 
        	    	}else{
        	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice</strong>.</div>'); 
        			 }
	        }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  Select Gateway and Card</strong>.</div>'); 
            }
                        
            		}
	}
	else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - Gateway and Card are required</strong>.</div>'); 
		  }		 
		   	 if($cusproID!=""){
			 	 redirect('home/view_customer/'.$cusproID,'refresh');
			 }
		   	 else{
		   	 redirect('home/invoices','refresh');
		   	 }

   

	    
	    
	}
	
	
	
	
	
	
	
	
}




