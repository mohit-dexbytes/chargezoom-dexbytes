<?php
class Xero extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
	    $this->load->model('general_model');
	   $this->load->library('session');
		
		 if($this->session->userdata('logged_in')!="")
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	   	redirect('Integration/home','refresh'); 
	}

/******************** Authentication Started ************************/	
		
			
		function testLinks()
        {
              $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
           
           
          
            if (isset($_SESSION['access_token']) || XRO_APP_TYPE == 'Private')
            {
                
            $in_data['accessToken'] = $_SESSION['access_token'];
            $in_data['access_token_secret'] = $_SESSION['oauth_token_secret'];
                $merchID  = $this->session->userdata('logged_in')['merchID'];
                $in_data['merchantID'] =  $merchID;
                
                	$chk_condition = array('merchantID'=>$merchID);
                	$tok_data = $this->general_model->get_row_data('tbl_xero_token',$chk_condition);
                
               if(!empty($tok_data)){
			
			    $this->general_model->update_row_data('tbl_xero_token',$chk_condition, $in_data);
			   }
			   else
				{
					 $this->general_model->insert_row('tbl_xero_token', $in_data);
				  
				}
				
				    $data1['appIntegration']    = "4";
					$data1['merchantID']    = $merchID;
				
				$app_integration = $this->general_model->get_row_data('app_integration_setting',$chk_condition);
			 if(!empty($app_integration)){
			
			    $this->general_model->update_row_data('app_integration_setting',$chk_condition, $data1);
				$user = $this->session->userdata('logged_in');
				$user['active_app'] = '4';
				$this->session->set_userdata('logged_in',$user);
			   }
			   else
				{
					 $this->general_model->insert_row('app_integration_setting', $data1);
					  
				     $user = $this->session->userdata('logged_in');
				     $user ['active_app'] = '4';
				     $this->session->set_userdata('logged_in',$user);
				  
				}
				
					redirect(base_url('Xero_controllers/Xero/success_auth'));
   
                      
				}
                
                
        
               
                if (XRO_APP_TYPE !== 'Private' && isset($_SESSION['access_token'])) {
                    echo '<li><a href="?wipe=1">Start Over and delete stored tokens</a></li>';
                } elseif(XRO_APP_TYPE !== 'Private') {
                    
                    $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/xeroAuth_process');
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
                   
                }
      
            
        }
        
        
     
        function persistSession($response)
        {
            if (isset($response)) {
                $_SESSION['access_token']       = $response['oauth_token'];
                $_SESSION['oauth_token_secret'] = $response['oauth_token_secret'];
              	if(isset($response['oauth_session_handle']))  $_SESSION['session_handle']     = $response['oauth_session_handle'];
            } else {
                return false;
            }
        
        }
        
        
        function retrieveSession()
        {
            if (isset($_SESSION['access_token'])) {
                $response['oauth_token']            =    $_SESSION['access_token'];
                $response['oauth_token_secret']     =    $_SESSION['oauth_token_secret'];
              
                return $response;
            } else {
                return false;
            }
        
        }
        
        function outputError($XeroOAuth)
        {
              $error = 'Error: ' . $XeroOAuth->response['response'] . PHP_EOL;
              $this->session->set_flashdata('message', '<div class="alert alert-danger">'.$error.'</div>');
            
            	redirect(base_url('Xero_controllers/Xero/xero_int'));
            
            $this->pr($XeroOAuth);
        }
        
        /**
         * Debug function for printing the content of an object
         *
         * @param mixes $obj
         */
        function pr($obj)
        {
        
            if (!($this->is_cli()))
                echo '<pre style="word-wrap: break-word">';
            if (is_object($obj))
                print_r($obj);
            elseif (is_array($obj))
                print_r($obj);
            else
                echo $obj;
            if (!($this->is_cli()))
                echo '</pre>';
        }
        
        function is_cli()
        {
            return (PHP_SAPI == 'cli' && empty(getClientIpAddr()));
        }
		


   public function xero_int()
	{
        if($this->session->userdata('logged_in')['merchID']!='')
        {
         $sess_array =	$this->session->userdata('logged_in');
          $sess_array['active_app'] = '0';
          $this->session->set_userdata('logged_in', $sess_array);

        } 
    
    
         
	    $data['primary_nav']  = primary_nav();
		$data['template']     = template_variable();
			  
	    define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );
		
	 
		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0',
			
		);
		if (XRO_APP_TYPE == "Private" || XRO_APP_TYPE == "Partner")
		{
        	$signatures ['rsa_private_key'] = FCPATH.'uploads/xero_keys/privatekey.pem';
        
        	$signatures ['rsa_public_key'] = FCPATH .'uploads/xero_keys/publickey.cer';
        }
		
		
	

		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );
    	
		$initialCheck = $XeroOAuth->diagnostics();
		
		$checkErrors = count ( $initialCheck ); 
		if ($checkErrors > 0) 
        {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
			
		
			
		} 
        else 
        {
			
			$here = XeroOAuth::php_self (); 
			$oauthSession = $this->retrieveSession ();
		  
		
			if (isset ( $_REQUEST ['oauth_verifier'] )) 
			{
			    
			   
			     
			   
				$XeroOAuth->config ['access_token'] = $_SESSION ['oauth'] ['oauth_token'];
				$XeroOAuth->config ['access_token_secret'] = $_SESSION ['oauth'] ['oauth_token_secret'];
				
				
				$code = $XeroOAuth->request ( 'GET', $XeroOAuth->url ( 'AccessToken', '' ), array (
						'oauth_verifier' => $_REQUEST ['oauth_verifier'],
						'oauth_token' => $_REQUEST ['oauth_token'] 
				) );
				
				if ($XeroOAuth->response ['code'] == 200) {
					
					$response = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
					$session = $this->persistSession ( $response );
					
					unset ( $_SESSION ['oauth'] );
              
					header ( "Location: {$here}" );
				} else {
					$this->outputError ( $XeroOAuth );
				}
				// start the OAuth dance
			}
           elseif (isset ( $_REQUEST ['authenticate'] ) || isset ( $_REQUEST ['authorize'] )) {
				$params = array (
						'oauth_callback' => OAUTH_CALLBACK 
				);
				
				$response = $XeroOAuth->request ( 'GET', $XeroOAuth->url( 'RequestToken', '' ), $params );
          
				if ($XeroOAuth->response ['code'] == 200) 
				{
					
					$scope = "";
					
					if ($_REQUEST ['authenticate'] > 1)
						$scope = 'payroll.employees,payroll.payruns,payroll.timesheets';
					
					$_SESSION ['oauth'] = $XeroOAuth->extract_params ( $XeroOAuth->response ['response'] );
					
					$authurl = $XeroOAuth->url ( "Authorize", '' ) . "?oauth_token={$_SESSION['oauth']['oauth_token']}&scope=" . $scope;
               
               
					redirect($authurl,'refresh');
				
				} 
                else {
                
               
					$this->outputError ( $XeroOAuth );
				}
			}
			
			
		$this->testLinks();
		}
	}
	
	
	public function success_auth()
	{
	          $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  
			   $merchID  = $this->session->userdata('logged_in')['merchID'];
			
			  $condition = array('merchID'=> $merchID); 
			  
		   	  $this->general_model->update_row_data('tbl_merchant_data',$condition,array('firstLogin'=>1));
		   	  $user = $this->session->userdata('logged_in');
		   	  
		   	  
					 if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID)) == '0')
					 {  
					     
						 $fromEmail       = $user['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata){
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchID,
									 'fromEmail'      => DEFAULT_FROM_EMAIL,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
						 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                 }
					 
				  
		   	  
		   	  
		   	  
			  $user['firstLogin'] = '1';
			  $this->session->set_userdata('logged_in',$user);
			  	$merchant_condition = ['merchID' => $merchID];
	            $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$merchant_condition);
			  	if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
              	{
				  /* Start campaign in hatchbuck CRM*/  
	                $this->load->library('hatchBuckAPI');
	                
	                $merchantData['merchant_type'] = 'Xero';        
	                
	                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                if($status['statusCode'] == 400){
	                    $resource = $this->hatchbuckapi->createContact($merchantData);
	                    if($resource['contactID'] != '0'){
	                        $contact_id = $resource['contactID'];
	                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_SETUP_WIZARD);
	                    }
	                }
	                /* End campaign in hatchbuck CRM*/ 
	            }
			  
			  
					 if($this->general_model->get_num_rows('tbl_email_template', array('merchantID'=>$merchID)) == '0')
					 {  
					     
						 $fromEmail       = $user['merchantEmail'];
						
						   $templatedatas =  $this->general_model->get_table_data('tbl_email_template_data','');
						
						  foreach($templatedatas as $templatedata){
						   $insert_data = array('templateName'  =>$templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									 'merchantID'      => $merchID,
									 'fromEmail'      => DEFAULT_FROM_EMAIL,
									 'message'		  => $templatedata['message'],
									 'emailSubject'   => $templatedata['emailSubject'],
									 'createdAt'      => date('Y-m-d H:i:s') 	
								 );
						 $this->general_model->insert_row('tbl_email_template', $insert_data);
						
						}
	                 }
					 
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/xero_Auth_success');
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	
	
	
	/************************* Authentication Ended ************************/
	
	

	
	
}
	