<?php
class Xero_plan_product extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
	    $this->load->model('general_model');
	   $this->load->library('session');
		
	
		 if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='4')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	   	redirect('Integration/home','refresh'); 
	}

	public function Item_detailes(){

		define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		
	
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if($this->session->userdata('logged_in')){
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}
			if($this->session->userdata('user_logged_in')){
				$user_id = $this->session->userdata('user_logged_in')['merchantID'];
				$merchID = $user_id;
			}
		     $val = array(
			'merchantID' => $merchID,
			);
     
	     $xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
		
		
			
    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
        
        $response = $XeroOAuth->request('GET', $XeroOAuth->url('Items', 'core'), array());

        	
         if ($XeroOAuth->response['code'] == 200) {
               $items = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);
               
               $items_arr = json_decode( json_encode($items) , 1);
               
                 
               
               foreach ($items_arr['Items'] as $Itemdata) {
                    
                
                       
                         if(!empty($Itemdata['ItemID'])){ 
                if (array_key_exists("QuantityOnHand",$Itemdata)){
                  $QuantityOnHand = $Itemdata['QuantityOnHand'];
                    }
                    else{
                    	$QuantityOnHand = '0';
                    }  
               if (array_key_exists("COGSAccountCode",$Itemdata['PurchaseDetails'])){
                  	 $AccountCode = $Itemdata['PurchaseDetails']['COGSAccountCode'];
                    }
                    else if(array_key_exists("AccountCode",$Itemdata['PurchaseDetails'])){
                    	$AccountCode = $Itemdata['PurchaseDetails']['AccountCode'];
                    }
                    else{
                    	$AccountCode = '0';
                    }
                    
                 
                   
                          $Xero_item_details = array(
				"productID" => $Itemdata['ItemID'],
				"Name" => $this->db->escape_str($Itemdata['Name']),
				"SalesDescription" => $this->db->escape_str($Itemdata['Description']),
				"saleCost" => $this->db->escape_str(number_format($Itemdata['SalesDetails']['UnitPrice'],2, '.', '')),
				"QuantityOnHand" => $this->db->escape_str($QuantityOnHand),
				"TimeModified" => $this->db->escape_str($Itemdata['UpdatedDateUTC']),
	    		"AccountCode" => $this->db->escape_str($AccountCode),
				"IsActive" => "active",
				"Code" => $this->db->escape_str($Itemdata['Code']),
				"merchantID" => $merchID
				);
				
				
				
                    if(!empty($this->general_model->get_row_data('Xero_test_item', array('merchantID'=>$merchID,'productID'=>$Itemdata['ItemID'])))) 
                    $this->general_model->update_row_data('Xero_test_item',array('merchantID'=>$merchID,'productID'=>$Itemdata['ItemID']), $Xero_item_details);  
                    else
			    	$this->general_model->insert_row('Xero_test_item', $Xero_item_details);  
                         
                     }
                     }
              
         }else{

         }

		}

        $data['plans'] = $this->general_model->get_table_data('Xero_test_item',$val);
        
         $data['primary_nav']  = primary_nav();
		 $data['template']   = template_variable();

		 $this->load->view('template/template_start', $data);
		 $this->load->view('template/page_head', $data);
		 $this->load->view('Xero_views/plan_products',$data);
		 $this->load->view('template/page_footer',$data);
		 $this->load->view('template/template_end', $data);

	}
	
	public function create_product_services()
	{
		define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
			}
		
		     $val = array(
			'merchantID' => $merchID,
			);

		}

		$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
		
    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
        

        if(!empty($this->input->post(null, true))){

        	$productName = $this->czsecurity->xssCleanPostInput('productName');
       			$productCode = $this->czsecurity->xssCleanPostInput('productCode');
            	$productDesc = $this->czsecurity->xssCleanPostInput('productDesc');
            	$salePrice = $this->czsecurity->xssCleanPostInput('salePrice');
            	$quantity = $this->czsecurity->xssCleanPostInput('quantity');	


           if($this->czsecurity->xssCleanPostInput('productID')!="" ){
           	
            $xml = "<Items><Item>
				  <Code>".$this->czsecurity->xssCleanPostInput('productID')."</Code>
				  <Name>".$productName."</Name>
				  <Description>".$productDesc."</Description>
				  <SalesDetails>
				    <UnitPrice>".$salePrice."</UnitPrice>
				  </SalesDetails>
				  
				</Item></Items>";


         $response = $XeroOAuth->request('POST', $XeroOAuth->url('Items', 'core'), array(), $xml);
           
	         if ($XeroOAuth->response['code'] == 200) {
	          	 $this->session->set_flashdata('success','Success'); 

	                redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }
	          else{
	          		$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
	       		
	   	        	 redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }	

            }

           else{
            $xml = "<Item>
				  <Code>".$productCode."</Code>
				  <Name>".$productName."</Name>
				  <Description>".$productDesc."</Description>
				  <SalesDetails>
				    <UnitPrice>".$salePrice."</UnitPrice>
				  </SalesDetails>
				 
				</Item>";


         $response = $XeroOAuth->request('POST', $XeroOAuth->url('Items', 'core'), array(), $xml);
           
	         if ($XeroOAuth->response['code'] == 200) {
	          	 $this->session->set_flashdata('success','Success'); 

	                redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }
	          else{
	          		$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
	       		
	   	        	 redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }	

	      }

        }

      if($this->uri->segment('4')){
					  $productID  			  = $this->uri->segment('4');	
					  $con                = array('productID'=> $productID);  
					   $data['item_pro'] = $this->general_model->get_row_data('Xero_test_item',$con);   
				} 


       $data['primary_nav']  = primary_nav();
			$data['template']   = template_variable();
		    
		    
		    $this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('Xero_views/create_product',$data);
			$this->load->view('template/page_footer',$data);
			$this->load->view('template/template_end', $data);

	}
	
	public function plan_product_details(){
		
	        	
				 $productID             =  $this->czsecurity->xssCleanPostInput('productID'); 
				 
		         $condition 		 = array('productID'=>$productID);
				$plandata		     = $this->general_model->get_row_data('Xero_test_item', $condition);
				if(!empty($plandata))
				{
			?>	
		 <table class="table table-bordered table-striped table-vcenter"   >
            <thead>
                <tr> 
                    <th class="text-left">Attribute</th>
                    <th class="visible-lg text-left">Details</th>
                </tr>
            </thead>
            <tbody>
			  	<tr>
					<th class="text-left"><strong>Item Code</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['Code']; ?></a></td>
			  </tr>	
			<tr>
					<th class="text-left"><strong>Name</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['Name']; ?></a></td>
			</tr>
           			
			<tr>
					<th class="text-left"><strong>Description</strong></th>
					<td class="text-left visible-lg"><?php echo ($plandata['SalesDescription'])?$plandata['SalesDescription']:'---'; ?></a></td>
			</tr>	
           	

            	<tr>
					<th class="text-left"><strong>  Sales Price</strong></th>
					<td class="text-left visible-lg">$<?php echo number_format($plandata['saleCost'], 2); ?></a></td>
			</tr>	
				
		
			<tr>
					<th class="text-left"><strong>  Sale Description</strong></th>
					<td class="text-left visible-lg"><?php echo $plandata['SalesDescription']; ?></a></td>
			</tr>
			
			<tr>
					<th class="text-left"><strong>  Created Time</strong></th>
					<td class="text-left visible-lg"><?php if(!empty($plandata['TimeCreated'])) echo date('M d, Y', strtotime($plandata['TimeCreated']));else echo ''; ?></a></td>
			</tr>	
			
			<tr>
					<th class="text-left"><strong>  Modified Time</strong></th>
					<td class="text-left visible-lg"><?php if(!empty($plandata['TimeModified'])) echo date('M d, Y', strtotime($plandata['TimeModified'])); else echo '';?></a></td>
			</tr>	
			
			</tbody>
        </table>
				
	<?php     }   
		
		 die;
		
		
	}
	
	public function det_plan_product(){
	 	
	 	define ( 'BASE_PATH', dirname(__FILE__) );

		define ( "XRO_APP_TYPE", "Public" );

		$useragent = "Chargezoom";
		
		$data= $this->general_model->get_table_data('xero_config','');
		 
	    if(!empty($data))
		{
				
			foreach($data as $dt)
			{
	           $url = $dt['oauth_callback'];
	           $key = $dt['consumer_key'];
		       $secret = $dt['shared_secret'];
	    	}
		}
		define ( "OAUTH_CALLBACK", $url );

		$signatures = array (
				'consumer_key' =>  $key,
				'shared_secret' => $secret,
				// API versions
				'core_version' => '2.0',
				'payroll_version' => '1.0',
				'file_version' => '1.0' 
		);
 
		$XeroOAuth = new XeroOAuth ( array_merge ( array (
				'application_type' => XRO_APP_TYPE,
				'oauth_callback' => OAUTH_CALLBACK,
				'user_agent' => $useragent 
		), $signatures ) );

		$initialCheck = $XeroOAuth->diagnostics ();
		$checkErrors = count ( $initialCheck );
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ( $initialCheck as $check ) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if($this->session->userdata('logged_in')){
			$user_id = $this->session->userdata('logged_in')['merchID'];
			$merchID = $user_id;
			}
		
		     $val = array(
			'merchantID' => $merchID,
			);

		}

		$xero_data = $this->general_model->get_row_data('tbl_xero_token',$val);
		
    	$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
        $XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];
        
        $item_id = $this->czsecurity->xssCleanPostInput('productID');

        $xml = "<Item>
				  <ItemID>". $item_id."</ItemID>
				</Item>";
        $delete_item = 'Items/'.$item_id;


       $response = $XeroOAuth->request('DELETE', $XeroOAuth->url($delete_item, 'core'), array(), $xml);
      
          if ($XeroOAuth->response['code'] == 200) {
	          	 $this->session->set_flashdata('success','Success'); 

	                redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }
	          else{
	          		$this->session->set_flashdata('success','Successfully Deleted');
	       		
	   	        	 redirect(base_url('Xero_controllers/Xero_plan_product/item_detailes'));
	          }	


	 }


}
