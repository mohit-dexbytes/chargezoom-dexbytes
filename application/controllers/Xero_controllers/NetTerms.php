<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class NetTerms extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('general_model');
		$this->load->model('Xero_models/xero_customer_model');

		
		 $this->db1 = $this->load->database('otherdb', TRUE);
		
		 if($this->session->userdata('logged_in')!="" &&  $this->session->userdata('logged_in')['active_app']=='4')
		  {
		   
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		 
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index(){
	   	redirect('Integration/home','refresh'); 
	}
	
	public function terms(){
	    
	          $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('logged_in');
			  $user_id    = $data['login_info']['merchID'];
	        
	         $condition	= array('merchantID'=>$user_id);
	         
			 $data['netterms']  = $this->xero_customer_model->get_terms_data($user_id);
			 
			
	          $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('Xero_views/page_terms', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	
	public function create_terms(){
	    
	     $data['login_info'] = $this->session->userdata('logged_in');
	     $user_id    = $data['login_info']['merchID'];
	     
	     $termID = $this->czsecurity->xssCleanPostInput('termID');
	     $dfid = $this->czsecurity->xssCleanPostInput('dfID');
	     
	     $name = $this->czsecurity->xssCleanPostInput('termname');
	     $neterm = $this->czsecurity->xssCleanPostInput('netterm');
	     $today = date('Y-m-d H:i:s');
	     
	      $term_status = $this->czsecurity->xssCleanPostInput('term_status');
	      
	     $c_cond = array("pt_netTerm" => $neterm,"pt_name" => $name, "merchantID" => $user_id, "enable" => 0);
	     
	     $check = $this->xero_customer_model->check_payment_term($name,$neterm,$user_id);
	     
	     
	     if(!empty($check) && $term_status == 'update' && $termID != "")
	     { 
	         
	         $data = array(
	        "termID" => $termID,
            "pt_name" => $name,
            "pt_netTerm" => $neterm,
            "date" => $today,
            "merchantID" => $user_id,
            "enable"=> 0
            );
            
	    
              $condition = array("id" => $termID);
              $table = $this->general_model->update_row_data('tbl_payment_terms',$condition, $data);
              $this->session->set_flashdata('success','Successfully Updated'); 
            
	     }else  if(empty($check) && $term_status == 'insert' && $termID == "")
	     { 
	       $data = array(
	        "termID" => 0,
            "pt_name" => $name,
            "pt_netTerm" => $neterm,
            "date" => $today,
            "merchantID" => $user_id,
            "enable"=>0
            );
	         
	        $table = $this->general_model->insert_row('tbl_payment_terms', $data);
            $this->session->set_flashdata('success','Successfully Inserted');
        
	     }
	     else{
	        
	        $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:</strong> Payment term already exists.</div>');   
	         
	     }  
	     
        redirect('Xero_controllers/NetTerms/terms','refresh'); 
	}
	
	 public function get_termedit_id(){
    
        $data['login_info'] = $this->session->userdata('logged_in');
	     $user_id    = $data['login_info']['merchID'];
        $termID = $this->czsecurity->xssCleanPostInput('termID');
        $cond = array("merchantID" => $user_id, "termID" => $termID);
        $term = $this->general_model->get_row_data('tbl_payment_terms',$cond);
        echo json_encode($term);
    }
	
	public function delete_term(){
	     $termID = $this->czsecurity->xssCleanPostInput('dtermID');
	     $dfID = $this->czsecurity->xssCleanPostInput('del_dfID');
	     $data['login_info'] = $this->session->userdata('logged_in');
	     $user_id    = $data['login_info']['merchID'];
	     
	     if($dfID == ""){
	      $cond = array("merchantID" => $user_id, "id" => $termID);
	      $term = $this->general_model->delete_row_data('tbl_payment_terms',$cond);
        
           $this->session->set_flashdata('success','Successfully Deleted');
	     }
	     
	      else if($dfID != "" && $termID != ""){
	          
	          $condition = array("id" => $termID);
	       $data = array("enable"=>1);   
	      $cond = array("merchantID" => $user_id, "id" => $termID);
	    $table = $this->general_model->update_row_data('tbl_payment_terms',$condition, $data);
        
           $this->session->set_flashdata('success','Successfully Updated');
	     }
	     
	     else{
	         $data = array("termID" => $dfID, "merchantID" => $user_id, "enable"=>1);
	         $table = $this->general_model->insert_row('tbl_payment_terms', $data);
	          $this->session->set_flashdata('success','Successfully Inserted');
	     }	 
        redirect('Xero_controllers/NetTerms/terms','refresh'); 
	}
	public function set_gateway_default()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('termid'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $id  = $this->czsecurity->xssCleanPostInput('termid');
            $val = array(
                'id' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0');
            $this->general_model->update_row_data('tbl_payment_terms', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'date' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_payment_terms', $val, $update_data);
            $this->session->set_flashdata('success', 'Successfully Updated');
        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('Xero_controllers/NetTerms/terms'));
        
    }
}

