<?php
/**
 * This Controller has Authorize.net Payment Gateway Process
 * 
 * Create_customer_sale for Sale process
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 
 */
 require_once dirname(__FILE__) . '/../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

 

class GlobalPayment extends CI_Controller
{
   
    private $resellerID;
	private $transactionByUser;
	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('globalpayments');
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		
        $this->load->model('customer_model');
        $this->load->model('company_model');
		$this->load->model('general_model');
		$this->load->model('card_model');
		
		$this->load->model('Xero_models/xero_model');
			
		$this->db1 = $this->load->database('otherdb', TRUE);
		$this->load->library('form_validation');
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
		  {
			$logged_in_data = $this->session->userdata('logged_in');
			$this->resellerID = $logged_in_data['resellerID'];
			$this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
			$logged_in_data = $this->session->userdata('user_logged_in');
			$this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
			$merchID = $logged_in_data['merchantID'];
			$rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
			$this->resellerID = $rs_Data['resellerID'];  
		  }else{
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{
	    
	    
	}
	    
	

	
	public function pay_invoice()
	{
	 
	      
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
                 $cusproID=''; $error='';
		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	
              
	    	if ($this->form_validation->run() == true)
		    {
		       
		          $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
			     
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			      $in_data   =    $this->xero_model->get_invoice_data_pay($invoiceID, $user_id);
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
			     
			    if(!empty( $in_data)) 
			    {
			          
    			        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			        $customerID =  $in_data['Customer_ListID'];
    				  	$c_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    			     	$companyID = $c_data['companyID'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
    			       
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
         if(!empty($cardID))
		 {
				
				if( $in_data['BalanceRemaining'] > 0)
				{
				    
            	$crtxnID='';  
                 $config = new PorticoConfig();
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
            
                ServicesContainer::configureService($config);
                $card = new CreditCardData();
                $card->number = $card_no;
                $card->expMonth = $expmonth;
                $card->expYear = $exyear;
                if($cvv!="")
                $card->cvn = $cvv;
                 $card->cardType=$cardType;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zipcode;
                $address->country = $country;
                
                
                $invNo  =mt_rand(1000000,2000000);
                 
             	try
                {
                     $response = $card->charge($amount)
                     
                   
                    ->withCurrency("USD")
                    ->withAddress($address)
                    ->withInvoiceNumber($invNo)
                    ->withAllowDuplicates(true)
                    ->execute();
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                        
						// add level three data
                        $transaction = new Transaction();
                        $transaction->transactionReference = new TransactionReference();
                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                        $level_three_request = [
                            'card_no' => $card_no,
                            'amount' => $amount,
                            'invoice_id' => $invNo,
                            'merchID' => $user_id,
                            'transaction_id' => $response->transactionId,
                            'transaction' => $transaction,
                            'levelCommercialData' => $levelCommercialData,
                            'gateway' => 7
                        ];
                        addlevelThreeDataInTransaction($level_three_request);

                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        
                            $txnID      = $in_data['TxnID'];  
							 $ispaid 	 = 'true';
							
							 $bamount    = $in_data['BalanceRemaining']-$amount;
							  if($bamount > 0)
							  $ispaid 	 = 'false';
							 $app_amount = $in_data['AppliedAmount']-$amount;
							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
							 $condition  = array('TxnID'=>$in_data['TxnID'] );
							 if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  $in_data['RefNumber']; 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }
							 $this->general_model->update_row_data('Xero_test_invoice',$condition, $data);
					
					
                  if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
				 {
				 
				       
				 		$card_no        =  $this->czsecurity->xssCleanPostInput('card_number');
						$cardType       = $this->general_model->getType($card_no);
						$friendlyname   =  $cardType.' - '.substr($card_no,-4);	
				 		
				      
						$card_condition = array(
										 'customerListID' =>$customerID, 
										 'customerCardfriendlyName'=>$friendlyname,
										);
										
							$cid      =    $customerID;			
							$expmonth =    $this->czsecurity->xssCleanPostInput('expiry');
						    $exyear   =    $this->czsecurity->xssCleanPostInput('expiry_year');
							$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
					
							$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;
					 
						if($crdata > 0)
						{
							
						  $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'', 
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'], 
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
									
					
						     $this->card_model->update_card_data($card_condition, $card_data);					 
						}
						else
						{
					      $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										   'CardType'    =>$cardType,
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>'',
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
										 'customerListID' =>$in_data['Customer_ListID'],
										 'customerCardfriendlyName'=>$friendlyname,
										 'companyID'     =>$companyID,
										  'merchantID'   => $user_id,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
								
				          $id1 =    $this->card_model->insert_card_data($card_data);		
						
						}
				
				 }
				    
                    $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                  
				    
				    	  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
				 	
						
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                            $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$in_data['Customer_ListID'],$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                  
                         
                     }		
						
                    
                 } 
                catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
               
				   
				   
				   
				           
		    }
		    else        
            {
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                
            }
                    
                				
		    }
		   
		   } else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			    }	
			    
			     if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
		   	  if($cusproID=="1") {
		   	 redirect('home/invoices','refresh');
		   	 }
        	 redirect('home/invoices','refresh');
			    
		  
	      
	}
	
	
	public function create_customer_sale()
	{ 
	      
	    
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
               
	    	if ($this->form_validation->run() == true)
		    {
	   
        			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			    
        			    if($this->czsecurity->xssCleanPostInput('setMail'))
                    $chh_mail =1;
                    else
                    $chh_mail =0;
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$c_data     =$this->general_model->get_select_data('Xero_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				
        				$companyID  = $c_data['companyID'];
        			 $comp  =$this->general_model->get_select_data('tbl_company',array('qbwc_username'), array('id'=>$companyID, 'merchantID'=>$user_id));
				    $user  = $comp['qbwc_username'];
                    
			   
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     	$address1 =  $this->czsecurity->xssCleanPostInput('baddress1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('bcity');
    	                        $country =$this->czsecurity->xssCleanPostInput('bcountry');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('bphone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('bstate');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('bzipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        					}
        					 
                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                              $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;
                            if($cvv!="")
                            $card->cvn = $cvv;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                              $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $invNo  =mt_rand(1000000,2000000);
                         	try
                            {
                             $response = $card->charge($amount)
                            ->withCurrency('USD')
                            ->withAddress($address)
                            ->withInvoiceNumber($invNo)
                            ->withAllowDuplicates(true)
                            ->execute();
        				
            	          	$crtxnID='';
            				
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {

								// add level three data
		                        $transaction = new Transaction();
		                        $transaction->transactionReference = new TransactionReference();
		                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
		                        $level_three_request = [
		                            'card_no' => $card_no,
		                            'amount' => $amount,
		                            'invoice_id' => $invNo,
		                            'merchID' => $user_id,
		                            'transaction_id' => $response->transactionId,
		                            'transaction' => $transaction,
		                            'levelCommercialData' => $levelCommercialData,
		                            'gateway' => 7
		                        ];
		                        addlevelThreeDataInTransaction($level_three_request);

                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                               
                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID);
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				       
            				 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
				            $refnum=array();
				           if(!empty($invoiceIDs))
				           {
				               
				              foreach($invoiceIDs as $inID)
				              {
        				            $theInvoice = array();
        							 
        							$theInvoice = $this->general_model->get_row_data('Xero_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
        							
        					
        								
        								if(!empty($theInvoice) )
        								{
        									$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
            						    	$tes = $this->general_model->update_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
            						
            						         $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$theInvoice['BalanceRemaining'],$user_id,$crtxnID, $this->resellerID,$inID, false, $this->transactionByUser);  
        					        
        								    $refnum[]= $theInvoice['RefNumber'];
        								}
				              }
				              
				              if($chh_mail =='1')
							 {
							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
							  $ref_number =  implode(',',$refnum); 
							  $tr_date   =date('Y-m-d H:i:s');
							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
							 }
						 
				          }
				          else
				          {
				              
				                    $transactiondata= array();
                    				$inID='';
                    			   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
				          }
				     
				     
            				       
            			         if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		
            				        
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',  
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',  
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				    $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
            			
            				 } 
            				 else
            				 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
                                     
                              }
                         
                         
                         
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			

                   redirect('Payments/create_customer_sale','refresh');
	    
	    
    	}
	
	
	public function create_customer_auth()
	{ 
	      
	    
	      if($this->session->userdata('logged_in')){
				
		
			$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
				
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
             
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
        
          
            }
            
           
               
	    	if ($this->form_validation->run() == true)
		    {
	   
        			     
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			     $apiloginID      = $gt_result['gatewayUsername'];
        				 $secretApiKey   = $gt_result['gatewayPassword'];
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				$comp_data  = $this->general_model->get_select_data('qb_test_customer', array('companyID'), array('ListID' => $customerID));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				     		$address1 =  $this->czsecurity->xssCleanPostInput('baddress1');
    	                	    $address2 = $this->czsecurity->xssCleanPostInput('baddress2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('bcity');
    	                        $country =$this->czsecurity->xssCleanPostInput('bcountry');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('bphone');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('bstate');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('bzipcode');
        				
        				  }
        				  else 
        				  {     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
                					$address1 = $card_data['Billing_Addr1'];
                					$city     =  $card_data['Billing_City'];
                					$zipcode  = $card_data['Billing_Zipcode'];
                					$state    = $card_data['Billing_State'];
                					$country  = $card_data['Billing_Country'];
        						 
        					
        					}
        					 
                             $config = new PorticoConfig();
                           
                              $config->secretApiKey = $secretApiKey;
                              $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;
                            if($cvv!="")
                            $card->cvn = $cvv;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                              $amount = $this->czsecurity->xssCleanPostInput('totalamount');
                            
                            $invNo  =mt_rand(1000000,2000000);
                         	try
                            {
                             $response = $card->authorize($amount)
                            ->withCurrency('USD')
                            ->withAddress($address)
                            ->execute();
        				  
        						
            			
            	          	$crtxnID='';
            				
            			  	 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                             {

								// add level three data
		                        $transaction = new Transaction();
		                        $transaction->transactionReference = new TransactionReference();
		                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
		                        $level_three_request = [
		                            'card_no' => $card_no,
		                            'amount' => $amount,
		                            'invoice_id' => $invNo,
		                            'merchID' => $user_id,
		                            'transaction_id' => $response->transactionId,
		                            'transaction' => $transaction,
		                            'levelCommercialData' => $levelCommercialData,
		                            'gateway' => 7
		                        ];
		                        addlevelThreeDataInTransaction($level_three_request);

                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                                 
                                 $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
                				 /* This block is created for saving Card info in encrypted form  */
            				 
            			        	if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  )
            				     {
            				 		
            				        
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
									
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',  
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$phone,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				    $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
            			
            				 } 
            				 else
            				 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                     
                              }
                         
                         
                         
                            $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
          
                             }
                            catch (BuilderException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ConfigurationException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (GatewayException $e)
                            {
                                $error= 'Transaction Failed - ' . $e->getMessage();
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (UnsupportedTransactionException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                            catch (ApiException $e)
                            {
                                $error='Transaction Failed - ' . $e->getMessage();
                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            }
                				       
        				}
        				else
        				{
        				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
        				}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			

                   redirect('Payments/create_customer_auth','refresh');
	    
	    
    	}
    	
    	
	public function create_customer_void()
	{
	
		
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			   {
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('strtxnvoidID');
				 
				 $con     = array('transactionID'=>$tID,'transactionType'=>'auth');
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gateway = $paydata['gatewayID'];
    			
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				
				  
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             
                
             	try
                {
                 $response = Transaction::fromId($tID)
                     ->void()
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         	$condition = array('transactionID'=>$tID);
					
				         	$update_data =   array('transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s'));
					
				        	$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				        	
				        	  if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                            
                            }
                            
                         $this->session->set_flashdata('success','Successfully Void Authorize Transaction'); 
                         
							 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
			}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
			}		 
			redirect('Payments/payment_capture','refresh');
        }else{
              
			redirect('Payments/payment_transaction','refresh');

		}
	}
	
		
		/*****************Capture Transaction***************/
	
  	public function create_customer_capture()
	{
		//Show a form here which collects someone's name and e-mail address
		
		if(!empty($this->input->post(null, true)))
		{
		    
		      	 if($this->session->userdata('logged_in'))
			    	 {
				
				
				$merchantID 				= $this->session->userdata('logged_in')['merchID'];
				}
				if($this->session->userdata('user_logged_in'))
				{
			
				$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
				}
				
			     $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
			  
    			 $gateway = $paydata['gatewayID'];
				 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
				  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
        		if($tID!='' && !empty($gt_result))
        		{
        			  
        		    $secretApiKey  =  $gt_result['gatewayPassword'];
        		
    		      $config = new PorticoConfig();
               
                  $config->secretApiKey = $secretApiKey;
                  $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
   
                $amount =  $paydata['transactionAmount'];
                ServicesContainer::configureService($config);
                $customerID = $paydata['customerListID'];
            	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
                
                
             	try
                {
                    $response= Transaction::fromId($tID)
                  ->capture($amount)
                   ->execute();
                    
                   
                     if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                     {
                         $msg = $response->responseMessage;
                         $trID = $response->transactionId;
                         
                         $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                         
                         	$condition = array('transactionID'=>$tID);
					
				         	$update_data =   array('transaction_user_status'=>"4");
					
				        	$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
				        	
				        	  if($chh_mail =='1')
                            {
                                $condition = array('transactionID'=>$tID);
                                $customerID = $paydata['customerListID'];
                                
                                $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                $tr_date   =date('Y-m-d H:i:s');
                                $ref_number =  $tID;
                                $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                            
                            }
                         $this->session->set_flashdata('success','Successfully Captured Authorize Transaction'); 
                         
							 
							 
                     }
                     else
                     {
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                           $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                         
                     }
                     
                     
                     
                     $id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$paydata['customerListID'],$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                 } 
                    catch (BuilderException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ConfigurationException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (GatewayException $e)
                    {
                        $error= 'Transaction Failed - ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (UnsupportedTransactionException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
                    catch (ApiException $e)
                    {
                        $error='Transaction Failed - ' . $e->getMessage();
                     $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    }
			   
				       
		}else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Gateway not availabe</strong></div>'); 
				
				 }
					redirect('Payments/payment_capture','refresh');

        }
              
				
		 return false;   

	}
	
  	public function test_index()
	   {
         $config = new PorticoConfig();
       
         $config->secretApiKey = 'skapi_cert_MYl2AQAowiQAbLp5JesGKh7QFkcizOP2jcX9BrEMqQ';
          $config->serviceUrl = 'https://cert.api2.heartlandportico.com';
   
   
        
        ServicesContainer::configureService($config);
        
         
        $card = new CreditCardData();
        $card->number = "4111111111111111";
        $card->expMonth = "12";
        $card->expYear = "2022";
        $card->cvn = "123";
         $card->cardType='VISA';
       
        $address = new Address();
        $address->streetAddress1 = "1231 E Dyer Rd Ste 290";
        $address->city = "Santa Ana";
        $address->state = "CA";
        $address->postalCode = "92705";
        $address->country = "United States";
        $invNo  =mt_rand(1000000,2000000);
     try
        {
            
       
          $response= Transaction::fromId('1176936353')
             ->capture(4)
             ->execute();
            
            
            $response = $card->authorize(15)
            ->withCurrency('USD')
            ->withAddress($address)
           ->execute();
    
           
          
             $response = $card->charge(15)
            ->withCurrency('USD')
            ->withAddress($address)
            ->withInvoiceNumber($invNo)
            ->withAllowDuplicates(true)
            ->execute();  
            echo "<pre>";
              print_r($response);
            $body = '<h1>Success!</h1>';
            $body .= '<p>Thank you, Test User for your order of $15.00</p>';
            echo "APPROVAL". $response->responseMessage;
            echo "Transaction Id: " . $response->transactionId;
            echo "<br />Invoice Number:".$invNo ;
        
            // i'm running windows, so i had to update this:
          
            } 
            catch (BuilderException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (ConfigurationException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (GatewayException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (UnsupportedTransactionException $e)
            {
               echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
            catch (ApiException $e)
            {
                echo 'Transaction Failed - ' . $e->getMessage();
                exit;
            }
    
	    
	}
	

     
     
     
	public function pay_multi_invoice()
	{
	 if($this->session->userdata('logged_in')){
			
		
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
			$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
	     $cardID_upd ='';
	 	 $invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
		 
       $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
	
      	$inv_data=array();
		 $cusproID=''; $error='';
        $invoiceIDs =  implode(',',$invoices); 
		$inv_data   =    $this->customer_model->get_due_invoice_data($invoices,$user_id);
			
			
			
			
			
	  if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
     
       if(!empty($invoices) &&  !empty( $inv_data) && $cardID!="" && $gateway!="")
       {  
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
              $cusproID     = $customerID  = $inv_data['Customer_ListID'];
			   	$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$user_id));
		        $companyID  = $comp_data['companyID'];
		
              $secretApiKey   = $gt_result['gatewayPassword'];
        
             
            		  
                            $Customer_ListID = $customerID;
                     
                            if($cardID!="new1")
            			     {
            			        
            			        $card_data=   $this->card_model->get_single_card_data($cardID);
            			        $card_no  = $card_data['CardNo'];
        				        $expmonth =  $card_data['cardMonth'];
            					$exyear   = $card_data['cardYear'];
            					$cvv      = $card_data['CardCVV'];
            					$cardType = $card_data['CardType'];
            					$address1 = $card_data['Billing_Addr1'];
            						$address2 = $card_data['Billing_Addr2'];
            					$city     =  $card_data['Billing_City'];
            					$zipcode  = $card_data['Billing_Zipcode'];
            					$state    = $card_data['Billing_State'];
            					$country  = $card_data['Billing_Country'];
            					
            			     }
            			     else
            			     {
            			          
            			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
            					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
            					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
            					$cardType = $this->general_model->getType($card_no);
            					$cvv ='';
            					if($this->czsecurity->xssCleanPostInput('cvv')!="")
            					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
            					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
            	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
            	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
            	                        $country =$this->czsecurity->xssCleanPostInput('country');
            	                        $phone   =  $this->czsecurity->xssCleanPostInput('phone');
            	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
            	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
            			         
            			     }   
               	  
            				if(!empty($invoices))
            				{
            				    
                             $amount  = $this->czsecurity->xssCleanPostInput('totalPay');
                        				           
                             $config = new PorticoConfig();
                              $config->secretApiKey = $secretApiKey;
                              $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                        
                            ServicesContainer::configureService($config);
                            $card = new CreditCardData();
                            $card->number = $card_no;
                            $card->expMonth = $expmonth;
                            $card->expYear = $exyear;
                            if($cvv!="")
                            $card->cvn = $cvv;
                             $card->cardType=$cardType;
                       
                            $address = new Address();
                            $address->streetAddress1 = $address1;
                            $address->city = $city;
                            $address->state = $state;
                            $address->postalCode = $zipcode;
                            $address->country = $country;
                            
                            
                            $invNo  =mt_rand(1000000,2000000);
                             
                         	try
                            {
                                 $response = $card->charge($amount)
                                 
                               
                                ->withCurrency("USD")
                                ->withAddress($address)
                                ->withInvoiceNumber($invNo)
                                ->withAllowDuplicates(true)
                                ->execute();
                                 if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                                 {
                                    
									// add level three data
			                        $transaction = new Transaction();
			                        $transaction->transactionReference = new TransactionReference();
			                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
			                        $level_three_request = [
			                            'card_no' => $card_no,
			                            'amount' => $amount,
			                            'invoice_id' => $invNo,
			                            'merchID' => $user_id,
			                            'transaction_id' => $response->transactionId,
			                            'transaction' => $transaction,
			                            'levelCommercialData' => $levelCommercialData,
			                            'gateway' => 7
			                        ];
			                        addlevelThreeDataInTransaction($level_three_request);

                                     $msg = $response->responseMessage;
                                     $trID = $response->transactionId;
                                     
                                     $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                     
                                     
                                     
                                     
                                       foreach($invoices as $invoiceID)
                                      {
                                         
                                          $pay_amounts=$this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                           $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
                                    		if(!empty($in_data))
                                    		{
                                    
                                                $txnID      = $in_data['TxnID'];  
                    							 $ispaid 	 = 'true';
                    							 $am         = (-$pay_amounts);
                                						 
                        				         $bamount =  $in_data['BalanceRemaining']-$pay_amounts;
            					              if($bamount >0)
            						 	      $ispaid 	 = 'false';
            						 	          
            						 	        $app_amount = $in_data['AppliedAmount']+(-$pay_amounts);
            						              $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
            					            	
                        						 $condition  = array('TxnID'=>$in_data['TxnID'] );
                                          
                        						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
                        				     	 $user = $in_data['qbwc_username'];
                        				     	 
                        				     	  $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$crtxnID='', $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                        				     	  $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
                        				     	  
                        				     	  
                        				     	  if($chh_mail =='1')
                    							 {
                    							  $condition_mail= array('templateType'=>'5', 'merchantID'=>$user_id); 
                    							  $ref_number =  $in_data['RefNumber']; 
                    							  $tr_date   =date('Y-m-d H:i:s');
                    							  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                    							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date);
                    							 }
                        				     	  
                        				     	  
                        				     	  
                                    		}
                                      }		
                              
                                    
                             if($this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=="new1"  &&  !($this->czsecurity->xssCleanPostInput('tc')) )
            				 {
            				 		
									$cardType       = $this->general_model->getType($card_no);
									$friendlyname   =  $cardType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$this->general_model->getType($card_no),
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'',  
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				 
            				   
							 
            				    	  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
            				 	
            						
                                 }
                                 else
                                 {
                                      $msg = $response->responseMessage;
                                      $trID = $response->transactionId;
                                       $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                     $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);      
                                 }		
            						
                               
                              
            				     
            				    
                             } 
                            catch (BuilderException $e)
                                {
                                    $error= 'Transaction Failed - ' . $e->getMessage();
                                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                }
                                catch (ConfigurationException $e)
                                {
                                    $error='Transaction Failed - ' . $e->getMessage();
                                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                }
                                catch (GatewayException $e)
                                {
                                    $error= 'Transaction Failed - ' . $e->getMessage();
                                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                }
                                catch (UnsupportedTransactionException $e)
                                {
                                    $error='Transaction Failed - ' . $e->getMessage();
                                   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                }
                                catch (ApiException $e)
                                {
                                    $error='Transaction Failed - ' . $e->getMessage();
                                 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                }
                           
            				   
            			
            				           
            		    }
                		    else        
                            {
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                                
                            }
                     
		   }
		   else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			   
		    	 if($customerID!="")
				 {
					 redirect('home/view_customer/'.$customerID,'refresh');
				 }
				 else{
				 redirect('home/invoices','refresh');
				 }
	      
	}
	
	
}

