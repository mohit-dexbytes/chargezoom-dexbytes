<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Xero_models/xero_company_model');
        $this->load->model('Xero_models/xero_customer_model');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);

        if ($this->session->userdata('logged_in') != "" && $this->session->userdata('logged_in')['active_app'] == '4') {

        } else if ($this->session->userdata('user_logged_in') != "") {

        } else {
            redirect('login', 'refresh');
        }
    }

    public function index()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        if (isset($user_id)) {
            $where = ['merchID' => $user_id, 'merchantPasswordNew' => null];

            $newPasswordNotFound = $this->general_model->get_row_data('tbl_merchant_data', $where);

            if ($newPasswordNotFound) {
                $dateDiff = time() - strtotime(getenv('PASSWORD_EXP_DATE'));
                $data['passwordExpDays'] = abs(round($dateDiff / (60 * 60 * 24)));
            }
        }

        $today      = date('Y-m-d');
        $condition2 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= " => $today, "inv.merchantID" => $user_id);
        $condition  = array("inv.merchantID" => $user_id, 'cust.merchantID' => $user_id, 'trans.merchantID' => $user_id);

        /* New updated dashboard date 27-10-2020 */
        $month = date("M-Y");
        $data['recent_pay']      = $this->xero_company_model->get_recent_volume_dashboard($user_id,$month);
        $data['card_payment'] = $this->xero_company_model->get_creditcard_payment($user_id,$month);
      
        $data['eCheck_payment'] = $this->xero_company_model->get_eCheck_payment($user_id,$month);

        $data['outstanding_total'] = $this->xero_company_model->get_outstanding_payment($user_id);
        $data['recent_paid']    = $this->xero_company_model->get_paid_recent($user_id);

        $plantype = $this->general_model->chk_merch_plantype_status($user_id);
		$data['plantype'] = $plantype;
        $data['plantype_as'] = $this->general_model->chk_merch_plantype_as($user_id);
        $data['plantype_vs'] = $this->general_model->chk_merch_plantype_vt($user_id);
        $data['plantype_vt'] = 0;
        $data['today_invoice'] = $this->xero_company_model->get_process_trans_count($user_id);
        $data['upcoming_inv']  = $this->xero_company_model->get_invoice_data_count($condition2);
        $data['failed_inv']    = $this->xero_company_model->get_invoice_data_count_failed($user_id);
       
        $data['oldest_invs']   = $this->xero_company_model->get_oldest_due($user_id);


        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/index', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    //------------- Merchant gateway START ------------//

    public function gateway()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $condition = array('merchantID' => $user_id);

        $data['all_gateway'] = $this->general_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/page_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    //---------------- To add gateway  ---------------//

    public function create_gateway()
    {
        $signature  = '';
        $cr_status  = 1;
        $ach_status = 0;
        if (!empty($this->input->post(null, true))) {

            $data['login_info'] = $this->session->userdata('logged_in');
            $user_id            = $data['login_info']['merchID'];
            $condition          = array('merchantID' => $user_id);
            $gatetype = $this->czsecurity->xssCleanPostInput('gateway_opt');

            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                if ($this->czsecurity->xssCleanPostInput('mni_cr_status')) {
                    $cr_status = 1;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;

                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '3') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');

            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');

            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');

            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');

            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            }

            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');

            $frname   = $this->czsecurity->xssCleanPostInput('frname');
            $gmID     = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewaySignature'                     => $signature,
                'gatewayType'                          => $gatetype,
                'merchantID'                           => $user_id,
                'gatewayFriendlyName'                  => $frname,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
            );

            if ($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)) {
                
                $val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Successfully Inserted</strong></div>');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('Integration/home/gateway'));
        }

    }

    //----------- TO update the gateway  --------------//

    public function update_gateway()
    {
        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {
            $ach_status = 0;
            $signature  = '';
            $cr_status  = 1;
            
            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype = $this->czsecurity->xssCleanPostInput('gateway');

            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1')) {
                    $cr_status = 1;
                }

                if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if (strtolower($gatetype) == 'authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;

                }

                if ($this->czsecurity->xssCleanPostInput('auth_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
			} else if ($gatetype == 'Paytrace') {
				$this->load->config('paytrace');
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');

            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');

            } else if ($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
                if ($this->czsecurity->xssCleanPostInput('heart_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('heart_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');

            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1')) {
                    $cr_status = 1;
                }else{
                    $cr_status = 0;
                }

                if ($this->czsecurity->xssCleanPostInput('cz_ach_status1')) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } 

            $frname = $this->czsecurity->xssCleanPostInput('fname');
            $gmID   = $this->czsecurity->xssCleanPostInput('mid');

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayMerchantID'                    => $gmID,
                'gatewayFriendlyName'                  => $frname,
                'gatewaySignature'                     => $signature,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
            );

            if ($this->general_model->update_row_data('tbl_merchant_gateway', $chk_condition, $insert_data)) {
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Successfully Updated</strong></div>');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
            }

            redirect(base_url('Integration/home/gateway'));
        }

    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
        if ($data['gatewayType'] == '1') {
            $data['gateway'] = "NMI";
        }
        if ($data['gatewayType'] == '2') {
            $data['gateway'] = "Authorize.net";
        }
        if ($data['gatewayType'] == '3') {
            $data['gateway'] = "Paytrace";
        }
        if ($data['gatewayType'] == '4') {
            $data['gateway'] = "Paypal";
        }
        if ($data['gatewayType'] == '5') {
            $data['gateway'] = "Stripe";
        }
        if ($data['gatewayType'] == '6') {
            $data['gateway'] = "USAePay";
        }
		if ($data['gatewayType'] == '9') {
			$data['gateway'] = "Chargezoom";
		}
        echo json_encode($data);die;
    }

    public function set_gateway_default()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {

            if ($this->session->userdata('logged_in')) {
                $da['login_info'] = $this->session->userdata('logged_in');

                $merchID = $da['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $da['login_info'] = $this->session->userdata('user_logged_in');

                $merchID = $da['login_info']['merchantID'];
            }

            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

        } else {

            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');
        }
        redirect(base_url('Integration/home/gateway'));
    }

    /**************Delete credit********************/

    public function delete_gateway()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
        $condition = array('gatewayID' => $gatewayID);
        $del       = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition);
        if ($del) {
            $this->session->set_flashdata('success', 'Successfully Deleted');
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
        }

        redirect(base_url('Integration/home/gateway'));

    }

    public function general_volume()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $get_result = $this->xero_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
            $result_set    = array();
            $result_value1 = array();
            $result_value2 = array();

            $result_online_value = array();
            $result_online_month = array();


            $result_eCheck_value = array();
            $result_eCheck_month = array();

            foreach ($get_result['data'] as $count_merch) {

                

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);

                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);


                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);

            }
            $opt_array['revenu_month'] =   $result_value1;
            $opt_array['revenu_volume'] =   $result_value2;
            
            $opt_array['online_month'] =   $result_online_month;
            $opt_array['online_volume'] =   $result_online_value;
            
            
            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];


            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $result_eCheck_value;
            echo json_encode($opt_array);

        }

    }

    public function get_invoice_due_company()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $condition  = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $get_result = array();
        $get_result = $this->xero_company_model->get_invoice_due_by_company($condition, 1);

        echo json_encode($get_result);
        die;

    }
    public function get_invoice_Past_due_company()
    {

        $data['login_info'] = $this->session->userdata('logged_in');
        $user_id            = $data['login_info']['merchID'];

        $condition  = array("inv.merchantID" => $user_id, "cust.merchantID" => $user_id);
        $get_result = array();
        $get_result = $this->xero_company_model->get_invoice_due_by_company($condition, 0);

        echo json_encode($get_result);
        die;

    }

    public function company()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');
            $merchantID         = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $merchantID         = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $condition         = array('merchantID' => $merchantID);
        $data['companies'] = $this->general_model->get_table_data('tbl_xero_token', $condition);

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);

        $this->load->view('Xero_views/page_company', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function xero_log()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['alls'] = $this->general_model->get_table_data('tbl_xero_log', array('merchantID' => $user_id));

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/page_log', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_gateway_data()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');
        $condition = array('gatewayID' => $gatewayID);

        $res = $this->general_model->get_row_data('tbl_merchant_gateway', $condition);

        if (!empty($res)) {

            $res['status'] = 'true';
            echo json_encode($res);
        }

        die;

    }

    public function get_subs_item_count_data()
    {
        $sbID           = $this->czsecurity->xssCleanPostInput('subID');
        $data1['items'] = $this->general_model->get_table_data('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));
        $data1['rows']  = $this->general_model->get_num_rows('tbl_subscription_invoice_item_xero', array('subscriptionID' => $sbID));
        echo json_encode($data1);
        die;
    }

    public function get_product_data()
    {
        if ($this->session->userdata('logged_in')) {
            $merchantID = $this->session->userdata('logged_in')['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $merchantID = $this->session->userdata('user_logged_in')['merchantID'];
        }

        $data      = $this->xero_customer_model->get_xero_item_data($merchantID);
        $data      = json_encode($data);
        echo $data = str_replace("'", "", $data);

        die;

    }

    public function get_item_test_data()
    {

        if ($this->session->userdata('logged_in') != "") {
            $user_id = $this->session->userdata('logged_in')['merchID'];

        } else if ($this->session->userdata('user_logged_in') != "") {
            $user_id = $this->session->userdata('logged_in')['merchantID'];

        }

        if ($this->czsecurity->xssCleanPostInput('itemID') != "") {

            $itemID   = $this->czsecurity->xssCleanPostInput('itemID');
            $itemdata = $this->general_model->get_row_data('Xero_test_item', array('productID' => $itemID, 'merchantID' => $user_id));

            $data      = json_encode($itemdata);
            echo $data = str_replace("'", "", $data);

            die;
        }
        return false;
    }

  
    public function my_account()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');

            $user_id                = $data['login_info']['merchID'];
            $data['loginType'] = 1;
        }else if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];

            $data['loginType'] = 2;
        }   

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        
        $condition  = array('merchantID' => $user_id);
        $data['invoices'] = $this->general_model->get_table_data('tbl_merchant_billing_invoice', $condition);

        $plandata = $this->general_model->chk_merch_plantype_data($user_id);
        $resellerID = $data['login_info']['resellerID'];
        $planID = $plandata->plan_id;
        $planname = $this->general_model->chk_merch_planFriendlyName($resellerID,$planID);

        if(isset($planname) && !empty($planname)){
            $data['planname'] = $planname;
        }else{
            $data['planname'] = $plandata->plan_name;
        }


        if($plandata->cardID > 0 && $plandata->payOption > 0){
            $carddata = $this->card_model->get_merch_card_data($plandata->cardID);
        }else{
            $carddata = [];
        }
        
        $data['plan'] = $plandata;

        $data['carddata'] = $carddata;

        $conditionMerch = array('merchID'=>$user_id);
        $data['merchantData'] = $this->general_model->get_row_data('tbl_merchant_data', $conditionMerch);

        $data['interface'] = 4;
        $data['merchantID'] = $user_id;
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/page_my_account', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function update_card_data()
    {
        
        $success_msg = null;
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }   
            
            
            $resellerID  = $this->czsecurity->xssCleanPostInput('resellerID');
                
            $merchantcardID = $this->czsecurity->xssCleanPostInput('cardID'); 

            $merchantID = $user_id;

            $condition = array('merchantListID'=>$merchantID);

            $conditionMerch = array('merchID'=>$merchantID);
            
            $billing_first_name = ($this->czsecurity->xssCleanPostInput('billing_first_name') != null)?$this->czsecurity->xssCleanPostInput('billing_first_name'):null;

            $billing_last_name = ($this->czsecurity->xssCleanPostInput('billing_last_name')!= null)?$this->czsecurity->xssCleanPostInput('billing_last_name'):null;

            $billing_phone_number = ($this->czsecurity->xssCleanPostInput('billing_phone_number')!= null)?$this->czsecurity->xssCleanPostInput('billing_phone_number'):null;

            $billing_email = ($this->czsecurity->xssCleanPostInput('billing_email')!= null)?$this->czsecurity->xssCleanPostInput('billing_email'):null;

            $billing_address = ($this->czsecurity->xssCleanPostInput('billing_address')!= null)?$this->czsecurity->xssCleanPostInput('billing_address'):null;

            $billing_state = ($this->czsecurity->xssCleanPostInput('billing_state')!= null)?$this->czsecurity->xssCleanPostInput('billing_state'):null;

            $billing_city = ($this->czsecurity->xssCleanPostInput('billing_city')!= null)?$this->czsecurity->xssCleanPostInput('billing_city'):null;

            $billing_zipcode = ($this->czsecurity->xssCleanPostInput('billing_zipcode')!= null)?$this->czsecurity->xssCleanPostInput('billing_zipcode'):null;
            $statusInsert = 0;
            /* check is_address_update condition 1 than only address update and 2 for all */
            if($this->czsecurity->xssCleanPostInput('is_address_update') == 1){
                $insert_array =  array( 
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                if($merchantcardID!="")
                {
                    
                    $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                    
                    
                    $success_msg = 'Address Updated Successfully';  

                }
            }else{
                /* Save credit card data */
                if($this->czsecurity->xssCleanPostInput('payOption') == 1){

                    $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
                    $card_type = $this->general_model->getcardType($card_no);
                    $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
                    $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
                    $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                    $type = 'Credit';
                    $friendlyname = $card_type . ' - ' . substr($card_no, -4);
                    $insert_array =  array( 'CardMonth'  =>$expmonth,
                                    'CardYear'   =>$exyear, 
                                    'resellerID'  =>$resellerID,
                                    'merchantListID'=>$merchantID,
                                    'accountNumber'   => null,
                                    'routeNumber'     => null,
                                    'accountName'   => null,
                                    'accountType'   => null,
                                    'accountHolderType'   => null,
                                    'secCodeEntryMethod'   => null,
                                    'merchantFriendlyName' => $friendlyname,
                                    "billing_first_name" => $billing_first_name,
                                    "billing_last_name" => $billing_last_name,
                                    "billing_phone_number" => $billing_phone_number,
                                    "billing_email" => $billing_email,
                                    "Billing_Addr1" => $billing_address,
                                    "Billing_Country" =>null,
                                    "Billing_State" => $billing_state,
                                    "Billing_City" =>$billing_city,
                                    "Billing_Zipcode" => $billing_zipcode
                                             );
                    if($merchantcardID!="")
                    {
                        
                           
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = ''; 
                        $insert_array['CardType']        = $card_type;
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                        $id = $this->card_model->update_merchant_card_data($condition, $insert_array);  
                        
                        
                        $success_msg = 'Credit Card Updated Successfully';  

                    }else{
                        
                        $insert_array['CardType']    = $card_type;
                        $insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
                        $insert_array['CardCVV']        = ''; 
                        $insert_array['createdAt']    = date('Y-m-d H:i:s');
                              
                            
                        $id = $this->card_model->insert_merchant_card_data($insert_array);
                        $statusInsert = 1;
                        $merchantcardID = $id;
                        $success_msg = 'Credit Card Inserted Successfully';  
                        
                    }

                }else if($this->czsecurity->xssCleanPostInput('payOption') == 2){
                    /* Save checking card data */
                    $acc_number   = $this->czsecurity->xssCleanPostInput('acc_number');
                    $route_number = $this->czsecurity->xssCleanPostInput('route_number');
                    $acc_name     = $this->czsecurity->xssCleanPostInput('acc_name');
                    $secCode      = $this->czsecurity->xssCleanPostInput('secCode');
                    $acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
                    $acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
                    $card_type = 'Checking';
                    $type = 'Checking';
                    $friendlyname = $type . ' - ' . substr($acc_number, -4);
                    $card_data = array(
                        'CardType'     => $card_type,
                        'CardMonth'  => null,
                        'CardYear'   => null, 
                        'MerchantCard' => null, 
                        'CardCVV' => null,
                        'accountNumber'   => $acc_number,
                        'routeNumber'     => $route_number,
                        'accountName'   => $acc_name,
                        'accountType'   => $acct_type,
                        'accountHolderType'   => $acct_holder_type,
                        'secCodeEntryMethod'   => $secCode,
                        'merchantListID'=>$merchantID,
                        'resellerID'  =>$resellerID,
                        'merchantFriendlyName' => $friendlyname,
                        'createdAt'     => date("Y-m-d H:i:s"),
                        "billing_first_name" => $billing_first_name,
                        "billing_last_name" => $billing_last_name,
                        "billing_phone_number" => $billing_phone_number,
                        "billing_email" => $billing_email,
                        "Billing_Addr1" => $billing_address,
                        "Billing_Country" =>null,
                        "Billing_State" => $billing_state,
                        "Billing_City" =>$billing_city,
                        "Billing_Zipcode" => $billing_zipcode
                                 
                    );
                    if($merchantcardID!="")
                    {
                        
                        $id = $this->card_model->update_merchant_card_data($condition, $card_data);  
                        
                        

                        $success_msg = 'Checking Card Updated Successfully';  
                    }else{
                        
                        $id = $this->card_model->insert_merchant_card_data($card_data);
                        $merchantcardID = $id;
                        $statusInsert = 1;
                        $success_msg = 'Checking Card Inserted Successfully';  
                    }


                }else{
                    /* do nothing*/
                    $id = false;
                }
            }
            if($statusInsert == 1){
                $merchant_condition = ['merchID' => $merchantID];
                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$merchant_condition);
                if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
                {
                    /* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    
                    $merchantData['merchant_type'] = 'Xero';        
                    
                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                    if($status['statusCode'] == 400){
                        $resource = $this->hatchbuckapi->createContact($merchantData);
                        if($resource['contactID'] != '0'){
                            $contact_id = $resource['contactID'];
                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                        }
                    }
                    /* End campaign in hatchbuck CRM*/ 
                }
            }   

            if( $id ){
                /* Update Pay option type in merchant table */
                $update_array =  array( 'payOption'  => $this->czsecurity->xssCleanPostInput('payOption'), 'cardID' => $merchantcardID );

                $update = $this->general_model->update_row_data('tbl_merchant_data',$conditionMerch, $update_array); 

                $this->session->set_flashdata('success', $success_msg);
            }else{
             
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
            }

            redirect('Integration/home/my_account/');
    }
    public function marchant_invoice()
    {


        if ($this->uri->segment(4) != "") {
            $data['primary_nav']    = primary_nav();
            $data['template']       = template_variable();
            if ($this->session->userdata('logged_in')) {
                $data['login_info']     = $this->session->userdata('logged_in');

                $user_id                = $data['login_info']['merchID'];
            }
            if ($this->session->userdata('user_logged_in')) {
                $data['login_info']     = $this->session->userdata('user_logged_in');

                $user_id                = $data['login_info']['merchantID'];
            }
            $invoiceID             =  $this->uri->segment(4);
            
            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);


            $this->load->view('Xero_views/marchant_invoice', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);
        } else {
            redirect(base_url('Integration/home/invoices'));
        }
    }

    public function level_three()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }

        $data['level_three_master_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master']);
        $data['level_three_visa_data']  = $this->general_model->get_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa']);
        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('Xero_views/level_three', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    // add or update visa details
    public function level_three_visa()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');
            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['customer_tax_id'] = $this->czsecurity->xssCleanPostInput('customer_tax_id');
            $insert_data['commodity_code'] = $this->czsecurity->xssCleanPostInput('commodity_code');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['addtnl_tax_freight'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_freight');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['line_item_commodity_code'] = $this->czsecurity->xssCleanPostInput('line_item_commodity_code');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['card_type'] = 'visa';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'visa']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'visa'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('Integration/home/level_three');
    }

    // add or update master details
    public function level_three_master_card()
    {
        $data['login_info'] = $this->session->userdata('logged_in');
        if ($data['login_info']) {
            $user_id = $data['login_info']['merchID'];
        }else{
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id                = $data['login_info']['merchantID'];
        }
        if($this->input->post(null, true)){
            $insert_data = [];
            $insert_data['customer_reference_id'] = $this->czsecurity->xssCleanPostInput('customer_reference_id');

            $insert_data['local_tax'] = $this->czsecurity->xssCleanPostInput('local_tax');
            $insert_data['national_tax'] = $this->czsecurity->xssCleanPostInput('national_tax');
            $insert_data['freight_amount'] = $this->czsecurity->xssCleanPostInput('freight_amount');
            $insert_data['destination_country'] = $this->czsecurity->xssCleanPostInput('destination_country');
            $insert_data['duty_amount'] = $this->czsecurity->xssCleanPostInput('duty_amount');
            $insert_data['addtnl_tax_amount'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_amount');
            $insert_data['destination_zip'] = $this->czsecurity->xssCleanPostInput('destination_zip');
            $insert_data['addtnl_tax_indicator'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_indicator');
            $insert_data['source_zip'] = $this->czsecurity->xssCleanPostInput('source_zip');
            $insert_data['description'] = $this->czsecurity->xssCleanPostInput('description');
            $insert_data['line_item_addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('line_item_addtnl_tax_rate');
            $insert_data['debit_credit_indicator'] = $this->czsecurity->xssCleanPostInput('debit_credit_indicator');
            $insert_data['product_code'] = $this->czsecurity->xssCleanPostInput('product_code');
            $insert_data['addtnl_tax_type'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_type');
            $insert_data['discount'] = $this->czsecurity->xssCleanPostInput('discount');
            $insert_data['unit_measure_code'] = $this->czsecurity->xssCleanPostInput('unit_measure_code');
            $insert_data['addtnl_tax_rate'] = $this->czsecurity->xssCleanPostInput('addtnl_tax_rate');
            $insert_data['discount_rate'] = $this->czsecurity->xssCleanPostInput('discount_rate');
            $insert_data['merchant_tax_id'] = $this->czsecurity->xssCleanPostInput('merchant_tax_id');
            $insert_data['net_gross_indicator'] = $this->czsecurity->xssCleanPostInput('net_gross_indicator');
            
            $insert_data['card_type'] = 'master';
            $insert_data['updated_date'] = date('Y-m-d H:i:s');
            $insert_data['merchant_id'] = $user_id;

            $check_exist = $this->general_model->get_select_data('merchant_level_three_data', ['merchant_id'], ['merchant_id' => $user_id, 'card_type' => 'master']);
            if($check_exist){
                $this->general_model->update_row_data('merchant_level_three_data', ['merchant_id' => $user_id, 'card_type' => 'master'], $insert_data);
                
            }else{
                $insert_data['created_date'] = date('Y-m-d H:i:s');
                $this->general_model->insert_row('merchant_level_three_data', $insert_data);
            }
            $this->session->set_flashdata('success', 'Level III Data Updated Successfully');
        }
        redirect('Integration/home/level_three');
    }
    public function dashboardReport()
    {
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        } else if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');
            $user_id            = $data['login_info']['merchantID'];
        }

        $get_result = $this->xero_company_model->chart_all_volume_new($user_id);
        if ($get_result['data']) {
         
            $result_set = array();
            $result_value1 = array();
            $result_value2 = array();
            $result_online_value = array();
            $result_online_month = array();
            $result_eCheck_value = array();
            $result_eCheck_month = array();
            foreach ($get_result['data'] as $count_merch) {

                array_push($result_value1, $count_merch['revenu_Month']);
                array_push($result_value2, (float) $count_merch['revenu_volume']);
                array_push($result_online_month, $count_merch['revenu_Month']);
                array_push($result_online_value, (float) $count_merch['online_volume']);
                array_push($result_eCheck_month, $count_merch['revenu_Month']);
                array_push($result_eCheck_value, (float) $count_merch['eCheck_volume']);
            }
 
            $ob[] = [];
            $obRevenu = [];
            $obOnline = [];
            $obeCheck = [];
            $in = 0;

            foreach ($result_value1 as $value) {
                $custmonths = date("M", strtotime($value));
                $ob1 = [];
                $obR = [];
                $obON = [];
                $obEC = [];
                $inc = strtotime($value);
                $ob1[0] = $inc;
                $obR[] = $inc;
                $obR[] = $custmonths;
                $ob1[1] = $result_value2[$in];

                $ob[] = $ob1;

                $obON[0] = $inc;
                $obON[1] = $result_online_value[$in];
                $obOnline[] = $obON;


                $obEC[0] = $inc;
                $obEC[1] = $result_eCheck_value[$in];
                $obeCheck[] = $obEC;

                $obRevenu[] = $obR; 
                $in++;
            }



            $opt_array['revenu_month'] =   $obRevenu;
            $opt_array['revenu_volume'] =   $ob;
            
            $opt_array['online_month'] =   $result_online_month;
            $opt_array['online_volume'] =   $obOnline;
            
            $opt_array['eCheck_month'] =   $result_eCheck_month;
            $opt_array['eCheck_volume'] =   $obeCheck;

            $opt_array['totalRevenue'] =   $get_result['totalRevenue'];
            $opt_array['totalCCA'] =   $get_result['totalCCA'];
            $opt_array['totalECLA'] =   $get_result['totalECLA'];
            echo json_encode($opt_array);
        }
    }
    public function batchReciept()
    {
        

        if ($this->session->userdata('logged_in')) {
            $data['login_info']     = $this->session->userdata('logged_in');
            $user_id                = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info']     = $this->session->userdata('user_logged_in');

            $user_id                = $data['login_info']['merchantID'];
        }

        $data['primary_nav']    = primary_nav();
        $data['template']       = template_variable();

        $page_data = $this->session->userdata('batch_process_receipt_data'); 
        
        $invoices = $page_data['data'];
        $invoiceObj = [];
        if(count($invoices) > 0){
            foreach ($invoices as $value) {
                
                $invoiceObj[] = $this->general_model->getInvoiceBatchTransactionList($value['invoice_id'],$value['customerID'],$value['transactionRowID'],4);

                # code...
            }
        }
        
        $data['allTransaction'] = $invoiceObj;
        $data['statusCode'] = array('200','100','111','1','102','120');
        $data['integrationType'] = 4;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('comman-pages/batch_invoice_reciept', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
}
