<?php
class Customer extends CI_Controller
{
    protected $loginDetails;
	public function __construct()
	{
		parent::__construct();
		include APPPATH . 'third_party/Xero.php';
		$this->load->model('general_model');
		$this->load->model('Xero_models/xero_customer_model');
		$this->load->model('Xero_models/xero_model');
		$this->load->library('session');
		$this->db1 = $this->load->database('otherdb', TRUE);

		$this->db1 = $this->load->database('otherdb', TRUE);

		if ($this->session->userdata('logged_in') != "" &&  $this->session->userdata('logged_in')['active_app'] == '4') {
		} else if ($this->session->userdata('user_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
		$this->loginDetails = get_names();
	}


	public function index()
	{
		redirect('Integration/home', 'refresh');
	}


	/********************** Function for: Create Customer ********************/

	public function create_customer()
	{

	
		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {


			
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);



			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			if (!empty($this->input->post(null, true))) {

				$this->load->library('form_validation');
				$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		
				$this->form_validation->set_rules('fullName', 'fullName', 'required|xss_clean');
			

				if ($this->form_validation->run() == true) {
					

					$insert['firstName'] =  $firstName      = $this->czsecurity->xssCleanPostInput('firstName');
					$insert['lastName'] =  $lastName       = $this->czsecurity->xssCleanPostInput('lastName');
					$insert['fullName'] = $fullName       = $this->czsecurity->xssCleanPostInput('fullName');
					$insert['userEmail'] = $userEmail      = $this->czsecurity->xssCleanPostInput('userEmail');
					$insert['Country'] =  $country        = $this->czsecurity->xssCleanPostInput('companyCountry');
					$insert['State'] =   $state          = $this->czsecurity->xssCleanPostInput('companyState');
					$insert['City'] =   $city           = $this->czsecurity->xssCleanPostInput('companyCity');
					$insert['phoneNumber'] =   $phoneNumber    = $this->czsecurity->xssCleanPostInput('phone');
					$insert['address1'] =    $address1       = $this->czsecurity->xssCleanPostInput('address1');
					$insert['address2'] = $address2       = $this->czsecurity->xssCleanPostInput('address2');
					$insert['zipcode'] = $zipCode        = $this->czsecurity->xssCleanPostInput('zipcode');
					$insert['ship_address1'] =  $ship_address1  = $this->czsecurity->xssCleanPostInput('saddress1');
					$insert['ship_address2'] =  $ship_address2   = $this->czsecurity->xssCleanPostInput('saddress2');
					$insert['ship_city'] =  $ship_city    = $this->czsecurity->xssCleanPostInput('scity');
					$insert['ship_country'] =  $ship_country  = $this->czsecurity->xssCleanPostInput('scountry');
					$insert['ship_state'] =  $ship_state    = $this->czsecurity->xssCleanPostInput('sstate');
					$insert['ship_zipcode'] =  $ship_zipcode = $this->czsecurity->xssCleanPostInput('szipcode');
					$insert['merchantID'] =  $merchantID     = $user_id;
					$insert['createdat'] =   $createdat      = date('Y-m-d H:i:s');


					if ($this->czsecurity->xssCleanPostInput('customerListID') != "") {



						$xml = "<Contact>
                      <ContactID>" . $this->czsecurity->xssCleanPostInput('customerListID') . "</ContactID>
                       <Name>" . $fullName . "</Name>
                       <FirstName>" . $firstName . "</FirstName>
                       <LastName>" . $lastName . "</LastName>
                       <EmailAddress>" . $userEmail . "</EmailAddress>
                       <Addresses>
						 <Address>
						     <AddressType>STREET</AddressType>
							 <AddressLine1>" . $address1 . "</AddressLine1>
							 <AddressLine2>" . $address2 . "</AddressLine2>
							 <City>" . $city . "</City>
							 <Region>" . $state . "</Region>
							 <PostalCode>" . $zipCode . "</PostalCode>
							 <Country>" . $country . "</Country>
						 </Address>
						 <Address>
						     <AddressType>STREET2</AddressType>
							 <AddressLine1>" . $ship_address1 . "</AddressLine1>
							 <AddressLine2>" . $ship_address2 . "</AddressLine2>
							 <City>" . $ship_city . "</City>
							 <Region>" . $ship_state . "</Region>
							 <PostalCode>" . $ship_zipcode . "</PostalCode>
							 <Country>" . $ship_country . "</Country>
						 </Address>
					  </Addresses>
					  
					  
					  <Phones>
						<Phone>
						    <PhoneType>MOBILE</PhoneType> 
						    <PhoneNumber>" . $phoneNumber . "</PhoneNumber>
						</Phone>
					  </Phones>
                     </Contact>";

						$response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $xml);

						
						if ($XeroOAuth->response['code'] == 200) {





							$contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

							$this->session->set_flashdata('success', 'Success');

							redirect(base_url('Xero_controllers/Customer/get_customer'));
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

							redirect(base_url('Xero_controllers/Customer/get_customer'));
						}
					} else {
						$xml = "<Contacts>
                     <Contact>
                       <Name>" . $fullName . "</Name>
                       <FirstName>" . $firstName . "</FirstName>
                       <LastName>" . $lastName . "</LastName>
                       <EmailAddress>" . $userEmail . "</EmailAddress>
                       <Addresses>
						 <Address>
						     <AddressType>STREET</AddressType>
							 <AddressLine1>" . $address1 . "</AddressLine1>
							<AddressLine2>" . $address2 . "</AddressLine2>
							 <City>" . $city . "</City>
							 <Region>" . $state . "</Region>
							 <PostalCode>" . $zipCode . "</PostalCode>
							 <Country>" . $country . "</Country>
						 </Address>
						  <Address>
						     <AddressType>STREET2</AddressType>
							 <AddressLine1>" . $ship_address1 . "</AddressLine1>
							 <AddressLine2>" . $ship_address2 . "</AddressLine2>
							 <City>" . $ship_city . "</City>
							 <Region>" . $ship_state . "</Region>
							 <PostalCode>" . $ship_zipcode . "</PostalCode>
							 <Country>" . $ship_country . "</Country>
						 </Address>
					  </Addresses>
					  <Phones>
						<Phone>
						    <PhoneType>MOBILE</PhoneType> 
						    <PhoneNumber>" . $phoneNumber . "</PhoneNumber>
						</Phone>
					  </Phones>
                     </Contact>
                   </Contacts>";

						$response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $xml);

						if ($XeroOAuth->response['code'] == 200) {

							$contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

							$this->session->set_flashdata('success', 'Success');

							redirect(base_url('Xero_controllers/Customer/get_customer'));
						} else {

							$lsID = mt_rand(7000000, 8000000);
							$insert['Customer_ListID'] = $lsID;
							$insert['customerStatus'] = 'false';

							if (empty($this->general_model->get_row_data('Xero_custom_customer', array('fullName' => $contact['Name'], 'merchantID' => $merchID))))
								$this->general_model->insert_row_data('Xero_custom_customer', $insert);

							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error </strong></div>');

							redirect(base_url('Xero_controllers/Customer/get_customer'));
						}
					}
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Validation Error </strong></div>');

					redirect(base_url('Xero_controllers/Customer/get_customer'));
				}
			}
		}
		if ($this->uri->segment('4')) {
			$customerListID             = $this->uri->segment('4');
			$con                = array('Customer_ListID' => $customerListID);
			$data['customer'] = $this->general_model->get_row_data('Xero_custom_customer', $con);
		}


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info']     = $this->session->userdata('logged_in');

		$country = $this->general_model->get_table_data('country', '');
		$data['country_datas'] = $country;

		$state = $this->general_model->get_table_data('state', '');
		$data['state_datas'] = $state;

		$city = $this->general_model->get_table_data('city', '');
		$data['city_datas'] = $city;
		$company = $this->general_model->get_table_data('tbl_company', array('merchantID' => $user_id));
		$data['companys'] = $company;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/create_customer', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/***************************************************** End *************************************************************/


	/*************** Function for: Get Customer ****************/

	public function get_customer()
	{

		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}else if ($this->session->userdata('user_logged_in')) {
				$user_id = $this->session->userdata('user_logged_in')['merchantID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);

			
			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Contacts', 'core'), array());

			
			if ($XeroOAuth->response['code'] == 200) {
				$contacts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

				$contact_arr = json_decode(json_encode($contacts), 1);




				if (isset($contact_arr['Contacts'])) {



					foreach ($contact_arr['Contacts'] as $contact) {



						if (count($contact_arr['Contacts']['Contact']) > 1) {



					

							if (isset($contact['Phones'])) {
								if (array_key_exists("PhoneNumber", $contact['Phones']['Phone'][1])) {
									$mobile_num = $contact['Phones']['Phone'][1]['PhoneNumber'];
								} else if (array_key_exists("PhoneNumber", $contact['Phones']['Phone'][3])) {
									$mobile_num = $contact['Phones']['Phone'][3]['PhoneNumber'];
								}
							}


							if (isset($contact['Addresses'])) {

								if (array_key_exists("AddressLine1", $contact['Addresses']['Address'][0])) {
									$address = $contact['Addresses']['Address'][0]['AddressLine1'];
								} else if (array_key_exists("AddressLine1", $contact['Addresses']['Address'][1])) {
									$address = $contact['Addresses']['Address'][1]['AddressLine1'];
								} else {
									$address = '';
								}
								if (array_key_exists("AddressLine2", $contact['Addresses']['Address'][0])) {
									$address2 = $contact['Addresses']['Address'][0]['AddressLine2'];
								} else if (array_key_exists("AddressLine2", $contact['Addresses']['Address'][1])) {
									$address2 = $contact['Addresses']['Address'][1]['AddressLine2'];
								} else {
									$address2 = '';
								}


								if (array_key_exists("PostalCode", $contact['Addresses']['Address'][0])) {
									$zipcode = $contact['Addresses']['Address'][0]['PostalCode'];
								} else if (array_key_exists("PostalCode", $contact['Addresses']['Address'][1])) {
									$zipcode = $contact['Addresses']['Address'][1]['PostalCode'];
								} else {
									$zipcode = '';
								}

								if (array_key_exists("Region", $contact['Addresses']['Address'][0])) {
									$state = $contact['Addresses']['Address'][0]['Region'];
								} else if (array_key_exists("Region", $contact['Addresses']['Address'][1])) {
									$state = $contact['Addresses']['Address'][1]['Region'];
								} else {
									$state = '';
								}

								if (array_key_exists("Country", $contact['Addresses']['Address'][0])) {
									$country = $contact['Addresses']['Address'][0]['Country'];
								} else if (array_key_exists("Country", $contact['Addresses']['Address'][1])) {
									$country = $contact['Addresses']['Address'][1]['Country'];
								} else {
									$country = '';
								}

								if (array_key_exists("City", $contact['Addresses']['Address'][0])) {
									$city = $contact['Addresses']['Address'][0]['City'];
								} else if (array_key_exists("City", $contact['Addresses']['Address'][1])) {
									$city = $contact['Addresses']['Address'][1]['City'];
								} else {
									$city = '';
								}
							}


							$Xero_customer_details = array(
								"Customer_ListID" => $contact['ContactID'],
								"firstName" => $this->db->escape_str($contact['FirstName']),
								"lastName" => $this->db->escape_str($contact['LastName']),
								"fullName" => $this->db->escape_str($contact['Name']),
								"userEmail" => $this->db->escape_str($contact['EmailAddress']),
								"phoneNumber" => $this->db->escape_str($mobile_num),
								"address1" => $this->db->escape_str($address),
								"address2" => $this->db->escape_str($address2),
								"zipCode" => $this->db->escape_str($zipcode),
								"Country" => $this->db->escape_str($country),
								"City" => $this->db->escape_str($city),
								"State" => $this->db->escape_str($state),
								"companyName" => $this->db->escape_str($contact['Name']),
								"ship_address1" => $this->db->escape_str($address),
								"ship_address2" => $this->db->escape_str($address2),
								"ship_city"    => $this->db->escape_str($city),
								"ship_country"  => $this->db->escape_str($country),
								"ship_state"    => $this->db->escape_str($state),
								"ship_zipcode" => $this->db->escape_str($zipcode),


								"updatedAt" => $this->db->escape_str($contact['UpdatedDateUTC']),
								"merchantID" => $merchID
							);


							if (!empty($this->general_model->get_row_data('Xero_custom_customer', array('fullName' => $contact['Name'], 'merchantID' => $merchID))))
								$this->general_model->update_row_data('Xero_custom_customer', array('fullName' => $contact['Name'], 'merchantID' => $merchID), $Xero_customer_details);
							else
								$this->general_model->insert_row('Xero_custom_customer', $Xero_customer_details);
							

						} else {



							$contact = $contact1;

							if (isset($contact['Phones'])) {


								if (array_key_exists("PhoneNumber", $contact['Phones']['Phone'][1])) {
									$mobile_num = $contact['Phones']['Phone'][1]['PhoneNumber'];
								} else if (array_key_exists("PhoneNumber", $contact['Phones']['Phone'][3])) {
									$mobile_num = $contact['Phones']['Phone'][3]['PhoneNumber'];
								}
							}


							if (isset($contact['Addresses'])) {

								if (array_key_exists("AddressLine1", $contact['Addresses']['Address'][0])) {
									$address = $contact['Addresses']['Address'][0]['AddressLine1'];
								} else if (array_key_exists("AddressLine1", $contact['Addresses']['Address'][1])) {
									$address = $contact['Addresses']['Address'][1]['AddressLine1'];
								} else {
									$address = '';
								}
								if (array_key_exists("AddressLine2", $contact['Addresses']['Address'][0])) {
									$address2 = $contact['Addresses']['Address'][0]['AddressLine2'];
								} else if (array_key_exists("AddressLine2", $contact['Addresses']['Address'][1])) {
									$address2 = $contact['Addresses']['Address'][1]['AddressLine2'];
								} else {
									$address2 = '';
								}


								if (array_key_exists("PostalCode", $contact['Addresses']['Address'][0])) {
									$zipcode = $contact['Addresses']['Address'][0]['PostalCode'];
								} else if (array_key_exists("PostalCode", $contact['Addresses']['Address'][1])) {
									$zipcode = $contact['Addresses']['Address'][1]['PostalCode'];
								} else {
									$zipcode = '';
								}

								if (array_key_exists("Region", $contact['Addresses']['Address'][0])) {
									$state = $contact['Addresses']['Address'][0]['Region'];
								} else if (array_key_exists("Region", $contact['Addresses']['Address'][1])) {
									$state = $contact['Addresses']['Address'][1]['Region'];
								} else {
									$state = '';
								}

								if (array_key_exists("Country", $contact['Addresses']['Address'][0])) {
									$country = $contact['Addresses']['Address'][0]['Country'];
								} else if (array_key_exists("Country", $contact['Addresses']['Address'][1])) {
									$country = $contact['Addresses']['Address'][1]['Country'];
								} else {
									$country = '';
								}

								if (array_key_exists("City", $contact['Addresses']['Address'][0])) {
									$city = $contact['Addresses']['Address'][0]['City'];
								} else if (array_key_exists("City", $contact['Addresses']['Address'][1])) {
									$city = $contact['Addresses']['Address'][1]['City'];
								} else {
									$city = '';
								}
							}



							$Xero_customer_details = array(
								"Customer_ListID" => $contact['ContactID'],
								"firstName" => $this->db->escape_str($contact['FirstName']),
								"lastName" => $this->db->escape_str($contact['LastName']),
								"fullName" => $this->db->escape_str($contact['Name']),
								"userEmail" => $this->db->escape_str($contact['EmailAddress']),
								"phoneNumber" => $this->db->escape_str($mobile_num),
								"address1" => $this->db->escape_str($address),
								"address2" => $this->db->escape_str($address2),
								"zipCode" => $this->db->escape_str($zipcode),
								"Country" => $this->db->escape_str($country),
								"City" => $this->db->escape_str($city),
								"State" => $this->db->escape_str($state),
								"companyName" => $this->db->escape_str($contact['Name']),
								"ship_address1" => $this->db->escape_str($address),
								"ship_address2" => $this->db->escape_str($address2),
								"ship_city"    => $this->db->escape_str($city),
								"ship_country"  => $this->db->escape_str($country),
								"ship_state"    => $this->db->escape_str($state),
								"ship_zipcode" => $this->db->escape_str($zipcode),

								"companyID" => 2,
								"updatedAt" => $this->db->escape_str($contact['UpdatedDateUTC']),
								"merchantID" => $merchID
							);

							if (!empty($this->general_model->get_row_data('Xero_custom_customer', array('Customer_ListID' => $contact['ContactID'], 'merchantID' => $merchID))))
								$this->general_model->update_row_data('Xero_custom_customer', array('Customer_ListID' => $contact['ContactID'], 'merchantID' => $merchID), $Xero_customer_details);
							else
								$this->general_model->insert_row('Xero_custom_customer', $Xero_customer_details);
						}
					}
				}
			}

			$data['primary_nav']  = primary_nav();
			$data['template']   = template_variable();

			$in_data['merchantID'] =  $merchID;
			$condition = array('merchantID' => $merchID);
			$data['customers'] = $this->xero_customer_model->get_customer_data($condition);


			$this->load->view('template/template_start', $data);
			$this->load->view('template/page_head', $data);
			$this->load->view('Xero_views/page_customers', $data);
			$this->load->view('template/page_footer', $data);
			$this->load->view('template/template_end', $data);
		}
	}

	public function delete_customer()
	{

		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);
		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			if ($this->session->userdata('logged_in')) {
				$user_id = $this->session->userdata('logged_in')['merchID'];
				$merchID = $user_id;
			}

			$val = array(
				'merchantID' => $merchID,
			);

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			$cust_id = $this->czsecurity->xssCleanPostInput('xerocustID');

			$xml = "<Contact>
				<ContactID>" . $cust_id . "</ContactID>
				<ContactStatus>ARCHIVED</ContactStatus>
				</Contact>";


			$response = $XeroOAuth->request('POST', $XeroOAuth->url('Contacts', 'core'), array(), $xml);

			if ($XeroOAuth->response['code'] == 200) {
				$contact = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

				$this->session->set_flashdata('success', 'Success');

				redirect(base_url('Xero_controllers/Customer/get_customer'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error</strong></div>');

				redirect(base_url('Xero_controllers/Customer/get_customer'));
			}
		}
	}
	/**************** End *****************/
	public function view_customer($cusID = '')
	{

		if ($this->session->userdata('logged_in')) {
			$data['login_info']     = $this->session->userdata('logged_in');

			$user_id                = $data['login_info']['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$data['login_info']     = $this->session->userdata('user_logged_in');

			$user_id                = $data['login_info']['merchantID'];
		}
		$data['page_num'] = 'customer_xero';

		$data['primary_nav']  = primary_nav();
		$data['template']      = template_variable();
	

		$data['customer']  = $this->xero_customer_model->customer_details($cusID, $user_id);

		// Convert added date in timezone 
        if(isset($data['customer']->TimeCreated) && isset($data['login_info']['merchant_default_timezone']) && !empty($data['login_info']['merchant_default_timezone']) ){
          $timezone = ['time' => $data['customer']->TimeCreated, 'current_format' => 'UTC', 'new_format' => $data['login_info']['merchant_default_timezone']];
          $data['customer']->TimeCreated = getTimeBySelectedTimezone($timezone);
        }
        
		$data['notes']   		  = $this->xero_customer_model->get_customer_note_data($cusID, $user_id);

		$data_invoice            = $this->xero_customer_model->get_customer_invoice_data_sum($cusID, $user_id);

		$data['invoices_count']    = ($data_invoice->incount) ? $data_invoice->incount : '0';
		
		$data['invoices']        = $this->xero_customer_model->get_invoice_upcomming_data($cusID, $user_id);
		$data['latest_invoice']      = $this->xero_customer_model->get_invoice_latest_data($cusID, $user_id);

		$paydata           =   $this->xero_customer_model->get_customer_invoice_data_payment($cusID, $user_id);

		$data['sum_invoice']  =   (floatval(($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00') +
			floatval(($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00') +
			floatval(($paydata->applied_amount) ? $paydata->applied_amount : '0.00'));

		$data['pay_invoice']       = ($paydata->applied_amount) ?: '0.00';
		$data['pay_upcoming']      = ($paydata->upcoming_balance) ? $paydata->upcoming_balance : '0.00';
		$data['pay_remaining']     = ($paydata->remaining_amount) ? $paydata->remaining_amount : '0.00';
		$data['pay_due_amount']  = ($paydata->applied_due) ? $paydata->applied_due : '0.00';
		$mail_con  = array('merchantID' => $user_id, 'customerID' => $cusID);

		$data['editdatas']   = $this->xero_customer_model->get_email_history($mail_con);
		$data['card_data_array']     = $this->get_card_expiry_data($cusID);

		$condition                 = array('merchantID' => $user_id);
		$data['gateway_datas']         = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

		$merchant_condition = [
            'merchID' => $user_id,
        ];

        $data['defaultGateway'] = false;
        if (!merchant_gateway_allowed($merchant_condition)) {
            $defaultGateway         = $this->general_model->merchant_default_gateway($merchant_condition);
            $data['defaultGateway'] = $defaultGateway[0];
        }

		$sub  = array('sbs.customerID' => $cusID);
		$data['getsubscriptions']   = $this->xero_customer_model->get_cust_subscriptions_data($sub);
		
		$data['from_mail'] = DEFAULT_FROM_EMAIL;
		$data['mailDisplayName'] = $this->loginDetails['companyName'];

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);

		$this->load->view('Xero_views/page_customer_details', $data);
		$this->load->view('Xero_views/page_xero_model', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function add_note()
	{

		if (!empty($this->czsecurity->xssCleanPostInput('customerID'))) {
			$cusID = $this->czsecurity->xssCleanPostInput('customerID');
			if ($this->czsecurity->xssCleanPostInput('private_note') != "") {

				$private_note  = $this->czsecurity->xssCleanPostInput('private_note');
				$data_ar = array('privateNote' => $private_note, 'privateNoteDate' => date('Y-m-d H:i:s'), 'customerID' => $cusID);
				$id   = $this->general_model->insert_row('tbl_private_note', $data_ar);
				if ($id > 0) {
					array('status' => "success");
					echo json_encode(array('status' => "success"));
					die;
				} else {
					array('status' => "error");
					echo json_encode(array('status' => "success"));
					die;
				}
			}
		}
	}
	public function  delele_note()
	{


		if ($this->czsecurity->xssCleanPostInput('noteID') != "") {

			$noteID = $this->czsecurity->xssCleanPostInput('noteID');

			if ($this->db->query("Delete from tbl_private_note where noteID =  '" . $noteID . "' ")) {

				array('status' => "success");
				echo json_encode(array('status' => "success"));
				die;
			}
			return false;
		}
	}


	public function account_list()
	{
		define('BASE_PATH', dirname(__FILE__));

		define("XRO_APP_TYPE", "Public");

		$useragent = "Chargezoom";

		$data = $this->general_model->get_table_data('xero_config', '');

		if (!empty($data)) {

			foreach ($data as $dt) {
				$url = $dt['oauth_callback'];
				$key = $dt['consumer_key'];
				$secret = $dt['shared_secret'];
			}
		}
		define("OAUTH_CALLBACK", $url);

		$signatures = array(
			'consumer_key' =>  $key,
			'shared_secret' => $secret,
			// API versions
			'core_version' => '2.0',
			'payroll_version' => '1.0',
			'file_version' => '1.0'
		);

		$XeroOAuth = new XeroOAuth(array_merge(array(
			'application_type' => XRO_APP_TYPE,
			'oauth_callback' => OAUTH_CALLBACK,
			'user_agent' => $useragent
		), $signatures));

		$initialCheck = $XeroOAuth->diagnostics();
		$checkErrors = count($initialCheck);

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$user_id    = $data['login_info']['merchID'];
		$merchID = $user_id;

		$val = array(
			'merchantID' => $merchID,
		);

		if ($checkErrors > 0) {
			// you could handle any config errors here, or keep on truckin if you like to live dangerously
			foreach ($initialCheck as $check) {
				echo 'Error: ' . $check . PHP_EOL;
			}
		} else {

			$del = $this->general_model->delete_row_data('tbl_xero_accounts', $val);

			$xero_data = $this->general_model->get_row_data('tbl_xero_token', $val);

			$XeroOAuth->config['access_token']  = $xero_data['accessToken'];
			$XeroOAuth->config['access_token_secret'] = $xero_data['access_token_secret'];

			$response = $XeroOAuth->request('GET', $XeroOAuth->url('Accounts', 'core'), array());


			if ($XeroOAuth->response['code'] == 200) {
				$accounts = $XeroOAuth->parseResponse($XeroOAuth->response['response'], $XeroOAuth->response['format']);

				$account_arra = json_decode(json_encode($accounts), 1);

				foreach ($account_arra['Accounts'] as $account_arrs) {

					foreach ($account_arrs as $account_data) {

						$xero_acc_details['accountID'] = $this->db->escape_str($account_data['AccountID']);
						$xero_acc_details['accountName'] = $this->db->escape_str($account_data['Name']);
						$xero_acc_details['accountCode'] = $this->db->escape_str($account_data['Code']);
						$xero_acc_details['class'] = $this->db->escape_str($account_data['Class']);
						$xero_acc_details['accountDescription'] = ($account_data['Description']) ? $account_data['Description'] : '';
						$xero_acc_details['merchantID'] = $merchID;
						$xero_acc_details['status'] = $this->db->escape_str($account_data['Status']);
						$xero_acc_details['merchantID'] = $merchID;
						$xero_acc_details['createdAt'] = date("Y-m-d H:i:s");
						$xero_acc_details['updatedAt'] = date("Y-m-d H:i:s", strtotime($account_data['UpdatedDateUTC']));

						$this->general_model->insert_row('tbl_xero_accounts', $xero_acc_details);
					}
				}
			}
		}
		$condition =  array('merchantID' => $merchID);
		$accounts = $this->general_model->get_table_data('tbl_xero_accounts', $condition);

		$data['accounts'] = $accounts;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('Xero_views/page_accounts', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function get_xero_sale_invoices()
	{
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$invoices 		  = $this->xero_customer_model->get_invoice_upcomming_data($customerID, $merchantID);

			$new_inv = '<div class="form-group" >
		        <div class="col-md-1  text-center"><b>Select</b></div>
		        <div class="col-md-2 text-left"><b>Number</b></div>
		         <div class="col-md-2 text-left"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
		        </div>';
			foreach ($invoices as $inv) {
				
				$new_inv .= '<div class="form-group" >
		       
		        <div class="col-md-1 text-center"><input type="checkbox" class="chk_pay check_'.$inv['refNumber'].'"  id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position1(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['refNumber'] . '</div>
		        <div class="col-md-2 text-left">' . date("m/d/y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">$' . number_format($inv['BalanceRemaining'], 2) . '</div>
		       <div class="col-md-3 text-left"><input type="text" name="pay_amount[]" onblur="chk_pay_position1(this);"  class="form-control   multiinv' . $inv['invoiceID'] . ' " data-id="multiinv' . $inv['invoiceID'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div></div>';
			}


			$card = '';
			$card_name = '';
			$customerdata = array();





			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);
			if (!empty($customerdata)) {

			
				$customerdata['status'] =  'success';

				$card_data =   $this->get_card_expiry_data($customerID);
				$customerdata['card']  = $card_data;
				$customerdata['invoices']   = $new_inv;

				echo json_encode($customerdata);
				die;
			}
		}
	}

	public function get_xero_customer_invoices()
	{
		if ($this->czsecurity->xssCleanPostInput('customerID') != "") {

			$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');

			if ($this->session->userdata('logged_in')) {
				$merchantID = $this->session->userdata('logged_in')['merchID'];
			}
			if ($this->session->userdata('user_logged_in')) {
				$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
			}

			$invoices 		  = $this->xero_customer_model->get_invoice_upcomming_data($customerID, $merchantID);

			$new_inv = '<div class="form-group" >
		        <div class="col-md-1  text-center"><b>Select</b></div>
		        <div class="col-md-2 text-left"><b>Number</b></div>
		         <div class="col-md-2 text-left"><b>Due Date</b></div>
		        <div class="col-md-2 text-right"><b>Amount</b></div>
		        <div class="col-md-3 text-left"><b>Payment</b></div>
		        </div>';
			foreach ($invoices as $inv) {
				
				$new_inv .= '<div class="form-group" >
		       
		        <div class="col-md-1 text-center"><input type="checkbox" class="chk_pay"  id="' . 'multiinv' . $inv['invoiceID'] . '"  onclick="chk_inv_position(this);" name="multi_inv[]" value="' . $inv['invoiceID'] . '" /> </div>
		        <div class="col-md-2 text-left">' . $inv['refNumber'] . '</div>
		        <div class="col-md-2 text-left">' . date("m/d/y", strtotime($inv['DueDate'])) . '</div>
		        <div class="col-md-2 text-right">$' . number_format($inv['BalanceRemaining'], 2) . '</div>
		       <div class="col-md-3 text-left"><input type="text" name="pay_amount[]" onblur="chk_pay_position(this);"  class="form-control   multiinv' . $inv['invoiceID'] . ' " data-id="multiinv' . $inv['invoiceID'] . '"  value="' . $inv['BalanceRemaining'] . '" /></div></div>';
			}


			$card = '';
			$card_name = '';
			$customerdata = array();


			$condition     =  array('Customer_ListID' => $customerID, 'merchantID' => $merchantID);
			$customerdata = $this->general_model->get_row_data('Xero_custom_customer', $condition);
			if (!empty($customerdata)) {

				
				$customerdata['status'] =  'success';

				$card_data =   $this->get_card_expiry_data($customerID);
				$customerdata['card']  = $card_data;
				$customerdata['invoices']   = $new_inv;

				echo json_encode($customerdata);
				die;
			}
		}
	}


	public function get_card_expiry_data($customerID)
	{

		$card = array();
		$this->load->library('encrypt');
		if ($this->session->userdata('logged_in')) {
			$merchantID = $this->session->userdata('logged_in')['merchID'];
		}
		if ($this->session->userdata('user_logged_in')) {
			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
		}



		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'   ";
		$query1 = $this->db1->query($sql);
		$card_datas =   $query1->result_array();
		if (!empty($card_datas)) {
			foreach ($card_datas as $key => $card_data) {

				$card_data['CardNo']  = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
				$card_data['CardID'] = $card_data['CardID'];
				$card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'];
				
				$card_data['accountNumber'] = $card_data['accountNumber'];
				$card_data['accountName']  = $card_data['accountName'];
				$card_data['CardType'] = $card_data['CardType'];
				$card[$key] = $card_data;
			}
		}

		return  $card;
	}
}
