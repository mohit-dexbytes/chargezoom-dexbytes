<?php

/**
 * This Controller has Stripe Payment Gateway Process
 * 
 * Create_customer_sale for Sale process : here we use the payment token
 * pay_invoice perform sale process  for one invoice it may be partial for full payment. 
 * 
 * multi_pay_invoice perform sale process  for one or more invoices, it may be partial for full payment.
 * create_customer_auth perform auhorize process
 * create_customer_capture perform settled operation for authorize transactions
 * create_customer_refund perform refund operation for settled transactions which is performed by capture or sale process
 */

class CyberSource extends CI_Controller
{
    private $resellerID;
    private $mName;
    private $transactionByUser;
   	public function __construct()
	{
		parent::__construct();
		
		
        $this->load->config('cyber_pay');
		$this->load->config('quickbooks');	
        $this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		
		$this->load->model('Xero_models/xero_model');
			
		$this->load->library('form_validation'); 
		 if($this->session->userdata('logged_in')!="" && $this->session->userdata('logged_in')['active_app']=='2' )
		  {
            $logged_in_data = $this->session->userdata('logged_in');
            $this->resellerID = $logged_in_data['resellerID'];
            $this->transactionByUser = ['id' => $logged_in_data['merchID'], 'type' => 1];
		    $this->mName      =  $logged_in_data['companyName'];
		  }else if($this->session->userdata('user_logged_in')!="")
		  {
		    $logged_in_data = $this->session->userdata('user_logged_in');
            $this->transactionByUser = ['id' => $logged_in_data['merchantUserID'], 'type' => 2];
            $merchID = $logged_in_data['merchantID'];
            $rs_Data = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID'), array('merchID' => $merchID));
            $this->resellerID = $rs_Data['resellerID'];
		    $this->mName = $rs_Data['companyName'];
		  }else{
			redirect('login','refresh');
		  }  
	}
	
	
	
	public function index()
	{
	    
    	redirect('home/index','refresh');
      
	    
	}
	
	
   	public function pay_invoice()
	{
	     
	      if($this->session->userdata('logged_in'))
	      {
			    $user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
		
			    $user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	      
			$this->form_validation->set_rules('invoiceProcessID', 'InvoiceID', 'required|xss_clean');			
			$this->form_validation->set_rules('inv_amount', 'Payment Amount', 'required|xss_clean');
			$this->form_validation->set_rules('gateway', 'Gateway', 'required|xss_clean');
			
    		$this->form_validation->set_rules('CardID', 'Card ID', 'trim|required|xss-clean');
    
    		if($this->czsecurity->xssCleanPostInput('CardID')=="new1")
            { 
            
              
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
            
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
               
                $this->form_validation->set_rules('contact', 'Phone Number', 'trim|required|xss-clean');
            }
            
                 $cusproID=''; $error='';
		    	 $cusproID   = $this->czsecurity->xssCleanPostInput('customerProcessID');
		    	
              
	    	if ($this->form_validation->run() == true)
		    {
		       
		          $cardID_upd ='';
		    	 
			     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
			     	
			     $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
			     
			     $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
			   
			     $amount               = $this->czsecurity->xssCleanPostInput('inv_amount');
			      
			     $in_data              =    $this->xero_model->get_invoice_data_pay($invoiceID, $user_id);
			     
			     if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			  
			    if(!empty( $in_data)) 
			    {
			          
    			        $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
    			        $customerID =  $in_data['Customer_ListID'];
    				  	$c_data     = $this->general_model->get_select_data('Xero_test_customer',array('companyID', 'FirstName', 'LastName',  'companyName','Phone', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    			     	$companyID = $c_data['companyID'];
    			        $gmerchantID    = $gt_result['gatewayUsername'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
						$secretKey      = $gt_result['gatewaySignature'];
    			     
    			     if($cardID!="new1")
    			     {
    			        
    			        $card_data=   $this->card_model->get_single_card_data($cardID);
    			       
    			        $card_no  = $card_data['CardNo'];
				        $expmonth =  $card_data['cardMonth'];
    					$exyear   = $card_data['cardYear'];
    					$cvv      = $card_data['CardCVV'];
    					$cardType = $card_data['CardType'];
    					$address1 = $card_data['Billing_Addr1'];
    					$city     =  $card_data['Billing_City'];
    					$zipcode  = $card_data['Billing_Zipcode'];
    					$state    = $card_data['Billing_State'];
    					$country  = $card_data['Billing_Country'];
    					$phone    = ($card_data['Billing_Contact'])?$card_data['Billing_Contact']:$c_data['Phone'];
    			     }
    			     else
    			     {
    			          
    			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
    					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
    					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
    					$cardType = $this->general_model->getType($card_no);
    					$cvv ='';
    					if($this->czsecurity->xssCleanPostInput('cvv')!="")
    					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
    					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
    	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
    	                        $country =$this->czsecurity->xssCleanPostInput('country');
    	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
    	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
    			         
    			     }                              
         
             	
    				if( $in_data['BalanceRemaining'] > 0)
    				{
        				    
                    	$crtxnID='';  
                        $flag="true";
                        $error=$user='';
                     
        				$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
                                {
                                   
        						   $env   = $this->config->item('SandboxENV');
                                }
                                else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => $this->mName
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $c_data['FirstName'],
                    		"lastName" => $c_data['LastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $phone,
                    		"company" => $c_data['companyName'],
                    		"email" => $c_data['Contact']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                    	
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  
        					   $this->session->set_flashdata('success','Successfully Processed Invoice');
        					  
                                     $txnID      = $in_data['TxnID'];  
        							 $ispaid 	 = 'true';
        							 $bamount    = $in_data['BalanceRemaining']-$amount;
        							  if($bamount > 0)
        							  $ispaid 	 = 'false';
        							 $app_amount = $in_data['AppliedAmount']-$amount;
        							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
        							 $condition  = array('TxnID'=>$in_data['TxnID'] );
        							 if($chh_mail =='1')
        							 {
        							  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
        							  $ref_number =  $in_data['RefNumber']; 
        							  $tr_date   =date('Y-m-d H:i:s');
        							  $toEmail = $c_data['Contact']; $company=$c_data['companyName']; $customer = $c_data['FullName'];  
        							 
        							  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
        							 }
        							 
        							 $this->general_model->update_row_data('Xero_test_invoice',$condition, $data);
        					
                            
                            				 if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
                    				 $this->session->set_flashdata('success','Successfully Processed Invoice');
        					    
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
        					      $this->session->set_flashdata('success',$error);
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                         
                        
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    	    
        			}  
        			
                      
        				   
        				           
        		    }
        		    else        
                    {
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                        
                    }
                 
        		   
		    } 
		    else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			
			  
		   
                 if($cusproID=="2"){
			 	 redirect('home/view_customer/'.$customerID,'refresh');
			 }
           if($cusproID=="3" && $in_data['TxnID']!=''){
			 	 redirect('home/invoice_details/'.$in_data['TxnID'],'refresh');
			 }
		   	  if($cusproID=="1") {
		   	 redirect('home/invoices','refresh');
		   	 }
        	 redirect('home/invoices','refresh');
	      
	}
	
	public function create_customer_sale()
	{ 
	      
	    
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    			
			
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
              
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
           
               
	    	if ($this->form_validation->run() == true)
		    {
	   
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        			   
        			  
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
        				
                        $comp_data     =$this->general_model->get_select_data('Xero_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        			 
        		
        		        $cardID = $this->czsecurity->xssCleanPostInput('card_list');
                          
        				$cvv='';	
        		       if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
        		       {	
        				     	$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
        						$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
        						
        						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
        						if($this->czsecurity->xssCleanPostInput('cvv')!="")
        					    $cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
        				}
        				else 
        				{     
        				            $card_data= $this->card_model->get_single_card_data($cardID);
                                	$card_no  = $card_data['CardNo'];
        							$expmonth =  $card_data['cardMonth'];
        							$exyear   = $card_data['cardYear'];
        							if($card_data['CardCVV'])
        							$cvv      = $card_data['CardCVV'];
        							$cardType = $card_data['CardType'];
        				}
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                      $contact=  $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');

						
                            $crtxnID='';  
                            $flag="true";
                            $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" =>  $this->mName,
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        					
        					
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                			     
            				 $invoiceIDs=array();      
        				     if(!empty($this->czsecurity->xssCleanPostInput('invoice_id')))
        				     {
        				     	$invoiceIDs =explode(',', $this->czsecurity->xssCleanPostInput('invoice_id'));
        				     }
								$refnum=array();
							   if(!empty($invoiceIDs))
							   {
								   
								  foreach($invoiceIDs as $inID)
								  {
										$theInvoice = array();
										 
										$theInvoice = $this->general_model->get_row_data('qb_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'));
										
								
											
											if(!empty($theInvoice) )
											{
												$app = $theInvoice['AppliedAmount']+(-$theInvoice['BalanceRemaining']);
												$tes = $this->general_model->update_row_data('Xero_test_invoice',array('TxnID'=>$inID, 'IsPaid'=>'false'),array('BalanceRemaining'=>'0.00','AppliedAmount'=>$app,'IsPaid'=>'true')) ;
										
												 $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$theInvoice['BalanceRemaining'],$user_id,$crtxnID, $this->resellerID,$inID, false, $this->transactionByUser);  
											   $refnum[]= $theInvoice['RefNumber'];
											   
											    $comp_data1 = $this->general_model->get_row_data('tbl_company',array('merchantID'=>$user_id));
                    				        $user      = $comp_data1['qbwc_username'] ;
                    				       $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
											  
												
											}
								  }
								  if($chh_mail =='1')
								 {
								  $condition_mail         = array('templateType'=>'5', 'merchantID'=>$user_id); 
								  $ref_number =  implode(',',$refnum); 
								  $tr_date   =date('Y-m-d H:i:s');
								  $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
								  $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   );
								 }
							  }
							   else
							   {
									  
											$transactiondata= array();
											$inID='';
										   $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
							   }
						 
				     
            				       
            			         if( $cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
            				     {
            				 		$crType =$this->general_model->getType($card_no);
            				        $friendlyname   = $crType.' - '.substr($card_no,-4);
            						$card_condition = array(
            										 'customerListID' =>$customerID, 
            										 'customerCardfriendlyName'=>$friendlyname,
            										);
            					
            						$crdata =	$this->card_model->check_friendly_name($customerID,$friendlyname)	;			
            					     
            					   
            						if(!empty($crdata))
            						{
            							
            						   $card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	     =>$exyear,
            										  'companyID'    =>$companyID,
            										  'merchantID'   => $user_id,
            										  'CardType'     =>$crType,
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										  'updatedAt'    => date("Y-m-d H:i:s"),
            										   'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										  );
            										  
            							     		  
            					
            						   $this->card_model->update_card_data($card_condition, $card_data);				 
            						}
            						else
            						{
            					     	$card_data = array('cardMonth'   =>$expmonth,
            										   'cardYear'	 =>$exyear, 
            										   'CardType'     =>$crType,
            										  'CustomerCard' =>$this->card_model->encrypt($card_no),
            										  'CardCVV'      =>'', 
            										 'customerListID' =>$customerID, 
            										 'companyID'     =>$companyID,
            										  'merchantID'   => $user_id,
            										 'customerCardfriendlyName'=>$friendlyname,
            										 'createdAt' 	=> date("Y-m-d H:i:s"),
            										 'Billing_Addr1'	 =>$address1,
                										 
                										  'Billing_City'	 =>$city,
                										  'Billing_State'	 =>$state,
                										  'Billing_Country'	 =>$country,
                										  'Billing_Contact'	 =>$contact,
                										  'Billing_Zipcode'	 =>$zipcode,
            										 );
            								
            				            $id1 =    $this->card_model->insert_card_data($card_data);	
            				            
            						
            						}
            				
            				 }
            				   
							   $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
            			
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                        $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser);  
                                     
                              }
                         
                         
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        				
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			

                   redirect('Payments/create_customer_sale','refresh');
	    
	    
    	}
	
   
	
	public function create_customer_auth()
	{ 
	      
	    
	        if($this->session->userdata('logged_in'))
		    {
				$user_id 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in'))
			{
				$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
	    			
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('gateway_list', 'Gateway', 'required|xss_clean');
    		$this->form_validation->set_rules('card_list', 'Card ID', 'trim|required|xss-clean');
    	    $this->form_validation->set_rules('totalamount', 'Amount', 'trim|required|xss-clean');
			$this->form_validation->set_rules('customerID', 'Customer ID', 'trim|required|xss-clean');
    		if($this->czsecurity->xssCleanPostInput('card_list')=="new1")
            { 
        
               $this->form_validation->set_rules('card_number', 'Card number', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry', 'Expiry Month', 'trim|required|xss-clean');
               $this->form_validation->set_rules('expiry_year', 'Expiry Year', 'trim|required|xss-clean');
          
            }
            
         
               
	    	if ($this->form_validation->run() == true)
		    {
	   
                       if($this->czsecurity->xssCleanPostInput('setMail'))
                        $chh_mail =1;
                       else
                        $chh_mail =0;  
        			   $gateway   = $this->czsecurity->xssCleanPostInput('gateway_list');
        			   $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
        		   
        			if(!empty($gt_result) )
        			{
        			  
        				
        				$customerID = $this->czsecurity->xssCleanPostInput('customerID');
                        $comp_data     =$this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
        				$companyID  = $comp_data['companyID'];
        		           $cardID = $this->czsecurity->xssCleanPostInput('card_list');
        				   $cvv='';	
						   if( $this->czsecurity->xssCleanPostInput('card_number')!="" && $cardID=='new1')
						   {	
									$card_no  = $this->czsecurity->xssCleanPostInput('card_number');
									$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
									
									$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
									if($this->czsecurity->xssCleanPostInput('cvv')!="")
									$cvv      =    $this->czsecurity->xssCleanPostInput('cvv'); 
							}
							else 
							{     
										$card_data= $this->card_model->get_single_card_data($cardID);
										$card_no  = $card_data['CardNo'];
										$expmonth =  $card_data['cardMonth'];
										$exyear   = $card_data['cardYear'];
										if($card_data['CardCVV'])
										$cvv      = $card_data['CardCVV'];
										$cardType = $card_data['CardType'];
							}
							
					
							    $companyName = $this->czsecurity->xssCleanPostInput('companyName');
						       $firstName  = $this->czsecurity->xssCleanPostInput('firstName');
							   $lastName      = $this->czsecurity->xssCleanPostInput('lastName');
								$amount   = $this->czsecurity->xssCleanPostInput('totalamount');
								$address1 =  $this->czsecurity->xssCleanPostInput('address1');
    	                    	$city     = $this->czsecurity->xssCleanPostInput('city');
    	                        $country  = $this->czsecurity->xssCleanPostInput('country');
    	                       $contact= $phone    =  $this->czsecurity->xssCleanPostInput('phone');
    	                        $state    =  $this->czsecurity->xssCleanPostInput('state');
    	                        $zipcode  =  $this->czsecurity->xssCleanPostInput('zipcode');
								$email    = $this->czsecurity->xssCleanPostInput('email');

						
                        $crtxnID='';  
                        $flag='false';
                        $error=$user='';
        				        $option =array();
        				        $option['merchantID']     = $gt_result['gatewayUsername'];
            			        $option['apiKey']         = $gt_result['gatewayPassword'];
        						$option['secretKey']      = $gt_result['gatewaySignature'];
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => $this->mName
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $firstName,
        					"lastName"  =>$lastName,
        					"address1"  => $address1,
        					"postalCode"=> $zipcode,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $exyear,
        					"number" => $card_no,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$type =   'Auth'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					    $code =   '200';
        					    $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                				 
								$transactiondata= array();
								$inID='';
							 
							 
            				     
                            
                             if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        								
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
            				   
							   $this->session->set_flashdata('success','Successfully Authorized Credit Card Payment'); 
            			
            				 } 
            				 else
            				 {
                                      $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                       $res = array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                       
                              }
                        
                             $id = $this->general_model->insert_gateway_transaction_data($res,'auth',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $this->resellerID,$inID='', false, $this->transactionByUser); 
                         
                        }
                        catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
					}
					else
					{
					$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed -  Please select Gateway</strong></div>'); 
					}		
        				        
                          
        		 }
                 else
        		    {
        		        
        		       
        		           $error='Validation Error. Please fill the requred fields';
        		           	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
        		    }
			

                   redirect('Payments/create_customer_auth','refresh');
	    
	    
    	}
    	
    	
    		
	public function create_customer_capture()
	{
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('strtxnID', 'Transaction ID', 'required|xss_clean');
			
             
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('strtxnID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                            $config    = $commonElement->ConnectionHost();
                        	$merchantConfig = $commonElement->merchantConfigObject();
                        	$apiclient    = new CyberSource\ApiClient($config, $merchantConfig);
                        	$api_instance = new CyberSource\Api\CaptureApi($apiclient);
                                
                                 $cliRefInfoArr = [
                    	    	"code" => $this->mName
                    	        ];
				 
                    		   $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                  $amountDetailsArr = [
                                      "totalAmount" => $amount,
                                      "currency" => CURRENCY
                                  ];
                                  $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                                
                                  $orderInfoArry = [
                                    "amountDetails" => $amountDetInfo
                                  ];
                                
                                  $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
                                  $requestArr = [
                                    "clientReferenceInformation" => $client_reference_information,
                                    "orderInformation" => $order_information
                                  ];
                                  //Creating model
                                  $request = new CyberSource\Model\CapturePaymentRequest($requestArr);
                                  $api_response = list($response,$statusCode,$httpHeader)=null;
                 
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->capturePayment($request, $tID);  
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        					  	$condition = array('transactionID'=>$tID);
            					$update_data =   array( 'transaction_user_status'=>"4",'transactionModified'=>date('Y-m-d H:i:s') );
            					$this->general_model->update_row_data('customer_transaction',$condition, $update_data);
        					  	if($chh_mail =='1')
                                {
                                    $condition = array('transactionID'=>$tID);
                                 
                                    $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                    $tr_date   =date('Y-m-d H:i:s');
                                    $ref_number =  $tID;
                                    $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                    $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'capture');
                                
                                }
                    				 $this->session->set_flashdata('success','Successfully Cpatured Transaction');
        					    
        					}
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
        					      $this->session->set_flashdata('success',$error);
        					}
        					
        			        	$id = $this->general_model->insert_gateway_transaction_data($res,'capture',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				  
			
					redirect('Payments/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('Payments/payment_capture','refresh');
			
	}
	
	
	
	/***************************** usaepay customer capture end***********************************/
	
	
	
	/**************************** uasepay  void Payment **************************/
	
			
	public function create_customer_void()
	{
		if($this->session->userdata('logged_in')){
				
		
			$merchantID 				= $this->session->userdata('logged_in')['merchID'];
			}
			if($this->session->userdata('user_logged_in')){
		
			$merchantID 				= $this->session->userdata('user_logged_in')['merchantID'];
			}
					
			$this->form_validation->set_rules('cybervoidID', 'Transaction ID', 'required|xss_clean');
			
             
	    	if ($this->form_validation->run() == true)
		    {
					
    			  $tID     = $this->czsecurity->xssCleanPostInput('cybervoidID');
				 
				 $con     = array('transactionID'=>$tID);
				 $paydata = $this->general_model->get_row_data('customer_transaction',$con);
				 $gateway = $paydata['gatewayID'];
				
				 $customerID = $paydata['customerListID'];
				 $amount  =  $paydata['transactionAmount']; 
				 $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			    	 
				 if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			   
                	$comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
             	
				 
    			            	$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                                $config = $commonElement->ConnectionHost();
                            	$merchantConfig = $commonElement->merchantConfigObject();
                            	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                            	$api_instance = new CyberSource\Api\VoidApi($apiclient);
                            	
                            	
                            		$cliRefInfoArr = [
                                        'code' => 'test_void'
                                      ];
                                      $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                                    
                                      $paymentRequestArr = [
                                        "clientReferenceInformation" => $client_reference_information
                                      ];
                                    
                                      $paymentRequest = new CyberSource\Model\VoidPaymentRequest($paymentRequestArr);
                                      $api_response = list($response,$statusCode,$httpHeader)=null;
                              try {
                                        $api_response = $api_instance->voidPayment($paymentRequest, $tID);
                                                                	
                            	
                            	
                                                
                                    if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
                					{
                					  $trID =   $api_response[0]['id'];
                					  $msg  =   $api_response[0]['status'];
                					  
                					  $code =   '200';
                					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                					  	$condition = array('transactionID'=>$tID);
                    					$update_data =   array('transaction_user_status'=>"3",'transactionModified'=>date('Y-m-d H:i:s'));
        					            $this->general_model->update_row_data('customer_transaction',$condition, $update_data);
                					    if($chh_mail =='1')
                                        {
                                            $condition = array('transactionID'=>$tID);
                                         
                                            $comp_data     = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$merchantID));
                                            $tr_date   =date('Y-m-d H:i:s');
                                            $ref_number =  $tID;
                                            $toEmail = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];  
                                            $this->general_model->send_mail_voidcapture_data($merchantID,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,'void');
                                        
                                        }
                            				 $this->session->set_flashdata('success','Successfully Cpatured Transaction');
                					    
                					}
                					else
                					{
                					     $trID  =   $api_response[0]['id'];
                					      $msg  =   $api_response[0]['status'];
                					      $code =   $api_response[1];
                					      $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ); 
                					      
                					      $error = $api_response[0]['status'];
                					      $this->session->set_flashdata('success','Payment '.$error);
                					}
                					
            			        	$id = $this->general_model->insert_gateway_transaction_data($res,'void',$gateway,$gt_result['gatewayType'],$customerID,$amount,$merchantID,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);  
                         
                         
                            
                            
                        }
                        	catch(Cybersource\ApiException $e)
        				{
        					
        					  $error = $e->getMessage();
        				print_r($e->getMessage());
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
                    
                  
				
			
					redirect('Payments/payment_capture','refresh');
		   
			}
			else	
			{
				$error ="Transaction ID is required to Captured";
			}
			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error' .$error.'</strong></div>'); 
				 
					redirect('Payments/payment_capture','refresh');
			
	}
	
	
	
     
	public function pay_multi_invoice()
	{
	  
	     $cusproID=''; $error='';
	 if($this->session->userdata('logged_in')){
			
		
		
		$user_id 				= $this->session->userdata('logged_in')['merchID'];
		}
		if($this->session->userdata('user_logged_in')){
	
		$user_id 				= $this->session->userdata('user_logged_in')['merchantID'];
		}
	     $cardID_upd ='';
	 	 $invoices           = $this->czsecurity->xssCleanPostInput('multi_inv');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID1');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway1');		
		 
       $cusproID = $this->czsecurity->xssCleanPostInput('customermultiProcessID');
	  
	  $inv_data=array();

        $invoiceIDs =  implode(',',$invoices); 
		$inv_data   =    $this->customer_model->get_due_invoice_data($invoices,$user_id);
		
			
     
       if(!empty($invoices) &&  !empty( $inv_data) && $cardID!="" && $gateway!="")
       {  
           
            
             $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
              $cusproID     = $customerID  = $inv_data['Customer_ListID'];
			   	$comp_data  = $this->general_model->get_row_data('qb_test_customer', array('ListID' =>$customerID , 'qbmerchantID'=>$user_id));
			   	
			   	
			   		    $crtxnID='';  
                        $flag="true";
                        $error=$user='';
			   	
			   	
		        $companyID  = $comp_data['companyID'];
		
                       
            		  
                            $Customer_ListID = $customerID;
                     
                            if($cardID!="new1")
            			     {
            			        
            			        $card_data=   $this->card_model->get_single_card_data($cardID);
            			        $card_no  = $card_data['CardNo'];
        				        $expmonth =  $card_data['cardMonth'];
            					$exyear   = $card_data['cardYear'];
            					$cvv      = $card_data['CardCVV'];
            					$cardType = $card_data['CardType'];
            					$address1 = $card_data['Billing_Addr1'];
            						$address2 = $card_data['Billing_Addr2'];
            					$city     =  $card_data['Billing_City'];
            					$zipcode  = $card_data['Billing_Zipcode'];
            					$state    = $card_data['Billing_State'];
            					$phone   =  $card_data['Billing_Contact'];;
            					$country  = $card_data['Billing_Country'];
            					
            			     }
            			     else
            			     {
            			          
            			        $card_no  = $this->czsecurity->xssCleanPostInput('card_number');
            					$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
            					$exyear   =$this->czsecurity->xssCleanPostInput('expiry_year');
            					$cardType = $this->general_model->getType($card_no);
            					$cvv ='';
            					if($this->czsecurity->xssCleanPostInput('cvv')!="")
            					$cvv      = $this->czsecurity->xssCleanPostInput('cvv');   
            					       $address1 =  $this->czsecurity->xssCleanPostInput('address1');
            	                	   $address2 = $this->czsecurity->xssCleanPostInput('address2');
            	                    	$city    =$this->czsecurity->xssCleanPostInput('city');
            	                        $country =$this->czsecurity->xssCleanPostInput('country');
            	                        $phone   =  $this->czsecurity->xssCleanPostInput('contact');
            	                        $state   =  $this->czsecurity->xssCleanPostInput('state');
            	                        $zipcode =  $this->czsecurity->xssCleanPostInput('zipcode');
            			         
            			     }   
               	  
            				if(!empty($invoices))
            				{
            				    
                             $amount  = $this->czsecurity->xssCleanPostInput('totalPay');
                             
                               $option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                    
                        $config = $commonElement->ConnectionHost();
                       
                    	$merchantConfig = $commonElement->merchantConfigObject();
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "test_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                        $billtoArr = [
                    		"firstName" => $comp_data['FirstName'],
                    		"lastName" => $comp_data['LastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $phone,
                    		"company" => $comp_data['companyName'],
                    		"email" => $comp_data['Contact']
                    	];
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                    	
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                    	
                    	
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    	
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                             
                        try
                        {
                            //Calling the Api
                            $api_response = $api_instance->createPayment($paymentRequest);
                            
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					  
        					  $code =   '200';
        					  $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );     
                             
                                     
                                $this->session->set_flashdata('success','Successfully Processed Invoice');      
                                     
                                     
                                       foreach($invoices as $invoiceID)
                                      {
                                         
                                          $pay_amounts=$this->czsecurity->xssCleanPostInput('pay_amount'.$invoiceID);
                                           $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
                                    		if(!empty($in_data))
                                    		{
                                    
                                                $txnID      = $in_data['TxnID'];  
                    							 $ispaid 	 = 'true';
                    							 $am         = (-$pay_amounts);
                                						 
                        				         $bamount =  $in_data['BalanceRemaining']-$pay_amounts;
            					              if($bamount >0)
            						 	      $ispaid 	 = 'false';
            						 	          
            						 	        $app_amount = $in_data['AppliedAmount']+(-$pay_amounts);
            						              $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app_amount , 'BalanceRemaining'=>$bamount );
            					            	
                        						 $condition  = array('TxnID'=>$in_data['TxnID'] );
                        						 
                        						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
                        				     	 $user = $in_data['qbwc_username'];
                        				     	 
                        				     	  $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$pay_amounts,$user_id,$crtxnID='', $this->resellerID,$in_data['TxnID'], false, $this->transactionByUser);  
                        				     	  $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT,  $id, '1','', $user);
                                    		}
                                      }		
                              
                                    
                          
                             if($cardID=="new1" && !($this->czsecurity->xssCleanPostInput('tc'))  )
                    				     {
                                         	   $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
												$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
												$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
													$cvv      = $this->czsecurity->xssCleanPostInput('cvv');	
                    				 		$card_type      =$this->general_model->getType($card_no);
                                         
                                         			$card_data = array('cardMonth'   =>$expmonth,
                    										   'cardYear'	 =>$exyear, 
                    										  'CardType'     =>$card_type,
                    										  'CustomerCard' =>$card_no,
                    										  'CardCVV'      =>$cvv, 
                    										 'customerListID' =>$customerID, 
                    										 'companyID'     =>$companyID,
                    										  'merchantID'   => $user_id,
                    										
                    										 'createdAt' 	=> date("Y-m-d H:i:s"),
                    										 'Billing_Addr1'	 =>$address1,
                        									 'Billing_Addr2'	 =>$address2,	 
                        										  'Billing_City'	 =>$city,
                        										  'Billing_State'	 =>$state,
                        										  'Billing_Country'	 =>$country,
                        										  'Billing_Contact'	 =>$phone,
                        										  'Billing_Zipcode'	 =>$zipcode,
                    										 );
                    								
                    				            $id1 =    $this->card_model->process_card($card_data);	
                                         }
            				    	  $this->session->set_flashdata('success','Successfully Processed Invoice'); 
            				 	
            						
                                 }
                                 else
                                 {
                                     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					      
        					      $error = $api_response[0]['status'];
                                  
                                       $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - '.$msg.'</strong></div>'); 
                                     $id = $this->general_model->insert_gateway_transaction_data($res,'sale',$gateway,$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID='', $this->resellerID,$inID='', false, $this->transactionByUser);      
                                 }		
            						
                               
                              
            				     
            				    
                             } 
                          	catch(Cybersource\ApiException $e)
        				{
        				
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        				}
            				   
            			
            				           
            		    }
                		    else        
                            {
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed -  This is not valid invoice! </strong>.</div>');
                                
                            }
                     
		   }
		   else
		    {
		        
		       
		          $error='Validation Error. Please fill the requred fields';
        		  $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
		    }
			   
			
		    	 if($cusproID!="")
				 {
					 redirect('home/view_customer/'.$cusproID,'refresh');
				 }
				 else{
				 redirect('home/invoices','refresh');
				 }
	      
	}
	
	
	
	
	
	
	}  