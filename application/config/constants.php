<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
//logo image
define("CZLOGO", getenv('CZLOGO'));
define("CZHEADERLOGO", getenv('CZHEADERLOGO'));
define("LOGOWIDTH", '300');
define("LOGOHEIGHT", '100');
define("DEFAULT_TIMEZONE", 'America/Los_Angeles');

define("ENCRYPTION_KEY", getenv('ENCRYPTION_KEY_CARD'));
 $logo = 'uploads/merchant_logo/';
define("LOGOURL1", getenv('LOGOURL'));
define("LOGOURL", $logo);
define('CUS_PORTAL', getenv('CUS_PORTAL'));
define('ADMIN_EMAIL','support@chargezoom.com');
define("CURRENCY", 'USD');
define("PLINK", '.com');
define('IMAGES','resources/img/');
define('JS','resources/js/');
define('CSS','resources/css/');
define('UPLOAD','uploads/');
define("MAXRESULT", 100);
define("RELEASE_VERSION", '2.0.1');
define("DEFAULT_FROM_EMAIL", "donotreply@payportal.com");
define("QBO_DISCONNECT_MSG", "Error 401: Authentication cannot be validated, the account is not connected.");
define("DEFAULT_AUTH_AMOUNT", '2');

define('iTransactGatewayName', 'iTransact');
define('FluidGatewayName', 'FluidPay');
define('TSYSGatewayName', 'TSYS');
define('BASYSGatewayName', 'BASYS iQ Pro');
define('PayArcGatewayName', 'PayArc Payment');
define('MaverickGatewayName', 'Maverick Payment');
define('EPXGatewayName', 'EPX');

define('HATCHBUCK_API_KEY', getenv('HATCHBUCK_API_KEY'));
define('HATCHBUCK_API_URL', getenv('HATCHBUCK_API_URL'));
define('HATCHBUCK_FIRST_SETUP_WIZARD', getenv('HATCHBUCK_FIRST_SETUP_WIZARD'));
define('HATCHBUCK_PAYMENT_INFO', getenv('HATCHBUCK_PAYMENT_INFO'));
define('HATCHBUCK_FIRST_TRANSACTION', getenv('HATCHBUCK_FIRST_TRANSACTION'));
define('SEND_GRID_API_KEY', getenv('SEND_GRID_API_KEY'));

define("NMIAUTHRIZEMINAMOUNT",'1.00');
define("STRIPEAUTHRIZEMINAMOUNT",'0.50');
define("FLUIDPAYAUTHRIZEMINAMOUNT",'0.10');
define("TSYSAUTHRIZEMINAMOUNT",'0.10');
define("ITRANSACTAUTHRIZEMINAMOUNT",'2.00');
define("AUTHAUTHRIZEMINAMOUNT",'0.10');
define("PAYTRACEAUTHRIZEMINAMOUNT",'0.50');
define("PAYPALAUTHRIZEMINAMOUNT",'1.00');
define("USAEPAYAUTHRIZEMINAMOUNT",'0.10');
define("HEARTLANDAUTHRIZEMINAMOUNT",'0.10');
define("CYBERSOURCEAUTHRIZEMINAMOUNT",'0.10');

define("LAST_ALL_CUSTOMER_SYNC",'LAST_ALL_CUSTOMER_SYNC');
define("LAST_ALL_INVOICE_SYNC",'LAST_ALL_INVOICE_SYNC');
define("LAST_ALL_ITEM_SYNC",'LAST_ALL_ITEM_SYNC');
define("LAST_ALL_ACCOUNT_SYNC",'LAST_ALL_ACCOUNT_SYNC');

define('COMMON_PLAN_CHECKOUT_METHOD', 'common_checkout');
define('MAVERICKGATEWAYID', 17);
define("EPXAUTHRIZEMINAMOUNT",'0.10');

define("SEND_GRID_CZ_WEBHOOK_PARSE_URL", 'https://payportal.com/Common_controllers/sendGrid/parseWebhookData');
define("SEND_GRID_HPS_WEBHOOK_PARSE_URL", 'https://heartlandpayportal.com/Common_controllers/sendGrid/parseWebhookData');

define("FRESHBOOK_DISCONNECT_URL", 'https://api.freshbooks.com/auth/oauth/revoke');
define("CARD_POINT_URL", 'https://fts-uat.cardconnect.com/cardconnect/rest');
define("FIRST_LOGIN_SUPPORT_URL", 'https://www.chargezoom.com/merchantsetup');

/* End of file constants.php */
/* Location: ./application/config/constants.php */

