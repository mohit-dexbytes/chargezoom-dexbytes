<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

$mode = '';
$CI   = &get_instance();

if ($CI->session->userdata('logged_in') != "") {

    if(isset($CI->session->userdata('logged_in')['gatewayMode']))
    {
        $mode = $CI->session->userdata('logged_in')['gatewayMode'];
    }
    

}
if ($CI->session->userdata('user_logged_in') != "") {

    if(isset($CI->session->userdata('user_logged_in')['gatewayMode'])){
        $mode = $CI->session->userdata('user_logged_in')['gatewayMode'];
    }
}
if($mode === '') {
    switch(getenv('ENV')){
        case 'development':
        case 'staging':
        case 'local':
            $mode = 0;
        break;
        case 'production':
            $mode = 1;
        break;
        default:
            $mode = 1;
        break;
    }
}

if ($mode == 0) {
  $config['environment'] = 'sandbox';
} else {
  $config['environment'] = 'production';
}
