<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting TSYS sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */

$mode = 1;
$CI   = &get_instance();
if ($CI->session->userdata('logged_in') != "") {
    $mode = $CI->session->userdata('logged_in')['gatewayMode'];

}
if ($CI->session->userdata('user_logged_in') != "") {

    $mode = $CI->session->userdata('user_logged_in')['gatewayMode'];

}

if ($mode == 0) {
  $config['environment'] = 'sandbox';
} else {
  $config['environment'] = 'production';
}
