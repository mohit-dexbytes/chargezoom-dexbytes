<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting Cybersource's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
require_once dirname(__FILE__) . '../../../vendor/autoload.php';

require_once dirname(__FILE__) . '../../../cybersource-rest-samples-php/Resources/ExternalConfiguration.php';
$config['Sandbox'] = false;

$mode = 1;
$CI   = &get_instance();
if ($CI->session->userdata('logged_in') != "") {
    if (isset($CI->session->userdata('logged_in')['gatewayMode'])) {
        $mode = $CI->session->userdata('logged_in')['gatewayMode'];
    } else {
        $mode = 0;
    }
}

if ($CI->session->userdata('user_logged_in') != "") {
    if (isset($CI->session->userdata('user_logged_in')['gatewayMode'])) {
        $mode = $CI->session->userdata('user_logged_in')['gatewayMode'];
    } else {
        $mode = 0;
    }
}

$config['mode'] = $mode;
if ($mode == 0) {
    $config['sandbox'] = true;
    $config['Sandbox'] = true;
} else {
    $config['sandbox'] = false;
    $config['Sandbox'] = false;
}

$config['SandboxENV']    = "cyberSource.environment.SANDBOX";
$config['ProductionENV'] = "cyberSource.environment.PRODUCTION";
