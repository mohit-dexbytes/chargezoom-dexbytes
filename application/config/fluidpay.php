<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting FluidPay's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */

$mode = 1;
$CI   = &get_instance();
if ($CI->session->userdata('logged_in') != "") {
    if(isset($CI->session->userdata('logged_in')['gatewayMode'])){
      $mode = $CI->session->userdata('logged_in')['gatewayMode'];
    } else {
      $mode = 0;
    }
    

}
if ($CI->session->userdata('user_logged_in') != "") {
  if(isset($CI->session->userdata('user_logged_in')['gatewayMode'])){
    $mode = $CI->session->userdata('user_logged_in')['gatewayMode'];
  } else {
    $mode = 0;
  }
    

}

if ($mode == 0) {
  $config['environment'] = 'sandbox';
} else {
  $config['environment'] = 'production';
}
