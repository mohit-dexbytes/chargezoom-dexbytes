<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once dirname(__FILE__) . '/../../vendor/quickbooks/v3-php-sdk/src/config.php';

$config['client_id']        = getenv('QBO_CIENT_ID');
$config['client_secret']    = getenv('QBO_CIENT_SECRET');

$config['authorizationRequestUrl']   = 'https://appcenter.intuit.com/connect/oauth2';
$config['tokenEndPointUrl']          = 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer';
$config['oauth_scope']               = 'com.intuit.quickbooks.accounting';
$config['openID_scope']              = 'openid profile email';
$config['oauth_redirect_uri']        = getenv('QBO_OAUTH_REDIRECT_URI');
$config['mainPage']                  = getenv('QBO_MAIN_PAGE');
$config['refreshTokenURL']           = getenv('QBO_REFRESH_TOKEN_URL');

// Production
$config['QBOURL'] = getenv('QBO_END_POINT_URL');
// Development
$config['AuthMode'] = 'oauth2';