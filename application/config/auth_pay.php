<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting PayPal's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */

if(ENVIRONMENT == 'production'){
    $config['Sandbox'] = FALSE;
    $config['auth_test_mode'] = FALSE;
}else{
    $config['Sandbox'] = TRUE;
    $config['auth_test_mode'] = TRUE;
}