FROM ubuntu:18.04

LABEL maintainer="Ankush Kumar <ankush.dexbytes@gmail.com>" version="0.1"

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update
RUN apt-get upgrade -y

RUN apt-get install -y locales locales-all
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN apt-get install -y software-properties-common \
    && apt-add-repository ppa:ondrej/php \
    && apt-get update -y --fix-missing

RUN apt-get install -y --allow-unauthenticated php7.3 \
    php7.3-bcmath php7.3-bz2 php7.3-cgi \
    php7.3-cli php7.3-common php7.3-curl php7.3-dba php7.3-dev \
    php7.3-enchant php7.3-fpm php7.3-gd php7.3-gmp php7.3-imap \
    php7.3-interbase php7.3-intl php7.3-json php7.3-ldap \
    php7.3-mbstring php7.3-mysql php7.3-odbc \
    php7.3-opcache php7.3-pgsql php7.3-phpdbg php7.3-pspell \
    php7.3-readline php7.3-recode php7.3-snmp php7.3-soap \
    php7.3-sqlite3 php7.3-sybase php7.3-tidy php7.3-xml \
    php7.3-xmlrpc php7.3-xsl php7.3-zip php-xdebug

RUN apt-get -y install php-xdebug php-imagick ffmpeg \
    libpng-dev libicu-dev libcurl3-dev libmagickwand-dev imagemagick

RUN apt-get install apache2 libapache2-mod-php7.3 -y --allow-unauthenticated
RUN apt-get install php-mbstring curl -y --allow-unauthenticated

RUN a2enmod rewrite \
    && a2enmod headers \
    && a2enmod ssl

RUN curl -sS https://getcomposer.org/installer | php \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer


RUN mkdir -p /var/www/chargezoom
ADD /install/docker/apache/ci.local.conf /etc/apache2/sites-available/ci.local.conf

ADD /install/certs /etc/ssl/certs

RUN a2ensite ci.local.conf
RUN a2dissite 000-default.conf
RUN echo "ServerName localhost" | tee /etc/apache2/conf-available/fqdn.conf
RUN a2enconf fqdn

ADD /install/docker/scripts/wait-for-it.sh /wait-for-it.sh
RUN chmod +x /wait-for-it.sh

ADD /install/docker/scripts/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

EXPOSE 80 443

RUN service apache2 restart

USER root
