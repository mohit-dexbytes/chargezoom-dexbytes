<?php
return array(
  'authorizationRequestUrl' => 'https://appcenter.intuit.com/connect/oauth2', //Example https://appcenter.intuit.com/connect/oauth2',
  'tokenEndPointUrl' => 'https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer', //Example https://oauth.platform.intuit.com/oauth2/v1/tokens/bearer',
  'client_id' => 'Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5', //Example 'Q0wDe6WVZMzyu1SnNPAdaAgeOAWNidnVRHWYEUyvXVbmZDRUfQ',
  'client_secret' => '3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA', //Example 'R9IttrvneexLcUZbj3bqpmtsu5uD9p7UxNMorpGd',
  'oauth_scope' => 'com.intuit.quickbooks.accounting', //Example 'com.intuit.quickbooks.accounting',
  'openID_scope' => 'openid profile email', //Example 'openid profile email',
  'oauth_redirect_uri' => 'https://payportal.net/Quickbooks_online/OAuth_2/OAuth2PHPExample.php', //Example https://d1eec721.ngrok.io/OAuth_2/OAuth2PHPExample.php',
  'openID_redirect_uri' => 'https://payportal.net/Quickbooks_online/OAuth_2/OAuthOpenIDExample.php',//Example 'https://d1eec721.ngrok.io/OAuth_2/OAuthOpenIDExample.php',
  'mainPage' => 'https://payportal.net/Quickbooks_online/OAuth_2/index.php', //Example https://d1eec721.ngrok.io/OAuth_2/index.php',
  'refreshTokenPage' => 'https://payportal.net/Quickbooks_online/OAuth_2/RefreshToken.php', //Example https://d1eec721.ngrok.io/OAuth_2/RefreshToken.php'
)
 ?>
