#!/bin/bash
mkdir -p chown -R www-data:www-data /var/www/chargezoom

cd /var/www/chargezoom \
    && composer install \

apachectl -D FOREGROUND