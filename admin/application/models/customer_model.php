<?php


Class Customer_model extends CI_Model
{
	private $tbl_user = 'qb_test_customer'; // user table name 
	function Customer()
	{
		parent::Model();
	}

	
	
		
    function get_customers($merchantID) 
	{
		 $sql = ' SELECT cust.ListID as ListID, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, cust.FullName as customerFullName, cust.Contact as customerEmail, cust.Phone as customerPhone, cust.TimeCreated as customerTime, comp.* , (select sum(BalanceRemaining) from qb_test_invoice where Customer_ListID = cust.ListID) as Payment from qb_test_customer cust  inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'" and customerStatus="1" ';
		 

		$query = $this->db->query($sql);
		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}


	
	function get_customers_data($merchantID) 
	{
		 $sql = ' SELECT cust.* from qb_test_customer  cust inner join tbl_company on tbl_company.id=cust.companyID   where tbl_company.merchantID = "'.$merchantID.'" and cust.customerStatus="1" ';
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}

	
	function get_customer_details($id){
      $data  =array();
	  
	 $customer_pro =   "call get_customer_details(?)";
	  
     $condition = array('usercutomerID'=>$id);
     $query = $this->db->query($customer_pro, $condition);
     $data =  $query->row();
    return $data;
	}
	
	
	function customer_by_id($id) 
	{
         $this->db->select('cus.*,cmp.qbwc_username ');
		
		$this->db->from('qb_test_customer cus');
		$this->db->join('tbl_company cmp','cmp.id = cus.companyID','INNER');
		$this->db->where('cus.ListID', $id);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 

		else

		return false; 
	}
	
	
	
	public function get_customer_invoice_data($custID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.*, 
	 ( case   when  IsPaid ='true' and userStatus =''    then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''   then 'Past Due'
	   
	  else 'Cancel' end ) as status FROM qb_test_invoice inv
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  WHERE `inv`.`Customer_ListID` = '$custID' ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
		
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, DATE_FORMAT(inv.DueDate, '%Y-%m-%d'), inv.IsPaid, `inv`.RefNumber,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, `inv`.DueDate,   cust.*,
	  (case 
	  
	     when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
		 when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  and userStatus =''  then 'Past Due'
	  
	     when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > '$today' AND `IsPaid` = 'false' and userStatus ='' 
		 and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	     when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > '$today' and IsPaid ='true'  and userStatus ='' then 'Paid'
	  
	  else 'Cancel' end ) as status 
	  FROM qb_test_invoice inv 
	 INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE   IsPaid ='false'  and userStatus =''  and  
	  
	  `inv`.`Customer_ListID` = '$userID'    order by   DueDate  asc  ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	
	
	
	public function get_invoice_latest_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	 
	  $query  =  $this->db->query("SELECT `inv`.TxnID, `inv`.RefNumber,(-`inv`.AppliedAmount) as AppliedAmount, `inv`.DueDate, `inv`.BalanceRemaining, cust.*,
	 (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  then 'Past Due'
	   when IsPaid ='true'  then 'Success'
	  else 'Cancel' end ) as status,
	  	 (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  then DATE_FORMAT(inv.DueDate, '%b %d, %Y')
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then DATE_FORMAT(inv.DueDate, '%b %d, %Y')
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  then DATE_FORMAT(inv.DueDate, '%b %d, %Y')
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  then DATE_FORMAT(inv.TimeModified,'%b %d, %Y %l:%i %p')
	  else 'Cancel' end ) as TimeModifiedinv
	 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE    (IsPaid ='true' or AppliedAmount!='0.00')   and  
	  
	  `inv`.`Customer_ListID` = '$userID'  order by   DATE_FORMAT(inv.TimeModified, '%b %d, %Y %l:%i %p')  desc ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	public function get_customer_invoice_data_sum($custID){
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,  
		  sum(inv.BalanceRemaining-inv.AppliedAmount) as amount   FROM qb_test_invoice inv  WHERE `inv`.`Customer_ListID` = '".$custID."'   ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
	public function get_customer_invoice_data_payment($custID){
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  sum(-AppliedAmount) as applied_amount,  
		  ifnull( (select sum(BalanceRemaining) from qb_test_invoice where `Customer_ListID` = '".$custID."' and BalanceRemaining!='0.00'  and AppliedAmount!='0.00'  ),'')as applied_due, 
ifnull( (select sum(BalanceRemaining) from qb_test_invoice where `Customer_ListID` = '".$custID."'  and  DATE_FORMAT(DueDate,'%Y-%m-%d') < '".$today."'),'')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from qb_test_invoice where `Customer_ListID` = '".$custID."' AND   IsPaid ='false'  and userStatus ='' and  DATE_FORMAT(DueDate,'%Y-%m-%d') >= '".$today."' ),'') as upcoming_balance FROM qb_test_invoice   WHERE `Customer_ListID` = '".$custID."'  ");
		 $res=$query->row();
		 return  $res;
		
	}	
	
	
	public function get_customer_invoice_data_count($custID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as incount  FROM qb_test_invoice   WHERE Customer_ListID = '".$custID."' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->incount;
		}
		return false;
	     
	}	
	
	
	
	
	public function get_customer_note_data($custID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `qb_test_customer` cust ON `pr`.`customerID` = `cust`.`ListID` WHERE `pr`.`customerID` = '".$custID."' order by pr.noteID desc");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
    
	public function get_transaction_data($userID){
		$today = date("Y-m-d H:i");
			$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $userID);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
		
    
	public function get_invoice_transaction_data($invoiceID, $userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $userID);
		 $this->db->where("tr.invoiceTxnID ", $invoiceID);
		   $this->db->where('cust.customerStatus', '1');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	
	 
	public function get_transaction_data_captue($userID){
		
		
		$query =$this->db->query("Select tr.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID )
		as RefNumber  ,cust.* from tbl_customer_tansaction tr inner join 
		qb_test_customer cust on  tr.customerListID = cust.ListID inner join tbl_company on cust.companyID= tbl_company.id 
		where tbl_company.merchantID='".$userID."'  and  (tr.transactionType ='auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		and tr.transaction_user_status !='success' and cust.customerStatus='1' ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	


	public function get_transaction_data_refund($userID){
		$today = date("Y-m-d H:i");
		
		$query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber , cust.* from tbl_customer_tansaction tr 
		inner join  qb_test_customer cust on  tr.customerListID = cust.ListID 
		inner join tbl_company on cust.companyID= tbl_company.id 
		where tbl_company.merchantID='".$userID."'
		and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and tr.transaction_user_status !='refund'  and tr.transactionID!=''  and cust.customerStatus='1'  ");

		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	
		
	
	public function get_invoice_data_template($con){

	$res =array();
 	$today  = date('Y-m-d');
	 
	 
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.RefNumber,`inv`.DueDate,  (-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*, 
	  ifnull((select transactionType from tbl_customer_tansaction where invoiceTxnID =inv.TxnID order by id desc limit 1),'' ) as  paymentType, 
	  ifnull((select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID  order by id desc limit 1),'') as tr_status,
	    ifnull((select transactionDate from tbl_customer_tansaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_date,
		ifnull((select transactionAmount from tbl_customer_tansaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_amount,
	    ifnull((select transactionStatus from tbl_customer_tansaction where invoiceTxnID =inv.TxnID order by id desc limit 1 ),'') as tr_data,
		
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE  inv.Customer_ListID !='' $con   and cust.customerStatus='1'  limit 1 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->row_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_customer_transaction_data($userID){
		
		$this->db->select('tr.*, cust.* ,(select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber' );
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("cust.ListID ", $userID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	public function get_invoice_customer_transaction_data($invoiceID, $userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("cust.ListID", $userID);
		 $this->db->where("tr.invoiceTxnID ", $invoiceID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	
	public function get_invoice_data_byID($invoiceID){
		$res =array();
		$this->db->select('inv.*, cust.*,comp.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("inv.TxnID", $invoiceID);
	
		
		
		  $query = $this->db->get();
		   $res= $query->row_array();
		  return $res;
		
	}
	
	

	 
	public function get_subscription_data($userID){
	
			$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("comp.merchantID ", $userID);
		  $this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
		
	public function get_refund_transaction_data($userID){
		

	$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`tbl_customer_tansaction` tr) 
	INNER JOIN `qb_test_customer` cust ON `tr`.`customerListID` = `cust`.`ListID`
	INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID` WHERE `cust`.`customerStatus` = '1' AND `comp`.`merchantID` = '$userID'
	AND (`tr`.`transactionType` = 'refund' OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;
	
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 	
	

public function get_merchant_gateway($merchantID){
		$res =array();
		$query = $this->db->query("Select gt.* from tbl_merchant_gateway gt inner join tbl_merchant_data mr on mr.merchID=gt.merchantID  where gt.merchantID ='$merchantID'  ");
		return  $res= $query->result_array(); 
		
		
		
	}	
	
	
	
public function get_admin_gateway(){
		$sql = " SELECT * from tbl_admin_gateway";
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
		
		
		
	}	
	
	function get_merchant_data() 
	{
		 $sql = " Select mr.* from tbl_merchant_data mr left join tbl_reseller tr on mr.resellerID=tr.resellerID where tr.isDelete = '0' and mr.isDelete='0'";
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}
	

	
 public function get_single_merchant_data($mID){

	$data =array();
 	
	  $query  = $this->db->query("select mr.*, ifnull(cy.country_name,'') as country_name, ifnull(st.state_name,'') as state_name, ifnull(ct.city_name,'') as city_name from tbl_merchant_data mr left join country cy on mr.merchantCountry=cy.country_id left join state st on mr.merchantState=st.state_id left join city ct on mr.merchantCity=ct.city_id where merchID= $mID ");
	  
		if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}	
	} 		 
	

}






