<?php
Class General_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
	
	function insert_row($table_name, $array, $skipKeys=['message'])
	{
		$dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		} 

		if($this->db->insert($table_name,$dataToInsert))

		return $this->db->insert_id();
		else
		return false;

	}
	
	  function insert_batch_rows($table_name, $array)
	{

		if($this->db->insert_batch($table_name,$array))

		return true;
		else
		return false;

	} 
	
	function update_row($table_name,$reg_no,$array)
	{
		$this->db->where('reg_no', $reg_no);
		if($this->db->update($table_name, $array))
			return true;
		else
			return false;
	}
	
	public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
		if($array !='')
        $this->db->where($array);
		
        $query = $this->db->get();
		if($query->num_rows() > 0)
			return true;
        
        else 
          return false; 
	}
	
	
	function update_row_data($table_name,$condition, $array, $skipKeys=['message'])
	{
		$dataToUpdate = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

		$this->db->where($condition);
		$trt = $this->db->update($table_name, $dataToUpdate); 
		if($trt)
			return true;
		else
			return false;
	}
	
	   public function get_row_data($table, $con) 
	   {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}

  
	  public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	  
	
	  public function get_table_data($table, $con) {
	    $data=array();
	 
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
	  
	  
	  
	  ///---------  to get the email history data --------///
	
	   public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	 //---------  to get the customer subscriptions --------///
	
	 
	public function get_subscriptions_data($condition){
	
			$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);
		
		$this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	  
	  
	  
	  
	  public function get_num_rows($table, $con) {
			
			$num = 0;	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			$num = $query->num_rows();
			return $num;
		
	}
	
	
	public function delete_row_data($table, $condition){
	   
     	    $this->db->where($condition);
             $this->db->delete($table);
            if($this->db->affected_rows()>0){
			  return true;
			}else{
			  return false;
			}
	
	}
	
      
//////// get data from tbl_auth//////


 public function get_admin_auth_data($userID) {
            
        $auth=array();
              $this->db->select('Group_Concat(au.authName SEPARATOR ",") as authName',FALSE);
              $this->db->select('Group_Concat(au.authValue SEPARATOR ",") as authValue',FALSE);
              $this->db->from('tbl_admin_role r');
              $this->db->join('tbl_admin_auth au','au.authValue=r.authID','inner');
              $this->db->join('tbl_admin ad','ad.adminID=r.adminUserID','inner');
              $this->db->where('r.adminUserID',$userID);
              $this->db->group_by('r.adminUserID');
			 
			   
			   $query  = $this->db->get();
			   if($query->num_rows()>0 ) {
				$datas = $query->row_array();
			
				
						   $auth = $datas; 
			
				
				  return $auth;
			   } else {
				return $auth;
					}
   }
		   
		   public function get_auth_data($auth_con) {
             $auth=array();
 

			 
			   $query  = $this->db->query("Select authName from tbl_admin_auth where authID in($auth_con)");
			   if($query->num_rows()>0 ) {
				$datas = $query->result_array();
				foreach($datas as $key=>$data){
				
						   $auth[] = $data['authName']; 
				}
				
				  return $auth;
			   } else {
				return $auth;
					}
   }
	  
	  

	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*,sum(cr.creditAmount) as balance, cust.companyName, cust.FullName');
		$this->db->from('tbl_credits cr');
		$this->db->join('qb_test_customer cust','cr.creditName = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where($con);
		$this->db->where('cust.customerStatus', '1');
		$this->db->group_by('cr.creditName');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
	
      
	  
	 
    public function get_merchant_user_data($userID, $muID='')
    {
		if($muID != "") {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantUserID = '".$muID."' and  tmu.merchantID = '".$userID."' ");	
   			$res = $query->row_array();
			
		} else {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantID = '".$userID."' ");
			$res = $query->result_array();
		} 
		return $res;
	}
	
      	public function track_user($user_type, $user_email, $loginID){
	               $this->load->library('user_agent');
                  $log['browser'] = $this->agent->browser();
                     $ip =   getClientIpAddr();
                    
                 $ch = curl_init(); 

                    // set url 
                    curl_setopt($ch, CURLOPT_URL, "http://ip-api.com/json/$ip"); 
            
                    //return the transfer as a string 
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            
                    // $output contains the output string 
                    $output = curl_exec($ch);
            
                    // close curl resource to free up system resources 
            		curl_close($ch); 
                 
                 
                 
                    
                      $user_data = json_decode($output, TRUE);
                      
                   $log_data['ip_address'] = $ip;
                   $log_data['City']      = $user_data['city'];
                   $log_data['State']     = $user_data['regionName'];
                   $log_data['Country']   = $user_data['country'];
                   $log_data['ISP']   = $user_data['isp'];
                   $log_data['browser']   = $log['browser'];
                    $log_data['UserType'] = $user_type;
                    $log_data['emailAddress']  =$user_email; 
                  $log_data['UserID'] = $loginID;
                       $log_data['Login_at'] = date('Y-m-d H:i:s'); 
                     $result =  $this->general_model->insert_row('tbl_security_log', $log_data);
                  
				
				
	}				
	
      
      
      	public function get_track_data(){
	                
		        	$this->db->select('*');
		        	$this->db->where('UserType', 'Reseller');
            		$this->db->Or_Where('UserType', 'Agent');
            	    $query = $this->db->get('tbl_security_log');
            		if($query->num_rows() > 0){
            		
            		  return $query->result_array();
            		}
		
        	} 
      public function get_qb_data()
      {
          $res=array();
          
		$this->db->select('m.merchID,c.*,m.companyName');
		$this->db->from('tbl_merchant_data m');
	
		$this->db->join('tbl_company c','c.merchantID = m.merchID','INNER');
			$this->db->join('app_integration_setting ap','ap.merchantID = m.merchID','INNER');
	   $this->db->where('ap.appIntegration', '2');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
          
          
         
          
          
      }
      
      
      	public function get_invoice_overview($invID)
        {
        $res=array();
        
        $this->db->select('count(merchantID) as merchant, pl.plan_name ');
        $this->db->from('tbl_reseller_invoice_item r');
        $this->db->join('plans pl','pl.plan_id=r.planID','inner');
        $this->db->where('r.resellerInvoice',$invID);
        $this->db->group_by('r.planID');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $res = $query->result_array();
        
        }
        
        	return $res;	
        
        
        
        }
      	
        	
   public  function getType($CCNumber)
 {
            
            
$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    );     

            $CCNumber= trim($CCNumber);
            $type='Unknown';
            foreach ($creditcardTypes as $card){
                if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                    continue;
                }
                $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
                if(preg_match($prefixes,$CCNumber) == 1 ){
                    $type= $card['Name'];
                    break;
                }
            }
            return $type;
        }
    	
      	 	
    public  function getcardType($CCNumber)
    {
           
		$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    	);     

        $CCNumber= trim($CCNumber);
        $type='Unknown';
        foreach ($creditcardTypes as $card){
            if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                continue;
            }
            $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
            if(preg_match($prefixes,$CCNumber) == 1 ){
                $type= $card['Name'];
                break;
            }
        }
      	return $type;
    } 

    public function get_merchant_invoice_overview($invID)
    {
        $res=array();
        
        $this->db->select('count(merchantID) as merchant, pl.plan_name ');
        $this->db->from('tbl_merchant_invoice_item r');
        $this->db->join('plans pl','pl.plan_id=r.planID','inner');
        $this->db->where('r.merchantInvoiceID',$invID);
        $this->db->group_by('r.planID');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $res = $query->result_array();
        
        }
        
        	return $res;	
        
        
        
    } 	
    public function get_table_select_data($table, $clm, $con)
	{

			 $data=array();	
			
			 $columns = implode(',',$clm);
			
			$query = $this->db->select($columns)->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}			
	}
	function plan_by_id($planid)
  	{

    	$query = $this->db->query("SELECT * FROM `plans` pl  LEFT JOIN `plan_friendlyname` fpl ON pl.plan_id = fpl.plan_id  WHERE pl.plan_id = '" . $planid . "' ");
    	if ($query->num_rows() > 0) {
      		$res = $query->row_array();
      		return $res;
    	} else {
      		return false;
    	}
  	}
  	public function getMerchantInvoice($invID)
    {
        $res=array();
        
        $this->db->select('*');
        $this->db->from('tbl_merchant_billing_invoice');
        $this->db->where('invoice',$invID);
        $this->db->where_in('invoiceStatus',[1,3]);
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $res = $query->row_array();
        
        }
        
       	return $res;	  
    } 	
  	
  	public function get_reseller_friendly_plan_name($planid, $rsID)
    {
	    $res = array();
	    $ress = array();
	    $today  = date('Y-m-d');
	    

	    $query  = $this->db->select('pl.*')->from('plans pl')->where('pl.plan_id', $planid)->get();


	    if ($query->num_rows() > 0) {
	      $r = $query->row_array();

	      
	        $q1 = $this->db->select('pf.friendlyname')->from('plan_friendlyname pf ')->where('pf.reseller_id', $rsID)->where('pf.plan_id', $r['plan_id'])->get();
	        if ($q1->num_rows() > 0) {
	          $fname = $q1->row_array()['friendlyname'];
	        } else {
	          $fname  = $r['plan_name'];
	        }

	        $r['friendlyname'] = $fname;

	        $res = $r;
	      
	    }
	    return  $res;
	  }
	  
	/**
	 *  Login Throttling
	 */
	public function addLoginAttempt($ip, $post_data=null)
	{
		$base64_data = ($post_data) ? base64_encode(json_encode($post_data)) : null;
		$data = ['ip' => $ip, 'base64_data' => $base64_data, 'created_at' => date('Y-m-d H:i:s')];
		$this->db->insert('tbl_login_attempts', $data);
	}

	public function getLastLoginAttemptCount($ip, $seconds=300)
	{
		
		$this->db->select('count(id) as c');
		$this->db->from('tbl_login_attempts');
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-$seconds) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->group_by('ip');

		$data = $this->db->get()->row_array();
		
		return ($data ? $data['c'] : 0);
	}

	public function deleteLoginAttempt($ip)
	{
		// Deleting past an hour 
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-3600) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->delete('tbl_login_attempts');
	}
	/**
	 * 
	 * End Login Throttling
	 */

	  // Add Payment Action Log
	public function addPaymentLog($gateway_id, $access_url=null, $request=null, $response=null, $status=null)
	{
		
		$data = [];
		$data['payment_gateway_id'] = $gateway_id;
		$data['access_url'] = $access_url;
		$data['access_block'] = 'admin';
		$data['request_payload'] = (is_array($request) ? json_encode($request): $request) ;
		$data['response_payload'] = (is_array($response) ? json_encode($response): $response) ;
		$this->db->insert('tbl_payment_logs', $data);
  	}

	public function logData(){
		$logType = $_POST['logType'];
		$column1 = array('emailAddress','ip_address','City','ISP','browser','Login_at');
		$i   = 0;
        $con = '';
        foreach ($column1 as $item) {
            if ($_POST['search']['value']) {
                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                }
            }
            $column[$i] = $item;
            $i++;
        }

		$this->db->select('*');
		$this->db->from('tbl_security_log');
		$this->db->where('UserType', $logType);
		if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
			$con = "AND $con";
        }
        $postLength = getTableRowLengthValue($_POST['length']);
        $startRow = getTableRowStartValue($_POST['start']);
		if($postLength != -1)
        	$this->db->limit($postLength, $startRow);

		if(!empty($_POST['order'])){
			$sortData = $_POST['order'];
			$colIndex = getTableRowOrderColumnValue($_POST['order'][0]['column']);
			$columnName = $column1[$colIndex];
			$sortingType = getTableRowOrderByValue($_POST['order'][0]['dir']);
			$this->db->order_by("$columnName", "$sortingType");
		}

		$query = $this->db->get();
		if($query->num_rows() > 0){
			$countQuery  = $this->db->query("Select count(1) as total_data from tbl_security_log WHERE UserType = '$logType' $con");
			$countData = $countQuery->row_array();
			$data = array('num' => $countData['total_data'], 'result' => $query->result_array());
		}else{
			$data = array('num' => '0', 'result' => []);
		}
		return 	$data; 
	}
	public function datetimeconvqbo($datetime, $from, $to)
    {
    	try {
        	if ($from['localeFormat'] != 'Y-m-d H:i:s') {
            	$datetime = DateTime::createFromFormat($from['localeFormat'], $datetime)->format('Y-m-d H:i:s');
        	}
	        $datetime = new DateTime($datetime, new DateTimeZone($from['olsonZone']));
	        $datetime->setTimeZone(new DateTimeZone($to['olsonZone']));
	        return $datetime->format('Y-m-d H:i:s');
    	} catch (\Exception $e) {
        	return null;
    	}
 	}
 	public function table_truncate($table_name)
 	{
 		if($table_name){
 			$this->db->truncate($table_name);
 		}
 	}

	public function getRevenueDataforAdminReports($month)
	{
		$res=array();
		$this->db->select('tmii.*');
		$this->db->from('tbl_merchant_billing_invoice tmi');
		$this->db->join('tbl_merchant_invoice_item tmii', 'tmi.invoice = tmii.merchantInvoiceID');
		$this->db->where('tmi.createdAt like "'.date("Y-m", strtotime($month)).'%"');
		$this->db->order_by('mrInvID', 'asc');
		$res = $this->db->get()->result_array();
		return $res;	  
	}

	public function getDisabledMerchantPlanData($month){
		$res = array();
		$this->db->select('sum(p.subscriptionRetail) as total_amount');
		$this->db->from('tbl_merchant_data mr');
		$this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
		$this->db->join('plans p', 'mr.plan_id = p.plan_id', 'inner');
		$this->db->where('(mr.isEnable = 0 AND  mr.isDelete = 0)');
		$this->db->where('rs.isTestmode', 0);
		$this->db->where('mr.updatedAt like "'.date("Y-m", strtotime($month)).'%"');
		$query = $this->db->get();
		$res = $query->row_array();
		return $res;
	}
}

 