<?php

class Card_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	
		$this->db1 = $this->load->database('otherdb', TRUE);
		
	}
	

   
     public function update_card_data($condition, $card_data=array())
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->update('reseller_card_data',$card_data);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('CardID');
            $this->db1->from('reseller_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['CardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	 }	 
	 
	 
     public function delete_card_data($condition)
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->delete('reseller_card_data');
		 if($updt)
		 {
            $status =true;
		 }	 
		 return $status;

	 }	 
	 
	  public function insert_card_data($card_data)
	 {  
	     $status=false;

		 $this->db1->insert('reseller_card_data',$card_data);

		
		 return $status = $this->db1->insert_id();
		 

	 }	 
	 

	
function sign($message, $key) {

    return hash_hmac('sha256', $message, $key) . $message;
}

function verify($bundle, $key) {
    return hash_equals(
      hash_hmac('sha256', mb_substr($bundle, 64, null, '8bit'), $key),
      mb_substr($bundle, 0, 64, '8bit')
    );
}

function getKey($password, $keysize = 40) {
    return hash_pbkdf2('sha256',$password,'some_token',100000,$keysize,true);
}

function encrypt($message) {
     
    if (empty($message)) {
        return null;
    }

    $password = ENCRYPTION_KEY;
    $iv       = random_bytes(16);
    $key      = $this->getKey($password);
    $result   = $this->sign(openssl_encrypt($message, 'aes-256-ctr', $key, OPENSSL_RAW_DATA, $iv), $key);
    return bin2hex($iv) . bin2hex($result);
}

function decrypt($hash) {


	if (empty($hash)) {
        return null;
    }

    $password = ENCRYPTION_KEY;
    $iv       = hex2bin(substr($hash, 0, 32));
    $data     = hex2bin(substr($hash, 32));
    $key      = $this->getKey($password);
    if (!$this->verify($data, $key)) {
        return null;
    }
    return openssl_decrypt(mb_substr($data, 64, null, '8bit'), 'aes-256-ctr', $key, OPENSSL_RAW_DATA, $iv);
}

	 
 public function get_card_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db1->select('*')->from($table)->where($con)->get();

			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
				if(	$data['CustomerCard']!="")
				$data['CustomerCard'] = $this->decrypt($data['CustomerCard']);
					if(	$data['CardCVV']!="")
				$data['CardCVV'] = $this->decrypt($data['CardCVV']);
			if(	$data['accountNumber']!="")
			$data['accountNumber'] = $this->decrypt($data['accountNumber']);
					if(	$data['routeNumber']!="")
			$data['routeNumber'] = $this->decrypt($data['routeNumber']);
				
			   return $data;
			} else {
				return $data;
			}				
	
	} 
	 
      
      public function get_reseller_single_card_data($cardResellerID)
  {  
  
                  $card = array();
   
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from  reseller_card_data c 
		     	where  c.cardResellerID='$cardResellerID'    "; 
				    $query1 = $this->db1->query($sql);
				    
				    
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						if($card_data['CustomerCard']!="")
						 $card['CardNo']     = $this->decrypt($card_data['CustomerCard']) ;
						 else
						  $card['CardNo']     = '';
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  if($card_data['CardCVV']!="")
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  else
						    $card['CardCVV']     = '';
						  $card['friendlyName']  = $card_data['friendlyName'] ;
						   $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
                          $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
                          $card['Billing_City']	     = $card_data['Billing_City'];
                          $card['Billing_State']	 = $card_data['Billing_State'];
                          $card['Billing_Country']	 = $card_data['Billing_Country'];
                          $card['Billing_Contact']	 = $card_data['Billing_Contact'];
                          $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];
                           $card['CardType']	     = $card_data['CardType'];
                           
                           $card['accountNumber']	 = $card_data['accountNumber'];	
                          $card['routeNumber'] 	 = $card_data['routeNumber'];
                        
                          $card['accountName']	 = $card_data['accountName'];
                          $card['secCodeEntryMethod']	 = $card_data['secCodeEntryMethod'];
                          $card['accountType']	         = $card_data['accountType'];
                          $card['accountHolderType']	 = $card_data['accountHolderType'];
                           
                           
				}		
					
					return  $card;

       }
	   
	   
	   public function get_reseller_card_expiry_data($resellerID){  
  
                       $card = array();
			     
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from reseller_card_data c 
		     	where  cardresellerID='$resellerID'"; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				   
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->decrypt($card_data['CustomerCard']),12) ;
						 $card_data['CardID'] = $card_data['CardID'] ;
						 $card_data['friendlyName']  = $card_data['friendlyName'] ;
						 
						 $card_data['accountNumber'] = $card_data['accountNumber'] ;
						 $card_data['accountName']  = $card_data['accountName'] ;
						 $card_data['CardType'] = $card_data['CardType'] ;
				
						 $card[$key] = $card_data;
					   }
				}		
				
           
				
                return  $card;

     }
     public function get_merch_card_data($cardID)
	{
	        $res=[];

	        $this->db1->select('mcd.*');
	        $this->db1->from('merchant_card_data mcd');
	        $this->db1->where('mcd.merchantCardID', $cardID);
	        $res = $this->db1->get()->row();
	        
	        if(isset($res->MerchantCard) && $res->MerchantCard != null){
	        	$res->MerchantCard = $this->decrypt($res->MerchantCard);
	        }
	        if(isset($res->CardCVV) && $res->CardCVV != null){
	        	$res->CardCVV = $this->decrypt($res->CardCVV);
	        }
	       
		 	return $res;
	    
	} 
	public function update_merchant_card_data($condition, $card_data=array())
	{  
	     $status=false;

	     $this->db1->where($condition);
		 $updt = $this->db1->update('merchant_card_data',$card_data);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('*');
            $this->db1->from('merchant_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['merchantCardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	}	    
	public function insert_merchant_card_data($card_data)
	 {  

		$this->db1->insert('merchant_card_data',$card_data);
    	return	 $status = $this->db1->insert_id();

	}  
 	public function get_merchant_card_expiry_data($merchantID, $type=1){  
  
                $card = array();
			     
			  	if($type == 1){
			  		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     		where  merchantListID='$merchantID' and MerchantCard !=''"; 
			  	}else{
			  		$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     		where  merchantListID='$merchantID' and accountNumber !=''"; 
			  	}
		        
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				   
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->decrypt($card_data['MerchantCard']),12) ;
						 $card_data['CardID'] = $card_data['merchantCardID'] ;
						 $card_data['friendlyName']  = $card_data['merchantFriendlyName'] ;
						 
						 $card_data['accountNumber'] = $card_data['accountNumber'] ;
						 $card_data['accountName']  = $card_data['accountName'] ;
						 $card_data['CardType'] = $card_data['CardType'] ;
				
						 $card[$key] = $card_data;
					   }
				}		
				
           
				
                return  $card;

    }
    public function get_merchant_single_card_data($cardMerchantID)
  	{  
  
        $card = array();
              
   
			  
        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from  merchant_card_data c 
     	where  c.merchantCardID='$cardMerchantID'    "; 
		    $query1 = $this->db1->query($sql);
		    
		    
           $card_data =   $query1->row_array();
		  if(!empty($card_data )){	
		  		
				if($card_data['MerchantCard']!="")
				 $card['CardNo']     = $this->decrypt($card_data['MerchantCard']) ;
				 else
				  $card['CardNo']     = '';
				 $card['cardMonth']  = $card_data['CardMonth'];
				  $card['cardYear']  = $card_data['CardYear'];
				  $card['CardID']    = $card_data['merchantCardID'];
				  if($card_data['CardCVV']!="")
				  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
				  else
				    $card['CardCVV']     = '';
				  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
				  $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
                  $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
                  $card['Billing_City']	     = $card_data['Billing_City'];
                  $card['Billing_State']	 = $card_data['Billing_State'];
                  $card['Billing_Country']	 = $card_data['Billing_Country'];
                  $card['Billing_Contact']	 = $card_data['billing_phone_number'];
                  $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];

                  $card['billing_phone_number'] 	= $card_data['billing_phone_number'];
				  $card['billing_email'] 			= $card_data['billing_email'];
				  $card['billing_first_name'] 		= $card_data['billing_first_name'];
				  $card['billing_last_name'] 		= $card_data['billing_last_name'];


                  $card['CardType']	     = $card_data['CardType'];
                   
                  $card['acc_number']	 = $card_data['accountNumber'];	
                  $card['route_number'] 	 = $card_data['routeNumber'];
                
                  $card['acc_name']	 = $card_data['accountName'];
                  $card['secCodeEntryMethod']	 = $card_data['secCodeEntryMethod'];
                  $card['acct_type']	         = $card_data['accountType'];
                  $card['acct_holder_type']	 = $card_data['accountHolderType'];
                   
                   
		}		
				
		return  $card;

    }
}