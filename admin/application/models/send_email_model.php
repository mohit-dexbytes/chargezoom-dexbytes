<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Send Email Class
 *
 * Send email | replace tags
 *
 * @author Mohit Gupta <mohit.gupta@chargezoom.com>
 */

class Send_email_model extends CI_Model
{
    /**
     * Constructor of the class
     *
     */
	public function __construct() 
	{ 
        parent::__construct(); 
        $this->load->model('general_model');
	}

    /**
     * Function to send email and replace tags
     * 
     * @param int       $adminID
     * @param int       $resellerID
     * @param int       $merchantID
     * @param string    $toEmail
     * @param array     $templateType | value: templateType & userTemplate
     * @param string    $sendTO = "MERCHANT" | MERCHANT & RESELLER
     * @param float     $invoiceAmount
     * @param array     $extraData = []
     * 
     * @return bool
     */
    public function sendEmail(int $adminID, int $resellerID, int $merchantID, string $toEmail, array $templateType, string $sendTO = "MERCHANT", float $invoiceAmount = 0, array $extraData = [] ) : bool
    {
        $adminName = '';
        $adminData = $this->general_model->get_row_data('tbl_admin', [ 'adminID' => $adminID ]);
        if(!$adminData){
            return false;
        }
        $adminName = $adminData['adminCompanyName'];

        $merchantName = '';
        $merchantData = $this->general_model->get_row_data('tbl_merchant_data', [ 'merchID' => $merchantID ]);
        if($merchantData){
            if(!$resellerID){
                $resellerID = $merchantData['resellerID'];
            }

            if(empty($toEmail) && $sendTO == 'MERCHANT'){
                $toEmail = $merchantData['merchantEmail'];
            }

            $merchantName = $merchantData['companyName'];
        }

        $loginURL = getenv('RSDOMAIN');
        if($resellerID == getenv('DIRECT_CZ_RESELLER_ID')){
            $loginURL = "https://manage.$loginURL";
        } else {
            $loginURL = "https://reseller.$loginURL";
        }

        $logoURL = $invoicePayLink = '';
        $resellerName = $resellerEmail = $resellerPhone = '';
        $resellerData = $this->general_model->get_row_data('tbl_reseller', [ 'resellerID' => $resellerID ]);
        if($resellerData){
            $resellerName = $resellerData['resellerCompanyName'];
            $resellerEmail = $resellerData['resellerEmail'];
            $resellerPhone = $resellerData['primaryContact'];
            $logoURL = $resellerData['resellerProfileURL'];

            if(empty($toEmail) && $sendTO == 'RESELLER'){
                $toEmail = $resellerEmail;
            }
        }

        if(empty($toEmail))
        {
            return false;
        }
        
        $invoiceID = $invoiceRef = '';
        $invoiceDate = date('Y-m-d');

        $emailData = $this->general_model->get_row_data('tbl_template_reseller_data', $templateType);
        if(!$emailData){
            return false;
        }

        $invoiceData = [];
        if(isset($extraData['invoiceData']) && !empty($extraData['invoiceData'])){
            $invoiceData = $extraData['invoiceData'];
            $invoiceID = $invoiceData['invoice'];
            $invoiceRef = $invoiceData['invoiceNumber'];
            $invoiceDate = $invoiceData['DueDate'];

            $invoicePayLink = createPublicInvoiceLink($invoiceID, $merchantID);
            $invoicePayLink = "<a href='$invoicePayLink' target='_blank'>$invoicePayLink</a>";
        }

        if(isset($extraData['invoice']) && !empty($extraData['invoice'])){
            $invoiceID = $extraData['invoice'];
            $invWhere = [
                'invoice' => $invoiceID,
                'merchantID' => $merchantID,
            ];

            $invoiceData = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $invWhere);
            if($invoiceData)
            {
                $invoiceRef = $invoiceData['invoiceNumber'];
                $invoiceDate = $invoiceData['DueDate'];
                $invoicePayLink = createPublicInvoiceLink($invoiceID, $merchantID);
                $invoicePayLink = "<a href='$invoicePayLink' target='_blank'>$invoicePayLink</a>";
            }
        }

        if(empty($logoURL))
        {
            $logoURL = CZLOGO; 
        }

        $invoiceDate = date('M d, Y', strtotime($invoiceDate));

        $logoURL=	"<img src='$logoURL' />";
        $fromEmail = (empty($emailData['fromEmail'])) ? DEFAULT_FROM_EMAIL : $emailData['fromEmail'];
        $subject = $emailData['emailSubject'];
        $message = $emailData['message'];

        #Replace Message Tags
        $subject = stripslashes(str_replace('{{invoice.refnumber}}',$invoiceRef ,$subject));
        $subject = stripslashes(str_replace('{{invoice_num}}',$invoiceRef ,$subject));
        $subject = stripslashes(str_replace('{{invoice_last_payment_amount}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$subject )); 
        $subject = stripslashes(str_replace('{{invoice_amount_paid}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$subject )); 
        $subject = stripslashes(str_replace('{{invoice_balance}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$subject )); 
        $subject = stripslashes(str_replace('{{invoice_date}}',$invoiceDate ,$subject));
        $subject = stripslashes(str_replace('{{invoice_payment_pagelink}}',$invoicePayLink ,$subject));

        $subject = stripslashes(str_replace('{{reseller_name}}',$resellerName ,$subject));
        $subject = stripslashes(str_replace('{{reseller_email}}',$resellerEmail ,$subject));
        $subject = stripslashes(str_replace('{{reseller_phone}}',$resellerPhone ,$subject));

        $subject = stripslashes(str_replace('{{merchant_name}}',$merchantName ,$subject));

        $subject = stripslashes(str_replace('{{admin_name}}',$adminName ,$subject));

        #Replace Message Tags
        $message = stripslashes(str_replace('{{logo}}',$logoURL ,$message));
        $message = stripslashes(str_replace('{{login_url}}',$loginURL ,$message));

        $message = stripslashes(str_replace('{{invoice.refnumber}}',$invoiceRef ,$message));
        $message = stripslashes(str_replace('{{invoice_num}}',$invoiceRef ,$message));
        $message = stripslashes(str_replace('{{invoice_last_payment_amount}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$message )); 
        $message = stripslashes(str_replace('{{invoice_amount_paid}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$message )); 
        $message = stripslashes(str_replace('{{invoice_balance}}', ($invoiceAmount)?('$'.number_format($invoiceAmount,2)):'0.00' ,$message )); 
        $message = stripslashes(str_replace('{{invoice_date}}',$invoiceDate ,$message));
        $message = stripslashes(str_replace('{{invoice_payment_pagelink}}',$invoicePayLink ,$message));

        $message = stripslashes(str_replace('{{reseller_name}}',$resellerName ,$message));
        $message = stripslashes(str_replace('{{reseller_email}}',$resellerEmail ,$message));
        $message = stripslashes(str_replace('{{reseller_phone}}',$resellerPhone ,$message));

        $message = stripslashes(str_replace('{{merchant_name}}',$merchantName ,$message));

        $message = stripslashes(str_replace('{{admin_name}}',$adminName ,$message));

        $isSend = sendMailBySendgrid($toEmail, $subject, $fromEmail, '', $message, $fromEmail);
        if($isSend)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}