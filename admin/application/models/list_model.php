<?php
class List_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function get_row_data($table, $con)
    {

        $data = array();

        $query = $this->db->select('*')->from($table)->where($con)->get();

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        } else {
            return $data;
        }

    }

    public function get_rss_revenue($rcon = array())
    {
        $m_datas  = array();
        $m_datas1 = array();
        $rrrr     = array();
        $m_datas2 = array();

        if (empty($rcon)) {$rcon = array('isDelete' => 0, 'isEnable' => 1);}

        $this->db->select('r.resellerID,r.isSuspend,trg.gatewayFriendlyName,r.resellerCompanyName,r.date as date_added,
	   (select count(*) from customer_transaction where resellerID=r.resellerID) as transaction');
        $this->db->select('(select count(*) from tbl_merchant_data mr inner join plans pl on pl.plan_id=mr.plan_id where resellerID=r.resellerID and mr.isDelete=0 and mr.isEnable=1 ) as merchants', false);
        $this->db->from('tbl_reseller r');
        $this->db->join('tbl_admin_gateway trg', 'trg.gatewayID = r.gatewayID', 'left');
        $this->db->where($rcon);
        $qur = $this->db->get();

        if ($qur->num_rows() > 0) {
            $rss = $qur->result_array();
            foreach ($rss as $re) {
                $rid = $re['resellerID'];
                

                $final        = 0;
                $totalamount  = 0;
                $tier         = 0;
                $new_merchant = 0;
                $sr_fee1      = 0;
                $sub_fee      = 0;
                $cms          = 0;
                $rev    = $this->get_reseller_total_payment_new($rid);

                $refund = $rev['refund'];
              
                $totalamount += $rev['rev'] - $refund;
                
                
                if (!empty($m_datas)) {

                    foreach ($m_datas as $md) {
                        $refund = 0;
                        
                        $refund = $rev['refund'];
                        $totalamount += $rev['rev'] - $refund;
                        $new_merchant = ($new_merchant + $md['m_count']);

                    }

                    $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
                    $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
                    $m_datas1['p_value']     = sprintf('%0.2f', ($sub_fee + $sr_fee1));
                    $m_datas1['volume']      = $totalamount;
                    $m_datas1['revenue']     = $final;
                    $m_datas1['tier_status'] = $tier;
                    $m_datas1['merchant']    = $new_merchant;
                } else {
                    $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
                    $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
                    $m_datas1['p_value']     = 0;
                    $m_datas1['volume']      = $totalamount;
                    $m_datas1['revenue']     = $final;
                    $m_datas1['tier_status'] = $tier;
                    $m_datas1['merchant']    = $new_merchant;

                }
                $m_datas1['merchant']            = $re['merchants'];
                $m_datas1['resellerID']          = $re['resellerID'];
                $m_datas1['resellerCompanyName'] = $re['resellerCompanyName'];
                $m_datas1['transaction']         = ($re['transaction']) ? $re['transaction'] : '0';
                $m_datas1['gatewayFriendlyName'] = $re['gatewayFriendlyName'];
                $m_datas1['isSuspend']           = $re['isSuspend'];
                $m_datas1['date_added']           = $re['date_added'];
                $m_datas2[]                      = $m_datas1;
            }
        }
        return $m_datas2;

    }

    public function get_reseller_payment($resellerID, $pln_id, $date = '')
    {
        $res = array();

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and mr.resellerID="' . $resellerID . '" and r.isDelete=0 and mr.plan_id="' . $pln_id . '"
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' .
            $date);

        $query2 = $this->db->query('select sum(tr.transactionAmount) as refund from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "Paypal_refund"  OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" or tr.transactionType = "Paypal_refund" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")

		and ( tr.transaction_user_status !="3" )
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and mr.resellerID="' . $resellerID . '" and r.isDelete=0  and mr.plan_id="' . $pln_id . '"

            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' . $date);

        if ($query1->num_rows() > 0) {
            $res['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $res['rev'] = '0';
        }

        if ($query2->num_rows() > 0) {

            $res['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $res['refund'] = '0';
        }

        return $res;

    }

    public function get_reseller_transaction($resellerID, $pln_id, $date = '')
    {
        $res = 0;

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select count(*) as total  from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where
         ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete="0" and mr.plan_id="' . $pln_id . '"
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' .
            $date);


        if ($query1->num_rows() > 0) {
            $res = $query1->row_array()['total'];
        } else {
            $res = '0';
        }

        return $res;

    }

    public function get_month_total_amount($mID, $dt = '')
    {
        $amount = 0;

        $total  = isset($rev['total']) ? $rev['total'] : 0;
        $refund = isset($rev['refund']) ? $rev['refund'] : 0;
        $rev    = $this->merchant_total_count($mID, $dt);

        $amount = $total - $refund;
        return $amount;

    }

    public function get_data_admin_merchant($searchKey= false, int $limit=0, int $start=0)
    {
        $rest1 = array();

        $this->db->select('mr.merchID,mr.firstName, mr.companyName,mr.isSuspend, mr.resellerID, mr.gatewayID, p.plan_id, mr.isDelete,p.plan_name as plan,rs.resellerCompanyName, ad.gatewayFriendlyName as merchantgatway,mr.date_added');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
        $this->db->join('tbl_admin_gateway ad', 'mr.gatewayID = ad.gatewayID', 'left');
        $this->db->join('plans p', 'p.plan_id = mr.plan_id', 'inner');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');

        if($searchKey){
            $this->db->where("(mr.firstName Like '%" . $searchKey . "%' or  mr.lastName  Like  '%" . $searchKey . "%' or mr.companyName Like '%" . $searchKey . "%' or p.plan_name Like  '%" . $searchKey . "%')");
        }

        // Apply Limit
        if($limit > 0){
            $this->db->limit($limit, $start);
        }

        $query = $this->db->get();
        if ($query->num_rows()) {
            $res = $query->result_array();
            foreach ($res as $ress) {
                $rest = $ress;


                $q1 = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

    	  	where tr.merchantID="' . $ress['merchID'] . '" and
    	  	(tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture"  or  tr.transactionType ="Offline Payment"
    	  	or tr.transactionType ="pay_sale" or tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture"
    	  	or tr.transactionType ="capture" or tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture")
    	  	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
    	  	 and tr.transaction_user_status!="3"
    	  	and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ');

                $q2 = $this->db->query('select sum(tr.transactionAmount) as refund
    	  	from customer_transaction tr

    	  	where tr.merchantID ="' . $ress['merchID'] . '"  and
    	  	 (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR
    	  	 UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND"
    	  	 OR UPPER(tr.transactionType) = "CREDIT" )

    	  	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
    	  	and  tr.transaction_user_status!="3"
    	  	and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)');


                $qq = $this->db->query("Select gatewayFriendlyName from tbl_merchant_gateway where merchantID='" . $ress['merchID'] . "' and set_as_default='1' ");
                if ($qq->num_rows() > 0) {
                    $rest['gatewayFriendlyName'] = $qq->row_array()['gatewayFriendlyName'];
                } else {
                    $rest['gatewayFriendlyName'] = '';
                }

                if ($q1->num_rows() > 0) {
                    $rest['total_amount'] = $q1->row_array()['total'];
                } else {
                    $rest['total_amount'] = '';
                }
                if ($q2->num_rows() > 0) {
                    $rest['refund_amount'] = $q2->row_array()['refund'];
                } else {
                    $rest['refund_amount'] = '';
                }

                $rest1[] = $rest;
            }
        }

        return $rest1;

    }

    public function merchant_total_count($merchantID, $dt)
    {
        $count = 0;
        $data  = array();
        $query = $this->db->query("Select * from app_integration_setting where merchantID='" . $merchantID . "' ");

        $row = $query->row_array();
        if ($dt != '') {
            $d_con = 'transactionDate BETWEEN DATE_ADD(date_format(LAST_DAY(now() - interval 1 month),"%Y-%m-01"),INTERVAL ' . $dt . ' DAY) AND date_format(LAST_DAY(now() - interval 1 month ),"%Y-%m-%d")';
        } else {
            $d_con = 'tr.transactionDate BETWEEN date_format(LAST_DAY(now() - interval 1 month),"%Y-%m-01") AND date_format(LAST_DAY(now() - interval 1 month ),"%Y-%m-%d")';
        }
        if (!empty($row)) {
            if ($row['appIntegration'] == '1') {

                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr


	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
	where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
	or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
	tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and (  tr.transaction_user_status!="3")   and
	' . $d_con . '  and   tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
	(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and (   tr.transaction_user_status!="3")  and ' . $d_con . '   and  tr.merchantID="' . $merchantID . '"  ');


            }
            if ($row['appIntegration'] == '2') {
                $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr
	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
	where    (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
	or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3" )    and
	' . $d_con . '  and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3")   and
	' . $d_con . '  and     tr.merchantID="' . $merchantID . '"  ');
            }
            if ($row['appIntegration'] == '3') {
                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
	where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or   tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
	or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
	tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3" )     and
	' . $d_con . '   and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and  ( tr.transaction_user_status !="3" )   and
	' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

            }
            if ($row['appIntegration'] == '4') {
                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
	where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
	or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
	tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and  ( tr.transaction_user_status !="3")   and
		' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
	(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3")   and
		' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

            }

            if ($row['appIntegration'] == '5') {
                $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr
	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
	where    (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
	or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3" )    and
	' . $d_con . '  and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

	inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	and ( tr.transaction_user_status !="3")   and
	' . $d_con . '  and     tr.merchantID="' . $merchantID . '"  ');
            }

            if ($rquery->num_rows() > 0) {
                $data['total'] = $rquery->row_array()['total'];

                $data['refund'] = $rfquery->row_array()['rm_amount'];
            } else {
                $data['total'] = 0;

                $data['refund'] = 0;
            }

        }
        return $data;

    }

    public function get_subscription_data($requestMerchantID = '')
    {
        $res   = array();
        $today = date('Y-m-d');
        $monthYear = strtotime(date('Y-m'));

        $today = $today;
        $resArr= [];

        $date = date('d');
        $this->db->select('mr.merchID as merchantID, mr.firstName,mr.lastName,mr.freeTrial, mr.companyName,mr.trial_expiry_date,mr.plan_expired,mr.plan_id,mr.cardID, p.*');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'INNER');
        $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'INNER');
        $this->db->join('tbl_merchant_billing_invoice tmbi', 'tmbi.merchantID=mr.merchID AND tmbi.BalanceRemaining > 0 AND tmbi.invoiceStatus IN (1,3,5)', 'LEFT');
        
        if($requestMerchantID != ""){
            $this->db->where('mr.merchID', $requestMerchantID);
        }else{
            $this->db->where('mr.cronRunMonth !=', $monthYear);
        }
        $this->db->where('tmbi.merchant_invoiceID IS NULL');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');
        $this->db->where('rs.isDelete', '0');
        $this->db->group_by('mr.merchID');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $value) {
                if($value['billingType'] == 2){
                    $value['volumn'] = $this->getMerchantTotalAmountProcessed($value['merchantID'],$value['trial_expiry_date']);
                }else{
                    $value['volumn'] = 0;
                }
                $resArr[] = $value;
            }
            return $resArr;
        }
        return $res;
    }

    public function new_merchant_count($mID)
    {
        $c_count = 0;
        $query   = $this->db->query("select count(*) as c_count from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_merchant_data.resellerID IN (SELECT tbl_merchant_data.resellerID FROM tbl_merchant_data WHERE tbl_merchant_data.isDelete = 0 AND tbl_merchant_data.merchID = '" . $mID . "') AND tbl_reseller.resellerID=tbl_merchant_data.resellerID and tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and MONTH(CURDATE()) = MONTH(Date_Format(tbl_merchant_data.date_added,'%Y/%m/%d')) AND YEAR(CURDATE()) = YEAR(Date_Format(tbl_merchant_data.date_added,'%Y/%m/%d'))");

        $c_count = $query->row_array()['c_count'];
        return $c_count;

    }

    public function get_total_transaction($mID)
    {
        $tr_ct = 0;
        $this->db->select('count(*) as tr_cnt ');
        $this->db->from('customer_transaction tr');
        $this->db->where('tr.merchantID', $mID);
        $this->db->where('UPPER(tr.transactionType)!="OFFLINE PAYMENT" ');
        $this->db->group_by('tr.transactionID');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $tr_count = $query->row_array();

            $tr_ct = $tr_count['tr_cnt'];
        }

        return $tr_ct;

    }

    public function get_reseller_subscription_data()
    {
        $res = array();

        $this->db->select('resellerID');
        $this->db->from('tbl_resller r');

    }

    public function get_rseller_invoice()
    {
        $result = array();
        $today  = date('Y-m-d');
        $today  = "2019-10-09";
        $this->db->select('r.resellerID,r.resellerCompanyName,r.resellerEmail,r.resellerfirstName,r.lastName, rb.*');
        $this->db->from('tbl_reseller_billing_invoice rb');
        $this->db->join('tbl_reseller r', 'r.resellerID=rb.resellerID', 'inner');
        $this->db->where('r.isDelete', 0);
        $this->db->where('r.isEnable', 1);
        $this->db->where('date_format(rb.DueDate,"%Y-%m-%d")', $today);

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
        }

        return $result;

    }
    
    public function get_reseller_subscription($rcon = array())
    {
        $m_datas  = array();
        $m_datas1 = array();
        $rrrr     = array();
        $m_datas2 = array();

        if (empty($rcon)) {$rcon = array('isDelete' => 0, 'isEnable' => 1, 'isTestmode' => 0);}

        $this->db->select('r.resellerID,r.isSuspend,trg.gatewayFriendlyName,r.resellerCompanyName');
        $this->db->from('tbl_reseller r');
        $this->db->join('tbl_reseller_gateway trg', 'trg.resellerID = r.resellerID', 'left');
        $this->db->where($rcon);
        $qur = $this->db->get();


        if ($qur->num_rows() > 0) {
            $rss = $qur->result_array();
            foreach ($rss as $re) {
                $rid = $re['resellerID'];

                $sql = $this->db->query("
				Select  mr.resellerID, mr.merchID as merchant,mr.companyName,  mr.plan_id,	p.plan_name,p.subscriptionRetail,p.serviceFee,p.planType,p.merchant_plan_type,
				 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "'  and plan_id=mr.plan_id and isEnable='1' and isDelete='0')  as m_count,
				 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "' and plan_id=mr.plan_id and  isEnable='1' and  isDelete='0'
				 and  YEAR(date_added) = YEAR(CURRENT_DATE()) and MONTH(date_added) = MONTH(CURRENT_DATE())  )  as tm_count
				 from  tbl_merchant_data mr
				inner join plans p on p.plan_id=mr.plan_id
				inner join tbl_reseller rs on rs.resellerID=mr.resellerID AND rs.isTestmode = 0
				where  mr.isDelete='0' and  rs.isDelete='0'and  mr.isEnable='1' and  rs.isEnable='1' and mr.resellerID='" . $rid . "'  ");

                $m_datas = $sql->result_array();

                $items        = array();
                $final        = 0;
                $totalamount  = 0;
                $tier         = 0;
                $new_merchant = 0;
                $sr_fee1      = 0;
                $sub_fee      = 0;
                $cms          = 0;
                $ss_amout     = 0;
                $vt_amout     = 0;
                $as_amout     = 0;
                $vt_merchant  = $ss_merchant  = $as_merchant = 0;

                if (!empty($m_datas)) {

                    foreach ($m_datas as $md) {
                        $refund = 0;
                        $new_merchant++;

                        $item['itemPrice']    = $md['subscriptionRetail'];
                        $item['itemName']     = $md['plan_name'];
                        $item['merchantName'] = $md['companyName'];

                        $item['planID']          = $md['plan_id'];
                        $item['itemQty']         = 1;
                        $item['merchantID']      = $md['merchant'];
                        $item['resellerInvoice'] = $rid;
                        $item['createdAt']       = date('Y-m-d H:i:s');

                        $items[] = $item;

                        $sub_fee += $md['subscriptionRetail'];

                        if ($md['merchant_plan_type'] == 'SS') {
                            $ss_amout += ($md['subscriptionRetail']);

                            $ss_merchant = $ss_merchant + 1;
                        }
                        if ($md['merchant_plan_type'] == 'VT') {
                            $vt_amout += ($md['subscriptionRetail']);
                            $vt_merchant = $vt_merchant + 1;
                        }

                        if ($md['merchant_plan_type'] == 'AS') {
                            $as_amout += ($md['subscriptionRetail']);

                            $as_merchant = $as_merchant + 1;
                        }

                    }

                    $final = $sub_fee;
                    
                    $m_datas1['ss_amount']   = sprintf('%0.2f', ($ss_amout));
                    $m_datas1['vt_amount']   = sprintf('%0.2f', ($vt_amout));
                    $m_datas1['as_amount']   = sprintf('%0.2f', ($as_amout));
                    $m_datas1['vt_merchant'] = $vt_merchant;
                    $m_datas1['ss_merchant'] = $ss_merchant;
                    $m_datas1['as_merchant'] = $as_merchant;
                    $m_datas1['p_value']     = sprintf('%0.2f', ($sub_fee + $sr_fee1));
                    
                    $m_datas1['revenue']      = sprintf('%0.2f', $final);
                    $m_datas1['invoice_item'] = $items;
                    $m_datas1['merchant']     = $new_merchant;
                } else {
                    $m_datas1['ss_amount']   = sprintf('%0.2f', ($ss_amout));
                    $m_datas1['vt_amount']   = sprintf('%0.2f', ($vt_amout));
                    $m_datas1['as_amount']   = sprintf('%0.2f', ($as_amout));
                    $m_datas1['p_value']     = 0;
                    $m_datas1['vt_merchant'] = $vt_merchant;
                    $m_datas1['ss_merchant'] = $ss_merchant;
                    $m_datas1['as_merchant'] = $as_merchant;
                    $m_datas1['revenue']     = sprintf('%0.2f', $final);
                    $m_datas1['merchant']     = $new_merchant;
                    $m_datas1['invoice_item'] = $items;

                }

                $m_datas1['resellerID']          = $re['resellerID'];
                $m_datas1['resellerCompanyName'] = $re['resellerCompanyName'];
                $m_datas1['isSuspend'] = $re['isSuspend'];
                $m_datas2[]            = $m_datas1;
            }
        }

        return $m_datas2;

    }
    public function getMerchantBillingPendinInvoices($payOption=null) 
    {
		$currentDate = date('Y-m-d');
        $date = new DateTime($currentDate);
        
        $invoiceDueDates = ["$currentDate"];
        $daysToPickInvoice = [2,3,5,7,10,15,20,27,40];
        foreach ($daysToPickInvoice as $dayValue) {
            $dayValue -= 1; #Substract one to the day to count current day as Day 1
            
            $date = new DateTime($currentDate);
            $date->sub(new DateInterval("P".$dayValue."D"));
            
            $newDate = $date->format('Y-m-d');
            $invoiceDueDates[] = "$newDate";
        }

        $data=array();  
        
        $sql = $this->db->select('mbi.*')
            ->from('tbl_merchant_billing_invoice as mbi')
            ->join('tbl_merchant_data as md','md.merchID=mbi.merchantID')
            ->where('mbi.status','pending')
            ->where('mbi.BalanceRemaining >',0)
            ->where_in('invoiceStatus', [1,3])
            ->where_in('DueDate', $invoiceDueDates)
            ->order_by('mbi.createdAt', 'desc');

        if($payOption != null){
            $sql->where('md.payOption', $payOption);
        }

        $query = $sql->get();
        
        if($query->num_rows()>0 ) {
            $data = $query->result_array();
            
           return $data;
        } else {
            return $data;
        }               
    
    }
    public function get_merchant_invoice_data($dataget,$typeFetch='') 
    {

        $limit = getTableRowLengthValue($dataget['length']);
        $start =  getTableRowStartValue($dataget['start']);
        
        $column = getTableRowOrderColumnValue($dataget['order'][0]['column']);
        if($column == 0){
            $orderName = 'merchantName';
        }elseif($column == 1){
            $orderName = 'invoiceNumber';
        }elseif($column == 2){
            $orderName = 'createdAt';
        }elseif($column == 3){
            $orderName = 'AppliedAmount';
        }elseif($column == 4){
            $orderName = 'status';
        }elseif($column == 5){
            $orderName = 'merchantName';
        }else{
            $orderName = 'merchantName';    
        }
        $orderBY = getTableRowOrderByValue($dataget['order'][0]['dir']);
        
        
        $data=array();  
        if($typeFetch == ''){
            $this->db->limit($limit, $start);
        }
        
        if($dataget['search']['value'] != null || !empty($dataget['search']['value'])){
            $search_key = $dataget['search']['value'];
            $this->db->like('merchantName', $search_key);
            $this->db->or_like('invoice', $search_key);
            $this->db->or_like('invoiceNumber', $search_key);
            $this->db->or_like('createdAt', $search_key);
            $this->db->or_like('AppliedAmount', $search_key);
            $this->db->or_like('status', $search_key);
        }

        if(isset($dataget['status_filter']) && $dataget['status_filter'] != '' && $dataget['status_filter'] != 'All'){
            if($dataget['status_filter'] == 'Pending'){
                $this->db->where_in('invoiceStatus', [1,3]);
            } elseif($dataget['status_filter'] == 'Unpaid'){
                $dataget['status_filter'] = 'Pending';
                $this->db->where('invoiceStatus', 5);
            } else if($dataget['status_filter'] == 'Void'){
                $dataget['status_filter'] = 'Pending';
                $this->db->where('invoiceStatus', 0);
            } else if($dataget['status_filter'] == 'Refund'){
                $this->db->where('invoiceStatus', 2);
            }
            $this->db->where('status', $dataget['status_filter']);
        }
        
        $query =  $this->db->select('*')->from('tbl_merchant_billing_invoice')->order_by($orderName, $orderBY)->get();
    
        if($query->num_rows()>0 ) {

            
            if($typeFetch == ''){
                $data = $query->result_array();
            }else{
                $data = $query->num_rows();
            }
           return $data;
        } else {
            if($typeFetch == 'total'){
                $data = 0;
            }
            return $data;
        }               
    
    }
    public function get_merchant_invoice_total($dataget) 
    {
        $limit = getTableRowLengthValue($dataget['length']);
        $start =  getTableRowStartValue($dataget['start']);
        
        $column = getTableRowOrderColumnValue($dataget['order'][0]['column']);
        if($column == 0){
            $orderName = 'merchantName';
        }elseif($column == 1){
            $orderName = 'invoiceNumber';
        }elseif($column == 2){
            $orderName = 'createdAt';
        }elseif($column == 3){
            $orderName = 'AppliedAmount';
        }elseif($column == 4){
            $orderName = 'status';
        }elseif($column == 5){
            $orderName = 'merchantName';
        }else{
            $orderName = 'merchantName';    
        }
        $orderBY = getTableRowOrderByValue($dataget['order'][0]['dir']);
        
        
        $data=array();  
        if($dataget['search']['value'] != null || !empty($dataget['search']['value'])){
            $search_key = $dataget['search']['value'];
            $this->db->like('merchantName', $search_key);
            $this->db->or_like('invoice', $search_key);
            $this->db->or_like('invoiceNumber', $search_key);
            $this->db->or_like('createdAt', $search_key);
            $this->db->or_like('AppliedAmount', $search_key);
            $this->db->or_like('status', $search_key);
        }

        if(isset($dataget['status_filter']) && $dataget['status_filter'] != '' && $dataget['status_filter'] != 'All'){
            if($dataget['status_filter'] == 'Pending'){
                $this->db->where('invoiceStatus', 1);
            } else if($dataget['status_filter'] == 'Void'){
                $dataget['status_filter'] = 'Pending';
                $this->db->where('invoiceStatus', 0);
            } else if($dataget['status_filter'] == 'Refund'){
                $this->db->where('invoiceStatus', 2);
            }
            $this->db->where('status', $dataget['status_filter']);
        }
        
        $query =  $this->db->select('*')->from('tbl_merchant_billing_invoice')->order_by($orderName, $orderBY)->get();
    
        return $query->num_rows();               
    
    }
    public function getMerchantBillingFreeTrailInvoices()
    {
        $res=array();  
        
        $this->db->select('mr.merchID as merchantID, mr.resellerID as resellerID,rs.downgradePlanID,mr.firstName,mr.lastName,mr.freeTrial, mr.companyName,mr.trial_expiry_date,mr.plan_id,mr.cardID, p.*');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'INNER');
        $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'INNER');
        $this->db->where('mr.freeTrial',1);
        $this->db->where('mr.isEnable',1);
        $this->db->where('mr.isDelete', '0');
        $this->db->where('rs.isDelete', '0');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }
    
    public function get_merchant_subscription($rcon = array())
    {
        $m_datas  = array();
        $m_datas1 = array();
        $rrrr     = array();
        $m_datas2 = array();

        if (empty($rcon)) {$rcon = array('isDelete' => 0, 'isEnable' => 1);}

                $sql = $this->db->query("
                Select  mr.resellerID, mr.merchID as merchant,mr.companyName,  mr.plan_id,  p.plan_name,p.subscriptionRetail,p.serviceFee,p.planType,p.merchant_plan_type
                 from tbl_merchant_data mr 
                inner join tbl_reseller rs  ON rs.resellerID = mr.resellerID
                inner join plans p on p.plan_id=mr.plan_id
                where  mr.isDelete='0' and  mr.isDelete='0'and  mr.isEnable='1' and  rs.isTestmode='0' and rs.isEnable='1' and rs.isDelete='0'");
                $m_datas = $sql->result_array();

                $items        = array();
                $final        = 0;
                $totalamount  = 0;
                $tier         = 0;
                $new_merchant = 0;
                $sr_fee1      = 0;
                $sub_fee      = 0;
                $cms          = 0;
                $ss_amout     = 0;
                $free_amount     = 0;
                $vt_amout     = 0;
                $as_amout     = 0;
                $vt_merchant  = $ss_merchant  = $as_merchant = $free_merchant = 0;
              
                if (!empty($m_datas)) {

                    foreach ($m_datas as $md) {
                        $rid = $md['resellerID'];
                        $refund = 0;
                        $new_merchant++;
                        
                        $invoice_total = $md['subscriptionRetail'];

                        $item['itemPrice']    = $md['subscriptionRetail'];
                        $item['itemName']     = $md['plan_name'];
                        $item['merchantName'] = $md['companyName'];

                        $item['planID']          = $md['plan_id'];
                        $item['itemQty']         = 1;
                        $item['merchantID']      = $md['merchant'];
                        $item['resellerInvoice'] = $rid;
                        $item['createdAt']       = date('Y-m-d H:i:s');

                        $items[] = $item;

                        $sub_fee += $md['subscriptionRetail'];

                        if ($md['merchant_plan_type'] == 'SS') {
                            $ss_amout += ($invoice_total);
                            
                            $ss_merchant = $ss_merchant + 1;
                        }
                        if ($md['merchant_plan_type'] == 'VT') {
                            $vt_amout += ($invoice_total);
                            $vt_merchant = $vt_merchant + 1;
                        }

                        if ($md['merchant_plan_type'] == 'AS') {
                            $as_amout += ($invoice_total);
                            
                            $as_merchant = $as_merchant + 1;
                        }

                        if ($md['merchant_plan_type'] == 'Free') {
                            $free_amount += ($invoice_total);
                            $free_merchant = $free_merchant + 1;
                        }
                       
                    }

                    $final = $sub_fee;
                   
                    $m_datas1['ss_amount']   = sprintf('%0.2f', ($ss_amout));
                    $m_datas1['vt_amount']   = sprintf('%0.2f', ($vt_amout));
                    $m_datas1['as_amount']   = sprintf('%0.2f', ($as_amout));
                    $m_datas1['free_amount']   = sprintf('%0.2f', ($free_amount));
                    $m_datas1['vt_merchant'] = $vt_merchant;
                    $m_datas1['ss_merchant'] = $ss_merchant;
                    $m_datas1['as_merchant'] = $as_merchant;
                    $m_datas1['free_merchant'] = $free_merchant;
                    $m_datas1['p_value']     = sprintf('%0.2f', ($sub_fee + $sr_fee1));
                    $m_datas1['revenue']      = sprintf('%0.2f', $final);
                    $m_datas1['invoice_item'] = $items;
                    $m_datas1['merchant']     = $new_merchant;
                } else {
                    $m_datas1['ss_amount']   = sprintf('%0.2f', ($ss_amout));
                    $m_datas1['vt_amount']   = sprintf('%0.2f', ($vt_amout));
                    $m_datas1['as_amount']   = sprintf('%0.2f', ($as_amout));
                    $m_datas1['free_amount']   = sprintf('%0.2f', ($free_amount));
                    $m_datas1['p_value']     = 0;
                    $m_datas1['vt_merchant'] = $vt_merchant;
                    $m_datas1['ss_merchant'] = $ss_merchant;
                    $m_datas1['as_merchant'] = $as_merchant;
                    $m_datas1['free_merchant'] = $free_merchant;
                    $m_datas1['revenue']     = sprintf('%0.2f', $final);
                    $m_datas1['merchant']     = $new_merchant;
                    $m_datas1['invoice_item'] = $items;
                   

                }

              
                $m_datas2[]            = $m_datas1;

        return $m_datas2;

    }
    public function get_reseller_total_payment_new($resellerID,$month = ''){

        $res = array();

        if ($month != '') {
            $condition = ' and  date_format(ct.transactionDate,"%b-%Y") = "' . $month . '"  ';

        } else {

            $condition = '';
        }
        $query1 = $this->db->query("
                    Select  sum(ct.transactionAmount) as total
                     from customer_transaction ct
                    inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                    inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                    where  (ct.transactionType ='sale' or ct.transactionType ='Paypal_sale' or ct.transactionType ='Paypal_capture' or  ct.transactionType ='Offline Payment'  or  ct.transactionType ='pay_sale' or  ct.transactionType ='prior_auth_capture' or ct.transactionType ='pay_capture' or  ct.transactionType ='capture' 
          or  ct.transactionType ='auth_capture' or ct.transactionType ='stripe_sale' or ct.transactionType ='stripe_capture') and  mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID." and mr.resellerID=".$resellerID." and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') ".$condition." ");
        
        if ($query1->num_rows() > 0) {
            $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $total['rev'] = '0';
        }

        $query2 = $this->db->query("
                Select  sum(ct.transactionAmount) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' )
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') 
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total;

    }
    public function get_merchant_invoice_paid_transaction($invoiceID) 
    {
        
        $data=array();  
        
        $query =  $this->db->select('transactionID,transactionAmount,merchantID,transactionType,transactionGateway,gateway,gatewayID,merchant_invoiceID,transactionCode,transactionCard,transaction_user_status')->from('tbl_merchant_tansaction')->where_in('transactionType',['sale','Paypal_sale','Paypal_capture','pay_sale','prior_auth_capture','pay_capture','capture','auth_capture','stripe_sale','stripe_capture'])->where_in('transactionCode',['100','111','1','200'])->where('merchant_invoiceID',$invoiceID)->get();
      
        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        } else {
            return $data;
        }              
    
    }
     public function getMerchantDataByID($merchID)
    {
        $res=array();  
        
        $this->db->select('mr.*,rs.downgradePlanID,rs.plan_id');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'INNER');
        $this->db->where('mr.merchID',$merchID);
        $this->db->where('mr.isEnable',1);
        $this->db->where('mr.isDelete', '0');
        $this->db->where('rs.isDelete', '0');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->row_array();
        }
        return $res;
    }
    public function getMerchantTotalAmountProcessed($mID,$trialExpiryDate)
    {
        $res= array();
        $earn = 0;
        $trialExpiryDate = date('Y-m-d', strtotime('+1 day', strtotime($trialExpiryDate)));
        
        $Trialmonth = date('M-Y', strtotime($trialExpiryDate.""));
        $month = date('M-Y', strtotime(date('Y-m').""));
        
        if($Trialmonth == $month){
            $TODate =  date("Y-m-t", strtotime($trialExpiryDate));

            $dateQuery = 'DATE(tr.transactionDate) BETWEEN "'.$trialExpiryDate.'" AND  "'.$TODate.'"';
        }else{
            $dateQuery = 'date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"';
        }

        $sql = 'SELECT  sum(tr.transactionAmount) as volume FROM  
        customer_transaction tr    
        inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
        WHERE tr.merchantID = "'.$mID.'"  AND    
        '.$dateQuery.' and    (tr.transactionType ="Sale"  or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
        or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( tr.transaction_user_status !="3" )   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
       
        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
    
        inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
        (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )  AND    
        '.$dateQuery.'
        and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( tr.transaction_user_status !="3" ) and     tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();

            $earn = $row['volume']- $rfquery->row_array()['rm_amount'];
            
            return $earn;
        }
       
        return $earn;
    }

    /**
	 * Function to update merchant status if a payment fails for an invoice
	 * 
	 * @param int $merchantID
	 * @param int $isSuspend
	 * @param int $isEnable
	 * 
	 * @return bool 
	 */
	public function updateMerchantStatus(int $merchantID, int $isSuspend = 0, int $isEnable = 0) : bool
	{

        $dataToUpdate = [
            'isSuspend' => $isSuspend,
            'isEnable' => $isEnable,
        ];

		$this->db->where('merchID', $merchantID);
        if ($this->db->update('tbl_merchant_data', $dataToUpdate)) {
            return true;
        } else {
            return false;
        }
	}
}
