<?php 
class Admin_panel_model extends CI_Model
{

    /**
     * Constructor of the class
     *
     */
	function __construct() 
	{ 
		parent::__construct(); 
		define('FORGET_PASSWORD_USER_TYPE', 'Admin'); 
	}

    public function general()
    {
        parent::Model();
        $this->load->database();

    }

    public function insert_row($table_name, $array, $skipKeys=['message'])
    {

        $dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		} 

		if($this->db->insert($table_name,$dataToInsert)) {
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update_row($table_name, $reg_no, $array)
    {
        $this->db->where('reg_no', $reg_no);
        if ($this->db->update($table_name, $array)) {
            return true;
        } else {
            return false;
        }

    }

    public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
        if ($array != '') {
            $this->db->where($array);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function reseller_reset_password($code, $email)
    {
        $passBcrypt = password_hash($code, PASSWORD_BCRYPT);

        $this->db->where('resellerEmail', $email);
        $this->db->set('resellerPasswordNew', $passBcrypt);
        if ($this->db->update('tbl_reseller')) {
            return true;
        } else {
            return false;
        }
    }

    public function temp_reset_password($code, $email)
    {
        $passBcrypt = password_hash($code, PASSWORD_BCRYPT);

        $this->db->where('adminEmail', $email);
        $this->db->set('adminPasswordNew', $passBcrypt);
        if ($this->db->update('tbl_admin')) {
            return true;
        } else {
            return false;
        }
    }

    public function update_row_data($table_name, $condition, $array, $skipKeys=['message'])
    {
        $dataToUpdate = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

        $this->db->where($condition);
        $trt = $this->db->update($table_name, $dataToUpdate);
        if ($trt) {
            return true;
        } else {
            return false;
        }

    }

    public function get_row_data($table, $con)
    {

        $data = array();

        $query = $this->db->select('*')->from($table)->where($con)->get();

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        } else {
            return $data;
        }

    }

    public function get_select_data($table, $clm, $con)
    {

        $data = array();

        $columns = implode(',', $clm);

        $query = $this->db->select($columns)->from($table)->where($con)->get();

        if ($query->num_rows() > 0) {
            $data = $query->row_array();
            return $data;
        } else {
            return $data;
        }
    }

    public function get_table_data($table, $con)
    {
        $data = array();
        
        $this->db->select('*');
        $this->db->from($table);
        if (!empty($con)) {
            $this->db->where($con);
        }

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $data = $query->result_array();
            return $data;
        } else {
            return $data;
        }
    }

    public function get_planfdn_data($plan)
    {

        $query = $this->db->query("SELECT * FROM `plans` pl  LEFT JOIN `plan_friendlyname` fpl ON pl.plan_id = fpl.plan_id AND pl.plan_id = $plan");
        if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return false;
			}
	}
	  
	  ///---------  to get the email history data --------///
	
	   public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	  
	  
	  
	          public function get_select_data_row($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row();
					   return $data;
					} else {
						return $data;
					}			
			}
	  
	  
	  public function get_num_rows($table, $con) {
			
			$num = 0;	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			$num = $query->num_rows();
			return $num;
		
	}
	
	
	
	public function delete_row_data($table, $condition){
	   
     	    $this->db->where($condition);
             $this->db->delete($table);
            if($this->db->affected_rows()>0){
			  return true;
			}else{
			  return false;
			}
	
	}
	public function deactive_plan($table, $condition)
    {

        $this->db->where($condition);
        $this->db->update($table,array('status' => '0'));
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }
    
    public function active_plan($table, $condition)
    {

        $this->db->where($condition);
        $this->db->update($table,array('status' => '1'));
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }

    }

         
    public function get_admin_login($email, $password)
    {
        $result = array();

        $this->db->select('
            adminID,adminFirstName,adminLastName,adminEmail,
            adminCompanyName,userAdminID,adminPassword,adminPasswordNew
        ');

        $this->db->from('tbl_admin');
        $this->db->where('adminEmail', $email);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $result = $query->row_array();

            if ($result['adminPasswordNew'] !== null) {
                if (!password_verify($password, $result['adminPasswordNew'])) {
                    return [];
                }
            } else {
                if (md5($password) !== $result['adminPassword']) {
                    return [];
                }
            }

            unset($result['adminPassword']);
            unset($result['adminPasswordNew']);

            if ($result['userAdminID'] != 0) {
                $query1 = $this->db->query("
                    SELECT GROUP_CONCAT(authID SEPARATOR ',') AS auth 
                    FROM tbl_admin_role 
                    WHERE adminUserID ='" . $result['adminID'] . "'
                ");

                if ($query1->num_rows() > 0) {
                    $result['previlege'] = explode(
                        ',',
                        $query1->row_array()['auth']
                    );
                } else {
                    $result['previlege'] = array();
                }
            }
        }

        return $result;
    }
      
      
	 function admin_logout()
	{ 
		$this->db->trans_start();
		$session_data = $this->session->userdata('admin_logged_in'); 
		$this->db->where('adminID', $session_data['adminID']); 
		$this->db->update('tbl_admin',array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('admin_logged_in'); 
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	} 

      
    /***********Dashboard****************/
    public function admin_count_dashboard($month = '',$type='')
    {

        $r_cnt = $mr_cnt = $c_cnt = 0;
        $this->db->select(" count(m.merchID) as mr_count");
        $this->db->from('tbl_merchant_data m');
        $this->db->join('tbl_reseller r', 'r.resellerID=m.resellerID', 'inner');
        $this->db->where('m.isDelete', '0');
        $this->db->where('r.isDelete', '0');
        $this->db->where('r.isEnable', '1');
        $this->db->where('m.isEnable', '1');
        $this->db->where('r.isTestmode', '0');
        
        if ($month != '') {
            $this->db->where('date_format(date_added,"%b-%Y")', $month);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $mr_cnt = $query->row_array()['mr_count'];
        }

        $this->db->select(" count(r.resellerID) as r_count");
        $this->db->from('tbl_reseller r');
        $this->db->where('r.isEnable', '1');
        $this->db->where('r.isDelete', '0');
        $this->db->where('r.isTestmode', '0');

        if ($month != '') {
            $this->db->where('date_format(r.date,"%b-%Y")', $month);
        }

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $r_cnt = $query->row_array()['r_count'];
        }
        if ($month != '' && $type == '') {
            $c_cnt                 = $this->get_reseller_customer_count($month);
            $revenue_cnt                 = $this->get_merchant_current_month_revenue($month);
        }else{
            $c_cnt  = 0;
            $revenue_cnt = 0;
        }
        
        
        $res['mr_count']       = $mr_cnt;
        $res['reseller_count'] = $r_cnt;
        $res['c_count']        = $c_cnt;
        $res['revenue_count']        = $revenue_cnt;
        return $res;

    }

    public function get_top_reseller_revenue()
    {
        $re_data = array();
        $res     = array();

        $query = $this->db->query("SELECT resellerID ,resellerCompanyName,
	     (select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where
	     tbl_merchant_data.isDelete=0  and tbl_merchant_data.isEnable=1 and tbl_reseller.isDelete=0 and  tbl_reseller.isEnable=1 and  tbl_merchant_data.resellerID=tbl_reseller.resellerID ) as mr_count


		 from `tbl_reseller` WHERE isDelete='0'AND isTestmode = 0 and isEnable='1' ");

        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            foreach ($res as $ress) {

                $revenue = $this->get_rss_revenue($ress['resellerID']);

                $ress['mr_count'] = $revenue['mr_count'];
                $ress['revenue']  = $revenue;
                $ress['volume']   = sprintf('%.2f', $revenue['volume']);

                $re_data[] = $ress;
            }

        }

        return $re_data;

    }

    public function get_rss_revenue($rid)
    {
        $m_datas  = array();
        $m_datas1 = array();
        $rrrr     = array();

        $sql = $this->db->query("
Select  mr.resellerID, t.*, mr.plan_id,p.subscriptionRetail,p.serviceFee,p.planType,
 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "' and plan_id=mr.plan_id and isDelete='0'
 )  as m_count,
 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "'
 and plan_id=mr.plan_id and isDelete='0' and  YEAR(date_added) = YEAR(CURRENT_DATE())
 and MONTH(date_added) = MONTH(CURRENT_DATE())  )  as tm_count
 from  tbl_merchant_data mr
    inner join plans p on p.plan_id=mr.plan_id
    inner join tbl_reseller rs on rs.resellerID=mr.resellerID
    inner join tiers t on rs.tier_id=t.tier_id
    where  mr.isDelete='0' and mr.resellerID='" . $rid . "'
    GROUP BY mr.resellerID, mr.plan_id");

        $m_datas = $sql->result_array();

        $final1       = 0;
        $sr_fee2      = 0;
        $sub_fee2     = 0;
        $final        = 0;
        $totalamount  = 0;
        $tier         = 0;
        $new_merchant = 0;
        $sr_fee       = 0;
        $sr_fee1      = 0;
        $sub_fee      = 0;
        $tomerchnt    = 0;

        if (!empty($m_datas)) {
            $cms = 0;
            foreach ($m_datas as $md) {

                $refund = 0;

                $rev = $this->get_reseller_month_payment($rid, $md['plan_id']);

                $refund = $rev['refund'];
                $totalamount += $rev['rev'] - $refund;
                
                $final += ($md['subscriptionRetail'] * $md['m_count']);

                $new_merchant = ($new_merchant + $md['m_count']);


            }

            $final1 = $final;

            
            $sr_fee2  = $sr_fee1;
            $sub_fee2 = $sub_fee;
           
            $tomerchnt += $md['m_count'];
            $m_datas1['revenue1']    = sprintf('%0.2f', ($final1));
            $m_datas1['ser_fee_new'] = sprintf('%0.2f', ($sr_fee2));
            $m_datas1['sub_fee_new'] = sprintf('%0.2f', ($sub_fee2));
            $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
            $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
            $m_datas1['p_value']     = sprintf('%0.2f', ($sub_fee + $sr_fee1));
            $m_datas1['volume']      = $totalamount;
            $m_datas1['revenue']     = $final;
            $m_datas1['tier_status'] = $tier;
            $m_datas1['newMerchant'] = $new_merchant;
            $m_datas1['mr_count']    = $tomerchnt;
        } else {
            $m_datas1['revenue1']    = sprintf('%0.2f', ($final1));
            $m_datas1['ser_fee_new'] = sprintf('%0.2f', ($sr_fee2));
            $m_datas1['sub_fee_new'] = sprintf('%0.2f', ($sub_fee2));
            $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
            $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
            $m_datas1['p_value']     = 0;
            $m_datas1['volume']      = $totalamount;
            $m_datas1['revenue']     = $final;
            $m_datas1['tier_status'] = $tier;
            $m_datas1['newMerchant'] = $new_merchant;
            $m_datas1['mr_count']    = $tomerchnt;

        }

        return $m_datas1;

    }

    public function get_reseller_month_payment($resellerID, $pln_id, $date = '')
    {

        $res = array();

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as total from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
        inner join plans p on p.plan_id=mr.plan_id

		where  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture"  or  tr.transactionType ="Offline Payment"
    	  	or tr.transactionType ="pay_sale" or tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture"
    	  	or tr.transactionType ="capture" or tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture")
    	  	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
    	  	 and tr.transaction_user_status!="3"
        and tr.resellerID="' . $resellerID . '" and r.isDelete=0 and mr.plan_id="' . $pln_id . '"  ' . $date);

        $query2 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id where

	 (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR
    	  	 UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND"
    	  	 OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")

		and ( tr.transaction_user_status !="3")
         and tr.resellerID="' . $resellerID . '" and r.isDelete=0  and mr.plan_id="' . $pln_id . '" ' . $date);

        if ($query1->num_rows() > 0) {
            $res['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $res['rev'] = '0';
        }

        if ($query2->num_rows() > 0) {

            $res['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $res['refund'] = '0';
        }

        return $res;

    }

    public function chart_get_merchant($type = '')
    {
       

         $m_datas  = array();
         $m_datas1 = array();
         $rrrr     = array();

         $res    = array();
         $months = array();
         $revenue_cnt = 0;
         $volume_cnt = 0;
         
         for ($i = 11; $i >= 0; $i--) {

             $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
             
         }

        
         $chart_arr = array();
         foreach ($months as $key => $month) {
            $final        = 0;
            $cms          = 0;
            $totalamount  = 0.00;
            $tier         = 0;
            $revenue      = 0;
            $new_merchant = 0;

            $tcode  = array('100', '200', '111', '1');
            $amount = 0;
           
            
            $revenueCC = $this->get_creditcard_payment($month);
            $revenueEC = $this->get_echeck_payment($month);
            if($type == 1){
                $revenueSubscription = $this->get_subscription_payment($month);
                $revenueBP = $this->get_basis_point_payment($month);
                $revenueByInvoice = $this->get_invoice_payment($month);
                $revenue = round($revenueByInvoice,2);
                $subRevenue = round($revenueSubscription,2);
                $bpRevenue = round($revenueBP,2);
            }else{
                $revenue = round($revenueCC,2) + round($revenueEC, 2);
                $subRevenue = 0;
                $bpRevenue = 0;
            }
            

            /*Get volumn of do payment by customer*/
           
            $volume               = round($revenue, 2); 

            $m_datas1['volume'][] = floatval($volume);
            $m_datas1['revenue'][] = floatval($revenue);
            $m_datas1['subRevenue'][] = floatval($subRevenue);
            $m_datas1['bpRevenue'][] = floatval($bpRevenue);
            $m_datas1['revenueCC'][] = floatval($revenueCC);
            $m_datas1['revenueEC'][] = floatval($revenueEC);
            $m_datas1['month'][]   = $month;

            $r_dt = $this->admin_count_dashboard($month,'revenue_history');
            if (!empty($r_dt)) {
                 $m_datas1['reseller_no'][] = intval($r_dt['reseller_count']);
                 $m_datas1['merchant'][]    = intval($r_dt['mr_count']);
            } else {
                 $m_datas1['reseller_no'][] = intval($n_dt);
                 $m_datas1['merchant'][]    = intval($m_dt);
            }
            
            
            $revenue_cnt = $revenue_cnt + $revenue;
            $volume_cnt = $volume_cnt + $volume;

         }
         $m_datas1['total_revenue'] = floatval($revenue_cnt);
         $m_datas1['total_volume'] = floatval($volume_cnt);
         return ($m_datas1);
         

    }

    public function get_merchant_payment($date = '')
    {
        $res = array();

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0)  as total from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
        inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (tr.transaction_user_status !="3")
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and  r.isDelete=0 and
             (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or
             cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' .
            $date);

        $query2 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where
	(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")

		and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL)
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and r.isDelete=0

            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' . $date);

        if ($query1->num_rows() > 0) {
            $res['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $res['rev'] = '0';
        }

        if ($query2->num_rows() > 0) {

            $res['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $res['refund'] = '0';
        }

        return $res;

    }

    public function get_total_processing_volume()
    {

        $res        = array();

        // Top 10 Resellers By Volume
        $res['reseller'] = $this->get_reseller_merchant_data(array('key' => 'volume'));
        
        // Top 10 Merchants
        $merchantData = array();
        $query = $this->db->query("SELECT mr.merchID, mr.companyName,COALESCE(SUM(ct.transactionAmount),0) as revenue  FROM tbl_merchant_data mr INNER JOIN tbl_reseller rs ON rs.resellerID=mr.resellerID
            LEFT JOIN customer_transaction ct ON ct.merchantID = mr.merchID
            WHERE rs.isDelete =  0
            AND rs.isEnable =  1
            AND rs.isTestmode =  0
            AND mr.isDelete =  0
            AND mr.isEnable =  1 GROUP BY mr.merchID ORDER BY revenue desc LIMIT 10");

        if ($query->num_rows() > 0) {
            $res_data1 = $query->result_array();
            $revenues = [];
            foreach ($res_data1 as $value) {
                $merchantIDs[] = $merchID = $value['merchID'];

                $revenue1 = $this->db->query("select COALESCE(SUM(ct2.transactionAmount),0) as amount from customer_transaction ct2 inner join tbl_merchant_data mr2 ON mr2.merchID = ct2.merchantID  where ct2.merchantID = $merchID and mr2.isDelete = 0 and mr2.isEnable = 1 and ((UPPER(ct2.transactionType) IN ( 'SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT' ) and ct2.transactionCode IN (100, 200, 111, 1) ) and (ct2.transaction_user_status !='3'))")->row()->amount;
                
                $revenue2 = $this->db->query("select COALESCE(SUM(ct1.transactionAmount),0) as amount from customer_transaction ct1 inner join tbl_merchant_data mr2 ON mr2.merchID = ct1.merchantID where ct1.merchantID = $merchID and mr2.isDelete = 0 and mr2.isEnable = 1 and ((UPPER(ct1.transactionType) IN ('REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'STRIPE_REFUND', 'CREDIT') and ct1.transactionCode IN (100, 200, 111, 1) ) OR ct1.transactionCode IS NULL) and (ct1.transaction_user_status !='2' or ct1.transaction_user_status !='3') or ct1.transaction_user_status IS NULL")->row()->amount;

                $revenues[$merchID] = $revenue1 - $revenue2;
            }

            // Get Merchant Revenue
            $query3 = $this->db->query("SELECT mr.merchID, mr.companyName , 0 AS revenue FROM tbl_merchant_data mr INNER JOIN tbl_reseller rs ON rs.resellerID=mr.resellerID
            LEFT JOIN customer_transaction ct ON ct.merchantID = mr.merchID
            WHERE rs.isDelete =  0
            AND mr.merchID IN (".implode(',', $merchantIDs).")
            AND rs.isEnable =  1
            AND rs.isTestmode =  0
            AND mr.isDelete =  0
            AND mr.isEnable =  1
            GROUP BY mr.merchID ORDER BY revenue DESC");

            if($query3->num_rows()){
                $merchantData = $query3->result_array();

                // Fetch Merchant Customer Count
                foreach($merchantData as $k=>$value){
                    $merchantData[$k]['revenue'] = $revenues[$value['merchID']];
                    $merchantData[$k]['customer_count'] = $this->customer_count_merchant($value['merchID']);
                }
                
            }
        }

        usort($merchantData, function($a, $b)
        {
            if ($a['revenue'] == $b['revenue']) {
                return 0;
            }
            return ($a['revenue'] > $b['revenue']) ? -1 : 1;
        });

        $res['merchant']   =  $merchantData;
        $res['plan_ratio'] = [];

        return $res;
    }

    public function get_reseller_merchant_data($order_by)
    {
        $tcode    = array('100', '200', '111', '1');
        $type     = " 'SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT' ";
        $res_data = array();

        $query = $this->db->query("SELECT rs.resellerID, rs.resellerCompanyName,COALESCE(SUM(ct.transactionAmount),0) as volume FROM tbl_reseller rs LEFT JOIN customer_transaction ct ON ct.resellerID = rs.resellerID LEFT JOIN tbl_merchant_data mr ON (ct.merchantID = mr.merchID AND mr.isDelete = 0 AND mr.isEnable = 1) WHERE rs.isDelete = 0 AND rs.isEnable = 1 AND rs.isTestmode = 0  GROUP BY rs.resellerID ORDER BY volume desc LIMIT 10");
        
        if ($query->num_rows() > 0) {
            
            $resellerList = $query->result_array();

            $resellerIDs = [];
            foreach ($resellerList as $value) {
                $resellerIDs[] = $value['resellerID'];
            }

            $query1 = $this->db->query("SELECT rs.resellerID, rs.resellerCompanyName,((select COALESCE(SUM(ct2.transactionAmount),0) from customer_transaction ct2  where ct2.resellerID = rs.resellerID  and rs.isDelete=0 and rs.isEnable=1 and ((UPPER(ct2.transactionType) IN ( 'SALE','CAPTURE','AUTH_CAPTURE','PRIOR_AUTH_CAPTURE','PAY_SALE','PAY_CAPTURE','STRIPE_SALE','STRIPE_CAPTURE','PAYPAL_SALE','PAYPAL_CAPTURE','OFFLINE PAYMENT' ) and ct2.transactionCode IN (100, 200, 111, 1) ) and (ct2.transaction_user_status !='3')) ) - (select COALESCE(SUM(ct1.transactionAmount),0) from customer_transaction ct1 where ct1.resellerID = rs.resellerID and rs.isDelete=0 and rs.isEnable=1 and ((UPPER(ct1.transactionType) IN ('REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'STRIPE_REFUND', 'CREDIT') and ct1.transactionCode IN (100, 200, 111, 1) ) OR ct1.transactionCode IS NULL) and (ct1.transaction_user_status !='2' or ct1.transaction_user_status !='3') or ct1.transaction_user_status IS NULL )) as volume,(select count(merchID) from tbl_merchant_data mr where resellerID = rs.resellerID  and isDelete=0 and isEnable=1) as mr_count FROM tbl_reseller rs LEFT JOIN tbl_merchant_data mr ON mr.resellerID = rs.resellerID WHERE rs.resellerID IN (".implode(',', $resellerIDs).") and rs.isDelete = 0 AND rs.isEnable = 1 AND rs.isTestmode = 0 AND mr.isDelete = 0 AND mr.isEnable = 1 GROUP BY rs.resellerID ORDER BY volume DESC");

            $res_data = $query1->result_array();
        }
        return $res_data;
    }

    public function customer_count_merchant($merchantID)
    {

        $count = 0;
        $data  = array();
        $query = $this->db->query("Select * from app_integration_setting where merchantID='" . $merchantID . "' ");
        $row   = $query->row_array();
        if (!empty($row)) {
            if ($row['appIntegration'] == '1') {

                $qur = $this->db->query("select count(QBW.Customer_ListID) as customer_count from QBO_custom_customer QBW INNER JOIN tbl_merchant_data MD ON QBW.merchantID = MD.merchID INNER JOIN tbl_reseller RS ON MD.resellerID = RS.resellerID AND RS.isTestmode = 0 where QBW.merchantID='" . $merchantID . "'  ");

            }
            if ($row['appIntegration'] == '2') {
                $qur = $this->db->query("select count(QTC.qb_cust_id) as customer_count from qb_test_customer QTC inner join tbl_company TCD on QTC.companyID= TCD.id INNER JOIN tbl_merchant_data MD ON TCD.merchantID = MD.merchID INNER JOIN tbl_reseller RS ON MD.resellerID = RS.resellerID AND RS.isTestmode = 0 where TCD.merchantID='" . $merchantID . "' and  QTC.customerStatus=1 ");

            }
            if ($row['appIntegration'] == '3') {
                $qur = $this->db->query("select count(FCC.Customer_ListID) as customer_count from Freshbooks_custom_customer FCC inner join tbl_freshbooks TFS on FCC.merchantID = TFS.merchantID INNER JOIN tbl_merchant_data MD ON FCC.merchantID = MD.merchID INNER JOIN tbl_reseller RS ON MD.resellerID = RS.resellerID AND RS.isTestmode = 0 where FCC.merchantID='" . $merchantID . "'  ");

            }
            if ($row['appIntegration'] == '4') {
                $qur = $this->db->query("select count(XCC.Customer_ListID) as customer_count from Xero_custom_customer XCC inner join  tbl_xero_token TXTC on XCC.merchantID = TXTC.merchantID INNER JOIN tbl_merchant_data MD ON XCC.merchantID = MD.merchID INNER JOIN tbl_reseller RS ON MD.resellerID = RS.resellerID AND RS.isTestmode = 0 where XCC.merchantID='" . $merchantID . "'  ");

            }
            if ($row['appIntegration'] == '5') {
                $qur = $this->db->query("select count(CTC.ListID) as customer_count from chargezoom_test_customer CTC inner join tbl_company TCD on CTC.companyID= TCD.id INNER JOIN tbl_merchant_data MD ON TCD.merchantID = MD.merchID INNER JOIN tbl_reseller RS ON MD.resellerID = RS.resellerID AND RS.isTestmode = 0 where TCD.merchantID='" . $merchantID . "' and  CTC.customerStatus=1 ");
            }
            if ($qur->num_rows() > 0) {

                $count = $qur->row_array()['customer_count'];

            } else {

                $count = 0;

            }
        } else {

            $count = 0;

        }
        return intval($count);

    }

    /*************END****************/

    /********Reseller LIST OR Mercahnt List*********/

    public function reasign_marchent()
    {
        $this->db->select('*');
        $this->db->from('tbl_reseller');
        $this->db->where('isEnable', '1');
        $this->db->where('isDelete', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        } else {
            return false;
        }

    }

    public function disable_admin_reseller()
    {
        $this->db->select('*');
        $this->db->from('tbl_reseller');
        $this->db->where('isEnable', '0');
        $this->db->where('isDelete', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        }
    }

    public function get_admin_search_merchant($key)
    {
        $res = array();
        $this->db->select(' mr.*, p.plan_name as plan, (select count(*) from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as customer_count,
		(select sum(-AppliedAmount) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID  and   MONTH(CURDATE()) = MONTH(qb_test_invoice.TimeModified) AND YEAR(CURDATE()) = YEAR(qb_test_invoice.TimeModified)  ) as applied,
		(select sum(BalanceRemaining) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as balance,
		rs.resellerfirstName');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
        $this->db->join('plans p', 'p.plan_id = mr.plan_id', 'LEFT');
        $this->db->join('tbl_company cmp', 'mr.merchID = cmp.merchantID', 'LEFT');
        $this->db->join('qb_test_customer cust', 'cust.companyID = cmp.id', 'LEFT');
        $this->db->join('qb_test_invoice inv', 'cust.ListID = inv.Customer_ListID', 'LEFT');
        $this->db->where('mr.isDelete = 0');
        $this->db->where("mr.firstName Like '%" . $key . "%' or  mr.lastName  Like  '%" . $key . "%' or mr.companyName Like '%" . $key . "%' or p.plan_name Like  '%" . $key . "%' ");
        $this->db->group_by('mr.merchID');
        $query = $this->db->get();

        if ($query->num_rows()) {

            $res = $query->result_array();
        }
       

        return $res;

    }

    public function get_merchant_plan_data($pid)
    {
        $res = array();
        $this->db->select('mr.merchID, mr.companyName,rs.resellerCompanyName,p.plan_id, p.plan_name');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'inner');
        $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'inner');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');
        $this->db->where('mr.isSuspend', '0');
        $this->db->where('mr.plan_id', $pid);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;

    }

    public function get_marchent($id)
    {
        $this->db->select('*');
        $this->db->from('tbl_merchant_data mr');
        $this->db->where('resellerID', $id);
        $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'inner');
        $this->db->where('isEnable', 1);
        $this->db->where('isDelete', 0);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
            return $res;
        }

    }

    /******************END***********/

    public function get_data_admin_reseller()
    {
        $res  = array();
        $rrrr = array();

        $query = $this->db->query('SELECT `rs`.*, (select sum(-AppliedAmount) from qb_test_invoice
    inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID
    inner join tbl_company on qb_test_customer.companyID=tbl_company.id
    inner join tbl_merchant_data on tbl_company.merchantID=tbl_merchant_data.merchID
    where rs.resellerID=tbl_merchant_data.resellerID and MONTH(CURDATE()) = MONTH(qb_test_invoice.TimeModified) AND YEAR(CURDATE()) = YEAR(qb_test_invoice.TimeModified) ) as applied, (select count(*) from tbl_customer_tansaction inner join qb_test_customer on qb_test_customer.ListID=tbl_customer_tansaction.customerListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id inner join tbl_merchant_data on tbl_company.merchantID=tbl_merchant_data.merchID where rs.resellerID=tbl_merchant_data.resellerID and MONTH(CURDATE()) = MONTH(tbl_customer_tansaction.transactionDate) AND YEAR(CURDATE()) = YEAR(tbl_customer_tansaction.transactionDate))as transaction, (select sum(tr.transactionAmount) from tbl_customer_tansaction tr inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id inner join tbl_merchant_data mr on cmp.merchantID=mr.merchID where rs.resellerID = mr.resellerID and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or tr.transactionType ="pay_sale" or tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or tr.transactionType ="capture" or tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") and tr.transaction_user_status !="refund" and tr.transactionID!="" and cust.customerStatus="1" and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as total_amount, (select sum(tr.transactionAmount) from tbl_customer_tansaction tr inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id inner join tbl_merchant_data mr on cmp.merchantID=mr.merchID where rs.resellerID = mr.resellerID and cust.customerStatus="1" AND (tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "credit" ) and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") and tr.transaction_user_status !="refund" and tr.transactionID!="" and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as refund_amount, (select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_reseller.resellerID=mr.resellerID and tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and MONTH(CURDATE()) != MONTH(Date_Format(tbl_merchant_data.date_added,"%Y/%m/%d"))) as merchant, (select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_merchant_data.resellerID IN (SELECT tbl_reseller.resellerID FROM tbl_reseller WHERE tbl_reseller.isDelete = 0 GROUP BY tbl_reseller.resellerID) AND tbl_reseller.resellerID=tbl_merchant_data.resellerID and tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and MONTH(CURDATE()) = MONTH(Date_Format(tbl_merchant_data.date_added,"%Y/%m/%d")) AND YEAR(CURDATE()) = YEAR(Date_Format(tbl_merchant_data.date_added,"%Y/%m/%d"))) as newMerchant FROM (`tbl_reseller` rs) LEFT JOIN `tbl_merchant_data` mr ON `rs`.`resellerID` = `mr`.`resellerID` LEFT JOIN `tbl_company` cmp ON `mr`.`merchID` = `cmp`.`merchantID` LEFT JOIN `qb_test_customer` cust ON `cust`.`companyID` = `cmp`.`id` WHERE `rs`.`isDelete` = 0 AND `rs`.`isEnable` = 1 GROUP BY `rs`.`resellerID`');

        if ($query->num_rows()) {

            $res = $query->result_array();


            foreach ($res as $ress) {
                $reven       = 0;
                $vol         = 0;
                $totalamount = 0;
                $total       = 0;
                $sql         = $this->db->query('SELECT trs.*,(SELECT sum(transactionAmount) FROM `customer_transaction` WHERE resellerID = "' . $ress['resellerID'] . '" AND transactionCode IN (100, 1, 200, 111) AND transactionDate BETWEEN date_format(LAST_DAY(now() - interval 1 month),"%Y-%m-01 24:00:00") AND date_format(LAST_DAY(now() - interval 1 month ),"%Y-%m-%d 00:00:00"))  as total_amount1  FROM tiers trs WHERE trs.tier_id ="' . $ress['tier_id'] . '"');

                $m_datas = $sql->result_array();


                if (!empty($m_datas)) {

                    foreach ($m_datas as $m_data) {
                        if ($m_data['total_amount1'] != "") {
                            if ($ress['merchant'] != 0) {
                                $totalamount = $m_data['total_amount1'];

                                $allmerchant = $ress['merchant'];
                                $newmerchant = $ress['newMerchant'];

                                if ($newmerchant <= $m_data['tier1_min_new_account']) {
                                    $tiercom = $totalamount * ($m_data['tier1_commission'] / 100);
                                } else if ($newmerchant <= $m_data['tier2_min_new_account']) {
                                    $tiercom = $totalamount * ($m_data['tier2_commission'] / 100);
                                } else {
                                    $tiercom = $totalamount * ($m_data['tier3_commission'] / 100);
                                }

                                
                                $total += $tiercom;
                            }
                        }
                    }
                }
                $ress['volume']      = $vol;
                $ress['revenue']     = $total;
                $ress['totalamount'] = $totalamount;

                $rrrr[] = $ress;
            }

        }

        return $rrrr;

    }

    public function get_data_admin_merchant()
    {
        $res   = array();
        $rest1 = array();
        $this->db->select('mr.merchID,mr.companyName,mr.isSuspend, mr.resellerID, mr.isDelete,p.plan_name as plan,rs.resellerCompanyName');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
        $this->db->join('plans p', 'p.plan_id = mr.plan_id', 'inner');
        $this->db->where('mr.isDelete = 0');
        
        $query = $this->db->get();
        if ($query->num_rows()) {

            $res = $query->result_array();
            foreach ($res as $ress) {
                $rest = $ress;

                $q1 = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

	  	where tr.merchantID="' . $ress['merchID'] . '" and
	  	(tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture"  or  tr.transactionType ="Offline Payment"
	  	or tr.transactionType ="pay_sale" or tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture"
	  	or tr.transactionType ="capture" or tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture")
	  	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	  	 and tr.transaction_user_status!="3"
	  	and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ');

                $q2 = $this->db->query('select sum(tr.transactionAmount) as refund
	  	from customer_transaction tr
	  	where tr.merchantID ="' . $ress['merchID'] . '"   and
	  (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
	  	and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
	  	and  tr.transaction_user_status!="3"
	  	and MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)');

                $qq = $this->db->query("Select gatewayFriendlyName from tbl_merchant_gateway where merchantID='" . $ress['merchID'] . "' and set_as_default='1' ");
                if ($qq->num_rows() > 0) {
                    $rest['gatewayFriendlyName'] = $qq->row_array()['gatewayFriendlyName'];
                } else {
                    $rest['gatewayFriendlyName'] = '';
                }

                if ($q1->num_rows() > 0) {
                    $rest['total_amount'] = $q1->row_array()['total'];
                } else {
                    $rest['total_amount'] = '';
                }
                if ($q2->num_rows() > 0) {
                    $rest['refund_amount'] = $q2->row_array()['refund'];
                } else {
                    $rest['refund_amount'] = '';
                }

                $rest1[] = $rest;
            }
        }

        return $rest1;

    }

    public function get_merchant_billing_data($invID)
    {
        $res   = array();
        $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");

        if ($query->num_rows() > 0) {
            $res = $query->row_array();
            return $res;
        } else {
            return $res;
        }

    }

    public function get_gateway_listing($merchantID)
    {
        $res   = array();
        $query = $this->db->query("Select gt.* from tbl_admin_gateway gt inner join tbl_merchant_data mr on mr.merchID=gt.'$merchantID' ");
        $query = $this->db->get();
        if ($query->num_rows()) {

            $res = $query->result_array();
        }

        return $res;

    }

    public function get_subscription_data()
    {

      $res   = array();
      $today = date('Y-m-d');

      $today = $today;

      $date = date('d');
      $this->db->select('mr.*, p.*, gt.*');
      $this->db->from('tbl_merchant_data mr');
      $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'INNER');
      $this->db->join('tbl_admin_gateway gt', 'gt.gatewayID=mr.gatewayID', 'INNER');
      $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'INNER');
      $this->db->where('mr.isDelete', '0');
      $this->db->where('rs.isDelete', '0');

      $query = $this->db->get();
      if ($query->num_rows() > 0) {
          return $res = $query->result_array();
      }
      return $res;
    }

    public function customer_count($mID)
    {
        $c_count = 0;
        $query   = $this->db->query("select count(*) as c_count from qb_test_customer INNER join tbl_company on tbl_company.id=qb_test_customer.companyID AND tbl_company.merchantID= '" . $mID . "' INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and  qb_test_customer.customerStatus=1 and MONTH(CURDATE()) != MONTH(Date_Format(qb_test_customer.TimeCreated,'%Y/%m/%d')) ");

        $c_count = $query->row_array()['c_count'];
        return $c_count;

    }
    public function get_total_amount($mID)
    {
        $amount = 0;
        $query  = $this->db->query("select sum(-AppliedAmount) as total_amount from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id  inner join tbl_merchant_data on tbl_company.merchantID=tbl_merchant_data.merchID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and  qb_test_customer.customerStatus=1 and    MONTH(CURDATE()) = MONTH(qb_test_invoice.TimeModified) AND YEAR(CURDATE()) = YEAR(qb_test_invoice.TimeModified)");

        $amount = $query->row_array()['total_amount'];
        return $amount;

    }

    public function get_subscription_listing()
    {
        $res = array();

        $this->db->select('mb.*, rs.resellerFirstName, rs.lastName, mr.firstName, mr.lastName,pl.* ');
        $this->db->from('merchant_subscription mb');
        $this->db->join('tbl_merchant_data mr', 'mb.merchantID = mr.merchID', 'inner');
        $this->db->join('plans pl', 'mr.plan_id = pl.plan_id', 'inner');
        $this->db->join('tbl_reseller rs', 'mr.resellerID = rs.resellerID', 'inner');
        $this->db->where('rs.isDelete', '0');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mb.isDelete', '0');
        $query = $this->db->get();
        if ($query->num_rows()) {

            $res = $query->result_array();
        }

        return $res;

    }

    public function get_invoice_listing($sID)
    {
        $sql = ' Select tmb.*, agt.gatewayFriendlyName from tbl_merchant_billing_invoice tmb inner join tbl_admin_gateway agt on tmb.gatewayID=agt.gatewayID  where tmb.merchantSubscriptionID="' . $sID . '"';

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function get_invoice_data_auto_pay()
    {

        $today = date('Y-m-d');

        $sql = "select bl.*, gt.*, mr.* from tbl_merchant_billing_invoice bl inner join tbl_admin_gateway gt on bl.gatewayID=gt.gatewayID inner join tbl_merchant_data mr on bl.merchantID=mr.merchID   where bl.status='pending' and DATE_FORMAT(bl.DueDate,'%Y-%m-%d') <='" . $today . "'";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function get_charge_subscription_data()
    {

        $res   = array();
        $today = date('Y-m-d');
        $date  = date('d');
        $this->db->select('tmb.*, LAST_DAY(tmb.invoiceGenerationDate) as chargeDate ,mr.firstName,mr.lastName, p.*, gt.*');
        $this->db->from('merchant_subscription tmb');
        $this->db->join('tbl_merchant_data mr', 'mr.merchID=tmb.merchantID', 'INNER');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=mr.resellerID', 'INNER');
        $this->db->join('tbl_admin_gateway gt', 'gt.gatewayID=tmb.gatewayID', 'INNER');
        $this->db->join('plans p', 'p.plan_id=mr.plan_id', 'INNER');
        $this->db->where("tmb.invoiceGenerationDate = '" . $today . "' ");
        $this->db->where('mr.isDelete', '0');
        $this->db->where('rs.isDelete', '0');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }

    public function login_as_mecrchant($merchID)
    {
        $this->db->where('merchID', $merchID);
        $this->db->update('tbl_merchant_data', array(
            'is_logged_in' => '1',
        ));
        return true;
    }

    public function new_customer_count($mID)
    {
        $c_count = 0;
        $query   = $this->db->query("select count(*) as c_count from qb_test_customer INNER join tbl_company on tbl_company.id=qb_test_customer.companyID AND tbl_company.merchantID= '" . $mID . "' INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and  qb_test_customer.customerStatus=1 and MONTH(CURDATE()) = MONTH(Date_Format(qb_test_customer.TimeCreated,'%Y/%m/%d')) AND YEAR(CURDATE()) = YEAR(Date_Format(qb_test_customer.TimeCreated,'%Y/%m/%d'))");

        $c_count = $query->row_array()['c_count'];
        return $c_count;

    }

    public function merchant_count($mID)
    {
        $c_count = 0;
        $query   = $this->db->query("SELECT `rs`.`resellerID`, (select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_reseller.resellerID=mr.resellerID and tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and MONTH(CURDATE()) != MONTH(Date_Format(tbl_merchant_data.date_added,'%Y/%m/%d'))) as merchant FROM (`tbl_reseller` rs) LEFT JOIN `tbl_merchant_data` mr ON `rs`.`resellerID` = `mr`.`resellerID` LEFT JOIN `tbl_company` cmp ON `mr`.`merchID` = `cmp`.`merchantID` LEFT JOIN `qb_test_customer` cust ON `cust`.`companyID` = `cmp`.`id` WHERE `rs`.`isDelete` = 0 GROUP BY `rs`.`resellerID` ");

        $c_count = $query->row_array()['c_count'];
        return $c_count;

    }

    public function new_merchant_count($mID)
    {
        $c_count = 0;
        $query   = $this->db->query("select count(*) as c_count from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_merchant_data.resellerID IN (SELECT tbl_merchant_data.resellerID FROM tbl_merchant_data WHERE tbl_merchant_data.isDelete = 0 AND tbl_merchant_data.merchID = '" . $mID . "') AND tbl_reseller.resellerID=tbl_merchant_data.resellerID and tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and MONTH(CURDATE()) = MONTH(Date_Format(tbl_merchant_data.date_added,'%Y/%m/%d')) AND YEAR(CURDATE()) = YEAR(Date_Format(tbl_merchant_data.date_added,'%Y/%m/%d'))");

        $c_count = $query->row_array()['c_count'];
        return $c_count;

    }

    public function get_month_total_amount($mID, $dt = '')
    {

        $amount = 0;
        
        $rev = $this->merchant_total_count($mID, $dt);

        $amount = $rev['total'] - $rev['refund'];
        return $amount;

    }

    public function merchant_total_count($merchantID, $dt)
    {
        $count = 0;
        $data  = array();
        $query = $this->db->query("Select * from app_integration_setting where merchantID='" . $merchantID . "' ");

        $row = $query->row_array();
        if ($dt != '') {
            $d_con = 'transactionDate BETWEEN DATE_ADD(date_format(LAST_DAY(now() - interval 1 month),"%Y-%m-01"),INTERVAL ' . $dt . ' DAY) AND date_format(LAST_DAY(now() - interval 1 month ),"%Y-%m-%d")';
        } else {
            $d_con = 'tr.transactionDate BETWEEN date_format(LAST_DAY(now() - interval 1 month),"%Y-%m-01") AND date_format(LAST_DAY(now() - interval 1 month ),"%Y-%m-%d")';
        }
        if (!empty($row)) {
            if ($row['appIntegration'] == '1') {

               

                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr


		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (  tr.transaction_user_status!="3")   and
		' . $d_con . '  and   tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (   tr.transaction_user_status!="3")  and ' . $d_con . '   and  tr.merchantID="' . $merchantID . '"  ');

                

            }
            if ($row['appIntegration'] == '2') {
                $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where mr1.merchID = cmp.merchantID and   (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3" )    and
		' . $d_con . '  and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")   and
		' . $d_con . '  and     tr.merchantID="' . $merchantID . '"  ');
            }
            if ($row['appIntegration'] == '3') {
                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or   tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3" )     and
		' . $d_con . '   and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr
			inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and  ( tr.transaction_user_status !="3" )   and
		' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

            }
            if ($row['appIntegration'] == '4') {
                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and  ( tr.transaction_user_status !="3")   and
			' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")   and
			' . $d_con . '  and    tr.merchantID="' . $merchantID . '"  ');

            }

            if ($rquery->num_rows() > 0) {
                $data['total'] = $rquery->row_array()['total'];

                $data['refund'] = $rfquery->row_array()['rm_amount'];
            } else {
                $data['total'] = 0;

                $data['refund'] = 0;
            }
        }
        return $data;

    }

    public function get_data_admin_dashboard()
    {
        $res = array();

        /****************  To get All total reseller count and Total merchant count From Stating of month to current date ****************/

        $query = $this->db->query("SELECT count(*) as reseller_count ,

	     (select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where
	     tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 && ( MONTH(CURDATE()) = MONTH(tbl_merchant_data.date_added) AND YEAR(CURDATE()) = YEAR(tbl_merchant_data.date_added)
) )as mr_count

		 from `tbl_reseller` WHERE isDelete='0' && ( MONTH(CURDATE()) = MONTH(tbl_reseller.date) AND YEAR(CURDATE()) = YEAR(tbl_reseller.date)) ");

      

        if ($query->num_rows() > 0) {
            $res            = $query->row_array();
            $res['c_count'] = $this->get_merchant_count();

            /* To get only current month customer transaction */
            $res['tr_count'] = $this->db->query('Select * from customer_transaction WHERE  MONTH(CURDATE()) = MONTH(customer_transaction.transactionDate) AND YEAR(CURDATE()) = YEAR(customer_transaction.transactionDate) ')->num_rows();

            
            return $res;
        } else {

            return $res;
        }

    }

    public function get_merchant_count()
    {
        $count = 0;
        $data  = array();

       
        /************************************* To get totel merchant of the current month ************************************/
        $query = $this->db->query("select merchID from tbl_merchant_data inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where
	     tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 && ( MONTH(CURDATE()) = MONTH(tbl_merchant_data.date_added) AND YEAR(CURDATE()) = YEAR(tbl_merchant_data.date_added)
        ) ");
        $re_array = $query->result_array();


        $tot_merch = 0;
        if (!empty($re_array)) {

            foreach ($re_array as $re_data) {
                $merchantID     = $re_data['merchID'];
                $total_merchant = $this->merchant_customer_count($merchantID);
                
                $tot_merch += $total_merchant['count'];
            }
        }

        return $tot_merch;

    }
    public function merchant_customer_count($merchantID)
    {
        $count = 0;
        $data  = array();
        $query = $this->db->query("Select * from app_integration_setting where merchantID='" . $merchantID . "' ");
        $row   = $query->row_array();
        if (!empty($row)) {
            if ($row['appIntegration'] == '1') {

                
                $qur = $this->db->query("select count(*) as customer_count from QBO_custom_customer  where QBO_custom_customer.merchantID='" . $merchantID . "'  ");

                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3" )   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (  tr.transaction_user_status !="3")  and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  tr.merchantID="' . $merchantID . '"  ');

               

            }
            if ($row['appIntegration'] == '2') {
                $qur    = $this->db->query("select count(*) as customer_count from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID='" . $merchantID . "' and  qb_test_customer.customerStatus=1 ");
                $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr
	    inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3" )    and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and     tr.merchantID="' . $merchantID . '"  ');
            }
            if ($row['appIntegration'] == '3') {
                $qur = $this->db->query("select count(*) as customer_count from Freshbooks_custom_customer inner join tbl_freshbooks on Freshbooks_custom_customer.merchantID=tbl_freshbooks.merchantID where Freshbooks_custom_customer.merchantID='" . $merchantID . "'  ");

                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or   tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (tr.transaction_user_status !="3" )     and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and  (tr.transaction_user_status !="3")   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   tr.merchantID="' . $merchantID . '"  ');

            }
            if ($row['appIntegration'] == '4') {
                $qur    = $this->db->query("select count(*) as customer_count from Xero_custom_customer inner join  tbl_xero_token  on Xero_custom_customer.merchantID=tbl_xero_token.merchantID where Xero_custom_customer.merchantID='" . $merchantID . "'  ");
                $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and  (tr.transaction_user_status !="3")   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and     tr.merchantID="' . $merchantID . '"  ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and     tr.merchantID="' . $merchantID . '"  ');

            }
            if ($row['appIntegration'] == '5') {
                $qur = $this->db->query("select count(*) as customer_count from chargezoom_test_customer
            inner join tbl_company on chargezoom_test_customer.companyID=tbl_company.id where tbl_company.merchantID='" . $merchantID . "' and  chargezoom_test_customer.customerStatus=1 ");
                $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr
	    inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID
		where (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3" )    and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '" ');

                $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr

		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and ( tr.transaction_user_status !="3")   and
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and     tr.merchantID="' . $merchantID . '"  ');
            }
            if ($qur->num_rows() > 0) {
                $data['total'] = $rquery->row_array()['total'];
                $data['count'] = $qur->row_array()['customer_count'];

                $data['refund'] = $rfquery->row_array()['rm_amount'];
            } else {
                $data['total']  = 0;
                $data['count']  = 0;
                $data['refund'] = 0;
            }
        } else {
            $data['total']  = 0;
            $data['count']  = 0;
            $data['refund'] = 0;
        }
        return $data;

    }

    public function get_reseller_payment($pln_id, $date = '')
    {
        $res = array();

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
        inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
		and (tr.transaction_user_status !="3")
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and  r.isDelete=0 and mr.plan_id="' . $pln_id . '"
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' .
            $date);

        $query2 = $this->db->query('select sum(tr.transactionAmount) as refund from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID
		where
	(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")

		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status !="success") or tr.transaction_user_status IS NULL)
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and r.isDelete=0  and mr.plan_id="' . $pln_id . '"

            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ' . $date);

        if ($query1->num_rows() > 0) {
            $res['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $res['rev'] = '0';
        }

        if ($query2->num_rows() > 0) {

            $res['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $res['refund'] = '0';
        }

        return $res;

    }

    public function get_reseller_transaction($resellerID, $pln_id, $date = '')
    {
        $res = 0;

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $query1 = $this->db->query('select count(*) as total  from customer_transaction tr
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id

		where
        tr.resellerID="' . $resellerID . '" and   UPPER(tr.transactionType)!="OFFLINE PAYMENT"  and r.isDelete="0" and mr.plan_id="' . $pln_id . '"    ' . $date . 'Group by tr.transactionID');


        if ($query1->num_rows() > 0) {
            $res = $query1->row_array()['total'];
        } else {
            $res = '0';
        }

        return $res;

    }

    public function get_data_admin_merchant_list()
    {
        $res = array();
        $this->db->select(' mr.*');

        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');

        $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'inner');
        $this->db->join('Config_merchant_portal pr', 'rs.resellerID = pr.resellerID', 'LEFT');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('rs.isDelete', '0');

        $this->db->group_by('mr.merchID');
        $query = $this->db->get();

        if ($query->num_rows()) {

            $res1 = $query->result_array();

            foreach ($res1 as $reus) {

                $tdata                  = $this->merchant_customer_count($reus['merchID']);
                $reus['customer_count'] = $tdata['count'];
                $reus['total_amount']   = $tdata['total'];
                $reus['refund_amount']  = $tdata['refund'];
                $reus['revenue']        = sprintf('%.2f', ($tdata['total'] - $tdata['refund']));

                $res[] = $reus;
            }


        }

        return $res;

    }

    public function get_rss_month_revenue($rid, $month)
    {
        $m_datas  = array();
        $m_datas1 = array();
        $rrrr     = array();

        if ($month != '') {
            $month = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $month = ' and  MONTH(CURDATE()) = MONTH(date_added) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }

        $sql = $this->db->query("
Select  mr.resellerID, t.*, mr.plan_id,p.subscriptionRetail,p.serviceFee,p.planType,
 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "' and plan_id=mr.plan_id and isDelete='0'
 and  YEAR(date_added) = YEAR(CURRENT_DATE()) and MONTH(date_added) = MONTH(CURRENT_DATE()))  as m_count,
 (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "' and plan_id=mr.plan_id and isDelete='0' and
 YEAR(date_added) = YEAR(CURRENT_DATE()) and MONTH(date_added) = MONTH(CURRENT_DATE())  )  as tm_count
 from  tbl_merchant_data mr
inner join plans p on p.plan_id=mr.plan_id
inner join tbl_reseller rs on rs.resellerID=mr.resellerID
inner join tiers t on rs.tier_id=t.tier_id
where  mr.isDelete='0' and mr.resellerID='" . $rid . "'
GROUP BY mr.resellerID, mr.plan_id");

        $m_datas = $sql->result_array();

        $final        = 0;
        $totalamount  = 0;
        $tier         = 0;
        $new_merchant = 0;
        $sr_fee1      = 0;
        $sub_fee      = 0;

        if (!empty($m_datas)) {

            foreach ($m_datas as $md) {
                $refund = 0;

                $rev    = $this->get_reseller_month_payment($rid, $md['plan_id']);
                $refund = $rev['refund'];
                $totalamount += $rev['rev'] - $refund;

                if ($md['planType'] == 2) {
                    $total_transaction = $this->get_reseller_transaction($rid, $md['plan_id']);
                    $sr_fee            = (($md['serviceFee']) * ($total_transaction)) + ($md['subscriptionRetail'] * $md['tm_count']);

                    $sr_fee1 += (($md['serviceFee']) * ($total_transaction));
                    $sub_fee += ($md['subscriptionRetail'] * $md['tm_count']);

                } else
                if ($md['planType'] == 1) {

                    $sr_fee1 += ($md['serviceFee'] / 100) * ($rev['rev'] - $refund);
                    $sub_fee += $md['subscriptionRetail'] * $md['tm_count'];

                    $sr_fee = (($md['serviceFee'] / 100) * ($rev['rev'] - $refund)) + ($md['subscriptionRetail'] * $md['tm_count']);

                }

                $new_merchant = ($new_merchant + $md['m_count']);

                $final += $sr_fee;

            }

            if ($new_merchant >= $m_datas[0]['tier1_min_new_account'] && $new_merchant < $m_datas[0]['tier2_min_new_account']) {
                $cms  = $m_datas[0]['tier1_commission'];
                $tier = 1;
            }
            if ($new_merchant >= $m_datas[0]['tier2_min_new_account'] && $new_merchant < $m_datas[0]['tier3_min_new_account']) {
                $cms  = $m_datas[0]['tier2_commission'];
                $tier = 2;
            }
            if ($new_merchant >= $m_datas[0]['tier3_min_new_account']) {
                $cms  = $m_datas[0]['tier3_commission'];
                $tier = 3;
            }

            $final = ($final * $cms) / 100;

            $sr_fee1 = ($sr_fee1 * $cms) / 100;
            $sub_fee = ($sub_fee * $cms) / 100;

            $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
            $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
            $m_datas1['p_value']     = sprintf('%0.2f', ($sub_fee + $sr_fee1));
            $m_datas1['volume']      = $totalamount;
            $m_datas1['revenue']     = $final;
            $m_datas1['tier_status'] = $tier;
            $m_datas1['newMerchant'] = $new_merchant;
        } else {
            $m_datas1['ser_fee']     = sprintf('%0.2f', ($sr_fee1));
            $m_datas1['sub_fee']     = sprintf('%0.2f', ($sub_fee));
            $m_datas1['p_value']     = 0;
            $m_datas1['volume']      = $totalamount;
            $m_datas1['revenue']     = $final;
            $m_datas1['tier_status'] = $tier;
            $m_datas1['newMerchant'] = $new_merchant;

        }

        return $m_datas1;

    }

    public function get_merchant_invoices($merchID = '')
    {
        if ($merchID != '') {
            $query = $this->db->query("Select inv.*, mr.merchID, mr.resellerID  from  tbl_merchant_billing_invoice inv inner join tbl_merchant_data mr on mr.merchID=inv.merchantID where inv.merchantID='" . $merchID . "'    ");
        } else {
            $query = $this->db->query("Select inv.*, mr.merchID, mr.resellerID  from  tbl_merchant_billing_invoice inv inner join tbl_merchant_data mr on mr.merchID=inv.merchantID    ");
        }

        $invoices = array();

        $invoices = $query->result_array();
        return $invoices;

    }

    public function get_merchant_data($con)
    {
        $res   = array();
        $query = $this->db->query('Select * from tbl_merchant_data where ' . $con);
        $res = $query->result_array();

        return $res;

    }

    public function get_res_getway_data()
    {
        $res = array();
        $this->db->select('rg.*, resellerCompanyName');
        $this->db->from('tbl_reseller_gateway rg');
        $this->db->join('tbl_reseller r', 'r.resellerID=rg.resellerID', 'inner');
        $this->db->where('isDelete', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;

    }

    public function get_revenue_data($revID)
    {
        $res = array();
        $this->db->select('rv.*, rs.*');
        $this->db->from('tbl_reseller_revenue rv');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=rv.resellerID', 'inner');
        $this->db->where('revID', $revID);
        $q   = $this->db->get();
        $res = $q->row_array();

        return $res;
    }

    public function check_reseller_edit_email($rID, $email)
    {

        $this->db->select('r.*');
        $this->db->from('tbl_reseller r');

        $this->db->where('resellerEmail', $email);
        $this->db->where('r.resellerID!=', $rID, false);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return false;
        } else {
            return true;
        }

    }

    public function get_reseller_gateway_details()
    {
        $res = array();

        $this->db->select('trg.*, tr.resellerCompanyName');
        $this->db->from('tbl_reseller_gateway trg');
        $this->db->join('tbl_reseller tr', 'tr.resellerID = trg.resellerID', 'left');
        $query = $this->db->get();

        if ($query->num_rows()) {

            $res = $query->result_array();
        }

        return $res;

    }

    public function get_reseller_customer_count($month)
    {
        $c1 = $c2 = $c3 = $c4 = $c5 = 0;

        $total_count = array();

        $query = $this->db->query("select Group_concat(DISTINCT merchID SEPARATOR ',') as merch,tm.resellerID, Group_concat(DISTINCT appIntegration SEPARATOR ',') api
                                    from tbl_merchant_data tm
                                    INNER JOIN plans p ON  tm.plan_id = p.plan_id
                                    INNER JOIN tbl_reseller tr ON  tr.resellerID = tm.resellerID AND tr.isTestmode = 0
                                    INNER JOIN app_integration_setting app ON  tm.merchID = app.merchantID
                     where    tm.isDelete=0 and tm.isEnable=1 and  tr.isDelete=0 Group By tm.resellerID, tm.plan_id ");


        if ($query->num_rows() > 0) {
            $r_data = $query->result_array();
        } else {
            $r_data = array();
        }

        foreach ($r_data as $res) {
            $c1 = $c2 = $c3 = $c4 = $c5 = 0;
            $appIntegration = explode(',', $res['api']);

            $mIDS = $res['merch'];
            if(!empty($mIDS)){
                $mIDS = explode(',', $mIDS);
                $newMID = [];
                foreach ($mIDS as $mID) {
                    if($mID > 0) {
                        $newMID[] = $mID;
                    }
                }

                $res['merch'] = implode(',', $newMID);
            }

            if (in_array('1', $appIntegration)) {

                $query = $this->db->query("select count(*) as customer_count
                                    from QBO_custom_customer qbc
                                    INNER JOIN tbl_merchant_data  md ON md.merchID =   qbc.merchantID
                                    INNER JOIN tbl_reseller tr ON  tr.resellerID = md.resellerID AND tr.isTestmode = 0
                     where    md.isDelete=0 and md.isEnable=1 and  tr.isDelete=0 and (date_format(qbc.createdAt,'%b-%Y')= '" . $month . "') and
                     qbc.merchantID  IN (" . $res['merch'] . ")  and  tr.resellerID = '" . $res['resellerID'] . "' ");

                $c1 = $query->row_array()['customer_count'];

            }

            if (in_array('2', $appIntegration)) {

                $query = $this->db->query("select count(*) as customer_count from qb_test_customer  qtc INNER JOIN tbl_merchant_data  md

            ON md.merchID =   qtc.qbmerchantID INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID AND tr.isTestmode = 0

          where  md.isEnable=1 and  md.isDelete=0  && (  date_format(qtc.TimeCreated,'%b-%Y') = '$month'  )
          and qtc.qbmerchantID IN (" . $res['merch'] . ")   and tr.resellerID = '" . $res['resellerID'] . "' ");

                $c2 = $query->row_array()['customer_count'];

            }

            if (in_array('3', $appIntegration)) {

                $query = $this->db->query("select count(*) as customer_count from Freshbooks_custom_customer fcc INNER JOIN tbl_merchant_data  md

         ON md.merchID =   fcc.merchantID  INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID AND tr.isTestmode = 0

          where  md.isEnable=1 and   md.isDelete=0  && (  date_format(fcc.updatedAt,'%b-%Y') = '$month'  )
          and fcc.merchantID IN (" . $res['merch'] . ")  and   tr.resellerID = '" . $res['resellerID'] . "' ");

                $c3 = $query->row_array()['customer_count'];

            }

            if (in_array('4', $appIntegration)) {

                $query = $this->db->query("select count(*) as customer_count from Xero_custom_customer  xcc INNER JOIN tbl_merchant_data  md

             ON  md.merchID =   xcc.merchantID  INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID AND tr.isTestmode = 0

            where md.isEnable=1 and   md.isDelete=0 && (  date_format(xcc.createdAt,'%b-%Y') = '$month'  )
            and xcc.merchantID IN (" . $res['merch'] . ")    and   tr.resellerID = '" . $res['resellerID'] . "' ");

                $c4 = $query->row_array()['customer_count'];

            }

            if (in_array('5', $appIntegration)) {

                $query = $this->db->query("select count(*) as customer_count from chargezoom_test_customer ctc INNER JOIN tbl_merchant_data  md

         ON  md.merchID =   ctc.qbmerchantID INNER JOIN tbl_reseller tr  ON tr.resellerID = md.resellerID AND tr.isTestmode = 0
          
         where  md.isEnable=1 and md.isDelete=0 && (  date_format(ctc.TimeCreated,'%b-%Y') = '$month'  ) 
         and ctc.qbmerchantID IN (".$res['merch'].")  and  tr.resellerID = '".$res['resellerID']."' ") ;
    
        $c5 =    $query->row_array()['customer_count'];
    
     }
     
    
   
    
       if($query->num_rows()>0)
       {
          
          $total_count[] =  $c1 + $c2 +  $c3 + $c4 + $c5;
          
       }else{
           
            $c1 = $c2 = $c3 = $c4 = $c5 = 0;
       }
       
 } 
      $sum=0; 
     $sum =  array_sum($total_count);      

	  return $sum;        
  
    }
  
 
     
 /*************************************************** To Get Plan Details and Merchant Count *********************************************/
 
  

 public function planTypeDetails()
 {
     
    $res_data = array();
    
    $this->db->select('plans.*');
    $this->db->select('(select count(*)  from tbl_merchant_data m  inner join tbl_reseller tr on tr.resellerID = m.resellerID where m.plan_id=plans.plan_id and tr.isDelete="0" and m.isDelete="0") as merchantCount ');
    $this->db->from('plans');
 
    $this->db->where('plans.status','1');
    $this->db->order_by('plan_name', 'asc');
    $query = $this->db->get(); 
 
  
    if($query->num_rows() > 0)
        $res_data = $query->result_array(); 
          
    return $res_data;
   
       
 }
 public function planTypeInactive()
    {

        $res_data = array();

        $this->db->select('plans.*');
        $this->db->select('(select count(*)  from tbl_merchant_data m  inner join tbl_reseller tr on tr.resellerID = m.resellerID where m.plan_id=plans.plan_id and tr.isDelete="0" and m.isDelete="0") as merchantCount ');
        $this->db->from('plans');

       
        $this->db->where('plans.status', '0');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $res_data = $query->result_array();
        }

        return $res_data;

    }
 
 public function get_gateway_data()
 {
     
   $res_data = array();
   
   $this->db->select('g.*,g.gatewayID as gateway, (select count(*) from tbl_merchant_data tm inner join tbl_admin_gateway g on g.gatewayID=tm.gatewayID inner join tbl_reseller r on r.resellerID=tm.resellerID where r.isDelete="0" and tm.isDelete="0" and tm.gatewayID=gateway) as gatewayCount');
   $this->db->from('tbl_admin_gateway g');
 
  
   $this->db->where('g.is_deleted','0');
   $query = $this->db->get(); 
  
    if($query->num_rows() > 0)
          $res_data = $query->result_array(); 
          
          return $res_data;
   
       
 }
 
 
 
 /****************************************************  End Here **************************************************************************/
 
 
    public function insert_inv_number($mID)
    {

        $inv_data['merchantID'] = $mID;
        $inv_data['prefix']     = DEFAULT_INVOICE_PREFIX;
        $inv_data['postfix']    = '10000';
        $inv_data['invoiceNo']  = DEFAULT_INVOICE_PREFIX.'-' . '10000';
        $this->insert_row('tbl_merchant_invoices', $inv_data);

    }
    public function insert_payterm($insert)
    {
        $pay_terms = array(
            array(
                'pt_name'    => 'Due on Receipt',
                'pt_netTerm' => 0,
                'merchantID' => $insert,
                'date'       => date('Y-m-d H:i:s'),
            ),
            array(
                'pt_name'    => 'Net 10',
                'pt_netTerm' => 10,
                'merchantID' => $insert,
                'date'       => date('Y-m-d H:i:s'),
            ),
            array(
                'pt_name'    => 'Net 15',
                'pt_netTerm' => 15,
                'merchantID' => $insert,
                'date'       => date('Y-m-d H:i:s'),
            ),
            array(
                'pt_name'    => 'Net 30',
                'pt_netTerm' => 30,
                'merchantID' => $insert,
                'date'       => date('Y-m-d H:i:s'),
            ),
        );
        $this->db->insert_batch('tbl_payment_terms', $pay_terms);

    }

    public function get_reseller_invoice_data()
    {
        $res = array();
        $this->db->select('inv.*,rs.payOption');
        $this->db->from('tbl_reseller_billing_invoice inv');
        $this->db->join('tbl_reseller rs', 'rs.resellerID=inv.resellerID', 'inner');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;
    }
    public function get_merchant_current_month_revenue($month){
        $query1 = $this->db->query("
                Select  sum(tr.transactionAmount) as total
                 from tbl_merchant_billing_invoice mbi
                inner join  tbl_merchant_data mr on mr.merchID = mbi.merchantID
                inner join tbl_merchant_tansaction tr on tr.merchantID=mr.merchID
                inner join tbl_reseller rs  ON rs.resellerID = mr.resellerID
                where  mr.isDelete='0' and   mr.isEnable='1' and  rs.isTestmode='0' and rs.isEnable='1' and rs.isDelete='0' and mbi.status='paid' and (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1' or tr.transactionCode='200') and (date_format(tr.transactionDate,'%b-%Y')= '" . $month . "')");
        
        if ($query1->num_rows() > 0) {
            $total = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $total = '0';
        }
        return $total;
    }
    public function get_merchant_volume($date = ''){
        $res = array();

        if ($date != '') {
            $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';

        } else {

            $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
        }
        

        $query1 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as total from customer_transaction tr
        inner join tbl_reseller r on r.resellerID=tr.resellerID
        inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
        inner join plans p on p.plan_id=mr.plan_id
        where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
        and (tr.transaction_user_status !="3")
        and  r.isDelete=0 and r.isTestmode =0 and r.isEnable=1 and mr.isDelete=0 and mr.isEnable=1 ' .
            $date);
        $query2 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr
        inner join tbl_reseller r on r.resellerID=tr.resellerID
        inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
        inner join plans p on p.plan_id=mr.plan_id
        where
        (UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR  UPPER(tr.transactionType) = "PAYPAL_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" )
        and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")
        and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") or tr.transaction_user_status IS NULL)
        and r.isDelete=0 and r.isTestmode =0 and r.isEnable=1 and mr.isDelete=0 and mr.isEnable=1 
        ' . $date);

        if ($query1->num_rows() > 0) {
            $res['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
        } else {
            $res['rev'] = '0';
        }

        if ($query2->num_rows() > 0) {

            $res['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $res['refund'] = '0';
        }

        return $res;

        
    }

    public function get_disable_data_admin_merchant(){
        $res = array();
        $this->db->select(' mr.merchID, mr.companyName, mr.firstName,mr.lastName, mr.date_added, mr.updatedAt, p.plan_name, (SELECT fpl.friendlyname FROM plan_friendlyname fpl WHERE fpl.plan_id = mr.plan_id limit 1 ) AS friendlyname');
        $this->db->from('tbl_merchant_data mr');
        $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
        $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'inner');
        $this->db->where('(mr.isEnable = 0 AND  mr.isDelete = 0)');
        $this->db->group_by('mr.merchID');
        $query = $this->db->get();
        if ($query->num_rows()) {
            $res = $query->result_array();
        }
        return $res;
    }
    public function getDisbaleddata(){
        $today = date('Y-m-d 23:59:59');
        $lastYear = date("Y-m-01 00:00:00", strtotime(date('Y-m') . " -11 months"));
        $sql = "SELECT count(1) as disable_count, date_format(tmd.updatedAt,'%b-%Y') as updated_at FROM tbl_merchant_data tmd
                WHERE isEnable = 0 AND tmd.updatedAt BETWEEN '$lastYear' AND '$today'
                GROUP BY date_format(tmd.updatedAt,'%b-%Y')";
        $query1 = $this->db->query($sql);
        if ($query1->num_rows() > 0) {
            $res = $query1->result_array();
        } else {
            $res = [];
        }

        return $res;
    }

    public function get_creditcard_payment($month)
	{   
		$res= 0.00;
		$earn = 0.00;
		$refund = 0.00;
		
		$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
			customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
			WHERE date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and  tr.gateway NOT LIKE "%echeck%"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
				or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
				tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
				and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2") and mr1.isDelete=0 and mr1.isEnable=1  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

		
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{    
			$row = $query->row_array();
			$earn = $row['volume'];
			
		}
		$res = $earn - $refund;
		return  $res; 
	}
    public function get_invoice_payment($month, $isMerhantCount= false)
    {   
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $merchant_count = 0;

        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume, COUNT(DISTINCT(tr.merchantID)) AS merchant_count FROM  
            tbl_merchant_tansaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            WHERE date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
                or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
                tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200")  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2") and mr1.isDelete=0 and mr1.isEnable=1
                  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
            $earn = $row['volume'];
            $merchant_count = $row['merchant_count'];
        }

        $res = $earn - $refund;
        if($isMerhantCount){
            $res = [
                'totalRevenue' => $res,
                'merchant_count' => $merchant_count,
            ];
        }
        
        return  $res; 
    }
    public function get_subscription_payment($month, $isMerhantCount= false)
    {   
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $merchant_count = 0;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(minvitem.itemPrice),0) as volume, COUNT(DISTINCT(tr.merchantID)) AS merchant_count FROM  
            tbl_merchant_tansaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            inner join tbl_merchant_invoice_item minvitem on tr.merchant_invoiceID=minvitem.merchantInvoiceID and tr.merchantID=minvitem.merchantID
            WHERE date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
                or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
                tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  and minvitem.itemName LIKE "%Subscription%" and mr1.isDelete=0 and mr1.isEnable=1
                  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
            $earn = $row['volume'];
            $merchant_count = $row['merchant_count'];
        }

        $res = $earn - $refund;
        if($isMerhantCount){
            $res = [
                'totalSubscriptionRevenue' => $res,
                'merchant_count' => $merchant_count,
            ];
        }
        return  $res; 
    }
    public function get_basis_point_payment($month, $isMerhantCount= false)
    {   
        $res= 0.00;
        $earn = 0.00;
        $refund = 0.00;
        $merchant_count = 0;
        
        $sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(minvitem.itemPrice),0) as volume, COUNT(DISTINCT(tr.merchantID)) AS merchant_count FROM  
            tbl_merchant_tansaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
            inner join tbl_merchant_invoice_item minvitem on tr.merchant_invoiceID=minvitem.merchantInvoiceID and tr.merchantID=minvitem.merchantID
            WHERE date_format(tr.transactionDate, "%b-%Y") ="'.$month.'" and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
                or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
                tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2")  and minvitem.itemName = "Basis Point Fee" and mr1.isDelete=0 and mr1.isEnable=1
                  GROUP BY date_format(transactionDate, "%b-%Y") ' ;

        
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
            $earn = $row['volume'];
            $merchant_count = $row['merchant_count'];
        }

        $res = $earn - $refund;
        if($isMerhantCount){
            $res = [
                'totalBasisRevenue' => $res,
                'merchant_count' => $merchant_count,
            ];
        }
        return  $res; 
    }
	public function get_echeck_payment($month)
	{   
	   
		$res= 0.00;
		$earn = 0.00;
		$refund = 0.00;
		$refund1 = 0.00;
		$sql = 'SELECT   Month(transactionDate) as Month , COALESCE(SUM(tr.transactionAmount),0) as volume FROM  
			customer_transaction tr inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
			WHERE date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and  tr.gateway LIKE "%echeck%"  and (tr.transactionType ="Sale" or tr.transactionType ="ECheck" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
			  or  tr.transactionType ="Auth_capture" or tr.transactionType = "auth_only" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
			  tr.transactionCode ="111" or tr.transactionCode ="120" or tr.transactionCode="1" or tr.transactionCode="200") 
			  and ( tr.transaction_user_status !="3"  and  tr.transaction_user_status !="2") and mr1.isDelete=0 and mr1.isEnable=1 GROUP BY date_format(transactionDate, "%b-%Y") ' ;

		
		$query = $this->db->query($sql);
		if($query->num_rows()>0)
		{    
		  $row = $query->row_array();
		  $earn = $row['volume'];
		 
		}

		if($earn >= $refund){
        	$earn = $earn - $refund;
        }
        
		$res = $earn;
		return  $res;
			
	}

    public function getRevenueDataforAdminLTVReports($month){
        $res=array();
        $this->db->select('sum(tmii.itemPrice) as itemPrice, md.date_added, tmii.merchantID');
        $this->db->from('tbl_merchant_billing_invoice tmi');
        $this->db->join('tbl_merchant_invoice_item tmii', 'tmi.invoice = tmii.merchantInvoiceID');
        $this->db->join('tbl_merchant_data md', 'tmii.merchantID = md.merchID');
        $this->db->where('tmi.createdAt like "'.date("Y-m", strtotime($month)).'%"');
        $this->db->where('tmii.merchantID > ', 0);
        $this->db->group_by('tmii.merchantID');
        $res = $this->db->get()->result_array();
       	return $res;	  
    }
} // end of class
