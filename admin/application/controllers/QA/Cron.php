<?php
require dirname(__FILE__) . '/../merchantPayment.php';

/**
 * Class Cron
 *
 * Class to process merchant invoice manually.
 *
 * @author Mohit Gupta <mohit.gupta@chargezoom.com>
 */

class Cron extends merchantPayment
{
    /**
	 * Function to process merchant invoice/s 
	 * 
	 * @return void 
	 */
    public function customCronInvoicePay()
    {        
        if(ENVIRONMENT != 'development'){
            echo 'Enviornment not allowed';
            exit();
        }

        $payOption = null;

        include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/nmiCustomerVault.class.php';
        $invoiceData = $this->getMerchantBillingPendinInvoices();

        $fp = fopen(realpath(APPPATH . '/../uploads') . '/customCronInvoicePay-' . time() . '.txt', 'a+');
        fwrite($fp, "\r\n" . '========= ' . ($payOption == null ? 'Card and ACH payment logs on ' : ($payOption == 1 ? 'Card Payments logged on ' : 'ACH Payments logged on ')) . date('d/M/Y H:i:s') . ' =========' . "\r\n");

        #All invoice list
        fwrite($fp, "\r\n\r\n--\r\n" . print_r($invoiceData, true) . "\r\n");

        foreach ($invoiceData as $in_data) {
            $logWrite = $this->billingChargeByInvoiceId($in_data, 1, $payOption);

            if (!empty($logWrite)) {
                fwrite($fp, $logWrite . "\r\n");
            }
        }

        fclose($fp);

        echo 'Done';
        exit();
    }

    /**
     * Function to fetch invoice of specific merchants
     * 
     * @return array
     */

    private function getMerchantBillingPendinInvoices() : array
    {
        $currentDate = date('Y-m-d');
        $date        = new DateTime($currentDate);

        $invoiceDueDates   = ["$currentDate"];
        $daysToPickInvoice = [3, 5, 10, 20, 27, 40];
        foreach ($daysToPickInvoice as $dayValue) {
            $dayValue -= 1; #Substract one to the day to count current day as Day 1
            $date->sub(new DateInterval("P" . $dayValue . "D"));
            $newDate           = $date->format('Y-m-d');
            $invoiceDueDates[] = "$newDate";
        }

        $data = array();

        $sql = $this->db->select('mbi.*')
            ->from('tbl_merchant_billing_invoice as mbi')
            ->join('tbl_merchant_data as md', 'md.merchID=mbi.merchantID')
            ->where('mbi.status', 'pending')
            ->where('mbi.BalanceRemaining >', 0)
            ->where_in('invoiceStatus', [1, 3])
            ->where_in('md.merchID', [244, 243, 241])
            ->where_in('DueDate', $invoiceDueDates)
            ->order_by('mbi.createdAt', 'desc');

        $query = $sql->get();

        if ($query->num_rows() > 0) {
            $data = $query->result_array();

            return $data;
        } else {
            return $data;
        }

    }
}
