<?php
ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException; 

class resellerPayment extends CI_Controller  
{
    
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('general_model');
		$this->load->model('card_model');
	    $this->db1= $this->load->database('otherdb', TRUE);
       
	}
	
	public function index(){
	   
	   
   }
   	
	public function test(){
	    $port_url = current_url();
	    redirect('wrong_url');   
	}
	
    
    public function reseller_payment()
	{
        
	    $paid_status = '';  
        $res11 = '';
        
		if(!empty($this->input->post(null, true)))
        {
                 $cardID = $this->czsecurity->xssCleanPostInput('resellerCardID');
                
               
         
                 $gateway = $this->czsecurity->xssCleanPostInput('gateway');
                 $sandbox  =TRUE;
                
                 $invoice = $this->czsecurity->xssCleanPostInput('invoice');
        
             	 
                if($invoice)
                {
					
        	    $con = array('invoice'=>$invoice);
				
        	    $in_data = $this->general_model->get_row_data('tbl_reseller_billing_invoice', $con);
				
        	    $data['get_invoice'] = $in_data;
        	    
        	    
				
				if($in_data)
                {
					$resellerID = $in_data['resellerID'];
					$resellerName = $in_data['resellerName'];
        	        $payamount = $in_data['BalanceRemaining'];
				
			   	$cone = array('gatewayID'=>$gateway);
				
        	    $get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $cone);
				
        	    $bill_email='';
        	    
			             if($cardID != 'new1')
						   {
						    $card_data = $this->card_model->get_reseller_single_card_data($cardID);
						   }
						   
						   
					
        	        $gatewayType = $get_gateway['gatewayType'];
        	        
					
            	    if($gatewayType == 1) 
					{
						
                        	include APPPATH . 'third_party/nmiDirectPost.class.php';
    	                    include APPPATH . 'third_party/nmiCustomerVault.class.php';
							
                            //start NMI
                            $nmiuser   = $get_gateway['gatewayUsername'];
            		        $nmipass   = $get_gateway['gatewayPassword'];
            		        $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
            		        
                           if($cardID != 'new1')
						   {					
							
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$exyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$Billing_Addr1	 = $card_data['Billing_Addr1'];	
							$Billing_Addr2	 = $card_data['Billing_Addr2'];
							$Billing_City	 = $card_data['Billing_City'];
							$Billing_State	 = $card_data['Billing_State'];
							$Billing_Country = $card_data['Billing_Country'];
							$Billing_Contact	 = $card_data['Billing_Contact'];
							$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
							$CardType	 = $card_data['CardType'];
							
						   }else{
							
						    //Billing Data
            		    	
						    $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
							$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							$exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						    $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
						   
						    
						   }
						   
                            if($payamount > 0)
							{
							    
							   
							    
                            	$transaction1 = new nmiDirectPost($nmi_data); 
        						$transaction1->setCcNumber($cnumber);
        					    $expmonth =  $expmonth;
        						
        						$exyear   = substr($exyear,2);
        						if(strlen($expmonth)==1){
        							$expmonth = '0'.$expmonth;
        						}
        					    $expry    = $expmonth.$exyear; 
        						$transaction1->setCcExp($expry);
        						$transaction1->setCvv($cvv);
        						$transaction1->setAmount($payamount);
								
								
        			            $transaction1->sale();
        					    $getwayResponse = $transaction1->execute(); 
        					   
        					    if( $getwayResponse['response_code']=="100"){
        					         $invoice      = $in_data['invoice'];  
            						 $status 	 = '1';
									 $paid_status = '1';
            						 $pay        = $payamount;
        					      	 $remainbal  = $in_data['BalanceRemaining']-$payamount;
            						 $data   	 = array('status'=>$status, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );
            						
  									 $condition  = array('invoice'=>$in_data['invoice'] );
									 
            						 $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);
								    				
								}
								 else{
        					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:'.$getwayResponse['responsetext'].'</strong> </div>');  
        					        	redirect($_SERVER['HTTP_REFERER']);
        					     }
            					     
								$transaction['transactionID']      = $getwayResponse['transactionid'];
								$transaction['transactionStatus']  = $getwayResponse['responsetext'];
								$transaction['transactionCode']    = $getwayResponse['response_code'];
								$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
								$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
								$transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['invoiceID']       = $in_data['invoice'];
								$transaction['gatewayID']          = $get_gateway['gatewayID'];
								$transaction['transactionGateway'] = $gateway;	
								$transaction['transactionAmount']  = $payamount;
								$transaction['gateway']            = "NMI";
								$transaction['userID']         = $resellerID;
							  
								 $id = $this->general_model->insert_row('reseller_transaction',$transaction); 
								
        					}
					}
            	       
					 
					if($gatewayType==2){
                       
                         include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

                 
                     
                         $this->load->config('auth_pay');
                         
                         
                        //Login in Auth
                        $apiloginID       = $get_gateway['gatewayUsername'];
    	                $transactionKey   = $get_gateway['gatewayPassword'];
                       
                        
                      if($cardID != 'new1')
						   {
						    
						    
						       
						 	$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$exyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$Billing_Addr1	 = $card_data['Billing_Addr1'];	
							$Billing_Addr2	 = $card_data['Billing_Addr2'];
							$Billing_City	 = $card_data['Billing_City'];
							$Billing_State	 = $card_data['Billing_State'];
							$Billing_Country = $card_data['Billing_Country'];
							$Billing_Contact	 = $card_data['Billing_Contact'];
							$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
							$CardType	 = $card_data['CardType'];
							
						   }
						   else
						   
						   {
						
						    //Billing Data
            		    	
						    $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
							$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							$exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						    $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
						    
						   	
						   
						   }
					
						
                        if($payamount > 0){
                            	
                            $transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
    						$exyear   = substr($exyear,2);
    						if(strlen($expmonth)==1){
    							$expmonth = '0'.$expmonth;
    						}
    			 	            $expry = $expmonth.$exyear;  
    			 	            
    						$getwayResponse = $transaction1->authorizeAndCapture($payamount,$cnumber,$expry);
    						
    					
								 
    						
    				       if( $getwayResponse->response_code=="1")
						   {
    				             $invoice      = $in_data['invoice'];  
								 $status 	 = '1';
								 $paid_status = '1';
								 $pay        = $payamount;
								 $remainbal  = $in_data['BalanceRemaining']-$payamount;
								 $app        = $in_data['AppliedAmount']+$payamount;
								 
								 $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
								
								 $condition  = array('invoice'=>$in_data['invoice'] );
							
								 $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);
        				        
								
							}
    					    else{
    					        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $getwayResponse->response_reason_text .'</div>');  
    					        
    					    }
							
							$transaction= array();
								$transaction['transactionID']      = $transaction1->transactionid;
								$transaction['transactionStatus']  = $transaction1->responsetext;
								$transaction['transactionCode']    = $transaction1->response_code;
								$transaction['transactionType']    = ($transaction1->type)?$transaction1->type:'Auth_sale';
								$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
								$transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['invoiceID']       = $in_data['invoice'];
								$transaction['gatewayID']          = $get_gateway['gatewayID'];
								$transaction['transactionGateway'] = $gateway;	
								$transaction['transactionAmount']  = $payamount;
								$transaction['gateway']            = "Auth";
								$transaction['userID']         = $resellerID;
					  
								  $id = $this->general_model->insert_row('reseller_transaction',$transaction);
							
							   	redirect($_SERVER['HTTP_REFERER']);	
							
    					}
						   
					} 

					
                        if($gatewayType==3){
                        
    		             include APPPATH . 'third_party/PayTraceAPINEW.php';
    		            $this->load->config('paytrace');
    		            
                        $payusername   = $get_gateway['gatewayUsername'];
            		    $paypassword   = $get_gateway['gatewayPassword'];
            		    $grant_type    = "password";
    		            
    		            if($cardID != 'new1')
						   {
						    
						    
						     
							$name = $resellerName;
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$address	 = $card_data['Billing_Addr1'];	
							$address2	 = $card_data['Billing_Addr2'];
							$city	 = $card_data['Billing_City'];
							$state	 = $card_data['Billing_State'];
							$country = $card_data['Billing_Country'];
							$contact	 = $card_data['Billing_Contact'];
							$zip	 = $card_data['Billing_Zipcode'];
							$CardType	 = $card_data['CardType'];
							
						
							
						   }else{
							
						    //Billing Data
            		    	
						 
								$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
							      $name = $fname." ".$lname;
						   
						   }
						
        		          $invoice_no = $in_data['invoice'];
						
                        if($payamount > 0){
    		                
                        	$expmonth = $expmonth;
    					    if(strlen($expmonth)==1){
    							$expmonth = '0'.$expmonth;
    						}
    					  
    					    $payAPI  = new PayTraceAPINEW();	
    					    
    					    $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
    					    
    					    
    					     
                        	//call a function of Utilities.php to verify if there is any error with OAuth token. 
    						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
    						
        				    if(!$oauth_moveforward){ 
        		                $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        			            //set Authentication value based on the successful oAuth response.
                				//Add a space between 'Bearer' and access _token 
                				
                				$oauth_token = sprintf("Bearer %s",$json['access_token']);
                				
        				        $request_data = array(
                                    "amount"            => $payamount,
                                    "credit_card"       => array (
                                        "number"            => $cnumber,
                                        "expiration_month"  =>$expmonth,
                                        "expiration_year"   =>$expyear
                                    ),
                                    
                                    "csc"               => $cvv,
                                    "invoice_id"        =>$invoice_no,
                                    
                                    "billing_address"=> array(
                                        "name"          =>$name,
                                        "street_address"=> $address,
                                        "city"          => $city,
                                        "state"         => $state,
                                        "zip"           => $zip
            						)
            					); 
                         
        				       $request_data = json_encode($request_data); 
        				       
        			           $gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );
							    
        				       $response  = $payAPI->jsonDecode($gatewayres['temp_json_response']); 
        				       
        				       
							   
        			           if ( $gatewayres['http_status_code']=='200' )
        			           {
            				       
            				     $invoice      = $in_data['invoice'];  
								 $status 	 = '1';
								 $paid_status = '1';
								 $pay        = $payamount;
								 $remainbal  = $in_data['BalanceRemaining']-$payamount;
								 $app        = $in_data['AppliedAmount']+$payamount;
								 
								 $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
								 
								 $condition  = array('invoice'=>$in_data['invoice'] );
								 $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);
        				        
            				    }
								else{
								    
        							 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>' . $response['status_message'] .'</div>'); 
								}
								$transaction= array();
								
								if(isset($response['transaction_id'])){
									$transaction['transactionID']   = $response['transaction_id'];
								}
								else{
									$transaction['transactionID']   = '';
								}
								
							
								$transaction['transactionStatus']  = $response['status_message'];
								$transaction['transactionCode']    = $response['response_code'];
								$transaction['transactionType']    = 'Pay_sale';
								$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
								$transaction['transactionModified']= date('Y-m-d H:i:s'); 
								$transaction['invoiceID']       = $in_data['invoice'];
								$transaction['gatewayID']          = $get_gateway['gatewayID'];
								$transaction['transactionGateway'] = $gateway;	
								$transaction['transactionAmount']  = $payamount;
								$transaction['gateway']            = "Paytrace";
								$transaction['userID']         = $resellerID;
							    
							   	$id = $this->general_model->insert_row('reseller_transaction',$transaction);
							 
							   redirect($_SERVER['HTTP_REFERER']);
							  
							   
        					}
						}
                        
				}
                        
                   if($gatewayType==4){
                       
                     
                
					include APPPATH . 'third_party/PayPalAPINEW.php';
					
    			        $this->load->config('paypal');
                      
                        $config = array(
    						'Sandbox' => $this->config->item('Sandbox'),			// Sandbox / testing mode option.
    						'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
    						'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
    						'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
    						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
    						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
    					  );
    					  
    					  $this->load->library('paypal/Paypal_pro', $config);	  
    					  
    					  if($config['Sandbox'])
        					{
        						error_reporting(E_ALL);
        						ini_set('display_errors', '1');
        					}
    					
    					 if($cardID != 'new1')
						   {					
							$name = $resellerName;
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$address	 = $card_data['Billing_Addr1'];	
							$address2	 = $card_data['Billing_Addr2'];
							$city	 = $card_data['Billing_City'];
							$state	 = $card_data['Billing_State'];
							$country = $card_data['Billing_Country'];
							$contact	 = $card_data['Billing_Contact'];
							$zip	 = $card_data['Billing_Zipcode'];
							$cardtype	 = $card_data['CardType'];
							
								       $Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
							
						   }else{
							
						    //Billing Data
            		    	
						 
								$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								$cardtype = '';
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
						   }
        		     
    					if($payamount > 0){
                       
                            $creditCardType   = 'Visa';
    						$creditCardNumber = $cnumber;
    						$expDateMonth     = $expmonth;
    					    $expDateYear      = $expyear;
    						$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
    						$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
    						$cvv2Number       =   $cvv;
    						$currencyID       = "USD";
    						
    						$firstName = $fname;
                            $lastName =  $lname; 
    						$address1 = $address; 
                            $address2 = $address2; 
    						$country  = $country; 
    						$city     = $city;
    						$state    = $state;		
    						$zip  = $zip;  
    						$email = $bill_email; 
    										
                    		$DPFields = array(
    							'paymentaction' => 'Sale', 	                // How you want to obtain payment.  
    							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
    							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                    		);
                    						
                    		$CCDetails = array(
    							'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
    							'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
    							'expdate'        => $expmonth.$expyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
    							'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
    							'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
    							'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
    						);
                    						
                    		$PayerInfo = array(
    							'email'          => $bill_email, 								// Email address of payer.
    							'payerid'        => '', 							// Unique PayPal customer ID for payer.
    							'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
    							'business'       => '' 							// Payer's business name.
    						);  
                    						
                    		$PayerName = array(
    							'salutation'     => '', 						// Payer's salutation.  20 char max.
    							'firstname'      => $fname, 							// Payer's first name.  25 char max.
    							'middlename'     => '', 						// Payer's middle name.  25 char max.
    							'lastname'       => $lname, 							// Payer's last name.  25 char max.
    							'suffix'         => ''								// Payer's suffix.  12 char max.
    						);
                    					
                    		$BillingAddress = array(
    							'street'         => $address1, 						// Required.  First street address.
    							'street2'        => $address2, 						// Second street address.
    							'city'           => $city, 							// Required.  Name of City.
    							'state'          => $state, 							// Required. Name of State or Province.
    							'countrycode'    => $country, 					// Required.  Country code.
    							'zip'            => $zip 						// Phone Number of payer.  20 char max.
    						);
                    	
                    							
    	                   $PaymentDetails = array(
    							'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
    							'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
    							'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
    							'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
    							'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
    							'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
    							'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
    							'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
    							'custom'         => '', 						// Free-form field for your own use.  256 char max.
    							'invnum'         => '', 						// Your own invoice or tracking number
    							'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
    							'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
    							'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
    						);					
    
    						$PayPalRequestData = array(
    							'DPFields'       => $DPFields, 
    							'CCDetails'      => $CCDetails, 
    							'PayerInfo'      => $PayerInfo, 
    							'PayerName'      => $PayerName, 
    							'BillingAddress' => $BillingAddress, 
    							'PaymentDetails' => $PaymentDetails, 
    							
    						);
    							
    				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
    				        
    					    if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])){
    					        
								$invoice      = $in_data['invoice'];  
								$status 	 = '1';
								$paid_status = '1';
								$pay        = $payamount;
								$remainbal  = $in_data['BalanceRemaining']-$payamount;
								$app        = $in_data['AppliedAmount']+$payamount;
								 
								 $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
								
								 $condition  = array('invoice'=>$in_data['invoice'] );
								 $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);
        				        
							 }
    					    else
							{
        					    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$PayPalResult["ACK"].'</div>'); 
        					   
        					}
							 
        					    $transaction= array();
                                $tranID ='' ;
                                $amt='0.00';
    					        if(isset($PayPalResult['TRANSACTIONID'])) { 
    					            $tranID = $PayPalResult['TRANSACTIONID'];   
    					            $amt=$PayPalResult["AMT"];  
    					        }
                                
                                
    				            $transaction['transactionID']       = $tranID;
    					        $transaction['transactionStatus']   = $PayPalResult["ACK"];
    					        $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
    					        $transactiondata['transactionModified']= date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
    					        $transaction['transactionCode']     = $code;  
    					        $transaction['transactionType']     = "Paypal_sale";	
    						    $transaction['gatewayID']           = $get_gateway['gatewayID'];
                                $transaction['transactionGateway']  = $get_gateway['gatewayType'];					
    					     	$transaction['invoiceID']        = $in_data['invoice'];
    					        $transaction['transactionAmount']   = $payamount;
    					        $transaction['gateway']             = "Paypal";
    					        $transaction['userID']         = $resellerID;
    					
    				           $id = $this->general_model->insert_row('reseller_transaction',$transaction);
    				           
    					       redirect($_SERVER['HTTP_REFERER']);
    					}
						   
				   }
						
                   if($gatewayType==5){
                        
                        
                            require_once APPPATH."third_party/stripe/init.php";	
    		                require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
    	
                           if($cardID != 'new1')
						   {					
							$name = $resellerName;
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$Billing_Addr1	 = $card_data['Billing_Addr1'];	
							$Billing_Addr2	 = $card_data['Billing_Addr2'];
							$Billing_City	 = $card_data['Billing_City'];
							$Billing_State	 = $card_data['Billing_State'];
							$Billing_Country = $card_data['Billing_Country'];
							$Billing_Contact	 = $card_data['Billing_Contact'];
							$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
							$CardType	 = $card_data['CardType'];
							
								$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
							
						   }else{
							
						    //Billing Data
            		    	
						 
						$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
						 	$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							   $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
						   }
                     
                        if($payamount > 0)
                        {
                            
							
							     $paidamount  =  (int)($payamount*100); 
							   
        										\Stripe\Stripe::setApiKey($get_gateway['gatewayUsername']);
        
        										$res =  \Stripe\Token::create([
		        									'card' => [
		        									'number' =>$cnumber,
		        									'exp_month' => $expmonth,
		        									'exp_year' =>  $expyear,
		        									'cvc' => $cvv,
		        									'address_city'    =>    $city,
				                                  	'address_country'   => $country,
				                                  	'address_line1'     => $address,
				                                  	'address_line1_check'     => null,
				                                  	'address_line2'     => $address2,
				                                  	'address_state'     => $state,
				                                  	'address_zip' => $zip,
				                                  	'address_zip_check' => null,
		        									'name' =>$name
        								   		]
        								   ]); 
        						  
        				
        						  
        							$tcharge= json_encode($res);
        									  
        							$rest = json_decode($tcharge);
        							
        						
        							if($rest->id)
        							{	
        								try {
											$customer = \Stripe\Customer::create(array(
											    'name' => $name,
											    'description' => 'CZ Reseller',
											    'source' => $rest->id,
											    "address" => ["city" => $city, "country" => $country, "line1" => $address, "line2" => $address2, "postal_code" => $zip, "state" => $state]
											));
											$customer= json_encode($customer);
							              	  
											$resultstripecustomer = json_decode($customer);

											if(isset($resultstripecustomer->id))
											{
												$charge =	\Stripe\Charge::create(array(
											  	  'customer' => $resultstripecustomer->id,
												  "amount" => $paidamount,
												  "currency" => "usd",
												  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
												 
												));	
											}
											else
											{
												$charge =	\Stripe\Charge::create(array(
		        								  "amount" => $paidamount,
		        								  "currency" => "usd",
		        								  "source" => $rest->id, 
		        								  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
		        								 
		        								));	
                               					
	        						   
											}
											$charge= json_encode($charge);
	                              	  
	        						   		$resultstripe = json_decode($charge);
										}catch (Exception $e) {
											
										  // Something else happened, completely unrelated to Stripe
										}
        								
								   
								   
								   	 	
                          
                           
                            $trID='';
                            if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
    				         {
    				            $trID       = $resultstripe->id;
    				            $code		=  '200';
    						    $invoiceID  = $in_data['invoice'];  
								$status 	= '1';
								$paid_status = '1';
								$pay        = $payamount;
								$remainbal  = $in_data['BalanceRemaining']-$payamount;
						     	$app        = $in_data['AppliedAmount']+$payamount;
								 
								 $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
								 
								 $condition  = array('invoice'=>$in_data['invoice'] );
								 $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);
        				        
    				        }
    				        else
							{
        					    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$resultstripe->status .'</div>'); 
        					   
        					}    
							
							 $transaction = array();
							 
							$transaction['transactionID']       = $trID;
							$transaction['transactionStatus']   = $resultstripe->status;
							$transaction['transactionDate']     = date('Y-m-d H:i:s');  
							$transaction['transactionModified']     = date('Y-m-d H:i:s'); 
							$transaction['transactionCode']     = $code;  
							$transaction['invoiceID']        = $in_data['invoice'];
							$transaction['transactionType']     = 'Stripe_sale';	
							$transaction['gatewayID']           = $get_gateway['gatewayID'];
							$transaction['transactionGateway']  = $get_gateway['gatewayType'] ;					
							$transaction['transactionAmount']   = $payamount;
							$transaction['gateway']             = "Stripe";
							$transaction['userID']         = $resellerID;
					
						  $id = $this->general_model->insert_row('reseller_transaction',$transaction);
					}
                       
				   }
                   }		
                   if($gatewayType==6){
					   
    		             require_once APPPATH."third_party/usaepay/usaepay.php";	
    		         
        		    if($cardID != 'new1')
						   {					
							$name = $card_data['resellerName'];
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$address1	 = $card_data['Billing_Addr1'];	
							$address2	 = $card_data['Billing_Addr2'];
							$city	 = $card_data['Billing_City'];
							$state	 = $card_data['Billing_State'];
							$country = $card_data['Billing_Country'];
							$contact	 = $card_data['Billing_Contact'];
							$zip	 = $card_data['Billing_Zipcode'];
							$cardtype	 = $card_data['CardType'];
							
							
							     	$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
										
						   }else{
							
						    //Billing Data
            		    	
						 
								$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
						   }
        		        
                        $payusername   = $get_gateway['gatewayUsername'];
	                    $password      = $get_gateway['gatewayPassword'];
                   
									
						$cvv='';	
        		       
        					 
						$invNo  =mt_rand(1000000,2000000); 
						$transaction = new umTransaction;
						$transaction->key=$payusername;
						$transaction->pin=$password;
					    $transaction->usesandbox=$sandbox;
						$transaction->invoice=$invNo;   		// invoice number.  must be unique.
						$transaction->description="Chargezoom Public Invoice Payment";	// description of charge
					
						$transaction->testmode=0;   // Change this to 0 for the transaction to process
						$transaction->command="sale";	
                        $transaction->card = $cnumber;
						$expyear   = substr($expyear,2);
						if(strlen($expmonth)==1){
							$expmonth = '0'.$expmonth;
						}
							$expry    = $expmonth.$expyear;  
							$transaction->exp = $expry;
                            if($cvv!="")
                            $transaction->cvv2 = $cvv;
                        
							$transaction->billfname = $fname;
							$transaction->billlname = $lname;
							$transaction->billstreet = $address1;
							$transaction->billstreet2 = $address2;
							$transaction->billcountry = $country;
							$transaction->billcity    = $city;
							$transaction->billstate = $state;
							$transaction->billzip = $zip;
							
							
							$transaction->shipfname = $fname;
							$transaction->shiplname = $lname;
							$transaction->shipstreet = $address1;
							$transaction->shipstreet2 = $address2;
							$transaction->shipcountry = $country;
							$transaction->shipcity    = $city;
							$transaction->shipstate = $state;
							$transaction->shipzip = $zip;
						
							$amount =$payamount;
                            
							
							$transaction->amount = $amount;
							$transaction->Process();
                            
                            $error=''; 	
                            
                             if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                             {			        
                                		
                                                     $msg = $transaction->result;
                                                      $trID = $transaction->refnum;
                                				     $code_data ="SUCCESS";
                                				      $tr_type  = 'sale';
                                				       $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                				 			$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
            										     $invoice      = $in_data['invoice'];  
                        								 $status 	 = '1';
                        								 $paid_status 	 = '1';
                        								 $pay        = $payamount;
                        								 $remainbal  = $in_data['BalanceRemaining']-$payamount;
                        								 $app        = $in_data['AppliedAmount']+$payamount;
								 
								                         $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
                        								$condition  = array('invoice'=>$in_data['invoice'] );
								
								                        $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);

            									}
            									else
            									{
            										
            										     $msg = $transaction->error;
                                                       $trID = $transaction->refnum;
                                                       $result =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
            											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
            											
														
            									
												}
												
												
								 $transaction1 = array();
							 
							$transaction1['transactionID']       = $result['transactionId'];
							$transaction1['transactionStatus']   = $result['status'];
							$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
							$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
							$transaction1['transactionCode']     =  $result['transactionCode']; 
							$transaction1['invoiceID']        = $in_data['invoice'];
							$transaction1['transactionType']     = 'Stripe_sale';	
							$transaction1['gatewayID']           = $get_gateway['gatewayID'];
							$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
							$transaction1['transactionAmount']   = $payamount;
							$transaction1['gateway']             = "usaePay";
							$transaction1['userID']         = $resellerID;
					
						  $id = $this->general_model->insert_row('reseller_transaction',$transaction1);					
            									
                            redirect($_SERVER['HTTP_REFERER']);        
        		                    
				   }
				   
                   if($gatewayType==7){
                        
    		           
        		        require_once dirname(__FILE__) . '../../../../vendor/autoload.php';
    		           
    		            $this->load->config('globalpayments');
    		            
    		           if($cardID != 'new1')
						   {					
							$name = $resellerName;
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$address1	 = $card_data['Billing_Addr1'];	
							$address2	 = $card_data['Billing_Addr2'];
							$city	 = $card_data['Billing_City'];
							$state	 = $card_data['Billing_State'];
							$country = $card_data['Billing_Country'];
							$contact	 = $card_data['Billing_Contact'];
							$zip	 = $card_data['Billing_Zipcode'];
							$cardtype	 = $card_data['CardType'];
							
							$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
							
						   }else{
							
						    //Billing Data
            		    	
						 
								$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
						   }
        		        
        		        
        		        
        		        
        		                      $payusername   = $get_gateway['gatewayUsername'];
            		                  $secretApiKey   = $get_gateway['gatewayPassword'];
        		                   
									
								    $config = new PorticoConfig();
               
                                    $config->secretApiKey = $secretApiKey;
									$sandbox = true;
									
                   					 if($sandbox)
                              		$config->serviceUrl =  $this->config->item('GLOBAL_URL');
                    				else
                             		 $config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
								 
                     		 		$config->developerId =  $this->config->item('DeveloperId');
                 					$config->versionNumber =  $this->config->item('VersionNumber');
                                 
        		                  
        		                        ServicesContainer::configureService($config);
                                        $card = new CreditCardData();
                                        $card->number = $cnumber;
                                        $card->expMonth = $expmonth;
                                        $card->expYear = $expyear;
                                        if($cvv!="")
                                        $card->cvn = $cvv;
                                        $card->cardType=$cardtype;
                                   
                                        $address = new Address();
                                        $address->streetAddress1 = $address1;
                                        $address->city = $city;
                                        $address->state = $state;
                                        $address->postalCode = $zip;
                                        $address->country = $country;
                                        
        		         
                                        $invNo  =mt_rand(5000000,20000000);
                                     	try
                                        {
                                                 $response = $card->charge($payamount)
                                                ->withCurrency("USD")
                                                ->withAddress($address)
                                                ->withInvoiceNumber($invNo)
                                                ->withAllowDuplicates(true)
                                                ->execute();
                            
                            			        $error=''; 	  
                                			   if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                                              {
                                                     $msg = $response->responseMessage;
                                                     $trID = $response->transactionId;
                                				     $code_data ="SUCCESS";
                                				      $tr_type  = 'sale';
                                				      
                                				       $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                				 			$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
            										     $invoice      = $in_data['invoice']; 
                        								 $status 	 = '1';
                        								 $paid_status = '1';
                        								 $pay        = $payamount;
                        								 $remainbal  = $in_data['BalanceRemaining']-$payamount;
                        								 $app        = $in_data['AppliedAmount']+$payamount;
								 
								                         $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
								                         
                        								$condition  = array('invoice'=>$in_data['invoice'] );
                        								
								                       $this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);

            									}
            									else
            									{
            										
            										   $msg = $response->responseMessage;
                                                        $trID = $response->transactionId;
                                                         $result =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
            											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
            									
            										
            										}
            									
                                           }
                                           
                                           
                                             catch (BuilderException $e)
                                            {
                                                $error= 'Build Exception Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ConfigurationException $e)
                                            {
                                                $error='ConfigurationException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (GatewayException $e)
                                            {
                                                $error= 'GatewayException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (UnsupportedTransactionException $e)
                                            {
                                                $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ApiException $e)
                                            {
                                                $error=' ApiException Failure: ' . $e->getMessage();
                                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            
                                           
                            $transaction1 = array();
							 
							$transaction1['transactionID']       = $result['transactionId'];
							$transaction1['transactionStatus']   = $result['status'];
							$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
							$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
							$transaction1['transactionCode']     =  $result['transactionCode']; 
							$transaction1['invoiceID']        = $in_data['invoice'];
							$transaction1['transactionType']     = 'hertLand_sale';	
							$transaction1['gatewayID']           = $get_gateway['gatewayID'];
							$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
							$transaction1['transactionAmount']   = $payamount;
							$transaction1['gateway']             = "hertLand";
							$transaction1['userID']         = $resellerID;
					
						  $id = $this->general_model->insert_row('reseller_transaction',$transaction1);		
						  
					      redirect($_SERVER['HTTP_REFERER']);
        		        
				   }
                        
                  if($gatewayType==8)
				   {    
                    
                    
    		        $this->load->config('cyber_pay');
					
        		        
        		     if($cardID != 'new1')
						   {					
							$resellerName = $resellerName;
							$cnumber     = $card_data['CardNo'] ;
							$expmonth  = $card_data['cardMonth'];
							$expyear  = $card_data['cardYear'];
							$CardID   = $card_data['CardID'];
							$cvv   = $card_data['CardCVV'];
							$friendlyName  = $card_data['friendlyName'] ;
							$address1	 = $card_data['Billing_Addr1'];	
							$address2	 = $card_data['Billing_Addr2'];
							$city	 = $card_data['Billing_City'];
							$state	 = $card_data['Billing_State'];
							$country = $card_data['Billing_Country'];
							$contact	 = $card_data['Billing_Contact'];
							$zip	 = $card_data['Billing_Zipcode'];
							$cardtype	 = $card_data['CardType'];
							
								$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
							
						   }else{
							
						    //Billing Data
            		    	
						        $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
										$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										
										$name = $fname." ".$lname;
								  
										$bill_email = $reseller_data['resellerEmail'];
						   
						   }
        		        
        		        $phone="4158880000"; $email="test@gmail.com"; $companyName='Dummy Company';
        		        $flag ='true';
                          $option =array();
                                
                                $option['merchantID']     = trim($get_gateway['gatewayUsername']);
            			        $option['apiKey']         = trim($get_gateway['gatewayPassword']);
        						$option['secretKey']      = trim($get_gateway['gatewaySignature']);
        						
        				        
								$sandbox = true;
								
        						if($sandbox)
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        						
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $payamount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $fname,
        					"lastName"  =>$lname,
        					"address1"  => $address1,
        					"postalCode"=> $zip,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $expyear,
        					"number" => $cnumber,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        			
        				
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				$tr_type  = 'sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				   
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					    $trID =   $api_response[0]['id'];
        					    $msg  =   $api_response[0]['status'];
        					  
        					    $code =   '200';
        					      $tr_type  = 'sale';
                                $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                				     $code_data ="SUCCESS";
                                				 
                                				 			$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
            										     $invoice      = $in_data['invoice'];  
                        								 $status 	 = '1';
														 $paid_status 	 = '1';
                        								 $pay        = $payamount;
                        								 $remainbal  = $in_data['BalanceRemaining']-$payamount;
                        								 $app        = $in_data['AppliedAmount']+$payamount;
								 
								                         $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
                        								
                        								$condition  = array('invoice'=>$in_data['invoice'] );
                        								$this->general_model->update_row_data('tbl_reseller_billing_invoice',$condition, $data);

            									}
            									else
            									{
            										
            						  $trID =   $api_response[0]['id'];
									  $msg  =   $api_response[0]['status'];
									  $code =   $api_response[1];
									  
                                    $tr_type  = 'sale';
                                	$result =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                                                     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
            										
            										}
        				}  
        		        catch(Cybersource\ApiException $e)
        				{
        					print_r($e->getResponseBody());
        			    	print_r($e->getMessage());  die;
        			    	
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
        					    redirect($_SERVER['HTTP_REFERER']);
        				}
        				
        				 $transaction1 = array();
							 
							$transaction1['transactionID']       = $result['transactionId'];
							$transaction1['transactionStatus']   = $result['status'];
							$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
							$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
							$transaction1['transactionCode']     =  $result['transactionCode']; 
							$transaction1['invoiceID']        = $in_data['invoice'];
							$transaction1['transactionType']     = 'Cyber_sale';	
							$transaction1['gatewayID']           = $get_gateway['gatewayID'];
							$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
							$transaction1['transactionAmount']   = $payamount;
							$transaction1['gateway']             = "cyber_pay";
							$transaction1['userID']         = $resellerID;
					
						  $id = $this->general_model->insert_row('reseller_transaction',$transaction1);			
						   redirect($_SERVER['HTTP_REFERER']);
				   }
				   
				   
				   
				   
					
						if($paid_status == '1' and  $cardID == 'new1')
						{
							
									
					
					
								$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
								$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
							    $exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
						        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
								
								$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
										$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
										$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
										$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
										$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
										$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
										$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
										$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
										
										
							$Rcon = array('resellerID'=>$resellerID);
							
										$reseller_data = $this->general_model->get_row_data('tbl_reseller', $Rcon);
										
										$fname = $reseller_data['resellerfirstName'];
										$lname = $reseller_data['lastName'];
										$billing_email = $reseller_data['resellerEmail'];
										$resellerCardData['cardResellerID'] = $resellerID;
										$resellerCardData['CustomerCard'] = $this->card_model->encrypt($cnumber);
										$resellerCardData['cardMonth'] = $expmonth;
										$resellerCardData['cardYear'] = $exyear;
										$resellerCardData['CardCVV'] = $this->card_model->encrypt($cvv);
										
										$cardTypeData = $this->general_model->getType($cnumber);
										
										$resellerCardData['cardtype'] = $cardTypeData;
										
										$resellerCardData['Billing_First_Name'] = $fname;
										$resellerCardData['Billing_Last_Name'] = $lname;
										$resellerCardData['Billing_Addr1'] = $address;
										$resellerCardData['Billing_Addr2'] = $address2;
										
										$resellerCardData['friendlyName'] = $cardTypeData.' '.'-'.substr($cnumber,-4);
										
										
										
										$resellerCardData['Billing_Country'] = $country;
										$resellerCardData['Billing_City'] = $city;
										$resellerCardData['Billing_State'] = $state;
										$resellerCardData['Billing_Zipcode'] = $zip;
										$resellerCardData['Billing_Contact'] = $contact;
										$resellerCardData['Billing_Email_Address'] = $billing_email;
										
										
										 $res11 = $this->card_model->get_card_row_data('reseller_card_data',array('cardResellerID'=>$resellerID));	
										
								
										 if($res11)
										
											$this->card_model->update_card_data(array('cardResellerID'=>$resellerID),$resellerCardData);
										else									 
											$this->card_model->insert_card_data($resellerCardData);	
									
									$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
									
                                      redirect('home/invoices/');	
                                     
                                    
								
						}  
						  
				
					}else{
        		        
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select Marchemt gateway.</div>'); 
						redirect($_SERVER['HTTP_REFERER']);
						
                } 
    	      
		
            }
            else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Email token not matched</div>'); 
        		redirect($_SERVER['HTTP_REFERER']);
             }
			 
			 
        }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid Request</div>'); 
        		redirect($_SERVER['HTTP_REFERER']);
        }
		
	
    }
   
	

    private function safe_decode($string) {
        return base64_decode(strtr($string, '-_-', '+/='));
    }
    
    
    public function wrong_url(){
	   
	   $this->load->view('QBO_views/wrong_page'); 
	    
	}

 
	


public function get_card_data(){
		$customerdata =array();
		if($this->czsecurity->xssCleanPostInput('cardID')!=""){
			 $crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_reseller_single_card_data($crID);
			if(!empty($card_data)){
			         $customerdata['status'] =  'success';	
					$customerdata['card']     = $card_data;
					echo json_encode($customerdata)	;
					die;
			}
		}
		
		
	}
	
	
public function check_vault()
	 {
		 
 	
		  $card=''; $card_name=''; $customerdata=array();
		  
		 $card_data2 = '';
		  
		 if($this->czsecurity->xssCleanPostInput('resellerID')!=""){
			 
				$resellerID 	= $this->czsecurity->xssCleanPostInput('resellerID'); 

			 	$condition     =  array('resellerID'=>$resellerID); 
			    $customerdata = $this->general_model->get_row_data('tbl_reseller',$condition);
				
				if(!empty($customerdata)){
	                 				 
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_reseller_card_expiry_data($resellerID);
					  
					 if(!empty($card_data))
					 {
						 
					  $customerdata['card']  = $card_data;
					  
					 }else{
						
						$customerdata['card']  = '';
					}
					
					 
		            echo json_encode($customerdata)	;
					die;
				  
					
			    } 	 
			 
	      }		 
		 
	 }		

} // end of class
