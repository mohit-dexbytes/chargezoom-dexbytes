<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 *
 * This is a tiny pretend application which throws something into the queue so
 * that the Web Connector can process it.
 *
 * @author Keith Palmer <keith@consolibyte.com>
 *
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class AdminCron extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/PayPalAPINEW.php';
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        include APPPATH . 'third_party/PayTraceAPINEW.php';
        require_once APPPATH . "third_party/stripe/init.php";
        require_once APPPATH . 'third_party/stripe/lib/Stripe.php';
        $this->load->config('paypal');
        $this->load->config('paytrace');
        $this->load->model('customer_model');
        $this->load->model('general_model');
        $this->load->model('admin_panel_model');
        $this->load->model('company_model');
        $this->db1 = $this->load->database('otherdb', true);

    }

    public function index()
    {
        

    }

    public function create_admin_invoice()
    {

        $in_datas = $this->admin_panel_model->get_subscription_data();

    }

    public function setcron()
    {

        $invoice_data = $this->admin_panel_model->get_invoice_data_auto_pay();

        if (!empty($invoice_data)) {

            foreach ($invoice_data as $in_data) {

                if ($in_data['BalanceRemaining'] != '0.00') {

                    if (date('Y-m-d', strtotime($in_data['DueDate']))) {
                        $card_data = $this->get_card_by_ID($in_data['cardID']);

                        if ($in_data['gatewayType'] == '1') {

                            $nmiuser  = $in_data['gatewayUsername'];
                            $nmipass  = $in_data['gatewayPassword'];
                            $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

                            $merchantID = $in_data['merchantID'];
                            $amount     = $in_data['BalanceRemaining'];

                            $transaction1 = new nmiDirectPost($nmi_data);
                            $transaction1->setCcNumber($card_data['CardNo']);
                            $expmonth = $card_data['CardMonth'];
                            $exyear   = $card_data['CardYear'];
                            $exyear   = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;
                            $transaction1->setCcExp($expry);
                            $transaction1->setCvv($card_data['CardCVV']);
                            $transaction1->setAmount($amount);

                            $transaction1->sale();
                            $result = $transaction1->execute();

                            if ($result['response_code'] == "200") {
                                $merchant_invoiceID = $in_data['merchant_invoiceID'];

                                $ispaid    = 'Completed';
                                $data      = array('status' => $ispaid);
                                $condition = array('merchant_invoiceID' => $in_data['merchant_invoiceID']);
                                $this->general_model->update_row_data('tbl_merchant_billing_invoice', $condition, $data);

                            }

                            $transaction['transactionID']      = $result['transactionid'];
                            $transaction['transactionStatus']  = $result['responsetext'];
                            $transaction['transactionCode']    = $result['response_code'];
                            $transaction['transactionType']    = 'sale';
                            $transaction['transactionDate']    = date('Y-m-d H:i:s');
                            $transaction['gatewayID']          = $in_data['gatewayID'];
                            $transaction['transactionGateway'] = $in_data['gatewayType'];
                            $transaction['merchant_invoiceID'] = $in_data['merchant_invoiceID'];
                            $transaction['merchantID']         = $in_data['merchantID'];
                            $transaction['transactionAmount']  = $in_data['BalanceRemaining'];

                            $id = $this->general_model->insert_row('tbl_merchant_tansaction', $transaction);

                        }

                        if ($in_data['gatewayType'] == '2') {

                            $apiLogin       = $in_data['gatewayUsername'];
                            $transactionkey = $in_data['gatewayPassword'];

                            $amount       = $in_data['BalanceRemaining'];
                            $transaction1 = new AuthorizeNetAIM($apiLogin, $transactionkey);
                            $card_no      = $card_data['CardNo'];
                            $expmonth     = $card_data['CardMonth'];
                            $exyear       = $card_data['CardYear'];
                            $exyear       = substr($exyear, 2);
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $expry = $expmonth . $exyear;

                            $result = $transaction1->authorizeAndCapture($amount, $card_no, $expry);

                            if ($result->response_code == "3") {
                                $merchant_invoiceID = $in_data['merchant_invoiceID'];
                                $ispaid             = 'Completed';
                                $data               = array('status' => $ispaid);
                                $condition          = array('merchant_invoiceID' => $in_data['merchant_invoiceID']);
                                $this->general_model->update_row_data('tbl_merchant_billing_invoice', $condition, $data);

                            }

                            $transactiondata                       = array();
                            $transactiondata['transactionID']      = $result->transaction_id;
                            $transactiondata['transactionStatus']  = $result->response_reason_text;
                            $transactiondata['transactionDate']    = date('Y-m-d h:i:s');
                            $transactiondata['transactionCode']    = $result->response_code;
                            $transactiondata['transactionCard']    = substr($result->account_number, 4);
                            $transactiondata['gatewayID']          = $in_data['gatewayID'];
                            $transactiondata['transactionGateway'] = $in_data['gatewayType'];
                            $transactiondata['transactionType']    = $result->transaction_type;
                            $transactiondata['merchant_invoiceID'] = $in_data['merchant_invoiceID'];
                            $transactiondata['merchantID']         = $in_data['merchantID'];
                            $transactiondata['transactionAmount']  = $result->amount;
                            $transactiondata['merchant_invoiceID'] = $in_data['merchant_invoiceID'];
                            $id = $this->general_model->insert_row('tbl_merchant_tansaction', $transactiondata);

                        }

                        if ($in_data['gatewayType'] == '3') {

                            $payusername = $in_data['gatewayUsername'];
                            $paypassword = $in_data['gatewayPassword'];
                            $grant_type  = "password";

                            $amount   = $in_data['BalanceRemaining'];
                            $card_no  = trim($card_data['CardNo']);
                            $expmonth = trim($card_data['CardMonth']);
                            $exyear   = trim($card_data['CardYear']);
                            $cvv      = trim($card_data['CardCVV']);
                            if ($cvv != '999') {
                                $cvv = '999';
                            }
                            if (strlen($expmonth) == 1) {
                                $expmonth = '0' . $expmonth;
                            }
                            $payAPI = new PayTraceAPINEW();

                            $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

                            //call a function of Utilities.php to verify if there is any error with OAuth token.
                            $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

                            //If IsFoundOAuthTokenError results True, means no error
                            //next is to move forward for the actual request

                            if (!$oauth_moveforward) {

                                $json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

                                //set Authentication value based on the successful oAuth response.
                                //Add a space between 'Bearer' and access _token
                                $oauth_token = sprintf("Bearer %s", $json['access_token']);

                                $name         = $in_data['firstName'];
                                $address      = $in_data['merchantAddress1'];
                                $city         = $in_data['merchantCity'];
                                $state        = $in_data['merchantState'];
                                $zipcode      = ($in_data['merchantZipCode']) ? $in_data['merchantZipCode'] : '85284';
                                $request_data = array(
                                    "amount"          => $amount,
                                    "credit_card"     => array(
                                        "number"           => $card_no,
                                        "expiration_month" => $expmonth,
                                        "expiration_year"  => $exyear),
                                    "csc"             => $cvv,
                                    "billing_address" => array(
                                        "name"           => $name,
                                        "street_address" => $address,
                                        "city"           => $city,
                                        "state"          => $state,
                                        "zip"            => $zipcode,
                                    ),
                                );

                                $request_data = json_encode($request_data);
                                $result = $payAPI->processTransaction($oauth_token, $request_data, URL_KEYED_SALE);
                                $response = $payAPI->jsonDecode($result['temp_json_response']);

                                if ($result['http_status_code'] == '200') {
                                    $merchant_invoiceID = $in_data['merchant_invoiceID'];
                                    $ispaid             = 'Completed';
                                    $data               = array('status' => $ispaid);
                                    $condition          = array('merchant_invoiceID' => $in_data['merchant_invoiceID']);
                                    $this->general_model->update_row_data('tbl_merchant_billing_invoice', $condition, $data);

                                }
                                $transactiondata = array();
                                if (isset($response['transaction_id'])) {
                                    $transactiondata['transactionID'] = $response['transaction_id'];
                                } else {
                                    $transactiondata['transactionID'] = '';
                                }
                                $transactiondata['transactionStatus']  = $response['status_message'];
                                $transactiondata['transactionDate']    = date('Y-m-d h:i:s');
                                $transactiondata['transactionCode']    = $result['http_status_code'];
                                $transactiondata['transactionCard']    = substr($response['masked_card_number'], 12);
                                $transactiondata['gatewayID']          = $in_data['gatewayID'];
                                $transactiondata['transactionGateway'] = $in_data['gatewayType'];
                                $transactiondata['transactionType']    = 'pay_sale';
                                $transactiondata['merchantID']         = $in_data['merchantID'];
                                $transactiondata['transactionAmount']  = $amount;
                                $id                                    = $this->general_model->insert_row('tbl_merchant_tansaction', $transactiondata);
                            }

                        }

                        if ($in_data['gatewayType'] == '4') {

                            $username  = $in_data['gatewayUsername'];
                            $password  = $in_data['gatewayPassword'];
                            $signature = $in_data['gatewaySignature'];

                            $config = array(
                                'Sandbox'      => $this->config->item('Sandbox'), // Sandbox / testing mode option.
                                'APIUsername'  => $username, // PayPal API username of the API caller
                                'APIPassword'  => $password, // PayPal API password of the API caller
                                'APISignature' => $signature, // PayPal API signature of the API caller
                                'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                                'APIVersion'   => $this->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                            );
                            // Show Errors
                            if ($config['Sandbox']) {
                                error_reporting(E_ALL);
                                ini_set('display_errors', '1');
                            }

                            $this->load->library('paypal/Paypal_pro', $config);

                            $amount = $in_data['BalanceRemaining'];

                            $creditCardType = 'Visa';
                            $creditCardNumber = $card_data['CardNo'];
                            $expDateMonth     = $card_data['CardMonth'];
                            $expDateYear      = $card_data['CardYear'];

                            // Month must be padded with leading zero
                            $padDateMonth = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                            $cvv2Number = $card_data['CardCVV'];
                            $currencyID = "USD";

                            $firstName   = $in_data['firstName'];
                            $lastName    = $in_data['lastName'];
                            $companyName = $in_data['companyName'];
                            $address1    = $in_data['merchantAddress1'];
                            $address2    = $in_data['merchantAddress2'];
                            $country     = $in_data['merchantCountry'];
                            $city        = $in_data['merchantCity'];
                            $state       = $in_data['merchantState'];
                            $zip         = $in_data['merchantZipCode'];
                            $phone       = $in_data['merchantContact'];
                            $email       = $in_data['merchantEmail'];

                            $DPFields = array(
                                'paymentaction'    => 'Sale', // How you want to obtain payment.
                                //Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                                'ipaddress'        => '', // Required.  IP address of the payer's browser.
                                'returnfmfdetails' => '0', // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                            );

                            $CCDetails = array(
                                'creditcardtype' => $creditCardType, // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                                'acct'           => $creditCardNumber, // Required.  Credit card number.  No spaces or punctuation.
                                'expdate'        => $padDateMonth . $expDateYear, // Required.  Credit card expiration date.  Format is MMYYYY
                                'cvv2'           => $cvv2Number, // Requirements determined by your PayPal account settings.  Security digits for credit card.
                                'startdate'      => '', // Month and year that Maestro or Solo card was issued.  MMYYYY
                                'issuenumber'    => '', // Issue number of Maestro or Solo card.  Two numeric digits max.
                            );
                            $PayerInfo = array(
                                'email'       => $email, // Email address of payer.
                                'payerid'     => '', // Unique PayPal customer ID for payer.
                                'payerstatus' => 'verified', // Status of payer.  Values are verified or unverified
                                'business'    => '', // Payer's business name.
                            );

                            $PayerName = array(
                                'salutation' => $companyName, // Payer's salutation.  20 char max.
                                'firstname'  => $firstName, // Payer's first name.  25 char max.
                                'middlename' => '', // Payer's middle name.  25 char max.
                                'lastname'   => $lastName, // Payer's last name.  25 char max.
                                'suffix'     => '', // Payer's suffix.  12 char max.
                            );

                            $BillingAddress = array(
                                'street'      => $address1, // Required.  First street address.
                                'street2'     => $address2, // Second street address.
                                'city'        => $city, // Required.  Name of City.
                                'state'       => $state, // Required. Name of State or Province.
                                'countrycode' => $country, // Required.  Country code.
                                'zip'         => $zip, // Required.  Postal code of payer.
                                'phonenum'    => $phone, // Phone Number of payer.  20 char max.
                            );

                            $PaymentDetails = array(
                                'amt'          => $amount, // Required.  Total amount of order, including shipping, handling, and tax.
                                'currencycode' => $currencyID, // Required.  Three-letter currency code.  Default is USD.
                                'itemamt'      => '', // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                                'shippingamt'  => '', // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                                'insuranceamt' => '', // Total shipping insurance costs for this order.
                                'shipdiscamt'  => '', // Shipping discount for the order, specified as a negative number.
                                'handlingamt'  => '', // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                                'taxamt'       => '', // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax.
                                'desc'         => '', // Description of the order the customer is purchasing.  127 char max.
                                'custom'       => '', // Free-form field for your own use.  256 char max.
                                'invnum'       => '', // Your own invoice or tracking number
                                'buttonsource' => '', // An ID code for use by 3rd party apps to identify transactions.
                                'notifyurl'    => '', // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                                'recurring'    => '', // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                            );

                            $PayPalRequestData = array(
                                'DPFields'       => $DPFields,
                                'CCDetails'      => $CCDetails,
                                'PayerInfo'      => $PayerInfo,
                                'PayerName'      => $PayerName,
                                'BillingAddress' => $BillingAddress,
                                'PaymentDetails' => $PaymentDetails,

                            );

                            $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);

                            if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {

                                $code               = '111';
                                $merchant_invoiceID = $in_data['merchant_invoiceID'];
                                $ispaid             = 'Completed';
                                $data               = array('status' => $ispaid);
                                $condition          = array('merchant_invoiceID' => $in_data['merchant_invoiceID']);
                                $this->general_model->update_row_data('tbl_merchant_billing_invoice', $condition, $data);

                            } else {
                                $code = '401';

                            }

                            $transaction = array();
                            $tranID      = '';
                            $amt         = '0.00';
                            if (isset($PayPalResult['TRANSACTIONID'])) {$tranID = $PayPalResult['TRANSACTIONID'];
                                $amt                               = $PayPalResult["AMT"];}

                            $transaction['transactionID']      = $tranID;
                            $transaction['transactionStatus']  = $PayPalResult["ACK"];
                            $transaction['transactionDate']    = date('Y-m-d h:i:s', strtotime($PayPalResult["TIMESTAMP"]));
                            $transaction['transactionCode']    = $code;
                            $transaction['transactionType']    = "Paypal_sale";
                            $transaction['gatewayID']          = $in_data['gatewayID'];
                            $transaction['transactionGateway'] = $in_data['gatewayType'];
                            $transaction['merchant_invoiceID'] = $in_data['merchant_invoiceID'];
                            $transaction['merchantID']         = $in_data['merchantID'];
                            $transaction['transactionAmount']  = $amt;

                            $id = $this->general_model->insert_row('tbl_merchant_tansaction', $transaction);

                        }

                    }
                }

            }
        }

    }

    public function get_vault_data($customerID)
    {
        $this->load->library('encrypt');
        $card = array();

        $sql       = "SELECT * from customer_card_data  where  customerListID='$customerID'  order by CardID desc limit 1  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']                   = $this->encrypt->decode($card_data['CustomerCard']);
            $card['cardMonth']                = $card_data['cardMonth'];
            $card['cardYear']                 = $card_data['cardYear'];
            $card['CardID']                   = $card_data['CardID'];
            $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function get_card_by_ID($merchantCardID)
    {
        $card = array();
        $this->load->library('encrypt');

        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c
		     	where  merchantCardID='$merchantCardID'    ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {

            $card['CardNo']               = $this->encrypt->decode($card_data['MerchantCard']);
            $card['CardMonth']            = $card_data['CardMonth'];
            $card['CardYear']             = $card_data['CardYear'];
            $card['merchantCardID']       = $card_data['merchantCardID'];
            $card['CardCVV']              = $this->encrypt->decode($card_data['CardCVV']);
            $card['merchantFriendlyName'] = $card_data['merchantFriendlyName'];
        }

        return $card;

    }

    public function send_mail_data($indata = array(), $type)
    {

        $customerID = $indata['Customer_ListID'];
        $companyID  = $indata['companyID'];
        $typeID     = $type;
        $invoiceID  = $indata['TxnID'];

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));

        $condition = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data = $this->company_model->template_data($condition);

        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchID' => $comp_data['merchantID']));

        $currency = "$";

        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = $merchant_data['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $tr_amount           = '0.00';
        $update_link         = $config_data['customerPortalURL'];
        $gateway_msg         = '';

        $condition1 = " and Customer_ListID='" . $customerID . "' ";
        $condition1 .= " and TxnID='" . $invoiceID . "' ";

        if ($typeID == '5') {
            $data = $this->customer_model->get_invoice_data_template($condition1);
        }
        if ($typeID == '4') {
            $data      = $this->customer_model->get_invoice_data_template($condition1);
            $card_data = $this->get_card_by_ID($indata['cardID']);
            if (!empty($card_data)) {
                $cardno        = $card_data['CustomerCard'];
                $friendly_name = $card_data['customerCardfriendlyName'];
            }

        }

        if (!empty($data)) {

            $customer = $data['FullName'];
            $amount   = $data['AppliedAmount'];

            $balance    = $data['BalanceRemaining'];
            $paymethods = $data['paymentType'];

            $duedate    = date('F d, Y', strtotime($data['DueDate']));
            $ref_number = $data['RefNumber'];
            $tr_date    = date('F d, Y', strtotime($data['tr_date']));
            $tr_data    = $data['tr_data'];
            $tr_amount  = $data['tr_amount'];
        }

        if ($typeID == '5' && isset($merchant_data['merchant_default_timezone'])  && !empty($merchant_data['merchant_default_timezone'])) {
            // Convert added date in timezone 
            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $merchant_data['merchant_default_timezone']];
            $tr_date = getTimeBySelectedTimezone($timezone);
            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
        }
        $subject = stripslashes(str_replace('{{ invoice.refnumber }}', $ref_number, $view_data['emailSubject']));
        if ($view_data['emailSubject'] == "Welcome to { company_name }") {
            $subject = 'Welcome to ' . $company;
        }

        $message = $view_data['message'];

        $message = stripslashes(str_replace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_replace('{{ creditcard.mask_number }}', $cardno, $message));
        $message = stripslashes(str_replace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_replace('{{ creditcard.url_updatelink }}', $update_link, $message));

        $message = stripslashes(str_replace('{{ customer.company }}', $customer, $message));
        $message = stripslashes(str_replace('{{ transaction.currency_symbol }}', $currency, $message));
        $message = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_replace('{{ transaction.amount }}', ($tr_amount) ? ($tr_amount) : '0.00', $message));

        $message = stripslashes(str_replace('{{ transaction.transaction_method }}', $paymethods, $message));
        $message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_date, $message));
        $message = stripslashes(str_replace('{{ transaction.transaction_detail }}', $tr_data, $message));

        $message = stripslashes(str_replace('{% transaction.gateway_method %}', $paymethods, $message));
        $message = stripslashes(str_replace('{{ invoice.days_overdue }}', $overday, $message));
        $message = stripslashes(str_replace('{{ invoice.refnumber }}', $ref_number, $message));
        $message = stripslashes(str_replace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_replace('{{ invoice.due_date|date("F j, Y") }}', $duedate, $message));
        $message = stripslashes(str_replace('{{ transaction.gateway_msg }}', $transaction_details, $message));

        $message = stripslashes(str_replace('{{ invoice.url_permalink }}', $in_link, $message));
        $message = stripslashes(str_replace('{{ merchant_email }}', $config_email, $message));
        $message = stripslashes(str_replace('{{ merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_replace('{{ current.date }}', $cur_date, $message));


        $fromEmail = $merchant_data['merchantEmail'];
        $toEmail   = $indata['Contact'];
        $addCC     = $view_data['addCC'];
        $addBCC    = $view_data['addBCC'];
        $replyTo   = $view_data['replyTo'];
        $this->load->library('email');

        $this->email->clear();
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($fromEmail);
        $this->email->to($toEmail);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->send();

    }

    public function set_deu_invoice()
    {

        $datas = $this->quickbooks->get_invoice_mail_data('1');

        foreach ($datas as $k => $data) {

            $this->set_template_datainvoice('1', $data);

        }

    }

    public function set_deu_invoice_past()
    {

        $datas = $this->quickbooks->get_invoice_mail_data('0');

        foreach ($datas as $k => $data) {

            $this->set_template_datainvoice('2', $data);
        }
    }

    public function set_credit_card_expired_soon()
    {

        $datas = $this->get_expiry_card_data('0');

        if (!empty($datas)) {
            foreach ($datas as $k => $data) {
                $row_data = $this->customer_model->customer_by_id($data['customerListID']);

                if (!empty($row_data)) {
                    $data['customerID']        = $row_data->ListID;
                    $data['companyID']         = $row_data->companyID;
                    $data['Customer_FullName'] = $row_data->FullName;
                    $data['Contact']           = $row_data->Contact;

                    $this->set_template_datainvoice('12', $data);
                }
            }
        }
    }

    public function set_credit_card_expired()
    {

        $datas = $this->get_expiry_card_data('1');

        if (!empty($datas)) {
            foreach ($datas as $k => $data) {
                $row_data = $this->customer_model->customer_by_id($data['customerListID']);

                if (!empty($row_data)) {
                    $data['customerID']        = $row_data->ListID;
                    $data['companyID']         = $row_data->companyID;
                    $data['Customer_FullName'] = $row_data->FullName;
                    $data['Contact']           = $row_data->Contact;

                    $this->set_template_datainvoice('11', $data);
                }
            }
        }

    }

    public function set_template_datainvoice($typeID, $invoce_data)
    {

        if ($typeID == '1' || $typeID == '2') {
            $customerID = $invoce_data['Customer_ListID'];
        }

        if ($typeID == '11' || $typeID == '12') {
            $customerID = $invoce_data['customerID'];
        }

        $companyID = $invoce_data['companyID'];

        $comp_data = $this->general_model->get_row_data('tbl_company', array('id' => $companyID));
        $condition = array('templateType' => $typeID, 'merchantID' => $comp_data['merchantID']);
        $view_data = $this->company_model->template_data($condition);

        $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $comp_data['merchantID']));
        $config_data   = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $comp_data['merchantID']));
        $currency      = "$";

        $config_email        = $merchant_data['merchantEmail'];
        $merchant_name       = $merchant_data['companyName'];
        $logo_url            = $merchant_data['merchantProfileURL'];
        $mphone              = $merchant_data['merchantContact'];
        $cur_date            = date('Y-m-d');
        $amount              = '';
        $paymethods          = '';
        $transaction_details = '';
        $tr_data             = '';
        $ref_number          = '';
        $overday             = '';
        $balance             = '0.00';
        $in_link             = '';
        $duedate             = '';
        $company             = '';
        $cardno              = '';
        $expired             = '';
        $expiring            = '';
        $friendly_name       = '';
        $update_link         = $config_data['customerPortalURL'];
        $inv_date            = '';
        $customer            = '';

        $customer = $invoce_data['Customer_FullName'];

        $company = $merchant_data['companyName'];
        if (isset($invoce_data['RefNumber'])) {
            $ref_number = $invoce_data['RefNumber'];
        }
        if (isset($invoce_data['TimeModified'])) {
            $tr_data = date('Y-m-d', strtotime($invoce_data['TimeModified']));
        }
        if (isset($invoce_data['BalanceRemaining'])) {
            $balance = ($invoce_data['BalanceRemaining']) ? $invoce_data['BalanceRemaining'] : '0.00';
        }
        if (isset($invoce_data['CardNo'])) {

            $cardno        = $invoce_data['CardNo'];
            $expiry        = $invoce_data['CardNo'];
            $friendly_name = $invoce_data['customerCardfriendlyName'];

        }

        $subject = stripslashes(str_replace('{{ invoice.refnumber }}', $ref_number, $view_data['emailSubject']));
        if ($view_data['emailSubject'] == "Welcome to { company_name }") {
            $subject = 'Welcome to ' . $company;
        }

        $message = $view_data['message'];
        $message = stripslashes(str_replace('{{ merchant_name }}', $merchant_name, $message));
        $message = stripslashes(str_replace('{{ logo }}', "<img src='$logo_url'>", $message));
        $message = stripslashes(str_replace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_replace('{{ creditcard.mask_number }}', $cardno, $message));
        $message = stripslashes(str_replace('{{ creditcard.type_name }}', $friendly_name, $message));
        $message = stripslashes(str_replace('{{ creditcard.url_updatelink }}', $update_link, $message));

        $message = stripslashes(str_replace('{{ customer.company }}', $customer, $message));
        $message = stripslashes(str_replace('{{ transaction.currency_symbol }}', $currency, $message));
        $message = stripslashes(str_replace('{{invoice.currency_symbol}}', $currency, $message));

        $message = stripslashes(str_replace('{{ transaction.amount }}', ($amount) ? ($amount) : '0.00', $message));

        $message = stripslashes(str_replace(' {{ transaction.transaction_method }}', $transaction_details, $message));
        $message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_data, $message));

        $message = stripslashes(str_replace('{{ invoice.days_overdue }}', $overday, $message));
        $message = stripslashes(str_replace('{{ invoice.refnumber }}', $ref_number, $message));
        $message = stripslashes(str_replace('{{invoice.balance}}', $balance, $message));
        $message = stripslashes(str_replace('{{ invoice.due_date|date("F j, Y") }}', $duedate, $message));

        $message = stripslashes(str_replace('{{ invoice.url_permalink }}', $in_link, $message));
        $message = stripslashes(str_replace('{{ merchant_email }}', $config_email, $message));
        $message = stripslashes(str_replace('{{ merchant_phone }}', $mphone, $message));
        $message = stripslashes(str_replace('{{ current.date }}', $cur_date, $message));

        $fromEmail = $merchant_data['merchantEmail'];
        $toEmail   = $invoce_data['Contact'];
        $addCC     = $view_data['addCC'];
        $addBCC    = $view_data['addBCC'];
        $replyTo   = $view_data['replyTo'];
        $this->load->library('email');
        $email_data = array('customerID' => $customerID,
            'merchantID'                     => $merchant_data['merchID'],
            'emailSubject'                   => $subject,
            'emailfrom'                      => $fromEmail,
            'emailto'                        => $toEmail,
            'emailcc'                        => $addCC,
            'emailbcc'                       => $addBCC,
            'emailreplyto'                   => $replyTo,
            'emailMessage'                   => $message,
            'emailsendAt'                    => date("Y-m-d H:i:s"),

        );

        $this->email->clear();
        $config['charset']  = 'utf-8';
        $config['wordwrap'] = true;
        $config['mailtype'] = 'html';
        $this->email->initialize($config);
        $this->email->from($fromEmail);
        $this->email->to($toEmail);
        $this->email->subject($subject);
        $this->email->message($message);
        if ($this->email->send()) {

            $this->general_model->insert_row('tbl_template_data', $email_data);

        } else {

            $this->general_model->insert_row('tbl_template_data', $email_data);

        }

    }

    public function create_invoice()
    {

        $subsdata = $this->company_model->get_subcription_data();
        if (!empty($subsdata)) {

            foreach ($subsdata as $subs) {

                $in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']));

                $inv_pre    = $in_data['prefix'];
                $inv_po     = $in_data['postfix'] + 1;
                $new_inv_no = $inv_pre . $inv_po;
                $duedate    = date("Y-m-d", strtotime($subs['nextGeneratingDate']));
                $randomNum  = substr(str_shuffle("0123456789"), 0, 5);

                if ($subs['generatedInvoice'] < $subs['totalInvoice']) {

                    if ($subs['generatedInvoice'] < $subs['freeTrial']) {

                        $free_trial = '1';
                    } else {

                        $free_trial = '0';
                    }

                    $inv_data = array(
                        'Customer_FullName'      => $subs['FullName'],
                        'Customer_ListID'        => $subs['customerID'],
                        'TxnID'                  => $new_inv_no,
                        'RefNumber'              => $inv_po,
                        'TimeCreated'            => date('Y-m-d H:i:S'),
                        'TimeModified'           => date('Y-m-d H:i:S'),
                        'DueDate'                => $duedate,
                        'ShipAddress_Addr1'      => $subs['address1'],
                        'ShipAddress_Addr2'      => $subs['address2'],
                        'ShipAddress_City'       => $subs['city'],
                        'ShipAddress_Country'    => $subs['country'],
                        'ShipAddress_State'      => $subs['state'],
                        'ShipAddress_PostalCode' => $subs['zipcode'],
                        'IsPaid'                 => 'false',
                        'insertInvID'            => $randomNum,
                        'invoiceRefNumber'       => $inv_po,
                        'freeTrial'              => $free_trial,
                        'gatewayID'              => $subs['paymentGateway'],
                        'autoPayment'            => $subs['automaticPayment'],
                        'cardID'                 => $subs['cardID'],
                    );

                    $in_num   = $subs['generatedInvoice'];
                    $paycycle = $subs['invoiceFrequency'];
                    $date     = $subs['firstDate'];

                    if ($paycycle == 'dly') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
                    }
                    if ($paycycle == '1wk') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                    }

                    if ($paycycle == '2wk') {
                        $in_num    = ($in_num) ? $in_num + 2 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
                    }
                    if ($paycycle == 'mon') {
                        $in_num    = ($in_num) ? $in_num + 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == '2mn') {
                        $in_num    = ($in_num) ? $in_num + 2 * 1 : '1';
                        $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                    }
                    if ($paycycle == 'qtr') {
                        $in_num    = ($in_num) ? $in_num + 3 * 1 : '1';
                        $next_date = strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month");
                    }
                    if ($paycycle == 'six') {
                        $in_num    = ($in_num) ? $in_num + 6 * 1 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == 'yrl') {
                        $in_num    = ($in_num) ? $in_num + 12 * 1 : '0';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }

                    if ($paycycle == '2yr') {
                        $in_num    = ($in_num) ? $in_num + 2 * 12 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }
                    if ($paycycle == '3yr') {
                        $in_num    = ($in_num) ? $in_num + 3 * 12 : '1';
                        $next_date = date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
                    }

                    if ($this->general_model->update_row_data('tbl_subscription_invoice_item',
                        array('subscriptionID' => $subs['subscriptionID']), array('invoiceDataID' => $randomNum))) {

                        $gen_inv = $subs['generatedInvoice'] + 1;
                        $this->general_model->update_row_data('tbl_subscriptions',
                            array('subscriptionID' => $subs['subscriptionID']), array('nextGeneratingDate' => $next_date, 'generatedInvoice' => $gen_inv));

                        $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $subs['merchantDataID']), array('postfix' => $inv_po));
                        $this->general_model->insert_row('tbl_custom_invoice', $inv_data);

                        $user = $subs['qbwc_username'];
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_INVOICE, $randomNum, '', '', $user);

                    }
                }
            }
        }

    }

    public function get_card_expiry_data($customerID, $companyID)
    {

        $card = array();
        $this->load->library('encrypt');

        $sql       = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where `companyID` = '$companyID' and customerListID='$customerID' limit 1  ";
        $query1    = $this->db1->query($sql);
        $card_data = $query1->row_array();
        if (!empty($card_data)) {
            $card['CustomerCard']             = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
            $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];
        }

        return $card;

    }

    public function get_expiry_card_data($type)
    {
        $new_card = array();
        $card     = array();
        $this->load->library('encrypt');

        if ($type == '1') {
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day ) ";
        }

        if ($type == '0') {
            $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  ";

        }
        $query1     = $this->db1->query($sql);
        $card_datas = $query1->result_array();
        if (!empty($card_datas)) {
            foreach ($card_datas as $k => $card_data) {
                $card['CardNo']                   = substr($this->encrypt->decode($card_data['CustomerCard']), 12);
                $card['cardMonth']                = $card_data['cardMonth'];
                $card['cardYear']                 = $card_data['cardYear'];
                $card['expiry']                   = $card_data['expired_date'];
                $card['CardID']                   = $card_data['CardID'];
                $card['CardCVV']                  = $this->encrypt->decode($card_data['CardCVV']);
                $card['customerListID']           = $card_data['customerListID'];
                $card['customerCardfriendlyName'] = $card_data['customerCardfriendlyName'];

                $new_card[$k] = $card;
            }
        }

        return $new_card;

    }

    public function pay_invoice()
    {

        $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
        if ($invoiceID != '') {

            $in_data = $this->quickbooks->get_invoice_data();

            if (!empty($in_data)) {

                if ($in_data['BalanceRemaining'] > 0) {

                    $nmiuser = $in_data['nmi_username'];
                    $nmipass = $in_data['nmi_password'];

                    $nmi_data          = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);
                    $customer_vault_id = $in_data['vaultID'];
                    $amount            = $in_data['BalanceRemaining'];
                    $vault             = new nmiCustomerVault($nmi_data);
                    $vault->setCustomerVaultId($customer_vault_id);
                    $vault->charge($amount);
                    $result = $vault->execute();

                    if ($result['response_code'] == "100") {
                        $txnID     = $in_data['TxnID'];
                        $ispaid    = 'true';
                        $data      = array('IsPaid' => $ispaid);
                        $condition = array('TxnID' => $in_data['TxnID'], 'EditSequence' => $in_data['EditSequence']);
                        $this->general_model->update_row_data('qb_test_invoice', $condition, $data);
                        $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID);
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');
                    } else {

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $result['responsetext'] . '.</div>');
                    }

                    $transaction['transactionID']     = $result['transactionid'];
                    $transaction['transactionStatus'] = $result['responsetext'];
                    $transaction['transactionCode']   = $result['response_code'];
                    $transaction['transactionType']   = ($result['type']) ? $result['type'] : 'auto-nmi';
                    $transaction['transactionDate']   = date('Y-m-d h:i:s');
                    $transaction['invoiceTxnID']      = $in_data['TxnID'];
                    $transaction['customerListID']    = $in_data['Customer_ListID'];
                    $transaction['transactionAmount'] = $in_data['BalanceRemaining'];
                    $id                               = $this->general_model->insert_row('tbl_customer_tansaction', $transaction);

                    redirect('home/index', 'refresh');

                }

            }

        }

    }
    public function tempSuspendAccountActivate()
    {
        $conditionC  = array('isLockedTemp' => 1);
        $getAccount     = $this->general_model->get_table_data('tbl_merchant_data', $conditionC);

        if(count($getAccount) > 0){
            foreach ($getAccount as $value) {
                $merchID = $value['merchID'];
                $dateSuspend = $value['accountLockedDate'];
                $current = strtotime(date('Y-m-d H:i:s'));
                $dateSuspend = strtotime($dateSuspend);
                $diff = $current - $dateSuspend;
                $hours = round($diff / ( 60 * 60 ) ); #Used for hours
                
                if($hours >= 24){
                    $input_data   = array('login_failed_attemp' => 0,'isSuspend' => 0,'isLockedTemp' => 0,'accountLockedDate' => null,'lockedMessage' => null);

                    $condition1   = array('merchID' => $value['merchID']);
                    
                    $this->general_model->update_row_data('tbl_merchant_data',$condition1, $input_data);
                    echo 'Account id:'.$merchID.' ('.$value['merchantEmail'].') is activated.<br>';
                }else{
                    $remaininghour = 24 - $hours;
                    echo 'Account id:'.$merchID.' ('.$value['merchantEmail'].') is  activated after '.$remaininghour.' hours.<br>';
                }
                
            }
        }else{
            echo 'No account suspended';
        }

    }

    public function admin_dashboard_chart_json()
	{
        ini_set('max_execution_time', 0);
        $this->load->helper('file');

        // Graph Cached File
        $cachedFile = FCPATH."uploads/admin_dashboard_chart.json";

        if($this->input->post('fetchData', true) && file_exists($cachedFile) ){
            $currentTime = time();
            $fileTime = filemtime($cachedFile);

            $fileUpdateHoursDiff = floor(($currentTime - $fileTime) / 3600);

            // If file update hours are less than 2 hours then only pull from the cached file
            if($fileUpdateHoursDiff < 2)
            {
                echo file_get_contents($cachedFile);exit;    
            }
        }

		$get_result = $this->admin_panel_model->chart_get_merchant(1);
		$disabled = $this->admin_panel_model->getDisbaleddata();
        
        $data['chart']        = 	$get_result;

		$get_result1 = $this->admin_panel_model->get_total_processing_volume();
		$data['merch_list']  = $get_result1['merchant'];
		$data['res_list']    =  $get_result1['reseller'];
		$data['plan_ratio'] = $get_result1['plan_ratio'];


		$ob = [];
		$obV = [];
		$obRevenu = [];
		
		$in = 0;

        $disbaledMerchant = $subscription = $basys_point = [];
		foreach ($data['chart']['month'] as $monKey => $value) {
			$ob1 = [];
			$obR = [];

			$inc = strtotime($value);
			$ob1[0] = $inc;
			$obR[] = $inc;
			$obR[] = $value;
			$ob1[1] = $data['chart']['revenue'][$in];

			$ob[] = $ob1;

            $subscription[] = [
                $inc,
                $data['chart']['subRevenue'][$in]
            ];
			$basys_point[] = [
                $inc,
                $data['chart']['bpRevenue'][$in]
            ];

			$monthNew = date("M", strtotime($obR[1])); 
			$data['chart']['month'][$monKey] = $monthNew;
			$obR[1] = $monthNew;
			$obRevenu[] = $obR; 
			$in++;

            $disabledData = 0;
            foreach($disabled as $disIndex => $disMerchant){
                if(isset($disMerchant['updated_at']) && $disMerchant['updated_at'] == $value){
                    $disabledData = -$disMerchant['disable_count'];
                    unset($disMerchant[$disIndex]);
                    continue;
                }
            }
            $disbaledMerchant[] = $disabledData;
		}

		$data['revenu_month'] =   $obRevenu;
		$data['revenu_volume'] =   $ob;
		$data['disbaledMerchant'] =   $disbaledMerchant;
		$data['merchant_chart_base'] =   0;
		$data['subscription'] =   $subscription;
		$data['basys_point'] =   $basys_point;
		$data['arp_report'] =   $this->generateARPMerchantReports();	
		$data['nrr_report'] = $this->generateNRRMerchantReports();
        $data['ltv_report'] = $this->generateLTVMerchantReports();

		$response = json_encode($data);
		write_file($cachedFile, $response,'w+');

        if($this->input->post('fetchData', true)){
            echo $response;exit;    
        }

		die;
	}
	public function admin_dashboard_processing_volumn_json()
	{
		$this->load->helper('file');

        // Graph Cached File
        $cachedFile = FCPATH."uploads/admin_dashboard_processing_volumn.json";

        if($this->input->post('fetchData', true) && file_exists($cachedFile) ){
            $currentTime = time();
            $fileTime = filemtime($cachedFile);

            $fileUpdateHoursDiff = floor(($currentTime - $fileTime) / 3600);

            // If file update hours are less than 2 hours then only pull from the cached file
            if($fileUpdateHoursDiff < 2)
            {
                echo file_get_contents($cachedFile);exit;
            }
        }

        $get_result = $this->admin_panel_model->chart_get_merchant();
        $data['chart']        = 	$get_result;

        $obV = [];
        $obRevenu = [];
        
        $in = 0;
        $ccVolume = $ecVolume = [];
        foreach ($data['chart']['month'] as $monKey => $value) {
            $ob1 = [];
            $obR = [];
            $ob2 = [];
            
            $inc = strtotime($value);
            
            $ob2[0] = $inc;
            $obR[] = $inc;
            $obR[] = $value;

            $monthNew = date("M", strtotime($obR[1])); 
            $data['chart']['month'][$monKey] = $monthNew;
            $obR[1] = $monthNew;
            
            $ob2[1] = $data['chart']['volume'][$in];

            $obV[] = $ob2;

            $ccVolume[] = [
                $inc,
                $data['chart']['revenueCC'][$in],
            ];

            $ecVolume[] = [
                $inc,
                $data['chart']['revenueEC'][$in],
            ];

            $obRevenu[] = $obR; 
            $in++;
        }

        $data['revenu_month'] =   $obRevenu;
        
        $data['volume'] =   $obV;	
        $data['ccVolume'] =   $ccVolume;	
        $data['ecVolume'] =   $ecVolume;	
        
        $response = json_encode($data);
        write_file(FCPATH."uploads/admin_dashboard_processing_volumn.json", $response,'w+');

        if($this->input->post('fetchData', true)){
            echo $response;exit;
        }
        die;
    
	}

    public function generateARPMerchantReports(){
		$arp_data = [];
		for ($i = 12; $i >= 1; $i--) {
            $calculatedARP = $calculatedSubscriptionARP = $calculatedBasisARP = 0;
			$monthNameY = date("M-Y", strtotime(date('Y-m') . " -$i months"));

			$subscription_data = $this->admin_panel_model->get_subscription_payment($monthNameY, true);
			$merchantCountSBS = $subscription_data['merchant_count'];
			$totalSubscriptionRevenue = $subscription_data['totalSubscriptionRevenue'];
            if($merchantCountSBS) {
                $calculatedSubscriptionARP = $totalSubscriptionRevenue / $merchantCountSBS;
            }

            $basis_data = $this->admin_panel_model->get_basis_point_payment($monthNameY, true);
			$merchantCountBasis = $basis_data['merchant_count'];
			$totalBasisRevenue = $basis_data['totalBasisRevenue'];
            if($merchantCountBasis) {
                $calculatedBasisARP = $totalBasisRevenue / $merchantCountBasis;
            }

            $calculatedARP = $calculatedSubscriptionARP + $calculatedBasisARP;

            $monthinSec = strtotime($monthNameY);
			$monthName = date("M", strtotime($monthNameY));

			$arp_data['month'][] = [
                $monthinSec,
                $monthName,
            ];
            
			$arp_data['subscription'][] = [
                $monthinSec,
                round($calculatedSubscriptionARP, 2)
            ];

            $arp_data['basis'][] = [
                $monthinSec,
                round($calculatedBasisARP, 2)
            ];

            $arp_data['total'][] = [
                $monthinSec,
                round($calculatedARP, 2)
            ];
		}

		return $arp_data;
	}

    public function generateNRRMerchantReports()
    {
        
        // NRR report code start
        $months = [];
        for ($i = 13; $i >= 1; $i--) {
            $months[] = date("M-Y", strtotime(date('Y-m-01') . " -$i months"));
        }
        $index = 0;
        $nrr_report = [];
        $last_month_merchant_details = [];

        foreach ($months as $month) {
            $revenue_data = $this->general_model->getRevenueDataforAdminReports($month);
            $month_str = strtotime($month);
            $nrr = 0;

            $upgrade_amount = 0;
            $downgrade_amount = 0;
            $disabled_merchant_amount = 0;
            $revenue = 0;

            if($revenue_data){
                foreach ($revenue_data as $value) {
                    if($index == 0){
                        if($value['itemName'] == 'Basis Point Fee'){
                            $last_month_merchant_details[$value['merchantID']]['basis_point'] = $value['itemPrice'];
                        }else{
                            $last_month_merchant_details[$value['merchantID']]['plan_amount'] = $value['itemPrice'];
                        }
                        continue;
                    }

                    $revenue += $value['itemPrice'];
                    if($value['merchantID']){
                        if($value['itemName'] == 'Basis Point Fee'){
                            if(isset($last_month_merchant_details[$value['merchantID']]['basis_point'])){
                                if($value['itemPrice'] > $last_month_merchant_details[$value['merchantID']]['basis_point']){
                                    $upgrade_amount += $value['itemPrice'] - $last_month_merchant_details[$value['merchantID']]['basis_point'];
                                }else if($last_month_merchant_details[$value['merchantID']]['basis_point'] > $value['basis_point']){
                                    $downgrade_amount += $last_month_merchant_details[$value['merchantID']]['basis_point'] - $value['itemPrice'];
                                }
                            }else{
                                $upgrade_amount += $value['itemPrice'];
                            }
                        }else{
                            if(isset($last_month_merchant_details[$value['merchantID']]['plan_amount'])){
                                if($value['itemPrice'] > $last_month_merchant_details[$value['merchantID']]['plan_amount']){
                                    $upgrade_amount += $value['itemPrice'] - $last_month_merchant_details[$value['merchantID']]['plan_amount'];
                                }else if($last_month_merchant_details[$value['merchantID']]['plan_amount'] > $value['itemPrice']){
                                    $downgrade_amount += $last_month_merchant_details[$value['merchantID']]['plan_amount'] - $value['itemPrice'];
                                }
                            }else{
                                $upgrade_amount += $value['itemPrice'];
                            }
                        }

                        if($value['itemName'] == 'Basis Point Fee'){
                            $last_month_merchant_details[$value['merchantID']]['basis_point'] = $value['itemPrice'];
                        }else{
                            $last_month_merchant_details[$value['merchantID']]['plan_amount'] = $value['itemPrice'];
                        }
                    }
                }
            }
            $index++;
            if($index == 1){
                continue;
            }

            $disabled_data = $this->general_model->getDisabledMerchantPlanData($month);
            $disabled_merchant_amount = $disabled_data['total_amount'];
            
            if($revenue > 0){
                $nrr = ($revenue + $upgrade_amount - $downgrade_amount - $disabled_merchant_amount) / $revenue;
            }

            $monthinSec = strtotime($month);
            $monthName = date("M", strtotime($month));

            $nrr_report['month'][] = [
                $monthinSec,
                $monthName,
            ];
            
            $nrr_report['arp'][] = [
                $monthinSec,
                round(($nrr*100), 2)
            ];
        }
        return $nrr_report;
    }

    public function generateLTVMerchantReports()
    {
        // NRR report code start
        $months = [];
        for ($i = 12; $i >= 1; $i--) {
            $months[] = date("01-M-Y", strtotime(date('Y-m-01') . " -$i months"));
        }

        $ltv_report = [];

        foreach ($months as $month) {
            $revenue_data = $this->admin_panel_model->getRevenueDataforAdminLTVReports($month);
            $ts1 = $monthinSec = strtotime($month);
            $year1 = date('Y', $ts1);
            $month1 = date('m', $ts1);
            $ltv = 0;
            if($revenue_data){
                $total_merchant = count($revenue_data);
                foreach ($revenue_data as $value) {
                    $ts2 = strtotime($value['date_added']);
                    $year2 = date('Y', $ts2);
                    $month2 = date('m', $ts2);
                    $merchant_length = abs((($year2 - $year1) * 12) + ($month2 - $month1));
                    $itemPrice = $value['itemPrice'];
                    $ltv += ( $itemPrice * $merchant_length ) / $total_merchant;
                }
            }

            $monthName = date("M", strtotime($month));

            $ltv_report['month'][] = [
                $monthinSec,
                $monthName,
            ];
            
            $ltv_report['arp'][] = [
                $monthinSec,
                round($ltv, 2)
            ];
        }
        return $ltv_report;
    }
}
