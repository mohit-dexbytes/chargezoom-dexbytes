<?php
class Client
{
    private $client_id;
    private $client_secret;

    /**
     * HTTP Methods
     */
    const HTTP_METHOD_GET    = 'GET';
    const HTTP_METHOD_POST   = 'POST';
    const HTTP_METHOD_PUT    = 'PUT';
    const HTTP_METHOD_DELETE = 'DELETE';
    const HTTP_METHOD_HEAD   = 'HEAD';
    const HTTP_METHOD_PATCH  = 'PATCH';

    public function __construct($client_id, $client_secret)
    {
        if (!extension_loaded('curl')) {
            throw new Exception('The PHP exention curl must be installed to use this library.', Exception::CURL_NOT_FOUND);
        }
        if (!isset($client_id) || !isset($client_secret)) {
            throw new Exception('The App key must be set.', Exception::InvalidArgumentException);
        }

        $this->client_id     = $client_id;
        $this->client_secret = $client_secret;

    }

    private function generateAccessTokenHeader($access_token)
    {
        $authorizationheader = 'Bearer ' . $access_token;
        return $authorizationheader;
    }

    public function getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state)
    {
        $parameters = array(
            'client_id'     => $this->client_id,
            'scope'         => $scope,
            'redirect_uri'  => $redirect_uri,
            'response_type' => $response_type,
            'state'         => $state,
        );
        $authorizationRequestUrl .= '?' . http_build_query($parameters, null, '&', PHP_QUERY_RFC1738);
        return $authorizationRequestUrl;
    }

    public function getAccessToken($tokenEndPointUrl, $code, $redirectUrl, $grant_type)
    {
        if (!isset($grant_type)) {
            throw new InvalidArgumentException('The grant_type is mandatory.', InvalidArgumentException::INVALID_GRANT_TYPE);
        }

        $parameters = array(
            'grant_type'   => $grant_type,
            'code'         => $code,
            'redirect_uri' => $redirectUrl,
        );
        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );

        //Try catch???
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    public function refreshAccessToken($tokenEndPointUrl, $grant_type, $refresh_token)
    {
        $parameters = array(
            'grant_type'    => $grant_type,
            'refresh_token' => $refresh_token,
        );

        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }

    private function generateAuthorizationHeader()
    {
        $encodedClientIDClientSecrets = base64_encode($this->client_id . ':' . $this->client_secret);
        $authorizationheader          = 'Basic ' . $encodedClientIDClientSecrets;
        return $authorizationheader;
    }

    private function executeRequest($url, $parameters = array(), $http_header, $http_method)
    {

        $curl_options = array();

        switch ($http_method) {
            case self::HTTP_METHOD_GET:
                $curl_options[CURLOPT_HTTPGET] = 'true';
                if (is_array($parameters) && count($parameters) > 0) {
                    $url .= '?' . http_build_query($parameters);
                } elseif ($parameters) {
                    $url .= '?' . $parameters;
                }
                break;
            case self::HTTP_METHOD_POST:
                $curl_options[CURLOPT_POST] = '1';
                if (is_array($parameters) && count($parameters) > 0) {
                    $body                             = http_build_query($parameters);
                    $curl_options[CURLOPT_POSTFIELDS] = $body;
                }
                break;
            default:
                break;
        }

        if (is_array($http_header)) {
            $header = array();
            foreach ($http_header as $key => $value) {
                $header[] = "$key: $value";
            }
            $curl_options[CURLOPT_HTTPHEADER] = $header;
        }

        $curl_options[CURLOPT_URL] = $url;
        $curl_options[CURLOPT_SSL_VERIFYPEER] = 0;
        $ch                        = curl_init();

        curl_setopt_array($ch, $curl_options);

        //Don't display, save it on result
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //Execute the Curl Request
        $result = curl_exec($ch);
        $headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
        if ($curl_error = curl_error($ch)) {
            throw new Exception($curl_error);
        } else {
            $json_decode = json_decode($result, true);
        }
        curl_close($ch);

        return $json_decode;
    }


    public function revokeToken($tokenEndPointUrl, $grant_type, $access_token)
    {
        $parameters = array(
            'token' => $access_token,
        );

        $authorizationHeaderInfo = $this->generateAuthorizationHeader();
        $http_header             = array(
            'Accept'        => 'application/json',
            'Authorization' => $authorizationHeaderInfo,
            'Content-Type'  => 'application/x-www-form-urlencoded',
        );
        $result = $this->executeRequest($tokenEndPointUrl, $parameters, $http_header, self::HTTP_METHOD_POST);
        return $result;
    }
}
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Facades\Item;
use QuickBooksOnline\API\Facades\Customer;

class AdminQuickBook extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
    }

    public function QBOIndex()
    {
        $data['login_info'] = $this->session->userdata('admin_logged_in');
        if (!$data['login_info']) {
            redirect('login');
        }

        $data['primary_nav']     = primary_nav();
        $data['template']       = template_variable();

        $val = array('merchantID' => 0,'is_admin' => 1);

        $data1 = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data1['accessToken'];
        $refreshToken = $data1['refreshToken'];
        $realmID      = $data1['realmID'];
        if(empty($refreshToken)){
            $refreshToken = '123456';
        }
        $dataService = DataService::Configure(array(
            'auth_mode'       => $this->config->item('AuthMode'),
            'ClientID'        => $this->config->item('client_id'),
            'ClientSecret'    => $this->config->item('client_secret'),
            'accessTokenKey'  => $accessToken,
            'refreshTokenKey' => $refreshToken,
            'QBORealmID'      => $realmID,
            'baseUrl'         => $this->config->item('QBOURL'),
        ));

        $dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
        $qbo_data['lastUpdated'] = date('Y-m-d H:i:s', strtotime('-12 hour'));
        if (!empty($qbo_data)) {
            $last_date = $qbo_data['lastUpdated'];
            $my_timezone = date_default_timezone_get();
            $from        = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
            $to          = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');
            $last_date   = $this->general_model->datetimeconvqbo($last_date, $from, $to);
            $last_date1  = date('Y-m-d H:i', strtotime('-9 hour', strtotime($last_date)));

            $latedata = date('Y-m-d', strtotime($last_date1)) . 'T' . date('H:i:s', strtotime($last_date1));
            $sqlQuery = "SELECT * FROM Item where Metadata.LastUpdatedTime > '$latedata'";
        } else {
            $sqlQuery = "SELECT * FROM Item";
        }
        $keepRunning = true;
        $entities = [];
        $st    = 0;
        $limit = 0;
        $total = 0;
        while($keepRunning === true){
            $mres   = MAXRESULT;
            $st_pos = MAXRESULT * $st + 1;
            $s_data = $dataService->Query("$sqlQuery  STARTPOSITION $st_pos MAXRESULTS $mres ");
            if ($s_data && !empty($s_data)) {
                $entities = array_merge($entities, $s_data);
                $total += count($s_data);
                $st = $st + 1;

                $limit = ($st) * MAXRESULT;
            } else {
                $keepRunning = false;
                break;
            }
        }
        $error = $dataService->getLastError();
        if ($error != null) {
            $data['connect'] = 'no';
        }else{
            $data['connect'] = 'yes';
        }
        $data['gt_result'] = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 1]);

        $in = $this->session->flashdata('disconnect_redircect');
        $data['rediect_dis']  = 0;
        if($in==1)
        {
            $data['rediect_dis'] = 1;
        }

        $condition         = array('merchantID' => 0,'is_admin' => 1);
        $data['companies'] = $this->general_model->get_table_data('QBO_token', $condition);
        $data['sync_settings'] = $this->general_model->get_select_data('tbl_admin_sync_settings', ['*'], 'id IS NOT NULL');
        $data['customers'] = $this->general_model->get_select_data('tbl_admin_sync_settings', ['*'], 'id IS NOT NULL');
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/page_company', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function qb_online()
    {
        $config_data = $this->general_model->get_table_data('QBO_quickbooksonline_config', ['adminQBO' => 1]);
        if (!empty($config_data)) {
            foreach ($config_data as $configs) {

                $authorizationRequestUrl = $configs['authorizationRequestUrl'];
                $tokenEndPointUrl        = $configs['tokenEndPointUrl'];
                $client_id               = $configs['client_id'];
                $client_secret           = $configs['client_secret'];
                $scope                   = $configs['oauth_scope'];
                $redirect_uri            = $configs['oauth_redirect_uri'];
            }
        }
        $response_type         = 'code';
        $state                 = 'RandomState';
        $include_granted_scope = 'false';
        $grant_type            = 'authorization_code';

        $client = new Client($client_id, $client_secret);

        if (!isset($_GET["code"])) {

            $authUrl = $client->getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state);
            header("Location: " . $authUrl);
            exit();
        } else {
            $code          = $_GET["code"];
            $responseState = $_GET['state'];
            if (strcmp($state, $responseState) != 0) {
                throw new Exception("The state is not correct from Intuit Server. Consider your app is hacked.");
            }
            $result = $client->getAccessToken($tokenEndPointUrl, $code, $redirect_uri, $grant_type);

            //record them in the session variable
            $in_data['realmID']      = $_GET['realmId'];
            $in_data['accessToken']  = $result['access_token'];
            $in_data['refreshToken'] = $result['refresh_token'];

            $merchID               = 0;
            $merchantID            = $merchID;
            $in_data['merchantID'] = $merchantID;
            $in_data['createdAt']  = date('Y-m-d H:i:s');
            $in_data['is_admin']  = 1;
            $in_data['Validity']   = "";

            $chk_condition = array('merchantID' => 0,'is_admin' => 1);
            $tok_data      = $this->general_model->get_row_data('QBO_token', $chk_condition);

            if (!empty($tok_data)) {

                $this->general_model->update_row_data('QBO_token', $chk_condition, $in_data);

            } else {
                $this->general_model->insert_row('QBO_token', $in_data);

            }
            $this->clean_qbo_data();
            $this->get_qbo_data();
            
            redirect('AdminQuickBook/auth_success');
        }
    }
    

    public function auth_success()
    {
        $data['primary_nav']  = primary_nav();
        $data['template']   = template_variable();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/qb_online_success_msg',$data);
        $this->load->view('template/page_footer',$data);
        $this->load->view('template/template_end', $data);
    }

    public function get_qbo_data()
    {
        require_once APPPATH .'libraries/qbo/QBO_data.php';
        $this->qbo = new QBO_data();
        
    }

    public function clean_qbo_data()
    {
        $this->delete_customer_data();
        $this->delete_items_data();
    }
    
    public function delete_customer_data()
    {
        $this->general_model->table_truncate('tbl_admin_qbo_custom_customer');
    }

    public function delete_items_data()
    {
        $this->general_model->table_truncate('tbl_admin_qbo_custom_customer');
    }

    public function disconnet_account()
    {
        $val = array('merchantID' => 0,'is_admin' => 1);
       
        $data = $this->general_model->get_row_data('QBO_token', $val);

        $accessToken  = $data['accessToken'];
        $refreshToken = $data['refreshToken'];
        $realmID      = $data['realmID'];

        $client = new Client($this->config->item('client_id'), $this->config->item('client_secret'));
        $grant_type = 'revoke token';
        $tokenEndPointUrl = 'https://developer.api.intuit.com/v2/oauth2/tokens/revoke';
        $revokeResult = $client->revokeToken($tokenEndPointUrl, $grant_type, $accessToken);
       
        $updateData = [
            'accessToken' => 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..g06ceuiEds-sNWQIf13OEw.7yX87FX8JrlLODzAprXviLm7QmgOeyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..g06ceuiEds-sNWQIf13OEw.7yX87FX8JrlLODzAprXviLm7Qmg',
            'refreshToken' => '',
            'realmID' => 'disconnected',
        ];
        $this->general_model->update_row_data('QBO_token', $val, $updateData);
        $this->session->set_flashdata('success', 'Successfully Disconnected');
        $res = array('status' => "success");
        echo json_encode($res);
        die;
    }
    public function qbo_log()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        if ($this->session->userdata('logged_in')) {
            $data['login_info'] = $this->session->userdata('logged_in');

            $user_id = $data['login_info']['merchID'];
        }
        if ($this->session->userdata('user_logged_in')) {
            $data['login_info'] = $this->session->userdata('user_logged_in');

            $user_id = $data['login_info']['merchantID'];
        }

        $data['alls'] = $this->general_model->get_table_data('tbl_admin_qbo_log', array());

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/page_log', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
    public function sync_event()
    {
        $args = [];
        $logID = $this->czsecurity->xssCleanPostInput('logID');
        $returnObj = array('status'=> 0,'message' => 'Not sync');
        $this->qboOBJ = new QBO_data();
        $logData = $this->general_model->get_row_data('tbl_admin_qbo_log', array('id' => $logID));
        
        redirect(base_url('QBO_controllers/home/qbo_log'), 'refresh');
    }
}