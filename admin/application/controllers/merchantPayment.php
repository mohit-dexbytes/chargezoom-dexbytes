<?php
ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException; 

class merchantPayment extends CI_Controller  
{
    
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->model('quickbooks');
		$this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('general_model');
		$this->load->model('list_model');
		$this->load->model('card_model');
		$this->load->model('send_email_model');
	    $this->db1= $this->load->database('otherdb', TRUE);
	    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
       
	}
	

	public function index(){
	   
	   
   }
   	
	public function test(){
	    $port_url = current_url();
	    redirect('wrong_url');   
	}
	

    private function safe_decode($string) {
        return base64_decode(strtr($string, '-_-', '+/='));
    }
    
    
    public function wrong_url(){
	   
	   $this->load->view('QBO_views/wrong_page'); 
	    
	}

	 public function check_vault_merchant()
	 {
		 
 	
		  $card=''; $card_name=''; $customerdata=array();
		  
		 $card_data2 = '';
		 
		 
		 if($this->czsecurity->xssCleanPostInput('merchantID')!=""){
			 
				$merchantID 	= $this->czsecurity->xssCleanPostInput('merchantID'); 
			
			 	$condition     =  array('merchID'=>$merchantID); 
			    $customerdata = $this->general_model->get_row_data('tbl_merchant_data',$condition);
				
				if(!empty($customerdata)){
	                 				
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_merchant_card_expiry_data($merchantID);
					  
					 if(!empty($card_data))
					 {
						 
					  	$customerdata['card']  = $card_data;
					  
					 }else{
						
						$customerdata['card']  = '';
					}
					
		            echo json_encode($customerdata)	;
					die;
				  
					
			    } 	 
			 
	      }		 
		 
	 }

	  /*Merchant payment do by admin*/

    public function merchantAutoBillCharge()
	{
		
	}
	 /*Merchant payment do by admin*/

    public function merchant_payment()
	{
        
  
      
	    $paid_status = '';  
        $res11 = '';
        
		if(!empty($this->input->post(null, true)))
       	{
            $cardID = $this->czsecurity->xssCleanPostInput('merchantCardID');
                
            $postData = $this->input->post(null, true);   
         
            $gateway = $this->czsecurity->xssCleanPostInput('gateway');
            
            $sandbox  =TRUE;
                
            $invoice = $this->czsecurity->xssCleanPostInput('invoice');

            if($invoice)
            {
					
        	    $con = array('invoice'=>$invoice);
				
        	    $in_data = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $con);
				
        	    $data['get_invoice'] = $in_data;

				if($in_data)
                {
					$merchantID = $in_data['merchantID'];
					$merchantName = $in_data['merchantName'];
        	        $payamount = $in_data['BalanceRemaining'];

					$merchant_data = $this->general_model->get_row_data('tbl_merchant_data', ['merchID' => $merchantID]);
					$in_data['merchant_data'] = $merchant_data;
				
			   		$cone = array('gatewayID'=>$gateway);
				
        	    	$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $cone);
				
        	    	$bill_email='';

        	    	$card_data = [];

        	    	$gatewayType = $get_gateway['gatewayType'];

	             	if($cardID != 'new1')
				   	{
				    	$card_data = $this->card_model->get_merchant_single_card_data($cardID);

				   	}
					
        	        if($gatewayType == 1) 
					{
						$dataCall = $this->nmiGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox); 
					}elseif($gatewayType == 2){       

                        $dataCall = $this->authorizeGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);  
					}elseif($gatewayType == 3){
                        
                    	 $dataCall = $this->payTraceGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox); 
                        
                    }elseif($gatewayType == 4){

                   		$dataCall = $this->payPalGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);
						   
				    }elseif($gatewayType==5){

                        $dataCall = $this->stripeGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);

                    }elseif($gatewayType==6){   
					   	$dataCall = $this->USAePayGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);
				    }elseif($gatewayType==7){
                        $dataCall = $this->heartLandGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);
    		            
				   	}elseif($gatewayType==8)
				   	{    
                    	$dataCall = $this->cyberSourceGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);
				   	}elseif($gatewayType==9)
				   	{    
                    	$dataCall = $this->nmiGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox);
				   	}elseif($paid_status == '1' &&  $cardID == 'new1')
					{
						$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('card_number'));
						$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry'));
					    $exyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('expiry_year'));
				        $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cvv')); 
						
						$friendlyname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('friendlyname'));
						$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address1'));
						$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
						$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
						$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
						$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
						$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zipcode'));
						$contact = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('contact'));
									
									
						$Rcon = array('merchID'=>$merchantID);
						
						$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
						
						$fname = $reseller_data['firstName'];
						$lname = $reseller_data['lastName'];
						$billing_email = $reseller_data['merchantEmail'];
						$resellerCardData['cardMerchantID'] = $merchantID;
						$resellerCardData['CustomerCard'] = $this->card_model->encrypt($cnumber);
						$resellerCardData['cardMonth'] = $expmonth;
						$resellerCardData['cardYear'] = $exyear;
						$resellerCardData['CardCVV'] = $this->card_model->encrypt($cvv);
						
						$cardTypeData = $this->general_model->getType($cnumber);
						
						$resellerCardData['cardtype'] = $cardTypeData;
						
						$resellerCardData['Billing_First_Name'] = $fname;
						$resellerCardData['Billing_Last_Name'] = $lname;
						$resellerCardData['Billing_Addr1'] = $address;
						$resellerCardData['Billing_Addr2'] = $address2;
						
						$resellerCardData['friendlyName'] = $cardTypeData.' '.'-'.substr($cnumber,-4);
						
						
						
						$resellerCardData['Billing_Country'] = $country;
						$resellerCardData['Billing_City'] = $city;
						$resellerCardData['Billing_State'] = $state;
						$resellerCardData['Billing_Zipcode'] = $zip;
						$resellerCardData['Billing_Contact'] = $contact;
						$resellerCardData['Billing_Email_Address'] = $billing_email;
									
						$res11 = $this->card_model->get_card_row_data('merchant_card_data',array('merchantListID'=>$merchantID));	
						if($res11){
							$this->card_model->update_card_data(array('cardMerchantID'=>$merchantID),$resellerCardData);
						}
						else{
							$this->card_model->insert_card_data($resellerCardData);	
						}									 
										
						$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
                        redirect('home/invoices/');	
                             	
					}else{

					}  
						  
				}else{
        		        
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select Marchemt gateway.</div>'); 
						redirect($_SERVER['HTTP_REFERER']);
						
                } 
    	      
		
            }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Email token not matched</div>'); 
    			redirect($_SERVER['HTTP_REFERER']);
         	}
			 
			 
    	}else{
            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid Request</div>'); 
    		redirect($_SERVER['HTTP_REFERER']);
    	}
		
	
    }

    public function nmiGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
		include APPPATH . 'third_party/nmiCustomerVault.class.php';
		//start NMI
		$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];

        $conditionMerch    =  array('merchID'=>$merchantID); 
		$merchantData = $this->general_model->get_row_data('tbl_merchant_data',$conditionMerch);
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$nmiuser   = $get_gateway['gatewayUsername'];
		$nmipass   = $get_gateway['gatewayPassword'];
		$sch_method = $postData['sch_method'];
		$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
		if($payamount > 0)
		{
			$transaction1 = new nmiDirectPost($nmi_data); 

			if($sch_method == 1){
				if($cardID != 'new1')
				{					
					
					$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];
					
					$CardType	 = $card_data['CardType'];

				}else{

					$cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				
				$transaction1->setCcNumber($cnumber);
		    	$expmonth =  $expmonth;
			
				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
		    	$expry    = $expmonth.$exyear; 
				$transaction1->setCcExp($expry);
				$transaction1->setCvv($cvv);

			}else if($sch_method == 2){

				if($cardID != 'new1')
				{					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					if($card_data['acct_type'] == 'saving'){
						$accountType = 'savings';
					}else{
						$accountType = $card_data['acct_type'];
					}
					
					$accountHolderType = $card_data['acct_holder_type'];
					

				}else{
					$accountName = $this->security->xss_clean($postData['acc_name']);
					$accountNumber = $this->security->xss_clean($postData['acc_number']);
					$routeNumber = $this->security->xss_clean($postData['route_number']);
					if($postData['acct_type'] == 'saving'){
						$accountType = 'savings'; 
					}else{
						$accountType = $this->security->xss_clean($postData['acct_type']); 
					}
					$accountHolderType = $this->security->xss_clean($postData['acct_holder_type']); 

				}

				if(isset($in_data['merchant_data'])){
					$accountName = $in_data['merchant_data']['companyName'];
				}

				$transaction1->setAccountName($accountName);
				$transaction1->setAccount($accountNumber);
				$transaction1->setRouting($routeNumber);

				$sec_code = 'WEB';

				$transaction1->setAccountType($accountType);
				$transaction1->setAccountHolderType($accountHolderType);
				$transaction1->setSecCode($sec_code);

				$transaction1->setPayment('check');
			}else{

			}
			$transaction1->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $in_data['invoiceNumber']);
			if($Billing_Addr1 != ''){
				$transaction1->setCountry($Billing_Addr1);
			}else{
				if($merchantData['merchantAddress1'] != ''){
					$transaction1->setAddress1($merchantData['merchantAddress1']);
				}
			}
			
			if($Billing_Country != ''){
				$transaction1->setCountry($Billing_Country);
			}else{
				if($merchantData['merchantCountry'] != ''){
					$transaction1->setCountry($merchantData['merchantCountry']);
				}else{
					$transaction1->setCountry('US');
				}
				
			}	
			
			if($Billing_City != ''){
				$transaction1->setCity($Billing_City);
			}else{
				if($merchantData['merchantCity'] != ''){
					$transaction1->setCity($merchantData['merchantCity']);
				}
			}
			
			if($Billing_Zipcode != ''){
				$transaction1->setZip($Billing_Zipcode);
			}else{
				if($merchantData['merchantZipCode'] != ''){
					$transaction1->setZip($merchantData['merchantZipCode']);
				}
			}
			if($billing_phone_number != ''){
				$transaction1->setPhone($billing_phone_number);
			}else{
				if($merchantData['merchantContact'] != ''){
					$transaction1->setPhone($merchantData['merchantContact']);
				}
			}
			
			if($Billing_State != ''){
				$transaction1->setState($Billing_State);
			}else{
				if($merchantData['merchantState'] != ''){
					$transaction1->setState($merchantData['merchantState']);
				}
			}


			$transaction1->setEmail($merchantData['merchantEmail']);
			$transaction1->setCompany($merchantData['companyName']);
			$transaction1->setFirstName($merchantData['firstName']);
			$transaction1->setLastName($merchantData['lastName']);

			$transaction1->setAmount($payamount);
			$transaction1->setTax('tax');
			$transaction1->sale();
			$getwayResponse = $transaction1->execute();
			if( $getwayResponse['response_code']=="100"){
				$invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status = '1';
				$pay        = $payamount;
				 $remainbal  = $in_data['BalanceRemaining']-$payamount;
				$data   	 = array('status'=>$status, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );

				$condition  = array('invoice'=>$in_data['invoice'] );
				 
				$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
			    				
			}else{
		        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:'.$getwayResponse['responsetext'].'</strong> </div>');  
		        	redirect($_SERVER['HTTP_REFERER']);
		    } 
		    $transaction = array();
		    $transaction['transactionID']      = $getwayResponse['transactionid'];
			$transaction['transactionStatus']  = $getwayResponse['responsetext'];
			$transaction['transactionCode']    = $getwayResponse['response_code'];
			$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
			$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['merchant_invoiceID']       = $in_data['invoice'];
			$transaction['gatewayID']          = $get_gateway['gatewayID'];
			$transaction['transactionGateway'] = 1;	
			$transaction['transactionAmount']  = $payamount;
			$transaction['gateway']            = "NMI";
			$transaction['merchantID']         = $merchantID;
		  
			 $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
			$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
            redirect('home/invoices/');	
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}	
		
    }

    public function authorizeGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	
    	$this->load->config('auth_pay');

    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];

    	if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}
    	//Login in Auth
        $apiloginID       = $get_gateway['gatewayUsername'];
        $transactionKey   = $get_gateway['gatewayPassword'];
        $sch_method = $postData['sch_method'];
        if($payamount > 0)
		{
			$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey);
			$transaction1->setSandbox($this->config->item('Sandbox'));
			$transaction1->__set('first_name',$billing_first_name);
			$transaction1->__set('last_name', $billing_last_name);
			$transaction1->__set('address', $Billing_Addr1);
			$transaction1->__set('country',$Billing_Country);
			$transaction1->__set('city',$Billing_City);
			$transaction1->__set('state',$Billing_State);
			$transaction1->__set('phone',$billing_phone_number);
			$transaction1->__set('email',$billing_email);

			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}

				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
	 	        $expry = $expmonth.$exyear;  

	 	        
	 	            
				$getwayResponse = $transaction1->authorizeAndCapture($payamount,$cnumber,$expry);

			}elseif($sch_method == 2){
				if($cardID != 'new1')
				{					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					$accountType = $card_data['acct_type'];
					$accountHolderType = $card_data['acct_holder_type'];
					

				}else{
					$accountName = $this->security->xss_clean($postData['acc_name']);
					$accountNumber = $this->security->xss_clean($postData['acc_number']);
					$routeNumber = $this->security->xss_clean($postData['route_number']);
					$accountType = $this->security->xss_clean($postData['acct_type']); 
					$accountHolderType = $this->security->xss_clean($postData['acct_holder_type']); 
					
					
				}
				$sec_code =     'WEB';
				

				$transaction1->setECheck($routeNumber, $accountNumber, $accountType, $bank_name='Wells Fargo Bank NA', $accountName, $sec_code);
				$getwayResponse = $transaction1->authorizeAndCapture($payamount);
			}else{

			}
			if( $getwayResponse->response_code=="1")
		   	{
				$invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status = '1';
				$pay        = $payamount;
				$remainbal  = $in_data['BalanceRemaining']-$payamount;
				$app        = $in_data['AppliedAmount']+$payamount;

				$data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );

				$condition  = array('invoice'=>$in_data['invoice'] );

				$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);        
				
			}else{
		        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $getwayResponse->response_reason_text .'</div>');  
		    }
			
			$transaction = array();
			$transaction['transactionID']      = $getwayResponse->transactionid;
			$transaction['transactionStatus']  = $getwayResponse->responsetext;
			$transaction['transactionCode']    = $getwayResponse->response_code;
			$transaction['transactionType']    = ($getwayResponse->type)?$getwayResponse->type:'Auth_sale';
			$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['merchant_invoiceID']       = $in_data['invoice'];
			$transaction['gatewayID']          = $get_gateway['gatewayID'];
			$transaction['transactionGateway'] = $get_gateway['gatewayType'];	
			$transaction['transactionAmount']  = $payamount;
			$transaction['gateway']            = "Auth";
			$transaction['merchantID']         = $merchantID;
	  
			$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
				
			
			$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
            redirect('home/invoices/');	

		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}

    }


    public function payTraceGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	include APPPATH . 'third_party/PayTraceAPINEW.php';
        $this->load->config('paytrace');
        
        $payusername   = $get_gateway['gatewayUsername'];
	    $paypassword   = $get_gateway['gatewayPassword'];
	    $grant_type    = "password";

	    $merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];

	    $integratorId = $get_gateway['gatewaySignature'];

	    $Rcon = array('merchID'=>$merchantID);
				
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
		
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
					
		$name = $fname." ".$lname;

		$invoice_no = $in_data['invoice'];

	    if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}
		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			$payAPI  = new PayTraceAPINEW();	
		    $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
        	//call a function of Utilities.php to verify if there is any error with OAuth token.
			$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if (!$oauth_moveforward) {

				$json = $payAPI->jsonDecode($oauth_result['temp_json_response']);

				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token
				$oauth_token = sprintf("Bearer %s", $json['access_token']);
			}

			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}

				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
	 	        $expry = $expmonth.$exyear;

	 	        $request_data = array(
					"amount"          => $payamount,
					"credit_card"     => array(
						"number"           => $cnumber,
						"expiration_month" => $expmonth,
						"expiration_year"  => $exyear),
					"csc"             => $cvv,
					"invoice_id"      => rand('500000', '200000'),
					"billing_address" => array(
						"name"           => $name,
						"street_address" => $Billing_Addr1,
						"city"           => $Billing_City,
						"state"          => $Billing_State,
						"zip"            => $Billing_Zipcode,
					),
				);
	 	       
			}elseif($sch_method == 2){

				if($cardID != 'new1')
				{					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					$accountType = $card_data['acct_type'];
					$accountHolderType = $card_data['acct_holder_type'];
					

				}else{
					$accountName = $this->security->xss_clean($postData['acc_name']);
					$accountNumber = $this->security->xss_clean($postData['acc_number']);
					$routeNumber = $this->security->xss_clean($postData['route_number']);
					$accountType = $this->security->xss_clean($postData['acct_type']); 
					$accountHolderType = $this->security->xss_clean($postData['acct_holder_type']); 
		
				}

				$request_data = array(
					"amount"          => $payamount,
					"check"     => array(
						"account_number"=> $accountNumber,
						"routing_number"=> $routeNumber,
					),
					"integrator_id" => $integratorId,
					"billing_address" => array(
						"name" => $accountName,
						"street_address" => $Billing_Addr1,
						"city" => $Billing_City,
						"state" => $Billing_State,
						"zip" => $Billing_Zipcode,
					),
				);

				
			}else{
				
			}

			

			$request_data = json_encode($request_data); 
           	$gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	 
	       	$response  = $payAPI->jsonDecode($gatewayres['temp_json_response']); 
           	if ( $gatewayres['http_status_code']=='200' )
           	{ 
				$invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status = '1';
				$pay        = $payamount;
				$remainbal  = $in_data['BalanceRemaining']-$payamount;
				$app        = $in_data['AppliedAmount']+$payamount;

				$data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );

				$condition  = array('invoice'=>$in_data['invoice'] );
				$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
		    }
			else{
			    
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>' . $response['status_message'] .'</div>'); 
			}
			$transaction = array();
			
			if(isset($response['transaction_id'])){
				$transaction['transactionID']   = $response['transaction_id'];
			}
			else{
				$transaction['transactionID']   = '';
			}
		
			$transaction['transactionStatus']  = $response['status_message'];
			$transaction['transactionCode']    = $response['response_code'];
			$transaction['transactionType']    = 'Pay_sale';
			$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['merchant_invoiceID']       = $in_data['invoice'];
			$transaction['gatewayID']          = $get_gateway['gatewayID'];
			$transaction['transactionGateway'] = $get_gateway['gatewayType'];	
			$transaction['transactionAmount']  = $payamount;
			$transaction['gateway']            = "Paytrace";
			$transaction['merchantID']         = $merchantID;
		   	$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
		 
		   $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
           redirect('home/invoices/');	
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}
    }

    public function payPalGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox)
    {
    	include APPPATH . 'third_party/PayPalAPINEW.php';
					
   		$this->load->config('paypal');

   		$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];

   		$config = array(
			'Sandbox' => $this->config->item('Sandbox'),			// Sandbox / testing mode option.
			'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
			'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
			'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
			'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
			'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
		);
		  
		$this->load->library('paypal/Paypal_pro', $config);	  
		  
	  	if($config['Sandbox'])
		{
			error_reporting(E_ALL);
			ini_set('display_errors', '1');
		}

		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}
		$Rcon = array('merchID'=>$merchantID);
				
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
		
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
		
		$name = $fname." ".$lname;
  
		$bill_email = $reseller_data['merchantEmail'];

		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			if($sch_method == 1){

				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
	 	        $expry = $expmonth.$exyear;

	 	        $creditCardType   = 'Visa';
				$creditCardNumber = $cnumber;
				$expDateMonth     = $expmonth;
			    $expDateYear      = $exyear;
				$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
				$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
				$cvv2Number       =   $cvv;
				$currencyID       = "USD";
				
				
								
        		$DPFields = array(
					'paymentaction' => 'Sale', 	                // How you want to obtain payment.  
					'ipaddress' => '', 							// Required.  IP address of the payer's browser.
					'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
        		);
        						
        		$CCDetails = array(
					'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
					'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
					'expdate'        => $expry, 							// Required.  Credit card expiration date.  Format is MMYYYY
					'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
					'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
					'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
				);
        						
        		$PayerInfo = array(
					'email'          => $bill_email, 								// Email address of payer.
					'payerid'        => '', 							// Unique PayPal customer ID for payer.
					'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
					'business'       => '' 							// Payer's business name.
				);  
        						
        		$PayerName = array(
					'salutation'     => '', 						// Payer's salutation.  20 char max.
					'firstname'      => $fname, 							// Payer's first name.  25 char max.
					'middlename'     => '', 						// Payer's middle name.  25 char max.
					'lastname'       => $lname, 							// Payer's last name.  25 char max.
					'suffix'         => ''								// Payer's suffix.  12 char max.
				);
        					
        		$BillingAddress = array(
					'street'         => $Billing_Addr1, 						// Required.  First street address.
					'street2'        => null, 						// Second street address.
					'city'           => $Billing_City, 							// Required.  Name of City.
					'state'          => $Billing_State, 							// Required. Name of State or Province.
					'countrycode'    => $Billing_Country, 					// Required.  Country code.
					'zip'            => $Billing_Zipcode 						// Phone Number of payer.  20 char max.
				);
        	
        							
               $PaymentDetails = array(
					'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
					'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
					'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
					'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
					'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
					'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
					'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
					'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
					'custom'         => '', 						// Free-form field for your own use.  256 char max.
					'invnum'         => '', 						// Your own invoice or tracking number
					'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
					'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
					'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
				);					

				$PayPalRequestData = array(
					'DPFields'       => $DPFields, 
					'CCDetails'      => $CCDetails, 
					'PayerInfo'      => $PayerInfo, 
					'PayerName'      => $PayerName, 
					'BillingAddress' => $BillingAddress, 
					'PaymentDetails' => $PaymentDetails, 
					
				);
					
		        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
		        
		        
			    if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])){
			        
					$invoice      = $in_data['invoice'];  
					$status 	 = 'paid';
					$paid_status = '1';
					$pay        = $payamount;
					$remainbal  = $in_data['BalanceRemaining']-$payamount;
					$app        = $in_data['AppliedAmount']+$payamount;
					 
					 $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
					
					 $condition  = array('invoice'=>$in_data['invoice'] );
					 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
			        
				 }
			    else
				{
				    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$PayPalResult["ACK"].'</div>'); 
				   
				}
				 
			    $transaction= array();
                $tranID ='' ;
                $amt='0.00';
		        if(isset($PayPalResult['TRANSACTIONID'])) { 
		            $tranID = $PayPalResult['TRANSACTIONID'];   
		            $amt=$PayPalResult["AMT"];  
		        }
                    
                    
	            $transaction['transactionID']       = $tranID;
		        $transaction['transactionStatus']   = $PayPalResult["ACK"];
		        $transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
		        $transactiondata['transactionModified']= date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
		        $transaction['transactionCode']     = $code;  
		        $transaction['transactionType']     = "Paypal_sale";	
			    $transaction['gatewayID']           = $get_gateway['gatewayID'];
                $transaction['transactionGateway']  = $get_gateway['gatewayType'];					
		     	$transaction['merchant_invoiceID']        = $in_data['invoice'];
		        $transaction['transactionAmount']   = $payamount;
		        $transaction['gateway']             = "Paypal";
		        $transaction['merchantID']         = $merchantID;
			
		        $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
		           
			    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
                redirect('home/invoices/');	


			}elseif($sch_method == 2){
				
			}else{
				
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}

    }

    public function stripeGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
    	
    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $Rcon = array('merchID'=>$merchantID);
						
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
			
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
		
		$name = $fname." ".$lname;
  		
		$bill_email = $reseller_data['merchantEmail'];
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{

			$plugin = new ChargezoomStripe();
		    $plugin->setApiKey($get_gateway['gatewayPassword']);
			
			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
	 	        $expry = $expmonth.$exyear;
	 	        
	 	        

				try {
				  	 $res =  \Stripe\Token::create([
						'card' => [
						'number' =>$cnumber,
						'exp_month' => $expmonth,
						'exp_year' =>  $exyear,
						'cvc' => $cvv,
						'address_city'    =>   $Billing_City,
	                    'address_country'   => $Billing_Country,
	                    'address_line1'     => $Billing_Addr1,
	                    'address_line1_check'     => null,
	                    'address_line2'     => $Billing_Addr2,
	                    'address_state'     => $Billing_State,
	                    'address_zip' => $Billing_Zipcode,
	                    'address_zip_check' => null,
						'name' => $merchantName
					   ]
					]);
               
					$tcharge= json_encode($res);  
					$rest = json_decode($tcharge);


				}catch (Exception $e) {
					
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$e->getMessage() .'</div>'); 
				  // Something else happened, completely unrelated to Stripe
				}
			}elseif($sch_method == 2){
				
			}else{
				
			}

			if($payamount > 0 && $payamount < 0.5){
				$payamount = 0.5;
			}
			$paidamount  =  (int)($payamount*100); 
			
			
			

			if(isset($rest->id))
			{	


				try {
					$customer = \Stripe\Customer::create(array(
					    'name' => $merchantName,
					    'description' => 'CZ Merchant',
					    'source' => $rest->id,
					    "address" => ["city" => $Billing_City, "country" => $Billing_Country, "line1" => $Billing_Addr1, "line2" => $Billing_Addr2, "postal_code" => $Billing_Zipcode, "state" => $Billing_State]
					));
					$customer= json_encode($customer);
	              	  
					$resultstripecustomer = json_decode($customer);
				}catch (Exception $e) {
					
				  // Something else happened, completely unrelated to Stripe
				}
				
				

			 	try {
			 		if(isset($resultstripecustomer->id))
					{	
						$charge =	\Stripe\Charge::create(array(
					  	  'customer' => $resultstripecustomer->id,
						  "amount" => $paidamount,
						  "currency" => "usd",
						  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
						 
						));	
	               
						
					}else{
						$charge =	\Stripe\Charge::create(array(
						  "amount" => $paidamount,
						  "currency" => "usd",
						  "source" => $rest->id, 
						  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
						 
						));	
	               
						
						
					}
				  	
					$charge= json_encode($charge);
	              	  
					$resultstripe = json_decode($charge);

				}catch (Exception $e) {
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$e->getMessage() .'</div>'); 
				  // Something else happened, completely unrelated to Stripe
				}
				
            	$trID='';
            	if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
	         	{
		            $trID       = $resultstripe->id;
		            $code		=  '200';
				    $invoiceID  = $in_data['invoice'];  
					$status 	= 'paid';
					$paid_status = '1';
					$pay        = $payamount;
					$remainbal  = $in_data['BalanceRemaining']-$payamount;
			     	$app        = $in_data['AppliedAmount']+$payamount;
					 
					$data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
					 
					$condition  = array('invoice'=>$in_data['invoice'] );
					$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

		        	$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
	        	}
	        	else
				{
					if(isset($resultstripe->status)){
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$resultstripe->status .'</div>'); 
					}
			    	
			   
				}    
			
			 	$transaction = array();
			 
				$transaction['transactionID']       = $trID;
				$transaction['transactionStatus']   = $resultstripe->status;
				$transaction['transactionDate']     = date('Y-m-d H:i:s');  
				$transaction['transactionModified']     = date('Y-m-d H:i:s'); 
				$transaction['transactionCode']     = $code;  
				$transaction['merchant_invoiceID']        = $in_data['invoice'];
				$transaction['transactionType']     = 'Stripe_sale';	
				$transaction['gatewayID']           = $get_gateway['gatewayID'];
				$transaction['transactionGateway']  = $get_gateway['gatewayType'] ;					
				$transaction['transactionAmount']   = $payamount;
				$transaction['gateway']             = "Stripe";
				$transaction['merchantID']         = $merchantID;
		
			  	$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
			  	
								
                redirect('home/invoices/');	
			}else{
				if(isset($rest->status)){
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$rest->status .'</div>');
				}
				 
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}
		redirect('home/invoices/');	
    }

    public function USAePayGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	require_once APPPATH."third_party/usaepay/usaepay.php";	
    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $Rcon = array('merchID'=>$merchantID);
						
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
			
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
		
		$name = $fname." ".$lname;
  
		$bill_email = $reseller_data['merchantEmail'];
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			$payusername   = $get_gateway['gatewayUsername'];
            $password      = $get_gateway['gatewayPassword'];	
			$cvv='';	
			$invNo  =mt_rand(1000000,2000000); 
			$transaction = new umTransaction;
			$transaction->key=$payusername;
			$transaction->pin=$password;
		    $transaction->usesandbox=$sandbox;
			$transaction->invoice=$invNo;// invoice number.  must be unique.
			$transaction->description="Chargezoom Public Invoice Payment";	// description of charge
			
			$transaction->testmode=0;   // Change this to 0 for the transaction to process
			$transaction->command="sale";	

			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$expyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$expyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				$transaction->card = $cnumber;
				$expyear   = substr($expyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
				$expry    = $expmonth.$expyear;  
				$transaction->exp = $expry;
                if($cvv!="")
                $transaction->cvv2 = $cvv;

			}elseif($sch_method == 2){
				
			}else{
				
			}


			$transaction->billfname = $fname;
			$transaction->billlname = $lname;
			$transaction->billstreet = $Billing_Addr1;
			$transaction->billstreet2 = null;
			$transaction->billcountry = $Billing_Country;
			$transaction->billcity    = $Billing_City;
			$transaction->billstate = $Billing_State;
			$transaction->billzip = $Billing_Zipcode;
			
			
			$transaction->shipfname = $fname;
			$transaction->shiplname = $lname;
			$transaction->shipstreet = $Billing_Addr1;
			$transaction->shipstreet2 = null;
			$transaction->shipcountry = $Billing_Country;
			$transaction->shipcity    = $Billing_City;
			$transaction->shipstate = $Billing_State;
			$transaction->shipzip = $Billing_Zipcode;
		
			$amount =$payamount;

			$transaction->amount = $amount;
			$transaction->Process();
            
            $error=''; 	
                			       
            if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
            {			        
                    		
                $msg = $transaction->result;
                $trID = $transaction->refnum;
                $code_data ="SUCCESS";
                $tr_type  = 'sale';
                $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
			    $invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status 	 = '1';
				$pay        = $payamount;
				$remainbal  = $in_data['BalanceRemaining']-$payamount;
				$app        = $in_data['AppliedAmount']+$payamount;

                $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
				$condition  = array('invoice'=>$in_data['invoice'] );

                $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

			}
			else
			{
										 
				$msg = $transaction->error;
                $trID = $transaction->refnum;
                $result =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
											
											
			}
									
									
			$transaction1 = array();
				 
			$transaction1['transactionID']       = $result['transactionId'];
			$transaction1['transactionStatus']   = $result['status'];
			$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
			$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
			$transaction1['transactionCode']     =  $result['transactionCode']; 
			$transaction1['merchant_invoiceID']        = $in_data['invoice'];
			$transaction1['transactionType']     = 'Stripe_sale';	
			$transaction1['gatewayID']           = $get_gateway['gatewayID'];
			$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
			$transaction1['transactionAmount']   = $payamount;
			$transaction1['gateway']             = "usaePay";
			$transaction1['merchantID']         = $merchantID;
	
		    $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction1);					
								
            $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
            redirect('home/invoices/');	
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}

    }

    public function heartLandGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){
    	require_once dirname(__FILE__) . '../../../../vendor/autoload.php';
    		           
    	$this->load->config('globalpayments');

    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $Rcon = array('merchID'=>$merchantID);
						
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
			
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
		
		$name = $fname." ".$lname;
  
		$bill_email = $reseller_data['merchantEmail'];
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}
		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			$payusername   = $get_gateway['gatewayUsername'];
            $secretApiKey   = $get_gateway['gatewayPassword'];

		    $config = new PorticoConfig();

            $config->secretApiKey = $secretApiKey;
			$sandbox = true;
			if($sandbox){
				$config->serviceUrl =  $this->config->item('GLOBAL_URL');
			}else{
				$config->serviceUrl =  $this->config->item('PRO_GLOBAL_URL');
			}
     		$config->developerId =  $this->config->item('DeveloperId');
			$config->versionNumber =  $this->config->item('VersionNumber');
                     
	        ServicesContainer::configureService($config);

			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$expyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$expyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				
				$card = new CreditCardData();
                $card->number = $cnumber;
                $card->expMonth = $expmonth;
                $card->expYear = $expyear;
                if($cvv!="")
                $card->cvn = $cvv;
                $card->cardType=$cardtype;
           
                $address = new Address();
                $address->streetAddress1 = $address1;
                $address->city = $city;
                $address->state = $state;
                $address->postalCode = $zip;
                $address->country = $country;
                
 
                $invNo  =mt_rand(5000000,20000000);
             	try
                {
                    $response = $card->charge($payamount)
                    ->withCurrency("USD")
                    ->withAddress($address)
                    ->withInvoiceNumber($invNo)
                    ->withAllowDuplicates(true)
                    ->execute();

			        $error=''; 	  
                    if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                    {
                        $msg = $response->responseMessage;
                        $trID = $response->transactionId;
    				    $code_data ="SUCCESS";
    				    $tr_type  = 'sale';
                        				      
                        $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
    					$invoice      = $in_data['invoice']; 
						$status 	 = 'paid';
						$paid_status = '1';
						$pay        = $payamount;
						$remainbal  = $in_data['BalanceRemaining']-$payamount;
						$app        = $in_data['AppliedAmount']+$payamount;
 
                        $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
                         
						$condition  = array('invoice'=>$in_data['invoice'] );
						
                        $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

    				}
    				else
    				{
    					$msg = $response->responseMessage;
                        $trID = $response->transactionId;
                        $result =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
    					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
    					redirect($_SERVER['HTTP_REFERER']);
    									
    				}
    									
                }
                catch (BuilderException $e)
                {
                    $error= 'Build Exception Failure: ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                catch (ConfigurationException $e)
                {
                    $error='ConfigurationException Failure: ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                catch (GatewayException $e)
                {
                    $error= 'GatewayException Failure: ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                catch (UnsupportedTransactionException $e)
                {
                    $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                    redirect($_SERVER['HTTP_REFERER']);
                }
                catch (ApiException $e)
                {
                    $error=' ApiException Failure: ' . $e->getMessage();
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                   redirect($_SERVER['HTTP_REFERER']);
                }
                                    
                                   
                $transaction1 = array();
				 
				$transaction1['transactionID']       = $result['transactionId'];
				$transaction1['transactionStatus']   = $result['status'];
				$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
				$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
				$transaction1['transactionCode']     =  $result['transactionCode']; 
				$transaction1['merchant_invoiceID']        = $in_data['invoice'];
				$transaction1['transactionType']     = 'hertLand_sale';	
				$transaction1['gatewayID']           = $get_gateway['gatewayID'];
				$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
				$transaction1['transactionAmount']   = $payamount;
				$transaction1['gateway']             = "hertLand";
				$transaction1['merchantID']         = $merchantID;
			
				$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction1);		
				  
			    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
                redirect('home/invoices/');	

			}elseif($sch_method == 2){
				
			}else{
				
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}
    }

    public function cyberSourceGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){

    	$this->load->config('cyber_pay');

    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $Rcon = array('merchID'=>$merchantID);
						
		$reseller_data = $this->general_model->get_row_data('tbl_merchant_data', $Rcon);
			
		$fname = $reseller_data['firstName'];
		$lname = $reseller_data['lastName'];
		
		$name = $fname." ".$lname;
  
		$bill_email = $reseller_data['merchantEmail'];
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$expyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$expyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}

				$phone="4158880000"; $email="test@gmail.com"; $companyName='Dummy Company';
	        	$flag ='true';
              	$option =array();
                    
                $option['merchantID']     = trim($get_gateway['gatewayUsername']);
		        $option['apiKey']         = trim($get_gateway['gatewayPassword']);
				$option['secretKey']      = trim($get_gateway['gatewaySignature']);
				
		        
				$sandbox = true;
				
				if($sandbox)
				$env   = $this->config->item('SandboxENV');
				else
				$env   = $this->config->item('ProductionENV');
				$option['runENV']      = $env;
					
				$commonElement = new CyberSource\ExternalConfiguration($option);
				$config = $commonElement->ConnectionHost();
				$merchantConfig = $commonElement->merchantConfigObject();
				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
				
				$cliRefInfoArr = [
					"code" => "test_payment"
				];
				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
			
				if ($flag == "true")
				{
					$processingInformationArr = [
						"capture" => true, "commerceIndicator" => "internet"
					];
				}
				else
				{
					$processingInformationArr = [
						"commerceIndicator" => "internet"
					];
				}
				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

				$amountDetailsArr = [
					"totalAmount" => $payamount,
					"currency" => CURRENCY,
				];
				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
				
				$billtoArr = [
					"firstName" => $fname,
					"lastName"  =>$lname,
					"address1"  => $address1,
					"postalCode"=> $zip,
					"locality"  => $city,
					"administrativeArea" => $state,
					"country"  => $country,
					"phoneNumber" => $phone,
					"company"  => $companyName,
					"email"    => $email
				];
				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
				
				$orderInfoArr = [
					"amountDetails" => $amountDetInfo, 
					"billTo" => $billto
				];
				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
				
				$paymentCardInfo = [
					"expirationYear" => $expyear,
					"number" => $cnumber,
					"securityCode" => $cvv,
					"expirationMonth" => $expmonth
				];
				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
				
				$paymentInfoArr = [
					"card" => $card
				];
				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

				$paymentRequestArr = [
					"clientReferenceInformation" =>$client_reference_information, 
					"orderInformation" =>$order_information, 
					"paymentInformation" =>$payment_information, 
					"processingInformation" =>$processingInformation
				];
			
				
				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
				
				$api_response = list($response, $statusCode, $httpHeader) = null;
				$tr_type  = 'sale'; 
             	try
				{
					//Calling the Api
					$api_response = $api_instance->createPayment($paymentRequest);
				
			   
				
					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
					{
				    	$trID =   $api_response[0]['id'];
				    	$msg  =   $api_response[0]['status'];
				  
				    	$code =   '200';
				     	$tr_type  = 'sale';
                    	$result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                    	$code_data ="SUCCESS";
                    				 
                    	$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
						$invoice      = $in_data['invoice'];  
            			$status 	 = 'paid';
						$paid_status 	 = '1';
						$pay        = $payamount;
						$remainbal  = $in_data['BalanceRemaining']-$payamount;
						$app        = $in_data['AppliedAmount']+$payamount;
 
                        $data   	 = array('status'=>$status, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
						
						$condition  = array('invoice'=>$in_data['invoice'] );
						$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

					}
					else
					{
										   
						$trID =   $api_response[0]['id'];
						$msg  =   $api_response[0]['status'];
						$code =   $api_response[1];
						  
                        $tr_type  = 'sale';
                    	$result =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
										
					}
				}  
		        catch(Cybersource\ApiException $e)
				{
					print_r($e->getResponseBody());
			    	print_r($e->getMessage());  die;
			    	
					$error = $e->getMessage();
					$this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
					redirect($_SERVER['HTTP_REFERER']);
				}
			
			 	$transaction1 = array();
				 
				$transaction1['transactionID']       = $result['transactionId'];
				$transaction1['transactionStatus']   = $result['status'];
				$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
				$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
				$transaction1['transactionCode']     =  $result['transactionCode']; 
				$transaction1['merchant_invoiceID']        = $in_data['invoice'];
				$transaction1['transactionType']     = 'Cyber_sale';	
				$transaction1['gatewayID']           = $get_gateway['gatewayID'];
				$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
				$transaction1['transactionAmount']   = $payamount;
				$transaction1['gateway']             = "cyber_pay";
				$transaction1['merchantID']         = $merchantID;
		
			    $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction1);			
			    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
                redirect('home/invoices/');
				
			}elseif($sch_method == 2){
				
			}else{
				
			}
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}
    }

    public function chargeZoomGateway($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox){

    	$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $Rcon = array('merchID'=>$merchantID);
						
		

		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$sch_method = $postData['sch_method'];
		if($payamount > 0)
		{
			if($sch_method == 1){
				if($cardID != 'new1')
				{
				    
				 	$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$expyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];			
					
				}
				else{
				
				    $cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$expyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				
							
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error:</strong><strong> Chargezoom Authentication Not Set </strong></div>'); 
						
                redirect('home/invoices/');	
			}elseif($sch_method == 2){
				if($cardID != 'new1')
				{					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					$accountType = $card_data['acct_type'];
					$accountHolderType = $card_data['acct_holder_type'];
					

				}else{
					$accountName = $this->security->xss_clean($postData['acc_name']);
					$accountNumber = $this->security->xss_clean($postData['acc_number']);
					$routeNumber = $this->security->xss_clean($postData['route_number']);
					$accountType = $this->security->xss_clean($postData['acct_type']); 
					$accountHolderType = $this->security->xss_clean($postData['acct_holder_type']); 
					
					
				}
			}else{
				
			}
			$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
								
            redirect('home/invoices/');
		}else{
			$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invoice amount is 0</div>'); 
			redirect('home/invoices/');	
		}
	}
	
	/* Invoice billing charge automatically */
    public function create_merchant_billing_charge($payOption=null)
    {	
		include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
		include APPPATH . 'third_party/nmiCustomerVault.class.php';
    	$inv_data = $this->list_model->getMerchantBillingPendinInvoices($payOption);
		
		$fp = fopen(realpath(APPPATH.'/../uploads').'/merchant_billing_charge_log.txt', 'a+');
		fwrite($fp, "\r\n".'========= '.($payOption == null ? 'Card and ACH payment logs on ' : ($payOption == 1 ? 'Card Payments logged on ' : 'ACH Payments logged on ')).date('d/M/Y H:i:s').' ========='."\r\n");
		
    	foreach ($inv_data as $in_data) {
			$log_write = $this->billingChargeByInvoiceId($in_data, 1, $payOption);
			
			if(!empty($log_write)){
				fwrite($fp, $log_write."\r\n");
			}
		}

		fclose($fp);

		echo 'Done';
		exit();
	}

    /* Invoice billing charge automatically using Credit Card */
    public function create_merchant_billing_charge_card()
    {	
    	$this->create_merchant_billing_charge(1);
	}
	
	/* Invoice billing charge automatically using ACH */
    public function create_merchant_billing_charge_ach()
    {	
    	$this->create_merchant_billing_charge(2);
    }

    /* Invoice billing charge automatically */
    public function billingChargeByInvoiceId($in_data,$typeCall = 0, $payOption=null)
    {
    	
        $sandbox  = TRUE;
            
        $invoice = $in_data['invoice'];
        $merchantID = $in_data['merchantID'];
		$con_m = array('merchID'=>$merchantID);
		   
		if($payOption != null)
		{
			$con_m['payOption'] = $payOption;
		}
			
	    $merchant_data = $this->general_model->get_row_data('tbl_merchant_data', $con_m);
		$in_data['merchant_data'] = $merchant_data;
	    $data['get_invoice'] = $in_data;

		$log_write = '';
		if($in_data && $merchant_data)
        {

			$payamount = $in_data['BalanceRemaining'];
        	$cardID = $in_data['cardID'];
        	if($cardID > 0 && $cardID != null){
        		
        		
				$merchantName = $in_data['merchantName'];

				$bill_email='';

	    		$card_data = [];	
	    		
	    		$card_data = $this->card_model->get_merchant_single_card_data($cardID);
	    		
	    		$postData = [];

	    		$postData['sch_method'] = $merchant_data['payOption'];
				$sch_method = $postData['sch_method'];
				if($sch_method == 1){

					$coneGetWay = array('gatewayType'=>5);
				
	    	    	$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);
	    	    	$gatewayType = $get_gateway['gatewayType'];
	    	    	$gateway = 5;
					if($card_data['CardNo'] != null){
						$dataCall = $this->stripeGatewayCharge($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox,$typeCall);

						// Log the merchant pay data
						$log_write = "Invoice (".$in_data['invoiceNumber'].") processed for the amount $payamount via ".($sch_method == 1 ? 'card' : 'ach').' on '.date('c');
					}
				}else{
					$coneGetWay = array('gatewayType'=>1);
				
	    	    	$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);
	    	    	$gatewayType = $get_gateway['gatewayType'];
	    	    	$gateway = 1;

					$dataCall = $this->nmiGatewayCharge($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox,$typeCall); 

					if($sch_method ==  2){
						// Log the merchant pay data
						$log_write = "Invoice (".$in_data['invoiceNumber'].") processed for the amount $payamount via ".($sch_method == 1 ? 'card' : 'ach').' on '.date('c');
					}
				}
				
			} else {
				$this->handleInvoiceFailedPayment($in_data, $payamount);
			}	
		}
		
		return $log_write;
    }


    public function nmiGatewayCharge($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox,$typeCall = 0){


		//start NMI
		$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];
        $conditionMerch    =  array('merchID'=>$merchantID); 
		$merchantData = $this->general_model->get_row_data('tbl_merchant_data',$conditionMerch);
		$id = 0;
		
		if($cardID != 'new1')
		{					
			
			$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

		}else{
			
			$Billing_Addr1	 = null;	
			$Billing_Addr2	 = null;
			$Billing_City	 = null;
			$Billing_State	 = null;
			$Billing_Country = null;
			$Billing_Contact	 = null;
			$Billing_Zipcode	 = null;
			$billing_phone_number = null;
			$billing_email = null;
			$billing_first_name = null;
			$billing_last_name = null;
			$CardType	 = null;
		}

		$nmiuser   = $get_gateway['gatewayUsername'];
		$nmipass   = $get_gateway['gatewayPassword'];
		$sch_method = $postData['sch_method'];
		$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);

		if($payamount > 0)
		{
			$transaction1 = new nmiDirectPost($nmi_data); 
			if($sch_method == 1){
				if($cardID != 'new1')
				{					

					$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];
					
					$CardType	 = $card_data['CardType'];

				}else{

					$cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				
				$transaction1->setCcNumber($cnumber);
		    	$expmonth =  $expmonth;
			
				$exyear   = substr($exyear,2);
				if(strlen($expmonth)==1){
					$expmonth = '0'.$expmonth;
				}
		    	$expry    = $expmonth.$exyear; 
				$transaction1->setCcExp($expry);
				$transaction1->setCvv($cvv);

			}else if($sch_method == 2){
				if($cardID != 'new1')
				{					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					
					if($card_data['acct_type'] == 'saving'){
						$accountType = 'savings';
					}else{
						$accountType = $card_data['acct_type'];
					}
					$accountHolderType = $card_data['acct_holder_type'];
					

				}else{
					$accountName = $this->security->xss_clean($postData['acc_name']);
					$accountNumber = $this->security->xss_clean($postData['acc_number']);
					$routeNumber = $this->security->xss_clean($postData['route_number']);
					if($postData['acct_type'] == 'saving'){
						$accountType = 'savings'; 
					}else{
						$accountType = $this->security->xss_clean($postData['acct_type']); 
					}
					
					$accountHolderType = $this->security->xss_clean($postData['acct_holder_type']); 

				}

				if(isset($in_data['merchant_data'])){
					$accountName = $in_data['merchant_data']['companyName'];
				}

				$transaction1->setAccountName($accountName);
				$transaction1->setAccount($accountNumber);
				$transaction1->setRouting($routeNumber);

				$sec_code = 'WEB';

				$transaction1->setAccountType($accountType);
				$transaction1->setAccountHolderType($accountHolderType);
				$transaction1->setSecCode($sec_code);

				$transaction1->setPayment('check');
			}else{

			}
			$transaction1->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $in_data['invoiceNumber']);

			if($Billing_Addr1 != ''){
				$transaction1->setCountry($Billing_Addr1);
			}else{
				if($merchantData['merchantAddress1'] != ''){
					$transaction1->setAddress1($merchantData['merchantAddress1']);
				}
			}
			
			if($Billing_Country != ''){
				$transaction1->setCountry($Billing_Country);
			}else{
				if($merchantData['merchantCountry'] != ''){
					$transaction1->setCountry($merchantData['merchantCountry']);
				}else{
					$transaction1->setCountry('US');
				}
				
			}	
			
			if($Billing_City != ''){
				$transaction1->setCity($Billing_City);
			}else{
				if($merchantData['merchantCity'] != ''){
					$transaction1->setCity($merchantData['merchantCity']);
				}
			}
			
			if($Billing_Zipcode != ''){
				$transaction1->setZip($Billing_Zipcode);
			}else{
				if($merchantData['merchantZipCode'] != ''){
					$transaction1->setZip($merchantData['merchantZipCode']);
				}
			}
			if($billing_phone_number != ''){
				$transaction1->setPhone($billing_phone_number);
			}else{
				if($merchantData['merchantContact'] != ''){
					$transaction1->setPhone($merchantData['merchantContact']);
				}
			}
			
			if($Billing_State != ''){
				$transaction1->setState($Billing_State);
			}else{
				if($merchantData['merchantState'] != ''){
					$transaction1->setState($merchantData['merchantState']);
				}
			}

			$transaction1->setEmail($merchantData['merchantEmail']);
			$transaction1->setCompany($merchantData['companyName']);
			$transaction1->setFirstName($merchantData['firstName']);
			$transaction1->setLastName($merchantData['lastName']);
			
			$transaction1->setAmount($payamount);
			$transaction1->setTax('tax');
			$transaction1->sale();
			$getwayResponse = $transaction1->execute();
			

			if( $getwayResponse['response_code']=="100"){
				$invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status = '1';
				$pay        = $payamount;
				 $remainbal  = $in_data['BalanceRemaining']-$payamount;
				$data   	 = array('status'=>$status,'invoiceStatus'=>1, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );

				$condition  = array('invoice'=>$in_data['invoice'] );
				 
				$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
			    				
			}else{
				#Alter transaction code as other gateway treats 200 as success
				if($getwayResponse['transactionCode'] == 200) {
					$getwayResponse['transactionCode'] = 401;
				}
				
				if($in_data['invoiceStatus'] == 3){
					$invoice      = $in_data['invoice'];  
					
					$data   	 = array('invoiceStatus'=>1);

					$condition  = array('invoice'=>$in_data['invoice'] );
					 
					$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

					/* Transaction failed case than merchant plan do downgrade only free trial expired case */

					$dataCall = $this->failedInvoiceFreeTrialDowngradePlan($merchantID);
				}

				if($typeCall != 3){
					$this->handleInvoiceFailedPayment($in_data, $payamount);
				}
			}
			
		    $transaction = array();
		    $transaction['transactionID']      = ($getwayResponse['transactionid'] != '')?$getwayResponse['transactionid']:'TXNFAILED-'.time();
			$transaction['transactionStatus']  = $getwayResponse['responsetext'];
			$transaction['transactionCode']    = $getwayResponse['response_code'];
			$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
			$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['merchant_invoiceID']       = $in_data['invoice'];
			$transaction['gatewayID']          = $get_gateway['gatewayID'];
			$transaction['transactionGateway'] = 1;	
			$transaction['transactionAmount']  = $payamount;
			$transaction['gateway']            = "NMI";
			$transaction['merchantID']         = $merchantID;
		  
			$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
		}	
		return $id; 
    }
	public function stripeGatewayCharge($get_gateway,$card_data,$payamount,$in_data,$merchantID,$postData,$cardID,$sandbox,$typeCall =0){

		$transactionid = 'TXNFAILED-'.time();

		$merchantID = $in_data['merchantID'];
		$merchantName = $in_data['merchantName'];
        $payamount = $in_data['BalanceRemaining'];

		$sch_method = $postData['sch_method'];
		$statusCode = $id = 0;
		$gatewayResponseCode =0;
		$responsetext = 'Transaction Failed';
		if($payamount > 0)
		{
			
			$plugin = new ChargezoomStripe();
			$plugin->setApiKey($get_gateway['gatewayPassword']);

			if($sch_method == 1){
				if($cardID != 'new1')
				{					

					$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];
					
					$CardType	 = $card_data['CardType'];

				}else{

					$cnumber = $this->security->xss_clean($postData['card_number']);
					$expmonth = $this->security->xss_clean($postData['expiry']);
					$exyear = $this->security->xss_clean($postData['expiry_year']);
					$cvv = $this->security->xss_clean($postData['cvv']); 
					
				}
				try {

				  	 $res =  \Stripe\Token::create([
						'card' => [
						'number' =>$cnumber,
						'exp_month' => $expmonth,
						'exp_year' =>  $exyear,
						'cvc' => $cvv,
						'address_city'	=>	$card_data['Billing_City'],
					    'address_country'	=> $card_data['Billing_Country'],
					    'address_line1'	=> $card_data['Billing_Addr1'],
					    'address_line1_check'	=> null,
					    'address_line2'	=> $card_data['Billing_Addr2'],
					    'address_state'	=> $card_data['Billing_State'],
					    'address_zip'	=> $card_data['Billing_Zipcode'],
					    'address_zip_check'	=> null,
					    'name'	=> $merchantName,
					   ]
					]); 
               
					$tcharge= json_encode($res);  
					$rest = json_decode($tcharge);


				}catch (Exception $e) {
					$responsetext = $e->getMessage();
					
				  // Something else happened, completely unrelated to Stripe
				}
				
		    	
		    	if($payamount > 0 && $payamount < 0.5){
					$payamount = 0.5;
				}
				$paidamount  =  (int)($payamount*100); 

				
				if(isset($rest->id))
				{
					try {
						$customer = \Stripe\Customer::create(array(
						    'name' => $merchantName,
						    'description' => 'CZ Merchant',
						    'source' => $rest->id,
						    "address" => ["city" => $card_data['Billing_City'], "country" => $card_data['Billing_Country'], "line1" => $card_data['Billing_Addr1'], "line2" => $card_data['Billing_Addr2'], "postal_code" => $card_data['Billing_Zipcode'], "state" => $card_data['Billing_State']]
						));
						$customer= json_encode($customer);
		              	  
						$resultstripecustomer = json_decode($customer);
					}catch (Exception $e) {
						
					  // Something else happened, completely unrelated to Stripe
					}

					try {

						if(isset($resultstripecustomer->id))
						{
							$charge =	\Stripe\Charge::create(array(
						  	  'customer' => $resultstripecustomer->id,
							  "amount" => $paidamount,
							  "currency" => "usd",
							  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
							 
							));	
						}
						else
						{
							$charge =	\Stripe\Charge::create(array(
							  "amount" => $paidamount,
							  "currency" => "usd",
							  "source" => $rest->id, 
							  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
							 
							));	
						}
					  	
	               
						$charge= json_encode($charge);
              	  
						$resultstripe = json_decode($charge);


					}catch (Exception $e) {
						$gatewayResponseCode = 0;
						$responsetext = $e->getMessage();
						
					  // Something else happened, completely unrelated to Stripe
					}

					if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
				 	{
				 		$statusCode = 200;
				 		$transactionid = $resultstripe->id;
				 		$gatewayResponseCode = 200;
						$responsetext = $resultstripe->status;
				 	}else{
				 		if(isset($result->status)){
				 			$gatewayResponseCode = $result->failure_code;
							$responsetext = $result->status;
				 		}
				 		
				 	}
				}
			}else{

			}


			if( $statusCode == 200){
				$invoice      = $in_data['invoice'];  
				$status 	 = 'paid';
				$paid_status = '1';
				$pay        = $payamount;
				$remainbal  = $in_data['BalanceRemaining']-$payamount;
				$data   	 = array('status'=>$status,'invoiceStatus'=>1,'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );

				$condition  = array('invoice'=>$in_data['invoice'] );
				 
				$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
			    				
			}else{
				if($in_data['invoiceStatus'] == 3){
					$invoice      = $in_data['invoice'];  
				
					$paid_status = '1';
					
					$data   	 = array('invoiceStatus'=>1);

					$condition  = array('invoice'=>$in_data['invoice'] );
					 
					$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);

					/* Transaction failed case than merchant plan do downgrade only free trial expired case */

					$dataCall = $this->failedInvoiceFreeTrialDowngradePlan($merchantID);
				}
				
		        $getwayResponse['response_code'] = 0;

				if($typeCall != 3){
					$this->handleInvoiceFailedPayment($in_data, $payamount);
				}
		    } 

		    $transaction = array();
		    $transaction['transactionID']      = $transactionid;
			$transaction['transactionStatus']  = $responsetext;
			$transaction['transactionCode']    = $gatewayResponseCode;
			$transaction['transactionType']    = 'sale';
			$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['merchant_invoiceID']       = $in_data['invoice'];
			$transaction['gatewayID']          = $get_gateway['gatewayID'];
			$transaction['transactionGateway'] = 1;	
			$transaction['transactionAmount']  = $payamount;
			$transaction['gateway']            = "Stripe";
			$transaction['merchantID']         = $merchantID;
		  
			$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
		}	
		
		return $id;
    }
    
    public function freeTrialMerchant(){
    	$m_data = $this->list_model->getMerchantBillingFreeTrailInvoices();
    	foreach ($m_data as $in_data) {
    		$trial_exp_date = $in_data['trial_expiry_date'];
    		$billing_up_to_date = date('Y-m-d');
    		$merchantID =  $in_data['merchantID'];
    		$plan_id = $in_data['plan_id'];
			$m_name =  $in_data['companyName'];
			$cardID =  $in_data['cardID'];
			
    		if($trial_exp_date < $billing_up_to_date){
    			/*Generated actual plan invoice prorated based */

				$inv_data = $this->general_model->get_row_data('tbl_merchant_number', array('numberType' => 'Merchant'));

				$inumber   = $inv_data['liveNumber'] + 1;
	            $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
	            $inv_id   = 'ADM' .mt_rand(100000, 999999);

				/* Plan per day calculation */
                $planAmount = $in_data['subscriptionRetail'];
                $numberOfDaysInMonth = date('t');
                $amountPerDay = round(($planAmount / $numberOfDaysInMonth),2);
                /*Days compare in month by plan*/
                $billing_up_to_date = date("Y-m-t");
                $datePurchase = date("Y-m-d");
                $currentdate = date("d");
                $nextMonth = date('Y-m-d',strtotime('first day of +1 month'));
                
                $datediff = strtotime($billing_up_to_date) - strtotime($datePurchase);
               	$different_days = round($datediff / (60 * 60 * 24)) + 1;

               	if($currentdate >= 25){
                	
					$nextMonthDay = date('t',strtotime($nextMonth)); 

					$nextMonthAmount = $planAmount;

                	$plan_expiry = date('Y-m-d', strtotime('-1 day', strtotime($nextMonth)));
                }else{
                	$nextMonthAmount = 0;

                	$plan_expiry = date('Y-m-d', strtotime('-1 day', strtotime($nextMonth)));
                }

                $totalInvoiceAmount = $different_days * $amountPerDay;
                $rental_amount = $totalInvoiceAmount + $nextMonthAmount;

				$due_date = date('Y-m-d');
				$balance = $rental_amount + $in_data['serviceFee'];
				
				if($balance == '0' || $balance == '0.00' ){
	                $status   = 'paid';
	            }else{
	                $status   = 'pending';
	            }
				
				$total1 = $in_data['serviceFee'];

				$subscriptionItemName = 'Subscription Fee'; 

				$subscriptionAmount = $rental_amount;
				/* ******Subscription saved******/
				$data     = array(
	                'merchantName'           => $m_name,
	                'merchantID'             => $merchantID,
	                'invoice'                => $inv_id,
	                'invoiceNumber'          => $refNumber,   
	                'DueDate'                => $due_date,
	                'BalanceRemaining'       => ($balance),
	                'status'                 => $status,
	                'gatewayID'              => 0,
	                'cardID'                 => $cardID,
	                'merchantSubscriptionID' => 0,
	                'Type'                   => 'Plan',
	                'serviceCharge'          => $total1,
	                'invoiceStatus'          => 3,
	                'createdAt'              => date('Y-m-d H:i:s'),
	                'updatedAt'              => date('Y-m-d H:i:s')
	            );
	            if ($this->general_model->insert_row('tbl_merchant_billing_invoice', $data)) {

	            	/*Update merchant free trail expired */

	            	$updateMerchant = array('freeTrial' => 0, 'free_trial_day' => 0,'plan_expired' => $plan_expiry);
	                $this->general_model->update_row_data('tbl_merchant_data', array('merchID' => $merchantID), $updateMerchant);

	                $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
	                $this->general_model->update_row_data('tbl_merchant_number', array('numberType' => 'Merchant'), $updata);

	                $pl_data['itemName']          = $subscriptionItemName;
	                $pl_data['itemPrice']         = $subscriptionAmount;
	                $pl_data['merchantInvoiceID'] = $inv_id;
	                $pl_data['planID'] = $plan_id;
	                $pl_data['merchantID'] = $merchantID;
	                $pl_data['merchantName'] = $m_name;

	                $pl_data['subscriptionID']    = $inv_id;
	                $pl_data['createdAt']         = date('Y-m-d H:i:s');
	                $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data);

	                $item_data['itemName']          = 'Service Charge';
	                $item_data['itemPrice']         = $total1;
	                $item_data['merchantInvoiceID'] = $inv_id;
	                $item_data['subscriptionID']    = $inv_id;
	                $item_data['planID'] = $plan_id;
	                $item_data['merchantID'] = $merchantID;
	                $item_data['merchantName'] = $m_name ;
	                $item_data['createdAt']         = date('Y-m-d H:i:s');
	                echo 'Success:-'.$merchantID.':For Company:-'.$m_name.'<br>';
	            }
	            /* Billing information not saved than merchant plan do downgrade only free trial expired case */
	            if($cardID == null || $cardID == 0){
	            	$dataCall = $this->failedInvoiceFreeTrialDowngradePlan($merchantID);
	            }
    		}else{
    			echo 'Not Expired:-'.$merchantID.':For Company:-'.$m_name.'<br>';
    		}
    	}
    }
    public function create_merchant_invoice_refund()
	{
		if (!empty($this->input->post(null, true))) {
			$data = [];
			$refAmt = 0;

			$txnInvoiceID = $this->czsecurity->xssCleanPostInput('txnInvoiceID');
			$paydata  = $this->list_model->get_merchant_invoice_paid_transaction($txnInvoiceID);

			$statusCode = 400;
			$TXNID = 'Failed'.time();
			$errorMsg = 'Transaction Failed';
			if(!empty($paydata)){
				
				$refAmt = $paydata['transactionAmount'];
				if($refAmt > 0){
					$amount     = $refAmt;
					$cone = array('gatewayID' => $paydata['gatewayID']);
					$tID = $paydata['transactionID'];
	        	    $gt_result = $this->general_model->get_row_data('tbl_admin_gateway', $cone);
	        	    if($gt_result['gatewayType'] == 5){
	        	    	include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
	        	    	$calamount=   (int)($amount*100);
		    		    $plugin = new ChargezoomStripe();
		                $plugin->setApiKey($gt_result['gatewayPassword']);
		    		   
						$charge = \Stripe\Refund::create(array(
						  "charge" => $tID,
						  "amount"=>$calamount,
						));
						$charge= json_encode($charge);
				  
				   		$result = json_decode($charge);
				   		

				   		if(strtoupper($result->status) == strtoupper('succeeded')){ 
				   			$statusCode = 200;
				   			$TXNID = $result->id;
				   		}else{
				   			$statusCode = 300;
				   		}
	        	    	$gatewayType = 'Stripe';
	        	    	$errorMsg = $result->status;
	        	    }else if($gt_result['gatewayType'] == 1){
	        	    	include APPPATH . 'third_party/nmiDirectPost.class.php';
						include APPPATH . 'third_party/nmiCustomerVault.class.php';
						$nmiuser  = $gt_result['gatewayUsername'];
		    		    $nmipass  = $gt_result['gatewayPassword'];
		    		    $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
						$transaction = new nmiDirectPost($nmi_data);
						$transaction->setPayment('check');
						$transaction->setTransactionId($tID);  
						$transaction->refund($tID,$amount);
	                	$getwayResponse     = $transaction->execute();
	                	
	                	if ($getwayResponse['response_code'] == '100') {
	                		$statusCode = 200;
	                		$TXNID = $getwayResponse['transactionid'];
	                	}else{
	                		$statusCode = $getwayResponse['response_code'];
	                		$TXNID = 'TXNFAILED'.time();
	                	}
	                	
	                	
	                	$gatewayType = 'NMI';

	                	$errorMsg = $getwayResponse['responsetext'];
	        	    }else{
	        	    	$statusCode = 400;
	        	    	$errorMsg = 'Something went wrong.';
	        	    }
	        	    
                	if($statusCode == 200)
                	{
                		$data   	 = array('status'=>'refund','invoiceStatus'=>2 );

						$condition  = array('invoice'=>$txnInvoiceID );
				 
						$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);	
                		$transaction = array();
					    $transaction['transactionID']      = $TXNID;
						$transaction['transactionStatus']  = 'Your transaction was successfully refunded.';
						$transaction['transactionCode']    = $statusCode;
						$transaction['transactionType']    = 'refund';
						$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
						$transaction['transactionModified']= date('Y-m-d H:i:s'); 
						$transaction['merchant_invoiceID']       = $txnInvoiceID;
						$transaction['gatewayID']          = $gt_result['gatewayID'];
						$transaction['transactionGateway'] = 1;	
						$transaction['transactionAmount']  = $amount;
						$transaction['gateway']            = $gatewayType;
						$transaction['merchantID']         = $paydata['merchantID'];
					  
						 $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
						 $this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success:</strong> Payment refunded successfully.</div>');
                	}else{
                		$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> '.$errorMsg.'</div>');
                	}
					
				}else{
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> Refund amount is greater than zero.</div>');
				}
				
			}

			
			
                   
      
		}else{
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> Transaction ID not found.</div>');
		}
		
                   
       	redirect(base_url('home/invoices/'));
		
	}
	public function failedInvoiceFreeTrialDowngradePlan($merchantID){
		$merchantData = $this->list_model->getMerchantDataByID($merchantID);

		$downgradePlanID = $merchantData['downgradePlanID'];

		if($downgradePlanID > 0){
			$plan =  $merchantData['plan_id'];
			$setPlanEnable = 0;
			if($plan != null){
	            $palnIDArr =  explode(',', $plan);
	            foreach ($palnIDArr as $planID) {
	                $planTypeGet = $this->general_model->get_select_data('plans', array('merchant_plan_type'), array('plan_id'=>$planID));
	                if($planTypeGet['merchant_plan_type'] == 'Free'){
	                    $setPlanEnable = 1;
	                }
	            }
	            
	        }
	        /* Set plan as downgrade */
	        if($setPlanEnable){
	        	$planType = $this->general_model->get_select_data('plans', array('merchant_plan_type'), array('plan_id'=>$downgradePlanID));

	        	$data   	 = array('plan_id'=>$downgradePlanID);

				$condition  = array('merchID'=> $merchantData['merchID'] );
				 
				$this->general_model->update_row_data('tbl_merchant_data',$condition, $data);
	        }
		}
	}

	/**
	 * Function to handle events on day basis if a payment fails for an invoice
	 * 
	 * @param array $invoiceData
	 * @param float $invoiceAmount
	 * 
	 * @return void 
	 */
	public function handleInvoiceFailedPayment(array $invoiceData, float $invoiceAmount) : void
	{
		$currentDate = date('Y-m-d');
		$date2 = new DateTime($currentDate);
		$date1 = new DateTime($invoiceData['DueDate']);
		$interval = $date1->diff($date2);

		$dateDifference = $interval->days + 1; #Add one to the difference to count current day as Day 1

		$templateType = [];
		if($dateDifference == 1){
			#Send Email to merchant for failed payment
			$templateType = [5];
		} elseif($dateDifference == 3 || $dateDifference == 5 || $dateDifference == 10) {
			#Send Email to merchant for Past Due invoice on Day 3/5/10

			$templateType = [6];
			if($dateDifference == 10){
				$templateType[] = 9;
				$this->list_model->updateMerchantStatus($invoiceData['merchantID'], 1, 1);
			}

		} elseif($dateDifference == 20){
			#Send Email to merchant for Acoount Downgrade and Account disable
			$templateType = [7,8];
		} elseif($dateDifference == 27){
			#Send Email to merchant for Acoount Downgraded and Account disabled
			$this->failedInvoiceFreeTrialDowngradePlan($invoiceData['merchantID']);

			$templateType = [14];
		} elseif ($dateDifference == 40) {
			#Update Invoice as bad Invoice
			$data   	 = array('invoiceStatus'=> 5 ); #Unpaid new status
			$condition  = array('invoice'=>$invoiceData['invoice'] );
			$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
		}

		$extraData = [
			'invoiceData' => $invoiceData
		];

		if(!empty($templateType))
		{
			foreach ($templateType as $template) {
				$mailCondition = [
					'userTemplate' => 1,
					'templateType' => $template,
				];
				$this->send_email_model->sendEmail(1, 0, $invoiceData['merchantID'], '', $mailCondition, 'MERCHANT', $invoiceAmount, $extraData);
			}
		}

		$templateType = 0;
		if($dateDifference == 5 || $dateDifference == 15) {
			#Send Email to reseller for merchant's Past Due invoice/account on Day 5/15
			$templateType = 12;
		}

		if($templateType > 0)
		{
			$mailCondition = [
				'userTemplate' => 1,
				'templateType' => $templateType,
			];
			$this->send_email_model->sendEmail(1, 0, $invoiceData['merchantID'], '', $mailCondition, 'RESELLER', $invoiceAmount, $extraData);
		}
	}

	/**
	 * Public invoice page
	 * 
	 * @param string $invoiceID
	 * @param string $merchantEncryptedID
	 * 
	 * @return void 
	 */
	public function viewPublicInvoice(string $invoiceID, string $merchantEncryptedID) : void
	{
		$this->session->unset_userdata("public_receipt_data");
		$invoiceID = $this->safe_decode($invoiceID);

		$enypt = $merchantEncryptedID;
		$enyptc= $this->safe_decode($enypt);
		$decodeData = explode("=",$enyptc);
		$merchantID = $decodeData[1];

		$invWhere = [
			'invoice' => $invoiceID,
			'merchantID' => $merchantID,
		];
		$invoiceData = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $invWhere);
		if(!$invoiceData || empty($invoiceData) || !in_array($invoiceData['invoiceStatus'], [1,3,5]) || strtolower($invoiceData['status']) != 'pending')
		{
			redirect('/paid_url');
		}

		$resellerData = $this->list_model->getMerchantDataByID($merchantID);
		if(!$resellerData || empty($resellerData))
		{
			redirect('/paid_url');
		}

		$data['template'] 		= template_variable();
		$data['action']	= base_url('merchantPayment/processPublicInvoice');
		$data['invoiceData']	= $invoiceData;
		$data['resellerData']	= $resellerData;
		$data['invoiceID'] 		= $invoiceID;
		$data['merchantID'] 	= $merchantID;

		$this->load->view('admin/public_merchant_invoice', $data);
	}

	/**
	 * Function to process public invoice
	 * 
	 * @return void 
	 */
	public function processPublicInvoice() : void
	{
		include_once APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
    	include_once APPPATH . 'third_party/nmiDirectPost.class.php';
		include_once APPPATH . 'third_party/nmiCustomerVault.class.php';

        $this->session->unset_userdata("public_receipt_data");
		$invoiceID    = $this->czsecurity->xssCleanPostInput('invid');
		$merchantID    = $this->czsecurity->xssCleanPostInput('mid');

		$invWhere = [
			'invoice' => $invoiceID,
			'merchantID' => $merchantID,
		];
		$invoiceData = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $invWhere);
		if(!$invoiceData || empty($invoiceData) || !in_array($invoiceData['invoiceStatus'], [1,3,5]) || strtolower($invoiceData['status']) != 'pending')
		{
			redirect('/paid_url');
		}

		$payamount = $invoiceData['BalanceRemaining'];

		$merchantData = $this->general_model->get_row_data('tbl_merchant_data', [ 'merchID' => $merchantID]);
		$invoiceData['merchant_data'] = $merchantData;

		$paymentCardDetails = [
			'merchantFriendlyName' => '',
			'Billing_Contact' => null,
			'billing_phone_number' => null,
			'billing_email' => null,
			'CardType' => null,

			'billing_first_name'	=> $this->czsecurity->xssCleanPostInput('first_name'),
			'billing_last_name'	=> $this->czsecurity->xssCleanPostInput('last_name'),
			'Billing_Addr1'	=> $this->czsecurity->xssCleanPostInput('address'),
			'Billing_Addr2'	=> $this->czsecurity->xssCleanPostInput('address2'),
			'Billing_City'	=> $this->czsecurity->xssCleanPostInput('city'),
			'Billing_State'	=> $this->czsecurity->xssCleanPostInput('state'),
			'Billing_Zipcode'	=> $this->czsecurity->xssCleanPostInput('zip'),
			'Billing_Country'	=> $this->czsecurity->xssCleanPostInput('country'),
		];

		$schMethod    = $this->czsecurity->xssCleanPostInput('sch_method');
		
		$conditionGateway = ($schMethod == 1) ? [ 'gatewayType' => 5 ]: [ 'gatewayType' => 1 ];
		$gatewayData = $this->general_model->get_row_data('tbl_admin_gateway', $conditionGateway);

		$processedPaymentID = 0;
		$postData['sch_method'] = $schMethod;
		
		if($schMethod == 1){
			$paymentType = "Credit Card";

			$paymentCardDetails['CardNo'] = $this->czsecurity->xssCleanPostInput('card_number');
			$paymentCardDetails['cardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
			$paymentCardDetails['cardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
			$paymentCardDetails['cvv'] = $this->czsecurity->xssCleanPostInput('cvv');
			$paymentCardDetails['CardID'] = '';
			$paymentCardDetails['CardType'] = '';

			$processedPaymentID = $this->stripeGatewayCharge($gatewayData, $paymentCardDetails, $payamount, $invoiceData, $merchantID, $postData, '', false, 3);
		} else {
			$paymentType = "ECheck";

			$paymentCardDetails['acc_number'] = $this->czsecurity->xssCleanPostInput('acc_number');
			$paymentCardDetails['route_number'] = $this->czsecurity->xssCleanPostInput('route_number');
			$paymentCardDetails['acct_type'] = 'saving';
			$paymentCardDetails['acct_holder_type'] = 'personal';

			$processedPaymentID = $this->nmiGatewayCharge($gatewayData, $paymentCardDetails, $payamount, $invoiceData, $merchantID, $postData, '', false, 3); 
		}

		$receipt_data = array(
            'paymentCardDetails'  => $paymentCardDetails,
            'payamount'  => $payamount,
            'invoiceData'  => $invoiceData,
            'postData'  => $postData,
			'paymentType' => $paymentType,
            'processedPaymentID'  => $processedPaymentID,
        );

        $this->session->set_userdata("public_receipt_data", $receipt_data);
		redirect('ThankYou');
	}
} // end of class
