<?php

/**
 * Billingcharges
 *

 */
class BillingCharge extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('general_model');
        $this->load->model('list_model');
        $this->db1 = $this->load->database('otherdb', true);
    }
    public function createInvoiceByID()
    {
        if($this->input->get('merchantID') != ""){
            $requestMerchantID = $this->input->get('merchantID');
            $this->index($requestMerchantID);
        }else{
            $requestMerchantID = '';
        }
    }

    public function index($requestMerchantID = '')
    {
        if($requestMerchantID == ''){
            $d1 = (int)date('d');
            if( $d1 != 1){
                echo  'Hey its '.date('d/M/Y').', this cron runs on 1st of month only.';
                exit;
            }
        }
        // Run it only at first of month
        


        $in_datas = $this->list_model->get_subscription_data($requestMerchantID);

        if (!empty($in_datas)) {

            foreach ($in_datas as $in_data) {
                $merchantID    = $in_data['merchantID'];
                $plan_id    = $in_data['plan_id'];
                $plan_expired_date = date('Y-m-d',strtotime($in_data['plan_expired']));
                $billing_up_to_date = date('Y-m-d');
                if($in_data['companyName'] == null){
                    $m_name        = $in_data['firstName'] . '' . $in_data['lastName'];
                }else{
                    $m_name        = $in_data['companyName'];
                }
                $inv_id   = 'ADM' .mt_rand(100000, 999999);
                if($requestMerchantID != ""){
                    $plan_expired_date = date('Y-m-d');
                }
                if(strtotime($plan_expired_date) <= strtotime($billing_up_to_date))
                {
                    
                    /* Plan per day calculation */
                    $planAmount = $in_data['subscriptionRetail'];
                    $numberOfDaysInMonth = date('t',strtotime('-1 day'));
                    $amountPerDay = round(($planAmount / $numberOfDaysInMonth),2);
                    $subscriptionItemName = 'Subscription Fee';

                    /* Free trial condition manage */    
                    if($in_data['freeTrial'] == 1){
                        $trial_exp_date = $in_data['trial_expiry_date'];

                        $trial_date = date('Y-m-d',strtotime($trial_exp_date));

                        
                        
                        if(strtotime($trial_date) < strtotime($billing_up_to_date)){
                            
                                $datediff = strtotime($billing_up_to_date) - strtotime($trial_exp_date);
                                $different_days = round($datediff / (60 * 60 * 24));
                                $totalInvoiceAmount = $different_days * $amountPerDay;
                                $rental_amount = $totalInvoiceAmount;

                                $subscriptionItemName = 'Subscription '.$different_days.' Fee';
                            
                        }else{
                            $rental_amount = 0;
                        }
                        
                            
                    }else{
                        $rental_amount = $in_data['subscriptionRetail'];
                    }
                    $subscriptionAmount = $rental_amount;
                    
                    

                    $service_fee = $in_data['serviceFee'];

                    $month_amount = 0;
                    $total1       = 0;
                    /************************Service Charge *******************/

                    $month_amount = $this->list_model->get_month_total_amount($merchantID);
                    $test[]       = $month_amount;
                    //Calculate service fee by percentage
                    if ($in_data['planType'] == '1') {
                        $total1  = $month_amount * ($service_fee / 100);
                        $balance = $rental_amount + $total1;
                    } else if ($in_data['planType'] == '2') {
                        $tr_cnt  = $this->list_model->get_total_transaction($merchantID);
                        $total1  = $service_fee * $tr_cnt;
                        $balance = $rental_amount + $total1;
                    } else if ($in_data['planType'] == '3') {
                        $total1  = 0;
                        $balance = $rental_amount + $total1;
                    }
                    
                    /*Apply condition commission*/
                    $commissionApply = 0;
                    $calculateCom = 0;
                    
                    if($in_data['billingType'] == 2 && $in_data['volumn'] > 0){
                        $commissionApply = 1;
                        $percentCal = $in_data['percent'];
                        $calculateCom = round(($in_data['volumn'] * $percentCal),2);
                    }
                    $balance = $balance + $calculateCom;


                    $inv_data = $this->general_model->get_row_data('tbl_merchant_number', array('numberType' => 'Merchant'));
                    $inumber   = $inv_data['liveNumber'] + 1;
                    $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
                    $due_date = date('Y-m-d');
                    
                    if($balance == '0' || $balance == '0.00' ){
                        $status   = 'paid';
                    }else{
                        $status   = 'pending';
                    }
                    
                    $data     = array(
                        'merchantName'           => $m_name,
                        'merchantID'             => $merchantID,
                        'invoice'                => $inv_id,
                        'invoiceNumber'          => $refNumber,   
                        'DueDate'                => $due_date,
                        'BalanceRemaining'       => ($balance),
                        'status'                 => $status,
                        'gatewayID'              => 0,
                        'cardID'                 => $in_data['cardID'],
                        'merchantSubscriptionID' => 0,
                        'Type'                   => 'Plan',
                        'serviceCharge'          => $total1,
                        'invoiceStatus'          => 1,
                        'createdAt'              => date('Y-m-d H:i:s'),
                        'updatedAt'              => date('Y-m-d H:i:s')
                    );
                    if ($this->general_model->insert_row('tbl_merchant_billing_invoice', $data)) {

                        $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
                        $this->general_model->update_row_data('tbl_merchant_number', array('numberType' => 'Merchant'), $updata);

                        $updataMR = array('cronRunMonth' => strtotime(date('Y-m')));
                        
                        $this->general_model->update_row_data('tbl_merchant_data', array('merchID' => $in_data['merchantID']), $updataMR);

                        $pl_data['itemName']          = $subscriptionItemName;
                        $pl_data['itemPrice']         = $subscriptionAmount;
                        $pl_data['merchantInvoiceID'] = $inv_id;
                        $pl_data['planID'] = $plan_id;
                        $pl_data['merchantID'] = $merchantID;
                        $pl_data['merchantName'] = $m_name;

                        $pl_data['subscriptionID']    = $inv_id;
                        $pl_data['createdAt']         = date('Y-m-d H:i:s');

                        

                        $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data);

                        /*Commision ENtry */
                        $pl_data1 =[];
                        if($commissionApply == 1){
                            $pl_data1['itemName']          = 'Basis Point Fee';
                            $pl_data1['itemPrice']         = $calculateCom;
                            $pl_data1['merchantInvoiceID'] = $inv_id;
                            $pl_data1['planID'] = $plan_id;
                            $pl_data1['merchantID'] = $merchantID;
                            $pl_data1['merchantName'] = $m_name;

                            $pl_data1['subscriptionID']    = $inv_id;
                            $pl_data1['createdAt']         = date('Y-m-d H:i:s');

                            $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data1);
                        }

                       
                        $item_data['itemName']          = 'Service Charge';
                        $item_data['itemPrice']         = $total1;
                        $item_data['merchantInvoiceID'] = $inv_id;
                        $item_data['subscriptionID']    = $inv_id;
                        $item_data['planID'] = $plan_id;
                        $item_data['merchantID'] = $merchantID;
                        $item_data['merchantName'] = $m_name ;
                        $item_data['createdAt']         = date('Y-m-d H:i:s');

                    }
                    echo 'Merchant Name: '.$m_name.'</br>';
                    echo 'Status: Success</br>';
                    echo 'Invoice ID: '.$inv_id.'</br>';
                    echo 'Amount: '.$balance.'</br>';
                    echo 'merchantID: '.$merchantID.'</br></br></br></br>';
                }else{
                    echo 'Merchant Name: '.$m_name.'</br>';
                    echo 'Status: Failed</br>';
                    echo 'Invoice ID: '.$inv_id.'</br>';
                    echo 'merchantID: '.$merchantID.'</br></br></br></br>';
                }
            }
        }else{

        }
    }

    public function create_reseller_billing()
    {

        $in_datas = $this->list_model->get_reseller_subscription();
        if (!empty($in_datas)) {
            foreach ($in_datas as $in_data) {
                $gatewayID = 0;

                if (!empty($in_data['invoice_item'])) {

                    $resellerID    = $in_data['resellerID'];
                    $reseller_data = $this->general_model->get_select_data('tbl_reseller', array('resellerAddress', 'resellerAddress2', 'resellerCity', 'resellerState', 'resellerCountry', 'zipCode', 'billingEmailAddress', 'payOption'), array('resellerID' => $resellerID));
                    $gat_data      = $this->general_model->get_select_data('tbl_admin_gateway', array('gatewayID'), array('set_as_default' => '1'));

                    $status = '0';
                    if (!empty($gat_data)) {
                        $gatewayID = $gat_data['gatewayID'];
                    }

                    $r_name = $in_data['resellerCompanyName'];
                    
                   
                    $balance = $in_data['revenue'];

                    $month_amount = 0;
                    
                    $inv_data = $this->general_model->get_row_data('tbl_reseller_number', array('numberType' => 'Reseller'));
                    $inumber   = $inv_data['liveNumber'] + 1;
                    $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
                    if ($reseller_data['payOption'] == '1') {
                        $due_date = date('Y-m-d', strtotime("20 day"));
                    } else {
                        $due_date = date('Y-m-d');
                    }

                    $inv_id = 'ADM' . mt_rand(7000000, 8000000);

                    $invdata = array(
                        'resellerName'        => $r_name,
                        'resellerID'          => $resellerID,
                        'invoice'             => $inv_id,
                        'DueDate'             => $due_date,
                        'invoiceNumber'       => $refNumber,
                        'BalanceRemaining'    => ($balance),
                        'status'              => $status,
                        'gatewayID'           => $gatewayID,
                        'cardID'              => 0,
                        'resSubscriptionID'   => 0,
                        'Type'                => 'Plan',
                        'Shipping_Addr1'      => $reseller_data['resellerAddress'],
                        'Shipping_Addr2'      => $reseller_data['resellerAddress2'],
                        'Shipping_City'       => $reseller_data['resellerCity'],
                        'Shipping_State'      => $reseller_data['resellerState'],
                        'Shipping_PostalCode' => $reseller_data['zipCode'],
                        'Shipping_Country'    => $reseller_data['resellerCountry'],
                        'Billing_Addr1'       => $reseller_data['resellerAddress'],
                        'Billing_Addr2'       => $reseller_data['resellerAddress2'],
                        'Billing_City'        => $reseller_data['resellerCity'],
                        'Billing_State'       => $reseller_data['resellerState'],
                        'Billing_PostalCode'  => $reseller_data['zipCode'],
                        'Billing_Country'     => $reseller_data['resellerCountry'],
                        'createdAt'           => date('Y-m-d H:i:s'),
                        'updatedAt'           => date('Y-m-d H:i:s'),

                    );
                    $aaa = 0;
                    

                    if ($this->general_model->insert_row('tbl_reseller_billing_invoice', $invdata)) {
                        $aaa      = '1';
                        $new_item = array();
                        $items    = $in_data['invoice_item'];
                        foreach ($items as $item) {
                            $itemarray                    = $item;
                            $itemarray['resellerInvoice'] = $inv_id;
                            $new_item[]                   = $itemarray;
                        }

                        $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
                        $this->general_model->update_row_data('tbl_reseller_number', array('numberType' => 'Reseller'), $updata);

                        $this->general_model->insert_batch_rows('tbl_reseller_invoice_item', $new_item);
                    }

                    $admin_data = $this->general_model->get_select_data('tbl_admin', array('adminCompanyName', 'adminEmail', 'primaryContact'), array('adminID' => '1'));

                    if ($reseller_data['payOption'] == '1' && $aaa == '2') {

                        $inv_date = date('m-d-Y');

                        $message = '<div id="welcome_msg"><p>Dear {{reseller_name}},</p><br/><p>This is a notice that an invoice has been generated on {{invoice_date}}. Your invoice due date is {{due_date}} and Invoice amount is {{amount}} .</p>' .

                            '<p>If you have any questions or concerns, please do not hesitate to call. I look forward to hearing from you soon.</p><br/><br/>' .

                            '<p><span style="font-size:11.0pt">Thanks for choosing us. </span></p>' .

                            '<p>Warm Regards,<br/>' .

                            '{{admin_name}}<br/>{{admin_email}}<br/>{{admin_phone}}{{logo}}' .
                            '<br />&nbsp;</p></div>';

                        $subject = 'Chargezoom Invoice ' . '#' . $refNumber;
                        $toEmail = $reseller_data['billingEmailAddress'];

                        $fromEmail = $config_email = $admin_data['adminEmail'];

                        $message = stripslashes(str_replace('{{invoice_date}}', $inv_date, $message));
                        $message = stripslashes(str_replace('{{reseller_name}}', $r_name, $message));
                        $message = stripslashes(str_replace('{{amount}}', $balance, $message));
                        $message = stripslashes(str_replace('{{due_date}}', $due_date, $message));

                        $message = stripslashes(str_replace('{{admin_name}}', $admin_data['adminCompanyName'], $message));
                        $message = stripslashes(str_replace('{{admin_email}}', $admin_data['adminEmail'], $message));
                        $message = stripslashes(str_replace('{{admin_phone}}', $admin_data['primaryContact'], $message));

                        $logo_url = CZLOGO;

                        $logo_url = "<img src='" . $logo_url . "' />";
                        $message  = stripslashes(str_replace('{{logo}}', $logo_url, $message));

                        $tr_date = date('Y-m-d H:i:s');

                        $replyTo    = $addBCC    = $addCC    = '';
                        $email_data = array(
                            'customerID'   => $resellerID,
                            'merchantID'   => 1,
                            'emailSubject' => $subject,
                            'emailfrom'    => $config_email,
                            'emailto'      => $toEmail,
                            'emailcc'      => $addCC,
                            'emailbcc'     => $addBCC,
                            'emailreplyto' => $replyTo,
                            'emailMessage' => $message,
                            'emailsendAt'  => $tr_date,
                            'mailType'     => '1',
                        );

                        $this->load->library('email');
                        $this->email->clear();
                        $config['charset']  = 'utf-8';
                        $config['wordwrap'] = true;
                        $config['mailtype'] = 'html';
                        $this->email->initialize($config);
                        $this->email->from($fromEmail);

                        $this->email->to($toEmail);

                        $this->email->subject($subject);
                        $this->email->message($message);

                        if ($this->email->send()) {
                            $this->insert_row('tbl_template_data', $email_data);
                        } else {
                            $email_data['mailStatus'] = 0;
                            $this->insert_row('tbl_template_data', $email_data);
                        }

                    }

                }

            }

        }
    }
    public function createMerchantInvoiceByID()
    {
        $this->load->model('admin_panel_model');
        $merchantID    = $this->czsecurity->xssCleanPostInput('merchantID');
        if (isset($_GET['merchantID']) && !empty($_GET['merchantID'] != '')) {
            $merchantID    = $_GET['merchantID'];
            $planID    = $_GET['plan_id'];
            $invoiceDate = $_GET['date'];

            $con1   = array('merchID' => $merchantID);
            $in_data = $this->admin_panel_model->get_row_data('tbl_merchant_data', $con1);

            $plan_expired_date = date('Y-m-d',strtotime('last day of this month'));
            $numberOfDaysInMonth = date('t',strtotime('-1 day'));

            $con   = array('plan_id' => $planID);
            $plans = $this->admin_panel_model->get_row_data('plans', $con);
            /* Plan per day calculation */
            if(isset($plans['subscriptionRetail'])){
                $planAmount = $plans['subscriptionRetail'];
            }else{
                $planAmount = 0;
            }
            $amountPerDay = round(($planAmount / $numberOfDaysInMonth),2);
            $subscriptionItemName = 'Subscription Fee';
                   
            if($in_data['companyName'] == null){
                $m_name        = $in_data['firstName'] . '' . $in_data['lastName'];
            }else{
                $m_name        = $in_data['companyName'];
            }
            $service_fee = $plans['serviceFee'];
            $month_amount = 0;
            $total1       = 0;
            $datediff = strtotime($plan_expired_date) - strtotime($invoiceDate);
            $different_days = round($datediff / (60 * 60 * 24));
            $different_days = $different_days + 1;
            $totalInvoiceAmount = $different_days * $amountPerDay;
            $subscriptionAmount = $balance = $totalInvoiceAmount;

            $subscriptionItemName = 'Subscription '.$different_days.' days Fee';
            /************************Service Charge *******************/
            $inv_data = $this->general_model->get_row_data('tbl_merchant_number', array('numberType' => 'Merchant'));
            $inumber   = $inv_data['liveNumber'] + 1;
            $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
            $due_date = $plan_expired_date;
            $inv_id   = 'ADM' .mt_rand(100000, 999999);
            if($balance == '0' || $balance == '0.00' ){
                $status   = 'paid';
            }else{
                $status   = 'pending';
            }
            
            $data     = array(
                'merchantName'           => $m_name,
                'merchantID'             => $merchantID,
                'invoice'                => $inv_id,
                'invoiceNumber'          => $refNumber,   
                'DueDate'                => $due_date,
                'BalanceRemaining'       => ($balance),
                'status'                 => $status,
                'gatewayID'              => 0,
                'cardID'                 => $in_data['cardID'],
                'merchantSubscriptionID' => 0,
                'Type'                   => 'Plan',
                'serviceCharge'          => 0,
                'invoiceStatus'          => 1,
                'createdAt'              => date('Y-m-d H:i:s',strtotime($invoiceDate)),
                'updatedAt'              => date('Y-m-d H:i:s',strtotime($invoiceDate))
            );
            if ($this->general_model->insert_row('tbl_merchant_billing_invoice', $data)) {

                $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_number', array('numberType' => 'Merchant'), $updata);

                $pl_data['itemName']          = $subscriptionItemName;
                $pl_data['itemPrice']         = $subscriptionAmount;
                $pl_data['merchantInvoiceID'] = $inv_id;
                $pl_data['planID'] = $planID;
                $pl_data['merchantID'] = $merchantID;
                $pl_data['merchantName'] = $m_name;

                $pl_data['subscriptionID']    = $inv_id;
                $pl_data['createdAt']         = date('Y-m-d H:i:s',strtotime($invoiceDate));

                $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data);
                echo 'Successfully created';
                die();
            }
                
                echo 'Something went wrong';die();
            
        }
    }
}
