<?php
ob_start();

/**
 * Class Thankyou
 *
 * Payments method Thankyou | Paid URL.
 *
 * @author Mohit Gupta <mohit.gupta@chargezoom.com>
 */
class Thankyou extends CI_Controller  
{
    
	function __construct()
	{
		parent::__construct();
		
		$this->load->model('general_model');
		$this->load->model('list_model');
		$this->load->model('send_email_model');
	}    

	/**
	 * Public thankyou page
	 * 
	 * @return void 
	 */
	public function index() : void
	{
		$receiptData = $this->session->userdata('public_receipt_data');
		$processedPaymentID = $receiptData['processedPaymentID'];
		if($processedPaymentID > 0)
		{
			$transactionData = $this->general_model->get_row_data('tbl_merchant_tansaction', [ 'id' =>  $processedPaymentID]);
			if(in_array($transactionData['transactionCode'], [ 100, 200 ]))
			{
				$transactionData['isSuccess'] = true;
			}
			else
			{
				$transactionData['isSuccess'] = false;
			}
		}
		else 
		{
			$transactionData = [
				'isSuccess' => false,
				'transactionStatus' => 'Transaction Declined',
				'transactionAmount' => '0',
			];
		}
		$data = $receiptData;
		$data['template'] 		= template_variable();
		$data['transactionData'] = $transactionData;
		$data['ip'] = getClientIpAddr();
    	$this->load->view('template/template_start', $data);
		$this->load->view('admin/thankyou', $data);
	}

    /**
	 * Public paid url
	 * 
	 * @return void 
	 */
    public function paid() : void
	{
        $this->load->view('admin/paid_url'); 
	}
}