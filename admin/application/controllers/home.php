<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
use \Chargezoom\Core\Http\SessionRequest;

class Home extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library('form_validation');
        $this->load->helper('url');
        $this->load->helper('general');
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('Gateway');
        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->load->model('admin_panel_model');
        if ($this->session->userdata('admin_logged_in') == "") {
            redirect('login', 'refresh');
        }

    }

    public function index()
    {
        redirect(base_url('Admin_panel/index'));
    }

    public function invoices()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');
        $rID                 = $data['login_info']['adminID'];

        $user_id = $rID;
        $filter = 'Pending'; 
        if($this->czsecurity->xssCleanPostInput('status_filter') != ''){
            $filter = $this->czsecurity->xssCleanPostInput('status_filter');
        }

        $invoices = $this->admin_panel_model->get_table_data('tbl_merchant_billing_invoice', '');

        $data['gateway_datas'] = array();
        $data['gateway_datas'] = $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');

        $condition          = array('adminID' => $user_id);
        $data['admin_data'] = $this->admin_panel_model->get_row_data('tbl_admin', $condition);

        $data['invoices'] = $invoices;
        $data['filter'] = $filter;
        
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/page_invoice', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function get_gateway_data()
    {

        $gatewayID = $this->czsecurity->xssCleanPostInput('gatewayID');
        $condition = array('gatewayID' => $gatewayID);

        $res = $this->general_model->get_row_data('tbl_admin_gateway', $condition);

        if (!empty($res)) {

            $res['status'] = 'true';
            echo json_encode($res);
        }

        die;

    }

    public function reseller_revenue()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();

        $data['r_results'] = $this->db->query("Select rv.*, rs.resellerCompanyName as resellerName from tbl_reseller_revenue rv inner join tbl_reseller rs on rs.resellerID=rv.resellerID ")->result_array();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/reseller_revenue', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function pay_invoice()
    {
        include APPPATH . 'third_party/nmiDirectPost.class.php';
        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        if (!empty($this->input->post(null, true))) {
            $revID        = $this->czsecurity->xssCleanPostInput('invID');
            $data_results = $this->admin_panel_model->get_revenue_data($revID);

            $resellerID = $data_results['resellerID'];
            $gt_result  = $this->general_model->get_row_data('tbl_reseller_gateway', array('resellerID' => $resellerID));

            if (!empty($gt_result)) {

                $type = $gt_result['gatewayType'];

                if ($type == '1') {
                    $nmiuser  = $gt_result['gatewayUsername'];
                    $nmipass  = $gt_result['gatewayPassword'];
                    $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

                    $transaction = new nmiDirectPost($nmi_data);

                    if ($data_results['accountNumber'] != "") {

                        $transaction->setAccountName($data_results['accountName']);
                        $transaction->setAccount($data_results['accountNumber']);
                        $transaction->setRouting($data_results['routNumber']);

                        $transaction->setAccountType($data_results['accountType']);
                        $transaction->setSecCode($data_results['secCodeEntryMethod']);

                        $transaction->setPayment('check');

                    }
                    $transaction->setCompany($data_results['resellerCompanyName']);
                    $transaction->setFirstName($data_results['resellerfirstName']);
                    $transaction->setLastName($data_results['lastName']);
                    $transaction->setAddress1($data_results['resellerAddress']);
                    $transaction->setCountry($data_results['resellerCountry']);
                    $transaction->setCity($data_results['resellerCity']);
                    $transaction->setState($data_results['resellerState']);
                    $transaction->setZip($data_results['zipCode']);
                    $transaction->setPhone($data_results['primaryContact']);

                    $transaction->setEmail($data_results['resellerEmail']);
                    $amount = $data_results['totalRevenue'];
                    $transaction->setAmount($amount);
                    $transaction->sale();
                    $result = $transaction->execute();

                    if ($result['response_code'] == '100') {

                        $this->general_model->update_row_data('tbl_reseller_revenue', array('revID' => $revID), array('payStatus' => 'Paid'));

                        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Success</strong></div>');
                    } else {

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $result['responsetext'] . '</div>');
                    }

                    $transactiondata                      = array();
                    $transactiondata['transactionID']     = $result['transactionid'];
                    $transactiondata['transactionStatus'] = $result['responsetext'];
                    $transactiondata['transactionDate']   = date('Y-m-d H:i:s');
                    $transactiondata['transactionCode']   = $result['response_code'];

                    $transactiondata['transactionType']    = $result['type'];
                    $transactiondata['gatewayID']          = $gt_result['gatewayID'];
                    $transactiondata['transactionGateway'] = $gt_result['gatewayType'];
                    $transactiondata['transactionAmount'] = $amount;
                    $transactiondata['userID']            = $data_results['resellerID'];
                    $transactiondata['userType']          = 1;
                    $transactiondata['gateway']   = "NMI ECheck";
                    $transactiondata['invoiceID'] = $revID;

                    $id = $this->general_model->insert_row('reseller_transaction', $transactiondata);

                }

                if ($type == '2') {
                    $apiloginID     = $gt_result['gatewayUsername'];
                    $transactionKey = $gt_result['gatewayPassword'];

                    $transaction = new AuthorizeNetAIM($apiloginID, $transactionKey);

                    if ($data_results['accountNumber'] != "") {

                        $acc_name = $data_results['accountName'];
                        $acc_no   = $data_results['accountNumber'];
                        $rout_no  = $data_results['routNumber'];

                        $acc_no = rand(100000000, 9999999999);

                        $type = $data_results['accountType'];
                        $sec_code = $data_results['secCodeEntryMethod'];

                    }

                    $transaction->setECheck($rout_no, $acc_no, $type, $bank_name = 'Wells Fargo Bank NA', $acc_name, $sec_code);

                    $transaction->__set('company', $data_results['resellerCompanyName']);
                    $transaction->__set('first_name', $data_results['resellerfirstName']);
                    $transaction->__set('last_name', $data_results['lastName']);
                    $transaction->__set('address', $data_results['resellerAddress']);
                    $transaction->__set('country', $data_results['resellerCountry']);
                    $transaction->__set('city', $data_results['resellerCity']);
                    $transaction->__set('state', $data_results['resellerState']);
                    $transaction->__set('zip', $data_results['zipCode']);

                    $transaction->__set('ship_to_address', $data_results['resellerAddress']);
                    $transaction->__set('ship_to_country', $data_results['resellerCountry']);
                    $transaction->__set('ship_to_city', $data_results['resellerCity']);
                    $transaction->__set('ship_to_state', $data_results['resellerState']);
                    $transaction->__set('ship_to_zip', $data_results['zipCode']);

                    $transaction->__set('phone', $data_results['primaryContact']);

                    $transaction->__set('email', $data_results['resellerEmail']);
                    $amount = $data_results['totalRevenue'];

                    $result = $transaction->authorizeAndCapture($amount);
                    if ($result->response_code == '1') {

                        /* This block is created for saving Card info in encrypted form  */

                        $this->general_model->update_row_data('tbl_reseller_revenue', array('revID' => $revID), array('payStatus' => 'Paid'));
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Success</strong></div>');
                    } else {

                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $result->response_reason_text . '</div>');
                    }
                    $transactiondata                       = array();
                    $transactiondata['transactionID']      = $result->transaction_id;
                    $transactiondata['transactionStatus']  = $result->response_reason_text;
                    $transactiondata['transactionDate']    = date('Y-m-d H:i:s');
                    $transactiondata['transactionCode']    = $result->response_code;
                    $transactiondata['gatewayID']          = $gt_result['gatewayID'];
                    $transactiondata['transactionGateway'] = $gt_result['gatewayType'];

                    $transactiondata['transactionType'] = $result->transaction_type;
                    $transactiondata['transactionAmount'] = $amount;
                    $transactiondata['gateway']   = "AUTH Echeck";
                    $transactiondata['invoiceID'] = $revID;
                    $transactiondata['userID']    = $resellerID;
                    $transactiondata['userType']  = 1;

                    $id = $this->general_model->insert_row('reseller_transaction', $transactiondata);

                }

            }

        }
        redirect(base_url('home/reseller_revenue'));

    }

    public function add_payment(){
        $input_data = $this->input->post(null, true);
        if (!empty($input_data)) {
            $invoiceData = $this->admin_panel_model->get_row_data('tbl_merchant_billing_invoice', ['invoice' => $input_data['invID']]);

            if(!empty($invoiceData)) {
                $transactiondata                      = array();

                $transactiondata['transactionID']       = $input_data['check_number'];
				$transactiondata['transactionStatus']    = 'Offline Payment';
				$transactiondata['transactionDate']     = date('Y-m-d H:i:s');
				$transactiondata['transactionCode']     = '100';
				$transactiondata['transactionType']    = "Offline Payment";
                $transactiondata['merchantID']            = $invoiceData['merchantID'];
                $transactiondata['transactionModified']     = date('Y-m-d H:i:s');
                
                $transactiondata['transactionAmount'] = $input_data['inv_amount'];
                $transactiondata['gateway']   = "Offline Payment";
                $transactiondata['transactionGateway']   = 0;
                $transactiondata['merchant_invoiceID'] = $input_data['invID'];

                
                $id = $this->general_model->insert_row('tbl_merchant_tansaction', $transactiondata);

                $balance = $invoiceData['BalanceRemaining'] - $input_data['inv_amount'];
                
                $isPaid = 'paid';
                if($balance > 0) 
                    $isPaid = 'pending';

                    $updatedData = [
                        'AppliedAmount' => ( $invoiceData['AppliedAmount'] + $input_data['inv_amount']), 
                        'BalanceRemaining' => $balance,
                        'invoiceStatus' => 1,
                        'status' => $isPaid,
                    ];
                
                $this->general_model->update_row_data('tbl_merchant_billing_invoice', ['invoice' => $input_data['invID']], $updatedData);

                $this->session->set_flashdata('success', '<div class="alert alert-success">Payment recored successfully</div>');

            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Invalid invoice</div>');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Please fill all details</div>');
        }
        redirect(base_url('home/invoices'));
    }

    public function change_password()
    {

        if (!empty($this->input->post(null, true))) {
            if ($this->czsecurity->xssCleanPostInput('current_password') === $this->czsecurity->xssCleanPostInput('new_password')) {
                $this->session->set_flashdata('message', 'Error: New password cannot match old password.');

                redirect(base_url('Admin_panel/index'));
            }

            $old_pass = $this->czsecurity->xssCleanPostInput('current_password');

            $newPassBcrypt = password_hash(
                $this->czsecurity->xssCleanPostInput('new_password'),
                PASSWORD_BCRYPT
            );

            // Check Password Strength
			$strength_check = $this->czsecurity->checkPasswordStrength($this->czsecurity->xssCleanPostInput('new_password'));

			if($strength_check['status'] != 'OK'){
				
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>'.$strength_check['message'].'</div>');
				
				redirect(base_url('Admin_panel/index'));
            }
            
            $adminID    = $this->czsecurity->xssCleanPostInput('adminID');
            $admin_data = $this->general_model->get_row_data('tbl_admin', array('adminID' => $adminID));
            $validPass  = false;

            if ($admin_data['adminPasswordNew'] !== null) {
                if (password_verify($old_pass, $admin_data['adminPasswordNew'])) {
                    $validPass = true;
                }
            } else {
                if (md5($old_pass) === $admin_data['adminPassword']) {
                    $validPass = true;
                }
            }

            if ($validPass) {
                $this->general_model->update_row_data(
                    'tbl_admin',
                    array('adminID' => $adminID),
                    array('adminPassword' => null, 'adminPasswordNew' => $newPassBcrypt)
                );

                /* Destroy all login session */
                $sessionRequest     = new SessionRequest($this->db);
                $updateSession = $sessionRequest->destroySessionUser(SessionRequest::USER_TYPE_ADMIN,$adminID);
                /*End destroy session*/

                $this->session->set_flashdata('success-msg', 'Password has been changed successfully.');
            } else {
                $this->session->set_flashdata('error-msg', 'Incorrect current password.');
            }
        } else {
            $this->session->set_flashdata('error-msg', 'Invalid request.');
        }

        redirect(base_url('Admin_panel/index'));
    }

    public function chk_new_password()
    {
        if (!empty($this->input->post(null, true))) {
            $adminID    = $this->session->userdata('admin_logged_in')['adminID'];
            $old_pass   = $this->czsecurity->xssCleanPostInput('current_password');
            $admin_data = $this->general_model->get_row_data('tbl_admin', array('adminID' => $adminID));

            if ($admin_data['adminPasswordNew'] !== null) {
                if (password_verify($old_pass, $admin_data['adminPasswordNew'])) {
                    $data['status'] = 'success';
                } else {
                    $data['status'] = 'error';
                }
            } else {
                if (md5($old_pass) === $admin_data['adminPassword']) {
                    $data['status'] = 'success';
                } else {
                    $data['status'] = 'error';
                }
            }

            echo json_encode($data);
            die;
        }

        echo "Invalid Request" . ' <a href="' . base_url('Admin_panel/index') . '">Go Back</a>';
        die;
    }

    public function security_log()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');

        if ($data['login_info'] = 'admin_logged_in') {
            $result = $this->admin_panel_model->get_table_data('tbl_security_log', array('UserType' => 'Admin'));
        }
        $data['result'] = $result;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/security_log', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }
    public function security_log_reseller()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');

        if ($data['login_info'] = 'admin_logged_in') {

            $result = $this->general_model->get_track_data();

        }

        $data['result'] = $result;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/security_log_reseller', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function security_log_merchant()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');

        if ($data['login_info'] = 'admin_logged_in') {
            $result = $this->admin_panel_model->get_table_data('tbl_security_log', array('UserType' => 'Merchant'));
        }
        $data['result'] = $result;
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/security_log_merchant', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function invoice_details()
    {

        $invoiceID = $this->uri->segment('3');

        if ($invoiceID != "") {
            $data['primary_nav'] = primary_nav();
            $data['template']    = template_variable();
            $condition2          = array('invoice' => $invoiceID);

            $data['invoice'] = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $condition2);

            $data['card_data'] = $this->card_model->get_merch_card_data($data['invoice']['cardID']);

            $merchantID      = $data['invoice']['merchantID'];
            $data['merchant_data'] = $this->general_model->get_select_data('tbl_merchant_data', array('merchantContact', 'merchantEmail','payOption'), array('merchID' => $merchantID));

            $data['items']          = $this->general_model->get_table_data('tbl_merchant_invoice_item', array('merchantInvoiceID' => $invoiceID));
            $data['plans_overview'] = $this->general_model->get_merchant_invoice_overview($invoiceID);

		    $data['paylink']  = createPublicInvoiceLink($invoiceID, $merchantID);

            $this->load->view('template/template_start', $data);
            $this->load->view('template/page_head', $data);
            $this->load->view('admin_pages/merchant_invoice_details', $data);
            $this->load->view('template/page_footer', $data);
            $this->load->view('template/template_end', $data);

        } else {
            redirect('home/invoices');

        }

    }

    public function edit_custom_invoice()
    {

        if (!empty($this->czsecurity->xssCleanPostInput('planName')) && !empty($this->czsecurity->xssCleanPostInput('planAmount'))) {

            $mnames    = $this->czsecurity->xssCleanPostInput('mName');
            $plans     = $this->czsecurity->xssCleanPostInput('planName');
            $amnts     = $this->czsecurity->xssCleanPostInput('planAmount');
            $invoice   = $this->czsecurity->xssCleanPostInput('invNo');
            $direction = $this->czsecurity->xssCleanPostInput('index');



            $new_items = array();
            $plamount  = 0;
            $inv_data  = $this->general_model->get_select_data('tbl_merchant_billing_invoice', array('BalanceRemaining','merchantID'), array('invoice' => $invoice));

            $merchant_data  = $this->general_model->get_select_data('tbl_merchant_data', array('merchID','firstName','lastName','plan_id'), array('merchID' => $inv_data['merchantID']));

            $planID = $merchant_data['plan_id'];
            $merchantID = $inv_data['merchantID']; 
            $merchantName = $merchant_data['firstName'].' '.$merchant_data['lastName'];
            
            foreach ($plans as $k => $plan) {
                if (!empty($amnts[$k])) {
                    $item['itemName']        = $plan;
                    $item['itemPrice']       = $amnts[$k];
                    $item['merchantName']    = $mnames[$k];
                    $item['merchantInvoiceID'] = $invoice;
                    $item['planID'] = $planID;
                    $item['merchantID'] = $merchantID;
                    $item['merchantName'] = $merchantName;
                    $item['createdAt']       = date('Y-m-d H:i:s');
                    $plamount += $amnts[$k];
                    $new_items[] = $item;
                }
            }
            if (!empty($new_items)) {
                $balance = $inv_data['BalanceRemaining'] + $plamount;

                $update = date('Y-m-d H:i:s');
                $this->general_model->insert_batch_rows('tbl_merchant_invoice_item', $new_items);
                $this->general_model->update_row_data('tbl_merchant_billing_invoice', array('invoice' => $invoice), array('BalanceRemaining' => $balance, 'updatedAt' => $update));
                $this->session->set_flashdata('message', '<strong>Invoice Successfully Updated</strong>');
            }
            if (strtoupper($direction) == 'OTHER') {
                redirect(base_url('home/invoices'));
            } else {
                redirect(base_url('home/invoice_details/' . $invoice));
            }

        }
        redirect(base_url('home/invoices'));
    }

    /******************************************** END ****************************************/

    public function invoice_details_print()
    {

        $invoiceID = $this->uri->segment(3);
        if ($invoiceID != "") {
            $data['template'] = template_variable();
            $condition2       = array('invoice' => $invoiceID);

            $invoice_data   = $this->general_model->get_row_data('tbl_merchant_billing_invoice', $condition2);
            $merchantID     = $invoice_data['merchantID'];
            $merchantDataGet = $this->general_model->get_select_data('tbl_merchant_data', array('plan_id','resellerID','firstName', 'lastName', 'merchantContact', 'merchantEmail', 'companyName','cardID'), array('merchID' => $merchantID));
            $rID = $merchantDataGet['resellerID'];
            $planObj =  $this->general_model->get_reseller_friendly_plan_name( $merchantDataGet['plan_id'] ,$rID);
            

            $card_data = $this->card_model->get_merch_card_data($merchantDataGet['cardID']);

            $logo = CZLOGO;

            $company_data = $this->general_model->get_select_data('tbl_admin', array('adminCompanyName', 'adminEmail', 'billingfirstName', 'billinglastName', 'adminCountry', 'adminCity', 'adminState',
                'primaryContact', 'adminAddress', 'adminAddress2', 'zipCode', 'adminNotes'), array('adminID' => '1'));
            

            $tt    = "\n" . $company_data['billingfirstName'] . " " . $company_data['billinglastName'] . "<br/>" . $company_data['adminCompanyName'] . "<br/>" . ($company_data['adminAddress']) . "<br/> " . ($company_data['adminAddress2']) . "<br/>" . ($company_data['adminCity']) . ", " . ($company_data['adminState']) . " " . ($company_data['zipCode']) . '<br/>' . ($company_data['adminCountry']);
            $email = $merchantDataGet['merchantEmail'];

            $invoice_items = $this->general_model->get_table_data('tbl_merchant_invoice_item', array('merchantInvoiceID' => $invoiceID));

            $no          = $invoice_data['invoice'];
            $pdfFilePath = "$no.pdf";

            ini_set('memory_limit', '320M');
            $this->load->library("TPdf");

            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            define(PDF_HEADER_TITLE, 'Merchant Invoice');
            define(PDF_HEADER_STRING, '');
            $pdf->setPrintHeader(false);
            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Chargezoom 1.0');
            $pdf->SetTitle($data['template']['title']);
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetFont('dejavusans', '', 10, '', true);
            // set cell padding
            $pdf->setCellPaddings(1, 1, 1, 1);

        // set cell margins
            $pdf->setCellMargins(1, 1, 1, 1);
            // Add a page

            $pdf->AddPage();

            $y        = 20;
            $logo_div = '<div style="text-align:left; float:left ">
		<img src="' . $logo . '"  border="0" />
		</div>';

            $pdf->SetFillColor(255, 255, 255);

            $pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);

            $y = 50;
            // set color for background
            $pdf->SetFillColor(255, 255, 255);

            $pdf->SetTextColor(51, 51, 51);

            $pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>" . $tt, 0, 0, 1, true, 'J', true);
            $pdf->SetTextColor(51, 51, 51);

            $pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice ID: </b>' . $invoice_data['invoiceNumber'] . "<br/><br/>" . '<b>Due Date: </b>' . date("m/d/Y", strtotime($invoice_data['DueDate'])), 0, 1, 1, true, 'J', true);

            $bill = '';
            
            if(isset($card_data->merchantCardID)){
                if($card_data->billing_first_name != ''){
                    $bill .= $card_data->billing_first_name;
                }
                if($card_data->billing_last_name != ''){
                    $bill .= ' '.$card_data->billing_last_name;
                }
                $bill .= '<br>';
                if($card_data->billing_phone_number != ''){
                    $bill .= '<i class="fa fa-phone"></i> '.$card_data->billing_phone_number.'<br>';
                }
                if($card_data->billing_email != ''){
                    $bill .= '<i class="fa fa-envelope-o"></i> '.$card_data->billing_email.'<br>';
                }
                if ($card_data->Billing_Addr1 != '') {
                    $bill .= $card_data->Billing_Addr1 . '<br>';
                }
                $bill .= ($card_data->Billing_City) ? $card_data->Billing_City . ', ' : '';
                $bill .= ($card_data->Billing_State) ? ', ' . $card_data->Billing_State . ' ' : '' . ' ';
                $bill .= ($card_data->Billing_Zipcode) ? ', ' . $card_data->Billing_Zipcode: '';
                $bill .= '<br>';
                $bill .= ($card_data->Billing_Country) ? $card_data->Billing_Country : '';
                $bill .= '<br/><br/>';
            }
            
            

            $lll_ship = '';

            
            $y = $pdf->getY();

            // write the first column
            $pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>" . $bill, 0, 0, 1, true, 'J', true);
            $pdf->setCellMargins(5, 5, 5, 5);
            $pdf->SetTextColor(51, 51, 51);
           
            $pdf->writeHTMLCell(80, 0, '', '', '<b>Plan</b>: ' . $planObj['friendlyname'] . "<br/>", 0, 1, 1, true, 'J', true);
            $pdf->setCellPaddings(1, 1, 1, 1);

        // set cell margins
            $pdf->setCellMargins(0, 1, 0, 0);
            
            $html = "\n\n\n" . '<h3></h3><table style="border: 1px solid #333333;" cellpadding="4"  >
                        <tr style="border: 1px solid #333333;background-color:#3f3f3f;color:#FFFFFF;">
                            <th style="border: 1px solid black;"colspan="2" align="center"><b>Plan</b></th>
                            <th style="border: 1px solid black;" align="center"><b>Qty</b></th>
                            <th style="border: 1px solid black;" align="center"><b>Unit Rate</b></th>
                            <th style="border: 1px solid black;" align="center"><b>Amount</b></th>

            </tr>';
            $html1     = '';
            $total_val = 0;
            $totaltax  = 0;
            $total     = 0;
            $tax       = 0;
            $payamount = $invoice_data['AppliedAmount'];
            $totalGet = $invoice_data['BalanceRemaining'] +  $invoice_data['AppliedAmount'];
            
            $Balance = $totalGet -  $invoice_data['AppliedAmount'];
            foreach ($invoice_items as $key => $item) {

                $total += $item['itemPrice'];

                $html .= '<tr style="border: 1px solid black;">
                        <td style="border: 1px solid black;" colspan="2">' . $item['itemName'] . '</td>
                        
                        <td  style="border: 1px solid black;"  align="center" >1</td>
                        <td  style="border: 1px solid black;" align="right">'.number_format($item['itemPrice'],2).'</td>

                        <td style="border: 1px solid black;" align="right">' . number_format($item['itemPrice'], 2) . '</td>
                    </tr>';
            }
            $html.= '<tr><td style="border: 1px solid black;" colspan="4" align="right">Tax 0%</td><td style="border: 1px solid black;" align="right">'.number_format(0,2).'</td></tr>';
            $html.= '<tr><td colspan="4" align="right">Subtotal</td><td style="border: 1px solid black;" align="right">'.number_format($total,2).'</td></tr>';  
            $html .= '<tr >
                    <td style="border: 1px solid black;" colspan="4" align="right">Total</td>
                    <td style="border: 1px solid black;" align="right">$ ' . number_format($total, 2) . '</td>
                </tr>';

            $html .= '<tr><td style="border: 1px solid black;" colspan="4" align="right" >Payment</td><td style="border: 1px solid black;"  align="right">$ ' . number_format(($payamount), 2) . '</td></tr>';
            $html.= '<tr><td   colspan="4" align="right">Balance</td><td align="right">'.number_format(($Balance),2).'</td></tr> ';

            $email1 = ($company_data['adminEmail']) ? $company_data['adminEmail'] : '#';

            $html .= '</table><br><br>';
            $html .= '<strong>Notes:</strong>
		<p> ' . $company_data['adminNotes'] . '</p>
		<br>
		';

            $pdf->writeHTML($html, true, false, true, false, '');
            // Close and output PDF document
            // This method has several options, check the source code documentation for more information.
            $pdf->Output($pdfFilePath, 'D');

        } else {
            redirect('home/invoices');

        }

    }

    public function add_edit_notes()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');
        $invoices            = $this->admin_panel_model->get_table_data('tbl_merchant_billing_invoice', '');

        $data['gateway_datas'] = array();
        $data['gateway_datas'] = $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');
        $rID                   = $data['login_info']['adminID'];

        $user_id = $rID;

        $con           = array('adminID' => $user_id);
        $data['admin'] = $this->admin_panel_model->get_row_data('tbl_admin', $con);

        $data['invoices'] = $invoices;
        if (!empty($this->input->post(null, true))) {
            $input_data['adminNotes'] = $this->czsecurity->xssCleanPostInput('adminNotes');

            if ($con != "") {

                $chk_condition = array('adminID' => $user_id);

                $this->admin_panel_model->update_row_data('tbl_admin', $chk_condition, $input_data);
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Added</strong></div>');
            }
        }
        redirect('home/invoices');

    }

    public function void_invoice()
    {
        if (!empty($this->czsecurity->xssCleanPostInput('pinvoiceID'))) {
            $invoiceID = $this->czsecurity->xssCleanPostInput('pinvoiceID');

            $m_inv = $this->general_model->getMerchantInvoice($invoiceID);
           
            if (!empty($m_inv)) {
                $stdd = $this->general_model->update_row_data('tbl_merchant_billing_invoice', array('merchant_invoiceID' => $m_inv['merchant_invoiceID']), array('invoiceStatus' => 0, 'updatedAt' => date('Y-m-d H:i:s')));
                $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Voided</strong></div>');
            }
            redirect(base_url('home/invoices'));
        }
        redirect(base_url('home/invoices'));
    }

    public function admin_user()
    {

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');
        $user_id             = $data['login_info']['adminID'];

        $users   = $this->general_model->get_table_data('tbl_admin', array('userAdminID' => $user_id));
        $usernew = array();
        foreach ($users as $key => $user) {
            $auth             = $this->general_model->get_admin_auth_data($user['adminID']);
            $user['authName'] = (!empty($auth)) ? $auth['authName'] : '';
            $usernew[$key]    = $user;
        }

        $data['user_data'] = $usernew;

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/admin_user', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_user()
    {
        $data['login_info'] = $this->session->userdata('admin_logged_in');
        $user_id            = $data['login_info']['adminID'];

        if (!empty($this->input->post(null, true))) {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
            $this->form_validation->set_rules('userFname', 'userFname', 'required');
            $this->form_validation->set_rules('userLname', 'userLname', 'required');
            $this->form_validation->set_rules('userCompany', 'Company', 'required');

            if ($this->form_validation->run() == true) {

                $input_data['adminFirstName']   = $this->czsecurity->xssCleanPostInput('userFname');
                $input_data['adminLastName']    = $this->czsecurity->xssCleanPostInput('userLname');
                $input_data['adminEmail']       = $this->czsecurity->xssCleanPostInput('userEmail');
                $input_data['adminCompanyName'] = $this->czsecurity->xssCleanPostInput('userCompany');

                if ($this->czsecurity->xssCleanPostInput('userID') == "") {
                    $input_data['adminPasswordNew'] = password_hash(
                        $this->czsecurity->xssCleanPostInput('userPassword'),
                        PASSWORD_BCRYPT
                    );
                }
                $input_data['userAdminID'] = $user_id;

                $roles = $this->czsecurity->xssCleanPostInput('role');

                if ($this->czsecurity->xssCleanPostInput('userID') != "") {

                    $id            = $this->czsecurity->xssCleanPostInput('userID');
                    $chk_condition = array('adminID' => $id, 'userAdminID' => $user_id);

                    $updt = $this->general_model->update_row_data('tbl_admin', $chk_condition, $input_data);
                    if ($updt) {
                        $this->general_model->delete_row_data('tbl_admin_role', array('adminUserID' => $id));
                        foreach ($roles as $role) {
                            $rol_data['adminUserID'] = $id;
                            $rol_data['authID']      = $role;
                            $rol[]                   = $rol_data;
                        }

                        $uss = $this->general_model->insert_batch_rows('tbl_admin_role', $rol);
                        $this->session->set_flashdata('message', '<strong> Successfully Updated</strong>');
                    } else {
                        $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
                    }
                } else {
                    $insert = $this->general_model->insert_row('tbl_admin', $input_data);
                    if ($insert) {
                        $rol = array();
                        foreach ($roles as $role) {
                            $rol_data['adminUserID'] = $insert;
                            $rol_data['authID']      = $role;
                            $rol[]                   = $rol_data;
                        }

                        $uss = $this->general_model->insert_batch_rows('tbl_admin_role', $rol);
                        $this->session->set_flashdata('message', '<strong>Successfully Created New User</strong>');
                    } else {
                        $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
                    }
                }

                redirect(base_url('home/admin_user'));

            }
        }

        if ($this->uri->segment('3')) {
            $userID           = $this->uri->segment('3');
            $con              = array('adminID' => $userID);
            $data['user']     = $this->general_model->get_select_data('tbl_admin', array('adminID', 'adminEmail', 'adminFirstName', 'userAdminID', 'adminLastName', 'adminCompanyName'), $con);
            $auth_data        = $this->general_model->get_admin_auth_data($userID);
            $data['authvals'] = explode(',', $auth_data['authValue']);
        }

        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $con1                = array('adminUserID' => $user_id);

        $data['auths'] = $this->general_model->get_table_data('tbl_admin_auth', '');
        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/adduser', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);
    }

    public function delete_user()
    {

        $data['login_info'] = $this->session->userdata('admin_logged_in');
        $user_id            = $data['login_info']['adminID'];

        $userID    = $this->czsecurity->xssCleanPostInput('adminID11');
        $condition = array('userAdminID' => $user_id, 'adminID' => $userID);

        $del = $this->general_model->delete_row_data('tbl_admin', $condition);
        if ($del) {
            $this->session->set_flashdata('message', '<strong> Successfully Deleted</strong>');
        } else {
            $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
        }

        redirect(base_url('home/admin_user'));

    }

    public function admin_gateway()
    {
        $data['primary_nav'] = primary_nav();
        $data['template']    = template_variable();
        $data['login_info']  = $this->session->userdata('admin_logged_in');

        $data['all_gateway'] = $this->admin_panel_model->get_table_data('tbl_master_gateway', '');
        $data['gateways']    = [];
        $data['admin_gateways']    = $this->admin_panel_model->get_gateway_data();

        $this->load->view('template/template_start', $data);
        $this->load->view('template/page_head', $data);
        $this->load->view('admin_pages/merchant_gateway', $data);
        $this->load->view('template/page_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function create_gateway()
    {
        $this->load->library('Gateway');
        $signature = '';
        if (!empty($this->input->post(null, true))) {
            $gt_status          = 0;
            $gt_obj             = new Gateway();
            $data['login_info'] = $this->session->userdata('admin_logged_in');
            $user_id            = $data['login_info']['adminID'];
            $condition          = array('merchantID' => $user_id);

            $gatetype = $this->czsecurity->xssCleanPostInput('gateway_opt');

            if ($gatetype == '1') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
            } else if ($gatetype == '2') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_auth_gateway_auth($auth_data);

            } else if ($gatetype == '3') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);
                $gt_status   = $gt_obj->chk_paytrace_gateway_auth($auth_data);

                $signature  = PAYTRACE_INTEGRATOR_ID;

				$gt_status = $gt_obj->chk_paytrace_gateway_auth($auth_data);
				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

            } else if ($gatetype == '4') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword, 'signature' => $signature);

                $gt_status = $gt_obj->chk_paypal_gateway_auth($auth_data);

            } else if ($gatetype == '5') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_stripe_gateway_auth($auth_data);

            } else if ($gatetype == '6') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_usaePay_gateway_auth($auth_data);

            } else if ($gatetype == '7') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                $auth_data   = array('user' => $nmiuser, 'password' => $nmipassword);

                $gt_status = $gt_obj->chk_heartland_gateway_auth($auth_data);

            } else if ($gatetype == '8') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey');
                $auth_data   = array('merchantID' => $nmiuser, 'apiKey' => $nmipassword, 'secretKey' => $signature);

                $gt_status = $gt_obj->chk_cybersource_gateway_auth($auth_data);

            } else if ($gatetype == '9') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword');
                $nmi_data    = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipassword);
                $gt_status   = $gt_obj->chk_nmi_gateway_auth($nmi_data);
            } 

            $gatedata = $this->czsecurity->xssCleanPostInput('g_list');

            $frname = $this->czsecurity->xssCleanPostInput('frname');
            $gmID   = $this->czsecurity->xssCleanPostInput('gatewayAdminID');

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayAdminID'                       => $gmID,
                'gatewaySignature'                     => $signature,
                'gatewayType'                          => $gatetype,
                'adminID'                              => $user_id,
                'gatewayFriendlyName'                  => $frname,

            );

            if ($gid = $this->admin_panel_model->insert_row('tbl_admin_gateway', $insert_data)) {
                $val1 = array(
                    'adminID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_admin_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_admin_gateway', $val, $update_data);
                
                $this->session->set_flashdata('message', '<strong> Successfully Created</strong>');
            } else {

                $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong...!');

            }

            redirect(base_url('home/admin_gateway'));

        }

    }

    public function update_gateway()
    {

        if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {

            $signature = "";

            $id            = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID' => $id);
            $gatetype = $this->czsecurity->xssCleanPostInput('gateway');

            $cr_status = 1; $ach_status = 0;

            if ($gatetype == 'NMI') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('nmiPassword1');

                if ($this->czsecurity->xssCleanPostInput('nmi_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;

            } else if ($gatetype == 'Authorize.net') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transactionKey1');

                if ($this->czsecurity->xssCleanPostInput('auth_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('auth_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
            } else if ($gatetype == 'Paytrace') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
            } else if ($gatetype == 'Paypal') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature   = $this->czsecurity->xssCleanPostInput('paypalSignature1');

            } else if ($gatetype == 'Stripe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('stripePassword1');

            } else if ($gatetype == 'USAePay') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('transtionPin1');

            } else if ($gatetype == 'Heartland') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('heartsecretkey1');

            } else if ($gatetype == 'Cybersource') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature   = $this->czsecurity->xssCleanPostInput('secretKey1');

            } else if ($gatetype == 'Chargezoom') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('czPassword1');
                if ($this->czsecurity->xssCleanPostInput('cz_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('cz_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
            } 

            $frname   = $this->czsecurity->xssCleanPostInput('fname');
            $gmID     = $this->czsecurity->xssCleanPostInput('aid');

            $insert_data = array('gatewayUsername' => $nmiuser,
                'gatewayPassword'                      => $nmipassword,
                'gatewayAdminID'                       => $gmID,
                'gatewayFriendlyName'                  => $frname,
                'gatewaySignature'                     => $signature,
                'creditCard'                           => $cr_status,
                'echeckStatus'                         => $ach_status,
            );

            if ($this->admin_panel_model->update_row_data('tbl_admin_gateway', $chk_condition, $insert_data)) {
                $this->session->set_flashdata('message', '<strong>Successfully Updated</strong>');
            } else {
                $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
            }

            redirect(base_url('home/admin_gateway'));
        }

    }

    public function get_gatewayedit_id()
    {

        $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
        $val = array(
            'gatewayID' => $id,
        );

        $data = $this->admin_panel_model->get_row_data('tbl_admin_gateway', $val);
        if ($data['gatewayType'] == '1') {
            $data['gateway'] = "NMI";
        }
        if ($data['gatewayType'] == '2') {
            $data['gateway'] = "Authorize.net";
        }
        if ($data['gatewayType'] == '3') {
            $data['gateway'] = "Paytrace";
        }
        if ($data['gatewayType'] == '4') {
            $data['gateway'] = "Paypal";
        }
        if ($data['gatewayType'] == '5') {
            $data['gateway'] = "Stripe";
        }
        if ($data['gatewayType'] == '6') {
            $data['gateway'] = "USAePay";
        }
        if ($data['gatewayType'] == '7') {
            $data['gateway'] = "Heartland";
        }
        if ($data['gatewayType'] == '8') {
            $data['gateway'] = "Cybersource";
        }
        echo json_encode($data);
    }

    public function update_gatewayes()
    {
        if (!empty($this->input->post(null, true))) {
            if ($this->session->userdata('logged_in')) {
                $data['login_info'] = $this->session->userdata('logged_in');

                $user_id = $data['login_info']['merchID'];
            }

            $sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));

            $old_gateway = $this->czsecurity->xssCleanPostInput('gateway_old');
            $new_gateway = $this->czsecurity->xssCleanPostInput('gateway_new');

            $condition = array('gatewayID' => $old_gateway);
            $num_rows  = $this->general_model->get_num_rows('tbl_merchant_data', $condition);

            if ($num_rows > 0) {
                $update_data = array('gatewayID' => $new_gateway);

                $sss = $this->db->query("update tbl_merchant_data set gatewayID='" . $new_gateway . "' where merchID IN ($sub_ID) ");

                if ($sss) {

                    $this->session->set_flashdata('message', '<strong> Success:</strong> Subscription gateway have changed. ');
                } else {
                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> In update process.</div>');

                }
            } else {
                $this->session->set_flashdata('message', ' <strong>Error:</strong> No data selected.');
            }
        }
        redirect(base_url('home/merchant_list'));

    }

    public function delete_gateway()
    {

        if ($this->czsecurity->xssCleanPostInput('gatewaydelID') != "") {

            $id = $this->czsecurity->xssCleanPostInput('gatewaydelID');

            $rowCount = $this->admin_panel_model->get_num_rows('tbl_reseller_billing_invoice', array('gatewayID' => $id));

            if ($rowCount > 0) {

                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> This gateway can no be deleted.</div>');
                redirect(base_url('home/admin_gateway'));

            } else {
                $condition = array('gatewayID' => $id, 'set_as_default' => '0');
                $del       = $this->admin_panel_model->delete_row_data('tbl_admin_gateway', $condition);

                if ($del) {
                    $this->session->set_flashdata('message', '<trong>Successfully Deleted</strong>');
                    redirect(base_url('home/admin_gateway'));
                } else {
                    $this->session->set_flashdata('message', '<strong>Error</strong> Something Is Wrong.');
                    redirect(base_url('home/admin_gateway'));
                }

            }

            $this->session->set_flashdata('message', '<strong>Error</strong> Something Is Wrong.');
            redirect(base_url('home/admin_gateway'));
        }

    }

    public function set_gateway_default()
    {

        $data['login_info'] = $this->session->userdata('admin_logged_in');

        if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {
            $id  = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );

            $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_admin_gateway', array(), $update_data1);

            $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

            $this->general_model->update_row_data('tbl_admin_gateway', $val, $update_data);

            $this->session->set_flashdata('message', '<strong>Success</strong>');

        } else {

            $this->session->set_flashdata('error', '<strong>Error</strong> Invalid Request');
        }
        redirect(base_url('home/admin_gateway'));
    }
    public function serverProcessingInvoice()
    {
        $this->load->model('list_model');
        if(isset($_GET['draw'])){
            $data = [];
            
            $data3 = [];
            
            $totalRecords = $this->list_model->get_merchant_invoice_data($_GET,'total');
           
            $invoices1 = $this->list_model->get_merchant_invoice_data($_GET);
            if(count($invoices1) > 0){
                $inc = 0;
                foreach ($invoices1 as  $value) {
                    # code...
                    $data2 = [];
                    $merchantlink = base_url().'Admin_panel/create_merchant/'.$value['merchantID'];
                    $data2[] = '<a href="'.$merchantlink.'" ><strong>'.$value['merchantName'].'</strong></a>';
                    $link="javascript:void(0);";
                     if($value['invoiceStatus']==1 || $value['invoiceStatus'] == 3)
                     {
                        $link = base_url().'home/invoice_details/'.$value['invoice'];
                     }else{
                         
                     }
                    if(($value['invoiceNumber'] == null || empty($value['invoiceNumber']))){
                        $linktext = '<strong><a href="'.$link.'" ><strong>'.$value['invoice'].'</strong></a></strong>';
                       $data2[] = $linktext;
                    }else{
                        $linktext = '<strong><a href="'.$link.'" ><strong>'.$value['invoiceNumber'].'</strong></a></strong>';
                        $data2[] = $linktext;
                    }


                    $data2[] = '<span class="hidden">'.$value['createdAt'].'</span>'.date('m/d/Y', strtotime($value['createdAt']));
                    $total = $value['BalanceRemaining'] + $value['AppliedAmount'];
                    
                    $data2[] = '<div class="hidden-xs text-right cust_view"> <a href="#pay_data_process" onclick="get_payment_data(\'' . $value['invoice'] . '\');" data-backdrop="static" data-keyboard="false" data-toggle="modal">' . '$' . (number_format($total,2)) . ' </a> </div>';
                    
                    if($value['status'] == 'pending' && $value['invoiceStatus']== 1){ 
                        $data2[] = ucfirst('Pending');
                        
                     }else if($value['status'] == 'pending' && $value['invoiceStatus']== 3){ 
                        $data2[] = ucfirst('Pending');
                        
                     }else if($value['status'] == 'pending' && $value['invoiceStatus']== 5){ 
                        $data2[] = ucfirst('Unpaid');
                        
                     }else if($value['status']=='pending' && $value['invoiceStatus']==0){
                        $data2[] = ucfirst('Void');
                            
                    } else if($value['invoiceStatus'] == 2 && $value['status']=='refund') {
                        $data2[] = ucfirst('Refunded');
                            
                    }else{ 
                        $data2[] = ucfirst('Paid');
                    }

                    $text = '<div class="btn-group dropbtn">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                            <ul class="dropdown-menu text-left">';
                            
                    if($value['status']=='pending' && $value['invoiceStatus']==1)
                    { 
                        $valueGet1 = " '".$value['invoice']."', '".$value['merchantID']."','".$value['BalanceRemaining']."'";

                        $valueGet2 = " '".$value['invoice']."', '".$value['merchantID']."','".$value['BalanceRemaining']."'";

                        $text .= '<li><a href="#set_subs" class=""  onclick="set_process_id('.$valueGet1.');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add Payment</a></li>
                                    <li><a href="#reseller_invoice_process1" class=""  onclick="set_reseller_invoice_process_id('.$valueGet2.');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Process</a></li>';
                             
                        if($value['AppliedAmount']==0 || $value['AppliedAmount']=='0.00'  )
                        {  
                            $valueGet3 = " '".$value['invoice']."', '".$value['merchantID']."'";
                            $text .= '<li><a href="#ress_void"   onclick="set_void_invoice_process_id('. $valueGet3.');" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Void</a></li>';
                        } 
                    }
                    else if($value['status']=='pending' && $value['invoiceStatus'] == 3)
                    { 
                        $valueGet1 = " '".$value['invoice']."', '".$value['merchantID']."','".$value['BalanceRemaining']."'";

                        $valueGet2 = " '".$value['invoice']."', '".$value['merchantID']."','".$value['BalanceRemaining']."'";

                        $text .= '<li><a href="#set_subs" class=""  onclick="set_process_id('.$valueGet1.');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add Payment</a></li>
                                    <li><a href="#reseller_invoice_process1" class=""  onclick="set_reseller_invoice_process_id('.$valueGet2.');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Process</a></li>';
                             
                        if($value['AppliedAmount']==0 || $value['AppliedAmount']=='0.00'  )
                        {  
                            $valueGet3 = " '".$value['invoice']."', '".$value['merchantID']."'";
                            $text .= '<li><a href="#ress_void"   onclick="set_void_invoice_process_id('. $valueGet3.');" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Void</a></li>';
                        } 
                    } else if($value['invoiceStatus'] == 0) {
                        $text .= '<li>    <a href="#" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Voided</a></li>';
                            
                    } else if($value['invoiceStatus'] == 2 && $value['status']=='refund') {
                        $text .= '<li>    <a href="#" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refunded</a></li>';
                            
                    } else { 
                        
                        $valueGet4 = "'".$value['invoice']."'";
                        if($value['AppliedAmount'] > 0){
                            $text .= '<li><a href="#payment_refund_popup_modal" onclick="set_refund_popup('.$valueGet4.');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a></li>';
                        }else{
                            $text .= '<li><a href="#" data-backdrop="static" data-keyboard="false" data-toggle="modal">Paid</a></li>';
                        }
                        
                       
                        
                              
                    }
                    $text .= '</ul>
                        </div>';
                    $data2[] = $text;    
                    $data3[$inc] = $data2;
                    $inc = $inc + 1;
                }
            }

            $data['draw'] = $_GET['draw'];
            $data['recordsTotal'] = $totalRecords;
            $data['recordsFiltered'] = $totalRecords;
            $data['data'] = $data3;


            echo json_encode($data);
          

        }
    }
    public function delete_merchant_gateway()
    {
    
         $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
         $merchantdata=$this->general_model->get_row_data('tbl_merchant_gateway',array('gatewayID'=>$gatewayID));
         $merchantID=$merchantdata['merchantID'];
    
         $condition1 =  array('gatewayID'=>$gatewayID,'set_as_default'=>'0'); 
         
      
         $del1      = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition1);

        if($del1)
        {
            $this->session->set_flashdata('success', 'Successfully Deleted');
                 
                   
        }
        else 
        {   
            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> This gateway can no be deleted.</div>');
                   
            redirect(base_url('Admin_panel/create_merchant/'.$merchantID));     
        }  
        redirect(base_url('Admin_panel/create_merchant/'.$merchantID));

    }
    //----------- TO update the gateway  --------------//
    
    public function update_merchant_gateway() 
    {
        if($this->czsecurity->xssCleanPostInput('gatewayEditID')!="" )
        {
            $ach_status       =  0;
            $signature='';
            $cr_status=1;
            $isSurcharge = $surchargePercentage = 0;
             
            $merchantID =$this->czsecurity->xssCleanPostInput('editmerchID');
            $id = $this->czsecurity->xssCleanPostInput('gatewayEditID');
            $chk_condition = array('gatewayID'=>$id);
            $extra1 = '';
            $gmID       =   $this->czsecurity->xssCleanPostInput('mid');             
            if($this->czsecurity->xssCleanPostInput('gateway')=='NMI'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword1');
                if($this->czsecurity->xssCleanPostInput('nmi_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('nmi_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
            if($this->czsecurity->xssCleanPostInput('gateway')== FluidGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
                $nmipassword     = '';
                if($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
            if($this->czsecurity->xssCleanPostInput('gateway')== BASYSGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
                $nmipassword     = '';
                if($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
            if($this->czsecurity->xssCleanPostInput('gateway')== TSYSGatewayName){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword1');
                $gmID            = $this->czsecurity->xssCleanPostInput('TSYSMerchantID1');
                
                if($this->czsecurity->xssCleanPostInput('TSYS_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;
                    
                if($this->czsecurity->xssCleanPostInput('TSYS_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }    
            if(strtolower($this->czsecurity->xssCleanPostInput('gateway'))=='authorize.net'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey1');
                    
                if($this->czsecurity->xssCleanPostInput('auth_cr_status1')){
                    $cr_status       =  1;
                }else{
                    $cr_status       =  0;
                }

                if($this->czsecurity->xssCleanPostInput('auth_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                
            }
                
            if($this->czsecurity->xssCleanPostInput('gateway')=='Paytrace'){
                $this->load->config('paytrace');
                $nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword1');
                $signature  = PAYTRACE_INTEGRATOR_ID; 

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            } 
            if($this->czsecurity->xssCleanPostInput('gateway')=='Paypal'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword1');
                $signature       = $this->czsecurity->xssCleanPostInput('paypalSignature1');
            
            }
            
            if($this->czsecurity->xssCleanPostInput('gateway')=='Stripe'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword1');
            
            }
            if($this->czsecurity->xssCleanPostInput('gateway')=='USAePay'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin1');
            
            }
            if($this->czsecurity->xssCleanPostInput('gateway')=='Heartland'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('heartpublickey1');
                if($this->czsecurity->xssCleanPostInput('heart_cr_status1')){
                    $cr_status       =  1;
                }
                else{
                    $cr_status       =  0;
                }    
                if($this->czsecurity->xssCleanPostInput('heart_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            }
            
            if($this->czsecurity->xssCleanPostInput('gateway')=='Cybersource')
            {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
                $signature       = $this->czsecurity->xssCleanPostInput('secretKey1');
            
            }

            if($this->czsecurity->xssCleanPostInput('gateway')=='Chargezoom'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('czUser1');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword1');
                if($this->czsecurity->xssCleanPostInput('cz_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;
                
                if($this->czsecurity->xssCleanPostInput('cz_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }

            if ($this->czsecurity->xssCleanPostInput('gateway')== iTransactGatewayName) {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
					$ach_status       = 1;
				else
                    $ach_status       =  0;
                    
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
            }
            if ($this->input->post('gateway')== EPXGatewayName) {
                $nmiuser         = $this->input->post('EPXCustNBR1',true);
                $nmipassword     = $this->input->post('EPXMerchNBR1',true);
                $signature     = $this->input->post('EPXDBANBR1',true);
                $extra1   = $this->input->post('EPXterminal1',true);
                if ($this->input->post('EPX_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } 
            
            if($this->input->post('gateway')== PayArcGatewayName){
                $nmiuser         = $this->input->post('payarcUser1', true);
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            }

            if($this->input->post('gateway')== MaverickGatewayName){
                $nmiuser         = $this->input->post('maverickAccessToken1', true);
                $nmipassword     = $this->input->post('maverickTerminalId1', true);
                if ($this->input->post('maverick_cr_status1'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('maverick_ach_status1'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }

            if ($this->czsecurity->xssCleanPostInput('gateway')== 'CardPointe') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
                $signature     = $this->czsecurity->xssCleanPostInput('cardpointeSiteName1');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }
			} 
           
            
            
            if($this->input->post('gateway')== PayArcGatewayName){
                $nmiuser         = $this->input->post('payarcUser1');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            }
            
            $gatetype   =   $this->czsecurity->xssCleanPostInput('gateway') ;
            $frname     =   $this->czsecurity->xssCleanPostInput('fname');
            
                
            $insert_data    = array(
                                    'gatewayUsername'=>$nmiuser,
                                    'gatewayPassword'=>$nmipassword, 
                                    'gatewayMerchantID'=>$gmID,
                                    'gatewayFriendlyName'=>$frname,
                                    'gatewaySignature' =>$signature,
                                    'extra_field_1' =>$extra1,
                                    'creditCard'       =>$cr_status,
                                    'echeckStatus'      =>$ach_status,
                                    'isSurcharge' => $isSurcharge,
                                    'surchargePercentage' => $surchargePercentage,
                                );

               
            if($this->general_model->update_row_data('tbl_merchant_gateway',$chk_condition, $insert_data) )
            {

                $this->session->set_flashdata('success', 'Successfully Updated');
                        
         
            }else{
                 
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
            
            }
        
            redirect(base_url('Admin_panel/create_merchant/'.$merchantID));
            
        }        
        else
        {
          redirect('Admin_panel/index','refresh');  
        }
     
    } 
    //---------------- To add gateway  ---------------//
    
    public function create_merchant_gateway(){
        $signature = '';
        $cr_status=1;
        $ach_status=0;
        $isSurcharge = $surchargePercentage = 0;
        $extra1 = '';
        $gmID        = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
        if(!empty($this->czsecurity->xssCleanPostInput('merchID')))
        {
            $user_id =$this->czsecurity->xssCleanPostInput('merchID');
            $condition              = array('merchantID'=>$user_id);
                
            if($this->czsecurity->xssCleanPostInput('gateway_opt')=='1')
            {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
                if($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;
                
                if($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='2'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
                
                if($this->czsecurity->xssCleanPostInput('auth_cr_status')){
                    $cr_status       =  1;
                }else{
                    $cr_status       =  0;
                }
                
                if($this->czsecurity->xssCleanPostInput('auth_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='3'){
                $this->load->config('paytrace');
                $nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
                $signature  = PAYTRACE_INTEGRATOR_ID;

                if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='4'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
                $signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
            
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='5'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
            
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='6'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='7'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
                if($this->czsecurity->xssCleanPostInput('heart_cr_status')){
                    $cr_status       =  1;
                }
                else{
                    $cr_status       =  0;
                } 
                if($this->czsecurity->xssCleanPostInput('heart_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='8'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
                $signature       = $this->czsecurity->xssCleanPostInput('secretKey');
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='9'){
                $nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
                    
                if($this->czsecurity->xssCleanPostInput('cz_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('cz_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            } else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '10') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
                if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
                if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
                    $isSurcharge = 1;
                }
                else{
                    $isSurcharge = 0;
                }
                $surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
            } else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='11') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
                $nmipassword     = '';
                if($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
             else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='12') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
                $nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
                $gmID            = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
                if($this->czsecurity->xssCleanPostInput('TSYS_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;
                    
                if($this->czsecurity->xssCleanPostInput('TSYS_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
             else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='13') {
                $nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
                $nmipassword     = '';
                if($this->czsecurity->xssCleanPostInput('basys_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if($this->czsecurity->xssCleanPostInput('basys_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            }
             else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='14') {
				$nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                $signature     = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } 
             else if ($this->input->post('gateway_opt') == '15') {
                $nmiuser         = $this->input->post('payarcUser');
                $nmipassword     = '';
                $cr_status       =  1;
                $ach_status       =  0;
            } else if($this->input->post('gateway_opt')== '17'){
                $this->load->config('paytrace');
                $nmiuser         = $this->input->post('maverickAccessToken', true);
                $nmipassword     = $this->input->post('maverickTerminalId', true);

                if ($this->input->post('maverick_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('maverick_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            
            }else if($this->input->post('gateway_opt')=='16') {
                $nmiuser         = $this->input->post('EPXCustNBR',true);
                $nmipassword     = $this->input->post('EPXMerchNBR',true);
                $signature     = $this->input->post('EPXDBANBR',true);
                $extra1   = $this->input->post('EPXterminal',true);
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
            } 
            $gatedata       =  $this->czsecurity->xssCleanPostInput('g_list');
            
            $gatetype       = $this->czsecurity->xssCleanPostInput('gateway_opt') ;
            $frname         =$this->czsecurity->xssCleanPostInput('frname');
            
           
            $val1 = array(
                'merchantID' => $user_id,
            );
            $update_data1 = array('set_as_default'=>'0', 'updatedAt'=>date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway',$val1, $update_data1);

            
            $insert_data    = array(
                'gatewayUsername'=>$nmiuser,
                'gatewayPassword'=>$nmipassword, 
                'gatewayMerchantID'=>$gmID,
                'gatewaySignature' =>$signature,
                'extra_field_1' =>$extra1,
                'gatewayType'=>$gatetype,
                'merchantID'=>$user_id, 
                'gatewayFriendlyName'=>$frname ,
                'creditCard'       =>$cr_status,
                'echeckStatus'      =>$ach_status,
                'set_as_default'    => '1',
                'isSurcharge' => $isSurcharge,
                'surchargePercentage' => $surchargePercentage,
            );
    
            if($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)){

                $val1 = array(
                    'merchantID' => $user_id,
                );
                
                
                $val = array(
                    'gatewayID' => $gid,
                );
                
                $this->session->set_flashdata('success', 'Successfully Inserted');
            }else{
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
            }
            redirect(base_url('Admin_panel/create_merchant/'.$user_id));
        } else {
            redirect('Admin_panel/index','refresh');  
        }           
    
    }
    public function set_merchant_gateway_default()
    {
        if(!empty($this->czsecurity->xssCleanPostInput('gatewayid')))
        {
            
         
            $merchID   = $this->czsecurity->xssCleanPostInput('defmerchID');
         
            $id = $this->czsecurity->xssCleanPostInput('gatewayid');
            $val = array(
                'gatewayID' => $id,
            );
            $val1 = array(
                'merchantID' => $merchID,
            );
            $update_data1 = array('set_as_default'=>'0', 'updatedAt'=>date('Y:m:d H:i:s'));
            $this->general_model->update_row_data('tbl_merchant_gateway',$val1, $update_data1);
            $update_data = array('set_as_default'=>'1', 'updatedAt'=>date('Y:m:d H:i:s'));
             
            $this->general_model->update_row_data('tbl_merchant_gateway',$val, $update_data);
             
             
              
        }else{
            
            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');   
        }     
        redirect(base_url('Admin_panel/create_merchant/'.$merchID));
    }
}
