<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
ob_start();

class Admin_panel extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		session_start();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('admin_panel_model');
		$this->load->model('general_model');
		$this->load->model('list_model');
		if (!$this->session->userdata('admin_logged_in')) {
			redirect('login', 'refresh');
		}
	}

	public function index()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

        $where = ['adminID' => $data['login_info']['adminID'], 'adminPasswordNew' => null];

        $newPasswordNotFound = $this->general_model->get_row_data('tbl_admin', $where);

        if ($newPasswordNotFound) {
            $dateDiff                = time() - strtotime(getenv('PASSWORD_EXP_DATE'));
            $data['passwordExpDays'] = abs(round($dateDiff / (60 * 60 * 24)));
        }

		$ss_amount = 0;
		$vt_amount = 0;
		$as_amount = 0;
		$free_amount = 0;
		$data['dashboard_list'] = $this->admin_panel_model->admin_count_dashboard(date('M-Y'));
		
		$data['dashboard_list1'] = $this->admin_panel_model->admin_count_dashboard();
		
		$balance = 0;
		$in_datas = $this->list_model->get_merchant_subscription();
		if (!empty($in_datas)) {

			foreach ($in_datas as $in_data) {


				$balance += $in_data['revenue'];
				$ss_amount += $in_data['ss_merchant'];
				$vt_amount += $in_data['vt_merchant'];
				$as_amount += $in_data['as_merchant'];
				$free_amount += $in_data['free_merchant'];
			}
		}
		$data['dashboard_list']['estm_amount_total'] = number_format($balance, 2);
		$data['dashboard_list']['ss_amount']  = $ss_amount;
		$data['dashboard_list']['vt_amount'] = $vt_amount;
		$data['dashboard_list']['as_amount'] = $as_amount;
		$data['dashboard_list']['free_amount'] = $free_amount;

		$data['dashboard_list']['total_plan_ratio'] = $ss_amount + $vt_amount;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/dashboard-new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function reseller_login()
	{

		$loginID = $this->uri->segment('3');
		$directResellerID = getenv('DIRECT_CZ_RESELLER_ID');
		
		$data = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $loginID));
		if (!empty($data)) {

			$data['login_type'] = 'RESELLER';

			$this->general_model->track_user("Reseller", $data['resellerEmail'], $data['resellerID']);

			$resellerRedirectURL = RSURL;
			if($directResellerID == $loginID){
				$resellerRedirectURL = str_replace('reseller.','manage.' ,$resellerRedirectURL);  
				$this->session->set_userdata('reseller_direct_logged_in', $data);
			} else {
				$this->session->set_userdata('reseller_logged_in', $data);
			}
			
			$this->session->set_flashdata('message', '');

			$result_tmps = $this->general_model->get_table_data('tbl_template_reseller_data', array('userTemplate' => '2'));

			foreach ($result_tmps as $res) {

				$insert_data = array(
					'templateName'  => $res['templateName'],
					'templateType' => $res['templateType'],
					'status'		=> '1',

					'emailSubject' => $res['emailSubject'],


					'createdAt' => date('Y-m-d H:i:s'),
					'message'		=> $res['message'],
					'resellerID' => $data['resellerID'],
				);
				$res_num = $this->general_model->get_num_rows('tbl_eml_temp_reseller', array('resellerID' => $data['resellerID'], 'templateType' => $res['templateType']));
				if ($res_num == 0)
					$this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data);
			}

			

			redirect($resellerRedirectURL, 'refresh');
		}
	}

	public function merchant_login()
	{
		$this->session->unset_userdata('disconnect');

		$loginID = $this->uri->segment('3');
		$userdata = $this->admin_panel_model->get_select_data('tbl_merchant_data', array('merchID', 'resellerID', 'agentID', 'plan_id', 'firstLogin', 'firstName', 'lastName', 'allowGateway', 'merchantContact', 'companyName', 'merchantEmail', 'merchant_default_timezone'), array('merchID' => $loginID));
		$user = json_decode(json_encode($userdata), FALSE);

		$rID = $user->resellerID;
		$Merchant_url = $this->admin_panel_model->get_row_data('Config_merchant_portal', array('resellerID' => $rID));

		if ($Merchant_url['merchantPortalURL'] != '') {
			$url = $Merchant_url['merchantPortalURL'];

			$url = preg_replace("/^http:/i", "https:", $url);
			$base_url = $url . "firstlogin/index/";

			if ($user && isset($user->merchantEmail)) {
				$usus_data = $this->admin_panel_model->login_as_mecrchant($loginID);

				if ($this->session->all_userdata()){
					$this->session->unset_userdata('logged_in');
				}

				$sess_array = (array) $user;
				$rs  =  $this->admin_panel_model->get_select_data('tbl_reseller', array('ProfileURL', 'Chat'), array('resellerID' => $rID));

				if (!empty($rs)) {
					$sess_array['logo_img'] = '';
					$sess_array['script']   =  '';
				}
				$this->general_model->track_user("Merchant", $sess_array['merchantEmail'], $sess_array['merchID']);

				if ($user->firstLogin == 0) {

					$condition  = array('merchantID' => $user->merchID);
					$config     = $this->admin_panel_model->get_row_data('tbl_config_setting', $condition);
					$appset     = $this->admin_panel_model->get_row_data('app_integration_setting', $condition);
					if (!empty($appset)) {
						$sess_array['active_app']	= $appset['appIntegration'];
					} else {
						$sess_array['active_app']	= '2';
					}

					$gatway_num =  $this->admin_panel_model->get_num_rows('tbl_merchant_gateway', $condition);

					if ($gatway_num) {
						$sess_array['merchant_gateway']	= '1';
					} else {
						$sess_array['merchant_gateway']	= '0';
					}

					$filenum = $this->admin_panel_model->get_num_rows('tbl_company', $condition);
					$qm =  $this->db->query('select  *  from tbl_company where merchantID="' . $user->merchID . '" and  fileID!=""  ');
					if ($qm->num_rows() > 0)
						$sess_array['fileID'] = $qm->num_rows();
					else {
						$sess_array['fileID']  = '';
					}


					if (!empty($config)) {

						$sess_array['portalprefix']      = $config['portalprefix'];
						$sess_array['ProfileImage']      = $config['ProfileImage'];
						$sess_array['merchantTagline']      = $config['merchantTagline'];
					} else {
						$sess_array['portalprefix']      = '';
						$sess_array['ProfileImage']      = '';
						$sess_array['merchantTagline']      = '';
					}


					if ($user->companyName != "" && $user->merchantAddress1 != "" && $user->merchantFullAddress != "" &&  $sess_array['merchant_gateway'] != "0" && $sess_array['fileID'] > 0) {

						$input_data   = array('firstLogin' => '1');
						$condition1   = array('merchID' => $user->merchID);
						$this->admin_panel_model->update_row_data('tbl_merchant_data', $condition1, $input_data);
					}


					$sess_array['active_app'] = '0';



					$this->session->set_userdata('logged_in', $sess_array);
					redirect($url . 'firstlogin/dashboard_first_login');
				} else {

					$condition  = array('merchantID' => $user->merchID);
					$gatway_num =  $this->admin_panel_model->get_num_rows('tbl_merchant_gateway', $condition);

					if ($gatway_num) {
						$sess_array['merchant_gateway']	= '1';
					} else {
						$sess_array['merchant_gateway']	= '0';
					}
					$config     = $this->admin_panel_model->get_row_data('tbl_config_setting', $condition);


					if (!empty($config)) {

						$sess_array['portalprefix']      = $config['portalprefix'];
						$sess_array['ProfileImage']      = $config['ProfileImage'];
						$sess_array['merchantTagline']      = isset($config['merchantTagline']) ? $config['merchantTagline'] : '';
					} else {
						$sess_array['portalprefix']      = '';
						$sess_array['ProfileImage']      = '';
						$sess_array['merchantTagline']      = '';
					}
					$appset     = $this->admin_panel_model->get_row_data('app_integration_setting', $condition);


					if (!empty($appset)) {
						$sess_array['active_app']	= $appset['appIntegration'];
						$sess_array['gatewayMode']	= $appset['transactionMode'];
					} else {
						$sess_array['active_app']	= '2';
						$sess_array['gatewayMode']	= 0;
					}

					//check session

					if ($this->session->userdata('logged_in')) {

						if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 30)) {
							$this->session->unset_userdata('logged_in', $sess_array);
						}
						$_SESSION['LAST_ACTIVITY'] = time();
					} else {
						$this->session->set_userdata('logged_in', $sess_array);
					}


					$merchantID       = $this->session->userdata('logged_in')['merchID'];
					if ($this->admin_panel_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

						$fromEmail       = DEFAULT_FROM_EMAIL;

						$templatedatas =  $this->admin_panel_model->get_table_data('tbl_email_template_data', '');

						foreach ($templatedatas as $templatedata) {
							$insert_data = array(
								'templateName'  => $templatedata['templateName'],
								'templateType'    => $templatedata['templateType'],
								'merchantID'      => $merchantID,
								'fromEmail'      => DEFAULT_FROM_EMAIL,
								'message'		  => $templatedata['message'],
								'emailSubject'   => $templatedata['emailSubject'],
								'createdAt'      => date('Y-m-d H:i:s')
							);
							$this->admin_panel_model->insert_row('tbl_email_template', $insert_data);
						}
					}


					if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

						$this->admin_panel_model->insert_inv_number($merchantID);
					}



					if ($this->admin_panel_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
						$this->admin_panel_model->insert_payterm($merchantID);
					}

					if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '1') {



						$furl  =	$url . 'QBO_controllers/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '3') {
						$furl  =	$url . 'FreshBooks_controllers/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '4') {
						$furl =	$url . 'Integration/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '5') {
						$furl =	$url . 'company/home/index';
					} else {
						$furl  =	$url . 'home/index';
					}

					redirect($furl, 'refresh');
				}


				$this->session->set_userdata('logged_in', $sess_array);


				redirect($base_url);
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Please create your portal url.</div>');
			redirect('Admin_panel/merchant_list', 'refresh');
		}
	}


	function populate_state()
	{
		$this->load->model('admin_panel_model');
		$id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->admin_panel_model->get_table_data('state', array('country_id' => $id));
		echo json_encode($data);
	}

	function populate_city()
	{
		$this->load->model('admin_panel_model');
		$id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->admin_panel_model->get_table_data('city', array('state_id' => $id));
		echo json_encode($data);
	}


	public function admin_plans()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		$planname =  $this->admin_panel_model->planTypeDetails();
		$data['plans'] = $planname;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_plans', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function admin_tiers()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');


		$tiername = $this->admin_panel_model->get_table_data('tiers', '');
		$data['tiers'] = $tiername;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_tiers', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function create_plan()

	{
		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('planName', 'Plan Name', 'required|xss_clean');
			$this->form_validation->set_rules('subscriptionRetail', 'Subscription Retail', 'required|xss_clean');

			if($this->czsecurity->xssCleanPostInput('is_free_trial') == 1){
				$this->form_validation->set_rules('free_trial_day', 'Free Trial Day', 'required|xss_clean');
			}

			if ($this->form_validation->run() == true) {

				$input_data['plan_name']	             = $this->czsecurity->xssCleanPostInput('planName');

				$input_data['merchant_plan_Type']	    = $this->czsecurity->xssCleanPostInput('merchantplantype');

				$input_data['planType']	       = '3';  /* This is for without  Flat Rate */

				$input_data['subscriptionRetail']	   = $this->czsecurity->xssCleanPostInput('subscriptionRetail');
				$input_data['flat_service']	   = $this->czsecurity->xssCleanPostInput('service_Fee3');

				if ($input_data['planType'] == 1) {
					$input_data['serviceFee']	   = $this->czsecurity->xssCleanPostInput('service_Fee1');
				}

				if($input_data['merchant_plan_Type'] == 'Free') {
					$input_data['is_free_trial']	   = 0;
				} else {
					$input_data['is_free_trial']	   = $this->czsecurity->xssCleanPostInput('is_free_trial');
				}

				if($input_data['is_free_trial'] == 1){
					$input_data['free_trial_day']	   = $this->czsecurity->xssCleanPostInput('free_trial_day');
				}else{
					$input_data['free_trial_day']	   = 0;
				}

				$bps = $this->input->post('bps',true);
				
				if($bps != '' && $bps > 0){
					$calBps = ($bps/10000);
					$input_data['billingType'] = 2;
					$input_data['percent'] = $calBps;
					$input_data['bps'] = $bps;
				}else{
					$input_data['billingType'] = 1;
					$input_data['percent'] = 0.00;
					$input_data['bps'] = 0;
				}
				

				$input_data['allowed_transactions']	   = $this->czsecurity->xssCleanPostInput('allowed_transactions');
				
				$input_data['date'] = date('Y-m-d');


				if ($this->czsecurity->xssCleanPostInput('planID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('planID');
					$chk_condition = array('plan_id' => $id);
					$this->admin_panel_model->update_row_data('plans', $chk_condition, $input_data);
					$this->session->set_flashdata('message', '<strong>Successfully Updated</strong>');
				} else {

					$this->admin_panel_model->insert_row('plans', $input_data);
					$this->session->set_flashdata('message', '<strong>Success:</strong> Your Information Stored successfully.');
				}
				if($this->czsecurity->xssCleanPostInput('customer_type') == 'InActive'){
					redirect(base_url('Admin_panel/inactive_paln', 'refresh'));
				}else{
					redirect(base_url('Admin_panel/admin_plans', 'refresh'));
				}
				
			}
		}
		redirect(base_url('Admin_panel/admin_plans', 'refresh'));

		if ($this->uri->segment('3')) {
			$planID  			  = $this->uri->segment('3');
			$con                = array('plan_id' => $planID);
			$data['plan'] 	  = $this->admin_panel_model->get_row_data('plans', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('admin_logged_in');


		$planname = $this->admin_panel_model->get_table_data('plans', '');
		$data['plans'] = $planname;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_plans', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_tier()

	{
		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');

			$this->form_validation->set_rules('tier1_commission', 'Tier1 Commission', 'required|xss_clean');
			$this->form_validation->set_rules('tier2_commission', 'Tier2 Commission', 'required|xss_clean');
			$this->form_validation->set_rules('tier2_commission', 'Tier3 Commission', 'required|xss_clean');

			if ($this->form_validation->run() == true) {
				$input_data['tier1_min_new_account']	   = $this->czsecurity->xssCleanPostInput('tier1_account');
				$input_data['tier1_commission']	   = $this->czsecurity->xssCleanPostInput('tier1_commission');
				$input_data['tier2_min_new_account']	   = $this->czsecurity->xssCleanPostInput('tier2_account');
				$input_data['tier2_commission']	   = $this->czsecurity->xssCleanPostInput('tier2_commission');
				$input_data['tier3_min_new_account']	   = $this->czsecurity->xssCleanPostInput('tier3_account');
				$input_data['tier3_commission']	   = $this->czsecurity->xssCleanPostInput('tier3_commission');

				$input_data['tier_name']	   = $this->czsecurity->xssCleanPostInput('tierName');

				$input_data['date'] = date('Y-m-d');


				if ($this->czsecurity->xssCleanPostInput('tierID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('tierID');
					$chk_condition = array('tier_id' => $id);
					$this->admin_panel_model->update_row_data('tiers', $chk_condition, $input_data);
					$this->session->set_flashdata('message', '<strong>Successfully Updated</strong>');
				} else {

					$this->admin_panel_model->insert_row('tiers', $input_data);
					$this->session->set_flashdata('message', '<strong>Success:</strong> Your Information Stored successfully.');
				}
				redirect(base_url('Admin_panel/admin_tiers'));
			}
		}

		if ($this->uri->segment('3')) {
			$planID  			  = $this->uri->segment('3');
			$con                = array('plan_id' => $planID);
			$data['plan'] 	  = $this->admin_panel_model->get_row_data('plans', $con);
		}

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('admin_logged_in');


		$tiername = $this->admin_panel_model->get_table_data('tiers', '');
		$data['tiers'] = $tiername;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_tiers', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function get_plan_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('plan_id');
		$val = array(
			'plan_id' => $id,
		);

		$data = $this->admin_panel_model->get_row_data('plans', $val);
		echo json_encode($data);
	}

	public function get_tier_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('tier_id');
		$val = array(
			'tier_id' => $id,
		);

		$data = $this->admin_panel_model->get_row_data('tiers', $val);
		echo json_encode($data);
	}


	public function delete_tier()
	{


		$id = $this->czsecurity->xssCleanPostInput('tier_id');
		$condition =  array('tier_id' => $id);
		$del      = $this->admin_panel_model->delete_row_data('tiers', $condition);
		if ($del) {
			$this->session->set_flashdata('message', '<strong> Successfully Deleted</strong>');
		} else {
			$this->session->set_flashdata('message', '<strong>Error: </strong>Something Is Wrong.');
		}

		redirect(base_url('Admin_panel/admin_tiers'));
	}

	/***************************** Reseller ************************************/

	public function reseller_list()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$reseller = $this->list_model->get_rss_revenue(array('isDelete' => 0, 'isEnable' => '1'));
		$data['gateways'] = $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');

		$directReseller = getenv('DIRECT_CZ_RESELLER_ID');
		$direct_reseller = [];
		foreach($reseller as $rkey => $rvalue){
			if($rvalue['resellerID'] == $directReseller){
				$direct_reseller[] = $rvalue;
				unset($reseller[$rkey]);
				break;
			}
		}
		$data['direct_reseller_list'] = $direct_reseller;
		$data['reseller_list'] = $reseller;

		$marchant = $this->admin_panel_model->reasign_marchent();
		$data['merchant'] = $marchant;
		$data['reasign'] = $marchant;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/reseller', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/***************************disable reseller *************************/
	public function disable_list()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		$reseller = $this->admin_panel_model->disable_admin_reseller();
		$data['reseller_list'] = $reseller;
		$marchant = $this->admin_panel_model->reasign_marchent();
		$data['merchant'] = $marchant;
		$data['reasign'] = $marchant;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/disable_reseller', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	/***************** 01-11-2018  pawan code  *********************/

	public function disable_merchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('merchIDs');
		if (!empty($id)) {
			$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '1'));
			if (!empty($res)) {

				$ins_data = array('merchID' => $id, 'isEnable' => '0', 'updatedAt' => date('Y-m-d H:i:s'));

				$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '1'), $ins_data);

				$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Disabled</div>');
				redirect(base_url('Admin_panel/merchant_list'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> Something Is Wrong</div>');
				redirect(base_url('Admin_panel/merchant_list'));
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Invalid Request</div>');
			redirect(base_url('Admin_panel/merchant_list'));
		}
	}


	public function suspend_merchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('merchID');
		if (!empty($id)) {
			$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isSuspend' => '0'));
			if (!empty($res)) {

				$ins_data = array('merchID' => $id, 'isSuspend' => '1', 'updatedAt' => date('Y-m-d H:i:s'));

				$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isSuspend' => '0'), $ins_data);
				$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Suspended</div>');
				redirect(base_url('Admin_panel/merchant_list'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> Something Is Wrong</div>');
				redirect(base_url('Admin_panel/merchant_list'));
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Invalid Request</div>');
			redirect(base_url('Admin_panel/merchant_list'));
		}
	}
	public function enable_suspend_merchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('merchID');
		if (!empty($id)) {
			$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isSuspend' => '1'));
			if (!empty($res)) {

				$ins_data = array('merchID' => $id, 'isSuspend' => '0', 'updatedAt' => date('Y-m-d H:i:s'),'login_failed_attemp' => 0,'isSuspend' => 0,'isLockedTemp' => 0,'accountLockedDate' => null,'lockedMessage' => null);

				$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isSuspend' => '1'), $ins_data);
				$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Enabled</div>');

				redirect(base_url('Admin_panel/merchant_list'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> Something Is Wrong</div>');
				redirect(base_url('Admin_panel/merchant_list'));
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Invalid Request</div>');
			redirect(base_url('Admin_panel/merchant_list'));
		}
	}


	//disable marchants list
	public function disable_marchants_list()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		
		$merchant_list = $this->admin_panel_model->get_disable_data_admin_merchant();
		$data['merchant_list'] = $merchant_list;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/disable_marchants', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function delete_marchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('marchID');

		$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '0'));
		if (!empty($res)) {

			$ins_data = array('merchID' => $id, 'isDelete' => '1', 'updatedAt' => date('Y-m-d H:i:s'));

			$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '0'), $ins_data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Deleted</div>');
			redirect(base_url('Admin_panel/disable_marchants_list'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Something Is Wrong</div>');
			redirect(base_url('Admin_panel/disable_marchants_list'));
		}
	}



	public function enable_marchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('marchIDs');

		$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '0'));
		if (!empty($res)) {

			$ins_data = array('merchID' => $id, 'isEnable' => '1', 'updatedAt' => date('Y-m-d H:i:s'));

			$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isEnable' => '0'), $ins_data);
			
			$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Enabled</div>');
			redirect(base_url('Admin_panel/disable_marchants_list'));
		} else {
			
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Something Is Wrong</div>');
			redirect(base_url('Admin_panel/disable_marchants_list'));
		}
	}



	/***************************disable reseller end*************************/


	/********************************************************************
	 *******************************************************      OLD Create Reseller 		*******************************************************
	 **********************************************************************                     ******************************************************/
	public function create_reseller_old()
	{

		$this->load->model('card_model');

		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->input->post(null, true))) {
			$id = '';
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('resellerCompanyName', 'Reseller Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('payOption', 'Payment Option', 'required');

			if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {
				$this->form_validation->set_rules('resellerEmail', 'Email', 'required');
				$this->form_validation->set_rules('resellerPassword', 'Password', 'required');
			} else {

				$id = $this->czsecurity->xssCleanPostInput('resellerID');
			}


			if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {

				$this->form_validation->set_rules('billingfirstName', 'Billing First Name', 'required');
				$this->form_validation->set_rules('billinglastName', 'Billing Last Name', 'required');
			}

			if ($this->czsecurity->xssCleanPostInput('payOption') == '2') {

				$this->form_validation->set_rules('cardNumber', 'Card Number', 'required');
				
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'required');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'required');
			}

			if ($this->czsecurity->xssCleanPostInput('payOption') == '3') {

				$this->form_validation->set_rules('accountName', 'Account Name', 'required');
				$this->form_validation->set_rules('accountNumber', 'Account Number', 'required|min_length[9]|max_length[16]|numeric');
				$this->form_validation->set_rules('routNumber', 'Route Number', 'required|min_length[9]|max_length[12]|numeric');
				$this->form_validation->set_rules('acct_holder_type', 'Account Type', 'required');
				$this->form_validation->set_rules('secCode', 'Entry Method', 'required|min_length[3]|max_length[3]');
			}


			if ($this->form_validation->run() == true) {
				if (!empty($_FILES['picture']['name'])) {
					$config['upload_path'] = 'uploads/reseller_logo/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['picture']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 200;
					$config['height'] = 50;
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('picture')) {
						$uploadData = $this->upload->data();
						$picture = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];
						
					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$picture = '';
					}
					$input_data['resellerProfileURL']  = $picture;
				}

				if (!empty($_FILES['pictureportal']['name'])) {
					$config['upload_path'] = 'uploads/reseller_logo/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['pictureportal']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 300;
					$config['height'] = 100;
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('pictureportal')) {
						$uploadData = $this->upload->data();
						$pictureportal = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];

					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$pictureportal = '';
					}
					$input_data['ProfileURL']  = $pictureportal;
				}

				$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
				$email   = $this->czsecurity->xssCleanPostInput('resellerEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$qq = implode(',', (array) $this->czsecurity->xssCleanPostInput('Plans'));
				$input_data['resellerCompanyName']	   = $this->czsecurity->xssCleanPostInput('resellerCompanyName');
				$input_data['resellerfirstName']	   = $FirstName;
				$input_data['lastName']	               = $LastName;
				$input_data['primaryContact']	   = $this->czsecurity->xssCleanPostInput('primaryContact');
				$input_data['resellerAddress']	   = $this->czsecurity->xssCleanPostInput('resellerAddress');
				$input_data['resellerAddress2']	   = $this->czsecurity->xssCleanPostInput('resellerAddress2');


				if ($this->czsecurity->xssCleanPostInput('federalTaxID') == "") {
					$input_data['federalTaxID']	   = '0';
				}
				$input_data['billingEmailAddress'] = $this->czsecurity->xssCleanPostInput('payEmail');
				$input_data['payOption']	   = $this->czsecurity->xssCleanPostInput('payOption');

				$input_data['resellerCountry']	   = $this->czsecurity->xssCleanPostInput('resellerCountry');
				$input_data['resellerState']	   = $this->czsecurity->xssCleanPostInput('resellerState');
				$input_data['resellerCity']	   = $this->czsecurity->xssCleanPostInput('resellerCity');
				$input_data['zipCode']	   = $this->czsecurity->xssCleanPostInput('zipCode');


				/*****************************  reseller_card_data  table data ***********************/

				if ($this->czsecurity->xssCleanPostInput('payOption') == '3') {
					$input_data2['routeNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('routNumber'));
					$input_data2['accountNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('accountNumber'));
					$input_data2['accountType'] = $this->czsecurity->xssCleanPostInput('acct_type');
					$input_data2['accountHolderType'] = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$input_data2['secCodeEntryMethod'] = $this->czsecurity->xssCleanPostInput('secCode');
					$input_data2['accountName'] = $this->czsecurity->xssCleanPostInput('accountName');
				}
				if ($this->czsecurity->xssCleanPostInput('payOption') == '2') {
					$input_data2['CustomerCard'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cardNumber'));
					$input_data2['CardCVV'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
					$input_data2['cardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
					$input_data2['cardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
				}


				$input_data2['Billing_First_Name']	    = $this->czsecurity->xssCleanPostInput('billingfirstName');
				$input_data2['Billing_Last_Name']	    = $this->czsecurity->xssCleanPostInput('billinglastName');
				$input_data2['Billing_Addr1']           = $this->czsecurity->xssCleanPostInput('billingAddress1');
				$input_data2['Billing_Addr2']           = $this->czsecurity->xssCleanPostInput('billingAddress2');
				$input_data2['Billing_Email_Address']   = $this->czsecurity->xssCleanPostInput('billingEmailAddress');
				$input_data2['Billing_City']            = $this->czsecurity->xssCleanPostInput('billingCity');
				$input_data2['Billing_State']           = $this->czsecurity->xssCleanPostInput('billingState');
				$input_data2['Billing_Country']         = $this->czsecurity->xssCleanPostInput('billingCountry');
				$input_data2['Billing_Zipcode']         = $this->czsecurity->xssCleanPostInput('billingPostalCode');
				$input_data2['Billing_Contact']	        = $this->czsecurity->xssCleanPostInput('billingContact');


				/******************************************* End Here ************************************/

				$input_data['plan_id']	   = $qq;
				$input_data['loginCode']	   = $code;

				$resGetway = $this->general_model->get_row_data('tbl_admin_gateway', array('set_as_default' => '1'));

				$input_data['gatewayID'] = $resGetway['gatewayID'];

				if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {
					$input_data['isEnable']	   = '0';
				}

				$input_data['date']	   = date('Y-m-d H:i:s');



				if ($this->czsecurity->xssCleanPostInput('resellerID') != "") {


					$input_data['updatedAt']	   = date('Y-m-d H:i:s');

					$id = $this->czsecurity->xssCleanPostInput('resellerID');
					$chk = $this->admin_panel_model->check_reseller_edit_email($id, $email);
					if ($chk) {
						$input_data['resellerEmail'] = $email;

						$chk_condition = array('resellerID' => $id);
						$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data);
						$this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Success:</strong> Successfully Updated.</div>');
					} else {
						$this->session->set_flashdata('error', '<div class="alert alert-danger"><strong>This is Email is already used</strong></div>');
						redirect(base_url('Admin_panel/create_reseller/' . $id), 'refresh');
					}

					$chk_condition = array('resellerID' => $id);
					$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data);


					if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {
						$input_data2['cardResellerID'] = $id;
						$billing_condition = array('cardResellerID' => $id);
						$card_ddt = $this->card_model->get_card_row_data('reseller_card_data', $billing_condition);
						if (!empty($card_ddt))
							$this->card_model->update_card_data($billing_condition, $input_data2);
						else
							$this->card_model->insert_card_data($input_data2);
					}
				} else {

					$results = $this->admin_panel_model->check_existing_user('tbl_reseller', array('resellerEmail' => $email));

					if ($results) {
						$this->session->set_flashdata('message', '<strong>Error:</strong> Email Already Exitsts.');
					} else {

						$input_data['resellerEmail']	   = $email;
						$input_data['isEnable']	   = 1;



						$username = $email;
						$pass     = $this->czsecurity->xssCleanPostInput('resellerPassword');
						$input_data['resellerPassword'] = md5($this->czsecurity->xssCleanPostInput('resellerPassword'));

						$insert = $this->admin_panel_model->insert_row('tbl_reseller', $input_data);

						if ($insert) {
							$input_data2['cardResellerID'] = $insert;
							if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {

								$this->card_model->insert_card_data($input_data2);
							}

							$results = $this->general_model->get_table_data('tbl_template_reseller_data', array('userTemplate' => 2));


							foreach ($results as $res) {

								$insert_data = array(
									'templateName'  => $res['templateName'],
									'templateType' => $res['templateType'],
									'status'		=> '1',

									'emailSubject' => $res['emailSubject'],


									'createdAt' => date('Y-m-d H:i:s'),
									'message'		=> $res['message'],
									'resellerID' => $insert,
								);

								$this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data);
							}



							$this->session->set_flashdata('message', '<strong>Successfully Created</strong>');
							$this->load->library('email');

							$subject = 'ChargeZoom, Verification mail';
							$this->email->from('support@chargezoom.com');
							$this->email->to($email);
							$this->email->subject($subject);
							$this->email->set_mailtype("html");
							$base_url = "http://reseller.payportal.com/login/enable_reseller/" . $code;

							$message = "<p>Dear $FirstName $LastName,<br><br>You are registered for ChargeZoom.</p><br> Your login details is given below : <br/><br/>Username : $username <br/>Password : $pass <br/><br/> <p> Please verify your email id by clicking following verification link.</p><br> <p><a href=" . $base_url . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
							$this->email->message($message);

							if ($this->email->send()) {
								$this->session->set_flashdata('message', '<strong>Successfully Created</strong>');
							} else {
								$this->session->set_flashdata('message', '<strong>Error:</strong> Email was not sent, please contact your administrator.' . $email);
							}
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> In register reseller process');
						}
						redirect('Admin_panel/reseller_list', 'refresh');
					}
				}

				redirect('Admin_panel/create_reseller/' . $id, 'refresh');
			}
		}


		if ($this->uri->segment('3')) {
			$resellerID  			  = $this->uri->segment('3');
			$con                = array('resellerID' => $resellerID);

			$con2                = array('cardResellerID' => $resellerID);


			$data['reseller'] 	  = $this->admin_panel_model->get_row_data('tbl_reseller', $con);

			$data['reseller_card_data'] 	  = $this->card_model->get_card_row_data('reseller_card_data', $con2);

			$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');
		}

		$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		$pay_option         = $this->admin_panel_model->get_table_data('tbl_payment_option', '');
		$data['pay_options'] = $pay_option;

		$plans = $this->admin_panel_model->planTypeDetails();
		$tiers = $this->admin_panel_model->get_table_data('tiers', '');
		$data['plan_list'] = $plans;
		$data['tier_list'] = $tiers;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_reseller', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/*********************************** New Create Reseller Start  Here  *************************/

	public function create_reseller11()
	{

		$this->load->model('card_model');

		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->input->post(null, true))) {
			$id = '';
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('resellerCompanyName', 'Reseller Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('payOption', 'Payment Option', 'required');

			if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {

				$this->form_validation->set_rules('resellerEmail', 'Email', 'required');
				$this->form_validation->set_rules('resellerPassword', 'Password', 'required');
			} else {

				$id = $this->czsecurity->xssCleanPostInput('resellerID');
			}

			if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {

				$this->form_validation->set_rules('billingfirstName', 'Billing First Name', 'required');
				$this->form_validation->set_rules('billinglastName', 'Billing Last Name', 'required');
			}

			if ($this->czsecurity->xssCleanPostInput('payOption') == '2') {

				$this->form_validation->set_rules('cardNumber', 'Card Number', 'required');
				$this->form_validation->set_rules('cvv', 'Security Code (CVV)', 'required');
				$this->form_validation->set_rules('expiry', 'Expiry Month', 'required');
				$this->form_validation->set_rules('expiry_year', 'Expiry Year', 'required');
			}

			if ($this->czsecurity->xssCleanPostInput('payOption') == '3') {

				$this->form_validation->set_rules('accountName', 'Account Name', 'required');
				$this->form_validation->set_rules('accountNumber', 'Account Number', 'required|min_length[9]|max_length[16]|numeric');
				$this->form_validation->set_rules('routNumber', 'Route Number', 'required|min_length[9]|max_length[12]|numeric');
				$this->form_validation->set_rules('acct_holder_type', 'Account Type', 'required');
				$this->form_validation->set_rules('secCode', 'Entry Method', 'required|min_length[3]|max_length[3]');
			}


			if ($this->form_validation->run() == true) {


				if (!empty($_FILES['picture']['name'])) {


					$config['upload_path'] = 'uploads/reseller_logo/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['picture']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 200;
					$config['height'] = 50;

					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('picture')) {
						$uploadData = $this->upload->data();
						$picture = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];
					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$picture = '';
					}

					$input_data['resellerProfileURL']  = $picture;
				}

				if (!empty($_FILES['pictureportal']['name'])) {
					$config['upload_path'] = 'uploads/reseller_logo/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['pictureportal']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 300;
					$config['height'] = 100;
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('pictureportal')) {
						$uploadData = $this->upload->data();
						$pictureportal = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];

					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$pictureportal = '';
					}
					$input_data['ProfileURL']  = $pictureportal;
				}

				$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
				$email   = $this->czsecurity->xssCleanPostInput('resellerEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$qq = implode(',', (array) $this->czsecurity->xssCleanPostInput('Plans'));


				$input_data['resellerCompanyName']	   = $this->czsecurity->xssCleanPostInput('resellerCompanyName');
				$input_data['resellerfirstName']	   = $FirstName;
				$input_data['lastName']	               = $LastName;
				$input_data['primaryContact']	   = $this->czsecurity->xssCleanPostInput('primaryContact');
				$input_data['resellerAddress']	   = $this->czsecurity->xssCleanPostInput('resellerAddress');
				$input_data['resellerAddress2']	   = $this->czsecurity->xssCleanPostInput('resellerAddress2');


				$input_data['federalTaxID']	   = $this->czsecurity->xssCleanPostInput('federalTaxID');


				if (!empty($this->czsecurity->xssCleanPostInput('resellerChat'))) {
					$input_data['Chat']	   = $this->czsecurity->xssCleanPostInput('resellerChat', false);
				}
				$input_data['billingEmailAddress'] = $this->czsecurity->xssCleanPostInput('payEmail');
				$input_data['payOption']	   = $this->czsecurity->xssCleanPostInput('payOption');

				$input_data['resellerCountry']	   = $this->czsecurity->xssCleanPostInput('resellerCountry');
				$input_data['resellerState']	   = $this->czsecurity->xssCleanPostInput('resellerState');
				$input_data['resellerCity']	   = $this->czsecurity->xssCleanPostInput('resellerCity');
				$input_data['zipCode']	   = $this->czsecurity->xssCleanPostInput('zipCode');


				/*****************************  reseller_card_data  table data ***********************/

				if ($this->czsecurity->xssCleanPostInput('payOption') == '3') {
					$input_data2['routeNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('routNumber'));
					$input_data2['accountNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('accountNumber'));
					$input_data2['accountType'] = $this->czsecurity->xssCleanPostInput('acct_type');
					$input_data2['accountHolderType'] = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$input_data2['secCodeEntryMethod'] = $this->czsecurity->xssCleanPostInput('secCode');
					$input_data2['accountName'] = $this->czsecurity->xssCleanPostInput('accountName');
				}
				if ($this->czsecurity->xssCleanPostInput('payOption') == '2') {
					$input_data2['CustomerCard'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cardNumber'));
					$input_data2['CardCVV'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
					$input_data2['cardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
					$input_data2['cardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
				}


				$input_data2['Billing_First_Name']	    = $this->czsecurity->xssCleanPostInput('billingfirstName');
				$input_data2['Billing_Last_Name']	    = $this->czsecurity->xssCleanPostInput('billinglastName');
				$input_data2['Billing_Addr1']           = $this->czsecurity->xssCleanPostInput('billingAddress1');
				$input_data2['Billing_Addr2']           = $this->czsecurity->xssCleanPostInput('billingAddress2');
				$input_data2['Billing_Email_Address']   = $this->czsecurity->xssCleanPostInput('billingEmailAddress');
				$input_data2['Billing_City']            = $this->czsecurity->xssCleanPostInput('billingCity');
				$input_data2['Billing_State']           = $this->czsecurity->xssCleanPostInput('billingState');
				$input_data2['Billing_Country']         = $this->czsecurity->xssCleanPostInput('billingCountry');
				$input_data2['Billing_Zipcode']         = $this->czsecurity->xssCleanPostInput('billingPostalCode');
				$input_data2['Billing_Contact']	        = $this->czsecurity->xssCleanPostInput('billingContact');


				/******************************************* End Here ************************************/

				$input_data['plan_id']	   = $qq;
				$input_data['loginCode']	   = $code;
				if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {
					$input_data['isEnable']	   = '0';
				}

				if ($this->czsecurity->xssCleanPostInput('resellerID') != "") {

					$input_data['updatedAt']	   = date('Y-m-d H:i:s');


					$id = $this->czsecurity->xssCleanPostInput('resellerID');
					$chk = $this->admin_panel_model->check_reseller_edit_email($id, $email);
					if ($chk) {
						$input_data['resellerEmail'] = $email;

						$chk_condition = array('resellerID' => $id);
						$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data);
						$this->session->set_flashdata('message', '<strong>Successfully Updated</strong>');
					} else {
						$this->session->set_flashdata('error', '<strong>This is Email is already used</strong>');
						redirect(base_url('Admin_panel/create_reseller/' . $id), 'refresh');
					}

					$chk_condition = array('resellerID' => $id);
					$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data);



					if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {
						$input_data2['cardResellerID'] = $id;
						$billing_condition = array('cardResellerID' => $id);
						$card_ddt = $this->card_model->get_card_row_data('reseller_card_data', $billing_condition);


						if (!empty($card_ddt))
							$this->card_model->update_card_data($billing_condition, $input_data2);
						else
							$sss =   $this->card_model->insert_card_data($input_data2);
					}
				} else {
					$input_data['date']	   = date('Y-m-d H:i:s');
					$results = $this->admin_panel_model->check_existing_user('tbl_reseller', array('resellerEmail' => $email));

					if ($results) {
						$this->session->set_flashdata('message', '<strong>Error:</strong> Email Already Exitsts.');
					} else {

						$input_data['resellerEmail']	   = $email;
						$input_data['isEnable']	   = 1;



						$username = $email;
						$pass     = $this->czsecurity->xssCleanPostInput('resellerPassword');
						$input_data['resellerPassword'] = md5($this->czsecurity->xssCleanPostInput('resellerPassword'));

						$insert = $this->admin_panel_model->insert_row('tbl_reseller', $input_data);

						$input_data2['cardResellerID'] = $insert;
						if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {

							$this->card_model->insert_card_data($input_data2);
						}

						$insertResellerCardData  = $this->card_model->insert_card_data($input_data2); // to insert card details in reseller_card_data table in cznet_chargezoom_card_db  database

						if ($insert) {
							$results = $this->general_model->get_table_data('tbl_template_reseller_data', array('userTemplate' => 2));

							foreach ($results as $res) {

								$insert_data = array(
									'templateName'  => $res['templateName'],
									'templateType' => $res['templateType'],

									'status'		=> '1',

									'emailSubject' => $res['emailSubject'],


									'createdAt' => date('Y-m-d H:i:s'),
									'message'		=> $res['message'],
									'resellerID' => $insert,
								);
								$this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data);
							}



							$this->session->set_flashdata('message', '<strong>Successfully Created</strong>');
							$this->load->library('email');

							$subject = 'ChargeZoom, Verification mail';
							$this->email->from('support@chargezoom.com');
							$this->email->to($email);
							$this->email->subject($subject);
							$this->email->set_mailtype("html");
							$base_url = "http://reseller.payportal.net/login/enable_reseller/" . $code;

							$message = "<p>Dear $FirstName $LastName,<br><br>You are registered for ChargeZoom.</p><br> Your login details is given below : <br/><br/>Username : $username <br/>Password : $pass <br/><br/> <p> Please verify your email id by clicking following verification link.</p><br> <p><a href=" . $base_url . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
							$this->email->message($message);

							if ($this->email->send()) {
								$this->session->set_flashdata('message', '<strong>Successfully Created</strong>');
							} else {
								$this->session->set_flashdata('message', '<strong>Error:</strong> Email was not sent, please contact your administrator.' . $email);
							}
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> In register reseller process');
						}
						redirect('Admin_panel/reseller_list', 'refresh');
					}
				}

				redirect('Admin_panel/create_reseller/' . $id, 'refresh');
			}
		}


		if ($this->uri->segment('3')) {
			$resellerID  			  = $this->uri->segment('3');
			$con                = array('resellerID' => $resellerID);

			$con2                = array('cardResellerID' => $resellerID);


			$data['reseller'] 	  = $this->admin_panel_model->get_row_data('tbl_reseller', $con);

			$data['reseller_card_data'] 	  = $this->card_model->get_card_row_data('reseller_card_data', $con2);

			$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');
		}

		$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		$pay_option         = $this->admin_panel_model->get_table_data('tbl_payment_option', '');
		$data['pay_options'] = $pay_option;

		$plans = $this->admin_panel_model->planTypeDetails();

		$data['plan_list'] = $plans;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_reseller', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	/*********************************** New Create Reseller End Here  *************************/

	public function create_reseller()
	{

		$this->load->model('card_model');

		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->input->post(null, true))) {
			$id = '';
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('resellerCompanyName', 'Reseller Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');

			if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {

				$this->form_validation->set_rules('resellerEmail', 'Email', 'required');
				$this->form_validation->set_rules('resellerPassword', 'Password', 'required');
			} else {

				$id = $this->czsecurity->xssCleanPostInput('resellerID');
			}

			if ($this->form_validation->run() == true) {
				$config['upload_path'] = dirname(FCPATH).'/admin/uploads/reseller_logo/'; 
				if (!empty($_FILES['picture']['name'])) {

					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['picture']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 200;
					$config['height'] = 50;
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('picture')) { 
						$uploadData = $this->upload->data();
						$picture = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];
					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$picture = '';
					}

					$input_data['resellerProfileURL']  = $picture;
				}


				if (!empty($_FILES['pictureportal']['name'])) {
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['pictureportal']['name'];
					$config['max_size'] = 60;
					$config['quality'] =  '60%';
					$config['width'] = 300;
					$config['height'] = 100;
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);

					$this->upload->initialize($config);

					if ($this->upload->do_upload('pictureportal')) {
						$uploadData = $this->upload->data();
						$pictureportal = base_url() . 'uploads/reseller_logo/' . $uploadData['file_name'];

					} else {
						$data['error_msg'] = $this->upload->display_errors();
						$pictureportal = '';
					}
					$input_data['ProfileURL']  = $pictureportal;
				}
				/* Used for Orange chat please dont remove or replace with security clean features*/
				$input_data['Chat'] =  $this->input->post('resellerChat');


				$rootDomain             = RSDOMAIN;
				$portal_url 	   =  $this->czsecurity->xssCleanPostInput('portal_url'); 
				$enable 		   =  $this->czsecurity->xssCleanPostInput('enable');
				$url = "https://".$portal_url.".".$rootDomain."/";
				   
				if(!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)){
		          	if(strpos($portal_url, ' ') > 0){
		          	     
    		      	 	$this->session->set_flashdata('message','<div class="alert alert-danger"> Error:</strong> Invalid portal url space not allowed.</div>'); 
    		      	 	redirect('home/index', 'refresh'); 
    	            }
			      
		     	 	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error:</strong> Invalid portal url special character not allowed.</div>'); 
		     	 	redirect('home/index', 'refresh'); 
		        }

				$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
				$email   = $this->czsecurity->xssCleanPostInput('resellerEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$qq = implode(',', (array) $this->czsecurity->xssCleanPostInput('Plans'));

				$input_data['resellerCompanyName']	   = $this->czsecurity->xssCleanPostInput('resellerCompanyName');
				$input_data['resellerfirstName']	   = $FirstName;
				$input_data['lastName']	               = $LastName;
				$input_data['primaryContact']	   = $this->czsecurity->xssCleanPostInput('primaryContact');
				$input_data['resellerAddress']	   = $this->czsecurity->xssCleanPostInput('resellerAddress');
				$input_data['resellerAddress2']	   = $this->czsecurity->xssCleanPostInput('resellerAddress2');

				$input_data['federalTaxID']	   = $this->czsecurity->xssCleanPostInput('federalTaxID');
				$input_data['billingEmailAddress'] = $this->czsecurity->xssCleanPostInput('payEmail');
				$input_data['payOption']	   = $this->czsecurity->xssCleanPostInput('payOption');

				$input_data['resellerCountry']	   = $this->czsecurity->xssCleanPostInput('resellerCountry');
				$input_data['resellerState']	   = $this->czsecurity->xssCleanPostInput('resellerState');
				$input_data['resellerCity']	   = $this->czsecurity->xssCleanPostInput('resellerCity');
				$input_data['zipCode']	   = $this->czsecurity->xssCleanPostInput('zipCode');

				if ($this->czsecurity->xssCleanPostInput('isTestmode'))
					$input_data['isTestmode'] = 1;
				else
					$input_data['isTestmode'] =  0;

				/*****************************  reseller_card_data  table data ***********************/

				if ($this->czsecurity->xssCleanPostInput('payOption') == '3') {
					$input_data2['routeNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('routNumber'));
					$input_data2['accountNumber'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('accountNumber'));
					$input_data2['accountType'] = $this->czsecurity->xssCleanPostInput('acct_type');
					$input_data2['accountHolderType'] = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$input_data2['secCodeEntryMethod'] = 'WEB';
					$input_data2['accountName'] = $this->czsecurity->xssCleanPostInput('accountName');
				}
				if ($this->czsecurity->xssCleanPostInput('payOption') == '2') {
					$input_data2['CustomerCard'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cardNumber'));
					$input_data2['CardCVV'] = $this->card_model->encrypt($this->czsecurity->xssCleanPostInput('cvv'));
					$input_data2['cardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
					$input_data2['cardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
				}

				$input_data2['Billing_First_Name']	    = $this->czsecurity->xssCleanPostInput('billingfirstName');
				$input_data2['Billing_Last_Name']	    = $this->czsecurity->xssCleanPostInput('billinglastName');
				$input_data2['Billing_Addr1']           = $this->czsecurity->xssCleanPostInput('billingAddress1');
				$input_data2['Billing_Addr2']           = $this->czsecurity->xssCleanPostInput('billingAddress2');
				$input_data2['Billing_Email_Address']   = $this->czsecurity->xssCleanPostInput('billingEmailAddress');
				$input_data2['Billing_City']            = $this->czsecurity->xssCleanPostInput('billingCity');
				$input_data2['Billing_State']           = $this->czsecurity->xssCleanPostInput('billingState');
				$input_data2['Billing_Country']         = $this->czsecurity->xssCleanPostInput('billingCountry');
				$input_data2['Billing_Zipcode']         = $this->czsecurity->xssCleanPostInput('billingPostalCode');
				$input_data2['Billing_Contact']	        = $this->czsecurity->xssCleanPostInput('billingContact');
				$input_data2['cardMonth']               = $this->czsecurity->xssCleanPostInput('expiry');
				$input_data2['cardYear']	            = $this->czsecurity->xssCleanPostInput('expiry_year');

				/******************************************* End Here ************************************/

				$input_data['plan_id']	   = $qq;
				$input_data['loginCode']	   = $code;
				/*Add commision value*/
				$input_data['commission']	            = $this->input->post('commission',true);

				if ($this->czsecurity->xssCleanPostInput('resellerID') == "") {
					$input_data['isEnable']	   = '0';
				}

				$resGetway = $this->general_model->get_row_data('tbl_admin_gateway', array('set_as_default' => '1'));

				$input_data['gatewayID'] = $resGetway['gatewayID'];

				if ($this->czsecurity->xssCleanPostInput('resellerID') != "") {

					$input_data['updatedAt']	   = date('Y-m-d H:i:s');


					$id = $this->czsecurity->xssCleanPostInput('resellerID');
					$chk = $this->admin_panel_model->check_reseller_edit_email($id, $email);
					if ($chk) {
						$input_data['resellerEmail'] = $email;

						$chk_condition = array('resellerID' => $id);
						$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data, ['Chat']);
						$this->session->set_flashdata('message', '<div class="alert alert-success">Successfully Updated</div>');
					} else {
						$this->session->set_flashdata('error', '<div class="alert alert-success"> This is Email is already used</div>');
						redirect(base_url('Admin_panel/create_reseller/' . $id), 'refresh');
					}

					$chk_condition = array('resellerID' => $id);
					$this->admin_panel_model->update_row_data('tbl_reseller', $chk_condition, $input_data, ['Chat']);

					if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {
						$input_data2['cardResellerID'] = $id;
						$billing_condition = array('cardResellerID' => $id);
						$card_ddt = $this->card_model->get_card_row_data('reseller_card_data', $billing_condition);


						if (!empty($card_ddt))
							$this->card_model->update_card_data($billing_condition, $input_data2);
						else
							$sss =   $this->card_model->insert_card_data($input_data2);
					}
					/* Portal save */
					$codition = array('resellerID'=>$id);
					$update_data= array('merchantPortal'=>$enable,'merchantPortalURL'=>$url,'merchantHelpText'=>null, 'portalprefix'=>$portal_url  );

					$this->general_model->update_row_data('Config_merchant_portal',$codition ,$update_data);
				} else {

					$input_data['date']	   = date('Y-m-d H:i:s');
					$results = $this->admin_panel_model->check_existing_user('tbl_reseller', array('resellerEmail' => $email));

					if ($results) {
						$this->session->set_flashdata('message', '<div class="alert alert-danger"> Email Already Exitsts</div>');
					} else {

						$input_data['resellerEmail']    = $email;
						$input_data['isEnable']	        = 1;

						$username = $email;
						$pass     = $this->czsecurity->xssCleanPostInput('resellerPassword');

                        $input_data['resellerPasswordNew'] = password_hash(
                            $this->czsecurity->xssCleanPostInput('resellerPassword'),
                            PASSWORD_BCRYPT
                        );

						$insert = $this->admin_panel_model->insert_row('tbl_reseller', $input_data, ['Chat']);

						$insert_data= array('merchantPortal'=>$enable,'merchantPortalURL'=>$url,'merchantHelpText'=>null,'merchantPortal' => 0, 'resellerID'=>$insert, 'portalprefix'=>$portal_url  );

						$this->general_model->insert_row('Config_merchant_portal',$insert_data);

						$input_data2['cardResellerID'] = $insert;
						if ($this->czsecurity->xssCleanPostInput('payOption') == '2' || $this->czsecurity->xssCleanPostInput('payOption') == '3') {
							$cardid =   $this->card_model->insert_card_data($input_data2);
						}

						if ($insert) {
							$results = $this->general_model->get_table_data('tbl_template_reseller_data', array('userTemplate' => 2));

							foreach ($results as $res) {

								$insert_data = array(
									'templateName'  => $res['templateName'],
									'templateType' => $res['templateType'],

									'status'		=> '1',

									'emailSubject' => $res['emailSubject'],


									'createdAt' => date('Y-m-d H:i:s'),
									'message'		=> $res['message'],
									'resellerID' => $insert,
								);

								$this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data, ['message']);
							}

							$this->session->set_flashdata('message', '<div class="alert alert-success">Successfully Created</div>');

							if (true) {
								
								$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Created</div>');
							} else {
								
								$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Email was not sent, please contact your administrator.' . $email . '</div>');
							}
						} else {
							
							$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> In register reseller process </div>');
						}

						redirect('Admin_panel/reseller_list', 'refresh');
					}
				}

				redirect('Admin_panel/create_reseller/' . $id, 'refresh');
			} else {
				
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Validation Error </div>');
				redirect('Admin_panel/create_reseller/' . $id, 'refresh');
			}
		}


		if ($this->uri->segment('3')) {
			$resellerID  			  = $this->uri->segment('3');
			$con                = array('resellerID' => $resellerID);

			$con2                = array('cardResellerID' => $resellerID);


			$data['reseller'] 	  = $this->admin_panel_model->get_row_data('tbl_reseller', $con);

			$data['reseller_card_data'] 	  = $this->card_model->get_card_row_data('reseller_card_data', $con2);
			$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');
		}

		$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		$pay_option         = $this->admin_panel_model->get_table_data('tbl_payment_option', '');
		$data['pay_options'] = $pay_option;

		$plans = $this->admin_panel_model->planTypeDetails();

		$data['plan_list'] = $plans;

		if ($this->uri->segment('3') != "") {
			$id = $this->uri->segment('3');
			$codition1 = array('resellerID'=>$id);
			$data['setting']  =  $this->general_model->get_row_data('Config_merchant_portal', $codition1);
		}else{
			$data['setting'] = [];
		}
		

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_reseller', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function disable_reseller()
	{

		$id = $this->czsecurity->xssCleanPostInput('resellerID');
		$rnum =  $this->admin_panel_model->get_num_rows('tbl_merchant_data', array('resellerID' => $id, 'isDelete' => '0', 'isEnable' => '1'));

		if ($id != '' &&  $rnum == 0) {
			$res = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '1'));
			if (!empty($res)) {

				$ins_data = array('resellerID' => $id, 'isEnable' => '0');

				$this->admin_panel_model->update_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '1'), $ins_data);
				$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Disabled</div>');
				redirect(base_url('Admin_panel/reseller_list'));
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>Something Is Wrong</div>');
				redirect(base_url('Admin_panel/reseller_list'));
			}
		} else {
			$this->session->set_flashdata('error', '<div class="alert alert-danger"> <strong>Error: </strong> Cannot Disable Reseller, Please Reassign Merchants </div>');
			redirect(base_url('Admin_panel/reseller_list'));
		}
	}



	public function delete_reseller()
	{

		$id = $this->czsecurity->xssCleanPostInput('resellerID');

		$res = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '0'));
		if (!empty($res)) {

			$ins_data = array('resellerID' => $id, 'isDelete' => '1');

			$this->admin_panel_model->update_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '0'), $ins_data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Deleted </div>');
			redirect(base_url('Admin_panel/disable_list'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>Something Is Wrong </div>');
			redirect(base_url('Admin_panel/disable_list'));
		}
	}



	public function enable_reseller()
	{

		$id = $this->czsecurity->xssCleanPostInput('resellerID');

		$res = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '0'));
		if (!empty($res)) {

			$ins_data = array('resellerID' => $id, 'isEnable' => '1');

			$this->admin_panel_model->update_row_data('tbl_reseller', array('resellerID' => $id, 'isEnable' => '0'), $ins_data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Enabled </div>');
			redirect(base_url('Admin_panel/disable_list'));
		} else {
			
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error: </strong>Something Is Wrong </div>');
			redirect(base_url('Admin_panel/disable_list'));
		}
	}




	public function check_exist_email()
	{


		$resellerEmail  = $this->czsecurity->xssCleanPostInput('resellerEmail');
		$condition     = array('resellerEmail' => $resellerEmail);

		if ($this->admin_panel_model->get_num_rows('tbl_reseller', $condition) == 0) {

			$res = array('status' => 'true');
			echo json_encode($res);
		} else {

			$error = array('resellerEmail' => 'Email already exist', 'status' => 'false');
			echo json_encode($error);
		}
		die;
	}

	/***************************** Merchant management******************************************************/

	public function search_merchant()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		$key = $this->czsecurity->xssCleanPostInput('search_data');
		$merchant = $this->list_model->get_data_admin_merchant($key);
		$data['merchant_list'] = $merchant;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function merchant_list()
	{
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		$merchant = $this->list_model->get_data_admin_merchant();
		$data['merchant_list'] = $merchant;
		$data['gateways'] = $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function get_reseller_plan_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('reID');
		$plan1 = array();

		$data = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id));

		$plans = explode(',', $data['plan_id']);
		foreach ($plans as $key => $plan) {
			$pdata = $this->admin_panel_model->get_row_data('plans', array('plan_id' => $plan));
			if(!$pdata){
				unset($plan1[$key]);
				continue;
			}
			$plan1[$key]['plan_id']   = $pdata['plan_id'];
			$plan1[$key]['plan_name'] = $pdata['plan_name'];
		}

		echo json_encode($plan1);
	}

	public function create_merchant11()
	{
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
				$this->form_validation->set_rules('merchantEmail', 'Email', 'required|valid_email');


				$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}
			if ($this->form_validation->run() == true) {

				$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$CompanyName = $this->czsecurity->xssCleanPostInput('companyName');
				$MerchantContact = $this->czsecurity->xssCleanPostInput('merchantContact');
				$MerchantAltNum   = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');
				$MerchantAddress1 = $this->czsecurity->xssCleanPostInput('merchantAddress1');
				$MerchantAddress2 = $this->czsecurity->xssCleanPostInput('merchantAddress2');
				$MerchantCity = $this->czsecurity->xssCleanPostInput('merchantCity');
				$MerchantState = $this->czsecurity->xssCleanPostInput('merchantState');
				$MerchantZipCode = $this->czsecurity->xssCleanPostInput('merchantZipCode');
				$MerchantCountry = $this->czsecurity->xssCleanPostInput('merchantCountry');



				$input_data['companyName']	   = $CompanyName;
				$input_data['firstName']	   = $FirstName;
				$input_data['lastName']	       = $LastName;
				$input_data['merchantContact']     = $MerchantContact;
				$input_data['merchantAlternateContact']	   = $MerchantAltNum;
				$input_data['weburl']	       = 	$weburl;
				$input_data['merchantAddress1']     = $MerchantAddress1;
				$input_data['merchantAddress2']	       = $MerchantAddress2;
				$input_data['merchantCity']     = $MerchantCity;
				$input_data['merchantState']	   = $MerchantState;
				$input_data['merchantZipCode']	       = $MerchantZipCode;
				$input_data['merchantCountry']     = $MerchantCountry;
				$input_data['resellerID']	   = $this->czsecurity->xssCleanPostInput('reseller');
				$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');


				if ($this->czsecurity->xssCleanPostInput('Plans')) {
					$input_data['plan_id']   =       $this->czsecurity->xssCleanPostInput('Plans');
				} else {
					$input_data['plan_id']    = 0;
				}
				
				if ($this->czsecurity->xssCleanPostInput('merchID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('merchID');
					
					$chk_condition = array('merchID' => $id);
					
					$this->admin_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data);

					$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Updated </div>');
					redirect(base_url('Admin_panel/merchant_list'), 'refresh');
				} else {

					$results = $this->admin_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));

					if ($results) {
						
						$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong> Email Already Exitsts </div>');
						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					} else {
						$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
						$input_data['loginCode']	   = $code;

						$input_data['isEnable']	   = '1';

						$input_data['merchantEmail']   = $email;
						$input_data['merchantPassword'] = md5($this->czsecurity->xssCleanPostInput('merchantPassword'));
						$insert = $this->admin_panel_model->insert_row('tbl_merchant_data', $input_data);
						$merchantID = $insert;


						if ($this->admin_panel_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];

							$templatedatas =  $this->admin_panel_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->admin_panel_model->insert_row('tbl_email_template', $insert_data);
							}
						}


						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->admin_panel_model->insert_inv_number($merchantID);
						}



						if ($this->admin_panel_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->admin_panel_model->insert_payterm($merchantID);
						}



						$this->load->library('email');

						$subject = 'ChargeZoom, Verification mail';
						$this->email->from('support@chargezoom.com');
						$this->email->to($email);
						$this->email->subject($subject);
						$this->email->set_mailtype("html");
						$base_url = "http://payportal.com/login/enable_merchant/" . $code;

						$message = "<p>Dear $FirstName $LastName,<br><br>Thanks for registering with ChargeZoom. Please verify your email id by clicking following verification link.</p><br> <p><a href=" . $base_url . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
						$this->email->message($message);

						if ($this->email->send()) {
							$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Created </div>');
						} else {
							$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Email was not sent, please contact your administrator.' . $email . ' </div>');
						}	
						$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Created </div>');
						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Validation errors exists </div>');
			}
		}
		if ($this->uri->segment('3')) {
			$merchID  			  = $this->uri->segment('3');
			$con                = array('merchID' => $merchID);
			$data['merchant']   = $this->admin_panel_model->get_row_data('tbl_merchant_data', $con);
			$rID    			  =  $data['merchant']['resellerID'];
			$rdata = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $rID));

			$plans = explode(',', $rdata['plan_id']);
			foreach ($plans as $key => $plan) {
				$pdata = $this->admin_panel_model->get_planfdn_data($plan);
				$plan1[$key]['plan_id']   = $pdata['plan_id'];
				if ($pdata['friendlyname'] != "") {
					$plan1[$key]['plan_name'] = $pdata['friendlyname'];
				} else {
					$plan1[$key]['plan_name'] = $pdata['plan_name'];
				}
			}
			$data['plans']    = $plan1;
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$reseller = $this->admin_panel_model->get_table_data('tbl_reseller', array('isDelete' => 0, 'isEnable' => '1'));
		$data['reseller_list'] = $reseller;
		$gateway	= $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');
		$data['gateways']		= $gateway;
		$plans = $this->admin_panel_model->get_table_data('plans', '');
		$data['plan_list'] = $plans;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_merchant()
	{

		$this->load->model('card_model');
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
				$this->form_validation->set_rules('merchantEmail', 'Email', 'required');


				$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}
			if ($this->czsecurity->xssCleanPostInput('gateway_opt') != "") {
				$this->form_validation->set_rules('gateway_opt', 'Gateway', 'required');
			}
			if ($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {
				if ($this->czsecurity->xssCleanPostInput('payOption') == 1 && $this->czsecurity->xssCleanPostInput('merchID') == "") {
					$this->form_validation->set_rules('cardNumber', 'Card Number', 'required|xss_clean|number|min:13|max:16');
					
				}else if($this->czsecurity->xssCleanPostInput('payOption') == 2 && $this->czsecurity->xssCleanPostInput('merchID') == ""){
					$this->form_validation->set_rules('accountNumber', 'Account Number', 'required|xss_clean|number|min:3|max:12');
					$this->form_validation->set_rules('routNumber', 'Routing Number', 'required|xss_clean|number|min:3|max:12');
					$this->form_validation->set_rules('accountName', 'Account Name', 'required|xss_clean');
				}
			}
			if ($this->form_validation->run() == true) {

				$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$CompanyName = $this->czsecurity->xssCleanPostInput('companyName');
				$MerchantContact = $this->czsecurity->xssCleanPostInput('merchantContact');
				$MerchantAltNum   = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');
				$MerchantAddress1 = $this->czsecurity->xssCleanPostInput('merchantAddress1');
				$MerchantAddress2 = $this->czsecurity->xssCleanPostInput('merchantAddress2');
				$MerchantCity = $this->czsecurity->xssCleanPostInput('merchantCity');
				$MerchantState = $this->czsecurity->xssCleanPostInput('merchantState');
				$MerchantZipCode = $this->czsecurity->xssCleanPostInput('merchantZipCode');
				$MerchantCountry = $this->czsecurity->xssCleanPostInput('merchantCountry');

				$resellerID = $this->czsecurity->xssCleanPostInput('reseller');

				$portal_url = $this->czsecurity->xssCleanPostInput('portal_url');
				$tagline = $this->czsecurity->xssCleanPostInput('tagline');
				$is_free_trial = ($this->czsecurity->xssCleanPostInput('is_free_trial') !== 0)?$this->czsecurity->xssCleanPostInput('is_free_trial'):0;
				$free_trial_day = ($this->czsecurity->xssCleanPostInput('free_trial_day') !== 0)?$this->czsecurity->xssCleanPostInput('free_trial_day'):0;

				$input_data['companyName']	   = $CompanyName;
				$input_data['firstName']	   = $FirstName;
				$input_data['lastName']	       = $LastName;
				$input_data['merchantContact']     = $MerchantContact;
				$input_data['merchantAlternateContact']	   = $MerchantAltNum;
				$input_data['weburl']	       = 	$weburl;
				$input_data['merchantAddress1']     = $MerchantAddress1;
				$input_data['merchantAddress2']	       = $MerchantAddress2;
				$input_data['merchantCity']     = $MerchantCity;
				$input_data['merchantState']	   = $MerchantState;
				$input_data['merchantZipCode']	       = $MerchantZipCode;
				$input_data['merchantCountry']     = $MerchantCountry;
				$input_data['resellerID']	   = $resellerID;
				$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$input_data['merchantEmail']   = $email;
				$input_data['merchantTagline']     = $tagline;

				$picture  = '';

				$statusInsert = 0;


				if (!empty($_FILES['picture']['name'])) {
					$config['upload_path'] = '../uploads/merchant_logo/';
					$config['allowed_types'] = 'jpg|jpeg|png|gif';
					$config['file_name'] = time() . $_FILES['picture']['name'];
					
					//Load upload library and initialize configuration
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if ($this->upload->do_upload('picture')) {
						$uploadData = $this->upload->data();
						$picture = $uploadData['file_name'];
					} 
					
				}
				
				
				if($this->czsecurity->xssCleanPostInput('payOption') != null){
					
					$input_data['payOption'] = ($this->czsecurity->xssCleanPostInput('payOption') != null)?$this->czsecurity->xssCleanPostInput('payOption'):0;
				}
				
				/*Billing address details*/
				$billing_first_name = ($this->czsecurity->xssCleanPostInput('billingfirstName') != null)?$this->czsecurity->xssCleanPostInput('billingfirstName'):null;

	        	$billing_last_name = ($this->czsecurity->xssCleanPostInput('billinglastName')!= null)?$this->czsecurity->xssCleanPostInput('billinglastName'):null;

	        	$billing_phone_number = ($this->czsecurity->xssCleanPostInput('billingContact')!= null)?$this->czsecurity->xssCleanPostInput('billingContact'):null;

	        	$billing_email = ($this->czsecurity->xssCleanPostInput('billingEmailAddress')!= null)?$this->czsecurity->xssCleanPostInput('billingEmailAddress'):null;

	        	$billing_address = ($this->czsecurity->xssCleanPostInput('billingAddress1')!= null)?$this->czsecurity->xssCleanPostInput('billingAddress1'):null;

	        	$billing_state = ($this->czsecurity->xssCleanPostInput('billingState')!= null)?$this->czsecurity->xssCleanPostInput('billingState'):null;

	        	$billing_city = ($this->czsecurity->xssCleanPostInput('billingCity')!= null)?$this->czsecurity->xssCleanPostInput('billingCity'):null;

        		$billing_zipcode = ($this->czsecurity->xssCleanPostInput('billingPostalCode')!= null)?$this->czsecurity->xssCleanPostInput('billingPostalCode'):null;
				$input_merchant_data['billing_first_name']   = $billing_first_name;
				$input_merchant_data['billing_last_name']   = $billing_last_name;
				$input_merchant_data['billing_phone_number']   = $billing_phone_number;
				$input_merchant_data['billing_email']   = $billing_email;
				$input_merchant_data['Billing_Addr1']   = $billing_address;
				$input_merchant_data['Billing_Country']   = null;
				$input_merchant_data['Billing_State']   = $billing_state;
				$input_merchant_data['Billing_City']   = $billing_city;
				$input_merchant_data['Billing_Zipcode']   = $billing_zipcode;
				$input_merchant_data['resellerID']  = $resellerID;
				$inputBillingUpdate = $input_merchant_data;
				if ($this->czsecurity->xssCleanPostInput('Plans')) {
					$plan_id = $this->czsecurity->xssCleanPostInput('Plans');
					$input_data['plan_id']   = $plan_id;
					$updataMain = $this->admin_panel_model->get_row_data('plans',array('plan_id'=>$plan_id));

				} else {
					$input_data['plan_id']    = 0;
				}

				if($this->czsecurity->xssCleanPostInput('payOption') == 1) {
					$card_no = $this->czsecurity->xssCleanPostInput('cardNumber');
					$card_type = $this->general_model->getcardType($card_no);
					$friendlyname = $card_type . ' - ' . substr($card_no, -4);
					$cvv = $this->czsecurity->xssCleanPostInput('cvv');

					$input_merchant_data['MerchantCard'] = $this->card_model->encrypt($card_no);
					$input_merchant_data['CardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
					$input_merchant_data['CardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
					$input_merchant_data['CardCVV'] = $this->card_model->encrypt($cvv);
					$input_merchant_data['CardType']  = $card_type;
					$input_merchant_data['merchantFriendlyName']  = $friendlyname;
					$input_merchant_data['createdAt']  = date('Y-m-d H:i:s');

					$input_merchant_data['accountNumber']  	 = null;
					$input_merchant_data['routeNumber']  		 = null;
					$input_merchant_data['accountName']  		 = null;
					$input_merchant_data['secCodeEntryMethod']  = null;
					$input_merchant_data['accountType']  	     = null;
					$input_merchant_data['accountHolderType']   = null;

				}else if($this->czsecurity->xssCleanPostInput('payOption') == 2) {
					$acc_number   = $this->czsecurity->xssCleanPostInput('accountNumber');
					$route_number = $this->czsecurity->xssCleanPostInput('routNumber');
					$acc_name     = $this->czsecurity->xssCleanPostInput('accountName');
					$secCode      = 'WEB';
					$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
					$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$card_type = 'Checking';

					$friendlyname = $card_type . ' - ' . substr($acc_number, -4);

					$input_merchant_data['accountNumber']  	 = $acc_number;
					$input_merchant_data['routeNumber']  		 = $route_number;
					$input_merchant_data['accountName']  		 = $acc_name;
					$input_merchant_data['secCodeEntryMethod']  = $secCode;
					$input_merchant_data['accountType']  	     = $acct_type;
					$input_merchant_data['accountHolderType']   = $acct_holder_type;

					$input_merchant_data['merchantFriendlyName']  = $friendlyname;
					
					$input_merchant_data['createdAt']  = date('Y-m-d H:i:s');

					$input_merchant_data['MerchantCard'] = null;
					$input_merchant_data['CardMonth'] = null;
					$input_merchant_data['CardYear'] = null;
					$input_merchant_data['CardCVV'] = null;
					$input_merchant_data['CardType']  = $card_type;

				}else{

				}
				

				
				if ($this->czsecurity->xssCleanPostInput('merchID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('merchID');

					/*Check email exits condition on change*/
					$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $id ));
					$current_email = $get_data['merchantEmail'];
					if($current_email != $email){
						$results = $this->admin_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));

						if ($results) {
							
							$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong> Email Already Exitsts </div>');
							redirect(base_url('Admin_panel/merchant_list'), 'refresh');
						} 
					}

					$input_merchant_data['merchantListID']  = $id;
					$inputBillingUpdate['merchantListID']  = $id;

					if (!empty($this->czsecurity->xssCleanPostInput('merchantPassword1'))){
                        $input_data['merchantPasswordNew'] = password_hash(
                            $this->czsecurity->xssCleanPostInput('merchantPassword1'),
                            PASSWORD_BCRYPT
                        );

                        $input_data['password_update_date'] = date('Y-m-d H:i:s');

						$merch_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $id));
					 	$password_update_date = $merch_data['password_update_date'];

						$current = strtotime(date('Y-m-d H:i:s'));
						$password_update_date = strtotime($password_update_date);
						$diff = $current - $password_update_date;
						$hours = round($diff / ( 60 * 60 ) );
					 	
					}
					$chk_condition = array('merchID' => $id);

					if($this->czsecurity->xssCleanPostInput('merchantCardID') > 0){
						$input_data['cardID']  = $this->czsecurity->xssCleanPostInput('merchantCardID');
					}
					$this->admin_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data);

					/*Save password record by user_id */
					if (!empty($this->czsecurity->xssCleanPostInput('merchantPassword1'))){

						#Create Log on Password Update
						$log_session_data = [
							'session' => $this->session->userdata,
							'http_data' => $_SERVER
						];

						$logData = [
							'request_data' => json_encode($this->input->post(null, true), true),
							'session_data' => json_encode( $log_session_data, true),
							'executed_sql' => $this->db->last_query(),
							'log_date'	   => date('Y-m-d H:i:s'),
							'action_interface' => 'Admin - Edit Merchant',
							'header_data' => json_encode(apache_request_headers(), true),
						];
						$this->general_model->insert_row('merchant_password_log', $logData);

						#Log scripts end here

						$pass_data = [];
					}

					$mcard_condition = array('merchantListID'=>$id);

					if($this->czsecurity->xssCleanPostInput('merchantCardID') > 0){
						if($this->czsecurity->xssCleanPostInput('is_card_edit') == 1 && ($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) ) {

							$cardid = $this->card_model->update_merchant_card_data($mcard_condition, $input_merchant_data);
						}else{

							$cardid = $this->card_model->update_merchant_card_data($mcard_condition, $inputBillingUpdate);
						}
						 

					}elseif($this->czsecurity->xssCleanPostInput('merchantCardID') == 0){
						if($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {
							$cardid = $this->card_model->insert_merchant_card_data($input_merchant_data); 

							$input_data1['cardID']  = $cardid;

							$this->admin_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data1);

							if($cardid > 0){
								if(ENVIRONMENT == 'production' && $this->czsecurity->xssCleanPostInput('reseller') == HATCHBUCK_RESELLER_ID){
									/* Start campaign in hatchbuck CRM*/  
				                    $this->load->library('hatchBuckAPI');
				                    
				                    $merchantData = $get_data;
				                    $merchantData['merchant_type'] = 'Admin Updated';        
				                    
				                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                    if($status['statusCode'] == 400){
				                        $resource = $this->hatchbuckapi->createContact($merchantData);
				                        if($resource['contactID'] != '0'){
				                            $contact_id = $resource['contactID'];
				                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                        }
				                    }else{
				                    	
						                $resource = $this->hatchbuckapi->updateContact($input_data);
				                    }
				                    /* End campaign in hatchbuck CRM*/ 
								}
							}
						}else{
							
							if(ENVIRONMENT == 'production' && $this->input->post('reseller') == HATCHBUCK_RESELLER_ID){
								/* Start campaign in hatchbuck CRM*/  
			                    $this->load->library('hatchBuckAPI');
			                    $merchantData = $get_data;
				               
			                    $merchantData['merchant_type'] = 'Admin Create';    	    
			                    $resource = $this->hatchbuckapi->createContact($merchantData,'Prospect: Not Boarded');

			                    /* End campaign in hatchbuck CRM*/ 
							}
						}
						
					}

					$configSettingData = [];
					if ($this->czsecurity->xssCleanPostInput('portal_url') != "") {
						$portalURL = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));
						$configSettingData['customerPortalURL'] = "https://" . $portalURL . '.'. RSDOMAIN . "/";
						$configSettingData['customerPortal'] = 1;
					}

					if ($picture != ""){
						$configSettingData['ProfileImage'] = $picture;
					}

					if ($tagline != ""){
						$configSettingData['customerHelpText'] = $tagline;
					}

					if(!empty($configSettingData)){
						if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $id))) {
							$this->general_model->update_row_data('tbl_config_setting', array('merchantID' => $id), $configSettingData);
						} else {
							$configSettingData['merchantID'] = $id;
							$this->general_model->insert_row('tbl_config_setting', $configSettingData);
						}
					}


					
					$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Updated </div>');
					redirect(base_url('Admin_panel/merchant_list'), 'refresh');
				} else {

					$results = $this->admin_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));

					if ($results) {
						
						$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong> Email Already Exitsts </div>');
						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					} 
					else
					{
						$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
						$input_data['loginCode']	        = $code;
						$input_data['isEnable']	            = '1';
						$input_data['merchantEmail']        = $email;
                        $input_data['merchantPasswordNew']  = password_hash(
                            $this->czsecurity->xssCleanPostInput('merchantPassword'),
                            PASSWORD_BCRYPT
                        );

						$input_data['date_added'] = date('Y-m-d H:i:s');


						$input_data['password_update_date'] = null;


						/*Add trual period if exits*/
						if($is_free_trial){
							$input_data['freeTrial'] = 1;
							$input_data['free_trial_day'] = $free_trial_day;
							$trialDayDate = $free_trial_day - 1;
							$input_data['trial_expiry_date'] = date('Y-m-d', strtotime('+'.$trialDayDate.' days'));
						}else{
							$input_data['freeTrial'] = 0;
							$input_data['free_trial_day'] = 0;
							$input_data['trial_expiry_date'] = null;
						}
						$input_data['date_added'] = date('Y-m-d H:i:s');
						$input_data['merchant_default_timezone'] = 'America/Los_Angeles';

						$insert = $this->admin_panel_model->insert_row('tbl_merchant_data', $input_data);

						$merchantID = $insert;

						/*Save password record by user_id */
						$pass_data = [];
						
						/*Save card and also update card id in merchant table*/
						$chk_condition1 = array('merchID' => $merchantID);
						if($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {
							$input_merchant_data['merchantListID']  = $merchantID;
							$cardid = $this->card_model->insert_merchant_card_data($input_merchant_data); 

							if($cardid){
								$cardid = $cardid;
								if(ENVIRONMENT == 'production' && $this->czsecurity->xssCleanPostInput('reseller') == HATCHBUCK_RESELLER_ID){
									/* Start campaign in hatchbuck CRM*/  
				                    $this->load->library('hatchBuckAPI');
				                    
				                    $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$chk_condition1);
				                    $merchantData['merchant_type'] = 'Admin Create';        
				                    
				                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                    if($status['statusCode'] == 400){
				                        $resource = $this->hatchbuckapi->createContact($merchantData);
				                        if($resource['contactID'] != '0'){
				                            $contact_id = $resource['contactID'];
				                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                        }
				                    }

				                    /* End campaign in hatchbuck CRM*/ 
								}
								


							}else{
								$cardid = 0;
							}
							$this->createInvoiceForMerchant($this->input->post(null, true),$cardid,$merchantID);

							$input_data1['cardID']  = $cardid;
							
							$this->admin_panel_model->update_row_data('tbl_merchant_data', $chk_condition1, $input_data1);
			    		   
						}else{
							$cardid = 0;

							if(ENVIRONMENT == 'production' && $this->czsecurity->xssCleanPostInput('reseller') == HATCHBUCK_RESELLER_ID){
								/* Start campaign in hatchbuck CRM*/  
			                    $this->load->library('hatchBuckAPI');
			                    
			                    $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$chk_condition1);
			                    $merchantData['merchant_type'] = 'Admin Create';    	    
			                    $resource = $this->hatchbuckapi->createContact($merchantData,'Prospect: Not Boarded');

			                    /* End campaign in hatchbuck CRM*/ 
							}
							$this->createInvoiceForMerchant($this->input->post(null, true),$cardid,$merchantID);
						}

						if ($this->admin_panel_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = DEFAULT_FROM_EMAIL; 

							$templatedatas =  $this->admin_panel_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->admin_panel_model->insert_row('tbl_email_template', $insert_data);
								
							}
						}

						/*Set gateway for merchant*/
						$signature = '';
						if ($this->czsecurity->xssCleanPostInput('gateway_opt') != '') {
							$gmID = '';
							$cr_status       =  1;
							$gmID                  = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
							$isSurcharge = $surchargePercentage = 0;
							$ach_status = 0;
							$extra1 = '';

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
								if ($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');

								if ($this->czsecurity->xssCleanPostInput('auth_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
								if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
								$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
								if ($this->czsecurity->xssCleanPostInput('heart_cr_status')){
									$cr_status       =  1;
								}
								else{
									$cr_status       =  0;
								}

								if ($this->czsecurity->xssCleanPostInput('heart_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
								$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '9') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
								if ($this->czsecurity->xssCleanPostInput('cz_cr_status')){
									$cr_status       =  1;
								}else{
									$cr_status       =  0;
								}

								if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '11') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
								$nmipassword     = '';
								if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '10') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
								if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
									
								if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
									$isSurcharge = 1;
								}
								else{
									$isSurcharge = 0;
								}
								$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '12') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
								$gmID     = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
								if ($this->czsecurity->xssCleanPostInput('TSYS_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('TSYS_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '13') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
								$nmipassword     = '';
								if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '14') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('cardpointeUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('cardpointePassword');
								$signature       = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
								if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->input->post('gateway_opt') == '16') {
								$nmiuser         = $this->input->post('EPXCustNBR',true);
								$nmipassword     = $this->input->post('EPXMerchNBR',true);
								$signature     = $this->input->post('EPXDBANBR',true);
								$extra1   = $this->input->post('EPXterminal',true);
								if ($this->input->post('EPX_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->input->post('EPX_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->input->post('gateway_opt') == '15') {
								$nmiuser         = $this->input->post('payarcUser');
								$nmipassword     = '';
								$cr_status       =  1;
								$ach_status       =  0;
							} 
							if ($this->input->post('gateway_opt') == '17') {
								$nmiuser         = $this->input->post('maverickAccessToken', true);
								$nmipassword     = $this->input->post('maverickTerminalId', true);
								if ($this->input->post('maverick_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->input->post('maverick_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							} 
							$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');

							$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
							$frname          = $this->czsecurity->xssCleanPostInput('frname');
							
							

							$insert_data    = array(
								'gatewayUsername' => $nmiuser,
								'gatewayPassword' => $nmipassword,
								'gatewayMerchantID' => $gmID,
								'gatewaySignature' => $signature,
								'extra_field_1' => $extra1,
								'gatewayType' => $gatetype,
								'merchantID' => $merchantID,
								'gatewayFriendlyName' => $frname,
								'creditCard'       => $cr_status,
								'echeckStatus'      => $ach_status,
								'set_as_default'    => '1',
								'isSurcharge' => $isSurcharge,
								'surchargePercentage' => $surchargePercentage,
							);

							$this->general_model->insert_row('tbl_merchant_gateway', $insert_data);
						}

						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->admin_panel_model->insert_inv_number($merchantID);
						}



						if ($this->admin_panel_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->admin_panel_model->insert_payterm($merchantID);
						}
						/* Portal data saved */
						if ($this->czsecurity->xssCleanPostInput('portal_url') != "") {
							$portal_url = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));
							$url = "https://" . $portal_url . '.'. RSDOMAIN . "/";
			
			
							if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $merchantID))) {
								$update_data = array('customerPortalURL' => $url, 'customerHelpText' => $tagline, 'portalprefix' => $portal_url);
			
								if ($picture != "")
									$update_data['ProfileImage'] = $picture;

								$this->general_model->update_row_data('tbl_config_setting', array('merchantID' => $merchantID), $update_data);
							} else {
								$insert_data = array(
									'customerPortalURL' => $url, 'customerHelpText' => $tagline,
									'merchantID' => $merchantID, 'portalprefix' => $portal_url,
									'customerPortal' => 1
								);
								if ($picture != "")
									$insert_data['ProfileImage'] = $picture;
								$this->general_model->insert_row('tbl_config_setting', $insert_data);
							}
						}

						$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Created </div>');
						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Validation errors exists </div>');
			}
		}
		if ($this->uri->segment('3')) {
			$merchID  			  = $this->uri->segment('3');
			$con                = array('merchID' => $merchID);
			$data['merchant']   = $this->admin_panel_model->get_row_data('tbl_merchant_data', $con);
			
			$rID    			  =  $data['merchant']['resellerID'];
			$rdata = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $rID));

			$plans = explode(',', $rdata['plan_id']);
			foreach ($plans as $key => $plan) {
				$updataMain = $this->admin_panel_model->get_row_data('plans',array('plan_id'=>$plan));
				$pdata = $this->admin_panel_model->get_planfdn_data($plan);
				$plan1[$key]['plan_id']   = $updataMain['plan_id'];
				$plan1[$key]['plan_name'] = $updataMain['plan_name'];
				
			}

			$data['plans']    = $plan1;

			$data['merchant_card_data'] = $this->card_model->get_merch_card_data($data['merchant']['cardID']);

			$condition	= array('merchantID' => $merchID);
			$data['get_data'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

			$config     = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchID));
			
			if (!empty($config)) {

				$data['merchant']['portalprefix']      = $config['portalprefix'];
				$data['merchant']['ProfileImage']      = $config['ProfileImage'];
			} else {
				$data['merchant']['portalprefix']      = '';
				$data['merchant']['ProfileImage']      = '';
			}
		}else{
			$data['merchant_card_data'] = [];
		}
		$data['monthData'] 	  = $this->admin_panel_model->get_table_data('tbl_month', '');

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$reseller = $this->admin_panel_model->get_table_data('tbl_reseller', array('isDelete' => 0, 'isEnable' => '1'));
		$data['reseller_list'] = $reseller;
		$gateway	= $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');
		$data['gateways']		= $gateway;

		$pay_option         = $this->admin_panel_model->get_table_data('tbl_payment_option', '');
		$data['pay_options'] = $pay_option;

		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');

		$plans = $this->admin_panel_model->get_table_data('plans', '');
		$data['plan_list'] = $plans;
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_merchant_new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function create_merchant_old()
	{
		$data['login_info'] = $this->session->userdata('admin_logged_in');
		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
				$this->form_validation->set_rules('merchantEmail', 'Email', 'required|valid_email');


				$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}
			if ($this->form_validation->run() == true) {

				$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$CompanyName = $this->czsecurity->xssCleanPostInput('companyName');
				$MerchantContact = $this->czsecurity->xssCleanPostInput('merchantContact');
				$MerchantAltNum   = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');
				$MerchantAddress1 = $this->czsecurity->xssCleanPostInput('merchantAddress1');
				$MerchantAddress2 = $this->czsecurity->xssCleanPostInput('merchantAddress2');
				$MerchantCity = $this->czsecurity->xssCleanPostInput('merchantCity');
				$MerchantState = $this->czsecurity->xssCleanPostInput('merchantState');
				$MerchantZipCode = $this->czsecurity->xssCleanPostInput('merchantZipCode');
				$MerchantCountry = $this->czsecurity->xssCleanPostInput('merchantCountry');



				$input_data['companyName']	   = $CompanyName;
				$input_data['firstName']	   = $FirstName;
				$input_data['lastName']	       = $LastName;
				$input_data['merchantContact']     = $MerchantContact;
				$input_data['merchantAlternateContact']	   = $MerchantAltNum;
				$input_data['weburl']	       = 	$weburl;
				$input_data['merchantAddress1']     = $MerchantAddress1;
				$input_data['merchantAddress2']	       = $MerchantAddress2;
				$input_data['merchantCity']     = $MerchantCity;
				$input_data['merchantState']	   = $MerchantState;
				$input_data['merchantZipCode']	       = $MerchantZipCode;
				$input_data['merchantCountry']     = $MerchantCountry;

				$input_data['resellerID']	   = $this->czsecurity->xssCleanPostInput('reseller');
				$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');


				if ($this->czsecurity->xssCleanPostInput('Plans')) {
					$input_data['plan_id']   =       $this->czsecurity->xssCleanPostInput('Plans');
				} else {
					$input_data['plan_id']    = 0;
				}
				if ($this->czsecurity->xssCleanPostInput('merchID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('merchID');
					$chk_condition = array('merchID' => $id);
					$this->admin_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data);

					$this->session->set_flashdata('message', '<strong> Successfully Updated</strong>');
					redirect(base_url('Admin_panel/merchant_list'), 'refresh');
				} else {

					$results = $this->admin_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));
					if ($results) {
						$this->session->set_flashdata('message', '<strong>Error</strong> Email Already Exitsts.');
						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					} else {
						$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
						$input_data['loginCode']	   = $code;

						$input_data['isEnable']	   = '1';

						$input_data['merchantEmail']   = $email;
						$input_data['merchantPassword'] = md5($this->czsecurity->xssCleanPostInput('merchantPassword'));
						$insert = $this->admin_panel_model->insert_row('tbl_merchant_data', $input_data);

						$merchantID = $insert;


						if ($this->admin_panel_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];

							$templatedatas =  $this->admin_panel_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->admin_panel_model->insert_row('tbl_email_template', $insert_data);
							}
						}


						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->admin_panel_model->insert_inv_number($merchantID);
						}



						if ($this->admin_panel_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->admin_panel_model->insert_payterm($merchantID);
						}
						$this->session->set_flashdata('message', '<strong> Successfully Created</strong>');
						$this->load->library('email');

						$subject = 'ChargeZoom, Verification mail';
						$this->email->from('support@chargezoom.com');
						$this->email->to($email);
						$this->email->subject($subject);
						$this->email->set_mailtype("html");
						$base_url = "http://payportal.com/login/enable_merchant/" . $code;

						$message = "<p>Dear $FirstName $LastName,<br><br>Thanks for registering with ChargeZoom. Please verify your email id by clicking following verification link.</p><br> <p><a href=" . $base_url . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
						$this->email->message($message);

						if ($this->email->send()) {
							$this->session->set_flashdata('message', '<strong> Successfully Created</strong>');
						} else {
							$this->session->set_flashdata('message', '<strong>Error:</strong> Email was not sent, please contact your administrator.' . $email);
						}

						redirect(base_url('Admin_panel/merchant_list'), 'refresh');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<strong>Error:</strong> Validation errors exists.');
			}
		}
		if ($this->uri->segment('3')) {
			$merchID  			  = $this->uri->segment('3');
			$con                = array('merchID' => $merchID);
			$data['merchant']   = $this->admin_panel_model->get_row_data('tbl_merchant_data', $con);
			$rID    			  =  $data['merchant']['resellerID'];
			$rdata = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $rID));

			$plans = explode(',', $rdata['plan_id']);
			foreach ($plans as $key => $plan) {
				$pdata = $this->admin_panel_model->get_planfdn_data($plan);
				$plan1[$key]['plan_id']   = $pdata['plan_id'];
				if ($pdata['friendlyname'] != "") {
					$plan1[$key]['plan_name'] = $pdata['friendlyname'];
				} else {
					$plan1[$key]['plan_name'] = $pdata['plan_name'];
				}
			}
			$data['plans']    = $plan1;
		}

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();


		$reseller = $this->admin_panel_model->get_table_data('tbl_reseller', array('isDelete' => '0'));
		$data['reseller_list'] = $reseller;

		$gateway	= $this->admin_panel_model->get_table_data('tbl_admin_gateway', '');
		$data['gateways']		= $gateway;

		$plans = $this->admin_panel_model->get_table_data('plans', '');

		$data['plan_list'] = $plans;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/add_merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function delete_merchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('merchID');
		$res = $this->admin_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id, 'isDelete' => '0'));
		if (!empty($res)) {

			$ins_data = array('merchID' => $id, 'isDelete' => '1');

			$this->admin_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id, 'isDelete' => '0'), $ins_data);
			$this->session->set_flashdata('message', '<div class="alert alert-success"> Successfully Deleted </div>');
			redirect(base_url('Admin_panel/merchant_list'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:</strong> Something Is Wrong... </div>');
			redirect(base_url('Admin_panel/merchant_list'));
		}
	}
	/*********************************** Merchant Gateway ******************************************/

	public function merchant_gateway()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		$data['all_gateway']  = $this->admin_panel_model->get_table_data('tbl_master_gateway', '');
		$data['gateways'] = $this->admin_panel_model->get_gateway_data();

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/merchant_gateway', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}


	public function create_gateway()
	{

		$signature = '';
		if (!empty($this->input->post(null, true))) {


			$data['login_info'] 	= $this->session->userdata('admin_logged_in');
			$user_id 				= $data['login_info']['adminID'];
			$condition				= array('merchantID' => $user_id);

			if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
			}

			if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
			}
			if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
			}

			if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
			}

			$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');

			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
			$frname           = $this->czsecurity->xssCleanPostInput('frname');
			$gmID                  = $this->czsecurity->xssCleanPostInput('gatewayAdminID');

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayAdminID' => $gmID,
				'gatewaySignature' => $signature,
				'gatewayType' => $gatetype,
				'adminID' => $user_id,
				'gatewayFriendlyName' => $frname

			);

			if ($this->admin_panel_model->insert_row('tbl_admin_gateway', $insert_data)) {
				$this->session->set_flashdata('message', '<strong> Successfully Created</strong>');
			} else {

				$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong...!');
			}

			redirect(base_url('Admin_panel/merchant_gateway'));
		}
	}

	public function update_gateway()
	{

		if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {

			$signature = "";
			
			$id = $this->czsecurity->xssCleanPostInput('gatewayEditID');
			$chk_condition = array('gatewayID' => $id);

			if ($this->czsecurity->xssCleanPostInput('gateway') == 'NMI') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword1');
			} else if ($this->czsecurity->xssCleanPostInput('gateway') == 'Authorize.net') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey1');
			} else if ($this->czsecurity->xssCleanPostInput('gateway') == 'Paytrace') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword1');
			} else if ($this->czsecurity->xssCleanPostInput('gateway') == 'Paypal') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword1');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature1');
			} else if ($this->czsecurity->xssCleanPostInput('gateway') == 'Stripe') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword1');
			}


			if ($this->czsecurity->xssCleanPostInput('gateway') == 'USAePay') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin1');
			}
			if ($this->czsecurity->xssCleanPostInput('gateway') == 'Heartland') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
			}

			if ($this->czsecurity->xssCleanPostInput('gateway') == 'Cybersource') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey1');
			}

			$gatetype         = $this->czsecurity->xssCleanPostInput('gateway');
			$frname           = $this->czsecurity->xssCleanPostInput('fname');
			$gmID                  = $this->czsecurity->xssCleanPostInput('aid');

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayAdminID' => $gmID,
				'gatewayFriendlyName' => $frname,
				'gatewaySignature' => $signature
			);
			if ($this->admin_panel_model->update_row_data('tbl_admin_gateway', $chk_condition, $insert_data)) {
				$this->session->set_flashdata('message', '<strong>Successfully Updated</strong>');
			} else {

				$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
			}

			redirect(base_url('Admin_panel/merchant_gateway'));
		}
	}



	public function get_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->admin_panel_model->get_row_data('tbl_admin_gateway', $val);
		if ($data['gatewayType'] == '1') {
			$data['gateway'] = "NMI";
		}
		if ($data['gatewayType'] == '2') {
			$data['gateway'] = "Authorize.net";
		}
		if ($data['gatewayType'] == '3') {
			$data['gateway'] = "Paytrace";
		}
		if ($data['gatewayType'] == '4') {
			$data['gateway'] = "Paypal";
		}
		if ($data['gatewayType'] == '5') {
			$data['gateway'] = "Stripe";
		}
		if ($data['gatewayType'] == '6') {
			$data['gateway'] = "USAePay";
		}
		if ($data['gatewayType'] == '7') {
			$data['gateway'] = "Heartland";
		}
		if ($data['gatewayType'] == '8') {
			$data['gateway'] = "Cybersource";
		}
		echo json_encode($data);
	}




	public function update_gatewayes()
	{

		if (!empty($this->input->post(null, true))) {
			if ($this->session->userdata('logged_in')) {
				$data['login_info'] 	= $this->session->userdata('logged_in');

				$user_id 				= $data['login_info']['merchID'];
			}
			$sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));
			$old_gateway =  $this->czsecurity->xssCleanPostInput('gateway_old');
			$new_gateway =  $this->czsecurity->xssCleanPostInput('gateway_new');

			$condition	  = array('gatewayID' => $old_gateway);
			$num_rows    = $this->general_model->get_num_rows('tbl_merchant_data', $condition);

			if ($num_rows > 0) {
				$update_data = array('gatewayID' => $new_gateway);
				if (!empty($sub_ID)) {
					$sss = $this->db->query("update tbl_merchant_data set gatewayID='" . $new_gateway . "' where merchID IN ($sub_ID) ");
					$this->session->set_flashdata('message', '<strong> Success:</strong> Subscription gateway have changed. ');
				} else {
					$this->session->set_flashdata('message', '<strong>Error:</strong> In update process.');
				}
			} else {
				$this->session->set_flashdata('message', ' <strong>Error:</strong> No data selected.');
			}
		}
		redirect(base_url('Admin_panel/merchant_list'));
	}


	public function set_gateway_default()
	{

		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {
			$id = $this->czsecurity->xssCleanPostInput('gatewayid');
			$val = array(
				'gatewayID' => $id,
			);

			$update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
			$this->general_model->update_row_data('tbl_admin_gateway', array(), $update_data1);

			$update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

			$this->general_model->update_row_data('tbl_admin_gateway', $val, $update_data);

			$this->session->set_flashdata('message', '<strong>Success</strong>');
		} else {

			$this->session->set_flashdata('error', '<strong>Error</strong> Invalid Request');
		}
		redirect(base_url('Admin_panel/merchant_gateway'));
	}


	public function set_gateway_res_default()
	{

		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->czsecurity->xssCleanPostInput('gatewayid'))) {
			$id = $this->czsecurity->xssCleanPostInput('gatewayid');
			$val = array(
				'gatewayID' => $id,
			);

			$update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
			$this->general_model->update_row_data('tbl_reseller_gateway', array(), $update_data1);

			$update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));

			$this->general_model->update_row_data('tbl_reseller_gateway', $val, $update_data);

			$this->session->set_flashdata('message', '<strong>Success</strong>');
		} else {

			$this->session->set_flashdata('error', '<strong>Error</strong> Invalid Request');
		}
		redirect(base_url('Admin_panel/reseller_gateway'));
	}

	public function reseller_gateway()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		$ress  = $this->admin_panel_model->get_table_data('tbl_reseller', array('isDelete' => 0, 'isEnable' => '1'));
		$reseller = array();
		if (!empty($ress)) {
			foreach ($ress as $res) {
				$rID = $res['resellerID'];
				$ds = $this->admin_panel_model->get_table_data('tbl_reseller_gateway', array('resellerID' => $rID));
				if (empty($ds)) {
					$reseller[] = $res;
				}
			}
		}
		$data['resellers'] = $reseller;
		$data['gateways'] = $this->admin_panel_model->get_res_getway_data();


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/reseller_gateway', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function create_reseller_gateway()
	{

		$signature = '';
		if (!empty($this->input->post(null, true))) {


			$data['login_info'] 	= $this->session->userdata('admin_logged_in');
			$user_id 				= $data['login_info']['adminID'];
			$condition				= array('merchantID' => $user_id);
			$resellerID             = $this->czsecurity->xssCleanPostInput('resellerID');
			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');

			if ($gatetype == '1' || $gatetype == '9') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '2') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
				if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == '9') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
				if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} 

			$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');

			$frname           = $this->czsecurity->xssCleanPostInput('frname');
			$gmID                  = $this->czsecurity->xssCleanPostInput('gatewayAdminID');

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayAdminID' => $gmID,
				'gatewaySignature' => $signature,
				'gatewayType' => $gatetype,
				'resellerID' => $resellerID,
				'gatewayFriendlyName' => $frname,
				'echeckStatus' => $ach_status

			);

			if ($this->admin_panel_model->insert_row('tbl_reseller_gateway', $insert_data)) {
				$this->session->set_flashdata('message', '<strong> Successfully Created</strong>');
			} else {

				$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong...!');
			}

			redirect(base_url('Admin_panel/reseller_gateway'));
		}

		redirect(base_url('Admin_panel/reseller_gateway'));
	}




	public function get_res_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->admin_panel_model->get_row_data('tbl_reseller_gateway', $val);
		if ($data['gatewayType'] == '1') {
			$data['gateway'] = "NMI";
		}
		if ($data['gatewayType'] == '2') {
			$data['gateway'] = "Authorize.net";
		}
		if ($data['gatewayType'] == '3') {
			$data['gateway'] = "Paytrace";
		}
		if ($data['gatewayType'] == '4') {
			$data['gateway'] = "Paypal";
		}
		if ($data['gatewayType'] == '5') {
			$data['gateway'] = "Stripe";
		}

		echo json_encode($data);
	}

	//----------- TO update the gateway  --------------//

	public function update_reseller_gateway()
	{
		if ($this->czsecurity->xssCleanPostInput('gatewayEditID') != "") {

			$signature = "";

			$id = $this->czsecurity->xssCleanPostInput('gatewayEditID');
			$chk_condition = array('gatewayID' => $id);
			$gatetype         = $this->czsecurity->xssCleanPostInput('gateway');

			if ($gatetype == 'NMI' || $gatetype == 'Chargezoom') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword1');
				if ($this->czsecurity->xssCleanPostInput('nmi_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'Authorize.net') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey1');
				if ($this->czsecurity->xssCleanPostInput('auth_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($gatetype == 'Paytrace') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword1');
			} else if ($gatetype == 'Paypal') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword1');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature1');
			} else if ($gatetype == 'Stripe') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword1');
			} else if ($gatetype == 'Chargezoom') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser1');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword1');
				if ($this->czsecurity->xssCleanPostInput('cz_ach_status1'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}

			$frname           = $this->czsecurity->xssCleanPostInput('fname');
			$gmID                  = $this->czsecurity->xssCleanPostInput('aid');

			$insert_data    = array(
				'gatewayUsername' => $nmiuser,
				'gatewayPassword' => $nmipassword,
				'gatewayAdminID' => $gmID,
				'gatewayFriendlyName' => $frname,
				'gatewaySignature' => $signature,
				'echeckStatus' => $ach_status
			);
			if ($this->admin_panel_model->update_row_data('tbl_reseller_gateway', $chk_condition, $insert_data)) {
				$this->session->set_flashdata('message', '<trong>Successfully Updated</strong>');
			} else {

				$this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');
			}

			redirect(base_url('Admin_panel/reseller_gateway'));
		}
	}





	public function delete_plan()
	{

		if (!empty($this->czsecurity->xssCleanPostInput('plan_id'))) {
			$id = $this->czsecurity->xssCleanPostInput('plan_id');
			$condition	  = array('plan_id' => $id);
			$num_rows    = $this->admin_panel_model->get_num_rows('tbl_merchant_data', $condition);

			if ($num_rows == 0) {
				$condition =  array('plan_id' => $id);
				$del      = $this->admin_panel_model->deactive_plan('plans', $condition);
				if ($del) {
					$this->session->set_flashdata('message', '<strong> Successfully Deleted</strong>');
				} else {
					$this->session->set_flashdata('error', '<strong>Error: </strong>Something Is Wrong...');
				}
			} else {
				$this->session->set_flashdata('error', '<strong>Error:</strong>This Plan is assigned to merchants');
			}
		} else {
			$this->session->set_flashdata('error', '<strong>Error:</strong> No data selected.');
		}
		redirect(base_url('Admin_panel/admin_plans'));
	}
	public function active_plan()
	{

		if (!empty($this->czsecurity->xssCleanPostInput('plan_id'))) {
			$id = $this->czsecurity->xssCleanPostInput('plan_id');
			$condition	  = array('plan_id' => $id);
			$num_rows    = $this->admin_panel_model->get_num_rows('tbl_merchant_data', $condition);

			if ($num_rows == 0) {
				$condition =  array('plan_id' => $id);
				$del      = $this->admin_panel_model->active_plan('plans', $condition);
				if ($del) {
					$this->session->set_flashdata('message', '<strong> Successfully Activated</strong>');
				} else {
					$this->session->set_flashdata('error', '<strong>Error: </strong>Something Is Wrong...');
				}
			} else {
				$this->session->set_flashdata('error', '<strong>Error:</strong>This Plan is assigned to merchants');
			}
		} else {
			$this->session->set_flashdata('error', '<strong>Error:</strong> No data selected.');
		}
		redirect(base_url('Admin_panel/admin_plans'));
	}
	public function update_plans()
	{

		if (!empty($this->input->post(null, true))) {
			$sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));

			$old_gateway =  $this->czsecurity->xssCleanPostInput('plan_old');
			$new_gateway =  $this->czsecurity->xssCleanPostInput('plan_new');

			$condition	  = array('plan_id' => $old_gateway);
			$num_rows    = $this->admin_panel_model->get_num_rows('tbl_merchant_data', $condition);

			if ($num_rows > 0) {
				$update_data = array('plan_id' => $new_gateway);

				$sss = $this->db->query("update tbl_merchant_data set plan_id='" . $new_gateway . "' where merchID IN ($sub_ID) ");

				if ($sss) {

					$this->session->set_flashdata('message', '<strong> Success:</strong> Merchant plan have changed. ');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process.');
				}
			} else {
				$this->session->set_flashdata('message', ' <div class="alert alert-danger"><strong>Error:</strong> No data selected.</div>');
			}
		}
		redirect(base_url('Admin_panel/admin_plans'));
	}


	//------------- To edit gateway ------------------//

	public function get_gateway_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gateway_id');

		$val = array(
			'gatewayID' => $id

		);

		$datas = $this->admin_panel_model->get_table_data('tbl_merchant_data', $val);

?>
		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Merchant</strong></th>
					<th class="text-center col-md-6"><strong>Select</strong></th>
				</tr>
				<?php
				if (!empty($datas)) {
					foreach ($datas as $c_data) {

				?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['companyName']; ?> </td>

							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['merchID']; ?>" /> </td>
						</tr>


					<?php     }
				} else { ?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Records Found"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php } ?>

			</tbody>
		</table>


	<?php die;
	}



	public function reseller_merchant_chart()
	{

		$get_result = $this->admin_panel_model->chart_get_merchant();
		$data['chart']        = 	$get_result;

		$get_result1 = $this->admin_panel_model->get_total_processing_volume();
		$data['merch_list']  = $get_result1['merchant'];
		$data['res_list']    =  $get_result1['reseller'];
		$data['plan_ratio'] = $get_result1['plan_ratio'];
		echo json_encode($data);
		die;
	}

	/**************Delete credit********************/

	public function delete_gateway()
	{
		if ($this->czsecurity->xssCleanPostInput('gatewaydelID') != "") {


			$id = $this->czsecurity->xssCleanPostInput('gatewaydelID');

			$condition =  array('gatewayID' => $id);
			$del      = $this->admin_panel_model->delete_row_data('tbl_admin_gateway', $condition);
			if ($del) {
				$this->session->set_flashdata('message', '<trong>Successfully Deleted</strong>');
				redirect(base_url('Admin_panel/merchant_gateway'));
			} else {
				$this->session->set_flashdata('message', '<strong>Error</strong> Something Is Wrong.');
				redirect(base_url('Admin_panel/merchant_gateway'));
			}
		}
		$this->session->set_flashdata('message', '<strong>Error</strong> Something Is Wrong.');
		redirect(base_url('Admin_panel/merchant_gateway'));
	}

	/************************************ Reseller Reporting **************************************************/

	public function reseller_reporting()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/reseller_reporting', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}





	public function merchant_reporting()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/merchant_reporting', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function get_merchant_plan_id()
	{


		if (!empty($this->czsecurity->xssCleanPostInput('plan_id'))) {
			$id = $this->czsecurity->xssCleanPostInput('plan_id');

			$val = array(
				'gatewayID' => $id

			);

			$datas = $this->admin_panel_model->get_merchant_plan_data($id);
			$tab = '<table class="table table-bordered table-striped table-vcenter">' . '<thead>' . '' .
				'<th class="text-left col-md-4"> <strong>Reseller Name</strong></th>
					<th class="text-left col-md-4"> <strong>Merchant</strong></th>
					<th class="text-center col-md-4"><strong>Select </strong></th>
			  </thead>	
			  <tbody>';
			if (!empty($datas)) {

				foreach ($datas as $c_data) {

					$tab .= '<tr>  
        				<td class="text-left visible-lg  col-md-4">' . $c_data['resellerCompanyName'] . '</td>
        				<td class="text-left visible-lg  col-md-4">' . $c_data['companyName'] . '</td>
        			    <td class="text-center visible-lg col-md-4"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="' . $c_data['merchID'] . '" /> </td>
        			</tr>';
				}
			} else {

				$tab .= '<tr>  
        			<td class="visible-lg col-md-12" colspan="3">No Records Found</td>
        			</tr>';
			}

			$tab .= '</tbody>
        </table>';
			$data['data'] = $tab;
			echo json_encode($data);
			die;
		} else {
			return false;
		}
	}




	public function reasign_marchent()
	{

		if (!empty($this->input->post(null, true))) {

			$sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));
			$reseller_old =  $this->czsecurity->xssCleanPostInput('reseller_old');
			$reseller_new =  $this->czsecurity->xssCleanPostInput('reseller_new');
			$plan_new = $this->input->post('Plans');
			
			$condition	  = array('resellerID' => $reseller_old);
			$num_rows    = $this->admin_panel_model->get_num_rows('tbl_merchant_data', $condition);

			if ($num_rows > 0) {
				$update_data = array('resellerID' => $reseller_new);
				$newPlan = '';
				if($plan_new && !empty($plan_new) && $plan_new > 0){
					$newPlan = ", plan_id = '$plan_new' ";	
				}

				$sss = $this->db->query("update tbl_merchant_data set resellerID='$reseller_new' $newPlan where merchID IN ($sub_ID) ");

				if ($sss) {
					$this->session->set_flashdata('message', '<strong> Success:</strong> Merchant is reassigned for seller. ');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In update process.');
				}
			} else {
				$this->session->set_flashdata('message', ' <div class="alert alert-danger"><strong>Error:</strong> No data selected.</div>');
			}
		}
		redirect(base_url('Admin_panel/reseller_list'));
	}




	public function get_merchant()
	{

		$id = $this->czsecurity->xssCleanPostInput('seller_id');

		$val = array(
			'merchantID' => $id

		);
		$status = 'failed';

		$datas = $this->admin_panel_model->get_marchent($id);
		$tab = '<table class="table table-bordered table-striped table-vcenter">' . '<thead>' . '' .
			'<th class="text-left "><strong>Marchant Name</strong></th>
					<th class="text-left "><strong>Current Plan </strong></th>
					<th class="text-center "><strong>Select List </strong></th>
			  </thead>	
			  <tbody>';
		if (!empty($datas)) {
			$status = 'success';
			foreach ($datas as $c_data) {

				$tab .= '<tr>  
				<td class="text-left visible-lg  col-md-6">' . $c_data['firstName'] . ' ' . $c_data['lastName'] . '</td>
				<td class="text-left visible-lg  col-md-6">'. $c_data['plan_name'] . '</td>
			    <td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="' . $c_data['merchID'] . '" /> </td>
			</tr>';
			}
		} else {

			$tab .= '<tr>  
			<td class="text-left visible-lg col-md-6" colspan="3">No Records Found</td>';
		}

		$tab .= '</tbody>
        </table>';
		$data['data'] = $tab;
		$data['status'] = $status;
		echo json_encode($data);
		die;
	}



	public function admin_setting()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		if (!empty($this->input->post(null, true))) {
			
			$input_data['adminCompanyName']	   = $this->czsecurity->xssCleanPostInput('adminCompanyName');
			
			$input_data['primaryContact']	   = $this->czsecurity->xssCleanPostInput('primaryContact');
			$input_data['billingfirstName']	   = $this->czsecurity->xssCleanPostInput('billingfirstName');
			$input_data['billinglastName']	   = $this->czsecurity->xssCleanPostInput('billinglastName');

			$input_data['adminAddress']	   = $this->czsecurity->xssCleanPostInput('adminAddress');
			$input_data['adminAddress2']	   = $this->czsecurity->xssCleanPostInput('adminAddress2');
			$input_data['adminCountry']	   = $this->czsecurity->xssCleanPostInput('adminCountry');
			$input_data['adminState']	   = $this->czsecurity->xssCleanPostInput('adminState');
			$input_data['adminCity']	   = $this->czsecurity->xssCleanPostInput('adminCity');
			$input_data['zipCode']	   = $this->czsecurity->xssCleanPostInput('zipCode');

			$input_data['date']	   = date('Y-m-d H:i:s');

			if ($this->czsecurity->xssCleanPostInput('adminID') != "") {

				$id = $this->czsecurity->xssCleanPostInput('adminID');
				if($id == 1){
					$chk_condition = array('adminID' => $id);
					$this->admin_panel_model->update_row_data('tbl_admin', $chk_condition, $input_data);
				}else{
					$chk_condition = array('adminID' => $id);
					$this->admin_panel_model->update_row_data('tbl_admin', $chk_condition, $input_data);
					$chk_condition1 = array('adminID' => 1);
					$this->admin_panel_model->update_row_data('tbl_admin', $chk_condition1, $input_data);
				}
				
				$this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Updated</strong></div>');
			}
		}


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$data['login_info'] 	= $this->session->userdata('admin_logged_in');

		$rID 				= 1;

		$data['loginAdminID'] = $data['login_info']['adminID'];

		$user_id			    = $rID;
		
		$con                = array('adminID' => $user_id);
		$data['admin'] 	  = $this->admin_panel_model->get_row_data('tbl_admin', $con);

		$country = $this->admin_panel_model->get_table_data('country', '');
		$data['country_datas'] = $country;

		$state = $this->admin_panel_model->get_table_data('state', '');
		$data['state_datas'] = $state;

		$city = $this->admin_panel_model->get_table_data('city', '');
		$data['city_datas'] = $city;


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_setting', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function admin_profile()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');


		if (!empty($this->input->post(null, true))) {
			$email   = $this->czsecurity->xssCleanPostInput('Email');
			$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
			$LastName = $this->czsecurity->xssCleanPostInput('lastName');
			$input_data['adminFirstName']	   = $FirstName;
			$input_data['adminLastName']	=  $LastName;
			$input_data['adminEmail']	=  $email;
			$input_data['date']	   = date('Y-m-d H:i:s');

			if ($this->czsecurity->xssCleanPostInput('adminID') != "") {

				$id = $this->czsecurity->xssCleanPostInput('adminID');
				$chk_condition = array('adminID' => $id);
				$this->admin_panel_model->update_row_data('tbl_admin', $chk_condition, $input_data);
				$this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Updated</strong></div>');
			}
		}

		$data['login_info'] 	= $this->session->userdata('admin_logged_in');

		$rID 				= $data['login_info']['adminID'];

		$user_id			    = $rID;
		$con                = array('adminID' => $user_id);
		$data['admin'] = $this->general_model->get_row_data('tbl_admin', $con);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_profile', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/**************************************************** To Get Reseller Gateway Edit Data *********************************************************/

	public function get_details_reseller()
	{

		$id = $this->czsecurity->xssCleanPostInput('gateway_id');

		$con = array(
			'gatewayID' => $id

		);

		$datas = $this->admin_panel_model->get_table_data('tbl_reseller', $con);

	?>
		<table class="table table-bordered table-striped table-vcenter">

			<tbody>

				<tr>
					<th class="text-left col-md-6"> <strong> Reseller </strong></th>
					<th class="text-center col-md-6"><strong>Select</strong></th>
				</tr>
				<?php
				if (!empty($datas)) {
					foreach ($datas as $c_data) {

				?>

						<tr>
							<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['resellerCompanyName']; ?> </td>

							<td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['resellerID']; ?>" /> </td>
						</tr>


					<?php     }
				} else { ?>

					<tr>
						<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Records Found"; ?> </td>

						<td class="text-right visible-lg col-md-6 control-label"> </td>

					<?php } ?>

			</tbody>
		</table>


<?php die;
	}

	/*************************************** Update  Gateway of Reseller  *************************************************************************/

	public function update_gateways_resellers()
	{

		if (!empty($this->input->post(null, true))) {

			$sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate'));

			$old_gateway =  $this->czsecurity->xssCleanPostInput('gateway_old');
			$new_gateway =  $this->czsecurity->xssCleanPostInput('gateway_new');

			$condition	  = array('gatewayID' => $old_gateway);
			$num_rows    = $this->general_model->get_num_rows('tbl_reseller', $condition);

			if ($num_rows > 0) {
				$update_data = array('gatewayID' => $new_gateway);

				$sss    = $this->db->query("update tbl_reseller set gatewayID='" . $new_gateway . "' where resellerID IN ($sub_ID) ");

				$tbl_invoice = $this->db->query("update tbl_reseller_billing_invoice set gatewayID='" . $new_gateway . "' where resellerID IN ($sub_ID) ");

				if ($sss) {

					$this->session->set_flashdata('message', '<strong> Success:</strong> Subscription gateway have changed. ');
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> In update process.');
				}
			} else {
				$this->session->set_flashdata('message', ' <strong>Error:</strong> No data selected.');
			}
		}
		redirect(base_url('Admin_panel/reseller_list'));
	}
	/**************************************************** End Here ***********************************************************************************/

	/********************************************** Suspend Reseller Start Here **********************************************************/

	public function update_reseller_status()
	{

		$id = $this->czsecurity->xssCleanPostInput('resellerID');
		$act = $this->czsecurity->xssCleanPostInput('acct');

		if ($act == 0) {


			if (!empty($id)) {

				$res = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id, 'isSuspend' => '0'));

				if (!empty($res)) {

					$ins_data = array('isSuspend' => '1');

					$this->admin_panel_model->update_row_data('tbl_reseller', array('resellerID' => $id, 'isSuspend' => '0'), $ins_data);
					$this->session->set_flashdata('message', '<strong> Successfully Suspended</strong>');
					redirect(base_url('Admin_panel/reseller_list'));
				} else {
					$this->session->set_flashdata('message', '<strong>Error: </strong>Something Is Wrong.');
					redirect(base_url('Admin_panel/reseller_list'));
				}
			} else {
				$this->session->set_flashdata('message', '<strong>Error: </strong>Invalid Request.');
				redirect(base_url('Admin_panel/reseller_list'));
			}
		} else {

			if (!empty($id)) {

				$res = $this->admin_panel_model->get_row_data('tbl_reseller', array('resellerID' => $id, 'isSuspend' => '1'));

				if (!empty($res)) {

					$ins_data = array('isSuspend' => '0');

					$this->admin_panel_model->update_row_data('tbl_reseller', array('resellerID' => $id, 'isSuspend' => '1'), $ins_data);
					$this->session->set_flashdata('message', '<strong> Successfully Resume Suspended Reseller </strong>');
					redirect(base_url('Admin_panel/reseller_list'));
				} else {
					$this->session->set_flashdata('message', '<strong>Error: </strong>Something Is Wrong.');
					redirect(base_url('Admin_panel/reseller_list'));
				}
			} else {
				$this->session->set_flashdata('message', '<strong>Error: </strong>Invalid Request.');
				redirect(base_url('Admin_panel/reseller_list'));
			}
		}
	} 
	/********************************************** Suspend Reseller End Here ***********************************************************/

	/************************************************* To get QuickBooks download file ***********************************************/

	public function quickbooks()
	{

		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['login_info'] = $this->session->userdata('logged_in');
		
		$data['companies'] = $this->general_model->get_qb_data();
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/page_company', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	/********************************************************** Get Recover Reseller Password and Send Mail To Reseller *****************************************/

	public function recover_Reseller_pwd()
	{

		$Reseler_url = 'https://reseller.payportal.com/';

		$data['login_info'] 	= $this->session->userdata('admin_logged_in');

		$adminID 				= $data['login_info']['adminID'];



		$resID = $this->uri->segment('3');

		$res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $resID));

		$results = $this->general_model->get_select_data('tbl_admin', array('adminFirstName', 'adminLastName', 'adminCompanyName', 'adminEmail'), array('adminID' => $adminID));

		if (!empty($results)) {

			if ($res_data['resellerCompanyName'] != "") {
				$rs_name = $res_data['resellerCompanyName'];
			} else {
				$rs_name = $res_data['resellerfirstName'] . '' . $res_data['lastName'];
			}

			$rsFullname = $res_data['resellerfirstName'] . ' ' . $res_data['lastName'];

			if ($results['adminCompanyName'] != "") {
				$mch_name = $results['adminCompanyName'];
			} else {
				$mch_name = $results['adminFirstName'] . '' . $results['adminLastName'];
			}

			$email = $res_data['resellerEmail'];
			$this->load->helper('string');
			$code = random_string('alnum', 10);
			$login_url = $Reseler_url;

			$update = $this->admin_panel_model->reseller_reset_password($code, $email);

			if ($update) {

				$temp_data = $this->general_model->get_row_data('tbl_template_reseller_data', array('templateType' => '2'));


				$message = $temp_data['message'];
				$subject = $temp_data['emailSubject'];

				if ($temp_data['fromEmail'] != '') {
					$fromemail = $temp_data['fromEmail'];
				} else {

					$fromemail = $results['adminEmail'];
				}

				$replyTo = DEFAULT_FROM_EMAIL;

				$login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';

				$subject = stripslashes(str_replace('{{reseller_name}}', $rs_name, $subject));
				$subject = stripslashes(str_replace('{{reseller_company}}', $rs_name, $subject));

				$message = stripslashes(str_replace('{{reseller_name}}', $rs_name, $message));

				$message = stripslashes(str_replace('{{merchant_name}}', $rsFullname, $message));

				$message = stripslashes(str_replace('{{login_url}}', $login_url, $message));
				$message = stripslashes(str_replace('{{merchant_email}}', $email, $message));
				$message = stripslashes(str_replace('{{merchant_password}}', $code, $message));
				$message = stripslashes(str_replace('{{reseller_name}}', $rs_name, $message));
				$message = stripslashes(str_replace('{{reseller_company}}', $rs_name, $message));


				$mail_sent = sendMailBySendgrid($email,$subject,$fromemail,$rs_name,$message,$replyTo);

				if($mail_sent)
				{
					$this->session->set_flashdata('message', '<div class="alert alert-success"><strong>Success: </strong> Successfully sent new password to Reseller</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>  Email was not sent, please contact your administrator.</div>');
				}
			}
			redirect('Admin_panel/reseller_list', 'refresh');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong> Email not available!</div>');
			redirect('Admin_panel/reseller_list', 'refresh');
		}
	}
	/******************************************** End of Password Recover Function ****************************************/
	/**********************************************************************/
	public function blank_dashboard()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');


		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/dashboard_blank', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function inactive_paln()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('admin_logged_in');

		$planname =  $this->admin_panel_model->planTypeInactive();
		$data['plans'] = $planname;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/admin_inactive_plans', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	/***********************for blank dashboard****************************/
	/************Inc=voice Merchant on create*****************/
	public function createInvoiceForMerchant($postData,$cardID,$merchantID)
	{
		if ($postData['Plans']) {
			$plan_id = $postData['Plans'];
			$m_name =  $postData['companyName'];
			$is_free_trial = isset($postData['is_free_trial'])?$postData['is_free_trial']:0;
			$free_trial_day = isset($postData['free_trial_day'])?$postData['free_trial_day']:0;
			$updataMain = $this->admin_panel_model->get_row_data('plans',array('plan_id'=>$plan_id));

			$inv_data = $this->general_model->get_row_data('tbl_merchant_number', array('numberType' => 'Merchant'));

			$inumber   = $inv_data['liveNumber'] + 1;
            $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
            $inv_id   = 'ADM' .mt_rand(100000, 999999);
            
			if($is_free_trial == 1){
				$free_trial_day = $free_trial_day;
				$freeTrialFay = $free_trial_day - 1;
				$trial_expiry_date = date('Y-m-d', strtotime('+'.$freeTrialFay.' days'));

				$due_date = $trial_expiry_date;
				
				$balance = 0;
				$total1 = 0;
				$subscriptionItemName = 'Free trial for '.$free_trial_day.' days';
				$subscriptionAmount = 0;
				$rental_amount = 0;

				$plan_expiry = $trial_expiry_date;
			}else{
				$free_trial_day = 0;
				$trial_expiry_date = null;

				/* Plan per day calculation */
                $planAmount = $updataMain['subscriptionRetail'];
                $numberOfDaysInMonth = date('t');
                $amountPerDay = round(($planAmount / $numberOfDaysInMonth),2);


                /*Days compare in month by plan*/
                $billing_up_to_date = date("Y-m-t");
                $datePurchase = date("Y-m-d");
                $currentdate = date("d");
                
                
                $datediff = strtotime($billing_up_to_date) - strtotime($datePurchase);
               	$different_days = round($datediff / (60 * 60 * 24)) + 1;



                 if($currentdate >= 25){
                	$nextMonth = date('Y-m-d',strtotime('last day of +1 month'));

					$nextMonthAmount = $planAmount;

                	$plan_expiry = $nextMonth;
                }else{
                	$nextMonth = date('Y-m-d',strtotime('first day of +1 month'));
                	$nextMonthAmount = 0;

                	$plan_expiry = date('Y-m-d', strtotime('-1 day', strtotime($nextMonth)));
                }
                

                $totalInvoiceAmount = $different_days * $amountPerDay;
                
                $rental_amount = $totalInvoiceAmount + $nextMonthAmount; 

				$due_date = date('Y-m-d');
				
				$balance = $rental_amount + $updataMain['serviceFee'];
				$total1 = $updataMain['serviceFee'];

				$subscriptionItemName = 'Subscription Fee';
				$subscriptionAmount = $rental_amount;

			}
			if($balance == '0' || $balance == '0.00' ){
                $status   = 'paid';
            }else{
                $status   = 'pending';
            }
			$data     = array(
                'merchantName'           => $m_name,
                'merchantID'             => $merchantID,
                'invoice'                => $inv_id,
                'invoiceNumber'          => $refNumber,   
                'DueDate'                => $due_date,
                'BalanceRemaining'       => ($balance),
                'status'                 => $status,
                'gatewayID'              => 0,
                'cardID'                 => $cardID,
                'merchantSubscriptionID' => 0,
                'Type'                   => 'Plan',
                'serviceCharge'          => $total1,
                'invoiceStatus'          => 1,
                'createdAt'              => date('Y-m-d H:i:s'),
                'updatedAt'              => date('Y-m-d H:i:s')
            );
            if ($this->general_model->insert_row('tbl_merchant_billing_invoice', $data)) {
            	
            	$updata1 = array('plan_expired' => $plan_expiry);
                $this->general_model->update_row_data('tbl_merchant_data', array('merchID' => $merchantID), $updata1);

            	if($cardID > 0 && $balance > 0){
					$statusUpdate = $this->getChargeOfInvoice($postData, $cardID, $balance,$merchantID,$m_name,$data);
				}

                $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_number', array('numberType' => 'Merchant'), $updata);

                $pl_data['itemName']          = $subscriptionItemName;
                $pl_data['itemPrice']         = $subscriptionAmount;
                $pl_data['merchantInvoiceID'] = $inv_id;
                $pl_data['planID'] = $plan_id;
                $pl_data['merchantID'] = $merchantID;
                $pl_data['merchantName'] = $m_name;

                $pl_data['subscriptionID']    = $inv_id;
                $pl_data['createdAt']         = date('Y-m-d H:i:s');
                $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data);

                $item_data['itemName']          = 'Service Charge';
                $item_data['itemPrice']         = $total1;
                $item_data['merchantInvoiceID'] = $inv_id;
                $item_data['subscriptionID']    = $inv_id;
                $item_data['planID'] = $plan_id;
                $item_data['merchantID'] = $merchantID;
                $item_data['merchantName'] = $m_name ;
                $item_data['createdAt']         = date('Y-m-d H:i:s');
            }

		}

		return true;
	}

	public function check_url()
	{
		$res = array();

		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');

		if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
			if (strpos($portal_url, ' ') > 0) {

				$res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
				echo json_encode($res);
				die;
			}

			$res = array('portal_url' => 'Only letters and numbers allowed', 'status' => 'false');
			echo json_encode($res);
			die;
		} else {
			$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url));
			if ($num == 0) {
				$res = array('status' => 'success');
			} else if ($num == 1) {
				$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url, 'merchantID' => $merchantID));
				if ($num == 1)
					$res = array('status' => 'success');
				else
					$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			} else {
				$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			}

			echo json_encode($res);
			die;
		}
	}
	public function get_merchant_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		$data['gateway'] =  getGatewayNames($data['gatewayType']);
		echo json_encode($data);
		die;
	}
	public function get_plan_data()
	{
    
    	$res=array();
        $planID = $this->czsecurity->xssCleanPostInput('plan_id');
        $plan = $this->general_model->plan_by_id($planID);
        
        $gateway='';
        if($plan['is_free_trial']){
        	$is_free_trial = 1;
            $free_trial_day = $plan['free_trial_day'];
        }else{
        	$is_free_trial = 0;
            $free_trial_day = 0;
        }
       
        
        $res['gateway'] =''; 
        $res['status']  ='success';
        $res['is_free_trial']  = $is_free_trial;
        $res['free_trial_day'] = $free_trial_day;
        
        
        echo json_encode($res); die;
    }
    public function getChargeOfInvoice($postData, $cardID, $balance,$merchantID,$merchantName,$in_data)
	{

		

    	$card_data = $this->card_model->get_merchant_single_card_data($cardID);

    	
		//start NMI

		$merchantName = $merchantName;
        $payamount = $balance;
        $txnID = '';
        if(isset($card_data['CardID'])){
        	$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

			
			$sch_method = $postData['payOption'];
			
			$gatewayResponseCode = 400;
			$gatewayType = 'NMI';
			if($payamount > 0)
			{

				

				if($sch_method == 1){
					$coneGetWay = array('gatewayType'=>5);
				
    				$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);

    				$stripepass = $get_gateway['gatewayPassword'];

					$gatewayType = 'Stripe';
					include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

					$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['cardMonth'];
					$exyear  = $card_data['cardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];
					$CardType	 = $card_data['CardType'];
					
					$plugin = new ChargezoomStripe();
				    $plugin->setApiKey($stripepass);

				    try {
					  	 $res = \Stripe\Token::create([
									'card' => [
									'number' =>$cnumber,
									'exp_month' => $expmonth,
									'exp_year' =>  $exyear,
									'cvc' => $cvv,
									'address_city'    =>   $Billing_City,
				                    'address_country'   => $Billing_Country,
				                    'address_line1'     => $Billing_Addr1,
				                    'address_line1_check'     => null,
				                    'address_line2'     => $Billing_Addr2,
				                    'address_state'     => $Billing_State,
				                    'address_zip' => $Billing_Zipcode,
				                    'address_zip_check' => null,
									'name' => $merchantName
								   ]
						]);
						$tcharge= json_encode($res);  
        				$rest = json_decode($tcharge);


					}catch (Exception $e) {
						$gatewayResponseCode = 0;
						$responsetext = $e->getMessage();
						
					  // Something else happened, completely unrelated to Stripe
					}
					
        			
        			if(isset($rest->id)){
        				$amount =  (int)($payamount*100);
        				$token = $rest->id; 

        				try {
							$customer = \Stripe\Customer::create(array(
							    'name' => $merchantName,
							    'description' => 'CZ Merchant',
							    'source' => $token,
							    "address" => ["city" => $Billing_City, "country" => $Billing_Country, "line1" => $Billing_Addr1, "line2" => $Billing_Addr2, "postal_code" => $Billing_Zipcode, "state" => $Billing_State]
							));
							$customer= json_encode($customer);
			              	  
							$resultstripecustomer = json_decode($customer);
						}catch (Exception $e) {
							
						  // Something else happened, completely unrelated to Stripe
						}

        				try {
						  	if(isset($resultstripecustomer->id))
							{
								$charge =	\Stripe\Charge::create(array(
							  	  'customer' => $resultstripecustomer->id,
								  "amount" => $amount,
								  "currency" => "usd",
								  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
								 
								));	
							}
							else
							{
								$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge for merchant invoice",
								 
								));
							}
        					
	        				$charge= json_encode($charge);
				        
					   		$result = json_decode($charge);

						}catch (Exception $e) {
							$gatewayResponseCode = 0;
							$responsetext = $e->getMessage();
							
						  // Something else happened, completely unrelated to Stripe
						}
        				 

				   		if($result->paid=='1' && $result->failure_code=="")
						{
						    $gatewayResponseCode = 200;
						    $responsetext = $result->status;
						    $txnID = $result->id;
						}else{
							if(isset($result->status)){
								$gatewayResponseCode = $result->failure_code;
								$responsetext = $result->status;
							}
							
						}

        			}else{
        				if(isset($rest->id)){
        					$txnID = 'TXN-'.$in_data['invoice'];
			        		$gatewayResponseCode = 0;
			        		$responsetext = $result->status;
        				}
        				
        			}
        			

				}else if($sch_method == 2){
					include APPPATH . 'third_party/nmiDirectPost.class.php';
					include APPPATH . 'third_party/nmiCustomerVault.class.php';
					$coneGetWay = array('gatewayType'=>1);
				
    				$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);
    				$nmiuser   = $get_gateway['gatewayUsername'];
					$nmipass   = $get_gateway['gatewayPassword'];
					$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
					$transaction1 = new nmiDirectPost($nmi_data); 

					$gatewayType = 'NMI';
					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					$accountType = $card_data['acct_type'];
					$accountHolderType = $card_data['acct_holder_type'];
					$transaction1->setAccountName($accountName);
					$transaction1->setAccount($accountNumber);
					$transaction1->setRouting($routeNumber);
					$sec_code = 'WEB';
					$transaction1->setAccountType($accountType);
					$transaction1->setAccountHolderType($accountHolderType);
					$transaction1->setSecCode($sec_code);
					$transaction1->setPayment('check');


					if($Billing_Addr1 != ''){
						$transaction1->setCountry($Billing_Addr1);
					}else{
						if($postData['merchantAddress1'] != ''){
							$transaction1->setAddress1($postData['merchantAddress1']);
						}
					}
					
					if($Billing_Country != ''){
						$transaction1->setCountry($Billing_Country);
					}else{
						if($postData['merchantCountry'] != ''){
							$transaction1->setCountry($postData['merchantCountry']);
						}else{
							$transaction1->setCountry('US');
						}
						
					}	
					
					if($Billing_City != ''){
						$transaction1->setCity($Billing_City);
					}else{
						if($postData['merchantCity'] != ''){
							$transaction1->setCity($postData['merchantCity']);
						}
					}
					
					if($Billing_Zipcode != ''){
						$transaction1->setZip($Billing_Zipcode);
					}else{
						if($postData['merchantZipCode'] != ''){
							$transaction1->setZip($postData['merchantZipCode']);
						}
					}
					if($billing_phone_number != ''){
						$transaction1->setPhone($billing_phone_number);
					}else{
						if($postData['merchantContact'] != ''){
							$transaction1->setPhone($postData['merchantContact']);
						}
					}
					
					if($Billing_State != ''){
						$transaction1->setState($Billing_State);
					}else{
						if($postData['merchantState'] != ''){
							$transaction1->setState($postData['merchantState']);
						}
					}


					$transaction1->setEmail($postData['merchantEmail']);
					$transaction1->setCompany($postData['companyName']);
					$transaction1->setFirstName($postData['firstName']);
					$transaction1->setLastName($postData['lastName']);


					$transaction1->setShippingAddress1($Billing_Addr1);
					$transaction1->setShippingCountry($Billing_Country);
					$transaction1->setShippingCity($Billing_City);
					$transaction1->setShippingState($Billing_State);
					$transaction1->setShippingZip($Billing_Zipcode);

					$transaction1->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $in_data['invoiceNumber']);

					$transaction1->setAmount($payamount);
					$transaction1->setTax('tax');
					$transaction1->sale();
					$getwayResponse = $transaction1->execute();
					$txnID = $getwayResponse['transactionid'];

					if($getwayResponse['response_code']=="100"){
						$gatewayResponseCode = 200;
						$responsetext = $getwayResponse['responsetext'];
					}
				}else{
					$getwayResponse = 0;
				}

				if($gatewayResponseCode == 200 ){
					
					$invoice      = $in_data['invoice'];  
					$status 	 = 'paid';
					$paid_status = '1';
					$pay        = $payamount;
					$remainbal  = $in_data['BalanceRemaining']-$payamount;
					$data   	 = array('status'=>$status, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );

					$condition  = array('invoice'=>$in_data['invoice'] );
					 
					$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
				    				
				}
				 
			    $transaction = array();
			    $transaction['transactionID']      = $txnID;
				$transaction['transactionStatus']  = $responsetext;
				$transaction['transactionCode']    = $gatewayResponseCode;
				$transaction['transactionType']    = 'sale';
				$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['merchant_invoiceID']       = $in_data['invoice'];
				$transaction['gatewayID']          = $get_gateway['gatewayID'];
				$transaction['transactionGateway'] = 1;	
				$transaction['transactionAmount']  = $payamount;
				$transaction['gateway']            = $gatewayType;
				$transaction['merchantID']         = $merchantID;
			  
				$id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
			}	
		
        }

        return true;
	}

	public function admin_dashboard_chart()
	{

		$get_result = $this->admin_panel_model->chart_get_merchant();
		$data['chart']        = 	$get_result;

		$get_result1 = $this->admin_panel_model->get_total_processing_volume();
		$data['merch_list']  = $get_result1['merchant'];
		$data['res_list']    =  $get_result1['reseller'];
		$data['plan_ratio'] = $get_result1['plan_ratio'];


		$ob = [];
		$obV = [];
		$obRevenu = [];
		
		$in = 0;

		foreach ($data['chart']['month'] as $monKey => $value) {
			$ob1 = [];
			$obR = [];
			
			$inc = strtotime($value);
			$ob1[0] = $inc;
			$obR[] = $inc;
			$obR[] = $value;
			$ob1[1] = $data['chart']['revenue'][$in];

			$ob[] = $ob1;
			
			$monthNew = date("M", strtotime($obR[1])); 
			$data['chart']['month'][$monKey] = $monthNew;
			$obR[1] = $monthNew;
			$obRevenu[] = $obR; 
			$in++;
		}

		$data['revenu_month'] =   $obRevenu;
		$data['revenu_volume'] =   $ob;
		
		echo json_encode($data);
		die;
	}
	public function admin_dashboard_processing_volumn()
	{

		$get_result = $this->admin_panel_model->chart_get_merchant();
		$data['chart']        = 	$get_result;

		
		$obV = [];
		$obRevenu = [];
		
		$in = 0;

		foreach ($data['chart']['month'] as $monKey => $value) {
			$ob1 = [];
			$obR = [];
			$ob2 = [];
			
			$inc = strtotime($value);
			
			$ob2[0] = $inc;
			$obR[] = $inc;
			$obR[] = $value;

			$monthNew = date("M", strtotime($obR[1])); 
			$data['chart']['month'][$monKey] = $monthNew;
			$obR[1] = $monthNew;
			
			$ob2[1] = $data['chart']['volume'][$in];

			$obV[] = $ob2;

			$obRevenu[] = $obR; 
			$in++;
		}

		$data['revenu_month'] =   $obRevenu;
		
		$data['volume'] =   $obV;	
		
		echo json_encode($data);
		die;
	}
} // end of class
