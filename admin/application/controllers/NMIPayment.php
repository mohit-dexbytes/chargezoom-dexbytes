<?php

class NMIPayment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
    	include APPPATH . 'third_party/nmiDirectPost.class.php';
	   include APPPATH . 'third_party/PayPalAPINEW.php';
			
	    include APPPATH . 'third_party/nmiCustomerVault.class.php';
	    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
		include APPPATH . 'third_party/PayTraceAPINEW.php';
		require_once APPPATH."third_party/stripe/init.php";	
		require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
		$this->load->config('paypal');
		$this->load->config('quickbooks');
		$this->load->config('paytrace');
		$this->load->model('quickbooks');
		 $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('admin_panel_model');
		$this->load->model('company_model');
		
		if(!$this->session->userdata('admin_logged_in'))
		{
			redirect('login', 'refresh');
		}
	}
	
	
	public function index(){

	}
	
	 
	 	   
	
public function pay_invoice(){
    
	   	 $invoiceID               = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $merchantCardID               = $this->czsecurity->xssCleanPostInput('merchantCardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
        $in_data   =    $this->admin_panel_model->get_merchant_billing_data($invoiceID);
		
		$gt_result = $this->general_model->get_row_data('tbl_admin_gateway', array('gatewayID'=>$gateway));
			  
		 
			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);   

        
       if($merchantCardID!="" &&  $gateway!=""){    
         
        
		if(!empty($in_data)){ 
		
		       $card_data    =   $this->get_single_card_data($merchantCardID);
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] !='0.00'){
				
					        $amount =	 $in_data['BalanceRemaining'];
					  		$transaction1 = new nmiDirectPost($nmi_data); 
							$transaction1->setCcNumber($card_data['CardNo']);
						    $expmonth =  $card_data['CardMonth'];
							$exyear   = $card_data['CardYear'];
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $expry    = $expmonth.$exyear;  
							$transaction1->setCcExp($expry);
							$transaction1->setCvv($card_data['CardCVV']);
							$transaction1->setAmount($amount);
				        
						    $transaction1->sale();
						    $result = $transaction1->execute();
					
					if( $result['response_code']=="100"){
						 $merchant_invoiceID      = $in_data['merchant_invoiceID'];  
						 $ispaid 	 = 'Completed';
							 $data   	 = array('status'=>$ispaid);
						 $condition  = array('merchant_invoiceID'=>$in_data['merchant_invoiceID']);	
						 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success!</strong></div>');  
					   } else{
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>'); 
					   }  
					   
					   $transaction['transactionID']      = $result['transactionid'];
					   $transaction['transactionStatus']  = $result['responsetext'];
					   $transaction['transactionCode']    =  $result['response_code'];
					    $transaction['transactionType']   =  'sale';
					   $transaction['transactionDate']    = date('Y-m-d h:i:s');  
					    $transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']  = $gt_result['gatewayType'] ;
					   $transaction['merchant_invoiceID']       = $in_data['merchant_invoiceID'];
					   $transaction['merchantID']     =     $in_data['merchantID'];
					   $transaction['transactionAmount']  = $in_data['BalanceRemaining'];
					
				       $id = $this->general_model->insert_row('tbl_merchant_tansaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Merchant has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }
			
			 
		    redirect('home/invoices','refresh');

    }     

		 	 
public function pay_paypal_invoice(){
        
	      $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $merchantCardID        = $this->czsecurity->xssCleanPostInput('merchantCardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
         $in_data   =    $this->admin_panel_model->get_merchant_billing_data($invoiceID);
		
		$gt_result = $this->general_model->get_row_data('tbl_admin_gateway', array('gatewayID'=>$gateway));
			   
			
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($merchantCardID!="" || $gateway!=""){  
					 
				   
			if(!empty($in_data))
			{ 
					  
						   $card_data    =   $this->get_single_card_data($merchantCardID);
					
		 if(!empty($card_data))
		 {
							
				if( $in_data['BalanceRemaining'] > 0){
					        $amount 	      =	 $in_data['BalanceRemaining'];
							
						   $creditCardType    = 'Visa'; 
							$creditCardNumber = $card_data['CardNo'];
						    $expDateMonth     =  $card_data['CardMonth'];
							$expDateYear      = $card_data['CardYear'];
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								$cvv2Number =   $card_data['CardCVV'];
							$currencyID     = "USD";
						  

                           $firstName = $in_data['firstName'];
                            $lastName =  $in_data['lastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['merchantAddress1']; 
                            $address2 = $in_data['merchantAddress2']; 
							$country  = $in_data['merchantCountry']; 
							$city     = $in_data['merchantCity'];
							$state    = $in_data['merchantState'];		
								$zip  = $in_data['merchantZipCode']; 
								$phone = $in_data['merchantContact']; 
								$email = $in_data['merchantEmail']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
							
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					     $code = '111';
						 $merchant_invoiceID      = $in_data['merchant_invoiceID'];  
						 $ispaid 	 = 'Completed';
						 $data   	 = array('status'=>$ispaid);
						 $condition  = array('merchant_invoiceID'=>$in_data['merchant_invoiceID']);	
						 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					    $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
				
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
					   }  
					   
					 }
						
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d h:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					   $transaction['transactionCode']     = $code;  
					  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;
                       $transaction['merchant_invoiceID']       = $in_data['merchant_invoiceID'];
					   $transaction['merchantID']      = $in_data['merchantID'];
					   $transaction['transactionAmount']   =$amt ;
					
				       $id = $this->general_model->insert_row('tbl_merchant_tansaction',   $transaction);
					   
				
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Merchant has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and 0.</div>'); 
		  }
			
			 
		    redirect('home/invoices','refresh');

    }     

	
public function pay_auth_invoice(){
    
	     
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $merchantCardID               = $this->czsecurity->xssCleanPostInput('merchantCardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
         $in_data   =    $this->admin_panel_model->get_merchant_billing_data($invoiceID);
		$gt_result = $this->general_model->get_row_data('tbl_admin_gateway', array('gatewayID'=>$gateway));
			   
		 
			$apiLogin         = $gt_result['gatewayUsername'];
			$transactionkey   = $gt_result['gatewayPassword'];
			
         
	 if($merchantCardID!="" &&  $gateway!="" && !empty($gt_result)){  
		if(!empty($in_data)){ 
		     $card_data      =   $this->get_single_card_data($merchantCardID);
			 
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					
							$amount  =	 $in_data['BalanceRemaining'];
					  		$transaction1 = new AuthorizeNetAIM($apiLogin,$transactionkey); 
							$card_no  = $card_data['CardNo'];
						    $expmonth =  $card_data['CardMonth'];
							$exyear   = $card_data['CardYear'];
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $expry    = $expmonth.$exyear;  
							

						     $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
					
					   if( $result->response_code=="1"){
						 $merchant_invoiceID      = $in_data['merchant_invoiceID'];  
						 $ispaid 	 = 'Completed';
						 $data   	 = array('status'=>$ispaid);
						 $condition  = array('merchant_invoiceID'=>$in_data['merchant_invoiceID']);	
						 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result->response_reason_text.'.</div>'); 
					   }  
					   
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result->response_code;  
					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
					    $transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = $result->transaction_type;	
						$transactiondata['merchant_invoiceID']       = $in_data['merchant_invoiceID'];
					   $transactiondata['merchantID']      = $in_data['merchantID'];
					   $transactiondata['transactionAmount']   = $result->amount;
					   $transactiondata['merchant_invoiceID']       = $in_data['merchant_invoiceID'];
					   $id = $this->general_model->insert_row('tbl_merchant_tansaction',   $transactiondata);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Merchant has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
	             }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }		 
			 
		    redirect('home/invoices','refresh');

    }     



      

  
	public function pay_trace_invoice(){
	 
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $merchantCardID               = $this->czsecurity->xssCleanPostInput('merchantCardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');	
		 
         $in_data   =    $this->admin_panel_model->get_merchant_billing_data($invoiceID);
		$gt_result = $this->general_model->get_row_data('tbl_admin_gateway', array('gatewayID'=>$gateway));
			   
		 
			$payusername   = $gt_result['gatewayUsername'];
			$paypassword   = $gt_result['gatewayPassword'];
			$grant_type    = "password";
			
	 if($merchantCardID!="" &&  $gateway!="" && !empty($gt_result)){  
		if(!empty($in_data)){ 
		
			
		     $card_data      =   $this->get_single_card_data($merchantCardID);
			 
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					
				$amount  =	 $in_data['BalanceRemaining'];	
				
					$card_no  =  trim($card_data['CardNo']);
						    $expmonth =  trim($card_data['CardMonth']);
							$exyear   = trim($card_data['CardYear']);
							$cvv	  = trim($card_data['CardCVV']);  
							if($cvv!='999'){
								$cvv ='999';
							}	
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						$payAPI  = new PayTraceAPINEW();	
					 
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

						//If IsFoundOAuthTokenError results True, means no error 
						//next is to move forward for the actual request 

		if(!$oauth_moveforward){ 
		
		
		         $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
				
				$name = $in_data['firstName'];
				$address = $in_data['merchantAddress1'];
				$city = $in_data['merchantCity'];
				$state = $in_data['merchantState'];
				$zipcode = ($in_data['merchantZipCode'])?$in_data['merchantZipCode']:'85284';
				
			
				
				$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);   
					

				     $request_data = json_encode($request_data);
			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
			
	             
				   if ( $result['http_status_code']=='200' ){
						 $merchant_invoiceID      = $in_data['merchant_invoiceID'];  
						 $ispaid 	 = 'Completed';
						 $data   	 = array('status'=>$ispaid);
						 $condition  = array('merchant_invoiceID'=>$in_data['merchant_invoiceID']);	
						 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					   
							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>'); 
					   }  
					   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					    $transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_sale';	   
					   $transactiondata['merchantID']      = $in_data['merchantID'];
					   $transactiondata['transactionAmount']   = $amount;
				       $id = $this->general_model->insert_row('tbl_merchant_tansaction',   $transactiondata);
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Authentication not valid.</div>'); 
				 }			
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Not valid.</div>'); 
				}			
                  }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Merchant has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
	             }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }		 
			 
		    redirect('home/invoices','refresh');

    }     
	
	public function pay_stripe_invoice(){
		 			
	     $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $merchantCardID               = $this->czsecurity->xssCleanPostInput('merchantCardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
        $in_data   =    $this->admin_panel_model->get_merchant_billing_data($invoiceID);
		$gt_result = $this->general_model->get_row_data('tbl_admin_gateway', array('gatewayID'=>$gateway));
			  
		
             	$nmiuser   = $gt_result['gatewayUsername'];
    		    $nmipass   = $gt_result['gatewayPassword'];
    		    
         
        if($merchantCardID!="" &&  $gateway!="" && !empty($gt_result)){  
         
       
		if(!empty($in_data)){ 
		  
        $this->load->model('card_model');    
	

		 $card_data = $this->card_model->get_merchant_single_card_data($merchantCardID);	

		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					     $cr_amount = 0;
					     $amount    =	 $in_data['BalanceRemaining']; 
					     $merchantName    =	 $in_data['companyName'];
					     
					  
							$amount =  (int)($amount*100);
							include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
							$plugin = new ChargezoomStripe();
		    				$plugin->setApiKey($get_gateway['gatewayPassword']);
					
							\Stripe\Stripe::setApiKey($gt_result['gatewayPassword']);

							try {
								$exyear =  $card_data['CardYear'];
								$exyear   = substr($exyear,2);
								$expmonth = $card_data['CardMonth'];
								if(strlen($expmonth)==1){
									$expmonth = '0'.$expmonth;
								}
					 	        
							  	 $res =  \Stripe\Token::create([
									'card' => [
									'number' => $card_data['CardNo'],
									'exp_month' => $expmonth,
									'exp_year' =>  $exyear,
									'cvc' => $card_data['CardCVV'],
									'address_city'	=>	$card_data['Billing_City'],
								    'address_country'	=> $card_data['Billing_Country'],
								    'address_line1'	=> $card_data['Billing_Addr1'],
								    'address_line1_check'	=> null,
								    'address_line2'	=> $card_data['Billing_Addr2'],
								    'address_state'	=> $card_data['Billing_State'],
								    'address_zip'	=> $card_data['Billing_Zipcode'],
								    'address_zip_check'	=> null,
								    'name'	=> $merchantName,
								   ]
								]); 
			               
								$tcharge= json_encode($res);  
								$rest = json_decode($tcharge);


							}catch (Exception $e) {
								$responsetext = $e->getMessage();
								
							  // Something else happened, completely unrelated to Stripe
							}
							$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
							if(isset($rest->id))
							{
								$token  = $rest->id;

								try {
									$customer = \Stripe\Customer::create(array(
									    'name' => $merchantName,
									    'description' => 'CZ Merchant',
									    'source' => $token,
									    "address" => ["city" => $card_data['Billing_City'], "country" => $card_data['Billing_Country'], "line1" => $card_data['Billing_Addr1'], "line2" => $card_data['Billing_Addr2'], "postal_code" => $card_data['Billing_Zipcode'], "state" => $card_data['Billing_State']]
									));
									$customer= json_encode($customer);
					              	  
									$resultstripecustomer = json_decode($customer);
								}catch (Exception $e) {
									
								  // Something else happened, completely unrelated to Stripe
								}
								if(isset($resultstripecustomer->id))
								{
									$charge =	\Stripe\Charge::create(array(
								  	  'customer' => $resultstripecustomer->id,
									  "amount" => $amount,
									  "currency" => "usd",
									  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
									 
									));	
								}
								else
								{
									$charge =	\Stripe\Charge::create(array(
									  "amount" => $amount,
									  "currency" => "usd",
									  "source" => $token, // obtained with Stripe.js
									  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
									 
									));	
								}
								
					   
							    $charge= json_encode($charge);
							    $result = json_decode($charge);
							}if(isset($rest->status)){
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$rest->status .'</div>');
							}

							
							
						   
						    $trID='';
							
						  
				 if($result->paid=='1' && $result->failure_code==""){
						  $code		 =  '200';
						  $trID 	 = $result->id;
						 $txnID      = $in_data['merchant_invoiceID']; 
                         $ispaid 	 = 'Completed';
						 $data   	 = array('status'=>$ispaid);
						 $condition  = array('merchant_invoiceID'=>$in_data['merchant_invoiceID']);	
						 $this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					       $code =  $result->failure_code;
					       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>  '.$result->status.'</div>'); 
					   	
					   }  
					   
					   
					   $transaction['transactionID']      = $trID;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d h:i:s');  
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['merchantID']     = $in_data['merchantID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  
				       $id = $this->general_model->insert_row('tbl_merchant_tansaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }
			
			 
		    redirect('home/invoices','refresh');
           
		 
	   } 
	 
	 
	
	
	public function get_card_data(){
		$customerdata =array();
		if($this->czsecurity->xssCleanPostInput('merchantCardID')!=""){
			 $crID = $this->czsecurity->xssCleanPostInput('merchantCardID');
			$card_data = $this->get_single_card_data($crID);
			
			if(!empty($card_data)){
			         $customerdata['status'] =  'success';	
					$customerdata['card']     = $card_data;
					echo json_encode($customerdata)	;
					die;
			}
		}
		
		
	}
	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 




	 public function check_vault(){
		 
		 
		  $card=''; $card_name=''; $merchantdata=array();
		 if($this->czsecurity->xssCleanPostInput('merchantID')!=""){
			 
				
				$merchantID 	= $this->czsecurity->xssCleanPostInput('merchantID');  
			

			 	$condition     =  array('merchID'=>$merchantID); 
			    $merchantdata = $this->general_model->get_row_data('tbl_merchant_data',$condition);
				if(!empty($merchantdata)){
	                 				
				   	 $merchantdata['status'] =  'success';	     
					
					 $card_data =   $this->get_card_expiry_data($merchantID);
					$merchantdata['card']  = $card_data;
					echo json_encode($merchantdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

  public function get_single_card_data($merchantCardID){  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	    	 $db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";   
				
			   
		             
	       
	  		     $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
			    	$this->db1 = $this->load->database($dsn, true);        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantCardID='$merchantCardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->encrypt->decode($card_data['MerchantCard']) ;
						 $card['CardMonth']  = $card_data['CardMonth'];
						  $card['CardYear']  = $card_data['CardYear'];
						  $card['merchantCardID']    = $card_data['merchantCardID'];
						  $card['CardCVV']   = $this->encrypt->decode($card_data['CardCVV']);
						  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
				}		
					
					return  $card;

       }
 

	   	
  public function get_card_expiry_data($merchantID){  
						$card_data = array();    
						$card      = array();
						$this->load->library('encrypt');
			    	    $db_user 	= "devcz_quicktest";
					   $db_pass     = "ecrubit@123";
			     		$db_name    = "devcz_chargezoom_card_db"; 
				     
	  		     $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 

			    	$this->db1 = $this->load->database($dsn, true);        
			  
	     	 $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where   merchantListID='$merchantID'  "; 
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->result_array();
			      
					
					return  $card_data;

 }
	
}