<?php
class AjaxRequest extends CI_Controller
{

	
	public function __construct()
	{

		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('general');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('card_model');

	}

	public function index()
	{
		redirect('login', 'refresh');
	}




	public function get_process_details_data()
	{

		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$invoiceID  = $this->czsecurity->xssCleanPostInput('invID');
		$type       = $this->czsecurity->xssCleanPostInput('type');

		$payOption = 0;

		$managePayOption = 0;

		$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID ));

		if(isset($get_data['payOption'])){
			$payOption = $get_data['payOption'];
		}

		$onGatewaychange = 'set_url(this);';
		$app_data  = $this->general_model->get_row_data('app_integration_setting', array('merchantID' => $merchantID ));

		if ($type == 2)
			$gateway_datas = $this->general_model->get_table_select_data('tbl_admin_gateway', array('gatewayID', 'set_as_default', 'gatewayFriendlyName'), array('gatewayType' => 1));
		if ($type == 1)
			$gateway_datas = $this->general_model->get_table_select_data('tbl_admin_gateway', array('gatewayID', 'set_as_default', 'gatewayFriendlyName'), array('gatewayType' => 5));
		$gate = '';

		

		$option = '';
		$option_card = '';

		$defaultGateway = '';

		if ($type == 1) {
			if (isset($gateway_datas) && !empty($gateway_datas)) {
				foreach ($gateway_datas as $gateway_data) {
					if($gateway_data['set_as_default']){
						$defaultGateway = $gateway_data['gatewayID'];
					}

					if ($gateway_data['set_as_default'] == 1) $sel = ' selected';
					else $sel = '';
					$option .=  '<option value="' . $gateway_data['gatewayID'] . '"  ' . $sel . '  >' . $gateway_data['gatewayFriendlyName'] . '</option>';
				}
			}

			$option_card .=  '<option value="new1"  >New Card</option>';
			$card =   $this->card_model->get_merchant_card_expiry_data($merchantID,1);
			
			if (isset($card) && !empty($card)) {
				$managePayOption = 1;
				foreach ($card as $card_data) {
					if (strtoupper($card_data['CardType']) != 'ECHECK' || strtoupper($card_data['CardType']) != 'CHECKING'){
						$option_card .=  '<option selected value="' . $card_data['merchantCardID'] . '"  >' . $card_data['merchantFriendlyName'] . '</option>';
					}
						
				}
			}


			$lable = "Card";
		}

		if ($type == 2) {
			
			if (isset($gateway_datas) && !empty($gateway_datas)) {
				foreach ($gateway_datas as $gateway_data) {
					if($gateway_data['set_as_default']){
						$defaultGateway = $gateway_data['gatewayID'];
					}
					if ($gateway_data['set_as_default'] == 1) $sel = ' selected';
					else $sel = '';
					$option .=  '<option value="' . $gateway_data['gatewayID'] . '" ' . $sel . '  >' . $gateway_data['gatewayFriendlyName'] . '</option>';
				}
			}

			$option_card .=  '<option value="new1"  >New Account</option>';
			$card =   $this->card_model->get_merchant_card_expiry_data($merchantID,2);
			if (isset($card) && !empty($card)) {
				$managePayOption = 1;
				foreach ($card as $card_data) {

					if (strtoupper($card_data['CardType']) == 'ECHECK' || strtoupper($card_data['CardType']) == 'CHECKING'){
						$option_card .=  '<option selected value="' . $card_data['merchantCardID'] . '"  >' . $card_data['merchantFriendlyName'] . '</option>';
					}
					
				}
			}

			$lable = "Checking Account";
		}

		$merchant_condition = [
            'merchID' => $merchantID,
        ];

        if (!merchant_gateway_allowed($merchant_condition)) {
			$gateways = '<div class="form-group "><input type="hidden" name="gateway" value="'.$defaultGateway.'"</div>';
        } else {
			$gateways = '<div class="form-group ">' .

			'<label class="col-md-4 control-label" for="card_list">Gateway</label>' .
			'<div class="col-md-8">' .
			'<select id="gateway"   name="gateway"   class="form-control">' .
			'' .
			$option .

			'</select></div></div>';
		}

		$gate = $gateways.'<div class="form-group ">' .

			'<label class="col-md-4 control-label" for="card_list">' . $lable . '</label><div class="col-md-8">' .
			'<select id="merchantCardID" name="merchantCardID" onchange="create_sch_card(this,\'' . $type . '\');"   class="form-control">' .
			$option_card .

			'</select>' .

			'</div></div>';      

		$data['schd'] = $gate;
		$data['payOption'] = $payOption;
		$data['managePayOption'] = $managePayOption;
		$data['status'] = "success";

		echo json_encode($data);
		die;
	}



	public function view_invoice_transaction(){
		$res = array();

		
		$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');

		$transactions  = $this->general_model->get_table_data('tbl_merchant_tansaction', array('merchant_invoiceID' => $invoiceID));

		$tr_data = '';
		$tr_head = '';
		if (!empty($transactions)) {
			foreach ($transactions as $transaction) {

				$gateway =  ($transaction['gateway']) ? ($transaction['gateway']) : $transaction['transactionType'];

				$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs" >Transaction ID</th>
								<th class="text-right hidden-xs">Amount</th>
								<th class="text-right visible-lg">Date</th>
								<th class="text-right hidden-xs">Gateway Type</th>
								<th class="hidden-xs text-right">Status</th>
							</tr>
						</thead>';

				if (($transaction['transactionCode'] == '200' || $transaction['transactionCode'] == '100' || $transaction['transactionCode'] == '1' || $transaction['transactionCode'] == '111') && $transaction['transaction_user_status'] != '3' && trim($transaction['transaction_user_status']) != '5') {
					if(trim($transaction['transactionType']) == 'refund'){
						$status = 'Refunded';
					}else{
						$status = 'Success';
					}
					
				} else if ($transaction['transaction_user_status'] == '3' || $transaction['transaction_user_status'] == '5') {
					
				} else {
					$status = 'Failed';
				}

				$tr_data .= '<tr class="paylist_data">' .

					'<td class="hidden-xs text-right">' . $transaction['transactionID'] . '</td>' .
					'<td class="hidden-xs text-right">$' . number_format($transaction['transactionAmount'], 2) . '</td>' .
					'<td class="hidden-xs text-right">' . date('M d, Y', strtotime($transaction['transactionDate'])) . '</td>
					<td class="hidden-xs text-right">' . $gateway . '</td>' .
					'<td class="text-right hidden-xs">' . $status . '</td>
						</tr>';
			}
		} else {
			$tr_head = '
						<thead>
							<tr class="paylist_heading">
								<th class="text-right hidden-xs" >Transaction ID</th>
								<th class="text-right hidden-xs">Amount</th>
								<th class="text-right visible-lg">Date</th>
								<th class="text-right hidden-xs">Gateway Type</th>
								<th class="hidden-xs text-right">Status</th>
							</tr>
						</thead>';
			$tr_data .= '<tr><td colspan="5" class="text-center">No Records Found</td></tr>';
		}

		$tr_data = $tr_head . $tr_data;


		$res['transaction'] = $tr_data;
		echo json_encode($res);
		die;
	}

	public function check_new_email()
	{
		$res = array();

		$merchantEmail  = $this->czsecurity->xssCleanPostInput('merchantEmail');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');
		if($merchantID > 0){
			$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID ));
			$current_email = $get_data['merchantEmail'];
			if($current_email == $merchantEmail){
				$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
				echo json_encode($res);
				die;
			}else{
				$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
			}
			
		}else{
			$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
		}
		if(count($app_data) > 0){
			$res = array('message' => 'Email Already Exitsts', 'status' => 'error');
			echo json_encode($res);
			die;
		}else{
			$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
			echo json_encode($res);
			die;
		}
	}

    /**
     * Check to ensure password is not the same as current password
     */
	public function chk_previous_password()
	{
		if (!empty($this->input->post(null, true))) {
			$data        = [];
			$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');
			$newPassword = $this->czsecurity->xssCleanPostInput('merchantPassword1');
            $merchant    = $this->general_model->get_row_data(
                'tbl_merchant_data',
                array('merchID' => $merchantID)
            );

			if (!empty($merchant)) {
                if ($merchant['merchantPasswordNew'] !== null) {
                    if (password_verify($newPassword, $merchant['merchantPasswordNew'])) {
                        $data['status'] = 'error';
                    } else {
                        $data['status'] = 'success';
                    }
                } else {
                    if (md5($newPassword) === $merchant['merchantPassword']) {
                        $data['status'] = 'error';
                    } else {
                        $data['status'] = 'success';
                    }
                }
			} else {
				$data['status'] = 'error';
			}

			echo json_encode($data);
			die;
		}

		echo "Invalid Request";
		die;
	}

	public function checkLastPasswordTime()
	{
		if (!empty($this->input->post(null, true))) {
			$data = [];
			$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
			
			$merch_data = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
		 	$password_update_date = $merch_data['password_update_date'];

			$current = strtotime(date('Y-m-d H:i:s'));
			$password_update_date = strtotime($password_update_date);
			$diff = $current - $password_update_date;
			$hours = round($diff / ( 60 * 60 ) );
		 	
			if ($hours == 0) {
				$data['status'] = 'error';
			} else {
				$data['status'] = 'success';
			}

			echo json_encode($data);
			die;
		}
		echo "Invalid Request";
		die;
	}
	public function check_url(){
		$res =array();
		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
		$resID = $this->czsecurity->xssCleanPostInput('resellerID');
		if(!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)){
			 if(strpos($portal_url, ' ') > 0){
    	       
    			 $res =array('portal_url'=>'Space is not allowed in url', 'status'=>'false');
           echo json_encode($res);
    		die;	
    	    }
			
			$res =array('portal_url'=>'Special character not allowed in url', 'status'=>'false');
    		echo json_encode($res);
    		die;	
		}else{
			$codition   = array('portalprefix'=>$portal_url);
			$setting    =  $this->general_model->get_row_data('Config_merchant_portal', $codition);
			if(isset($setting['ConfigID'])){
				if($setting['resellerID'] == $resID ){
					$res =array('status'=>'true');
		    		echo json_encode($res);
				}else{
					$res =array('portal_url'=>'This Portal URL cannot used','status'=>'false');
		    		echo json_encode($res);
				}
		    	
			}else{
				$res =array('status'=>'true');
		    	echo json_encode($res);
			}
			die;
		    
		}
	}	

	public function admin_security_log()
    {
        $data['login_info']  = $this->session->userdata('admin_logged_in');

		$data = [];
		$no = $_POST['start'];
		$result = $this->general_model->logData();
		$logData = $result['result'];
		$count = $result['num'];
		if(!empty($logData)){
			foreach($logData as $securityLog){
				$no++;
				$row = array();
				$row[] = '<td class="text-left">'. $securityLog["emailAddress"] .'</td>';
				$row[] = '<td class="text-right">'. $securityLog["ip_address"] .'</td>';
				$row[] = '<td class="text-right">'. $securityLog["City"] .'</td>';
				$row[] = '<td class="text-right">'. $securityLog["ISP"] .'</td>';
				$row[] = '<td class="text-center">'. $securityLog["browser"] .'</td>';
				$row[] = '<td class="text-center">'. date('M d, Y H:i A', strtotime( $securityLog['Login_at'])) .'</td>';
				$data[] = $row;
			}
		}
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" =>  $count,
			"recordsFiltered" => $count,
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);die;
	}
	public function check_session()
	{
		if ($this->session->userdata('admin_logged_in') != "") {
			echo json_encode(array('status' => 'success')); die;
		}else{
			echo json_encode(array('status' => 'failed')); die;
		}
	}
}
