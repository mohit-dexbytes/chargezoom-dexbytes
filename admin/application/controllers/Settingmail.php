<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Settingmail extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
	     if($this->session->userdata('admin_logged_in')!="")
		  {
		   
		  }else{
		  
			redirect('login','refresh');
		  }
		  delete_cache();
	}
	
	
	public function index()
	{   
		redirect('login','refresh');	
	}
	

	
	
	
	public function email_template()
	{
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
	
	
		$data['templates']      =  $this->general_model->get_table_data('tbl_template_reseller_data',array('userTemplate'=>1));
		
		$data['merchant_templates']      =  $this->general_model->get_table_data('tbl_email_template_data',[]);

		$data['reseller_templates']      =   $this->general_model->get_table_data('tbl_template_reseller_data',array('userTemplate'=>2));
		
	 	$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/page_email', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	public function create_template(){
	
	
		 
	    if($this->czsecurity->xssCleanPostInput('templateName') !=""){
		
		  	$templateName =  $this->czsecurity->xssCleanPostInput('templateName');
		 
		
		 
			$message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
			$subject   =  $this->czsecurity->xssCleanPostInput('emailSubject');
		  	$createdAt   = date('Y-m-d H:i:s');
		  	$message  = htmlspecialchars_decode($message);
		 
		  	$insert_data = array('templateName'  =>$templateName,
		                       
								 'message'		=>$message,
								 'emailSubject' => $subject,
								 
								
		             );
			if($this->czsecurity->xssCleanPostInput('tempID')!='')
			{
				$tempID = $this->czsecurity->xssCleanPostInput('tempID');
				$results = $this->general_model->update_row_data('tbl_template_reseller_data',array('rtempID'=>$tempID),$insert_data, ['message']);   
			}else{
				$type 	   =  $this->czsecurity->xssCleanPostInput('type'); 
				$insert_data['templateType'] = $type;
				$tempID = $this->general_model->insert_row('tbl_template_reseller_data',$insert_data, ['message']);  
			}
			redirect('Settingmail/create_template/'.$tempID); 
	   	}
	 
		
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
	
		
		if($this->uri->segment('3')){
			$temID  				  = $this->uri->segment('3');	

			$condition              = array('rtempID'=>$temID);  
			$data['templatedata']	  = $this->general_model->get_row_data('tbl_template_reseller_data', $condition);	

		}
		  
		
		$data['subject']              =  'Welcome to {{company_name}} Payment Portal';

		$data['types']      =  $this->general_model->get_table_data('tbl_template_reseller_data',array('userTemplate'=>1));
		$data['email_temp'] ='<div id="welcome_msg">
			<p>Dear {{merchant_name}},</p>

			<p>Thank you for registering for {{company_name}} Payment Portal.</p>

			<p>Your login details are below :</p>

			<p>Username : {{merchant_email}}<br />
			Password : {{merchant_password}}</p>

			<p>Click {{link_url}} to activate your account.</p>

			<p>Please contact us at {{reseller_phone}} if you need assistance.</p>

			<p>&nbsp;</p>

			<p>Thanks,</p>

			<p>Reseller Name :{{reseller_name}}<br />
			Reseller Company :{{merchant_name}}<br />
			Reseller Phone :{{reseller_phone}}<br />
			Reseller Email :{{reseller_email}}</p>
			</div>
			';
		 
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/page_email_template', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	
	   
	}
	
	
	public function copy_template(){
	
				
	     	  $temID  = $this->uri->segment('3'); 
			  
			  
			  $condition              = array('templateID'=>$temID); 
			  $templatedata			  = $this->general_model->get_row_data('tbl_eml_temp_reseller', $condition);	
		   
		 
		   
			 $insert_data = array('templateName'  =>$templatedata['templateName'],
		                        'templateType'    => $templatedata['templateType'],
								 'resellerID'      =>$templatedata['resellerID'], 
								 'fromEmail'      => $templatedata['fromEmail'],
								 'toEmail'		  => $templatedata['toEmail'],
								 'addCC'          => $templatedata['addCC'],
								  'addBCC'		  =>$templatedata['addBCC'],
								 'replyTo'        => $templatedata['replyTo'],
								 'message'		  =>$templatedata['message'],
								 'emailSubject'   =>$templatedata['emailSubject'],
								 'attachedTo'	  =>$templatedata['attachedTo'],
								 'createdAt'      => date('Y-m-d H:i:s') 
								
		             );
			   
			  
			 if( $this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data)){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Copied</strong></div>');  
			 
			 }else{
		 	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong>.</div>'); 
			 }
			
          redirect('Settingmail/email_temlate','refresh');
	
	}
	
	
	
	
	public function delete_template(){
	
	
	     	  $temID 				  = $this->czsecurity->xssCleanPostInput('tempateDelID'); 
			 if( $this->db->query("Delete from tbl_eml_temp_reseller where templateID = $temID")){
			  $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Success</strong></div>');  
			 
			 }else{
			 	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
			 }
      redirect('Settingmail/email_temlate', 'refresh');
	
	}
	
	
	
	public function view_template(){
	
	
	     	  $temID 				  = $this->czsecurity->xssCleanPostInput('tempateViewID'); 
    	      $condition              = array('templateID'=>$temID);		
			 $view_data             = $this->general_model->template_data($condition); 
			
			  $view_data=$view_data[0];
		?>		 
		 <table class="table table-bordered table-striped table-vcenter"   >
          
            <tbody>
				
			<tr>
					<th class="text-left"><strong> Template Name</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['templateName']; ?></a></td>
			</tr>	
		<tr>
					<th class="text-left"><strong> From Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['fromEmail']; ?></a></td>
			</tr>	
          <tr>
					<th class="text-left"><strong>Cc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addCC']; ?></a></td>
			</tr>	
           <tr>
					<th class="text-left"><strong>Bcc Email</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['addBCC']; ?></a></td>
			</tr>	
            
            	<tr>
					<th class="text-left"><strong> Set to Reply</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['replyTo']; ?></a></td>
			</tr>	
             <tr>
					<th class="text-left"><strong> Email Subject</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['emailSubject']; ?></a></td>
			</tr>	
			<tr>
					<th class="text-left"><strong> Template Type</strong></th>
					<td class="text-left visible-lg"><?php echo $view_data['typeText']; ?></td>
			</tr>	
			<tr>
					<th colspan="2" class="text-left"><strong> Message</strong></th>
					
			</tr>	
            <tr>
					<td colspan="2" class="text-left"><?php echo $view_data['message']; ?></td>
					
			</tr>	
		
			</tbody>
        </table>
        
      <?php  
	       die;
      
	
	}
	
	
	public function set_template(){
	           
			   
			  $customerID        = $this->czsecurity->xssCleanPostInput('customerID'); 
		      $companyID		  = $this->czsecurity->xssCleanPostInput('companyID'); 
			    $typeID			  = $this->czsecurity->xssCleanPostInput('typeID'); 
			    $customer         = $this->czsecurity->xssCleanPostInput('customer'); 
				
				
			    
    	       $comp_data= $this->general_model->get_row_data('tbl_company',array('id'=>$companyID));
                 
    	       $condition         = array('templateType'=>$typeID, 'resellerID'=>$comp_data['resellerID']);  
			   
			   
			   
			   $merchant_data     = $this->general_model->get_row_data('tbl_merchant_data', array('resellerID'=>$comp_data['resellerID'])); 
			   $config_data     = $this->general_model->get_row_data('tbl_config_setting', array('resellerID'=>$comp_data['resellerID'])); 
			   
               $currency          = "$";   			   
			   
			   $config_email      = $merchant_data['merchantEmail'];
			   $merchant_name     = $merchant_data['companyName'];
			   $logo_url          = $merchant_data['merchantProfileURL']; 
			   $mphone            =  $merchant_data['merchantContact'];
			   $cur_date          = date('Y-m-d'); 
			   $amount 		='';  
			   $paymethods   ='';  
			   $transaction_details = '';
			   $tr_data      ='';  
			   $ref_number   = '';  
			    $overday      = '';
				$balance      = '0.00';
				$in_link      = '';
				$duedate      = '';
				$company      ='';
				$cardno       = '';
				$expired      = '';
				$expiring     = '';
				$friendly_name = '';
				$update_link  = $config_data['customerPortalURL'];
				$in_link  = $config_data['customerPortalURL'];
			
			
			    $data['login_info']	    = $this->session->userdata('logged_in');
		        $company			    = $data['login_info']['companyName'];  
			
			    $condition1 = " and Customer_ListID='".$customerID."' and  companyID='".$companyID."' ";
			    if($this->czsecurity->xssCleanPostInput('invoiceID')!=""){
				
				  $invoiceID			  = $this->czsecurity->xssCleanPostInput('invoiceID'); 
				   $condition1.= " and TxnID='".$invoiceID."' " ;
				   
				   	if($typeID =='1' || $typeID =='3'){
				 $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				 }
				 if($typeID == '2' ){
				   $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				} 
				if($typeID == '5'){  
				
				 $condition1.=" and   `IsPaid` = 'true' and userStatus=''  ";
				}
				if($typeID == '4'){
				 $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus='' and  (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300' ";
				}
				   
				}else{
				
				
				if($typeID =='1' || $typeID =='3'){
				       $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				 }
				 if($typeID == '2' ){
				        $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				} 
				if($typeID == '4'){
				      $condition1.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus='' and  (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300' ";
				}
			   if($typeID == '5'){
				       $condition1.=" and   `IsPaid` = 'true' and userStatus=''  ";
				}
				
				
				
				if($typeID == '12' || $typeID == '11' ){
				
			        $card_data =   $this->get_expiry_card_data($customerID,'1');
					
					if(!empty($card_data))
					{
						  $cardno          =  $card_data['CustomerCard'];
						  $friendly_name     =  $card_data['customerCardfriendlyName'];
					}
			   } 	
			 }  
			     
				  
			   
					  if(!empty($data)){
						   $customer = $data['FullName'];
						  $amount    = $data['AppliedAmount'] ;  
						 $balance    = $data['BalanceRemaining'] ;  
					  $paymethods    = $data['paymentType'] ;  
							
					 $duedate        = date('F d, Y', strtotime($data['DueDate'])) ;   
					$ref_number      = $data['RefNumber'] ;  
        			      $tr_date   =  date('F d, Y', strtotime($data['TimeCreated']));
					}	
					
			   
			 
	    	   $subject =	stripslashes(str_replace('{{ invoice.refnumber }}',$ref_number, $view_data['emailSubject']));
			   if( $view_data['emailSubject'] =="Welcome to { company_name }")
			   $subject = 'Welcome to '.$company;
		
		       if($typeID == '11' ){ 
			  
			     $subject =	stripslashes(str_replace('{{ creditcard.mask_number }}',$cardno, $view_data['emailSubject'])); 
			   } 
		     
		      $logo_url = 'http://localhost/quickbook/uploads/merchant_logo/charge_logo.png';    
			   $message = $view_data['message'];
			   $message = stripslashes(str_replace('{{ merchant_name }}',$merchant_name ,$message));
			 
			   $message = stripslashes(str_replace('{{ creditcard.mask_number }}',$cardno ,$message ));
			    $message = stripslashes(str_replace('{{ creditcard.type_name }}',$friendly_name ,$message ));
			   $message = stripslashes(str_replace('{{ creditcard.url_updatelink }}',$update_link ,$message ));
			   
			   $message = stripslashes(str_replace('{{ customer.company }}',$customer ,$message));
			   $message = stripslashes(str_replace('{{ transaction.currency_symbol }}',$currency ,$message ));
			   $message = stripslashes(str_replace('{{invoice.currency_symbol}}',$currency ,$message ));
			    
			   $message = stripslashes(str_replace('{{ transaction.amount }}',($amount)?($amount):'0.00' ,$message )); 
			   $message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_date, $message ));
			  
			   $message =	stripslashes(str_replace('{{ invoice.refnumber }}',$ref_number, $message));
			   $message =	stripslashes(str_replace('{{invoice.balance}}',$balance, $message));
			   $message =	stripslashes(str_replace('{{ invoice.due_date|date("F j, Y") }}',$duedate, $message));
				 $message = stripslashes(str_replace('{{ invoice.days_overdue }}', $overday, $message )); 
			   $message =	stripslashes(str_replace('{{ invoice.url_permalink }}',$in_link, $message));
			   $message = stripslashes(str_replace('{{ merchant_email }}',$config_email ,$message ));
			   $message = stripslashes(str_replace('{{ merchant_phone }}',$mphone ,$message ));
			   $message = stripslashes(str_replace('{{ current.date }}',$cur_date ,$message ));  
			   
			   
			   $new_data_array=array();
			          $new_data_array['message']= $message;
				$new_data_array['emailSubject'] = $subject;
				      $new_data_array['message']= $message;
				       $new_data_array['addCC'] = $view_data['addCC'];
				       $new_data_array['addBCC']= $view_data['addBCC'];
				
				      $new_data_array['replyTo']= $view_data['replyTo'];
			   
		
		       echo json_encode($new_data_array);
	           die;    
	}
	
	
	public function set_due_template(){
	           
			   
			   $customerID        = $this->czsecurity->xssCleanPostInput('customerID'); 
		      $companyID		  = $this->czsecurity->xssCleanPostInput('companyID'); 
			    $typeID			  = $this->czsecurity->xssCleanPostInput('typeID'); 
			    $customer         = $this->czsecurity->xssCleanPostInput('customer');  

    	       $comp_data= $this->general_model->get_row_data('tbl_company',array('id'=>$companyID));
                 
    	       $condition         = array('templateType'=>$typeID, 'resellerID'=>$comp_data['resellerID']);  
			  
			     $merchant_data     = $this->general_model->get_row_data('tbl_merchant_data', array('merchID'=>$comp_data['resellerID'])); 
			   $config_data     = $this->general_model->get_row_data('tbl_config_setting', array('merchID'=>$comp_data['resellerID'])); 
			   
               $currency          = "$";   			   
			   
			   $config_email      = $merchant_data['merchantEmail'];
			   $merchant_name     = $merchant_data['companyName'];
			   $logo_url          = $merchant_data['merchantProfileURL']; 
			   $mphone            =  $merchant_data['merchantContact'];
			   $cur_date          = date('Y-m-d'); 
			   
               $currency          = "$";   			   
			   
			   $amount 		='';  
			   $paymethods   ='';  
			   $transaction_details = '';
			   $tr_data      ='';  
			   $ref_number   = '';  
			    $overday      = '';
				$balance      = '0.00';
				$in_link      = '';
				$duedate      = '';
				$company      ='';
				$cardno       = '';
				$expired      = '';
				$expiring     = '';
				$friendly_name = '';
				$update_link  = $config_data['customerPortalURL'];
			
			
			    $data['login_info']	    = $merchant_data;
		         $company			    = $merchant_data['companyName'];  
			
			    $condtion = " and Customer_ListID='".$customerID."' and  companyID='".$companyID."' ";
				if($typeID =='1' || $typeID =='3'){
				 $condtion.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				 }
				 if($typeID == '2' ){
				   $condtion.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus=''  ";
				} 
				if($typeID == '5'){
				 $condtion.=" and  DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <= '".date('Y-m-d')."' AND `IsPaid` = 'false' and userStatus='' and  (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300' ";
				}
				if($typeID == '12'){
				
			        $card_data =   $this->get_card_expiry_data($customerID, $companyID);
					if(!empty($card_data))
					{
						  $cardno          =  $card_data['CustomerCard'];
						$friendly_name     =  $card_data['customerCardfriendlyName'];
					}
			   } 		
				   
				
			  if(!empty($data)){
				   $customer = $data['FullName'];
				  $amount  = $data['AppliedAmount'] ;  
				 $balance  = $data['BalanceRemaining'] ;  
			  $paymethods  = $data['paymentType'] ;  
					
					  $duedate  = date('F d, Y', strtotime($data['DueDate'])) ;   
					  $ref_number  = $data['RefNumber'] ;  
			
			}	
			
	    	   $subject =	stripslashes(str_replace('{{ invoice.refnumber }}',$ref_number, $view_data['emailSubject']));
				  if( $view_data['emailSubject'] =="Welcome to { company_name }")
			   $subject = 'Welcome to '.$company;
			   
			   $message = $view_data['message'];
			   
			   $message = stripslashes(str_replace('{{ creditcard.type_name }}',$friendly_name ,$message));
			   $message = stripslashes(str_replace('{{ creditcard.mask_number }}',$cardno ,$message ));
			    $message = stripslashes(str_replace('{{ creditcard.type_name }}',$friendly_name ,$message ));
			   $message = stripslashes(str_replace('{{ creditcard.url_updatelink }}',$update_link ,$message ));
			   $message = stripslashes(str_replace('{{ customer.company }}',$customer ,$message));
			   $message = stripslashes(str_replace('{{ transaction.currency_symbol }}',$currency ,$message ));
			    $message = stripslashes(str_replace('{{invoice.currency_symbol}}',$currency ,$message ));
			    
			   $message = stripslashes(str_replace('{{ transaction.amount }}',($amount)?($amount):'0.00' ,$message )); 
			   
			   $message = stripslashes(str_replace(' {{ transaction.transaction_method }}',$transaction_details ,$message));
			   $message = stripslashes(str_replace('{{ transaction.transaction_date}}', $tr_data, $message ));
			    $message = stripslashes(str_replace('{{ transaction.transaction_detail }}', $transaction_details, $message ));
			   $message = stripslashes(str_replace('{{ invoice.days_overdue }}', $overday, $message ));
			   $message =	stripslashes(str_replace('{{ invoice.refnumber }}',$ref_number, $message));
			   $message =	stripslashes(str_replace('{{invoice.balance}}',$balance, $message));
			   $message =	stripslashes(str_replace('{{ invoice.due_date|date("F j, Y") }}',$duedate, $message));
				 
			   $message =	stripslashes(str_replace('{{ invoice.url_permalink }}',$update_link, $message));
			   $message = stripslashes(str_replace('{{ merchant_email }}',$config_email ,$message ));
			   $message = stripslashes(str_replace('{{ merchant_phone }}',$mphone ,$message ));
			   $message = stripslashes(str_replace('{{ current.date }}',$cur_date ,$message ));
			  
			   
			   $new_data_array=array();
			    $new_data_array['message']= $message;
				$new_data_array['emailSubject'] = $subject;
				  $new_data_array['message']= $message;
				  $new_data_array['addCC'] = $view_data['addCC'];
				  $new_data_array['addBCC']= $view_data['addBCC'];
				
				  $new_data_array['replyTo']= $view_data['replyTo'];
			   
		
		       echo json_encode($new_data_array);
	           die;    
	}
	
	
	
	
	
	
	
	
	//-------------------   START -------------------------------//
	
	 //---------- View email history ----------------------//
	 
	 public function get_history_id()
	 
	  {
		 
				 $historyID          =  $this->czsecurity->xssCleanPostInput('customertempID'); 
		         $condition 		= array('mailID'=> $historyID);
				 $historydatas		= $this->general_model->get_row_data('tbl_template_data', $condition);
				 
						if(!empty($historydatas))  
						{ 
				
			?>	
			
		 	 
		 <table class="table table-bordered table-striped table-vcenter">
          
            <tbody>
				
			
		<tr>
					<th class="text-right col-md-1 control-label"><strong> To </strong> </th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailto']; ?></a> </td>
			</tr>	
          <tr>
					<th class="text-right col-md-1 control-label"><strong>Cc </strong></th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailcc']; ?> </a> </td>
			</tr>	
           <tr>
					<th class="text-right col-md-1 control-label"><strong>Bcc </strong></th>
					<td class="text-left visible-lg col-md-11"><?php echo $historydatas['emailbcc']; ?></td>
			</tr>	
            
				 <tr>
					<th class="text-right col-md-1 control-label"><strong> Date</strong></th>
					<td class="text-left visible-lg col-md-11"> <?php echo date('M d, Y - h:i A', strtotime($historydatas['emailsendAt'])); ?> </a></td>
			</tr>	
			
             <tr>
					<th class="text-right col-md-1 control-label"><strong>Subject</strong></th>
					<td class="text-left visible-lg col-md-11"> <?php echo $historydatas['emailSubject']; ?> </a></td>
			</tr>	
			
			
			
	
            <tr>
					<td colspan="2" class="text-left"><?php echo $historydatas['emailMessage']; ?></td>
					
			</tr>	
						
			</tbody>
        </table>
        
      <?php  } die;
      
	
	 
	  }
	 
	 
	 
	public function send_mail()
	{
			  if($this->session->userdata('reseller_logged_in')){
			
				$data['login_info']	    = $this->session->userdata('logged_in');
				$user_id			    = $data['login_info']['resellerIndexID'];
			}
			if($this->session->userdata('agent_logged_in')){
			
			$data['login_info']	    = $this->session->userdata('agent_logged_in');
			$user_id			    = $data['login_info']['resellerIndexID'];
			}
		   $merchant_data     = $this->general_model->get_row_data('tbl_merchant_data', array('resellerID'=>$user_id)); 
		   $logo_url  = $merchant_data['merchantProfileURL']; 
		 
	
			$this->load->library('email');
	 
			$type 	   =  $this->czsecurity->xssCleanPostInput('type'); 
			$fromEmail  =  $this->czsecurity->xssCleanPostInput('fromEmail');
		    $toEmail   =  $this->czsecurity->xssCleanPostInput('toEmail');
			$addCC 	   =  $this->czsecurity->xssCleanPostInput('ccEmail'); 
			$addBCC 	   =  $this->czsecurity->xssCleanPostInput('bccEmail'); 
		    $replyTo   =  $this->czsecurity->xssCleanPostInput('replyEmail'); 
			$message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
			$subject   =  $this->czsecurity->xssCleanPostInput('emailSubject');
			$message = stripslashes(str_replace('{{ logo }}',"<img src='$logo_url'>" ,$message));
			
	
			$date     			 = date('Y-m-d h-i-s');
			$customerID 		= $this->czsecurity->xssCleanPostInput('customertempID');

			$email_data    = array(     'customerID'=>$customerID,
										'resellerID'=>$user_id, 
										'emailSubject'=>$subject,
										'emailfrom'=>$fromEmail,
										'emailto'=>$toEmail,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
										'emailsendAt'=>$date,
										
										);



			 $this->email->clear();
            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from($fromEmail);					
			$this->email->to($toEmail);
			$this->email->subject($subject);
			$this->email->message($message);
			
			 if ($this->email->send()){
				 
				 
			   $this->general_model->insert_row('tbl_template_data', $email_data);
			  $this->session->set_flashdata('message','<div class="alert alert-success"><strong>Success</strong></div>'); 
			  
			 
			 }else{ 
			 
				$this->general_model->insert_row('tbl_template_data', $email_data);
			 	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
			 }
             redirect('home/index','refresh');
 }
 
//-------------------   END -------------------------------//
		
 
	
  public function get_expiry_card_data($customerID, $type){  
  
                       $card = array();
               		   $this->load->library('encrypt');
					   
			  if($type=='1'){ 
			    /**************Expired Card***********/
			    
		     	$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date 
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  "; 
			  }
            
			  if($type=='0'){
				  
				  /**************Expiring SOON Card***********/
				$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where 
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";  
			 			  
			  }	
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->row_array();
					
			        if(!empty($card_data)){  
					      
			             $card['CustomerCard']     = substr($this->encrypt->decode($card_data['CustomerCard']),12); ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['expiry']    = $card_data['expired_date'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->encrypt->decode($card_data['CardCVV']);
						  $card['customerListID'] = $card_data['customerListID'];
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						  
						
					}
					
					return  $card;
					
					
				 
    }
 
	public function create_template_merchant(){

	    if($this->czsecurity->xssCleanPostInput('templateName') !=""){
		
		  	$templateName =  $this->czsecurity->xssCleanPostInput('templateName');
			$message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
			$subject   =  $this->czsecurity->xssCleanPostInput('emailSubject');
		  	$createdAt   = date('Y-m-d H:i:s');
		  	$message  = htmlspecialchars_decode($message);
		 
		  	$insert_data = array('templateName'  =>$templateName,
								 'message'		=>$message,
								 'emailSubject' => $subject,	
		             );
			if($this->czsecurity->xssCleanPostInput('tempID')!='')
			{
				$tempID = $this->czsecurity->xssCleanPostInput('tempID');
				$results = $this->general_model->update_row_data('tbl_email_template_data',array('id'=>$tempID),$insert_data, ['message']);   
			}else{
				$type 	   =  $this->czsecurity->xssCleanPostInput('type'); 
				$insert_data['templateType'] = $type;
				$tempID = $this->general_model->insert_row('tbl_email_template_data',$insert_data, ['message']);  
			}
			redirect('Settingmail/create_template_merchant/'.$tempID); 
	   	}
	 
		
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
	
		
		if($this->uri->segment('3')){
			$temID  				  = $this->uri->segment('3');	

			$condition              = array('id'=>$temID);  
			$data['templatedata']	  = $this->general_model->get_row_data('tbl_email_template_data', $condition);	

		}
		  
		
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('admin_pages/page_merchant_email_template', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	
	   
	}

}



