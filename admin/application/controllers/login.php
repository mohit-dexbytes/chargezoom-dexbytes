<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use \Chargezoom\Core\Http\LoginRequest;
use \Chargezoom\Core\Authentication\Throttle;
use \Chargezoom\Core\Http\SessionRequest;
class Login extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('general_model');
		$this->load->model('admin_panel_model');
		
		define('INTERFACE_TYPE','Admin');
		if ($this->session->userdata('admin_logged_in')) {
			redirect('Admin_panel/index', 'refresh');
		}
	}

	public function index()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$this->load->view('template/template_start', $data);
		$this->load->view('pages/login', $data);
		$this->load->view('template/template_scripts', $data);
		$this->load->view('template/template_end', $data);
	}


	public function user_login_old()
	{
		$email = $this->czsecurity->xssCleanPostInput('login-email');
		$password = $this->czsecurity->xssCleanPostInput('login-password');

		$user = $this->admin_panel_model->get_admin_login($email, $password);	
		if (!empty($user)) {

			$this->general_model->track_user("Admin", $user['adminEmail'], $user['adminID']);
			$this->session->set_userdata('admin_logged_in', $user);
			$this->session->set_flashdata('message', '');
			redirect(base_url('Admin_panel/index', 'refresh'));
		} else {
			$this->session->set_flashdata('message', '<strong>Error:</strong> Login credentials does not match.');
			redirect(base_url('login/login', 'refresh'));
		}

		redirect(base_url('login/user_login'));
	}

	public function user_login()
	{
		if (!$this->czsecurity->csrf_verify()) {
			$this->session->set_flashdata('error', $this->czsecurity->errorMessages('token_expired'));

			redirect(base_url('login/login', 'refresh'));
		}

		$email 		= $this->czsecurity->xssCleanPostInput('login-email');
		$password 	= $this->czsecurity->xssCleanPostInput('login-password');
		$user 		= $this->admin_panel_model->get_admin_login($email, $password);
		$request 	= new LoginRequest($email);
		$throttle 	= new Throttle($this->db, $request);
		$sessionRequest 	= new SessionRequest($this->db);
		try {
			$seconds = $throttle->checkLock();

			if ($seconds) {
				$this->session->set_flashdata(
					'error',
					'You have exceeded the maximum login attempts. You can retry again after ' . $seconds . ' seconds.'
				);

				redirect(base_url('login/login', 'refresh'));
			}
		} catch (\Exception $ex) {
			// For the moment... silence the exception
			// In the future - pass exception to Sentry
		}

		if (!empty($user)) {
			try {
				$throttle->addAttempt(true);
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - pass exception to Sentry
			}

			$this->general_model->track_user("Admin", $user['adminEmail'], $user['adminID']);

			$this->session->set_userdata('admin_logged_in', $user);

			
			/* Update session record */
			$updateSession = $sessionRequest->updateSessionUser(SessionRequest::USER_TYPE_ADMIN,$user['adminID'],$this->session->userdata('session_id'));
			/*End session update*/

			if ($user['userAdminID'] != 0) {
				$mD_array = $user['previlege'];

				if (!in_array('8', $mD_array)) {
					redirect(base_url('Admin_panel/blank_dashboard', 'refresh'));
				}
			}

			redirect(base_url('Admin_panel/index', 'refresh'));
		} else {
			try {
				$throttle->addAttempt(false);
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - throw exception to Sentry
			}

			$this->session->set_flashdata('error', 'Login credentials do not match.');

			redirect(base_url('login/login', 'refresh'));
		}

		redirect(base_url('login/user_login'));
	}



	function enable_admin()
	{

		$code = $this->uri->segment('3');


		if ($code != "") {

			$res = $this->general_model->get_row_data('tbl_admin', array('loginCode' => $code, 'isEnable' => '0'));
			if (!empty($res)) {

				$ins_data = array('loginCode' => '', 'isEnable' => '1');

				$this->general_model->update_row_data('tbl_admin', array('loginCode' => $code, 'isEnable' => '0'), $ins_data);
				$this->session->set_flashdata('message', 'Thanks for verifying your email id. You may login now.');
				redirect(base_url('login#login'), 'refresh');
			} else {
				$this->session->set_flashdata('message', 'You have enabled you login email');
				redirect(base_url('login#login'), 'refresh');
			}
		}
	}


	function user_register()
	{

		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<strong>Error: </strong> '.$this->czsecurity->errorMessages('token_expired'));
			redirect(base_url('login#login'), 'refresh');
		}

		$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
		$email   = $this->czsecurity->xssCleanPostInput('register-email');
		$FirstName = $this->czsecurity->xssCleanPostInput('register-firstname');
		$LastName = $this->czsecurity->xssCleanPostInput('register-lastname');

        $passBcrypt = password_hash(
            $this->czsecurity->xssCleanPostInput('register-password'),
            PASSWORD_BCRYPT
        );

		$datas = array(
			'adminFirstName' => $FirstName,
			'adminLastName' => $LastName,
			'adminEmail' => $this->czsecurity->xssCleanPostInput('register-email'),
			'adminPasswordNew' => $passBcrypt,
			'loginCode' => $code,
			'isEnable'  => '0',
			'date_added' => date('Y-m-d h:i:s')
		);

		$results = $this->general_model->check_existing_user('tbl_admin', array('adminEmail' => $email));
		if ($results) {
			$this->session->set_flashdata('message', '<strong>Error:</strong> Email Already Exitsts.');
		} else {

			$insert = $this->general_model->insert_row('tbl_admin', $datas);
			$this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong>Success:</strong> Thanks, for registering with us. Please check your email and verify your account.');
			$this->load->library('email');

			$subject = 'Verification mail';
			$this->email->from('support@chargezoom.com');
			$this->email->to($email);
			$this->email->subject($subject);
			$this->email->set_mailtype("html");

			
			$message = "<p>Dear $FirstName $LastName,<br><br>Thanks for registering with ChargeZoom. Please verify your email id by clicking following verification link.</p><br> <p><a href=" . base_url('login/enable_admin/' . $code) . ">Click Here</a> </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";
			$this->email->message($message);

			if ($this->email->send()) {
				$this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong>Success:</strong> Thanks, for registering with us. Please check your email and verify your account.');
			} else {
				$this->session->set_flashdata('message', '<strong>Error:</strong> Email was not sent, please contact your administrator.' . $email);
			}
		}

		redirect(base_url('login#login'), 'refresh');
	}


	public function recover_password()
	{
		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<strong>Error: </strong> '.$this->czsecurity->errorMessages('token_expired'));
			redirect(base_url('login#reminder'), 'refresh');
		}

		$email  = $this->czsecurity->xssCleanPostInput('reminder-email');

		$result = $this->general_model->get_row_data('tbl_admin', array('adminEmail' => $email));
		if ($result) {

			$request 	= new LoginRequest($email);
			$throttle 	= new Throttle($this->db, $request, 0, 3600);
			try {
				$attemptCount = $throttle->getForgetAttemptCount(INTERFACE_TYPE);
				if($attemptCount >= 3){
					$this->session->set_flashdata('error', 'Password Reset request limit reached. Request has been locked for an hour.');
					redirect('/', 'refresh');
				}
				
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - pass exception to Sentry
			}

			$this->load->helper('string');
			$code = random_string('alnum', 10);

			$update = $this->admin_panel_model->temp_reset_password($code, $email);
			if ($update) {

				try {
					$throttle->addForgetAttempt(INTERFACE_TYPE);
				} catch (\Exception $ex) {
					// For the moment... silence the exception
					// In the future - pass exception to Sentry
				}

				$emailData = $this->general_model->get_row_data('tbl_template_reseller_data', array('templateType' => '2', 'userTemplate' => 1));

				$message = $emailData['message'];
				$subject = $emailData['emailSubject'];

				if ($emailData['fromEmail'] != '') {
					$fromEmail = $emailData['fromEmail'];
				} else {
					$fromEmail = DEFAULT_FROM_EMAIL;
				}

				$loginURL = "https://".RSDOMAIN."/ai";
				$rsName = "Chargezoom Portal";

				$subject = stripslashes(str_replace('{{reseller_name}}', $rsName, $subject));
				$subject = stripslashes(str_replace('{{reseller_company}}', $rsName, $subject));

				$message = stripslashes(str_replace('{{merchant_name}}', $result['adminCompanyName'], $message));
				$message = stripslashes(str_replace('{{login_url}}', $loginURL, $message));
				$message = stripslashes(str_replace('{{merchant_email}}', $email, $message));
				$message = stripslashes(str_replace('{{merchant_password}}', $code, $message));
				$message = stripslashes(str_replace('{{reseller_name}}', $rsName, $message));
				$message = stripslashes(str_replace('{{reseller_company}}', $rsName, $message));

				$isMailSend = sendMailBySendgrid($email,$subject, $fromEmail, $rsName, $message, DEFAULT_FROM_EMAIL);
				if ($isMailSend) {
					$this->session->set_flashdata('forget-success', 'Send');
				} else {
					$this->session->set_flashdata('message', '<strong>Error:</strong> Email was not sent, please contact your administrator.');
				}
			}

			redirect('login', 'refresh');
		} else {
			$this->session->set_flashdata('message', '<strong>Error:</strong> Email not available.');
			redirect('login#reminder', 'refresh');
		}
	}


	public function admin_logout()
	{
		redirect(base_url('login/user_login'));
	}
}
