<?php

class Subscription extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('admin_panel_model');
		$this->load->model('company_model');
		$this->db1= $this->load->database('otherdb', TRUE);
		if($this->session->userdata('admin_logged_in'))
		{
		
		}else{
		    
		    	redirect('login', 'refresh');
		}
	}
	
	
	public function index(){
		
	}
	
	
	
	
	
			
/********** Add, Update and Edit Admin Users ********/
	
	
	public function admin_user()
	{
    

	 $data['primary_nav']  = primary_nav();
     $data['template']   = template_variable();
     $data['login_info'] = $this->session->userdata('admin_logged_in');
     $user_id    = $data['login_info']['adminID'];
	 
      $users = $this->general_model->get_table_data('tbl_admin',array('userAdminID'=>$user_id));
	  $usernew = array();
	  foreach($users as $key=>$user)
	  {
		 $auth = $this->general_model->get_admin_auth_data($user['adminID']);
         $user['authName'] = (!empty($auth))?$auth['authName']:'';
         $usernew[$key] = $user;		   
	  }
	 
	 $data['user_data']  =$usernew;
	 
     $this->load->view('template/template_start', $data);
     $this->load->view('template/page_head', $data);
     $this->load->view('admin_pages/admin_user', $data);
     $this->load->view('template/page_footer',$data);
     $this->load->view('template/template_end', $data);
	 
 
	}
	
			
    public function create_user()
	{
	    	        $data['login_info']     = $this->session->userdata('admin_logged_in');
					$user_id = $data['login_info']['adminID'];
	    
	 if(!empty($this->input->post(null, true))){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');			
			$this->form_validation->set_rules('userFname', 'userFname', 'required');			
			$this->form_validation->set_rules('userLname', 'userLname', 'required');
	        $this->form_validation->set_rules('userCompany', 'Company', 'required');
			
		
	if ($this->form_validation->run() == true)
		{
			
			$input_data['adminFirstName']	   = $this->czsecurity->xssCleanPostInput('userFname');
			$input_data['adminLastName']	   = $this->czsecurity->xssCleanPostInput('userLname');
	       	$input_data['adminEmail']	   = $this->czsecurity->xssCleanPostInput('userEmail');
			$input_data['adminCompanyName']	   = $this->czsecurity->xssCleanPostInput('userCompany');
			
			if($this->czsecurity->xssCleanPostInput('userID')=="" ){
			$input_data['adminPasswordNew']    = password_hash($this->czsecurity->xssCleanPostInput('userPassword'), PASSWORD_BCRYPT);
			}
			$input_data['userAdminID']	   = $user_id;
			
		  $roles = $this->czsecurity->xssCleanPostInput('role');
		
		if($this->czsecurity->xssCleanPostInput('userID')!="" )
		{
				      
				         $id = $this->czsecurity->xssCleanPostInput('userID');
						 $chk_condition = array('adminID'=>$id,'userAdminID'=>$user_id);
						 
						$updt = $this->general_model->update_row_data('tbl_admin',$chk_condition, $input_data);
					        	if($updt)
					        	{
					        	    $this->general_model->delete_row_data('tbl_admin_role',array('adminUserID'=>$id));
					        	     foreach($roles as $role)
                            		  {
                            		      $rol_data['adminUserID'] = $id;
                            		      $rol_data['authID'] = $role;
                            		      $rol[] = $rol_data;
                            		  }
                            	
						           $uss = $this->general_model->insert_batch_rows('tbl_admin_role',$rol);
    	                            $this->session->set_flashdata('message', '<strong> Successfully Updated</strong>');  
    	                          }
    	                          else
    	                          {
                                 $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');        
    	                         }	  
			    	}
					else
					{
						 $insert = $this->general_model->insert_row('tbl_admin', $input_data); 
						 if($insert){
						     $rol=array();
						               foreach($roles as $role)
                            		  {
                            		      $rol_data['adminUserID'] = $insert;
                            		      $rol_data['authID'] = $role;
                            		      $rol[] = $rol_data;
                            		  }
                            	
						           $uss = $this->general_model->insert_batch_rows('tbl_admin_role',$rol);
    	                            $this->session->set_flashdata('message', '<strong>Successfully Created New User</strong>');  
    	                          }else{
                                 $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');        
    	                         }	 
					}
					
		  			
					
					   redirect(base_url('Subscription/admin_user'));
		

		}
	}
						
               if($this->uri->segment('3')){
					   $userID  			  = $this->uri->segment('3');	
					  $con                = array('adminID'=> $userID);  
					  $data['user'] 	  = $this->general_model->get_select_data('tbl_admin',array('adminID','adminEmail','adminFirstName','userAdminID','adminLastName','adminCompanyName'), $con);	
					  $auth_data  = $this->general_model->get_admin_auth_data($userID);
						$data['authvals'] =explode(',',$auth_data['authValue']);
				}
							 
					$data['primary_nav'] 	= primary_nav();
					$data['template'] 		= template_variable();
					  $con1                = array('adminUserID'=> $user_id);  	
					
		               $data['auths'] = $this->general_model->get_table_data('tbl_admin_auth',''); 			 
					$this->load->view('template/template_start', $data);
					$this->load->view('template/page_head', $data);
					$this->load->view('admin_pages/adduser', $data);
					$this->load->view('template/page_footer',$data);
					$this->load->view('template/template_end', $data);
 }
	
	
	/**************Delete Admin Users********************/
	
	public function delete_user()
	{
	    
        $data['login_info']     = $this->session->userdata('admin_logged_in');
		$user_id = $data['login_info']['adminID'];
	
		 $userID = $this->czsecurity->xssCleanPostInput('adminID11');
		 $condition =  array('userAdminID'=>$user_id,'adminID'=>$userID); 
		 
		 $del      = $this->general_model->delete_row_data('tbl_admin', $condition);
      if($del){
	            $this->session->set_flashdata('message', '<strong> Successfully Deleted</strong>');  
	  }else{
                $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong.');        
	  }	  
	  
	  	redirect(base_url('company/MerchantUser/admin_user'));
    	 
	
	}
    	 
    	 
	
	
	public function get_card_data(){  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	    
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c "; 
				
				$query1 = $this->db1->query($sql);
						if($query1 -> num_rows() > 0)

						return $query1->result_array(); 

						else

					 	return false;
				 

       }
	
public function subscription_details(){
		

		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				
		        $data['subscription_list']   = $this->admin_panel_model->get_subscription_listing(); 
				$this->load->view('template/template_start', $data);
			    $this->load->view('template/page_head', $data);
				$this->load->view('admin_pages/subscription_detail', $data);
			    $this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
		
	}	
	
	public function invoice_details(){
		

		        $data['primary_nav'] 	= primary_nav();
				$data['template'] 		= template_variable();
				
				 $sbID = $this->uri->segment('3');
				 
			    $data['card_list'] =   $this->get_card_data();
	            $data['subscription']		= $this->admin_panel_model->get_invoice_listing($sbID);
				$this->load->view('template/template_start', $data);
			    $this->load->view('template/page_head', $data);
				$this->load->view('admin_pages/invoice_details', $data);
			    $this->load->view('template/page_footer',$data);
				$this->load->view('template/template_end', $data);
		
	}	
	
	
	public function create_admin_subscription(){
		
		      $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  
	if(!empty($this->input->post(null, true))){
			
			
			$input_data['subscriptionName']	   = $this->czsecurity->xssCleanPostInput('sub_name');
			$input_data['merchantID']	   = $this->czsecurity->xssCleanPostInput('merchantID');
			$merchantID = $input_data['merchantID'];
			$input_data['subscriptionStartDate']	   = $this->czsecurity->xssCleanPostInput('sub_start_date');
			$input_data['invoiceGenerationDate']	   = $this->czsecurity->xssCleanPostInput('invoice_date');
			$input_data['serviceInvoiceGenerationDate']	   = $this->czsecurity->xssCleanPostInput('invoice_date');
			$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');
			$input_data['cardID']	   = $this->czsecurity->xssCleanPostInput('card_list');
			$input_data['merchantAddress1']	   = $this->czsecurity->xssCleanPostInput('address1');
			$input_data['merchantAddress2']	   = $this->czsecurity->xssCleanPostInput('address2');
			$input_data['merchantCountry']	   = $this->czsecurity->xssCleanPostInput('country');
			$input_data['merchantState']	   = $this->czsecurity->xssCleanPostInput('state');
			$input_data['merchantCity']	   = $this->czsecurity->xssCleanPostInput('city');
			$input_data['merchantZipCode']	   = $this->czsecurity->xssCleanPostInput('zipcode');
			$input_data['merchantContact']	   = $this->czsecurity->xssCleanPostInput('phone');   
              if($this->czsecurity->xssCleanPostInput('card_number')!=""){
                 $card =array();
			     	$friendlyName = $this->czsecurity->xssCleanPostInput('friendlyname');
				       $card['card_number']  = $this->czsecurity->xssCleanPostInput('card_number'); 
						 $card['expiry']     = $this->czsecurity->xssCleanPostInput('expiry');
						$card['expiry_year']      = $this->czsecurity->xssCleanPostInput('expiry_year');
						$card['cvv']         = $this->czsecurity->xssCleanPostInput('cvv');
						$card['friendlyname']= $friendlyName;
						$card['merchantID']  = $merchantID; 
						 
						$card_data = $this->check_friendly_name($merchantID, $friendlyName);
					
				 if(!empty($card_data)){
				 	  $this->update_card($card, $merchantID,$friendlyName );
					  $merchantCardID  = $card_data['merchantCardID'];
					  $input_data['cardID'] = $merchantCardID;
				 }else{
				        
                    				     
						$merchantCardID = $this->insert_new_card($card);
						$input_data['cardID'] = $merchantCardID;
				 }
				}	
				
		if($this->czsecurity->xssCleanPostInput('subID')!="" ){
				      
				         $id = $this->czsecurity->xssCleanPostInput('subID');
						 $chk_condition = array('subscriptionID'=>$id);
						 $this->general_model->update_row_data('merchant_subscription',$chk_condition, $input_data);
			    	}
		else
					{
					    
					    
					    
					    $mdata =$this->general_model->get_select_data('tbl_merchant_data',array('merchID, freeTrial'), array('merchID'=>$merchantID)); 
					    if($mdata['freeTrial']=='1')
					    {
					      	$input_data['remainingDays']	   = 30;
		                	$input_data['serviceremainingDays']	   = 30;  
					    }
					    
						 $this->general_model->insert_row('merchant_subscription', $input_data); 
						 $this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong> Successfully Created</strong>');
					}
					   redirect(base_url('Subscription/subscription_details'));
		

		}
	
			  
			  $data['login_info'] = $this->session->userdata('admin_logged_in');

			  $merchantdata				= $this->customer_model->get_merchant_data();
			  $data['merchants']		= $merchantdata	;

               $gateway				= $this->customer_model->get_admin_gateway();
			  $data['gateways']		= $gateway	;			  
			
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('admin_pages/create_admin_subscription', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);

	}
	
	
	
	
	public function terminate_subscription(){
	
	      $subscID = $this->czsecurity->xssCleanPostInput('subscID');
		  $res = $this->admin_panel_model->get_row_data('merchant_subscription',array('subscriptionID'=>$subscID, 'isDelete'=>'0' )) ;
		  if(!empty($res)){
			  
			  $ins_data = array('subscriptionID'=>$subscID, 'isDelete'=>'2');
			  
			  $this->admin_panel_model->update_row_data('merchant_subscription',array('subscriptionID'=>$subscID, 'isDelete'=>'0' ), $ins_data);
			  $this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong> Successfully Terminated</strong>');
			  redirect(base_url('Subscription/subscription_details'));
			  
		  }else{
		       $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong...');
			   redirect(base_url('Subscription/subscription_details'));
		        
		  }
	
	}
	
	
	
	 public function delete_merchant_subscription(){
          
          
           $subID  = $this->czsecurity->xssCleanPostInput('merchantID');
	      $res = $this->admin_panel_model->get_row_data('merchant_subscription',array('subscriptionID'=>$subID, 'merchantStatus'=>'0' )) ;	
		  if(!empty($res)){
			  
			  $ins_data = array('subscriptionID'=>$subID, 'merchantStatus'=>'1');
			  
			  $this->admin_panel_model->update_row_data('merchant_subscription',array('subscriptionID'=>$subID, 'merchantStatus'=>'0' ), $ins_data);
			  $this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong> Successfully Suspend</strong>');
			  redirect(base_url('Subscription/subscription_details'));
			  
		  }else{
		       $this->session->set_flashdata('message', '<strong>Error:</strong> Something Is Wrong...');
			   redirect(base_url('Subscription/subscription_details'));
		        
		  }
	
	}	
	
	
	 public function activate_merchant_subscription(){
          
          
           $subID  = $this->czsecurity->xssCleanPostInput('merchantActiveID');
	      $res = $this->admin_panel_model->get_row_data('merchant_subscription',array('subscriptionID'=>$subID, 'merchantStatus'=>'1' )) ;	
		  if(!empty($res)){
			  
			  $ins_data = array('subscriptionID'=>$subID, 'merchantStatus'=>'0');
			  
			  $this->admin_panel_model->update_row_data('merchant_subscription',array('subscriptionID'=>$subID, 'merchantStatus'=>'1' ), $ins_data);
			  $this->session->set_flashdata('message', '<i class="fa fa-check"></i><strong> Successfully Activated</strong>');
			  redirect(base_url('Subscription/subscription_details'));
			  
		  }else{
		       $this->session->set_flashdata('message', '<strong>Error:</strong>Something Is Wrong...');
			   redirect(base_url('Subscription/subscription_details'));
		        
		  }
	
	}	
	
	
	
	 
	 	  public function check_friendly_name($mid, $cfrname){
		         $card = array();
               		  $this->load->library('encrypt');
				      

				$query1 = $this->db1->query("Select * from merchant_card_data where merchantListID = '".$mid."' and merchantFriendlyName ='".$cfrname."' ");
					$card = $query1->row_array();	
					
						return $card;
		
	}	

	
    public function insert_new_card($card){
		         
               		   $this->load->library('encrypt');
			    
					 
				    	$card_no  = $card['card_number']; 
						$expmonth = $card['expiry'];
						$exyear   = $card['expiry_year'];
						$cvv      = $card['cvv'];
						$friendlyname = $card['friendlyname'];
				        $merchantID   = $card['merchantID'];
				 
			    	      
						$insert_array =  array( 'CardMonth'  =>$expmonth,
										   'CardYear'	 =>$exyear, 
										  'MerchantCard' =>$this->encrypt->encode($card_no),
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										 'merchantListID'     =>$merchantID,
										 'merchantFriendlyName'=>$friendlyname,
										);
					
					$this->db1->insert('merchant_card_data', $insert_array);
					return $this->db1->insert_id();;
					
		
	}	
	
	
	
    public function update_card($card,$mid, $cfrname){
               		   $this->load->library('encrypt');
				
				    	$card_no  = $card['card_number']; 
						$expmonth = $card['expiry'];
						$exyear   = $card['expiry_year'];
						$cvv      = $card['cvv'];
						 
						$update_data =  array( 'CardMonth'  =>$expmonth,
										   'CardYear'	 =>$exyear, 
										  'MerchantCard' =>$this->encrypt->encode($card_no),
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										 'updatedAt' 	=> date("Y-m-d H:i:s") );					
						$this->db1->where('merchantListID', $mid);
						$this->db1->where('merchantFriendlyName', $cfrname);
						$this->db1->update('merchant_card_data', $update_data);
						return true;
		
	}	
	
	
  public function get_card_expiry_data($merchantID){  
  
                       $card = array();
               		   $this->load->library('encrypt');
					
	   	     
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantListID='$merchantID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $customer_card['CardNo']  = substr($this->encrypt->decode($card_data['MerchantCard']),12) ;
						 $customer_card['merchantCardID'] = $card_data['merchantCardID'] ;
						 $customer_card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
						 $card[$key] = $customer_card;
					   }
				}		
					
                return  $card;

     }
 
	
	 public function check_vault(){
		 
		 
		  $card=''; $card_name=''; $merchantdata=array();
		 if($this->czsecurity->xssCleanPostInput('merchantID')!=""){
			 
				 $merchantID 	= $this->czsecurity->xssCleanPostInput('merchantID');  
			

			 	$condition     =  array('merchID'=>$merchantID); 
			    $merchantdata = $this->customer_model->get_single_merchant_data($merchantID);
				if(!empty($merchantdata)){
	                 				 
				   	 $merchantdata['status'] =  'success';	     
					
					 $card_data =   $this->get_card_expiry_data($merchantID);
					$merchantdata['card']  = $card_data;
					echo json_encode($merchantdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

	  
	
   

}