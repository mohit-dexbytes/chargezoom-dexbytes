<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->model('admin_panel_model'); 
	}


	public function index()

	{  
		$data['title'] = 'Logout';

		$status = $this->admin_panel_model->admin_logout();

		 redirect('login', 'refresh'); 
	}
	
	

}



