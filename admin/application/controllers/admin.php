<?php if (!defined('BASEPATH')) die();
class Admin extends CI_Controller {
	
	function __construct()	
	{
		parent::__construct();
		$this->load->model(array('common_model'));
		$this->load->library('tank_auth');
		$this->tank_auth->islog($this->tank_auth->is_logged_in());
	}
	
	
	public function index()
	{
		$data['primary_nav'] 	= primary_nav();
		$data['templates'] 		= template_variable();
		$this->template
				->set_layout('admin')
				->build('admin/index',$data);
	}
	
	public function get_quote()
	{
		$data['quote'] = $this->common_model->get_single_content('*','quote','id',$this->czsecurity->xssCleanPostInput('id'));
		$this->load->view('single_quote',$data);
	}
	
	public function edit_action()
	{
		$data = array(
				'quote_cost' => $this->czsecurity->xssCleanPostInput('quote_name',TRUE)
					);
		$this->common_model->edit('quote',$data,'id',$this->czsecurity->xssCleanPostInput('id_quote'),'Quote Updated','Error Updating');
		
	}
	
	public function send()
	{
		$data['quote'] = $this->common_model->get_single_content('*','quote','id',$this->czsecurity->xssCleanPostInput('id'));
		$query_m_send = "select * from message 
			where user_id = '".$this->tank_auth->get_user_id()."' and message_type = 'send quote'";
		$data['mquote'] = $this->common_model->single_result_query($query_m_send);
		$this->load->view('send_view',$data);
	}
	
	public function sendfile_view()
	{
		$query_f_send = "select * from message 
			where user_id = '".$this->tank_auth->get_user_id()."' and message_type = 'send file'";
		$data['mfile'] = $this->common_model->single_result_query($query_f_send);
		$data['quote'] = $this->common_model->get_single_content('*','quote','id',$this->czsecurity->xssCleanPostInput('id'));
		$this->load->view('sendfile_view',$data);
	} 
	
	public function export()
	{
		$this->excel->setActiveSheetIndex(0);
                //name the worksheet
                $this->excel->getActiveSheet()->setTitle('Report');
                //set cell A1 content with some text
                $this->excel->getActiveSheet()->setCellValue('A1', 'List of Quoted');
                //merge cell A1 until C1
                $this->excel->getActiveSheet()->mergeCells('A1:C1');
                //set aligment to center for that merged cell (A1 to C1)
                $this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                //make the font become bold
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
                $this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
                $this->excel->getActiveSheet()->getStyle('A1')->getFill()->getStartColor()->setARGB('#333');
       for($col = ord('A'); $col <= ord('C'); $col++){  
                 //change the font size
                $this->excel->getActiveSheet()->getStyle(chr($col))->getFont()->setSize(12);
                 
                $this->excel->getActiveSheet()->getStyle(chr($col))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
                //retrive contries table data
                $query = "SELECT id,name,email,phone,quote_cost,date,note,design_name,logo_position,machine_format,fabric_type,width,height,unit_type FROM quote WHERE date BETWEEN '" . $this->session->userdata('from') . "' AND  '" . $this->session->userdata('to') . "'
ORDER by id DESC"; 
                $rs = $this->db->query($query);
                $exceldata="";
        foreach ($rs->result_array() as $row){
                $exceldata[] = $row;
        }
                //Fill data 
                $this->excel->getActiveSheet()->fromArray($exceldata, null, 'A4');
                 
                $this->excel->getActiveSheet()->getStyle('A4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                 
                $filename='Quote_report.xls'; //save our workbook as this file name
                header('Content-Type: application/vnd.ms-excel'); //mime type
                header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
                header('Cache-Control: max-age=0'); //no cache
 
                //save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
                //if you want to save it as .XLSX Excel 2007 format
                $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
                //force user to download the Excel file without writing it to server's HD
                $objWriter->save('php://output');
	}
	
	public function get_data()
	{
		$data['info'] = $this->common_model->get_single_content('*','quote','id',$this->czsecurity->xssCleanPostInput('id'));
		$this->load->view('quote_view',$data);
	}
	
	public function delete_quote()
	{
		$this->common_model->delete_contents('quote','id',$this->czsecurity->xssCleanPostInput('id'));
	}

}

/* End of file frontpage.php */
/* Location: ./application/controllers/frontpage.php */
