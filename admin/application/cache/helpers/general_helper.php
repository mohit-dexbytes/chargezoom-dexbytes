<?php
	function mymessage($message)
	{
		echo '
		<div class="alert alert-info">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$message.'
		</div>
		';
	}

 
	function get_services()
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_table('*', 'services');
		return $service;
	}


	function get_services_by_id($id)
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_allservices_by_id($id);
		return $service;
	}	 

?>