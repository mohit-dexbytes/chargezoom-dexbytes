
<!-- Page content -->
<div id="page-content">
    
    
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i> Merchant Gateway <br> <small>You can manage your gateway information here. </small>
            </h1>
	        <div class="msg_data">
			    <?php echo $this->session->flashdata('message');   ?>
		    </div>
        </div>
    </div>
  
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
		<div class="block">  
		    
		  
					    <div class="block-options pull-right">
                            <div class="btn-group">
                               <a href="#add_gateway" class="btn btn-sm btn-success"  onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create New" >Add New</a>
                            </div>
                        </div> 
					
					
					
					<!-- All Orders Content -->
        <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="visible-lg text-left">Gateway </th>
                    <th class="visible-lg text-left">Merchant ID</th>
					<th class="visible-lg text-left">Gateway Name </th>
					<th class="visible-lg text-left">User Name</th>
					<th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>
			
			
			
			<?php 
				if(isset($gateways) && $gateways)
				{
					foreach($gateways as $gateway)
					{
						
						
				?>
				<tr>
					
					<td class="text-left visible-lg"><?php if($gateway['gatewayType']=='1'){echo"NMI";} else if($gateway['gatewayType']=='2'){echo"Authorize.Net";} else{
					echo"Pay Trace";} ?> </a> </td>
					
					<td class="text-left visible-lg"><?php echo $gateway['gatewayMerchantID'];?> </a> </td>
					
					<td class="text-left visible-lg"><?php echo $gateway['gatewayFriendlyName']; ?> </a> </td>
					
                    <td class="text-left visible-lg"><?php echo  $gateway['gatewayUsername']; ?> </a> </td>
				
					
				<td class="text-center">
					<div class="btn-group btn-group-sm">
						
						<a href="#edit_gateway" class="btn btn-default" onclick="set_edit_gateway('<?php echo $gateway['gatewayID'];  ?>');" title="Edit Details" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>  
              
						<a href="#del_gateway" onclick="del_gateway_id('<?php echo $gateway['gatewayID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
				</div>
					
		   </td>
		</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        </br>
    
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>



     <!------------ Add popup for gateway   ------->

 <div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/create_gateway">
			  
			<input type="hidden" id="gatewayID" name="creditID" value=""  />
			
		       <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />
								
						</div>
						
					</div>

					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt"  class="form-control">
								   <option value="" >Select Gateway</option>
								   <option value="1" >Chargezoom</option>
								   <option value="2" >Authorize.Net</option>
								   <option value="3" >Pay Trace</option>
							</select>
							</div>
					</div>
					
			<div id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="Username..">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="password" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="Password..">
                           
                        </div>
                    </div>
				</div>
				
               <div id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username..">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
					
				</div>	
				
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  placeholder="Merchant ID">
                           
                        </div>
						
                   </div>

                  
	         <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>
					
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
             </div> 
			
	   </form>		
                
            </div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>

</div>



<!-------------------------- Modal for Edit Gateway ------------------------------>

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
		
			 <h2 class="modal-title text-center"> Edit Gateway </h2>
			 <hr/>
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/update_gateway">
			 
			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />
			
               
		    <div class="col-md-12">   
                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="">
							</div>
			     </div>
          </div>  

		 <div class="col-md-12">   
                    <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>

          </div> 

		  
		  <div id="nmi_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="password" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="Password..">
                           
                        </div>
                    </div>
				</div>	
                 <div id="auth_div1" style="display:none">					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
				</div>	
				
			<div id="pay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="PayTrace  Username..">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
				</div>	
				
		         <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="mid"  name="mid" class="form-control"  value="" placeholder=""> 
						</div>
			      </div>
             </div> 
							
	            <div class="form-group">
					<div class="col-md-4 pull-right">
					
					<button type="submit" class="submit btn btn-sm btn-success"> Update </button>
					
					<button  type="button" align="right" class="btn btn-sm  btn-danger close1" data-dismiss="modal"> Cancel </button>
                   
                   </div>
            </div>
	 </form>	
	<!--------- END ---------------->
		</div>

	</div>
 
 </div>

     <!------- Modal for Delete Gateway ------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway" method="post" action='<?php echo base_url(); ?>home/delete_gateway' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Gateway ? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantgatewayid" name="merchantgatewayid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Yes"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merch_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();





function del_gateway_id(id){
	
	     $('#merchantgatewayid').val(id);
}


function set_edit_gateway(gatewayid){
	
	
	if(gatewayid !=""){
		
    
     $.ajax({
		url: '<?php echo base_url("home/get_gatewayedit_id")?>',
		type: 'POST',
		data:{gatewayid:gatewayid},
		dataType: 'json',
		success: function(data){
		
		 $('#gatewayEditID').val(data.gatewayID);		
			 
	          
			  if(data.gatewayType=='1')
			  {
				  $('#nmi_div1').show();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
		          $('#nmiUser1').val(data.gatewayUsername);
			      $('#nmiPassword1').val(data.gatewayPassword);
              }
			  
				if(data.gatewayType=='2')
				{
			      $('#nmi_div1').hide();
				  $('#auth_div1').show();
				  $('#pay_div1').hide();
				  $('#apiloginID1').val(data.gatewayUsername);
			      $('#transactionKey1').val(data.gatewayPassword);
				}
				
				if(data.gatewayType=='3')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').show();
		          $('#paytraceUser1').val(data.gatewayUsername);
			      $('#paytracePassword1').val(data.gatewayPassword);
				}		
			  
				$('#fname').val(data.gatewayFriendlyName);
				$('#gateway').val(data.gateway);
				$('#mid').val(data.gatewayMerchantID);
			 
			 
	}	
  }); 
	
  }

}


$(function(){  
     
	  $('#gateway_opt').change(function(){
		    var gateway_value =$(this).val();
           if(gateway_value=='3'){			
		       $('#pay_div').show();
			   $('#auth_div').hide();
				$('#nmi_div').hide();
		   }else if(gateway_value=='2'){
				$('#auth_div').show();
				$('#pay_div').hide();
				$('#nmi_div').hide();
		   }else{
			   $('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').show();		
           }
	  }); 
	  
	  
  $('#nmiform').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			
		 'gateway_opt':{
				 required: true,
		 },	
		 
		  'nmiUser': {
                 required: true,
                 minlength: 3
                },
			'nmiPassword':{
				 required : true,
				 minlength: 5,
				},
		 
		 
		 
		 
			'apiloginID':{
				required: true,
                 minlength: 3
			},
			'transactionKey':{
				required: true,
                 minlength: 3
			},		 
		 
			'frname':{
				 required: true,
				 minlength: 2
			},	
		 
			'nmiUser1': {
                 required: true,
                 minlength: 3
             },
			'nmiPassword1':{
				 required : true,
				 minlength: 5,
				},
		 
		 
			'apiloginID1':{
				required: true,
                 minlength: 3
			},
			
			'transactionKey1':{
				required: true,
                 minlength: 3
			},
		 
		 
			'paytracePassword':{
				required: true,
                minlength: 5
			},
			
			'paytraceUser':{
				required: true,
                 minlength: 3
			},
		 
			'paytracePassword1':{
				required: true,
                minlength: 5
			},
			'paytraceUser1':{
				required: true,
                minlength: 3
			},
		 
			'fname':{
			 required:true,
			 minlength:2,
			}
		 
			
	 },
  });

  
	$('#nmiform1').validate({ // initialize plugin
		 ignore:":not(:visible)",			
		 rules: {
			
		  'gateway_opt':{
				 required: true,
		  },	
		 
		  'nmiUser': {
                 required: true,
                 minlength: 3
          },
		  
		  'nmiPassword':{
				 required : true,
				 minlength: 5,
		  },
		 
		  'apiloginID':{
				required: true,
                minlength: 3
		  },
		  
		  'transactionKey':{
				required: true,
                minlength: 3
		  },		 
		 
		  'frname':{
				 required: true,
				 minlength: 2
		  },	
		 
		  'nmiUser1': {
                 required: true,
                 minlength: 3
           },
			
			'nmiPassword1':{
				 required : true,
				 minlength: 5,
		    },
		 
         'apiloginID1':{
				required: true,
                 minlength: 3
		 },
		 
		'transactionKey1':{
				required: true,
                 minlength: 3
		 },
		 
		'paytracePassword':{
				required: true,
                 minlength: 3
		 },
		 
		'paytraceUser':{
				required: true,
                 minlength: 3
		 },
		 
        'paytracePassword1':{
				required: true,
                 minlength: 5
		 },
		 
		'paytraceUser1':{
				required: true,
                 minlength: 3
		 },
		 
		 'fname':{
			 required:true,
			 minlength:2,
		 }
		 
		 
	 },
  });

	
       var GetMyRemote=function(element_name){
	        $('#update_btn').attr("disabled", true); 
            remote={
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Payments/testNMI',
                    type: "post",
					data:
                        {
                            nmiuser: function(){return $('input[name=nmiUser]').val();},
                            nmipassword:  function(){return $('input[name=nmiPassword]').val();}
                        },
                          dataFilter:function(response){
							
						   data=$.parseJSON(response);
                             if (data['status'] === 'true') {
                                  
									 
							    $('#update_btn').removeAttr('disabled');
						     	$('#nmiPassword').removeClass('error');	 
								 $('#nmiPassword-error').hide();	 
								  
									   return data['status'] ;				   
									 
                              } else {
                                
								   if(data['status'] =='false'){
                                       $('#nmiform').validate().showErrors(function(){
										   
                                                       return {key:'nmiPassword'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }
 });
 
</script>


