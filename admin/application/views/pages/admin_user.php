
<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
	           <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>
        <!-- All Orders Title -->
        <div class="block-title">
             <h2><strong>Admin</strong> Users </h2>
			 
            <div class="block-options pull-right">
                            <div class="btn-group">
							
							
							
							
						   <a class="btn btn-sm  btn-success" title="Create User"  href="<?php echo base_url(); ?>MerchantUser/create_user">Add New</a>
                                
                            </div>
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
					<th class="visible-lg text-left"> User Name </th>
					<th class="visible-lg text-left"> Role Name </th>
					<th class="visible-lg text-left"> Email ID</th>
                    <th class="text-left visible-lg"> Powers </th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($user_data) && $user_data)
				{
					foreach($user_data as $user)
					{
					

				?>
				<tr>
				
					<td class="text-left visible-lg"> <?php echo $user['userFname']; ?></td>
					<td class="text-left visible-lg"> <?php echo $user['roleName']; ?></td>
					<td class="text-left visible-lg"> <?php echo $user['userEmail']; ?></td>
					
                    <td class="text-left visible-lg"><?php echo implode(', ',$user['authName']); ?> </td>
					
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
						
						<a href="<?php echo base_url('MerchantUser/create_user/'.$user['merchantUserID']); ?>" data-toggle="tooltip" title="Edit User" class="btn btn-default"><i class="fa fa-edit"> </i> </a>
							
                        <a href="#del_user" onclick="del_user_id('<?php  echo $user['merchantUserID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete User" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
							
					</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->
