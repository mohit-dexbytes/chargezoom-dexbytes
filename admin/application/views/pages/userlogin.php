 
<!-- Login Alternative Row -->
<div class="container">
	<div class="row">
		
		<div class="col-md-4 col-md-offset-4">
		      <h1 class="push-top-bottom">
					<img src="<?php echo base_url(IMAGES); ?>/login_logo.png" alt="avatar"><br>
					<small>Welcome to User!</small>
				</h1
			<!-- Login Container -->
			<div id="login-container">
				<!-- Login Title -->
				<div class="login-title text-center">
					<h1><strong>Login</strong></h1>
				</div>
				<!-- END Login Title -->
<style> .error{color:red; }</style> 
				<!-- Login Block -->
				<div class="block push-bit">
				
					<?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
					?> 
					
					<!-- Login Form -->
					<form action="<?php echo base_url('user/login/user_login'); ?>" method="post" id="login_form" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"> <i class="gi gi-envelope"> </i> </span>
									<input type="text" id="userEmail" name="userEmail"  class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" id="userPassword" name="userPassword" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-8 text-right">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-angle-right "></i> Login to User</button>
							</div>
						</div>
						
					</form>
					
				</div>
				<!-- END Login Block -->
			</div>
			<!-- END Login Container -->
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>

<script>
$(document).ready(function(){

 
    $('#login_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       
				'userEmail': {
                        required: true,
                        email: true,
                         
                    },
                    'userPassword': {
                        required: true,
                        minlength:5,
                    },
			
                    
			},
    });
});	    
		
	

</script>