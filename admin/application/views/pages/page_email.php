	<div id="page-content">
      <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i>E-mail<br><small>Personalization.</small>
            </h1>
	        
        </div>
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    </div>
    
    
    
    
    
    <div class="block full">
        <!-- Working Tabs Title -->
        <div class="block-title">
            <h2>E-mail <small> Personalization</small></h2>
			              
        </div>
        <!-- END Working Tabs Title -->

        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                <div class="block full">
                    <!-- Block Tabs Title -->
                    
                    <div class="block-title">
                         <div class="block-options pull-right">
                          
                               
                                <a class="btn btn-sm btn-success"  data-toggle="tooltip" title=""   href="<?php echo base_url(); ?>Settingmail/create_template">Add New</a>
                                
                            
                        </div>
                        <ul class="nav nav-tabs " data-toggle="tabs">
                            <li class="active"><a href="#emailtemplate">Emails</a></li>
                            <li><a href="#emailtemplateasign">Assignment</a></li>
                           
                        </ul>
                    </div>
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="emailtemplate">
                        
                         <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                    <th class="text-left">Name</th>
                    <th class="visible-lg text-left">Subject</th>
                    <th class="text-left hidden-xs">Type</th>
                    <th class="hidden-xs text-center">Action</th>
                   
                </tr>
            </thead>
            <tbody>
			
			 
                <?php 
				
				
				
				
				 if(!empty($templates)){   foreach($templates as $template){  ?>
					<tr>
					  <td class="cust_view"><a href="<?php echo base_url(); ?>Settingmail/create_template/<?php echo $template['templateID'];  ?>"><?php echo $template['templateName'];  ?></a></td>
                        <td><?php echo $template['emailSubject'];  ?></td>
                        <td> <?php echo $template['typeText'];  ?></td>
                        <td class="text-center">
                            <div class="btn-group btn-group-sm">
                            <a href="#teplate_view" class="btn btn-default"   onclick="view_template_details('<?php echo $template['templateID'];  ?>');" title="Preview"  data-backdrop="static" data-keyboard="false" data-toggle="modal"><i class="fa fa-eye"></i></a>
                                 <a href="<?php echo base_url(); ?>Settingmail/create_template/<?php echo $template['templateID'];  ?>" data-toggle="tooltip" title="Edit" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                  <a href="<?php echo base_url(); ?>Settingmail/copy_template/<?php echo $template['templateID'];  ?>" data-toggle="tooltip" title="Copy" class="btn btn-primary"><i class="fa fa-clone"></i></a>
                                   <a href="#teplate_del" class="btn btn-danger"   onclick="delete_template_details('<?php echo $template['templateID'];  ?>');" title="Delete"  data-backdrop="static" data-keyboard="false" data-toggle="modal"><i class="fa fa-times"></i></a>
                               
                            </div>
                        </td>
				</tr>
				
				
			<?php }	} 
			else { echo'<tr><td colspan="4"> No Records Found </td></tr>'; }  
			?>
             
             
				
			</tbody>
        </table>
                        </div>
                        <div class="tab-pane" id="emailtemplateasign">Profile..</div>
                      
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Block Tabs -->
            </div>
           
        </div>
        <!-- END Working Tabs Content -->
    </div>
    
</div>    
     
<div id="set_email_tag" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Template Guide</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
              
		    	 <div class="form-group ">
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-7"  >
                                          <label> <p>You can use this following tags for your template</p>
                                           </label>
                                          </div> 
                                                  
                                             <table class="table table-bordered table-striped ecom-orders table-vcenter">
                                              <th>S.N.</th>
                                              <th>Tag</th>
                                              <th>Description</th>
                                              <tbody>
                                              <tr>
                                              <td>1</td>
                                              <td> <?php  echo "{{ creditcard.type_name }}"; ?></td>
                                              <td>Card Friendly Name</td>
                                              </tr>
                                              <tr>
                                              <td>2</td>
                                              <td><?php   echo "{{ invoice.url_permalink }}" ?></td>
                                              <td>  Link </td>
                                              </tr>
                                              <tr>
                                              <td>3</td>
                                              <td><?php echo  "{{ transaction.amount }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              <td>4</td>
                                              <td><?php  echo  "{{ transaction.transaction_method }}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              <td>5</td>
                                              <td><?php echo  "{{ transaction.transaction_date}}"; ?></td>
                                              <td> Transaction Date</td>
                                              </tr>
                                              <tr>
                                              <td>6</td>
                                              <td><?php echo "{{ transaction.transaction_detail }}"; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              <td>7</td>
                                              <td><?php echo "{'%'  config.email_show_poweredby '%'}"; ?> </td>
                                              <td>Company promotion message</td>
                                              </tr>
                                                  <td>8</td>
                                              <td><?php   echo "{{ invoice.refnumber }}" ?></td>
                                              <td> Invoice Number </td>
                                              </tr>
                                              <tr>
                                              <td>9</td>
                                              <td><?php echo  "{{ invoice.days_overdue }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              <td>10</td>
                                              <td><?php  echo  "{{invoice.balance}}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              <td>11</td>
                                              <td><?php echo  "{{ transaction.currency_symbol }}"; ?></td>
                                              <td>Currency</td>
                                              </tr>
                                              <tr>
                                              <td>12</td>
                                              <td><?php echo '{{ invoice.due_date|date("F j, Y") }}'; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              <td>13</td>
                                              <td><?php echo "{{ creditcard.url_updatelink }}"; ?> </td>
                                              <td>Credit Card Update Link</td>
                                              </tr>
                                              </tbody>
                                             </table>       
                                            
							    
                                        <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Close</button>
                                        
                                  		
                                         
                                          
                                        </div>
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>   
   
   
<div id="teplate_view" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">View Template</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_template11"  style="height: 300px; min-height:300px;  overflow: auto; " >
			    <label class="label-control" id="template_name"></label>
		    	
			</div>
			    <hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="teplate_del" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Template</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="" method="post" action='<?php echo base_url(); ?>Settingmail/delete_template' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this template?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="tempateDelID" name="tempateDelID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
    
 function delete_template_details(tempateViewID){
	
	 $('#tempateDelID').val(tempateViewID);
  } 
        
 function view_template_details(tempateViewID){
	 
	
	 if(tempateViewID!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>Settingmail/view_template',
		  data : {'tempateViewID':tempateViewID },
		  success: function(data){
			        
			     $('#data_form_template11').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 } 
         
</script>
   
   
   </div>