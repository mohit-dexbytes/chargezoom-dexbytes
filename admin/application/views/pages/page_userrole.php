<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
	         <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Admin Roles</h2>
            <div class="block-options pull-right">
                    <div class="btn-group">
                              
                    <a href="#add_role" class="btn btn-sm btn-success"  onclick="set_addrole();" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create Role" >Add New</a>
                            </div>
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="visible-lg text-left">Role Name</th>
                    <th class="text-left visible-lg">Powers</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($roles_data) && $roles_data)
				{
					foreach($roles_data as $role)
					{
				?>
				<tr>
					
					<td class="text-left visible-lg"><?php echo $role['roleName']; ?></a></td>
                    <td class="text-left visible-lg"><?php echo  implode(', ',$role['authName']); ?></td>
					
					
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
						
							 <a href="#add_role" onclick="set_edit_role('<?php  echo $role['roleID']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Edit Role" class="btn btn-alt btn-sm btn-default"  > <i class="fa fa-edit"></i></a>
							
                            <a href="#del_role" onclick="del_role_id('<?php  echo $role['roleID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Role" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                            
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>


<script>

function set_addrole(id){
	$('#roleName').val(id);
          	
	 $("input[type=checkbox]").each(function() {
				
				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = false;
				
			});
}	
function set_edit_role(role_id){
    
          	
    	 $("input[type=checkbox]").each(function() {
				
				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = false;
				
			});
    
    $.ajax({
    url: '<?php echo base_url("MerchantUser/get_role_id")?>',
    type: 'POST',
	data:{role_id:role_id},
	dataType: 'json',
    success: function(data){
		
             $('#roleID').val(data.roleID);		
			 $('#roleName').val(data.roleName);

			 var newarray = JSON.parse("[" + data.authID + "]");
			
			 for(var val in  newarray) {
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }
			 
	}	
});
	
}	
  

</script>


<!------    Add popup form    ------->

<div id="add_role" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
	<style> .error{color:red; }</style> 
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add/Edit Role</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="form" class="form form-horizontal" action="<?php echo base_url(); ?>MerchantUser/create_role">
			<input type="hidden" id="roleID" name="roleID" value=""  />
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Power Name</label>
							<div class="col-md-8">
								<input type="text" id="roleName"  name="roleName" class="form-control"  value="" placeholder="Your role name.."></div>
						</div>
                       </div>      
		    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Select Powers</label>
							<div class="col-md-8"></div>
						</div>
                       </div>      
							
					<?php    foreach ($auths as $key=>$auth ){  ?>     
						   
					   <div class="col-md-6">
					   <div class="form-group">
					   <label class="col-md-6 control-label" for="example-username">  </label>
					  
					   <input type="checkbox" name="role[]" id="role<?php echo $key; ?>"  value="<?php echo $auth['authID']; ?>" >  <?php echo $auth['authName'] ; ?>
					 
					  </div>
					  </div>
					  <?php } ?>
                    
                    
                  
	                <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
					
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



</div>
<!-- END Page Content -->