
<!-- Page content -->
<div id="page-content">
                   <!-- Forms General Header -->
                        <div class="content-header">
                            <div class="header-section">
                                <h1>
                                    <i class="gi gi-notes_2"></i>Sale<br><small>Enter the sale details to charge a customer.</small>
                                </h1>
                            </div>
                        </div>
                      
                        <!-- END Forms General Header -->

                        <div class="row">  <div class="col-md-12">
                                <!-- Form Validation Example Block -->
								
								
							
                                <div class="block">
                                    <!-- Form Validation Example Title -->
                                  
                                    <!-- END Form Validation Example Title -->
                             
                                    <!-- Form Validation Example Content -->  
							       		
						    			
									
                                    <form id="form-validation" action="<?php echo base_url(); ?>Payments/create_customer_sale" method="post" class="form-horizontal form-bordered">
                                        <fieldset>
                                            <legend><i class="fa fa-angle-right"></i> Payment Info</legend>
											 
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                                                <div class="col-md-6">
                                                    <select id="customerID" name="customerID" class="form-control">
                                                      
                                                        <option value>Choose Customer</option>
														<?php   foreach($customers as $customer){       ?>
														
                                                        <option value="<?php echo $customer['ListID']; ?>"><?php echo  $customer['FullName'] ; ?></option>
														<?php } ?>
                                                    </select>
                                                </div>
                                            </div>
									   <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway_list" name="gateway_list"  class="form-control">
                                                        <option value="" >Select gateway</option>
														  <?php foreach($gateways as $gateway){ ?>
                                                           <option value="<?php echo $gateway['gatewayID'];  ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
														   <?php } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="card_list" name="card_list"  class="form-control">
                                                        <option value="" >Select Card</option>
                                                        <option  value="new1" ><strong>New Card</strong></option>
                                                        
                                                    </select>
														
                                                </div>
                                            </div>
											
								 </fieldset>			
								 <fieldset id="set_credit">		
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="card_number">Credit Card Number<span class="text-danger">*</span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="card_number" name="card_number" class="form-control" placeholder="Credit Card Number..."  autocomplete="off">
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
														
                                                    </div>
                                                </div>
                                            </div>
                                        
                                          
                                              <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month  <span class="text-danger">*</span></label>
                                                <div class="col-md-2">
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-2 control-label" for="expry_year">Expiry Year  <span class="text-danger">*</span></label>
                                                <div class="col-md-2">
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="cvv">Card Security Code (CVV)<span class="text-danger">*</span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="cvv" name="cvv" class="form-control" placeholder="1234"  autocomplete="off">
                                                        <span class="input-group-addon"><i class="fa fa-cc-stripe"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="friendlyname"> Card Friendly Name<span class="text-danger">*</span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Enter card friendly name">
                                                        <span class="input-group-addon"><i class="fa fa-cc-stripe"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
									
							    	 </fieldset>


									 <fieldset>			
                                             <div class="form-group">
                                                <label class="col-md-4 control-label" for="amount">Amount<span class="text-danger">*</span></label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="amount" name="amount" class="form-control" placeholder="1.00">
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
												<label class="col-md-2 control-label" for="country_code">Currency</label>
                                                <div class="col-md-2">
                                                    <select id="country_code" name="country_code" class="form-control">
                                                       
                                                        <option value="USD">USD</option>
                                                        <option value="AUD">AUD</option>
                                                        <option value="CAD">CAD</option>
                                                        <option value="EUR">EUR</option>
                                                        <option value="NZD">NZD</option>
                                                    </select>
                                                </div>
                                            </div>
											
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_skill">Surcharge Type</label>
                                                <div class="col-md-2">
                                                    <select id="surcharge_type" name="surcharge_type"  class="form-control">
                                                      
                                                        <option value="1">No Surcharge</option>
                                                        <option value="2">Fixed</option>
                                                        <option value="3">Percentage</option>
                                                    </select>
                                                </div>
												   <label class="col-md-2 control-label" for="val_digits">Surcharge</label>
                                                <div class="col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="surchargeVal" name="surchargeVal" class="form-control" readonly  placeholder="0.00">
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
												
                                            </div>
											
									 </fieldset>	
                                    <fieldset id="card_data">										 
											
											 <div class="form-group vaul_div" id="vaul_div"  >
                                                <label class="col-md-4 control-label" for="vaultID">Current Vault ID</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="vaultID" name="vaultID" class="form-control"  readonly ='readonly'>
                                                        <span class="input-group-addon"><i class="fa fa-cc-stripe"></i></span>
                                                    </div>
                                                </div>
                                            </div>
											 <div class="form-group vaul_div" id=""  >
                                                <label class="col-md-4 control-label" for="vaultcardID">Card Number</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="vaultcardID" name="vaultcardID" class="form-control"  readonly ='readonly'>
                                                        <span class="input-group-addon"><i class="fa fa-cc-stripe"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            
											
											
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_digits">Total Amount</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="totalamount" name="totalamount" class="form-control" placeholder="0.00" readonly ='readonly' >
                                                        <span class="input-group-addon"><i class="gi gi-usd"></i></span>
                                                    </div>
                                                </div>
                                            </div>
											
											
											
											
                                        </fieldset>
								
                                        <fieldset>
                                            <legend><i class="fa fa-angle-right"></i> Card Billing Address</legend>
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="firstName">First Name</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="firstName" name="firstName" class="form-control" placeholder="First Name...">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="lastName">Last Name </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="lastName" name="lastName" class="form-control" placeholder="Last Name...">
                                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                                    </div>
                                                </div>
                                            </div>
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="companyName">Company Name </label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="companyName" name="companyName" class="form-control" placeholder="Company Name...">
                                                        <span class="input-group-addon"><i class="fa fa-university"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											 <div class="form-group">
                                                
												
												<label class="col-md-4 control-label" for="example-typeahead">Country</label>
												<div class="col-md-6">
												  <div class="input-group">
													<input type="text" id="country" name="country" class="form-control input-typeahead" autocomplete="off" value="" placeholder="Search Country...">
													<span class="input-group-addon"><i class="gi gi-home"></i></span>
												</div>
												
                                            </div>
											</div>
											<div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">State/Province</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="state" name="state" class="form-control input-typeahead" autocomplete="off" placeholder="Search State...">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                          </div>
                                          <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">City</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" placeholder="Search City...">
                                                        <span class="input-group-addon"><i class="gi gi-road"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
									     <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Address</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="address" name="address" class="form-control "  placeholder="Address...">
                                                        <span class="input-group-addon"><i class="gi gi-home"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										 
											  
										 <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_username">Zip/Postal code</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" placeholder="Zipcode..">
                                                        <span class="input-group-addon"><i class="gi gi-direction"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											
											  <div class="form-group">
                                                <label class="col-md-4 control-label" for="phone">Contact Number</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="phone" name="phone" class="form-control" placeholder="Contact Number...">
                                                        <span class="input-group-addon"><i class="gi gi-phone_alt"></i></span>
                                                    </div>
                                                </div>
                                          </div>
										
									    <div class="form-group">
                                                <label class="col-md-4 control-label" for="val_email">Email</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="email" name="email" class="form-control" placeholder="test@example.com">
                                                        <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                                                    </div>
                                                </div>
                                            </div>		
											
                                          
                                        
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="reference">Future Reference/Memo</label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="reference" name="reference" class="form-control" placeholder="Enter reference/memo for future reference...">
                                                        <span class="input-group-addon"><i class="gi gi-notes"></i></span>
                                                    </div>
                                                </div>
                                            </div>	
											 </fieldset>
											 
										
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Submit</button>
                                              
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Example Content -->

                                
                                    <!-- END Terms Modal -->
                                </div>
                                <!-- END Validation Block -->
                            </div>
					  </div>

    

 <style>
.use_vault{display:none;}
.update_vault{display:none;}
.add_vault{display:none;}
#set_credit{display:none;}
#vaul_div{display:none;}
.vaul_div{display:none;}
 
</style> 
         


    <script>
	$(function(){  nmiValidation.init(); 
	
	  $('#gateway_list').change(function(){
		    var gateway_value =$(this).val();
			
			if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3'){			
											var url   = "<?php echo base_url()?>PaytracePayment/create_customer_sale";
										}else if(gtype=='2'){
									var url   = "<?php echo base_url()?>AuthPayment/create_customer_sale";
									}else{
									var url   = "<?php echo base_url()?>Payments/create_customer_sale";
									}			
				
				             $("#form-validation").attr("action",url);
					}   
				   
			   });
			}	
			
         
			
	  });
	
	$('#customerID').change(function(){
		
		                    $('#vaultID').val('');
		                    
		                       $('#check_status').val('0');
						  
		
		var cid  = $(this).val();
	
		if(cid!=""){
			$('#card_list').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					     if(data['status']=='success'){
						
                              var s=$('#card_list');
							 console.log (data['card']);
							  var card1 = data['card'];
							     $(s).append('<option value="new1">New Card</option>');
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							    $('#companyName').val(data['companyName']);
							    $('#firstName').val(data['FirstName']);
								$('#lastName').val(data['LastName']);
								$('#address').val(data['ShipAddress_Addr1']);
								$('#city').val(data['ShipAddress_City']);
								$('#state').val(data['	ShipAddress_State']);
								$('#zipcode').val(data['ShipAddress_PostalCode']);
								$('#phone').val(data['Phone']);
								$('#email').val(data['Contact']);
					   }	   
					
				}
				
				
			});
			
		}	
    });		
	
	
	
	
	
	var amount=0;
	$('#surcharge_type').change(function(){
		
		if($(this).val()=='1'){
			
			 $('#surchargeVal').attr('readonly','readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
		if($(this).val()=='2'){
			    $('#surchargeVal').removeAttr('readonly');
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			    $('#totalamount').val(amount1.toFixed(2));

		}	
		if($(this).val()=='3'){
			 $('#surchargeVal').removeAttr('readonly');
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					     amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	});
	
	$('#surchargeVal').change(function(){
       
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3'){
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					    amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
	
	$('#amount').change(function(){
       
	   
	   if($(this).val()=='1'){
			 $('#surchargeVal').removeAttr('readonly');
			  $('#surchargeVal').val('0');
			var amount = $('#amount').val();
			$('#totalamount').val(amount.toFixed(2));
		}	
	  
	   	if( $('#surcharge_type').val()=='2'){
			
				var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
		if( $('#surcharge_type').val()=='3');{
		      var amount 	  = $('#amount').val();
				var surcharge = $('#surchargeVal').val();
				     surcharge = (amount*surcharge)/100;
				   
					   amount1=	parseFloat(amount)+ parseFloat(surcharge);
			      $('#totalamount').val(amount1.toFixed(2));


		}	
	   

	});
	
	
	
		
  $('#card_list').change( function(){
	   if($(this).val()=='new1'){
	   $('#set_credit').show();
	   }else{
		      $('#card_number').val('');
	$('#set_credit').hide();
	   }
  });
	

	
	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');


	
	
	
	});
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
                    
                    	 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                    customerID: {
                         required: true,
                       
                    },
					 amount: {
                        required: true
                    },
					check_status:{
						  required: true,
					}	
                  
                },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                   
                    expry: {
                        required: 'Please select a valid month',
                         min: 'Please select a valid month',
                      
                    },
					amount:{
						  required: 'Please enter the amount',
					},
					check_status:{
						 required: 'Please select the option',
					}
                  
                   
                }
            });

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
	
	
	
	
	
	
	</script>
</div>