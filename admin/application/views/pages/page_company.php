
<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Companies</h2>
            <div class="block-options pull-right">
                            <div class="btn-group">
                               <a class="btn btn-sm  btn-success" title="Create Company"  href="<?php echo base_url(); ?>QuickBooks/create_company">Add New</a>
                                
                            </div>
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="visible-lg text-left">Company Name</th>
                    <th class="text-left visible-lg">Company Email</th>
                    <th class="hidden-xs text-right">User Name</th>
                     <th class="hidden-xs text-right">Company Password</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($companies) && $companies)
				{
					foreach($companies as $company)
					{
				?>
				<tr>
					
					<td class="text-left visible-lg cust_view"><a href="#"><?php echo $company['companyName']; ?></a></td>
                    <td class="text-left visible-lg cust_view"><a href="mailto:<?php echo $company['companyEmail']; ?>"><?php echo $company['companyEmail']; ?></a></td>
					
					<td class="hidden-xs text-right"><?php echo $company['qbwc_username']; ?></td>
					<td class="text-right hidden-xs"><?php echo $company['qbwc_password']; ?></td>
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="<?php echo base_url('QuickBooks/create_company/'.$company['id']); ?>" data-toggle="tooltip" title="Edit Company" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="#del_company" onclick="set_company_id('<?php  echo $company['id']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Company" class="btn btn-danger"><i class="fa fa-times"></i></a>
                            <a href="<?php if($company['qbwc_username']!=""){ echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file'.$company['qbwc_username'].'.qwc'; } ?>" data-toggle="tooltip" title="Download qwc file" class="btn btn-success"><i class="fa fa-download"></i></a>
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>




</div>
<!-- END Page Content -->