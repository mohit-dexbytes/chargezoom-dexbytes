

<!-- Page content -->
<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-settings"></i><?php if(isset($company)){ echo "Edit Company Date";}else{ echo "Create new company"; }?> 
            </h1>
        </div>
    </div>
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="form_new" class="form form-horizontal" action="<?php echo base_url(); ?>QuickBooks/create_company">
		         <input type="hidden" name="companyID" value="<?php if(isset($company)){echo $company['id']; } ?>"  />
                 
                  
                    <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($company)){echo $company['companyName']; } ?>" placeholder="Your company name.."><?php echo form_error('companyName'); ?>
							</div>
						</div>
                       </div> 
                        <div class="col-md-6">   
                    <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Username</label>
							<div class="col-md-8">
								<input type="text" id="qwbc_username" name="qwbc_username" class="form-control"  readonly="readonly" value="<?php if(isset($company)){echo $company['qbwc_username']; }else{ echo ($qb_username)?$qb_username:'';} ?>"  placeholder="Enter your username">
							</div>
						</div>
                       </div>
                        <?php if(!isset($company)){  ?>
                         <div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">QWBC Password</label>
							<div class="col-md-8">
								<input type="password" id="qwbc_password" name="qwbc_password"   class="form-control" placeholder="Enter your password">
							</div>
						</div>
                        </div>
                            <div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Confirm Password</label>
							<div class="col-md-8">
								<input type="password" id="confirm_password" name="confirm_password"   class="form-control" placeholder="Enter your password">
							</div>
						</div>
                        </div>
                     <?php } ?>
					<div class="col-sm-6">
                    
                   
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress1" name="companyAddress1"   value="<?php if(isset($company)){echo $company['companyAddress1']; } ?>" class="form-control" placeholder="Address line 1.."><?php echo form_error('companyAddress1'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress2" name="companyAddress2"  class="form-control"  value="<?php if(isset($company)){echo $company['companyAddress2']; } ?>"  placeholder="Address line 2 (Optional)..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Email</label>
							<div class="col-md-8">
								<input type="text" id="companyEmail"  name="companyEmail" class="form-control"   value="<?php if(isset($company)){echo $company['companyEmail']; } ?>"  placeholder="test@example.com"><?php echo form_error('companyEmail'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Phone</label>
							<div class="col-md-8">
								<input type="text" id="companyContact" name="companyContact" class="form-control" value="<?php if(isset($company)){echo $company['companyContact']; } ?>" placeholder="Your contact number.."><?php echo form_error('companyContact'); ?>
							</div>
						</div>
					</div>
				
					<div class="col-sm-6">
					
						<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Select Country</label>
						   <div class="col-md-8">
								<select id="country" class="form-control " name="companyCountry" >
								<option value="Select Country">Select Country ..</option>
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_id']; ?>" <?php if(isset($company)){ if($company['companyCountry']==$country['country_id'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
					
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">Select State</label>
						   <div class="col-md-8">
								<select id="state" class="form-control input-typeahead" name="companyState">
								<option value="Select State">Select State ..</option>
								   <?php foreach($state_datas as $state){  ?>
								   <option value="<?php echo $state['state_id']; ?>"<?php if(isset($company)){if($company['companyState']==$state['state_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $state['state_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">Select City</label>
						   <div class="col-md-8">
								<select id="city" class="form-control input-typeahead" name="companyCity">
								<option value="Select City">Select City ..</option>
								   <?php foreach($city_datas as $city){  ?>
								   <option value="<?php echo $city['city_id']; ?>" <?php if(isset($company)){if($company['companyCity']==$city['city_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $city['city_name']; ?> </option>
								   <?php } ?> 
								</select>
							</div>
                        </div>	
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Zip Code</label>
							<div class="col-md-8">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php if(isset($company)){echo $company['zipCode']; } ?>" placeholder="Zip code.."><?php echo form_error('zipCode'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Alternate Phone</label>
							<div class="col-md-8">
								<input type="text" id="alternateContact" name="alternateContact" class="form-control"
								value="<?php if(isset($company)){echo $company['alternateContact']; } ?>" placeholder="Your alternate contact number (Optional)..">
							</div>
						</div>
                        
					</div>  
                    
                    <div class="col-md-12">
                    </div>
                  
					
		
				  	
	              <div class="form-group">
					<div class="col-md-8 col-md-offset-11">
						
						<button type="submit" class="submit btn btn-sm btn-success" >Save</button>	
					</div>
				</div>					
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->

<script>
$(document).ready(function(){

 
    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'companyAddress1': {
                        required: true,
                        minlength: 5
                    },
					
					'companyEmail':{
					      required:true,
						  email:true,
						  
					},
					  'companyCountry': {
                        required: true,
                        
                    },
                    'companyState': {
                        required: true,
                        
                    },
					  'companyCity': {
                        required: true,
                         
                    },
					  'companyContact': {
                        required: true,
						 minlength: 10,
						 number : true,
						 maxlength: 12,
                       
                    },
                    'zipCode': {
                        required: true,
                         minlength: 3
                    },
					'qwbc_password':{
					    required: true,
					   minlength: 6,
					   },
					'confirm_password':{
					    required: true,
					   equalTo: '#qwbc_password',
                    }
			
			},
    });
    
    
    $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('QuickBooks/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
    
});	
       
	   
	   var checkRemoteEmail=function(element_name){
        
           
            remote={
				     
                     url: '<?php echo base_url(); ?>home/check_exist_email',
                    type: "post",
					data:
                        {
                            companyEmail: function(){return $('input[name=companyEmail]').val();},
                            
                        },
                        dataFilter:function(response){
							
						   data=$.parseJSON(response);
                             if (data['status'] === 'true') {
							  
						         $('#companyEmail').removeClass('error');	 
								 $('#companyEmail-error').hide();	 
								  
									   return data['status'] ;				   
									 
                              } else {
                                
								   if(data['status'] =='false'){
                                       $('.form').validate().showErrors(function(){
										   
                                                       return {key:'companyEmail'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	

</script>

</div>

