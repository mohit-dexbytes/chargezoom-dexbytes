
<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Customers Transactions</h2>
        </div>
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center" style="width: 100px;">Trans ID</th>
                    <th class="visible-lg">Name</th>
                    <th class="text-right">Invoice</th>
                     <th class="text-right hidden-xs">Amount</th>
					 
                    
                    <th class="text-right visible-lg">Date </th>
                     <th class="text-right hidden-xs">Type</th>
                    
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-center"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="visible-lg"><?php echo $transaction['FullName']; ?></a></td>
				
					<td class="text-right hidden-xs"><?php echo ($transaction['RefNumber'])?$transaction['RefNumber']:'--'; ?></td>
					<td class="hidden-xs text-right">$<?php echo ($transaction['transactionAmount'])?$transaction['transactionAmount']:'0.00'; ?></td>
					
				
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
					<td class="text-center visible-lg"><?php   if($transaction['transactionCode']=='300' || $transaction['transactionID']=='' || $transaction['transactionCode']=='3'){ ?> 
					<span class="btn btn-sm btn-danger remove-hover">Failed</span> <?php }else if($transaction['transactionCode']=='100'|| $transaction['transactionCode']=='200'|| $transaction['transactionCode']=='1'){ ?> <span class="btn btn-sm btn-success remove-hover">Success</span><?php } ?></td>
					
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#pay_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>




</div>
<!-- END Page Content -->