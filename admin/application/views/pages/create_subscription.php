
<!-- Page content -->
<div id="page-content">
    
    
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i><?php if(isset($subs)){ echo "Edit"; }else{ echo "Create"; } ?> Subscription<br><small>You can create a new subscription here.</small>
            </h1>
	        <div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
		    </div>
        </div>
    </div>
  
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
        <div class="col-md-12">
		<div class="block">
		
		   
		    <form id="form-validation" action="<?php echo base_url(); ?>SettingSubscription/create_subscription" method="post" class="form-horizontal form-bordered">
                
				
				
                 <fieldset>  
				 
				   <div class="col-md-2 form-group">
						<label class="control-label" for="customerID">Subscription Name</label>
						<div >
						
						  <input type="text" id="sub_name" name="sub_name"  class="form-control" value="<?php if(isset($subs)){ echo  $subs['subscriptionName']; }; ?>" placeholder="Subscription Name" />
						 
						  
						</div>
                   </div>
			        <div class="col-md-2 form-group">
						 <label class="control-label" for="customerID">Customer</label>
						  <div>
                            <select id="customerID" name="customerID" class="form-control">
                                <option value>Select Customer</option>
						        <?php   foreach($customers as $customer){       ?>
						        <option value="<?php echo $customer['ListID']; ?>" <?php if(isset($subs) &&  $subs['customerID']==$customer['ListID']){  echo "selected" ;} ?>  ><?php echo  $customer['companyName'] ; ?></option>
						        <?php } ?>
                            </select>
						 </div>	
                   </div>    
                    
                   <div class="col-md-2  form-group">
						<label class="control-label" for="customerID">Recurrence</label>
						<div >
						
						  <input type="text" id="duration_list" name="duration_list"  class="form-control" value="<?php if(isset($subs)){ echo  $subs['subscriptionPlan']; }; ?>" placeholder="0 for Unlimited" />
						 
						 
						</div>	
					 </div>
                   <div class="col-md-2  form-group">   
                        <label class=" control-label" for="firstName">Subscription Start Date</label>
                           <div>
                            <div class="input-group input-date">
						
                                <input type="text" id="sub_start_date" name="sub_start_date" class="form-control"  value="<?php  if(isset($subs)){  echo date('m/d/Y', strtotime($subs['startDate'])); }?>" data-date-format="mm-dd-yyyy" placeholder="mm-dd-yyyy">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
                       </div>
                    
                    </div>
					 
			
               		
                   <div class ="col-md-2  form-group">    
                   
                        <label class=" control-label" for="firstName">Invoice Generation Date</label>
                        <div> 
                            <div class="input-group input-date">
                                <input type="text" id="invoice_date" name="invoice_date" class="form-control " value="<?php if(isset($subs)){  echo date("m/d/Y",strtotime( $subs['firstDate'])); }?>" placeholder="Date">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            </div>
						</div>
                    
					 </div>
                   <div class="col-md-2 form-group">   
                       
                       
						<label class="control-label" for="customerID">Frequency</label>
                           <div>  
                               <select name="paycycle" id="paycycle" class="form-control">
							 <option value="" >Select Frequency</option>
							 <?php  foreach($frequencies as $frequecy){ ?>
							 <option value="<?php echo $frequecy['frequencyValue']; ?>"
							 <?php if(isset($subs) && $frequecy['frequencyValue']==$subs['invoiceFrequency'] ){echo "selected";} ?>><?php echo $frequecy['frequencyText']; ?> </option>
							 <?php } ?>
							   </select>
                         </div>
                     </div>
				 
					 
			</fieldset>
                    
					<fieldset><legend> Item Details</legend>	
					<div class="form-group form-actions">
					<div class="col-sm-3">
					    <div class="form-group">	<label class="control-label">Products & Services</label> </div>
			        	</div> 
					<div class="col-sm-4">  <div class="form-group"><label class="control-label">Description </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Unit Rate </label> </div></div>
					<div class="col-sm-1"><div class="form-group"><label class="control-label">Quantity </label></div> </div>
					<div class="col-sm-2"><div class="form-group"><label class="control-label">Total</label></div> </div>
					
					</div>
					
											
						  <div id="item_fields">
						 <?php 
						       if(isset($items) && !empty($items)){
							     foreach($items as $k=>$item ){  ?>
								<div class="col-sm-3 nopadding"><div class="form-group">
								  <select class="form-control"  onchange="select_plan_val('<?php echo $k+1; ?>');"  id="productID<?php echo $k+1; ?>" name="productID[]">
								<option>Select Plans</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>" <?php if($plan['ListID']==$item['itemListID']){ echo "selected";} ?>  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description<?php echo $k+1; ?>" name="description[]" value="<?php echo $item['itemDescription']; ?>" placeholder="Description "></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control" id="unit_rate<?php echo $k+1; ?>" name="unit_rate[]" value="<?php echo $item['itemRate']; ?>" onblur="set_unit_val('<?php echo $k+1; ?>');" placeholder="Unit Rate"></div></div>
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('<?php echo $k+1; ?>');" id="quantity<?php echo $k+1; ?>" name="quantity[]" value="<?php echo $item['itemQuantity']; ?>" placeholder="Quantity"></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total<?php echo $k+1; ?>" name="total[]" value="<?php echo ($item['itemQuantity']*$item['itemRate']); ?>" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('<?php echo $k+1; ?>');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>

						<?php		 }								 
							   }else{  
						 ?>
						   <div class="form-group removeclass1">
							<div class="col-sm-3 nopadding"><div class="form-group ">
								  <select class="form-control"  onchange="select_plan_val('1');"  id="productID1" name="productID[]">
								<option>Select Plans</option>		
								 <?php foreach($plans as $plan){ ?>
								 <option value="<?php echo $plan['ListID']; ?>"  > 
								 <?php echo $plan['FullName']; ?> </option> <?php }  ?>
								   </select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description1" name="description[]" value="" placeholder="Description "></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group">
								   <input type="text" class="form-control" id="unit_rate1" name="unit_rate[]" value="" onblur="set_unit_val('1');" placeholder="Unit Rate"></div></div>
								   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('1');" id="quantity1" name="quantity[]" value="" placeholder="Quantity"></div></div>
								   <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total1" name="total[]" value="" placeholder="Total"> 
								   <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('1');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>
							   
							</div>   <?php } ?>
								  
						 </div>
						
				 
				       <div class="col-md-12">
					    <div class="form-group">
						
						  <div class=" form-actions">		
							<label class="control-label "></label>						  
						 <div class="group-btn">
							<button class="btn btn-sm btn-success" type="button"  onclick="item_fields();"> Add New </button>
						 
							<label class="btn btn-sm btn-success pull-right remove-hover"  ><strong>Total :</strong></strong> <span class="fa fa-usd" aria-hidden="true"></span> <span id="grand_total"> <strong>0.00</strong></span>  </label>
						  </div>
				        </div>
						</div>
						
					</div>	
					
				 <div class="col-md-12"	>
				 
				  <div class="form-group col-sm-3">
				   <div class="">
						 
					 <label class="control-label ">Free Trial Recurrence</label>
					
					 <input type="text" class="form-control" id="freetrial" name="freetrial" value="<?php if(isset($subs))echo $subs['freeTrial']; ?>" placeholder="Free trial">
					
				  </div>
				</div> 			
						
				<div class="form-group col-sm-3">	
						<div class="col-sm-12">
						  
						     <label style="padding:5px;" class=" control-label">Automatic Payment</label>
						</div>	
						<div class="col-sm-12">
							<label class="radio-inline control-label">
							<input type="radio"   name="autopay" onclick="chk_payment(this.value);" <?php if(isset($subs)&& $subs['automaticPayment']=='1'){ echo "checked"; } ?>  value="1" >Yes</label>
							<label class="radio-inline control-label">
							<input type="radio"   name="autopay" onclick="chk_payment(this.value);" <?php if(isset($subs)&& $subs['automaticPayment']=='0')echo "checked"; ?>   value="0" >No</label>
						 
						 </div> 
				</div> 		 
			 <div id="set_pay_data" <?php if(isset($subs)&& $subs['automaticPayment']=='0'){ ?>  style="display:none;" <?php }else if(isset($subs)&& $subs['automaticPayment']=='1'){ ?>  style="display:block;" <?php }else{ ?> style="display:none;" <?php }?> >	
				  	<div class="form-group col-sm-3">	
					<div class="">	
					                                            
						<label class=" control-label" for="card_list">Gateway</label>
						
							<select id="gateway_list" name="gateway_list"  class="form-control">
								<option value="" >Select gateway</option>
								  <?php foreach($gateways as $gateway){ ?>
								   <option value="<?php echo $gateway['gatewayID'];  ?>" <?php if(isset($subs) && $subs['paymentGateway']==$gateway['gatewayID'])echo "selected"; ?> ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								
								
							</select>
					</div>		
					</div>
					<div class="form-group col-sm-3">
					<div class=" ">	
						<label class=" control-label" for="card_list">Select Card</label>
							<select id="card_list" name="card_list"  class="form-control">
								<option value="" >Select Card</option>
								<option  value="new1" ><strong>New Card</strong></option>
							<?php	if(isset($c_cards) && !empty($c_cards)){ 
							            foreach($c_cards as $c_card){
							        ?>
								<option value="<?php echo $c_card['CardID'] ; ?>" <?php if($c_card['CardID']==$subs['cardID']) echo "selected"; ?> ><?php echo $c_card['customerCardfriendlyName']; ?></option>
										<?php } } ?>
								
							</select>
								
						</div>

					</div>
				 </div>
			   </div>		
			 
			<div class="col-sm-12 ">
			         <div id="set_credit" style="display:none;">	
					          
                                         <div class="col-sm-3 form-group">	
                                            <div>
                                                <label class="control-label" >Credit Card Number</label>
                                               
                                                        <input type="text" id="card_number" name="card_number" class="form-control" placeholder="Credit Card Number..." autocomplete="off">
                                                  </div>    
                                            </div>
											
											<div class="col-sm-3 form-group">	
                                            <div>
                                                <label  class="control-label" for="friendlyname"> Card Friendly Name</label>
                                               
                                                        <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Enter card friendly name">
                                             </div>   
                                            </div>
                                        
										  <div class="col-sm-2 form-group">	
                                              
                                                <label class="control-label" for="expry">Expiry Month  </label>
                                                
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
											
											 <div class="col-sm-2 form-group">	
                                                <div>
												    <label class=" control-label" for="expry_year">Expiry Year</label>
                                               
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php  echo "20".$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                               </div>  
                                            </div>  
										 <div class="col-sm-2 form-group">					
											<div>
                                                <label class="control-label" for="cvv">Card Security Code (CVV)</label>

                                                        <input type="text" id="cvv" name="cvv" class="form-control" placeholder="1234" autocomplete="off">
                                                     
                                            </div>
                                            </div>
										 
									
                                    </div>
							</div>
			


						</fieldset>
                    
					<fieldset>
											<div id="set_bill_data">
							<legend>Billing Address</legend>						
									 <div class="col-sm-12 form-group">										
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 1</label>
                                              
                                                        <input type="text" id="address1" name="address1" class="form-control " value="<?php if(isset($subs)) echo $subs['address1']; ?>"  placeholder="Address...">
                                                  
                                          </div>
										</div>	
										 <div class="col-sm-12 form-group">					
									     <div class="">
                                                <label class=" control-label" for="val_username">Address Line 2</label>
                                              
                                                        <input type="text" id="address2" name="address2" class="form-control "  value="<?php if(isset($subs))echo $subs['address2']; ?>" placeholder="Address...">
                                                  
                                             </div>
										   </div>
								<div class="col-sm-3 form-group">	   
								 <div class="">
										
											
                                                
												<label class="control-label" for="example-typeahead">Country</label>
												
													<input type="text" id="country" name="country" class="form-control input-typeahead" autocomplete="off" value="<?php if(isset($subs)) echo $subs['country']; ?>" placeholder="Search Country...">
													
											
										</div>	
									</div>
										<div class="form-group col-sm-3">	
										<div class=" ">
											
                                                <label class=" control-label" for="val_username">State/Province</label>
                                               
                                                        <input type="text" id="state" name="state" class="form-control input-typeahead"  value="<?php if(isset($subs)) echo $subs['state']; ?>" autocomplete="off" placeholder="Search State...">
                                                   
                                        
										 </div>
										</div>
										<div class="form-group col-sm-2">
										 <div class="">
                                         
                                                <label class=" control-label" for="val_username">City</label>
                                              
                                                        <input type="text" id="city" name="city" class="form-control input-typeahead" autocomplete="off" value="<?php  if(isset($subs))echo $subs['city']; ?>" placeholder="Search City...">
                                                    
                                           
										</div>
										</div>
										<div class="form-group col-sm-2">
										 <div class="">		  
										
                                                <label class=" control-label" for="val_username">Zip/Postal Code</label>
                                              
                                                        <input type="text" id="zipcode" name="zipcode" class="form-control" value="<?php if(isset($subs)) echo $subs['zipcode']; ?>" placeholder="Zipcode..">
                                                     
	
										  </div>
										</div>
										<div class="form-group col-sm-2">		
										 <div class="">
											  
                                                <label class=" control-label" for="phone">Contact Number</label>
                                               
                                                        <input type="text" id="phone" name="phone" class="form-control"  placeholder="Contact Number...">
                                                     
                                         
										   </div>
									</div>
								
				           </div>
				
			</fieldset>
               
				
                  
					<input type="hidden" name="subID" value="<?php if(isset($subs)){  echo $subs['subscriptionID']; }?>" />
				
				
				<div class="form-group pull-right">
                    
            <a href="<?php echo base_url();?>SettingSubscription/subscriptions"class="btn btn-sm btn-danger">Cancel</a>            
                    <button type="submit" class="btn btn-sm btn-success">Save</button>
                      
                    
                </div>
            </form>
          </div>      
        </div>
            
    </div>


    <script>
	
        $(function(){
		
			
			
			
            nmiValidation.init();
		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              autoclose: true
            });  
			
			$('#sub_start_date').datepicker({
			format: 'yyyy-mm-dd',
			startDate:today,
			autoclose: true
			});
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			
					
		  $('#card_list').change( function(){
			   if($(this).val()=='new1'){
			   $('#set_credit').show();
			   }else{
					  $('#card_number').val('');
			$('#set_credit').hide();
			   }
		  });
					
		$('#customerID').change(function(){
			
			var cid  = $(this).val();
		
			if(cid!=""){
				$('#card_list').find('option').not(':first').remove();
				$.ajax({
					type:"POST",
					url : "<?php echo base_url(); ?>Payments/check_vault",
					data : {'customerID':cid},
					success : function(response){
						
							 data=$.parseJSON(response);
							
							 if(data['status']=='success'){
							
								  var s=$('#card_list');
								 console.log (data['card']);
								  var card1 = data['card'];
									 $(s).append('<option value="new1">New Card</option>');
									for(var val in  card1) {
										
									  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
									}
																   
									$('#address1').val(data['ShipAddress_Addr1']);
									$('#address2').val(data['ShipAddress_Addr2']);
									$('#city').val(data['ShipAddress_City']);
									$('#state').val(data['	ShipAddress_State']);
									$('#zipcode').val(data['ShipAddress_PostalCode']);
									$('#phone').val(data['Phone']);
									
								  
						   }	   
						
					}
					
					
				});
				
			}	
		});		
				
	

	});
	 var jsdata = '<?php if(isset($items)){ echo json_encode($items);}?>';
	
	if(jQuery.isEmptyObject(jsdata))
   {
	  
		var room = 0;
	  
		
	}else{
		
		var grand_total1=0;
		var room = '<?php if(isset($k)){ echo $k+1; }else{ echo "1";} ?>';
		$( ".total_val" ).each(function(){
			
               grand_total1+= parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total1);

	}
	function item_fields()
	{
		room++;
		var  jsplandata = '<?php echo json_encode($plans); ?>';
		
		var plan_data = $.parseJSON(jsplandata); 
		var plan_html ='<option val="">Select Plan</option>';
		for(var val in  plan_data) {         plan_html+='<option value="'+ plan_data[val]['ListID']+'">'+plan_data[val]['FullName']+'</option>'; }
		
		
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-4 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="Unit Rate"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" class="form-control" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Quantity"></div></div>	<div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest)
  }
  
  
   function remove_education_fields(rid)
   {
	   var rid_val = $('#total'+rid).val();
	   
	   var gr_val  = $('#grand_total').html();
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(dif.toFixed(2));
		}
	   $('.removeclass'+rid).remove();
	   
	    
	   
	   
  }
	
	
	
	
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     sub_name:{
						  required:true,
						  minlength:3,
					 },
					 sub_start_date: {
							 required:true,
						},
					autopay:{
							 required:true,
					},
					gateway_list:{
							 required:true,
					},
					card_list:{
							 required:true,
					},
					paycycle:{
							 required:true,
					},		
			    	 duration_list: {
                        required: true,
                        number: true,
						maxlength:2,
						
                    },
					invoice_date: {
                        required: true,
                        
                    },
                    customerID: {
                         required: true,
                       
                    },
					 subsamount: {
                        required: true,
						number:true,
						
                    },
					friendlyname:{
						 required: true,
						 minlength: 3,
					},
					  card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },	
					
					address1:{
						required:true,
					},
					address2:{
						required:true,
					},
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},
					
                  freetrial:{
						required: true,
						digits:true,
						check_free:{
							sub_start_date:'#sub_start_date',
							paycycle:'#paycycle',
							duration_list:'#duration_list',
						}
					},
                    	
					
                },
               
            });
					
		 $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
					
				
		 $.validator.addMethod('check_free', function(value, element, params) {  
			
		  var duration = $(params.duration_list).val();
		  
		 
		  if(duration =='0')return true;
		  
		  var frequency = $(params.paycycle).val();
		  var stdate     = new  Date( Date.parse($(params.sub_start_date).val()));
		  
		  var free = value;
		
			
		  return (duration > free);
		}, 'Value should be less than Recurrence value');
						

        }
    };
}();
   
/**********Check the Validation for free trial**********************/
	function weeksBetween(d1, d2) {
		
     return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
	
   }
   function daysbeween(d1, d2) {
		
     return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));
	
   }
   
   function get_months(d1, d2){
	 

return difference = (d2.getFullYear()*12 + d2.getMonth()) - (d1.getFullYear()*12 + d1.getMonth()); 
	   
   }  
   
       function get_frequncy_val(du, new1date, fr){
		   var res='';
		 
		       var CurrentDate =  new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0); 
				  CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
                  var newdate = new Date(CurrentDate);
				
		 if(fr=='dly'){
           res= daysbeween(new Date(new1date), new Date(newdate));
		 }else if(fr=='1wk'){
			  res= weeksBetween(new Date(new1date), new Date(newdate));
		 }else if(fr=='2wk'){
           res= weeksBetween(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='mon'){
           res= get_months(new Date(new1date), new Date(newdate));
		 }else if(fr=='2mn'){
         res= get_months(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='qtr'){
          res= get_months(new Date(new1date), new Date(newdate))/3;
		 }else if(fr=='six'){
           res= get_months(new Date(new1date), new Date(newdate))/6;
		 }else if(fr=='yr1'){
          res= get_months(new Date(new1date), new Date(newdate))/12;
		 }else if(fr=='yr2'){
		 res= get_months(new Date(new1date), new Date(newdate))/24;
		 }else if(fr=='yr3'){
			 res= get_months(new Date(new1date), new Date(newdate))/36;
		 }	
           return res;    			 
		   
	   } 
	   
/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:"<?php echo base_url() ?>SettingSubscription/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['FullName']);
             $('#unit_rate'+rid).val(item_data['AverageCost']);
			 $('#quantity'+rid).val(item_data['QuantityOnHand']);
			
			}	
		});
		
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var total  = qty*rate; 
		$('#total'+rid).val(total.toFixed(2));
		

		var grand_total=0;
		$( ".total_val" ).each(function(){
			
               grand_total+=parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total);
	}
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var total  = qty*rate; 
		$('#total'+rid).val(total.toFixed(2));
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
               grand_total=parseFloat(grand_total)+parseFloat($(this).val());
			});
			
		$('#grand_total').html(grand_total.toFixed(2));
		
	}	
	
	
	</script>
</div>