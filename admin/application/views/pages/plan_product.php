
<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Products & Services</strong></h2>
             <?php if($this->session->userdata('logged_in')){ ?>
            <div class="block-options pull-right">
                           
                   <a class="btn btn-sm  btn-success" title="Create New"  href="javascript:void(0);">Add New</a>
                 
            </div>
            <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr> 
                   
                    <th class="visible-lg text-left">Item Name</th>
                      <th class="visible-lg text-left">Description</th>
                    <th class="text-left hidden-xs">Subitem Of</th>
                    <th class="text-left visible-lg">Type</th>
                    <th class="hidden-xs text-right">Sales Price</th>
                    <th class="text-right hidden-xs">Cost</th>
                    <th class="hidden-xs text-center">Action</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($plans) && $plans)
				{
					foreach($plans as $plan)
					{
				?>
				<tr>
				
					<td class="text-left visible-lg"><?php echo $plan['Name']; ?></a></td>
                    <td class="text-left visible-lg"><?php echo $plan['FullName']; ?></a></td>
					<td class="text-left hidden-xs"><strong><?php echo $plan['Parent_FullName']; ?></strong></td>
					<td class="text-left visible-lg"><?php echo $plan['Type']; ?></a></td>
					<td class="hidden-xs text-right">$<?php echo number_format($plan['SalesPrice'],2); ?></td>
					<td class="hidden-xs text-right">$<?php echo number_format($plan['PurchaseCost'],2); ?></td>
				
				      <td class="text-center">						
					<div class="btn-group btn-group-sm">		
					    <a href="#plan_product" class="btn btn-sm btn-default" onclick="view_plan_details('<?php echo $plan['ListID']; ?>');"  data-backdrop="static" data-keyboard="false" data-toggle="modal"><i class="fa fa-eye"></i></a>
							
							
						<a href="#" onclick="del_role_id('<?php  echo $plan['ListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Role" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>	
				
					</div>
					</td>
				
					
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<div id="plan_product" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">Products & Services</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_product"  style="height: 300px; min-height:300px;  overflow: auto; " >
			
			<tr colspan="2">No data found! </tr>
			</div>
			<hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
 
 function view_plan_details(planID){
	 
	
	 if(planID!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>home/plan_product_details',
		  data : {'planID':planID},
		  success: function(data){
			  
			     $('#data_form_product').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 }


</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>


</div>
<!-- END Page Content -->