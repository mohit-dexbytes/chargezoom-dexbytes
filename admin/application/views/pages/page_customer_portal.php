	<div id="page-content">
      <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i>Customer Portal
            </h1>
        </div>
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    </div>
    
    
    
    
    
    <div class="block full">
        <!-- Working Tabs Title -->
        <div class="block-title">
            <h2>Customer <small> Portal</small></h2>
        </div>
        <!-- END Working Tabs Title -->

        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                <div class="block full">
                    <!-- Block Tabs Title -->
                    
                    <div class="block-title">
                     
                        <ul class="nav nav-tabs " data-toggle="tabs">
                            <li class="active"><a href="#portal_setting">Your Customer Portal</a></li>
                            <li><a href="#portal_preview">Preview</a></li>
                           
                        </ul>
                    </div>
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="portal_setting">
                              <form id="form-portal" action="<?php echo base_url(); ?>SettingConfig/setting_customer_portal" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                   
                                     <div class="form-group">
                                        <label class="col-md-4 control-label" for="type">Enable Customer Portal?</label>
                                        <div class="col-md-7">
                                  
                                          <select id="enable" name="enable" class="form-control">
                                                <option value="0" <?php if(isset($setting) && !empty($setting) && $setting['customerPortal']=='0'){ echo "selected"; } ?>  >No</option>
                                                <option value="1" <?php if(isset($setting) && !empty($setting) && $setting['customerPortal']=='1'){ echo "selected"; } ?> >Yes</option>
                                              
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="portal_url">Your Customer Portal URL</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                            
                                             <input type="text" id="portal_url" name="portal_url"  value="<?php if(isset($setting) && !empty($setting)){ echo $setting['portalprefix']; } ?>"   class="form-control" placeholder="Enter the portal url">
                                             
                                        <span class="input-group-addon"> .chargezoom.net </span>
                                        
                                        </div>
                                     </div>
                                </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customer_help_text">Customer Help Text</label>
                                        <div class="col-md-7">
	                                        <textarea id="customer_help_text" name="customer_help_text"  class="form-control"     placeholder="Enter the text"> <?php if(isset($setting) && !empty($setting) && $setting['customerHelpText']!=''){ echo $setting['customerHelpText']; } ?></textarea>
                                            
                                        </div>
                                    </div>
                                      
                                    
                                       <div class="form-group">
                                        <label class="col-md-4 control-label" for="type">Show "My Info" tab at Customer Portal?</label>
                                        <div class="col-md-7">
                                  
                                          <select id="myInfo" name="myInfo" class="form-control">
                                                <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showInfoTab']=='1'){ echo "selected"; } ?> >Yes</option>
                                                <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showInfoTab']=='0'){ echo "selected"; } ?> >No</option>
                                              
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   <div class="form-group">
                                        <label class="col-md-4 control-label" for="type">Show "My Password" tab at Customer Portal?</label>
                                        <div class="col-md-7">
                                  
                                          <select id="myPass" name="myPass" class="form-control">
                                                <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showPassword']=='1'){ echo "selected"; } ?> >Yes</option>
                                                <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showPassword']=='0'){ echo "selected"; } ?>  >No</option>
                                              
                                            </select>  
                                            
                                        </div>
                                    </div>
                                  <div class="form-group">
                                        <label class="col-md-4 control-label" for="type">Show "My Subscriptions" tab at Customer Portal?</label>
                                        <div class="col-md-7">
                                  
                                          <select id="mySubscriptions" name="mySubscriptions" class="form-control">
                                                <option value="1"  <?php if(isset($setting) && !empty($setting) && $setting['showSubscription']=='1'){ echo "selected"; } ?> >Yes</option>
                                                <option value="0"  <?php if(isset($setting) && !empty($setting) && $setting['showSubscription']=='0'){ echo "selected"; } ?> >No</option>
                                              
                                            </select>  
                                            
                                        </div>
                                    </div>
                                    
                                 
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-4">
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Save Settings</button>
                                       
                                        
                                      
                                    </div>
                                </div>  
                                
                           </form>
                        </div>
                        <div class="tab-pane" id="portal_preview"><iframe width="100%" height="450" src="<?php if(isset($setting) && !empty($setting)){ echo $setting['customerPortalURL']; }else{ echo "http://defaul.chargezoom.net/";} ?>"> </iframe></div>
                      
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Block Tabs -->
            </div>
           
        </div>
        <!-- END Working Tabs Content -->
    </div>
   
   <br>
   <br>
   
<script>


$(function(){       
   
    $('#form-portal').validate({ // initialize plugin
		ignore:":not(:visible)",

       errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
		
		rules: {
		       'customer_help_text': {
                        required: true,
                         minlength: 3
                    },
                  	
					'portal_url':{
						 required : true,
						 minlength: 3,
						 "remote" :function(){return Chk_url('portal_url');} 
					}			
				 
			
			},
    });
  
});  

       var Chk_url=function(element_name){ 
       
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>SettingConfig/check_url',
                    type: "post",
					data:
                        {
                            portal_url: function(){return $('input[name=portal_url]').val();},
                        },
                        dataFilter:function(response){
						   data=$.parseJSON(response);
						
                             if (data['status'] == 'true') {
                                  
							
							    
						     	$('#portal_url').removeClass('error');	 
								 $('#portal_url-error').hide();	 
								 
								 
									   return data['status'] ;				   
									 
                              } else {
                               
								   if(data['status'] =='false'){
								      
                                       $('#form-portal').validate().showErrors(function(){
										   
                                                  return {key:'portal_url'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	

  </script>
   
   </div>