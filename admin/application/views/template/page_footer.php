<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>

  
 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"> </script>
 
 
    <script>
	
var gtype ='';
	  function stripeResponseHandler_res(status, response) {

                if (response.error) {
                    // Re-enable the submit button
                    $('#submit_btn').removeAttr("disabled");
                    // Show the errors on the form

                    $('#payment_error').text(response.error.message);

                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

                }
            }
			
			
$(document).ready(function(){
 $('#merchantCardID').change(function(){
			
		var cardlID =  $(this).val();
		
		  if(cardlID!='' && cardlID !='new1' ){
			  
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_data",
				data : {'merchantCardID':cardlID},
				success : function(response){
					
					     data=$.parseJSON(response);
					    if(data['status']=='success'){
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['CardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['CardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						   

									// Prevent the form from submitting with the default action
									
					        }	   
					
				}
				
				
			});
		  }
		
	});		
     
});

function set_subs_id(sID){
	
	  $('#subscID').val(sID);
	
}

  

function set_customerList_id(sID){
	
	  $('#custID').val(sID);
	
}


function del_credit_id(id){
	
	     $('#invoicecreditid').val(id);
}



function del_user_id(id){
	
	     $('#merchantID').val(id);
}



function set_company_id(id){
	
	     $('#companyID').val(id);
}	        
  
function set_invoice_id(id){
	
	     $('#invoiceID').val(id);
}	      

	
	
	

function set_invoice_schedule_id(id, ind_date){
	$('#scheduleID').val(id);

	var nowDate = new Date(ind_date);
	
	
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
	 
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}	



	
	
	
	
	
	
	
	
function set_invoice_process_id(id, mid){
	
	     $('#invoiceProcessID').val(id);
	     
	     
		 
		if(mid!=""){
			$('#merchantCardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_vault",
				data : {'merchantID':mid},
				success : function(response){
					
					     data=$.parseJSON(response);

					     if(data['status']=='success'){
						
                              var s=$('#merchantCardID');
							
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['merchantCardID'], text: card1[val]['merchantFriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}	      

	      
/********************Show The Payment Data**************/
 
function set_payment_data(id){
	
    if(id !=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>Payments/view_transaction",
			data:{ 'invoiceID':id},
			success : function(response){
			
				$('#pay-content-data').html(response);
				
			}
			
		});
		
	}  
  
}

function set_url(){  
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>Payments/pay_trace_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Payments/pay_auth_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Payments/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>Payments/pay_paypal_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>Payments/pay_stripe_invoice";
							   
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	 
	
	
	
 	  
function set_subs_id(sID){
	
	  $('#subscID').val(sID);
	
}
	
 function set_merchantList_active_id(sID){
	
	  $('#merchantActiveID').val(sID);
	
}
  

function set_merchantList_id(sID){
	
	  $('#merchantID').val(sID);
	
}



</script>  <!-- Footer -->
           
            <!-- END Footer -->
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->




<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<?php $session = $this->session->userdata('admin_logged_in'); ?>
<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->



<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Password</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                
                <?php //print_r($session); ?>
                <form  id="admin_form" action="<?php echo base_url(); ?>home/change_password" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Name</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['adminFirstName'].' '.$session['adminLastName']; ?></p>
								<input type="hidden" value="<?php echo $session['adminID'];  ?>"  name="adminID" id="adminID" />
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['adminEmail'];  ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Current Password</label>
                            <div class="col-md-8">
                                <input type="password" id="current_password" name="current_password" class="form-control">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="new_password" name="new_password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="admin_password" name="admin_password" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12 text-right">
                                 <button type="submit" class="btn btn-sm btn-success newSaveButton">Save</button>
                                <button type="button" class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
                               
                            </div>
                        </div>
                        <div class="form-group">
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="del_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Terminate Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Subscription/terminate_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to terminate this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="subscID" name="subscID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Terminate Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="active_merchant_sub" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Activate Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Subscription/activate_merchant_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantActiveID" name="merchantActiveID" class="form-control"  value="" />
                              <input type="hidden" id="status" name="status" class="form-control"  value="1" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Activate Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<div id="del_merchant_sub" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Suspend Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Subscription/delete_merchant_subscription' class="form-horizontal" >
                     
                 
					<p>Do you really want to suspend this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantID" name="merchantID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Suspend Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="del_cust" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Customer</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>Payments/delete_customer' class="form-horizontal" >
                     
                 
					<p>Do you really want to deactivate this Customer?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="custID" name="custID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 <!----------------- to view email popup modal --------------------->

 
<!-------Delete Invoice Credit------>

<div id="del_credit" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Credit</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_credit" method="post" action='<?php echo base_url(); ?>Payments/delete_credit' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Credit ? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoicecreditid" name="invoicecreditid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<div id="pay_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Invoice Payment Details</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			 <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center" >Txn ID</th>
                     <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right visible-lg">Date</th>
                     <th class="text-right hidden-xs">Type</th>
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
			  <tbody id="pay-content-data">
                
			
				
			</tbody>
        </table>
                  <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Close</button>
        			</div>
                     <br />
                    <br />
			   
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





<div id="invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Payments/pay_invoice' class="form-horizontal" >
                     
                 
					<p>Do you really want to process this invoice before due date?</p> 
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_url();"  class="form-control">
                                                        <option value="" >Select gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas as $gateway_data){
																?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="merchantCardID" name="merchantCardID"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-warning" value="Process Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



 <script>



</script> 

<!-- END User Settings -->

<script type="text/javascript">

$(document).ready(function(){
   
 
    
    $('#admin_form').validate({
         
               	ignore:":not(:visible)",

       errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                 
	        	rules: {
                   
                    'current_password':{
                         required: true,
                          minlength:6,
						maxlength:12,
							 remote : {
						     
						        beforeSend:function() { 
					      	 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'  style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					       },
					           complete:function() {
						              $(".overlay1").remove();
					             },
                                url :"<?php echo base_url(); ?>home/chk_new_password",
                                type: "POST",
                                
                                dataType: 'json',
                                dataFilter: function(response) {
                                    
                                    var rsdata = jQuery.parseJSON(response);
                                 
                                     if(rsdata.status=='success')
                                     return true;
                                     else{
                                       
                                     return false;
                                     }
                                }
                            },
				    
					
                    },
					'new_password': {
                        required: true,
				    	 minlength:6,
						maxlength:12,
                    },
					'admin_password': {
                        required: true,
                        equalTo:'#new_password',
					
                    }
                
                   
                  
              },
              
            
                
                messages:
               {
                'current_password':
                {
                    required: "This is required",
                      minlength: "Please enter minimun 5 characters",
                      remote: 'Password is not same, enter correct password',
                },
               
            },
     });     
  
});
  function checkForm(form)
  {
	var curr_pass = $('#current_password').val();
	var new_pass = $('#new_password').val();
	var confirm_pass = $('#admin_password').val();
	
	if(curr_pass == '') {
		alert("Please enter current password!");
        $('#current_password').focus();
        return false;
	}
	else if(new_pass == '') {
		alert("Please enter new password!");
        $('#new_password').focus();
        return false;
	}
    else if( new_pass != "" && new_pass == confirm_pass ) {
      if(new_pass.length < 5){
        alert("Password must contain at least Five characters!");
        $('#new_password').focus();
        return false;
      }
    } 
	else {
      alert("Confirm Password dismatched.!");
      $('#admin_password').focus();
      return false;
    } 
    return true;
  }
	setInterval(function(){
	    $.ajax({
	        type: 'POST',
	        url : "<?php echo base_url(); ?>ajaxRequest/check_session",
	        success: function(response){
	            data=$.parseJSON(response);    

	            if(data['status']=='failed'){
	                location.reload(true);
	            }
	            
	        }
	    });
	},5000);
  $(document).on('click','.cvvMask',function(){
  
  	$(this).attr('type','password');
  	$(this).attr('maxlength','4');
  });
  $(document).on('click','.CCMask',function(){
	    $(this).attr('type','password');
  });
</script>


