  
   <!-- Page content -->
	<div id="page-content">
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?>
	</div>
	
    <legend class="leg">General Settings</legend>

    <!-- Progress Bar Wizard Block -->
    <div class="block">
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="reseller_form"  class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/admin_setting" enctype="multipart/form-data" >
			
			 <input type="hidden"  id="adminID" name="adminID" value="<?php if(isset($loginAdminID)){echo $loginAdminID; } ?>" /> 
			<div class= "col-md-6">
			        <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Company Name </label>
					<div class="col-md-6">
					<input type="text" id="adminCompanyName" name="adminCompanyName" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminCompanyName']; } ?>" placeholder="Your Company Name"><?php echo form_error('adminCompanyName'); ?></div>
					</div>
			   
				
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Phone Number </label>
					<div class="col-md-6">
					<input type="text" id="primaryContact" name="primaryContact" class="form-control"  value="<?php if(isset($admin)){ echo $admin['primaryContact']; } ?>" placeholder="Your Primary Contact"><?php echo form_error('primaryContact'); ?></div>
					</div>
				
					
						<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Address Line 1 </label>
					<div class="col-md-6">
					<input type="text" id="adminAddress" name="adminAddress" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminAddress']; } ?>" placeholder="Address 1"><?php echo form_error('adminAddress'); ?></div>
					</div>
					
						
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Address Line 2  </label>
					<div class="col-md-6">
					<input type="text" id="adminAddress2" name="adminAddress2" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminAddress2']; } ?>" placeholder="Address 2"><?php echo form_error('adminAddress2'); ?></div>
					</div>
					
					
				
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Zip Code </label>
					<div class="col-md-6">
					<input type="text" id="zipCode" name="zipCode" class="form-control"  value="<?php if(isset($admin)){ echo $admin['zipCode']; } ?>" placeholder="Your Zip Code"><?php echo form_error('zipCode'); ?></div>
					</div>
					
					</div>
					
					<div class="col-md-6">
					    
					 
					 <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing First Name </label>
					<div class="col-md-6">
					<input type="text" id="billingfirstName" name="billingfirstName" class="form-control"  value="<?php if(isset($admin)){ echo $admin['billingfirstName']; } ?>" placeholder="Your Billing First Name"><?php echo form_error('firstName'); ?></div>
					</div> 
					  <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing Last Name </label>
					<div class="col-md-6">
					<input type="text" id="billinglastName" name="billinglastName" class="form-control"  value="<?php if(isset($admin)){ echo $admin['billinglastName']; } ?>" placeholder="Your Billing last Name"><?php echo form_error('firstName'); ?></div>
					</div>   
				
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">City</label>
						   <div class="col-md-6">
						       	<input type="text" id="city" name="adminCity" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminCity']; } ?>" placeholder="City"><?php echo form_error('adminCity'); ?>
						       
							</div>
                        </div>	
					
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">State</label>
						   <div class="col-md-6">
						       <input type="text" id="state" name="adminState" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminState']; } ?>" placeholder="State"><?php echo form_error('state_name'); ?>
							
							</div>
                        </div>	
					
				
					
					 	<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-6">
								<select id="country" class="form-control " name="adminCountry" >
							
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_name']; ?>" <?php if(isset($admin)){ if($admin['adminCountry']==$country['country_name'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
					  
				
                        
					</div>
				
					<div class="col-md-12">
	                <div class="form-group text-right margin-right-0px">
														
					<button type="submit" class="submit btn btn-sm btn-success">Update</button>					
				
					</div>
					
				    </div>					
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

 
    $('#reseller_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			   'adminCompanyName': {
                        required: true,
                        minlength: 3,
                         
                    },
		       'firstName': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'lastName': {
                        required: true,
                        minlength:2,
                    },
					'primaryContact': {
                        required: true,
                       
                    },
                    
                    'url': {
                        required: true,
                       
                    },
					
				'Email': {
                        required: true,
                        email: true,
						  
                         
                    },
                    
					'billingContact': {
                        required: true,
                       
                    },
					'billingEmailAddress': {
                        required: true,
                        email: true,
                    },
					'federalTaxID': {
                        required: true,
                        
                    },
                    'country': {
                        required: true,
                        
                    },
                    'state': {
                        required: true,
                        
                    },
                    'city': {
                        required: true,
                        
                    },
                    'adminAddress': {
                        required: true,
                        minlength:5,
                    },
					'adminAddress2': {
                        required: true,
                        minlength:5,
                    },
                    'zipCode': {
                        required: true,
                        
                    },
					
                    
			
			},
    });
	
	
	$('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Admin_panel/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Admin_panel/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
	
	
	
});	    

</script>

</div>


