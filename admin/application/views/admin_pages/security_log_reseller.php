<!-- Page content -->
<div id="page-content">
    <legend class="leg">Login Info</legend>
    <div class="block-main full" style="position: relative">
        <table id="security_log_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden-xs">Email Address</th>
                    <th class="text-right hidden-xs">IP Address</th>
                    <th class="text-right hidden-xs">Location</th>
                    <th class="text-right">ISP</th>
                    <th class="text-center">Browser</th>
                    <th class="text-center">Login At</th>
                </tr>
            </thead>
            <tbody>
				
			</tbody>
        </table>
                            
    </div>
</div>


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
   
    
<script>
$(function(){ // EcomOrders.init(); 
Pagination_view.init(); });


var Pagination_view = function() {

return {
    init: function() {
        / Extend with date sort plugin /
        $.extend($.fn.dataTableExt.oSort, {
       
        } );

        / Initialize Bootstrap Datatables Integration /
        App.datatables();

        / Initialize Datatables /
        $('#security_log_page').dataTable({
            "columnDefs": [
                { className: "hidden-xs text-left cust_view", "targets": [ 0 ] },
                { className: "text-right cust_view", "targets": [ 1 ] },
                { className: "hidden-xs text-right", "targets": [ 2 ] },
                { className: "hidden-xs text-right cust_view", "targets": [ 3 ] },
                { className: "hidden-xs text-right", "targets": [ 4 ] },
                { className: "text-right", "targets": [ 5 ] },
            ],
            "order": [[5, 'desc']],
            "sPaginationType": "bootstrap",
            pageLength: 10,
            lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
            "processing": true,
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "ajax": {
                "url": "<?php echo base_url(); ?>ajaxRequest/admin_security_log",
                "type": "POST" ,
                "data":function(data) {
                    data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                    data.logType = 'Reseller';
                },
            },
        });

        / Add placeholder attribute to the search input /
       $('.dataTables_filter input').attr('placeholder', 'Search');
    }
};
}();



</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>