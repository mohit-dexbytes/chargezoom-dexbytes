<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php
        echo $this->session->flashdata('message');
        echo $this->session->flashdata('error'); 
    ?></div>
    <legend class="leg">Disabled Merchants</legend>
    <div class="block-main full" style="position: relative">
        <table id="reseller_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Company Name </th>
                    <th class="text-left hidden-xs">Reseller Name</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
                    if(isset($reseller_list) && $reseller_list){
                        foreach($reseller_list as $reseller){
                            ?>
                                <tr>
                                    <td class="text-left cust_view">
                                        <a href="<?php echo base_url('Admin_panel/create_reseller/'.$reseller['resellerID']); ?>" data-toggle="tooltip" title="Edit Reseller"> <?php echo $reseller['resellerCompanyName']; ?></a>
                                    </td>
                                    <td class="text-left hidden-xs"> <?php echo $reseller['resellerfirstName'].' '.$reseller['lastName']; ?></td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs">
                                            <a href="#enable_reseller" onclick="set_enable_reseller('<?php  echo $reseller['resellerID']?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  title="Delete Reseller" class="btn btn-success">Enable</a>	
                                            <a href="#del_reseller" onclick="set_del_reseller('<?php  echo $reseller['resellerID']?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  title="Delete Reseller" class="btn btn-danger"> <i class="fa fa-times"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                        }
                    }  
                ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    </div>
    <!-- END All Orders Block -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#reseller_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_reseller(id){
	 
	  $('#resellerID').val(id);
	
}

function set_enable_reseller(id){
	 
	  $('#resellerIDs').val(id);
	
}

</script>	
	
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	
	<!--------------------del admin Reseller------------------------>

<div id="del_reseller" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Reseller</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_reseller" method="post" action='<?php echo base_url(); ?>Admin_panel/delete_reseller' class="form-horizontal" >                     
                 
					<p>Do you really want to Delete this Reseller?</p> 					
				    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="resellerID" name="resellerID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





	<!--------------------enable admin Reseller------------------------>

<div id="enable_reseller" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Enable Reseller</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="enable_reseller" method="post" action='<?php echo base_url(); ?>Admin_panel/enable_reseller' class="form-horizontal" >                     
                 
					<p>Do you really want to enable this Reseller?</p> 					
				    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="resellerIDs" name="resellerID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Enable Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>











<script>
$(document).ready(function(){
	$('#reseller_old').change(function(){
	
    var gID =  $('#reseller_old').val();
	$.ajax({
    url: '<?php echo base_url("Admin_panel/get_merchant")?>',
    type: 'POST',
	data:{seller_id:gID},
    success: function(data){
		var data =JSON.parse(data)
		$('#t_data').html(data.data);
	}	
  });
	
	});	
});




function validate_form()
{
valid = true;

if($('input[type=checkbox]:checked').length == 0)
{
    alert ( "ERROR! Please select at least one checkbox" );
    valid = false;
}

return valid;
}
</script>





<!--reassign marchant-->

<div id="reassign_marchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reassign Maechant</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>Admin_panel/reasign_marchent' class="form-horizontal" onsubmit="return validate_form();">
				 
                     
                 <p> Reassign Maechant here.</p> 
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Select Maechant</label>
						 <div class="col-md-6">
						   <select id="reseller_old" name="reseller_old"  class="form-control" required>
								   <option value="" >Select Reseller</option>
								    <?php foreach($merchant as $merchant){  ?>
								    <option value="<?php echo $merchant['resellerID']; ?>" ><?php echo $merchant['resellerCompanyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
						
						<div class="form-group ">
                        <label class="col-md-4 control-label" for="card_list">Select Maechant</label>
						 <div class="col-md-6">
						 
						   <select id="reseller_new" name="reseller_new"  class="form-control" required>
								   <option value="" >Select Reseller</option>
								    <?php foreach($reasign as $reasign){  ?>
								    <option value="<?php echo $reasign['resellerID']; ?>" ><?php echo $reasign['resellerCompanyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				
					
						<table>
					    <hr/>
							<tbody id="t_data">
									 
						   </tbody>
						</table>
					
					
                    <br />
                    <br />
						
				<div class="form-group ">
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel1" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"   class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					    <hr/>
				<tbody id="t_data">
						 
		       </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>



