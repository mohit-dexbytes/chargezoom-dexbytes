<!-- Page content -->
<div id="page-content">
    	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- eCommerce Dashboard Header -->
    
    <!-- END eCommerce Dashboard Header -->

    <div class="block full">
    <div class="row">
     
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-handshake-o" aria-hidden="true"></i>

              </div>
               <h4 class="widget-content-light"><strong>Resellers</strong></h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php if(isset($dashboard_list)){ echo $dashboard_list['reseller_count']; } ?> </strong></span>
            
            </div>
          </div>
        </div>
        
        
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-money"></i>
              </div>
               <h4 class="widget-content-light"><strong>Merchants</strong> </h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php if(isset($dashboard_list)){ echo $dashboard_list['mr_count']; } ?></strong></span>
              </div>
          </div>
        </div>
        
        
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-users" aria-hidden="true"></i>
              </div>
               <h4 class="widget-content-light"><strong>Customers </strong></h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php if(isset($dashboard_list)){ echo $dashboard_list['c_count']; } ?></strong></span>
              
            </div>
          </div>
        </div>
        
       <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
               <i class="fa fa-file-o" aria-hidden="true"></i>

              </div>
               <h4 class="widget-content-light"><strong>Revenue </strong></h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong>$<?php if(isset($dashboard_list)){ echo $dashboard_list['revenue_count']; } ?></strong></span>
              
            </div>
          </div>
        </div>
     </div>   
     <div class="col-md-12"><br/></div>
    <!-- END Quick Stats -->
<!-- Page content -->

    <!-- eShop Overview Block -->
    
       <div class="row ">
       
             <div class="col-md-12">
             <div class="col-md-12 block full">
                <div class="block-title text-left">
                  <h2><strong>Revenue History</strong>: <span id="totalPayment">0</span></h2>
                </div>
                 <div id="chart-classic" style="height: 350px;" class="chart">
                </div>  
             </div>  
           </div>
           
		</div>	
         

		
	    <div class="row">
		 <div class="col-md-4">
            <!-- Bars Chart Block -->
            <div class="block full">
                <!-- Bars Chart Title -->
                <div class="block-title text-left">
                    <h2><strong>Resellers: <span><?php if(isset($dashboard_list)){ echo $dashboard_list1['reseller_count']; } ?></span></strong></h2>
                </div>
               
                <div id="chart-bars-new-res" class="chart chart_new"></div>
                <!-- END Bars Chart Content -->
            </div>
            <!-- END Bars Chart Block -->
        </div>
        <div class="col-md-4">
            <!-- Bars Chart Block -->
            <div class="block full">
                <!-- Bars Chart Title -->
                <div class="block-title text-left">
                    <h2><strong>Merchants: <span><?php if(isset($dashboard_list)){ echo $dashboard_list1['mr_count']; } ?></span></strong></h2>
                </div>
               
                <div id="chart-bars-new" class="chart chart_new"></div>
                <!-- END Bars Chart Content -->
            </div>
            <!-- END Bars Chart Block -->
        </div>
 
        
        <div class="col-md-4">
            <!-- Classic Chart Block -->
            <div class="block full">
                <!-- Classic Chart Title -->
                <div class="block-title text-left">
                    <h2><strong> Plan Ratio:</strong> ES:<span id="ss_per">0 </span>%  / AS:<span id="as_per">0 </span>%  / VT:<span id="vt_per">0</span>%</span></h2>
                   
                </div>
                <!-- END Classic Chart Title -->

                <!-- Classic Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-pie1" class="chart chart_new"></div>
                <!-- END Classic Chart Content -->
            </div>
            <!-- END Classic Chart Block -->
        </div>
         <div class="col-md-12">
         <div class="col-sm-12 block full">
                <div class="block-title text-left">
                  <h2><strong>Total Processing Volume: </strong> <span id="totalVolume">0</span></h2>
                </div>
		
			  <div id="chart2" style="height: 350px;" class="chart">
                </div>  
             </div>
             </div>
    </div>
    
     <div class="row">
        <div class="col-lg-6">
            <!-- Latest Orders Block -->
            <div class="block">
                <!-- Latest Orders Title -->
                <div class="block-title text-left">
                   
                    <h2><strong>Top 10 Resellers</strong></h2>
                </div>
                <!-- END Latest Orders Title -->
			    <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                        
                           <th class="text-left" ><strong>Reseller</strong></th>
                            <th class="text-right" ><strong>Merchants</strong></th>
							  <th class="text-right"><strong>Volume</strong></th>
                          
					 </thead>	
						<tbody id='res_dt'>
                       
                        
                    </tbody>
                </table>
              
                <!-- END Latest Orders Content -->
            </div>
            <!-- END Latest Orders Block -->
        </div>
        <div class="col-lg-6">
            <!-- Top Products Block -->
            <div class="block">
                <!-- Top Products Title -->
                <div class="block-title text-left">
                  
                    <h2><strong>Top 10 Merchants</strong></h2>
                </div>
                <!-- END Top Products Title -->

                <!-- Top Products Content -->
               <table class="table table-bordered table-striped table-vcenter">
                    <thead>
                       
                            <th class="text-left" ><strong>Merchant</strong></th>
                            <th class="text-right" ><strong>Customers</strong></th>
							  <th class="text-right"><strong>Volume</strong></th>

				</thead>
				<tbody id="mer_dt">
				    
				</tbody>
						
                       
                        
                    </tbody>
                </table>
                <!-- END Top Products Content -->
            </div>
            <!-- END Top Products Block -->
        </div>
    </div>
   

	 </div>  
	
        
    </div>
    <!-- END Quick Stats -->

    
    
    
	  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	  <script src="<?php echo base_url(JS); ?>/pages/compCharts.js"></script>
       <link id="themecss" rel="stylesheet" type="text/css" href="//www.shieldui.com/shared/components/latest/css/light/all.min.css" />          
    <script src="<?php echo base_url(JS); ?>/pages/shieldui-all.min.js"></script>    
    
<script type="text/javascript">
 
var pchart_color = []; 
var chartColor=[];  

      pchart_color.push({label:'Subscription Suite', data:parseFloat(<?php echo  $dashboard_list['ss_amount']; ?>) });
      pchart_color.push({label:'Virtual Terminal', data:parseFloat(<?php echo  $dashboard_list['vt_amount']; ?>)  });
      pchart_color.push({label:'Automation Terminal', data:parseFloat(<?php echo  $dashboard_list['as_amount']; ?>)  });
   	chartColor.push('#46216f');
	
	 chartColor.push('#0077f7');
	 chartColor.push('#28a745');
  
     function format2(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return "$" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];


}
     
     
  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
}  
                
$(function(){        var merchant=[];var res_val=[];var plan=[];

  var data3 = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
CompChartsnew1.init();          
                
           $.ajax({
               
			type:"POST",
			url : "<?php echo base_url(); ?>Admin_panel/reseller_merchant_chart",
            success: function (data) {
              data=$.parseJSON(data);
            var ch_date='';  var  earnwww='0.00';
            var revn=''; 
            
               revn  = data['chart']['revenue'];
             ch_date =data['chart']['month'];
             earnwww =data['chart']['volume'];
             newDataSalesmerch =data['chart']['merchant'];
              newDataSalesreseller =data['chart']['reseller_no'];
              var totalPayment  =data['chart']['total_revenue'];
              var totalVolume  =data['chart']['total_volume'];
             var s=$('#res_dt');
              var s1=$('#mer_dt');
              $('#totalPayment').html(format2(totalPayment));
              $('#totalVolume').html(format2(totalVolume));
               
               if (!jQuery.isEmptyObject(data['res_list'])){ 
                    var reseller = data['res_list']
							            for(var val1 in  reseller) {
							                 res_val.push(parseFloat(reseller[val1]['volume']));
							             $('<tr ><td  class="text-left">'+reseller[val1]['resellerCompanyName']+'</td><td  class="text-right">'+reseller[val1]['mr_count']+'</td><td  class="text-right">'+ '<br>'+ format2(reseller[val1]['volume'],2) +'</td></tr>').appendTo(s);           
							                
							            }
                        } 
             if (!jQuery.isEmptyObject(data['merch_list'])){ 
                                     var merch =data['merch_list'];
							            for(var val in  merch) {
							             
							         $('<tr ><td  class="text-left">'+merch[val]['companyName']+'</td><td  class="text-right">'+merch[val]['customer_count']+'</td><td  class="text-right">'+ '<br>'+format2(merch[val]['revenue'],2) +'</td></tr>').appendTo(s1);        
							            }
             }  
             
              if (!jQuery.isEmptyObject(data['plan_ratio'])){ 
                                     var merch =data['plan_ratio'];
							            for(var val in  merch) {
							              plan.push(parseInt(merch[val]['mr_count'])); 
							              
							            }
             }  
            
             
    
       
        sum = 0;
           $.each(newDataSalesmerch,function(){sum+=parseFloat(this) || 0;});
         var revenue =0;   
          $.each(revn,function(){revenue+=parseFloat(this) || 0;});
          // chart_new_com
           
           $('#com_merch').html(format2(revenue,2)) ; 
  $('#tot_merch').html(sum) ;  
   $("#chart-classic").shieldChart({
                        theme: "light",
              tooltipSettings: {
                   chartBound: true,
     
                   customPointText: function (point, chart){
                       return  shield.format(
                            '<span>{value}</span>',
                            {
                                value: format2(point.y,2)
                            }
                        );
                    }
                },
            
            primaryHeader: {
                text: ""
            },
            exportOptions: {
                image: false,
                print: false
            },
            axisX: {
                categoricalValues: ch_date
            },
          
			
            dataSeries: [{
                seriesType: 'splinearea',
                collectionAlias: "$ ",
                data: revn
            }]
			
        });
   
   

            $("#chart2").shieldChart({
				primaryHeader: {
                text: ""
            },
			
	
                exportOptions: {
                    image: false,
                    print: false
                },
                axisY: {
                    title: {
                        text: ""
                    },
                    min: 10
                    },
								
					axisX: {
						axisTickText: {
						textAngle: 23
					},
                categoricalValues: ch_date
                   },
            tooltipSettings: {
                chartBound: true,
                axisMarkers: {
                    enabled: true,
                    mode: 'xy'
                }                    
            },


                dataSeries: [{
					collectionAlias: "Merchant",
                    seriesType: "line",
                    stackIndex: 0,
                    color: "#46216f",
                    data: earnwww
                }],
                 tooltipSettings: {
         chartBound: true,
     
        customPointText: function (point, chart) {
         
            return  shield.format(
                '<span>{value}</span>',
                {
                    value: format2(point.y)
                }
            );
        }
    },
				
            });

       
           
        
   $("#chart-bars-new").shieldChart({
                theme: "light",
                 tooltipSettings: {
         chartBound: true,
    
        customPointText: function (point, chart) {
            return shield.format(
                '<span>{value}</span>',
                {
                    value: point.y
                }
            );
        }
    },
                primaryHeader: {
                    text: " "
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                axisX: {
                    categoricalValues:ch_date
                },
                axisY: {
                    title: {
                        text: ""
                    }
                },
                dataSeries: [{
                    seriesType: "bar",
                    collectionAlias: "Merchants",
                    data: newDataSalesmerch
                }]
            });
      $("#chart-bars-new-res").shieldChart({
                theme: "light",
                 tooltipSettings: {
         chartBound: true,
    
        customPointText: function (point, chart) {
            return shield.format(
                '<span>{value}</span>',
                {
                    value: point.y
                }
            );
        }
    },
                primaryHeader: {
                    text: " "
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                axisX: {
                    categoricalValues:ch_date
                },
                axisY: {
                    title: {
                        text: ""
                    }
                },
                dataSeries: [{
                    seriesType: "bar",
                    collectionAlias: "Reseller",
                    data: newDataSalesreseller
                }]
            });
            
            
     $("#chart_new_com").shieldChart({
                theme: "light",
                 tooltipSettings: {
         chartBound: true,
      
        customPointText: function (point, chart) {
            return shield.format(
                '<span>{value}</span>',
                {
                    value: format2(point.y,2)
                }
            );
        }
    },
                primaryHeader: {
                    text: " "
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                axisX: {
                    categoricalValues:ch_date
                },
                axisY: {
                    title: {
                        text: ""
                    }
                },
                dataSeries: [{
                    seriesType: "bar",
                    collectionAlias: "Commission",
                    data: revn,
                }]
            });        
        }
    		});    
                  

});


var CompChartsnew1 = function() {

    return {
        init: function() {
           
                var chartPie = $('#chart-pie1');
			
	               $.plot(chartPie,  pchart_color,
                    {
                    colors: chartColor,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) { 
                                    
                                    if(label=="Subscription Suite")
                                    {
                                        
                                        $('#ss_per').html(Math.round(pieSeries.percent));
                                    
                                    }
                                    
                                     if(label=="Virtual Terminal")
                                     {
                                         
                                         $('#vt_per').html(Math.round(Math.round(pieSeries.percent)));
                                     }
                                     if(label=="Automation Terminal")
                                     {
                                         
                                         $('#as_per').html(Math.round(Math.round(pieSeries.percent)));
                                     }
                                    if(label=="Subscription Suite")
                                    {
                                        var dis_label = 'Enterprise Suite';
                                    }
                                   
                                    if(label=="Virtual Terminal")
                                     {
                                        var dis_label = 'Virtual Terminal';
                                    }
                                    if(label=="Automation Terminal")
                                    {
                                        var dis_label = 'Automation Suite';
                                    }
                                    return '<div class="chart-pie-label">' + dis_label + '<br>'+pieSeries.data[0][1]+ '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );
        
        
        }
    }
}();


var CompChartsnew = function() {

    return {
        init: function() {
           
                var chartPie = $('#chart-pie');
			
	               $.plot(chartPie,  pchart_color,
                    {
                    colors: chartColor,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) { 
                                  
                                    return '<div class="chart-pie-label">' + label + '<br>'+pieSeries.percent+ '%</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );
        
        
        }
    }
}();


	</script>
	
	
	 
<style>
	#chart-classic g text tspan {
    visibility: visible;
}
#chart-classic g {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-classic text tspan {
    visibility: hidden;
}



	#chart-bars-new-res g text tspan {
    visibility: visible;
}
#chart-bars-new-resg {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-bars-new-res text tspan {
    visibility: hidden;
}

	#chart-bars-new g text tspan {
    visibility: visible;
}
#chart-bars-newg {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-bars-new text tspan {
    visibility: hidden;
}
	#chart_new_com g text tspan {
    visibility: visible;
}
#chart_new_com g {
    opacity: 0.7;
	borderWidth:2px;
}

#chart_new_com text tspan {
    visibility: hidden;
}

.chart_new {
    height: 260px !important;
}
.widget.widg_border {
    border: 1px solid #e8e8e8 !important;
}




#chart2 g text tspan {
    visibility: visible;
}


#chart2 text tspan {
    visibility: hidden;
}


</style>
    
    