<!-- Page content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/adminDashboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/global.css">

<div id="page-content">

    <?php if (isset($passwordExpDays)) { ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-non-fade alert-warning">
                    <strong>Password Update:</strong> &nbsp;
                    A required password change is due in <?= $passwordExpDays ?> days. &nbsp;Click
                    <a class="alert-link" href="#modal-user-settings" data-toggle="modal" data-original-title="" title="" style="font-weight:700;">here</a>
                    to change your password.
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="row">
        <?php if ($this->session->flashdata('error-msg')) { ?>
      <div class="alert alert-danger">
          <strong>Error:</strong> <?= $this->session->flashdata('error-msg') ?>
      </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success-msg')) { ?>
            <div class="alert alert-success">
                <?= $this->session->flashdata('success-msg') ?>
            </div>
        <?php } ?>
      <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
      <div class="col-sm-6 col-lg-3">
          <div class="card-box">
              <div class="media">
                  <div class="avatar-md bg-success rounded-circle mr-2">
                      <i class="fa fa-handshake-o ion-logo-usd avatar-title font-26 text-white"></i>
                      
                  </div>
                  <div class="media-body align-self-center">
                      <div class="text-right">
                          <h4 class="font-20 my-0 font-weight-bold">
                            <span data-plugin="counterup">
                              <?php if(isset($dashboard_list)){ echo $dashboard_list['reseller_count']; } ?>
                                
                            </span>
                          </h4>
                          <p class="text-truncate">Resellers</p>
                      </div>
                  </div>
              </div>
              
          </div>
          <!-- end card-box-->
      </div>
      <div class="col-sm-6 col-lg-3">
          <div class="card-box">
              <div class="media">
                  <div class="avatar-md bg-info rounded-circle mr-2">
                      <i class="fa fa-money ion-logo-usd avatar-title font-26 text-white"></i>
                     
                  </div>
                  <div class="media-body align-self-center">
                      <div class="text-right">
                          <h4 class="font-20 my-0 font-weight-bold">
                            <span data-plugin="counterup">
                              <?php if(isset($dashboard_list)){ echo $dashboard_list['mr_count']; } ?>
                                
                            </span>
                          </h4>
                          <p class="text-truncate">Merchants</p>
                      </div>
                  </div>
              </div>
              
          </div>
          <!-- end card-box-->
      </div>

      <div class="col-sm-6 col-lg-3">
          <div class="card-box">
              <div class="media">
                  <div class="avatar-md bg-warning rounded-circle mr-2">
                      <i class="fa fa-users ion-logo-usd avatar-title font-26 text-white"></i>
                      
                  </div>
                  <div class="media-body align-self-center">
                      <div class="text-right">
                          <h4 class="font-20 my-0 font-weight-bold">
                            <span data-plugin="counterup">
                              <?php if(isset($dashboard_list)){ echo $dashboard_list['c_count']; } ?>
                            </span>
                          </h4>
                          <p class="text-truncate">Customers</p>
                      </div>
                  </div>
              </div>
              
          </div>
          <!-- end card-box-->
      </div>

      <div class="col-sm-6 col-lg-3">
          <div class="card-box">
              <div class="media">
                  <div class="avatar-md bg-danger rounded-circle mr-2">
                      <i class="fa fa-file-o ion-logo-usd avatar-title font-26 text-white"></i>
                      
                  </div>
                  <div class="media-body align-self-center">
                      <div class="text-right">
                          <h4 class="font-20 my-0 font-weight-bold">
                            <span data-plugin="counterup">
                              $<?php if(isset($dashboard_list)){ echo $dashboard_list['revenue_count']; } ?>
                            </span>
                          </h4>
                          <p class="text-truncate">Revenue</p>
                      </div>
                  </div>
              </div>
              
          </div>
          <!-- end card-box-->
      </div>

    </div>   

    <div class="row ">
      <div class="col-sm-12">
        <div class="block full">
            <div class="card-header py-3 bg-transparent">
                
                <h5 class="card-title">Revenue History: <span id="totalPayment">$0.00</span></h5>
            </div>
            
            <div class="revenueGraph">
              
                <div id="placeholder" style="height:350px;" class="demo-placeholder"></div>
                <div id="choices" >
                    
                </div>

                <div id="legendContainer" class="legend" ></div>

            </div>
        </div>
            
      </div>
           
    </div>  

    <div class="row ">
      <div class="col-sm-12">
        <div class="block full">
            <div class="card-header py-3 bg-transparent">
                
                <h5 class="card-title">Total Processing Volume: <span id="totalVolume">0</span></h5>
            </div>
            
            <div class="volumnGraph">
              
                <div id="volumnPlaceholder" style="height:350px;" class="volumn-placeholder"></div>
                <div id="volumnChoices" >
                    
                </div>

                <div id="legendVolumnContainer" class="legend" ></div>

            </div>
        </div>
            
      </div>
           
    </div> 


    <div class="row ">
      <div class="col-sm-12">
        <div class="block full">
            <div class="card-header py-3 bg-transparent">
                <h5 class="card-title">Merchants: <span ><?php if(isset($dashboard_list)){ echo $dashboard_list1['mr_count']; } ?></span></h5>
            </div>
            
            <div class="merchantGraph">
              
              <canvas id="chart-bars-new" height='100'></canvas>
              

          </div>
        </div>
            
      </div>
           
    </div> 


    <div class="row">
      <div class="col-md-6">
        <!-- Bars Chart Block -->
        <div class="block full">
            <div class="card-header py-3 bg-transparent">
                
                <h5 class="card-title">Resellers: <span ><?php if(isset($dashboard_list)){ echo $dashboard_list1['reseller_count']; } ?></span></h5>
            </div>
            
            <div class="resellerGraph">
              
                <div id="chart-bars-new-res" class="chart chart_new"></div>

            </div>
        </div>
        
        <!-- END Bars Chart Block -->
      </div>

      
      <div class="col-md-6">
        <!-- Bars Chart Block -->
        <div class="block full">
            <div class="card-header py-3 bg-transparent">
                <?php
                  $ss_per_ptg = 0;
                  $vt_per_ptg = 0;
                  $as_per_ptg = 0;
                  $free_per_ptg = 0;
                  $chart_sum = $dashboard_list['ss_amount'] + $dashboard_list['vt_amount'] + $dashboard_list['as_amount'] + $dashboard_list['free_amount'];
                  if($chart_sum > 0){
                    $ss_per_ptg = ($dashboard_list['ss_amount'] * 100 / $chart_sum);
                    $vt_per_ptg = ($dashboard_list['vt_amount'] * 100 / $chart_sum);
                    $as_per_ptg = ($dashboard_list['as_amount'] * 100 / $chart_sum);  
                    $free_per_ptg = ($dashboard_list['free_amount'] * 100 / $chart_sum);  
                  }
                ?>
                <h5 class="card-title">Plan Ratio</h5>
            </div>
            
            <div class="resellerGraph">
              
                <div id="chart-pie1" class=""></div>

            </div>
        </div>
        
        <!-- END Bars Chart Block -->
      </div>
    </div>     
        
    <div class="row">
      <div class="col-lg-6">
            <!-- Latest Orders Block -->
        <div class="block full">
          <div class="card-header py-3 bg-transparent bottomPad" >
                
              <h5 class="card-title">Top 10 Resellers </h5>
          </div>
              
         <!-- END Latest Orders Title -->
          <table class="table table-bordered table-striped table-vcenter">
            <thead>
                        
              <th class="text-left" ><strong>Reseller</strong></th>
              <th class="text-right" ><strong>Merchants</strong></th>
              <th class="text-right"><strong>Volume</strong></th>
                          
            </thead> 
            <tbody id='res_dt'>
            </tbody>
          </table>
              
                <!-- END Latest Orders Content -->
        </div>
            <!-- END Latest Orders Block -->
      </div>

      <div class="col-lg-6">
            <!-- Latest Orders Block -->
        <div class="block full">
          <div class="card-header py-3 bg-transparent bottomPad" >
                
              <h5 class="card-title">Top 10 Merchants </h5>
          </div>
              
         <!-- END Latest Orders Title -->
          <table class="table table-bordered table-striped table-vcenter">
            <thead>
                       
              <th class="text-left" ><strong>Merchant</strong></th>
              <th class="text-right" ><strong>Customers</strong></th>
              <th class="text-right"><strong>Volume</strong></th>

            </thead>
            <tbody id="mer_dt">
                
            </tbody>
            
          </table>
              
                <!-- END Latest Orders Content -->
        </div>
            <!-- END Latest Orders Block -->
      </div>
    </div>
  
</div>  
  
<!-- flot chart -->
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.time.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.selection.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.stack.js"></script>

<link href="<?php echo base_url(CSS); ?>/moltran/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />    
    
<script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="<?php echo base_url(JS); ?>/pages/compCharts.js"></script>
<link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
<script type="text/javascript" src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>    
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    
<script type="text/javascript">
 
var pchart_color = []; 
var chartColor=[];  

var label1 = 'Free: <?php echo $dashboard_list['free_amount']; ?>';
var label2 = 'Virtual Terminal: <?php echo $dashboard_list['vt_amount']; ?>';
var label3 = 'Automation Suite: <?php echo $dashboard_list['as_amount']; ?>';
var label4 = 'Enterprise Suite: <?php echo $dashboard_list['ss_amount']; ?>';

    pchart_color = [[label1, parseFloat(<?php echo $dashboard_list['free_amount']; ?>)],[label2, parseFloat(<?php echo $dashboard_list['vt_amount']; ?>)],[label3, parseFloat(<?php echo $dashboard_list['as_amount']; ?>)],[label4, parseFloat(<?php echo $dashboard_list['ss_amount']; ?>)]];

    showLegend = true;
    chartColor.push('#7f7f7f');
    chartColor.push('#0077f7');
    chartColor.push('#28a745');
    chartColor.push('#46216f');
  
<?php 
    if($chart_sum == 0){
?>
pchart_color=[['No Data', 0.0001]];
chartColor = ['#7F7F7F'];
showLegend = false;
<?php
    }

?>
  function format2(num)
  {
   
    var p = parseFloat(num).toFixed(2).split(".");
    return "$" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
  }
     
     
  function roundN(num,n){
    return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
  }  
                
  $(function(){
      var merchant=[];var res_val=[];var plan=[];

      var data3 = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
                
      $.ajax({
          type:"POST",
          url : "<?php echo base_url(); ?>AdminCron/admin_dashboard_chart_json",
          data:{'fetchData':true},
          dataType:'json',
          success: function (data)
          {
               
                var ch_date='';  var  earnwww='0.00';
                var revn=''; 

                revn  = data['chart']['revenue'];
                ch_date =data['chart']['month'];
                earnwww =data['chart']['volume'];
                newDataSalesmerch =data['chart']['merchant'];
                newDataSalesreseller =data['chart']['reseller_no'];
                var totalPayment  =data['chart']['total_revenue'];
               
                var s=$('#res_dt');
                var s1=$('#mer_dt');
                $('#totalPayment').html(format2(totalPayment));
                
           
                if (!jQuery.isEmptyObject(data['res_list'])){ 
                    var reseller = data['res_list'];
                    for(var val1 in  reseller) {
                         res_val.push(parseFloat(reseller[val1]['volume']));
                          $('<tr ><td  class="text-left">'+reseller[val1]['resellerCompanyName']+'</td><td  class="text-right">'+reseller[val1]['mr_count']+'</td><td  class="text-right">'+ '<br>'+ format2(reseller[val1]['volume'],2) +'</td></tr>').appendTo(s);           
                        
                    }
                } 
                if (!jQuery.isEmptyObject(data['merch_list'])){ 
                    var merch =data['merch_list'];
                    for(var val in  merch) {
                       
                      $('<tr ><td  class="text-left">'+merch[val]['companyName']+'</td><td  class="text-right">'+merch[val]['customer_count']+'</td><td  class="text-right">'+ '<br>'+format2(merch[val]['revenue'],2) +'</td></tr>').appendTo(s1);        
                    }
                }  
             
                if (!jQuery.isEmptyObject(data['plan_ratio'])){ 
                    var merch =data['plan_ratio'];
                    for(var val in  merch) {
                      plan.push(parseInt(merch[val]['mr_count'])); 
                           
                    }
                }  
       
                sum = 0;
                $.each(newDataSalesmerch,function(){sum+=parseFloat(this) || 0;});
                var revenue =0;   
                $.each(revn,function(){revenue+=parseFloat(this) || 0;});
         
                $('#com_merch').html(format2(revenue,2)) ; 
                $('#tot_merch').html(sum) ;  
   
                var newChart = {
                    data1: newDataSalesmerch,
                    data2: data.disbaledMerchant
                }
                renderChartJS('chart-bars-new', newChart, ch_date, data.merchant_chart_base);

                $("#chart-bars-new-res").shieldChart({
                    theme: "light",
                    tooltipSettings: {
                    chartBound: true,
                    customPointText: function (point, chart) {
                        return shield.format(
                            '<span>{value}</span>',
                            {
                                value: point.y
                            }
                        );
                      }
                    },
                   
                    primaryHeader: {
                        text: " "
                    },
                    chartLegend: {
                        enabled: false
                    },
                    exportOptions: {
                        image: false,
                        print: false
                    },
                    axisX: {
                        categoricalValues:ch_date
                    },
                    axisY: {
                        title: {
                            text: ""
                        }
                    },
                    dataSeries: [{
                        seriesType: "bar",
                        collectionAlias: "Reseller",
                        data: newDataSalesreseller,
                    }]
                });



                var x1 = data.revenu_volume;
                var x2 = data.subscription;
                var x3 = data.basys_point;

                var y1 = data.revenu_month;

                
                var legendContainer = document.getElementById("legendContainer");

                var datasets = {
                    "Revenue": {
                        data: x1, label: "Revenue",color:'#33b86c',hoverable:true, shadowSize: 2,highlightColor: '#007bff',clickable: true
                     },
                    "Subscriptions": {
                        data: x2, label: "Subscriptions",color:'#007bff',hoverable:true,clickable: true
                    },
                    "Basis Points": {
                        data: x3, label: "Basis Points",color:'#7e57c2',hoverable:true,clickable: true
                    }
                };

                var i = 1;

                var choiceContainer = $("#choices");

                $.each(datasets, function(key, val) {
                    var checkM = 'checked';
                    var classOpacity = '';
                    
                    
                    choiceContainer.append("<div class='displayLegend " + classOpacity + " ' id='color" + i + "'><input class='checkboxNone' type='checkbox' name='" + key +
                        "' " + checkM + " data-id='" + i + "' id='id" + key + "'></input> <label id='color" + i + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + i + "'></label>" +
                        "<label class='legendLabel' for='id" + key + "'>"
                        + val.label + "</label> </div>");
                    ++i;
                    
                });

                

                var data = [];

                choiceContainer.find("input:checked").each(function () {
                    var key = $(this).attr("name");
                    if (key && datasets[key]) {
                        data.push(datasets[key]);
                    }
                });
                choiceContainer.find("input").click(plotAccordingToChoices);

                $('.checkboxNone').change(function() {
                    var id = $(this).attr("data-id");
                    
                    var checkGraph = choiceContainer.find("input:checked").length;
                    if(checkGraph != 0){
                      if ($(this).prop('checked') == true){
                          $("#color"+id).removeClass('opacityManage');
                      } else {
                          $("#color"+id).addClass('opacityManage');
                      }
                    }
                });

               


                function plotAccordingToChoices() {

                    var data = [];

                    choiceContainer.find("input:checked").each(function () {
                        var key = $(this).attr("name");
                        
                        if(key != 'Revenue'){
                            if (key && datasets[key]) {
                                data.push(datasets[key]);
                            }
                        }else{
                            if ($('#idRevenue').prop('checked') == true){
                                if (key && datasets[key]) {
                                    data.push(datasets[key]);
                                }
                            }
                        
                        }
                        
                        
                        
                    });

                    if (data.length > 0) {
                        $.plot("#placeholder", data, {
                            series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
                            legend: {
                                position: "nw",
                                margin: [0, -24],
                                noColumns: 0,
                                backgroundColor: "transparent",
                                labelBoxBorderColor: null,
                                labelFormatter: function (o, t) {
                                    return o + "&nbsp;&nbsp;";
                                },
                                width: 30,
                                height: 2,
                                container: legendContainer,
                                onItemClick: {
                                      toggleDataSeries: true
                                  },
                            },
                            grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
                            yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            xaxis: { ticks: y1,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            tooltip: !0,
                            tooltipOpts: { content: function(data, x, y, dataObject) {
                                          
                                            var XdataIndex = dataObject.dataIndex;
                                            var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
                                            return 'Value of '+XdataLabel+' is '+y;
                                        }, shifts: { x: -60, y: 25 }, defaultTheme: !1 },

                            
                            
                            yaxis: {
                                autoScale:"exact"
                            }
                        }); 
                    }
                }
                plotAccordingToChoices();
          }
      });    
                  
      

      $.ajax({
          type:"POST",
          url : "<?php echo base_url(); ?>AdminCron/admin_dashboard_processing_volumn_json",
          data:{'fetchData':true},
          dataType:'json',
          success: function (data)
          {
                

                var totalVolume  =data['chart']['total_volume'];

                $('#totalVolume').html(format2(totalVolume));
       
                sum = 0;


                var x2 = data.volume;
                var x3 = data.ccVolume;
                var x4 = data.ecVolume;


                var y2 = data.revenu_month;

                var legendVolumnContainer = document.getElementById("legendVolumnContainer");


                var datasetsVolumn = {
                    "Total": {
                        data: x2, label: "Total",color:'#33b86c',hoverable:true, shadowSize: 2,highlightColor: '#007bff',clickable: true
                    },
                    "Credit Card": {
                        data: x3, label: "Credit Card",color:'#007bff',hoverable:true,clickable: true
                    },
                    "eCheck": {
                        data: x4, label: "eCheck",color:'#7e57c2',hoverable:true,clickable: true
                    }
                };
                var i = 1;
                var j = 1;

                var choiceVolumnContainer = $("#volumnChoices");

                $.each(datasetsVolumn, function(key, val) {
                    var checkM = 'checked';
                    var classOpacity = '';
                    
                    
                    choiceVolumnContainer.append("<div class='displayLegend " + classOpacity + " ' id='vcolor" + i + "'><input class='checkboxNoneVolumn' type='checkbox' name='" + key +
                        "' " + checkM + " data-id='" + j + "' id='id" + key + "'></input> <label id='vcolor" + j + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + j + "'></label>" +
                        "<label class='legendLabel' for='id" + key + "'>"
                        + val.label + "</label> </div>");
                    ++j;
                    
                });

                var volumnData = [];

                choiceVolumnContainer.find("input:checked").each(function () {
                    var key = $(this).attr("name");
                    if (key && datasetsVolumn[key]) {
                        volumnData.push(datasetsVolumn[key]);
                    }
                });

                choiceVolumnContainer.find("input").click(plotAccordingToChoicesVolumn);

                

                
                function plotAccordingToChoicesVolumn() {

                    var data = [];

                    choiceVolumnContainer.find("input:checked").each(function () {
                        var key = $(this).attr("name");
                        
                        if(key != 'Revenue'){
                            if (key && datasetsVolumn[key]) {
                                data.push(datasetsVolumn[key]);
                            }
                        }else{
                            if ($('#idRevenue').prop('checked') == true){
                                if (key && datasetsVolumn[key]) {
                                    data.push(datasetsVolumn[key]);
                                }
                            }
                        
                        }
                        
                        
                        
                    });

                    if (data.length > 0) {
                        $.plot("#volumnPlaceholder", data, {
                            series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
                            legend: {
                                position: "nw",
                                margin: [0, -24],
                                noColumns: 0,
                                backgroundColor: "transparent",
                                labelBoxBorderColor: null,
                                labelFormatter: function (o, t) {
                                    return o + "&nbsp;&nbsp;";
                                },
                                width: 30,
                                height: 2,
                                container: legendContainer,
                                onItemClick: {
                                      toggleDataSeries: true
                                  },
                            },
                            grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
                            yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            xaxis: { ticks: y2,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            tooltip: !0,
                            tooltipOpts: { content: function(data, x, y, dataObject) {

                                            var XdataIndex = dataObject.dataIndex;
                                            var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
                                            return 'Value of '+XdataLabel+' is '+y;
                                        }, shifts: { x: -60, y: 25 }, defaultTheme: !1 },

                            
                            
                            yaxis: {
                                autoScale:"exact"
                            }
                        }); 
                    }
                }
                plotAccordingToChoicesVolumn();
          }
      });
});

$(function(){   
    var newcratd = $('#chart-pie1');
       CompCharts.init(pchart_color, newcratd, chartColor, showLegend);  
});
var CompCharts = function() {
    return {
        init: function(pchart_data,char_id, color, showLegend = true) {
       
       var colorPalette = color;
            
         char_id.shieldChart({
            events: {
                legendSeriesClick: function (e) {
                    // stop the series item click event, so that 
                    // user clicks do not toggle visibility of the series
                    e.preventDefault();
                }
            },
            theme: "bootstrap",
            seriesPalette: colorPalette,
            seriesSettings: {
                donut: {
                    enablePointSelection: false,
                    addToLegend: showLegend,
                    activeSettings: {
                        pointSelectedState: {
                            enabled: false
                        }
                    },
                    enablePointSelection: false,
                    dataPointText: "",
                    borderColor: '#ffffff',
                    borderWidth:2
                }
            },
            
            chartLegend: {
                enabled: true,
                align: "right",
                renderDirection: 'vertical',
                verticalAlign: "middle",
                legendItemSettings: {
                    bottomSpacing: 15
                }
            },
            exportOptions: {
                image: false,
                print: false
            },
            primaryHeader: {
                text: " "
            },
            tooltipSettings: {
                customHeaderText: function (point, chart) {
                    return point[0].collectionAlias.split(':')[0];
                },
                customPointText: function (point, chart) {
                    let sum = 0;
                    pchart_data.map(data => {
                        sum += data[1];
                    });
                    let percentage = 0;
                    if (point.y !== 0.0001) {
                        percentage = (point.y*100 / sum).toFixed(2);
                    }
                    return shield.format(
                        '<span>{value}</span>',
                        {
                            value: percentage+"%"
                        }
                    );
                }
            },
            axisY: {
                title: {
                    text: ""
                }
            },
            dataSeries: [{
                seriesType: "donut",
                collectionAlias: "Merchants",
                data: pchart_data
            }]
        });
             
            
         

        }
    };
}();

    function renderChartJS(chartID, chartData, yaxisData, baseNumber = 0) {
        baseNumber = parseInt(baseNumber);

        var ctx = document.getElementById(chartID).getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: yaxisData,
                datasets: [{
                    label: 'Onboarded',
                    data: chartData.data1,
                    backgroundColor: [
                        'rgba(54, 162, 235, 0.2)',
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                    ],
                    borderWidth: 1
                },{
                    label: 'Disabled',
                    data: chartData.data2,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                maintainAspectRatio: true,
                plugins: {
                    legend: {
                        position: 'bottom',
                    },
                },
                scales: {
                    y: {
                        beginAtZero: true
                    },
                },
                base: (baseNumber % 2 == 0) ? baseNumber : (baseNumber + 1)
            }
        });
    }

  </script>
  
  
   
<style>

#chart-pie1{
    height: 350px !important;
    margin-bottom: -35px;
}
#chart-pie1 .shield-container {
    margin-top: -40px;
}
#chart-pie1 text tspan {
    visibility: hidden;
}
#chart-pie1 g text tspan {
    visibility: visible;
}
#chart-pie1 g {
    opacity: 1.5;
    borderWidth:2px;
}
  #chart-classic g text tspan {
    visibility: visible;
}
#chart-classic g {
    opacity: 0.7;
  borderWidth:2px;
}

#chart-classic text tspan {
    visibility: hidden;
}



  #chart-bars-new-res g text tspan {
    visibility: visible;
}
#chart-bars-new-resg {
    opacity: 0.7;
  borderWidth:2px;
}

#chart-bars-new-res text tspan {
    visibility: hidden;
}

  #chart-bars-new g text tspan {
    visibility: visible;
}
#chart-bars-newg {
    opacity: 0.7;
  borderWidth:2px;
}

#chart-bars-new text tspan {
    visibility: hidden;
}
  #chart_new_com g text tspan {
    visibility: visible;
}
#chart_new_com g {
    opacity: 0.7;
  borderWidth:2px;
}

#chart_new_com text tspan {
    visibility: hidden;
}

.chart_new {
    height: 260px !important;
}
.widget.widg_border {
    border: 1px solid #e8e8e8 !important;
}




#chart2 g text tspan {
    visibility: visible;
}


#chart2 text tspan {
    visibility: hidden;
}


</style>
    
    