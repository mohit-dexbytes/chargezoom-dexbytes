<!-- Page content -->
<style>
.subs-btn{
	position: absolute;
    top: 10px;
    left: 90px;
    z-index: 1;
	background-color: #f1efef05 !important;
    border-color: #e0d7d7 !important;
    padding: 6px 12px !important;
}
#is_free_trial {
    width: 11%;
    position: relative;
    bottom: 0px;
}
   
#day_field_grp{
    display: none;
}

.showHideBtnGrp{
	position: relative;
    left: 188px;
}

</style>

<div id="page-content">
    <div class="msg_data ">     
		<?php 
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != ""){
						echo mymessage($message);
						}
						$error = $this->session->flashdata('error');
						if(isset($error) && $error != ""){
						echo getmessage($error);
						}
					?></div>
    <legend class="leg"> Plans</legend>
    <div class="block-main full" style="position: relative">
        <div class="showHideBtnGrp" >
            <a id="sh_cust" class="btn btn-primary1 subs-btn" href="<?php echo base_url(); ?>Admin_panel/admin_plans">Show/Hide Active</a>
		</div>
        <table id="plan_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Plan Name</th>
                    <th class="text-right hidden-sm">Subscription Price</th>
                    <th class="text-right hidden-xs">Subscribers</th>
                    <th class="text-right hidden-xs">Plan Type</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
                    if(isset($plans) && $plans){
                        foreach($plans as $plan){
                            ?>
                                <tr>
                                    <td class="text-left cust_view"><a href="#add_plans" onclick="set_edit_plan('<?php  echo $plan['plan_id']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Edit Plan" class=""  ><?php echo $plan['plan_name']; ?></a></td>
                                    <td class="text-right hidden-xs"><?php echo "$".number_format($plan['subscriptionRetail'],2); ?></td>
                                    <td class="text-right hidden-xs"><?php echo $plan['merchantCount']; ?></td>
                                    <td class="text-right hidden-xs">
                                        <?php if($plan['merchant_plan_type'] == 'SS'){
                                                echo 'ES';
                                            } else{
                                                echo $plan['merchant_plan_type'] ;
                                            } 
                                        ?>
                                    </td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs">
                                            <a href="#del_plan" onclick="set_del_plan('<?php  echo $plan['plan_id']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Enable Plan" class="btn btn-success"> <i class="fa fa-check"> </i> </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                        }
                    }  
                ?>
				
			</tbody>
        </table>
                            
    </div>
</div>
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<!------    Add popup form    ------->

<div id="add_plans" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
               <h2 class="modal-title" id="title">Plans</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="plan_form" class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_plan">
              	<input type="hidden" name="customer_type" id="customer_type" value="InActive">
				<input type="hidden" id="planID" name="planID" value=""  />
		        <div class="col-md-12">   
		        		<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Plan Type</label>
							<div class="col-md-8">
								<select id="merchantplantype" name="merchantplantype" class="form-control">
								   
								  <option value="">Select Plan Type </option> 
							 	     <option value="VT">Virtual Terminal</option>
								     <option value="AS">Automation Suite</option>
									 <option value="SS">Enterprise Suite</option>
									 <option value="Free">Free</option>
								</select>
								<?php echo form_error('merchant_plan_type'); ?>
							</div>
						</div>
                     	<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Plan Name</label>
							<div class="col-md-8">
								<input type="text" id="planName"  name="planName" class="form-control"  value="" data-placeholder="Plan Name"><?php echo form_error('planName'); ?></div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Subscription Price</label>
							<div class="col-md-8">
								<input type="text" id="subscriptionRetail"  name="subscriptionRetail" class="form-control"  value="" data-placeholder="Subscription Price"><?php echo form_error('planPrice'); ?></div>
						</div>
						  
						<div class="form-group bps_class">
							<label class="col-md-4 control-label" for="bps">Basis Points</label>
							<div class="col-md-8">
								<input type="text" id="bps"  name="bps" class="form-control"  value="0" ><?php //echo form_error('planPrice'); ?></div>
						</div>
						<div class="form-group" id="day_field_grp">
							<label class="col-md-4 control-label" for="example-username">Number Of Days</label>
							<div class="col-md-8">
								<input type="text" name="free_trial_day" id="free_trial_day" value="30" class="checkbox form-control"  >
							</div>
						</div>
					
					
                       </div>      
		      
						<div class="form-group">
							<div class="col-md-4 no-pad">
								<label class="col-md-12 control-label transaction_class " for="example-username" style="padding-right: 10px;">Free Trial</label>
							</div>
							
							<div class="col-md-8">
								<div class="col-md-6 checkboxPlanSection ">
									<input type="checkbox" id="is_free_trial" name="is_free_trial" value="0" class="transaction_class checkbox form-control"  >
								</div>
								<div class="col-md-6 align-right">
									<button type="submit" class="planBtn submit btn btn-sm btn-success">Save</button>
									
						            <button type="button" class="planBtn btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
								</div>
						    </div>
						</div>    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

</div>
<!-- END Page Content -->

<!--------------------del admin plan------------------------>

<div id="del_plan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Activate Plan</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_plan" method="post" action='<?php echo base_url(); ?>Admin_panel/active_plan' class="form-horizontal" >
                     
                 
					<p>Do you really want to activate this Plan?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="plan_id" name="plan_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Activate"  />
                    <button type="button" class="btn btn-sm btn-primary close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="Edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Plans in Bulk</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>Admin_panel/update_plans' class="form-horizontal" >
				 
                     
               <!--  <p> You may update plan in bulk here.</p> -->
				   
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Plan</label>
						 <div class="col-md-6">
						   <select id="plan_old" name="plan_old"  class="form-control">
								   <option value="" >Select Current Plan</option>
								    <?php foreach($plans as $plan){  ?>
								    <option value="<?php echo $plan['plan_id']; ?>" ><?php echo $plan['plan_name']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Plan</label>
						 <div class="col-md-6">
						   <select id="plan_new" name="plan_new"  class="form-control">
								   <option  value="" >Select New Plan</option>
								 <?php foreach($plans as $plan){  ?>
								    <option value="<?php echo $plan['plan_id']; ?>" ><?php echo $plan['plan_name']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel1" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"   class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					 <hr/>
					
					<table>
					   
				<tbody id="t_data">
						 
		       </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
	
    $("input[name$='check']").click(function() {
        var test = $(this).val();

       $("div.desc").hide();
        $("#cnt" + test).show();
		
	 });

});
</script>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(document).ready(function(){
    $('#plan_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'planName': {
                        required: true,
                        minlength: 3,
                         
                    },
					'subscriptionRetail': {
                        required: true,
						number:true,
						maxlength:6
						
                    },
                    'merchantplantype': {
                        required: true,
                        minlength:1
                    },
                    'service_Fee': {
                        required: true,
						number:true,
                    },
                    'free_trial_day': {
                        required: true,
                        min:1,
						number:true,
                    },
			},
    }); 
	
	
    Pagination_view.init();



	
	$.validator.addMethod("greaterThan",
    function(value, max, min){
        return parseFloat(value) > parseFloat($(min).val());
    }, "Price must be greater than cost"
);
	$.validator.addMethod("greaterThanplan",
    function(value, max, min){
        return parseFloat(value) > parseFloat($(min).val());
    }, "Price must be greater than cost"
);
	

 $('#plan_old').change(function(){
	 
	
    var gID =  $('#plan_old').val();

	$.ajax({
    url: '<?php echo base_url("Admin_panel/get_merchant_plan_id")?>',
    type: 'POST',
	data:{plan_id:gID},
    success: function(data){
		var data =JSON.parse(data)
		$('#t_data').html(data.data);
		
	
		
			
     }	
  });
	
});	

 	$('#merchantplantype').change(function(){
		var plans = $('#merchantplantype').val();
		if(plans == 'Free'){
			$( ".allowed_transactions" ).show();
			$( ".transaction_class" ).hide();
			$( "#subscriptionRetail" ).val('0.00');

			$( "#allowed_transactions" ).val(20);
			$('#bps').val('0');
			$( ".bps_class" ).hide();
			$("#is_free_trial").attr('checked',false);
		 	$("#free_trial_day").val(30);
		 	$( "#day_field_grp" ).hide();
		} else {
			$( "#allowed_transactions" ).val(0);
			$( ".transaction_class" ).show();
			$( ".bps_class" ).show();
			$( ".allowed_transactions" ).hide();
			if($( "#is_free_trial:checked" ).length == 1){
				$( "#is_free_trial" ).val(1);
				$( "#day_field_grp" ).show();
			}else{
				$( "#is_free_trial" ).val(0);
				$( "#day_field_grp" ).hide();
			}
		}
	});
});

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#plan_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_plan(id){
	
	  $('#plan_id').val(id);
	
}

function set_addplan(id){
    
    $('#plan_form').trigger('reset');
    $('#title').text("Add New Plan");
	$('#planName').val(id);
	$('#planDescription').val(id);
	$('#planCost').val(id);
	$('#planPrice').val(id);
	$('#revenueCost').val(id);
	$('#revenue').val(id);
	$('#serviceFee').val(id);

}	
$( "#is_free_trial" ).on( "click", function(){
	
	if($( "#is_free_trial:checked" ).length == 1){
		$( "#is_free_trial" ).val(1);
		$( "#day_field_grp" ).show();
	}else{
		$( "#is_free_trial" ).val(0);
		$( "#day_field_grp" ).hide();
	}
} );
function set_edit_plan(plan_id){
    $('#title').text("Edit Plan");
	$('.submit').text("Update");
    $.ajax({
    url: '<?php echo base_url("Admin_panel/get_plan_id")?>',
    type: 'POST',
	data:{plan_id:plan_id},
	dataType: 'json',
    success: function(data){
             $('#planID').val(data.plan_id);		
			 $('#planName').val(data.plan_name);
			 $('#merchantplantype').val(data.merchant_plan_type);
			 $('#subscriptionRetail').val(data.subscriptionRetail);
			 $('#service_Fee').val(data.serviceFee);
			 $('#bps').val(data.bps);
			 $('#flat').val(data.flat_service);
   			 $("input:radio[name$=check] :selected").val(data.planType);
   			 $("#is_free_trial").val(data.is_free_trial);
			$( "#allowed_transactions" ).val(data.allowed_transactions);
			
			if(data.merchant_plan_type == 'Free'){
				$( ".allowed_transactions" ).show();
				$( ".transaction_class" ).hide();
				$( ".bps_class" ).hide();
			} else {
				$( ".transaction_class" ).show();
				$( ".bps_class" ).show();
				$( ".allowed_transactions" ).hide();
			}
			
   			 if(data.is_free_trial == 1){

   			 	$("#is_free_trial").prop('checked',true);
   			 	$("#free_trial_day").val(data.free_trial_day);
   			 	$( "#day_field_grp" ).show();

   			 }else{
   			 	$("#is_free_trial").prop('checked',false);
   			 	$("#free_trial_day").val(30);
   			 	$( "#day_field_grp" ).hide();
   			 }
    
		    if(data.planType==1){
		     $('#revenue').val(data.serviceFee);
		       $("#cnt1").show();
		      $("#cnt2").hide();
		       $("#cnt3").hide();
		    }
		    if(data.planType==2){
		     $('#serviceFee').val(data.serviceFee);
		     $("#cnt1").hide();
		      $("#cnt2").show();
		       $("#cnt3").hide();
		    }
		     if(data.planType==3){
		     $('#flat').val(data.flat_service);
		     $("#cnt1").hide();
		      $("#cnt2").hide();
		       $("#cnt3").show();
		    }
     		$("input[name='check'][value='"+data.planType+"']").prop('checked', true);
     		
           $('#merchantplantype option[value="'+data.merchant_plan_type+'"]').attr('selected','selected');
 
        	
	
			 
	}	
});
	
}

    	
  

</script>
