<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php
        $message = $this->session->flashdata('message');
        if(isset($message) && $message != "")
            echo mymessage($message);
    ?></div>
    <legend class="leg">Admin Users</legend>
    <div class="block-main full" style="position: relative">
        <div class="addNewFixRight">
            <a class="btn btn-sm  btn-success" title="Create User"  href="<?php echo base_url(); ?>home/create_user">Add New</a>     
        </div>
        <table id="admin_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left"> Name </th>
                    <th class="hidden-xs text-left"> Email Address</th>
                    <th class="text-left hidden-xs "> Powers </th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
                    if(isset($user_data) && $user_data){
                        foreach($user_data as $user){
                            ?>
                                <tr>
                                    <td class="text-left cust_view">
                                        <a href="<?php echo base_url('home/create_user/'.$user['adminID']); ?>" data-toggle="tooltip" title="Edit User"><?php echo $user['adminFirstName'].' '.$user['adminLastName']; ?></a>
                                    </td>
                                    <td class="text-left hidden-xs"> <?php echo $user['adminEmail']; ?></td>
                                    <td class="text-left hidden-xs"><?php echo $user['authName']; ?> </td> 
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs">
                                            <a href="#del_user_company" onclick="del_adminuser_id('<?php  echo $user['adminID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete User" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                        }
                    }  
                ?>
				
			</tbody>
        </table>
                            
    </div>
</div>


<div id="del_user_company" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Admin User</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_user" method="post" action='<?php echo base_url(); ?>home/delete_user' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this User?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="adminID11" name="adminID11" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>
function del_adminuser_id(id){

	     $('#adminID11').val(id);
}
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#admin_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>	