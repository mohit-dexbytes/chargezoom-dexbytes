
 <!-- Page content -->
	<div id="page-content">
	        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- Wizard Header -->
     <ol class="breadcrumb">
	  <li class="breadcrumb-item"><strong><a href="<?php echo base_url().'Admin_panel/index' ?>" >Dashboard</a></strong></li>
	    <li class="breadcrumb-item"><small>Email Personalization</small></li>
	 </ol>

    <div class="block full">
        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                
                    <!-- Block Tabs Title -->
                    
                    
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    
                        
                         
                            
                                <form id="form-validation" action="<?php echo base_url();?>Settingmail/create_template" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Email Type</label>
                                        <div class="col-md-7">
                                            <select id="type" name="type" disabled class="form-control">
                                              
                                               <?php 
											      foreach($types as $type){
											   ?>
                                               	<option value="<?php echo $type['templateType']; ?>" <?PHP if(isset($templatedata)&& $templatedata['templateType']==$type['templateType'] ){ ?>   selected="selected" <?php } ?>><?php  echo  ucwords($type ['templateName']); ?></option>
					                          <?php  } ?>
												
                                                 
                                                    
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Template Name</label>
                                        <div class="col-md-7">
                                                <input type="text" id="templateName" name="templateName"  value="<?php  if(isset($templatedata)){ echo ($templatedata['templateName'])?$templatedata['templateName']:''; }else{ echo "Welcome Message"; } ?> "   class="form-control" placeholder="Enter the name">
                                        </div>
                                    </div>
                         
                                     
                                    
                                 
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)){ echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:'';}else{ echo $subject; }  ?>"  class="form-control" placeholder="Enter the Subject">
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)){ echo ($templatedata['message'])?$templatedata['message']:''; }else{ echo $email_temp; } ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="tempID" id="tempID" value="<?php if(isset($templatedata)) echo ($templatedata['rtempID'])?$templatedata['rtempID']:''; ?>" />
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"> Save </button>
                                      
                                      <a href="<?php echo base_url().'Settingmail/email_template'; ?>" class="btn btn-sm btn-default close1 newCloseButton" >Cancel</a>
                                    </div>
                                </div>  
                           </form>	
                         
                           <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="modal-header ">
                                    <h2 class="modal-title pull-left">Template Guide </h2>
                                </div>
                            </div>
                            
                            <div class="modal-body">
                                <div id="data_form_template">
                    			    <label class="label-control" id="template_name"></label>
                                    <div class="form-group ">
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-7"  >
                                            <label>
                                                <p>You may use following tags in your email templates</p>
                                            </label>
                                        </div>
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-9 col-md-offset-1">
                                            <div class="">
                                                <table class="table table-bordered table-striped ecom-orders table-vcenter">
                                                    <th>Tag</th>
                                                    <th>Description</th>
                                                    <tbody>
                                                          
                                                            <tr>                                              
                                                      <td> <?php  echo "{{invoice_balance}}"; ?></td>
                                                      <td>Invoice Balance</td>
                                                      </tr>
                                                        <tr>                                              
                                                      <td> <?php  echo "{{invoice_date_due}}"; ?></td>
                                                      <td>Invoice Due Date</td>
                                                      </tr>
                                                              <tr>                                              
                                                      <td> <?php  echo "{{invoice_num}}"; ?></td>
                                                      <td>Invoice Number</td>
                                                      </tr>
                                                           <tr>                                              
                                                      <td> <?php  echo "{{payment_transaction_id}}"; ?></td>
                                                      <td>Transaction ID</td>
                                                      </tr>
                                                      <tr>                                              
                                                        <td> <?php  echo "{{transaction.amount}}"; ?></td>
                                                        <td>Transaction Amount</td>
                                                      </tr>
                                                      <tr>                                              
                                                        <td> <?php  echo "{{surcharge.amount}}"; ?></td>
                                                        <td>Surcharge Amount</td>
                                                      </tr>
        
                                                      <tr>                                              
                                                        <td> <?php  echo "{{authorized.amount}}"; ?></td>
                                                        <td>Total Amount</td>
                                                      </tr>
                                                             <tr>  
        
        
                                                      <td> <?php  echo "{{invoice_last_payment_amount}}"; ?></td>
                                                      <td>Invoice Amount</td>
                                                      </tr>
                                                           <tr>                                              
                                                      <td> <?php  echo "{{invoice_amount_paid}}"; ?></td>
                                                      <td>Total Invoice Amount</td>
                                                      </tr>
                                                          <tr>                                              
                                                      <td> <?php  echo "{{invoice_amount_paid}}"; ?></td>
                                                      <td>Invoice Amount</td>
                                                      </tr>
                                                          <tr>                                              
                                                      <td> <?php  echo "{{invoice_date}}"; ?></td>
                                                      <td>Invoice Date</td>
                                                      </tr>
                                                          <tr>                                              
                                                      <td> <?php  echo "{{login_url}}"; ?></td>
                                                      <td>Login URL</td>
                                                      </tr>
                                                     <tr>                                              
                                                      <td> <?php  echo "{{merchant_name}}"; ?></td>
                                                      <td>Merchant Name</td>
                                                      </tr>
                                                      <tr>                                              
                                                      <td> <?php  echo "{{company_name}}"; ?></td>
                                                      <td>Company Name</td>
                                                      </tr>
                                                      
                                                      <tr>                                              
                                                      <td> <?php  echo "{{merchant_email}}"; ?></td>
                                                      <td>Merchant Email </td>
                                                      </tr>
                                                      <tr>                                              
                                                      <td> <?php  echo "{{merchant_password}}"; ?></td>
                                                      <td>Merchant Password </td>
                                                      </tr>
                                                      
                                                       <tr>                                              
                                                      <td> <?php  echo "{{reseller_name}}"; ?></td>
                                                      <td> Reseller Name</td>
                                                      </tr>
                                                      
                                                      <tr>                                              
                                                      <td> <?php  echo "{{reseller_email}}"; ?></td>
                                                      <td> Reseller Email</td>
                                                      </tr>
                                                      
                                                      <tr>                                              
                                                      <td> <?php  echo "{{reseller_phone}}"; ?></td>
                                                      <td> Reseller Phone</td>
                                                      </tr>
                                                     
                                                      <tr>                                              
                                                      <td> <?php  echo "{{link}}"; ?></td>
                                                      <td>Link URL</td>
                                                      </tr>
                                                      <?php if(isset($templatedata['userTemplate']) && $templatedata['userTemplate'] == 2){ ?>
                                                        <tr>                                              
                                                          <td> <?php  echo "{{plan_name}}"; ?></td>
                                                          <td>Friendly Name</td>
                                                        </tr>
                                                      <?php } ?>
                                                      </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
	  
             
                    <!-- END Tabs Content -->
               
                <!-- END Block Tabs -->
            </div>
    


    


	
        </div>
        <!-- END Working Tabs Content -->
		 </div>
		       
   <!-- Modal Header -->
			
           
   </div> 
        
   
        <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"></script>
        <script>
        
        function goBack() {
  window.history.back();
}
        
        $(function(){    nmiValidation.init();
		
					CKEDITOR.replace( 'textarea-ckeditor', {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
			});
			CKEDITOR.config.allowedContent = true;
		    $('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
					  $('#cc_sapn').hide();
			});
			  $('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
					   $('#bcc_sapn').hide();
			});
			 $('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
			});
			
			
			$('#type').change(function(){
	     var t_name=	$('#type option:selected').text();    
			
			$('#templateName').val(t_name);
			    
			});
			
		 });
         
         
         
         
         
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    templateName: {
                        required: true,
						minlength:3,
                    },
					
					
                   		
					 emailSubject: {
                        required: true,
                       
                    },
                    message: {
                         required: true,
                       
                    },
                  
                },
             
            });
			

			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
         
         
         
        
         
         </script>

   
    