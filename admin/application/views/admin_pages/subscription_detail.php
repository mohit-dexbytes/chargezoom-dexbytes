
<!-- Page content -->
<div id="page-content">
    
     
    <!-- All Orders Block -->
    <div class="block full">
        <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>
        <div class="block-title">
            <h2><strong> Merchant Subscriptions</strong></h2>
            <div class="block-options pull-right">
							
                               <a class="btn  btn-sm btn-success" title="Create New"  href="<?php echo base_url(); ?>Subscription/create_admin_subscription">Add New </a>
                               
				
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="subscription" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class="text-left">Merchant</th>
                    <th class="text-left ">Reseller</th>
					 <th class="hidden-xs text-left">Plan</th>
                      <th class="text-right hidden-xs">Next Charge Amount</th>
                      <th class="text-center ">Action</th>
					
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				
				if(isset($subscription_list) && $subscription_list)
				{
					foreach($subscription_list as $subs)
					{ 
					
				?>
				<tr>
					
					<td class="text-left "><a href="javascript:void(0);"><?php echo $subs['firstName']." ".$subs['lastName']; ?> </a> </td>
					
                    <td class="text-left "><?php echo $subs['resellerFirstName']; ?></td>
					
                    <td class="text-left hidden-xs"><?php echo $subs['plan_name']; ?>  </td> 
					
					<td class=" text-right hidden-xs">$<?php echo $subs['subscriptionRetail'].'+' ;  echo ($subs['serviceFee'])?$subs['serviceFee'] .'% total collections':'0' ;  ?></td>
					
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
						
						 <?php if($subs['merchantStatus']=='0'){  ?>
					   <a href="<?php echo   base_url('Admin_panel/create_merchant/'.$subs['merchantID']);?>" data-toggle="tooltip" title="Edit Merchant" class="btn btn-xs btn-default"><i class="fa fa-pencil"> </i> </a>
						
						<a href="<?php echo   base_url('Subscription/invoice_details/'.$subs['subscriptionID']);?>" data-toggle="tooltip" title="View Invoice History" class="btn btn-xs btn-default"><i class="fa fa-eye"> </i> </a>
						
				        <a href="#del_merchant_sub" onclick="set_merchantList_id('<?php echo $subs['subscriptionID']; ?>')"  title="Suspend Merchant" data-toggle="modal"  class="btn btn-info"><i class="fa fa-ban"></i></a>
							
						<a href="#del_subs" onclick="set_subs_id('<?php echo $subs['subscriptionID']; ?>');" data-toggle="modal"  title="Terminate Merchant" class="btn btn-xs btn-danger"> <i class="fa fa-times"> </i> </a>
							
					   <?php } if($subs['merchantStatus']=='1'){ ?>	
                          <a href="#active_merchant_sub" title="Activate Merchant" onclick="set_merchantList_active_id('<?php echo $subs['subscriptionID']; ?>')" data-toggle="modal"  class="btn btn-xm btn-info"><i class="fa fa-check"></i></a>
					   <?php } ?>	
						
					</div>
					</td>
				
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->


<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#subscription').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [1,3,4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
<!-- END Page Content -->