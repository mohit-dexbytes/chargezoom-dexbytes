 <!-- Page content -->
	<div id="page-content">
	    <div class="msg_data ">
            <?php echo $this->session->flashdata('message');   ?>
        </div>
    
        <legend class="leg">E-mail Templates</legend>

    
    <div class="block full emailtemplate-ul-li-div">
        <div class="block-title full">
            <ul class="nav nav-tabs">
                <li class="active" id="defaultActive"><a data-toggle="tab" href="#admin">Admin</a></li>
                <li><a data-toggle="tab" href="#reseller">Reseller Default</a></li>
                <li><a data-toggle="tab" href="#merchant">Merchant Default</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="admin" class="tab-pane active">
            
                <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr> 
                        <th class="text-left">Type</th>
                        <th class="hidden-xs text-left">Subject</th>
                        
                    </tr>
                </thead>
                <tbody>

    
                <?php 
    
                            if(!empty($templates)){   foreach($templates as $template){   ?>
                            <tr>
                                <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Settingmail/create_template/<?php echo $template['rtempID'];  ?>"><?php echo ucwords($template['templateName']);  ?></a></td>
                                <td class="hidden-xs"><?php echo $template['emailSubject'];  ?></td>
                                
                                
                        </tr>
                        
                        
                        <?php }	} ?>
                    </tbody>
            </table>
            </div>
            <!-- Reseller Tabs Content -->
            <div id="reseller" class="tab-pane">
                <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr> 
                        <th class="text-left">Type</th>
                        <th class="hidden-xs text-left">Subject</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    if(!empty($reseller_templates)){   foreach($reseller_templates as $template){   ?>
                    <tr>
                    <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Settingmail/create_template/<?php echo $template['rtempID'];  ?>"><?php echo ucwords($template['templateName']);  ?></a></td>
                    <td class="hidden-xs"><?php echo $template['emailSubject'];  ?></td>
                                
                                
                </tr>
                <?php } } ?> 
                </tbody>
            </table>
            </div>
            <!-- Merchant Tabs Content -->
            <div id="merchant" class="tab-pane">
            
                <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
                <thead>
                    <tr> 
                        <th class="text-left">Type</th>
                        <th class="hidden-xs text-left">Subject</th>
                        
                    </tr>
                </thead>
                <tbody>


                    <?php 

                    if(!empty($merchant_templates)){   foreach($merchant_templates as $template){   ?>
                    <tr>
                        <td class="text-left cust_view"><a href="<?php echo base_url(); ?>Settingmail/create_template_merchant/<?php echo $template['id'];  ?>"><?php echo ucwords($template['templateName']);  ?></a></td>
                        <td class="hidden-xs"><?php echo $template['emailSubject'];  ?></td>
                                
                    </tr>
                    
                    
                    <?php } } ?>
                        
                        
                
                </tbody>
                </table>
            </div>
            <div class="tab-pane" id="emailtemplateasign">Profile..</div>
            
        </div>
        
    </div>
      
     
<div id="set_email_tag" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Template Guide</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
              
		    	 <div class="form-group ">
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-7"  >
                                          <label> <p>You can use this following tags for your template</p>
                                           </label>
                                          </div> 
                                                  
                                             <table class="table table-bordered table-striped ecom-orders table-vcenter">
                                              <th>S.N.</th>
                                              <th>Tag</th>
                                              <th>Description</th>
                                              <tbody>
                                              <tr>
                                              <td>1</td>
                                              <td> <?php  echo "{{ creditcard.type_name }}"; ?></td>
                                              <td>Card Friendly Name</td>
                                              </tr>
                                              <tr>
                                              <td>2</td>
                                              <td><?php   echo "{{ invoice.url_permalink }}" ?></td>
                                              <td>  Link </td>
                                              </tr>
                                              <tr>
                                              <td>3</td>
                                              <td><?php echo  "{{ transaction.amount }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              <td>4</td>
                                              <td><?php  echo  "{{ transaction.transaction_method }}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              <td>5</td>
                                              <td><?php echo  "{{ transaction.transaction_date}}"; ?></td>
                                              <td> Transaction Date</td>
                                              </tr>
                                              <tr>
                                              <td>6</td>
                                              <td><?php echo "{{ transaction.transaction_detail }}"; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              <td>7</td>
                                              <td><?php echo "{'%'  config.email_show_poweredby '%'}"; ?> </td>
                                              <td>Company promotion message</td>
                                              </tr>
                                                  <td>8</td>
                                              <td><?php   echo "{{ invoice.refnumber }}" ?></td>
                                              <td> Invoice Number </td>
                                              </tr>
                                              <tr>
                                              <td>9</td>
                                              <td><?php echo  "{{ invoice.days_overdue }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              <td>10</td>
                                              <td><?php  echo  "{{invoice.balance}}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              <td>11</td>
                                              <td><?php echo  "{{ transaction.currency_symbol }}"; ?></td>
                                              <td>Currency</td>
                                              </tr>
                                              <tr>
                                              <td>12</td>
                                              <td><?php echo '{{ invoice.due_date|date("F j, Y") }}'; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              <td>13</td>
                                              <td><?php echo "{{ creditcard.url_updatelink }}"; ?> </td>
                                              <td>Credit Card Update Link</td>
                                              </tr>
                                              </tbody>
                                             </table>       
                                            
							    
                                        <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Close</button>
                                        
                                  		
                                         
                                          
                                        </div>
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>   
   
   
<div id="teplate_view" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header text-center">
                <h2 class="modal-title">View Template</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
             <div id="data_form_template11"  style="height: 300px; min-height:300px;  overflow: auto; " >
			    <label class="label-control" id="template_name"></label>
		    	
			</div>
			    <hr>
				<div class="pull-right">
        		
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Close</button>
                    </div>
                    <br />
                    <br />  				
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="teplate_del" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Template</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="" method="post" action='<?php echo base_url(); ?>Settingmail/delete_template' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Template?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="tempateDelID" name="tempateDelID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<script>
    
 function delete_template_details(tempateViewID){
	
	 $('#tempateDelID').val(tempateViewID);
  } 
        
 function view_template_details(tempateViewID){
	 
	
	 if(tempateViewID!=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>Settingmail/view_template',
		  data : {'tempateViewID':tempateViewID },
		  success: function(data){
			        
			     $('#data_form_template11').html(data);
			  
			  
		  }
	   });	   
		 
	
	 
	 } 
 } 
         
</script>
   
   
   </div>