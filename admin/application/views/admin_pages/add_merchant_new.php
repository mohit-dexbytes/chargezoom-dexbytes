<!-- Page content -->
<style>
    .error {
        color: red;
    }
    .pay_value_label {
        padding: 4% 4%;
        background: #f2f2f2;
        width: 90%;
        float: left;
        border-radius: 4px;
        height: 38px;
        font-size: 14px;
    }
    span#viewEditMode {
        cursor: pointer;
        position: relative;
        top: 10px;
        left: 4%;
    }
    .hide_payment_field{
        display: none ;
    }
    .show_payment_field{
        display: block;
    }
    .bottom_right_mechant_btn {
        right: 2% !important;
    }
    }
    .form-bordered .form-group {
        margin: 0;
        border: none;
        padding: 10px;
    }
    #portal_url-error{
        position: absolute;
        top: 32px;
        left: 0px;
    }
    fieldset legend {
    
        padding: 13px 0 10px;
        
    }
    .block-border-none{
        border: none;
    }
    .btn_can {
        background-color: #f5f5f5 !important;
        color: #333;
        border-color: #e8e8e8;
    }
    .green{
        color: green;
    }
    .red{
        color: red;
    }
</style>
<?php
if($_SERVER['HTTP_HOST'] == 'web.payportalstaging.com'){
    $domain = 'payportalstaging.com';
}elseif($_SERVER['HTTP_HOST'] == 'web.payportaltest.com'){
    $domain = 'payportaltest.com';
}elseif($_SERVER['HTTP_HOST'] == 'demo.payportal.com'){
    $domain = 'payportal.com';
}elseif($_SERVER['HTTP_HOST'] == 'heartlandsync.com'){
    $domain = 'heartlandsync.com';
}else{
    $domain = 'payportal.com';
}
?>
<div id="page-content">
    	
    <div class="row">
        <div class="col-md-12">
            <legend class="leg"><strong>Merchant Info</strong></legend>    

            <form method="POST" id="merchant_form" class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_merchant" enctype="multipart/form-data">
                <input type="hidden" name="free_trial_day" id="free_trial_day" value="0">
                <input type="hidden" id="merchID" name="merchID" value="<?php if (isset($merchant)) {
                                echo $merchant['merchID'];
                            } ?>" />
                <input type="hidden" name="merchantCardID" id="merchantCardID" value="<?php echo (isset($merchant) && $merchant['cardID'] > 0)?$merchant['cardID']:0; ?>">

                <input type="hidden" name="is_card_edit" id="is_card_edit" value="0">

                <div class="block full">
                    <fieldset>
                                                        
                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Company Name <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="companyName" name="companyName" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['companyName'];
                                    } ?>" data-placeholder="Company Name"><?php echo form_error('companyName'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">First Name <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="firstName" name="firstName" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['firstName'];
                                    } ?>" data-placeholder="First Name"><?php echo form_error('firstName'); ?>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Last Name <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="lastName" name="lastName" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['lastName'];
                                    } ?>" data-placeholder="Last Name"><?php echo form_error('lastName'); ?>
                                </div>
                            </div>
                            <?php if (isset($merchant)) { ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Password </label>
                                <div class="col-md-8">
                                    <input type="password" id="merchantPassword1" name="merchantPassword1" class="form-control" value="" data-placeholder="Password">
                                </div>
                            </div>

                            <?php } ?>

                            <?php if (isset($merchant)) { ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Email Address <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantEmail" name="merchantEmail" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['merchantEmail'];
                                    } ?>" data-placeholder="Email Address"><?php echo form_error('merchantEmail'); ?>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if (!isset($merchant)) { ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Email Address<span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantEmail" name="merchantEmail" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['merchantEmail'];
                                    } ?>" data-placeholder="Email Address"><?php echo form_error('merchantEmail'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Password <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="password"  autocomplete="off" id="merchantPassword" name="merchantPassword" class="form-control" value="<?php if (isset($merchant)) {
                                        echo $merchant['merchantPassword'];
                                    } ?>" data-placeholder="Password"> <?php echo form_error('merchantPassword'); ?>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Confirm Password <span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <input type="password"  autocomplete="off" id="confirmPassword" name="confirmPassword" class="form-control" value="" data-placeholder="Password Again"> </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Phone Number </label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantContact" name="merchantContact" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantContact'];
                                    } ?>" data-placeholder="Phone Number"><?php echo form_error('merchantContact'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Alternate Number </label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantAlternateContact" name="merchantAlternateContact" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantAlternateContact'];
                                        } ?>" data-placeholder="Alternate Number"><?php echo form_error('merchantAlternateContact'); ?>
                                    </div>
                            </div>
                        </div>



                        <div class="col-md-6">

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Address Line 1 </label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantAddress1'];
                                    } ?>" data-placeholder="Address Line 1 "><?php echo form_error('merchantAddress1'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Address Line 2</label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantAddress2'];
                                    } ?>" data-placeholder="Address Line 2 "><?php echo form_error('merchantAddress2'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">City</label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantCity" name="merchantCity" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantCity'];
                                    } ?>" data-placeholder="City"><?php echo form_error('merchantCity'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">State</label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantState" name="merchantState" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantState'];
                                    } ?>" data-placeholder="State"><?php echo form_error('merchantState'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">ZIP Code</label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantZipCode'];
                                    } ?>" data-placeholder="ZIP Code"><?php echo form_error('merchantZipCode'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Country</label>
                                <div class="col-md-8">
                                    <input type="text" id="merchantCountry" name="merchantCountry" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantCountry'];
                                    } ?>" data-placeholder="Country"><?php echo form_error('merchantCountry'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username">Website </label>
                                <div class="col-md-8">
                                    <input type="text" id="weburl" name="weburl" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['weburl'];
                                    } ?>" data-placeholder="Website"><?php echo form_error('website'); ?>
                                </div>
                            </div>

                        </div>
                    <fieldset>
                </div>
                <!-- Customer info section  -->
                <legend class="leg"><strong>Customer Portal</strong></legend>
                <div class="block">
                    <fieldset>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="portal_url">Portal URL</label>
                                <div class="col-md-8">
                                    <div class="input-group ">
                                        <input type="text" id="portal_url" name="portal_url" value="<?php if(isset($merchant) && isset($merchant['portalprefix'])){ echo $merchant['portalprefix']; } ?>" class="form-control" data-placeholder="">
                                        <span class="input-group-btn"><a class="btn btn-primary">.<?php echo $domain ;?></a> </span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="tagline">Customer Help Text</label>
                                <div class="col-md-8">
                                    <input type="text" id="tagline" name="tagline"  value="<?php if(isset($merchant) && isset($merchant['merchantTagline'])){ echo $merchant['merchantTagline']; } ?>" class="form-control" data-placeholder="Customer Help Text">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="example-file-input">Upload Logo</label>
                                <div class="col-md-8">
                                    <span id="fileselector">
                                        <label class="btn btn-default" for="upload-file-selector">
                                            <input id="upload-file-selector"  name="picture" type="file">
                                            <i class="fa_icon icon-upload-alt margin-correction"></i>
                                        </label>
                                    </span>
                                    <br>
                                    <span class="text-primary"><strong>Recommended Logo Size is <?php echo LOGOWIDTH; ?> x <?php echo LOGOHEIGHT; ?></strong></span>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- Gateway section -->
                <legend class="leg"><strong>Gateway</strong></legend>
                <div class="block">
                    <fieldset>
                        <?php if(!isset($merchant)){    
                            $option='';
                            foreach($all_gateway as $gat_data){ 
                                $option.='<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
                            }
                        ?>
                        <div class="col-md-6">

                            <div class="form-group ">
                                <label class="col-md-4 control-label" for="card_list">Gateway Type<span class="text-danger">*</span></label>
                                <div class="col-md-8">
                                    <select id="gateway_opt" name="gateway_opt" onchange="set_gateway_data(this);"  class="form-control">
                                        <option value="" >Select Gateway</option><?php echo $option; ?>
                                    </select>
                                </div>
                            </div>

                            <div id="pay_data" >        
                                <div id="fr_div" class="form-group " style="display:none">
                                    <label class="col-md-4 control-label" for="card_list">Friendly Name<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <input type="text" id="frname" name="frname"  class="form-control " data-placeholder="Friendly Name" />
                                    </div>
                                </div>
                                        
                                <div class="gateway-div" id="nmi_div" style="display:none">         
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">Username <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <input type="text" id="nmiUser" name="nmiUser" class="form-control"  data-placeholder="Username">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Password<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  data-placeholder="Password">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="gateway-div" id="fluid_div" style="display:none">         
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">API KEY <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <input type="text" id="fluidUser" name="fluidUser" class="form-control"  data-placeholder="API KEY">
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="fluid_cr_status" name="fluid_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="fluid_ach_status" name="fluid_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>
                                <div  class="gateway-div" id="basys_div" style="display:none">         
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">API KEY <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <input type="text" id="basysUser" name="basysUser" class="form-control"  data-placeholder="API KEY">
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="basys_cr_status" name="basys_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="basys_ach_status" name="basys_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="gateway-div" id="payarc_div" style="display:none">         
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">Secret KEY <span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <textarea id="payarcUser" name="payarcUser" class="form-control"  placeholder="Secret KEY"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div  class="gateway-div"  id="cz_div" style="display:none">          
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">Username</label>
                                        <div class="col-md-8">
                                                <input type="text" id="czUser" name="czUser" class="form-control"  data-placeholder="Username">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Password</label>
                                        <div class="col-md-8">
                                            <input type="text" id="czPassword" name="czPassword" class="form-control"  data-placeholder="Password">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>

                                <div  class="gateway-div"  id="iTransact_div" style="display:none">			
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="iTransactUsername">API Username</label>
                                        <div class="col-md-6">
                                            <input type="text" id="iTransactUsername" name="iTransactUsername" class="form-control"  data-placeholder="API Username">
                                        </div>
                                </div>
                                    
                                <div class="form-group">
                                        <label class="col-md-4 control-label" for="iTransactAPIKIEY">API Key</label>
                                    <div class="col-md-6">
                                            <input type="text" id="iTransactAPIKIEY" name="iTransactAPIKIEY" class="form-control"  data-placeholder="API Key">
                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="iTransact_cr_status">Credit Card</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="iTransact_cr_status" name="iTransact_cr_status" checked="checked" value='1' >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="iTransact_ach_status">Electronic Check</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="iTransact_ach_status" name="iTransact_ach_status" value='1'>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="add_surcharge_box">Surcharge</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="add_surcharge_box" name="add_surcharge_box" value='1' onchange="surchageCheckChange(this)">
                                        </div>
                                    </div>
                                    <div class="form-group" id='surchargePercentageBox' style="display: none">
                                        <label class="col-md-4 control-label" for="surchargePercentage">Surcharge Rate</label>
                                        <div class="col-md-6">
                                            <input type="text" id="surchargePercentage" name="surchargePercentage" class="form-control"  data-placeholder="Surcharge Rate" value='0'>
                                        </div>
                                    </div>
                                </div>
                                        
                                <div  class="gateway-div" id="auth_div" style="display:none">                    
                                
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">API LoginID<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <input type="text" id="apiloginID" name="apiloginID" class="form-control" data-placeholder="API LoginID">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Transaction Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  data-placeholder="Transaction Key">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>  
                                    
                                    
                                <div  class="gateway-div" id="pay_div" style="display:none" >        
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">PayTrace Username<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" data-placeholder="PayTrace  Username">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">PayTrace Password<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  data-placeholder="PayTrace  password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="paytrace_cr_status">Credit Card</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="paytrace_cr_status" name="paytrace_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="paytrace_ach_status">Electronic Check</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" id="paytrace_ach_status" name="paytrace_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>  
                                        
                                <div  class="gateway-div" id="paypal_div" style="display:none" >     
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">API Username<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="paypalUser" name="paypalUser" class="form-control" data-placeholder="Enter  Username">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">API Password<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  data-placeholder="Enter  password">
                                        </div>
                                    </div>
                                    
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Signature<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  data-placeholder="Enter  Signature">
                                        </div>
                                    </div>
                                </div>  
                                    
                                <div  class="gateway-div" id="stripe_div" style="display:none" >     
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">Publishable Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="stripeUser" name="stripeUser" class="form-control" data-placeholder="Publishable Key">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Secret API Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  data-placeholder="Secret API Key">
                                        </div>
                                    </div>
                                </div>  
                                        
                                <div  class="gateway-div" id="usaepay_div" style="display:none" >        
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                <input type="text" id="transtionKey" name="transtionKey" class="form-control"; data-placeholder="USAePay Transaction Key">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">USAePay PIN<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  data-placeholder="USAePay PIN">
                                        </div>
                                    </div>
                                </div>
                                
                                <div  class="gateway-div" id="heartland_div" style="display:none" >      
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">Public Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; data-placeholder="Public Key">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Secret Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  data-placeholder="Secret Key">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="heart_cr_status">Credit Card</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" value='1' id="heart_cr_status" name="heart_cr_status" checked="checked" >
                                        
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="heart_ach_status">Electronic Check</label>
                                        <div class="col-md-6">
                                            <input type="checkbox" value='1' id="heart_ach_status" name="heart_ach_status" >
                                        </div>
                                    </div>
                                </div>
                                            
                                <div  class="gateway-div" id="cyber_div" style="display:none" >  
                                        <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  data-placeholder="Enter  MerchantID">
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="customerID">APIKeyID<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" data-placeholder="Enter API Key ID">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Secret Key<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="secretKey" name="secretKey" class="form-control"  data-placeholder="Enter  Secret Key">
                                        </div>
                                    </div>
                                </div>
                    <!-- New CardPointe Gateway Form -->
                            <div class="gateway-div" id="cardpointe_div" style="display:none">	
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="customerID">Site Variable</label>
                                    <div class="col-md-6">
                                        <input type="text" id="cardpointeSiteName" name="cardpointeSiteName" class="form-control"  placeholder="fts (default) if you any custom variable name please insert here" value="fts">
                                        <i class="fa fa fa-info-circle gatewayInformationIcon" aria-hidden="true" data-toggle="tooltip" title="fts (default) if you any custom variable name please insert here"></i>
                                    </div>
                                </div>  		
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="customerID">Username</label>
                                    <div class="col-md-6">
                                        <input type="text" id="cardpointeUser" name="cardpointeUser" class="form-control"  placeholder="Username">
                                    </div>
                            </div>
                                
                            <div class="form-group">
                                    <label class="col-md-4 control-label" for="card_number">Password</label>
                                <div class="col-md-6">
                                        <input type="text" id="cardpointePassword" name="cardpointePassword" class="form-control"  placeholder="Password">
                                    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="cardpointeMerchID">Gateway Merchant ID</label>
                                    <div class="col-md-6">
                                        <input type="text" id="cardpointeMerchID" name="cardpointeMerchID" class="form-control"  placeholder="Enter Gateway Merchant ID">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="cardpointe_cr_status">Credit Card</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" id="cardpointe_cr_status" name="cardpointe_cr_status" checked="checked" >
                                    
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="cardpointe_ach_status">Electronic Check</label>
                                    <div class="col-md-6">
                                        <input type="checkbox" id="cardpointe_ach_status" name="cardpointe_ach_status"  >
                                    
                                    </div>
                                </div>
                            </div>	
				    <!-- New Cardpointe Gateway form ends here -->
                                <div  class="gateway-div" id="TSYS_div" style="display:none">          
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="TSYSUser">User ID</label>
                                        <div class="col-md-8">
                                                <input type="text" id="TSYSUser" name="TSYSUser" class="form-control"  data-placeholder="User ID">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="TSYSPassword">Password</label>
                                        <div class="col-md-8">
                                            <input type="text" id="TSYSPassword" name="TSYSPassword" class="form-control"  data-placeholder="Password">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="TSYSMerchantID">TSYS Merchant ID</label>
                                        <div class="col-md-8">
                                            <input type="text" id="TSYSMerchantID" name="TSYSMerchantID" class="form-control"  data-placeholder="TSYS Merchant ID">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="TSYS_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="TSYS_cr_status" name="TSYS_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="TSYS_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="TSYS_ach_status" name="TSYS_ach_status"  >

                                        </div>
                                    </div>
                                </div>
                                <?php include(APPPATH.'/views/gateways/maverick_add.php'); ?>
                                <div class="gateway-div" id="EPX_div" style="display:none">          
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPXCustNBR">CUST NBR</label>
                                        <div class="col-md-8">
                                                <input type="text" id="EPXCustNBR" name="EPXCustNBR" class="form-control"  >
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPXMerchNBR">MERCH NBR</label>
                                        <div class="col-md-8">
                                            <input type="text" id="EPXMerchNBR" name="EPXMerchNBR" class="form-control" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPXDBANBR">DBA NBR</label>
                                        <div class="col-md-8">
                                            <input type="text" id="EPXDBANBR" name="EPXDBANBR" class="form-control" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPXterminal">TERMINAL NBR</label>
                                        <div class="col-md-8">
                                            <input type="text" id="EPXterminal" name="EPXterminal" class="form-control" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPX_cr_status">Credit Card</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="EPX_cr_status" name="EPX_cr_status" checked="checked" >
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="EPX_ach_status">Electronic Check</label>
                                        <div class="col-md-8">
                                            <input type="checkbox" id="EPX_ach_status" name="EPX_ach_status"  >
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="block block-border-none">
                            <div class="col-md-12">
                                <div class="block-options pull-right" >
                                    <a href="#set_def_gateway" class="btn btn-sm btn-info" style="margin-top:6px;" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Set Default Gateway</a>
                                    <a href="#add_gateway" class="btn btn-sm btn-success" style="margin-top:6px;" onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Add New</a>
                                </div>

                                <div class="row">
                                    <table id="merch_page" class="table table-bordered table-striped table-vcenter">
                                        <thead>
                                            <tr>
                                                <th class=" text-left">Gateway </th>
                                                
                                                <th class=" text-left">Gateway Name </th>
                                                <th class="hidden-xs text-left">User Name</th>
                                                <th class="hidden-xs text-left">Default</th>
                                                <th class="text-center"> Action </th> 
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $defaultGatewayId = 0;
                                                if(isset($get_data) && $get_data)
                                                {
                                                    foreach($get_data as $data)
                                                    {
                                                        if($data['set_as_default'] == 1){
                                                            $defaultGatewayId = $data['gatewayID'];
                                                        }
                                                        ?>
                                                            <tr>
                                                                <td class="text-left "><?php echo getGatewayNames($data['gatewayType']); ?></td>
                    
                                                                <td class="text-left "><?php echo $data['gatewayFriendlyName']; ?>  </td>
                                                                
                                                                <td class="text-left hidden-xs"><?php echo  $data['gatewayUsername']; ?>  </td>
                                                                <td class="text-left hidden-xs">
                                                                    <?php 
                                                                    if($data['set_as_default'] == 1){
                                                                        echo '<label class="green">Yes</label>';
                                                                    }else{
                                                                        echo '<label class="red">No</label>';
                                                                    }
                                                                    ?>  
                                                                </td>

                                                                <td class="text-center">
                                                                    <div class="btn-group btn-group-xs">
                                                                        <a href="#edit_gateway" class="btn btn-default" onclick="set_edit_gateway('<?php echo $data['gatewayID'];  ?>');" title="Edit" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>  
                                                                        <a href="#del_gateway" onclick="del_gateway_id('<?php echo $data['gatewayID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn  btn_can"> <i class="fa fa-times"> </i> </a>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        <?php
                                                    }
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                    <tbody>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    </fieldset>
                </div>
                <!-- Subscription section -->
                <legend class="leg"><strong>Subscription</strong></legend>
                <div class="block  payment_info">
                    <fieldset>
                        <div class="col-md-6">
                            <div class="col-md-12 no-pad">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="example-typeahead">Reseller<span class="text-danger">*</span></label>
                                    <div class="col-md-8">
                                        <select id="reseller" class="form-control " name="reseller">
                                            <option value="">Select Reseller..</option>
                                            <?php foreach ($reseller_list as $reseller) {  ?>
                                                <option value="<?php echo $reseller['resellerID']; ?>" <?php if (isset($merchant)) {
                                                    if ($merchant['resellerID'] == $reseller['resellerID']) { ?> selected="selected" <?php }  ?> <?php } ?>> <?php echo $reseller['resellerCompanyName']; ?> 
                                                </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="example-typeahead">Plan<span class="text-danger">*</span></label>
                                    <div class="col-md-8"><?php //print_r($plans);die(); ?>
                                        <select id="Plans" class="form-control " name="Plans" onchange="check_plan_data(this);">
                                            <option value="">Select Plans</option>
                                            <?php if (isset($plans)) {
                                            foreach ($plans as $plan) {  ?>
                                            <option value="<?php echo $plan['plan_id']; ?>" <?php if ($plan['plan_id'] == $merchant['plan_id']) echo "selected"; ?>> <?php echo $plan['plan_name']; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div id="free_trial_section" >
                                    
                                </div>
                            </div>
                            <div class="col-md-12 no-pad">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="card_number">Payment Option</label>
                                    <div class="col-md-8">
                                        <select name="payOption" id='payOption' class="form-control" <?php echo isset($merchant_card_data->merchantCardID)?'disabled=true':'';  ?>  >
                                            
                                            <option value="">Select Method</option>
                                            <option value="1" <?php echo (isset($merchant) && $merchant['payOption'] == 1)?'selected':''; ?>>Credit Card</option>
                                            <option value="2" <?php echo (isset($merchant) && $merchant['payOption'] == 2)?'selected':''; ?>>eCheck</option>
                                        </select>
                                            
                                    </div>
                                </div>  
                                        
                            </div>

                            <?php 

                            if(isset($merchant) && $merchant['payOption'] > 0)  {
                                $class_manage = 'hide_payment_field';
                                $bill_display = 'style="display: block;"';
                                ?>

                                <div id="exist_payment_method" class="exist_payment_method col-md-12 no-pad" >
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">
                                            <?php 
                                            if($merchant['payOption'] == 1){
                                                echo 'Credit Card';
                                            }else{
                                                echo 'Checking Account';
                                            }
                                            ?> 
                                        </label>
                                        <div class="col-md-8">
                                            <div class="pay_value_label"> 
                                                <?php

                                                if(isset($merchant_card_data->merchantFriendlyName)){ 

                                                    echo $merchant_card_data->merchantFriendlyName; 

                                                }
                                                    ?>                                   
                                            </div> 
                                            <span id="viewEditMode" class="edit_pay_option">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </span> 
                                        </div>
                                    </div>  
                                </div>
                            <?php }else{
                                $class_manage = 'show_payment_field';
                                $bill_display = '';
                            }
                            ?>
                            
                            <!-- Email div -->
                            <div id="email_div" class="<?php echo $class_manage; ?>" <?php if(isset($merchant) && $merchant['payOption'] != '0'){ ?> style="display:none;" <?php }else{ ?> style="display:block;" <?php } ?>  >
                                
                            </div>
                            <!--   End -->
                            <!-- echeck Field  -->
                            <div id="chk_div" class="<?php echo $class_manage; ?>">
                                <div class="col-md-12 no-pad">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Account_Number">Account Number<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="accountNumber" name="accountNumber" class="form-control"  value="" data-placeholder="Account Number"><?php echo form_error('accountNumber'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Routing_Number">Routing Number<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="routNumber" name="routNumber" class="form-control"  value="" data-placeholder="Routing Number"><?php echo form_error('routingNumber'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account_name">Account Name<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text" id="accountName" data-stripe="accountName" name="accountName" value="" class="form-control" data-placeholder="Account Name">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Account Type">Account Type</label>
                                        <div class="col-md-8">
                                            
                                            <select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false">
                                                
                                                <option value="checking" <?php if(isset($merchant_card_data->accountHolderType) && $merchant_card_data->accountHolderType=='checking'){ echo ""; } ?> >Checking</option>
                                                <option value="savings" <?php if(isset($merchant_card_data->accountHolderType) && ($merchant_card_data->accountHolderType =='savings' || $merchant_card_data->accountHolderType =='saving') ){ echo ""; } ?> >Saving</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label>
                                        <div class="col-md-8">
                                            
                                            <select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false">
                                                
                                                <option value="business" <?php if(isset($merchant_card_data->accountType ) && $merchant_card_data->accountType =='bussiness'){ echo ""; } ?> >Business</option>
                                                <option value="personal" <?php if(isset($merchant_card_data->accountType) && $merchant_card_data->accountType =='personal'){ echo ""; } ?> >Personal</option>
                                                
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- End echeck field -->
                            <!-- Card field  -->
                            <div id="crd_div" class="<?php echo $class_manage; ?>"  >
                                <div class="col-md-12 no-pad">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Card Number<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <input type="text"  autocomplete="cc-number" id="cardNumber" data-stripe="cardNumber" maxlength="17" name="cardNumber" value="" class="form-control" data-placeholder="Card Number">
                                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="expry">Expiry Month<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                                        
                                            <select id="expiry" name="expiry" class="form-control">
                                                <?php
                                                        
                                                            
                                                foreach($monthData as $data){  $sel = '';    if(isset($merchant_card_data->cardMonth) && $merchant_card_data->cardMonth == intval($data['monthValue'])) $sel=''; ?> 
                                                                    
                                                    <option value="<?php echo $data['monthValue'];  ?>" <?php echo $sel ; ?> >  <?php echo $data['monthName']; ?></option>
                                                                
                                                            <?php  }  ?>
                                            </select>          
                                                    
                                        </div>
                                    </div> 
                                    <div class="form-group">            
                                        <label class="col-md-4 control-label" for="expiry_year">Expiry Year<span class="text-danger">*</span></label>
                                        <div class="col-md-8">
                                            <select id="expiry_year" name="expiry_year" class="form-control">
                                                <?php 
                                                $cruy = date('y');
                                                $dyear = $cruy+25;
                                                for($i =$cruy; $i< $dyear ;$i++ ){
                                                    
                                                    $sel = '';    if(isset($merchant_card_data->cardYear) && $merchant_card_data->cardYear == '20'.$i ) $sel=''; 
                                                ?>
                                                    <option value="<?php echo '20'.$i; ?>" <?php echo $sel; ?> > <?php echo "20".$i;  ?> </option>
                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                                    
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="cvv" >Security Code (CVV)</label>
                                        <div class="col-md-8">
                                            
                                            <input type="text" id="cvv" name="cvv" class="form-control" value=""  data-placeholder="Security Code (CVV)" autocomplete="off" />
                                                
                                            
                                        </div>
                                    </div>   
                                </div>

                            </div>
                            <!-- End card field  -->
                        </div>

                        <!--  Right billing field section -->
                        <div class="col-md-6 ">
                            
                            <div id="bill_div" <?php echo $bill_display; ?> class="<?php echo $class_manage; ?>"  >
                                <div class="col-md-12 no-pad">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing First Name </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingfirstName" name="billingfirstName" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_first_name; } ?>" data-placeholder="Billing First Name"><?php echo form_error('firstName'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Last Name </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billinglastName" name="billinglastName" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_last_name; } ?>" data-placeholder="Billing last Name"><?php echo form_error('firstName'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Phone Number</label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingContact" name="billingContact" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_phone_number; } ?>" data-placeholder="Billing Phone Number"><?php echo form_error('billingContact'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Email Address </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingEmailAddress" name="billingEmailAddress" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_email; } ?>" data-placeholder="Billing Email Address"><?php echo form_error('Billing_Email_Address'); ?>
                                        </div>
                                    </div>  
                                <!-- </div>
                                <div class="col-md-6"> -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Address </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingAddress1" name="billingAddress1" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_Addr1; } ?>" data-placeholder="Billing Address1"><?php echo form_error('Billing_Address1'); ?>
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing City</label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingCity" name="billingCity" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_City; } ?>" data-placeholder="Billing City"><?php echo form_error('billingContact'); ?>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing State </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingState" name="billingState" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_State; } ?>" data-placeholder="Billing State"><?php echo form_error('Billing_State'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Zipcode </label>
                                        <div class="col-md-8">
                                            <input type="text" id="billingPostalCode" name="billingPostalCode" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_Zipcode; } ?>" data-placeholder="Billing Postal Code"><?php echo form_error('Billing_Address2'); ?>
                                        </div>
                                    </div>
                                </div>
                                    
                            </div>
                            
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                <label class="col-md-4 control-label" for="example-username"></label>
                                <div class="col-md-8">

                                <?php if (!isset($merchant)) { ?>
                                        <button type="submit" class="submit btn btn-sm btn-success">Save</button>
                                    <?php } else { ?>
                                        <button type="submit" class="submit btn btn-sm btn-success">Update</button>
                                    <?php } ?>
                                    <a href="<?php echo base_url('Admin_panel/merchant_list'); ?>" class="btn btn-sm btn-default close1 newCloseButton">Cancel</a>
                                    
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    
                </div>
            </form>
        </div>
    </div>

 <!--======================================== Modal for Delete Gateway ================================================------>

<div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway11" method="post" action='<?php echo base_url(); ?>home/delete_merchant_gateway' class="form-horizontal" >
                     
                 
                    <p> Do you really want to delete this Gateway? </p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantgatewayid" name="merchantgatewayid" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
                 <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn_can" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!--=====================================   Modal for Edit Gateway  =================================================--->

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway</h2>
            </div>
                        
            <div class="modal-body">    
                <form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/update_merchant_gateway">
                    <input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />
                    <input type="hidden" id="editmerchID" name="editmerchID" value="<?php echo $this->uri->segment(3); ?>"  />
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
                        <div class="col-md-6">
                            <input type="text" id="fname"  name="fname" class="form-control"  value="" data-placeholder="">
                        </div>
                    </div>
        
                    <div class="form-group ">
                        <label class="col-md-4 control-label" for="card_list">Gateway Type<span class="text-danger">*</span></label>
                            <div class="col-md-6">
                            <input type="text" name="gateway" id="gateway" readonly class="form-control" />
                        </div>
                    </div>
                            
                
                    <div class="gateway-div1" id="nmi_div1" style="display:none">            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">Username</label>
                            <div class="col-md-6">
                                <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; data-placeholder="Username..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Password</label>
                            <div class="col-md-6">
                                <input type="text" id="nmiPassword1" name="nmiPassword1" class="form-control"  data-placeholder="password..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="nmi_cr_status1">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="nmi_cr_status1" name="nmi_cr_status1" checked="checked" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1"  >
                            </div>
                        </div>
                    </div>  
                    <div class="gateway-div1" id="fluid_div1" style="display:none">            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="fluidUser1">API KEY</label>
                            <div class="col-md-6">
                                <input type="text" id="fluidUser1" name="fluidUser1" class="form-control"; data-placeholder="API KEY">
                            </div>
                        </div>
                            
                        
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="fluid_cr_status1">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="fluid_cr_status1" name="fluid_cr_status1"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="fluid_ach_status1">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="fluid_ach_status1" name="fluid_ach_status1"  >
                            </div>
                        </div>
                    </div>
                    <div class="gateway-div1" id="basys_div1" style="display:none">            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="basysUser1">API KEY</label>
                            <div class="col-md-6">
                                <input type="text" id="basysUser1" name="basysUser1" class="form-control"; data-placeholder="API KEY">
                            </div>
                        </div>
                            
                        
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="basys_cr_status1">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="basys_cr_status1" name="basys_cr_status1"  >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="basys_ach_status1">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="basys_ach_status1" name="basys_ach_status1"  >
                            </div>
                        </div>
                    </div>
                    <div class="gateway-div" id="payarc_div1" style="display:none">         
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="payarcUser1">Secret KEY <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                                    <textarea id="payarcUser1" name="payarcUser1" class="form-control"  placeholder="Secret KEY"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="gateway-div1" id="cz_div1" style="display:none">         
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">Username</label>
                            <div class="col-md-6">
                                <input type="text" id="czUser1" name="czUser1" class="form-control"; data-placeholder="Username..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Password</label>
                            <div class="col-md-6">
                                <input type="text" id="czPassword1" name="czPassword1" class="form-control"  data-placeholder="password..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cz_cr_status1">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="cz_cr_status1" name="cz_cr_status1" checked="checked" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1"  >
                            </div>
                        </div>
                    </div>

                    <div class="gateway-div1" id="iTransact_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactUsername1">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="iTransactUsername1" name="iTransactUsername1" class="form-control"  data-placeholder="API Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactAPIKIEY1">API Key</label>
                       <div class="col-md-6">
                            <input type="text" id="iTransactAPIKIEY1" name="iTransactAPIKIEY1" class="form-control"  data-placeholder="API Key">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_cr_status1" name="iTransact_cr_status1" checked="checked" value='1' >
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_ach_status1" name="iTransact_ach_status1" value='1'>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="add_surcharge_box1">Surcharge</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="add_surcharge_box1" name="add_surcharge_box1" value='1' onchange="surchageCheckChange(this, 1)">
                        </div>
                    </div>
					<div class="form-group" id='surchargePercentageBox1' style="display: none">
                        <label class="col-md-4 control-label" for="surchargePercentage1">Surcharge Rate</label>
                        <div class="col-md-6">
							<input type="text" id="surchargePercentage1" name="surchargePercentage1" class="form-control"  data-placeholder="Surcharge Rate">
                        </div>
                    </div>
				</div>
                <?php include(APPPATH."/views/gateways/maverick_edit.php"); ?>
                    <div class="gateway-div1" id="auth_div1" style="display:none">                   
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                            <div class="col-md-6">
                                <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" data-placeholder="API LoginID..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                            <div class="col-md-6">
                                <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  data-placeholder="Transaction Key..">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="auth_cr_status1" name="auth_cr_status1" checked="checked" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1"   >
                            </div>
                        </div>
                    </div>  
                        
                    <div class="gateway-div1" id="pay_div1" style="display:none" >       
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                            <div class="col-md-6">
                                <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; data-placeholder="PayTrace  Username..">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                            <div class="col-md-6">
                                <input type="text" id="paytracePassword1" name="paytracePassword1" class="form-control"  data-placeholder="PayTrace  password..">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="paytrace_cr_status1">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" value='1' id="paytrace_cr_status1" name="paytrace_cr_status1" checked="checked" >
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="paytrace_ach_status1">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" value='1' id="paytrace_ach_status1" name="paytrace_ach_status1" >
                            </div>
                        </div>
                    </div>  
                        
                    <div class="gateway-div1" id="paypal_div1" style="display:none" >        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">API Username</label>
                            <div class="col-md-6">
                                <input type="text" id="paypalUser1" name="paypalUser1" class="form-control" data-placeholder="Enter  Username">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">API Password</label>
                            <div class="col-md-6">
                                <input type="text" id="paypalPassword1" name="paypalPassword1" class="form-control"  data-placeholder="Enter  password">
                            </div>
                        </div>
                            
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Signature</label>
                            <div class="col-md-6">
                                <input type="text" id="paypalSignature1" name="paypalSignature1" class="form-control"  data-placeholder="Enter  Signature">
                                
                            </div>
                        </div>
                            
                        </div>
                        
                        <div  class="gateway-div1" id="cyber_div1" style="display:none" > 
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                                <div class="col-md-6">
                                    <input type="text" id="cyberMerchantID1" name="cyberMerchantID1" class="form-control"  data-placeholder="Enter  MerchantID">
                                
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                                <div class="col-md-6">
                                    <input type="text" id="apiSerialNumber1" name="apiSerialNumber1" class="form-control" data-placeholder="Enter API Key ID">
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="secretKey1" name="secretKey1" class="form-control"  data-placeholder="Enter  Secret Key">
                                
                                </div>
                            </div>
                            
                            
                            
                        </div>
                        
                        <div class="gateway-div1" id="stripe_div1" style="display:none" >        
                            
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="stripeUser1" name="stripeUser1" class="form-control" data-placeholder="Publishable Key..">
                                </div>
                                
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="stripePassword1" name="stripePassword1" class="form-control"  data-placeholder="Secret API Key..">
                                
                                </div>
                            </div>
                            
                        </div>
                        
                        <div class="gateway-div1" id="usaepay_div1" style="display:none" >       
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="transtionKey1" name="transtionKey1" class="form-control"; data-placeholder="USAePay Transaction Key">
                                </div>
                        </div>
                        
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                                <div class="col-md-6">
                                    <input type="text" id="transtionPin1" name="transtionPin1" class="form-control"  data-placeholder="USAePay PIN">
                                
                                </div>
                            </div>
                        </div>  
                        
                        <div class="gateway-div1" id="heartland_div1" style="display:none" >     
                        
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">Public Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="heartpublickey1" name="heartpublickey1" class="form-control"; data-placeholder="Public Key">
                                </div>
                            </div>
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                                <div class="col-md-6">
                                    <input type="text" id="heartsecretkey1" name="heartsecretkey1" class="form-control"  data-placeholder="Secret Key">
                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="heart_cr_status1">Credit Card</label>
                                <div class="col-md-6">
                                    <input type="checkbox" value='1' id="heart_cr_status1" name="heart_cr_status1" checked="checked" >
                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="heart_ach_status1">Electronic Check</label>
                                <div class="col-md-6">
                                    <input type="checkbox" value='1' id="heart_ach_status1" name="heart_ach_status1" >
                                </div>
                            </div>
                        </div>  
                        <div class="gateway-div1" id="TSYS_div1" style="display:none">          
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="TSYSUser1">User ID</label>
                                <div class="col-md-8">
                                        <input type="text" id="TSYSUser1" name="TSYSUser1" class="form-control"  data-placeholder="User ID">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="TSYSPassword1">Password</label>
                                <div class="col-md-8">
                                    <input type="text" id="TSYSPassword1" name="TSYSPassword1" class="form-control"  data-placeholder="Password">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="TSYSMerchantID1">TSYS Merchant ID</label>
                                <div class="col-md-8">
                                    <input type="text" id="TSYSMerchantID1" name="TSYSMerchantID1" class="form-control"  data-placeholder="TSYS Merchant ID">
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="TSYS_cr_status1">Credit Card</label>
                                <div class="col-md-8">
                                    <input type="checkbox" id="TSYS_cr_status1" name="TSYS_cr_status1" checked="checked" >
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="TSYS_ach_status1">Electronic Check</label>
                                <div class="col-md-8">
                                    <input type="checkbox" id="TSYS_ach_status1" name="TSYS_ach_status1"  >
                                    
                                </div>
                            </div>
                        </div>
                        <?php include(APPPATH."/views/gateways/maverick_edit.php"); ?>
                        <div class="gateway-div1"  id="EPX_div1" style="display:none">          
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPXCustNBR1">CUST NBR</label>
                                <div class="col-md-8">
                                        <input type="text" id="EPXCustNBR1" name="EPXCustNBR1" class="form-control"  >
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPXMerchNBR1">MERCH NBR</label>
                                <div class="col-md-8">
                                    <input type="text" id="EPXMerchNBR1" name="EPXMerchNBR1" class="form-control" >
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPXDBANBR1">DBA NBR</label>
                                <div class="col-md-8">
                                    <input type="text" id="EPXDBANBR1" name="EPXDBANBR1" class="form-control" >
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPXterminal1">TERMINAL NBR</label>
                                <div class="col-md-8">
                                    <input type="text" id="EPXterminal1" name="EPXterminal1" class="form-control" >
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPX_cr_status1">Credit Card</label>
                                <div class="col-md-8">
                                    <input type="checkbox" id="EPX_cr_status1" name="EPX_cr_status1" checked="checked" >
                                    
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="EPX_ach_status1">Electronic Check</label>
                                <div class="col-md-8">
                                    <input type="checkbox" id="EPX_ach_status1" name="EPX_ach_status1"  >
                                    
                                </div>
                            </div>
                        </div>
                    <!-- Edit CardPointe Gateway Form -->
                        <div class="gateway-div1" id="cardpointe_div1" style="display:none">
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">Site Variable</label>
                                <div class="col-md-6">
                                    <input type="text" id="cardpointeSiteName1" name="cardpointeSiteName1" class="form-control"  placeholder="fts (default) if you any custom variable name please insert here" value="fts">
                                    <i class="fa fa fa-info-circle gatewayInformationIcon" aria-hidden="true" data-toggle="tooltip" title="fts (default) if you any custom variable name please insert here"></i>
                                </div>
                            </div>			
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="customerID">Username</label>
                                <div class="col-md-6">
                                    <input type="text" id="cardpointeUser1" name="cardpointeUser1" class="form-control"  placeholder="Username">
                                </div>
                            </div>
                            
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="card_number">Password</label>
                            <div class="col-md-6">
                                    <input type="text" id="cardpointePassword1" name="cardpointePassword1" class="form-control"  placeholder="Password">
                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cardpointeMerchID">Gateway Merchant ID</label>
                                <div class="col-md-6">
                                    <input type="text" id="cardpointeMerchID1" name="cardpointeMerchID1" class="form-control"  placeholder="Enter Gateway Merchant ID">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cardpointe_cr_status1">Credit Card</label>
                                <div class="col-md-6">
                                    <input type="checkbox" id="cardpointe_cr_status1" name="cardpointe_cr_status1" checked="checked" >
                                
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="cardpointe_ach_status1">Electronic Check</label>
                                <div class="col-md-6">
                                    <input type="checkbox" id="cardpointe_ach_status1" name="cardpointe_ach_status1"  >
                                
                                </div>
                            </div>
                        </div>	
				    <!-- New Cardpointe Gateway form ends here -->
                        <div class="form-group">
                                <label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
                            <div class="col-md-6">
                                <input type="text" id="mid"  name="mid" class="form-control"  value="" data-placeholder="MerchantID (optional)"> 
                            </div>
                        </div>
                                    
                        <div class="form-group">
                            <div class="col-md-4 pull-right">
                            
                            <button type="submit" class="submit btn btn-sm btn-success"> Save </button>
                            
                            <button  type="button" align="right" class="btn btn-sm  btn_can close1" data-dismiss="modal"> Cancel </button>
                        
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!--------- END ---------------->
    </div> 
</div>

<!--------------==============================  Add New Gateway Start Here   ================================-------->

<div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/create_merchant_gateway">
               <input type="hidden"  id="merchID" name="merchID" value="<?php if(isset($merchant)){echo $merchant['merchID']; } ?>" />
             <div class="form-group">
                                              
                 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
                        <div class="col-md-6">
                           <input type="text" id="frname" name="frname"  class="form-control " />
                                
                        </div>
                        
                    </div>

                    <div class="form-group ">
                                              
                        <label class="col-md-4 control-label" for="card_list">Gateway Type</label>
                         <div class="col-md-6">
                           <select id="gateway_opt" name="gateway_opt" onchange="set_gateway_data(this);"  class="form-control">
                               
                                   <option value="" >Select Gateway</option>
                                   <?php foreach($all_gateway as $gat_data){ 
                                   echo '<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
                                   } 
                                   ?>
                                  
                            </select>
                            </div>
                    </div>
                    
                <div class="gateway-div" id="nmi_div" style="display:none">         
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  data-placeholder="Username">
                        </div>
                   </div>
                    
                   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  data-placeholder="Password">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                        </div>
                    </div>
                </div>
                <div class="gateway-div" id="fluid_div" style="display:none">         
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API KEY</label>
                        <div class="col-md-6">
                             <input type="text" id="fluidUser" name="fluidUser" class="form-control"  data-placeholder="API KEY">
                        </div>
                   </div>
                    
                   
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="fluid_cr_status" name="fluid_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="fluid_ach_status" name="fluid_ach_status"  >
                        </div>
                    </div>
                </div>
                <div class="gateway-div" id="basys_div" style="display:none">         
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="basysUser">API KEY</label>
                        <div class="col-md-6">
                             <input type="text" id="basysUser" name="basysUser" class="form-control"  data-placeholder="API KEY">
                        </div>
                   </div>
                    
                   
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="basys_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="basys_cr_status" name="basys_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="basys_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="basys_ach_status" name="basys_ach_status"  >
                        </div>
                    </div>
                </div>
                <div class="gateway-div" id="payarc_div" style="display:none">         
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Secret KEY <span class="text-danger">*</span></label>
                        <div class="col-md-8">
                                <textarea id="payarcUser" name="payarcUser" class="form-control"  placeholder="Secret KEY"></textarea>
                        </div>
                    </div>
                </div>
                <div class="gateway-div" id="cz_div" style="display:none">          
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  data-placeholder="Username">
                        </div>
                   </div>
                    
                   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  data-placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                           
                        </div>
                    </div>
                </div>
                
                <div class="gateway-div" id="iTransact_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactUsername">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="iTransactUsername" name="iTransactUsername" class="form-control"  data-placeholder="API Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransactAPIKIEY">API Key</label>
                       <div class="col-md-6">
                            <input type="text" id="iTransactAPIKIEY" name="iTransactAPIKIEY" class="form-control"  data-placeholder="API Key">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_cr_status" name="iTransact_cr_status" checked="checked" value='1' >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="iTransact_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="iTransact_ach_status" name="iTransact_ach_status" value='1'>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="add_surcharge_box">Surcharge</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="add_surcharge_box" name="add_surcharge_box" value='1' onchange="surchageCheckChange(this)">
                        </div>
                    </div>
					<div class="form-group" id='surchargePercentageBox' style="display: none">
                        <label class="col-md-4 control-label" for="surchargePercentage">Surcharge Rate</label>
                        <div class="col-md-6">
							<input type="text" id="surchargePercentage" name="surchargePercentage" class="form-control"  data-placeholder="Surcharge Rate" value='0'>
                        </div>
                    </div>
				</div>

                <div class="gateway-div" id="auth_div" style="display:none">                    
                    
                   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" data-placeholder="API LoginID">
                        </div>
                    </div>
                    
                 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  data-placeholder="Transaction Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
             </div> 
            
            
                <div class="gateway-div" id="pay_div" style="display:none" >        
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" data-placeholder="PayTrace  Username">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  data-placeholder="PayTrace  password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" value='1' id="paytrace_cr_status" name="paytrace_cr_status" checked="checked" >
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" value='1' id="paytrace_ach_status" name="paytrace_ach_status" >
                        </div>
                    </div>
                    
                </div>  
                
                <div class="gateway-div" id="paypal_div" style="display:none" >     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" data-placeholder="Enter  Username">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  data-placeholder="Enter  password">
                           
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  data-placeholder="Enter  Signature">
                           
                        </div>
                    </div>
                    
                </div>  
            
                <div class="gateway-div" id="stripe_div" style="display:none" >     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" data-placeholder="Publishable Key">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  data-placeholder="Secret API Key">
                           
                        </div>
                    </div>
                    
                </div>  
                
                 <div class="gateway-div" id="usaepay_div" style="display:none" >       
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; data-placeholder="USAePay Transaction Key">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  data-placeholder="USAePay PIN">
                           
                        </div>
                    </div>
                </div>
                
                 <div class="gateway-div" id="heartland_div" style="display:none" >     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; data-placeholder="Public Key">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  data-placeholder="Secret Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" value='1' id="heart_cr_status" name="heart_cr_status" checked="checked" >
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="heart_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" value='1' id="heart_ach_status" name="heart_ach_status" >
                        </div>
                    </div>
                </div>
                
                
                <div class="gateway-div" id="cyber_div" style="display:none" >  
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  data-placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
                
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" data-placeholder="Enter API Key ID">
                        </div>
                        
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  data-placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
                    
                     
                    
                </div>  
                
                <div class="gateway-div" id="TSYS_div" style="display:none">          
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="TSYSUser">User ID</label>
                        <div class="col-md-8">
                                <input type="text" id="TSYSUser" name="TSYSUser" class="form-control"  data-placeholder="User ID">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="TSYSPassword">Password</label>
                        <div class="col-md-8">
                            <input type="text" id="TSYSPassword" name="TSYSPassword" class="form-control"  data-placeholder="Password">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="TSYSMerchantID">TSYS Merchant ID</label>
                        <div class="col-md-8">
                            <input type="text" id="TSYSMerchantID" name="TSYSMerchantID" class="form-control"  data-placeholder="TSYS Merchant ID">
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="TSYS_cr_status">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="TSYS_cr_status" name="TSYS_cr_status" checked="checked" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="TSYS_ach_status">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="TSYS_ach_status" name="TSYS_ach_status"  >
                            
                        </div>
                    </div>
                </div>
                <?php include(APPPATH."/views/gateways/maverick_add.php"); ?>

            <!-- New CardPointe Gateway Form -->
                <div class="gateway-div" id="cardpointe_div" style="display:none">
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">Site Variable</label>
                            <div class="col-md-6">
                                <input type="text" id="cardpointeSiteName" name="cardpointeSiteName" class="form-control"  placeholder="fts (default) if you any custom variable name please insert here" value="fts">
                                <i class="fa fa fa-info-circle gatewayInformationIcon" aria-hidden="true" data-toggle="tooltip" title="fts (default) if you any custom variable name please insert here"></i>
                            </div>
                        </div>  			
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="customerID">Username</label>
                            <div class="col-md-6">
                                <input type="text" id="cardpointeUser" name="cardpointeUser" class="form-control"  placeholder="Username">
                            </div>
                        </div>
                        
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                                <input type="text" id="cardpointePassword" name="cardpointePassword" class="form-control"  placeholder="Password">
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cardpointeMerchID">Gateway Merchant ID</label>
                            <div class="col-md-6">
                                <input type="text" id="cardpointeMerchID" name="cardpointeMerchID" class="form-control"  placeholder="Enter Gateway Merchant ID">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cardpointe_cr_status">Credit Card</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="cardpointe_cr_status" name="cardpointe_cr_status" checked="checked" >
                            
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="cardpointe_ach_status">Electronic Check</label>
                            <div class="col-md-6">
                                <input type="checkbox" id="cardpointe_ach_status" name="cardpointe_ach_status"  >
                            
                            </div>
                        </div>
                </div>	
                <div class="gateway-div" id="EPX_div" style="display:none">          
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXCustNBR">CUST NBR</label>
                        <div class="col-md-8">
                                <input type="text" id="EPXCustNBR" name="EPXCustNBR" class="form-control"  >
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXMerchNBR">MERCH NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXMerchNBR" name="EPXMerchNBR" class="form-control" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXDBANBR">DBA NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXDBANBR" name="EPXDBANBR" class="form-control" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPXterminal">TERMINAL NBR</label>
                        <div class="col-md-8">
                            <input type="text" id="EPXterminal" name="EPXterminal" class="form-control" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPX_cr_status">Credit Card</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="EPX_cr_status" name="EPX_cr_status" checked="checked" >
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="EPX_ach_status">Electronic Check</label>
                        <div class="col-md-8">
                            <input type="checkbox" id="EPX_ach_status" name="EPX_ach_status"  >
                            
                        </div>
                    </div>
                </div>
				    <!-- New Cardpointe Gateway form ends here -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  data-placeholder="Merchant ID (optional)">
                           
                        </div>
                        
                   </div>

                  
             <div class="form-group">
                    <div class="col-md-4 pull-right">
                    <button type="submit" class="submit btn btn-sm btn-success">Add</button>
                    
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
                    
                    </div>
             </div> 
            
       </form>      
                
            </div>
            
            <!-- END Modal Body -->
        </div>
     </div>
     
</div>

<!--======================================== Set Default Gateway =================================---->

<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>home/set_merchant_gateway_default' class="form-horizontal card_form" >
                    <input type="hidden"  id="defmerchID" name="defmerchID" value="<?php if(isset($merchant)){echo $merchant['merchID']; } ?>" />
                 
                    
                    <div class="form-group ">
                      
                        <label class="col-md-4 control-label">Gateway</label>
                        <div class="col-md-6">
                            <select id="gatewayid" name="gatewayid" class="form-control">
                                <option value="" >Select Gateway</option>
                                <?php if(isset($get_data) && !empty($get_data) ){
                                        foreach($get_data as $data){
                                        ?>
                                   <option value="<?php echo $data['gatewayID'] ?>"  <?php if($data['set_as_default']=='1'){ echo 'selected'; } ?>   ><?php echo $data['gatewayFriendlyName'] ?></option>
                                        <?php } } ?>
                            </select>
                            
                        </div>
                    </div>      
                                            
                    
                 
                    <div class="pull-right">
                        <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">  
                         
                        <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-info" value="Save"  />
                        <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- END Page Content -->


    <script>
    	$(document).ready(function() {
            var opt = $('#payOption').val();
            if(opt == 1){
               
            }else if(opt == 2){
                
            }else{
                $('#chk_div').hide();
                $('#crd_div').hide();
                $('#bill_div').hide();
            }
         
            $('#viewEditMode').click(function(){
                $('#is_card_edit').val(1);
                $('#exist_payment_method').hide();
                $('#payOption').attr('disabled',false);
                var opt = $('#payOption').val();
                
                if(opt == 1){
               
                    $('#chk_div').hide();
                    $('#crd_div').show();
                    $('#bill_div').show();
                }else if(opt == 2){
                    
                    $('#chk_div').show();
                    $('#crd_div').hide();
                    $('#bill_div').show();
                }else{
                    
                    $('#chk_div').hide();
                    $('#crd_div').hide();
                    $('#bill_div').hide();
                }

            });
            $('#payOption').change(function(){
                var opt = $(this).val();
                $('#is_card_edit').val(1);
                $('#exist_payment_method').hide();
                if(opt == "2")
                {
                    $('#chk_div').css('display','block');
                    $('#crd_div').css('display','none');
                    $('#bill_div').css('display','block');
                    $('#email_div').css('display','none');
                   
                }
                else if(opt=="1")
                {
                    $('#chk_div').css('display','none');
                    $('#bill_div').css('display','block');
                    $('#crd_div').css('display','block'); 
                    $('#email_div').css('display','none');
                }else{
                    $('#chk_div').css('display','none');
                    $('#crd_div').css('display','none');  
                    $('#bill_div').css('display','none');
                    $('#email_div').css('display','block');
               }
            }); 

    		$('#reseller').change(function() {
    			var reID = $(this).val();
    			$("#Plans > option").remove();
    			$.ajax({
    				type: "POST",
    				url: "<?php echo base_url('Admin_panel/get_reseller_plan_id'); ?>",
    				data: {
    					reID: reID
    				},
    				dataType: 'json',
    				success: function(data) {

    					var s = $('#Plans');
    					// data=$.parseJSON(res);
                        $("<option />", {
                                value: '',
                                text: 'Select Plan'
                        }).appendTo(s);
    					for (var val in data) {

    						$("<option />", {
    							value: data[val]['plan_id'],
    							text: data[val]['plan_name']
    						}).appendTo(s);
    					}

    				}
    			});
    		});
            
            $('#billingContact').mask('999-999-9999'); 
    		$('#merchant_form').validate({ // initialize plugin
    			ignore: ":not(:visible)",
    			rules: {
                    'companyName':{
                        required: true,
                        minlength: 3,
                    },
    				'firstName': {
    					required: true,
    					minlength: 2,

    				},
                    'iTransactUsername':{
                        required: true,
                    },
                    'iTransactAPIKIEY':{
                        required: true,
                    },	
                    'EPXCustNBR':{
                        required: true,
                    },
                    'EPXMerchNBR':{
                        required: true,
                    },  
                    'EPXterminal':{
                        required: true,
                    },  
                    'EPXDBANBR':{
                        required: true,
                    },  
                    'surchargePercentage':{
                        surcharge_valid_rate: 'add_surcharge_box',
                    }, 
    				'lastName': {
    					required: true,
    					minlength: 2,
    				},

    				'merchantEmail': {
    					required: true,
    					isemail: true,
                        remote: {

                            beforeSend: function () {
                                $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

                            },
                            complete: function () {
                                $(".overlay1").remove();
                            },
                            url: "<?php echo base_url(); ?>ajaxRequest/check_new_email",
                            type: "POST",
                            data: {
                                merchantID: function() {
                                    return $( "#merchID" ).val();
                                }
                            },
                            dataType: 'json',
                            dataFilter: function (response) {

                                var rsdata = jQuery.parseJSON(response);

                                if (rsdata.status == 'success')
                                    return true;
                                else {
                                   
                                    return false;
                                }
                            }
                        },
    				},
                    'merchantContact':{
                        number:true,
                    },
                    'merchantAlternateContact':{
                        number:true,
                    },
    				'merchantPassword': {
    					required: true,
                        minlength:8,
                        maxlength:24,
                        check_atleast_one_number: true,
                        check_atleast_one_special_char: true,
                        check_atleast_one_lower: true,
                        check_atleast_one_upper:true,
                        check_name:true,
                        company_name:true,
                        check_email:true,
    				},

    				'confirmPassword': {
    					required: true,
                        minlength:8,
                        maxlength:24,
    					equalTo: '#merchantPassword',
    				},
    				
    				'Plans': {
    					required: true,
    				},
    				'reseller': {
    					required: true,
    				},
    				'gateway_list': {
    					required: true,
    				},
                    'payOption': {
                        
                    },
                    'cardNumber': {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
          
                    'expiry_year': {
                        CCExp: {
                          month: '#expiry',
                          year: '#expiry_year'
                        }
                    },
                    'cvv': {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    'accountNumber':{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                    'routNumber':{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 12,
                    },
                    'accountName':{
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                    },
                    'billingPostalCode':{
                    
                        minlength:3,
                        maxlength:10,
                        number:true,

                    },
                    'billingContact':{
                    
                        minlength: 10,
                        maxlength: 17,
                     
                    },
                  
                    'billingEmailAddress':{
                        isemail:true
                    },
                    'nmiUser': {
                        required: true,
                        minlength: 3
                    },
                    'nmiPassword':{
                        required : true,
                        minlength: 5,
                    },
                    'maverickAccessToken': {
                        required: true
                    },
                    'fluidUser': {
                        required: true,
                        minlength: 3
                    },
                    'basysUser': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSUser': {
                        required: true
                    },
                    'TSYSPassword': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSMerchantID': {
                        required: true
                    },
                    'cardpointeUser': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeSiteName': {
                        required: true
                    },
                    'cardpointePassword': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeMerchID': {
                        required: true
                    },
                    'apiloginID':{
                        required: true,
                        minlength: 3
                    },
                    'transactionKey':{
                        required: true,
                        minlength: 3
                    },       
         
                    'frname':{
                        required: true,
                        minlength: 2
                    },  
                    'paytracePassword':{
                        required: true,
                        minlength: 5
                    },
                
                    'paytraceUser':{
                        required: true,
                        minlength: 3
                    },

                    'paypalPassword':{
                        required: true,
                        minlength: 5
                    },
                    
                    'paypalUser':{
                        required: true,
                        minlength: 3
                    },
                 
                    'paypalSignature':{
                        required: true,
                        minlength: 5
                    },
            
                
                    'cyberMerchantID':{
                        required: true,
                        minlength: 5
                    },
                
                    'apiSerialNumber':{
                        required: true,
                         minlength: 3
                    },
                 
                    'secretKey':{
                        required: true,
                        minlength: 5
                    },
            
                    'stripeUser': {
                        required: true,
                        minlength: 3
                    },
                    'stripePassword':{
                        required : true,
                        minlength: 3,
                                
                    },  
            
                    'heartsecretkey':{
                        required: true,
                        minlength: 3
                    },
                 
                    'heartpublickey':{
                        required: true,
                        minlength: 5
                    },
                    'transtionPin':{
                        required: true,
                        minlength: 3
                    },
                 
                    'transtionKey':{
                        required: true,
                        minlength: 5
                    },
                    'gateway_opt':{
                        check_gateway:true,
                    },
                    'portal_url':{
                    
                        minlength: 3,
                        maxlength: 20,
                        remote: {
                            
                                beforeSend:function() { 
                                $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
                        
                            },
                            complete:function() {
                                $(".overlay1").remove();
                            },
                            url: "<?php echo base_url(); ?>Admin_panel/check_url",
                            type: "POST",
                            data: {
                                merchantID: function() {
                                    return $( "#merchID" ).val();
                                }
                            },
                            dataType: 'json',
                            dataFilter: function(response) {
                                
                                var rsdata = jQuery.parseJSON(response);
                                
                            
                                if(rsdata.status=='success')
                                    return true;
                                else{
                                    message1 = rsdata.portal_url;
                                
                                    return false;
                                }
                            }
                        },
                    },    
                    
    			},

                messages: {
                    'portal_url':{
                        remote: "This URL is currently being used"
                    },
                    'merchantEmail': {
                        remote: "Email Already Exitsts",
                    },
                    'merchantPassword1':
                    {
                        remote: 'The password must be different from your current password.',
                    },
                    'accountNumber': {
                        maxlength: "Please enter no more than 20 digits.",
                        minlength: "Please enter at least 3 digits.",
                    },
                    'routNumber': {
                        maxlength: "Please enter no more than 12 digits.",
                        minlength: "Please enter at least 3 digits.",
                    },

                },

    		});

            $('#nmiform').validate({ // initialize plugin
                ignore:":not(:visible)",            
                rules: {
                    
                    'gateway_opt':{
                        required: true,
                    },

                    'iTransactUsername':{
                        required: true,
                    },
                    'iTransactAPIKIEY':{
                        required: true,
                    },	
                    'surchargePercentage':{
                        surcharge_valid_rate: 'add_surcharge_box',
                    }, 
                 
                    'nmiUser': {
                        required: true,
                        minlength: 3
                    },
                    'nmiPassword':{
                        required : true,
                        minlength: 5,
                    },
                    'maverickAccessToken': {
                        required: true
                    },
                    'fluidUser': {
                        required: true,
                        minlength: 3
                    },
                    'basysUser': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSUser': {
                        required: true
                    },
                    'TSYSPassword': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSMerchantID1': {
                        required: true
                    },
                    'TSYSUser1': {
                        required: true
                    },
                    'TSYSPassword1': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSMerchantID1': {
                        required: true
                    },
                    'EPXCustNBR':{
                        required: true,
                    },
                    'EPXMerchNBR':{
                        required: true,
                    },  
                    'EPXterminal':{
                        required: true,
                    },  
                    'EPXDBANBR':{
                        required: true,
                    },  
                    'cardpointeUser': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeSiteName': {
                        required: true
                    },
                    'cardpointePassword': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeMerchID': {
                        required: true
                    },
                    'cardpointeUser1': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointePassword1': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeSiteName1': {
                        required: true
                    },
                    'cardpointeMerchID1': {
                        required: true
                    },
                    'apiloginID':{
                        required: true,
                        minlength: 3
                    },
                    'transactionKey':{
                        required: true,
                        minlength: 3
                    },       
                 
                    'frname':{
                        required: true,
                        minlength: 2
                    },  
                 
                    'nmiUser1': {
                        required: true,
                        minlength: 3
                     },
                    'nmiPassword1':{
                        required : true,
                        minlength: 5,
                    },
                    'maverickAccessToken': {
                        required: true
                    },
                    'apiloginID1':{
                        required: true,
                        minlength: 3
                    },
                    
                    'transactionKey1':{
                        required: true,
                        minlength: 3
                    },
                    'paytracePassword':{
                        required: true,
                        minlength: 5
                    },
                    
                    'paytraceUser':{
                        required: true,
                        minlength: 3
                    },
                 
                    'paytracePassword1':{
                        required: true,
                        minlength: 5
                    },
                    'paytraceUser1':{
                        required: true,
                        minlength: 3
                    },
                    
                    'paypalPassword':{
                        required: true,
                        minlength: 5
                    },
                    
                    'paypalUser':{
                        required: true,
                        minlength: 3
                    },
                 
                    'paypalSignature':{
                        required: true,
                        minlength: 5
                    },
                    'paypalUser1':{
                        required: true,
                        minlength: 3
                    },
                    'paypalPassword1':{
                        required: true,
                        minlength: 5
                    },
                    
                    'paypalSignature1':{
                        required: true,
                        minlength: 5
                    },
                    'cyberMerchantID':{
                        required: true,
                        minlength: 5
                    },
                    
                    'apiSerialNumber':{
                        required: true,
                        minlength: 3
                    },
                 
                    'secretKey':{
                        required: true,
                        minlength: 5
                    },
                    'fname':{
                        required:true,
                        minlength:2,
                    }    
                },
            });
            $('#nmiform1').validate({ // initialize plugin
                ignore:":not(:visible)",            
                rules: {
                    
                    'gateway':{
                        required: true,
                    },

                    'iTransactUsername1':{
                        required: true,
                    },
                    'iTransactAPIKIEY1':{
                        required: true,
                    },  
                    'surchargePercentage1':{
                        surcharge_valid_rate: 'add_surcharge_box',
                    }, 
                 
                    'EPXCustNBR1':{
                        required: true,
                    },
                    'EPXMerchNBR1':{
                        required: true,
                    },  
                    'EPXterminal1':{
                        required: true,
                    },  
                    'EPXDBANBR1':{
                        required: true,
                    },  
                    'fluidUser1': {
                        required: true,
                        minlength: 3
                    },
                    'basysUser1': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSUser1': {
                        required: true
                    },
                    'TSYSPassword1': {
                        required: true,
                        minlength: 3
                    },
                    'TSYSMerchantID1': {
                        required: true
                    },
                    'cardpointeUser1': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointePassword1': {
                        required: true,
                        minlength: 3
                    },
                    'cardpointeSiteName1': {
                        required: true
                    },
                    'cardpointeMerchID1': {
                        required: true
                    },
                           
                 
                    'frname':{
                        required: true,
                        minlength: 3
                    },  
                 
                    'nmiUser1': {
                        required: true,
                        minlength: 3
                     },
                    'nmiPassword1':{
                        required : true,
                        minlength: 5,
                    },
                    'maverickAccessToken1': {
                        required: true
                    },
                    'apiloginID1':{
                        required: true,
                        minlength: 3
                    },
                    
                    'transactionKey1':{
                        required: true,
                        minlength: 3
                    },
                    
                    'paytracePassword1':{
                        required: true,
                        minlength: 5
                    },
                    'paytraceUser1':{
                        required: true,
                        minlength: 3
                    },
                    
                    'paypalPassword1':{
                        required: true,
                        minlength: 5
                    },
                    
                    'paypalUser1':{
                        required: true,
                        minlength: 3
                    },
                 
                    'paypalSignature1':{
                        required: true,
                        minlength: 5
                    },
                   
                   
                    'cyberMerchantID1':{
                        required: true,
                        minlength: 5
                    },
                    
                    'apiSerialNumber1':{
                        required: true,
                        minlength: 3
                    },
                 
                    'secretKey1':{
                        required: true,
                        minlength: 5
                    },
                    'fname':{
                        required:true,
                        minlength:2,
                    }    
                },
            });
            $.validator.addMethod('isemail', function(value, element, params) {
                  var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                
                if(value.length == 0){
                    return true;
                }else{
                     if(!regex.test(value)) {
                        return false;
                      }else{
                        return true;
                      }
                }
                  
            }, 'Please enter a valid email address.');

            $.validator.addMethod('surcharge_valid_rate', function(value, element, param) {
                var result = true;
                if($('#'+param).prop('checked') == true){
                    surchangeMessage = 'Please enter valid surcharge rate';
                    if(value > 0) {
                        var regexp = /^[0-9]+([,.][0-9]+)?$/g;
                        return regexp.test(value);
                    } else {
                        return false;
                    }
                }
                return this.optional(element) || result;
            }, function() { return surchangeMessage; });

            $.validator.addMethod('CCExp', function(value, element, params) {
                  var minMonth = new Date().getMonth() + 1;
                  var minYear = new Date().getFullYear();
                  var month = parseInt($(params.month).val(), 10);
                  var year = parseInt($(params.year).val(), 10);
                  return (year > minYear || (year === minYear && month >= minMonth));
            }, 'Your Credit Card Expiration date is invalid.');

            $.validator.addMethod("check_atleast_one_number", function(value, element) {
                if(value.length == 0){
                    return true ;
                }else{
                    if (value.search(/[0-9]/) < 0)
                    return false;
                   
                }
                 return true ;
         
            }, "This field should contain at least one number.");

            $.validator.addMethod("check_atleast_one_special_char", function(value, element) {
                var regularExpression  = /^[a-zA-Z0-9 ]*$/;
                if(value.length == 0){
                    return true ;
                }
                if (regularExpression.test(value)) {
                    return false;
                } 
                return true ;
            }, "This field should contain at least one special character.");

            $.validator.addMethod("check_atleast_one_lower", function(value, element) {
                    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
                    if(value.length == 0){
                        return true;
                    }
                    if (value.match(/([a-z])/) && value.match(/([0-9])/)){
                        return true;
                    }else{
                      return false;
                    }
                    return true ;
            }, "This field should contain at least one lower characters.");
            $.validator.addMethod("check_atleast_one_upper", function(value, element) {
                    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
                    if(value.length == 0){
                        return true;
                    }
                    if (value.match(/([A-Z])/) && value.match(/([0-9])/)){
                        return true;
                    }else{
                      return false;
                    }
                    return true ;
            }, "This field should contain at least one uppercase characters.");
            $.validator.addMethod('check_name', function(value, element, params) {
                if(value.length == 0){
                    return true ;
                }else{
                    var firstName = $('#firstName').val();
                    if(firstName.length == 0){
                        return true ;
                    }else{

                        if(value.indexOf(firstName) != -1){
                            return false;
                        }
                    }
                    if (value.search(/[0-9]/) < 0)
                    return false;
                   
                }
                return true ;
            }, 'First name not used in password.');

            $.validator.addMethod('company_name', function(value, element, params) {
                if(value.length == 0){
                    return true ;
                }else{
                    var companyName = $('#companyName').val();
                    if(companyName.length == 0){
                        return true ;
                    }else{

                        if(value.indexOf(companyName) != -1){
                            return false;
                        }
                    }
                    if (value.search(/[0-9]/) < 0)
                    return false;
                   
                }
                return true ;
            }, 'Company name not used in password.');

            $.validator.addMethod('check_email', function(value, element, params) {
                if(value.length == 0){
                    return true ;
                }else{
                    var merchantEmail = $('#merchantEmail').val();
                    if(merchantEmail.length == 0){
                        return true ;
                    }else{
                       
                        var res = merchantEmail.split("@");
                        if(value.indexOf(res[0]) != -1){
                            return false;
                        }
                    }
                    if (value.search(/[0-9]/) < 0)
                    return false;
                   
                }
                return true ;
            }, 'Email not used in password.');
    	});
            
            
            $.validator.addMethod("check_gateway", function(value, element) {
    
                if(value=='' && $('#chk_gateway').val()==0)
                return true;
                
                return value > 0 ;

            }, "This field is required.");

            function set_gateway_data(el)
            {
                var gateway_value= $('#'+el.id).val();
                
                $('.gateway-div').hide();
                $('.gateway-div1').hide();

                if(gateway_value!="")
                {
                    $("#fr_div").show();
                    
                    if(gateway_value=='3'){          
                        $('#pay_div').show();
                    }else if(gateway_value=='2'){
                        $('#auth_div').show();
                    }else if(gateway_value=='1'){
                        $('#nmi_div').show();   
                        
                    }else if(gateway_value=='4'){
                        $('#paypal_div').show();    
                        
                    }else if(gateway_value=='5'){
                        $('#stripe_div').show();
                        
                    }else if(gateway_value=='6'){
                        $('#usaepay_div').show();
                        
                    }else if(gateway_value=='7'){
                        $('#heartland_div').show();  
                        
                    }else if(gateway_value=='8'){
                        $('#cyber_div').show(); 
                        
                    }else if(gateway_value=='11'){
                        $('#fluid_div').show();
                        
                    }else if(gateway_value=='10'){
                        $('#iTransact_div').show();
                        
                    }else if(gateway_value=='12'){
                        $('#TSYS_div').show();
                    }else if(gateway_value=='13'){
                        $('#basys_div').show();
                   
                    }else if(gateway_value=='14'){
                        $('#cardpointe_div').show();          
                    }else if(gateway_value=='15'){
                        $('#payarc_div').show();
                        
                    }else if(gateway_value== '17')
                    {
                        $('#maverick_div').show();
                    }else if(gateway_value=='16')
                    {
                        $('#EPX_div').show();
                        
                    }
                }
              
            }

            
            function surchageCheckChange(e, divId = ''){
                if($('#'+e.id).prop('checked') == true){
                    $('#surchargePercentageBox'+divId).show();
                } else {
                    $('#surchargePercentageBox'+divId).hide();
                }
            }

            function del_gateway_id(id){
    
                $('#merchantgatewayid').val(id);
            }
            // Function to  get  gateway 

            function set_edit_gateway(gatewayid){
                
                if(gatewayid !=""){
                   
                    $.ajax({
                        url: '<?php echo base_url("Admin_panel/get_merchant_gatewayedit_id")?>',
                        type: 'POST',
                        data:{gatewayid:gatewayid},
                        dataType: 'json',
                        success: function(data){
                    
                            $('#gatewayEditID').val(data.gatewayID);       
                         
                            $('.gateway-div1').hide();

                            if(data.gatewayType=='1')
                            {
                                $('#nmi_div1').show();
                                $('#nmiUser1').val(data.gatewayUsername);
                                $('#nmiPassword1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                                if(data.creditCard==1){
                                    $('#nmi_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#nmi_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#nmi_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#nmi_ach_status1').removeAttr('checked');
                                }
                            }
                          
                            if(data.gatewayType=='2')
                            {
                                $('#auth_div1').show();
                                
                                $('#apiloginID1').val(data.gatewayUsername);
                                $('#transactionKey1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                                if(data.creditCard==1){
                                    $('#auth_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#auth_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#auth_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#auth_ach_status1').removeAttr('checked');
                                }
                            }
                            
                            if(data.gatewayType=='3')
                            {
                                $('#pay_div1').show();
                                
                                $('#paytraceUser1').val(data.gatewayUsername);
                                $('#paytracePassword1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                                if(data.creditCard==1){
                                    $('#paytrace_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#paytrace_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#paytrace_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#paytrace_ach_status1').removeAttr('checked');
                                }
                            }   

                            if(data.gatewayType=='4')
                            {
                                
                                $('#paypal_div1').show();
                                $('#paypalUser1').val(data.gatewayUsername);
                                $('#paypalPassword1').val(data.gatewayPassword);
                                $('#paypalSignature1').val(data.gatewaySignature);
                            }   
                            if(data.gatewayType=='5')
                            {
                                
                                $('#stripe_div1').show();
                                $('#stripeUser1').val(data.gatewayUsername);
                                $('#stripePassword1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                            }                   
                            if(data.gatewayType=='6')
                            {
                                
                                $('#usaepay_div1').show();
                                $('#transtionKey1').val(data.gatewayUsername);
                                $('#transtionPin1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                            }
                            if(data.gatewayType=='7')
                            {
                                $('#heartland_div1').show(); 
                                $('#heartpublickey1').val(data.gatewayUsername);
                                $('#heartsecretkey1').val(data.gatewayPassword);

                                if(data.creditCard==1){
                                    $('#heart_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#heart_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#heart_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#heart_ach_status1').removeAttr('checked');
                                }
                            }   
                            
                            if(data.gatewayType=='8')
                            {
                                $('#cyber_div1').show();
                                $('#cyberMerchantID1').val(data.gatewayUsername);
                                $('#apiSerialNumber1').val(data.gatewayPassword);
                                $('#secretKey1').val(data.gatewaySignature);
                                $('#EPX_div1').hide();
                            }

                            if(data.gatewayType=='9')
                            {
                                $('#cz_div1').show();
                                $('#czUser1').val(data.gatewayUsername);
                                $('#czPassword1').val(data.gatewayPassword);
                                $('#EPX_div1').hide();
                                if(data.creditCard==1){
                                    $('#cz_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#cz_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#cz_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#cz_ach_status1').removeAttr('checked');
                                }

                            }
                            if(data.gatewayType=='11')
                            {
                                $('#fluid_div1').show();
                                $('#fluidUser1').val(data.gatewayUsername);
                                $('#EPX_div1').hide();
                              
                                if(data.creditCard==1){
                                    $('#fluid_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#fluid_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#fluid_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#fluid_ach_status1').removeAttr('checked');
                                }
                            }

                            if(data.gatewayType=='10')
                            {
                                $('#iTransact_div1').show();
                                $('#iTransactUsername1').val(data.gatewayUsername);
                                $('#iTransactAPIKIEY1').val(data.gatewayPassword);
                                $('#surchargePercentage1').val(data.surchargePercentage);
                                $('#EPX_div1').hide();
                                
                                if(data.creditCard=='1'){
                                    $('#iTransact_cr_status1').attr('checked','checked');  
                                    
                                }else{
                                    $('#iTransact_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus=='1')
                                {
                                    $('#iTransact_ach_status1').attr('checked','checked');
                                }
                                else{
                                    $('#iTransact_ach_status1').removeAttr('checked');
                                }

                                if(data.isSurcharge=='1')
                                {
                                    $('#add_surcharge_box1').attr('checked','checked');
                                    $('#surchargePercentageBox1').show();
                                }
                                else{
                                    $('#add_surcharge_box1').removeAttr('checked');
                                    $('#surchargePercentageBox1').hide();
                                }
                            }
                            if(data.gatewayType=='12')
                            {
                                
                                $('#TSYS_div1').show();
                                $('#TSYSUser1').val(data.gatewayUsername);
                                $('#TSYSPassword1').val(data.gatewayPassword);
                                $('#TSYSMerchantID1').val(data.gatewayMerchantID);
                                $('#EPX_div1').hide();
                                if(data.creditCard==1){
                                    $('#TSYS_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#TSYS_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#TSYS_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#TSYS_ach_status1').removeAttr('checked');
                                }
                            }
                            if(data.gatewayType=='13')
                            {
                                $('#basys_div1').show();
                                $('#basysUser1').val(data.gatewayUsername);
                                if(data.creditCard==1){
                                    $('#basys_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#basys_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#basys_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#basys_ach_status1').removeAttr('checked');
                                }
                            }
                            if(data.gatewayType=='14')
                            {
                                $('#cardpointe_div1').show();          
                                $('#cardpointeUser1').val(data.gatewayUsername);
                                $('#cardpointePassword1').val(data.gatewayPassword);
                                $('#cardpointeMerchID1').val(data.gatewayMerchantID);
                                $('#cardpointeSiteName1').val(data.gatewaySignature);
                                if(data.creditCard==1){
                                    $('#cardpointe_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#cardpointe_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#cardpointe_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#cardpointe_ach_status1').removeAttr('checked');
                                }
                            }
                            if(data.gatewayType=='15')
                            {
                                $('#payarc_div1').show();
                                $('#payarcUser1').val(data.gatewayUsername);
                                
                            }
                            if(data.gatewayType=='16')
                            {
                                
                                $('#EPX_div1').show();
                                $('#EPXCustNBR1').val(data.gatewayUsername);
                                $('#EPXMerchNBR1').val(data.gatewayPassword);
                                $('#EPXDBANBR1').val(data.gatewaySignature);
                                $('#EPXterminal1').val(data.extra_field_1);
                               
                                if(data.creditCard==1){
                                    $('#EPX_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#EPX_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#EPX_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#EPX_ach_status1').removeAttr('checked');
                                }
                            }

                            if(data.gatewayType == '17')
                            {

                                $('#maverick_div1').show();
                                $('#maverickAccessToken1').val(data.gatewayUsername);
                                $('#maverickTerminalId1').val(data.gatewayPassword);

                                if(data.creditCard==1){
                                    $('#maverick_cr_status1').attr('checked','checked');  
                                }else{
                                    $('#maverick_cr_status1').removeAttr('checked');    
                                }
                                if(data.echeckStatus==1){
                                    $('#maverick_ach_status1').attr('checked','checked');
                                }
                                else {
                                    $('#maverick_ach_status1').removeAttr('checked');
                                }
                            }
                            
                            $('#fname').val(data.gatewayFriendlyName);
                            $('#gateway').val(data.gateway);
                            $('#mid').val(data.merchantID);
                        }   
                    }); 
                }
            }
            var html_trial = '<div class ="form-group"><label class="col-md-4 control-label" for="example-typeahead">Free Trial </label><div class="col-md-8"><input type="checkbox" onclick="onFreeTrialCheck()" id="is_free_trial" name="is_free_trial" checked="true" value="1"></div></div>';
            var html_un_trial = '<input type="hidden" id="is_free_trial" name="is_free_trial" checked="true" value="0">';
            function check_plan_data(el)
            {
                
               var plan= $('#'+el.id).val();
               
                $.ajax({
                    url: '<?php echo base_url("Admin_panel/get_plan_data")?>',
                    type: 'POST',
                    data:{plan_id:plan},
                    success: function(data){
                      var plan = jQuery.parseJSON(data);
                     
                        <?php
                        if(isset($merchant) && !empty($merchant)) { ?>
                            $('#free_trial_section').html(html_un_trial);
                            $('#free_trial_day').val(0);
                        <?php }else{
                        ?>
                            if(plan.is_free_trial == 1){
                                $('#free_trial_day').val(plan.free_trial_day);
                                $('#free_trial_section').html(html_trial);
                            }else{
                                $('#free_trial_section').html(html_un_trial);
                                $('#free_trial_day').val(0);
                            }
                        <?php
                        }
                        ?>
                    }
                });
              
            }
            function onFreeTrialCheck(){
                if($( "#is_free_trial:checked" ).length == 1){
                    $('#is_free_trial').val(1);
                    
                }else{
                    $('#is_free_trial').val(0);
                }
            }
    </script>
</div>