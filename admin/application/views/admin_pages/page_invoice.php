<style>
.status_filter{
  	width: 150px;
    position: absolute;
    top: 9px;
    left: 90px;
    z-index: 1;
}
 .disable_load{
    opacity: 0.5;
    pointer-events: none;
 }
 .statusGrp{
    position: relative;
    left: 186px;
 }
</style>
<!-- Page content -->
<div id="page-content">
    <div class="row ">
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    </div>
    <legend class="leg">Merchant Invoices</legend>
    <div class="block-main full" style="position: relative">
        <div class="statusGrp">
            <form class="filter_form" action="<?php echo base_url();?>home/invoices" method="post">
                <select class="form-control status_filter" id='status_filter' name="status_filter">
                    <option value="All" <?php if($filter == 'All'){ echo 'Selected';}?>>All</option>
                    <option value="Pending" <?php if($filter == 'Pending'){ echo 'Selected';}?>>Pending</option>
                    <option value="Unpaid" <?php if($filter == 'Unpaid'){ echo 'Selected';}?>>Unpaid</option>
                    <option value="Paid" <?php if($filter == 'Paid'){ echo 'Selected';}?>>Paid</option>
                    <option value="Void" <?php if($filter == 'Void'){ echo 'Selected';}?>>Void</option>
                    <option value="Refund" <?php if($filter == 'Refund'){ echo 'Selected';}?>>Refunded</option>
                </select>
            </form>
        </div>
        <div class="addNewFixRight">
            <a class="btn  btn-sm btn-info" title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#notes" >Add/Edit Notes</a>      
        </div>
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left hidden-xs">Merchant</th>
                    <th class="text-left">Invoice</th>
                    <th class="hidden-xs text-right hidden-sm">Invoice Date</th>
                    <th class="text-right hidden-xs">Amount</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            
        </table>
        <!--END All Orders Content-->
    </div>
</div>

    
<div id="notes" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add/Edit Notes</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
           <div class="modal-body">
                 <form id="addnotes" method="post" action='<?php echo base_url(); ?>home/add_edit_notes' class="form-horizontal" >
				 <div class="form-group ">
                         
						 <div class="col-md-12">
						    <textarea id="adminNotes" name="adminNotes" class="form-control" rows="4" placeholder="Update Invoice Notes here"><?php echo $admin_data['adminNotes']; ?></textarea>
							</div>
						
						</div>
                        <div class ="form-group">
                            <label class="col-md-6 control-label" for="example-typeahead"></label>
                            <div class="col-md-6 text-right">
                                <input type="submit"  name="submit" class="btn btn-sm btn-success" value="Save"  />
                                <button type="button"   class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
					
				
			    </form>	
				

            </div>
                      				
           
			
			
            <!-- END Modal Body -->
        </div>
    </div>
   </div> 
   
    <div id="reseller_invoice_process1" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>merchantPayment/merchant_payment' class="form-horizontal " >
                     <input type="hidden" id="scheduleID" name="scheduleID" class="form-control"  value="" />
                     <input type="hidden" id="merchantID" name="merchantID" class="form-control"  value="" />
                 
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="card_list">Payment Method</label>
                        <div class="col-md-8" id="radioSection">
                            <label class="col-md-6 row text-left"><input id="creditCheckbox" value="1" onclick="get_process_details(this);"  checked type="radio" name="sch_method" class="radio_pay"></input>  Credit Card </label>      
                            <label class="col-md-6 row"><input value="2" id="accountCheckbox" onclick="get_process_details(this);"  type="radio" name="sch_method" class="radio_pay"></input> Checking Account </label>
                       </div>
                    </div>
                    <div id="pay_data">
                    <?php 
                        if(!isset($defaultGateway) || !$defaultGateway){
                    ?>
                        <div class="form-group">
                              
                            <label class="col-md-4 control-label" for="card_list">Gateway</label>
                            <div class="col-md-6">
                                <select id="gateway" name="gateway"  class="form-control">
                                    
                                    <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                            foreach($gateway_datas as $gateway_data){
                                            ?>
                                       <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                            <?php } } ?>
                                </select>
                                    
                            </div>
                        </div>
                        <?php 
                        } else { ?>
                            <input type="hidden" name="gateway" value="<?php echo $defaultGateway['gatewayID'];  ?>">
                        <?php }  ?>         
                            
                        <div class="form-group ">
                              
                            <label class="col-md-4 control-label" for="card_list">Select Card</label>
                            <div class="col-md-6">
                                
                                <select id="merchantCardID" name="merchantCardID"  class="form-control">
                                   
                                </select>
                                        
                            </div>
                        </div>
                    </div>
					
					<div class="card_div" id="card_div" >
                    </div>
                    <div class="card_div" id="ca_div" >
                    </div>
					   
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoice" name="invoice" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
 <div id="ress_void" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Void Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>home/void_invoice' class="form-horizontal" >
                     
                 
					<p>Do you really want to void this invoice?</p> 
					
					
					   
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="pinvoiceID" name="pinvoiceID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-danger" value="Void"  />
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<div id="set_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Mark Invoice as Paid</h2>
                  
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="sub_ad_payment" method="post" action='<?php echo base_url(); ?>home/add_payment' class="form-horizontal" >
                     
                 
					 <div class="form-group">
                     <label class="col-md-4 control-label">Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date">
                            <input type="text" id="payment_date" name="payment_date" class="form-control input-datepicker" data-date-format="mm/dd/yyyy" placeholder="mm/dd/yyyy" value="">
                        </div>
                      </div>    
                    </div>
                    
                     <div class="form-group">
                     <label class="col-md-4 control-label">Check Number</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="check_number" placeholder="Check Number" size="40" name="check_number" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                    <div class="form-group">
                     <label class="col-md-4 control-label" >Amount</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="inv_amount" placeholder="Amount" size="40" name="inv_amount" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                   
                    
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="qbo_subscID" name="subscID" class="form-control"  value="" />
						
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  class="btn btn-sm btn-success" value="Add Payment"  />
                    <button type="button" class="btn btn-sm btn-primary close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();
            var status_filter = $('#status_filter').val();
            / Initialize Datatables /
            $('#invoice_page').dataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '<?php echo base_url();?>home/serverProcessingInvoice?status_filter='+status_filter,
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [5] }
                ],
                "columns": [
                            {
                                "class" : "text-left hidden-xs cust_view",    // or "className" : 
                            },
                 
                            {
                                 "class" : "text-left cust_view",
                            },
                            {
                                "class" : "hidden-xs text-right hidden-sm",    // or "className" : 
                            },
                 
                            {
                                 "class" : "text-right hidden-xs",
                            },
                 
                            {
                                 "class" : "text-center",
                            },
                 
                            {
                                 "class" : "text-center",
                            }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

$('#status_filter').change(function(){
    $('.filter_form').submit();
});

</script>
<div id="payment_refund_popup_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header text-center">
        <h2 class="modal-title">Refund Payment</h2>
      </div>
      <!-- END Modal Header -->

      <!-- Modal Body -->
      <div class="modal-body">

        <div id="refund_msg"></div>

        <form id="data_form" method="post" action='<?php echo base_url(); ?>merchantPayment/create_merchant_invoice_refund' class="form-horizontal">
          <p id="message_data">Are you sure you want to refund this Invoice?</p>

          <div id="ref_id">

          </div>

          <input type="hidden" id="txnInvoiceID" name="txnInvoiceID" value="" />

          <div class="pull-right">
            <input type="submit" id="rf_btn" name="btn_cancel" class="btn btn-sm btn-warning" value="Refund" />
            <button type="button" class="btn btn-sm btn-default  close1" data-dismiss="modal">Cancel</button>
          </div>
          <br />
          <br />
        </form>

      </div>
      <!-- END Modal Body -->
    </div>
  </div>
</div>
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>

<script>
	
var gtype ='';

	  

$(document).ready(function(){
	$('#card_div').hide();
    $('#ca_div').hide();
     
     var date = new Date();
    date.setDate(date.getDate());

    $('#payment_date').datepicker({ 
        startDate: date
    });
    
    $("#thest_pay").validate({
        rules: {

            gateway: {
                required: true,
            
            },
        
            merchantCardID:{
                required: true,
            },
            card_number:{
                required:true,
                minlength: 13,
                maxlength: 16,
                number: true
            },
            expiry_year: {
                CCExp: {
                  month: '#expiry11',
                  year: '#expiry_year11'
                }
            },
            cvv: {
                number: true,
                minlength: 3,
                maxlength: 4,
            },
            acc_number:{
                required: true,
                number: true,
                minlength: 3,
                maxlength: 20,
            },
            route_number:{
                required: true,
                number: true,
                minlength: 3,
                maxlength: 12,
            },
            acc_name:{
                required: true,
                minlength: 3,
                maxlength: 30,
            }
        } 
    });
    $.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
    }, 'Your Credit Card Expiration date is invalid.');
    $("#sub_ad_payment").validate({
        rules: {

            payment_date: {
                required: true,
            
            },
        
            inv_amount:{
                required: true,
                number: true,
                check_amount:true,
            },
        
            check_number: {
                required: true,
                minlength: 3,
                maxlength: 20,
            },
        } 
     });
     
     $.validator.addMethod("check_amount", function(value, element) {
         
        var actual_val = $('#inv_pay_amount').val();

        actual_val = parseFloat(actual_val);
        value = parseFloat(value);
        
        var returnVal = true;
        if(value ==''){
            returnVal = false;
        }else if (value > actual_val){
            returnVal = false;
        }

        return returnVal;
    }, "Entered amount is exceeded than actual amount");
 
});
function get_payment_data(invoice) {


    if (invoice != "") {


        $.ajax({

            type: 'POST',
            url: '<?php echo  base_url(); ?>ajaxRequest/view_invoice_transaction',
            data: { 'invoiceID': invoice, 'action': 'invoice' },
            dataType: 'json',
            success: function (response) {
                $('#ecom-orders').html(response.transaction);

            }

        });

    }

}

function set_subs_id(sID){
	
	  $('#subscID').val(sID);
	
}

  

function set_customerList_id(sID){
	
	  $('#custID').val(sID);
	
}


function del_credit_id(id){
	
	     $('#invoicecreditid').val(id);
}



function del_user_id(id){
	
	     $('#merchantID').val(id);
}



function set_company_id(id){
	
	     $('#companyID').val(id);
}	        
  
function set_invoice_id(id){
	
	     $('#invoiceID').val(id);
}	      


function set_void_invoice_process_id(invID)
{
     $('#pinvoiceID').val(invID);
}
	
	
	

function set_invoice_schedule_id(id, ind_date){
	$('#scheduleID').val(id);

	var nowDate = new Date(ind_date);
	
	
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
	 
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}	



function set_process_id(inv,resellerID,amt)
{
    
  var form =  $("#sub_ad_payment");
  $('#inv_amount').val(amt);
  $('#invID').remove();
  $('#inv_pay_amount').remove();
$('<input>', {
										'type': 'hidden',
										'id'  : 'invID',
										'name': 'invID',
										'value':inv 
										}).appendTo(form);
										
$('<input>', {
										'type': 'hidden',
										'id'  : 'inv_pay_amount',
										'name': 'inv_pay_amount',
										'value':amt 
										}).appendTo(form);										
	
}	
function set_reseller_invoice_process_id(invoice, rid){
	
	     $('#invoice').val(invoice);
	     $('#scheduleID').val(invoice);
         $('#merchantID').val(rid);
         $('#card_div').hide();
         $('#ca_div').hide();

         $('#thest_pay').addClass('disable_load'); 
         

         $('#radioSection').html('<label class="col-md-6 row text-left"><input value="1" onclick="get_process_details(this);"  checked type="radio" id="creditCheckbox" name="sch_method" class="radio_pay"></input>  Credit Card </label><label class="col-md-6 row"><input value="2" onclick="get_process_details(this);"  type="radio" id="accountCheckbox" name="sch_method" class="radio_pay"></input> Checking Account </label>');
	    
         
                var d = new Date();
                var i;
                var opt;
                var cruy = d.getFullYear();
                var nextThreeYear = cruy + 3;
                
                var dyear = cruy + 25;
                for(i = cruy; i < dyear ;i++ ){  
                    if(nextThreeYear == i){
                        opt+='<option selected value='+i+'>'+i+'</option>';
                    }else{
                        opt+='<option value='+i+'>'+i+'</option>';
                    }
                    
                }
                var card_daata='<fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="Card Number"  autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11" ngonblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="1234" autocomplete="off" /></div></div> <div class="form-group hidden"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc" checked disabled="true"  /> Do not save Credit Card</div></div></fieldset>';
           
                var ca_daata='<fieldset><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Number</label><div class="col-md-8"><input type="text" id="acc_number" name="acc_number" class="form-control" value="" placeholder="Account Number"></div></div><div class="form-group"><label class="col-md-4 control-label" for="card_number">Routing Number</label><div class="col-md-8"><input type="text" id="route_number" name="route_number" class="form-control" placeholder="Routing Number"></div></div><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Name</label><div class="col-md-8"><input type="text" id="acc_name" name="acc_name" class="form-control" value="" placeholder="Account Name"></div> </div><div class="form-group"><label class="col-md-4 control-label" for="Entry Methods">Entry Method</label><div class="col-md-6"><div class="input-group"><select id="secCode" name="secCode" class="form-control valid" aria-invalid="false"><option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option></select>  </div></div></div><div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Type</label><div class="col-md-6"><select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false"><option value="checking">Checking</option><option value="saving">Saving</option> </select></div></div><div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label><div class="col-md-6"><select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false"><option value="business">Business</option><option value="personal">Personal</option></select></div></div></fieldset>';
            
                             
                                
        

        $('#card_div').html(card_daata);
        $('#ca_div').html(ca_daata);
                 
		 $.ajax({
            type: "POST",
            url: '<?php echo  base_url(); ?>ajaxRequest/get_process_details_data',
            data: { 'invID': invoice, 'type': 1,'merchantID' : rid },
            dataType: 'json',
            success: function (response) {


                if (response.status == 'success') {


                    $('#pay_data').html(response.schd);
                    
                    if(response.payOption == 2){
                        $('#accountCheckbox').click();
                    }else if(response.payOption == 0){
                        $("#merchantCardID").val("new1").change();
                    }else if(response.managePayOption == 0){
                        $("#merchantCardID").val("new1").change();
                    }

                }
                $('#thest_pay').removeClass('disable_load'); 

            }


        });
		if(rid!=""){
			
			
			
		}	
		 
		 
}	      

	      
/********************Show The Payment Data**************/
 
function set_payment_data(id){
	
    if(id !=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>Payments/view_transaction",
			data:{ 'invoiceID':id},
			success : function(response){
			
				$('#pay-content-data').html(response);
				
			}
			
		});
		
	}  
  
}

function create_sch_card(eid,type) {
    
    var crd = eid.value;
    var rid = $('#merchantID').val();
    if (crd == 'new1') {

        
        
        if(type == 1){
            $('#card_div').show();
            $('#ca_div').hide();
        }else if(type == 2){
            $('#ca_div').show();
            $('#card_div').hide();
        }
        
        
    }else{
        $('#card_div').hide();
        $('#ca_div').hide();
    }
}

/* Get change process invoice tab */
function get_process_details(eid) {

    $('#thest_pay').addClass('disable_load'); 
    var type = eid.value;

    var sch = $('#scheduleID').val();
    $('#card_div').hide();
    $('#ca_div').hide();
    var merchantID = $('#merchantID').val();

    if (sch != "") {


        $.ajax({
            type: "POST",
            url: '<?php echo  base_url(); ?>ajaxRequest/get_process_details_data',
            data: { 'invID': sch, 'type': type,'merchantID' : merchantID },
            dataType: 'json',
            success: function (response) {


                if (response.status == 'success') {

                    $('#pay_data').html(response.schd);
                    
                    if(response.payOption == 2){
                        if(type == 1){
                            $("#merchantCardID").val("new1").change();
                        }else if(response.managePayOption == 0){
                            $("#merchantCardID").val("new1").change();
                        }
                       
                    }else if(response.payOption == 1){
                        if(type == 2){
                            $("#merchantCardID").val("new1").change();
                        }else if(response.managePayOption == 0){
                            $("#merchantCardID").val("new1").change();
                        }
                       
                    }else if(response.payOption == 0){
                        $("#merchantCardID").val("new1").change();
                    }

                }
                $('#thest_pay').removeClass('disable_load'); 
            }


        });
    }

}

function set_refund_popup(txnInvoiceID) {

    if (txnInvoiceID != "") {
      $('#txnInvoiceID').val(txnInvoiceID);
    }
  }
</script> 



