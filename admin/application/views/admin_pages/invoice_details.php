
<!-- Page content -->
<div id="page-content">
    
    <!-- All Orders Block -->
    <div class="block full">
       
        <div class="block-title">
            <h2><strong> Merchants Invoice History</strong></h2>
            
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="subscription" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class="text-left">Merchant</th>
                    <th class="text-right ">Invoice</th>
					 <th class="hidden-xs text-right">Due Date</th>
                      <th class="text-right hidden-xs">Balance Remaining</th>
                      <th class="text-center hidden-xs">Status</th>
                      <th class="text-left hidden-xs hidden-sm">Gateway</th>
                      <th class="text-left hidden-xs hidden-sm">Card</th>
                      
					
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				if(isset($subscription) && $subscription)
				{
					foreach($subscription as $subs)
					{ 
					
				?>
				<tr>
					
					<td class="text-left "><?php echo $subs['merchantName']; ?></td>
					<td class="text-right "><?php echo $subs['invoice']; ?></td>
					<td class="text-right hidden-xs"><?php echo $subs['DueDate']; ?></td>
					<td class="text-right hidden-xs ">$<?php $vall= number_format((float)$subs['BalanceRemaining'],2); echo ($vall)?$vall:'0.00'; ?></td>
					<td class="text-center hidden-xs"><?php echo $subs['status']; ?></td>
					<td class="text-left hidden-xs hidden-sm"><?php echo $subs['gatewayFriendlyName']; ?></td>
					<td class="text-left hidden-xs hidden-sm">
					<?php foreach($card_list as $card)
					   { ?>
					<?php if(isset($card)){ if($card['merchantCardID']==$subs['cardID'] ){ echo $card['merchantFriendlyName']; }}?>
					 <?php }?>
					</td>
				 
					
					
				
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->


<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#subscription').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
<!-- END Page Content -->