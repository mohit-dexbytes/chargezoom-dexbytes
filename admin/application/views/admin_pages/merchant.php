<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?></div>
    <legend class="leg">Merchant Management</legend>
    <div class="block-main full" style="position: relative">
        <div class="addNewFixRight">
            <a class="btn btn-sm  btn-success" title="Create Merchant"  href="<?php echo base_url(); ?>Admin_panel/create_merchant">Add New</a>      
        </div>
        <table id="merchant_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left"> Merchant </th>
					<th class="hidden-xs text-left"> Reseller </th>
					<th class="text-left hidden-xs"> Plan</th>
                    <th class="text-left hidden-xs"> Revenue</th>
                    <th class="text-left hidden-xs"> Created</th>
                    <th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
                    if(isset($merchant_list) && $merchant_list){
                        foreach($merchant_list as $merchant){
                            if($merchant['total_amount'])   
                                $volume = $merchant['total_amount']-$merchant['refund_amount'];
                            else
                                $volume =0;
                            ?>
                                <tr>
                                    <td class="text-left cust_view"><a href="<?php echo base_url('Admin_panel/create_merchant/'.$merchant['merchID']); ?>" data-toggle="tooltip" title="Edit Merchant"> <?php echo ($merchant['companyName'])?$merchant['companyName']:'-----'; ?></a></td>
                                    <td class="text-left hidden-xs"> <?php echo $merchant['resellerCompanyName']; ?></td>
                                    <td class="text-left hidden-xs"> <?php echo $merchant['plan']; ?></td>
                                    <td class="text-left hidden-xs">$<?php echo calculate_revenue($merchant['merchID'],$volume, $merchant['plan_id']); ?></td>
                                    <td class="text-left hidden-xs" data-sort="<?php echo strtotime($merchant['date_added']); ?>"><?php echo date('M d, Y', strtotime($merchant['date_added'])); ?></td>
                                    <td class="text-center">
                                        <div class="btn-group dropbtn">
                                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                            <ul class="dropdown-menu text-left">
                                                <li> <?php if($merchant['isSuspend']=='0'){  ?>
                                                    <a href="<?php echo base_url('Admin_panel/merchant_login/'.$merchant['merchID']); ?>" target="_blank" class="" data-toggle="tooltip" title="">Login as Merchant</a>
                                                    <?php } ?>
                                                </li>
                                                <?php if($merchant['isSuspend']=='0'){  ?>	 
                                                <li>    <a href="#del_merchant" onclick="set_merchant_id('<?php echo $merchant['merchID']; ?>','0');"  title="Suspend Merchant" data-toggle="modal">Suspend Merchant</a>
                                                </li> <?php 	}   if($merchant['isSuspend']=='1'){  ?> 
                                                <li> <a href="#del_merchant" onclick="set_merchant_id('<?php echo $merchant['merchID']; ?>','1');"  title="Resume Merchant" data-toggle="modal"  class="">Resume Merchant</a></li>
                                                <?php } ?>
                                                <li> 
                                                    <a href="#dis_merchant" onclick="set_dis_merchant('<?php  echo $merchant['merchID']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Disable Merchant" class="">Disable Merchant </a>
                                                </li>  
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                        }
                    }  
                ?>
				
			</tbody>
        </table>
                            
    </div>
</div>
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>

$(function(){ Pagination_view.init();


 $('#gateway_old').change(function(){
	
	 
    var gID =  $('#gateway_old').val();

	$.ajax({
    url: '<?php echo base_url("Admin_panel/get_gateway_id")?>',
    type: 'POST',
	data:{gateway_id:gID},
    success: function(data){
		
		$('#t_data').html(data);
		
		var newarray = JSON.parse("[" + data.merchID + "]");
			 for(var val in  newarray) {
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }
		
			
     }	
  });
	
});	
	
 });

 
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merchant_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_merchant_id(id, act){
     var form = $("#del_merchant_form");
    if(act=='0')
    {
	                    	var url   = "<?php echo base_url()?>Admin_panel/suspend_merchant";
									 
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchIDww',
											'name': 'merchID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchIDww',
											'name': 'merchID',
											'value': id
											}).appendTo(form);
									
										 $("#ddeee").val('Suspend')	;							
			$("#sus_txt").html("Suspend Merchant");	
				             $("#del_merchant_form").attr("action",url);
}else{
    
    	var url   = "<?php echo base_url()?>Admin_panel/enable_suspend_merchant";
									
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchIDww',
											'name': 'merchID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchIDww',
											'name': 'merchID',
											'value': id
											}).appendTo(form);
										$("#sus_txt").html("Resume Merchant");	
									    $("#sub_content").html("Are you sure you want to resume this Merchant?");
									  $("#ddeee").val('Yes')	;						
				
				                          $("#del_merchant_form").attr("action",url);
}             

}


function set_dis_merchant(id){
	   
	  $('#merchIDs').val(id);
	
}
</script>	
	
	
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	<!--------------------del admin Merchant------------------------>

<div id="del_merchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="sus_txt">Suspend Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_merchant_form" method="post" action='<?php echo base_url(); ?>Admin_panel/suspend_merchant' class="form-horizontal" >
                     
                 
					<p id="sub_content">Do you really want to suspend this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                          
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  id="ddeee" name="btn_cancel" class="btn btn-sm btn-info" value="Suspend"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<!--  pawan Code --->


<div id="dis_merchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disable Merchant</h2>
            </div>
            <div class="modal-body">
                <form id="del_merchant" method="post" action='<?php echo base_url(); ?>Admin_panel/disable_merchant' class="form-horizontal">
                    <p>Do you really want to disable this Merchant?</p> 
					<div class="form-group">
                      <div class="col-md-8">
                            <input type="hidden" id="merchIDs" name="merchIDs" class="form-control"  value="" />
                        </div>
                    </div>
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Disable" />
                    <button type="button" class="btn btn-sm btn-success close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>






<!-- Pawan code end-->

<div id="Edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Gateway in Bulk</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>Admin_panel/update_gatewayes' class="form-horizontal" >
				 
                     
                 <p> You may update gateways in bulk here.</p> 
				   
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_old" name="gateway_old"  class="form-control">
								   <option value="" >Select Current Gateway</option>
								    <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_new" name="gateway_new"  class="form-control">
								   <option  value="" >Select New Gateway</option>
								   <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel1" name="btn_cancel1" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"  id="btn_cancel" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					    <hr/>
				<tbody id="t_data">
						 
						 </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>
