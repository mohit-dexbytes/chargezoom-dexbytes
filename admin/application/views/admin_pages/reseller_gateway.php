
<!-- Page content -->
<div id="page-content">
    
    
    <!-- Forms General Header -->
   
  
    <!-- END Forms General Header -->
    <div class="row">
        <!-- Form Validation Example Block -->
		<div class="block"> 
		<?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>
		<div class="block-title">
                   <h4><strong>Reseller Gateway</strong></h4>		
		    
		  
					    <div class="block-options pull-right">
					        <?php if(!empty($resellers)){ ?>
                            <div class="btn-options">
                                 <a class="btn btn-sm btn-info" title="Set Default Gateway" data-backdrop="static" data-keyboard="false" data-toggle="modal" href="#set_def_gateway">Set Default Gateway</a>
                               <a href="#add_gateway" class="btn btn-sm btn-success"  onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create New" >Add New</a>
                            </div>
                            <?php } ?>
                        </div> 
					
			</div>		
					
					<!-- All Orders Content -->
        <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="visible-lg text-left">Gateway </th>
					<th class="visible-lg text-left">Gateway Name </th>
					<th class="visible-lg text-left">Username</th>
					<th class="visible-lg text-left">Status</th>
					<th class="text-center"> Action </th>
                </tr>
            </thead>
            <tbody>
			
			
			
			<?php 
				if(isset($gateways) && $gateways)
				{
					foreach($gateways as $gateway)
					{
						
						
				?>
				<tr>
					
					<td class="text-left visible-lg"><?php if($gateway['gatewayType']=='1'){echo "NMI";} else if($gateway['gatewayType']=='2'){echo"Authorize.Net";} else{
					echo"Pay Trace";} ?> </td>
					
					
					<td class="text-left visible-lg"><?php echo $gateway['gatewayFriendlyName']; ?> </a> </td>
					
                    <td class="text-left visible-lg"><?php echo  $gateway['gatewayUsername']; ?> </a> </td>
					<td class="text-left visible-lg"><?php if($gateway['set_as_default']=='1'){echo"Default";} else if($gateway['set_as_default']=='0'){echo"Backup";} else{
					echo"Pay Trace";} ?></td>
					
				<td class="text-center">
					<div class="btn-group btn-group-xs ">
						
						<a href="#edit_gateway" class="btn btn-sm btn-default" onclick="set_edit_gateway('<?php echo $gateway['gatewayID'];  ?>');" title="Edit Details" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>  
              
					
				</div>
					
		   </td>
		</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        </br>
    
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>


<!---------------------Default Gateway Modal------------------>

<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Admin_panel/set_gateway_res_default' class="form-horizontal card_form" >
                     
                 
                    
                                                <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gatewayid" name="gatewayid" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateways) && !empty($gateways) ){
                                                                foreach($gateways as $gateway){
                                                                ?>
                                                           <option value="<?php echo $gateway['gatewayID'] ?>"  <?php if($gateway['set_as_default']=='1'){ echo 'selected'; } ?> ><?php echo $gateway['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                    
                 
                    <div class="pull-right">
                        
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





     <!------------ Add popup for gateway   ------->

 <div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
                
                 
            </div>
            <!-- END Modal Header -->
 
            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_reseller_gateway">
			  
		          	<input type="hidden" id="gatewayID" name="creditID" value=""  />
			        <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Reseller</label>
						 <div class="col-md-6">
						   <select id="resellerID" name="resellerID"  class="form-control">
								   <option value="" >Select Reseller</option>
								   <?php if(!empty($resellers))
								          {  foreach($resellers as $res){   
								          
								          ?>
								          
								          <option value="<?php echo $res['resellerID']; ?>" ><?php echo $res['resellerCompanyName']; ?></option> 
								          <?php } } ?>
								  
							</select>
							</div>
					</div>
		       <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />
								
						</div>
						
					</div>

					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt"  class="form-control">
								   <option value="" >Select Gateway</option>
								   <option value="1" >NMI</option>
								   <option value="2" >Authorize.Net</option>
								  
							</select>
							</div>
					</div>
					
				<div id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="NMI Username..">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">NMI Password</label>
                       <div class="col-md-6">
                            <input type="password" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="NMI Password..">
                           
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                           
                        </div>
                    </div>
				</div>

				<div id="cz_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="nmiUser" class="form-control"  placeholder="Username..">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="password" id="czPassword" name="nmiPassword" class="form-control"  placeholder="Password..">
                           
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
               <div id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username..">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
					
				</div>	
				
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Reseller Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayAdminID" name="gatewayAdminID" class="form-control"  placeholder="Admin ID">
                           
                        </div>
						
                   </div>

                  
	         <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>
					
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
             </div> 
			
	   </form>		
                
            </div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>





<!-------------------------- Modal for Edit Gateway ------------------------------>

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
		
			 <h2 class="modal-title text-center"> Edit Gateway </h2>
			 <hr/>
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>Admin_panel/update_reseller_gateway">
			 
			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />
			
               
		    <div class="col-md-12">   
                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="">
							</div>
			     </div>
          </div>  

		 <div class="col-md-12">   
                    <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>

          </div> 

		  
		  		<div id="nmi_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="NMI Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">NMI Password</label>
                        <div class="col-md-6">
                            <input type="password" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="NMI Password..">
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1"  >
                           
                        </div>
                    </div>
				</div>

				<div id="cz_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser1" name="czUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="password" id="czPassword1" name="czPassword1" class="form-control"  placeholder="Password..">
                        </div>
                    </div>
                    
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1"  >
                           
                        </div>
                    </div>
				</div>

                 <div id="auth_div1" style="display:none">					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
                      <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1"   >
                           
                        </div>
                    </div>
				</div>	
				
			<div id="pay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="PayTrace  Username..">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
				</div>	
				
		         <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Reseller Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="aid"  name="aid" class="form-control"  value="" placeholder=""> 
						</div>
			      </div>
             </div> 
							
	            <div class="form-group">
					<div class="col-md-4 pull-right">
					
					<button type="submit" class="submit btn btn-sm btn-success"> Update </button>
					
					<button  type="button" align="right" class="btn btn-sm  btn-danger close1" data-dismiss="modal"> Cancel </button>
                   
                   </div>
            </div>
	 </form>	
	<!--------- END ---------------->
		</div>

	</div>
 
 </div>

     <!------- Modal for Delete Gateway ------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway" method="post" action='<?php echo base_url(); ?>Admin_panel/delete_gateway' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Gateway ? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="gatewayid" name="gatewayid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Yes"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merch_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();





function del_gateway_id(id){
	
	     $('#gatewayid').val(id);
}


function set_edit_gateway(gatewayid){
	
	
	if(gatewayid !=""){
	
     $.ajax({
		url: '<?php echo base_url("Admin_panel/get_res_gatewayedit_id")?>',
		type: 'POST',
		data:{gatewayid:gatewayid},
		dataType: 'json',
		success: function(data){
		
		 $('#gatewayEditID').val(data.gatewayID);		
			 
			  if(data.gatewayType=='1')
			  {
				  $('#nmi_div1').show();
				  $('#cz_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
		          $('#nmiUser1').val(data.gatewayUsername);
			      $('#nmiPassword1').val(data.gatewayPassword);
			        if(data.echeckStatus==1)
			        {
			        $('#nmi_ach_status1').attr('checked','checked');
			       }
			     else{
			    
			     $('#nmi_ach_status1').removeAttr('checked');
			     }
              }
			  
				if(data.gatewayType=='2')
				{
			      $('#nmi_div1').hide();
				  $('#auth_div1').show();
				  $('#pay_div1').hide();
				  $('#cz_div1').hide();
				  $('#apiloginID1').val(data.gatewayUsername);
			      $('#transactionKey1').val(data.gatewayPassword);
			         if(data.echeckStatus==1)
			     $('#auth_ach_status1').attr('checked','checked');
			     else
			      
			     $('#auth_ach_status1').removeAttr('checked');
			
				}
				
				if(data.gatewayType=='3')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').show();
				  $('#cz_div1').hide();
		          $('#paytraceUser1').val(data.gatewayUsername);
			      $('#paytracePassword1').val(data.gatewayPassword);
				}	

				if(data.gatewayType=='9')
				{
					$('#cz_div1').show();
					$('#nmi_div1').hide();
					$('#auth_div1').hide();
					$('#pay_div1').hide();
					$('#czUser1').val(data.gatewayUsername);
					$('#czPassword1').val(data.gatewayPassword);
					if(data.creditCard=='1'){
						$('#cz_cr_status1').attr('checked','checked');  
					}else{
						$('#cz_cr_status1').removeAttr('checked');    
					}
					if(data.echeckStatus=='1')
					{	
						$('#cz_ach_status1').attr('checked','checked');
					}
					else{
						$('#cz_ach_status1').removeAttr('checked');
					}
				}	
			  
				$('#fname').val(data.gatewayFriendlyName);
				$('#gateway').val(data.gateway);
				$('#aid').val(data.gatewayAdminID);
			 
			 
	}	
  }); 
	
  }

}


$(function(){  
     
	  $('#gateway_opt').change(function(){
		    var gateway_value =$(this).val();
           if(gateway_value=='3'){			
		       $('#pay_div').show();
			   $('#auth_div').hide();
				$('#cz_div').hide();
				$('#nmi_div').hide();
		   }else if(gateway_value=='2'){
				$('#auth_div').show();
				$('#pay_div').hide();
				  $('#cz_div').hide();
				$('#nmi_div').hide();
		   }else if(gateway_value=='9'){			
		       $('#pay_div').hide();
			   $('#auth_div').hide();
				$('#cz_div').show();
				$('#nmi_div').hide();
		   }else{
				  $('#cz_div').hide();
			   $('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').show();		
           }
	  }); 
	  
	  
  $('#nmiform').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			
			  'resellerID':{
				 required: true,
		  },	
		 'gateway_opt':{
				 required: true,
		 },	
		 
		  'nmiUser': {
				required: true,
				minlength: 3
			},
			'nmiPassword':{
				required : true,
				minlength: 5,
			},

			'czUser': {
				required: true,
				minlength: 3
			},
			'czPassword':{
				required : true,
				minlength: 5,
			},
		 
		 
		 
		 
			'apiloginID':{
				required: true,
                 minlength: 3
			},
			'transactionKey':{
				required: true,
                 minlength: 3
			},		 
		 
			'frname':{
				 required: true,
				 minlength: 3
			},	
		 
			'nmiUser1': {
                 required: true,
                 minlength: 3
             },
			'nmiPassword1':{
				 required : true,
				 minlength: 5,
				},
			'czUser1': {
				required: true,
				minlength: 3
			},
			'czPassword1':{
				required : true,
				minlength: 5,
			},
		 
			'apiloginID1':{
				required: true,
                 minlength: 3
			},
			
			'transactionKey1':{
				required: true,
                 minlength: 3
			},
		 
		 
			'paytracePassword':{
				required: true,
                minlength: 5
			},
			
			'paytraceUser':{
				required: true,
                 minlength: 3
			},
		 
			'paytracePassword1':{
				required: true,
                minlength: 5
			},
			'paytraceUser1':{
				required: true,
                minlength: 3
			},
		 
			'fname':{
			 required:true,
			 minlength:2,
			}
		 
			
	 },
  });

  
	$('#nmiform1').validate({ // initialize plugin
		 ignore:":not(:visible)",			
		 rules: {
		    
			
		  'gateway_opt':{
				 required: true,
		  },	
		 
		  'nmiUser': {
                 required: true,
                 minlength: 3
          },
		  
		  'nmiPassword':{
				 required : true,
				 minlength: 5,
		  },

		'czUser': {
			required: true,
			minlength: 3
		},
		'czPassword':{
			required : true,
			minlength: 5,
		},
		 
		  'apiloginID':{
				required: true,
                minlength: 3
		  },
		  
		  'transactionKey':{
				required: true,
                minlength: 3
		  },		 
		 
		  'frname':{
				 required: true,
				 minlength: 3
		  },	
		 
		  'nmiUser1': {
                 required: true,
                 minlength: 3
           },
			
			'nmiPassword1':{
				 required : true,
				 minlength: 5,
		    },

			'czUser1': {
				required: true,
				minlength: 3
			},
			'czPassword1':{
				required : true,
				minlength: 5,
			},
		 
         'apiloginID1':{
				required: true,
                 minlength: 3
		 },
		 
		'transactionKey1':{
				required: true,
                 minlength: 3
		 },
		 
		'paytracePassword':{
				required: true,
                 minlength: 3
		 },
		 
		'paytraceUser':{
				required: true,
                 minlength: 3
		 },
		 
        'paytracePassword1':{
				required: true,
                 minlength: 5
		 },
		 
		'paytraceUser1':{
				required: true,
                 minlength: 3
		 },
		 
		 'fname':{
			 required:true,
			 minlength:2,
		 }
		 
		 
	 },
  });

	
       var GetMyRemote=function(element_name){
	        $('#update_btn').attr("disabled", true); 
            remote={
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Payments/testNMI',
                    type: "post",
					data:
                        {
                            nmiuser: function(){return $('input[name=nmiUser]').val();},
                            nmipassword:  function(){return $('input[name=nmiPassword]').val();}
                        },
                          dataFilter:function(response){
							
						   data=$.parseJSON(response);
                             if (data['status'] === 'true') {
                                  
									 
							    $('#update_btn').removeAttr('disabled');
						     	$('#nmiPassword').removeClass('error');	 
								 $('#nmiPassword-error').hide();	 
								  
									   return data['status'] ;				   
									 
                              } else {
                                
								   if(data['status'] =='false'){
                                       $('#nmiform').validate().showErrors(function(){
										   
                                                       return {key:'nmiPassword'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }
 });
 
</script>


