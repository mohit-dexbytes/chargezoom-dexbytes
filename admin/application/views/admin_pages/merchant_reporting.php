<!-- Page content -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/adminDashboard.css">

<div id="page-content">
	<legend class="leg">Merchant Reports</legend>
	<div class="row ">
      	<div class="col-sm-12">
	        <div class="block full">
	            <div class="card-header py-3 bg-transparent">
	                <h5 class="card-title">Net Revenue Retention (NRR)</h5>
	            </div>
	            <div class="revenueGraph">
	                <div id="nrr_line_chart" style="height:350px;" class="demo-placeholder"></div>
	                <div id="nrr_line_chart_choices" class="legendPosition" ></div>
	                <div id="nrr_legendContainer" class="legend" ></div>
	            </div>
	        </div>
     	</div>
    </div> 

    <div class="row">
      	<div class="col-sm-12">
	        <div class="block full">
	            <div class="card-header py-3 bg-transparent">
	                <h5 class="card-title">Average Revenue Per User/Unit (ARPU)</h5>
	            </div>
	            <div class="revenueGraph">
	                <div id="arpu_line_chart" style="height:350px;" class="demo-placeholder"></div>
	                <div id="arpu_line_chart_choices" class="legendPosition" ></div>
	                <div id="arpu_legendContainer"></div>
	            </div>
	        </div>
     	</div>
    </div>

    <div class="row">
      	<div class="col-sm-12">
	        <div class="block full">
	            <div class="card-header py-3 bg-transparent">
	                <h5 class="card-title">Lifetime Value (LTV)</h5>
	            </div>
	            <div class="revenueGraph">
	                <div id="ltv_line_chart" style="height:350px;" class="demo-placeholder"></div>
	                <div id="ltv_line_chart_choices" class="legendPosition" ></div>
	                <div id="ltv_legendContainer" class="legend" ></div>
	            </div>
	        </div>
     	</div>
    </div>
</div>

<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.tooltip.min.js"></script>
<link href="<?php echo base_url(CSS); ?>/moltran/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />    
<script>
	$(document).ready(function(){

		function format2(num){
			var p = parseFloat(num).toFixed(2).split(".");
			return "$" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
				return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
			}, "") + "." + p[1];
		}
			
		function roundN(num,n){
			return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
		} 
		renderNRRReport();
		function renderNRRReport() {
			var graphData = {
				url: "<?php echo base_url(); ?>uploads/admin_dashboard_chart.json",
				graph_id: "nrr_line_chart",
				choice_id: "nrr_line_chart_choices",
				legend_id: "nrr_legendContainer",
				legend_title: "NRR Report"
			};
			renderReportGraph(graphData);
		};

		renderARPUReport();
		function renderARPUReport() {
			var graphData = {
				url: "<?php echo base_url(); ?>uploads/admin_dashboard_chart.json",
				graph_id: "arpu_line_chart",
				choice_id: "arpu_line_chart_choices",
				legend_id: "arpu_legendContainer",
				legend_title: "ARPU Report"
			};
			renderReportGraph(graphData);
		};

		renderLTVReport();
		function renderLTVReport() {
			var graphData = {
				url: "<?php echo base_url(); ?>uploads/admin_dashboard_chart.json",
				graph_id: "ltv_line_chart",
				choice_id: "ltv_line_chart_choices",
				legend_id: "ltv_legendContainer",
				legend_title: "LTV Report"
			};
			renderReportGraph(graphData);
		};

		function renderReportGraph(graphData) {
			$.ajax({
	          	type:"POST",
	          	url : graphData['url'],
	          	success: function (data){

					sum = 0;

					var color = "#33b86c";
					var legendTitle = graphData['legend_title'];

					if(legendTitle == "NRR Report"){
						x2 = data.nrr_report.arp;
						y2 = data.nrr_report.month;

						var datasetsVolumn = {};
						datasetsVolumn[legendTitle] = {
							data: x2, label: `${legendTitle}`,color ,hoverable:true, shadowSize: 2,clickable: true
						};
					} else if(legendTitle == "ARPU Report"){
						var arpData = data.arp_report;

						y2 = data.arp_report.month;
						color = "#007bff";

						var datasetsVolumn = {
							"Total": {
								data: arpData.total, label: "Total",color:'#7e57c2',hoverable:true, shadowSize: 2, highlightColor: '#007bff',clickable: true
							},
							"Subscriptions": {
								data: arpData.subscription, label: "Subscriptions",color:'#007bff',hoverable:true,clickable: true
							},
							"Basis Points": {
								data: arpData.basis, label: "Basis Points",color:'#33b86c',hoverable:true,clickable: true
							}
						};
					} else if(legendTitle == "LTV Report") {
						x2 = data.ltv_report.arp;
						y2 = data.ltv_report.month;

						var datasetsVolumn = {};
						datasetsVolumn[legendTitle] = {
							data: x2, label: `${legendTitle}`,color ,hoverable:true, shadowSize: 2,clickable: true
						};
					}

					var legendVolumnContainer = document.getElementById(`#${graphData.legend_id}`);

					var i = 1;
					var j = 1;

					var choiceVolumnContainer = $(`#${graphData.choice_id}`);
					var legendContainer = $(`#${graphData.choice_id}`);

					$.each(datasetsVolumn, function(key, val) {
						var checkM = 'checked';
						var classOpacity = '';
						
						choiceVolumnContainer.append("<div class='displayLegend " + classOpacity + " ' id='vcolor" + i + "'><input class='checkboxNoneVolumn' type='checkbox' name='" + key +
							"' " + checkM + " data-id='" + j + "' id='id" + key + "'></input> <label id='vcolor" + j + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + j + "'></label>" +
							"<label class='legendLabel' for='id" + key + "'>"
							+ val.label + "</label> </div>");
						++j;
						
					});

					var volumnData = [];
					choiceVolumnContainer.find("input:checked").each(function () {
						var key = $(this).attr("name");
						if (key && datasetsVolumn[key]) {
							volumnData.push(datasetsVolumn[key]);
						}
					});

					var legendContainer = document.getElementById("");

					choiceVolumnContainer.find("input").click(plotAccordingToChoicesVolumn);

					function plotAccordingToChoicesVolumn() {

						var data = [];
						choiceVolumnContainer.find("input:checked").each(function () {
							var key = $(this).attr("name");
							if(key != 'Revenue'){
								if (key && datasetsVolumn[key]) {
									data.push(datasetsVolumn[key]);
								}
							}else{
								if ($('#idRevenue').prop('checked') == true){
									if (key && datasetsVolumn[key]) {
										data.push(datasetsVolumn[key]);
									}
								}
							}
						});

						if (data.length > 0) {
							$.plot(`#${graphData.graph_id}`, data, {
								series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
								legend: {
									position: "nw",
									margin: [0, -24],
									noColumns: 0,
									backgroundColor: "transparent",
									labelBoxBorderColor: null,
									labelFormatter: function (o, t) {
										return o + "&nbsp;&nbsp;";
									},
									width: 30,
									height: 2,
									container: legendContainer,
									onItemClick: {
										toggleDataSeries: true
									},
								},
								grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
								yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
								xaxis: { ticks: y2,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
								tooltip: !0,
								tooltipOpts: { 
									content: function(data, x, y, dataObject) {
										var XdataIndex = dataObject.dataIndex;
										var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
										return 'Value of '+XdataLabel+' is $'+y;
									},
									shifts: { x: -60, y: 25 },
									defaultTheme: !1 
								},
								
								yaxis: {
									autoScale:"exact"
								}
							}); 
						}
					}
					plotAccordingToChoicesVolumn();
					
	          	}
	      	});    
		};
	});
</script>
<style type="text/css">
	.card-title {
		font-size: 14px;
		text-transform: uppercase;
		font-weight: 600;
		margin: 0;
	}

	#nrr_line_chart  g text tspan, #arpu_line_chart  g text tspan, #ltv_line_chart  g text tspan {
		visibility: visible;
	}
	
	#nrr_line_chart  text tspan, #arpu_line_chart  text tspan, #ltv_line_chart  text tspan, .legend {
		visibility: hidden;
	}

	.legendPosition{
        width: 100%;
        padding: 3% 30% 0% 30%;
        text-align: center;
    }
</style>