<!-- Page content -->

<div id="page-content">
     <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
	           ?>

    <!-- All Orders Block -->
    <div class="block full">
	
	
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Tiers</strong> </h2>
            <div class="block-options pull-right">
                    <div class="btn-group">
                              
                    <a href="#add_tiers" class="btn btn-sm btn-success"  onclick="set_addtier();" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Create Tier">Add New</a>
                            </div>
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="plan_page" class="table table-bordered table-striped table-vcenter">
            <thead class="table_plan">
                <tr>
                    
                    <th class="text-left">Tier Name</th>
                    <th class="text-right hidden-xs">Tier 1</th>
                    <th class="text-right hidden-xs">Tier 2</th>
                    <th class="text-right hidden-xs">Tier 3</th>
                   <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($tiers) && $tiers)
				{
					foreach($tiers as $tier)
					{
				?>
				<tr>
					
					<td class="text-left"><?php echo $tier['tier_name']; ?></td>
					
					<td class="text-right hidden-xs"><?php echo ($tier['tier1_min_new_account'])." / ".number_format($tier['tier1_commission']); ?> %</td>
					
					<td class="text-right hidden-xs"><?php echo ($tier['tier2_min_new_account'])." / ".number_format($tier['tier2_commission']); ?> %</td>
					
					<td class="text-right hidden-xs"><?php echo ($tier['tier3_min_new_account'])." / ".number_format($tier['tier3_commission']); ?> %</td>
					<td class="text-center">
						<div class="btn-group btn-group-xs">
						
							 <a href="#add_tiers" onclick="set_edit_tier('<?php  echo $tier['tier_id']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Edit Tier" class="btn btn-alt btn-sm btn-default"  > <i class="fa fa-edit"></i></a>
							
                            <a href="#del_plan" onclick="set_del_tier('<?php  echo $tier['tier_id']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Tier" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                            
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(document).ready(function(){
    
    
$('#tier_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'tierName': {
                        required: true,
                        minlength: 3,
                         
                    },
                    'tier1_name':{
                        required: true
                    },
                    'tier2_name':{
                        required: true
                    },
                    'tier3_name':{
                        required: true
                    },
                    'tier1_account':{
                        required: true,
                        number:true,
                    },
                    'tier2_account':{
                        required: true,
                        number:true,
                    },
                    'tier3_account':{
                        required: true,
                        number:true,
                    },
                    'tier1_commission':{
                        required: true,
                        number:true,
                    },
                     'tier2_commission':{
                        required: true,
                        number:true,
                    },
                     'tier3_commission':{
                        required: true,
                        number:true,
                    }
			
			},
    });
    
    $(function(){ Pagination_view.init();
 
	
	
	$.validator.addMethod("greaterThan",
    function(value, max, min){
        return parseFloat(value) > parseFloat($(min).val());
    }, "Price must be greater than cost"
);
	$.validator.addMethod("greaterThanplan",
    function(value, max, min){
        return parseFloat(value) > parseFloat($(min).val());
    }, "Price must be greater than cost"
);
	
	
});
});</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#plan_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [6] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_tier(id){
	
	  $('#tier_id').val(id);
	
}

function set_addtier(id){
    
    $('#tier_form').trigger('reset');
    $('#title').text("Add New Tier");
	$('#planName').val(id);
	$('#planDescription').val(id);
	$('#planCost').val(id);
	$('#planPrice').val(id);
	$('#revenueCost').val(id);
	$('#revenuePrice').val(id);

}	
function set_edit_tier(tier_id){
    $('#title').text("Edit Tier");
	$('.update').text("Update")
    $.ajax({
    url: '<?php echo base_url("Admin_panel/get_tier_id")?>',
    type: 'POST',
	data:{tier_id:tier_id},
	dataType: 'json',
    success: function(data){
		  
             $('#tierID').val(data.tier_id);		
			 $('#tierName').val(data.tier_name);
			 $('#tier1_account').val(data.tier1_min_new_account);
			 $('#tier1_commission').val(data.tier1_commission);
			 $('#tier2_account').val(data.tier2_min_new_account);
			 $('#tier2_commission').val(data.tier2_commission);
			 $('#tier3_account').val(data.tier3_min_new_account);
			 $('#tier3_commission').val(data.tier3_commission);
			
			 
	}	
});
	
}

    	
  

</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<!------    Add popup form    ------->

<div id="add_tiers" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
               <h2 class="modal-title" id="title">Tiers</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="tier_form" class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_tier">
			<input type="hidden" id="tierID" name="tierID" value=""  />
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Tier Name</label>
							<div class="col-md-8">
								<input type="text" id="tierName"  name="tierName" class="form-control"  value="" placeholder="Tier Name"><?php echo form_error('tierName'); ?></div>
						</div>
						
						
						

						<div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Minimum Accounts</label>
                           <div class="col-md-8">
                            <input type="text" id="tier1_account"  name="tier1_account" class="form-control"  value="" placeholder="Minimum Accounts"><?php echo form_error('planDescription'); ?></div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Commission (%) </label>
                           <div class="col-md-8">
                            <input type="text" id="tier1_commission"  name="tier1_commission" class="form-control"  value="" placeholder="Commission"><?php echo form_error('planDescription'); ?></div>
                          </div>
                          
                          
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Minimum Accounts</label>
                           <div class="col-md-8">
                            <input type="text" id="tier2_account"  name="tier2_account" class="form-control"  value="" placeholder="Minimum Accounts"><?php echo form_error('planDescription'); ?></div>
                          </div>
                          
                          <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Commission (%)</label>
                           <div class="col-md-8">
                            <input type="text" id="tier2_commission"  name="tier2_commission" class="form-control"  value="" placeholder="Commission"><?php echo form_error('planDescription'); ?></div>
                          </div>
                         
                           <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Minimum Accounts</label>
                           <div class="col-md-8">
                            <input type="text" id="tier3_account"  name="tier3_account" class="form-control"  value="" placeholder="Minimum Accounts"><?php echo form_error('planDescription'); ?></div>
                          </div>
					        
					        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Commission (%)</label>
                           <div class="col-md-8">
                            <input type="text" id="tier3_commission"  name="tier3_commission" class="form-control"  value="" placeholder="Commission"><?php echo form_error('planDescription'); ?></div>
                          </div> 
					        
                       </div>      
		      
			  <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success update">Save</button>
					
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>  
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

</div>
<!-- END Page Content -->

<!--------------------del admin plan------------------------>

<div id="del_plan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Tier</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_plan" method="post" action='<?php echo base_url(); ?>Admin_panel/delete_tier' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this Tier?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="tier_id" name="tier_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>