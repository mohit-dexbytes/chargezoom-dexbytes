<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php
        echo $this->session->flashdata('message');
        echo $this->session->flashdata('error'); 
    ?></div>
    <legend class="leg">Disabled Merchant</legend>
    <div class="block-main full" style="position: relative">
        <table id="reseller_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Merchant</th>
                    <th class="text-left hidden-xs">Name</th>
                    <th class="hidden-xs text-right">Plan</th>
                    <th class="hidden-xs text-right"> Termination Date</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
                    if(isset($merchant_list) && $merchant_list){
                        foreach($merchant_list as $merchant){
                            ?>
                                <tr>
                                    <td class="text-left"><?php echo $merchant['companyName']; ?></td>
                                    <td class="text-left hidden-xs"> <?php echo $merchant['firstName'].' '.$merchant['lastName']; ?></td>
                                    <td class="text-right hidden-xs">
                                        <?php if(isset($merchant['friendlyname']) && $merchant['friendlyname'] != ""){ echo $merchant['friendlyname']; } else{  echo ($merchant['plan_name'])?$merchant['plan_name']:'-----';  } ?>
                                    </td>
                                    <td class="text-right"><span class="hidden"><?php echo $merchant['updatedAt']; ?></span><?php echo date('F d, Y', strtotime($merchant['updatedAt'])); ?></td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-xs">
                                            <a href="#enable_marchant" onclick="set_enable_marchant('<?php  echo $merchant['merchID']?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  title="Enable Marchant" class="btn btn-success">Enable</a>	
                                            <a href="#del_marchant" onclick="set_del_marchant('<?php  echo $merchant['merchID']?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"  title="Delete Marchant" class="btn btn-danger"> <i class="fa fa-times" aria-hidden="true"></i> </a>
                                        </div>
                                    </td>
                                </tr>
                            <?php
                        }
                    }  
                ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
</div>
    <!-- END All Orders Block -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#reseller_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_del_marchant(id){
	 
	  $('#marchID').val(id);
	
}

function set_enable_marchant(id){
	 
	  $('#marchIDs').val(id);
	
}

</script>	
	
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	
	<!--------------------del admin Reseller------------------------>

<div id="del_marchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_reseller" method="post" action='<?php echo base_url(); ?>Admin_panel/delete_marchant' class="form-horizontal" >                     
                 
					<p>Do you really want to Delete this Merchant?</p> 					
				    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="marchID" name="marchID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-success close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





	<!--------------------enable admin Reseller------------------------>

<div id="enable_marchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Enable Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="enable_reseller" method="post" action='<?php echo base_url(); ?>Admin_panel/enable_marchant' class="form-horizontal" >                     
                 
					<p>Do you really want to enable this Merchant?</p> 					
				    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="marchIDs" name="marchIDs" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Enable"  />
                    <button type="button" class="btn btn-sm btn-success close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>











