   <!-- Page content -->
	<div id="page-content">
	      <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
					?>
	 
	    
	
    <!-- END Wizard Header -->
	<legend class="leg"><strong><?php if(isset($user)){ echo "Edit User";}else{ echo "Create New User"; }?> </strong>  </legend>

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	          
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>home/create_user">
			
			 <input type="hidden"  id="userID" name="userID" value="<?php if(isset($user)){echo $user['adminID']; } ?>" /> 
			
			
			   
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="userFname" name="userFname" class="form-control"  value="<?php if(isset($user)){ echo $user['adminFirstName']; } ?>" data-placeholder="First Name"><?php echo form_error('userFname'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="userLname" name="userLname" class="form-control"  value="<?php if(isset($user)){ echo $user['adminLastName']; } ?>" data-placeholder="Last Name"><?php echo form_error('userLname'); ?> </div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Company Name </label>
					<div class="col-md-6">
					<input type="text" id="userCompany" name="userCompany" class="form-control"  value="<?php if(isset($user)){ echo $user['adminCompanyName']; } ?>" data-placeholder="Company Name"><?php echo form_error('userCompany'); ?> </div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email Address </label>
					<div class="col-md-6">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($user)){ echo $user['adminEmail']; } ?>" data-placeholder="Email address"></div>
					</div>
					
					<?php if(!isset($user)) { ?>
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Password </label>
					<div class="col-md-6">
					<input type="password" id="userPassword" name="userPassword" class="form-control" value="" data-placeholder="Password"> </div>
					</div>
					<?php } ?>
					
				<div class="col-md-12">   
                  	<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Select Powers</label>
						
							<div class="col-md-7">	<?php    foreach ($auths as $key=>$auth ){  ?>     
						   
					  
					   <div class="col-md-6 form-group">
					   <label class="control-label" for="example-username">  </label>
					  
					   <input type="checkbox" name="role[]" id="role<?php echo $key; ?>" <?php if(isset($user) && !empty($authvals) && in_array($auth['authID'],$authvals) ){ echo "Checked"; } ?>  value="<?php echo $auth['authID']; ?>" >  <?php echo $auth['authName'] ; ?>
					  </div>
					  
					  
					  <?php } ?></div>
							 <br>
							  <div class="sele"></div>
					</div>
               </div>      
						
					
                    <div class="form-group">
					<label class="col-md-4 control-label" for="example-username"></label>
					<div class="col-md-6">
					    <div class="pull-right">
					        	<?php if(!isset($user)) {?>				
            					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
            					<?php } else {?>
            					<?php /*<a href="#" class="submit btn btn-sm btn-danger">Reset Password</a> */ ?>
            					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
            					<?php }?>
					    </div>
					</div>
                  				
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->
</div>

<script>
$(document).ready(function(){

 
    $('#role_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		         'userFname': {
                        required: true,
                        minlength: 2,
                        maxlength: 100,
                        validate_char:true, 
                    },
                    'userLname': {
                        required: true,
                        minlength:2,
                        maxlength: 100,
                        validate_char:true,
                    },
					
			    	'userEmail': {
                        required: true,
                        email: true,
                         
                    },
                    'userPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'userCompany': {
                        required: true,
                        minlength:3,
                      
                        validate_char:true,
                    },
                    'role[]': {
                        required: true,
                        minlength:1,
                    
                    }
			
			},
			
	errorPlacement: function (error, element) {
    if (element.attr("type") == "checkbox") {
         error.appendTo('.sele');
    } else {
          error.insertAfter(element);
    }
    }
    });
		
	     
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
 });	
</script>




