    <!-- Page content -->
<div id="page-content">
	  
<style> 
.error{color:red; }

#Plans_chosen .chosen-choices {
    border-color: #e8e8e8!important;
    height: 44px !important;
    font-size: 14px;
    overflow: auto;
}
#Plans_chosen .search-field{
	padding-top: 4px;
}
#portal_url{
	width: 64%;
}
.domainName {
    position: relative;
    font-size: 0;
    white-space: nowrap;
    top: -47px;
    float: right;
    z-index: 999;
}

</style>   
<?php
echo  $this->session->flashdata('message');


$rs_pass = base_url().'Admin_panel/recover_Reseller_pwd/';

?> 
	<div class="row">
	    <!-- END Wizard Header -->
	    <div class="col-md-12">
	    	<!-- Contact section  -->
	    	<legend class="leg">
				<strong>Contact </strong>
			</legend>
			<form method="POST" id="reseller_form"  class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_reseller" enctype="multipart/form-data" >
			<input type="hidden"  id="resellerID" name="resellerID" value="<?php if(isset($reseller)){echo $reseller['resellerID']; } ?>" /> 

				<div class="block">
					<fieldset>
						<div class= "col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Company Name <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<input type="text" id="resellerCompanyName" name="resellerCompanyName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerCompanyName']; } ?>" data-title="Company Name"><?php echo form_error('resellerCompanyName'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">First Name <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerfirstName']; } ?>" data-title="First Name"><?php echo form_error('firstName'); ?>
								</div>
							</div>

							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Last Name <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['lastName']; } ?>" data-title="Last Name"><?php echo form_error('lastName'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Primary Phone Number</label>
								<div class="col-md-8">
									<input type="text" id="primaryContact" name="primaryContact" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['primaryContact']; } ?>" data-title="Primary Phone Number"><?php echo form_error('primaryContact'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Email Address <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<input type="text" id="resellerEmail" name="resellerEmail" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerEmail']; } ?>" data-title="Email Address" ><?php echo form_error('resellerEmail'); ?>
								</div>
							</div>
							<?php if(!isset($reseller)) { ?>
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Password <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<input type="password" id="resellerPassword" name="resellerPassword" class="form-control" value="<?php if(isset($reseller)){ echo $reseller['resellerPassword']; } ?>" data-title="Password"> <?php echo form_error('resellerPassword'); ?>
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Confirm Password <span class="text-danger"><strong>*</strong></span> </label>
								<div class="col-md-8">
									<input type="password" id="confirmPassword" name="confirmPassword" class="form-control"  data-title="Confirm Password"> 
								</div>
							</div>
							<?php } ?>


						</div>
						<div class= "col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label" for="resellerAddress">Address Line 1 </label>
								<div class="col-md-8">
									<input type="text" id="resellerAddress" name="resellerAddress" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerAddress']; } ?>" data-title="Address Line 1"><?php echo form_error('resellerAddress'); ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="resellerAddress2">Address Line 2  </label>
								<div class="col-md-8">
									<input type="text" id="resellerAddress2" name="resellerAddress2" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerAddress2']; } ?>" data-title="Address Line 2"><?php echo form_error('resellerAddress2'); ?>
								</div>
							</div>
							<div class="form-group">
							   <label class="col-md-4 control-label" for="resellerCountry">Country</label>
							   <div class="col-md-8">
									<input type="text" id="country" class="form-control " name="resellerCountry" value="<?php if(isset($reseller)){ echo $reseller['resellerCountry']; } ?>" data-title="Country" >
								
								</div>
	                        </div>	
	                        <div class ="form-group">
							   <label class="col-md-4 control-label"  name="state" for="example"> State</label>
							   <div class="col-md-8">
									<input type="text" id="state" name="resellerState" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerState']; } ?>" data-title="State"><?php echo form_error('state_name'); ?>
								</div>
	                        </div>
	                        <div class ="form-group">
							   <label class="col-md-4 control-label" name="city" >City</label>
							   <div class="col-md-8">
							       	<input type="text" id="city" name="resellerCity" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerCity']; } ?>" data-title="City"><?php echo form_error('city_name'); ?>
								</div>
	                        </div>
	                        <div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Zip Code </label>
								<div class="col-md-8">
									<input type="text" id="zipCode" name="zipCode" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['zipCode']; } ?>" data-title="Zip Code"><?php echo form_error('zipCode'); ?></div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="example-username">Federal Tax ID </label>
								<div class="col-md-8">
									<input type="text" id="federalTaxID" name="federalTaxID" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['federalTaxID']; } ?>" data-title="Federal Tax ID"><?php echo form_error('federalTaxID'); ?>
								</div>
							</div>

						</div>
					</fieldset>
				</div>
				<!-- Account section  -->
				<legend class="leg">
					<strong>Account </strong>
				</legend>
				<div class="block">
					<fieldset>
						<div class= "col-md-6">
							<div class ="form-group">
							   	<label class="col-md-4 control-label" for="example-chosen-multiple">Select Plans</label>
							   	<div class="col-md-8">
							    	<select id="Plans" name="Plans[]" class="select-chosen planField" data-placeholder=" "  multiple>
										
										<?php foreach($plan_list as $plan){  ?>
										<?php   $select_plan = explode(',',$reseller['plan_id']);  ?>
										   <option value="<?php echo $plan['plan_id']; ?>"<?php if(isset($reseller)){  echo in_array($plan['plan_id'], $select_plan)?'selected' : ''; }?> > <?php echo $plan['plan_name']; ?> </option>
										   
										<?php } ?>  
									</select>
									<?php echo form_error('Plans'); ?>
								</div>
	                        </div>

	                        
		                    <div class="form-group">
		                        <label class="col-md-4 control-label" for="isTestmode">Test Reseller</label>
		                        <div class="col-md-8">
		                            <input type="checkbox" id="isTestmode" name="isTestmode" style="margin-top: 11px;" value='1' <?php if(isset($reseller) && $reseller['isTestmode']){ echo 'checked'; } ?>>
		                        </div>
		                    </div>
						</div>
						<div class= "col-md-6">
							<div class="form-group">
		                        <label class="col-md-4 control-label" for="customer_help_text">Chat</label>
		                        <div class="col-md-8">
			                        <textarea id="merchant_chat" name="resellerChat"  class="form-control" data-title="Enter the text"> <?php if(isset($reseller) && !empty($reseller) && $reseller['Chat']!=''){ echo $reseller['Chat']; } ?></textarea>
		                                            
		                     	</div>
		                    </div>
		                    <div class="form-group">
		                        <label class="col-md-4 control-label" for="commission">Commission</label>
		                        <div class="col-md-8">
		                            
		                            <input type="text" maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  id="commission" name="commission" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['commission']; }else{ echo 0; } ?>">
		                        </div>
		                    </div>
						</div>
					</fieldset>
				</div>

				<!-- Branding section  -->
				<legend class="leg">
					<strong>Branding </strong>
				</legend>
				<div class="block">
					<fieldset>
						<div class= "col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label" for="portal_url">Portal URL <span class="text-danger"><strong>*</strong></span></label>
								<div class="col-md-8">
									<div class="input-groups">
										<input type="text" id="portal_url" name="portal_url" value="<?php if(isset($setting) && isset($setting['portalprefix'])){ echo $setting['portalprefix']; } ?>" class="form-control" data-title="">
										<span class="domainName" ><a class="custom-btn btn btn-primary" style="padding: 11px;">.<?php echo RSDOMAIN ;?></a> </span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-4 control-label" for="pictureportal">Portal Logo</label>
								<div class="col-md-8">
									<label class="btn btn-default" for="pictureportal">
										<input type="file" id="pictureportal" name="pictureportal" class="upload" >
										<i class="fa_icon icon-upload-alt margin-correction"></i>
									</label>
										<?php if(isset($reseller) && !empty($reseller['ProfileURL'])){ ?>
											<img src="<?php if(isset($reseller) && !empty($reseller['ProfileURL'])){echo $reseller['ProfileURL']; } ?>" height="50px" width="200px">
										<?php 	}?>
											
										
									
								</div>
							</div>
						</div>
						<div class= "col-md-6">
							<div class="form-group">
								<label class="col-md-4 control-label" for="picture">Invoice / Login Logo</label>
								<div class="col-md-8">
									<label class="btn custom-btn btn-default" for="upload_logo">
										<input type="file" id="upload_logo" name="picture" class="upload" >
										<i class="fa_icon icon-upload-alt margin-correction"></i>
									</label>
									<?php if(isset($reseller) && !empty($reseller['resellerProfileURL'])){ ?>
										<img src="<?php if(isset($reseller) && !empty($reseller['resellerProfileURL'])){echo $reseller['resellerProfileURL']; }?>" height="50px" width="200px">
									<?php 	}?>

								</div>
							</div>	
						</div>
						<div class="col-md-12">
							<div class="form-group pull-right">
								<div class="col-md-12">
								
									<?php if(!isset($reseller)) {?>				
									<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
									<?php } else {?>
									<a href="<?php echo $rs_pass.$reseller['resellerID']; ?>" class="btn btn-sm btn-info ">Reset Password</a>
									<button type="submit" class="submit btn btn-sm btn-success">Update</button>					
									<?php }?>
									<a href="<?php echo base_url('Admin_panel/reseller_list'); ?>" class="btn btn-sm btn-default newCloseButton">Cancel</a>
								</div>
								
						    </div>
						</div>	
					</fieldset>
				</div>

  			</form>

	    </div>   
    </div>   
       
              
    
		
</div>
        <!-- END Progress Bar Wizard Content -->  
<!-- END Page Content -->


<script>
$(document).ready(function(){

 
    $('#reseller_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			   'resellerCompanyName': {
                        required: true,
                        minlength: 3,
                        validate_char:true,
                         
                    },
		       'firstName': {
                        required: true,
                        minlength: 2,
                        validate_char:true, 
                    },
                    'lastName': {
                        required: true,
                        minlength:2,
                        validate_char:true,
                    },
					'primaryContact': {
                       
                    },
					
				'resellerEmail': {
                        required: true,
                        email: true,
						  
                         
                    },
                    'resellerPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'confirmPassword': {
                        required: true,
                        equalTo: '#resellerPassword',
                    },
					'billingContact': {
                        phoneUS:true,
                       
                    },
					'billingEmailAddress': {
                        required: true,
                        email: true,
                    },
					'federalTaxID': {
                        
                    },
                    'country': {
                        validate_char:true,
                        
                    },
                    'state': {
                        validate_char:true,
                    },
                    'city': {
                        validate_char:true,
                    },
                    'resellerAddress': {
                        minlength:5,
                        validate_addre:true,
                    },
				
                    'zipCode': {
                      
                      ZIPCode:true,
                        
                    },
					'Plans': {
                        required: true,
                        
                    },
                    'Tiers': {
                        required: true,
                        
                    },
                    'routNumber':{
                        required: true, 
                        echeckRouteNumber: true,
                    },
                      'accountNumber':{
                        required: true, 
                        number:true,
                        echeckAccountNumberMax: true,
                        echeckAccountNumberMin: true,
                    },
                      'accountName':{
                        required: true, 
                    },
                    'payOption':{
                    },
                   'payEmail':{
                        required: true,
                        email:true,
                    },
               
                  'acct_holder_type':{
                        required: true,
                    },  
                'acct_type':{
                        required: true,
                    },         
			     'cardNumber':{
                        required: true,
                        minlength:14,
                        maxlength:17,
                    },
                   'cvv':{
                        required: true,
                        minlength:3,
                        maxlength:4,
                    }, 
                    'expiry':{
                        required: true,
                    },
                 'expiry_year':{
                        required: true,
                         CCExp: {
								month: '#expiry',
								year: '#expiry_year'
						  }
                    },
               'billingfirstName':{
                        required: true,
                        minlength:2,
                         validate_char:true,
                    },
                 'billinglastName':{
                        required: true,
                    },
                 'billingPostalCode':{
                    }, 
                'billingCity':{
                    },
                 'billingState':{
                    }, 
                'portal_url':{
					 required : true,
					 minlength: 3,
					 "remote" :function(){return Chk_url('portal_url');} 
				}			        
			},
    });

    
    $.validator.addMethod("echeckAccountNumberMin", function(value, element) {
        var opt = $('select#payOption').children("option:selected").val();
        var accountNumber = $('#accountNumber').val();

        var rtValue = true;
        if(opt == 3 && accountNumber.length < 9){
            rtValue = false;
        }
        return rtValue;
    },"Please enter atleast 9 digits." );

    $.validator.addMethod("echeckAccountNumberMax", function(value, element) {
        var opt = $('select#payOption').children("option:selected").val();
        var accountNumber = $('#accountNumber').val();

        var rtValue = true;
        if(opt == 3 && accountNumber.length > 17){
            rtValue = false;
        }
        return rtValue;
    },"Maximum 17 digits are allowed" );

    $.validator.addMethod("echeckRouteNumber", function(value, element) {
        var opt = $('select#payOption').children("option:selected").val();
        var routNumber = $('#routNumber').val();

        var rtValue = true;
        if(opt == 3 && routNumber.length != 9){
            rtValue = false;
        }
        return rtValue;
    },"Route number should be 9 digits only." );

    /***************Credit Card Validation************/
        $.validator.addMethod('CCExp', function(value, element, params) {
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');
	/***************Phone Number Validation************/
	$.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
           return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
    	/***************Zipcode Number Validation************/                        
     $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
         
     	/***************String Sanitizatino Validation************/       
       $.validator.addMethod("validate_char", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-._ ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hyphen or underscore.");
    
    	/***************Address Validation************/   
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-._/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
   
   
	
$('#payOption').change(function(){
    
    getAddressDetails();
    
    var opt = $(this).val();
   if(opt=="2")
   {
       $('#chk_div').css('display','none');
       $('#crd_div').css('display','block');
       $('#bill_div').css('display','block');
        $('#email_div').css('display','none');
       
   }
   else if(opt=="3")
   {
       $('#chk_div').css('display','block');
       $('#bill_div').css('display','block');
       $('#crd_div').css('display','none'); 
          $('#email_div').css('display','none');
   }else{
        $('#chk_div').css('display','none');
       $('#crd_div').css('display','none');  
       $('#bill_div').css('display','none');
       $('#email_div').css('display','block');
   }
});	
	
	
});	   

var Chk_url=function(element_name){ 
       
    remote={
		
		     beforeSend:function() { 
				 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
				
			 },
			 complete:function() {
				   $(".overlay1").remove();
			 },
             url: '<?php echo base_url(); ?>ajaxRequest/check_url',
            type: "post",
			data:
                {
                    portal_url: function(){return $('input[name=portal_url]').val();},
                    resellerID: function(){return $('input[name=resellerID]').val();},
                },
                dataFilter:function(response){
				   data=$.parseJSON(response);
				
                     if (data['status'] == 'true') {
				     	$('#portal_url').removeClass('error');	 
						 $('#portal_url-error').hide();	 
							   return data['status'] ;				   
							 
                      } else {
                       
						   if(data['status'] =='false'){
						      
                               
						   }					   
                       }
					   
                       return JSON.stringify(data[element_name]);
                 }
            }
                                     
    return remote;
}		

</script>
<script>


function getAddressDetails()
{
    var email = $('#resellerEmail').val();
    var firstName = $('#firstName').val();
    var lastName = $('#lastName').val();
    var primaryContact = $('#primaryContact').val();
    var resellerAddress = $('#resellerAddress').val();
    var zipCode = $('#zipCode').val();
    var city = $('#city').val();
    var state = $('#state').val();
    
    
    $('#billingEmailAddress').val(email);   
    $('#billingfirstName').val(firstName);   
    $('#billinglastName').val(lastName);   
    $('#billingContact').val(primaryContact);   
    $('#billingAddress1').val(resellerAddress);   
    $('#billingPostalCode').val(zipCode);   
    $('#billingCity').val(city);   
    $('#billingState').val(state);   
 
}
</script>


