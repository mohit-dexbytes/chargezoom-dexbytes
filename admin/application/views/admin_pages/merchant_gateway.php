<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php
        $message = $this->session->flashdata('message');
        if (isset($message) && $message != "") {
            echo mymessage($message);
        }
        ?>
                          <?php
        $error = $this->session->flashdata('error');
        if (isset($error) && $error != "") {
            echo getmessage($error);
        } 
    ?></div>
	<div class="row">
        <div class="col-md-12 padding-bottom-25px">
			<legend class="leg">Admin Gateway</legend>
			<div class="block-main full" style="position: relative">
				<table id="merch_page_admin" class="table table-bordered table-striped table-vcenter">
					<thead>
						<tr>
							<th class="text-left">Gateway </th>
							<th class="text-left">Gateway Name </th>
							<th class="hidden-xs text-left">User Name</th>
							<th class="hidden-xs text-left">Function</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
						
							if(isset($admin_gateways) && $admin_gateways){
								foreach($admin_gateways as $admin_gateway){
									?>
										<tr>
											<td class="text-left ">
												<a style="color: #4d73e0;" href="#edit_gateway" class="" onclick="set_edit_gateway('<?php echo $admin_gateway['gatewayID']; ?>');" title="Edit Details" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $admin_gateway['gatewayFriendlyName']; ?></a>
											</td>

											<td class="text-left ">
												<?php echo  getGatewayNames($admin_gateway['gatewayType']); ?>
											</td>

											<td> <?php echo $admin_gateway['gatewayUsername']; ?></td>

											<td class="text-left hidden-xs"><?php if ($admin_gateway['creditCard'] == '1' && $admin_gateway['echeckStatus'] == '1') {
													echo "CC / EC";
												} else if ($admin_gateway['creditCard'] == '1') {
													echo " CC ";
												} else if ($admin_gateway['echeckStatus'] == '1') {
													echo " EC ";
												} else {
													echo " CC ";
												}
												?>
											</td>
										</tr>
									<?php
								}
							}  
						?>
						
					</tbody>
				</table>
				<!--END All Orders Content-->
			</div>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12 padding-bottom-25px">
			<legend class="leg">Gateway</legend>
			<div class="block-main full" style="position: relative">
				<table id="merch_page" class="table table-bordered table-striped table-vcenter">
					<thead>
						<tr>
							<th class="text-left">Gateway </th>
							<th class="text-left">Gateway Name </th>
							<th class="hidden-xs text-left">User Name</th>
							<th class="hidden-xs text-left">Function</th>
						</tr>
					</thead>
					<tbody>
					
						<?php 
						
							if(isset($gateways) && $gateways){
								foreach($gateways as $gateway){
									?>
										<tr>
											<td class="text-left ">
												<a style="color: #4d73e0;" href="#edit_gateway" class="" onclick="set_edit_gateway('<?php echo $gateway['gatewayID']; ?>');" title="Edit Details" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $gateway['gatewayFriendlyName']; ?></a>
											</td>

											<td class="text-left ">
												<?php echo  getGatewayNames($gateway['gatewayType']); ?>
											</td>

											<td> <?php echo $gateway['gatewayUsername']; ?></td>

											<td class="text-left hidden-xs"><?php if ($gateway['creditCard'] == '1' && $gateway['echeckStatus'] == '1') {
													echo "CC / EC";
												} else if ($gateway['creditCard'] == '1') {
													echo " CC ";
												} else if ($gateway['echeckStatus'] == '1') {
													echo " EC ";
												} else {
													echo " CC ";
												}
												?>
											</td>
										</tr>
									<?php
								}
							}  
						?>
						
					</tbody>
				</table>
				<!--END All Orders Content-->
			</div>
		</div>
	</div>
</div>


<div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/create_gateway">

			<input type="hidden" id="gatewayID" name="creditID" value=""  />

		       <div class="form-group ">

				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />

						</div>

					</div>

					<div class="form-group ">

						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						     <select id="gateway_opt" name="gateway_opt"  class="form-control">

								   <option value="" >Select Gateway</option>
								   <?php foreach ($all_gateway as $gat_data) {
    echo '<option value="' . $gat_data['gateID'] . '" >' . $gat_data['gatewayName'] . '</option>';
}
?>

							</select>
							</div>
					</div>

		    	<div id="nmi_div" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="NMI Username">
                        </div>
                   </div>

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">NMI Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="NMI Password">

                        </div>
                    </div>
                    <div class="form-group" style="display:none">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checcked" value='1'>

                        </div>
                    </div>
                    <div class="form-group" style="display:none">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status" value='1' >
                        </div>
                    </div>
				</div>

				<div id="cz_div" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  placeholder="Password">

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checcked" value='1' >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status" value='1' >

                        </div>
                    </div>
				</div>

                <div id="auth_div" style="display:none">

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID">
                        </div>
                    </div>

				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key">

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checcked" value='1'>

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status" value='1' >

                        </div>
                    </div>
			 </div>


				<div id="pay_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password">

                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_cr_status" name="paytrace_cr_status" checked="checcked" value='1'>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_ach_status" name="paytrace_ach_status" value='1' >
                        </div>
                    </div>

				</div>

				<div id="paypal_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="Enter  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="Enter  password">

                        </div>
                    </div>

					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="Enter  Signature">

                        </div>
                    </div>

				</div>

				<div id="stripe_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="Publishable Key">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="Secret API Key">

                        </div>
                    </div>

				</div>

				 <div id="usaepay_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">

                        </div>
                    </div>
				</div>
				 <div id="heartland_div" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key">

                        </div>
                    </div>
				</div>


				<div id="cyber_div" style="display:none" >
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  placeholder="Enter  MerchantID">

                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" placeholder="Enter API Key ID">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  placeholder="Enter  Secret Key">

                        </div>
                    </div>



				</div>



                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Admin ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayAdminID" name="gatewayAdminID" class="form-control"  placeholder="Admin ID">

                        </div>

                   </div>

                    <div class="form-group">
						<label class="col-md-4 control-label" for="reference"></label>
                         <div class="col-md-6">
							<input type="checkbox" id="setdefaultgateway" name="setdefaultgateway" class="set_checkbox" /> Set as Default Gateway
						</div>
					</div>


	         <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>

                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                    </div>
             </div>

	   </form>

            </div>

            <!-- END Modal Body -->
        </div>
     </div>

  </div>




<!---------------------Default Gateway Modal------------------>

<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>


            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Admin_panel/set_gateway_default' class="form-horizontal card_form" >



                                                <div class="form-group ">

                                                <label class="col-md-4 control-label">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gatewayid" name="gatewayid" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if (isset($gateways) && !empty($gateways)) {
    foreach ($gateways as $gateway) {
        ?>
                                                           <option value="<?php echo $gateway['gatewayID'] ?>"  <?php if ($gateway['set_as_default'] == '1') {echo 'selected';}?> ><?php echo $gateway['gatewayFriendlyName'] ?></option>
                                                                <?php }}?>
                                                    </select>

                                                </div>
                                            </div>



                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">

                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>






<!-------------------------- Modal for Edit Gateway ------------------------------>


<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			 <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway</h2> </div>

            	<div class="modal-body">
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/update_gateway">

			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />


                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="">
							</div>
			     </div>

                    <div class="form-group ">

						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>


				<div id="nmi_div1" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="NMI Username..">
                        </div>
                   </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">NMI Password</label>
                        <div class="col-md-6">
                            <input type="text" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="NMI password..">

                        </div>
                    </div>

                    <div class="form-group" style="display:none">
                        <label class="col-md-4 control-label" for="nmi_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status1" name="nmi_cr_status1" checked="checked" value='1'>

                        </div>
                    </div>
					<div class="form-group" style="display:none">
                        <label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1" value='1' >

                        </div>
                    </div>
				</div>

				<div id="cz_div1" style="display:none">
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser1" name="czUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="czPassword1" name="czPassword1" class="form-control"  placeholder="Password..">

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status1" name="cz_cr_status1" checked="checked" value='1' >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1" value='1' >

                        </div>
                    </div>
				</div>

                 <div id="auth_div1" style="display:none">

					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="Transaction Key..">

                        </div>
                    </div>
                                        <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status1" name="auth_cr_status1" checked="checked" value='1' >

                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1" value='1'  >

                        </div>
                    </div>


				</div>

				<div id="pay_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="PayTrace  Username..">
                        </div>
                  	</div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="PayTrace  password..">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_cr_status1" name="paytrace_cr_status1" checked="checcked" value='1' >

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="paytrace_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="paytrace_ach_status1" name="paytrace_ach_status1" value='1' >
                        </div>
                    </div>
				</div>


				<div id="paypal_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser1" name="paypalUser1" class="form-control" placeholder="Enter  Username">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword1" name="paypalPassword1" class="form-control"  placeholder="Enter  password">

                        </div>
                    </div>

					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature1" name="paypalSignature1" class="form-control"  placeholder="Enter  Signature">

                        </div>
                    </div>

				</div>

				<div id="cyber_div1" style="display:none" >
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID1" name="cyberMerchantID1" class="form-control"  placeholder="Enter  MerchantID">

                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber1" name="apiSerialNumber1" class="form-control" placeholder="Enter API Key ID">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey1" name="secretKey1" class="form-control"  placeholder="Enter  Secret Key">

                        </div>
                    </div>



				</div>

				<div id="stripe_div1" style="display:none" >


					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser1" name="stripeUser1" class="form-control" placeholder="Publishable Key..">
                        </div>

                    </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword1" name="stripePassword1" class="form-control"  placeholder="Secret API Key..">

                        </div>
                    </div>

				</div>

				<div id="usaepay_div1" style="display:none" >
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey1" name="transtionKey1" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>

				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin1" name="transtionPin1" class="form-control"  placeholder="USAePay PIN">

                        </div>
                    </div>
				</div>

				<div id="heartland_div1" style="display:none" >

				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey1" name="heartpublickey1" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey1" name="heartsecretkey1" class="form-control"  placeholder="Secret Key">

                        </div>
                    </div>
				</div>


                 <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Admin ID</label>
					<div class="col-md-6">
						<input type="text" id="aid"  name="aid" class="form-control"  value="" placeholder="">
					</div>
		       </div>


	            <div class="form-group">
					<div class="col-md-4 pull-right">

					<button type="submit" class="submit btn btn-sm btn-success"> Update </button>

					<button  type="button" align="right" class="btn btn-sm  btn-danger close1" data-dismiss="modal"> Cancel </button>

                   </div>
            </div>
	 </form>
	 </div>
	<!--------- END ---------------->
		</div>

	</div>

 </div>


     <!------- Modal for Delete Gateway ------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>


            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway" method="post" action='<?php echo base_url(); ?>Admin_panel/delete_gateway' class="form-horizontal" >


					<p> Do you really want to delete this Gateway? </p>

				    <div class="form-group">

                        <div class="col-md-8">
                            <input type="hidden" id="gatewaydelID" name="gatewaydelID" class="form-control"  value="" />
                        </div>
                    </div>


                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Yes"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />

			    </form>

            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script>
	$(function(){
		Pagination_view.init('merch_page');
		Pagination_view.init('merch_page_admin');
	});
</script>
<script>

var Pagination_view = function() {

    return {
        init: function(table_id) {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $(`#${table_id}`).dataTable({
				bFilter: false, bInfo: false,bPaginate: false,
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [-1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
			$(`#${table_id}_wrapper [class='row']`).addClass('hide');
        }
    };
}();

function del_gateway_id(id){
    $('#gatewaydelID').val(id);
}

function set_edit_gateway(gatewayid){


	if(gatewayid !=""){
		
     $.ajax({
		url: '<?php echo base_url("home/get_gatewayedit_id") ?>',
		type: 'POST',
		data:{gatewayid:gatewayid},
		dataType: 'json',
		success: function(data){

		$('#gatewayEditID').val(data.gatewayID);

		if(data.gatewayType=='1')
		{
			$('#nmi_div1').show();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#cz_div1').hide();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#nmiUser1').val(data.gatewayUsername);
			$('#nmiPassword1').val(data.gatewayPassword);
			if(data.creditCard=='1'){
				$('#nmi_cr_status1').attr('checked','checked');  
			}else{
				$('#nmi_cr_status1').removeAttr('checked');    
			}
	
			if(data.echeckStatus=='1'){
				$('#nmi_ach_status1').attr('checked','checked');
			} else {
				$('#nmi_ach_status1').removeAttr('checked');
			}
		}

		if(data.gatewayType=='2')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').show();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#cz_div1').hide();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#apiloginID1').val(data.gatewayUsername);
			$('#transactionKey1').val(data.gatewayPassword);
			if(data.creditCard==1){
				$('#auth_cr_status1').attr('checked','checked');  
			}else{
				$('#auth_cr_status1').removeAttr('checked');    
			}
			if(data.echeckStatus==1)
				$('#auth_ach_status1').attr('checked','checked');
			else
				$('#auth_ach_status1').removeAttr('checked');
		}

		if(data.gatewayType=='3')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').show();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#cz_div1').hide();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#paytraceUser1').val(data.gatewayUsername);
			$('#paytracePassword1').val(data.gatewayPassword);
			if(data.creditCard==1){
				$('#paytrace_cr_status1').attr('checked','checked');  
			}else{
				$('#paytrace_cr_status1').removeAttr('checked');    
			}
			if(data.echeckStatus==1){
				$('#paytrace_ach_status1').attr('checked','checked');
			}
			else {
				$('#paytrace_ach_status1').removeAttr('checked');
			}
		}

		if(data.gatewayType=='4')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').show();
			$('#stripe_div1').hide();
			$('#cz_div1').hide();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#paypalUser1').val(data.gatewayUsername);
			$('#paypalPassword1').val(data.gatewayPassword);
			$('#paypalSignature1').val(data.gatewaySignature);
		}

		if(data.gatewayType=='5')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').show();
			$('#cz_div1').hide();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#stripeUser1').val(data.gatewayUsername);
			$('#stripePassword1').val(data.gatewayPassword);
		}
		if(data.gatewayType=='6')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#usaepay_div1').show();
			$('#heartland_div1').hide();
			$('#cyber_div1').hide();
			$('#cz_div1').hide();
			$('#transtionKey1').val(data.gatewayUsername);
			$('#transtionPin1').val(data.gatewayPassword);
		}

		if(data.gatewayType=='7')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#usaepay_div1').hide();
			$('#heartland_div1').show();
			$('#cyber_div1').hide();
			$('#cz_div1').hide();
			$('#heartpublickey1').val(data.gatewayUsername);
			$('#heartsecretkey1').val(data.gatewayPassword);
		}

		if(data.gatewayType=='8')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#usaepay_div1').hide();
			$('#heartland_div1').hide();
			$('#cyber_div1').show();
			$('#cz_div1').hide();
			$('#cyberMerchantID1').val(data.gatewayUsername);

			$('#apiSerialNumber1').val(data.gatewayPassword);
			$('#secretKey1').val(data.gatewaySignature);
		}

		if(data.gatewayType=='9')
		{
			$('#nmi_div1').hide();
			$('#auth_div1').hide();
			$('#pay_div1').hide();
			$('#paypal_div1').hide();
			$('#stripe_div1').hide();
			$('#cz_div1').show();
			$('#usaepay_div1').hide();
			$('#cyber_div1').hide();
			$('#heartland_div1').hide();
			$('#czUser1').val(data.gatewayUsername);
			$('#czPassword1').val(data.gatewayPassword);

			if(data.creditCard=='1'){
				$('#cz_cr_status1').attr('checked','checked');  
				
			}else{
				$('#cz_cr_status1').removeAttr('checked');    
			}
			if(data.echeckStatus=='1')
			{	
				$('#cz_ach_status1').attr('checked','checked');
			}
			else{
				$('#cz_ach_status1').removeAttr('checked');
			}
		}

		$('#fname').val(data.gatewayFriendlyName);
		$('#gateway').val(data.gateway);
		$('#aid').val(data.gatewayAdminID);
	}
  });

  }

}


$(function(){

	$('#gateway_opt').change(function(){
		var gateway_value =$(this).val();
		if(gateway_value=='3'){
			$('#pay_div').show();
			$('#auth_div').hide();
			$('#nmi_div').hide();
				$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		}else if(gateway_value=='2'){
			$('#auth_div').show();
			$('#pay_div').hide();
			$('#nmi_div').hide();
				$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		}else if(gateway_value=='1'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').show();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
		}else if(gateway_value=='4'){
				$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').show();
			$('#cyber_div').hide();
			$('#cz_div').hide();
			}else if(gateway_value=='5'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').show();
			$('#paypal_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
			}
			else   if(gateway_value=='6'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
			$('#usaepay_div').show();
			$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();
			}
		else   if(gateway_value=='7'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
				$('#usaepay_div').hide();
				$('#heartland_div').show();
			$('#cyber_div').hide();
			$('#cz_div').hide();
			}
			else  if(gateway_value=='8'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
				$('#usaepay_div').hide();
				$('#heartland_div').hide();
			$('#cyber_div').show();
			$('#cz_div').hide();
			}else if(gateway_value=='9'){
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').show();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
		}
			else{

				$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();
			$('#stripe_div').hide();
			$('#paypal_div').hide();
				$('#usaepay_div').hide();
				$('#heartland_div').hide();
			$('#cyber_div').hide();
			$('#cz_div').hide();

			}
	});

	$('#cz_div').show();



  $('#nmiform').validate({ // initialize plugin
		ignore:":not(:visible)",
		rules: {

		 'gateway_opt':{
			required: true,	
		 },

		'nmiUser': {
			required: true,
			minlength: 3
		},
		'nmiPassword':{
			required : true,
			minlength: 5,
		},

		'czUser': {
				required: true,
				minlength: 3
		},

		'czPassword':{
				required : true,
				minlength: 5,
		},

		'apiloginID':{
			required: true,
				minlength: 3
		},
		'transactionKey':{
			required: true,
				minlength: 3
		},

		'frname':{
				required: true,
				minlength: 3
		},

		'nmiUser1': {
				required: true,
				minlength: 3
			},
		'nmiPassword1':{
			required : true,
			minlength: 5,
		},

		'czUser1': {
				required: true,
				minlength: 3
			},
		'czPassword1':{
			required : true,
			minlength: 5,
		},

		'apiloginID1':{
			required: true,
				minlength: 3
		},

		'transactionKey1':{
			required: true,
				minlength: 3
		},


		'paytracePassword':{
			required: true,
			minlength: 5
		},

		'paytraceUser':{
			required: true,
				minlength: 3
		},

		'paytracePassword1':{
			required: true,
			minlength: 5
		},
		'paytraceUser1':{
			required: true,
			minlength: 3
		},

		'paypalPassword':{
			required: true,
			minlength: 5
		},

		'paypalUser':{
			required: true,
				minlength: 3
		},

		'paypalSignature':{
			required: true,
			minlength: 5
		},
		'paypalUser1':{
			required: true,
			minlength: 3
		},
		'paypalPassword1':{
			required: true,
			minlength: 5
		},

		'paypalSignature1':{
			required: true,
			minlength: 5
		},



		'fname':{
			required:true,
			minlength:2,
		},

		'stripeUser':{
		     required:true,
			 minlength:3,
		},
			'stripePassword':{
		     required:true,
			 minlength:5,
		}






	 },
  });


	$('#nmiform1').validate({ // initialize plugin
		ignore:":not(:visible)",
		rules: {

		'gateway_opt':{
				required: true,
		},

		'nmiUser': {
				required: true,
				minlength: 3
		},

		'nmiPassword':{
				required : true,
				minlength: 5,
		},

		'czUser': {
				required: true,
				minlength: 3
		},

		'czPassword':{
				required : true,
				minlength: 5,
		},

		'apiloginID':{
			required: true,
			minlength: 3
		},

		'transactionKey':{
			required: true,
			minlength: 3
		},

		'frname':{
				required: true,
				minlength: 3
		},

		'nmiUser1': {
				required: true,
				minlength: 3
		},

		'nmiPassword1':{
				required : true,
				minlength: 5,
		},

		'czUser1': {
				required: true,
				minlength: 3
		},

		'czPassword1':{
				required : true,
				minlength: 5,
		},

		'apiloginID1':{
			required: true,
				minlength: 3
		},

		'transactionKey1':{
				required: true,
                 minlength: 3
		 },

		'paytracePassword':{
				required: true,
                 minlength: 3
		 },

		'paytraceUser':{
				required: true,
                 minlength: 3
		 },

        'paytracePassword1':{
				required: true,
                 minlength: 5
		 },

		'paytraceUser1':{
				required: true,
                 minlength: 3
		 },



			'stripeUser1':{
		     required:true,
			 minlength:3,
		},
			'stripePassword1':{
		     required:true,
			 minlength:5,
		},


		 'fname':{
			 required:true,
			 minlength:2,
		 }


	 },
  });


       var GetMyRemote=function(element_name){
	        $('#update_btn').attr("disabled", true);
            remote={
				     beforeSend:function() {
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Payments/testNMI',
                    type: "post",
					data:
                        {
                            nmiuser: function(){return $('input[name=nmiUser]').val();},
                            nmipassword:  function(){return $('input[name=nmiPassword]').val();}
                        },
                          dataFilter:function(response){

						   data=$.parseJSON(response);
                             if (data['status'] === 'true') {


							    $('#update_btn').removeAttr('disabled');
						     	$('#nmiPassword').removeClass('error');
								 $('#nmiPassword-error').hide();

									   return data['status'] ;

                              } else {

								   if(data['status'] =='false'){
                                       $('#nmiform').validate().showErrors(function(){

                                                       return {key:'nmiPassword'};
                                                       })
								   }
                               }

                               return JSON.stringify(data[element_name]);
                         }
                    }

            return remote;
        }
 });
</script>

<style>
.padding-bottom-25px{
    padding-bottom: 25px !important;
}

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }

 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }

 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>