
<div id="page-content">

					
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Products Block -->
    <div class="block">
        <!-- Products Title -->
       <div class="block-title">
			<div class="block-options pull-right">
			    <a href="<?php echo base_url(); ?>home/invoice_details_print/<?php echo  $invoice['invoice']; ?>"  class="btn  btn-sm btn-danger" data-toggle="tooltip" title="">Download PDF</a>
        	</div>
           
          
            <h2><strong>Invoice</strong> Details</h2>
		
		
        </div>
      
      
        
             <form id="form-validationRTRT" action="<?php echo base_url().'home/edit_custom_invoice'; ?>" method="post" enctype="multipart/form-data" >
	
			     
                <h4>Invoice Number: <strong><?php  echo ($invoice['invoiceNumber'] == null || empty($invoice['invoiceNumber']))? $invoice['invoice'] :$invoice['invoiceNumber']; ?></strong></h4>
                <h5>Merchant Name:  <strong><?php echo $invoice['merchantName']; ?></strong></h5>
                <h5>Invoice Date:  <?php echo date('m/d/Y',strtotime($invoice['createdAt'])); ?></h5>
                <h5>Due Date:  <?php echo date('m/d/Y',strtotime($invoice['DueDate'])); ?></h5>
                <h5>Invoice Status:  <strong><?php if($invoice['status']=='paid') echo 'Paid'; else echo 'Pending';  ?></strong></h5>
                <h5>Payment Method: <strong> -- <?php 
                    if($merchant_data['payOption'] == 2){
                        echo 'eCheck';
                    }else{
                        echo 'Credit Card';
                    }
                 ?>  </strong></h5>
                    <div class="l_space">
                <h4>Overview</h4>
             				  
				<?php $pl='';  foreach($plans_overview as $plview){  ?>
                    
                         <?php echo '<h5>'.$plview['plan_name'].'</h5> '; ?>
                     
                    <?php } ?>
               
                </div>
                <?php
                    if(in_array($invoice['invoiceStatus'], [ 1,3,5 ]) && strtolower($invoice['status']) == 'pending') {
                        ?>
                            <div class="l_space">
                                <h5>Preview Invoice Page:<strong class="cust_view"><a target="_blank" href="<?php echo $paylink; ?>"><?php echo $paylink; ?></a> </strong></h5>               
                            </div>
                        <?php
                    }
                
                ?>
          	 
					<div class="table-responsive">
				    <table class="table table-bordered table-vcenter">
					<thead class="block-title">
                    <tr>
                        <th>Plan Name</th>
                        <th></th>
                        
                         <th class="text-right">Amount</th>
					 </tr>
                    </thead>
                    <tbody  id="item_fields">
				       
				     <?php  $total=0;
				     foreach($items as $k=>$item)
				     {  
				         $total =$total+$item['itemPrice'];
				         
				        if($item['merchantID'] !=0 )
				        { ?>
                        <tr class="removeclass<?php echo $k+1; ?>  rd ">
                        <td><?php echo $item['itemName'];   ?></td> 
                        <td></td>
                       
                        <td class="text-right">$<?php echo number_format($item['itemPrice'],2); ?>
                        <input type="hidden"  id="totalVal<?php echo $k+1; ?>" name="totalVal[]" value="<?php echo $item['itemPrice']; ?>" class="total_val" /> </td>
                       
                        </tr>
			     <?php } 
			            else
			            { ?>	
			         <tr class="removeclass<?php echo $k+1; ?>  rd ">
                         <td><?php echo $item['itemName'];   ?></td>
                         <td> </td>
                        
                             <td class="text-right">$<?php echo number_format($item['itemPrice'],2); ?>
                        <input type="hidden"  id="totalVal<?php echo $k+1; ?>" name="totalVal[]" value="<?php echo $item['itemPrice']; ?>" class="total_val" /> </td>
                     
                       
                        </tr>
                 <?php } 
                    } 
                 ?>
                 
                    
					<tr class="info">
                        <td colspan="2" class="text-right text-uppercase"><strong>Subtotal</strong></td>
                        <td class="text-right">$<?php echo number_format($total,2); ?></td>
                    </tr>
				
					<tr class="danger">
                        <td colspan="2" class="text-right text-uppercase"><strong>Total Due</strong></td>
                        <td class="text-right"><strong>$<span id="grand_total"><?php echo number_format($invoice['BalanceRemaining'],2); ?></span></strong></td>
                    </tr>
					</tbody>
					
				</table>
				
				 <input type="hidden"  id="invNo" name="invNo" value="<?php echo $invoice['invoice']; ?>" />
				 
					
					
		        	</div>	
			
					
				
									 
					
					
			    </form>  
			
        </div>
           <div class="block1">    
   <div class="row">
        <div class="col-sm-12 addrDiv">
            <!-- Billing Address Block -->
          
                <!-- Billing Address Title -->
                <div class="block-title_1">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
               
			     
                <h4><strong><?php echo   $invoice['merchantName']; ?></strong></h4>
                <div class="col-sm-6">
                <?php if(isset($card_data->merchantCardID)){ ?>
				<address>
                        <?php   if($card_data->billing_first_name != ''){
                                    echo $card_data->billing_first_name;
                                }
                                if($card_data->billing_last_name != ''){
                                    echo ' '.$card_data->billing_last_name;
                                }

                          ?>

                         <br>

                         <?php  if($card_data->billing_phone_number != ''){
                                    echo '<i class="fa fa-phone"></i> '.$card_data->billing_phone_number.'<br>';
                                }
                                if($card_data->billing_email != ''){
                                    echo '<i class="fa fa-envelope-o"></i> '.$card_data->billing_email.'<br>';
                                }

                          ?>
						<?php    if($card_data->Billing_Addr1 != ''){
                         echo $card_data->Billing_Addr1; } ?><br>
                         <?php echo ($card_data->Billing_City)?$card_data->Billing_City.', ':'-- ,'; ?>
                         <?php echo ($card_data->Billing_State)?$card_data->Billing_State.' ':'-- ';?>
                         <?php echo ($card_data->Billing_Zipcode)?$card_data->Billing_Zipcode:'--'; ?> <br>
                         
                         <?php echo $card_data->Billing_Country; ?><br><br>
				</address>
            <?php } ?>
				</div>
				<div class="col-sm-6">
				    	<address>
				
                                <i class="fa fa-phone"></i>  <?php echo ($merchant_data['merchantContact'])?$merchant_data['merchantContact']:'--'; ?>  <br>
                                <i class="fa fa-envelope-o"></i> <?php echo ($merchant_data['merchantEmail'])?$merchant_data['merchantEmail']:'--'; ?> <br>
				    </address>
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
  
    <!-- END Addresses -->

    <!-- Log Block -->
    
    <!-- END Log Block -->   
</div>
</div>  
    </div>
            <!-- END Billing Address Block -->
      
          
		        	
		
 



<script>

var room='<?php echo $k+2 ?>';
     function item_invoice_fields()
	{
		
   
         room++;
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("tr");
		divtest.setAttribute("class", " rd removeclass"+room);
		var rdiv = 'removeclass'+room;
		
    	divtest.innerHTML = '<td> <input type="text" class="form-control" id="planName'+room+'" name="planName[]" value="" placeholder="Description "></td><td</td> <td> <div class="input-group"><input type="text" data-id="'+room+'"  onblur="chk_cal_amount(this);" maxlength="6"  class="form-control" id="planAmount'+room+'" name="planAmount[]" value="" placeholder="Amount "> <input type="hidden" class="form-control total_val" id="total'+room+'" name="total[]" value=""/><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_item_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></td> ';
        $(divtest).insertAfter($('table tr.rd:last'));
	
  }
    function remove_item_fields(rid)
    {
	  var gr_val=0;

       $('.removeclass'+rid).remove();
     	$( ".total_val" ).each(function(){
			var test = $(this).val();
			if(test!="" && test!='undefined')
            gr_val+= parseFloat(test);
			});
   			$('#grand_total').html(format22(gr_val));
	   
    }  

function chk_cal_amount(evn)
{

    var rid = $(evn).data("id");
    $('#total'+rid).val($(evn).val());
    $('#total11'+rid).html(format22($(evn).val()));
    	var grand_total=0;
		$( ".total_val" ).each(function(){
		   var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
		});
		
		$('#grand_total').html(format22(grand_total));
}
 $(function(){ 
     
 $('.testbtn').click(function(){

	var index = ''; 
	 var test =$('#form-validationRTRT').valid();
	
	 
	 if(test)
	 {
	   if($(this).val()=='Save')
	   {
			index ="self";
	   }
		else{
		index ="other";	
		}
		
		
								$('#index').remove(); 
			 	 
                                    
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'index',
											'name': 'index',
										
											'value':index ,
											}).appendTo($('#form-validationRTRT'));
	
	$('#form-validationRTRT').submit();
	
	 }
	
 });
 
   $('#form-validationRTRT').validate({
               errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('td').append(error);
                },
                highlight: function(e) {
                    $(e).closest('td').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                   rules: {
						
						 'planName[]': {
							  required: true,
							    minlength:3,
						},
				       'planAmount[]': {
							  required: true,
							  minlength:1,
							  number:true
						},	
                   	
                  
                  
                },
              
            });
 
 });
 
 
 function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) { 
        
        
        return  num=="-" ? (-acc) : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];


}

function isNumberKey(evt)
{
  var charCode = (evt.which) ? evt.which : event.keyCode;
  if (charCode != 46 && charCode != 189 && charCode > 31
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
}

  
          

		




</script>