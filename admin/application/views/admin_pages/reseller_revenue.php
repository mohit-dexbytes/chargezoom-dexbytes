

<!-- Page content -->
<div id="page-content">
   
    <!-- Quick Stats -->
    <div class="row text-center">
        	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        
    </div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            
            <h2><strong>Reseller</strong> Revenue</h2>
            
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="reseller_revenue" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class="text-left">Reseller</th>
                    <th class="text-left">Month</th>
                    <th class="text-right">Payout Date</th>
                     <th class="hidden-xs text-right">Total</th>
                     <th class="text-center">Action</th>
                    
                    
                </tr>
            </thead>
            <tbody>
			 
			     <?php if(!empty($r_results)) { 
			          foreach($r_results as $r_res)
			          { ?>
			     
			     <tr>
			         	<td class="text-left"><?php echo ucfirst($r_res['resellerName']); ?></td>
					<td class="text-left"><?php echo date('F Y',strtotime($r_res['revenueMonth'])); ?></td>
					<td class="text-right"><?php echo date('F d, Y',strtotime($r_res['payoutDate'])); ?></td>
					
                    <td class="hidden-xs text-right">$<?php echo number_format($r_res['totalRevenue'],2); ?></td>
				
					
					<td class="text-center">
					    
					    <?php if($r_res['payStatus']=='Paid'){ ?>
		 <a href="javascript:void();" class="btn btn-sm btn-success" disabled  data-backdrop="static" data-keyboard="false"   data-toggle="modal"
>Processed</a>
			
		         <?php }else{ ?> 			
 <a href="#rev_pay" class="btn btn-sm btn-success"  data-backdrop="static" data-keyboard="false"   data-toggle="modal"
 onclick="set_reseller_pay('<?php  echo $r_res['revID']; ?>','<?php  echo number_format($r_res['totalRevenue'],2); ?>','<?php echo strtoupper($r_res['resellerName']); ?>');">Process</a>
				  <?php }?> 	
					</td>

				</tr>
				
			<?php } } ?>	
				
			</tbody>
			
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->


<div id="rev_pay" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Commission</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="resv" method="post" action='<?php echo base_url()?>home/pay_invoice' class="form-horizontal" >
                     
                 
                    <p id="para_text">Are you sure you want to release payment $xxxx.xx to RESELLERNAME?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invID" name="invID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Release Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#reseller_revenue').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [2] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();




function set_reseller_pay(inv_id, payment, rs_name)
{
       $('#invID').val(inv_id);
       $('#para_text').html("Are you sure you want to release payment $"+payment + ' to '+ rs_name+'?' );
    
}

</script>




<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
