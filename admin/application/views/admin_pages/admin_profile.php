
   <!-- Page content -->
	<div id="page-content">
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?>
	</div>
	
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <legend class="leg">Accounting Package</legend>

    <div class="block">
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="form_new" class="form form-horizontal"  enctype="multipart/form-data" action="<?php echo base_url(); ?>Admin_panel/admin_profile">
		         <input type="hidden"  id="adminID" name="adminID" value="<?php if(isset($admin)){echo $admin['adminID']; } ?>" /> 
                 
				 
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminFirstName']; } ?>" placeholder="Your First Name"><?php echo form_error('adminFirstName'); ?></div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminLastName']; } ?>" placeholder="Your Last Name"><?php echo form_error('adminLastName'); ?></div>
					</div>
					
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email Address </label>
					<div class="col-md-6">
					<input type="text" id="Email" name="Email" class="form-control"  value="<?php if(isset($admin)){ echo $admin['adminEmail']; } ?>" placeholder="Your Email" ><?php echo form_error('adminEmail'); ?></div>
					</div>
                    <div class="form-group">
						<div class="col-xs-10 text-right">
							<button type="submit" class="btn btn-sm btn-success">Save</button>
                            <button type="button" class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
							
						</div>
					</div>
					   
		
  	</form>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

   
    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		     
    });

 
   
    
		
	

</script>

</div>