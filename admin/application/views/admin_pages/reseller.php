<!-- Page content -->
<style>
.block-options1 {
    margin: 0px 75px;
    line-height: 37px;
    color: black;
}

.left--100px{
    left: -100px;
}

.padding-bottom-25px{
    padding-bottom: 25px !important;
}
</style>

<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message'); ?></div>
    <div class="row">
        <div class="col-md-12 padding-bottom-25px">
            <legend class="leg">Direct Management</legend>
            <div class="block-main full" >
                <table id="direct_reseller_page" class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>
                        <th class="text-left">Company Name </th>
                            <th class="text-left hidden-xs">Merchants</th>
                            <th class="text-left hidden-xs hidden-sm">Transactions </th>
                            <th class="hidden-xs">Revenue</th>
                            <th class="text-left hidden-xs">Activation Date</th>
                            <th class="text-center">Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        <?php 
                        
                            if(isset($direct_reseller_list) && $direct_reseller_list){
                                foreach($direct_reseller_list as $direct_reseller){
                                    $total_direct_merchant = $direct_reseller['merchant'];
                                    ?>
                                        <tr>
                                            <td class="text-left cust_view">
                                                <a href="<?php echo base_url('Admin_panel/create_reseller/'.$direct_reseller['resellerID']); ?>" data-toggle="tooltip" title="Edit Reseller"> <?php echo $direct_reseller['resellerCompanyName']; ?></a>
                                            </td>
                                            <td class="text-left hidden-xs"> <?php echo $direct_reseller['merchant']; ?></td>
                                            <td class="text-left hidden-xs hidden-sm"> <?php echo $direct_reseller['transaction']; ?></td>
                                            <td class="text-left hidden-xs"> $<?php echo number_format($direct_reseller['volume'],2); ?></td>
                                            <td class="text-left hidden-xs"> <?php echo ($direct_reseller['date_added']) ? date('M d, Y', strtotime($direct_reseller['date_added'])) : '-----'; ?></td>
                                            <td class="text-center">
                                                <div class="block-options">
                                                    <div class="btn-group">
                                                        <div class="btn-group dropbtn">
                                                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                                            <ul class="dropdown-menu text-right">
                                                                <li>
                                                                    <?php if($direct_reseller['isSuspend']=='0'){  ?>
                                                                        <a href="<?php echo base_url('Admin_panel/reseller_login/'.$direct_reseller['resellerID']); ?>" target="_blank"  data-toggle="tooltip" title="">Login as Reseller</a>
                                                                    <?php } 
                                                                        if($direct_reseller['isSuspend']=='1'){  ?>
                                                                            <a href="javascript:void(0);" target="_blank" disabled  data-toggle="tooltip" title="">Login as Reseller</a>
                                                                    <?php } ?>
                                                                </li>
                                                                <li></li>
                                                                <li> 	 
                                                                    <a href="<?php echo   base_url('home/invoices/'.$direct_reseller['resellerID']);?>" data-toggle="tooltip" title="" >View Invoice History</a>     
                                                                    </li>
                                                                <li></li>
                                                                <li> 
                                                                    <?php if($direct_reseller['isSuspend']=='0'){  ?>	 
                                                                        <a href="#suspend_reseller" onclick="set_reseller_id('<?php echo $direct_reseller['resellerID']; ?>','0','<?php echo $direct_reseller['resellerCompanyName']; ?>');"  data-toggle="modal">Suspend Reseller</a>
                                                                    <?php 	}   if($direct_reseller['isSuspend']=='1'){  ?> 
                                                                        <a href="#suspend_reseller" onclick="set_reseller_id('<?php echo $direct_reseller['resellerID']; ?>','1','<?php echo $direct_reseller['resellerCompanyName']; ?>');"  data-toggle="modal" >Resume Reseller</a>
                                                                    <?php } ?>
                                                                </li>
                                                                <li></li>
                                                                <li>
                                                                    <a href="#disable_reseller" onclick="set_disable_reseller('<?php  echo $direct_reseller['resellerID'];?>', '<?php echo $total_direct_merchant; ?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"> Disable Reseller </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            }  
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-12 padding-bottom-25px">
            <legend class="leg">Reseller Management</legend>
            <div class="block-main full" style="position: relative">
                <div class="addNewFixRight">
                    <a  title="Create Reseller" class="btn btn-sm  btn-success"  href="<?php echo base_url(); ?>Admin_panel/create_reseller">Add New</a>       
                    <div class="btn-group">
                        <div class="btn-group dropbtn">
                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                            <ul class="dropdown-menu text-right left--100px">
                                <li>
                                    <a  title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#reassign_marchant">Reassign Merchants</a> 
                                </li>
                                <li> 	 
                                    <a  title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#Edit_gateway" >Change Gateway in Bulk</a>   
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <table id="reseller_page" class="table table-bordered table-striped table-vcenter">
                    <thead>
                        <tr>
                        <th class="text-left">Company Name </th>
                            <th class="text-left hidden-xs">Merchants</th>
                            <th class="text-left hidden-xs hidden-sm">Transactions </th>
                            <th class="hidden-xs">Revenue</th>
                            <th class="text-left hidden-xs">Activation Date</th>
                            <th class="text-center">Action </th>
                        </tr>
                    </thead>
                    <tbody>
                    
                        <?php 
                        
                            if(isset($reseller_list) && $reseller_list){
                                foreach($reseller_list as $reseller){
                                    $total_merchant = $reseller['merchant'];
                                    ?>
                                        <tr>
                                            <td class="text-left cust_view">
                                                <a href="<?php echo base_url('Admin_panel/create_reseller/'.$reseller['resellerID']); ?>" data-toggle="tooltip" title="Edit Reseller"> <?php echo $reseller['resellerCompanyName']; ?></a>
                                            </td>
                                            <td class="text-left hidden-xs"> <?php echo $reseller['merchant']; ?></td>
                                            <td class="text-left hidden-xs hidden-sm"> <?php echo $reseller['transaction']; ?></td>
                                            <td class="text-left hidden-xs"> $<?php echo number_format($reseller['volume'],2); ?></td>
                                            <td class="text-left hidden-xs"> <?php echo ($reseller['date_added']) ? date('M d, Y', strtotime($reseller['date_added'])) : '-----'; ?></td>
                                            <td class="text-center">
                                                <div class="block-options">
                                                    <div class="btn-group">
                                                        <div class="btn-group dropbtn">
                                                            <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                                            <ul class="dropdown-menu text-right">
                                                                <li>
                                                                    <?php if($reseller['isSuspend']=='0'){  ?>
                                                                        <a href="<?php echo base_url('Admin_panel/reseller_login/'.$reseller['resellerID']); ?>" target="_blank"  data-toggle="tooltip" title="">Login as Reseller</a>
                                                                    <?php } 
                                                                        if($reseller['isSuspend']=='1'){  ?>
                                                                            <a href="javascript:void(0);" target="_blank" disabled  data-toggle="tooltip" title="">Login as Reseller</a>
                                                                    <?php } ?>
                                                                </li>
                                                                <li></li>
                                                                <li> 	 
                                                                    <a href="<?php echo   base_url('home/invoices/'.$reseller['resellerID']);?>" data-toggle="tooltip" title="" >View Invoice History</a>     
                                                                    </li>
                                                                <li></li>
                                                                <li> 
                                                                    <?php if($reseller['isSuspend']=='0'){  ?>	 
                                                                        <a href="#suspend_reseller" onclick="set_reseller_id('<?php echo $reseller['resellerID']; ?>','0','<?php echo $reseller['resellerCompanyName']; ?>');"  data-toggle="modal">Suspend Reseller</a>
                                                                    <?php 	}   if($reseller['isSuspend']=='1'){  ?> 
                                                                        <a href="#suspend_reseller" onclick="set_reseller_id('<?php echo $reseller['resellerID']; ?>','1','<?php echo $reseller['resellerCompanyName']; ?>');"  data-toggle="modal" >Resume Reseller</a>
                                                                    <?php } ?>
                                                                </li>
                                                                <li></li>
                                                                <li>
                                                                    <a href="#disable_reseller" onclick="set_disable_reseller('<?php  echo $reseller['resellerID'];?>', '<?php echo $total_merchant; ?>');"  data-backdrop="static" data-keyboard="false"  data-toggle="modal"> Disable Reseller </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php
                                }
                            }  
                        ?>
                        
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
 
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	
<!--------------------Disable admin Reseller------------------------>

<div id="disable_reseller" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disable Reseller</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
               
               <div id="merchantNotExist">
                   
                    <form id="del_reseller" method="post" action='<?php echo base_url(); ?>Admin_panel/disable_reseller' class="form-horizontal" >                     
                 
					<p>Do you really want to Disable this Reseller?</p> 					
				    <div class="form-group">                     
                        <div class="col-md-8">
                            <input type="hidden" id="resellerID" name="resellerID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Disable"  />
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>	
			    </div>
               <div id="merchantExist">
                	<p style="Color:red;">Reseller can not be disabled, Merchants must be reassigned.</p> 	
                	
                	  <div class="pull-right">
        		      <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </div>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<!--- Edit Gateway model for reseller start from here -->
        
  <div id="Edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Gateway in Bulk</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>Admin_panel/update_gateways_resellers' class="form-horizontal" >
				 
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_old" name="gateway_old"  class="form-control">
								   <option value="" >Select Current Gateway</option>
								    <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_new" name="gateway_new"  class="form-control">
								   <option  value="" >Select New Gateway</option>
								   <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="col-md-10 text-right">
        			 <input type="submit" id="btn_cancel1" name="btn_cancel1" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"  id="btn_cancel" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					    <hr/>
				<tbody id="t_data">
						 
						 </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!--- End Of Modal  Here -->


<!--reassign marchant-->

<div id="reassign_marchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reassign Merchant</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>Admin_panel/reasign_marchent' class="form-horizontal" onsubmit="return validate_form();">
				 
                     
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Reseller</label>
						 <div class="col-md-6">
						   <select id="reseller_old" name="reseller_old"  class="form-control" required>
								   <option value="" >Select Reseller</option>
								    <?php foreach($merchant as $merchant){  ?>
								    <option value="<?php echo $merchant['resellerID']; ?>" ><?php echo $merchant['resellerCompanyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
						
						<div class="form-group ">
                        <label class="col-md-4 control-label" for="card_list">Assign Reseller</label>
						 <div class="col-md-6">
						 
						   <select id="reseller_new" name="reseller_new"  class="form-control" required>
								   <option value="" >Select Reseller</option>
								    <?php foreach($reasign as $reasign){  ?>
								    <option value="<?php echo $reasign['resellerID']; ?>" ><?php echo $reasign['resellerCompanyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				     <div class ="form-group">
    						   <label class="col-md-4 control-label" for="example-typeahead">Assign Plans</label>
    						   <div class="col-md-6">
    								<select id="Plans" class="form-control " name="Plans" >
    								<option value="">Select Plans</option>
    								<?php if(isset($plans)){ foreach ($plans as $plan){  ?>
    								<option value="<?php echo $plan['plan_id']; ?>" 
    								<?php if($plan['plan_id']==$merchant['plan_id'])echo "selected"; ?>> <?php echo $plan['plan_name']; ?></option>
    								<?php } } ?>
    								</select>
    							</div>
                            </div>

                            <div class ="form-group">
    						   <label class="col-md-4 control-label" for="example-typeahead"></label>
    						   <div class="col-md-6 text-right">
                               <input type="submit"  name="btn_cancel1" id="upt_btn_res" class="btn btn-sm btn-success" value="Save" disabled />
                    <button type="button"   class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
    							</div>
                            </div>
						
				   <div id="t_data_res">
				       
				   </div>
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------------------ End of Modal Resign ------------------------------>

<!----------------------========================================Start of Suspend Modal====================================================------------------------>

<div id="suspend_reseller" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="sus_txt">Suspend Reseller</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="suspend_reseller_form" method="post" action='' class="form-horizontal" >
                     
                 
					<p id="sub_content">Do you really want to suspend this Reseller?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                          
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  id="ddeee" name="btn_cancel" class="btn btn-sm btn-info" value="Suspend"  />
                    <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!--  End of suspended Modal  --->

        
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>

<script>
    $(function(){
        const directCustomSetting = {
            bFilter: false, bInfo: false,bPaginate: false,
        };
        Pagination_view.init('reseller_page'); 
        Pagination_view.init('direct_reseller_page', directCustomSetting, 'padding-bottom-25px'); 
    });

var Pagination_view = function() {

    return {
        init: function(tableId, customSetting = {}, addclass = false) {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            var datatableSetting = {
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [-1] }
                ],
                ...customSetting,
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            };

            / Initialize Datatables /
            $(`#${tableId}`).dataTable(datatableSetting);

            if(addclass){
    			$(`#${tableId}_wrapper div.row:last`).addClass(`${addclass}`);
            }

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_disable_reseller(id,total_merchant){
	 
	  $('#resellerID').val(id);
	   
	   if(total_merchant > 0){
	        
	        $('#merchantExist').show(); 
	        $('#merchantNotExist').hide(); 
	        
	   }else{
	       
	       $('#merchantExist').hide(); 
	       $('#merchantNotExist').show();  
	   }
	
}

</script>	

<script>
$(document).ready(function(){
	$('#reseller_old').change(function(){
	
    var gID =  $('#reseller_old').val();
	$.ajax({
    url: '<?php echo base_url("Admin_panel/get_merchant")?>',
    type: 'POST',
	data:{seller_id:gID},
    dataType: 'json',
    success: function(data){
	
		if(data.status=='success')
		$('#upt_btn_res').removeAttr('disabled');
		else
		$('#upt_btn_res').attr('disabled','disabled');
		
		$('#t_data_res').html(data.data);
		
	}	
  });
	
	});	
	
	
	
$('#reseller_new').change(function(){
    var reID = $(this).val();
    $("#Plans > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('Admin_panel/get_reseller_plan_id'); ?>",
        data: {reID: reID},
        dataType: 'json',
        success:function(data){
			
			var s = $('#Plans');
			 
			   for(var val in  data) {
         
              $("<option />", {value: data[val]['plan_id'], text: data[val]['plan_name'] }).appendTo(s);
           }
			
        }
    });
});



 $('#gateway_old').change(function(){
	
	 
    var gID =  $('#gateway_old').val();

	$.ajax({
    url: '<?php echo base_url("Admin_panel/get_details_reseller")?>',
    type: 'POST',
	data:{gateway_id:gID},
    success: function(data){
		
		$('#t_data').html(data);
		
		var newarray = JSON.parse("[" + data.resllerID + "]");

			 for(var val in  newarray) {
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }
		
			
     }	
  });
	
});	
	

	
});




function validate_form()
{
var valid = true;

if($('input[type=checkbox]:checked').length == 0)
{
    alert ( "ERROR! Please select at least one checkbox" );
    valid = false;
}

return valid;
}


//****************************** To Suspend  and Resume Reseller **********************************

function set_reseller_id(id, act, name)
{
    
     var form = $("#suspend_reseller_form");
            $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											
											}).remove();
    if(act=='0')
    {
	                    	var url   = "<?php echo base_url()?>Admin_panel/update_reseller_status";
									 
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID',
											'value': id
											}).appendTo(form);
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											'value': act
											}).appendTo(form);
									
										 $("#ddeee").val('Suspend')	;							
		                   	  $("#sus_txt").html("Suspend Reseller");	
		                	  $("#sub_content").html("Are you sure to suspend "+name+" Reseller?");
				              $("#suspend_reseller_form").attr("action",url);
     }
     else
     {
    
    	var url   = "<?php echo base_url()?>Admin_panel/update_reseller_status";
									
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID',
											'value': id
											}).appendTo(form);
								    $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											'value': act
											}).appendTo(form);
										$("#sus_txt").html("Resume Reseller");	
									    $("#sub_content").html("Are you sure to resume "+name+"  Reseller?");
									    $("#ddeee").val('Yes')	;						
				
				                        $("#suspend_reseller_form").attr("action",url);
}             

}


</script>
