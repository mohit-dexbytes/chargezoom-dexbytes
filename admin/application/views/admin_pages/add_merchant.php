    <!-- Page content -->
    <div id="page-content">
    	

    	<style>
    		.error {
    			color: red;
    		}
            .pay_value_label {
                padding: 4% 4%;
                background: #f2f2f2;
                width: 90%;
                float: left;
                border-radius: 4px;
                height: 38px;
                font-size: 14px;
            }
            span#viewEditMode {
                cursor: pointer;
                position: relative;
                top: 10px;
                left: 4%;
            }
            .hide_payment_field{
                display: none ;
            }
            .show_payment_field{
                display: block;
            }
    	</style>
    	<!-- END Wizard Header -->

    	<!-- Progress Bar Wizard Block -->

    	<div class="block full">
    		<div class="block-title">
    			<h2><strong> <?php if (isset($merchant)) {
									echo "Edit";
								} else {
									echo "Create";
								} ?></strong> Merchant</h2>
    			<?php
				echo	$message = $this->session->flashdata('message');

				?>
    		</div>

    		<!-- All Orders Title -->

    		<!-- Progress Bar Wizard Block -->

    		<!-- Progress Bar Wizard Content -->
    		<div class="row">



    			<form method="POST" id="merchant_form" class="form form-horizontal" action="<?php echo base_url(); ?>Admin_panel/create_merchant">

    				<input type="hidden" id="merchID" name="merchID" value="<?php if (isset($merchant)) {
								echo $merchant['merchID'];
							} ?>" />
                    <input type="hidden" name="merchantCardID" id="merchantCardID" value="<?php echo (isset($merchant) && $merchant['cardID'] > 0)?$merchant['cardID']:0; ?>">

                    <input type="hidden" name="is_card_edit" id="is_card_edit" value="0">
                <div class="col-md-12 ">
                        
                                                     
                    <div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Company Name <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="companyName" name="companyName" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['companyName'];
                                } ?>" placeholder="Company Name"><?php echo form_error('companyName'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">First Name <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="firstName" name="firstName" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['firstName'];
                                } ?>" placeholder="First Name"><?php echo form_error('firstName'); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Last Name <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="lastName" name="lastName" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['lastName'];
                                } ?>" placeholder="Last Name"><?php echo form_error('lastName'); ?>
                            </div>
                        </div>
                        <?php if (isset($merchant)) { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Password </label>
                            <div class="col-md-6">
                                <input type="password"  autocomplete="off" id="merchantPassword1" name="merchantPassword1" class="form-control" value="" placeholder="Password">
                            </div>
                        </div>

                        <?php } ?>

                        <?php if (isset($merchant)) { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Email Address <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="merchantEmail" name="merchantEmail" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantEmail'];
                                } ?>" placeholder="Email Address"><?php echo form_error('merchantEmail'); ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!isset($merchant)) { ?>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Email Address<span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="text" id="merchantEmail" name="merchantEmail" class="form-control" value="<?php if (isset($merchant)) {
                                    echo $merchant['merchantEmail'];
                                } ?>" placeholder="Email Address"><?php echo form_error('merchantEmail'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Password <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="password"  autocomplete="off" id="merchantPassword" name="merchantPassword" class="form-control" value="<?php if (isset($merchant)) {
                                 echo $merchant['merchantPassword'];
                                } ?>" placeholder="Password"> <?php echo form_error('merchantPassword'); ?>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Confirm Password <span class="text-danger">*</span></label>
                            <div class="col-md-6">
                                <input type="password"  autocomplete="off" id="confirmPassword" name="confirmPassword" class="form-control" value="" placeholder="Password Again"> </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-typeahead">Reseller</label>
                            <div class="col-md-6">
                                <select id="reseller" class="form-control " name="reseller">
                                    <option value="">Select Reseller..</option>
                                    <?php foreach ($reseller_list as $reseller) {  ?>
                                        <option value="<?php echo $reseller['resellerID']; ?>" <?php if (isset($merchant)) {
                                            if ($merchant['resellerID'] == $reseller['resellerID']) { ?> selected="selected" <?php }  ?> <?php } ?>> <?php echo $reseller['resellerCompanyName']; ?> 
                                        </option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Phone Number </label>
                            <div class="col-md-6">
                                <input type="text" id="merchantContact" name="merchantContact" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantContact'];
                                } ?>" placeholder="Phone Number"><?php echo form_error('merchantContact'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Alternate Number </label>
                            <div class="col-md-6">
                                <input type="text" id="merchantAlternateContact" name="merchantAlternateContact" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantAlternateContact'];
                                 } ?>" placeholder="Alternate Number"><?php echo form_error('merchantAlternateContact'); ?>
                             </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Website </label>
                            <div class="col-md-6">
                                <input type="text" id="weburl" name="weburl" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['weburl'];
                                } ?>" placeholder="Website"><?php echo form_error('website'); ?>
                            </div>
                        </div>

                    </div>



    				<div class="col-md-6">

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Address Line 1 </label>
                            <div class="col-md-6">
                                <input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantAddress1'];
                                } ?>" placeholder="Address Line 1 "><?php echo form_error('merchantAddress1'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Address Line 2</label>
                            <div class="col-md-6">
                                <input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantAddress2'];
                                } ?>" placeholder="Address Line 2 "><?php echo form_error('merchantAddress2'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">City</label>
                            <div class="col-md-6">
                                <input type="text" id="merchantCity" name="merchantCity" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantCity'];
                                } ?>" placeholder="City"><?php echo form_error('merchantCity'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">State</label>
                            <div class="col-md-6">
                                <input type="text" id="merchantState" name="merchantState" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantState'];
                                } ?>" placeholder="State"><?php echo form_error('merchantState'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">ZIP Code</label>
                            <div class="col-md-6">
                                <input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantZipCode'];
                                } ?>" placeholder="ZIP Code"><?php echo form_error('merchantZipCode'); ?>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="example-username">Country</label>
                            <div class="col-md-6">
                                <input type="text" id="merchantCountry" name="merchantCountry" class="form-control" value="<?php if (isset($merchant)) {
                                echo $merchant['merchantCountry'];
                                } ?>" placeholder="Country"><?php echo form_error('merchantCountry'); ?>
                            </div>
                        </div>

    				</div>
    				
                </div>
                <div class="col-md-12  payment_info">
                    <fieldset>
                        <legend>Subscription</legend>
                        <div class="col-md-6">
                            <div class="col-md-12 no-pad">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="example-typeahead">Plan<span class="text-danger">*</span></label>
                                    <div class="col-md-6"><?php //print_r($plans);die(); ?>
                                        <select id="Plans" class="form-control " name="Plans">
                                            <option value="">Select Plans</option>
                                            <?php if (isset($plans)) {
                                            foreach ($plans as $plan) {  ?>
                                            <option value="<?php echo $plan['plan_id']; ?>" <?php if ($plan['plan_id'] == $merchant['plan_id']) echo "selected"; ?>> <?php echo $plan['plan_name']; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 no-pad">
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="card_number">Payment Option</label>
                                    <div class="col-md-6">
                                        <select name="payOption" id='payOption' class="form-control" <?php echo isset($merchant_card_data->merchantCardID)?'disabled=true':'';  ?>  >
                                          
                                            <option value="">Select Method</option>
                                            <option value="1" <?php echo (isset($merchant) && $merchant['payOption'] == 1)?'selected':''; ?>>Credit Card</option>
                                            <option value="2" <?php echo (isset($merchant) && $merchant['payOption'] == 2)?'selected':''; ?>>eCheck</option>
                                        </select>
                                          
                                    </div>
                                </div>  
                                       
                            </div>

                            <?php 

                            if(isset($merchant) && $merchant['payOption'] > 0)  {
                                $class_manage = 'hide_payment_field';
                                $bill_display = 'style="display: block;"';
                             ?>

                                <div id="exist_payment_method" class="exist_payment_method col-md-12 no-pad" >
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">
                                           <?php 
                                            if($merchant['payOption'] == 1){
                                                echo 'Credit Card';
                                            }else{
                                                echo 'Checking Account';
                                            }
                                            ?> 
                                        </label>
                                        <div class="col-md-6">
                                            <div class="pay_value_label"> 
                                                <?php

                                                if(isset($merchant_card_data->merchantFriendlyName)){ 

                                                    echo $merchant_card_data->merchantFriendlyName; 

                                                }
                                                 ?>                                   
                                            </div> 
                                            <span id="viewEditMode" class="edit_pay_option">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </span> 
                                        </div>
                                    </div>  
                                </div>
                            <?php }else{
                                $class_manage = 'show_payment_field';
                                $bill_display = '';
                            }
                            ?>
                            
                            <!-- Email div -->
                            <div id="email_div" class="<?php echo $class_manage; ?>" <?php if(isset($merchant) && $merchant['payOption'] != '0'){ ?> style="display:none;" <?php }else{ ?> style="display:block;" <?php } ?>  >
                                
                            </div>
                            <!--   End -->
                            <!-- echeck Field  -->
                            <div id="chk_div" class="<?php echo $class_manage; ?>">
                                <div class="col-md-12 no-pad">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Account_Number">Account Number<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" id="accountNumber" name="accountNumber" class="form-control"  value="" placeholder="Account Number"><?php echo form_error('accountNumber'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Routing_Number">Routing Number<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" id="routNumber" name="routNumber" class="form-control"  value="" placeholder="Routing Number"><?php echo form_error('routingNumber'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account_name">Account Name<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" id="accountName" data-stripe="accountName" name="accountName" value="" class="form-control" placeholder="Account Name">
                                        </div>
                                    </div>  
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="Account Type">Account Type</label>
                                        <div class="col-md-6">
                                            
                                            <select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false">
                                               
                                                <option value="checking" <?php if(isset($merchant_card_data->accountHolderType) && $merchant_card_data->accountHolderType=='checking'){ echo ""; } ?> >Checking</option>
                                                <option value="saving" <?php if(isset($merchant_card_data->accountHolderType) && $merchant_card_data->accountHolderType =='saving'){ echo ""; } ?> >Saving</option>
                                               
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label>
                                        <div class="col-md-6">
                                            
                                            <select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false">
                                               
                                                <option value="business" <?php if(isset($merchant_card_data->accountType ) && $merchant_card_data->accountType =='bussiness'){ echo ""; } ?> >Business</option>
                                                <option value="personal" <?php if(isset($merchant_card_data->accountType) && $merchant_card_data->accountType =='personal'){ echo ""; } ?> >Personal</option>
                                               
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <!-- End echeck field -->
                            <!-- Card field  -->
                            <div id="crd_div" class="<?php echo $class_manage; ?>"  >
                                <div class="col-md-12 no-pad">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="card_number">Card Number<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <input type="text" id="cardNumber" data-stripe="cardNumber" maxlength="17" autocomplete="cc-number" name="cardNumber" value="" class="form-control" placeholder="Card Number">
                                                          
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="expry">Expiry Month<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                                        
                                            <select id="expiry" name="expiry" class="form-control">
                                                <?php
                                                       
                                                           
                                                foreach($monthData as $data){  $sel = '';    if(isset($merchant_card_data->cardMonth) && $merchant_card_data->cardMonth == intval($data['monthValue'])) $sel=''; ?> 
                                                                  
                                                    <option value="<?php echo $data['monthValue'];  ?>" <?php echo $sel ; ?> >  <?php echo $data['monthName']; ?></option>
                                                               
                                                          <?php  }  ?>
                                            </select>          
                                                    
                                        </div>
                                    </div> 
                                    <div class="form-group">            
                                        <label class="col-md-4 control-label" for="expiry_year">Expiry Year<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                            <select id="expiry_year" name="expiry_year" class="form-control">
                                                <?php 
                                                $cruy = date('y');
                                                $dyear = $cruy+25;
                                                for($i =$cruy; $i< $dyear ;$i++ ){
                                                    
                                                 $sel = '';    if(isset($merchant_card_data->cardYear) && $merchant_card_data->cardYear == '20'.$i ) $sel=''; 
                                                ?>
                                                    <option value="<?php echo '20'.$i; ?>" <?php echo $sel; ?> > <?php echo "20".$i;  ?> </option>
                                                    
                                                <?php } ?>
                                            </select>
                                        </div>
                                                   
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="cvv" >Security Code (CVV)<span class="text-danger">*</span></label>
                                        <div class="col-md-6">
                                           
                                            <input type="text" id="cvv" name="cvv" class="form-control" value=""  placeholder="Security Code (CVV)" autocomplete="off" />
                                                
                                            
                                        </div>
                                    </div>   
                                </div>

                            </div>
                            <!-- End card field  -->
                        </div>

                        <!--  Right billing field section -->
                        <div class="col-md-6 ">
                            
                            <div id="bill_div" <?php echo $bill_display; ?> class="<?php echo $class_manage; ?>"  >
                                <div class="col-md-12 no-pad">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing First Name </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingfirstName" name="billingfirstName" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_first_name; } ?>" placeholder="Billing First Name"><?php echo form_error('firstName'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Last Name </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billinglastName" name="billinglastName" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_last_name; } ?>" placeholder="Billing last Name"><?php echo form_error('firstName'); ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Phone Number</label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingContact" name="billingContact" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_phone_number; } ?>" placeholder="Billing Phone Number"><?php echo form_error('billingContact'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Email Address </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingEmailAddress" name="billingEmailAddress" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->billing_email; } ?>" placeholder="Billing Email Address"><?php echo form_error('Billing_Email_Address'); ?>
                                        </div>
                                    </div>  
                                <!-- </div>
                                <div class="col-md-6"> -->
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Address </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingAddress1" name="billingAddress1" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_Addr1; } ?>" placeholder="Billing Address1"><?php echo form_error('Billing_Address1'); ?>
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing City</label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingCity" name="billingCity" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_City; } ?>" placeholder="Billing City"><?php echo form_error('billingContact'); ?>
                                        </div>
                                    </div>
                                
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing State </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingState" name="billingState" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_State; } ?>" placeholder="Billing State"><?php echo form_error('Billing_State'); ?>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="example-username">Billing Zipcode </label>
                                        <div class="col-md-6">
                                            <input type="text" id="billingPostalCode" name="billingPostalCode" class="form-control"  value="<?php if(isset($merchant_card_data) && !(empty($merchant_card_data))){ echo $merchant_card_data->Billing_Zipcode; } ?>" placeholder="Billing Postal Code"><?php echo form_error('Billing_Address2'); ?>
                                        </div>
                                    </div>
                                </div>
                                  
                            </div>
                            
                        </div>
                        <div class="col-md-12 bottom_right_mechant_btn">
                            
                           
                            <div class="pull-right">
                                <?php if (!isset($merchant)) { ?>
                                        <button type="submit" class="submit btn btn-sm btn-success">Save</button>
                                    <?php } else { ?>
                                        <button type="submit" class="submit btn btn-sm btn-success">Update</button>
                                    <?php } ?>
                                    <a href="<?php echo base_url('Admin_panel/merchant_list'); ?>" class="btn btn-sm btn-danger ">Cancel</a>
                                  
                            </div>
                            
                            
                        </div>
                    </fieldset>
                    
                </div>
    		</form>



    		</div>
    		<!-- END Progress Bar Wizard Content -->
    	</div>
    	<!-- END Progress Bar Wizard Block -->



    	<!-- END Page Content -->


    	<script>
    		$(document).ready(function() {
                var opt = $('#payOption').val();
                if(opt == 1){
                   
                }else if(opt == 2){
                    
                 
                }else{
                    $('#chk_div').hide();
                    $('#crd_div').hide();
                    $('#bill_div').hide();
                }
             
                $('#viewEditMode').click(function(){
                    $('#is_card_edit').val(1);
                    $('#exist_payment_method').hide();
                    $('#payOption').attr('disabled',false);
                    var opt = $('#payOption').val();
                    
                    if(opt == 1){
                   
                        $('#chk_div').hide();
                        $('#crd_div').show();
                        $('#bill_div').show();
                    }else if(opt == 2){
                        
                        $('#chk_div').show();
                        $('#crd_div').hide();
                        $('#bill_div').show();
                    }else{
                        
                        $('#chk_div').hide();
                        $('#crd_div').hide();
                        $('#bill_div').hide();
                    }

                });
                $('#payOption').change(function(){
                    var opt = $(this).val();
                    $('#is_card_edit').val(1);
                    $('#exist_payment_method').hide();
                    if(opt == "2")
                    {
                        $('#chk_div').css('display','block');
                        $('#crd_div').css('display','none');
                        $('#bill_div').css('display','block');
                        $('#email_div').css('display','none');
                       
                    }
                    else if(opt=="1")
                    {
                        $('#chk_div').css('display','none');
                        $('#bill_div').css('display','block');
                        $('#crd_div').css('display','block'); 
                        $('#email_div').css('display','none');
                    }else{
                        $('#chk_div').css('display','none');
                        $('#crd_div').css('display','none');  
                        $('#bill_div').css('display','none');
                        $('#email_div').css('display','block');
                   }
                }); 

    			$('#reseller').change(function() {
    				var reID = $(this).val();
    				$("#Plans > option").remove();
    				$.ajax({
    					type: "POST",
    					url: "<?php echo base_url('Admin_panel/get_reseller_plan_id'); ?>",
    					data: {
    						reID: reID
    					},
    					dataType: 'json',
    					success: function(data) {

    						var s = $('#Plans');

    						for (var val in data) {

    							$("<option />", {
    								value: data[val]['plan_id'],
    								text: data[val]['plan_name']
    							}).appendTo(s);
    						}

    					}
    				});
    			});

    			
                $('#billingContact').mask('999-999-9999'); 
    			$('#merchant_form').validate({ // initialize plugin
    				ignore: ":not(:visible)",
    				rules: {
                        'companyName':{
                            required: true,
                            minlength: 3,
                        },
    					'firstName': {
    						required: true,
    						minlength: 2,

    					},
    					'lastName': {
    						required: true,
    						minlength: 2,
    					},

    					'merchantEmail': {
    						required: true,
    						isemail: true,
    					},
                        'merchantContact':{
                            number:true,
                        },
                        'merchantAlternateContact':{
                            number:true,
                        },
    					'merchantPassword': {
    						required: true,
                            minlength:8,
                            check_atleast_one_number: true,
                            check_atleast_one_special_char: true,
    					},

    					'confirmPassword': {
    						required: true,
    						equalTo: '#merchantPassword',
    					},
    					'merchantPassword1': {
    						minlength: 8,
                            check_atleast_one_number: true,
                            check_atleast_one_special_char: true,
    					},
    					'Plans': {
    						required: true,
    					},
    					'reseller': {
    						required: true,
    					},
    					'gateway_list': {
    						required: true,
    					},
                        'payOption': {
                           
                        },
                        'cardNumber': {
                            required: true,
                            minlength: 13,
                            maxlength: 16,
                            number: true
                        },
              
                        'expiry_year': {
                            CCExp: {
                              month: '#expiry',
                              year: '#expiry_year'
                            }
                        },
                        'cvv': {
                            required: true,
                            number: true,
                            minlength: 3,
                            maxlength: 4,
                        },
                        'accountNumber':{
                            required: true,
                            number: true,
                            minlength: 3,
                            maxlength: 12,
                        },
                        'routNumber':{
                            required: true,
                            number: true,
                            minlength: 3,
                            maxlength: 12,
                       },
                       'accountName':{
                            required: true,
                            minlength: 3,
                            maxlength: 30,
                       },
                       'billingPostalCode':{
                        
                            minlength:3,
                            maxlength:10,
                            number:true,

                      },
                      'billingContact':{
                        
                            minlength: 10,
                            maxlength: 17,
                         
                      },
                      
                      'billingEmailAddress':{
                         isemail:true
                      },
                      

    				},
    			});
                $.validator.addMethod('isemail', function(value, element, params) {
                      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                    if(value.length == 0){
                        return true;
                    }else{
                         if(!regex.test(value)) {
                            return false;
                          }else{
                            return true;
                          }
                    }
                      
                }, 'Please enter a valid email address.');

                $.validator.addMethod('CCExp', function(value, element, params) {
                      var minMonth = new Date().getMonth() + 1;
                      var minYear = new Date().getFullYear();
                      var month = parseInt($(params.month).val(), 10);
                      var year = parseInt($(params.year).val(), 10);
                      return (year > minYear || (year === minYear && month >= minMonth));
                }, 'Your Credit Card Expiration date is invalid.');

                $.validator.addMethod("check_atleast_one_number", function(value, element) {
                    if(value.length == 0){
                        return true ;
                    }else{
                        if (value.search(/[0-9]/) < 0)
                        return false;
                       
                    }
                     return true ;
             
                }, "This field should contain at least one number.");

                $.validator.addMethod("check_atleast_one_special_char", function(value, element) {
                    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
                    if(value.length == 0){
                        return true ;
                    }
                    if (regularExpression.test(value)) {
                        return false;
                    } 
                    return true ;
                }, "This field should contain at least one special character.");
    		});
    	</script>
    </div>