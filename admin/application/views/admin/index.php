<script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script src="<?php echo base_url('resources/js/wait_me.js');?>"></script>
<link href="<?php echo base_url('resources/css/wait_me.css');?>" rel="stylesheet">
<link href="http://formvalidation.io/vendor/formvalidation/css/formValidation.min.css" rel="stylesheet">

 <script>
 	 	$(document).ready(function() {
 		
 			$('#register').formValidation({
             	framework: 'bootstrap',
              	excluded: [':disabled'],
              	icon: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
            	},
              fields: {
                  name: {
                   validators: {
                       notEmpty: {
                           message: 'Name is required'
                       }
                   }
              	},
              	phone: {
                   validators: {
                       notEmpty: {
                           message: 'Phone is required'
                       }
                   }
              	},
              	company_name: {
                   validators: {
                       notEmpty: {
                           message: 'Company name is required'
                       }
                   }
              	},
              	 email: {
                   validators: {
                       notEmpty: {
                           message: 'Email is required'
                       },
                       emailAddress: {
                          	message: 'Email address is not valid'
                      	},
                        remote: {
                           url: '<?php echo base_url();?>auth/check_email',
                           data: function(validator,$field, value) {
                               return {
                                   email: validator.getFieldElements('email').val(),
                                   csrf_name: $('input[name=csrf_name]').val(),
                                   field:'email',
                                  
                               };
                           },
                           message: 'Email is not available',
                            type: 'POST'
                      	}
                   }
              	},
              	password: {
   	            validators: {
   	            	notEmpty: {
                           message: 'Password is required'
                       }
   	            }
          		}
              }
          })
          .on('success.form.fv', function(e) {
            	register_user();
            	return false;	
            })
            .end();
 	});
 	
 	function register_user()
	{
		run_waitme('ios','#register');
		var csrf_protect = $('input[name=csrf_protect]').val();
		var name    = $('#name').val();
		var phone    = $('#phone').val();
		var company_name    = $('#company_name').val();
		var password    = $('#password').val();
		var email    = $('#email').val();
		   	
		$.post('<?php echo base_url();?>auth/register_user',
		{
			csrf_protect:csrf_protect,
			name:name,
			phone:phone,
			company_name:company_name,
		   	password:password,
		   	email:email
		},
		function(data){
			
		   	$('#register').waitMe("hide");
		   	$('#register')[0].reset();
		   
		});
		   	return false;
	}
 </script>
<div id="page-content">
	<div class="block full">
		<div class="block-title">
            <h2><strong>Create</strong> Reseller</h2>
        </div>
        
            <div class="container">
               <div class="row">
                  <div class="col-md-8 col-sm-12 col-xs-12">
                    
                        <div class="col-md-12 col-xs-12 col-sm-12">
                           <div class="heading-inner">
                              <p class="title">Register Account</p>
                           </div>
                        </div>
                        
                        <?php
                        $attributes = array('class' => 'register', 'id' => 'register');
                        echo form_open('#', $attributes);
                        ?>
                           
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <label  class="col-sm-4 col-form-label">Name</label>
                                 <div class="col-sm-8">
                                 <div class="right-inner-addon">
                                    <input type="text" class="form-control" name="name" id="name" placeholder="" value="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <label class="col-sm-4 col-form-label">Email</label>
                                 <div class="col-sm-8">
                                 <div class="right-inner-addon">
                                    <input type="text" class="form-control" name="email" id="email" placeholder="" value="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <label class="col-sm-4 col-form-label">Phone</label>
                                 <div class="col-sm-8">
                                 <div class="right-inner-addon">
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="" value="">
                                	</div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <label  class="col-sm-4 col-form-label">Company Name</label>
                                 <div class="col-sm-8">
                                 <div class="right-inner-addon">
                                    <input type="text" class="form-control" name="company_name" id="company_name" placeholder="" value="">
                               	</div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                              <div class="form-group">
                                 <label class="col-sm-4 col-form-label">Password</label>
                                 <div class="col-sm-8">
                                 <div class="right-inner-addon">
                                    <input type="password"  autocomplete="off" class="form-control" name="password" id="password" placeholder="" value="">
                                    </div>
                                 </div>
                              </div>
                           </div>
                           
                           <div class="col-md-12 col-sm-12 col-xs-12">
                             
                                 
                                 <button type="submit" class="btn btn-default btn-search-submit pull-left">Register<i class="fa fa-angle-double-right"></i></button>	
                              
                           </div>
                        </form>
                     
                  </div>
                  
               </div>
            </div>
         </section>
	</div>
</div>