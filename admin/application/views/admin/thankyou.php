<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css'>
        <style>
            .jumbotron.thank_page {
                margin: 0 auto;
                position: relative;
                top: 178px;
            }
            label{
                font-size: 13px;
                color: #38485b;
                font-weight: 800;
                margin-bottom: 7px;
            }
            .row{
                padding-bottom: 10px !important;
            }
            .first-row{
                padding-bottom: 0px !important;
            }
            .success{
                color: #3FBF3F;
            }
            .faild{
                color: #cf4436;
            }
            a{
                color: #167bc4;
            }
            hr{
                margin: 10px;
            }
            p {
                margin-bottom: 0px !important;
                color: #000;
            }
            .transaction-print-btn{
                font-size: 13px !important;
            }
            @media print {
            #DivIdToPrint {
                    font-size: 11pt;
                font-family: Consolas;
                padding: 0px;
                margin: 0px;
                }
            }
            .display-3{
            text-align: center;
                font-weight: 200;
            }
            .container{
                background: #ffffff;
                width: 100%;
            }
        </style>
</head>
<body>
    <?php
        $transactionAmount = isset($transactionData['transactionAmount']) && ($transactionData['transactionAmount'] != null) ? number_format($transactionData['transactionAmount'], 2) : number_format(0, 2);
    ?>
    <div class="container">
        <header>
            <div class="col-md-12">
                <div class="navbar-brand-centered" style="padding: 20px 0px;">
                    <img src="<?php echo CZLOGO; ?>" class="img-responsive center-block"  />
                </div>
            </div>
        </header>
    </div>
    <div id="page-content">
        <!-- Products Block -->
        <div class="block thank-block">
            <div class="row first-row">
                <div class="col-md-6">
                    <h5><strong style="font-weight: 600;">Transaction Receipt</strong></h5>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-md-6">
                    <?php if(isset($transactionData['isSuccess']) && $transactionData['isSuccess']){ ?>
                        <h5><strong class="success">Transaction Successful</strong></h5>
                    <?php } else { ?>
                        <div class="alert alert-danger">
                            <strong>Error:</strong> <?php echo $transactionData['transactionStatus']; ?>
                        </div>
					<?php } ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label class="amount_label">Amount: </label><span class="amount_label"> $<?php echo $transactionAmount; ?></span>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?php
                        $datetime = (isset($transactionData['transactionDate']) && !empty($transactionData['transactionDate'])) ? $transactionData['transactionDate'] : date("Y-m-d H:i:s");
                        if (isset($merchant_default_timezone) && !empty($merchant_default_timezone)) {
                            $timezone = ['time' => $datetime, 'current_format' => 'UTC', 'new_format' => $merchant_default_timezone];
                            $datetime = getTimeBySelectedTimezone($timezone);
                        }
                    ?>
                    <label>Date: </label> <span><?php echo date("m/d/Y h:i A", strtotime($datetime)); ?></span>
                </div>
                <div class="col-md-6">
                    <label>Transaction ID:  </label> <span><?php echo $transactionData['transactionID']; ?> </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>IP Address: </label><span>  <?php echo $ip; ?></span>
                </div>
                <div class="col-md-6">
                    <label>Invoice: </label> <span><?php echo $invoiceData['invoiceNumber']; ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <label>Payment Type: </label><span> <?php echo $paymentType; ?></span>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            
        </div>
    </div>
</body>
<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>
</html>
