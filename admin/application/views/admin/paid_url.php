<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css'>
        <style>
            .jumbotron.wrong_page {
                margin: 0 auto;
                position: relative;
                top: 178px;
            }
        </style>
    </head>
    <body>
    
    <div class="jumbotron wrong_page  text-xs-center">
        <div class="msg_data "> <?php echo $this->session->flashdata('message');   ?> </div> 
        <h1 class="display-3">Paid URL!</h1>
        <p class="lead"><strong>You have entered paid Invoice URL.</strong> </p>
        <p class="lead">Please check your URL again or contact your reseller.</p>
        <hr>
    </div>
    <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>
    </body>
</html>