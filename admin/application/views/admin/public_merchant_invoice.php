<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
        <style>
            body{background:#ffffff!important;
                font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
            }
            .backg{background:#f2f2f3;min-height:400px;border-top:1px solid #DADADA;}
            .back{background:#ffffff;width: 100%;float: left;border:1px solid #DADADA;}
            .nopadding{padding:0px;}
            .heading {padding: 8px 0 0 15px;}
            .padding{padding:15px 0px!important;}
            .myblock{background:#f2f2f3;width: 100%;float: left;}
            .myblock2{background:#ffffff;width: 100%;float: left;;border:1px solid #DADADA;margin-top:40px;}
            .policy{margin:3px 5px;}
            .myblock3 {float: left;width: 100%;margin: 30px 0px 5px 0px}
            .btn-primary{background:#2676A4;border:1px solid #2676A4;}
            .error{color:red;font-size:11px;}
            .servererror{color:red;font-size:11px;width:100%;}
            .form-group {margin-bottom: 15px; min-height: 70px;}
            @media (min-width:992px)
            {
                .container{width:786px}
            }
            @media (min-width:1200px) and (max-width:1400px)
            {
                .container{
                width:786
                }
            }
            .invoice_h3{
                padding-right: 0px !important;
                padding-left: 0px !important; 	
            }
            .invoice_h3>h3{
                color: #38485b !important;
                margin-bottom: 20px !important;
                margin-top: 20px !important;
                font-weight: 600 !important;
                font-size: 20px !important;
            }
            .form-control{
                border-color: #e8e8e8 !important;
                height: 44px !important;
                font-size: 14px !important;
                border-top: 0px !important;
            }
            label {
                font-size: 13px;
                color: #38485b;
                font-weight: 600;
                margin-bottom: 7px;
            }
            .grey
            {
                background: #F8F8F8;
                clear: both;
                display: inline-block;
                padding: 15px 0 5px 0px;
            }
            .greyeCheck{
                background: #F8F8F8;
                padding: 15px 0 5px 0px;
                clear: both;
                display: flex;
            }
            .cards
            {
                    padding-top: 3.4rem;
            }
            h5>strong{
                font-weight: 600 !important;
            }
            .submit-btn{
                margin-top: -75px;
            }
            .showDiv{
                display: block;
            }
            .hideDiv{
                display: none ;
            }
            #i_authorize_checkbox{
                margin-right: 16px;  
            }
            #pay_btn{
                padding: 12px 18px;
            }

            .noAction{
                pointer-events: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <header>
                <div class="col-md-12">
                    <div class="navbar-brand-centered" style="padding: 20px 0px;">
                        <img src="<?php echo CZLOGO; ?>" class="img-responsive center-block"  />
                    </div>
                </div>
                <span class = "customer_base_url" id="<?php echo base_url(); ?>"></span>
            </header>
        </div>
        <div class="container-fluid backg p-0">
            <div class="container">
                <div class="row">
                    <div class="msg_data">
                        <?php
                            if(isset($errorMessage)){
                                echo $errorMessage;
                            }
                        ?>
                    </div>
                </div>
                <?php
                    $invoiceBalance = round($invoiceData['BalanceRemaining'],2);
                    if ($invoiceBalance > 0 || in_array($invoiceData['status'], [1,3,5])) {
                        ?>
                        <div class="row">
                            <div class="col-md-7 invoice_h3 col-xs-7">
                                <h3>
                                    Invoice No:<?php if (!empty($invoiceData['invoiceNumber'])) { echo $invoiceData['invoiceNumber']; } else { echo $invoiceData['invoice']; }?>
                                </h3>
                            </div>
                            <div class="col-md-5 invoice_h3 col-xs-5 amount-div-invoice">
                                <h3 class="text-right">
                                    Amount: <?php echo '$' . number_format($invoiceBalance, 2); ?>
                                </h3>
                            </div>
                        </div>

                        <div class="row">
                            <form method="post" id="merchant_public_invoice_pay" action='<?php echo $action; ?>'>
                                <input type="hidden" value="<?php echo $merchantID; ?>" name="mid" />
                                <input type="hidden" value="<?php echo $invoiceID ?>" name="invid" />

                                <div class="back">
                                    <div class="col-md-12 nopadding">
                                        <div class="heading">
                                            <h5> <strong>Cardholder Information</strong></h5>
                                        </div>
                                        <hr/>

                                        <div class="container">
                                            <div class="row nopadding">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">FIRST NAME</label>
                                                        <input type="text" name="first_name" id="first_name" class="form-control" placeholder="">
                                                        <?php echo form_error('first_name', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">LAST NAME</label>
                                                        <input type="text" name="last_name" id="last_name" class="form-control" placeholder="">
                                                        <?php echo form_error('last_name', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>

                                        <div class="container">
                                            <div class="row nopadding">
                                                <div class="toggleIcon">
                                                    <div class="col-md-12">
                                                        <div class="col-md-8" id="radioSection">
                                                            <label class="col-md-4 row text-left">
                                                                <input value="1" onclick="get_process_details(this);" checked="" type="radio" name="sch_method" class="radio_pay">  Credit Card
                                                            </label>
                                                            <label class="col-md-4 row">
                                                                <input value="2" onclick="get_process_details(this);" type="radio" name="sch_method" class="radio_pay"> eCheck
                                                            </label>
                                                            <!-- CheckIT -->
                                                            <input type="hidden" id="paymentType" name="paymentType" class="form-control"  value="1" />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="eCheckView" style="display:none">
                                                    <div class="col-md-12 grey col-xs-12">
                                                        <div class="col-md-12 ">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">ROUTING NUMBER</label>
                                                                    <input type="text" name="route_number" id="routeNumber" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">ACCOUNT NUMBER</label>
                                                                    <input type="text" name="acc_number" id="accountNumber" class="form-control" >
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="control-label">CONFIRM ACCOUNT NUMBER</label>
                                                                    <input type="text" name="confirmAccountNumber" id="confirmAccountNumber" class="form-control" >
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="text-right">
                                                                    <label for="i_authorize_checkbox" style="padding-right: 10px;">I authorize <?php echo (isset($resellerData['companyName']) && !empty($resellerData['companyName'])) ? $resellerData['companyName'] : $resellerData['resellerCompanyName']; ?> to debit my account</label>
                                                                    <input type="checkbox" required id="i_authorize_checkbox" name="i_authorize_checkbox">
                                                                </div>
                                                                <label id="i_authorize_checkbox-error" class="error" for="i_authorize_checkbox" style="display: none;float: right;padding-right: 16px;width: 100%;text-align: right;"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 pull-right">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label><input type="hidden" value="demoinput" id="demoinput" name="demoinput" /> </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div id="creditCardView">
                                                    <div class="grey col-md-12 col-xs-12">
                                                        <div class="col-md-12 ">
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="control-label">CARD NUMBER</label>
                                                                    <input type="text" autocomplete="off" name="card_number" id="card_number11" class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">EXPIRATION MONTH</label>
                                                                    <input type="text" name="expiry" id="expiry11" class="form-control" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-3">
                                                                <div class="form-group">
                                                                    <label class="control-label">EXPIRATION YEAR</label>
                                                                    <input type="text" name="expiry_year" id="expiry_year11" class="form-control" placeholder="">
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <label class="control-label">CVV</label>
                                                                    <input type="text" id="ccv11" name="cvv"  class="form-control" placeholder=""  autocomplete="off">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 cards text-right">
                                                                <img src="<?php echo base_url(); ?>resources/img/amex.png">
                                                                <img src="<?php echo base_url(); ?>resources/img/visa.png" />
                                                                <img src="<?php echo base_url(); ?>resources/img/master.png" />
                                                                <img src="<?php echo base_url(); ?>resources/img/discover.png" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="container">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">STREET ADDRESS</label>
                                                        <input type="text" name="address" id="address" class="form-control" placeholder="">
                                                        <?php echo form_error('address', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label">ADDRESS 2</label>
                                                        <input type="text" name="address2" id="address2" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">CITY</label>
                                                        <input type="text" name="city" id="city" class="form-control" placeholder="">
                                                        <?php echo form_error('city', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">STATE</label>
                                                        <input type="text" name="state" id="state" class="form-control" placeholder="">
                                                        <?php echo form_error('state', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">ZIP/POSTAL</label>
                                                        <input type="text" name="zip" id="zip" class="form-control" placeholder="">
                                                        <?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label class="control-label">COUNTRY</label>
                                                        <input type="text" name="country" id="country" class="form-control" placeholder="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                        <div class="form-group" style="margin-top: 1rem">
                                                        <label><input type="hidden" value="sendrecipt" id="sendrecipt" name="sendrecipt" /> </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 submit-btn pull-right">
                                                    <input type="submit" name="pay" id="pay_btn" value="Pay Now" class="btn btn-success pull-right" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="myblock3" style="font-size: 12px;">
                                    
                                </div>
                            </form>
                        </div>

                        <?php
                    } else {
                        header("location:" . base_url() . "/wrong_url");
                    }
                ?>
            </div>
        </div>
    </body>
    
    <script src="https://payportal.com/resources/js/vendor/jquery-1.11.3.min.js"></script>
    <script src="https://payportal.com/resources/js/vendor/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function () {
            $("#card_number11").attr("maxlength", 17);
            $("#expiry11").attr("maxlength", 2);
            $("#expiry_year11").attr("maxlength", 4);
            $("#ccv11").attr("maxlength", 4);
            
            $("#merchant_public_invoice_pay").validate({
                rules: {
                    first_name: {
                        required: true,
                        minlength: 1,
                        maxlength: 100,
                    },
                    last_name: {
                        required: true,
                        minlength: 1,
                        maxlength: 100,
                    },
                    cardNumber: {
                        required: true,
                        number:true,
                        minlength: 13,
                        maxlength: 16,
                    },
                    cardExpiry: {
                        required: true,
                        number:true,
                        minlength: 2,
                        min: 1,
                        max: 12,
                    },
                    cardExpiry2: {
                        required: true,
                        number:true,
                        minlength: 4,
                        maxlength: 4,
                        CCExp: {
                            month: '#expiry11',
                            year: '#expiry_year11'
                        }
                    },
                    cardCVC: {
                        number:true,
                        minlength: 3,
                        maxlength: 4
                    },
                    accountNumber:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                    confirmAccountNumber:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 20,
                        equalTo: "#accountNumber"
                    },
                    routeNumber:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 12,
                    },
                    i_authorize_checkbox: {
                        required: true
                    }
                },
                messages: {
                    cardCVC: {
                        required: "Enter CVV",
                        minlength: "Must be min 3 number"
                    },
                    cardExpiry2: {
                        required: "Enter Expiry Year",
                        minlength: "Expiry year must be min 4 number. exp:2000"
                    },
                    cardExpiry: {
                        required: "Enter Expiry Month",
                        minlength: "Expiry month must be min 2 number"
                    },
                    cardfriendlyname: {
                        required: "Enter Card Friendly Name"
                    },
                    cardNumber: {
                        required: "Enter Card Number",
                        minlength: "Must be min 13 digits",
                        maxlength: "Must be max 16 digits"
                    },
                    accountNumber:{
                        required: "Enter Account Number",
                    },
                    confirmAccountNumber:{
                        required: "Enter Confirm Account Number",
                        equalTo: "Account number and confirm account number not match",
                    },
                    routeNumber:{
                        required: "Enter Routing Number",
                    }
                },
                submitHandler: function (form) {
                    $("#pay_btn").addClass("disabled noAction");
                    return true;
                }
            });

            $.validator.addMethod('CCExp', function(value, element, params) {
                var minMonth = new Date().getMonth() + 1;
                var minYear = new Date().getFullYear();
                var month = parseInt($(params.month).val(), 10);
                var year = parseInt($(params.year).val(), 10);

                return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
            }, 'Your Credit Card Expiration date is invalid.');
        });

        /* Get change process invoice tab */
        function get_process_details(eid) {
            var type = eid.value;
            $('#paymentType').val(type);

            if(type == 2){
                $('#creditCardView').hide();
                $('#eCheckView').show();
            }else{
                $('#eCheckView').hide();
                $('#creditCardView').show();
            }
        }
    </script>
</html>
