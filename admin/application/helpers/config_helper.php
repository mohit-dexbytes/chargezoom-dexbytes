<?php
function template_variable()
{
	$template = array(
		'name'              => 'Chargezoom',
		'version'           => 'ver. 2.23',
		'author'            => 'Chargezoom',
		'robots'            => 'noindex, nofollow',
		'title'             =>  'Chargezoom - Cloud Accounting Automation Software',
		'description'       => 'Chargezoom automates your payment processing and accounting reconciliation so you can focus on your business.',
		'page_preloader'    => true,
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['REQUEST_URI'])
	);
 
	return $template;	
}

     
function primary_nav()
{ 
         $sess_data=array();
         $CI = & get_instance(); 
         if( $CI->session->userdata('admin_logged_in'))
         $sess_data =  $CI->session->userdata('admin_logged_in');
         
      
         $power=array(1,2,3,4,5,6,7,8,9,10);
      $plans=$reseller=$dashboard=$merchants=$accounting=$gateways=$qbs=$config=$security=$reports=array();
      
      
      	$plans = array(
			'name'  => 'Plans',
			'icon'  => 'fa fa-sitemap',
			'url'   => base_url('Admin_panel/admin_plans'),
			'page_name'=> 'admin_plans'
		);
		$reseller = 	array(
			'name'  => 'Resellers',
			'icon'  => 'fa fa-users',
			 'sub'   => array(
                array(
				    'name'  => 'Resellers',
					'url'   => base_url('Admin_panel/reseller_list'),
		        	'page_name'=> 'reseller_list'
					),
				array(
				    'name'  => 'Disabled Resellers',
					'url'   => base_url('Admin_panel/disable_list'),
					'page_name'=> 'disable_list'
					)
					
			),
		
		);
		$dashboard  = array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('Admin_panel/index'),
			'page_name'=> 'index'
		);
		$merchants  = array(
			'name'  => 'Merchants',
			'icon'  => 'fa fa-users',
			 'sub'   => array(
                array(
				    'name'  => 'Merchants',
					'url'   => base_url('Admin_panel/merchant_list'),
		        	'page_name'=> 'merchant_list'
					),
				array(
				    'name'  => 'Disabled Merchants',
					'url'   => base_url('Admin_panel/disable_marchants_list'),
					'page_name'=> 'disable_marchants_list'
					)
					
			),
		
		);
		$accounting = array(
             'name' => 'Accounting',
             'icon' => 'fa fa-address-card',
               'sub' => array(
                   	array(
        						'name'  => 'Merchant Invoice',
        						'url'   =>  base_url('home/invoices'),
        						'page_name'=> 'invoices'
        	        ),
                   	array(
										'name'  => 'Accounting Package',
										'url'   => base_url('AdminQuickBook/QBOIndex'),
										'page_name'=> 'QBOIndex'
									),
        	    ) 
            
          );
         
        $gateways   =  array(
			'name'  => 'Gateway',
			'icon'  =>  'gi gi-lock',
			'url'   => base_url('home/admin_gateway'),
			'page_name'=> 'merchant_gateway'
		);  
		$reports    =  array(
			'name'  => 'Reports',
			'icon'  => 'gi gi-signal',
			 'sub'   => array(
                array(
				    'name'  => 'Reseller Reports',
					'url'   => base_url('Admin_panel/reseller_reporting'),
		        	'page_name'=> 'reseller_reporting'
					),
				array(
				    'name'  => 'Merchant Reports',
					'url'   => base_url('Admin_panel/merchant_reporting'),
					'page_name'=> 'merchant_reporting'
					)
					
			),
		);
		
		$config     = array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			 'sub'   => array(
			      array(
				    'name'  => 'Admin User',
					'url'   => base_url('home/admin_user'),
		        	'page_name'=> 'admin_user'
					),
			     
                array(
				    'name'  => 'Profile',
					'url'   => base_url('Admin_panel/admin_profile'),
		        	'page_name'=> 'admin_profile'
					),
				array(
				    'name'  => 'Settings',
					'url'   => base_url('Admin_panel/admin_setting'),
					'page_name' => 'admin_setting'
					),
				array(
				    'name'  => 'Email Templates',
					'url'   => base_url('Settingmail/email_template'),
					'page_name' => 'email_template'
					)	
			),
				
		);
		$security   = array(
		    'name' => 'Security',
		    'icon' => 'gi gi-shield',
		    'sub'   => array(
		         array(
		            'name'  => 'Admin',
        		    'url' =>base_url('home/security_log'),
        			 'page_name' => 'security_log'
        		    ),
        		    array(
		            'name'  => 'Reseller',
        		    'url' =>base_url('home/security_log_reseller'),
        			 'page_name' => 'security_log_reseller'
        		    ),
        		    array(
		            'name'  => 'Merchant',
        		    'url' =>base_url('home/security_log_merchant'),
        			 'page_name' => 'security_log_merchant'
        		    )
		    )
		    );
		    

	if(!empty($sess_data) && $sess_data['userAdminID']!=0)
	{
	      $m_array = $sess_data['previlege'];
	      
	    
	      
	      if(!in_array('1',$m_array))$plans=array();
	      if(!in_array('2',$m_array))$reseller=array();
	      if(!in_array('3',$m_array))$merchants=array();
	      if(!in_array('4',$m_array))$accounting=array();
	      if(!in_array('5',$m_array))$gateways=array();
	      if(!in_array('6',$m_array))$config=array();
	      if(!in_array('7',$m_array))$security=array();
          if(!in_array('8',$m_array))$dashboard=array(
                                    'name'  => 'Dashboard',
                                    'icon'  => 'gi gi-home',
                                    'url'   => base_url('Admin_panel/blank_dashboard'),
                                    'page_name'=> 'dashboard');
          if(!in_array('9',$m_array))$qbs=array();
          if(!in_array('10',$m_array))$reports=array();
	}
	
		$primary_nav = array(
		$dashboard,
    	$plans,
    	$reseller,
		$merchants,
		$accounting,	
        $gateways,
	    $qbs,
		$reports,
		$config,
		$security,
	);
 
	return $primary_nav;	
}



function calculate_revenue($merchantID,$val,$planID)
{
    
    $CI = & get_instance();
      
      $CI->load->model('list_model');
     
       $pl_data       =   $CI->list_model->get_row_data('plans',array('plan_id'=>$planID));
    
       $month_amount  = $val;
       $rental_amount = $pl_data['subscriptionRetail'];
       $service_fee   =  $pl_data['serviceFee']; 
       $balance       = 0; 
         if($pl_data['planType']=='1')
	            {
	              	$total1  = $month_amount * ($service_fee/100);   
	                $balance  =$rental_amount+$total1;
	            }
	            
	            
	             if($pl_data['planType']=='2')
	             {
	                  if($val > 0)
	                  $tr_cnt = $CI->list_model->get_total_transaction($merchantID);
	                  else 
	                  $tr_cnt = 0;
	                
	                 $total1 =  $service_fee*$tr_cnt;
	                  $balance  =$rental_amount+$total1;
	                 
	             }
	         
	             if($pl_data['planType']=='3')
	             {
	                  $total1=0;
	                   $balance  =$rental_amount+$total1;
	                 
	             }
	             
	             
	     return number_format($balance,2);         
    
}