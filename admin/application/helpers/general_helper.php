<?php
	function mymessage($message)
	{
		echo '
		<div class="alert alert-success">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$message.'
		</div>
		';
	}
	
	function getmessage($error)
	{
		echo '
		<div class="alert alert-danger">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$error.'
		</div>
		';
	}
	
	
	function get_services()
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_table('*', 'services');
		return $service;
	}


	function get_services_by_id($id)
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_allservices_by_id($id);
		return $service;
	}
	
	
	function merchant_gateway_allowed($args){
		$CI = &get_instance();
		$isAllowed = false;
		
		$merchant_condition = [
			'merchID' => $args['merchID'],
			'allowGateway' => 1
		];

		$userData = $CI->general_model->get_row_data('tbl_merchant_data', $merchant_condition);
		if ($userData) {
			$isAllowed = true;
		}

		return $isAllowed;
	}

	function getGatewayNames($gatewayType){
		$gateway = 'Unknown';
		switch ($gatewayType) {
			case 1:
				$gateway = 'NMI';
				break;
			case 2:
				$gateway = 'Authorize.Net';
				break;
			case 3:
				$gateway = 'Paytrace';
				break;
			case 4:
				$gateway = 'Paypal';
				break;
			case 5:
				$gateway = 'Stripe';
				break;
			case 6:
				$gateway = 'USAePay';
				break;
			case 7:
				$gateway = 'Heartland';
				break;
			case 8:
				$gateway = 'Cybersource';
				break;
			case 9:
				$gateway = 'Chargezoom';
				break;
			case 10:
				$gateway = iTransactGatewayName;
				break;
			case 11:
				$gateway = FluidGatewayName;
				break;
			case 12:
				$gateway = TSYSGatewayName;
				break;
			case 13:
				$gateway = BASYSGatewayName;
				break;
			case 16:
				$gateway = EPXGatewayName;
				break;
			case 14:
				$gateway = 'CardPointe';
				break;
			case 15:
				$gateway = PayArcGatewayName;
			break;
			case MAVERICKGATEWAYID:
				$gateway = MaverickGatewayName;
			break;
			default:
				$gateway;
				break;
		}
		return $gateway;
	}

	// function for convert time 
	function getTimeBySelectedTimezone($timezone)
	{
		$date = new DateTime($timezone['time'], new DateTimeZone($timezone['current_format']));
		$date->setTimezone(new DateTimeZone($timezone['new_format']));
		return $date->format('Y-m-d H:i:s');
	}
	
	function delete_cache($uri_string=null)
	{
		$CI =& get_instance();
		$path = $CI->config->item('cache_path');
		$path = rtrim($path, DIRECTORY_SEPARATOR);

		$cache_path = ($path == '') ? APPPATH.'cache/' : $path;

		$uri =  base_url($uri_string);
		$cache_path .= md5($uri);

		if (file_exists($cache_path)){
			return unlink($cache_path);
		} else {
			header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
		}

		return true;
	}


	function getClientIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	function getTableRowLengthValue($length)
	{
		$returnValue = 10;
		if($length >= 0){
			$returnValue = $length;
		}
		return $returnValue;
	}
	function getTableRowOrderColumnValue($columnNumber)
	{
		$returnValue = 0;
		if($columnNumber >= 0){
			$returnValue = $columnNumber;
		}
		return $returnValue;
	}
	function getTableRowStartValue($start)
	{
		$returnValue = 0;
		if($start >= 0){
			$returnValue = $start;
		}
		return $returnValue;
	}
	function getTableRowOrderByValue($orderBy)
	{
		$returnValue = 'desc';
		if($orderBy == 'desc' || $orderBy == 'asc'){
			$returnValue = $orderBy;
		}
		return $returnValue;
	}

	/**
     * Function to send email using Send Grid
     *
	 * @param string $toEmail
	 * @param string $subject
	 * @param string $fromEmail
	 * @param string $senderName
	 * @param string $message
	 * @param string $replyTo
	 * @param string $addCC
	 * @param string $addBCC
	 * 
     * @return bool
     */
	function sendMailBySendgrid(string $toEmail, string $subject, string $fromEmail, string $senderName, string $message, string $replyTo, string $addCC = '', string $addBCC = '') : bool
	{
		$toaddCCArrEmail = [];
		if($addCC != ''){
			$toaddCCArr = (explode(";",$addCC));
			foreach ($toaddCCArr as $value) {
				$toaddCCArrEmail[]['email'] = trim($value);
			}
		}
		
		$toaddBCCArrEmail = [];
		if($addBCC != ''){
			$toaddBCCArr = (explode(";",$addBCC));
			foreach ($toaddBCCArr as $value) {
				$toaddBCCArrEmail[]['email'] = trim($value);
			}
		}

		$request_array = [
			"personalizations" => [
				[
				"to" => [
					[
					"email" => $toEmail
					]
				],
				"cc"=> $toaddCCArrEmail,
				"bcc"=>$toaddBCCArrEmail,
				"subject" => $subject
				]
			],
			"from" => [
				"email" => $fromEmail,
				"name" => $senderName
			],
			"reply_to" => [
				"email" => $replyTo
			],
			"content" => [
				[
				"type" => "text/html",
				"value" => $message
				]
			]
		];

		if(count($toaddCCArrEmail) == 0){
			unset($request_array['personalizations'][0]['cc']);
		}
		if(count($toaddBCCArrEmail) == 0){
			unset($request_array['personalizations'][0]['bcc']);
		}

		// get api key for send grid
		$api_key = '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';
		$url = 'https://api.sendgrid.com/v3/mail/send';

		// set authorization header
		$headerArray = [
			'Authorization: Bearer '.$api_key,
			'Content-Type: application/json',
			'Accept: application/json'
		];

		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		// add authorization header
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

		$response = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// parse header data from response
		$header_text = substr($response, 0, $header_size);
		// parse response body from curl response
		$body = substr($response, $header_size);

		$header = array();
		

		// if mail sent success
		if($httpcode == '202' || $httpcode == '200'){
			$success = true;
		}else{
			$success = false;
		}
		return $success;
	}

	/**
	 * Function to create invoice public invoice URL
	 * 
	 * @param string $invoiceID
	 * @param int $merchantID
	 * 
	 * @return string 
	 */
	function createPublicInvoiceLink(string $invoiceID, int $merchantID) : string
	{
		$str      = 'abcdefghijklmnopqrstuvwxyz01234567891011121314151617181920212223242526';
		$shuffled = str_shuffle($str);
		$shuffled = substr($shuffled, 1, 12) . '=' . $merchantID;
		$codeMerchantID     =    safe_encode($shuffled);
		$invcode    =  safe_encode($invoiceID);

		#Redirection managed at route
		return base_url("merchant_invoice_pay/$invcode/$codeMerchantID");
	}

	/**
	 * Function used to encrypt strings
	 * 
	 * @param string $string
	 * 
	 * @return string 
	 */
	function safe_encode(string $string) : string
	{
		return strtr(base64_encode($string), '+/=', '-_-');
	}

