<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['Sandbox'] = TRUE;

$config['GLOBAL_URL'] = getenv('GLOBAL_PAYMENT_URL');
$config['PRO_GLOBAL_URL'] = sgetenv('GLOBAL_PAYMENT_URL');

$config['DeveloperId']   ='002914';
$config['VersionNumber'] ='3411'; 