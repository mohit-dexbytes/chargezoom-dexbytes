<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting Cybersource's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
require_once dirname(__FILE__) . '../../../../vendor/autoload.php';

 require_once dirname(__FILE__) . '../../../../cybersource-rest-samples-php/Resources/ExternalConfiguration.php';
$config['Sandbox'] 		 = TRUE;
$config['SandboxENV']    = "cyberSource.environment.SANDBOX";
$config['ProductionENV'] = "cyberSource.environment.PRODUCTION";
