<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Turn on some error reporting
error_reporting(E_ALL);
ini_set('display_errors', 1);

// Turn off auto-loading (possible conflict with CI?)
define('QUICKBOOKS_LOADER_AUTOLOADER', true);



/**
 * Configuration parameter for the quickbooks_config table, used to keep track of the last time the QuickBooks sync ran
 */
define('QB_QUICKBOOKS_CONFIG_LAST', 'last');

/**
 * Configuration parameter for the quickbooks_config table, used to keep track of the timestamp for the current iterator
 */
define('QB_QUICKBOOKS_CONFIG_CURR', 'curr');

/**
 * Maximum number of customers/invoices returned at a time when doing the import
 */
define('QB_QUICKBOOKS_MAX_RETURNED', 10);

/**
 * 
 */
define('QB_PRIORITY_PURCHASEORDER', 4);

/**
 * Request priorities, items sync first
 */
define('QB_PRIORITY_ITEM', 3);

/**
 * Request priorities, customers
 */
define('QB_PRIORITY_CUSTOMER', 10);

/**
 * Request priorities, salesorders
 */
define('QB_PRIORITY_SALESORDER', 1);

/**
 * Request priorities, invoices last... 
 */
define('QB_PRIORITY_INVOICE', 0);

// A username and password you'll use in: 
//	a) Your .QWC file
//	b) The Web Connector
//	c) The QuickBooks framework
//
// 	NOTE: This has *no relationship* with QuickBooks usernames, Windows usernames, etc. 
// 		It is *only* used for the Web Connector and SOAP server! 
$config['quickbooks_user'] = 'quickbooks';
$config['quickbooks_pass'] = 'password';

// Time zone that QuickBooks is in
$config['quickbooks_tz'] = 'America/Los_Angeles';
//  Memory limit
$config['quickbooks_memorylimit'] = '512M';
