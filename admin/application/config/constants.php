<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
//logo image
define("CZLOGO", getenv('CZLOGO'));
define("CZHEADERLOGO", getenv('CZHEADERLOGO'));
define("RSURL", getenv('RSURL'));

define("ENCRYPTION_KEY", getenv('ENCRYPTION_KEY_CARD'));
define("CURRENCY", 'USD');
define("DEFAULT_FROM_EMAIL", "donotreply@payportal.com");
define("DEFAULT_TIMEZONE", 'America/Los_Angeles');


define('IMAGES','resources/img/');
define('JS','resources/js/');
define('CSS','resources/css/');
define('UPLOAD','uploads/');
define("RELEASE_VERSION", '2.0.1');
define("RSDOMAIN", getenv('RSDOMAIN'));
define("LOGOWIDTH", '300');
define("LOGOHEIGHT", '100');

define('iTransactGatewayName', 'iTransact');
define('FluidGatewayName', 'FluidPay');
define('TSYSGatewayName', 'TSYS');
define('BASYSGatewayName', 'BASYS iQ Pro');
define('EPXGatewayName', 'EPX');

define('PayArcGatewayName', 'PayArc Payment');
define('MaverickGatewayName', 'Maverick Payment');


define('HATCHBUCK_API_KEY', getenv('HATCHBUCK_API_KEY'));
define('HATCHBUCK_API_URL', getenv('HATCHBUCK_API_URL'));
define('HATCHBUCK_FIRST_SETUP_WIZARD', getenv('HATCHBUCK_FIRST_SETUP_WIZARD'));
define('HATCHBUCK_PAYMENT_INFO', getenv('HATCHBUCK_PAYMENT_INFO'));
define('HATCHBUCK_FIRST_TRANSACTION', getenv('HATCHBUCK_FIRST_TRANSACTION'));
define('HATCHBUCK_RESELLER_ID', 23);
define('MAVERICKGATEWAYID', 17);

define("MAXRESULT", 100);
define("DEFAULT_INVOICE_PREFIX", "CHZ");
/* End of file constants.php */
/* Location: ./application/config/constants.php */