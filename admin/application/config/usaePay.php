<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting PayPal's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
 
 $config['Sandbox'] = TRUE;
 $config['TESTMode'] = 0;
 $config['ignoresslcerterrors'] = TRUE;
