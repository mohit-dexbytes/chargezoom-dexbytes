<?php
class ChargezoomStripe {
    public $stripe_api_ver = '2020-08-27';
    ////////////////////
    // Load and set APP Info //
    ////////////////////
    public function __construct()
    {
       // parent::__construct();
        require_once APPPATH . 'plugins/Chargezoom-Stripe/stripe-php/init.php';
        require_once(APPPATH.'plugins/Chargezoom-Stripe/stripe-php/lib/Stripe.php');
        $data = \Stripe\Stripe::setAppInfo( 'Chargezoom MyStripePlugin', '2.2.1', 'https://chargezoom.com', 'pp_partner_FBuWxWYWhg4UQO' );
        
    }
    

    ////////////////////
    // Load and set API KEY //
    ////////////////////
    public function setApiKey($apiKey) {
    	
    	\Stripe\Stripe::setApiKey($apiKey);
    	
        return true;
    }
    ////////////////////
    // Load and set stripe version //
    ////////////////////
    public function setApiVersion($version) {
    	
    	\Stripe\Stripe::setApiVersion( $version );
    	return true;
    }

    public function paymentFormsDefault(){
        $html = '<script src="https://js.stripe.com/v3/"></script>
            <form action="/charge" method="post" id="payment-form">
              <div class="form-row">
                <label for="card-element">
                  Credit or debit card
                </label>
                <div id="card-element">
                  <!-- A Stripe Element will be inserted here. -->
                </div>

                <!-- Used to display form errors. -->
                <div id="card-errors" role="alert"></div>
              </div>

              <button>Submit Payment</button>
            </form>';
    }
}