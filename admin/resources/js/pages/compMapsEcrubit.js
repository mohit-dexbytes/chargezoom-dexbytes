/*
 *  Document   : compMaps.js
 *  Author     : Ecrubit
 *  Description: Custom javascript code used in Maps page
 */


var CompMapsEcrubit_Gmap_Top = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '350px');

            // Initialize top map
            new GMaps({
                div: '#ecrubit_gmap-top',
                lat: -33.865,
                lng: 151.20,
                zoom: 15,
                disableDefaultUI: true,
                scrollwheel: false
            });
        }
    };
}();


var CompMapsEcrubit_Gmap_Terrain = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '350px');

            // Initialize terrain map
            new GMaps({
                div: '#ecrubit-gmap-terrain',
                lat: 0,
                lng: 0,
                zoom: 1,
                scrollwheel: false
            }).setMapTypeId(google.maps.MapTypeId.TERRAIN);
        }
    };
}();

var CompMapsEcrubit_Gmap_Satellite = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '350px');

            // Initialize satellite map
            new GMaps({
                div: '#ecrubit-gmap-satellite',
                lat: 0,
                lng: 0,zoom: 1,
                scrollwheel: false
            }).setMapTypeId(google.maps.MapTypeId.SATELLITE);
        }
    };
}();

var CompMapsEcrubit_Gmap_Markers = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '450px');

            new GMaps({
                div: '#ecrubit-gmap-markers',
                lat: 21.0000,
                lng: 78.0000,
                zoom: 5,
                scrollwheel: false
            }).addMarkers([
                {lat: 28.38, lng: 77.12, title: 'Franchise Delhi', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Delhi</strong>'}},
                {lat: 20.00, lng: 76.00, title: 'Franchise Amravati', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Amravati</strong>'}},
                {lat: 18.5203, lng: 73.8567, title: 'Franchise Pune', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Pune</strong>'}},
                {lat: 18.9750, lng: 72.8258, title: 'Franchise Mumbai', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Mumbai</strong>'}},
				{lat: 20.0000, lng: 73.7800, title: 'Franchise Nashik', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Nashik</strong>'}},
				{lat: 18.7481, lng: 73.4072, title: 'Franchise Lonawala', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Lonawala</strong>'}},
				{lat: 22.5667, lng: 88.3667, title: 'Franchise Kolkata', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Kolkata</strong>'}},
				{lat: 17.3700, lng: 78.4800, title: 'Franchise Hyderabad', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Hyderabad</strong>'}},
				{lat: 13.0827, lng: 80.2707, title: 'Franchise Chennai', animation: google.maps.Animation.DROP, infoWindow: {content: '<strong>ABC Systems<br /> Chennai</strong>'}},
            ]);
        }
    };
}();

var CompMapsEcrubit_Gmap_Street = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '350px');

             // Initialize street view panorama
            new GMaps.createPanorama({
                el: '#ecrubit-gmap-street',
                lat: 50.059139,
                lng: -122.958407,
                pov: {heading: 300, pitch: 5},
                scrollwheel: false
            });
        }
    };
}();

var CompMapsEcrubit_Gmap_Geolocation = function() {

    return {
        init: function() {
            /*
             * With Gmaps.js, Check out examples and documentation at http://hpneo.github.io/gmaps/examples.html
             */

            // Set default height to all Google Maps Containers
            $('.gmap').css('height', '350px');

             // Initialize street view panorama
            // Initialize map geolocation
            var gmapGeolocation = new GMaps({
                div: '#ecrubit-gmap-geolocation',
                lat: 0,
                lng: 0,
                scrollwheel: false
            });
        }
    };
}();

