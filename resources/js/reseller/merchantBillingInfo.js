function updateBillingInfo(e){
    if ($(e).prop('checked')==true){ 
        $('#billingfirstName').val($('#firstName').val());
        $('#billinglastName').val($('#lastName').val());
        $('#billingContact').val($('#merchantContact').val());
        $('#billingEmailAddress').val($('#merchantEmail').val());
        $('#billingAddress1').val($('#merchantAddress1').val());
        $('#billingCity').val($('#merchantCity').val());
        $('#billingState').val($('#merchantState').val());
        $('#billingPostalCode').val($('#merchantZipCode').val());
    }
}