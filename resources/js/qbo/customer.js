function set_template_data_qbo(c_id,cmp_name,cmp_id,cust_Email,replyEmail='' ){
   document.getElementById('customertempID').value=c_id;
   document.getElementById('tempCompanyID').value=cmp_id;
   document.getElementById('tempCompanyName').value=cmp_name;
   document.getElementById('toEmail').value=cust_Email;
   if(replyEmail != ''){
      document.getElementById('replyEmail').value = replyEmail;
   }
}


$(function () {
   $('#open_cc').click(function () {
      $('#cc_div').show();
      $(this).hide();
   });
   $('#open_bcc').click(function () {
      $('#bcc_div').show();
      $(this).hide();
   });
   $('#open_reply').click(function () {
      $('#reply_div').show();
      $(this).hide();
   });


   $('#open_from_email').click(function () {
      $('#from_email_div').show();
      $(this).hide();
   });
   $('#open_display_name').click(function () {
      $('#display_name_div').show();
      $(this).hide();
   });

   $('#type').change(function () {
      var templateID = $(this).val();
      var customerID = $('#customertempID').val();
      if (templateID != "") {
         $.ajax({
            type: "POST",
            url: base_url + 'QBO_controllers/Settingmail/set_template_cus_details',
            data: { templateID: templateID, customerID: customerID },
            success: function (data) {
               data = $.parseJSON(data);
               CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
               $('#textarea-ckeditor').html(data['message']);
               $('#emailSubject').val(data['emailSubject']);
               $('#toEmail').val(data['toEmail']);
               $('#ccEmail').val(data['addCC']);
               $('#bccEmail').val(data['addBCC']);
               $('#replyEmail').val(data['replyTo']);
            }
         });
      }
   });
   // Toolbar groups configuration.
      CKEDITOR.config.toolbarGroups = [
         { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
         { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
         { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ] },
         //{ name: 'forms' },
         '/',
         { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
         { name: 'paragraph', groups: [  'align', 'bidi' ] },
         { name: 'links' },
         { name: 'insert' },
         '/',
         { name: 'styles' },
         { name: 'colors' },

      ];
     /* Initialize Form Validation */
      $('#form-validation1').validate({
                ignore: [] ,
                rules: {
                 replyEmail: {
                     required: true,
                     email: true
                 },
            
               emailSubject: {
                    required: true,
                    minlength: 2,
               }
             },
           
         });
 
});
