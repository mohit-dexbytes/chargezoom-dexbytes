var customerListIDJS = $("#customerID").val();
if (typeof customerListIDJS === undefined){
  customerListIDJS = "";
}
if (typeof InvoiceCount === undefined) {
    var InvoiceCount = 0;
}

$(function() {
  if(customerListIDJS){
    Pagination_view_invoice.init();
    $(document).on('change', '.status_filter', function() {
        status_filter = $(this).val();
        $('#invoice_page').DataTable().draw();
    });
  }
});
var status_filter = 'open';
var Pagination_view_invoice = function() {
	return {
		init: function() {
		/ Extend with date sort plugin /
		$.extend($.fn.dataTableExt.oSort, {

		});

		/ Initialize Bootstrap Datatables Integration /
		App.datatables();

		/ Initialize Datatables /
		$('#invoice_page').dataTable({
			"processing": true,
			"serverSide": true, //Feature control DataTables' server-side processing mode.
				"lengthMenu": [[10, 50, 100, 500], [10, 50, 100,   500]],
					"pageLength": 10,
				// Load data for the table's content from an Ajax source
				"ajax": {
					"url": base_url+"FreshBooks_controllers/Freshbooks_invoice/ajax_invoices_list",
					"type": "POST" ,
					
					"data":function(data) {
					
					data.status_filter = status_filter;
					data.customerID = customerListIDJS;
				},
					
				},
				"order": [[1, 'desc']],
				
					"sPaginationType": "bootstrap",
			"stateSave": true 

			});


		/ Add placeholder attribute to the search input /
		$('.dataTables_filter input').attr('placeholder', 'Search');
		
		if(customerListIDJS){
			
			$('#invoice_page_filter').prepend('<a class="btn btn-sm  btn-success" title="Create New"  href="'+base_url+'FreshBooks_controllers/Freshbooks_invoice/add_invoice/'+customerListIDJS+'">Add Invoice</a>');
			$('#invoice_page_filter label').append('<select class="form-control status_filter" name="status_filter"><option value="open">Open</option><option value="Past Due">Overdue</option><option value="Partial">Partial</option><option value="Unpaid">Unpaid</option><option value="Failed">Failed</option><option value="Paid">Paid</option><option value="Cancelled">Voided</option><option value="All">All</option></select>');
			$('#invoice_page_filter').parent('div').addClass('col-sm-11').removeClass('col-sm-6');
			$('#invoice_page_length').parent('div').addClass('col-sm-1').removeClass('col-sm-6');
				
			if(InvoiceCount > 0){
				$('#invoice_page_filter').prepend('<a href="#fb_invoice_multi_process" class="btn btn-sm btn-success batch_button_color" data-backdrop="static" data-keyboard="false" data-toggle="modal" style="margin-right: 10px;" onclick="set_fb_invoice_process_multiple('+customerListIDJS+');">Batch Process</a>');
			}
		}    
		

		$("#search").on('keyup', function (){
			$('#invoice_page').DataTable().search( this.value ).draw();
		});
		}
	};
}();

function invoice_tax_list(itemRoom){
	var taxItems = $('#taxItems').html();
	var checkedTaxJSON = $(`#totalTaxItems${itemRoom}`).val();
	if(checkedTaxJSON !== ''){
		checkedTaxJSON = JSON.parse(checkedTaxJSON);
	}

	if(taxItems != ""){
		taxItems = JSON.parse(taxItems);
		var invItem = '';
		for (var t = 0; t < taxItems.length; t++) {
			var checked = '';
			var selectedTaxItem = $.grep(checkedTaxJSON, function(v,i) {
				return v.id === taxItems[t].id;
			})[0];

			if(selectedTaxItem !== undefined){
				checked = 'checked';
			}

			invItem += `<div class="form-group alignTableInvoiceList">
				<div class="col-md-1 text-center">
					<input type="checkbox" ${checked} class="chk_pay tax${itemRoom}" onclick="" name="multi_inv[]" value="1" tax-id="${taxItems[t].id}">
				</div>
				<div class="col-md-4 text-center">${taxItems[t].friendlyName}</div>
				<div class="col-md-3 text-center">${taxItems[t].number}</div>
				<div class="col-md-3 text-left">
					<input type="text" class="form-control  geter" readonly data-value="${taxItems[t].taxRate}" value="${taxItems[t].taxRate}" id="tax-rate${itemRoom}">
				</div>
			</div>`;
		}
		$('#inv_tax_item').html(invItem);
	}
	
	$('#invItemAddButton').attr('itemRoom',itemRoom);
}

function addTaxInvoice(){
	var itemRoom = $('#invItemAddButton').attr('itemRoom');
	var taxItems = $('#taxItems').html();
	if(taxItems != ""){
		taxItems = JSON.parse(taxItems);
	}

	var checkedTaxJSON = [];
	var checkedTaxTitle = [];

	var totalTaxRate = 0;
	$(`.tax${itemRoom}`).each(function () {
		
		if ($(this).is(':checked')) {
			var taxID = $(this).attr('tax-id');
			var selectedTaxItem = $.grep(taxItems, function(v,i) {
				return (v.id === taxID);
			})[0];

			totalTaxRate += parseFloat(selectedTaxItem.taxRate);
			checkedTaxTitle.push(`${selectedTaxItem.friendlyName} (${selectedTaxItem.taxRate}%)`);
			checkedTaxJSON.push(selectedTaxItem);
		}
	});

	if(checkedTaxTitle.length != 0){
		$(`#taxButton${itemRoom}`).addClass('cust_view');
		checkedTaxTitle = checkedTaxTitle.join(", ");
		$(`#taxButton${itemRoom}`).html(`<a href="#fb_invoice_tax_modal" onclick="invoice_tax_list(${itemRoom})" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-original-title="" title="">${checkedTaxTitle}</a>`);
	} else {
		$(`#taxButton${itemRoom}`).removeClass('cust_view');
		$(`#taxButton${itemRoom}`).html(`<a href="#fb_invoice_tax_modal" class="btn btn-sm btn-info" onclick="invoice_tax_list(${itemRoom})" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-original-title="" title=""><strong> + </strong></a>`);
	}

	checkedTaxJSON = JSON.stringify(checkedTaxJSON);
	$(`#totalTaxItems${itemRoom}`).val(checkedTaxJSON);
	$(`#totalTaxRate${itemRoom}`).val(totalTaxRate);

	if ( typeof set_tax_val == 'function' ) { 
		set_tax_val(itemRoom);
	}
	
	if ( typeof calculateAmountInv == 'function' ) { 
		calculateAmountInv();
	}
	return true;
}