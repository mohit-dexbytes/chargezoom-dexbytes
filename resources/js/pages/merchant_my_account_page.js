  
  var base_url = $('#js_base_url').val();
   var gtype ='';    
 
function set_invoice_schedule_id(id,cid,in_val, ind_date)
{
   
}      


 function set_view_history(eID)
 {
  
}


var Pagination_email = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();
            
              $.fn.DataTable.ext.pager.numbers_length = 3;

                $.fn.DataTable.ext.pager.numbers_no_ellipses = function(page, pages){
                       var numbers = [];
                       var buttons = $.fn.DataTable.ext.pager.numbers_length;
                       var half = Math.floor( buttons / 2 );
                     
                       var _range = function ( len, start ){
                          var end;
                        
                          if ( typeof start === "undefined" ){
                             start = 0;
                             end = len;
                     
                          } else {
                             end = start;
                             start = len;
                          }
                     
                          var out = [];
                          for ( var i = start ; i < end; i++ ){ out.push(i); }
                        
                          return out;
                       };
                         
                     
                       if ( pages <= buttons ) {
                          numbers = _range( 0, pages );
                     
                       } else if ( page <= half ) {
                          numbers = _range( 0, buttons);
                     
                       } else if ( page >= pages - 1 - half ) {
                          numbers = _range( pages - buttons, pages );
                     
                       } else {
                          numbers = _range( page - half, page + half + 1);
                       }
                     
                       numbers.DT_el = 'span';
                     
                       return [ numbers ];
                    };  

            / Initialize Datatables /
            $('#email_hist').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                 pagingType: 'numbers_no_ellipses'
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

  
var Pagination_view1 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#outstand').dataTable({
                columnDefs: [
                    { type: 'date', targets: [0, 1] },
                    { orderable: false, targets: [5] }
                ],
                 
                order: [[1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

  
var Pagination_view2 = function() { 
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date', targets: [1] },
                    { orderable: false, targets: [5] }
                ],
                 
                order: [[ 1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });
            

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
var Pagination_view3 = function() { 
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            
             $('#subs').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0, 1] },
                    { orderable: false, targets: [4] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


        
        
$(function(){ 
       
      
       
       
  $('#btn_mask_account').click(function(){
    $('#m_account_id').hide();  
    
    $('.acc_number_manage').show();
  });

  $('.billing_phone_number').mask('999-999-9999'); 

  $('#btn_mask_card').click(function(){
    $('#m_card_id').hide();  
    
    $('.card_number_manage').show();
  });


$('.radio_pay').click(function(){
  var method = $(this).val(); 
   if(method==1)
   {
     $('#checkingform').hide();
     $('#ccform').show();
   }else if (method==2){
     $('#checkingform').show();
     $('#ccform').hide();
   }else {
     
   }       
 });
   

        Pagination_view1.init();
        Pagination_view2.init();
       Pagination_email.init();  
     Pagination_view3.init(); 
           
        $('#open_cc').click(function(){
                $('#cc_div').show();
            $(this).hide();
      });
        $('#open_bcc').click(function(){
                $('#bcc_div').show();
             $(this).hide();
      });
       $('#open_reply').click(function(){
                $('#reply_div').show();
             $(this).hide();
      });
    
          /*********************Template Methods**************/ 

  $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
            minlength: 13,
                        maxlength: 16,
              number: true
                    },
          
           edit_expiry_year: {
                CCExp: {
                  month: '#edit_expiry',
                  required: true,
                  year: '#edit_expiry_year'
                }
            },
          
                      
           edit_cvv: {
                        number: true,
            minlength: 3,
                        maxlength: 4,
                    },
                    
                    baddress1:{
            
          },
          
          edit_friendlyname:{
            
             minlength: 3,
          },
        
          bcountry:{
            
          },
          
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  },
                  
                  
                },
              
            });
     $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
          
                    expiry_year: {
                        CCExp: {
                          month: '#expiry',
                          year: '#expiry_year'
                        }
                    },
          
                      
                    cvv: {
                       // required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number:{
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                   route_number:{
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 12,
                   },
                   acc_name:{
                    required: true,
                    minlength: 3,
                    maxlength: 30,
                   },
                  friendlyname:{
                      required:true,
                      minlength: 3,
                  },
                  billing_state:{
                  },
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  billing_first_name:{
                  },
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  },
                  
                },
                messages: {
                    acc_number: {
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 3 digits.',
                        maxlength: 'Please enter no more than 12 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                }
            });
          $('#thestModal').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
          
                    expiry_year: {
                        CCExp: {
                          month: '#expiry',
                          year: '#expiry_year'
                        }
                    },
          
                      
                    cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number:{
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                   route_number:{
                        required: true,
                        digits: true,
                        noDecimal: true,
                        minlength: 9,
                        maxlength: 9,
                   },
                   acc_name:{
                    required: true,
                    minlength: 3,
                    maxlength: 30,
                   },
                  friendlyname:{
                      required:true,
                      minlength: 3,
                  },
                  billing_state:{
                  },
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  billing_first_name:{
                  },
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  },
                  
                },
                messages: {
                    acc_number: {
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 9 digits.',
                        maxlength: 'Please enter no more than 9 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                }
            });
          $.validator.addMethod("noDecimal", function(value, element) {
                return !(value % 1);
            }, "Please enter valid routing number.");
           $('#del_ccjkck_o').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    payment_date: {
                        required: true,
          
                    },
          
           check_number: {
                required: true,
                minlength: 3,
                               maxlength: 20,
            },
             
                    
               inv_amount: {
                required: true,
                number:true,
            },  
                  
                  
                },
              
            });
      
        
           $('#Qbd_form1_schedule').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    schedule_date: {
                        required: true,
          
                    },
              card_number: {
                        required: true,
            minlength: 13,
                        maxlength: 16,
              number: true
                    },
          
           expiry_year: {
                CCExp: {
                  month: '#expiry11',
                  year: '#expiry_year11'
                }
            },
          
                      
           cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
           sch_method: {
                required: true,
            },
               sch_gateway: {
                required: true,
            },  
            
             schCardID: {
                required: true,
                
            },
               schAmount: {
                required: true,
                number:true
            },  
                   
                  
                  
                },
              
            });
      
          
 $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
                  
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');









});
  

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
          
                   expiry_year: {
                        CCExp: {
                          month: '#expiry',
                          year: '#expiry_year'
                        }
                    },
                  
                              
                   cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number:{
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                  route_number:{
                        required: true,
                        digits: true,
                        noDecimal: true,
                        minlength: 9,
                        maxlength: 9,
                  },
                  acc_name:{
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                  },
                  friendlyname:{
                     required:true,
                     minlength: 3,
                  },
                  billing_state:{
                  },
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  billing_first_name:{
                  },
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  },
                  
                },
                 messages: {
                    acc_number: {
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 9 digits.',
                        maxlength: 'Please enter no more than 9 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                  }
              
            });
            $('#thestModal').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
          
                   expiry_year: {
                        CCExp: {
                          month: '#expiry',
                          year: '#expiry_year'
                        }
                    },
                  
                              
                   cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number:{
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                    },
                  route_number:{
                        required: true,
                        digits: true,
                        noDecimal: true,
                        minlength: 9,
                        maxlength: 9,
                  },
                  acc_name:{
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                  },
                  friendlyname:{
                     required:true,
                     minlength: 3,
                  },
                  billing_state:{
                  },
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  billing_first_name:{
                  },
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  },
                  
                },
                 messages: {
                    acc_number: {
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 9 digits.',
                        maxlength: 'Please enter no more than 9 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                  }
            });
            $.validator.addMethod("noDecimal", function(value, element) {
                return !(value % 1);
            }, "Please enter valid routing number.");

  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
      
           

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
            minlength: 13,
                        maxlength: 16,
              number: true
                    },
          
           edit_expiry_year: {
                CCExp: {
                  month: '#edit_expiry',
                  required: true,
                  year: '#edit_expiry_year'
                }
            },
          
                      
           edit_cvv: {
                        number: true,
            minlength: 3,
                        maxlength: 4,
                    },
                    
                    baddress1:{
            
          },
          
          edit_friendlyname:{
            
             minlength: 3,
          },billing_state:{
                  },
                  billing_city:{
                      minlength:2,
                  },
                  billing_zipcode:{
                    
                    minlength:3,
                    maxlength:10,
                    number:true,

                  },
                  billing_phone_number:{
                    
                     minlength: 10,
                     maxlength: 17,
                  },
                  billing_first_name:{
                  },
                  billing_email:{
                     email:true
                  },
                  billing_address:{
                  }, 
                  
                },
              
            });
            
         $.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
        return  value.match(/^\d{10}$/) || 
             value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
                            
  $.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
              
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
      
      

         
        }
    };
}();

  

    
var addvalidation= function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#del_ccjkck_o').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    payment_date: {
                        required: true,
          
                    },
          
           check_number: {
                required: true,
                minlength: 6,
                               maxlength: 9,
            },
             
                    
               inv_amount: {
                required: true,
                number:true,
            },  
                  
                  
                },
              
            });
      
 
    
        }
    };
}();

  
var schvalidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#Qbd_form1_schedule').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    schedule_date: {
                        required: true,
          
                    },
              card_number: {
                        required: true,
            minlength: 13,
                        maxlength: 16,
              number: true
                    },
          
           expiry_year: {
                CCExp: {
                  month: '#expiry',
                  year: '#expiry_year'
                }
            },
          
                      
           cvv: {
                        number: true,
            minlength: 3,
                        maxlength: 4,
                    },
           sch_method: {
                required: true,
            },
               sch_gateway: {
                required: true,
            },  
            
             schCardID: {
                required: true,
                
            },
               schAmount: {
                required: true,
                number:true
            },  
                   
                  
                  
                },
              
            });
      
          
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
      
    
        }
    };
}();

    
  
  
function delele_notes(note_id){
  
  
}

function add_notes(){
    
   
    
}

 function set_card_user_data(id, name){
      
     $('#customerID11').val(id);
     $('#customername').val(name);
 }
     
  function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();   
     $('#editccform').css('display','none');
     $('#editcheckingform').css('display','none');
    
       
  } 
  

                                          

 function set_qbd_url_multi_pay()
 {
 
          
        var gateway_value =$("#gateway1").val();
    
      
  } 

 
  
function set_invoice_process_id(id, cid, in_val,pag)
{
  
}


 function set_url()
 {  
 
      
}
  
function set_qbd_invoice_process_multiple(cid)
{

     
     
}
  
  function set_template_data_company(c_id,cmp_name,cmp_id,cust_Email ){
    
     
            document.getElementById('customertempID').value=c_id;
       document.getElementById('tempCompanyID').value=cmp_id;
       document.getElementById('tempCompanyName').value=cmp_name;
       document.getElementById('toEmail').value=cust_Email;
} 
  
function qbd_schedule_payment()
{
   
    
    return false;


}
  function set_template_data_temp(invoiceID, custID,typeID ){
    
     
  } 

   $(function(){  
    
         
      
          $('#open_cc').click(function(){
                $('#cc_div').show();
            $(this).hide();
        });
          $('#open_bcc').click(function(){
                $('#bcc_div').show();
             $(this).hide();
        });
          $('#open_reply').click(function(){
                $('#reply_div').show();
             $(this).hide();
        }); 
        
        
        $('#open_from_email').click(function(){
          $('#from_email_div').show();
           $(this).hide();
        });
        $('#open_display_name').click(function(){
              $('#display_name_div').show();
              $(this).hide();
        });
        
        
        
       $('#type').change(function(){
         
      var templateID = $(this).val();
         

    var customerID =  $('#customertempID').val();
         
      
      if(templateID!=""){
     
     
   } 





 }); 

     
$('#CardID').change(function(){
    
});   
       
  
      
   });  
    

 function set_refund_invoices(invID)
{
     
}

function set_subs_company_id(sID)
{
  
}

/******************Invoice Details Page ********************/

  
  function select_plan_val(rid){
    
    
  } 
  
  
    function set_qty_val(rid){
      
    
  }
  
function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

}



     


  function item_invoice_fields()
  {
    
   
    

  }
      
      
  
   function remove_education_fields(rid)
   {
    
     
  }



function isNumberKeys(evt)
  {
               
}

      
function set_tax_val(mythis, rid)
  {
    
   
    
    }
  
  


  
   function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
    

    
