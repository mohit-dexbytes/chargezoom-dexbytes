function set_sub_status_id(sID, cID) {

    $('.pay_chk').attr('checked', false);
    $('#cr_div').html('');
    $('#credit_div').hide();
    $('#del_ccjkck_o #c_user').remove();
    $('<input>', {
        'type': 'hidden',
        'id': 'c_user',
        'name': 'c_user',
        'value': cID
    }).appendTo($('#del_ccjkck_o'));
    $('#subscID').val(sID);
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('General_controller/set_offline_amount'); ?>",
        data: {
            tid: sID
        },
        dataType: 'json',
        success: function(data) {
            $('#inv_amount').val(data[0]['BalanceRemaining']);
        }
    });
}