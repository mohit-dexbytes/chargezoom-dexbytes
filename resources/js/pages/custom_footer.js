var base_url = $('#js_base_url').val();
var d = new Date();
var n = d.getFullYear();
var dyear = n + 25;

var opt = '';
var i;
for (i = n; i < dyear; i++) {
	opt += '<option value="' + i + '">' + i + '</option>';
}

var card_daata = ' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="Credit Card Number" autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">' + opt + '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="Security Code (CVV)" autocomplete="off" /></div></div> <div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div><div id="frdname" class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Friendly Name" /></div></div></fieldset>' +
	'<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder="Address Line 1"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder="Address Line 2"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder="City"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder="State/Province"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="ZIP Code"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder="Country"></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder="Phone Number"></div></div></fieldset>';

function roundN(num, n) {
	return parseFloat(Math.round(num * Math.pow(10, n)) / Math.pow(10, n)).toFixed(n);
}


//This is for card saving 


function format2(num) {

	var p = parseFloat(num).toFixed(2).split(".");
	return "$" + p[0].split("").reverse().reduce(function (acc, num, i, orig) {
		return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
	}, "") + "." + p[1];

}
function set_template_data(c_id, cmp_name, cmp_id, cust_Email) {


	document.getElementById('customertempID').value = c_id;
	document.getElementById('tempCompanyID').value = cmp_id;
	document.getElementById('tempCompanyName').value = cmp_name;
	document.getElementById('toEmail').value = cust_Email;
}

var gtype = '';
function stripeResponseHandler_res(status, response) {


	$("#thest_pay").find('input[name="stripeToken"]').remove();

	if (response.error) {
		// Re-enable the submit button
		$('#submit_btn').removeAttr("disabled");
		// Show the errors on the form
		$('#payment_error').text(response.error.message);
	} else {
		var form = $("#thest_pay");
		// Getting token from the response json.

		$('<input>', {
			'type': 'hidden',
			'name': 'stripeToken',
			'value': response.id
		}).appendTo(form);

		
		$("#btn_process").attr("disabled", false);
	}
}


function create_Token_stripe() {




	var pub_key = $('#stripeApiKey').val();
	if (pub_key != '') {
		Stripe.setPublishableKey(pub_key);
		Stripe.createToken({
			number: $('#card_number11').val(),
			cvc: $('#ccv11').val(),
			exp_month: $('#expiry11').val(),
			exp_year: $('#expiry_year11').val()
		}, stripeResponseHandler_res);

	}
	// Prevent the form from submitting with the default action
	return false;


}












function set_qbo_subs_id(sID) {

	$('#qbosubscID').val(sID);

}


function set_xero_subs_id(sID) {

	$('#xerosubscID').val(sID);

}



function del_company_role_id(id) {

	$('#merchantroleID').val(id);
	$('#del_roleww').attr('action', base_url + 'company/MerchantUser/delete_role');
}



function del_freshBooks_role_id(id) {

	$('#merchantroleID').val(id);
	$('#del_roleww').attr('action', base_url + 'FreshBooks_controllers/MerchantUser/delete_role');
}



function set_company_invoice_id(id) {

	$('#invoiceID').val(id);
	$('#Qbwc-form').attr('action', base_url + 'company/SettingSubscription/update_customer_invoice_cancel');
}

function set_common_invoice_id(id) {

	$('#invoiceID').val(id);
	$('#Qbwc-form').attr('action', base_url + 'Integration/Invoices/voidInvoice');
}


function company_set_productList_active_id(sID) {

	$('#proactiveID').val(sID);
	$('#del_ccjkckqq').attr('action', base_url + 'company/MerchantUser/change_product_status');

}


function company_del_product_id(sID) {

	$('#prolistID').val(sID);
	$('#del_ccjkckxero').attr('action', base_url + 'company/MerchantUser/change_product_status');

}


function company_customerList_active_id(sID) {

	$('#custactiveID').val(sID);
	$('#del_ccjkckde').attr('action', base_url + 'company/MerchantUser/delete_customer');
}


function company_customerList_id(sID) {

	$('#custID').val(sID);
	$('#del_ccjkckre').attr('action', base_url + 'company/MerchantUser/delete_customer');

}

function set_customerList_active_id(sID) {

	$('#custactiveID').val(sID);

}


function set_customerList_id(sID) {

	$('#custID').val(sID);

}


function set_productList_active_id(sID) {

	$('#proactiveID').val(sID);

}


function del_product_id(sID) {

	$('#prolistID').val(sID);

}





function del_role_id(id) {

	$('#merchantroleID').val(id);
}



function del_credit_id(id) {

	$('#invoicecreditid').val(id);
}

function qbo_del_credit_id(id) {

	$('#qbo_invoicecreditid').val(id);
}
function fb_del_credit_id(id) {

	$('#fb_invoicecreditid').val(id);
}

function xero_del_credit_id(id) {

	$('#xero_invoicecreditid').val(id);
}


function del_user_id(id) {

	$('#merchantID').val(id);
}



function set_company_id(id) {

	$('#companyID').val(id);
}

function set_invoice_id(id) {

	$('#invoiceID').val(id);
}








function create_card_data() {

	if ($('#CardID').val() == 'new1') {

		$('.card_div').css('display', 'block');
		var CardID = '';
		var hideAll = true;
	}
	else {
		$('.card_div').css('display', 'none');
		var CardID = 2;
		var hideAll = false;
	}

	showInvoiceSurcharge({
		CardID,
		hideAll,
		surchargePercentage: $('#invDefaultsurchargeRate').val()
	})
}


function create_card_schedule_data() {

	if ($('#schCardID').val() == 'new1') {
		$('.card_div_csh').css('display', 'block');
	}
	else {
		$('.card_div_csh').css('display', 'none');



	}
}


function create_card_multi_data() {

	

	if ($('#CardID1').val() == 'new1') {
		$('.card_div').css('display', 'block');
		var CardID = '';
		var hideAll = false;
	} else {
		$('.card_div').css('display', 'none');
		var CardID = '1';
		var hideAll = false;
	}
	showBatchInvoiceSurcharge({
		CardID,
		hideAll,
		surchargePercentage: $('#invDefaultsurchargeRate').val()
	});
}



/********************Show The Payment Data**************/



function set_company_payment_data(id) {

	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_company_transaction",
			data: { 'invoiceID': id, 'action': 'invoice' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}


function set_company_payment_transaction_data(id, title = '') {
	if (title != "")
		$('#h_title').html('Authorization Details');
	else
		$('#h_title').html('Invoice Payment Details');

	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_company_transaction",
			data: { 'trID': id, 'action': 'transaction' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}

function set_payment_data(id) {

	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_transaction",
			data: { 'invoiceID': id, 'action': 'invoice' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}


function set_payment_transaction_data(id, title = '') {
	if (title != "")
		$('#h_title').html('Authorization Details');
	else
		$('#h_title').html('Invoice Payment Details');
	if (id != "") {
		$.ajax({
			type: 'POST',
			url: base_url + "ajaxRequest/view_transaction",
			data: { 'trID': id, 'action': 'transaction' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);
			}
		});
	}
}

function set_common_transaction_data(id, title = '') {
	if (title != "")
		$('#h_title').html('Authorization Details');
	else
		$('#h_title').html('Invoice Payment Details');
	if (id != "") {
		$.ajax({
			type: 'POST',
			url: base_url + "ajaxRequest/view_integrations_transaction",
			data: { 'trID': id, 'action': 'transaction' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);
			}
		});
	}
}

function set_qbo_payment_data(id) {


	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_qbo_transaction",
			data: { 'invoiceID': id, 'action': 'invoice' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}

function set_qbo_payment_transaction_data(id, title = '') {
	if (title != "")
		$('#h_title').html('Authorization Details');
	else
		$('#h_title').html('Invoice Payment Details');
	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_qbo_transaction",
			data: { 'trID': id, 'action': 'transaction' },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}

function set_fb_payment_data(id) {


	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_fb_transaction",
			data: { 'invoiceID': id, 'action': 'invoice' },
			dataType: 'json',
			success: function (response) {

				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}

function set_fb_payment_transaction_data(id) {


	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_fb_transaction",
			data: { 'trID': id, 'action': 'transaction' },
			dataType: 'json',
			success: function (response) {

				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}


function set_xero_payment_data(id) {


	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "xero_controllers/Payments/view_transaction",
			data: { 'invoiceID': id },
			success: function (response) {

				$('#pay-content-data').html(response);

			}

		});

	}

}


function del_confirm() {
	if (confirm("do you really want this")) return true; else return false;

}


$(document).ready(function () {

	$('#change_merch_pwd').validate({

		ignore: ":not(:visible)",

		errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
		errorElement: 'div',
		errorPlacement: function (error, e) {
			e.parents('.form-group > div').append(error);
		},
		highlight: function (e) {
			$(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
			$(e).closest('.help-block').remove();
		},
		success: function (e) {
			// You can use the following if you would like to highlight with green color the input after successful validation!
			e.closest('.form-group').removeClass('has-success has-error'); 
			e.closest('.help-block').remove();
		},

		rules: {

			'user_settings_currentpassword': {
				required: true,
				remote: {

					beforeSend: function () {
						$("<div class='overlay1'> <img src='" + base_url + "admin/uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

					},
					complete: function () {
						$(".overlay1").remove();
					},
					url: base_url + "ajaxRequest/chk_new_password",
					type: "POST",

					dataType: 'json',
					dataFilter: function (response) {

						var rsdata = jQuery.parseJSON(response);

						if (rsdata.status == 'success')
							return true;
						else {
							
							return false;
						}
					}
				},


			},
			'user_settings_password': {
				required: true,
				minlength: 8,
				maxlength: 24,
				check_atleast_one_number: true,
                check_atleast_one_special_char: true,
                check_atleast_one_lower: true,
                check_atleast_one_upper:true,
                check_name:true,
				company_name:true,
				check_email:true,
				remote: {

					beforeSend: function () {
						$("<div class='overlay1'> <img src='" + base_url + "admin/uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

					},
					complete: function () {
						$(".overlay1").remove();
					},
					url: base_url + "ajaxRequest/chk_previous_password",
					type: "POST",

					dataType: 'json',
					dataFilter: function (response) {

						var rsdata = jQuery.parseJSON(response);

						if (rsdata.status == 'success')
							return true;
						else {
							
							return false;
						}
					}
				},
			},
			'user_settings_repassword': {
				required: true,
				equalTo: '#user_settings_password',

			}



		},



		messages:
		{
			user_settings_currentpassword:
			{
				required: "This is required",
				minlength: "Please enter minimun 6 characters",
				remote: 'Password is not same, enter correct password',
			},
			user_settings_password:
			{
				remote: 'The password must be different from the previous 5 passwords.',
			}
		},
	});
	$.validator.addMethod("check_atleast_one_number", function(value, element) {
        if(value.length == 0){
            return true ;
        }else{
            if (value.search(/[0-9]/) < 0)
            return false;
           
        }
         return true ;
 
    }, "This field should contain at least one number.");

    $.validator.addMethod("check_atleast_one_special_char", function(value, element) {
        var regularExpression  = /^[a-zA-Z0-9 ]*$/;
        if(value.length == 0){
            return true ;
        }
        if (regularExpression.test(value)) {
            return false;
        } 
        return true ;
    }, "This field should contain at least one special character.");
    $.validator.addMethod("check_atleast_one_lower", function(value, element) {
            var regularExpression  = /^[a-zA-Z0-9 ]*$/;
            if(value.length == 0){
                return true;
            }
            if (value.match(/([a-z])/) && value.match(/([0-9])/)){
                return true;
            }else{
              return false;
            }
            return true ;
    }, "This field should contain at least one lower characters.");
    $.validator.addMethod("check_atleast_one_upper", function(value, element) {
            var regularExpression  = /^[a-zA-Z0-9 ]*$/;
            if(value.length == 0){
                return true;
            }
            if (value.match(/([A-Z])/) && value.match(/([0-9])/)){
                return true;
            }else{
              return false;
            }
            return true ;
    }, "This field should contain at least one uppercase characters.");

    $.validator.addMethod('check_name', function(value, element, params) {
        if(value.length == 0){
            return true ;
        }else{
            var firstName = $('#firstName').val();
            if(firstName.length == 0){
                return true ;
            }else{

                if(value.indexOf(firstName) != -1){
                    return false;
                }
            }
            if (value.search(/[0-9]/) < 0)
            return false;
           
        }
         return true ;
    }, 'First name not used in password.');

    $.validator.addMethod('company_name', function(value, element, params) {
          if(value.length == 0){
            return true ;
        }else{
            var companyName = $('#companyName').val();
            if(companyName.length == 0){
                return true ;
            }else{

                if(value.indexOf(companyName) != -1){
                    return false;
                }
            }
            if (value.search(/[0-9]/) < 0)
            return false;
           
        }
         return true ;
    }, 'Company name not used in password.');

    $.validator.addMethod('check_email', function(value, element, params) {
          if(value.length == 0){
            return true ;
        }else{
            var merchantEmail = $('#merchantEmail').val();
            if(merchantEmail.length == 0){
                return true ;
            }else{
               
                var res = merchantEmail.split("@");
                if(value.indexOf(res[0]) != -1){
                    return false;
                }
            }
            if (value.search(/[0-9]/) < 0)
            return false;
           
        }
         return true ;
    }, 'Email not used in password.');

	$('#recurringform').validate({

		ignore: ":not(:visible)",
		rules: {

			'rec_cardID': {
				required: true,


			},
			'recurAuto': {
				required: true,

			},
			'recAmount': {
				required: true,
				number: true,

			},
			calendar_date_day:{
				required: true,
				max: 31,
				min: 1,
				digits: true,
			}
		},
		messages:{
			calendar_date_day:{
				max: 'Please enter a valid day of month.',
				min: 'Please enter a valid day of month.',
				digits: 'Please enter a valid day of month.',
			}
		}
	});




});

function set_recurring_payment(custID, type) {

	var url = '';
	if (custID != "") {
		if (type == 1) {
			url = base_url + 'QBO_controllers/SettingPlan/recurring_payment';
		}
		if (type == 2) {
			url = base_url + 'SettingPlan/recurring_payment';
		}
		if (type == 3) {
			url = base_url + 'FreshBooks_controllers/SettingPlan/recurring_payment';
		}
		if (type == 4) {
			url = base_url + 'Integration/SettingPlan/recurring_payment';
		}
		if (type == 5) {
			url = base_url + 'company/SettingPlan/recurring_payment';
		}
		$('<input>', {
			'type': 'hidden',
			'id': 'recCustomer',
			'name': 'recCustomer',

		}).remove();
		$('<input>', {
			'type': 'hidden',
			'id': 'recCustomer',
			'name': 'recCustomer',

			'value': custID,
		}).appendTo($('#recurringform'));
		$('#recurringform').attr('action', url);
		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/get_recurring_data",
			data: { 'customerID': custID },
			dataType: 'json',
			success: function (response) {
				$('#calendar_date_day_div').hide();
				$('#calendar_date_day').val('');
				if(response.rec_data){
					if(response.rec_data.optionData == 4){
						$('#calendar_date_day_div').show();
						$('#calendar_date_day').val(response.rec_data.month_day);
					}
				}
				
				if (!jQuery.isEmptyObject(response.sch_data)) {

					$('#sch_div').show();
					$('#recurring-content-data').hide();
					$('#sch_div').html(response.sch_data);
				}
				else {
					$('#recurring-content-data').show();
					$('#sch_div').hide();
					$('#rec_cardID').html('');
					var s = $('#rec_cardID');
					$(s).append('<option value="">Please Select</option>');
					var card1 = response.card;
					if (!jQuery.isEmptyObject(card1))
						for (var val in card1) {
							if (card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase() != 'ECHECK')
								$("<option />", { value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
						}
					var t = $('#rec_payTem');
					var pay_terms = response.pay_term;
					if (!jQuery.isEmptyObject(pay_terms))

						for (var val in pay_terms) {

							t.append('<option value="' + pay_terms[val]['pt_netTerm'] + '">' + pay_terms[val]['pt_name'] + '</option>');

						}

					t.trigger("chosen:updated");


				}


			}

		});

	}
}



function edit_recurring_payment(custID) {

	if (custID != "") {
		$('#recurring-content-data').show();

		$('<input>', {
			'type': 'hidden',
			'id': 'editrecID',
			'name': 'editrecID',

			'value': custID,
		}).appendTo($('#recurringform'));

		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/edit_recurring_data",
			data: { 'editID': custID },
			dataType: 'json',
			success: function (response) {
				

				var s = $('#rec_cardID');
				$(s).find('option').remove();
				$(s).append('<option value=" ">Please Select</option>');
				var card1 = response.card;
				if (!jQuery.isEmptyObject(card1))
					for (var val in card1) {
						if (card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase() != 'ECHECK') {
							var sel = false;
							if (response.rec_data['cardID'] == card1[val]['CardID']) { sel = true; }
							$("<option />", { value: card1[val]['CardID'], selected: sel, text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
						}
					}
				var t = $('#rec_payTem');
				$('ul.chosen-results').empty(); t.empty();
				var pay_terms = response.pay_term;


				if (!jQuery.isEmptyObject(pay_terms)) {
					var p_array = response.rec_data['paymentTerm'].split(',');
					var sel = '';
					for (var val in pay_terms) {



						var st = $.inArray(pay_terms[val]['pt_netTerm'], p_array);

						if (st >= 0) { sel = 'selected'; } else { sel = ''; }

						t.append('<option value="' + pay_terms[val]['pt_netTerm'] + '"  ' + sel + '     >' + pay_terms[val]['pt_name'] + '</option>');


					}

					t.trigger("chosen:updated");

				}
				$('#recurAuto').val(response.rec_data['optionData']);
				$('#recAmount').val(response.rec_data['amount']);
				if(response.rec_data['recurring_send_mail'] =='1'){
					$('#setMailRecurring').attr('checked','checked');  
				} else {
					$('#setMailRecurring').removeAttr('checked');    
				}



			}

		});

	}

	return false;
}



function checkForm(form) {
	var curr_pass = $('#user_settings_currentpassword').val();
	var new_pass = $('#user_settings_password').val();
	var confirm_pass = $('#user_settings_repassword').val();

	if (curr_pass == '') {
		alert("Please enter current password!");
		$('#user_settings_currentpassword').focus();
		return false;
	}
	else if (new_pass == '') {
		alert("Please enter new password!");
		$('#user_settings_password').focus();
		return false;
	}
	else if (new_pass != "" && new_pass == confirm_pass) {
		if (new_pass.length < 5) {
			alert("Password must contain at least Five characters!");
			$('#user_settings_password').focus();
			return false;
		}
	}
	else {
		alert("Confirm New Password does not match.");
		$('#user_settings_repassword').focus();
		return false;
	}
	return true;
}


$(document).ready(function () {


	$(document).on('click', '#tc', function () {
		if ($(this).is(':checked')) {

			$(' #frdname').hide();
		} else
			$(' #frdname').show();

	});



	$('#changePicture').change(function () {
		var customerID = $('#customerID').val();
		var file_data = $("#changePicture").prop("files")[0];
		var form_data = new FormData();
		form_data.append("profile_picture", file_data);
		form_data.append("customerID", customerID);
		var img = base_url + 'uploads/loading_upload.gif';

		var cr_url = $('#cr_url').val();


		$.ajax({


			type: 'POST',
			url: base_url + 'ajaxRequest/change_profile_picture',

			beforeSend: function () {
				$("<div class='overlay1'> <img src='" + img + "'   style='position:absolute;top:30%;left:45%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

			},
			complete: function () {
				$(".overlay1").remove();
			},


			cache: false,
			contentType: false,
			processData: false,
			data: form_data,
			dataType: 'json',

			success: function (data) {

				window.location.href = cr_url;


			}
		});


	});





	$(document).on('click', '.processed_trans', function () {
		$('#invoice_modal_popup').modal('show');
		$.ajax({
			type: "POST",
			url: base_url + 'General_controller/get_process_trans',
			dataType: 'json',
			success: function (data) {
				var s = $('#processed_server_data');
				$(s).empty();
				if (data != 2) {

					for (var val in data) {
						if (data[val]['transactionCode'] == '300' || data[val]['transactionCode'] == '' || data[val]['transactionCode'] == '400' || data[val]['transactionCode'] == '401' || data[val]['transactionCode'] == '3') {
							var status = '<span class="btn btn-sm btn-danger remove-hover">Failed</span>';
						} else if (data[val]['transactionCode'] == '100' || data[val]['transactionCode'] == '200' || data[val]['transactionCode'] == '111' || data[val]['transactionCode'] == '1') {
							var status = '<span class="btn btn-sm btn-success remove-hover">Success</span>';
						}
						$(s).append('<tr><td>' + data[val]['id'] + '</td><td>' + data[val]['FullName'] + '</td><td class="hidden-xs text-right">' + data[val]['RefNumber'] + '</td><td>$' + data[val]['transactionAmount'] + '</td><td>' + data[val]['transactionType'] + '</td><td>' + status + '</td></tr>');
					}

				} else {
					$('.processed_table').empty();
					$('.processed_table').append("<h2>No data...</h2>");
				}
			}
		});
	});


	$(document).on('click', '.scheduled_invoice', function () {
		$('#scheduled_invoice_modal_popup').modal('show');
		$.ajax({
			type: "POST",
			url: base_url + 'General_controller/get_scheduled_incoice',
			dataType: 'json',
			success: function (data) {
				var s = $('#scheduled_server_data');
				$(s).empty();
				if (data != 2) {
					for (var val in data) {
						if (data[val]['userStatus'] == 'Paid') {
							var status = "Schedule";
						} else if (data[val]['userStatus'] == '') {
							var status = "Schedule";
						}
						else {
							var status = "Past Due";
						}

						$(s).append('<tr><td>' + data[val]['Customer_FullName'] + '</td><td class="hidden-xs text-right">' + data[val]['RefNumber'] + '</td><td class="hidden-xs text-right">' + data[val]['DueDate'] + '</td><td>$' + data[val]['AppliedAmount'] + '</td><td>$' + data[val]['BalanceRemaining'] + '</td><td><strong>' + status + '</strong></td></tr>');
					}
				} else {
					$('.scheduled_table').empty();
					$('.scheduled_table').append("<h2>No data...</h2>");
				}
			}
		});
	});

	$(document).on('click', '.failed_invoice', function () {
		$('#failed_invoice_modal_popup').modal('show');
		$.ajax({
			type: "POST",
			url: base_url + 'General_controller/get_failed_incoice',
			dataType: 'json',
			success: function (data) {
				var s = $('#failed_server_data');
				$(s).empty();
				if (data != 2) {

					for (var val in data) {

						$(s).append('<tr><td>' + data[val]['Customer_FullName'] + '</td><td class="hidden-xs text-right">' + data[val]['RefNumber'] + '</td><td class="hidden-xs text-right">' + data[val]['DueDate'] + '</td><td>$' + data[val]['AppliedAmount'] + '</td><td>$' + data[val]['BalanceRemaining'] + '</td><td><strong>Failed</strong></td></tr>');
					}

				} else {
					$('.failed_table').empty();
					$('.failed_table').append("<h2>No data...</h2>");
				}
			}
		});
	});



	$('.card_form').validate({


		errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
		errorElement: 'div',
		errorPlacement: function (error, e) {
			e.parents('.form-group > div').append(error);
		},
		highlight: function (e) {
			$(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
			$(e).closest('.help-block').remove();
		},
		success: function (e) {
			// You can use the following if you would like to highlight with green color the input after successful validation!
			e.closest('.form-group').removeClass('has-success has-error'); 
			e.closest('.help-block').remove();
		},
		rules: {
			card_number: {
				required: true,
				minlength: 13,
				maxlength: 16,
				number: true
			},
			expiry_year: {
				CCExp1: {
					month: '#expiry11',
					year: '#expiry_year11'
				}
			},

			cvv: {
				number: true,
				minlength: 3,
				maxlength: 4,
			},

			friendlyname: {
				required: true,
				minlength: 3,
			},

			inv_amount: {
				required: true,
				number: true,
				min: 0.01,
				remote: {
					url: base_url + 'ajaxRequest/check_invoice_payment_data',
					type: "POST",
					cache: false,
					dataType: "json",
					data: {
						invoiceID: function () { return $("#invoiceProcessID").val(); },
					},
					dataFilter: function (response) {

						var rsdata = jQuery.parseJSON(response)
						if (rsdata.status == 'success')
							return true;
						else
							return false;
					}
				},


			},

			
			email: {
				email: true
			},
			phone: {
				minlength: 10,
				maxlength: 17,
				phoneUS: true,
			},
		
			acc_number:{
				required: true,
				digits: true,
				minlength: 3,
				maxlength: 20,
				
			},
			route_number:{
				required: true,
				digits: true,
				minlength: 3,
				maxlength: 12,
			},
			acc_name:{
				required: true,
				minlength: 3,
				maxlength: 30,
			},




		},
		messages: {
			customerID: {
				required: 'Please select a customer',

			},

			expry: {
				required: 'Please select a valid month',
				minlength: 'Please select a valid month',

			},
			inv_amount: {
				required: "Please enter amount",
				number: "Please enter valid amount",
				min: "Please enter valid amount",
				remote: "You are not allowed to enter more than invoice amount"
			},
			check_status: {
				required: 'Please select the option',
			},
			acc_number: {
				remote: 'Account details already exists.',
				minlength: "Please enter at least 3 digits.",
				maxlength: "Please enter no more than 20 digits.",
				digits: 'Please enter valid account number.'
			},
			route_number: {
				minlength: 'Please enter at least 3 digits.',
				maxlength: 'Please enter no more than 12 digits.',
				digits: 'Please enter valid routing number.'
			},
			


		},
		submitHandler: function (form) {
            $("#qbd_process").attr("disabled", true);
            return true;
        }
	});

	$.validator.addMethod('CCExp1', function (value, element, params) {
		var minMonth = new Date().getMonth() + 1;
		var minYear = new Date().getFullYear();


		var month = parseInt($(params.month).val(), 10);
		var year = parseInt($(params.year).val(), 10);
		return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');



	$('#thest_pay1').validate({
		ignore: ":not(:visible)",
		rules: {
			'pay_amount[]': {
				required: true,
				minlength: 1,

			},
			'multi_inv[]': {
				required: true,
				minlength: 1,

			},

			'card_number': {
				required: true,
				minlength: 13,
				maxlength: 16,
				number: true
			},
			'expiry_year': {
				CCExp2: {
					month: '#expiry11',
					year: '#expiry_year11'
				}
			},

			'cvv': {
				number: true,
				minlength: 3,
				maxlength: 4,
			},
			'friendlyname': {
				required: true,
				minlength: 3,
			},

			'gateway1': {
				required: true
			},
			'CardID1': {
				required: true
			},

			'city': {
				minlength: 2,
			},
			
			'email': {
				email: true
			},
			'phone': {
				minlength: 10,
				maxlength: 17,
				phoneUS: true,
			},
			'totalPay': {
				required: true,
				number: true,
				total_chk: {
					total: '#totalPay',

				}
			}
		},
		submitHandler: function (form) {
            $("#submit_btn").attr("disabled", true);
            return true;
        }

	});



	$.validator.addMethod('CCExp2', function (value, element, params) {
		var minMonth = new Date().getMonth() + 1;
		var minYear = new Date().getFullYear();

		var month = parseInt($('#thest_pay1').find(params.month).val(), 10);
		var year = parseInt($('#thest_pay1').find(params.year).val(), 10);

		return (year > minYear || (year === minYear && month >= minMonth));
	}, 'Your Credit Card Expiration date is invalid.');

	$.validator.addMethod("phoneUS", function (value, element) {

		if (value == '')
			return true;
		return value.match(/^\d{10}$/) ||
			value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);

	}, "Please specify a valid phone number");

	$.validator.addMethod("ZIPCode", function (value, element) {

		return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
	}, "Only alphanumeric and hyphen is allowed");

	$.validator.addMethod('total_chk', function (value, element, params) {

		var real = 0;

		$('.chk_pay').each(function () {

			var r_id = $(this).attr('id');
			real = parseFloat(real) + parseFloat($('.' + r_id).val());


		});

		var total = parseFloat($('#thest_pay1').find(params.total).val());

		return (total <= real);
	}, 'Your are not allowed to enter more than invoices amount');



	$('#CardID1').change(function () {

		var cardlID = $(this).val();
		$( "#invDefaultcardSurchargeValue" ).val(0);

		if (cardlID != '' && cardlID != 'new1') {
			$("#btn_process").attr("disabled", true);
			$("#card_loader").show();
			$.ajax({
				type: "POST",
				url: base_url + 'ajaxRequest/get_card_data',
				data: { 'cardID': cardlID },
				success: function (response) {
					
					$("#card_loader").hide();
					data = $.parseJSON(response);
					$( "#invDefaultcardSurchargeValue" ).val(data['card']['isSurcharge']);

					$('#thest_pay1 #number').remove();
					$('#thest_pay1 #exp_year').remove();
					$('#thest_pay1 #exp_month').remove();
					$('#thest_pay1 #cvc').remove();

					if (data['status'] == 'success') {
						$("#btn_process").attr("disabled", false);
						var form = $("#thest_pay1");
						$('<input>', {
							'type': 'hidden',
							'id': 'number',
							'name': 'number',
							'value': data['card']['CardNo']
						}).appendTo(form);
						$('<input>', {
							'type': 'hidden',
							'id': 'exp_year',
							'name': 'exp_year',
							'value': data['card']['cardYear']
						}).appendTo(form);

						$('<input>', {
							'type': 'hidden',
							'id': 'exp_month',
							'name': 'exp_month',
							'value': data['card']['cardMonth']
						}).appendTo(form);

						$('<input>', {
							'type': 'hidden',
							'id': 'cvc',
							'name': 'cvc',
							'value': data['card']['CardCVV']
						}).appendTo(form);


						

						// Prevent the form from submitting with the default action

					}
					var surchargePercentage = $( "#invDefaultsurchargeRate" ).val();
					showBatchInvoiceSurcharge({surchargePercentage, CardID: data['card']['CardID']});
				}


			});
		} else {
			$("#card_loader").hide();
			$("#btn_process").attr("disabled", false);
		}

	});

});
$("#totalPay").blur(function () {



	var val = parseFloat($(this).val());
	
	var total = 0;

	total = val;
	$('#inv_div').find('.geter').each(function () {
		var inv = parseFloat($(this).val());
		var id = $(this).attr('data-id');
		if (total >= inv) {
			$('#' + id).trigger('click');
			total = total - inv;
			
		}
		else if (total < inv && total > 0) {
			$('.' + id).val(roundN(total, 2));
			$('#' + id).trigger('click');

			total = 0;
		}
		else {
			
			$('#' + id).attr('checked', false);
			
		}
	});
});


$("#totalamount").blur(function () {
	var val = parseFloat($(this).val());
	var total = 0;
	$('#amount').val(val);
	total = val;
	$('.geter').each(function () {
		var inv = parseFloat($(this).val());
		var id = $(this).attr('data-id');
		if (total >= inv) {
			total = total - inv;
		}
		else if (total < inv && total > 0) {
			$('.' + id).val(roundN(total, 2));

			total = 0;
		}
		else {
			$('#' + id).attr('checked', false);
		}
	});
});



function calculateInvoiceNumberPrice() {
	var sum = 0;
	
	$('.chk_pay').each(function () {
		var r_id = $(this).attr('id');
		
		if ($(this).is(':checked')) {
			sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
			$('#test-inv-checkbox-'+$(this).val()).prop('checked', true);
		}else{
			$('#test-inv-checkbox-'+$(this).val()).prop('checked', false);
		}
	});
	
	
	var tmp = [];
	var paymentStr = [];
	var loopCounter = 0;
	var totalInvoiceAmount = 0.00;
    $(".test").each(function() {
        if ($(this).is(':checked')) {
        	loopCounter++;
            var checked1 = $(this).attr("rel");

            var amount = $(this).val();
            totalInvoiceAmount = parseFloat(totalInvoiceAmount) + parseFloat(amount);
            tmp.push(checked1);
            paymentStr.push(amount);
        }
    });	
    
    $('#invoice_pay_amount').val(paymentStr);

    if(loopCounter > 0){
    	$('#totalamount1').html(totalInvoiceAmount.toFixed(2));
		$('#totalamount').val(totalInvoiceAmount.toFixed(2));
		$('#amount').val(totalInvoiceAmount.toFixed(2));
		$('#invoiceTotalAmountCal').val(totalInvoiceAmount.toFixed(2));

    }else{
    	$('#totalamount1').html(0.00);
		$('#totalamount').val(0.00);
		$('#amount').val(0.00);
		$('#invoiceTotalAmountCal').val(0.00);
    }

}
function chk_inv_position1(elm) {
	var sum = 0;
	var tmp = [];
	var payAmount = [];
	var surchargeAmountOnly = 0;
	if ($(elm).is(':checked')) {
	
		
		$('.chk_pay').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();

			if ($(this).is(':checked')) {
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
    			$('.'+r_id).attr('readonly', false);
				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
			}else{
    			$('.'+r_id).attr('readonly', true);
			}
		});
		if($('#invoice_surcharge_type').val() == 'percentage' && $('#invoice_surcharge').val() > 0){
			var surchargePercentageAmount = ( $('#invoice_surcharge').val() / 100 ) * sum;
			var surchargeAmount = sum + surchargePercentageAmount; 
			surchargeAmountOnly = surchargePercentageAmount;
			$('#surcharge_total').val(format2(surchargeAmount));
		}else{
			$('#surcharge_total').val(format2(sum));
		}
		$('#totalamount1').html(sum.toFixed(2));
		$('#totalamount').val(sum.toFixed(2));
		$('#amount').val(sum.toFixed(2));
		
		$('#invoice_ids').val(tmp);
		$('#invoice_pay_amount').val(payAmount);
		var pub_key = '';
		
	
	}
	else {
	
		var class_pay = $(elm).attr('id');
	
		var sum = 0;
		var tmp = [];
		var payAmount = [];

		$('.chk_pay').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();
			
			if ($(this).is(':checked')) {
				
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				$('.'+r_id).attr('readonly', false);
				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
	
			}else{
				
				$('.'+r_id).attr('readonly', true);
			}
		});

		if($('#invoice_surcharge_type').val() == 'percentage' && $('#invoice_surcharge').val() > 0){
			var surchargePercentageAmount = ( $('#invoice_surcharge').val() / 100 ) * sum;
			var surchargeAmount = sum + surchargePercentageAmount; 
			surchargeAmountOnly = surchargePercentageAmount;
			$('#surcharge_total').val(format2(surchargeAmount));
		}else{
			$('#surcharge_total').val(format2(sum));
		}
		$('#totalamount1').html(sum.toFixed(2));
		$('#totalamount').val(sum.toFixed(2));
		$('#amount').val(sum.toFixed(2));
		
		$('#invoice_ids').val(tmp);
		$('#invoice_pay_amount').val(payAmount);
	}
	$('#surchargeAmountOnly').val(surchargeAmountOnly.toFixed(2));


		

        var invoice_number_options = '';
        $("#invoice_number option").each(function()
        {
            if($.inArray($(this).val(), tmp) !== -1){
                invoice_number_options += '<option   value="'+ $(this).val() +'" selected>'+ $(this).html() +'</option>';
            }else{
                invoice_number_options += '<option  value="'+ $(this).val() +'">'+ $(this).html() +'</option>';
            }
        });
        $('#invoice_number').empty(); //remove all child nodes
        $('#invoice_number').html(invoice_number_options);
        $('#invoice_number').trigger("chosen:updated");

        $('#isCustomize').val(1);
	}
var show_invoice_select_popup = true;
var focusAmountInputAuto = true;
var customerInvoiceArray = [];
var customerInvoiceDisplayArray = [];
$("#invoice_number").chosen();
$("#invoice_number").chosen().change(function(e, params){
  
    var oldInvoice = $('#invoice_ids').val();
    var invoiceStr = '';
    var sumCal = 0;
    var selectInvoiceTotal = 0;
    var oldAmountTotal = $('#totalamount1').html();
    if(params.selected !== undefined){
    	
    	if(oldInvoice != ''){
	    	var oldInvoiceArr = oldInvoice.split(",");
	    	
	    	var index = oldInvoiceArr.length;
	    	
	    	oldInvoiceArr[index] = params.selected;
	    	invoiceStr =  oldInvoiceArr.toString(); 
	    }else{
	    	 invoiceStr = params.selected;
	    }
	   
        $('#multiinv'+params.selected).prop('checked', true);
        $('#test-inv-checkbox-'+params.selected).prop('checked', true);
    }else{
    	if(oldInvoice != ''){

	    	 invoiceStr = removeValueFromString(oldInvoice,params.deselected,',');
	    }
	    
        $('#multiinv'+params.deselected).prop('checked', false);
        $('#test-inv-checkbox-'+params.deselected).prop('checked', false);
    }
   	if(customerInvoiceArray){
		for (var i = 0; i < customerInvoiceArray.length; i++) {
		    var checkCUstomORnot = jQuery.inArray( customerInvoiceArray[i], $("#invoice_number").chosen().val() );
		    if(checkCUstomORnot < 0){
		    	show_invoice_select_popup = false;
		    }else{
		    	show_invoice_select_popup = true;
		    	break;
		    }
		}
    }

    $('#invoice_ids').val(invoiceStr);
   
    $('#amount').focus();
   calculateInvoiceNumberPriceUpdate();
   
    if(!show_invoice_select_popup){
    	if ( typeof auth_org_amount !== 'undefined') {
    		$('#amount').val(auth_org_amount);
		}
    }
    $(this).valid();
    
});
function calculateInvoiceNumberPriceUpdate() {
	var payAmount = [];
	var tmp = [];
	var sum = 0;
	var surchargeAmountOnly = 0;
	$('.chk_pay').each(function () {
		var inv_id = $(this).val();
		var r_id = $(this).attr('id');

		if ($(this).is(':checked')) {
			sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
			tmp.push(inv_id);
			payAmount.push(parseFloat($('.' + r_id).val()));
			$('.'+r_id).attr('readonly',false);
		}else{
			$('.'+r_id).attr('readonly',true);
		}
	});
	
	if($('#invoice_surcharge_type').val() == 'percentage' && $('#invoice_surcharge').val() > 0){
		var surchargePercentageAmount = ( $('#invoice_surcharge').val() / 100 ) * sum;
		var surchargeAmount = sum + surchargePercentageAmount; 
		surchargeAmountOnly = surchargePercentageAmount;
		$('#surcharge_total').val(format2(surchargeAmount));
	}else{
		$('#surcharge_total').val(format2(sum));
	}
	$('#amount').val(sum.toFixed(2));
    $('#totalamount1').html(sum.toFixed(2));
    $('#totalamount').html(sum.toFixed(2));
    $('#surchargeAmountOnly').val(surchargeAmountOnly.toFixed(2));

	$('#invoice_ids').val(tmp);
    $('#invoice_pay_amount').val(payAmount);
}	
function removeValueFromString(list, value, separator) {
  separator = separator || ",";
  var values = list.split(separator);
  for(var i = 0 ; i < values.length ; i++) {
    if(values[i] == value) {
      values.splice(i, 1);
      return values.join(separator);
    }
  }
  return list;
}
function chk_inv_position(elm) {

	if ($(elm).is(':checked')) {

		
		var sum = 0;

		$('.chk_pay').each(function () {
			if ($(this).is(':checked')) {
				var r_id = $(this).attr('id');

				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
			}
		});

		$('#totalMultiInvoiceTotal').html(sum.toFixed(2));
		$('#totalPay').val(sum.toFixed(2));
		var pub_key = '';
		if ($('#stripeApiKey').val() !== 'undefined' && $('#stripeApiKey').val() != "") {

			var pub_key = $('#stripeApiKey').val();

			if ($('#CardID1').val() == 'new1') {

				if (pub_key != '') {

					if ($('#thest_pay1').find('#card_number11').val() != "") {
						Stripe.setPublishableKey(pub_key);
						Stripe.createToken({
							number: $('#thest_pay1').find('#card_number11').val(),
							cvc: $('#thest_pay1').find('#ccv11').val(),
							exp_month: $('#thest_pay1').find('#expiry11').val(),
							exp_year: $('#thest_pay1').find('#expiry_year11').val()
						}, stripeResponseHandler_res_multi);
					} else {

						return false;
					}

				}
			} else {   

				if (pub_key != '') {
					if ($('#number').val() != "") {
						Stripe.setPublishableKey(pub_key);
						Stripe.createToken({
							number: $('#number').val(),
							cvc: $('#cvc').val(),
							exp_month: $('#exp_month').val(),
							exp_year: $('#exp_year').val()
						}, stripeResponseHandler_res_multi);


					}
					else {

						alert("Please select card options first then select the invoice");
						return false;
					}

				}

			}

		}

	}
	else {

		var class_pay = $(elm).attr('id');

		var sum = 0;
		$('.chk_pay').each(function () {
			if ($(this).is(':checked')) {
				var r_id = $(this).attr('id');
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());

			}
		});
		$('#totalMultiInvoiceTotal').html(sum.toFixed(2));
		$('#totalPay').val(sum.toFixed(2));
	}
}


function chk_pay_position(elm) {
	var at_id = $(elm).data('id');
	if ($('#' + at_id).is(':checked')) {
		$('#' + at_id).trigger('click');
	}

}

function chk_pay_position1(elm) {
	var at_id = $(elm).data('id');
	var oldAmount = parseFloat($(elm).attr('data-value'));
	var currentAmount = parseFloat($(elm).val());
	if(currentAmount > oldAmount ){
		alert('Please select amount less than or equal to invoice total amount');
		$(elm).val(oldAmount.toFixed(2));
	}else{
		
		$(elm).val(currentAmount.toFixed(2));
	}
	var surchargeAmountOnly = 0;

	if ($('#' + at_id).is(':checked')) {
		var sum = 0;
		var tmp = [];
		var payAmount = [];
		
		$('.chk_pay').each(function () {
			var inv_id = $(this).val();

			if ($(this).is(':checked')) {
				var r_id = $(this).attr('id');
	
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
			}
		});
		
		$('#totalamount1').html(sum.toFixed(2));
		$('#totalamount').val(sum.toFixed(2));
		$('#amount').val(sum.toFixed(2));

		$('#invoice_ids').val(tmp);
        $('#invoice_pay_amount').val(payAmount);
		$('#totalamountInv').val(sum.toFixed(2));
		$('#isCustomize').val(1);

		if(apply_surcharge_on_invoice && show_invoice_select_popup){
			var invoice_surcharge = parseFloat($('#invoice_surcharge').val());

			if($('#invoice_surcharge_type').val() == 'percentage' && !isNaN(invoice_surcharge)){				
				var surchargePercentageAmount = ( invoice_surcharge / 100 ) * sum;
				var surchargeAmount = sum + surchargePercentageAmount; 
				surchargeAmountOnly = surchargePercentageAmount;
				$('#surcharge_total').val(format2(surchargeAmount));
			}else{
				$('#surcharge_total').val(format2(sum));
			}

			$('#invoice_surcharge').val(invoice_surcharge.toFixed(2));
		}else{
			$('#surcharge_total').val(format2(sum));
		}
	}
	$('#surchargeAmountOnly').val(surchargeAmountOnly.toFixed(2));

}

function chk_refund_position(elm) {
	var at_id = $(elm).data('id');
	var oldAmount = parseFloat($(elm).attr('data-value'));
	var currentAmount = parseFloat($(elm).val());
	if(currentAmount > oldAmount ){
		alert('Please select amount less than or equal to amount');
		$(elm).val(oldAmount.toFixed(2));
	}else{
		
		$(elm).val(currentAmount.toFixed(2));
	}
	

	if ($('#' + at_id).is(':checked')) {
		var sum = 0;
		var tmp = [];
		var payAmount = [];
		var tmpRowID = [];
		$('.chk_pay_refund').each(function () {
			var inv_id = $(this).val();
			var row_id = $(this).attr('data-rowid');

			if ($(this).is(':checked')) {
				var r_id = $(this).attr('id');
	
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				tmpRowID.push(row_id);
				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
			}
		});
		
		if(sum > 0){
	      	$('#refundBtn').show();
	    }else{
	    	$('#refundBtn').hide();
	    }
		
		$('#refundTotalamountInput').val(sum.toFixed(2));
		$('#refundTotalamount').html(sum.toFixed(2));
		$('#txnRowIDs').val(tmpRowID);
		$('#refundInvoiceIDs').val(tmp);
        $('#refundInvoiceAmount').val(payAmount);
	}

}
function chk_invoice_refund_position(elm) {
	
	var at_id = $(elm).data('id');
	var oldAmount = parseFloat($(elm).attr('data-value'));
	var currentAmount = parseFloat($(elm).val());
	if(currentAmount > oldAmount ){
		alert('Please select amount less than or equal to amount');
		$(elm).val(oldAmount.toFixed(2));
	}else{
		
		$(elm).val(currentAmount.toFixed(2));
	}
	

	if ($('#' + at_id).is(':checked')) {
		var sum = 0;
		var tmp = [];
		
		$('.chk_pay_invoice_refund').each(function () {
			var inv_id = $(this).val();
			
			if ($(this).is(':checked')) {
				var r_id = $(this).attr('id');
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				tmp.push(inv_id);
			}
		});
		if(sum > 0){
	      	$('#refundInvoiceBtn').show();
	    }else{
	    	$('#refundInvoiceBtn').hide();
	    }

		$('#refundTotalInvoiceAmountInput').val(sum.toFixed(2));
		$('#refundTotalInvoiceAmount').html(sum.toFixed(2));
		$('#refundInvoiceID').val(tmp);
		
	}

}

function chk_refund_position_by_checkbox(elm) {
	var sum = 0;
	var tmp = [];
	var payAmount = [];
	var tmpRowID = [];
	if ($(elm).is(':checked')) {
	
		$('.chk_pay_refund').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();
			var row_id = $(this).attr('data-rowid');
			if ($(this).is(':checked')) {
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
    			
    			$('.'+r_id).attr('readonly', false);
				
				tmpRowID.push(row_id);

				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
			}else{
    			
    			$('.'+r_id).attr('readonly', true);
			}
		});
	}
	else {
		var class_pay = $(elm).attr('id');
		
		$('.chk_pay_refund').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();
			var row_id = $(this).attr('data-rowid');
			if ($(this).is(':checked')) {
				
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				$('.'+r_id).attr('readonly', false);
				tmp.push(inv_id);
				payAmount.push(parseFloat($('.' + r_id).val()));
				tmpRowID.push(row_id);
			}else{
				$('.'+r_id).attr('readonly', true);
			}
		});
	}
	if(sum > 0){
      	$('#refundBtn').show();
    }else{
    	$('#refundBtn').hide();
    }
	$('#refundTotalamountInput').val(sum.toFixed(2));
	$('#refundTotalamount').html(sum.toFixed(2));

	$('#txnRowIDs').val(tmpRowID);
	$('#refundInvoiceIDs').val(tmp);
    $('#refundInvoiceAmount').val(payAmount);
}
function chk_invoice_refund_position_by_checkbox(elm) {
	var sum = 0;
	var tmp = [];
	var payAmount = [];
	if ($(elm).is(':checked')) {
	
		$('.chk_pay_invoice_refund').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();
			
			if ($(this).is(':checked')) {
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
    			$('.'+r_id).attr('readonly', false);
				
				tmp.push(inv_id);
			}else{
    			
    			$('.'+r_id).attr('readonly', true);
			}
		});
	}
	else {
		var class_pay = $(elm).attr('id');
		
		$('.chk_pay_invoice_refund').each(function () {
			var r_id = $(this).attr('id');
			var inv_id = $(this).val();
			
			if ($(this).is(':checked')) {
				sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
				$('.'+r_id).attr('readonly', false);
				tmp.push(inv_id);
				
			}else{
				$('.'+r_id).attr('readonly', true);
			}
		});
	}
	if(sum > 0){
      	$('#refundInvoiceBtn').show();
    }else{
    	$('#refundInvoiceBtn').hide();
    }
	$('#refundTotalInvoiceAmountInput').val(sum.toFixed(2));
	$('#refundTotalInvoiceAmount').html(sum.toFixed(2));
	$('#refundInvoiceID').val(tmp);
}

function stripeResponseHandler_res_multi(status, response) {



	if (response.error) {
		// Re-enable the submit button
		$('#submit_btn').removeAttr("disabled");
		// Show the errors on the form
		$('#payment_error').text(response.error.message);
	} else {
		var form = $("#thest_pay1");
		// Getting token from the response json.

		$('<input>', {
			'type': 'hidden',
			'name': 'stripeToken[]',
			'value': response.id
		}).appendTo(form);

		// Doing AJAX form submit to your server.
		
		$("#btn_process").attr("disabled", false);
	}
}


function get_ach_details(eid) {


	var type = eid.value;

	var sch = $('#scheduleID').val();
	if (sch != "") {
		$('.card_div_csh').css('display', 'none');

		$.ajax({
			type: "POST",
			url: base_url + 'ajaxRequest/get_ach_details_data',
			data: { 'invID': sch, 'type': type },
			dataType: 'json',
			success: function (response) {


				if (response.status == 'success') {

					$('#pay_data').html(response.schd);


				}

			}


		});
	}

}
function get_ach_details_pay(eid) {
	
	var type = eid.value;

	var sch = $('#invoiceProcessID').val();
	
	$('#invDefaultsurchargeRate').val(0);
	$('#invDefaultcardSurchargeValue').val(0);


	if (sch != "") {
		$('.card_div_csh').css('display', 'none');
		$('.card_div').css('display', 'none');
		$.ajax({
			type: "POST",
			url: base_url + 'ajaxRequest/get_ach_details_data',
			data: { 'invID': sch, 'type': type },
			dataType: 'json',
			success: function (response) {
				

				if (response.status == 'success') {

					$('#pay_data_1').html(response.schd);
	
					$('#invDefaultsurchargeRate').val(response.surchargePercentage);
	                showInvoiceSurcharge({hideAll: true});
					create_sch_card({value: response.defaultCard },type);
				}
			}
		});
	}

}
function create_sch_card(eid, type) {
	var crd = eid.value;
	var CardID = '';
	var hideAll = false;
	if (crd == 'new1') {

		$('.card_div_csh').css('display', 'block');
		$('.card_div').css('display', 'block');
		if (type == 1) {
			$('.sch_crd').css('display', 'block');
			$('.sch_chk').css('display', 'none');
		}
		if (type == 2) {
			$('.sch_crd').css('display', 'none');
			$('.sch_chk').css('display', 'block');
		}
	} else {
		CardID = cardlID = crd;
		hideAll = false;
		$('.sch_crd').css('display', 'none');
		$('.sch_chk').css('display', 'none');

		$("#btn_process").attr("disabled", true);
			$("#card_loader").show();
		 $.ajax({
			 type:"POST",
			 url : base_url+'ajaxRequest/get_card_data',
			 data : {'cardID':cardlID},
			 success : function(response){
				 
				  $("#card_loader").hide();
					  data=$.parseJSON(response);
					 
					 if(data['status']=='success'){
						 $("#btn_process").attr("disabled", false);	
						 
						 $('#invDefaultcardSurchargeValue').val(data['card']['isSurcharge']);
						
						 if(gtype=='5')
						 {
							 
					  var form = $("#thest_pay");	
					 
					  $('#thest_pay #number').remove();	
					  $('#thest_pay #exp_year').remove();	
					  $('#thest_pay #exp_month').remove();	
					  $('#thest_pay #cvc').remove();	
					 
					  $('<input>', {
									 'type': 'hidden',
									 'id'  : 'number',
									 'name': 'number',
									 'value': data['card']['CardNo']
									 }).appendTo(form);	
					  $('<input>', {
									 'type': 'hidden',
									 'id'  : 'exp_year',
									 'name': 'exp_year',
									 'value': data['card']['cardYear']
									 }).appendTo(form);
									 
					  $('<input>', {
									 'type': 'hidden',
									 'id'  : 'exp_month',
									 'name': 'exp_month',
									 'value': data['card']['cardMonth']
									 }).appendTo(form);
									 
						$('<input>', {
									 'type': 'hidden',
									 'id'  : 'cvc',
									 'name': 'cvc',
									 'value': data['card']['CardCVV']
									 }).appendTo(form);	
					 
					 
							 var pub_key = $('#stripeApiKey').val();
						  if(pub_key){
								  Stripe.setPublishableKey(pub_key);
								  Stripe.createToken({
												 number: $('#number').val(),
												 cvc: $('#cvc').val(),
												 exp_month: $('#exp_month').val(),
												 exp_year: $('#exp_year').val()
											 }, stripeResponseHandler_res);
					 }
								 // Prevent the form from submitting with the default action
							  
						 }
						 var surchargePercentage = $( "#invDefaultsurchargeRate" ).val();
						 showInvoiceSurcharge({surchargePercentage, CardID: data['card']['CardID']});
			 }
			 
			 }	
		 });
	}

	if(type == 1){
		showInvoiceSurcharge({
			CardID,
			hideAll,
			surchargePercentage: $('#invDefaultsurchargeRate').val()
		})
	}

}


function delete_card(url, card_id, cusID) {
	if (url != "" && card_id != "") {
		$('#thest_del #delCardID').remove();
		$('#thest_del #delCustodID').remove();
		$('<input>').attr({
			type: 'hidden',
			id: 'delCustodID',
			name: 'delCustodID',
			value: cusID
		}).appendTo('#thest_del');
		$('<input>').attr({
			type: 'hidden',
			id: 'delCardID',
			name: 'delCardID',
			value: card_id
		}).appendTo('#thest_del');

		$('#thest_del').attr('action', url);
	}
	return false;

}
function get_payment_transaction_data(id,type) {
	$('#h_title').html('Payment Transaction Details');

	if (id != "") {
		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_company_transaction_details",
			data: { 'trID': id, 'action': 'transaction','type':type },
			dataType: 'json',
			success: function (response) {
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}
function get_card_surcharge_details(args) {
	var cardId = (args.cardId == undefined) ? 0 : args.cardId;
	var cardNumber = (args.cardNumber == undefined) ? 0 : args.cardNumber;
	var gatewaySurchargeRate = $( "#gatewaySurchargeRate" ).val();
	if(args.type != undefined){
		gatewaySurchargeRate = $( "#invDefaultsurchargeRate" ).val();
	}


	if((cardId > 0 || cardNumber.length >= 6) && gatewaySurchargeRate > 0) {
		$.ajax({
			type: "POST",
			url: base_url + 'ajaxRequest/check_card_surcharge',
			data: {
				cardId, cardNumber
			},
			dataType: 'json',
			success: function (response) {
				if(args.type != undefined){
					$( "#invDefaultcardSurchargeValue" ).val(response.data);
					if(args.type == 1){
						showInvoiceSurcharge({surchargePercentage: gatewaySurchargeRate});
					} else {
						showBatchInvoiceSurcharge({surchargePercentage: gatewaySurchargeRate});
					}
				} else {
					showHideSurchargeNotice(response.data);
				}
			}
		});
	} else {
		if(args.type != undefined){
			if(args.type == 1){
				showInvoiceSurcharge({hideAll: true, surchargePercentage: gatewaySurchargeRate});
			} else {
				showBatchInvoiceSurcharge({hideAll: true, surchargePercentage: gatewaySurchargeRate});
			}
		} else {
			showHideSurchargeNotice(0);
		}
	}
}

function showHideSurchargeNotice(isShow = 0){
	var gatewaySurchargeRate = $( "#gatewaySurchargeRate" ).val();
	$( "#cardSurchargeValue" ).val(isShow);

	if(isShow > 0 && gatewaySurchargeRate > 0) {
		$( "#surchargeNotice" ).show();
		$( "#surchargeNotice1" ).show();
	} else {
		$( "#surchargeNotice" ).hide();
		$( "#surchargeNotice1" ).hide();
	}
}
function setInvSurchargeNotice(surchargePercentage){
	const noticeVar = `Surcharging has been enabled for credit cards that are not greater than the cost of acceptance. This surcharge rate is ${surchargePercentage}% and is not assessed on debit card transactions`;
	$( ".surchargeNoticeText" ).html(noticeVar);
} 


function showInvoiceSurcharge(args){
	setInvSurchargeNotice(args.surchargePercentage);
	var invDefaultcardSurchargeValue = $( "#invDefaultcardSurchargeValue" ).val();
	if(args.hideAll || args.surchargePercentage <= 0 || invDefaultcardSurchargeValue <= 0){
		$('.surchargeNoticeClass1').css('display', 'none');
		$('.surchargeNoticeClass').css('display', 'none');
	} else if(args.surchargePercentage > 0 && invDefaultcardSurchargeValue > 0) {
		if(args.CardID != '' && args.CardID > 0 && args.CardID != 'new1'){
			$('.surchargeNoticeClass1').css('display', 'block');
			$('.surchargeNoticeClass').css('display', 'none');
		} else{
			$('.surchargeNoticeClass1').css('display', 'none');
			$('.surchargeNoticeClass').css('display', 'block');
		}
	} else {
		$('.surchargeNoticeClass1').css('display', 'none');
		$('.surchargeNoticeClass').css('display', 'none');
	}
}

function showBatchInvoiceSurcharge(args){
	setInvSurchargeNotice(args.surchargePercentage);
	var invDefaultcardSurchargeValue = $( "#invDefaultcardSurchargeValue" ).val();
	if(args.hideAll || args.surchargePercentage <= 0 || invDefaultcardSurchargeValue <= 0){
		$('.surchargeNoticeClass3').css('display', 'none');
		$('.surchargeNoticeClass2').css('display', 'none');
	} else if(args.surchargePercentage > 0 && invDefaultcardSurchargeValue > 0) {
		if(args.CardID != '' && args.CardID > 0 && args.CardID != 'new1'){
			$('.surchargeNoticeClass3').css('display', 'block');
			$('.surchargeNoticeClass2').css('display', 'none');
		} else{
			$('.surchargeNoticeClass3').css('display', 'none');
			$('.surchargeNoticeClass2').css('display', 'block');
		}
	} else {
		$('.surchargeNoticeClass3').css('display', 'none');
		$('.surchargeNoticeClass2').css('display', 'none');
	}
}

$(document).on('change', '#recurAuto', function () {
	if($(this).val() == 4){
		$('#calendar_date_day_div').show();
	}else{
		$('#calendar_date_day_div').hide();
	}
});

$(document).ready(function() {
	$('#recurringform').validate({
		ignore: ":not(:visible)",
		rules: {
			calendar_date_day: {
				required: false,
				max: 31,
				min: 1,
			}
		}
	});
});

function getPrintTransactionReceiptData(id, type) {
	$.ajax({
		type: "POST",
		url: base_url + 'ajaxRequest/getPrintTransactionReceiptData',
		data: {
			id, id,
			type, type
		},
		dataType: 'json',
		success: function (response) {
			window.open(base_url+'ajaxRequest/transactionReceipt/'+id, '_blank').focus();
		}
	});
}


$(document).on('click', '.refunAmountCustom', function () {
  var transactionID = $(this).attr('transaction-id');
  var gatewaytype = $(this).attr('transaction-gatewaytype');
  var gatewayName = $(this).attr('transaction-gatewayName');
  var integrationType = $(this).attr('integration-type');
  var URL = $(this).attr('data-url');
  
  $('#transactionID').val(transactionID);
  $('#gatewaytype').val(gatewaytype);

  if (transactionID != "") {
      $.ajax({
          type: "POST",
          url: URL,
          data: {
              'transactionID': transactionID,
              'gatewaytype': gatewaytype,
              'integrationType': integrationType
          },
          success: function(response) {
            data = $.parseJSON(response);
                    
            if (data['status'] == 'success') {
              $('#refundAmountTable').html('');
              
              if (!jQuery.isEmptyObject(data['invoices'])) {  
                $('#refundAmountTable').html(data['invoices']);
                $('#refundTotalamount').html(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                $('#refundTotalamountInput').val(parseFloat(data['totalInvoiceAmount']).toFixed(2));
                $('#integrationType').val(data['integrationType']);
                $('#gatewayName').val(gatewayName);

                if(parseFloat(data['totalInvoiceAmount']) > 0){
                    $('#refundBtn').show();
                }else{
                  $('#refundBtn').hide();
                }

                $("#refundAmountModel").modal("show");
              }
              var tmp = [];
              var payAmount = [];
              var tmpRowID = [];
              var sum = 0;
              if(data.applySurcharge == true){
                $('.chk_pay_refund').hide();
              }else{
                $('.chk_pay_refund').show();
              }
              $('.chk_pay_refund').each(function () {
                  var r_id = $(this).attr('id');
                  var inv_id = $(this).val();
                  var row_id = $(this).attr('data-rowid');
                  

                  if ($(this).is(':checked')) {
                      sum = parseFloat(sum) + parseFloat($('.' + r_id).val());
                      $('#test-inv-checkbox-'+$(this).val()).prop('checked', true);
                      if(data.applySurcharge != true){
                        $('.'+r_id).attr('readonly', false);
                      }

                      tmpRowID.push(row_id);
                      
                      tmp.push(inv_id);
                      payAmount.push(parseFloat($('.' + r_id).attr('data-value')));
                  }else{
                      $('#test-inv-checkbox-'+$(this).val()).prop('checked', false);
                      $('.'+r_id).attr('readonly', true);
                  }
              });  

              $('#refundInvoiceIDs').val(tmp);
              $('#refundInvoiceAmount').val(payAmount);
              $('#txnRowIDs').val(tmpRowID);

              $('#refundRemainingAmount').val(payAmount);
            }
          }
      });
  }
});

function set_common_payment_data(id) {

	if (id != "") {


		$.ajax({

			type: 'POST',
			url: base_url + "ajaxRequest/view_integrations_transaction",
			data: { 'invoiceID': id, 'action': 'invoice' },
			dataType: 'json',
			success: function (response) {
				// console.log(response['transaction']);
				$('#pay-content-data').html(response.transaction);

			}

		});

	}

}
$(document).on('change', '#invoice_surcharge_type', function () {
	if($('#amount').val() != ''){
		var amount = parseFloat($('#amount').val());
	}else{
		var amount = 0;
	}
	var surchargeAmountOnly = 0;

	if(apply_surcharge_on_invoice && show_invoice_select_popup){
		let invoiceSelect = $('#invoice_ids').val();
		if($(this).val() == 'percentage' && invoiceSelect != ''){
			$('#invoice_surcharge').attr('readonly', false);
			var invoice_surcharge = parseFloat($('#invoice_surcharge').val());
			
			var surchargePercentageAmount = ( invoice_surcharge / 100 ) * amount;
				
			var surchargeAmount = amount + surchargePercentageAmount;

			surchargeAmountOnly = surchargePercentageAmount;
			$('#surcharge_total').val(format2(surchargeAmount));
		}else{
			$('#invoice_surcharge').attr('readonly', true);
			$('#surcharge_total').val(format2(amount));
		}
	}else{
		$('#surcharge_total').val(format2(amount));
	}
	$('#surchargeAmountOnly').val(surchargeAmountOnly.toFixed(2));

});

$(document).on('change', '#invoice_surcharge', function () {
	if($('#amount').val() != ''){
		var amount = parseFloat($('#amount').val());
	}else{
		var amount = 0;
	}
	var surchargeAmountOnly = 0;
	if(apply_surcharge_on_invoice && show_invoice_select_popup){
		var invoice_surcharge = parseFloat($('#invoice_surcharge').val());
		if($('#invoice_surcharge_type').val() == 'percentage'){
			var surchargePercentageAmount = ( invoice_surcharge / 100 ) * amount;
			var surchargeAmount = amount + surchargePercentageAmount; 
			surchargeAmountOnly = surchargePercentageAmount;
			$('#surcharge_total').val(format2(surchargeAmount));
		}else{
			$('#surcharge_total').val(format2(amount));
		}
		$('#invoice_surcharge').val(invoice_surcharge.toFixed(2));
	}else{
		$('#surcharge_total').val(format2(amount));
	}
	$('#surchargeAmountOnly').val(surchargeAmountOnly.toFixed(2));

});

var check_card_surcharge_ajax = '';
function checkSurchargeInBinDataBase(){
	let invoiceSelect = $('#invoice_ids').val();
	if(check_card_surcharge_ajax){
		check_card_surcharge_ajax.abort();
	}
	var cardId = $('#card_list').val();
	var cardNumber = $('#card_number').val();
	if($('#amount').val() != ''){
		var amount = parseFloat($('#amount').val());
	}else{
		var amount = 0;
	}
	if(cardId != 'new1'){
		check_card_surcharge_ajax = $.ajax({
			type: "POST",
			url: base_url + 'ajaxRequest/customSurcharge',
			data: {
				cardId, cardNumber
			},
			dataType: 'json',
			success: function (response) {
				if(response.data == 1 && invoiceSelect != ''){
					apply_surcharge_on_invoice = true;
					$('#invoice_surcharge').val(invoice_surcharge_percentage).attr('readonly', false);
					
					let surchargePercentageAmount = ( invoice_surcharge_percentage / 100 ) * amount;
					let surchargeAmount = amount + surchargePercentageAmount; 
					if(show_invoice_select_popup){
						$('#surcharge_total').val(format2(surchargeAmount));
					}else{
						$('#surcharge_total').val(format2(amount));
					}
					
				}else{
					apply_surcharge_on_invoice = false;
					$('#surcharge_total').val(format2(amount));
					$('#invoice_surcharge').val('Ineligible').attr('readonly', true);
				}
			}
		});
	}else{
		if (cardNumber.length >= 6  && invoiceSelect != '') {
			cardId = 0;
			check_card_surcharge_ajax = $.ajax({
				type: "POST",
				url: base_url + 'ajaxRequest/customSurcharge',
				data: {
					cardId, cardNumber
				},
				dataType: 'json',
				success: function (response) {
					if(response.data == 1){
						apply_surcharge_on_invoice = true;
						$('#invoice_surcharge').val(invoice_surcharge_percentage).attr('readonly', false);
						let surchargePercentageAmount = ( invoice_surcharge_percentage / 100 ) * amount;
						let surchargeAmount = amount + surchargePercentageAmount; 
						if(show_invoice_select_popup){
							$('#surcharge_total').val(format2(surchargeAmount));
						}else{
							$('#surcharge_total').val(format2(amount));
						}
						
					}else{
						apply_surcharge_on_invoice = false;
						$('#surcharge_total').val(format2(amount));
						$('#invoice_surcharge').val('Ineligible').attr('readonly', true);
					}
				}
			});
		}else{
			$('#invoice_surcharge').val('Ineligible').attr('readonly', true);
			$('#surcharge_total').val(format2(amount));
		}
	}
	
}
$("#invoice_number_capture").chosen().change(function(e, params){
   	var selected_invoice_value = $(this).val();
  	
    var selected_invoice_amount = $('.multiinv'+selected_invoice_value).val();
    show_invoice_select_popup = false;
    if ( typeof selected_invoice_amount !== 'undefined') {
		$('#amount').val(selected_invoice_amount);
		$('#invoice_total_amount').html(format2(selected_invoice_amount));
	}else{
		if ( typeof auth_org_amount !== 'undefined') {
    		$('#amount').val(auth_org_amount);
		}
		$('#invoice_total_amount').html('$0.00');
	}
    $('#invoice_ids').val(selected_invoice_value);
    $(this).valid();
    $('#amount').valid();
});
function onlyNumberKeyAllow(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
  	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
     	return false;
    }
  	return true;
}
function formatPhoneNumber(str) {
	if(str != ''){
		const match = str.replace(/\D+/g, '').match(/([^\d]*\d[^\d]*){1,10}$/)[0]
		const part1 = match.length > 2 ? `${match.substring(0,3)}` : match
		const part2 = match.length > 3 ? `-${match.substring(3, 6)}` : ''
		const part3 = match.length > 6 ? `-${match.substring(6, 10)}` : ''    
		$('#phone').val(`${part1}${part2}${part3}`);
	}
};
function zipCodeAllowPermission(str,type){
	if(str != ''){
		if (str.match(/[^a-zA-Z0-9]/g)) {
            str = str.replace(/[^a-zA-Z0-9]/g, '');
            if(type == 1){
            	$('#bzipcode').val(str);
            }else if(type == 2){
            	$('#zipcode').val(str);
            }
            
        }
	}
}
function validateInvoiceNumber(str){
	if(str != ''){
		if (str.match(/[^a-zA-Z0-9,-]/g)) {
            str = str.replace(/[^a-zA-Z0-9]/g, '');
            $('#invoiceNumber').val(str);
            
        }
	}
}
