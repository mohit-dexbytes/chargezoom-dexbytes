/*
 *  Document   : formsWizard.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Forms Wizard page
 */
 
 

var FormsWizard = function() {

    return {
        init: function() {
            /*
             *  Jquery Wizard, Check out more examples and documentation at http://www.thecodemine.org
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Progress Wizard */
            $('#progress-wizard').formwizard({focusFirstInput: true, disableUIStyles: true, inDuration: 0, outDuration: 0});

            // Get the progress bar and change its width when a step is shown
            var progressBar = $('#progress-bar-wizard');
            progressBar
                .css('width', '20%')
                .attr('aria-valuenow', '20');

            $("#progress-wizard").bind('step_shown', function(event, data){
		if (data.currentStep === 'progress-first') {
                    progressBar
                        .css('width', '20%')
                        .attr('aria-valuenow', '20')
                        .removeClass('progress-bar-warning progress-bar-info progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-danger');
                }
                else if (data.currentStep === 'progress-second') {
                    progressBar
                        .css('width', '40%')
                        .attr('aria-valuenow', '40')
                        .removeClass('progress-bar-danger progress-bar-info progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-warning');
                }
                else if (data.currentStep === 'progress-third') {
                    progressBar
                        .css('width', '60%')
                        .attr('aria-valuenow', '60')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-info');
                }
				else if (data.currentStep === 'progress-fourth') {
                    progressBar
                        .css('width', '80%')
                        .attr('aria-valuenow', '80')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-info progress-bar-success')
                        .addClass('progress-bar-primary');
                }
				else if (data.currentStep === 'progress-fifth') {
                    progressBar
                        .css('width', '100%')
                        .attr('aria-valuenow', '100')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-info progress-bar-primary')
                        .addClass('progress-bar-success');
                }
            });

            /* Initialize Basic Wizard */
            $('#basic-wizard').formwizard({disableUIStyles: true, inDuration: 0, outDuration: 0});

            /* Initialize Advanced Wizard with Validation */
            $('#advanced-wizard').formwizard({
                disableUIStyles: true,
                validationEnabled: true,
                validationOptions: {
                    errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                    errorElement: 'span',
                    errorPlacement: function(error, e) {
                        e.parents('.form-group > div').append(error);
                    },
                    highlight: function(e) {
                        $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                        $(e).closest('.help-block').remove();
                    },
                    success: function(e) {
                        // You can use the following if you would like to highlight with green color the input after successful validation!
                        e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                        e.closest('.help-block').remove();
                    },
                   
                    rules: {
                         'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'companyAddress1': {
                        required: true,
                        minlength: 5
                    },
					  'companyCountry': {
                        required: true,
                         minlength: 3
                    },
                    'companyState': {
                        required: true,
                        minlength: 2
                    },
					  'companyCity': {
                        required: true,
                         minlength: 3
                    },
					  'companyContact': {
                        required: true,
                         minlength: 3
                    },
                    'zipCode': {
                        required: true,
                        minlength: 5
                    }
                    },
               
              
                  
                   
                },
                inDuration: 0,
                outDuration: 0
            });

            /* Initialize Clickable Wizard */
            var clickableWizard = $('#clickable-wizard');

            clickableWizard.formwizard({disableUIStyles: true, inDuration: 0, outDuration: 0});

            $('.clickable-steps a').on('click', function(){
                var gotostep = $(this).data('gotostep');

                clickableWizard.formwizard('show', gotostep);
            });
        }
    };
}();