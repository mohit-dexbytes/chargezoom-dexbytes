var jsplandata;
var base_url = $('#js_base_url').val();
  
	 	$(function() {
		$('#taxes').change( function(){
			   if($(this).val() !=="" ){
			   $('.set_taxes').show();
			   $('.show_check').show();
			   }else{
					$('#tax_check').val('');
					$('.set_taxes').hide();
			   }
		  });
	
	
         $('.tax_div').change( function(){
            var tax_id = $(this).val();
            var res = tax_id.split(",");
            var rateid = res[0];
            $.ajax({
            url: base_url+"FreshBooks_controllers/SettingSubscription/get_tax_id",
            type: 'POST',
        	data:{tax_id:rateid},
        	dataType: 'json',
            success: function(data){
        	//	console.log(data);
                     		
        			 $('.tax_checked').val(data.taxRate);
        			 
        			 
             	}	
             });
	
        });



		  
	      var nowDate = new Date();
          var today = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
			 $("#invoice_date").datepicker({ 
              format: 'yyyy-mm-dd',
              startDate:today,
              autoclose: true
            });  
			
			  $('.inv_date').click(function () {
                $('#invoice_date').datepicker('show');
            });
            
			$('#sub_start_date').datepicker({
			format: 'yyyy-mm-dd',
			startDate:today,
			autoclose: true
			});
			
			 $('.sub_date').click(function () {
                $('#sub_start_date').datepicker('show');
            });
            
            
			$('#subsamount').blur(function(){
				
				var dur = $('#duration_list').val();
              var subsamount = $('#subsamount').val();
			  var tot_amount = subsamount*dur ;
			  $('#total_amount').val(tot_amount.toFixed(2));
			  $('#total_invoice').val(dur);
				
			});
			
			
					
		  $('#card_list').change( function(){
			   if($(this).val()=='new1'){
			   $('#set_credit').show();
			   $('#set_bill_data').show();
			   }else{
					  $('#card_number').val('');
			$('#set_credit').hide();
			$('#set_bill_data').hide();
			   }
		  });
					
		$('#customerID').change(function(){
			
			var cid  = $(this).val();
		
			if(cid!==""){
				$('#card_list').find('option').not(':first').remove();
				$.ajax({
					type:"POST",
					url : base_url+"FreshBooks_controllers/Transactions/check_vault",
					data : {'customerID':cid},
					success : function(response){
						
							 data=$.parseJSON(response);
							//console.log(data);
							 if(data['status']=='success'){
							
								  var s=$('#card_list');
								 //console.log (data['card']);
								  var card1 = data['card'];
									 $(s).append('<option value="new1">New Card</option>');
									for(var val in  card1) {
										
									  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
									}
																   
									$('#address1').val(data['address1']);
									$('#address2').val(data['address2']);
									$('#city').val(data['City']);
									$('#state').val(data['State']);
									$('#zipcode').val(data['zipCode']);
									$('#phone').val(data['phoneNumber']);
									
								  
						   }	   
						
					}
					
					
				});
				
			}	
		});		
				
	   
        $('.force-numeric').keydown(function(e)
        {
            
   var key = e.charCode || e.keyCode || 0;
            // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
            // home, end, period, and numpad decimal
            return (
                key == 8 || 
                key == 9 ||
                key == 13 ||
                key == 46 ||
                key == 110 ||
                key == 190 ||
                (key >= 35 && key <= 40) ||
                (key >= 48 && key <= 57) ||
                (key >= 96 && key <= 105));
        });
	    	$('input.float').bind('keypress', function() {
			  this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
			});
			
              $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                
                
                   rules: {
                     sub_name:{
						  required:true,
						  minlength:3,
					 },
					 sub_start_date: {
							 required:true,
						},
					autopay:{
							 required:true,
					},
					gateway_list:{
							 required:true,
					},
					card_list:{
							 required:true,
					},
					paycycle:{
							 required:true,
					},		
			    	 duration_list: {
                        required: true,
                        number: true,
						maxlength:2,
						
                    },
					invoice_date: {
                        required: true,
                        
                    },
                    customerID: {
                         required: true,
                       
                    },
					 subsamount: {
                        required: true,
						number:true,
						
                    },
					friendlyname:{
						 required: true,
						 minlength: 3,
					},
					  card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },	
					
					address1:{
						required:true,
					},
					address2:{
						required:true,
					},
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},
					
					phone:{
						required:true,
						minlength:10,
						maxlength:10,
						digits:true,
					},
				
					
                  freetrial:{
						required: true,
						digits:true,
						check_free:{
							sub_start_date:'#sub_start_date',
							paycycle:'#paycycle',
							duration_list:'#duration_list',
						}
					},
					
					 baddress1:{
						required:true,
					},
				
                  	bcountry:{
						required:true,
					},
					bstate:{
						required:true,
					},
					bcity:{
						required:true,
					},
					bzipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},	
						
				
					bphone:{
						required:true,
						minlength:10,
						maxlength:10,
						digits:true,
					},
                    	
					
                },
               
            });
			  $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
			  $.validator.addMethod('check_free', function(value, element, params) {  
			
		  var duration = $(params.duration_list).val();
		  
		 // alert(duration);
		  if(duration =='0')return true;
		  
		  var frequency = $(params.paycycle).val();
		  var stdate     = new  Date( Date.parse($(params.sub_start_date).val()));
		  
		  var free = value;
		//  var result  =  get_frequncy_val(duration,new Date(stdate), frequency);
			
		  return (duration > free);
		}, 'Value should be less than Recurrence value');
								
		
	});
	
	 var sbID =document.getElementById('subID').value;
		var grand_total1=0;
	  var  jsdata = '';
	  var jsplandata='';
    var room=0;
    async function init() { 
     
          await $.ajax({
              
               'type': "POST",
              
               'data':{'subID':sbID},
	            'url':base_url+"FreshBooks_controllers/home/get_subs_item_count_data",  
	            'success':function(data) {
	              
	             //  console.log(data);
                  jsdata=  $.parseJSON(data); 
               
                return jsdata;   
	            }
             });
     
	if(jQuery.isEmptyObject(jsdata['items']))
   {
	    
		 room=1;
	  	$('#grand_total').html(format22(grand_total1));
		
	}else{
	
	
	var room =jsdata['rows'] ;
		$( ".total_val" ).each(function(){
			var test = $(this).val();
            grand_total1+= parseFloat(test);
			});
		
		$('#grand_total').html(format2(grand_total1,2));

	}
	

	    
	    await $.ajax({
           
            'type': "POST",
             'async' : true,
            'url'  :base_url+"FreshBooks_controllers/home/get_product_data",  
            'success':function(data) {
             jsplandata = 	 $.parseJSON(data); 
            }
         });
         
         
	 
    }
    
    init();
    
    
    
    

         
	function item_fields()
	{
	   	room++;
	
		var plan_data =jsplandata; 
		var plan_html ='<option val="">Select Product and Servicesss</option>';
		for(var val in  plan_data) {     
		    plan_html+='<option value="'+ plan_data[val]['productID']+'">'+plan_data[val]['Name']+'</option>'; }
	
		var onetime_html ='<option val="0">Recurring</option><option val="1">One Time Charge</option>';
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		var show_tax = '';
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('.tax_checked').val();
		}
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge'+room+'" name="onetime_charge[]">'+ onetime_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="Price"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center"   maxlength="3" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Quantity"></div></div>   <div class="col-sm-1 nopadding"><div class="set_taxes" ><div class="form-group"> <input type="checkbox" id="tax_check'+room+'" onchange="set_tax_val(this, '+room+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest);
		
	     
  }
  
  
     function item_invoice_fields()
	{
	
	
		room++;
	
		var plan_data =jsplandata; 
		var plan_html ='<option value="">Select Product or Service</option>';
		for(var val in  plan_data) {      console.log(plan_data[val]);   plan_html+='<option value="'+ plan_data[val]['Name']+'">'+plan_data[val]['Name']+'</option>'; }
		
		
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("div");
		divtest.setAttribute("class", "form-group removeclass"+room);
		var rdiv = 'removeclass'+room;
		
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('.tax_checked').val();
		}
		
		divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control" id="unit_rate'+room+'" name="unit_rate[]" value="" onblur="set_unit_val('+room+');" placeholder="Unit Rate"></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control quantity_input_box" maxlength="3" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Quantity"></div></div>  <div class="col-sm-1 nopadding"><div class="set_taxes" ><div class="form-group"> <input type="checkbox" onkeypress="return isNumberKeys(event)" id="tax_check'+room+'" onchange="set_tax_val(this, '+room+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	<div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+room+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div>';
		
		objTo.appendChild(divtest)
  }
  
  
  
  
   function remove_education_fields(rid)
   {
	  
	   var rid_val = $('#total'+rid).val();
	   
	   var gr_val  = 0;
	   
	   	$( ".total_val" ).each(function(){
			
            gr_val+= parseFloat($(this).val());
			});
	    if(rid_val){
	   var dif     = parseFloat(gr_val)-parseFloat(rid_val);
	   $('#grand_total').html(format22(dif));
		}
	   $('.removeclass'+rid).remove();
	   
  }


   
   
/**********Check the Validation for free trial**********************/
	function weeksBetween(d1, d2)
	{
		
     return Math.round((d2 - d1) / (7 * 24 * 60 * 60 * 1000));
	
   }
   function daysbeween(d1, d2) 
   {
		
     return Math.round((d2 - d1) / (24 * 60 * 60 * 1000));
	
   }
   
   function get_months(d1, d2){
	 

return difference = (d2.getFullYear()*12 + d2.getMonth()) - (d1.getFullYear()*12 + d1.getMonth()); 
	   
   }  
   
  function get_frequncy_val(du, new1date, fr){
		   var res='';
		 
		       var CurrentDate =  new Date(new1date.getFullYear(), new1date.getMonth(), new1date.getDate(), 0, 0, 0, 0); 
				  CurrentDate.setMonth(CurrentDate.getMonth() + parseInt(du));
                  var newdate = new Date(CurrentDate);
				
		 if(fr=='dly'){
           res= daysbeween(new Date(new1date), new Date(newdate));
		 }else if(fr=='1wk'){
			  res= weeksBetween(new Date(new1date), new Date(newdate));
		 }else if(fr=='2wk'){
           res= weeksBetween(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='mon'){
           res= get_months(new Date(new1date), new Date(newdate));
		 }else if(fr=='2mn'){
         res= get_months(new Date(new1date), new Date(newdate))/2;
		 }else if(fr=='qtr'){
          res= get_months(new Date(new1date), new Date(newdate))/3;
		 }else if(fr=='six'){
           res= get_months(new Date(new1date), new Date(newdate))/6;
		 }else if(fr=='yr1'){
          res= get_months(new Date(new1date), new Date(newdate))/12;
		 }else if(fr=='yr2'){
		 res= get_months(new Date(new1date), new Date(newdate))/24;
		 }else if(fr=='yr3'){
			 res= get_months(new Date(new1date), new Date(newdate))/36;
		 }	
           return res;    			 
		   
	   } 
	   
/************End*******************/	   
	
   
   function chk_payment(r_val)  {  
   
     
      if(r_val=='1'){
		  $('#set_pay_data').show();
	  }else{
		$('#set_pay_data').hide();
      }	  

   }   
 
   
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		
		$.ajax({ 
		     type:"POST",
			 url:base_url+"FreshBooks_controllers/Freshbooks_invoice/get_item_data",
			data: {'itemID':itemID },
			success:function(data){

			var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['SalesDescription']);
             $('#unit_rate'+rid).val(roundN(item_data['saleCost'],2));		
		 $('#quantity'+rid).val(1);		
		 $('#total'+rid).val(roundN(($('#quantity'+rid).val()*$('#unit_rate'+rid).val()),2));
            
            var grand_total=0;
	    	$( ".total_val" ).each(function(){
            var tval = $(this).val() != '' ? $(this).val() : 0;
            grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
	    	$('#grand_total').html(format22(grand_total));
		
		
            }	
		});
	}
   

	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
		$('#total'+rid).val(total.toFixed(2));
		
		//$(total_val)
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
	}
	
	function set_qty_val(rid){
       
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val(); 
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax;   
		$('#total'+rid).val(total.toFixed(2));
			//$(total_val)
		var grand_total=0;
		$( ".total_val" ).each(function(){ 
				var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));
		
	}	
	
	function set_tax_val(mythis, rid){
		
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = $('#tax_check'+rid).val();
  
		if(mythis.checked){
			var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
			$('#total'+rid).val(total.toFixed(2));
		}
		else {
			var total  = qty*rate; 
			$('#total'+rid).val(total.toFixed(2));
		}
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
		   var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
		});
			
		$('#grand_total').html(format22(grand_total));
	}
	
	


	
	 function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode;
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
	  
	  
function IntegerAndDecimal(e,obj,isDecimal)
{
    if ([e.keyCode||e.which]==8) //this is to allow backspace
    return true;

    if ([e.keyCode||e.which]==46) //this is to allow decimal point
    {
      if(isDecimal=='true')
      {
        var val = obj.value;
        if(val.indexOf(".") > -1)
        {
            e.returnValue = false;
            return false;
        }
        return true;
      }
      else
      {
        e.returnValue = false;
        return false;
      }
    }

    if ([e.keyCode||e.which] < 48 || [e.keyCode||e.which] > 57)
    e.preventDefault? e.preventDefault() : e.returnValue = false; 
}



function isNumberKeys(evt)
           {
               var charCode = (evt.which) ? evt.which : event.keyCode;
 
               if (charCode == 46)
               {
                   var inputValue = $("#inputfield").val()
                   if (inputValue.indexOf('.') < 1)
                   {
                       return true;
                   }
                   return false;
               }
               if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
               {
                   return false;
               }
               return true;
           }

    	
 async function set_plan_change()
 {
       
       var plan_id = $('#sub_plan').val();
       
        $('#item_fields').empty();
     
		room++;
		
		
		await $.ajax({
           
            'type': "POST",
            'url'  :base_url+"FreshBooks_controllers/home/get_product_data",  
            'success':function(datas) {
             jsplandata = 	 $.parseJSON(datas); 
             
            }
         });
         
    	var	plan_data = jsplandata;
		plan_html = '<option value="0" >Select Product or Service</option>';
			for(var val in  plan_data) {   
			    plan_html+='<option value="'+ plan_data[val]['productID']+'">'+plan_data[val]['Name']+'</option>'; 
			    
				}
		var onetime_html ='<option value="0">Recurring</option><option value="1">One Time Charge</option>';
			var show_tax = '';
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('.tax_checked').val();
		}
		 var i = 1;
       	$.ajax({
					type:"POST",
					url : base_url+ "FreshBooks_controllers/SettingSubscription/get_subplan",
					data : {'planID':plan_id},
					success : function(response){
					
							  data=$.parseJSON(response);
						   
							 $('#freetrial').val(data[0].freeTrial);
						
							 $('#paycycle').val(data[0].invoiceFrequency);
							 $('#duration_list').val(data[0].subscriptionPlan);
							 $('#grand_total').html((data[0].subscriptionAmount));
							
							if(data[0].emailRecurring == 1){
							    $("input[name=email_recurring][value='1']").prop("checked",true);
							}else{
							      $("input[name=email_recurring][value='0']").prop("checked",true);
							}
							
							if(data[0].automaticPayment == 1){
							    $("input[name=autopay][value='1']").prop("checked",true);
							    $('#set_pay_data').show();
							}else{
							    $("input[name=autopay][value='0']").prop("checked",true);
							     $('#set_pay_data').hide();
							}
							
						var itemval;
							for(var t = 0; t < data.length; t++){
							   
							   $('#item_fields').append( '<div class="form-group removeclass'+i+'"><div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control tester"  onchange="select_plan_val('+i+');"  id="productID'+i+'" name="productID[]">'+ plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge'+i+'" name="onetime_charge[]">'+ onetime_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+i+'" name="description[]" value="" placeholder="Description "></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+i+'" name="unit_rate[]" value="" onblur="set_unit_val('+i+');" placeholder="Price"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control" onblur="set_qty_val('+i+');" id="quantity'+i+'" name="quantity[]" value="" placeholder="Quantity"></div></div>   <div class="col-sm-1 nopadding"><div class="set_taxes" ><div class="form-group"> <input type="checkbox" id="tax_check'+i+'" onchange="set_tax_val(this, '+i+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+i+'" name="total[]" value="" placeholder="Total"> <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ i +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></div> </div> <div class="clear"></div></div> ');
							   $('#item_fields').find('.tester option[value="0"]').attr('selected',true);
							   $('#productID'+i).val(data[t].itemListID);
							   $('#onetime_charge'+i).val(data[t].oneTimeCharge);
							   $('#description'+i).val(data[t].itemDescription);
							   $('#unit_rate'+i).val(data[t].itemRate);
							   $('#quantity'+i).val(data[t].itemQuantity);
							   $('#total'+i).val((data[t].itemRate * data[t].itemQuantity).toFixed(2));
							   i++;
							   itemval = data[t].itemListID;
							  
							}
							 for(var val in  plan_data) { 
						            if(plan_data[val]['Name'] === itemval ){
		                           console.log(plan_data[val]['productID']);
							             $("#tester > [value=" + plan_data[val]['productID'] + "]").attr("selected", "true");
						            }
                                }
						
						
						setTimeout(function(){
						    	$("input[name='quantity']").trigger('click');
						}, 1000);
					
					
					    
					}
					
					
				});
        
}
    
    
    
function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

 // return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}




function set_fb_subs_id(sID)
{
	
	  $('#fbsubscID').val(sID);
	
}

function delete_fb_subscription()
{
    
  var formdata=  $('#del_ccjkckfb').serialize();
//  alert ('formdata');
$(".close1" ).trigger("click");
$.ajax({  
		       
			type:'POST',
			url:  base_url+"FreshBooks_controllers/SettingSubscription/delete_subscription",
		        
			data: formdata,
		
			success : function(response){
			 data= $.parseJSON(response);
			 
			  if(data['status']=='success'){
			        
			      
					  location.reload(true);
				  } 
			}
		});



}
    
    window.setTimeout("fadeMyDiv();", 2000);
        function fadeMyDiv() {
		   // $(".msg_data").fadeOut('slow');
}