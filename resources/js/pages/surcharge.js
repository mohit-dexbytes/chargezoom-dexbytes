$(document).ready(function(){
    $('#surchargeEnableForm').validate({
        errorPlacement : function(error, element) {
            if (element.attr('name') == 'task'){
                $('ul.task-list').find('label.checkbox').addClass('state-error');
            } else {
                error.insertAfter(element.parent());
            }
        },
        rules: {
            'isAgree': {
                required: true,
            }
        },
        submitHandler: function (form) {
            $("#btn_enable").attr("disabled", true);
            return true;
        }
    });  
    $('#saveSurchargeForm').validate({
        rules: {
            'default_item_id': {
                required: true,
            },
            'default_account_id': {
                required: true,
            },
            'surcharge_percent': {
                required: true,
                min:0,
                max:4
            }
        },
        messages: {
            'surcharge_percent': {
                min: 'Please enter a value greater than or equal to 0.00.',
                max:'Please enter a value less than or equal to 4.00.'
            }
        },
    }); 
           
});
/* convert surcharge amount value in decimal value */
$('#surcharge_percent').blur(function(){
    var surcharge_percent = $(this).val();
    /* Check percentage value is not null */
    if(surcharge_percent != ''){
        $('#surcharge_percent').val(parseFloat(surcharge_percent).toFixed(2));
    } 
});