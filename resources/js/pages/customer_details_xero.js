  
  var base_url = $('#js_base_url').val();
  var gtype='';
  



function set_invoice_schedule_date_id(id,cid,in_val, ind_date)
{
    
    $('#scheduleID').val(id);
    $('#schedule_date1').val('');

    var nowDate = new Date(ind_date);
    
   
     var inv_day = new Date( nowDate.getMonth(), nowDate.getDate()+1, nowDate.getFullYear()); 
     
            $("#schedule_date1").datepicker({ 
               format: 'mm/dd/yyyy',
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
              
              $('#schAmount').val(in_val);
		 
		if(cid!==''){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : base_url+'Xero_controllers/Payments/check_vault',
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              var s=$('#schCardID');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									if(card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase()!='ECHECK')
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
            
    
}      

 function get_invoice_id(inv_id){
    $('#invID').val(inv_id);
}   


 function set_view_history(eID)
 {
    if(eID!==""){
        
        
     $.ajax({
    url: base_url+'Xero_controllers/Settingmail/get_history_id',
    type: 'POST',
    data:{customertempID:eID},
    success: function(data){
        
        $('#data_history').html(data);
              
             
             
    }   
}); 
    
}

}





    
var Pagination_view1 = function() { 

    return {
        init: function() {
            $.extend($.fn.dataTableExt.oSort, {
           
            } );   

            /* Initialize Bootstrap Datatables Integration */
              App.datatables();

            /* Initialize Datatables */
            $('#outstand').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                
            });
        

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
    
    
var Pagination_view2 = function() { 
    
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#pinv').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [5] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

 
    
var Pagination_email11 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#email_hist11').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: true, targets: [1] }
                ],
                 
                order: [[ 1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
    
    
var Pagination_view31 = function() { 
    
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#sub_dta').dataTable({
                columnDefs: [
                     { type: 'date-custom', targets: [0, 1] },
                    { orderable: false, targets: [4] }
                ],
                 
                order: [[ 3, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
	


   
        
        $(function(){ 
            
            
           
       $('#btn_mask').click(function(){
    
     $('#m_card_id').hide();  
     $('#edit_card_number').removeAttr('disabled');
     $('#edit_card_number').attr('type','text');
      });
      
      

$('.radio_pay').click(function(){
	  var method = $(this).val(); 
	   if(method==1)
	   {
		   $('#checkingform').hide();
		   $('#ccform').show();
	   }else if (method==2){
			 $('#checkingform').show();
		   $('#ccform').hide();
	   }else {
		   
		 }		   
   });
      
        nmiValidation.init(); nmiValidation1.init();
        schvalidation.init();
        Pagination_view1.init();
      
        Pagination_view2.init();
       
         Pagination_view31.init(); 
                                 
         Pagination_email11.init(); 
        
      
    
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');




});
    

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
                    
                     expiry_year: {
                              CCExp: {
                                    month: '#expiry',
                                    year: '#expiry_year'
                              }
                        },
                    
                        
                     cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    
					edit_friendlyname:{
						 minlength: 3,
					},
				
                  	
					zipcode:{
						minlength:3,
						maxlength:10,

					},
					contact:{
                         minlength: 10,
                         maxlength: 17,
                         phoneUS:true,
					}, 
                  
                  
                  
                },
              
            });
            
   $.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
           return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
                            
$.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
         
                  
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
            
            

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									required: true,
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                    
                
					edit_friendlyname:{
						 minlength: 3,
					},
				
                  	
					bzipcode:{
						minlength:3,
						maxlength:10,

					},
					bcontact:{
                         minlength: 10,
                         maxlength: 17,
                         phoneUS:true,
					}, 
                  
                  
                },
              
            });
     $.validator.addMethod("phoneUS", function(value, element) {
         
         if(value=='')
         return true;
           return value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);
   
        }, "Please specify a valid phone number");
                            
$.validator.addMethod("ZIPCode", function(value, element) {
       
       return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
    },"Only alphanumeric and hyphen is allowed" );
         			
      $.validator.addMethod('CCExp', function(value, element, params) {  
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
		
        }
    };
}();

	
var schvalidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#Qbo-form1_schedule').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    schedule_date: {
                        required: true,
					
                    },
					
					 sch_method: {
							  required: true,
						},
				       sch_gateway: {
							  required: true,
						},	
					    schCardID: {
							  required: true,
						},
				       schAmount: {
							  required: true,
							  number:true,
						},	
                   	
                  
                  
                },
              
            });
			
 
		
        }
    };
}();

	
	
function delele_notes(note_id){
	
	if(note_id !=""){
		
		
		$.ajax({  
		       
			type:'POST',
			url:base_url+'QBO_controllers/home/delele_note',
			data:{ 'noteID': note_id},
			success : function(response){
				
				
				   data=$.parseJSON(response);
				  
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
	}
	
}

function add_notes(){
    
    var formdata= $('#pri_form').serialize();
    if($('#private_note').val()!=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:base_url+'QBO_controllers/home/add_note',
			data:formdata,
			success : function(response){
				
				
		     	 data=$.parseJSON(response);
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
    }
    
}

 function set_card_user_data(id, name){
      
	   $('#customerID11').val(id);
	   $('#customername').val(name);
 }
		 
	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		 
		if(cardID!=""){
			
			$.ajax({
				type:"POST",
				url : base_url+'QBO_controllers/Payments/get_card_edit_data',
				data : {'cardID':cardID},
				success : function(response){
					     data=$.parseJSON(response);
						
					       if(data['status']=='success'){
							    
                                $('#edit_cardID').val(data['card']['CardID']);
                                if(data['card']['CardNo']!="" || data['card']['CardCVV']!="")
                                {
                                    $('#editccform').css('display','block');
    							    $('#edit_card_number').val(data['card']['CardNo']);
    								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
    							  	document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
    								 $('#m_card_id').show();  
                                     $('#edit_card_number').attr('disabled','disabled');
                                     $('#edit_card_number').attr('type','hidden');
                                     $('#m_card').html(data['card']['CardNo']);
    								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
    								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
                                }
                                else
                                {
                                       $('#editcheckingform').css('display','block');
        							    $('#edit_acc_number').val(data['card']['accountNumber']);
        								document.getElementById("edit_acc_name").value =data['card']['accountName'];
        								document.getElementById("edit_route_number").value =data['card']['routeNumber'];
        						
        								$('select[name="edit_secCode"]').find('option[value="'+data['card']['secCodeEntryMethod']+'"]').attr("selected",true);
        								$('select[name="edit_acct_type"]').find('option[value="'+data['card']['accountType']+'"]').attr("selected",true);
        								$('select[name="edit_acct_holder_type"]').find('option[value="'+data['card']['accountHolderType']+'"]').attr("selected",true);
                                    
                                }
						        $('#baddress1').val(data['card']['Billing_Addr1']);
							    $('#baddress2').val(data['card']['Billing_Addr2']);
							    $('#bcity').val(data['card']['Billing_City']);
							    $('#bstate').val(data['card']['Billing_State']);
							    $('#bcountry').val(data['card']['Billing_Country']);
							    $('#bcontact').val(data['card']['Billing_Contact']);
							    $('#bzipcode').val(data['card']['Billing_Zipcode']);
								  if(data['card']['is_default'] == 1){
                      $('#defaultMethod').prop('checked', true);
                      $('#defaultMethod').prop('disabled', true);
                      $('#defaultMethod').val(1);
                  }else{
                      $('#defaultMethod').prop('checked', false);
                      $('#defaultMethod').prop('disabled', false);
                      $('#defaultMethod').val(0);
                  }
							
					   }	 	   
					
				}
				
				
			});
			
		}	  
		   
	}	
	


function set_qbo_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    $('#totalPay').val(0.00);
      $('#totalMultiInvoiceTotal').html('0.00');
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url :  base_url+'QBO_controllers/home/get_qbo_customer_invoices',
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
										if(card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase()!='ECHECK')
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
					     	 var card_daata1=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control"  autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11"   name="cvv" class="form-control" autocomplete="off"  /></div></div><div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div><div id="frdname" class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control"  /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="'+data['BillingAddress_Addr1']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="'+data['BillingAddress_Addr1']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="'+data['BillingAddress_City']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="'+data['BillingAddress_State']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="'+data['BillingAddress_PostalCode']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="'+data['BillingAddress_Country']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="'+data['Phone']+'" ></div></div></fieldset>';

						 $('.card_div').html(card_daata1);  
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}
                                         

 function set_qbo_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : base_url+'QBO_controllers/home/get_gateway_data',
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
						gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   =  base_url+'QBO_controllers/PaytracePayment/pay_multi_invoice';
							  }else if(gtype=='2'){
									var url   =  base_url+'QBO_controllers/AuthPayment/pay_multi_invoice';
							 }else if(gtype=='1'){
							   var url   =  base_url+'QBO_controllers/Payments/pay_multi_invoice';
							 }else if(gtype=='4'){
							   var url   =  base_url+'QBO_controllers/PaypalPayment/pay_multi_invoice';
							 }else if(gtype=='5'){    
							   var url   =  base_url+'QBO_controllers/StripePayment/pay_multi_invoice';
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }else if(gtype=='6') {
							     var url = base_url+'QBO_controllers/UsaePay/pay_multi_invoice';
							 } else if(gtype=='7'){
							     var url = base_url+'QBO_controllers/GlobalPayment/pay_multi_invoice';
							 }	
                			else if(gtype=='8'){
							     var url = base_url+'QBO_controllers/CyberSource/pay_multi_invoice';
							 }	
				            
				            $("#thest_pay1").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	



function set_template_data_qbo(c_id,cmp_name,cmp_id,cust_Email )
{
		
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		

	
 function set_xero_url()
 {
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : base_url+'Xero_controllers/home/get_gateway_data',
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
						       gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   =base_url+'Xero_controllers/PaytracePayment/pay_invoice';
							  } if(gtype=='2'){
									var url   = base_url+'Xero_controllers/AuthPayment/pay_invoice';
							 } if(gtype=='1'){
							   var url   = base_url+'Xero_controllers/Payments/pay_invoice';
							 } if(gtype=='4'){
							   var url   = base_url+'Xero_controllers/PaypalPayment/pay_invoice';
							 } if(gtype=='5'){
							   var url   = base_url+'Xero_controllers/StripePayment/pay_invoice';
							   
							   
							   
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  	
							 
							 if(gtype=='6'){
							   var url   = base_url+'Xero_controllers/UsaePay/pay_invoice';
							 }
							 
							 if(gtype=='7'){
							   var url   = base_url+'Xero_controllers/GlobalPayment/pay_invoice';
							 }
							 
							 
							 
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }
	
	
	function set_template_data_temp(invoiceID, custID,typeID ){
		
	   
          document.getElementById('invoicetempID').value=invoiceID;
		 document.getElementById('customertempID').value=custID;
		  
		  	if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : base_url+'QBO_controllers/Settingmail/set_template',
			data : {typeID:typeID,invoiceID:invoiceID,customerID:custID },
			success: function(data){
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
				if(data['replyTo'] != '') {
					$('#replyTo').val(data['replyTo']);
				}
				$('#emailSubject').val(data['emailSubject']);
				$('#toEmail').val(data['toEmail']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
				$('#fromEmail').val(data['fromEmail']);
				$('#mailDisplayName').val(data['mailDisplayName']);
						$('#invoiceCode').val(data['invCode']);
		  
		  }
	   });	 
	   
	 } 
	
    }	

	 $(function(){  
		
				 	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'insert' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
				});
				
		  $('#type').change(function(){
			var typeID = $(this).val();

			var invoiceID  =  $('#invoicetempID').val();

		//	var customer   = $('#tempCompanyName').val();

		var customerID =  $('#customertempID').val();
		    
		  
			if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : base_url+'QBO_controllers/Settingmail/set_template',
			data : {typeID:typeID,invoiceID:invoiceID,customerID:customerID },
			success: function(data){
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
				$('#emailSubject').val(data['emailSubject']);
				$('#toEmail').val(data['toEmail']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
					$('#replyEmail').val(data['replyTo']);
						$('#invoiceCode').val(data['invCode']);
		  
		  }
	   });	 
	   
	 } 





 }); 

 $('#open_cc').click(function(){
	$('#cc_div').show();
	$(this).hide();
});
$('#open_bcc').click(function(){
	$('#bcc_div').show();
	$(this).hide();
});
	$('#open_reply').click(function(){
	  $('#reply_div').show();
	   $(this).hide();
}); 

$('#open_from_email').click(function(){
	$('#from_email_div').show();
	$(this).hide();
});
$('#open_display_name').click(function(){
	$('#display_name_div').show();
	$(this).hide();
});

     
        	$('#CardID').change(function(){
		var cardlID =  $(this).val();
	
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : base_url+'QBO_controllers/Payments/get_card_data',
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
					        
					       
					        if(gtype=='5')
					        {
					            
						 var form = $("#thest_pay");	
                        
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
                        
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
                             if(pub_key){
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);
                        }
									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				}	
			});
		  }
	});		
       
	         $('#del_ccjkck_qbo').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    check_number: {
                        required: true,
					    minlength: 3,
					   	maxlength: 20,
                    },
					
					 payment_date: {
							  required: true,
						},
				      
				       inv_amount: {
							  required: true,
							  number:true,
						},	
                   	
                  
                  
                },
              
            });
			
 
		
				
			
	 });    

	
 function set_refund_invoices(invID)
{
         
      if(invID!=="")
      {
        $('#ref_invID').val(invID);
        $.ajax({
             type:"POST",
             url:base_url+"ajaxRequest/get_invoice_transactions",
             data:{invID:invID},
             success:function(response){
                   data=$.parseJSON(response); 
                 
                   if(data.status=='success')
                   {
                       
                       $('#ref_id').html(data.transactions);
                        $('#rf_btn').removeAttr('disabled');
                       
                   }else{
                       
                      $('#ref_id').html('<span>N/A</span>');
                      
                      $('#rf_btn').attr('disabled','disabled');
                   }
                 
             }
              
          });
       
      }
    
}

	
	
function qbo_schedule_payment()
{
    
      var test =  $('#Qbo-form1_schedule').valid();
 
    if(test)
    {
  var formdata=  $('#Qbo-form1_schedule').serialize();
$(".close1" ).trigger("click");
$.ajax({  
		       
			type:'POST',
			url:  base_url+"QBO_controllers/home/invoice_schedule",
		        
			data: formdata,
		
			success : function(response){
			 data= $.parseJSON(response);
			 
			  location.reload(true);
				  
			}
		});

    }
    return false;

}

 
function set_sub_status_id(sID,amount){
	  $('#qbo_subscID').val(sID);
	  $('#inv_amount1').val(amount);

    
}
 

	
	
	
