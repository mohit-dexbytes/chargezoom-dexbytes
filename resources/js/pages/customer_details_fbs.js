  
var base_url = $('#js_base_url').val();

 function set_invoice_schedule_date_id(id,cid,in_val, ind_date)
{
    
    $('#scheduleID').val(id);
    $('#schedule_date1').val('');

    var nowDate = new Date(ind_date);
    
   
     var inv_day = new Date( nowDate.getMonth(), nowDate.getDate()+1, nowDate.getFullYear()); 
     
            $("#schedule_date1").datepicker({ 
               format: 'mm/dd/yyyy',
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
              
              $('#schAmount').val(in_val);
		 
		if(cid!==''){
			$('#schCardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : base_url+'FreshBooks_controllers/Transactions/check_vault',
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              
							  var s=$('#schCardID');
						  $('#schCardID').empty();
							  
                            $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							  var recentCard = data.recent_card;
							    
							    for(var val in  card1) {
									var selectedCard = false;
									if(card1[val]['CardID'] == recentCard){
										selectedCard = true;
									}
										if(card1[val]['CardType']==null || card1[val]['CardType'].toUpperCase()!='ECHECK')
								  $("<option />", {value: card1[val]['CardID'], selected: selectedCard, text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							  var card_daata='<div class="sch_crd"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control"  autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11" name="cvv" class="form-control"  autocomplete="off" /></div></div></fieldset></div>'+
                   
                   '<div style="display:none;" class="sch_chk"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Number</label><div class="col-md-8"><input type="text" id="acc_number" name="acc_number" class="form-control" value="" ></div></div>'+
                    '<div class="form-group"><label class="col-md-4 control-label" for="card_number">Routing Number</label><div class="col-md-8"><input type="text" id="route_number" name="route_number" class="form-control" ></div></div>'+
					'<div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Name</label><div class="col-md-8"><input type="text" id="acc_name" name="acc_name" class="form-control" value="" ></div> </div>'+
					
					'<div class="form-group"><label class="col-md-4 control-label" for="Entry Methods">Entry Method</label><div class="col-md-6"><div class="input-group"><select id="secCode" name="secCode" class="form-control valid" aria-invalid="false">'+
'<option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option>'+
 '</select>	</div></div></div>'+ 
                '<div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label><div class="col-md-6">'+
    '<select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false"><option value="business"  >Business</option><option value="personal"  >Personal</option> </select></div></div>'+
	'<div class="form-group"><label class="col-md-4 control-label" for="acct_type">Account Type</label><div class="col-md-6">'+
	'<select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false"><option value="checking" >Checking</option><option value="saving"  >Saving</option>'+
    '</select></div></div></fieldset></div>'+ 
     '<div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div>'+
      '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="'+data['address1']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="'+data['address2']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="'+data['City']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="'+data['State']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="'+data['zipCode']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="'+data['Country']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="'+data['phoneNumber']+'" ></div></div></fieldset>';
                   
                      $('.card_div_csh').html(card_daata);   
					  create_card_schedule_data();
					  var schValue = $("#ccChecked").val();
					  if(schValue == 2){
						  get_ach_details({value: 2});
						  return true;
					  } else if(s.val() == 'new1'){
							$('.card_div_csh').css('display','block');
						}else{
							$('.card_div_csh').css('display','none');
						}
					   }	   
					
				}
				
				
			});
			
		}	
            
    
}     


 function get_invoice_id(inv_id)
 {
    $('#invID').val(inv_id);
}       
        
 function set_view_history(eID){
    if(eID!==""){
        
        
     $.ajax({
    url: base_url+'FreshBooks_controllers/Settingmail/get_history_id',
    type: 'POST',
    data:{customertempID:eID},
    success: function(data){
        
        $('#data_history').html(data);
              
             
             
    }   
}); 
    
}

}

function set_sub_status_id(sID,amount){
	
	  $('#fb_subscID').val(sID);
	 $('#fb_inv_amount').val(amount);
}

        
        
$(function()
{    
  $('#btn_mask').click(function(){
    
  $('#m_card_id').hide();  
  $('#edit_card_number').removeAttr('disabled');
  $('#edit_card_number').attr('type','text');
});



$('.radio_pay').click(function(){
	  var method = $(this).val(); 
	   if(method==1)
	   {
		   $('#checkingform').hide();
		   $('#ccform').show();
	   }else if (method==2){
			 $('#checkingform').show();
		   $('#ccform').hide();
	   }else {
		   
		 }		   
   });
       
        nmiValidation.init(); nmiValidation1.init();
         schvalidation.init();
         addvalidation.init();
        Pagination_view1.init();
      
        Pagination_view2.init();
       
                                 
         Pagination_email.init();    
       
     
          
        
          /*********************Template Methods**************/ 

    
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
    



var Pagination_email = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#email_hist').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: true, targets: [1] }
                ],
                order: [[ 1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
				rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
						number: true,
						remote: {

							beforeSend: function () {
								$("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
		
							},
							complete: function () {
								$(".overlay1").remove();
							},
							url: base_url + "ajaxRequest/check_account_exists",
							type: "POST",
							data: {
								card_number: function () { return $("#card_number").val(); },
								acc_number: '',
								customerID: function () { return $("#customerID").val(); },
								account_type: function () { return $("input[name='formselector']:checked").val(); },
							},
							dataType: 'json',
							dataFilter: function (response) {
		
								var rsdata = jQuery.parseJSON(response);
		
								if (rsdata.status == 'success')
									return true;
								else {
									return false;
								}
							}
						},
                    },
                    
                     expiry_year: {
                              CCExp: {
                                    month: '#expiry',
                                    year: '#expiry_year'
                              }
                        },
                    
                        
                     cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    
                    friendlyname:{
						required:true,
						 minlength: 3,
					},
					acc_number:{
						required: true,
						digits: true,
						minlength: 3,
						maxlength: 20,
						remote: {

							beforeSend: function () {
								$("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
		
							},
							complete: function () {
								$(".overlay1").remove();
							},
							url: base_url + "ajaxRequest/check_account_exists",
							type: "POST",
							data: {
								acc_number: function () { return $("#acc_number").val(); },
								card_number: '',
								customerID: function () { return $("#customerID").val(); },
								account_type: function () { return $("input[name='formselector']:checked").val(); },
							},
							dataType: 'json',
							dataFilter: function (response) {
		
								var rsdata = jQuery.parseJSON(response);
								if (rsdata.status == 'success')
									return true;
								else {
									return false;
								}
							}
						},
                    },
                   route_number:{
                   		required: true,
						digits: true,
						minlength: 9,
						maxlength: 9,
                   },
					acc_name:{
						required: true,
						minlength: 3,
						maxlength: 30,
					},
                    
                    
					city:{
						minlength:2,
					},
					zipcode:{
						minlength:3,
						maxlength:10,
					},
					contact:{
                         minlength: 10,
                         maxlength: 17,
                         phoneUS:true,
					}, 
                },
				messages:{
					card_number: {
						remote: 'Card details already exists.'
					},
					acc_number: {
						remote: 'Account details already exists.',
						minlength: "Please enter at least 3 digits.",
						maxlength: "Please enter no more than 20 digits.",
						digits: 'Please enter valid account number.'
					},
					route_number: {
						minlength: 'Please enter at least 9 digits.',
						maxlength: 'Please enter no more than 9 digits.',
						digits: 'Please enter valid routing number.'
					},
				}
            });
            
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
            
            

            
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
				rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
						number: true,
						remote: {
							beforeSend: function () {
								$("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
		
							},
							complete: function () {
								$(".overlay1").remove();
							},
							url: base_url + "ajaxRequest/check_account_exists",
							type: "POST",
							data: {
								card_number: function () { return $("#edit_card_number").val(); },
								acc_number: '',
								customerID: function () { return $("#customerID").val(); },
								edit_cardID: function () { return $("#edit_cardID").val(); },
								account_type: 1,
							},
							dataType: 'json',
							dataFilter: function (response) {
		
								var rsdata = jQuery.parseJSON(response);
		
								if (rsdata.status == 'success')
									return true;
								else {
									return false;
								}
							}
						},
                    },
                    
					edit_expiry_year: {
						CCExp: {
							month: '#edit_expiry',
							year: '#edit_expiry_year'
						}
					},
                    
                        
					edit_cvv: {
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    
					
					edit_friendlyname:{
						minlength: 3,
					},
				
					
					bcity:{
						minlength:2,
					},
					bzipcode:{
						minlength:3,
						maxlength:10,
					},
					bcontact:{
						minlength: 10,
						maxlength: 17,
						phoneUS:true,
					},
					edit_acc_number:{
						required: true,
						digits: true,
						minlength: 3,
						maxlength: 20,
						remote: {

							beforeSend: function () {
								$("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
		
							},
							complete: function () {
								$(".overlay1").remove();
							},
							url: base_url + "ajaxRequest/check_account_exists",
							type: "POST",
							data: {
								acc_number: function () { return $("#edit_acc_number").val(); },
								card_number: '',
								customerID: function () { return $("#customerID").val(); },
								edit_cardID: function () { return $("#edit_cardID").val(); },
								account_type: 2,
							},
							dataType: 'json',
							dataFilter: function (response) {
		
								var rsdata = jQuery.parseJSON(response);
								if (rsdata.status == 'success')
									return true;
								else {
									return false;
								}
							}
						},
					},
					edit_route_number:{
						required: true,
						digits: true,
						minlength: 9,
						maxlength: 9,
					},
					edit_acc_name:{
						required: true,
						minlength: 3,
						maxlength: 30,
					},
				},
				messages:{
					edit_card_number: {
						remote: 'Card details already exists.'
					},
					edit_acc_number: {
						remote: 'Account details already exists.',
						minlength: "Please enter at least 3 digits.",
						maxlength: "Please enter no more than 20 digits.",
						digits: 'Please enter valid account number.'
					},
					edit_route_number: {
						minlength: 'Please enter at least 3 digits.',
						maxlength: 'Please enter no more than 12 digits.',
						digits: 'Please enter valid routing number.'
					},
				}
              
            });
            
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
            
            

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

    
var addvalidation= function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#del_ccjkck_fb').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
						payment_date: {
							required: true,
						
						},
					
					 	check_number: {
							required: true,
							minlength: 3,
							maxlength: 20,
						},
				     
                   	
				       inv_amount: {
							  required: true,
							  number:true,
						},	
                  
                  
                },
              
            });
			
 
		
        }
    };
}();

var schvalidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('FRB_form1_schedule').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    schedule_date: {
                        required: true,
					
                    },
					
					 sch_method: {
							  required: true,
						},
				       sch_gateway: {
							  required: true,
						},	
                   	 schCardID: {
							  required: true,
						},
				       schAmount: {
							  required: true,
							  number:true,
						},	
                  
                  
                },
              
            });
			
 
		
        }
    };
}();

	    
    
    
function delele_notes(note_id){
    
    if(note_id !==""){
        
        
        $.ajax({  
               
            type:'POST',
            url:base_url+'FreshBooks_controllers/home/delele_note',
            data:{ 'noteID': note_id},
            success : function(response){
                
                
                   data=$.parseJSON(response);
                  
                 
                  if(data['status']=='success'){
                      location.reload(true);
                  } 
            }
            
        });
        
    }
    
}

function add_notes(){
    
    var formdata=$("#pri_form").serialize();
    if($('#private_note').val()!=""){
    
    
        $.ajax({  
               
            type:'POST',
            url:base_url+'FreshBooks_controllers/home/add_note',
            data:formdata,
            success : function(response){
                
                
                 data=$.parseJSON(response);
                 
                  if(data['status']=='success'){
                      location.reload(true);
                  } 
            }
            
        });
        
    }
    
}

 function set_card_user_data(id, name){
      
       $('#customerID11').val(id);
       $('#customername').val(name);
 }
         
    function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();       
         
        if(cardID!=""){
            
            $.ajax({
                type:"POST",
                url : base_url+'FreshBooks_controllers/Transactions/get_card_edit_data',
                data : {'cardID':cardID},
                success : function(response){
                    
                         data=$.parseJSON(response);
                        
                           if(data['status']=='success'){
                                $('#edit_cardID').val(data['card']['CardID']);
                                if(data['card']['CardNo']!="")
                                {
                                    $('#editccform').css('display','block');
    							    $('#edit_card_number').val(data['card']['CardNo']);
    								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
    								 $('#m_card_id').show();  
                                     $('#edit_card_number').attr('disabled','disabled');
                                     $('#edit_card_number').attr('type','hidden');
                                     $('#m_card').html(data['card']['CardNo']);
    								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
    								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
                                }
                                else
                                {
                                       $('#editcheckingform').css('display','block');
        							    $('#edit_acc_number').val(data['card']['accountNumber']);
        								document.getElementById("edit_acc_name").value =data['card']['accountName'];
        								document.getElementById("edit_route_number").value =data['card']['routeNumber'];
        						
        								$('select[name="edit_secCode"]').find('option[value="'+data['card']['secCodeEntryMethod']+'"]').attr("selected",true);
        								$('select[name="edit_acct_type"]').find('option[value="'+data['card']['accountType']+'"]').attr("selected",true);
        								$('select[name="edit_acct_holder_type"]').find('option[value="'+data['card']['accountHolderType']+'"]').attr("selected",true);
                                    
                                }
                                
                             
                                $('#baddress1').val(data['card']['Billing_Addr1']);
							    $('#baddress2').val(data['card']['Billing_Addr2']);
							    $('#bcity').val(data['card']['Billing_City']);
							    $('#bstate').val(data['card']['Billing_State']);
							    $('#bcountry').val(data['card']['Billing_Country']);
							    $('#bcontact').val(data['card']['Billing_Contact']);
							    $('#bzipcode').val(data['card']['Billing_Zipcode']);
								if(data['card']['is_default'] == 1){
									$('#defaultMethod').prop('checked', true);
									$('#defaultMethod').prop('disabled', true);
									$('#defaultMethod').val(1);
								}else{
									$('#defaultMethod').prop('checked', false);
									$('#defaultMethod').prop('disabled', false);
									$('#defaultMethod').val(0);
								}
                       }       
                    
                }
                
                
            });
            
        }     
           
    }   
    
    
var Pagination_view1 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date', targets: [0, 1] },
                    { orderable: false, targets: [4] }
                ],
                 
                order: [[ 1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
    
    
var Pagination_view2 = function() { 
    
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#sub_page').dataTable({
                columnDefs: [
                    { type: 'date', targets: [1] },
                    { orderable: false, targets: [3] }
                ],
                 
                order: [[ 3, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
function set_template_data_freshbook(c_id,cmp_name,cmp_id,cust_Email,replyEmail='' )
{
		
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
		   if(replyEmail != ''){
		      document.getElementById('replyEmail').value = replyEmail;
		   }
    }	
    
  
   function set_fb_invoice_process_multiple(cid)
{

	    $("#btn_process").attr("disabled", true);
	    $('#totalPay').val(0.00);
        $('#totalMultiInvoiceTotal').html('0.00');
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay1');
		  $('#invDefaultcardSurchargeValue').val(0);
		if(cid!=""){
		     $('#inv_div').html('');
		     $('.card_div').html('');
		     
			$('#CardID1').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url :  base_url+'FreshBooks_controllers/Freshbooks_Customer/get_fb_customer_invoices',
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              var s=$('#CardID1');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									var selectedCard = false;
									if(data.recent_card == card1[val]['CardID']) {
										selectedCard = true;
										$('#invDefaultcardSurchargeValue').val(card1[val]['isSurcharge']);
									}
										if(card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase()!='ECHECK')
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
							    
							  var invoices = data['invoices'];
							    
							   $('#inv_div').html(invoices);
					     	 var card_daata1=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" onChange="get_card_surcharge_details({type :2 , cardNumber: this.value})" class="form-control" autocomplete="off" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11"   name="cvv" class="form-control" autocomplete="off" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div></fieldset>'+
						   
							  '<div class="panel panel-info notice_box surchargeNoticeClass2" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>' +

                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="'+data['address1']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="'+data['address2']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="'+data['City']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="'+data['State']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="'+data['zipCode']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="'+data['Country']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="'+data['phoneNumber']+'" ></div></div></fieldset>';

						 $('.card_div').html(card_daata1);  
						 create_card_multi_data();
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}                                      

 function set_fb_url_multi_pay()
 {
 
          
		    var gateway_value =$("#gateway1").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : base_url+'FreshBooks_controllers/home/get_gateway_data',
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   =  base_url+'FreshBooks_controllers/PaytracePayment/pay_multi_invoice';
							  }else if(gtype=='2'){
									var url   =  base_url+'FreshBooks_controllers/AuthPayment/pay_multi_invoice';
							 }else if(gtype=='1'){
								var url   =  base_url+'FreshBooks_controllers/Transactions/pay_multi_invoice';
							  }else if(gtype=='4'){
							   var url   =  base_url+'FreshBooks_controllers/PaypalPayment/pay_multi_invoice';
							 }else if(gtype=='5'){
							   var url   =  base_url+'FreshBooks_controllers/StripePayment/pay_multi_invoice';
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							      	 var form = $("#thest_pay1");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 } else if(gtype=='6'){
								var url   = base_url+'FreshBooks_controllers/UsaePay/pay_multi_invoice';
							} else if(gtype=='7'){
								var url   = base_url+'FreshBooks_controllers/GlobalPayment/pay_multi_invoice';
							} else if(gtype=='8'){
								var url   = base_url+'FreshBooks_controllers/CyberSource/pay_multi_invoice';
							} else if(gtype=='9'){
								var url   = base_url+'FreshBooks_controllers/Transactions/pay_multi_invoice';
							} else if(gtype=='10'){
								var url   = base_url+'FreshBooks_controllers/iTransactPayment/pay_multi_invoice';
							} else if(gtype=='11'){
								var url   = base_url+'FreshBooks_controllers/FluidpayPayment/pay_multi_invoice';
							} else if(gtype=='12'){
								var url   = base_url+'FreshBooks_controllers/TSYSPayment/pay_multi_invoice';
							} else if(gtype=='15'){
								var url   = base_url+'FreshBooks_controllers/PayarcPayment/pay_multi_invoice';
							} else if(gtype=='13'){
								var url   = base_url+'FreshBooks_controllers/BasysIQProPayment/pay_multi_invoice';
							} else if(gtype=='17'){
								var url   = base_url+'FreshBooks_controllers/MaverickPayment/pay_multi_invoice';
							} else if(gtype=='16'){
								var url   = base_url+'FreshBooks_controllers/EPXPayment/pay_multi_invoice';
							}			
				
				            // $("#thest_pay1").attr("action",url);
							var surchargePercentage = (data.isSurcharge == 1) ? data.surchargePercentage : 0;
							$('#invDefaultsurchargeRate').val(surchargePercentage);
							create_card_multi_data();
					}   
				   
			   });
			}			
			
			
	  }	
    

function set_fb_invoice_process_id(id, cid, in_val)
{

	    $("#btn_process").attr("disabled", true);
	     $('#invoiceProcessID').val(id);
	      $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbo_pay'
			}).appendTo('#thest_pay');
		   $('#thest_pay #inv_amount').val(in_val);
		   $('#invDefaultcardSurchargeValue').val(0);

		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : base_url+'FreshBooks_controllers/Transactions/check_vault',
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
										if(card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase()!='ECHECK')
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
									 var card_daata=' <div class="sch_crd"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" onChange="get_card_surcharge_details({ type : 1 ,cardNumber: this.value})" id="card_number11" name="card_number" class="form-control"  autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control"  autocomplete="off" /></div></div> <div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div></fieldset></div>'+
									 
									 '<div style="display:none;" class="sch_chk"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Number</label><div class="col-md-8"><input type="text" id="acc_number" name="acc_number" class="form-control" value="" ></div></div>'+
                    '<div class="form-group"><label class="col-md-4 control-label" for="card_number">Routing Number</label><div class="col-md-8"><input type="text" id="route_number" name="route_number" class="form-control" ></div></div>'+
					'<div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Name</label><div class="col-md-8"><input type="text" id="acc_name" name="acc_name" class="form-control" value="" "Account Name"></div> </div>'+
					
					'<div class="form-group"><label class="col-md-4 control-label" for="Entry Methods">Entry Method</label><div class="col-md-6"><div class="input-group"><select id="secCode" name="secCode" class="form-control valid" aria-invalid="false">'+
'<option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option>'+
 '</select>	</div></div></div>'+ 
                '<div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label><div class="col-md-6">'+
    '<select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false"><option value="business"  >Business</option><option value="personal"  >Personal</option> </select></div></div>'+
	'<div class="form-group"><label class="col-md-4 control-label" for="acct_type">Account Type</label><div class="col-md-6">'+
	'<select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false"><option value="checking" >Checking</option><option value="saving"  >Saving</option>'+
    '</select></div></div></fieldset></div>'+ 
	//  '<div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div>'+
						   
	 '<div class="panel panel-info notice_box surchargeNoticeClass" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>' +
	 
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="'+data['address1']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="'+data['address2']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="'+data['City']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="'+data['State']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="'+data['zipCode']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="'+data['Country']+'" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="'+data['phoneNumber']+'" ></div></div></fieldset>';
                   
                      $('.card_div').html(card_daata);   
					  var schValue = $("#ccChecked").val();
						if(schValue == 2){
							get_ach_details_pay({value: 2});
							return true;
						} else {
							create_card_data();
						}
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
}



function schedule_payment()
{
    
  var formdata=  $('#FRB_form1_schedule').serialize();
$(".close1" ).trigger("click");
$.ajax({  
		       
			type:'POST',
			url:  base_url+"FreshBooks_controllers/home/invoice_schedule",
		        
			data: formdata,
		
			success : function(response){
			 data= $.parseJSON(response);
			 
			  if(data['status']=='success'){
			        
			      
					  location.reload(true);
				  } 
				  
			}
		});



}
 


function set_fb_url(e=null)
{
 
          
			var gateway_value =$("#gateway").val();
			var isEcheck = false;
			if(!gateway_value){
		    	var gateway_value =e.value;
				isEcheck = true;
			}
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url :  base_url+'FreshBooks_controllers/home/get_gateway_data',
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   =  base_url+'FreshBooks_controllers/PaytracePayment/pay_invoice';
							  }else if(gtype=='2'){
									var url   = base_url+'FreshBooks_controllers/AuthPayment/pay_invoice';
							 }else if(gtype=='1'){
							   var url   = base_url+'FreshBooks_controllers/Transactions/pay_invoice';
							 }else if(gtype=='9'){
								var url   = base_url+'FreshBooks_controllers/Transactions/pay_invoice';
							 }else if(gtype=='4'){
							   var url   = base_url+'FreshBooks_controllers/PaypalPayment/pay_invoice';
							 }else if(gtype=='5'){
							   var url   = base_url+'FreshBooks_controllers/StripePayment/pay_invoice';
							     $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }else if(gtype=='6'){
								var url   = base_url+'FreshBooks_controllers/UsaePay/pay_invoice';
							} else if(gtype=='7'){
								var url   = base_url+'FreshBooks_controllers/GlobalPayment/pay_invoice';
							} else if(gtype=='8'){
								var url   = base_url+'FreshBooks_controllers/CyberSource/pay_invoice';
							} else if(gtype=='9'){
								var url   = base_url+'FreshBooks_controllers/Transactions/pay_invoice';
							} else if(gtype=='10'){
								var url   = base_url+'FreshBooks_controllers/iTransactPayment/pay_invoice';
							} else if(gtype=='11'){
								var url   = base_url+'FreshBooks_controllers/FluidpayPayment/pay_invoice';
							} else if(gtype=='12'){
								var url   = base_url+'FreshBooks_controllers/TSYSPayment/pay_invoice';
							} else if(gtype=='15'){
								var url   = base_url+'FreshBooks_controllers/PayarcPayment/pay_invoice';
							} else if(gtype=='13'){
								var url   = base_url+'FreshBooks_controllers/BasysIQProPayment/pay_invoice';
							} else if(gtype=='17'){
								var url   = base_url+'FreshBooks_controllers/MaverickPayment/pay_invoice';
							} else if(gtype=='16'){
								var url   = base_url+'FreshBooks_controllers/EPXPayment/pay_invoice';
							} 		
				
				            // $("#thest_pay").attr("action",url);
							var surchargePercentage = (data.isSurcharge == 1) ? data.surchargePercentage : 0;
                			$('#invDefaultsurchargeRate').val(surchargePercentage);
							if(isEcheck){
								create_sch_card();
							} else {
								create_card_data();
							}
						}   
				   
			   });
			}			
			
			
	  }
	
	
	function set_template_data_temp(invoiceID, custID,typeID ){
		
	   
          document.getElementById('invoicetempID').value=invoiceID;
		 document.getElementById('customertempID').value=custID;
		  
		  	if(typeID!=""){
		 
			$.ajax({
			type:"POST",
			url : base_url+'FreshBooks_controllers/Settingmail/set_template',
			data : {typeID:typeID,invoiceID:invoiceID,customerID:custID },
			success: function(data){
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
				if(data['replyTo'] != '') {
					$('#replyTo').val(data['replyTo']);
				}
				$('#emailSubject').val(data['emailSubject']);
				$('#toEmail').val(data['toEmail']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
				$('#fromEmail').val(data['fromEmail']);
				$('#mailDisplayName').val(data['mailDisplayName']);
				$('#invoiceCode').val(data['invCode']);
				if(data['templateName'] != '') {
					$('#type_text').val(data['templateName']);
				}
		  
		  }
	   });	 
	   
	 } 
	
    }	
	
	
	
	 $(function(){  
		
				 	CKEDITOR.replace( 'textarea-ckeditor', {
				    toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'insert' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
				});
			
					$('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
				});
					$('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
				});
					$('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
				}); 
				
				$('#open_from_email').click(function(){
					$('#from_email_div').show();
					 $(this).hide();
				});
				$('#open_display_name').click(function(){
							$('#display_name_div').show();
							$(this).hide();
				});
				
				
				
				
				
		  $('#type').change(function(){
		     
			var templateID = $(this).val();
         
		
		var customerID =  $('#customertempID').val();
		     
		  
			if(templateID!=""){
		
			$.ajax({
			type:"POST",
			url : base_url+'FreshBooks_controllers/Settingmail/set_template_cus_details',
			data : {templateID:templateID,customerID:customerID },
			success: function(data){
		
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			     $('#textarea-ckeditor').html(data['message']);
				$('#emailSubject').val(data['emailSubject']);
				$('#toEmail').val(data['toEmail']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
			
				
		  
		  }
	   });	 
	   
	 } 





 }); 



        	$('#CardID').change(function(){
		var cardlID =  $(this).val();
		$('#invDefaultcardSurchargeValue').val(0);
	
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : base_url+'FreshBooks_controllers/Transactions/get_card_data',
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
							$('#invDefaultcardSurchargeValue').val(data['card']['isSurcharge']);
					        
					       
					        if(gtype=='5')
					        {
					            
						 var form = $("#thest_pay");	
                        
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
                        
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
                             if(pub_key){
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);
                        }
									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
						var surchargePercentage = $( "#invDefaultsurchargeRate" ).val();
						showInvoiceSurcharge({surchargePercentage, CardID: data['card']['CardID']});
				
				}	
			});
		  }
	}); 
	 $('#del_ccjkck_qbo').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    check_number: {
                        required: true,
					    minlength: 3,
					   	maxlength: 20,
                    },
					
					 payment_date: {
							  required: true,
						},
				      
				       inv_amount: {
							  required: true,
							  number:true,
						},	
                   	
                  
                  
                },
              
            });
	init();			
			
	 });  
	
	


	
	function select_plan_val(rid){
		
		
		
		var itemID = $('#productID'+rid).val();
		$('#productID' + rid).valid();
		$.ajax({ 
		     type:"POST",
			 url:base_url+"FreshBooks_controllers/Freshbooks_invoice/get_item_data",
			data: {'itemID':itemID },
			success:function(data){
            	var item_data = $.parseJSON(data); 
			 $('#description'+rid).val(item_data['SalesDescription']);
             $('#rate'+rid).html(roundN(item_data['saleCost'],2));
			 $('#unit_rate'+rid).val(roundN(item_data['saleCost'],2));
			
			$('#quantity'+rid).val(1);	
        	 $('#total11'+rid).html(roundN(($('#quantity'+rid).val()*$('#unit_rate'+rid).val()),2));
             $('#total'+rid).val(roundN(($('#quantity'+rid).val()*$('#unit_rate'+rid).val()),2));
            
            var grand_total=0;
	    	$(".total_val").each(function(){
            var tval = $(this).val();
			if(tval=="") tval =0;
			
            grand_total=parseFloat(grand_total)+parseFloat(tval);  
		
		   
            });
            calculateAmountInv();
			}
		});
	}
	
	function set_unit_val(rid){
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val();
		var tax    = 0;
		
		if ($('input#tax_check'+rid).is(':checked')) {
			tax = $('#tax_check'+rid).val();
		}
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*rate + total_tax; 
		$('#total'+rid).val(total.toFixed(2));
		
		var grand_total=0;
		$( ".total_val" ).each(function(){
			
              var tval = $(this).val() != '' ? $(this).val() : 0;
               grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
			
		$('#grand_total').html(format22(grand_total));

		calculateAmountInv();

	}
	
	

   function set_plan_change()
     {
        
       var plan_id = $('#sub_plan').val();
     
        $('#item_fields').empty();
		room++;
		var plan_data = jsplandata; 
		var plan_html ='<option value="0">Select Product or Service</option>';
			 for(var val in  plan_data) 
                            {   
                               
			    				plan_html+='<option value="'+plan_data[val]['productID']+'"   >'+plan_data[val]['Name']+'</option>'; 
                                   
                                   
							}
		var onetime_html ='<option value="0">Recurring</option><option value="1">One Time Charge</option>';
			var show_tax = '';
		
		if($('#taxes').val() == ''){
			var show_tax = 'style="display:none;"';
			var tax_val = 0;
		}else {
			var tax_val = $('#taxes').val();
		}
		 var i = 1;
       	$.ajax({
					type:"POST",
					url : base_url+"FreshBooks_controllers/SettingSubscription/get_subplan",
					data : {'planID':plan_id},
					success : function(response){
					
							 data=$.parseJSON(response);
							 $('#freetrial').val(data[0].freeTrial);
						
							 $('#paycycle').val(data[0].invoiceFrequency);
							 $('#duration_list').val(data[0].subscriptionPlan);
							 $('#grand_total').html((data[0].subscriptionAmount));
							
							if(data[0].emailRecurring == 1){
							    $("input[name=email_recurring][value='1']").prop("checked",true);
							}else{
							      $("input[name=email_recurring][value='0']").prop("checked",true);
							}
							
							if(data[0].automaticPayment == 1){
							    $("input[name=autopay][value='1']").prop("checked",true);
							    $('#set_pay_data').show();
							}else{
							    $("input[name=autopay][value='0']").prop("checked",true);
							     $('#set_pay_data').hide();
							}
							
							
							for(var t = 0; t < data.length; t++){
                           
                            
                            
                            
							   $('#item_fields').append( '<div class="form-group removeclass'+i+'"><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val('+i+');"  id="productID'+i+'" name="productID[]">'+plan_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge'+i+'" name="onetime_charge[]">'+onetime_html+'</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description'+i+'" name="description[]" value="" ></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate'+i+'" name="unit_rate[]" value="" onblur="set_unit_val('+i+');" ></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val('+i+');" id="quantity'+i+'" name="quantity[]" value="" ></div></div>   <div class="col-sm-1 nopadding"><div class="set_taxes" ><div class="form-group"> <input type="checkbox" id="tax_check'+i+'" onchange="set_tax_val(this, '+i+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></div></div></div> 	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total'+i+'" name="total[]" value="" > </div></div> </div> <div class="clear"></div></div> ');
							    $('#productID'+i).val(data[t].itemListID);
							   $('#onetime_charge'+i).val(data[t].oneTimeCharge);
							   $('#description'+i).val(data[t].itemDescription);
							   $('#unit_rate'+i).val(data[t].itemRate);
							   $('#quantity'+i).val(data[t].itemQuantity);
							   $('#total'+i).val((data[t].itemRate * data[t].itemQuantity).toFixed(2));
                        
                            
							   i++;
							}
						
						setTimeout(function(){
						    	$("input[name='quantity']").trigger('click');
						}, 1000);
					
					
					    
					}
					
					
				});
        
    }
    
    
	function set_qty_val(rid){
      
		var qty    = $('#quantity'+rid).val();
		var rate   = $('#unit_rate'+rid).val(); 
		var tax    = 0;
		if(qty > 0)
		{	
		
		
		var total_tax  = (qty*rate)*tax / 100; 
			var total  = qty*parseFloat(rate) + parseFloat(total_tax);   
        
		$('#total11'+rid).html(total.toFixed(2));
			   $('#total'+rid).val(total.toFixed(2));
		 var grand_total=0;
	    	$( ".total_val" ).each(function(){
				
				
            var tval = $(this).val();
		
			if(tval=="") tval =0;
            grand_total=parseFloat(grand_total)+parseFloat(tval);
			});
				
		
			
        
        // calculate_tax_total(grand_total);
		calculateAmountInv();
		
		}
		
	}
    
    
    function format22(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

}
   	var room =0; var grand_total1=0;
     var inID = $('#invNo').val();

      
 async function init() { 
       
    await  $.ajax({
               
               'type': "POST",
              
               'data':{'invID':inID},
	            'url':base_url+"FreshBooks_controllers/home/get_invoice_item_count_data",  
	            'success':function(data){
	        
                  jsdata=  $.parseJSON(data); 
               
                return jsdata;   
	            }
             });
     
	if(jQuery.isEmptyObject(jsdata['items']))
   {
	    
		 room=1;
		
	}else{
	
		
	        room =jsdata['rows'] ;
	

	}


await $.ajax({
           
            'type': "POST",
          
            'url'  :base_url+"FreshBooks_controllers/home/get_product_data",  
            'success':function(data) {
            
             jsplandata = $.parseJSON(data); 
            }
         });
	 
	 
    } 

	

  function item_invoice_fields()
	{
		
   
    room++;
	
  
		
		var plan_data =jsplandata; 
	
		var plan_html ='<option value="">Select Product or Service</option>';
		for(var val in  plan_data) {       plan_html+='<option value="'+ plan_data[val]['productID']+'">'+plan_data[val]['Name']+'</option>'; }
		
		
		var objTo = document.getElementById('item_fields')
		var divtest = document.createElement("tr");
		divtest.setAttribute("class", " rd removeclass"+room);
	 
		
		divtest.innerHTML = '<td><select class="form-control calculateAmount" room-id="'+room+'" onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></td><td> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" ></td><td class="text-right">$<span id="rate'+room+'"> 0.00 </span><input type="hidden" id="unit_rate'+room+'"  name="unit_rate[]" value="0" /> </td><td class="text-right"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-left" maxlength="4" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" ></td> <td class="text-right" id="taxButton'+room+'"> <a href="#fb_invoice_tax_modal" class="btn btn-sm btn-info" onclick="invoice_tax_list('+room+')" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-original-title="" title=""><strong> + </strong></a></td> <input type="hidden" class="hidden" id="totalTaxItems'+room+'" name="totalTaxItems[]" value=""> <input type="hidden" class="hidden" id="totalTaxRate'+room+'" name="totalTaxRate[]" value=""> <td> <div class="input-group"> <input type="hidden" class="form-control total_val" id="total'+room+'" name="total[]" value=""/>$<span id="total11'+room+'">0.00</span><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></td> ';
  
    $(divtest).insertAfter($('table tr.rd:last'));
  }
    
 function remove_education_fields(rid)
   {
	  var gr_val=0;
	 
      $('.removeclass'+rid).remove();
     	$( ".total_val" ).each(function(){
			var test = $(this).val();
            gr_val+= parseFloat(test);
			});
			calculateAmountInv();
	
	   
  }


  
function calculate_tax_total(grand_total){
  		var total_tax  =0;
    	var rate=0;
    	 var subtotal = grand_total;
          rate = $('#txID').val();
    
    
    /*******In case of Flat Tax******/
    	 total_tax =subtotal*rate/100;
      	 grand_total =grand_total+total_tax;
       
		$('#grand_total').html(format22(grand_total));
	     
       		$('#tax_val').html(format22(total_tax));
			$('#sub_total').html(format22(subtotal));
			$('#total_amt').html(format22(grand_total));
			
	    	$('#grand_total').html(format22(grand_total));

}

function set_tax_val(rid) {

	var qty = $('#quantity' + rid).val();
	var rate = $('#unit_rate' + rid).val();
	var tax = $('#totalTaxRate' + rid).val();
	
	if (tax > 0) {
		var total_tax = (qty * rate) * tax / 100;
		var total = qty * rate + total_tax;
	}
	else {
		var total = qty * rate;
	}

	var grand_total = 0;
	$(".total_val").each(function () {
		var tval = $(this).val() != '' ? $(this).val() : 0;
		grand_total = parseFloat(grand_total) + parseFloat(tval);
	});

	$('#sub_total').html(format22(grand_total));
	$('#total_amt').html(format22(grand_total));
	$('#grand_total').html(format22(grand_total));
}
	    
      
    
	  
	function isNumberKey(evt)
	{
	 var charCode = (evt.which) ? evt.which : event.keyCode;
	 if (charCode == 46 ){
	    return true;
	  }
	 if (charCode > 31 && (charCode < 48 || charCode > 57))
	    return false;

	 return true;
	}
	/* Initialize Form Validation */
      $('#form-validation1').validate({
                ignore: [] ,
                rules: {
                 replyEmail: {
                     required: true,
                     email: true
                 },
            
               emailSubject: {
                    required: true,
                    minlength: 2,
               }
             },
           
         });

function calculateAmountInv(){
	var total = 0;
	var totalTax = 0;

	$(`.calculateAmount`).each(function () {
		var roomID = $(this).attr('room-id');

		var qty = $('#quantity' + roomID).val();
		var rate = $('#unit_rate' + roomID).val();

		var tax = 0;
		tax = $('#totalTaxRate' + roomID).val();

		var item_tax = (qty * rate) * tax / 100;
		var itemTotal = qty * rate;

		total += itemTotal;
		totalTax += item_tax;
	});

	$('#tax_val').html(format22(totalTax));
	$('#sub_total').html(format22(total));
	$('#total_amt').html(format22(total+totalTax));
	$('#grand_total').html(format22(total+totalTax));
}