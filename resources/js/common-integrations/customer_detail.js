
var base_url = $('#js_base_url').val();
var gtype = '';


function set_refund_invoices(invID) {

    if (invID !== "") {
        $('#ref_invID').val(invID);
        $.ajax({
            type: "POST",
            url: base_url + "ajaxRequest/get_invoice_transactions",
            data: { invID: invID },
            success: function (response) {
                data = $.parseJSON(response);

                if (data.status == 'success') {

                    $('#ref_id').html(data.transactions);
                    $('#rf_btn').removeAttr('disabled');

                } else {

                    $('#ref_id').html('<span>N/A</span>');

                    $('#rf_btn').attr('disabled', 'disabled');
                }

            }

        });

    }

}

function set_invoice_schedule_id(id, cid, in_val, ind_date) {

    $('#scheduleID').val(id);
    $('#schedule_date1').val('');

    var nowDate = new Date(ind_date);


    var inv_day = new Date(nowDate.getMonth(), nowDate.getDate() + 1, nowDate.getFullYear());

    $("#schedule_date1").datepicker({
        format: 'mm/dd/yyyy',
        startDate: inv_day,
        endDate: 0,
        autoclose: true
    });

    $('#schAmount').val(in_val);

    if (cid !== '') {
        $.ajax({
            type: "POST",
            url: base_url + 'Integration/home/check_vault',
            data: { 'customerID': cid },
            success: function (response) {

                data = $.parseJSON(response);
                //   console.log (data);
                if (data['status'] == 'success') {
                    var s = $('#schCardID');
                    $('#schCardID').empty();

                    $(s).append('<option value="new1">New Card</option>');
                    var card1 = data['card'];
                    var recentCard = data.recent_card;
                    for (var val in card1) {
                        var selectedCard = false;
                        if (card1[val]['CardID'] == recentCard) {
                            selectedCard = true;
                        }
                        if (card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase() != 'ECHECK')
                            $("<option />", { value: card1[val]['CardID'], selected: selectedCard, text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
                    }

                    var card_daata = '<div class="sch_crd"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">' + opt + '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11" name="cvv" class="form-control"  /></div></div></fieldset></div>' +

                        '<div style="display:none;" class="sch_chk"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Number</label><div class="col-md-8"><input type="text" id="acc_number" name="acc_number" class="form-control" value="" ></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="card_number">Routing Number</label><div class="col-md-8"><input type="text" id="route_number" name="route_number" class="form-control" ></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Name</label><div class="col-md-8"><input type="text" id="acc_name" name="acc_name" class="form-control" value="" ></div> </div>' +

                        '<div class="form-group"><label class="col-md-4 control-label" for="Entry Methods">Entry Method</label><div class="col-md-6"><div class="input-group"><select id="secCode" name="secCode" class="form-control valid" aria-invalid="false">' +
                        '<option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option>' +
                        '</select>	</div></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label><div class="col-md-6">' +
                        '<select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false"><option value="business"  >Business</option><option value="personal"  >Personal</option> </select></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="acct_type">Account Type</label><div class="col-md-6">' +
                        '<select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false"><option value="checking" >Checking</option><option value="saving"  >Saving</option>' +
                        '</select></div></div></fieldset></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div>' +
                        '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="' + data['BillingAddress_Addr1'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="' + data['BillingAddress_Addr2'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="' + data['BillingAddress_City'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="' + data['BillingAddress_State'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="' + data['BillingAddress_PostalCode'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="' + data['BillingAddress_Country'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="' + data['Phone'] + '" ></div></div></fieldset>';

                    $('.card_div_csh').html(card_daata);
                    var schValue = $("#ccChecked").val();
                    if (schValue == 2) {
                        get_ach_details({ value: 2 });
                        return true;
                    } else {
                        get_ach_details({ value: 1 });
                        return true;
                    }




                }

            }


        });

    }





}

function set_view_history(eID) {
    if (eID !== '') {


        $.ajax({
            url: base_url + 'Settingmail/get_history_id',
            type: 'POST',
            data: { customertempID: eID },
            //dataType: 'json',
            success: function (data) {

                $('#data_history').html(data);



            }
        });

    }

}


var Pagination_email = function () {

    return {
        init: function () {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {

            });

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            $.fn.DataTable.ext.pager.numbers_length = 3;

            $.fn.DataTable.ext.pager.numbers_no_ellipses = function (page, pages) {
                var numbers = [];
                var buttons = $.fn.DataTable.ext.pager.numbers_length;
                var half = Math.floor(buttons / 2);

                var _range = function (len, start) {
                    var end;

                    if (typeof start === "undefined") {
                        start = 0;
                        end = len;

                    } else {
                        end = start;
                        start = len;
                    }

                    var out = [];
                    for (var i = start; i < end; i++) { out.push(i); }

                    return out;
                };


                if (pages <= buttons) {
                    numbers = _range(0, pages);

                } else if (page <= half) {
                    numbers = _range(0, buttons);

                } else if (page >= pages - 1 - half) {
                    numbers = _range(pages - buttons, pages);

                } else {
                    numbers = _range(page - half, page + half + 1);
                }

                numbers.DT_el = 'span';

                return [numbers];
            };

            / Initialize Datatables /
            $('#email_hist').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [1] }
                ],
                order: [[0, "desc"]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]],
                pagingType: 'numbers_no_ellipses'
            });

            / Add placeholder attribute to the search input /
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

var customerListIDJS = $("#customerID").val();
if (typeof customerListIDJS === undefined) {
    customerListIDJS = "";
}
if (typeof InvoiceCount === undefined) {
    var InvoiceCount = 0;
}

$(function () {
    if (customerListIDJS) {
        Pagination_view_invoice.init();
        $(document).on('change', '.status_filter', function () {
            status_filter = $(this).val();
            $('#invoice_page').DataTable().draw();
        });
    }
});


var status_filter = 'Open';
var Pagination_view_invoice = function () {


    var serachString = '';

    return {
        init: function () {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {

            });

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                "processing": true,
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "lengthMenu": [[10, 50, 100, 500], [10, 50, 100, 500]],
                "pageLength": 10,
                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": base_url + "Integration/Invoices/invoice_list_ajax",
                    "type": "POST",

                    "data": function (data) {

                        data.status_filter = status_filter;
                        data.customerID = customerListIDJS;
                    },

                },
                "order": [[1, 'desc']],

                "sPaginationType": "bootstrap",
                "stateSave": true

            });


            / Add placeholder attribute to the search input /
            $('.dataTables_filter input').attr('placeholder', 'Search');

            if (customerListIDJS) {
                batchProcessFunction = `customer_invoice_batch_process('${customerListIDJS}');`;
                $('#invoice_page_filter').prepend('<a class="btn btn-sm  btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal" title="Create New" href="' + base_url + 'Integration/Invoices/create_invoice/' + customerListIDJS + '">Add Invoice</a>');
                $('#invoice_page_filter label').append('<select class="form-control status_filter" name="status_filter"><option value="Open">Open</option><option value="Past Due">Overdue</option><option value="Partial">Partial</option><option value="Unpaid">Unpaid</option><option value="Failed">Failed</option><option value="Paid">Paid</option><option value="Cancelled">Voided</option><option value="All">All</option></select>');
                $('#invoice_page_filter').parent('div').addClass('col-sm-11').removeClass('col-sm-6');
                $('#invoice_page_length').parent('div').addClass('col-sm-1').removeClass('col-sm-6');

                if (InvoiceCount > 0) {
                    $('#invoice_page_filter').prepend('<a href="#qbd_invoice_multi_process" class="btn btn-sm btn-success batch_button_color" data-backdrop="static" data-keyboard="false" data-toggle="modal" style="margin-right: 10px;" onclick="' + batchProcessFunction + '">Batch Process</a>');
                }
            }


            $("#search").on('keyup', function () {
                $('#invoice_page').DataTable().search(this.value).draw();
            });

        }
    };
}();

var Pagination_view1 = function () {

    return {
        init: function () {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {

            });

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#outstand').dataTable({
                columnDefs: [
                    { type: 'date', targets: [0, 1] },
                    { orderable: false, targets: [5] }
                ],

                order: [[1, "desc"]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();




var Pagination_view2 = function () {
    return {
        init: function () {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {

            });

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date', targets: [1] },
                    { orderable: false, targets: [5] }
                ],

                order: [[1, "desc"]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });


            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();
var Pagination_view3 = function () {
    return {
        init: function () {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {

            });

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();


            $('#subs').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0, 1] },
                    { orderable: false, targets: [4] }
                ],

                order: [[2, "desc"]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();




$(function () {




    $('#btn_mask').click(function () {
        $('#m_card_id').hide();
        $('#edit_card_number').removeAttr('disabled');
        $('#edit_card_number').attr('type', 'text');
    });



    $('.radio_pay').click(function () {
        var method = $(this).val();
        if (method == 1) {
            $('#checkingform').hide();
            $('#ccform').show();
        } else if (method == 2) {
            $('#checkingform').show();
            $('#ccform').hide();
        } else {

        }
    });


    nmiValidation.init();
    nmiValidation1.init();
    addvalidation.init();
    // schvalidation.init();
    Pagination_view1.init();
    Pagination_view2.init();
    Pagination_email.init();
    Pagination_view3.init();

    $('#open_cc').click(function () {
        $('#cc_div').show();
        $(this).hide();
    });
    $('#open_bcc').click(function () {
        $('#bcc_div').show();
        $(this).hide();
    });
    $('#open_reply').click(function () {
        $('#reply_div').show();
        $(this).hide();
    });

    /*********************Template Methods**************/

    $('#Qbd_form1_schedule').validate({
        errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            // You can use the following if you would like to highlight with green color the input after successful validation!
            e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
            e.closest('.help-block').remove();
        },
        rules: {
            schedule_date: {
                required: true,
            },
            sch_method: {
                required: true,
            },
            sch_gateway: {
                required: true,
            },
            schCardID: {
                required: true,
            },
            schAmount: {
                required: true,
                number: true,
                min: 0.01
            },
            card_number: {
                required: true,
                minlength: 13,
                maxlength: 16,
                number: true
            },
            cvv: {
                //required: true,
                number: true,
                minlength: 3,
                maxlength: 4,
            },
            acc_number: {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 20,

            },
            route_number: {
                required: true,
                digits: true,
                minlength: 3,
                maxlength: 12,
            },
            acc_name: {
                required: true,
                minlength: 3,
                maxlength: 30,
            },
        },
        messages: {
            schAmount: {
                required: "Please enter amount",
                number: "Please enter valid amount",
                min: "Please enter valid amount",
            },
            acc_number: {
                minlength: "Please enter at least 3 digits.",
                maxlength: "Please enter no more than 20 digits.",
                digits: 'Please enter valid account number.'
            },
            route_number: {
                minlength: 'Please enter at least 3 digits.',
                maxlength: 'Please enter no more than 12 digits.',
                digits: 'Please enter valid routing number.'
            },
        }

    });



    $.validator.addMethod('CCExp', function (value, element, params) {
        var minMonth = new Date().getMonth() + 1;
        var minYear = new Date().getFullYear();
        var month = parseInt($(params.month).val(), 10);
        var year = parseInt($(params.year).val(), 10);
        return (year > minYear || (year === minYear && month >= minMonth));
    }, 'Your Credit Card Expiration date is invalid.');







});



var nmiValidation = function () {

    return {
        init: function () {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true,
                        remote: {

                            beforeSend: function () {
                                $("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

                            },
                            complete: function () {
                                $(".overlay1").remove();
                            },
                            url: base_url + "ajaxRequest/check_account_exists",
                            type: "POST",
                            data: {
                                card_number: function () { return $("#card_number").val(); },
                                acc_number: '',
                                customerID: function () { return $("#customerID").val(); },
                                account_type: function () { return $("input[name='formselector']:checked").val(); },
                            },
                            dataType: 'json',
                            dataFilter: function (response) {

                                var rsdata = jQuery.parseJSON(response);

                                if (rsdata.status == 'success')
                                    return true;
                                else {
                                    return false;
                                }
                            }
                        },
                    },

                    expiry_year: {
                        CCExp: {
                            month: '#expiry',
                            year: '#expiry_year'
                        }
                    },


                    cvv: {
                        //required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                        remote: {

                            beforeSend: function () {
                                $("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

                            },
                            complete: function () {
                                $(".overlay1").remove();
                            },
                            url: base_url + "ajaxRequest/check_account_exists",
                            type: "POST",
                            data: {
                                acc_number: function () { return $("#acc_number").val(); },
                                card_number: '',
                                customerID: function () { return $("#customerID").val(); },
                                account_type: function () { return $("input[name='formselector']:checked").val(); },
                            },
                            dataType: 'json',
                            dataFilter: function (response) {

                                var rsdata = jQuery.parseJSON(response);
                                if (rsdata.status == 'success')
                                    return true;
                                else {
                                    return false;
                                }
                            }
                        },
                    },
                    route_number: {
                        required: true,
                        digits: true,
                        minlength: 9,
                        maxlength: 9,
                    },
                    acc_name: {
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                    },
                    friendlyname: {
                        required: true,
                        minlength: 3,
                    },
                    address1: {
                        //required:true,
                    },

                    country: {
                        //required:true,
                    },
                    state: {
                        //required:true,
                    },
                    city: {
                        //required:true,
                        minlength: 2,
                    },
                    zipcode: {
                        //required:true,
                        minlength: 3,
                        maxlength: 10,
                        //ZIPCode:true,
                    },
                    contact: {
                        //required: true,
                        minlength: 10,
                        maxlength: 17,
                        phoneUS: true,
                    },




                },
                messages: {
                    card_number: {
                        remote: 'Card details already exists.'
                    },
                    acc_number: {
                        remote: 'Account details already exists.',
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 9 digits.',
                        maxlength: 'Please enter no more than 9 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                }

            });
            $.validator.addMethod("phoneUS", function (value, element) {

                if (value == '')
                    return true;
                return value.match(/^\d{10}$/) ||
                    value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);

            }, "Please specify a valid phone number");

            $.validator.addMethod("ZIPCode", function (value, element) {
                return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
            }, "Only alphanumeric and hyphen is allowed");

            $.validator.addMethod('CCExp', function (value, element, params) {
                var minMonth = new Date().getMonth() + 1;
                var minYear = new Date().getFullYear();
                var month = parseInt($(params.month).val(), 10);
                var year = parseInt($(params.year).val(), 10);



                return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
            }, 'Your Credit Card Expiration date is invalid.');




        }
    };
}();





var nmiValidation1 = function () {

    return {
        init: function () {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    edit_card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true,
                        remote: {
                            beforeSend: function () {
                                $("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

                            },
                            complete: function () {
                                $(".overlay1").remove();
                            },
                            url: base_url + "ajaxRequest/check_account_exists",
                            type: "POST",
                            data: {
                                card_number: function () { return $("#edit_card_number").val(); },
                                acc_number: '',
                                customerID: function () { return $("#customerID").val(); },
                                edit_cardID: function () { return $("#edit_cardID").val(); },
                                account_type: 1,
                            },
                            dataType: 'json',
                            dataFilter: function (response) {

                                var rsdata = jQuery.parseJSON(response);

                                if (rsdata.status == 'success')
                                    return true;
                                else {
                                    return false;
                                }
                            }
                        },
                    },

                    edit_expiry_year: {
                        CCExp: {
                            month: '#edit_expiry',
                            required: true,
                            year: '#edit_expiry_year'
                        }
                    },


                    edit_cvv: {
                        // required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },

                    baddress1: {
                        //required:true,
                    },

                    edit_friendlyname: {
                        //	required:true,
                        minlength: 3,
                    },

                    bcountry: {
                        //	required:true,
                    },
                    bstate: {
                        //	required:true,
                    },
                    bcity: {
                        //	required:true,
                        minlength: 2,
                    },
                    bzipcode: {
                        //required:true,
                        minlength: 3,
                        maxlength: 10,
                        //	ZIPCode:true,
                    },
                    bcontact: {
                        //	required: true,
                        minlength: 10,
                        maxlength: 17,
                        phoneUS: true,
                    },
                    edit_acc_number: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,
                        remote: {

                            beforeSend: function () {
                                $("<div class='overlay1'> <img src='" + base_url + "uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");

                            },
                            complete: function () {
                                $(".overlay1").remove();
                            },
                            url: base_url + "ajaxRequest/check_account_exists",
                            type: "POST",
                            data: {
                                acc_number: function () { return $("#edit_acc_number").val(); },
                                card_number: '',
                                customerID: function () { return $("#customerID").val(); },
                                edit_cardID: function () { return $("#edit_cardID").val(); },
                                account_type: 2,
                            },
                            dataType: 'json',
                            dataFilter: function (response) {

                                var rsdata = jQuery.parseJSON(response);
                                if (rsdata.status == 'success')
                                    return true;
                                else {
                                    return false;
                                }
                            }
                        },
                    },
                    edit_route_number: {
                        required: true,
                        digits: true,
                        minlength: 9,
                        maxlength: 9,
                    },
                    edit_acc_name: {
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                    },
                },
                messages: {
                    edit_card_number: {
                        remote: 'Card details already exists.'
                    },
                    edit_acc_number: {
                        remote: 'Account details already exists.',
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                    },
                    edit_route_number: {
                        minlength: 'Please enter at least 9 digits.',
                        maxlength: 'Please enter no more than 9 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                }

            });

            $.validator.addMethod("phoneUS", function (value, element) {

                if (value == '')
                    return true;
                return value.match(/^\d{10}$/) ||
                    value.match(/^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/);

            }, "Please specify a valid phone number");

            $.validator.addMethod("ZIPCode", function (value, element) {
                return this.optional(element) || /^[a-z0-9A-Z\\-]+$/i.test(value);
            }, "Only alphanumeric and hyphen is allowed");

            $.validator.addMethod('CCExp', function (value, element, params) {
                var minMonth = new Date().getMonth() + 1;
                var minYear = new Date().getFullYear();
                var month = parseInt($(params.month).val(), 10);
                var year = parseInt($(params.year).val(), 10);



                return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
            }, 'Your Credit Card Expiration date is invalid.');



            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();




var addvalidation = function () {

    return {
        init: function () {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#del_ccjkck_o').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    payment_date: {
                        required: true,

                    },

                    check_number: {
                        required: true,
                        minlength: 3,
                        maxlength: 20,
                    },


                    inv_amount: {
                        required: true,
                        number: true,
                    },


                },

            });



        }
    };
}();


var schvalidation = function () {

    return {
        init: function () {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#Qbd_form1_schedule').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function (error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function (e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function (e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); // e.closest('.form-group').removeClass('has-success has-error').addClass('has-success');
                    e.closest('.help-block').remove();
                },
                rules: {
                    schedule_date: {
                        required: true,

                    },

                    sch_method: {
                        required: true,
                    },
                    sch_gateway: {
                        required: true,
                    },

                    schCardID: {
                        required: true,

                    },
                    schAmount: {
                        required: true,
                        number: true
                    },
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
                    // expiry_year: {
                    // 	CCExp2: {
                    // 		month: '#expiry11',
                    // 		year: '#expiry_year11'
                    // 	}
                    // },

                    cvv: {
                        //required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    acc_number: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 20,

                    },
                    route_number: {
                        required: true,
                        digits: true,
                        minlength: 3,
                        maxlength: 12,
                    },
                    acc_name: {
                        required: true,
                        minlength: 3,
                        maxlength: 30,
                    },

                },
                messages: {
                    acc_number: {
                        minlength: "Please enter at least 3 digits.",
                        maxlength: "Please enter no more than 20 digits.",
                        digits: 'Please enter valid account number.'
                    },
                    route_number: {
                        minlength: 'Please enter at least 3 digits.',
                        maxlength: 'Please enter no more than 12 digits.',
                        digits: 'Please enter valid routing number.'
                    },
                }



            });



        }
    };
}();




function delele_notes(note_id) {

    if (note_id != "") {
        $.ajax({
            type: 'POST',
            url: base_url + 'Integration/home/delele_note',
            data: { 'noteID': note_id },
            success: function (response) {
                data = $.parseJSON(response);
                if (data['status'] == 'success') {
                    location.reload(true);
                }
            }

        });

    }

}

function add_notes() {
    var formdata = $('#pri_form').serialize();
    if ($('#private_note').val() != "") {
        $.ajax({
            type: 'POST',
            url: base_url + 'Integration/home/add_note',
            data: formdata,
            success: function (response) {
                data = $.parseJSON(response);
                if (data['status'] == 'success') {
                    location.reload(true);
                }
            }

        });

    }

}

function set_card_user_data(id, name) {

    $('#customerID11').val(id);
    $('#customername').val(name);
}

function set_edit_card(cardID) {

    $('#thest_form').show();
    $('#can_div').hide();
    $('#editccform').css('display', 'none');
    $('#editcheckingform').css('display', 'none');
    if (cardID != "") {

        $.ajax({
            type: "POST",
            url: base_url + 'Integration/Payments/get_card_edit_data',
            data: { 'cardID': cardID },
            success: function (response) {

                data = $.parseJSON(response);

                if (data['status'] == 'success') {

                    $('#edit_cardID').val(data['card']['CardID']);
                    if (data['card']['CardNo'] != "") {
                        $('#editccform').css('display', 'block');
                        $('#edit_card_number').val(data['card']['CardNo']);
                        //$('#edit_cvv').val(data['CardCVV']);
                        document.getElementById("edit_cvv").value = data['card']['CardCVV'];
                        //	document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
                        $('#m_card_id').show();
                        $('#edit_card_number').attr('disabled', 'disabled');
                        $('#edit_card_number').attr('type', 'hidden');
                        $('#m_card').html(data['card']['CardNo']);
                        //	$('#edit_expiry').val(data['cardMonth']);
                        $('select[name="edit_expiry"]').find('option[value="' + data['card']['cardMonth'] + '"]').attr("selected", true);
                        $('select[name="edit_expiry_year"]').find('option[value="' + data['card']['cardYear'] + '"]').attr("selected", true);
                    }
                    else {
                        $('#editcheckingform').css('display', 'block');
                        $('#edit_acc_number').val(data['card']['accountNumber']);
                        //$('#edit_cvv').val(data['CardCVV']);
                        document.getElementById("edit_acc_name").value = data['card']['accountName'];
                        //$('#edit_friendlyname').val(data['customerCardfriendlyName']);
                        document.getElementById("edit_route_number").value = data['card']['routeNumber'];

                        $('select[name="edit_secCode"]').find('option[value="' + data['card']['secCodeEntryMethod'] + '"]').attr("selected", true);
                        $('select[name="edit_acct_type"]').find('option[value="' + data['card']['accountType'] + '"]').attr("selected", true);
                        $('select[name="edit_acct_holder_type"]').find('option[value="' + data['card']['accountHolderType'] + '"]').attr("selected", true);

                    }
                    $('#baddress1').val(data['card']['Billing_Addr1']);
                    $('#baddress2').val(data['card']['Billing_Addr2']);
                    $('#bcity').val(data['card']['Billing_City']);
                    $('#bstate').val(data['card']['Billing_State']);
                    $('#bcountry').val(data['card']['Billing_Country']);
                    $('#bcontact').val(data['card']['Billing_Contact']);
                    $('#bzipcode').val(data['card']['Billing_Zipcode']);

                    if (data['card']['is_default'] == 1) {
                        $('#defaultMethod').prop('checked', true);
                        $('#defaultMethod').prop('disabled', true);
                        $('#defaultMethod').val(1);
                    } else {
                        $('#defaultMethod').prop('checked', false);
                        $('#defaultMethod').prop('disabled', false);
                        $('#defaultMethod').val(0);
                    }
                }

            }


        });

    }

}




function set_common_url_multi_pay() {
    var gateway_value = $("#gateway1").val();

    if (gateway_value > 0) {
        $.ajax({
            type: "POST",
            url: base_url + 'Integration/home/get_gateway_data',
            data: { 'gatewayID': gateway_value },
            success: function (response) {
                data = $.parseJSON(response);
                gtype = data['gatewayType'];
                if (gtype == '5') {
                    var url = base_url + 'Integration/Payments/pay_multi_invoice';

                    $('<input>', {
                        'type': 'hidden',
                        'id': 'stripeApiKey',
                    }).remove();

                    var form = $("#thest_pay1");
                    $('<input>', {
                        'type': 'hidden',
                        'id': 'stripeApiKey',
                        'name': 'stripeApiKey',
                        'value': data['gatewayUsername']
                    }).appendTo(form);

                }

                $("#thest_pay1").attr("action", url);
                var surchargePercentage = (data.isSurcharge == 1) ? data.surchargePercentage : 0;
                $('#invDefaultsurchargeRate').val(surchargePercentage);
                // showBatchInvoiceSurcharge({surchargePercentage});
                create_card_multi_data();
            }
        });
    }
}



function set_invoice_process_id(id, cid, in_val, pag) {
    $('#customerProcessID').val(pag);
    $('#invoiceProcessID').val(id);
    $('#thest_pay #qbo_check').remove();

    $('<input>').attr({
        type: 'hidden',
        id: 'qbo_check',
        name: 'qbo_check',
        value: 'qbd_pay'
    }).appendTo('#thest_pay');

    $('#thest_pay #inv_amount').val(in_val);

    $('#invDefaultcardSurchargeValue').val(0);


    if (cid != "") {
        $('#CardID').find('option').not(':first').remove();
        $.ajax({
            type: "POST",
            url: base_url + 'Integration/home/check_vault',
            data: { 'customerID': cid },
            success: function (response) {

                data = $.parseJSON(response);

                if (data['status'] == 'success') {

                    $("input[name=sch_method][value='1']").prop("checked", true);

                    var s = $('#CardID');
                    $(s).empty();
                    $(s).append('<option value="new1">New Card</option>');
                    var card1 = data['card'];
                    for (var val in card1) {
                        var selectedCard = false;
                        if (data.recent_card == card1[val]['CardID']) {
                            selectedCard = true;
                            $('#invDefaultcardSurchargeValue').val(card1[val]['isSurcharge']);
                        }
                        if (card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase() != 'ECHECK')
                            $("<option />", { value: card1[val]['CardID'], selected: selectedCard, text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
                    }
                    var card_daata = ' <div class="sch_crd"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" onChange="get_card_surcharge_details({type :1 , cardNumber: this.value})" id="card_number11" name="card_number" class="form-control" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">' + opt + '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" /></div></div> </fieldset></div>' +

                        '<div style="display:none;" class="sch_chk"><fieldset><div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Number</label><div class="col-md-8"><input type="text" id="acc_number" name="acc_number" class="form-control" value="" ></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="card_number">Routing Number</label><div class="col-md-8"><input type="text" id="route_number" name="route_number" class="form-control" ></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="customerID">Account Name</label><div class="col-md-8"><input type="text" id="acc_name" name="acc_name" class="form-control" value="" ></div> </div>' +

                        '<div class="form-group"><label class="col-md-4 control-label" for="Entry Methods">Entry Method</label><div class="col-md-6"><div class="input-group"><select id="secCode" name="secCode" class="form-control valid" aria-invalid="false">' +
                        '<option value="ACK">Acknowledgement Entry (ACK)</option><option value="ADV">Automated Accounting Advice (ADV)</option><option value="ARC">Accounts Receivable Entry (ARC)</option><option value="ATX">Acknowledgement Entry (ATX)</option><option value="BOC">Back Office Conversion (BOC)</option><option value="CBR">Corporate Cross-Border Payment (CBR)</option><option value="CCD">Corporate Cash Disbursement (CCD)</option><option value="CIE">Consumer Initiated Entry (CIE)</option><option value="COR">Automated Notification of Change (COR)</option><option value="CTX">Corporate Trade Exchange (CTX)</option><option value="DNE">Death Notification Entry (DNE)</option><option value="ENR">Automated Enrollment Entry (ENR)</option><option value="MTE">Machine Transfer Entry (MTE)</option><option value="PBR">Consumer Cross-Border Payment (PBR)</option><option value="POP">Point-Of-Presence (POP)</option><option value="POS">Point-Of-Sale Entry (POP)</option><option value="PPD">Prearranged Payment &amp; Deposit (PPD)</option><option value="RCK">Re-presented Check Entry (RCK)</option><option value="SHR">Shared Network Transaction (SHR)</option><option value="TEL">Telephone Initiated Entry (TEL)</option><option value="TRC">Truncated Entry (TRC)</option><option value="TRX">Truncated Entry (TRX)</option><option value="WEB">Web Initiated Entry (WEB)</option><option value="XCK">Destroyed Check Entry (XCK)</option>' +
                        '</select>	</div></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="acct_holder_type">Account Holder Type</label><div class="col-md-6">' +
                        '<select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false"><option value="business"  >Business</option><option value="personal"  >Personal</option> </select></div></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="acct_type">Account Type</label><div class="col-md-6">' +
                        '<select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false"><option value="checking" >Checking</option><option value="saving"  >Saving</option>' +
                        '</select></div></div></fieldset></div>' +
                        '<div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc"  /> Do not save Credit Card</div></div>' +

                        '<div class="panel panel-info notice_box surchargeNoticeClass" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>' +

                        '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="' + data['address1'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="' + data['address2'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="' + data['City'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="' + data['State'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="' + data['zipCode'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="' + data['Country'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="' + data['phoneNumber'] + '" ></div></div></fieldset>';

                    $('.card_div').html(card_daata);
                    var schValue = $("#ccChecked").val();
                    if (schValue == 2) {
                        get_ach_details_pay({ value: 2 });
                    } else {
                        get_ach_details_pay({ value: 1 });
                    }
                }

            }


        });

    }


}


function set_url(e = null) {

    var gateway_value = $("#gateway").val();
    var isEcheck = false;
    if (!gateway_value) {
        var gateway_value = e.value;
        isEcheck = true;
    }
    if (gateway_value > 0) {
        $.ajax({
            type: "POST",
            url: base_url + 'Integration/home/get_gateway_data',
            data: { 'gatewayID': gateway_value },
            success: function (response) {
                data = $.parseJSON(response);
                gtype = data['gatewayType'];

                var surchargePercentage = (data.isSurcharge == 1) ? data.surchargePercentage : 0;
                $('#invDefaultsurchargeRate').val(surchargePercentage);
                $('#invDefaultsurchargeRate').val(surchargePercentage);
                // showInvoiceSurcharge({surchargePercentage});
                if (isEcheck) {
                    create_sch_card();
                } else {
                    create_card_data();
                }
            }

        });
    }
}

function customer_invoice_batch_process(cid) {

    $("#btn_process").attr("disabled", true);
    $('#totalPay').val(0.00);
    $('#totalMultiInvoiceTotal').html('0.00');
    $('<input>').attr({
        type: 'hidden',
        id: 'qbo_check',
        name: 'qbo_check',
        value: 'qbo_pay'
    }).appendTo('#thest_pay1');

    $('#invDefaultcardSurchargeValue').val(0);

    // $('#thest_pay #inv_amount').val(in_val);
    if (cid != "") {
        $('#inv_div').html('');
        $('.card_div').html('');

        $('#CardID1').find('option').not(':first').remove();
        $.ajax({
            type: "POST",
            url: base_url + 'Integration/home/get_customer_all_invoices',
            data: { 'customerID': cid },
            success: function (response) {

                data = $.parseJSON(response);
                //console.log (data);
                if (data['status'] == 'success') {

                    var s = $('#CardID1');
                    $(s).append('<option value="new1">New Card</option>');
                    var card1 = data['card'];

                    for (var val in card1) {
                        if (card1[val]['CardType'] == null || card1[val]['CardType'].toUpperCase() != 'ECHECK')
                            $("<option />", { value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
                    }

                    var invoices = data['invoices'];

                    $('#inv_div').html(invoices);
                    var card_daata1 = ' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" onChange="get_card_surcharge_details({ type :2 ,cardNumber: this.value})" class="form-control" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">' + opt + '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label><div class="col-md-8"><input type="text" id="ccv11"   name="cvv" class="form-control"  /></div></div><div class="form-group"><label class="col-md-4 control-label" for="reference"></label><div class="col-md-6"><input type="checkbox" name="tc" id="tc" onclick="check_frdname();"  /> Do not save Credit Card</div></div></fieldset>' +

                        '<div class="panel panel-info notice_box surchargeNoticeClass2" style="display:none"> <div class="panel-heading"> <h3 class="panel-title">Surcharge Notice</h3> </div> <div class="panel-body surchargeNoticeText"></div> </div>' +
                        '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="' + data['address1'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="' + data['address2'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="' + data['City'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="' + data['State'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="' + data['zipCode'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="' + data['Country'] + '" ></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="' + data['phoneNumber'] + '" ></div></div></fieldset>';

                    $('.card_div').html(card_daata1);
                    create_card_multi_data();

                }

            }


        });

    }


}

function qbd_schedule_payment() {
    var test = $('#Qbd_form1_schedule').valid();

    if (test) {


        var formdata = $('#Qbd_form1_schedule').serialize();
        //  alert ('formdata');
        $(".close1").trigger("click");
        $.ajax({

            type: "POST",
            url: base_url + "SettingSubscription/invoice_schedule",

            data: formdata,

            success: function (response) {
                data = $.parseJSON(response);

                if (data['status'] == 'success') {

                    setTimeout(function () {
                        $('#schDiv').html('<div class="alert alert-success"><strong>Successfully Scheduled</strong></div>');
                    }, 2500);
                    location.reload(true);
                }
                else {
                    setTimeout(function () {
                        $('#schDiv').html('<div class="alert alert-error"><strong>Error:</strong> Scheduling Failed</div>');
                    }, 2500);
                }

            }
        });

    }

    return false;

}
function set_template_data_temp(invoiceID, custID, typeID) {


    document.getElementById('invoicetempID').value = invoiceID;
    document.getElementById('customertempID').value = custID;



    if (typeID != "") {

        $.ajax({
            type: "POST",
            url: base_url + 'Integration/Settings/set_template',
            data: { typeID: typeID, invoiceID: invoiceID, customerID: custID },
            success: function (data) {
                //   console.log(data);
                data = $.parseJSON(data);


                CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
                // $('#textarea-ckeditor').html(data['message']);
                if (data['replyTo'] != '') {
                    $('#replyTo').val(data['replyTo']);
                }
                $('#emailSubject').val(data['emailSubject']);
                $('#toEmail').val(data['toEmail']);
                $('#ccEmail').val(data['addCC']);
                $('#bccEmail').val(data['addBCC']);
                $('#replyEmail').val(data['replyTo']);
                $('#fromEmail').val(data['fromEmail']);
                $('#mailDisplayName').val(data['mailDisplayName']);
                if (data['templateName'] != '') {
                    $('#type_text').val(data['templateName']);
                }
                $('#invoiceCode').val(data['invCode']);

            }
        });

    }

}

$(function () {

    CKEDITOR.replace('textarea-ckeditor', {
        toolbarGroups: [
            { name: 'document', groups: ['mode', 'document'] },			// Displays document group with its two subgroups.
            { name: 'clipboard', groups: ['clipboard', 'undo'] },			// Group's name will be used to create voice label.
            '/',																// Line break - next group will be placed in new line.

            { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
            { name: 'links' },
            { name: 'insert' },
            { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align'] },
            { name: 'styles' },
            { name: 'colors' },
        ]

        // NOTE: Remember to leave 'toolbar' property with the default value (null).
    });

    $('#open_cc').click(function () {
        console.log('--------');
        $('#cc_div').show();
        $(this).hide();
    });
    $('#open_bcc').click(function () {
        $('#bcc_div').show();
        $(this).hide();
    });
    $('#open_reply').click(function () {
        $('#reply_div').show();
        $(this).hide();
    });

    $('#open_from_email').click(function () {
        $('#from_email_div').show();
        $(this).hide();
    });
    $('#open_display_name').click(function () {
        $('#display_name_div').show();
        $(this).hide();
    });



    $('#type').change(function () {
        var templateID = $(this).val();
        var customerID = $('#customertempID').val();
        if (templateID != "") {

            $.ajax({
                type: "POST",
                url: base_url + 'Integration/Settings/set_template_cus_details',
                data: { templateID: templateID, customerID: customerID },
                success: function (data) {
                    console.log(data);
                    // alert($data);
                    data = $.parseJSON(data);


                    CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
                    $('#textarea-ckeditor').html(data['message']);
                    $('#emailSubject').val(data['emailSubject']);
                    $('#toEmail').val(data['toEmail']);
                    $('#ccEmail').val(data['addCC']);
                    $('#bccEmail').val(data['addBCC']);
                    $('#replyEmail').val(data['replyTo']);
                    /*	$('#form-validation1').remove();	
                                
                                 $('<input>', {
                                                'type': 'hidden',
                                                'id'  : 'systemEmail',
                                                'name': 'systemEmail',
                                                'value': data['invCode']
                                                }).appendTo($('#form-validation1'));  */


                }
            });

        }
    });

    $('#CardID').change(function () {
        var cardlID = $(this).val();
        $('#invDefaultcardSurchargeValue').val(0);

        if (cardlID != '' && cardlID != 'new1') {
            $("#btn_process").attr("disabled", true);
            $("#card_loader").show();
            $.ajax({
                type: "POST",
                url: base_url + 'Payments/get_card_data',
                data: { 'cardID': cardlID },
                success: function (response) {

                    $("#card_loader").hide();
                    data = $.parseJSON(response);
                    //console.log(data);

                    if (data['status'] == 'success') {
                        $("#btn_process").attr("disabled", false);

                        $('#invDefaultcardSurchargeValue').val(data['card']['isSurcharge']);

                        if (gtype == '5') {

                            var form = $("#thest_pay");

                            $('#thest_pay #number').remove();
                            $('#thest_pay #exp_year').remove();
                            $('#thest_pay #exp_month').remove();
                            $('#thest_pay #cvc').remove();

                            $('<input>', {
                                'type': 'hidden',
                                'id': 'number',
                                'name': 'number',
                                'value': data['card']['CardNo']
                            }).appendTo(form);
                            $('<input>', {
                                'type': 'hidden',
                                'id': 'exp_year',
                                'name': 'exp_year',
                                'value': data['card']['cardYear']
                            }).appendTo(form);

                            $('<input>', {
                                'type': 'hidden',
                                'id': 'exp_month',
                                'name': 'exp_month',
                                'value': data['card']['cardMonth']
                            }).appendTo(form);

                            $('<input>', {
                                'type': 'hidden',
                                'id': 'cvc',
                                'name': 'cvc',
                                'value': data['card']['CardCVV']
                            }).appendTo(form);


                            var pub_key = $('#stripeApiKey').val();
                            if (pub_key) {
                                Stripe.setPublishableKey(pub_key);
                                Stripe.createToken({
                                    number: $('#number').val(),
                                    cvc: $('#cvc').val(),
                                    exp_month: $('#exp_month').val(),
                                    exp_year: $('#exp_year').val()
                                }, stripeResponseHandler_res);
                            }
                            // Prevent the form from submitting with the default action

                        }
                        var surchargePercentage = $("#invDefaultsurchargeRate").val();
                        showInvoiceSurcharge({ surchargePercentage, CardID: data['card']['CardID'] });
                    }

                }
            });
        }
    });


    init();

});



/******************Invoice Details Page ********************/


function select_plan_val(rid) {



    var itemID = $('#productID' + rid).val();
    //alert(itemID);
    $('#productID' + rid).valid();
    $.ajax({
        type: "POST",
        url: base_url + "SettingSubscription/get_item_data",
        data: { 'itemID': itemID },
        success: function (data) {

            var item_data = $.parseJSON(data);
            $('#description' + rid).val(item_data['SalesDesc']);
            $('#rate' + rid).html(roundN(item_data['SalesPrice'], 2));
            $('#unit_rate' + rid).val(roundN(item_data['SalesPrice'], 2));

            $('#quantity' + rid).val(1);
            $('#total11' + rid).html(roundN(($('#quantity' + rid).val() * $('#unit_rate' + rid).val()), 2));
            $('#total' + rid).val(roundN(($('#quantity' + rid).val() * $('#unit_rate' + rid).val()), 2));

            var grand_total = 0;
            $(".total_val").each(function () {
                var tval = $(this).val();
                //	alert(tval)
                if (tval == "") tval = 0;
                grand_total = parseFloat(grand_total) + parseFloat(tval);



            });

            calculate_tax_total(grand_total);


        }
    });

}

function calculate_tax_total(grand_total) {
    var total_tax = 0;
    var rate = 0;
    var subtotal = grand_total;
    rate = $('#txID').val();


    /*******In case of Flat Tax******/
    total_tax = subtotal * rate / 100;

    grand_total = grand_total + total_tax;

    $('#grand_total').html(format22(grand_total));

    $('#tax_val').html(format22(total_tax));
    $('#taxv_rate').html('<strong>TAX (' + rate + '%)</strong>');

    $('#sub_total').html(format22(subtotal));
    $('#total_amt').html(format22(grand_total));

    $('#grand_total').html(format22(grand_total));

}
function calculate_tax_total_qbd(grand_total) {
    var total_tax = 0;
    var rate = 0;
    var subtotal = grand_total;
    rate = $('#txID').val();

    var checkedTotal = 0;
    $('.tax_checked:checkbox:checked').each(function () {
        var checkID = this.id;
        var checkedAmountId = checkID.replace('tax_check', '');

        checkedTotal += parseFloat($('#total' + checkedAmountId).val());
    });

    /*******In case of Flat Tax******/
    total_tax = checkedTotal * rate / 100;

    // console.log('	--------', checkedTotal)
    grand_total = grand_total + total_tax;

    $('#grand_total').html(format22(grand_total));

    $('#tax_val').html(format22(total_tax));
    $('#taxv_rate').html('<strong>TAX (' + rate + '%)</strong>');

    $('#sub_total').html(format22(subtotal));
    $('#total_amt').html(format22(grand_total));

    $('#grand_total').html(format22(grand_total));

}
function set_qty_val(rid) {

    var qty = $('#quantity' + rid).val();
    var rate = $('#unit_rate' + rid).val();
    var tax = 0;
    if (qty > 0) {
        if ($('input#tax_check' + rid).is(':checked')) {
            tax = $('#tax_check' + rid).val();
        }

        var total_tax = (qty * rate) * tax / 100;
        var total = qty * parseFloat(rate) + parseFloat(total_tax);

        $('#total11' + rid).html(total.toFixed(2));
        $('#total' + rid).val(total.toFixed(2));
        var grand_total = 0;
        $(".total_val").each(function () {


            var tval = $(this).val();

            if (tval == "") tval = 0;
            grand_total = parseFloat(grand_total) + parseFloat(tval);
        });



        //	$('#grand_total').html(format22(grand_total));

        calculate_tax_total(grand_total);

    }

}

function format22(num) {

    var p = parseFloat(num).toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
        return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

    // return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

var room = 0; var grand_total1 = 0;
var inID = $('#invNo').val();
async function init() {

    await $.ajax({

        'type': "POST",

        'data': { 'invID': inID },
        'url': base_url + "home/get_invoice_item_count_data",
        'success': function (data) {
            // console.log("HELLO");  
            //  console.log(data);
            jsdata = $.parseJSON(data);

            return jsdata;
        }
    });

    if (jQuery.isEmptyObject(jsdata['items'])) {

        room = 1;
        //	$('#grand_total').html(format22(grand_total1));

    } else {


        room = jsdata['rows'];


    }


    await $.ajax({

        'type': "POST",

        'url': base_url + "home/get_product_data",
        'success': function (data) {
            /// $.parseJSON(data); 
            //console.log(data);
            jsplandata = $.parseJSON(data);

            // return jsplandata;
        }
    });

}




function item_invoice_fields() {


    room++;



    var plan_data = jsplandata;

    var plan_html = '<option value="">Select Product or Service</option>';
    for (var val in plan_data) { plan_html += '<option value="' + plan_data[val]['ListID'] + '">' + plan_data[val]['FullName'] + '</option>'; }


    var objTo = document.getElementById('item_fields')
    var divtest = document.createElement("tr");
    divtest.setAttribute("class", " rd removeclass" + room);
    var rdiv = 'removeclass' + room;


    if ($('#taxes').val() == '') {
        var show_tax = 'style="display:none;"';
        var tax_val = 0;
    } else {
        var show_tax = ' ';
        var tax_val = $('.tax_checked').val();
    }

    divtest.innerHTML = '<td><select class="form-control"  onchange="select_plan_val(' + room + ');"  id="productID' + room + '" name="productID[]">' + plan_html + '</select></td><td> <input type="text" class="form-control" id="description' + room + '" name="description[]" value="" ></td><td class="text-right">$<span id="rate' + room + '"> 0.00 </span><input type="hidden" id="unit_rate' + room + '"  name="unit_rate[]" value="0" /> </td><td class="text-right"><div class="input-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-left" maxlength="4" onblur="set_qty_val(' + room + ');" id="quantity' + room + '" name="quantity[]" value="" ></div></td> <td class="set_taxes">   <div class="col-sm-1 nopadding"><div class="form-group"> <input type="checkbox" onkeypress="return isNumberKeys(event)" id="tax_check' + room + '" onchange="set_tax_val(this, ' + room + ')" name="tax_check[]" class="tax_checked" value="' + tax_val + '"><input type="hidden" id="is_tax_check' + room + '" name="is_tax_check[]" ' + show_tax + ' value="0"><input type="hidden" id="txn_line_id' + room + '" name="txn_line_id[]" value="-1"></div></div></td> <td> <div class="input-group"> <input type="hidden" class="form-control total_val" id="total' + room + '" name="total[]" value=""/>$<span id="total11' + room + '">0.00</span><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></td> ';

    /*	divtest.innerHTML = '<td><select class="form-control"  onchange="select_plan_val('+room+');"  id="productID'+room+'" name="productID[]">'+ plan_html+'</select></td><td> <input type="text" class="form-control" id="description'+room+'" name="description[]" value="" placeholder="Description "></td><td class="text-right">$<span id="rate'+room+'"> 0.00 </span><input type="hidden" id="unit_rate'+room+'"  name="unit_rate[]" value="0" /> </td><td class="text-right"><div class="input-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-left" maxlength="3" onblur="set_qty_val('+room+');" id="quantity'+room+'" name="quantity[]" value="" placeholder="Qty"><label class="set_taxes" style="position:absolute" > <input type="checkbox" onkeypress="return isNumberKeys(event)" id="tax_check'+room+'" onchange="set_tax_val(this, '+room+')" name="tax_check[]" '+show_tax+' class="show_check tax_checked" value="'+tax_val+'"></labe> </div></td>  <td> <div class="input-group"> <input type="hidden" class="form-control total_val" id="total'+room+'" name="total[]" value=""/>$<span id="total11'+room+'">0.00</span><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_education_fields('+ room +');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></td> ';
        */
    // objTo.closest("tr rd").appendChild(divtest);
    $(divtest).insertAfter($('table tr.rd:last'));
    //objTo.appendChild(divtest)
}

function remove_education_fields(rid) {
    var gr_val = 0;
    $('.removeclass' + rid).remove();
    $(".total_val").each(function () {
        var test = $(this).val();
        gr_val += parseFloat(test);
    });
    calculate_tax_total(gr_val);
}

function isNumberKeys(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (charCode == 46) {
        var inputValue = $("#inputfield").val()
        if (inputValue.indexOf('.') < 1) {
            return true;
        }
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function set_tax_val(mythis, rid) {
    var total_tax = 0;
    var qty = $('#quantity' + rid).val();
    var rate = $('#unit_rate' + rid).val();
    var tax = $('#tax_check' + rid).val();

    if (mythis.checked) {
        total_tax = (qty * rate) * tax / 100;
        var total = qty * rate + total_tax;
        $('#total11' + rid).html(format22(total.toFixed(2)));
        $('#is_tax_check' + rid).val(1);

    }
    else {
        var total = qty * rate;
        $('#is_tax_check' + rid).val(0);
        //	$('#total'+rid).val(total.toFixed(2));
        $('#total11' + rid).html(total.toFixed(2));
    }

    var grand_total = 0;
    $(".total_val").each(function () {
        var tval = $(this).val() != '' ? $(this).val() : 0;
        grand_total = parseFloat(grand_total) + parseFloat(tval);
    });
    var subtotal = grand_total;

    /*******In case of Flat Tax******/




    $('#grand_total').html(format22(grand_total));

    calculate_tax_total_qbd(grand_total);


}

function set_tax_valnew() {

    var rate = 0;
    var total_tax = 0;
    var tax_id = $('#taxesee').val();

    if (tax_id !== "") {

        $.ajax({
            url: base_url + "SettingSubscription/get_tax_id",
            async: false,
            type: 'POST',
            data: { tax_id: tax_id },
            dataType: 'json',
            success: function (data) {
                console.log(data);

                // $('.tax_checked').val(data.taxRate);

                $('#txID').remove();


                $('<input>', {
                    'type': 'hidden',
                    'id': 'txID',
                    'name': 'txID',

                    'value': data.taxRate,
                }).appendTo($('#form-validation'));


            }
        });

    }
    else {

        $('#txID').remove();


        $('<input>', {
            'type': 'hidden',
            'id': 'txID',
            'name': 'txID',

            'value': '',
        }).appendTo($('#form-validation'));
    }


    var grand_total = 0;
    $(".total_val").each(function () {
        var tval = $(this).val() != '' ? $(this).val() : 0;
        grand_total = parseFloat(grand_total) + parseFloat(tval);
    });


    calculate_tax_total(grand_total);
    /* var subtotal = grand_total;
        rate = $('#txID').val();
 
 
 
     total_tax =subtotal*rate/100;
       grand_total =grand_total+total_tax;
   
    $('#grand_total').html(format22(grand_total));
     
           $('#tax_val').html(format22(total_tax));
          
        $('#sub_total').html(format22(subtotal));
        $('#total_amt').html(format22(grand_total));
        
        $('#grand_total').html(format22(grand_total));
*/

}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode == 46) {
        return true;
    }
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}


