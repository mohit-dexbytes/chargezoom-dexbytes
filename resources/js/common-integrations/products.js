var base_url = $('#js_base_url').val();
var roomCount = $('#roomCount').val();

room = 0;
let taxListData = [];
if (roomCount !== undefined) {
    room = roomCount;
}

async function init() {

    var item = $('#item-exist').val();
    await $.ajax({

        'type': "POST",

        'url': base_url + "Integration/Items/get_product_data",
        'success': function (data) {
            allData = $.parseJSON(data);
            jsplandata = allData.product_list;
            taxListData = allData.tax_list;
            item_invoice_fields(true);
            if (item == 0) {
                item_fields(true);
            }
        }
    });

}
init();
function item_fields(first=false) {

	room++;
 
	var plan_data = jsplandata;
	var plan_html = '<option val="">Select Product and Service</option>';
	for (var val in plan_data) {
		plan_html += '<option value="' + plan_data[val]['Code'] + '">' + plan_data[val]['Name'] + '</option>';
	}

    var tax_data = taxListData
    var tax_html = '';
    for (var val in tax_data) { tax_html += '<option value="' + tax_data[val]['taxID'] + '">' + tax_data[val]['friendlyName'] + '</option>'; }

	var onetime_html = '<option val="0">Recurring</option><option val="1">One Time Charge</option>';
	var objTo = document.getElementById('item_fields')
	var divtest = document.createElement("div");
	divtest.setAttribute("class", "form-group removeclass" + room);
	var rdiv = 'removeclass' + room;
	var show_tax = '';

	if ($('#taxes').val() == '') {
		var show_tax = 'disabled"';
		var tax_val = 0;
	} else {
		var tax_val = $('.tax_checked').val();
	}

	var deleteButton = ' <div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_fields(' + room + ');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>';
	if(first) {
		deleteButton = '';
	}

	divtest.innerHTML = '<div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control select-chosen"  onchange="select_plan_val(' + room + ', this);"  id="productID' + room + '" name="productID[]">' + plan_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge' + room + '" name="onetime_charge[]">' + onetime_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description' + room + '" name="description[]" value="" ></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float text-left" id="unit_rate' + room + '" name="unit_rate[]" value="" onblur="set_unit_val(' + room + ');" ><input type="hidden" id="sbsProduct' + room + '" name="sbsProduct[]" value="" ><input type="hidden" class="form-control" id="AccountCode' + room + '" name="AccountCode[]" value=""></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val(' + room + ');" id="quantity' + room + '" name="quantity[]" value="" ></div></div>  <div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control select-chosen ptaxSelect" ' + show_tax + ' id="ptaxID' + room + '" name="ptaxID[]" onchange="calculateAmount()">' + tax_html + '</select></div></div></div> 	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total' + room + '" name="total[]" value="" >'+deleteButton+'</div></div> </div> <div class="clear"></div>';

	objTo.appendChild(divtest);
	$(".select-chosen").chosen();

}

function item_invoice_fields(first = false) {
    room++;

    var plan_data = jsplandata;
    var plan_html = '<option value="">Select Product or Service</option>';
    for (var val in plan_data) { plan_html += '<option value="' + plan_data[val]['Code'] + '">' + plan_data[val]['Name'] + '</option>'; }
    
    var tax_data = taxListData
    var tax_html = '';
    for (var val in tax_data) { tax_html += '<option value="' + tax_data[val]['taxID'] + '">' + tax_data[val]['friendlyName'] + '</option>'; }

    if ($("#item_field_ins").length) {
        var objTo = document.getElementById('item_field_ins')
        var divtest = document.createElement("div");
        divtest.setAttribute("class", "form-group removeclass" + room);
        var rdiv = 'removeclass' + room;

        var deleteButton = '<div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_fields(' + room + ');"> <span class="fa fa-times" aria-hidden="true"></span></button></div>';

        if (first) {
            deleteButton = '';
        }

        if ($('#taxes').val() == '') {
            var show_tax = 'style="display:none;"';
            var tax_val = 'disabled';
        } else {
            var tax_val = '';
        }

        divtest.innerHTML = '<div class="col-sm-3 nopadding"><div class="form-group"><select class="form-control select-chosen"  onchange="select_plan_val(' + room + ', this);"  id="productID' + room + '" name="productID[]">' + plan_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description' + room + '" name="description[]" value=""></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-left" id="unit_rate' + room + '" name="unit_rate[]" value="" onblur="set_unit_val(' + room + ');"></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val(' + room + ');" id="quantity' + room + '" name="quantity[]" value=""></div></div>  <div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control select-chosen ptaxSelect" ' + tax_val + ' id="ptaxID' + room + '" name="ptaxID[]" onchange="calculateAmount()">' + tax_html + '</select></div></div></div> 	<div class="col-sm-2 row nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total' + room + '" name="total[]" value=""> ' + deleteButton + '</div></div> </div> <div class="clear"></div> <input type="hidden" class="form-control" id="AccountCode' + room + '" name="AccountCode[]" value="">';

        objTo.appendChild(divtest)
        $(".select-chosen").chosen();
    }
}

function item_invoice_details_fields() {
    room++;
    var plan_data = jsplandata;

    var plan_html = '<option value="">Select Product or Service</option>';
    for (var val in plan_data) { plan_html += '<option value="' + plan_data[val]['Code'] + '">' + plan_data[val]['Name'] + '</option>'; }

    var tax_data = taxListData
    var tax_html = '';
    for (var val in tax_data) { tax_html += '<option value="' + tax_data[val]['taxID'] + '">' + tax_data[val]['friendlyName'] + '</option>'; }

    var objTo = document.getElementById('item_fields')
    var divtest = document.createElement("tr");
    divtest.setAttribute("class", " rd removeclass" + room);
    var rdiv = 'removeclass' + room;


    if ($('#taxes').val() == '') {
        var show_tax = 'disabled';
        var tax_val = 0;
    } else {
        var show_tax = '';
        var tax_val = $('.tax_checked').val();
    }

    divtest.innerHTML = '<td><select class="form-control"  onchange="select_plan_val(' + room + ', this);"  id="productID' + room + '" name="productID[]">' + plan_html + '</select></td><td> <input type="text" class="form-control" id="description' + room + '" name="description[]" value="" ></td><td class="text-right">$<span id="rate' + room + '"> 0.00 </span><input type="hidden" id="unit_rate' + room + '"  name="unit_rate[]" value="0" /> </td><td class="text-right"><div class="input-group pull-right"> <input type="text" onkeypress="return isNumberKey(event)" size="6" class="form-control text-right" maxlength="4" onblur="set_qty_val(' + room + ');" id="quantity' + room + '" name="quantity[]" value="1" ></div></td> <td class=""><select class="form-control ptaxSelect" onchange="calculateAmount()" id="ptaxID' + room + '" name="ptaxID[]" ' + show_tax + '>' + tax_html + '</select></td> <td> <div class="input-group"><input type="hidden" class="form-control" id="AccountCode' + room + '" name="AccountCode[]" value=""><input type="hidden" class="form-control" id="xeroitemID' + room + '" name="xeroitemID[]" value=""> <input type="hidden" class="form-control total_val" id="total' + room + '" name="total[]" value=""/>$<span id="total11' + room + '">0.00</span><div class="input-group-btn"><button class="btn btn-danger" type="button" onclick="remove_fields(' + room + ');"> <span class="fa fa-times" aria-hidden="true"></span></button></div></div></td> ';

    $(divtest).insertAfter($('table tr.rd:last'));
}

function remove_fields(rid) {
    var gr_val = 0;
    var rid_val = $('#total' + rid).val();
    $(".total_val").each(function () {
        var test = $(this).val();
        gr_val += parseFloat(test);
    });
    //  var gr_val  = $('#grand_total').html();
    if (rid_val) {
        var dif = parseFloat(gr_val) - parseFloat(rid_val);
        $('#grand_total').html(format22(dif));
        $('#sub_total').html(format22(dif));
        $('#total_amt').html(format22(dif));
    }
    $('.removeclass' + rid).remove();

}

function select_plan_val(rid, e) {
    var index = jsplandata.map(function (productData) { return productData.Code; }).indexOf(e.value);
    var item_data = jsplandata[index];
    $('#description' + rid).val(item_data['SalesDescription']);
    $('#unit_rate' + rid).val(roundN(item_data['saleCost'], 2));
    $('#quantity' + rid).val(1);
    $('#total' + rid).val(roundN(($('#quantity' + rid).val() * $('#unit_rate' + rid).val()), 2));
    $('#AccountCode' + rid).val(item_data['AccountCode']);

    var grand_total = 0;
    $(".total_val").each(function () {
        var tval = $(this).val() != '' ? $(this).val() : 0;
        grand_total = parseFloat(grand_total) + parseFloat(tval);
    });
    $('#grand_total').html(format22(grand_total));

    $('#total11' + rid).html(roundN(($('#quantity' + rid).val() * $('#unit_rate' + rid).val()), 2));
    $('#sub_total').html(format22(grand_total));
    $('#total_amt').html(format22(grand_total));
    $('#rate' + rid).html(roundN(item_data['saleCost'], 2));

    $('#sbsProduct' + rid).val(item_data['productID']);
    calculateAmount();
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;

    if (charCode == 46) {
        var inputValue = $("#inputfield").val()
        if (inputValue.indexOf('.') < 1) {
            return true;
        }
        return false;
    }
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function set_qty_val(rid) {

    var qty = $('#quantity' + rid).val();
    var rate = $('#unit_rate' + rid).val();
    var tax = 0;
    if (qty > 0) {
        if ($('input#tax_check' + rid).is(':checked')) {
            tax = $('#tax_check' + rid).val();
        }

        var total_tax = (qty * rate) * tax / 100;
        var total = qty * parseFloat(rate) + parseFloat(total_tax);

        $('#total11' + rid).html(total.toFixed(2));
        $('#total' + rid).val(total.toFixed(2));
        var grand_total = 0;
        $(".total_val").each(function () {

            var tval = $(this).val();
            
            if (tval == "") tval = 0;
            grand_total = parseFloat(grand_total) + parseFloat(tval);
        });
        // calculate_tax_total(grand_total);

    }
    calculateAmount();
}

function format22(num) {
    var p = parseFloat(num).toFixed(2).split(".");
    return p[0].split("").reverse().reduce(function (acc, num, i, orig) {
        return num == "-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];
}

function set_unit_val(rid) {


    var qty = $('#quantity' + rid).val();
    if (qty > 0) {

        var rate = $('#unit_rate' + rid).val();
        var tax = 0;

        if ($('input#tax_check' + rid).is(':checked')) {
            tax = $('#tax_check' + rid).val();
        }
        var total_tax = (qty * rate) * tax / 100;
        var total = qty * rate + total_tax;
        $('#total' + rid).val(total.toFixed(2));

        //$(total_val)
        var grand_total = 0;
        $(".total_val").each(function () {

            var tval = $(this).val() != '' ? $(this).val() : 0;
            grand_total = parseFloat(grand_total) + parseFloat(tval);
        });

        $('#grand_total').html(format22(grand_total));

    }

    calculateAmount();
}

function set_tax_val(mythis, rid) {

    var qty = $('#quantity' + rid).val();
    var rate = $('#unit_rate' + rid).val();
    var tax = $('#tax_check' + rid).val();

    if (mythis.checked) {
        var total_tax = (qty * rate) * tax / 100;
        var total = qty * rate + total_tax;
        $('#total' + rid).val(total.toFixed(2));
    }
    else {
        var total = qty * rate;
        $('#total' + rid).val(total.toFixed(2));
    }

    var grand_total = 0;
    $(".total_val").each(function () {
        var tval = $(this).val() != '' ? $(this).val() : 0;
        grand_total = parseFloat(grand_total) + parseFloat(tval);
    });

    $('#grand_total').html(format22(grand_total));
}

function calculate_tax_total(grand_total) {
    var total_tax = 0;
    var rate = 0;
    var subtotal = grand_total;
    rate = $('#txID').val();

    if(!rate){
        rate = 0;
    }

    /*******In case of Flat Tax******/
    total_tax = subtotal * rate / 100;

    grand_total = grand_total + total_tax;

    $('#grand_total').html(format22(grand_total));

    $('#tax_val').html(format22(total_tax));
    $('#taxv_rate').html('<strong>TAX (' + rate + '%)</strong>');

    $('#sub_total').html(format22(subtotal));
    $('#total_amt').html(format22(grand_total));

    $('#grand_total').html(format22(grand_total));

}

$('#customerID').change(function () {

    var cid = $(this).val();
    var gatewayID = $('#gateway_list').val();
    if (cid !== "") {
        $('#card_list').find('option').not(':first').remove();
        $.ajax({
            type: "POST",
            url: base_url + "Integration/home/check_vault",
            data: { 'customerID': cid, 'gatewayID': gatewayID },
            success: function (response) {

                data = $.parseJSON(response);
                //console.log(data);
                if (data['status'] == 'success') {

                    var s = $('#card_list');
                    //console.log (data['card']);
                    var card1 = data['card'];
                    var cardTypeOption = data['cardTypeOption'];
                    //$(s).append('<option value="new1">New Card</option>');
                    if (cardTypeOption == 1) {
                        $(s).append('<option value="new1">New Card</option>');
                        $(s).append('<option value="new2">New eCheck</option>');
                    } else if (cardTypeOption == 2) {
                        $(s).append('<option value="new1">New Card</option>');
                    } else if (cardTypeOption == 3) {
                        $(s).append('<option value="new2">New eCheck</option>');
                    } else if (cardTypeOption == 4) {

                    } else {

                    }
                    for (var val in card1) {
                        $("<option />", { value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
                    }

                    $('#baddress1').val(data['address1']);
                    $('#baddress2').val(data['address2']);
                    $('#bcity').val(data['City']);
                    $('#bstate').val(data['State']);
                    $('#bzipcode').val(data['zipCode']);
                    $('#bcountry').val(data['Country']);
                    $('#bphone').val(data['phoneNumber']);
                    $('#email').val(data['userEmail']);
                    $('#address1').val(data['ship_address1']);
                    $('#address2').val(data['ship_address2']);
                    $('#city').val(data['ship_city']);
                    $('#state').val(data['ship_state']);
                    $('#zipcode').val(data['ship_zipcode']);
                    $('#country').val(data['ship_country']);
                    $('#phone').val(data['phoneNumber']);

                    if ($('#chk_add_copy').is(':checked')) {

                        $('#address1').val($('#baddress1').val());
                        $('#address2').val($('#baddress2').val());
                        $('#city').val($('#bcity').val());
                        $('#state').val($('#bstate').val());
                        $('#zipcode').val($('#bzipcode').val());
                        $('#country').val($('#bcountry').val());
                        $('#phone').val($('#bphone').val());


                    }


                }

            }


        });

    }
});

$('.tax_div').change(function () {
    var tax_id = $(this).val();
    var res = tax_id.split(",");
    var rateid = res[0];
    $.ajax({
        url: base_url + "Integration/home/get_tax_id",
        type: 'POST',
        data: { tax_id: rateid },
        dataType: 'json',
        success: function (data) {
            $('.tax_checked').val(data.taxRate);
            $('#txID').val(data.taxRate);
            calculateAmount();
        }
    });

});

$('#taxes').change(function () {
    if ($(this).val() !== "") {
        $('.set_taxes').show();
        $('.show_check').show();

        $('.ptaxSelect').prop("disabled", false);
        $('.ptaxSelect').prop('disabled', false).trigger("chosen:updated");
    } else {
        $('#tax_check').val('');
        $('.set_taxes').hide();
        $('.ptaxSelect').prop("disabled", true);
        $('.ptaxSelect').prop('disabled', true).trigger("chosen:updated");
    }
});

function calculateAmount() {
    var taxType = $(`select#taxes option`).filter(":selected").val();

    var grand_total = 0;
    var grand_tax = 0;
    var grand_subtotal = 0;
    $('.total_val').each(function () {
        var checkID = this.id;
        var rid = checkID.replace('total', '');

        var qty = parseFloat($('#quantity' + rid).val());
        var rate = parseFloat($('#unit_rate' + rid).val());
        var tax = 0; //parseFloat($('#tax_check' + rid).val());

        if(taxType !=''){
            var selectedTax = $(`select#ptaxID${rid} option`).filter(":selected").val();
            var index = taxListData.map(function (taxData) { return taxData.taxID; }).indexOf(selectedTax);
            var se_tax_data = taxListData[index];
            tax = se_tax_data['taxRate'];
        }

        var total_tax = (qty * rate) * tax / 100;
        var total = qty * rate;
        $('#total' + rid).val(total.toFixed(2));
        
        grand_subtotal = parseFloat(grand_subtotal) + parseFloat(total);
        grand_tax = parseFloat(grand_tax) + parseFloat(total_tax);
        grand_total = parseFloat(grand_total) + parseFloat(total + total_tax);
    });

    $('#grand_total').html(format22(grand_total));
    $('#total_amt').html(format22(grand_total));
    $('#tax_val').html(format22(grand_tax));
    $('#sub_total').html(format22(grand_subtotal));
}

function set_template_data_temp(invoiceID, custID, typeID) {
    document.getElementById('invoicetempID').value = invoiceID;
    document.getElementById('customertempID').value = custID;

    if (typeID != "") {

        $.ajax({
            type: "POST",
            url: base_url + 'Integration/Settings/set_template',
            data: { typeID: typeID, invoiceID: invoiceID, customerID: custID },
            success: function (data) {
                data = $.parseJSON(data);
                CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
                if (data['replyTo'] != '') {
                    $('#replyTo').val(data['replyTo']);
                }
                $('#emailSubject').val(data['emailSubject']);
                $('#toEmail').val(data['toEmail']);
                $('#ccEmail').val(data['addCC']);
                $('#bccEmail').val(data['addBCC']);
                $('#replyEmail').val(data['replyTo']);
                $('#fromEmail').val(data['fromEmail']);
                $('#mailDisplayName').val(data['mailDisplayName']);
                if (data['templateName'] != '') {
                    $('#type_text').val(data['templateName']);
                }
                $('#invoiceCode').val(data['invCode']);
            }
        });

    }

}

function set_plan_change() {

	var plan_id = $('#sub_plan').val();

	$('#item_fields').empty();
	room++;
	var plan_data = jsplandata;
	var plan_html = '<option value="">Select Product or Service</option>';
	for (var val in plan_data) {
		plan_html += '<option value="' + plan_data[val]['Code'] + '"   >' + plan_data[val]['Name'] + '</option>';
	}

    var tax_data = taxListData
    var tax_html = '';
    for (var val in tax_data) { tax_html += '<option value="' + tax_data[val]['taxID'] + '">' + tax_data[val]['friendlyName'] + '</option>'; }

	var onetime_html = '<option value="0">Recurring</option><option value="1">One Time Charge</option>';
	var show_tax = '';

	if ($('#taxes').val() == '') {
		var show_tax = 'disabled';
		var tax_val = 0;
	} else {
		var tax_val = $('#taxes').val();
	}
	var i = 1;
	$.ajax({
		type: "POST",
		url: base_url + "Integration/SettingSubscription/get_subplan",
		data: { 'planID': plan_id },
		success: function (response) {

			data = $.parseJSON(response);
			// console.log(data);
			$('#freetrial').val(data[0].freeTrial);

			$('#paycycle').val(data[0].invoiceFrequency);
			$('#duration_list').val(data[0].subscriptionPlan);
			$('#grand_total').html((data[0].subscriptionAmount));

			if (data[0].emailRecurring == 1) {
				$("input[name=email_recurring][value='1']").prop("checked", true);
			} else {
				$("input[name=email_recurring][value='0']").prop("checked", true);
			}

			if (data[0].automaticPayment == 1) {
				$("input[name=autopay][value='1']").prop("checked", true);
				$('#set_pay_data').show();
			} else {
				$("input[name=autopay][value='0']").prop("checked", true);
				$('#set_pay_data').hide();
			}


			for (var t = 0; t < data.length; t++) {

				$('#item_fields').append('<div class="form-group removeclass' + i + '"><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"  onchange="select_plan_val(' + i + ', this);"  id="productID' + i + '" name="productID[]">' + plan_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control"   id="onetime_charge' + i + '" name="onetime_charge[]">' + onetime_html + '</select></div></div><div class="col-sm-2 nopadding"><div class="form-group"> <input type="text" class="form-control" id="description' + i + '" name="description[]" value="" ><input type="hidden" id="sbsProduct' + i + '" name="sbsProduct[]" value="" ><input type="hidden" class="form-control" id="AccountCode' + i + '" name="AccountCode[]" value=""></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKeys(event)" class="form-control float" id="unit_rate' + i + '" name="unit_rate[]" value="" onblur="set_unit_val(' + i + ');" ></div></div><div class="col-sm-1 nopadding"><div class="form-group"> <input type="text" onkeypress="return isNumberKey(event)" class="form-control text-center" maxlength="4" onblur="set_qty_val(' + i + ');" id="quantity' + i + '" name="quantity[]" value="" ></div></div>  <div class="col-sm-2 nopadding"><div class="form-group"><select class="form-control select-chosen ptaxSelect" ' + show_tax + ' id="ptaxID' + i + '" name="ptaxID[]" onchange="calculateAmount()">' + tax_html + '</select></div></div>	     <div class="col-sm-2 nopadding"><div class="form-group"><div class="input-group"> <input type="text" class="form-control total_val" id="total' + i + '" name="total[]" value="" > </div></div> </div> <div class="clear"></div></div> ');
				$('#productID' + i).val(data[t].Code);
				$('#AccountCode' + i).val(data[t].AccountCode);
				$('#sbsProduct' + i).val(data[t].itemListID);
				$('#onetime_charge' + i).val(data[t].oneTimeCharge);
				$('#description' + i).val(data[t].itemDescription);
				$('#unit_rate' + i).val(data[t].itemRate);
				$('#quantity' + i).val(data[t].itemQuantity);
				$('#total' + i).val((data[t].itemRate * data[t].itemQuantity).toFixed(2));

				i++;
			}

			setTimeout(function () {
				$("input[name='quantity']").trigger('click');
			}, 1000);



		}


	});

}

function chk_payment(r_val) {
	if (r_val == '1') {
		$('#set_pay_data').show();
	} else {
		$('#set_pay_data').hide();
		$('#set_credit').hide();
		$('#set_eCheck').hide();
		$('#card_list').val('');
	}
}