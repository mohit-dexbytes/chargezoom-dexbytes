function get_card_surcharge_details(args) {
	var base_url 	= $('.customer_base_url').attr('id');

	var cardId = (args.cardId == undefined) ? 0 : args.cardId;
	var cardNumber = (args.cardNumber == undefined) ? 0 : args.cardNumber;
	if((cardId > 0 || cardNumber.length >= 6)) {
		$.ajax({
			type: "POST",
			url: base_url + 'General_controller/check_card_surcharge',
			data: {
				cardId, cardNumber
			},
			dataType: 'json',
			success: function (response) {
				if(response.data > 0) {
                    $( "#surchargeNotice" ).show();
                } else {
                    $( "#surchargeNotice" ).hide();
                }
			}
		});
	} else {
		$( "#surchargeNotice" ).hide();
	}
}
