/*
 *  Document   : ecomDashboard.js
 *  Author     : Ecrubit
 *  Description: Custom javascript code used in eCommerce Dashboard page
 */

var ServiceView = function() {

    return {
        init: function() {
            /*
             * Flot Jquery plugin is used for charts
             *
             * For more examples or getting extra plugins you can check http://www.flotcharts.org/
             * Plugins included in this template: pie, resize, stack, time
             */

            // Get the elements where we will attach the charts
            var chartOverview = $('#chart-overview');
			var chartFranchise = $('#chart-franchise');
			

            // Random data for the charts
			var dataOrders    = [[1, 40], [2, 20], [3, 15], [4, 35], [5, 32], [6, 38], [7, 45], [8, 91], [9, 55], [10, 65], [11, 85], [12, 99]];
            var dataVendors   = [[1, 10], [2, 12], [3, 18], [4, 18], [5, 18], [6, 25], [7, 26], [8, 26], [9, 26], [10, 26], [11, 29], [12,35]];
			var dataFranchise = [[1, 5], [2, 7], [3, 10], [4, 10], [5, 12], [6, 15], [7, 16], [8, 18], [9, 20], [10, 26], [11, 29], [12,35]];

            // Array with month labels used in Classic and Stacked chart
            var chartMonths   = [[1, 'Aug-14'], [2, 'Sep-14'], [3, 'Oct-14'], [4, 'Nov-14'], [5, 'Dec-14'], [6, 'Jan-15'], [7, 'Feb-15'], [8, 'Mar-15'], [9, 'Apr-15'], [10, 'May-15'], [11, 'Jun-15'], [12, 'Jul-15']];

            // Overview Chart
            $.plot(chartOverview,
                [
                    {
                        label: 'Orders',
                        data: dataOrders,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                        points: {show: true, radius: 6}
                    },
                    {
                        label: 'Vendors',
                        data: dataVendors,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                        points: {show: true, radius: 6}
                    }
                ],
                {
                    colors: ['#1bbae1', '#333333'],
                    legend: {show: true, position: 'nw', margin: [10, 10]},
                    grid: {borderWidth: 0, hoverable: true, clickable: true},
                    yaxis: {ticks: 6, tickColor: '#f1f1f1'},
                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                }
            );

            // Creating and attaching a tooltip to the classic chart
            var previousPoint = null, ttlabel = null;
            chartOverview.bind('plothover', function(event, pos, item) {

                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $('#chart-tooltip').remove();
                        var x = item.datapoint[0], y = item.datapoint[1];

                        if (item.seriesIndex === 1) {
                            ttlabel = '<strong>' + y + '</strong> Vendors';
                        } 
						else
						{
						    ttlabel = '<strong>' + y + '</strong> Orders';
						}

                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                            .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                    }
                }
                else {
                    $('#chart-tooltip').remove();
                    previousPoint = null;
                }
            });
			
			// Overview Chart
            $.plot(chartFranchise,
                [
                    {
                        label: 'Franchise',
                        data: dataFranchise,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                        points: {show: true, radius: 6}
                    }
                ],
                {
                    colors: ['#1bbae1', '#333333'],
                    legend: {show: true, position: 'nw', margin: [10, 10]},
                    grid: {borderWidth: 0, hoverable: true, clickable: true},
                    yaxis: {ticks: 5, tickColor: '#f1f1f1'},
                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                }
            );

            // Creating and attaching a tooltip to the classic chart
            var previousPoint = null, ttlabel = null;
            chartFranchise.bind('plothover', function(event, pos, item) {

                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $('#chart-tooltip').remove();
                        var x = item.datapoint[0], y = item.datapoint[1];

                        ttlabel = '<strong>' + y + '</strong> Franchise';
						
                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                            .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                    }
                }
                else {
                    $('#chart-tooltip').remove();
                    previousPoint = null;
                }
            });
		}
    };
}();