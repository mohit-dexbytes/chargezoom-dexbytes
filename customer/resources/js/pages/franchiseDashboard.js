/*
 *  Document   : ecomDashboard.js
 *  Author     : Ecrubit
 *  Description: Custom javascript code used in eCommerce Dashboard page
 */

var FranchiseDashboard = function() {

    return {
        init: function() {
            /*
             * Flot Jquery plugin is used for charts
             *
             * For more examples or getting extra plugins you can check http://www.flotcharts.org/
             * Plugins included in this template: pie, resize, stack, time
             */

            // Get the elements where we will attach the charts
            var chartOverview = $('#chart-overview');

            // Random data for the charts
            var dataOrders    = [[1, 116], [2, 139], [3, 149], [4, 165], [5, 160], [6, 182], [7, 160], [8, 169], [9, 185], [10, 178], [11, 140], [12, 158]];
            var dataFranchise      = [[1, 30], [2, 35], [3, 39], [4, 40], [5, 48], [6, 52], [7, 56], [8, 64], [9, 69], [10, 76], [11, 81], [12,91]];

            // Array with month labels used in Classic and Stacked chart
            var chartMonths     = [[1, 'Aug-14'], [2, 'Sep-14'], [3, 'Oct-14'], [4, 'Nov-14'], [5, 'Dec-14'], [6, 'Jan-15'], [7, 'Feb-15'], [8, 'Mar-15'], [9, 'Apr-15'], [10, 'May-15'], [11, 'Jun-15'], [12, 'Jul-15']];

            // Overview Chart
            $.plot(chartOverview,
                [
                    {
                        label: 'Orders',
                        data: dataOrders,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                        points: {show: true, radius: 6}
                    },
                    {
                        label: 'Vendor',
                        data: dataFranchise,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.15}, {opacity: 0.15}]}},
                        points: {show: true, radius: 6}
                    }
                ],
                {
                    colors: ['#1bbae1', '#333333'],
                    legend: {show: true, position: 'nw', margin: [15, 10]},
                    grid: {borderWidth: 0, hoverable: true, clickable: true},
                    yaxis: {ticks: 3, tickColor: '#f1f1f1'},
                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                }
            );

            // Creating and attaching a tooltip to the classic chart
            var previousPoint = null, ttlabel = null;
            chartOverview.bind('plothover', function(event, pos, item) {

                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $('#chart-tooltip').remove();
                        var x = item.datapoint[0], y = item.datapoint[1];

                        if (item.seriesIndex === 1) {
                            ttlabel = '<strong>' + y + '</strong> Vendor';
                        } else {
                            ttlabel = '<strong>' + y + '</strong> Orders';
                        }

                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                            .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                    }
                }
                else {
                    $('#chart-tooltip').remove();
                    previousPoint = null;
                }
            });
        }
    };
}();