$(document).ready(function(e) {
	$('.view_selected_franchise').click(function(){
		var sel = $('#selected_franchise').val();
		var base_url = $('.logo').attr('id');
		var url = base_url+'franchise/view/'+sel;
		window.location = url;
	});
	$('.edit_selected_franchise').click(function(){
		var sel = $('#selected_franchise').val();
		var base_url = $('.logo').attr('id');
		var url = base_url+'franchise/edit/'+sel;
		window.location = url;
	});
	
	$('.view_selected_vendor').click(function(){
		var sel = $('#selected_vendor').val();
		var base_url = $('.logo').attr('id');
		var url = base_url+'vendor/view/'+sel;
		window.location = url;
	});
	$('.edit_selected_vendor').click(function(){
		var sel = $('#selected_vendor').val();
		var base_url = $('.logo').attr('id');
		var url = base_url+'vendor/edit/'+sel;
		window.location = url;
	});
	
	
	$('#updatepassword').click(function(){
			var base_url = $('.logo').attr('id');
			var conf = confirm('Are you sure to update password');
			var newpassword = $('#password').val();
			var id 			= $('#user_id').val();
			if(newpassword != '')
			{
				if(conf)
				{
					$.ajax({
						type:'POST',
						url: base_url+'franchise/update_password',
						data:{ id : id, password : newpassword},
						success: function(result)
						{
							if(result == 'success'){
								alert('password updated for this franchise.!');
								$('#password').val('');
							}
							else
							alert('Unable to update password');
						}
					});
				}
			} else alert('Password Field Should not be empty.');
	});
	
	
	$('#update_vendorpassword').click(function(){
			var base_url = $('.logo').attr('id');
			var conf = confirm('Are you sure to update password');
			var newpassword = $('#password').val();
			var id 			= $('#user_id').val(); 
			
			if(newpassword != '' )
			{	
				if(conf)
				{
					$.ajax({
						type:'POST',
						url: base_url+'vendor/update_password',
						data:{ id : id, password : newpassword},
						success: function(result)
						{
							if(result == 'success'){
								alert('password updated for this Vendor.!');
								$('#password').val('');
							}
							else
							alert('Unable to update password');
						}
					});
				}
			} else alert('Password Field Should not be empty.');
	});
	


	$(".customer_order_view_form").submit(function(){

		var base_url 	= $('.logo').attr('id');
		var order_id 	= $("#order_id").val();
		var sender_type = 'admin';
		var id = $("#customer_id").val();
		var receiver_type = 'customer';
		var message  	= $("#customer_message").val();
		var message_type = 'message';
		var to_email	= $("#customer_email").val();
		
		$.ajax({
				type:'POST',
				url: base_url+'sales/send_message',
				data:{order_id: order_id, sender_type: sender_type, receiver_id : id, receiver_type: receiver_type, message: message, message_type: message_type, to_email: to_email},
				success: function(result){
					if(result == 'success'){
						$("#customer_message").val('');
					}
				}
			  });
	
	});
	
	
	$(".vendor_order_view_form").submit(function(){

		var base_url 	= $('.logo').attr('id');
		var order_id 	= $("#order_id").val();
		var sender_type = 'admin';
		var id = $("#vendor_id").val();
		var receiver_type = 'vendor';
		var message  	= $("#vendor_message").val();
		var message_type = 'message';
		var to_email	= $("#vendor_email").val();
		
		$.ajax({
				type:'POST',
				url: base_url+'sales/send_message',
				data:{order_id: order_id, sender_type: sender_type, receiver_id : id, receiver_type: receiver_type, message: message, message_type: message_type, to_email: to_email},
				success: function(result){
					if(result == 'success'){
						$("#vendor_message").val('');
					}
				}
			  });
	
	});
	
	
	$(".franchise_order_view_form").submit(function(){

		var base_url 	= $('.logo').attr('id');
		var order_id 	= $("#order_id").val();
		var sender_type = 'admin';
		var id = $("#franchise_id").val();
		var receiver_type = 'franchise';
		var message  	= $("#franchise_message").val();
		var message_type = 'message';
		var to_email	= $("#franchise_email").val();
		
		$.ajax({
				type:'POST',
				url: base_url+'sales/send_message',
				data:{order_id: order_id, sender_type: sender_type, receiver_id : id, receiver_type: receiver_type, message: message, message_type: message_type, to_email: to_email},
				success: function(result){
					if(result == 'success'){
						$("#franchise_message").val('');
					}
				}
			  });
	
	});



});




 



function edit_services(mythis)
{
	$('#edit_id').val($(mythis).data('id'));
	$('#edit_service_name').val($(mythis).data('service_name'));
	$('#edit_description').val($(mythis).data('description'));
	$('#edit_fixed_rate').val($(mythis).data('fixed_rate'));
	$('#edit_minimum_rate').val($(mythis).data('minimum_rate')); 
	$('#edit_parent').val($(mythis).data('parent')); 
	if($(mythis).data('status') == 1)
	{
	$("#edit_status1").prop("checked", true);
	}
	else
	{
	$("#edit_status2").prop("checked", true);
	}
	$('select').trigger("chosen:updated");
}



function delete_services(id, mythis)
{
	var conf = confirm('Are you sure to Delete this service?\nPlease note that all of the data related to this service will be deleted and cannot be recovered.');
	var base_url = $('.logo').attr('id');
	if(conf)
	{
		$.ajax({
			type:'POST',
			url: base_url+'sales/delete_services',
			data:{ id : id},
			success: function(result)
			{
				if(result == 'success')
				$(mythis).parent().parent().parent().fadeOut();
				else
				alert('Unable to Delete this record');
			}
		});
	}
}


function change_access(mythis, id, type)
{ 
	if($(mythis).is(':checked'))
		var status = 1;  // checked
	else
		var status = 0;  // unchecked
	
	var base_url = $('.logo').attr('id');
	$.ajax({
		type:'POST',
		url: base_url+'info/change_status',
		data:{ id : id, type : type , status : status},
		success: function(result)
		{
			
		}
	});	
}

function run_waitme(effect,container)
{
	$(container).waitMe({
		effect: effect,
		text: 'Please wait...',
		bg: 'rgba(255,255,255,0.7)',
		color:'#000',
		sizeW:'',
		sizeH:'',
		source: 'img.svg',
			onClose: function() {}
		});
}
 
