<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Wrong_url extends CI_Controller {
    
	function __construct()
	{
		parent::__construct();
	}
	
	public function index(){
	    
	    $data['primary_nav'] 	= primary_customer_nav();
        	$data['template'] 		= template_variable();
    	$this->load->view('template/template_start', $data);
		$this->load->view('customer/wrong_page', $data);
	   	$this->session->unset_userdata('records');
	}
	
}