<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;

class QBO_cron extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
	}
	
	

	public function index(){
	    
	    $QBO_Token_data = $this->general_model->get_table_data('QBO_token','');
    
	   if(!empty($QBO_Token_data)){
			
			foreach($QBO_Token_data as $QBO_Token)
			{ 
			   
			    
		    	$ID            = $QBO_Token['ID'];
		    	 
		    	$refresh_token = $QBO_Token['refreshToken'];
		    
		    	$access_token  = $QBO_Token['accessToken'];
             $realmID      = $QBO_Token['realmID']; 
            
            if($realmID=="")
            $realmID ="123145932193969";
		    	
		  	$dataService = DataService::Configure(array(
		  'auth_mode' => 'oauth2',
		  'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
         'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		  'accessTokenKey' => $access_token,
		  'refreshTokenKey' =>$refresh_token,
		  'QBORealmID' =>$realmID,
         'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));
		
		
		$dataService->setLogLocation("/Users/hlu2/Desktop/newFolderForLog");
		$OAuth2LoginHelper = $dataService->getOAuth2LoginHelper();
		$accessToken          = $OAuth2LoginHelper->refreshToken();
		
			
		$accessToken = (array)$accessToken;
		$searchkey = 'OAuth2AccessTokenrefresh_token';
		$accesskey = 'OAuth2AccessTokenaccessTokenKey';
		$validation = 'OAuth2AccessTokenaccessTokenValidationPeriod';
		foreach($accessToken as $key=>$data)
		{
		       //Remove null bytes (or) null character.
		     $clearKey = preg_replace('/\\0/', "", str_replace('\\',' ', $key)  ) ; 
		     
		    
		     if( strpos($clearKey, $searchkey)){
		         $searchkey =  $data; 
		       
		     }
		     if( strpos($clearKey, $accesskey)){
		         
		          $accesskey =  $data;
		         
		     }
		     if( strpos($clearKey, $validation)){
		         
		         $validation =  $data;
		       
		     }
			 
			 $QBO_Cron['accessToken'] = $accesskey;
			 $QBO_Cron['refreshToken'] = $searchkey;
			 $QBO_Cron['Validity'] = $validation;
             $QBO_Cron['updatedAT'] = date('Y-m-d H:i:s');
           
		
			 $condition = array('ID'=>$ID);
			 $this->general_model->update_row_data('QBO_token',$condition, $QBO_Cron);
		  } 
		

	     } 
	   }      
	       
	   }
			
}