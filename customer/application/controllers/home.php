<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('customer_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		  $this->db1= $this->load->database('otherdb', TRUE);
		if(!$this->session->userdata('customer_logged_in'))
		{
			redirect('login', 'refresh');
		}
	}
	


	
	public function index()
	{
	   $mID='';
	   
	    $data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
		$data['login_info']     = $this->session->userdata('customer_logged_in');
	    $cusID                  = $data['login_info']['ListID']; 
        	$user_id            = $data['login_info']['merchantID'];
	   
        $data['page_num']      = 'customer_qbd';
    	$data['types']  		  =	 $this->general_model->get_table_data('tbl_teplate_type','');	
     	$condition				  = array('merchantID'=>$user_id);
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($user_id);
	 
	  $data['service'] =  $this->general_model->chk_echeck_gateway_status($user_id);
	
	    $data['check_details'] 		  = $this->customer_model->customer_by_id($cusID, $user_id);  
        if($data['check_details'] == ""){
              $data['customer'] 		  = $this->customer_model->customer_details($cusID);  
        }
        else{
            $data['customer']  = $this->customer_model->customer_by_id($cusID);  
        }
	   
	   $data['invoices'] 		  = $this->customer_model->get_invoice_upcomming_data($cusID,$user_id);
	   $data['latest_invoice']	  = $this->customer_model->get_invoice_latest_data($cusID); 
	
	   $data_invoice	          = $this->customer_model->get_customer_invoice_data_sum($cusID);
	   $data['invoices_count']    = ($data_invoice->incount)?$data_invoice->incount:'0';
	   $data['sum_invoice']       = ($data_invoice->amount)?$data_invoice->amount:'0.00';
	    
	   $paydata					  =	$this->customer_model->get_customer_invoice_data_payment($cusID);	
	   $data['pay_invoice']       = ($paydata->applied_amount)?:'0.00';
       $data['pay_upcoming']      = ($paydata->upcoming_balance)?$paydata->upcoming_balance:'0.00';	 
       $data['pay_remaining']     = ($paydata->remaining_amount)?$paydata->remaining_amount :'0.00';	
       $data['pay_due_amount']  = ($paydata->applied_due)?$paydata->applied_due :'0.00';
       $data['card_data_array']     = $this->card_model->get_card_expiry_data($cusID,$user_id );
     	   

     	 
          $sub  = array('customerID'=>$cusID,'sbs.merchantDataID'=>$user_id);
     	  $data['getsubscriptions']   = $this->general_model->get_subscriptions_data($sub);
     	  
    
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
		
		$this->load->view('customer/page_customer_details', $data);
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	
	public function get_gateway_data(){
	
		
			$gatewayID  = $this->czsecurity->xssCleanPostInput('gatewayID');
		    $condition     = array('gatewayID'=>$gatewayID);
			 
			 $res  = $this->general_model->get_row_data('tbl_merchant_gateway',$condition);
		
           if(!empty($res)){
          
			$res['status'] ='true';
			  echo json_encode($res);
		   }
			   
	    die;
	
	
	}

	
	public function invoices()
	{
	  
		$data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
		
		
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['ListID'];
		$mID               = $data['login_info']['merchantID']; 
		$today 				    = date('Y-m-d');

	
		$invoices    			 = $this->customer_model->get_customer_invoice_data($user_id, $mID);
		
	
		$data['gateway_datas']		  = $this->general_model->get_gateway_data($mID);
	
			$data['invoices']    = $invoices; 
		$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
		$this->load->view('customer/page_invoices', $data);
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	public function invoice_details()
	{
	    $invoiceID               =$this->uri->segment(3);  
		$condition1 			= array('item_ListID'=>$invoiceID);
		$data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['ListID'];
		
		$condition2 			= array('TxnID'=>$invoiceID);
		$invoice_data           = $this->general_model->get_row_data('qb_test_invoice',$condition2);
		
	   
	 
	 
		$condition3 			= array('ListID'=>$invoice_data['Customer_ListID'] );
		$customer_data			= $this->general_model->get_row_data('qb_test_customer',$condition3);	
    
		$data['notes']   		= $this->customer_model->get_customer_note_data($customer_data['ListID']);
		$data['customer_data']  = $customer_data ;
		$data['invoice_data']   = $invoice_data ;
	 
	     $data['invoice_items']   = $this->company_model->get_invoice_item_data( $invoiceID);
	     
	     $data['gateway_datas'] = $this->customer_model->get_merchant_gateway($user_id);
	   
		$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
			
	
	
	    $this->load->view('customer/page_invoice_details', $data);
	
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	
	public function invoice_details_print()
	{
		 	 $invoiceID             =  $this->uri->segment(3);  
	  
		$data['template'] 		= template_variable();	
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['ListID'];
		
		$condition2 			= array('TxnID'=>$invoiceID);
		$invoice_data           = $this->general_model->get_row_data('qb_test_invoice',$condition2);
		
	
		$condition3 			= array('ListID'=>$invoice_data['Customer_ListID'] );
		
		
		$customer_data			= $this->general_model->get_row_data('qb_test_customer',$condition3);	
		$condition4 			= array('id'=>$customer_data['companyID'] );
		$company_data			= $this->general_model->get_row_data('tbl_company',$condition4);	
		
		 $data['notes']   		  = $this->customer_model->get_customer_note_data($invoice_data['ListID']);
		$data['customer_data']  = $customer_data ;
		$data['company_data']    = $company_data;
		$data['invoice_data']   = $invoice_data ;
	
	    $data['invoice_items']   = $this->company_model->get_invoice_item_data( $invoiceID);
	
	
		
	     	$no = $invoiceID;
			$pdfFilePath = "$no.pdf"; 
			
		
			 ini_set('memory_limit','32M'); 
			
			$html = $this->load->view('customer/page_invoice_details_print', $data, true);
			
			 $this->load->library('pdf');
			 $pdf = $this->pdf->load();
			 $pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($pdfFilePath, 'D'); // save to file because we can
	
		

	}
	
	
	
	
		
	public function changePass()
    {
		
		
		$session_data = $this->session->userdata('customer_logged_in');
		$id = $session_data['loginID'];
	   $this->czsecurity->xssCleanPostInput('user-settings-password') ; 
		$query = $this->customer_login_model->savenewpass($id, $this->czsecurity->xssCleanPostInput('user-settings-password') );			
		$message = '<div class="alert alert-success"><i class="fa fa-check"></i><strong>Success</strong> Password Successfully Updated</div>' ;

		$this->session->set_flashdata('message', $message);
		redirect('customer/home', 'refresh');
	}
	 
	
	
	
		
	
	  
	  
  public function get_card_expiry_data($customerID, $merchantID){  
  
                       $card = array();
               		   $this->load->library('encrypt');
           
	   	        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'   "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
                 
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $customer_card['CardNo']  = substr($this->encrypt->decode($card_data['CustomerCard']),12) ;
						 $customer_card['CardID'] = $card_data['CardID'] ;
						 $customer_card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 $card[$key] = $customer_card;
					   }
				}		
					
                return  $card;

     }
 

	
	
	
		

        	   
	 public function update_customer_invoice_cancel(){
	     
        
    		$today 				    = date('Y-m-d');
    	 
    		$txnID           =  $this->czsecurity->xssCleanPostInput('invoiceID'); 
    		$condition		 = array("DATE_FORMAT(DueDate,'%Y-%m-%d') > "=>$today,"IsPaid"=>"false", 'TxnID'=>$txnID);
    	    $invData         = $this->general_model->get_row_data('qb_test_invoice',$condition);
    	   
    	      if(!empty($invData)){
    	        $updateData  =array();
    	        $updateData['userStatus'] = 'cancel';
    	      
    	         if($this->general_model->update_row_data('qb_test_invoice',$condition,$updateData)){
			     
			        $this->session->set_flashdata('message','<div class="alert alert-success">  <strong>Success!</strong>successful or positive action.</div>'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error!</strong>Error in process.</div>'); 
				 }
				
    	        
    	        
    	    }
    		
          redirect('home/invoices', 'refresh');

    	}	 
		

  	

	 	   
	 public function update_invoice_cancel(){
	     
        
    		$today 				    = date('Y-m-d');
    		
    		$txnID           =  $this->uri->segment(3); 
    		$condition		 = array("DATE_FORMAT(DueDate,'%Y-%m-%d') > "=>$today,"IsPaid"=>"false", 'TxnID'=>$txnID);
    	    $invData         = $this->general_model->get_row_data('qb_test_invoice',$condition);
    	  
    	    if(!empty($invData)){
    	        $updateData  =array();
    	        $updateData['userStatus'] = 'cancel';
    	      
    	         if($this->general_model->update_row_data('qb_test_invoice',$condition,$updateData)){
			     
			        $this->session->set_flashdata('message','<div class="alert alert-success">  <strong>Success!</strong>successful or positive action.</div>'); 
				 }else{
					 
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error!</strong>Error in process.</div>'); 
				 }
				
    	        
    	        
    	    }
    		
          redirect('home/invoices', 'refresh');

    	}	 
		
		
	
	
	public function add_note(){
	     
	
	      if(!empty($this->czsecurity->xssCleanPostInput('customerID'))){
		
			$cusID = $this->czsecurity->xssCleanPostInput('customerID');
			if($this->czsecurity->xssCleanPostInput('private_note') !=""){
				
			  $private_note  = $this->czsecurity->xssCleanPostInput('private_note'); 
			  $data_ar = array('privateNote'=>$private_note, 'privateNoteDate'=>date('Y-m-d H:i:s'), 'customerID'=>$cusID);
			
			  $id   = $this->general_model->insert_row('tbl_private_note', $data_ar);
				if($id >0){
					 array('status'=>"success");
					 echo json_encode(array('status'=>"success"));
				  die;
					
				}else{
					 array('status'=>"error");
					 echo json_encode(array('status'=>"success"));
					  die;
					} 
		}	
	
	}
	
	}
	
	
	
}
