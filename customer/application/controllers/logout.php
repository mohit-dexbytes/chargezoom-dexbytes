<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
	        $this->load->model('customer_login_model');
	}


	public function index()

	{  
	  
		$data['title'] = 'Logout';
		$status = $this->customer_login_model->customer_logout();
        

		 redirect('login', 'refresh'); 
	}

}



