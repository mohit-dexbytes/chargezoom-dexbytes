<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();
class Login extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->library('form_validation'); 
		$this->load->model('general_model');
		$this->load->model('customer_login_model');
		
	

	
		if($this->session->userdata('customer_logged_in'))
		{
		            $sess_array =  $this->session->userdata('customer_logged_in');
					  if($sess_array['packageType']=='2')
						redirect(base_url('home/index'));
						if($sess_array['packageType']=='1')
						redirect(base_url('QBO_controllers/home/index'));
						if($sess_array['packageType']=='3')
						redirect(base_url('FreshBooks_controllers/home/index'));
		}
   
	}
	
	public function index()
	{
	  
           
	  
	  	 $port_url = current_url();
	  
		 $new_url   = explode('://',$port_url);
		 $logo_data = explode('.',$new_url[1]);
		
		 $l_con    = array('portalprefix'=>$logo_data[0]);
	     $l_data = $this->customer_login_model->get_domain_logo($l_con);
	     
	    
	  
		if(!empty($l_data )){
		    
		    $data['logo']    = ($l_data['ProfileImage'])?$l_data['ProfileImage']:'';
		    $data['m_name'] = $l_data['companyName'];
		    $data['customerHelpText'] = $l_data['customerHelpText'];
		     $data['ownerID'] = $l_data['merchantID'];
		}else{
		    
		     redirect(base_url('not_found'));
		}

	    
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
	
		$this->load->view('template/template_start', $data);
		$this->load->view('customer/login', $data);
		$this->load->view('template/template_scripts', $data);
		$this->load->view('template/template_end', $data);
	}


  

   function user_login()
  {   
  
      $pacakgeType ='';
        $email     = $this->czsecurity->xssCleanPostInput('login-email');
		$password  =  md5($this->czsecurity->xssCleanPostInput('login-password'));
		$mID       =   $this->czsecurity->xssCleanPostInput('testID');

				$con       = array('customerEmail'=>$email, 'customerPassword'=>$password,'cl.merchantID'=>$mID);
				$user = $this->customer_login_model->get_customer_login_details($con); 
        
				if ($user && isset($user['customerEmail']))
				{  
					 
						$sess_array = $user;
						$this->session->set_userdata('customer_logged_in', $sess_array);
					  if($sess_array['packageType']=='2')
						redirect(base_url('home/index'));
						if($sess_array['packageType']=='1')
						redirect(base_url('QBO_controllers/home/index'));
						if($sess_array['packageType']=='3')
						redirect(base_url('FreshBooks_controllers/home/index'));
							if($sess_array['packageType']=='4')
						redirect(base_url('Xero_controllers/home/index'));
								if($sess_array['packageType']=='5')
						redirect(base_url('company/home/index'));
						
						
				}
				else{
				
				
					 $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> In-Correct email or password</div>');
					
				}		 
	
		 redirect(base_url('/'));
	} 
	
	
	
	function enable_customer(){
		
	 $code = $this->uri->segment('3');
	  if($code!=""){
	
		  $res = $this->customer_login_model->get_customer_login_details(array('loginCode'=> $code, 'isEnable'=>'0' )) ;	
	
		  if(!empty($res)){
			  
			  $ins_data = array('loginCode'=>'', 'isEnable'=>'1');
			  
			  $this->general_model->update_row_data('tbl_customer_login',array('loginCode'=> $code, 'isEnable'=>'0' ), $ins_data);
			  
			  redirect(base_url('login#login'), 'refresh');
		  }else{
		       $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> You have enabled you login email.</div>');
		        redirect(base_url('login#login'), 'refresh');
		  }
			
	  }	
	  
	}	
	
	function user_register() {

    	$email     = $this->czsecurity->xssCleanPostInput('register-email');
        $condition = array('cust.Contact'=>$email);
		
		if($this->customer_login_model->check_domain($condition) > 0)
        {
    		$results = $this->general_model->get_row_data('qb_test_customer', array('Contact'=> $this->czsecurity->xssCleanPostInput('register-email') ) );
    		
    		
    		if($results)
    		{
		            
		          
      		      $code =substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
	              
			        $email   = $this->czsecurity->xssCleanPostInput('register-email');
					$password1 = "password123";
		            $password = md5("password123");
					$datas   = array(
					'customerEmail'   =>$this->czsecurity->xssCleanPostInput('register-email'),
					'customerPassword'=>$password,
					'loginCode'       => $code,
                     'isEnable'   	  => '0',				 
					'createdAt'       => date('Y-m-d H:i:s')
				   );
			
			$chk_dup = $this->general_model->check_existing_user('tbl_customer_login', array('customerEmail'=> $email));

			if(!$chk_dup){
			    
			    
			    
	         $insert = $this->general_model->insert_row('tbl_customer_login', $datas);
			 $this->session->set_flashdata('message', 'Login details has send to your email. Please check it.!');
        		$this->load->library('email');
				
				$subject = 'Chargezoom - Customer Login Details';
				$this->email->from('support@chargezoom.com');
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->set_mailtype("html");
              $fnane = $results['FullName'];
			
			$message = "<p>Dear $fnane,<br><br>Your Login Details is given following . Use this login detail for login </p><br><p> Email : ".$email."</p><br><br> <p> Password : ".$password1."</p><br> <p>Please click on following link to enable your login <a href=".base_url('login/enable_customer/'.$code).">Click</a> </p><br><br> Thanks,<br>Chargezoom Support Team, <br>www.chargezoom.com";	
				$this->email->message($message);
			
				if($this->email->send())
				{	
					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success: </strong>Login details has send to your email. Please check it.</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email was not sent, please contact your administrator...'.$email.'</div>');
				}

    				
    			}else{
    				$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success:</strong> Your login details was created, please check your mail or go to forgot password.</div>');
    			}				
    				
    				redirect(base_url('login#login'), 'refresh');
    		}else{
    		  $message = "This is not exist for any customer";	
    		}
        }else{
			 $message = "Your customer portal is not enabled!";	
		}			
	
		$this->session->set_flashdata('message', $message);
		
		redirect(base_url('login#register'), 'refresh');
	}
	
	
	/****************Recover Password****************/
	
	
	
	public function recover_password() 
	{
		$email  = $this->czsecurity->xssCleanPostInput('reminder-email');
		
		$result = $this->customer_login_model->get_customer_login_details(array('customerEmail'=> $email ) );
		
		if($result)
		{
			$this->load->helper('string');
			$code = random_string('alnum',10);
            
			$update = $this->customer_login_model->temp_reset_customer_password($code,$email);
			if($update)
			{
				$this->load->library('email');
				
				$subject = 'Chargezoom - Reset Password';
				$this->email->from('support@chargezoom.com');
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->set_mailtype("html");

				$message = "<p>Hello,<br><br>Your new password has been updated successfully. Please use the new password: ".$code."</p><br><br> Thanks,<br>Chargezoom Support Team, <br>www.chargezoom.com";
				
				$this->email->message($message);
			
				if($this->email->send())
				{	
					$this->session->set_flashdata('message', '<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong>New password has been sent to your email. Please check it.</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>>Email was not sent, please contact your administrator...'.$code.'</div>');
				}
				
			}
			
			redirect('login#login', 'refresh');
		}
		else
		{
		   $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email not available.</div>'); 
		   redirect('login#reminder', 'refresh');
		}
	
	}
	

	
	 
	 
}
 
 
 