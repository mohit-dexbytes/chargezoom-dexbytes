<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General_controller extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
	}

	function get_states(){
		$country_id = $this->czsecurity->xssCleanPostInput('id');
		$data = array("country_id" => $country_id);
		$state = $this->general_model->get_table_data('state',$data);
		if($state != ""){
			echo json_encode($state);
		}
	}

	function get_city(){
		$state_id = $this->czsecurity->xssCleanPostInput('id');
		$data = array("state_id" => $state_id);
		$city = $this->general_model->get_table_data('city',$data);
		if($city != ""){
			echo json_encode($city);
		}
	}

	function get_process_trans(){
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$today = date('Y-m-d');
			$today_trans =$this->general_model->get_process_trans_data($user_id);
			if($today_trans != false){
				echo json_encode($today_trans);
			}
			else{
				echo 2;
			}
	}

	function get_scheduled_incoice(){

			$today = date('Y-m-d');
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$condition2 			= array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') >= "=>$today,"IsPaid"=>"false", 'userStatus'=>'', "merchantID"=>$user_id);

			$sch_data =$this->general_model->get_modal_invoice_data($condition2);
			if($sch_data != false){
				echo json_encode($sch_data);
			}else{
				echo 2;
			}

	}
	function get_failed_incoice(){

			$today = date('Y-m-d');
			$data['login_info'] 	= $this->session->userdata('logged_in');
			$user_id 				= $data['login_info']['merchID'];
			$condition3 			 = array("DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < "=>$today,"IsPaid"=>"false", 'userStatus'=>'', "cmp.merchantID"=>$user_id);
			$sch_data =$this->general_model->get_modal_invoice_data($condition3);
			if($sch_data != false){
				echo json_encode($sch_data);
			}else{
				echo 2;
			}

	}
	
	function set_offline_amount(){
	    $tnID = $this->czsecurity->xssCleanPostInput('tid');
	    	$data = array("TxnID " => $tnID);
	    $bal_amnt = $this->general_model->get_invoice_bal_data('qb_test_invoice',$data);
	    if($bal_amnt != ""){
			echo json_encode($bal_amnt);
		}
	}

	public function check_card_surcharge(){
		$status = $surcharge = 0;
		$message = 'Invalid Request';
		$cardId = $this->czsecurity->xssCleanPostInput('cardId');
		$cardNumber = $this->czsecurity->xssCleanPostInput('cardNumber');

		if($cardId > 0){
			$message = 'Surcharge Value';
		} elseif($cardNumber > 0){
			$message = 'Surcharge Value';
			$surcharge = $this->general_model->chkCardSurcharge(['cardNumber' => $cardNumber]);
		}
		echo json_encode(array('success' => $status, 'message' => $message, 'data' => $surcharge)); die;
	}
	public function transactionReceipt($transaction_id)
	{
		$pdf_file_name = 'Transaction-Receipt-'.$transaction_id.'.pdf';
		$path = FCPATH.'../uploads/transaction_pdf/'.$pdf_file_name;
		header("Content-type: application/pdf");
		header("Content-Disposition: inline; filename=".$pdf_file_name);
		@readfile($path);
	}
	public function printInvoiceTransactionReceiptPDF($appIntegration)
	{
		$transaction_id = $this->session->userdata('tranID');
		$invoice_data = $this->session->userdata('sess_invoice_id');
		
		if($invoice_data){
			foreach($invoice_data as $key => $rcData){
				$invoice_data[$key] = strip_tags($rcData);
			}

			$user_id = $invoice_data['merchantID'];
			$con = array('transactionID' => $transaction_id);
	        $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
	        $transactionAmount = ($pay_amount) ? $pay_amount['transactionAmount'] : '0.00';
	        $transactionDetail = $pay_amount;
	        
			if($appIntegration == 1){
				$val = array(
	                'merchantID' => $user_id,
	            );
	    
	            $data1 = $this->general_model->get_row_data('QBO_token', $val);
	    
	            $accessToken = $data1['accessToken'];
	            $refreshToken = $data1['refreshToken'];
	            $realmID      = $data1['realmID'];
				$invoice_number = $invoice_data['refNumber'];
	          
	            $condition3 			= array('Customer_ListID' => $invoice_data['CustomerListID'],'merchantID' => $user_id, 'companyID' => $realmID);
	            
	            $customer_data	= $this->general_model->get_row_data('QBO_custom_customer', $condition3);

				$address_data = array(
					'billing_name' => ($customer_data['firstName']) ? $customer_data['firstName'].' '.$customer_data['lastName'] : $customer_data['fullName'],
					'FullName' => $customer_data['fullName'],
					'billing_address1' => $invoice_data['BillAddress_Addr1'],
					'billing_address2' => $invoice_data['BillAddress_Addr2'],
					'billing_city' => $invoice_data['BillAddress_City'],
					'billing_zip' => $invoice_data['BillAddress_PostalCode'],
					'billing_state' => $invoice_data['BillAddress_State'],
					'billing_country' => $invoice_data['BillAddress_Country'],
					'shipping_name' => ($customer_data['firstName']) ? $customer_data['firstName'].' '.$customer_data['lastName'] : $customer_data['fullName'],
					'shipping_address1' => $invoice_data['ShipAddress_Addr1'],
					'shipping_address2' => $invoice_data['ShipAddress_Addr2'],
					'shipping_city' => $invoice_data['ShipAddress_City'],
					'shipping_zip' => $invoice_data['ShipAddress_PostalCode'],
					'shipping_state' => $invoice_data['ShipAddress_State'],
					'shiping_counry' => $invoice_data['ShipAddress_Country'],
					'Phone' => $customer_data['phoneNumber'],
					'Contact' => $customer_data['userEmail'],
				);
			}else if($appIntegration == 2){
				$invoice_number = $invoice_data['RefNumber'];
				$condition3 			= array('ListID' => $invoice_data['Customer_ListID']);
				$customer_data			= $this->general_model->get_row_data('qb_test_customer', $condition3);

				$address_data = array(
					'billing_name' => ($customer_data['FirstName']) ? $customer_data['FirstName'].' '.$customer_data['LastName'] : $customer_data['FullName'],
					'FullName' => $customer_data['FullName'],
					'billing_address1' => $invoice_data['BillingAddress_Addr1'],
					'billing_address2' => $invoice_data['BillingAddress_Addr2'],
					'billing_city' => $invoice_data['BillingAddress_City'],
					'billing_zip' => $invoice_data['BillingAddress_PostalCode'],
					'billing_state' => $invoice_data['BillingAddress_State'],
					'billing_country' => $invoice_data['BillingAddress_Country'],
					'shipping_name' => ($customer_data['FirstName']) ? $customer_data['FirstName'].' '.$customer_data['LastName'] : $customer_data['FullName'],
					'shipping_address1' => $invoice_data['ShipAddress_Addr1'],
					'shipping_address2' => $invoice_data['ShipAddress_Addr2'],
					'shipping_city' => $invoice_data['ShipAddress_City'],
					'shipping_zip' => $invoice_data['ShipAddress_PostalCode'],
					'shipping_state' => $invoice_data['ShipAddress_State'],
					'shiping_counry' => $invoice_data['ShipAddress_Country'],
					'Phone' => $customer_data['Phone'],
					'Contact' => $customer_data['Contact'],
				);
			}else if($appIntegration == 5){

				$invoice_number = $invoice_data['RefNumber'];

				$condition3 			= array('ListID' => $invoice_data['Customer_ListID']);
				$customer_data			= $this->general_model->get_row_data('chargezoom_test_customer', $condition3);

				$address_data = array(
					'billing_name' => ($customer_data['FirstName']) ? $customer_data['FirstName'].' '.$customer_data['LastName'] : $customer_data['FullName'],
					'FullName' => $customer_data['FullName'],
					'billing_address1' => $invoice_data['BillingAddress_Addr1'],
					'billing_address2' => $invoice_data['BillingAddress_Addr2'],
					'billing_city' => $invoice_data['BillingAddress_City'],
					'billing_zip' => $invoice_data['BillingAddress_PostalCode'],
					'billing_state' => $invoice_data['BillingAddress_State'],
					'billing_country' => $invoice_data['BillingAddress_Country'],
					'shipping_name' => ($customer_data['FirstName']) ? $customer_data['FirstName'].' '.$customer_data['LastName'] : $customer_data['FullName'],
					'shipping_address1' => $invoice_data['ShipAddress_Addr1'],
					'shipping_address2' => $invoice_data['ShipAddress_Addr2'],
					'shipping_city' => $invoice_data['ShipAddress_City'],
					'shipping_zip' => $invoice_data['ShipAddress_PostalCode'],
					'shipping_state' => $invoice_data['ShipAddress_State'],
					'shiping_counry' => $invoice_data['ShipAddress_Country'],
					'Phone' => $customer_data['Phone'],
					'Contact' => $customer_data['Contact'],
				);
			}

			ini_set('memory_limit','320M'); 
		  	$this->load->library("TPdf");
			/* Check merchant logo is available if yes than display otherwise display default chargezoom */
			$config_data = $this->general_model->get_select_data('tbl_config_setting', array('ProfileImage'), array('merchantID'=> $user_id ));
			if($config_data['ProfileImage']!=""){
				$logo = LOGOURL.$config_data['ProfileImage']; 
			}else{
				$logo  = CZLOGO;
			}

			$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);   
			if(!defined('PDF_HEADER_TITLE')){
				define(PDF_HEADER_TITLE,'Invoice');
			} 

			if(!defined('PDF_HEADER_STRING')){
				define(PDF_HEADER_STRING,'');
			} 
		    $pdf->SetPrintHeader(False);
		    $pdf->SetCreator(PDF_CREATOR);
		    $pdf->SetAuthor('Chargezoom 1.0');
		    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		    $pdf->SetFont('dejavusans', '', 10, '', true);   
		    $pdf->setCellPaddings(1, 1, 1, 1);
			$pdf->setCellMargins(1, 1, 1, 1);
	    	$pdf->AddPage();
	  		
	  		$y = 20;
			$logo_div = '<div style="text-align:left; float:left ">
			<img src="'.$logo.'" border="0" height="50"/>
			</div>';
			
			$path = FCPATH.'../uploads/transaction_pdf/';
			$pdf_file_name = 'Transaction-Receipt-'.$transaction_id.'.pdf';
	        $pdfFilePath = $path.$pdf_file_name;

	        if (!file_exists($path)) {
			    mkdir($path, 0777, true);
			}

			// set color for background
			$pdf->SetFillColor(255, 255, 255);
			// write the first column
			$pdf->writeHTMLCell(80, 50, 12, $y, $logo_div, 0, 0, 1, true, 'J', true);


			$y = 50;

			// set color for background
			$pdf->SetFillColor(255, 255, 255);

			// set color for text
			$pdf->SetTextColor(51, 51, 51);
			$merchant_default_timezone = '';
			if($user_id){
                // get merchant selected timezone
                $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $user_id));
                if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                    $merchant_default_timezone = $m_data['merchant_default_timezone'];
                }
            }

	        $date = (isset($transactionDetail['transactionDate']) && !empty($transactionDetail['transactionDate'])) ? $transactionDetail['transactionDate'] : date("Y-m-d H:i:s");
	        if(!empty($merchant_default_timezone)){
	            $timezone = ['time' => $date, 'current_format' => 'UTC', 'new_format' => $merchant_default_timezone];
	            $date = getTimeBySelectedTimezone($timezone);
	        }
	        $custom_data = (isset($transactionDetail) && isset($transactionDetail['custom_data_fields'])) ? $transactionDetail['custom_data_fields'] : false;
            $payment_type = '';
            if($custom_data){
                $json_data = json_decode($custom_data, 1);
                
                if(isset($json_data['payment_type'])){
                    $payment_type = $json_data['payment_type'];
                }
            }
			$html = '';
			$html .= '<h2>Transaction Receipt</h2></br></br></br></br>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', $y, $html, 0, 0, 0, true, 'J', true);
			$html = '';
			$html .= '<b style="font-size: 14px;">Amount: </b><span style="font-size: 12px;">'.priceFormat($transactionAmount, true).'</span><br/>';
			$pdf->writeHTMLCell(80, 50, '', 60, $html, 0, 0, 0, true, 'J', true);
			$html = '';
			$html .= '<br><b>Date: </b><span>'.date("m/d/Y h:i A", strtotime($date)).'</span><br/><br/>';
			$html .= '<b>IP Address: </b><span>'.getClientIpAddr().'</span><br/><br/>';
			$html .= '<b>Payment Type: </b><span>'.$payment_type.'</span><br/><br/>';
			// write the first column
			$pdf->writeHTMLCell(80, 50, '', 70, $html, 0, 0, 0, true, 'J', true);

			$html = ' ';
			$html .= '<br/>';
			$html .= '<b>Transaction ID: </b><span>'.$transaction_id.'</span><br/><br/>';
			$html .= '<b>Invoice(s): </b><span>'.$invoice_number.'</span><br/><br/>';

			// write the first column
			$pdf->writeHTMLCell(80, 50, '', '', $html, 0, 1, 1, true, 'J', true);

	        $billingAdd = 0;
            $shippingAdd = 0;

            $isAdd = 0;
            
            if($address_data['billing_address1']  || $address_data['billing_address2'] || $address_data['billing_city'] || $address_data['billing_state'] || $address_data['billing_zip'] || $address_data['billing_country']){
                $billingAdd = 1;
                $isAdd = 1;
            }

            if($address_data['shipping_address1']  || $address_data['shipping_address2'] || $address_data['shipping_city'] || $address_data['shipping_state'] || $address_data['shipping_zip'] || $address_data['shiping_counry'] ){
                $shippingAdd = 1;
                $isAdd = 1;
            }
            
	        if($isAdd){
	        	if($billingAdd){
	        		$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Billing Address</th>
				    	</tr>';

				   	$address_html = '<br><br><span>  '.$address_data['billing_name'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$address_data['FullName'].'</span><br/><br/><span>  </span>';
				   
				   	if ($address_data['billing_address1'] != '') {
	                	$address_html .= $address_data['billing_address1'].'<br><span>  </span>'; 
	                }

	                if ($address_data['billing_address2'] != '') {
	                	$address_html .= $address_data['billing_address2'].'<br><span>  </span>'; 
	                }

	                $address_html .= ''.($address_data['billing_city']) ? $address_data['billing_city'] . ', ' : '';

	                    $address_html .= ($address_data['billing_state']) ? $address_data['billing_state'].' ' : '';
	                    $address_html .= ($address_data['billing_zip']) ? $address_data['billing_zip'].'<br><span>  </span>' : ''; 
	                    $address_html .= ($address_data['billing_country']) ? $address_data['billing_country'].'<br><span>  </span>' : ''; 

					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
	        		
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 0, 1, true, 'J', true);
				}

				if($shippingAdd){
	        		
				   	$html = '<table style="border: 1px solid #e8e8e8;" cellpadding="4"  >
				    	<tr style="border: 1px solid #e8e8e8;background-color:#f9f9f9;">
				        	<th style="border: 1px solid #f9f9f9;color: #333333;" align="left"> Shipping Address</th>
				    	</tr>';

				    $address_html = '<br><br><span>  '.$address_data['shipping_name'].'</span><br/><span>  </span>';
				   	$address_html .= '<span>'.$address_data['FullName'].'</span><br/><br/><span>  </span>';

				   	if ($address_data['shipping_address1'] != '') {
	                	$address_html .= $address_data['shipping_address1'].'<br><span>  </span>'; 
	                }

	                if ($address_data['shipping_address2'] != '') {
	                	$address_html .= $address_data['shipping_address2'].'<br><span>  </span>';
	                }

	                $address_html .= ''.($address_data['shipping_city']) ? $address_data['shipping_city'] . ', ' : '';

	                    $address_html .= ($address_data['shipping_state']) ? $address_data['shipping_state'].' ' : '';
	                    $address_html .= ($address_data['shipping_zip']) ? $address_data['shipping_zip'].'<br><span>  </span>' : ''; 
	                    $address_html .= ($address_data['shiping_counry']) ? $address_data['shiping_counry'].'<br><span>  </span>' : ''; 
	                
					$html .=  '<tr style="border: 1px solid #e8e8e8;"><td  style="border: 1px solid #e8e8e8;" colspan="2">'.$address_html .'</td></tr>';

				   	$html .= '</table>';
				   	$pdf->writeHTMLCell(80, 70, '', '', $html, 0, 1, 1, true, 'J', true);
				}
			}

			$pdf->Output($pdfFilePath, 'F');
			redirect('General_controller/transactionReceipt/'.$transaction_id);
		}else{
			if ($appIntegration == '1') {
				redirect('QBO_controllers/Thankyou');
			}else if ($appIntegration == '2') {
				redirect('Thankyou');
			}else{
				redirect('company/Thankyou');
			}
		}
	}

}