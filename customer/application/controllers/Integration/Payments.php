<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/integration/XeroSync.php';
include_once APPPATH . 'libraries/Manage_payments.php';

class Payments extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Common/customer_model');
        $this->load->model('Common/data_model');
        $this->load->library('form_validation');

        $this->load->model('general_model');
        $this->load->model('card_model');
        $this->db1 = $this->load->database('otherdb', true);
    }

    public function pay_invoice()
    {
        $user_id    = $this->czsecurity->xssCleanPostInput('mid');
        $invoice_no = $invoiceID = $this->czsecurity->xssCleanPostInput('invid');

        $rs_Data    = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'merchant_default_timezone'), array('merchID' => $user_id));
        $resellerID = $rs_Data['resellerID'];

        $condition2 = array('merchantID' => $user_id, 'invoiceID' => $invoiceID);
        $in_data    = $this->general_model->get_row_data('Xero_test_invoice', $condition2);

        $sch_method = $this->czsecurity->xssCleanPostInput('sch_method');

        if ($this->czsecurity->xssCleanPostInput('setMail')) {
            $chh_mail = 1;
        } else {
            $chh_mail = 0;
        }

        $accountData = $this->general_model->get_row_data('tbl_xero_accounts', ['merchantID' => $user_id, 'isPaymentAccepted' => 1]);

        $responseId = '';
        $thankyou   = $_SERVER['HTTP_REFERER'];
        if (!empty($in_data) && !empty($accountData)) {

            $con        = array('merchantID' => $user_id, 'set_as_default' => 1);
            $gt_result  = $this->general_model->get_row_data('tbl_merchant_gateway', $con);
            $gateway_id = $gt_result['gatewayID'];
            if ($gt_result['gatewayType'] == 5) {
                $data['userName'] = $gt_result['gatewayUsername'];
            } else {
                $data['userName'] = '';
            }

            $payamount = $in_data['BalanceRemaining'];
            $payamount = round($payamount, 2);

            $customerID   = $in_data['CustomerListID'];
            $customerData = $this->general_model->get_select_data('Xero_custom_customer', array('*'), array('Customer_ListID' => $customerID, 'merchantID' => $user_id));

            $CustomerListID = $in_data['CustomerListID'];

            $cardID_upd = '';
            $scheduleID = $this->czsecurity->xssCleanPostInput('scheduleID');

            $paymentObj = new Manage_payments($user_id);
            if ($scheduleID == 2) {
                $eCheckStatus     = '1';
                $accountName      = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountName'));
                $accountNumber    = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountNumber'));
                $routeNumber      = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('routeNumber'));
                $acct_holder_type = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_holder_type'));
                $acct_type        = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_type'));
                $sec_code         = 'WEB';

                $accountName      = ($accountName != '') ? $accountName : $fname . ' ' . $lname;
                $acct_holder_type = ($acct_holder_type != '') ? $acct_holder_type : 'business';
                $acct_type        = ($acct_type != '') ? $acct_type : 'checking';

                //card data array
                //save card process
                $card_data = array(
                    'accountName'        => $accountName,
                    'accountNumber'      => $accountNumber,
                    'routeNumber'        => $routeNumber,
                    'accountHolderType'  => $acct_holder_type,
                    'accountType'        => $acct_type,
                    'secCodeEntryMethod' => $sec_code,
                    'customerListID'     => $customerID,
                    'companyID'          => $companyID,
                    'merchantID'         => $user_id,
                    'createdAt'          => date("Y-m-d H:i:s"),
                    'Billing_Addr1'      => $address,
                    'Billing_Addr2'      => $address2,
                    'Billing_City'       => $city,
                    'Billing_State'      => $state,
                    'Billing_Country'    => $country,
                    'Billing_Contact'    => $phone,
                    'Billing_Zipcode'    => $zip,
                    'CardType'           => 'Echeck',
                );

            } else {
                $eCheckStatus = '0';
                $cnumber      = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
                $expmonth     = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
                $expyear      = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
                $cvv          = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
                $card_no      = $cnumber;

                $exyear   = $expyear;
                $cardtype = $this->general_model->getType($cnumber);

                $card_data = array(
                    'cardMonth'       => $expmonth,
                    'cardYear'        => $expyear,
                    'CardType'        => $cardtype,
                    'CustomerCard'    => $cnumber,
                    'CardNo'          => $cnumber,
                    'CardCVV'         => $cvv,
                    'customerListID'  => $customerID,
                    'companyID'       => $companyID,
                    'merchantID'      => $user_id,
                    'createdAt'       => date("Y-m-d H:i:s"),
                    'Billing_Addr1'   => $address,
                    'Billing_Addr2'   => $address2,
                    'Billing_City'    => $city,
                    'Billing_State'   => $state,
                    'Billing_Country' => $country,
                    'Billing_Contact' => $phone,
                    'Billing_Zipcode' => $zip,

                );
            }

            $saleData = [
                'paymentDetails'    => $card_data,
                'transactionByUser' => ['id' => $customerID, 'type' => 3],
                'ach_type'          => $sch_method,
                'invoiceID'         => $in_data['invoiceID'],
                'crtxnID'           => '',
                'companyName'       => $customerData['companyName'],
                'fullName'          => $customerData['fullName'],
                'firstName'         => $customerData['firstName'],
                'lastName'          => $customerData['lastName'],
                'contact'           => $customerData['phoneNumber'],
                'email'             => $customerData['userEmail'],
                'amount'            => $payamount,
                'gatewayID'         => $gateway_id,
                'storeResult'       => false,
                'returnResult'      => true,
                'customerListID'    => $customerData['Customer_ListID'],
            ];

            $ref_number = $in_data['refNumber'];
            $toEmail    = $customerData['userEmail'];
            $company    = $customerData['fullName'];
            $customer   = $customerData['fullName'];

            $saleData   = array_merge($saleData, $in_data);
            $paidResult = $paymentObj->_processSaleTransaction($saleData);
            
            $crtxnID = '';
            if (isset($paidResult['response']) && $paidResult['response']) {
                $tr_date           = date('Y-m-d H:i:s');
                $updateInvoiceData = [
                    'invoiceID'       => $in_data['invoiceID'],
                    'accountCode'     => $accountData['accountCode'],
                    'trnasactionDate' => $tr_date,
                    'amount'          => $payamount,
                ];

                $xeroSync = new XeroSync($user_id);
                $crtxnID  = $xeroSync->createPayment($updateInvoiceData);

                $nf = $this->addNotificationForMerchant($payamount, $customer, $customerID, $user_id, $invoice_no);
                if (!empty($sendrecipt)) {
                    $this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payamount, $tr_date);
                }

                if ($gt_result['gatewayType'] == 5 && $gt_result['isSurcharge']) {
                    $condition_mail = array('templateType' => '16', 'merchantID' => $user_id);
                    $tr_date        = date('Y-m-d H:i:s');
                    $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payamount, $tr_date, $paidResult['result']['id']);
                }
                $thankyou    = 'Integration/Payments/Thankyou';

                $this->session->set_flashdata('success', '<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
                $newINV = array_merge($customerData, $in_data);
                $this->session->set_userdata("sess_invoice_id", $newINV);
            } else {
                $nf = $this->failedNotificationForMerchant($payamount, $customer, $customerID, $user_id, $invoice_no);
            }

            $responseId = $this->general_model->insert_gateway_transaction_data($paidResult['result'], 'sale', $gateway_id, $gt_result['gatewayType'], $customerID, $payamount, $user_id, $crtxnID, $resellerID, $in_data['invoiceID'], $eCheckStatus, ['id' => $customerID, 'type' => 3]);

            if ($responseId) {
                $condition2  = array('id' => $responseId);
                $transaction = $this->general_model->get_row_data('customer_transaction', $condition2);
                $this->session->set_userdata("tranID", $transaction['transactionID']);
            }
        } else {
            $errorMessage = "Transaction Failed -  This is not valid invoice";
            if (empty($accountData)) {
                $errorMessage = "Missing default payment account information. Please contact to merchant";
            }
            $this->session->set_flashdata('message', "<div class='alert alert-danger'><strong>$errorMessage</strong>.</div>");
        }

        redirect($thankyou);
    }

    public function addNotificationForMerchant($payAmount, $customerName, $customerID, $merchantID, $invoiceNumber = null)
    {
        /*Notification Saved*/

        $payDateTime = date('M d, Y h:i A');
        if ($merchantID) {
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if (isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])) {
                $timezone    = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if ($payDateTime) {
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if ($invoiceNumber == null) {
            $title   = 'Sale Payments';
            $nf_desc = 'A payment for ' . $customerName . ' was made for <b>$' . $payAmount . '</b> on ' . $payDateTime . '.';
            $type    = 1;
        } else {
            $title   = 'Invoice Checkout Payments';
            $in_data = $this->general_model->get_row_data('Xero_test_invoice', array('invoiceID' => $invoiceNumber, 'merchantID' => $merchantID));
            if (isset($in_data['refNumber'])) {
                $invoiceRefNumber = $in_data['refNumber'];
            } else {
                $invoiceRefNumber = $invoiceNumber;
            }
            $nf_desc = 'A payment for Invoice <b>' . $invoiceRefNumber . '</b> was made for <b>$' . $payAmount . '</b> on ' . $payDateTime . '';
            $type    = 2;
        }

        $notifyObj = array(
            'sender_id'    => $customerID,
            'receiver_id'  => $merchantID,
            'title'        => $title,
            'description'  => $nf_desc,
            'is_read'      => 1,
            'recieverType' => 2,
            'type'         => $type,
            'typeID'       => $invoiceNumber,
            'status'       => 1,
            'created_at'   => date('Y-m-d H:i:s'),
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification', $notifyObj);
        /* Update merchant new notification comes*/
        $con        = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0);
        $update     = $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

        $condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
		$ref_number     = $in_data['refNumber'];
		$tr_date        = date('Y-m-d H:i:s');
		$toEmail        = false;
		$company        = $customerName;
		$customer       = $customerName;
		$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payAmount, $tr_date);
        return true;
    }

    public function failedNotificationForMerchant($payAmount, $customerName, $customerID, $merchantID, $invoiceNumber = null)
    {
        /*Notification Saved*/

        $payDateTime = date('M d, Y h:i A');
        if ($merchantID) {
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if (isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])) {
                $timezone    = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if ($payDateTime) {
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title   = 'Failed Invoice Checkout payments';
        $in_data = $this->general_model->get_row_data('Xero_test_invoice', array('invoiceID' => $invoiceNumber, 'merchantID' => $merchantID));
        if (isset($in_data['refNumber'])) {
            $invoiceRefNumber = $in_data['refNumber'];
        } else {
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>' . $invoiceRefNumber . '</b> was attempted on ' . $payDateTime . ' but failed';
        $type    = 2;

        $notifyObj = array(
            'sender_id'    => $customerID,
            'receiver_id'  => $merchantID,
            'title'        => $title,
            'description'  => $nf_desc,
            'is_read'      => 1,
            'recieverType' => 2,
            'type'         => $type,
            'typeID'       => $invoiceNumber,
            'status'       => 1,
            'created_at'   => date('Y-m-d H:i:s'),
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification', $notifyObj);
        /* Update merchant new notification comes*/
        $con        = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0);
        $update     = $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }

    public function Thankyou(){
	  
        $data['primary_nav'] 	= primary_customer_nav();
        $data['template'] 		= template_variable();
        $data['transaction_id'] = $this->session->userdata('tranID');
        $data['invoice_data'] = $this->session->userdata('sess_invoice_id');

        $transaction_id = $data['transaction_id'];
		$payAmount = $data['invoice_data']['BalanceRemaining'];
		$surCharge = 0;
		$isSurcharge = 0;
		$totalAmount = 0;
		$transactionType = 0;
        $merchant_default_timezone = '';
        $transactionCode = 0;

		if($transaction_id != null){
			$con = array('transactionID' => $transaction_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            if(isset($pay_amount['transactionAmount'])){
                $transactionCode = $pay_amount['transactionCode'];
            	$payAmount = $pay_amount['transactionAmount'];
            	$transactionType = $pay_amount['transactionGateway'];

                $user_id = $data['invoice_data']['merchantID'];
                if($user_id){
                    // get merchant selected timezone
                    $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $user_id));
                    if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                        $merchant_default_timezone = $m_data['merchant_default_timezone'];
                    }
                }

            	if($pay_amount['transactionGateway'] == 10){
            		$resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
            		$totalAmount = $resultAmount['totalAmount'];
            		$surCharge = $resultAmount['surCharge'];
            		$isSurcharge = $resultAmount['isSurcharge'];
            		if($resultAmount['payAmount'] != 0){
            			$payAmount = $resultAmount['payAmount'];
            		}   
            	}
            	
            }
		}
		$data['transactionCode'] = $transactionCode;
        $data['merchant_default_timezone'] = $merchant_default_timezone;
        $data['transactionAmount'] = $payAmount;
        $data['surchargeAmount'] = $surCharge;
        $data['totalAmount'] = $totalAmount;
        $data['transactionType'] = $transactionType;
        $data['isSurcharge'] = $isSurcharge;

		$data['ip'] = $this->input->ip_address();
    	$this->load->view('template/template_start', $data);
		$this->load->view('Integration/thankyou', $data);
	   	$this->session->unset_userdata('records');
	}
}
