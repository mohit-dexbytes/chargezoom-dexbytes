<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Not_Found extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
	        $this->load->model('customer_login_model');
	}


	public function index()

	{  
	  
		$data['title'] = 'Page Not Found';
	
		$this->load->view('customer/page_missing', $data);
		$this->load->view('template/template_scripts', $data);
		$this->load->view('template/template_end', $data);
	
	}

}



