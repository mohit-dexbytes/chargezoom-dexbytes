<?php
ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
use GlobalPayments\Api\PaymentMethods\ECheck;


include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;
include APPPATH . 'third_party/Fluidpay.class.php';
class Update_payment extends CI_Controller {
    private $gatewayEnvironment;  
	function __construct()
	{
		parent::__construct();

		$this->load->model('general_model');
		$this->load->model('comp_company_model');
		$this->load->model('card_model');
		$this->load->config('TSYS');
		$this->load->config('fluidpay');
		$this->load->config('globalpayments');
		$this->load->config('EPX');
		$this->load->config('payarc');
        $this->load->library('PayarcGateway');

		$this->load->config('maverick');
        $this->load->library('MaverickGateway');

	    $this->gatewayEnvironment = $this->config->item('environment');
	    $this->db1= $this->load->database('otherdb', TRUE);
       
	}
	
	
	
	public function test(){
	    $port_url = current_url();
	    redirect('wrong_url');   
	}
	
	
	
	
	public function index(){
	   
	    $this->session->unset_userdata("tranID");
		$this->session->unset_userdata("sess_invoice_id");
	    
	      
	     if(!empty($this->input->post(null, true)))
        { 
        	$custom_data_fields = [];
			$marchant_id = $this->czsecurity->xssCleanPostInput('mid');
			$thankyou ='company/Thankyou';
			$invoice_no = $this->czsecurity->xssCleanPostInput('invid');
			$token = $this->czsecurity->xssCleanPostInput('token');
			$con = array('emailCode'=>$token);
			
			$savepaymentinfo='';
			$checkCode = $this->general_model->get_num_rows('tbl_template_data', $con);
              
			
					$con = array('TxnID'=>$invoice_no);
					$result = $this->general_model->get_row_data('chargezoom_test_invoice', $con);
					
					if(!empty($result))
					{ 
						$data['get_invoice'] = $result;	
						
						$payamount = round($result['BalanceRemaining'],2);
						
						
						$in_data   = $this->comp_company_model->get_invoice_data_pay($invoice_no);
						
						$user = $in_data['qbwc_username'];
				
						$cond = array('merchID'=>$marchant_id);
						$rs_Data = $this->general_model->get_row_data('tbl_merchant_data',$cond );
						$resellerID = $rs_Data['resellerID'];
						
						$cone = array('merchantID'=>$marchant_id,'set_as_default'=>1);
						$get_gateway = $this->general_model->get_row_data('tbl_merchant_gateway', $cone);
				
				
						$scheduleID = $this->czsecurity->xssCleanPostInput('scheduleID');
						
						
						$bill_email='';
					
						$paymethod = $scheduleID;
						
						//Billing Data
						$fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
						$lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
						
						
						$country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
						$address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
						$address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
						$city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
						$state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
						$zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
					
						$customerID = $in_data['Customer_ListID'];
						$transactionByUser = ['id' => $customerID, 'type' => 3];
						$companyID = $in_data['companyID'];
						$phone = $in_data['Phone'];

						if($scheduleID == 2){
							$eCheckStatus = '1';
							$accountName = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountName'));
							$accountNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountNumber'));
							$routeNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('routeNumber'));
							$acct_holder_type = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_holder_type'));
							$acct_type = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_type'));
							$sec_code =     'WEB';	

							$accountName = ($accountName != '')?$accountName:$fname.' '.$lname;
							$acct_holder_type = ($acct_holder_type != '')?$acct_holder_type:'business';
							$acct_type = ($acct_type != '')?$acct_type:'checking';
							
							//card data array
							//save card process
							$card_data = array(
								'accountName'      =>$accountName,
								'accountNumber'	   =>$accountNumber, 
								'routeNumber'       =>$routeNumber,
								'accountHolderType' =>$acct_holder_type,
								'accountType'      =>$acct_type, 
								'secCodeEntryMethod' => $sec_code,
								'customerListID' =>$customerID, 
								'companyID'      =>$companyID,
								'merchantID'     => $marchant_id,
								'createdAt' 	 => date("Y-m-d H:i:s"),
								'Billing_Addr1'	 =>$address,
								'Billing_Addr2'	 =>$address2,	 
								'Billing_City'	 =>$city,
								'Billing_State'	 =>$state,
								'Billing_Country'	 =>$country,
								'Billing_Contact'	 =>$phone,
								'Billing_Zipcode'	 =>$zip,
								'CardType'  => 'Echeck',
							);
							$friendlyname = 'Echeck' . ' - ' . substr($card_data['accountNumber'], -4);
                        	$custom_data_fields['payment_type'] = $friendlyname;
						
								
						}else{
							$eCheckStatus = '0';
							$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
							$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
							$expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
							$cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
							
							$cardtype      = $this->general_model->getType($cnumber);
							
							$card_data = array(
								'cardMonth'    =>$expmonth,
								'cardYear'	   =>$expyear, 
								'CardType'     =>$cardtype,
								'CustomerCard' =>$cnumber,
								'CardCVV'      =>$cvv, 
								'customerListID' =>$customerID, 
								'companyID'      =>$companyID,
								'merchantID'     => $marchant_id,
								'createdAt' 	 => date("Y-m-d H:i:s"),
								'Billing_Addr1'	 =>$address,
								'Billing_Addr2'	 =>$address2,	 
								'Billing_City'	 =>$city,
								'Billing_State'	 =>$state,
								'Billing_Country'	 =>$country,
								'Billing_Contact'	 =>$phone,
								'Billing_Zipcode'	 =>$zip,
								
							);	
							$friendlyname = $cardtype . ' - ' . substr($cnumber, -4);
                        	$custom_data_fields['payment_type'] = $friendlyname;
						}
						
						
						
					
						$savepaymentinfo = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('savepaymentinfo'));
						
						$sendrecipt = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('sendrecipt'));
					
							
						$condition_mail  = array('templateType'=>'5', 'merchantID'=>$marchant_id); 
						$ref_number =  $result['RefNumber'];

						$tr_date   =date('Y-m-d h:i A');

						// Convert added date in timezone 
						if(isset($rs_Data['merchant_default_timezone']) && !empty($rs_Data['merchant_default_timezone'])){
							$timezone = ['time' => $tr_date, 'current_format' => 'UTC', 'new_format' => $rs_Data['merchant_default_timezone']];
							$tr_date = getTimeBySelectedTimezone($timezone);
							$tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
						}

						$email = $toEmail = $in_data['Contact']; 
						$company=$in_data['companyName']; 
						$customer = $in_data['FullName'];

						if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL))
	                    {
	                       $email = 'devteam@chargezoom.com';
	                    }
							
						if($this->czsecurity->xssCleanPostInput('pay')){
							$gateway = $get_gateway['gatewayType'];
						
							switch ($gateway) {
								case "1":
									
									include APPPATH . 'third_party/nmiDirectPost.class.php';
									include APPPATH . 'third_party/nmiCustomerVault.class.php';
									//start NMI
									$nmiuser   = $get_gateway['gatewayUsername'];
									$nmipass   = $get_gateway['gatewayPassword'];
									$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);

									
									
									if($payamount > 0){
										
										$transaction1 = new nmiDirectPost($nmi_data); 
										
										if($paymethod =='2'){ // this payment option is for eCheck 
					
										if($eCheckStatus == '1'){
											
										
											$transaction1->setAccountName($accountName);
											$transaction1->setAccount($accountNumber);
											$transaction1->setRouting($routeNumber);
											$sec_code =     'WEB';
											$transaction1->setAccountType($acct_type);
											
											$transaction1->setAccountHolderType($acct_holder_type);
											$transaction1->setSecCode($sec_code);
											$transaction1->setPayment('check');
											
												$transaction1->setCompany($company);
												$transaction1->setFirstName($fname);
												$transaction1->setLastName($lname);
												$transaction1->setCountry($country);
												$transaction1->setCity($city);
												$transaction1->setState($state);
												$transaction1->setZip($zip);
												$transaction1->setPhone($phone);
												$transaction1->setEmail($toEmail);
											
											
											}
											
										}else{
											
											
											
												$transaction1->setCcNumber($cnumber);
												
											
												
												$expmonth =  $expmonth;
												$exyear   = $expyear;
												$exyear   = substr($exyear,2);
												if(strlen($expmonth)==1){
													$expmonth = '0'.$expmonth;
												}
												$expry    = $expmonth.$exyear;  
												$transaction1->setCcExp($expry);
												if(!empty($cvv)){
													$transaction1->setCvv($cvv);
												}
												// add level III data
									            $level_request_data = [
									                'transaction' => $transaction1,
									                'card_no' => $cnumber,
									                'merchID' => $marchant_id,
									                'amount' => $payamount,
									                'invoice_id' => $invoice_no,
									                'gateway' => 1
									            ];
									            $transaction1 = addlevelThreeDataInTransaction($level_request_data);
											
												  
										}
										
										$transaction1->setAmount($payamount);
										$transaction1->sale();
										$getwayResponse = $transaction1->execute(); 
										
									
									
										
										if( $getwayResponse['response_code']=="100"){
											$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										
											$txnID      = $in_data['TxnID'];  
											
											$pay        = $payamount;
											$remainbal  = $in_data['BalanceRemaining']-$payamount;
											if($remainbal == '0')
												$ispaid 	 = 'true';
											else
												$ispaid 	 = 'false';
												
											$app        = $in_data['AppliedAmount']-$payamount;
											$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											$condition  = array('TxnID'=>$in_data['TxnID'] );
											$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										}
										else{
												$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
												$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$getwayResponse['responsetext'].'</strong> </div>');  
												redirect($_SERVER['HTTP_REFERER']);
										}
											
										$transaction['transactionID']      = $getwayResponse['transactionid'];
										$transaction['transactionStatus']  = $getwayResponse['responsetext'];
										$transaction['transactionCode']    = $getwayResponse['response_code'];
										$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
										$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
										$transaction['transactionModified']= date('Y-m-d H:i:s'); 
										$transaction['invoiceTxnID']       = $in_data['TxnID'];
										$transaction['gatewayID']          = $get_gateway['gatewayID'];
										$transaction['transactionGateway'] = $gateway;	
										$transaction['customerListID']     = $in_data['ListID'];
										$transaction['transactionAmount']  = $payamount;
										$transaction['merchantID']         = $marchant_id;
										if($eCheckStatus == '1'){
											$transaction['gateway']            = "NMI ECheck";
										}else{
											$transaction['gateway']            = "NMI";
										}
										$transaction['resellerID']         = $resellerID;
										$transaction['paymentType']         = $scheduleID;
										$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);

										if(!empty($transactionByUser)){
										    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
										    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
										}	
										if($custom_data_fields){
                                            $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                        }
										$id = $this->general_model->insert_row('customer_transaction',$transaction);
									
										if($id){
											if(!empty($savepaymentinfo)){
									$cardId =   $this->card_model->process_card($card_data);
											}
											//save card process end
											
											if(!empty($sendrecipt)){
												$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
											}
											
											
											$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
											$this->session->set_userdata("tranID",$transaction['transactionID'] );
									$this->session->set_userdata("sess_invoice_id",$in_data); 
										redirect($thankyou);
											
										}else{
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Payment record not inserted</div>');  
										}
									
									}else{
											$this->session->set_flashdata('message','<div class="alert alert-danger"> Payment is not greater than 0</div>'); 
											redirect($_SERVER['HTTP_REFERER']);
									}
							
								break;
							case "2":
								
								include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
								$this->load->config('auth_pay');
								//Login in Auth
								$apiloginID       = $get_gateway['gatewayUsername'];
								$transactionKey   = $get_gateway['gatewayPassword'];
								if($this->config->item('auth_test_mode')){
									$sandbox = TRUE;
								}else{
									$sandbox = FALSE;
								}
								
								
								if($payamount > 0){
									$transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
									$transaction1->setSandbox($sandbox);
									
							
									if($paymethod =='2'){ // this payment option is for eCheck 
					
										if($eCheckStatus == '1'){
										
										
											$sec_code =     'WEB';   
										
											$transaction1->setECheck($routeNumber, $accountNumber, $acct_type, $bank_name='Wells Fargo Bank NA', $accountName, $sec_code);
												
											
												
												$transaction1->__set('company',$company);
												$transaction1->__set('first_name',$fname);
												$transaction1->__set('last_name', $lname);
												$transaction1->__set('address',$address);
												$transaction1->__set('country',$country);
												$transaction1->__set('city',$city);
												$transaction1->__set('state',$state);
												$transaction1->__set('zip',$zip);
												
												$transaction1->__set('ship_to_address', $address);
												$transaction1->__set('ship_to_country',$country);
												$transaction1->__set('ship_to_city',$city);
												$transaction1->__set('ship_to_state',$state);
												$transaction1->__set('ship_to_zip',$zip);
												
												
												$transaction1->__set('phone',$phone);
											
												$transaction1->__set('email',$toEmail);
											
											
												
										$getwayResponse = $transaction1->authorizeAndCapture($payamount);
												
										
											
											
											}
											
										}else
										{
											
											
											$card_no  = $cnumber;
											$expmonth = $expmonth;
											$exyear   = $expyear;
											$exyear   = substr($exyear,2);
											if(strlen($expmonth)==1){
												$expmonth = '0'.$expmonth;
											}
												$expry = $expmonth.$exyear;  
											$getwayResponse = $transaction1->authorizeAndCapture($payamount,$card_no,$expry);
									
										}
								if( $getwayResponse->response_code=="1" && $getwayResponse->transaction_id != 0 && $getwayResponse->transaction_id != ''){
										$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$txnID      = $in_data['TxnID'];  
									
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										
										if($remainbal == '0')
											$ispaid 	 = 'true';
										else
											$ispaid 	 = 'false';
											
										$app        = $in_data['AppliedAmount']-$payamount;
											$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										
										
									}
									else{
											$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'. $getwayResponse->response_reason_text .'</div>');  
											redirect($_SERVER['HTTP_REFERER']);
									}
									$transactiondata= array();
									$transactiondata['transactionID']       = ($getwayResponse->transaction_id != 0 && $getwayResponse->transaction_id != '')?$getwayResponse->transaction_id:'TXNFAILED'.time();
									
									$transactiondata['transactionStatus']   = $getwayResponse->response_reason_text;
									$transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
									$transactiondata['transactionModified']= date('Y-m-d H:i:s');
									$transactiondata['transactionCode']     = $getwayResponse->response_code;  
									$transactiondata['transactionCard']     = substr($getwayResponse->account_number,4);  
									$transactiondata['transactionType']     = $getwayResponse->transaction_type;	   
									$transactiondata['gatewayID']           = $get_gateway['gatewayID'];
									$transactiondata['transactionGateway']  = $get_gateway['gatewayType'];
									$transactiondata['customerListID']      = $in_data['ListID'];
									$transactiondata['transactionAmount']   = $payamount;
									$transactiondata['invoiceTxnID']        = $in_data['TxnID'];
									$transactiondata['merchantID']          = $marchant_id;
									$transactiondata['gateway']             = "Auth";
									$transactiondata['resellerID']          = $resellerID;
									$transactiondata['paymentType']         = $scheduleID;
									$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transactiondata['transactionCode']);
									if(!empty($transactionByUser)){
									    $transactiondata['transaction_by_user_type'] = $transactionByUser['type'];
									    $transactiondata['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
									if($id){
										if(!empty($savepaymentinfo)){
									$cardId = $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
										$this->session->set_userdata("tranID",$transactiondata['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
										redirect($thankyou);
									}else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Payment record not inserted</div>');  
									} 
								}
								
								break;
							case "3":
								
								include APPPATH . 'third_party/PayTraceAPINEW.php';
								$this->load->config('paytrace');
								$payusername   = $get_gateway['gatewayUsername'];
								$paypassword   = $get_gateway['gatewayPassword'];
								$integratorId   = $get_gateway['gatewaySignature'];
								$grant_type    = "password";
								$name = $fname." ".$lname;
								if($payamount > 0){
									$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
									$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
									$expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
									$cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
									$expmonth = $expmonth;
									if(strlen($expmonth)==1){
										$expmonth = '0'.$expmonth;
									}
								
									$payAPI  = new PayTraceAPINEW();	
									$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
									//call a function of Utilities.php to verify if there is any error with OAuth token. 
									$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
									
									if(!$oauth_moveforward){ 
										$json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
										//set Authentication value based on the successful oAuth response.
										//Add a space between 'Bearer' and access _token 
										$oauth_token = sprintf("Bearer %s",$json['access_token']);
										$request_data = array(
											"amount"            => $payamount,
											"credit_card"       => array (
												"number"            => $cnumber,
												"expiration_month"  =>$expmonth,
												"expiration_year"   =>$expyear
											),
											
											"csc"               => $cvv,
											"invoice_id"        =>$invoice_no,
											
											"billing_address"=> array(
												"name"          =>$name,
												"street_address"=> $address,
												"city"          => $city,
												"state"         => $state,
												"zip"           => $zip
											)
										);  

										if(empty($cvv)){
											unset($request_data['csc']);
										}
								
									$request_data = json_encode($request_data); 
									$gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
									$response  = $payAPI->jsonDecode($gatewayres['temp_json_response']);
									if ( $gatewayres['http_status_code']=='200' ){
										$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										
										// add level three data in transaction
					                    if($response['success']){
					                        $level_three_data = [
					                            'card_no' => $cnumber,
					                            'merchID' => $marchant_id,
					                            'amount' => $payamount,
					                            'token' => $oauth_token,
					                            'integrator_id' => $integratorId,
					                            'transaction_id' => $response['transaction_id'],
					                            'invoice_id' => $invoice_no,
					                            'gateway' => 3,
					                        ];
					                        addlevelThreeDataInTransaction($level_three_data);
					                    }
										
										$txnID      = $in_data['TxnID'];  
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
											$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										}
										else{
												$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
												$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>' . $response['status_message'] .'</div>'); 
											redirect($_SERVER['HTTP_REFERER']);
										}
										
										
										$transactiondata= array();
										if(isset($response['transaction_id']))
										{
											$transactiondata['transactionID']   = $response['transaction_id'];
										}
										else
										{
											$transactiondata['transactionID']   = '';
										}
										$transactiondata['transactionStatus']   = $response['status_message'];
										$transactiondata['transactionDate']     = date('Y-m-d H:i:s');  
											$transactiondata['transactionModified']= date('Y-m-d H:i:s');
										$transactiondata['transactionCode']     = $gatewayres['http_status_code'];
										$transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
										$transactiondata['transactionType']     = 'pay_sale';
										$transactiondata['gatewayID']           = $get_gateway['gatewayID'];
										$transactiondata['transactionGateway']  = $get_gateway['gatewayType'] ;
										$transactiondata['customerListID']      = $in_data['ListID'];
										$transactiondata['invoiceTxnID']        = $in_data['TxnID'];
										$transactiondata['transactionAmount']   = $payamount;
										$transactiondata['merchantID']          = $marchant_id;
										$transactiondata['gateway']             = "Paytrace";
										$transactiondata['resellerID']          = $resellerID;
										$transactiondata['paymentType']         = $scheduleID;
										$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transactiondata['transactionCode']);
										if(!empty($transactionByUser)){
										    $transactiondata['transaction_by_user_type'] = $transactionByUser['type'];
										    $transactiondata['transaction_by_user_id'] = $transactionByUser['id'];
										}
										if($custom_data_fields){
	                                        $transactiondata['custom_data_fields']  = json_encode($custom_data_fields);
	                                    }
										$id = $this->general_model->insert_row('customer_transaction',   $transactiondata);
										
										if($id){
											if(!empty($savepaymentinfo)){
											$cardId = $this->card_model->process_card($card_data);
											}
											//save card process end
											if(!empty($sendrecipt)){
												$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
											}
											
											$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
											$this->session->set_userdata("tranID",$transactiondata['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											redirect($thankyou);
										}else{
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Payment record not inserted</div>');  
										}
									} else{
										$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Transaction Failed - </strong> Authentication not valid.</div>'); 
									}
									redirect($_SERVER['HTTP_REFERER']);
								}
								
								break;
								
							case "4":
								include APPPATH . 'third_party/PayPalAPINEW.php';
								$this->load->config('paypal');
								if($this->config->item('mode') == 0){
									$sandbox = TRUE;
								}else{
									$sandbox = FALSE;
								}
								$config = array(
									'Sandbox' => $sandbox, 			// Sandbox / testing mode option.
									'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
									'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
									'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
									'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
									'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
								);
								$this->load->library('paypal/Paypal_pro', $config);	  
								if($config['Sandbox'])
									{
										error_reporting(E_ALL);
										ini_set('display_errors', '1');
									}
								
								$name = $fname." ".$lname;
								if($payamount > 0){
							
									$creditCardType   = 'Visa';
									$creditCardNumber = $cnumber;
									$expDateMonth     = $expmonth;
									$expDateYear      = $expyear;
									$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
									$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
									$cvv2Number       =   $cvv;
									$currencyID       = "USD";
									
									$firstName = $fname;
									$lastName =  $lname; 
									$address1 = $address; 
									$address2 = $address2; 
									$country  = $country; 
									$city     = $city;
									$state    = $state;		
									$zip  = $zip;  
									$email = $bill_email; 
													
									$DPFields = array(
										'paymentaction' => 'Sale', 	                // How you want to obtain payment.  
										'ipaddress' => '', 							// Required.  IP address of the payer's browser.
										'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
									);
													
									$CCDetails = array(
										'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
										'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
										'expdate'        => $expmonth.$expyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
										'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
										'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
										'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
									);

									if(empty($cvv)){
										unset($CCDetails['cvv2']);
									}
													
									$PayerInfo = array(
										'email'          => $bill_email, 								// Email address of payer.
										'payerid'        => '', 							// Unique PayPal customer ID for payer.
										'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
										'business'       => '' 							// Payer's business name.
									);  
													
									$PayerName = array(
										'salutation'     => '', 						// Payer's salutation.  20 char max.
										'firstname'      => $fname, 							// Payer's first name.  25 char max.
										'middlename'     => '', 						// Payer's middle name.  25 char max.
										'lastname'       => $lname, 							// Payer's last name.  25 char max.
										'suffix'         => ''								// Payer's suffix.  12 char max.
									);
												
									$BillingAddress = array(
										'street'         => $address1, 						// Required.  First street address.
										'street2'        => $address2, 						// Second street address.
										'city'           => $city, 							// Required.  Name of City.
										'state'          => $state, 							// Required. Name of State or Province.
										'countrycode'    => $country, 					// Required.  Country code.
										'zip'            => $zip 						// Phone Number of payer.  20 char max.
									);
								
														
								$PaymentDetails = array(
										'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
										'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
										'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
										'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
										'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
										'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
										'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
										'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
										'custom'         => '', 						// Free-form field for your own use.  256 char max.
										'invnum'         => '', 						// Your own invoice or tracking number
										'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
										'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
										'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
									);					
			
									$PayPalRequestData = array(
										'DPFields'       => $DPFields, 
										'CCDetails'      => $CCDetails, 
										'PayerInfo'      => $PayerInfo, 
										'PayerName'      => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										'PaymentDetails' => $PaymentDetails, 
										
									);
										
									$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
									if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])){
										$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										
										$txnID      = $in_data['TxnID'];  
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
											$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
									
									}
									else
									{
										$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$PayPalResult["ACK"].'</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
									}
									
										$transaction= array();
										$tranID ='' ;
										$amt='0.00';
										if(isset($PayPalResult['TRANSACTIONID'])) { 
											$tranID = $PayPalResult['TRANSACTIONID'];   
											$amt=$PayPalResult["AMT"];  
										}
										
										$transaction['transactionID']       = $tranID;
										$transaction['transactionStatus']   = $PayPalResult["ACK"];
										$transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
											$transactiondata['transactionModified']= date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
										$transaction['transactionCode']     = $code;  
										$transaction['transactionType']     = "Paypal_sale";	
										$transaction['gatewayID']           = $get_gateway['gatewayID'];
										$transaction['transactionGateway']  = $get_gateway['gatewayType'];					
										$transaction['customerListID']      = $in_data['ListID'];
										$transaction['invoiceTxnID']        = $in_data['TxnID'];
										$transaction['transactionAmount']   = $payamount;
										$transaction['merchantID']          = $marchant_id;
										if($eCheckStatus == '1'){
											$transaction['gateway']            = "Paypal ECheck";
										}else{
											$transaction['gateway']            = "Paypal";
										}
										
										$transaction['resellerID']          = $resellerID;
										$transaction['paymentType']         = $scheduleID;
										$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);
										if(!empty($transactionByUser)){
										    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
										    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
										}
										if($custom_data_fields){
	                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
	                                    }
										$this->general_model->insert_row('customer_transaction',   $transaction);
										if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
										
										
												if(!empty($savepaymentinfo)){
													$cardId =  $this->card_model->process_card($card_data);
												}
												//save card process end
												if(!empty($sendrecipt)){
													$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
												}
											
											$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
											$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
												redirect($thankyou);
										
										}
									
								}
								
								break;
							case "5":
								
								require_once APPPATH."third_party/stripe/init.php";	
								require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
					
							 
							
								if($payamount > 0)
								{
									
									$paidamount =  (int)($payamount*100);
									
									\Stripe\Stripe::setApiKey($get_gateway['gatewayPassword']);
									$cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
                                    $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
                                    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
                                    $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));

									$stripeCard = [
										'card' => [
											'number'    => $cnumber,
											'exp_month' => $expmonth,
											'exp_year'  => $expyear,
											'cvc'       => $cvv,
											'name'      => $customer,
										],
									];
									if(empty($cvv)){
										unset($stripeCard['card']['cvc']);
									}
									$res = \Stripe\Token::create($stripeCard);

			                        $tcharge = json_encode($res);

			                        $rest = json_decode($tcharge);
			                        $trID='failed'.time();
			                        if ($rest->id) {
			                        	$charge =	\Stripe\Charge::create(array(
										"amount" => $paidamount,
										"currency" => "usd",
										"source" => $rest->id, // obtained with Stripe.js
										"description" => "Charge Using Stripe Gateway",
										
										));	
										$charge= json_encode($charge);
										
										$resultstripe = json_decode($charge);

										if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
										{
											$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$trID       = $resultstripe->id;
											$code		=  '200';
											$txnID      = $in_data['TxnID'];  
											$ispaid 	 = 'true';
											$pay        = $payamount;
											$remainbal  = $in_data['BalanceRemaining']-$payamount;
											$app        = $in_data['AppliedAmount']-$payamount;
											$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
												
											$condition  = array('TxnID'=>$in_data['TxnID'] );
											$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										}
										else
										{
											$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$resultstripe->status .'</div>'); 
											redirect($_SERVER['HTTP_REFERER']);
										}  
			                        }else{
			                        	$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
			                        	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$rest->status .'</div>'); 
											redirect($_SERVER['HTTP_REFERER']);
			                        }
									
									
									  
									
									$transaction['transactionID']       = $trID;
									$transaction['transactionStatus']   = $resultstripe->status;
									$transaction['transactionDate']     = date('Y-m-d H:i:s');  
									$transaction['transactionModified']     = date('Y-m-d H:i:s'); 
									$transaction['transactionCode']     = $code;  
									$transaction['invoiceTxnID']        = $in_data['TxnID'];
									$transaction['transactionType']     = 'stripe_sale';	
									$transaction['gatewayID']           = $get_gateway['gatewayID'];
									$transaction['transactionGateway']  = $get_gateway['gatewayType'] ;					
									$transaction['customerListID']      = $in_data['ListID'];
									$transaction['transactionAmount']   = $payamount;
									$transaction['merchantID']          = $marchant_id;
									if($eCheckStatus == '1'){
										$transaction['gateway']            = "Stripe ECheck";
									}else{
										$transaction['gateway']            = "Stripe";
									}
									
									$transaction['resellerID']          = $resellerID;
									$transaction['paymentType']         = $scheduleID;
									$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction['transactionCode']);
									if(!empty($transactionByUser)){
									    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',   $transaction); 
									
									if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
									{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
										$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data);
										redirect($thankyou);
									
									}
								}
								
								break;
							
							case "7":
							
						require_once dirname(__FILE__) . '/../../../../vendor/autoload.php';
						
										$payusername   = $get_gateway['gatewayUsername'];
										$secretApiKey   = $get_gateway['gatewayPassword'];
										
										$card_no = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
										$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
										$expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
										$cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
										$address1 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));

										$cardType = $this->general_model->getType($card_no);
										
										$config = new PorticoConfig();
				
										$config->secretApiKey = $secretApiKey;
										$config->serviceUrl =  $this->config->item('GLOBAL_URL');
										$customerID = $in_data['ListID'] ;
											ServicesContainer::configureService($config);
											$card = new CreditCardData();
											$card->number = $card_no;
											$card->expMonth = $expmonth;
											$card->expYear = $expyear;
											if($cvv!="")
											$card->cvn = $cvv;
										$card->cardType=$cardType;
									
											$address = new Address();
											$address->streetAddress1 = $address1;
											$address->city = $city;
											$address->state = $state;
											$address->postalCode = $zip;
											$address->country = $country;
											
							
											$invNo  =mt_rand(5000000,20000000);
											try
											{
												$eCheck_payment = false;
												if($paymethod =='2' && $eCheckStatus == '1'){ // this payment option is for eCheck
													$eCheck_payment = true;
													$check = new ECheck();
							                        $check->accountNumber = $accountNumber;
							                        $check->routingNumber = $routeNumber;
							                        if(strtolower($acct_type) == 'checking'){
							                            $check->accountType = 0;
							                        }else{
							                            $check->accountType = 1;
							                        }

							                        if(strtoupper($acct_holder_type) == 'PERSONAL'){
							                            $check->checkType = 0;
							                        }else{
							                            $check->checkType = 1;
							                        }
							                        $check->checkHolderName = $accountName;
							                        $check->secCode = "WEB";

							                        $response = $check->charge($payamount)
							                        ->withCurrency(CURRENCY)
							                        ->withAddress($address)
							                        ->withInvoiceNumber($invNo)
							                        ->withAllowDuplicates(true)
							                        ->execute();
												}else{

													$response = $card->charge($payamount)
													->withCurrency("USD")
													->withAddress($address)
													->withInvoiceNumber($invNo)
													->withAllowDuplicates(true)
													->execute();
												}
												
												if($response->responseCode != 0 && $response->responseCode != '00')
							                    {
							                        $error='Gateway Error. Invalid Account Details';
							                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
							                        redirect($_SERVER['HTTP_REFERER']);
							                    }
							                    
												
													$error=''; 	  
												if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
												{
													if($paymethod =='2' && $eCheckStatus == '1'){ 
														
													}else{
														// add level three data
								                        $transaction = new Transaction();
								                        $transaction->transactionReference = new TransactionReference();
								                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
								                        $level_three_request = [
								                            'card_no' => $card_no,
								                            'amount' => $payamount,
								                            'invoice_id' => $invNo,
								                            'merchID' => $marchant_id,
								                            'transaction_id' => $response->transactionId,
								                            'transaction' => $transaction,
								                            'levelCommercialData' => $levelCommercialData,
								                            'gateway' => 7
								                        ];
								                        addlevelThreeDataInTransaction($level_three_request);
													}

														$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
														$msg = $response->responseMessage;
														$trID = $response->transactionId;
														$code_data ="SUCCESS";
														$tr_type  = 'sale';
														$result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID,'paymentType' => $scheduleID );
																$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
															$txnID      = $in_data['TxnID'];  
															$ispaid 	 = 'true';
															$pay        = $payamount;
															$remainbal  = $in_data['BalanceRemaining']-$payamount;
															$app        = $in_data['AppliedAmount']-$payamount;
									
															$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
															$condition  = array('TxnID'=>$in_data['TxnID'] );
															$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);   	
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], $eCheck_payment, $transactionByUser, $custom_data_fields);												 
															
															
															if(!empty($savepaymentinfo)){
																	$cardId =  $this->card_model->process_card($card_data);
																}
																//save card process end
																if(!empty($sendrecipt)){
																	$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
																}
																
																$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
																$this->session->set_userdata("tranID",$response->transactionId );
										$this->session->set_userdata("sess_invoice_id",$in_data);  
																redirect($thankyou);
													}
													else
													{
														
														$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
														$msg = $response->responseMessage;
															$trID = $response->transactionId;
															$result =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID,'paymentType' => $scheduleID );
															$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'. $msg .'</div>'); 
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], false, $transactionByUser, $custom_data_fields);												 
																								 
															redirect($_SERVER['HTTP_REFERER']);
														
														}
													
											}
											
											
												catch (BuilderException $e)
												{
													$error= 'Build Exception Failure: ' . $e->getMessage();
													$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
												}
												catch (ConfigurationException $e)
												{
													$error='ConfigurationException Failure: ' . $e->getMessage();
													$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
												}
												catch (GatewayException $e)
												{
													$error= 'GatewayException Failure: ' . $e->getMessage();
													$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
												}
												catch (UnsupportedTransactionException $e)
												{
													$error='UnsupportedTransactionException Failure: ' . $e->getMessage();
												$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
												}
												catch (ApiException $e)
												{
													$error=' ApiException Failure: ' . $e->getMessage();
												$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
												}
												
												
												if($error!="")
												redirect($_SERVER['HTTP_REFERER']);
							
							
							
							break;    
							
							
							case "6":
							
							require_once APPPATH."third_party/usaepay/usaepay.php";	
							$this->load->config('usaePay');
							
							$customerID = $in_data['ListID'];
							
							$payusername   = $get_gateway['gatewayUsername'];
							$password      = $get_gateway['gatewayPassword'];
					
										
							$cvv='';	
							if($this->config->item('mode') == 0){
								$sandbox = TRUE;
							}else{
								$sandbox = FALSE;
							}
								
							$invNo  =mt_rand(1000000,2000000); 
							$transaction = new umTransaction;
							$transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
							
							$transaction->key=$payusername;
							$transaction->pin=$password;
							$transaction->usesandbox=$sandbox;
							$transaction->invoice=$invNo;   		// invoice number.  must be unique.
							$transaction->description="Chargezoom Public Invoice Payment";	// description of charge
						
							$transaction->testmode=0;    // Change this to 0 for the transaction to process
							$transaction->command="sale";	
							$transaction->card = $card_no;
							$expyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
								$expry    = $expmonth.$expyear;  
								$transaction->exp = $expry;
								if($cvv!="")
								$transaction->cvv2 = $cvv;
							
								$transaction->billfname = $fname;
								$transaction->billlname = $lname;
								$transaction->billstreet = $address1;
								$transaction->billstreet2 = $address2;
								$transaction->billcountry = $country;
								$transaction->billcity    = $city;
								$transaction->billstate = $state;
								$transaction->billzip = $zipcode;
								
								
								$transaction->shipfname = $fname;
								$transaction->shiplname = $lname;
								$transaction->shipstreet = $address1;
								$transaction->shipstreet2 = $address2;
								$transaction->shipcountry = $country;
								$transaction->shipcity    = $city;
								$transaction->shipstate = $state;
								$transaction->shipzip = $zipcode;
							
								$amount =$payamount;
								
								
								$transaction->amount = $amount;
								$transaction->Process();
								
													$error=''; 	
													
													
								if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
								{			        
														$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
														$msg = $transaction->result;
														$trID = $transaction->refnum;
														$code_data ="SUCCESS";
														$tr_type  = 'sale';
														$result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID ,'paymentType' => $scheduleID);
																$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
															$txnID      = $in_data['TxnID'];  
															$ispaid 	 = 'true';
															$pay        = $payamount;
															$remainbal  = $in_data['BalanceRemaining']-$payamount;
															$app        = $in_data['AppliedAmount']-$payamount;
									
															$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
															$condition  = array('TxnID'=>$in_data['TxnID'] );
															$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);   	
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], false, $transactionByUser, $custom_data_fields);												 
															
															if(!empty($savepaymentinfo)){
																	$cardId =  $this->card_model->process_card($card_data);
																}
																//save card process end
																if(!empty($sendrecipt)){
																	$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
																}
																
																$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>');  
																$this->session->set_userdata("tranID",$transaction->refnum );
										$this->session->set_userdata("sess_invoice_id",$in_data);
																redirect($thankyou);
													}
													else
													{
														
														$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
														$msg = $transaction->error;
														$trID = $transaction->refnum;
															$result =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID,'paymentType' => $scheduleID );
															$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'. $msg .'</div>'); 
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], false, $transactionByUser, $custom_data_fields);												 
															redirect($_SERVER['HTTP_REFERER']);
														}
													
											
							
							
							
							break;
							case "9":
									
								include APPPATH . 'third_party/nmiDirectPost.class.php';
								include APPPATH . 'third_party/nmiCustomerVault.class.php';
								//start NMI
								$nmiuser   = $get_gateway['gatewayUsername'];
								$nmipass   = $get_gateway['gatewayPassword'];
								$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
								
								if($payamount > 0){
									
									$transaction1 = new nmiDirectPost($nmi_data); 
									
									if($paymethod =='2'){ // this payment option is for eCheck 
				
										if($eCheckStatus == '1'){
											
										
											$transaction1->setAccountName($accountName);
											$transaction1->setAccount($accountNumber);
											$transaction1->setRouting($routeNumber);
											$sec_code =     'WEB';
											$transaction1->setAccountType($acct_type);
											
											$transaction1->setAccountHolderType($acct_holder_type);
											$transaction1->setSecCode($sec_code);
											$transaction1->setPayment('check');
											
											$transaction1->setCompany($company);
											$transaction1->setFirstName($fname);
											$transaction1->setLastName($lname);
											$transaction1->setCountry($country);
											$transaction1->setCity($city);
											$transaction1->setState($state);
											$transaction1->setZip($zip);
											$transaction1->setPhone($phone);
											$transaction1->setEmail($toEmail);
											
											
										}
										
									}else{
										$transaction1->setCcNumber($cnumber);	
										$expmonth =  $expmonth;
										$exyear   = $expyear;
										$exyear   = substr($exyear,2);
										if(strlen($expmonth)==1){
											$expmonth = '0'.$expmonth;
										}
										$expry    = $expmonth.$exyear;  
										$transaction1->setCcExp($expry);
										if(!empty($cvv)){
											$transaction1->setCvv($cvv);
										}
									
									}
									
									$transaction1->setAmount($payamount);
									$transaction1->sale();
									$getwayResponse = $transaction1->execute(); 
								
									if( $getwayResponse['response_code']=="100"){
										$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
									
										$txnID      = $in_data['TxnID'];  
										
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										if($remainbal == '0')
											$ispaid 	 = 'true';
										else
											$ispaid 	 = 'false';
											
										$app        = $in_data['AppliedAmount']-$payamount;
										$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
									}
									else{
											$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$getwayResponse['responsetext'].'</strong> </div>');  
											redirect($_SERVER['HTTP_REFERER']);
									}
										
									$transaction['transactionID']      = $getwayResponse['transactionid'];
									$transaction['transactionStatus']  = $getwayResponse['responsetext'];
									$transaction['transactionCode']    = $getwayResponse['response_code'];
									$transaction['transactionType']    = ($getwayResponse['type'])?$getwayResponse['type']:'auto-nmi';
									$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
									$transaction['transactionModified']= date('Y-m-d H:i:s'); 
									$transaction['invoiceTxnID']       = $in_data['TxnID'];
									$transaction['gatewayID']          = $get_gateway['gatewayID'];
									$transaction['transactionGateway'] = $gateway;	
									$transaction['customerListID']     = $in_data['ListID'];
									$transaction['transactionAmount']  = $payamount;
									$transaction['merchantID']         = $marchant_id;
									
									if($eCheckStatus == '1'){
										$transaction['gateway']            = "NMI ECheck";
									}else{
										$transaction['gateway']            = "NMI";
									}
									$transaction['resellerID']         = $resellerID;
									$transaction['paymentType']         = $scheduleID;
									if(!empty($transactionByUser)){
									    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',$transaction);
								
									if($id){
										if(!empty($savepaymentinfo)){
											$cardId =   $this->card_model->process_card($card_data);
										}
										//save card process end
										
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
										$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
										redirect($thankyou);
										
									}else{
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Payment record not inserted</div>');  
									}
							
								}else{
										$this->session->set_flashdata('message','<div class="alert alert-danger"> Payment is not greater than 0</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
								}
						
							break;
							case "10":

								$apiUsername   = $get_gateway['gatewayUsername'];
                  				$apiKey   = $get_gateway['gatewayPassword'];
                  				$isSurcharge = $get_gateway['isSurcharge'];
                  				$trID = 'TXNFAILED'.time();
                  				if($payamount > 0){
                  					$amount = $payamount;
                  					if($eCheckStatus == '1'){
                  						$request_data = [
			                                "amount" => ($amount * 100),
			                                "ach" => [
                                                "name" => $accountName,
                                                "account_number" => $accountNumber,
                                                "routing_number" => $routeNumber, 
                                                "phone_number" => '9493019414' ,
                                                "sec_code" => $sec_code,
                                                "savings_account" => (strtolower($acct_type) == 'savings') ? true : false, 
                                            ],
			                                "address" => [
			                                    "line1"       => $address,
												"line2"       => $address2,
												"city"        => $city,
												"state"       => $state,
												"postal_code" => $zip,
			                                ],
			                               
			                            ];
                  					}else{
                  						$request_data = array(
											"amount"          => ($amount * 100),
											"card"            => array(
												"name"      => $fname. " " . $lname,
												"number"    => $cnumber,
												"exp_month" => $expmonth,
												"exp_year"  => $expyear,
												"cvv"       => $cvv,
											),
											"address" => array(
												"line1"       => $address,
												"line2"       => $address2,
												"city"        => $city,
												"state"       => $state,
												"postal_code" => $zip,
											),
										);

										if(empty($cvv)){
											unset($request_data['card']['cvv']);
										}
                  					}
                  					
									$sdk       = new iTTransaction();
									$getwayResponse    = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);

									if ($getwayResponse['status_code'] == '200' || $getwayResponse['status_code'] == '201') {
		                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$code		=  '200';
										$trID       = $getwayResponse['id'];
											
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
										$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);	
										

									}
									else{
										$code = $getwayResponse['status_code'];
										$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
	                  					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$getwayResponse['responsetext'].'</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
									}

									$transaction['transactionID']       = $trID;
									$transaction['transactionStatus']   = $getwayResponse['status'];
									$transaction['transactionDate']     = date('Y-m-d H:i:s');  
									$transaction['transactionModified']     = date('Y-m-d H:i:s'); 
									$transaction['transactionCode']     = $code;  
									$transaction['invoiceTxnID']        = $in_data['TxnID'];
									$transaction['transactionType']     = 'sale';	
									$transaction['gatewayID']           = $get_gateway['gatewayID'];
									$transaction['transactionGateway']  = $get_gateway['gatewayType'] ;					
									$transaction['customerListID']      = $in_data['ListID'];
									$transaction['transactionAmount']   = $payamount;
									$transaction['merchantID']          = $marchant_id;
									if($eCheckStatus == '1'){
										$transaction['gateway']            = iTransactGatewayName." ECheck";
									}else{
										$transaction['gateway']            = iTransactGatewayName;
									}
									
									$transaction['resellerID']          = $resellerID;
									$transaction['paymentType']         = $scheduleID;
									if(!empty($transactionByUser)){
									    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',   $transaction); 
											
									if ($getwayResponse['status_code'] == '200' || $getwayResponse['status_code'] == '201') 
									{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										if($isSurcharge){
			                                $condition_mail = array('templateType' => '16', 'merchantID' => $marchant_id);
			                                
			                                $tr_date        = date('Y-m-d H:i:s');
			                               
			                                $this->general_model->surcharge_send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payamount, $tr_date,$trID);
			                            }
										$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
										$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											
										redirect($thankyou);		
									}
                  				}

							break; 
							case "13":
							case "11": 
								if($payamount > 0){
									
									$amount = $payamount;
									$calamount = $amount * 100;

									if($eCheckStatus == '1'){
										$transactionData = array(
						                    "type"                => "sale",
						                    "amount"              => round($calamount,2),
						                    "currency"            => "USD",
						                    "description"         => 'Payportal Esale',
						                    "po_number"           => null,
						                    "ip_address"          => getClientIpAddr(),
						                    "payment_method"      => array(
						                        "ach" => array(
						                            "routing_number" => $routeNumber,
						                            "account_number" => $accountNumber,
						                            "sec_code"       => $sec_code,
						                            "account_type"   => $acct_type,
						                        ),
						                    ),
						                    "billing_address"     => array(
												"first_name"     => $fname,
												"last_name"      => $lname,
												"company"        => $company,
												"address_line_1" => $address,
												"address_line_2" => $address2,
												"city"           => $city,
												"state"          => $state,
												"postal_code"    => $zip,
												"phone"          => $phone,
												"fax"            => $phone,
												"email"          => $toEmail,
											),
											"shipping_address"    => array(
												"first_name"     => $fname,
												"last_name"      => $lname,
												"company"        => $company,
												"address_line_1" => $address,
												"address_line_2" => $address2,
												"city"           => $city,
												"state"          => $state,
												"postal_code"    => $zip,
												"phone"          => $phone,
												"fax"            => $phone,
												"email"          => $toEmail,
											),
						                );
									}else{
										$card_no = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
										$expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
										$expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
										$cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
										$expyear   = substr($expyear,2);
										if(strlen($expmonth)==1){
											$expmonth = '0'.$expmonth;
										}
										$expry    = $expmonth.$expyear;

										$transactionData = array(
											"type"                => "sale",
											"amount"              => round($calamount,2),
											"currency"            => "USD",
											"ip_address"          => getClientIpAddr(),
											"payment_method"      => array(
												"card" => array(
													"entry_type"      => "keyed",
													"number"          => $card_no,
													"expiration_date" => $expry,
													"cvc"             => $cvv,
												),
											),
											"billing_address"     => array(
												"first_name"     => $fname,
												"last_name"      => $lname,
												"company"        => $company,
												"address_line_1" => $address,
												"address_line_2" => $address2,
												"city"           => $city,
												"state"          => $state,
												"postal_code"    => $zip,
												"phone"          => $phone,
												"fax"            => $phone,
												"email"          => $toEmail,
											),
											"shipping_address"    => array(
												"first_name"     => $fname,
												"last_name"      => $lname,
												"company"        => $company,
												"address_line_1" => $address,
												"address_line_2" => $address2,
												"city"           => $city,
												"state"          => $state,
												"postal_code"    => $zip,
												"phone"          => $phone,
												"fax"            => $phone,
												"email"          => $toEmail,
											),
										);

										if(empty($cvv)){
											unset($transactionData['payment_method']['card']['cvc']);
										}

									}
									
									$responseCode = 300;
									$responseId = '';
									$gatewayTransaction              = new Fluidpay();
									$gatewayTransaction->environment = $this->gatewayEnvironment;
									$gatewayTransaction->apiKey      = $get_gateway['gatewayUsername'];
									$result = $gatewayTransaction->processTransaction($transactionData);

									if ($result['status'] == 'success') {
										$responseId = $result['data']['id'];
										$responseCode = $result['data']['response_code'];
		                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$txnID      = $in_data['TxnID'];  
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
										
										$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('qb_test_invoice',$condition, $data);
									}else {
		                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$err_msg      = $getwayResponse['status']      = $getwayResponse['error']['message'];
										$getwayResponse['id'] = (isset($getwayResponse['error']['transaction_id'])) ? $getwayResponse['error']['transaction_id'] :'TXNFAILED'.time();
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$getwayResponse['responsetext'].'</strong> </div>');  
										redirect($_SERVER['HTTP_REFERER']);
									}
											
									$transaction['transactionID']      = $responseId;
									$transaction['transactionStatus']  = $result['msg'];
									$transaction['transactionCode']    = $responseCode;
									$transaction['transactionType']    = 'sale';
									$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
									$transaction['transactionModified']= date('Y-m-d H:i:s'); 
									$transaction['invoiceTxnID']       = $in_data['TxnID'];
									$transaction['gatewayID']          = $get_gateway['gatewayID'];
									$transaction['transactionGateway'] = $gateway;	
									$transaction['customerListID']     = $in_data['ListID'];
									$transaction['transactionAmount']  = $payamount;
									$transaction['merchantID']         = $marchant_id;
									if($eCheckStatus == '1'){
										$transaction['gateway']            = ($gateway == 11 ? FluidGatewayName : BASYSGatewayName)." ECheck";
									}else{
										$transaction['gateway']            = ($gateway == 11 ? FluidGatewayName : BASYSGatewayName);
									}
									
									$transaction['resellerID']         = $resellerID;
									$transaction['paymentType']         = $scheduleID;
									if(!empty($transactionByUser)){
									    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',$transaction);

									if ($result['status'] == 'success') 
									{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
										$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											
										redirect($thankyou);		
									}
								}
							break;
							case "14":
									include APPPATH . 'third_party/Cardpointe.class.php';
									$cardpointeuser   = $get_gateway['gatewayUsername'];
									$cardpointepass   = $get_gateway['gatewayPassword'];
									$cardpointeMerchID = $get_gateway['gatewayMerchantID'];
									$cardpointeSiteName  = $get_gateway['gatewaySignature'];

									if($payamount > 0){
									
										$amount = $payamount;
										$calamount = round($amount,2);
										$client = new CardPointe();
										$fullName = $fname . ' ' . $lname;
										if($eCheckStatus == '1'){
											$result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountNumber, $routeNumber, $calamount, $acct_type, $fullName, $address, $city, $state,$zip);
										}else{
											$card_no = $this->security->xss_clean($this->input->post('cardNumber'));
											$expmonth = $this->security->xss_clean($this->input->post('cardExpiry'));
											$expyear = $this->security->xss_clean($this->input->post('cardExpiry2'));
											$cvv = $this->security->xss_clean($this->input->post('cardCVC'));
											$expyear   = substr($expyear,2);
											if(strlen($expmonth)==1){
												$expmonth = '0'.$expmonth;
											}
											$expry    = $expmonth.$expyear;
	
											$result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $calamount, $cvv, $fullName, $address, $city, $state,$zip);
										}
										
										$responseCode = 300;
										$responseId = 'TXNFAILED' . time();
	
										if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
											$responseId = $result['retref'];
											$responseCode = 100;
											$txnID      = $in_data['TxnID'];  
											
											$pay        = $payamount;
											$remainbal  = $in_data['BalanceRemaining']-$payamount;
											if($remainbal == '0')
												$ispaid 	 = 'true';
											else
												$ispaid 	 = 'false';
												
											$app        = $in_data['AppliedAmount']-$payamount;
											$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											$condition  = array('TxnID'=>$in_data['TxnID'] );
											$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										}else {
											$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$err_msg      = $getwayResponse['status']      = $result['resptext'];
											$getwayResponse['id'] = (isset($getwayResponse['error']['transaction_id'])) ? $getwayResponse['error']['transaction_id'] :'TXNFAILED'.time();
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:'.$result['resptext'].'</strong> </div>');  
											redirect($_SERVER['HTTP_REFERER']);
										}
												
										$transaction['transactionID']      = $responseId;
										$transaction['transactionStatus']  = $result['resptext'];
										$transaction['transactionCode']    = $responseCode;
										$transaction['transactionType']    = 'sale';
										$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
										$transaction['transactionModified']= date('Y-m-d H:i:s'); 
										$transaction['invoiceTxnID']       = $in_data['TxnID'];
										$transaction['gatewayID']          = $get_gateway['gatewayID'];
										$transaction['transactionGateway'] = $gateway;	
										$transaction['customerListID']     = $in_data['ListID'];
										$transaction['transactionAmount']  = $payamount;
										$transaction['merchantID']         = $marchant_id;
										if($eCheckStatus == '1'){
											$transaction['gateway']            = "CardPointe ECheck";
										}else{
											$transaction['gateway']            = "CardPointe";
										}
										
										$transaction['resellerID']         = $resellerID;
										$transaction['paymentType']         = $scheduleID;
										if(!empty($transactionByUser)){
											$transaction['transaction_by_user_type'] = $transactionByUser['type'];
											$transaction['transaction_by_user_id'] = $transactionByUser['id'];
										}
										if($custom_data_fields){
	                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
	                                    }
										$id = $this->general_model->insert_row('customer_transaction',$transaction);
	
										if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') 
										{
											if(!empty($savepaymentinfo)){
												$cardId =  $this->card_model->process_card($card_data);
											}
											//save card process end
											if(!empty($sendrecipt)){
												$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
											}
											
											$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
											$this->session->set_userdata("tranID",$transaction['transactionID'] );
											$this->session->set_userdata("sess_invoice_id",$in_data); 
												
											redirect($thankyou);		
										}
									}
							break;

							case "12":

									include APPPATH . 'third_party/TSYS.class.php';
									
				                    
				                    $deviceID = $get_gateway['gatewayMerchantID'].'01';            
				                    $gatewayTransaction              = new TSYS();
				                    $gatewayTransaction->environment = $this->gatewayEnvironment;
				                    $gatewayTransaction->deviceID = $deviceID;
				                    $result = $gatewayTransaction->generateToken($get_gateway['gatewayUsername'],$get_gateway['gatewayPassword'],$get_gateway['gatewayMerchantID']);
				                    $generateToken = '';
				                    $responseErrorMsg = '';
				                    
				                    if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
				                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
										$responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
				                    }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
				                        $generateToken = $result['GenerateKeyResponse']['transactionKey'];
				                        
				                    }
				                    
				                    $gatewayTransaction->transactionKey = $generateToken;

				                    if($eCheckStatus == '1'){
										$transaction['Ach'] = array(
						                    "deviceID"              => $deviceID,
						                    "transactionKey"        => $generateToken,
						                    "transactionAmount"     => (int)($payamount * 100),
						                    "accountDetails"        => array(
						                            "routingNumber" => $routeNumber,
						                            "accountNumber" => $accountNumber,
						                            "accountType"   => strtoupper($acct_type),
						                            "accountNotes"  => "count",
						                            "addressLine1"  => $address1,
						                            "zip"           => ($zip != '')?$zip:'None',
						                            "city"          => ($city != '')?$city:'None'
						                    ),
						                    "achSecCode"                => "WEB",
						                    "originateDate"             => date('Y-m-d'),
						                    "addenda"                   => "addenda",
						                    "firstName"                 => (($fname != ''))?$fname:'None',
						                    "lastName"                  => (($lname != ''))?lfname:'None',
						                    "addressLine1"              => ($address != '')?$address:'None',
						                    "zip"                      => ($zip != '')?$zip:'None',
						                    "city"                      => ($city != '')?$city:'None'  
						                );

									}else{
										$exyear1   = substr($expyear,2);
										if(empty($exyear1)){
                        					$exyear1  = $exyear;
                      					}
					                    if(strlen($expmonth)==1){
					                      $expmonth = '0'.$expmonth;
					                    }
					                    $expry    = $expmonth.'/'.$exyear1;

					                    $transaction['Sale'] = array(
					                                "deviceID"                          => $deviceID,
					                                "transactionKey"                    => $generateToken,
					                                "cardDataSource"                    => "MANUAL",  
					                                "transactionAmount"                 => (int)($payamount * 100),
					                                "currencyCode"                      => "USD",
					                                "cardNumber"                        => $cnumber,
					                                "expirationDate"                    => $expry,
					                                "cvv2"                              => $cvv,
					                                "addressLine1"                      => ($address != '')?$address:'None',
					                                "zip"                               => ($zip != '')?$zip:'None',
					                                "orderNumber"                       => $invoice_no,
					                                "firstName"                         => (($fname != ''))?$fname:'None',
					                                "lastName"                          => (($lname != ''))?$lname:'None',
					                                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
					                                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
					                                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
					                                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
					                                "terminalOutputCapability"          => "DISPLAY_ONLY",
					                                "maxPinLength"                      => "UNKNOWN",
					                                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
					                                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
					                                "cardPresentDetail"                 => "CARD_PRESENT",
					                                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
					                                "cardholderAuthenticationEntity"    => "OTHER",
					                                "cardDataOutputCapability"          => "NONE",

					                                "customerDetails"   => array( 
					                                    "contactDetails" => array(
					                                        "addressLine1"=> ($address != '')?$address:'None',
					                                         "addressLine2"  => ($address2 != '')?$address2:'None',
					                                        "city"=>($city != '')?$city:'None',
					                                        "zip"=>($zip != '')?$zip:'None',
					                                    ),
					                                    "shippingDetails" => array( 
					                                        "firstName"=>(($fname != ''))?$fname:'None',
					                                        "lastName"=>(($lname != ''))?$lname:'None',
					                                        "addressLine1"=>($address != '')?$address:'None',
					                                         "addressLine2" => ($address2 != '')?$address2:'None',
					                                        "city"=>($city != '')?$city:'None',
					                                        "zip"=>($zip != '')?$zip:'None' 
					                                       
					                                     )
					                                )
						                );
						               	if($cvv == ''){
						                    unset($transaction['Sale']['cvv2']);
						                }
									}

				                  	$responseType = 'SaleResponse';
									if($generateToken != ''){
				                        $result = $gatewayTransaction->processTransaction($transaction);
				                    }else{
				                        $responseType = 'GenerateKeyResponse';
				                    }

				                  	$trID='';
									if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
	                  				{
	                  					$responseErrorMsg = $result[$responseType]['responseMessage'];
	                  					$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);

	                  					$trID       = $result[$responseType]['transactionID'];
										$code		=  '200';
										$txnID      = $in_data['TxnID'];  
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
										$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
	                  				}else{
	                  					$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
	                  					$err_msg = $result[$responseType]['responseMessage'];
					                    if($responseErrorMsg != ''){
					                        $err_msg = $responseErrorMsg;
					                    }

					                    $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
										redirect($_SERVER['HTTP_REFERER']);
	                  				}
	                  				$transaction1 = [];
									$transaction1['transactionID']       = $trID;
									$transaction1['transactionStatus']   = $responseErrorMsg;
									$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
									$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
									$transaction1['transactionCode']     = $code;  
									$transaction1['invoiceTxnID']        = $in_data['TxnID'];
									$transaction1['transactionType']     = 'sale';	
									$transaction1['gatewayID']           = $get_gateway['gatewayID'];
									$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
									$transaction1['customerListID']      = $in_data['ListID'];
									$transaction1['transactionAmount']   = $payamount;
									$transaction1['merchantID']          = $marchant_id;
									if ($eCheckStatus == '1') {
										$transaction1['gateway'] = TSYSdGatewayName." ECheck";
									} else {
										$transaction1['gateway'] = TSYSdGatewayName;
									}
									
									$transaction1['resellerID']          = $resellerID;
									$transaction1['paymentType']         = $scheduleID;
									$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction1['transactionCode']);
									if(!empty($transactionByUser)){
									    $transaction1['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction1['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction1['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',$transaction1); 
									
									if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
									{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success', '<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
										$this->session->set_userdata("tranID",$transaction1['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											
										redirect($thankyou);		
									}
                  				
								
									
								break;
							
							case "8":
							
							$this->load->config('cyber_pay');
						
							$flag  = 'true';
							
							
							$phone="4158880000"; $email="test@gmail.com"; $companyName='Dummy Company';
							
							$option =array();
									$option['merchantID']     = $get_gateway['gatewayUsername'];
									$option['apiKey']         = $get_gateway['gatewayPassword'];
									$option['secretKey']      = $get_gateway['gatewaySignature'];
									
									if($this->config->item('mode') == 0){
										$sandbox = TRUE;
									}else{
										$sandbox = FALSE;
									}
									if($sandbox == 1)
									$env   = $this->config->item('SandboxENV');
									else
									$env   = $this->config->item('ProductionENV');
									$option['runENV']      = $env;
									$amount =  $payamount;
							$commonElement = new CyberSource\ExternalConfiguration($option);
							$config = $commonElement->ConnectionHost();
							$merchantConfig = $commonElement->merchantConfigObject();
							$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
							$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
							
							$cliRefInfoArr = [
								"code" => "test_payment"
							];
							$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
							
							if ($flag == "true")
							{
								$processingInformationArr = [
									"capture" => true, "commerceIndicator" => "internet"
								];
							}
							else
							{
								$processingInformationArr = [
									"commerceIndicator" => "internet"
								];
							}
							$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
			
							$amountDetailsArr = [
								"totalAmount" => $payamount,
								"currency" => CURRENCY,
							];
							$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
							
							$billtoArr = [
								"firstName" => $fname,
								"lastName"  =>$lname,
								"address1"  => $address1,
								"postalCode"=> $zipcode,
								"locality"  => $city,
								"administrativeArea" => $state,
								"country"  => $country,
								"phoneNumber" => $phone,
								"company"  => $companyName,
								"email"    => $email
							];
							$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
							
							$orderInfoArr = [
								"amountDetails" => $amountDetInfo, 
								"billTo" => $billto
							];
							$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
							
							$paymentCardInfo = [
								"expirationYear" => $exyear,
								"number" => $card_no,
								"securityCode" => $cvv,
								"expirationMonth" => $expmonth
							];
							$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
							
							$paymentInfoArr = [
								"card" => $card
							];
							$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
			
							$paymentRequestArr = [
								"clientReferenceInformation" =>$client_reference_information, 
								"orderInformation" =>$order_information, 
								"paymentInformation" =>$payment_information, 
								"processingInformation" =>$processingInformation
							];
							$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
							
							$api_response = list($response, $statusCode, $httpHeader) = null;
							$tr_type  = 'sale'; 
							try
							{
								//Calling the Api
								$api_response = $api_instance->createPayment($paymentRequest);
								
							
								
								if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
								{
									$trID =   $api_response[0]['id'];
									$msg  =   $api_response[0]['status'];
									$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
									$code =   '200';
									$tr_type  = 'sale';
									$result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID,'paymentType' => $scheduleID );
														$code_data ="SUCCESS";
													
																$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong>Payment Successfully Updated</strong></div>');  
															$txnID      = $in_data['TxnID'];  
															$ispaid 	 = 'true';
															$pay        = $payamount;
															$remainbal  = $in_data['BalanceRemaining']-$payamount;
															$app        = $in_data['AppliedAmount']-$payamount;
									
															$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
															$condition  = array('TxnID'=>$in_data['TxnID'] );
															$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);   	
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], false, $transactionByUser, $custom_data_fields);												 
															
															if(!empty($savepaymentinfo)){
																	$cardId =  $this->card_model->process_card($card_data);
																}
																//save card process end
																if(!empty($sendrecipt)){
																	$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
																}
																
																$this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid </strong></div>'); 
																$this->session->set_userdata("tranID",$api_response[0]['id'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
																redirect($thankyou);
													}
													else
													{
														$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
														
																	
													$trID =   $api_response[0]['id'];
													$msg  =   $api_response[0]['status'];
													$code =   $api_response[1];
													
													$tr_type  = 'sale';
													$result =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID,'paymentType' => $scheduleID );
															$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'. $msg .'</div>'); 
															$trid = $this->general_model->insert_gateway_transaction_data($result, 'sale',   $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$crtxnID='',$resellerID, $in_data['TxnID'], false, $transactionByUser, $custom_data_fields);												 
															redirect($_SERVER['HTTP_REFERER']);
														}
							}  
								catch(Cybersource\ApiException $e)
							{
								
								$error = $e->getMessage();
								$this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
									redirect($_SERVER['HTTP_REFERER']);
							}
						
							
							break; 
							
							case "15": 
								if($payamount > 0){
									
									$amount = $payamount;

									$card_no = $this->security->xss_clean($this->input->post('cardNumber'));
									$expmonth = $this->security->xss_clean($this->input->post('cardExpiry'));
									$expyear = $this->security->xss_clean($this->input->post('cardExpiry2'));
									$cvv = $this->security->xss_clean($this->input->post('cardCVC'));
									if(strlen($expmonth)==1){
										$expmonth = '0'.$expmonth;
									}
										
									
									$responseCode = 300;
									$responseId = '';

									// PayArc Payment Gateway, set enviornment and secret key
									$this->payarcgateway->setApiMode($this->gatewayEnvironment);
									$this->payarcgateway->setSecretKey($get_gateway['gatewayUsername']);

									// Create Credit Card Token
									$address_info = ['address_line1' => $address, 'address_line2' => $address2, 'state' => $state, 'country' => ''];

									$token_response = $this->payarcgateway->createCreditCardToken($card_no, $expmonth, $expyear, $cvv, $address_info);
					
									$token_data = json_decode($token_response['response_body'], 1);
					
									if(isset($token_data['status']) && $token_data['status'] == 'error'){
										
										$this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->gatewayEnvironment,'accessKey'=>$get_gateway['gatewayUsername'], 'card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
										// Error while creating the credit card token
										$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										
										$err_msg = $token_data['message'];

										$result['message'] = $err_msg;
										$getwayResponse['status'] = 'FAILED';

										$getwayResponse['id'] = 'TXNFAILED'.time();
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$err_msg.'</strong> </div>');  
										redirect($_SERVER['HTTP_REFERER']);
					
									} else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {
					
										// If token created
										$token_id = $token_data['data']['id'];
					
										$charge_payload = [];
					
										$charge_payload['token_id'] = $token_id;
										
										if(!empty($toEmail) && filter_var($toEmail, FILTER_VALIDATE_EMAIL)) {
											$charge_payload['email'] = $toEmail; // Customer's email address.
										}

										if(!empty($phone)){
											$charge_payload['phone_number'] = $phone; // Customer's contact phone number..
										}
					
										$charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD
					
										$charge_payload['currency'] = 'usd'; 
					
										$charge_payload['capture'] = 1; // 0 for authorize and 1 for capture instantly
					
										$charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.
					
										if(isset($inputData['zipcode']) && $inputData['zipcode']) {
											$charge_payload['ship_to_zip'] = $inputData['zipcode']; 
										};
					
										$charge_payload['statement_description'] = 'Invoice Payment';
					
										$charge_response = $this->payarcgateway->createCharge($charge_payload);
					
										$result = json_decode($charge_response['response_body'], 1);

										// Handle Card Decline Error
										if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
										{
											$result['message'] = $result['data']['failure_message'];
										}
					
										if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
					
											$responseId = $result['data']['id'];
											$responseCode = $result['data']['response_code'];
											$result['message'] = 'Payment Success';
											$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$txnID      = $in_data['TxnID'];  
											$ispaid 	 = 'true';
											$pay        = $payamount;
											$remainbal  = $in_data['BalanceRemaining']-$payamount;
											$app        = $in_data['AppliedAmount']-$payamount;
											
											$data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal );
											$condition  = array('TxnID'=>$in_data['TxnID'] );
											$this->general_model->update_row_data('qb_test_invoice',$condition, $data);
										}else {

											

											$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
											$err_msg = $result['message'];
											$getwayResponse['status'] = 'FAILED';

											$getwayResponse['id'] = 'TXNFAILED'.time();
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - '.$err_msg.'</strong> </div>');  
											redirect($_SERVER['HTTP_REFERER']);
										}
									}
											
									$transaction['transactionID']      = $responseId;
									$transaction['transactionStatus']  = $result['message'];
									$transaction['transactionCode']    = $responseCode;
									$transaction['transactionType']    = 'sale';
									$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
									$transaction['transactionModified']= date('Y-m-d H:i:s'); 
									$transaction['invoiceTxnID']       = $in_data['TxnID'];
									$transaction['gatewayID']          = $get_gateway['gatewayID'];
									$transaction['transactionGateway'] = $gateway;	
									$transaction['customerListID']     = $in_data['ListID'];
									$transaction['transactionAmount']  = $payamount;
									$transaction['merchantID']         = $marchant_id;
									
									$transaction['gateway']            = (PayArcGatewayName);
									
									$transaction['resellerID']         = $resellerID;
									$transaction['paymentType']        = $scheduleID;
									if(!empty($transactionByUser)){
									    $transaction['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',$transaction);

									if (isset($result['data']) && $result['data']['object']== 'Charge') 
									{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
										$this->session->set_userdata("tranID",$transaction['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											
										redirect($thankyou);		
									}
								}
							break;
							/** Maverick Payment STart */
							case "17":

								// Maverick Payment Gateway
								$this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
								$this->maverickgateway->setTerminalId($get_gateway['gatewayPassword']);
								$this->maverickgateway->setAccessToken($get_gateway['gatewayUsername']);

								

								if($eCheckStatus == '1'){
									// get DBA id
									$dbaId = $this->maverickgateway->getDba(true);

									// electronic Sale
									$request_payload = [
										'amount'          => $payamount,
										'routingNumber'   => $routeNumber,
										'accountName'     => $accountName,
										'accountNumber'   => $accountNumber,
										'accountType'     => ucwords($acct_type), // Checking or Savings
										'transactionType' => 'Debit', // Debit or Credit
										'customer' => [
											'email'     => $email,
											'firstName' => $fname,
											'lastName'  => $lname,
											"address1"  => $address,
											"address_2" => $address2,
											"city"      => $city,
											"state"     => $state,
											"zipCode"   => $zip,
											"country"   => $country,
											"phone"     => $phone,
										],
										'dba' => [
											'id' => $dbaId,
										],
									];

									// Process ACH
									$r = $this->maverickgateway->processAch($request_payload);


								}else{
									$exyear   = substr($expyear,-2);
									if(strlen($expmonth)==1){
									  $expmonth = '0'.$expmonth;
									}
									$expry    = $expmonth.'/'.$exyear;

									// Sale Payload
									$request_payload = [
										'level' => 1,
										'threeds' => [
											'id' => null,
										],
										'amount' => $payamount,
										'card' => [
											'number' => $cnumber,
											'cvv'    => $cvv,
											'exp'    => $expry,
											'save'   => 'No',
											'address' => [
												'street' => $address1,
												'city' => $city,
												'state' => $state,
												'country' => $country,
												'zip' => $zip,
											]
										],
										'contact' => [
											'name'   => $fname.' '.$lname,
											'email'  => $email,
											'phone' => $phone,
										]
									];
				
									// Process Sale
									$r = $this->maverickgateway->processSale($request_payload);
								}

								$result = [];

								$rbody = json_decode($r['response_body'], true);

								$result['response_code'] = $r['response_code'];

								$result['data'] = $rbody;

								// Response                
								if($r['response_code'] >= 200 && $r['response_code'] < 300) {
									if(isset($rbody['id']) && $rbody['id']){
										$result['status'] = 'success';
										$result['msg'] = $result['message'] = 'Payment success.';
										$result['data']['id'] =  $rbody['id'];
									} else {
										$result['status'] = 'failed';
										$result['msg'] = $result['message'] = 'Payment failed.';    
									}

								} else {
									$result['status'] = 'failed';
									$result['msg'] = $result['message'] = $rbody['message'];
								}

								$responseType = 'SaleResponse';
								$trID='';

								if ($result['status'] == 'success') 
								{
									$responseErrorMsg = 'Payment successfull';
									$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);

									$trID       = $result['data']['id'];
									$code		=  '200';
									$txnID      = $in_data['TxnID'];  
									$ispaid 	 = 'true';
									$pay        = $payamount;
									$remainbal  = $in_data['BalanceRemaining']-$payamount;
									$app        = $in_data['AppliedAmount']-$payamount;
									$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
									
									$condition  = array('TxnID'=>$in_data['TxnID'] );
									$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);

								} else {
									$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
									$err_msg = $responseErrorMsg = $result['message'];
									$this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');
									redirect($_SERVER['HTTP_REFERER']);
								}
								
								$transaction1 = [];
								$transaction1['transactionID']       = $trID;
								$transaction1['transactionStatus']   = $responseErrorMsg;
								$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
								$transaction1['transactionModified'] = date('Y-m-d H:i:s'); 
								$transaction1['transactionCode']     = $code;  
								$transaction1['invoiceTxnID']        = $in_data['TxnID'];
								$transaction1['transactionType']     = 'sale';	
								$transaction1['gatewayID']           = $get_gateway['gatewayID'];
								$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
								$transaction1['customerListID']      = $in_data['ListID'];
								$transaction1['transactionAmount']   = $payamount;
								$transaction1['merchantID']          = $marchant_id;
								if ($eCheckStatus == '1') {
									$transaction1['gateway'] = MaverickGatewayName." ECheck";
								} else {
									$transaction1['gateway'] = MaverickGatewayName;
								}
								
								$transaction1['resellerID']          = $resellerID;
								$transaction1['paymentType']         = $scheduleID;
								$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction1['transactionCode']);
								if(!empty($transactionByUser)){
									$transaction1['transaction_by_user_type'] = $transactionByUser['type'];
									$transaction1['transaction_by_user_id'] = $transactionByUser['id'];
								}
								if($custom_data_fields){
                                    $transaction1['custom_data_fields']  = json_encode($custom_data_fields);
                                }
								$id = $this->general_model->insert_row('customer_transaction',$transaction1); 
								
								if ($result['status'] == 'success') 
								{
									if(!empty($savepaymentinfo)){
										$cardId =  $this->card_model->process_card($card_data);
									}
									//save card process end
									if(!empty($sendrecipt)){
										$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
									}
									
									$this->session->set_flashdata('success', '<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
									$this->session->set_userdata("tranID",$transaction1['transactionID'] );
									$this->session->set_userdata("sess_invoice_id",$in_data); 
										
									redirect($thankyou);		
								}
							break;
							/** Maverick Payment Ends */

							case "16":

									include APPPATH . 'third_party/EPX.class.php';

									$CUST_NBR = $get_gateway['gatewayUsername'];
					                $MERCH_NBR = $get_gateway['gatewayPassword'];
					                $DBA_NBR = $get_gateway['gatewaySignature'];
					                $TERMINAL_NBR = $get_gateway['extra_field_1'];
					                $amount = number_format($payamount,2,'.','');
					                $transaction = array(
			                                'CUST_NBR' => $CUST_NBR,
			                                'MERCH_NBR' => $MERCH_NBR,
			                                'DBA_NBR' => $DBA_NBR,
			                                'TERMINAL_NBR' => $TERMINAL_NBR,
			                                'AMOUNT' => $amount,
			                                'TRAN_NBR' => rand(1,10),
			                                'BATCH_ID' => time(),
			                                'VERBOSE_RESPONSE' => 'Y',
			                        );
			                        if($fname != ''){
			                            $transaction['firstName'] = $fname;
			                        }
			                        if($lname != ''){
			                            $transaction['lastName'] = $lname;
			                        }
									
				                    if($eCheckStatus == '1'){
				                    	$transaction['RECV_NAME'] = $accountName;
			                            $transaction['ACCOUNT_NBR'] = $accountNumber;
			                            $transaction['ROUTING_NBR'] = $routeNumber;

			                            if($accountType == 'savings'){
			                                $transaction['TRAN_TYPE'] = 'CKS2';
			                            }else{
			                                $transaction['TRAN_TYPE'] = 'CKC2';
			                            }
			                            if($address != ''){
			                                $transaction['ADDRESS'] = $address;
			                            }
			                            if($city != ''){
			                                $transaction['CITY'] = $city;
			                            }
			                            if( $zip != ''){
			                                $transaction['ZIP_CODE'] = $zip;
			                            }
										

									}else{
										
					                    if (strlen($expmonth) == 1) {
			                                $expmonth = '0' . $expmonth;
			                            }
			                            $exyear1  = substr($expyear, 2);
			                            $transaction['EXP_DATE'] = $exyear1.$expmonth;
			                            $transaction['ACCOUNT_NBR'] = $cnumber;
			                            $transaction['TRAN_TYPE'] = 'CCE1';

			                            if($address != ''){
			                                $transaction['ADDRESS'] = $address;
			                            }
			                            if($city != ''){
			                                $transaction['CITY'] = $city;
			                            }
			                            if($zip != ''){
			                                $transaction['ZIP_CODE'] = $zip;
			                            }
			                            if($cvv && !empty($cvv)){
						                    $transaction['CVV2'] = $cvv;
						                }
									}


				                  	$gatewayTransaction              = new EPX();
                        			$result = $gatewayTransaction->processTransaction($transaction);

									if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                        			{
	                  					$nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);

	                  					$trID       = $responseId = $transactionID = $result['AUTH_GUID'];
										$code		=  '100';
										$txnID      = $in_data['TxnID'];  
										$ispaid 	 = 'true';
										$pay        = $payamount;
										$remainbal  = $in_data['BalanceRemaining']-$payamount;
										$app        = $in_data['AppliedAmount']-$payamount;
										$data      = array('IsPaid'=>$ispaid, 'AppliedAmount'=>$app , 'BalanceRemaining'=>$remainbal,'TimeModified'=>date('Y-m-d H:i:s') );
											
										$condition  = array('TxnID'=>$in_data['TxnID'] );
										$this->general_model->update_row_data('chargezoom_test_invoice',$condition, $data);
										$msg = 'success';
	                  				}else{
	                  					$code		=  '400';
	                  					$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
	                  					$msg = $result['AUTH_RESP_TEXT'];
	                  					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>'.$msg.'</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
	                  				}
	                  				$transaction1 = [];
									$transaction1['transactionID']       = $trID;
									$transaction1['transactionStatus']   = $msg;
									$transaction1['transactionDate']     = date('Y-m-d H:i:s');  
									$transaction1['transactionModified']     = date('Y-m-d H:i:s'); 
									$transaction1['transactionCode']     = $code;  
									$transaction1['invoiceTxnID']        = $in_data['TxnID'];
									$transaction1['transactionType']     = 'sale';	
									$transaction1['gatewayID']           = $get_gateway['gatewayID'];
									$transaction1['transactionGateway']  = $get_gateway['gatewayType'] ;					
									$transaction1['customerListID']      = $in_data['ListID'];
									$transaction1['transactionAmount']   = $payamount;
									$transaction1['merchantID']          = $marchant_id;
									if ($eCheckStatus == '1') {
										$transaction1['gateway'] = EPXGatewayName." ECheck";
									} else {
										$transaction1['gateway'] = EPXGatewayName;
									}
									
									$transaction1['resellerID']          = $resellerID;
									$transaction1['paymentType']         = $scheduleID;
									$CallCampaign = $this->general_model->triggerCampaign($marchant_id,$transaction1['transactionCode']);
									if(!empty($transactionByUser)){
									    $transaction1['transaction_by_user_type'] = $transactionByUser['type'];
									    $transaction1['transaction_by_user_id'] = $transactionByUser['id'];
									}
									if($custom_data_fields){
                                        $transaction1['custom_data_fields']  = json_encode($custom_data_fields);
                                    }
									$id = $this->general_model->insert_row('customer_transaction',$transaction1); 
									
									if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                        			{
										if(!empty($savepaymentinfo)){
											$cardId =  $this->card_model->process_card($card_data);
										}
										//save card process end
										if(!empty($sendrecipt)){
											$this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
										}
										
										$this->session->set_flashdata('success', '<div class="alert alert-success"><strong> Successfully Paid </strong></div>');
										$this->session->set_userdata("tranID",$transaction1['transactionID'] );
										$this->session->set_userdata("sess_invoice_id",$in_data); 
											
										redirect($thankyou);		
									}
							break;
							default:
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong> Please select Marchemt gateway.</div>'); 
								redirect($_SERVER['HTTP_REFERER']);
								break;
						}
					}
					}
					else
					{
						$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Invalid Request</div>'); 
						redirect($_SERVER['HTTP_REFERER']);  
					}
           
        }
		else{
                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Transaction Failed - </strong>Invalid Request</div>'); 
        		redirect($_SERVER['HTTP_REFERER']);
        }     
        
	   
   }
   
   
    
    private function safe_decode($string) {
        return base64_decode(strtr($string, '-_-', '+/='));
    }
    
    public function thankyou(){
     $this->load->view('customer/thankyou');   
    }
    
    public function wrong_url(){
	   
	   $this->load->view('customer/wrong_page'); 
	    
	}
	public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
        	$title = 'Sale Payments';
        	$nf_desc = 'A payment for '.$customerName.' was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
        	$type = 1;
        }else{
        	$title = 'Invoice Checkout Payments';
        	$in_data =    $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID'=>$invoiceNumber));
            if(isset($in_data['RefNumber'])){
                $invoiceRefNumber = $in_data['RefNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }
        	$nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
        	$type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

		$condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
		$ref_number     = $in_data['RefNumber'];
		$tr_date        = date('Y-m-d H:i:s');
		$toEmail        = false;
		$company        = $customerName;
		$customer       = $customerName;
		$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payAmount, $tr_date);
		
        return true;
    }
	public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID'=>$invoiceNumber));
        if(isset($in_data['RefNumber'])){
            $invoiceRefNumber = $in_data['RefNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }

}