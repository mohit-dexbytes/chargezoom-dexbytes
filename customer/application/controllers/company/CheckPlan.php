<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
error_reporting(0);
ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
use GlobalPayments\Api\PaymentMethods\ECheck;

include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include APPPATH . 'third_party/Fluidpay.class.php';

class CheckPlan extends CI_Controller {
  private $gatewayEnvironment;  
	function __construct()
	{
		parent::__construct();
		
	  $this->load->config('TSYS');
      $this->load->config('EPX');
	  $this->load->config('globalpayments');
		$this->load->config('usaePay');
		$this->load->config('paytrace');
    $this->load->config('fluidpay');
	  $this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		$this->load->helper('url');
    $this->load->library('session');
    
    $this->load->config('payarc');
    $this->load->library('PayarcGateway');
    
    $this->db1 = $this->load->database('otherdb', TRUE);
	  $this->gatewayEnvironment = $this->config->item('environment');
		if($this->session->userdata('customer_logged_in'))
		{
		   	redirect('login');    
		}
		
	}
	
	public function index(){
	       $port_url = current_url();
	     
	   
	  	redirect('wrong_url');   
	}
	
	
	

	
	public function wrong_url(){
	   
	    $data['primary_nav'] 	= primary_customer_nav();
        	$data['template'] 		= template_variable();
    	$this->load->view('template/template_start', $data);
		$this->load->view('customer/QBO/wrong_page', $data);
	
	    
	}
	
	public function check_out()
	{
	     $data['records'] = '';
	     $port_url = current_url();
	  
	    $marchent_id = $this->uri->segment(2); 
	    $plan = $this->uri->segment(3);
	    $merchid = base64_decode($marchent_id);
      $merchid = $merchid;
	    $con = array('merchantID'=>$merchid);
	    
	    $this->session->unset_userdata("tranID");
      $this->session->unset_userdata("sess_invoice_id");
	    $port_url = current_url();

		 $new_url   = explode('://',$port_url);

		 $logo_data = explode('.',$new_url[1]);
		
		 $l_con    = array('portalprefix'=>$logo_data[0],'merchantID'=>$merchid);
		
		 $rdata    = $this->general_model->get_row_data('tbl_config_setting',$l_con);
		
		 
		 if(empty($rdata))
		 {

			

		    
		 }
		 
	
	    
        
   
	    
	    $m_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID', 'companyName', 'weburl'), array('merchID'=>$merchid));
	    
	    $data['m_data'] = $m_data ;
	     $resellerID = 	$m_data['resellerID']; 
       
        $comp_data     = $this->general_model->get_select_data('chargezoom_test_customer',array('companyID','companyName', 'Contact','FullName',), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
    
      
		
	    $result = $this->general_model->get_row_data('app_integration_setting',$con);
	     $con_sb   = array('merchantDataID' =>$merchid,  'postPlanURL' => $plan);
         $splan = $this->general_model->get_row_json_data('tbl_chargezoom_subscriptions_plan',$con_sb);
         
        $proRateday=0; $nextInvoiceDate=0 ; $proRate=0;
        
        $condition1 = array('planID '=>$splan->planID);
    	$item = $this->general_model->get_table_data('tbl_chargezoom_subscription_plan_item',$condition1);
    	$data['items'] = $item;
      $prorata_heading   = '';
      $currentProDate = date('m/d/Y');
      
      $prorata_data = $this->czsecurity->xssCleanPostInput('prorata_data');
      if(!$prorata_data){
        $prorata_data = 1;
      }

    	$proRateday=0; $nextInvoiceDate=0 ; $proRate=0;
        $sub_amount =$payable=0;   
         $proRateday=0; $nextInvoiceDate=0 ; $proRate=0;$sub_recurring=0;
         $proRateday=0; $nextInvoiceDate=0 ; $proRate=0;$sub_amount=0;
        if(trim($splan->invoiceFrequency)=='mon')
         {
             
              $onetime=$recurring=0;
            
            
            foreach($item as $itm)
            {
                if($itm['oneTimeCharge']==0)
                {
                    $recurring+=$itm['itemQuantity']*$itm['itemRate'];
                    
                }
                if($itm['oneTimeCharge']==1)
                {
                    $onetime+=$itm['itemQuantity']*$itm['itemRate']; 
                }
                 
                
            }
           
          
         $sub_amount = $recurring;
            $data['onetime']   = $onetime;
            $data['recurring'] = $recurring; 
             
             if(trim($splan->proRate)==1)
             {
                 $proRate         = 1;
                 $proRateday      = $splan->proRateBillingDay;
                 $nextInvoiceDate = $splan->nextMonthInvoiceDate;  
                 $lstm            = date('m', strtotime('first day of last month'));
                 $nm              = date('m', strtotime('first day of next month'));
                   $orderdate      = date('Y-m-d');
           
            
              
                 if($proRateday < $nextInvoiceDate)
               
                 $cutoffdate    = date("Y-$lstm-$nextInvoiceDate");
                 else
                 $cutoffdate    = date("Y-m-$nextInvoiceDate");
                
               
              
               
               $billing_data   =  date("Y-m-$proRateday");
                
                $lastbillingdata = date("Y-$lstm-$proRateday");
              
                
                 $subAmount =0;
                  if((strtotime($orderdate)<= strtotime($billing_data)) && strtotime($orderdate) > strtotime($cutoffdate)  )
                  {  
                       
                         $ndate1 = new DateTime($orderdate);
                         $ndate2 = new DateTime($billing_data);
                           $diff = $ndate2->diff($ndate1);
                           $remainingdays = $diff->format("%a")+1;
                     
                           $ndatelast    = new DateTime($lastbillingdata);
                          
                           $diff         = $ndate2->diff($ndatelast);
                         $monthdays = $diff->format("%a");
                       
                         
                       $subAmount   = ($recurring/$monthdays)*$remainingdays ;
						$data['prorata_data'] = ($diff->d)*(1/$remainingdays); 
                       
					  	$subAmount += $onetime;
                     
                       
                       if($splan->freeTrial== 0)
                      {
                       $data['recurring'] = $recurring; 
                      $splan->subscriptionAmount =  $subAmount+$recurring;  
                    }
                    else
                    {
                          $data['recurring']         = $recurring; 
                          $data['recurring_onetime'] = 0.00;
                          $splan->subscriptionAmount =  $recurring; 
                    }

                    $proHeaderDate = date('m/d/Y', strtotime($lastbillingdata .' -1 day'));
                    $prorata_heading = "($currentProDate - $proHeaderDate)";
                      
                  }
                  else
                  {
                      
                      $Y = date('Y', strtotime('first day of next month'));

                      
                          $next = date("$Y-$nm-$proRateday"); 
                         $next1 = date("Y-m-d",strtotime($next));
                        $date1  = date_create($next1);
                
                      $ndate2     = date("Y-m-d",strtotime($billing_data)); 
                     
                       $date2=date_create($ndate2);
                       $diff    =date_diff($date1, $date2);  
                      
                    $monthdays = $diff->format("%a");
                
                      $date1 = new DateTime($orderdate);
                      $date2 = new DateTime($next); 
                      $diff  = $date1->diff($date2);
                
                      $subAmount = ($diff->d)*($recurring/$monthdays);  
					  $data['prorata_data'] = ($diff->d)*(1/$monthdays); 
                      
                         if($splan->freeTrial== 0)
                      {
                       $data['recurring'] = $recurring;
                      $splan->subscriptionAmount =  $subAmount;  
                    }
                    else
                    {
                          $data['recurring'] = $recurring;
                          $data['recurring_onetime'] = 0.00;
                          $splan->subscriptionAmount =  0.00; 
                    }
                      
                    $proHeaderDate = date('m/d/Y', strtotime($next .' -1 day'));
                    $prorata_heading = "($currentProDate - $proHeaderDate)";
                  }
                 
                 
             
                 
             }
             else
             {
                 
				 $proRateday      = $splan->proRateBillingDay;
				 $nextInvoiceDate = $splan->nextMonthInvoiceDate;
				 $splan->subscriptionAmount =  $onetime+$recurring;  
                  if($splan->freeTrial== 0)
                      {
                       $data['recurring'] = $recurring; 
                       
                    }
                    else
                    {
                          $data['recurring']         = $recurring; 
                          $data['recurring_onetime'] = 0.00;
                          $splan->subscriptionAmount =  0.00; 
                    }
             }
                   
                    
                    $data['recurring_onetime1'] =$recurring;
            
         }  
         else
         {
            
                $onetime=$recurring=0;
               foreach($item as $itm)
               {
                if($itm['oneTimeCharge']==0)
                {
                    $recurring+=$itm['itemQuantity']*$itm['itemRate'];
                    
                }
                if($itm['oneTimeCharge']==1)
                {
                    $onetime+=$itm['itemQuantity']*$itm['itemRate']; 
                }
                 
                
            }
            
             $sub_recurring = $recurring;
            $sub_amount = $recurring;
            $data['onetime']   = $onetime;
            if($splan->freeTrial== 0)
              {
              $data['recurring'] = $recurring; 
             $splan->subscriptionAmount =  $recurring; 
            }
            else
            {
                  $data['recurring']          = $recurring; 
                   $data['recurring_onetime'] =  0.00;
              $splan->subscriptionAmount =  $recurring; 
            }
              $data['recurring_onetime1'] =$recurring; 
         }
        
        
            $data['prorata_heading'] = $prorata_heading;
            $data['selected_plan'] = $splan;
     
          
          	$con1  = array("gatewayID" =>$splan->paymentGateway);
	    	$chk_gateway = $this->general_model->get_row_json_data('tbl_merchant_gateway',$con1);
	    	
	    
          
         if($data['selected_plan']->confirm_page_url!="")
         $thank_url = $data['selected_plan']->confirm_page_url;
         else
          $thank_url ='Thankyou';
          
   
 
 
    $payable    = $splan->subscriptionAmount +$data['onetime'];

   $date1 = date('Y-m-d');
  $next_billing_date = $this->general_model->set_next_date($splan->invoiceFrequency,$date1, 1,$proRate,$proRateday);       
          
       $sts='';
      
	 if(!empty($this->input->post(null, true)))
	 {
        $custom_data_fields = [];
	                             if($this->czsecurity->xssCleanPostInput('setMail'))
			     $chh_mail =1;
			     else
			      $chh_mail =0;
			      
		           $this->load->library('form_validation');
                    

                    $this->form_validation->set_rules('email', 'Email', 'required');
                  
                   
                    $this->form_validation->set_rules('first_name', 'First Name', 'required');
                    $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                    
                  
                    $string = base64_encode(mt_rand(1111111,9999999));
		
		
	    if ($this->form_validation->run() == true)
		{
	        $scheduleID = $this->czsecurity->xssCleanPostInput('scheduleID');
	 
		                    	 session_start(); 
                           
                               $sessiondata = array(
                                   'first_name'=>$this->czsecurity->xssCleanPostInput('first_name'),
                                   'last_name'=>$this->czsecurity->xssCleanPostInput('last_name'),
                               'customer'  => $this->czsecurity->xssCleanPostInput('first_name').' '.$this->czsecurity->xssCleanPostInput('last_name'),
                               'email'  =>  $this->czsecurity->xssCleanPostInput('email'),
                               'username' =>  $this->czsecurity->xssCleanPostInput('email'),
                               'password' =>  $string,
                               'fname'  =>  $this->czsecurity->xssCleanPostInput('first_name'),
                               'lname'  => $this->czsecurity->xssCleanPostInput('last_name'),
                               'company'  => $this->czsecurity->xssCleanPostInput('company'),
                               'mobile' =>  $this->czsecurity->xssCleanPostInput('mobile'),
                               'country' =>  $this->czsecurity->xssCleanPostInput('country'),
                               'zip'  =>  $this->czsecurity->xssCleanPostInput('zip'),
                               'add1'     =>  $this->czsecurity->xssCleanPostInput('address'),
                               'add2' =>  $this->czsecurity->xssCleanPostInput('address2'),
                               'city'  => $this->czsecurity->xssCleanPostInput('city'),
                               'state' =>  $this->czsecurity->xssCleanPostInput('state'),
                               
                               'scountry' =>$this->czsecurity->xssCleanPostInput('scountry'),
                               'szip' =>  $this->czsecurity->xssCleanPostInput('szip'),
                               'sadd1' =>  $this->czsecurity->xssCleanPostInput('saddress'),
                               'sadd2'  =>   $this->czsecurity->xssCleanPostInput('saddress2'),
                               'scity'     =>  $this->czsecurity->xssCleanPostInput('scity'),
                               'sstate' =>  $this->czsecurity->xssCleanPostInput('sstate'),
                               'key' => $this->czsecurity->xssCleanPostInput('key'),
                               'token'=> $this->czsecurity->xssCleanPostInput('stripeToken'),
                                'card'     =>  $this->czsecurity->xssCleanPostInput('cardNumber'),
                               'cardfriendlyname'=> $this->czsecurity->xssCleanPostInput('cardType').'-'.substr($this->czsecurity->xssCleanPostInput('cardNumber'), -4),
                               'ccv' =>  $this->czsecurity->xssCleanPostInput('cardCVC'),
                               'exp1' =>  $this->czsecurity->xssCleanPostInput('cardExpiry'),
                               'exp2'  =>  $this->czsecurity->xssCleanPostInput('cardExpiry2'),

                               'accountNumber'    => $this->czsecurity->xssCleanPostInput('accountNumber'),
                                'routeNumber'      => $this->czsecurity->xssCleanPostInput('routeNumber'),
                                'accountName'        => 'Echeck - '.substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'accountfriendlyname' => 'Echeck - '.substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'secCodeEntryMethod'             => 'WEB',
                                'accountType'             => 'checking',
                                'accountHolderType'             => 'business',

                               'accept'  =>  $this->czsecurity->xssCleanPostInput('accept'),
                               'merchantid'     => $merchid,
                               'scheduleID'       => $scheduleID
                             );
                        if($scheduleID == 2){
                            $sessiondata['CardType'] = 'Echeck';
                            $creditStatus = 0;

                            $accountNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountNumber'));
                            $accountfriendlyname = 'Echeck - '.substr($accountNumber, -4);
                            $accountName = $accountfriendlyname;
                            
                            $routeNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('routeNumber'));
                            $acct_holder_type = 'business';
                            $acct_type ='checking';
                            $sec_code =     'WEB';  

                        }else{
                            $sessiondata['CardType'] = $this->czsecurity->xssCleanPostInput('cardType');
                            $creditStatus = 1;
                        }
                        $this->session->set_userdata('records', $sessiondata);
							          $cust_sess= $this->session->userdata('records');
				
			                 $code_data ='';
		 		             $subscriptionAmount = $sub_amount;
		 		
		 	                    $amount = $payable;	
                $amount = round($amount,2);         
                $exyear = $this->czsecurity->xssCleanPostInput('cardExpiry2');
                $expmonth = $this->czsecurity->xssCleanPostInput('cardExpiry');
                $cvv = $this->czsecurity->xssCleanPostInput('cardCVC');
                
                
								if( $amount > 0)
                            {    
        
                              

								if($chk_gateway->gatewayType ==1 || $chk_gateway->gatewayType == 9)
								{
									include APPPATH . 'third_party/nmiDirectPost.class.php';
									$gatewayuser = $chk_gateway->gatewayUsername;
									$gatewaypass = $chk_gateway->gatewayPassword;
									$marchantID =  $chk_gateway->gatewayMerchantID;
									$nmi_data  = array('nmi_user'=>$gatewayuser, 'nmi_password'=>$gatewaypass);
									
									$transaction = new nmiDirectPost($nmi_data);
									
        					  if ($creditStatus) {	
  										$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('cardNumber'));
  										$expmonth =  $this->czsecurity->xssCleanPostInput('cardExpiry');
  										if(strlen($expmonth)==1){
  												$expmonth = '0'.$expmonth;
  											}
  										$exyear   = $this->czsecurity->xssCleanPostInput('cardExpiry2');
  										$exyear   = substr($exyear,2);
  										$expry    = $expmonth.$exyear; 
  										$transaction->setCcExp($expry);
  										$transaction->setCvv($this->czsecurity->xssCleanPostInput('cardCVC'));
  								    // add level III data
                      $level_request_data = [
                          'transaction' => $transaction,
                          'card_no' => $this->czsecurity->xssCleanPostInput('cardNumber'),
                          'merchID' => $merchid,
                          'amount' => $amount,
                          'invoice_id' => '',
                          'gateway' => 1
                      ];
                      $transaction = addlevelThreeDataInTransaction($level_request_data);
        						}else{
                      $transaction->setAccountName($accountName);
                      $transaction->setAccount($accountNumber);
                      $transaction->setRouting($routeNumber);
                      $sec_code =     'WEB';
                      $transaction->setAccountType($acct_type);
                      
                      $transaction->setAccountHolderType($acct_holder_type);
                      $transaction->setSecCode($sec_code);
                      $transaction->setPayment('check');
                    }
        						
										$transaction->setCompany($cust_sess['company']);
										$transaction->setFirstName($cust_sess['fname']);
										$transaction->setLastName($cust_sess['lname']);
										$transaction->setAddress1($cust_sess['add1']);
										$transaction->setAddress2($cust_sess['add2']);
										$transaction->setCountry($cust_sess['country']);
										$transaction->setCity($cust_sess['city']);
										$transaction->setState($cust_sess['state']);
										$transaction->setZip($cust_sess['zip']);
									
									
									 if(!empty($cust_sess['sadd1']!=""))
									 {
  										$transaction->setShippingAddress1($cust_sess['sadd1']);
  										$transaction->setShippingAddress2($cust_sess['sadd2']);
  										$transaction->setShippingCountry($cust_sess['scountry']);
  										$transaction->setShippingCity($cust_sess['scity']);
  										$transaction->setShippingState($cust_sess['sstate']);
  										$transaction->setShippingZip($cust_sess['szip']);
									 }
										$transaction->setEmail($this->czsecurity->xssCleanPostInput('email'));
										$transaction->setAmount($amount);
										$transaction->sale();
										$result = $transaction->execute();

										if($result['response_code'] == '100') 
										{
										   $tr_type='sale';
									        $sts='SUCCESS';
										}
										else
										{
										   $data['records'] = $this->session->userdata('records');
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>'); 
											   $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'</div>';
										}
								}

								if($chk_gateway->gatewayType ==10)
								{
									$apiUsername = $chk_gateway->gatewayUsername;
									$apiKey = $chk_gateway->gatewayPassword;
									$marchantID =  $chk_gateway->gatewayMerchantID;
									if ($creditStatus) {
                    if( !empty($cust_sess['card']) )
                    { 
                      $expmonth =  $cust_sess['exp1'];
                      $exyear   = $cust_sess['exp2'];
                  
                    } 
                    if (strlen($expmonth) > 1 && $expmonth <= 9) {
                      $expmonth = substr($expmonth, 1);
                    }
                    $request_data = array(
                      "amount"          => ($amount * 100),
                      "card"            => array(
                        "name"      => $cust_sess['fname']. " " . $cust_sess['lname'],
                        "number"    => $cust_sess['card'],
                        "exp_month" => $expmonth,
                        "exp_year"  => $exyear,
                        "cvv"       => $cust_sess['ccv'],
                      ),
                      "address" => array(
                        "line1"       => $cust_sess['add1'],
                        "line2"       => $cust_sess['add2'],
                        "city"        => $cust_sess['city'],
                        "state"       => $cust_sess['state'],
                        "postal_code" => $cust_sess['zip'],
                      ),
                    );
                  }else{
                      $request_data = array(
                          "amount" => ($amount * 100),
                          "ach" => array(
                              "name" => $accountName, 
                              "account_number" => $accountNumber, 
                              "routing_number" => $routeNumber, 
                              "phone_number" => '9493019414' ,
                              "sec_code" => $sec_code, 
                              "savings_account" => FALSE , 
                           
                          ),
                          "address" => array(
                              "line1"       => $cust_sess['add1'],
                              "line2"       => $cust_sess['add2'],
                              "city"        => $cust_sess['city'],
                              "state"       => $cust_sess['state'],
                              "postal_code" => $cust_sess['zip'],
                          ),
                         
                      );
                    }

										
										$sdk       = new iTTransaction();
										$getwayResponse    = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);
										if ($getwayResponse['status_code'] == '200' || $getwayResponse['status_code'] == '201') {
											$getwayResponse['status_code'] = 200;
											$code_data ="SUCCESS";
                      $sts="SUCCESS";
											$tr_type='sale';
										}else{
											$err_msg      = $getwayResponse['status']      = $getwayResponse['error']['message'];
											$getwayResponse['id'] = (isset($getwayResponse['error']['transaction_id'])) ? $getwayResponse['error']['transaction_id'] : '';
											$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
											$data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
										}
									$result = $getwayResponse;
								}

								if ($chk_gateway->gatewayType == 11 || $chk_gateway->gatewayType == 13) {

									$apiUsername = $chk_gateway->gatewayUsername;
									$marchantID  = $chk_gateway->gatewayMerchantID;
                  $calamount = $amount * 100;
			            if ($creditStatus) { 
                    $exyear   = substr($exyear,2);
                    if(strlen($expmonth)==1){
                      $expmonth = '0'.$expmonth;
                    }
                    $expry    = $expmonth.$exyear;
                    $transaction = array(
                      "type"                => "sale",
                      "amount"              => round($calamount,2),
                      "currency"            => "USD",
                      "ip_address"          => getClientIpAddr(),
                      "payment_method"      => array(
                        "card" => array(
                          "entry_type"      => "keyed",
                          "number"          => $cust_sess['card'],
                          "expiration_date" => $expry,
                          "cvc"             => $cust_sess['ccv'],
                        ),
                      ),
                      "billing_address"     => array(
                        "first_name"     => $cust_sess['fname'],
                        "last_name"      => $cust_sess['lname'],
                        "company"        => $cust_sess['company'],
                        "address_line_1" => $cust_sess['add1'],
                        "address_line_2" => $cust_sess['add2'],
                        "city"           => $cust_sess['city'],
                        "state"          => $cust_sess['state'],
                        "postal_code"    => $cust_sess['zip'],
                      ),
                      "shipping_address"    => array(
                        "first_name"     => $cust_sess['fname'],
                        "last_name"      => $cust_sess['lname'],
                        "company"        => $cust_sess['company'],
                        "address_line_1" => $cust_sess['add1'],
                        "address_line_2" => $cust_sess['add2'],
                        "city"           => $cust_sess['city'],
                        "state"          => $cust_sess['state'],
                        "postal_code"    => $cust_sess['zip'],
                      ),
                    );
                  }else{

                    $transaction = array(
                        "type"                => "sale",
                        "amount"              => round($calamount,2),
                        "currency"            => "USD",
                        "description"         => 'Payportal Esale',
                        "po_number"           => null,
                        "ip_address"          => getClientIpAddr(),
                        "payment_method"      => array(
                            "ach" => array(
                                "routing_number" => $routeNumber,
                                "account_number" => $accountNumber,
                                "sec_code"       => $sec_code,
                                "account_type"   => $acct_type ,
                            ),
                        ),
                        "billing_address"     => array(
                            "first_name"     => $cust_sess['fname'],
                            "last_name"      => $cust_sess['lname'],
                            "company"        => $cust_sess['company'],
                            "address_line_1" => $cust_sess['add1'],
                            "address_line_2" => $cust_sess['add2'],
                            "city"           => $cust_sess['city'],
                            "state"          => $cust_sess['state'],
                            "postal_code"    => $cust_sess['zip'],
                        ),
                        "shipping_address"    => array(
                           "first_name"     => $cust_sess['fname'],
                            "last_name"      => $cust_sess['lname'],
                            "company"        => $cust_sess['company'],
                            "address_line_1" => $cust_sess['add1'],
                            "address_line_2" => $cust_sess['add2'],
                            "city"           => $cust_sess['city'],
                            "state"          => $cust_sess['state'],
                            "postal_code"    => $cust_sess['zip'],
                        ),
                    );
                  } 
									
									$gatewayTransaction              = new Fluidpay();
									$gatewayTransaction->environment = $this->gatewayEnvironment;
									$gatewayTransaction->apiKey      = $apiUsername;
									$result = $gatewayTransaction->processTransaction($transaction);
			
									if ($result['status'] == 'success') {
										$responseId = $result['data']['id'];
										$code_data = "SUCCESS";
                    $sts="SUCCESS";
										$tr_type   = 'sale';
										$this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
									} else {
										$err_msg      = $result['msg'];
										$data['records'] = $this->session->userdata('records');
			
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
										$data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
									}
                }
                
                /** PayArc */

            

                if ($chk_gateway->gatewayType == 15) {

                  $apiUsername = $chk_gateway->gatewayUsername;
                  $marchantID  = $chk_gateway->gatewayMerchantID;
                  $exyear   = $exyear;
                  if(strlen($expmonth)==1){
                    $expmonth = '0'.$expmonth;
                  }
                  $this->payarcgateway->setApiMode($this->gatewayEnvironment);
                  $this->payarcgateway->setSecretKey($apiUsername);

                  // Create Credit Card Token
                  $address_info = ['address_line1' => $cust_sess['add1'], 'address_line2' => $cust_sess['add2'], 'state' => $cust_sess['state'], 'country' => ''];

                  $token_response = $this->payarcgateway->createCreditCardToken($cust_sess['card'], $expmonth, $exyear, $cust_sess['ccv'], $address_info);

                  $token_data = json_decode($token_response['response_body'], 1);
                  
                  
                  if(isset($token_data['status']) && $token_data['status'] == 'error'){
                    $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->gatewayEnvironment,'accessKey'=>$apiUsername, 'card_no' => $cust_sess['card'], 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cust_sess['ccv'], 'address' => $address_info], $token_data);
                    // Error while creating the credit card token
                      $err_msg      = $token_data['message'];
                      $data['records'] = $this->session->userdata('records');
			
										  $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
										  $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
              
                  } else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {
                  
                      // If token created
                      $token_id = $token_data['data']['id'];
              
                      $charge_payload = [];
              
                      $charge_payload['token_id'] = $token_id;
                      
                      if(isset($cust_sess['email']) && $cust_sess['email']){
                          $charge_payload['email'] = $cust_sess['email']; // Customer's email address.
                      }
                      if(isset($cust_sess['mobile']) && $cust_sess['mobile']){
                          $charge_payload['phone_number'] = $cust_sess['mobile']; // Customer's contact phone number..
                      }
              
                      $charge_payload['amount'] = $amount * 100; // must be in cents and min amount is 50c USD
              
                      $charge_payload['currency'] = 'usd'; 
              
                      $charge_payload['capture'] = '1';
              
                      $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.
              
                      if($cust_sess['zip']) {
                          $charge_payload['ship_to_zip'] = $cust_sess['zip']; 
                      };

                      $charge_payload['statement_description'] = '';
              
                      $charge_response = $this->payarcgateway->createCharge($charge_payload);
              
                      $result = json_decode($charge_response['response_body'], 1);

                      // Handle Card Decline Error
                      if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
                      {
                          $result['message'] = $result['data']['failure_message'];
                      }
                  
                      if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
                        $responseId = $result['data']['id'];
                        $code_data = "SUCCESS";
                        $sts="SUCCESS";
                        $tr_type   = 'sale';
                    
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
                      } else {
                        $err_msg      = $result['message'];
                        $data['records'] = $this->session->userdata('records');
          
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
                        $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
                      }

                  }
                  
                }

								/* Tsys gateway*/
                if ($chk_gateway->gatewayType == 12) 
                {
                    include APPPATH . 'third_party/TSYS.class.php';
                    $marchantID  = $chk_gateway->gatewayMerchantID;

                    $deviceID = $chk_gateway->gatewayMerchantID.'01';            
                    $gatewayTransaction              = new TSYS();
                    $gatewayTransaction->environment = $this->gatewayEnvironment;
                    $gatewayTransaction->deviceID = $deviceID;
                    $result = $gatewayTransaction->generateToken($chk_gateway->gatewayUsername,$chk_gateway->gatewayPassword,$chk_gateway->gatewayMerchantID);
                    $generateToken = '';
                    $responseErrorMsg = '';
                    if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                        $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                    }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                        $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                        
                    }
                    $gatewayTransaction->transactionKey = $generateToken;
                    if ($creditStatus) { 
                      $exyear1   = substr($exyear,2);
                      if(empty($exyear1)){
                        $exyear1  = $exyear;
                      }
                      if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                      }
                      $expry    = $expmonth.'/'.$exyear1;

                      $transaction['Sale'] = array(
                                  "deviceID"                          => $deviceID,
                                  "transactionKey"                    => $generateToken,
                                  "cardDataSource"                    => "MANUAL",  
                                  "transactionAmount"                 => (int)($amount * 100),
                                  "currencyCode"                      => "USD",
                                  "cardNumber"                        => $cust_sess['card'],
                                  "expirationDate"                    => $expry,
                                  "cvv2"                              => $cust_sess['ccv'],
                                  "addressLine1"                      => ($cust_sess['add1'] != '')?$cust_sess['add1']:'None',
                                  "zip"                               => ($cust_sess['zip'] != '')?$cust_sess['zip']:'None',
                                  "orderNumber"                       => mt_rand(10000000, 77777777),
                                  "firstName"                         => (($cust_sess['fname'] != ''))?$cust_sess['fname']:'None',
                                  "lastName"                          => (($cust_sess['lname'] != ''))?$cust_sess['lname']:'None',
                                  "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                  "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                  "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                  "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                  "terminalOutputCapability"          => "DISPLAY_ONLY",
                                  "maxPinLength"                      => "UNKNOWN",
                                  "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                  "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                  "cardPresentDetail"                 => "CARD_PRESENT",
                                  "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                  "cardholderAuthenticationEntity"    => "OTHER",
                                  "cardDataOutputCapability"          => "NONE",

                                  "customerDetails"   => array( 
                                      "contactDetails" => array(
                                          "addressLine1"=> ($cust_sess['add1'] != '')?$cust_sess['add1']:'None',
                                           "addressLine2"  => ($cust_sess['add2'] != '')?$cust_sess['add2']:'None',
                                          "city"=>($cust_sess['city'] != '')?$cust_sess['city']:'None',
                                          "zip"=>($cust_sess['zip'] != '')?$cust_sess['zip']:'None',
                                      ),
                                      "shippingDetails" => array( 
                                          "firstName"=>(($cust_sess['fname'] != ''))?$cust_sess['fname']:'None',
                                          "lastName"=>(($cust_sess['lname'] != ''))?$cust_sess['lname']:'None',
                                          "addressLine1"=>($cust_sess['add1'] != '')?$cust_sess['add1']:'None',
                                           "addressLine2" => ($cust_sess['add2'] != '')?$cust_sess['add2']:'None',
                                          "city"=>($cust_sess['city'] != '')?$cust_sess['city']:'None',
                                          "zip"=>($cust_sess['zip'] != '')?$cust_sess['zip']:'None' 
                                         
                                       )
                                  )
                      );
                      if($cvv == ''){
                          unset($transaction['Sale']['cvv2']);
                      }
                    }else{
                      $transaction['Ach'] = array(
                              "deviceID"              => $deviceID,
                              "transactionKey"        => $generateToken,
                              "transactionAmount"     => (int)($amount * 100),
                              "accountDetails"        => array(
                                      "routingNumber" => $routeNumber,
                                      "accountNumber" => $accountNumber,
                                      "accountType"   => strtoupper($acct_type) ,
                                      "accountNotes"  => "count",
                                      "addressLine1"  => ($cust_sess['add1'] != '')?$cust_sess['add1']:'None',
                                      "zip"           => ($cust_sess['zip'] != '')?$cust_sess['zip']:'None' ,
                                      "city"          => ($cust_sess['city'] != '')?$cust_sess['city']:'None',
                              ),
                              "achSecCode"                => "WEB",
                              "originateDate"             => date('Y-m-d'),
                              "addenda"                   => "addenda",
                              "firstName"                 => (($cust_sess['fname'] != ''))?$cust_sess['fname']:'None',
                              "lastName"                  => (($cust_sess['lname'] != ''))?$cust_sess['lname']:'None',
                              "addressLine1"              => ($cust_sess['add1'] != '')?$cust_sess['add1']:'None',
                              "zip"                      => ($cust_sess['zip'] != '')?$cust_sess['zip']:'None' ,
                              "city"                      => ($cust_sess['city'] != '')?$cust_sess['city']:'None',
                          );
                    }
                    $responseType = 'SaleResponse';
                    if($generateToken != ''){
                        $result = $gatewayTransaction->processTransaction($transaction);
                    }else{
                        $responseType = 'GenerateKeyResponse';
                    }
                  if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
                  {
                            
                    $responseId = $result[$responseType]['transactionID'];
                    $code_data = "SUCCESS";
                    $sts="SUCCESS";
                    $tr_type   = 'sale';
                    $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
                  } else {
                    $err_msg      = $result[$responseType]['responseMessage'];
                    $data['records'] = $this->session->userdata('records');
                    if($responseErrorMsg != ''){
                        $err_msg = $responseErrorMsg;
                    }
      
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - </strong> ' . $err_msg . '</div>');
                    $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
                  }
                }
                /* Tsys gateway*/
                if ($chk_gateway->gatewayType == 16) 
                {
                    include APPPATH . 'third_party/EPX.class.php';
                    $marchantID  = $chk_gateway->gatewayMerchantID;
                    $CUST_NBR = $chk_gateway->gatewayUsername;
                    $MERCH_NBR = $chk_gateway->gatewayPassword;
                    $DBA_NBR = $chk_gateway->gatewaySignature;
                    $TERMINAL_NBR = $chk_gateway->extra_field_1;
                    $amount = number_format($amount,2,'.','');
                    $transaction = array(
                            'CUST_NBR' => $CUST_NBR,
                            'MERCH_NBR' => $MERCH_NBR,
                            'DBA_NBR' => $DBA_NBR,
                            'TERMINAL_NBR' => $TERMINAL_NBR,
                            'AMOUNT' => $amount,
                            'TRAN_NBR' => rand(1,10),
                            'BATCH_ID' => time(),
                            'VERBOSE_RESPONSE' => 'Y',
                    );
                    if($cust_sess['fname'] != ''){
                        $transaction['firstName'] = $cust_sess['fname'];
                    }
                    if($cust_sess['lname'] != ''){
                        $transaction['lastName'] = $cust_sess['lname'];
                    }
                    if ($creditStatus) { 
                        
                        if (strlen($expmonth) == 1) {
                            $expmonth = '0' . $expmonth;
                        }
                        $exyear1  = substr($exyear, 2);
                        $transaction['EXP_DATE'] = $exyear1.$expmonth;
                        $transaction['ACCOUNT_NBR'] = $cust_sess['card'];
                        $transaction['TRAN_TYPE'] = 'CCE1';

                        if($cust_sess['add1'] != ''){
                            $transaction['ADDRESS'] = $cust_sess['add1'];
                        }
                        if($cust_sess['city'] != ''){
                            $transaction['CITY'] = $cust_sess['city'];
                        }
                        if($cust_sess['zip'] != ''){
                            $transaction['ZIP_CODE'] = $cust_sess['zip'];
                        }
                        if($cust_sess['ccv'] && !empty($cust_sess['ccv'])){
                            $transaction['CVV2'] = $cust_sess['ccv'];
                        }
                      
                    }else{
                        $transaction['RECV_NAME'] = $accountName;
                        $transaction['ACCOUNT_NBR'] = $accountNumber;
                        $transaction['ROUTING_NBR'] = $routeNumber;

                        if($acct_type == 'savings'){
                            $transaction['TRAN_TYPE'] = 'CKS2';
                        }else{
                            $transaction['TRAN_TYPE'] = 'CKC2';
                        }
                        if($cust_sess['add1'] != ''){
                            $transaction['ADDRESS'] = $cust_sess['add1'];
                        }
                        if($cust_sess['city'] != ''){
                            $transaction['CITY'] = $cust_sess['city'];
                        }
                        if( $cust_sess['zip'] != ''){
                            $transaction['ZIP_CODE'] = $cust_sess['zip'];
                        }
                      
                    }
                    $gatewayTransaction              = new EPX();
                    $result = $gatewayTransaction->processTransaction($transaction);
                    if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                    {
                            
                        $responseId = $result['AUTH_GUID'];
                        $code_data = "SUCCESS";
                        $sts="SUCCESS";
                        $tr_type   = 'sale';
                        $code       =  '100';
                        $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
                    } else {
                        $err_msg      = $result['AUTH_RESP_TEXT'];
                        $data['records'] = $this->session->userdata('records');
                        $code       =  '401';
                        $responseId = 'TXNFAILED-'.time();
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
                        $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
                    }
                }

								//second gateway
								if($chk_gateway->gatewayType ==2)
								{
									include APPPATH.'third_party/authorizenet_lib/AuthorizeNetAIM.php';
                  $this->load->config('auth_pay');
                  if($this->config->item('auth_test_mode')){
                      $sandbox = TRUE;
                  }else{
                      $sandbox = FALSE;
                  }


									$apiloginID      =$chk_gateway->gatewayUsername;
						        	$transactionKey  =$chk_gateway->gatewayPassword;
									$transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
                  $transaction->setSandbox($sandbox);
									if( !empty($cust_sess['card'])){	
									   
										$card_no  = $cust_sess['card'];
										$expmonth =  $cust_sess['exp1'];
										if(strlen($expmonth)==1){
												$expmonth = '0'.$expmonth;
											}
										$exyear   = substr($cust_sess['exp2'],2);
										$expry    = $expmonth.$exyear;
									}
									$transaction->__set('company',$cust_sess['company']);
									$transaction->__set('first_name', $cust_sess['fname']);
									$transaction->__set('last_name',  $cust_sess['lname']);
									$transaction->__set('address',  $cust_sess['add1']);
									$transaction->__set('country', $cust_sess['country']);
									$transaction->__set('city', $cust_sess['city']);
									$transaction->__set('state', $cust_sess['state']);
									$transaction->__set('phone', $cust_sess['mobile']);
									$transaction->__set('email', $cust_sess['email']);
									
									 if(!empty($cust_sess['sadd1']!=""))
									   {
									       
									   }
									$amount = $amount;
                  if ($creditStatus) {
                      $result = $transaction->authorizeAndCapture($amount,$card_no,$expry);
                  }else{

                      $transaction->setECheck($routeNumber, $accountNumber, $acct_type, $bank_name='Wells Fargo Bank NA', $accountName, $sec_code);

                      $result = $transaction->authorizeAndCapture($amount);

                  }
									
							
									if($result->response_code == '1'  && $result->transaction_id != 0 && $result->transaction_id != '')
									{
									    $sts = "SUCCESS";
									      $tr_type='auth_capture';
									}else{
										   $data['records'] = $this->session->userdata('records');
											$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $result->response_reason_text .'</div>'); 
											 $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong> '.$result->response_reason_text.'</div>';
										}
								}
									//Third gateway
								if($chk_gateway->gatewayType ==3)
								{
                  
                      include APPPATH . 'third_party/PayTraceAPINEW.php';
                      $card_no='';
                      $expmonth='';
                      $exyear='';
                     
                      $payusername   = $chk_gateway->gatewayUsername;
                      $paypassword   = $chk_gateway->gatewayPassword;
                      $integratorId = $chk_gateway->gatewaySignature;

                      $grant_type    = "password";
                      $payAPI = new PayTraceAPINEW();
                        //set the properties for this request in the class
                      $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                     
                      $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
                      if(!$oauth_moveforward)
                      {
                        //Decode the Raw Json response.
                        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
                        $oauth_token = sprintf("Bearer %s",$json['access_token']);

                        $name    = $cust_sess['fname'].' '.$cust_sess['lname'];

                        $address =  $cust_sess['add1'];
                        $country = $cust_sess['country'];
                        $city    = $cust_sess['city'];
                        $state   = $cust_sess['state'];
                        $amount = $amount;    
                        $zipcode  = $cust_sess['zip'];  


                        $invoice_number = mt_rand(10000000, 77777777);

                        
                        /* Manage request data */
                        if ($creditStatus) {
                          

                            if( !empty($cust_sess['card'])){  
                         
                                $card_no  = $cust_sess['card'];
                                $expmonth =  $cust_sess['exp1'];
                                if(strlen($expmonth)==1){
                                    $expmonth = '0'.$expmonth;
                                  }
                                $exyear   = $cust_sess['exp2'];
                                $cvv     =  $cust_sess['ccv'];
                            }

                            $request_data = array(
                                            "amount" => $amount,
                                            "credit_card"=> array (
                                                 "number"=> $card_no,
                                                 "expiration_month"=> $expmonth,
                                                 "expiration_year"=> $exyear ),
                                            "csc"=>$cvv,
                                           'invoice_id'=> $invoice_number,
                                           "billing_address"=> array(
                                                "name"=>$name,
                                                "street_address"=> $address,
                                                "city"=> $city,
                          "state"=> $state,
                          "zip"=> $zipcode
                                                )
                                  );

                            $keyRun = URL_KEYED_SALE;
                        }else{
                            $request_data = array(
                                "amount"          => $amount,
                                "check"     => array(
                                  "account_number"=> $accountNumber,
                                  "routing_number"=> $routeNumber,
                                ),
                                'invoice_id'=> $invoice_number,
                                "integrator_id" => $integratorId,
                                "billing_address" => array(
                                  "name" => $name,
                                  "street_address" => $address,
                                  "city" => $city,
                                  "state" => $state,
                                  "zip" => $zipcode,
                                ),
                              );
                            $keyRun = URL_ACH_SALE;
                        }
                      
                        $request_data = json_encode($request_data);
                       
                        


                         $result1    =  $payAPI->processTransaction($oauth_token,$request_data, $keyRun );   

                        $response = $payAPI->jsonDecode($result['temp_json_response']); 
                                
                          if ( $result1['http_status_code']=='200' )
                          {
                            // add level three data in transaction
                            if($response['success']){


                              if ($creditStatus) {
                                $level_three_data = [
                                    'card_no' => $card_no,
                                    'merchID' => $merchid,
                                    'amount' => $amount,
                                    'token' => $oauth_token,
                                    'integrator_id' => $integratorId,
                                    'transaction_id' => $response['transaction_id'],
                                    'invoice_id' => $invoice_number,
                                    'gateway' => 3,
                                ];
                                addlevelThreeDataInTransaction($level_three_data);
                            }
                            
                             $sts    =  'SUCCESS';  
                               $tr_type='pay_sale';
                              $result = $response;
                          }
                          else
                          {  
                               $data['records'] = $this->session->userdata('records');
                              $err_msg='';
                                if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
                              $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $err_msg .'</div>'); 
                             
                              $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>'. $err_msg .'</div>';
                            }
                          }
                        }
                        else
                        {
                            $data['records'] = $this->session->userdata('records');
                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>'); 
                                   $data['error_msg'] = '<div class="alert alert-danger">   <strong>Error: Authentication not valid.</strong></div>';
                         }      
                        
					    	            
			           }
								if($chk_gateway->gatewayType == 4)
									{
  								    if ($creditStatus) { 
                        $username   = $chk_gateway->gatewayUsername;
                        $password   =  $chk_gateway->gatewayPassword;
                        $signature = $chk_gateway->gatewaySignature;
                        include APPPATH . 'third_party/PayPalAPINEW.php';
                        $this->load->config('paypal');

                        $config = array(
                            'Sandbox' => $this->config->item('Sandbox'),      // Sandbox / testing mode option.
                            'APIUsername' => $username,   // PayPal API username of the API caller
                            'APIPassword' => $password, // PayPal API password of the API caller
                            'APISignature' => $signature,   // PayPal API signature of the API caller
                            'APISubject' => '',     // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                            'APIVersion' => $this->config->item('APIVersion') // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                            );
                    
                          // Show Errors
                          if($config['Sandbox'])
                          {
                            error_reporting(E_ALL);
                            ini_set('display_errors', '1');
                          }
                          $this->load->library('paypal/Paypal_pro', $config);   
          
          
          
                              if(!empty($cust_sess['card']))
                              { 
                                $card     = $cust_sess['card'];
                                $expmonth =  $cust_sess['exp1'];
                                $exyear   =$cust_sess['exp2'];
                                if(strlen($expmonth)==1){
                                  $expmonth = '0'.$expmonth;
                                }

                                $cvv    = $cust_sess['ccv'];
                              }
                               
                               
                              $paymentType    = 'Sale';
                              $companyName        =$cust_sess['company'];
                              $firstName      = $cust_sess['fname'];
                              $lastName       = $cust_sess['lname'];
                              $creditCardType   = 'Visa';
                              $creditCardNumber   = $cust_sess['card'];
                              $expDateMonth     = $cust_sess['exp1'];
                              // Month must be padded with leading zero
                              $padDateMonth     = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                                        
                              $expDateYear=   $exyear;
                              $cvv2Number =   $cvv;
                              $address1 =   $cust_sess['add1'];
                              $address2   = $cust_sess['add2'];
                              $city     =   $cust_sess['city'];
                              $state    = $cust_sess['state'];
                              $zip    = $cust_sess['zip'];
                              $email      = $cust_sess['email'];
                              $phone      = $cust_sess['mobile'];
                              
                              $country='';
                              $currencyID ='';
                              if($cust_sess['country']=="United States" || $cust_sess['country']=="United State"){
                                $country  = 'US'; // US or other valid country code
                                $currencyID = 'USD';
                               
                              }
                              
                              if($cust_sess['country']=="Canada" || $cust_sess['country']=="canada"){
                                $country    = 'CAD';  // US or other valid country code
                                $currencyID = 'CAD';
                              }
                              
                              $DPFields = array(
                              'paymentaction' => 'Sale',  // How you want to obtain payment.  
                                                  //Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                              'ipaddress' => '',              // Required.  IP address of the payer's browser.
                              'returnfmfdetails' => '0'           // Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                            );
                
                                    $CCDetails = array(
                              'creditcardtype' => $creditCardType,          // Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                              'acct' => $creditCardNumber,                // Required.  Credit card number.  No spaces or punctuation.  
                              'expdate' => $padDateMonth.$expDateYear,              // Required.  Credit card expiration date.  Format is MMYYYY
                              'cvv2' => $cvv2Number,                // Requirements determined by your PayPal account settings.  Security digits for credit card.
                               'startdate' => '',               // Month and year that Maestro or Solo card was issued.  MMYYYY
                              'issuenumber' => ''                 // Issue number of Maestro or Solo card.  Two numeric digits max.
                            );
        
                                    $PayerInfo = array(
                              'email' => $email,                // Email address of payer.
                              'payerid' => '',              // Unique PayPal customer ID for payer.
                              'payerstatus' => 'verified',            // Status of payer.  Values are verified or unverified
                              'business' => ''              // Payer's business name.
                            );  
                
                                    $PayerName = array(
                              'salutation' => $cust_sess['company'],            // Payer's salutation.  20 char max.
                              'firstname' => $firstName,              // Payer's first name.  25 char max.
                              'middlename' => '',             // Payer's middle name.  25 char max.
                              'lastname' => $lastName,              // Payer's last name.  25 char max.
                              'suffix' => ''                // Payer's suffix.  12 char max.
                            );
                                          
                                    $BillingAddress = array(
                            'street' => $address1,            // Required.  First street address.
                            'street2' => $address2,             // Second street address.
                            'city' => $city,              // Required.  Name of City.
                            'state' => $state,              // Required. Name of State or Province.
                            'countrycode' => $country,          // Required.  Country code.
                            'zip' => $zip,              // Required.  Postal code of payer.
                            'phonenum' => $phone            // Phone Number of payer.  20 char max.
                          );
      
                  
                                     $PaymentDetails = array(
                              'amt' => $amount,               // Required.  Total amount of order, including shipping, handling, and tax.  
                              'currencycode' => $currencyID,          // Required.  Three-letter currency code.  Default is USD.
                              'itemamt' => '',            // Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                              'shippingamt' => '',          // Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                              'insuranceamt' => '',           // Total shipping insurance costs for this order.  
                              'shipdiscamt' => '',          // Shipping discount for the order, specified as a negative number.
                              'handlingamt' => '',          // Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                              'taxamt' => '',             // Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
                              'desc' => '',               // Description of the order the customer is purchasing.  127 char max.
                              'custom' => '',             // Free-form field for your own use.  256 char max.
                              'invnum' => '',             // Your own invoice or tracking number
                              'buttonsource' => '',           // An ID code for use by 3rd party apps to identify transactions.
                              'notifyurl' => '',            // URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                              'recurring' => ''           // Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                            );          

                            $PayPalRequestData = array(
                        'DPFields' => $DPFields, 
                        'CCDetails' => $CCDetails, 
                        'PayerInfo' => $PayerInfo, 
                        'PayerName' => $PayerName, 
                        'BillingAddress' => $BillingAddress, 
                        
                        'PaymentDetails' => $PaymentDetails, 
                        
                      );
                    $code='';
                                $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
                                
                                
                      if(!empty($PayPalResult["RAWRESPONSE"]) && "SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) 
                        {
                            $sts='SUCCESS';
                        $tr_type='Paypal_sale';
                         $result = $PayPalResult;
                              $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
                                redirect($thank_url);
                      }
                      else{
                           $data['records'] = $this->session->userdata('records');
                            if(!empty($PayPalResult["RAWRESPONSE"])) 
                          $responsetext= $PayPalResult['L_LONGMESSAGE0'];
                          else
                           $responsetext="Payment Declined! Please try again";
                          $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:'.$responsetext.'</strong></div>'); 
                        
                         $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>'. $responsetext .'</div>';
                        }
                      }else {
                        $data['records']   = $this->session->userdata('records');
                        $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>';
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>');
                      }

			                      
								  }
							
									//Forth Stripe gateway
								
									if($chk_gateway->gatewayType ==5)
							    	{
								    
								      if ($creditStatus) { 
                          require_once APPPATH."third_party/stripe/init.php"; 
                                  require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
                          $payusername   = $chk_gateway->gatewayUsername;
                          $paypassword   =  $chk_gateway->gatewayPassword;
                        
                          if(!empty($cust_sess['card'])){ 
                                    $card     = $cust_sess['card'];
                                  $expmonth =  $cust_sess['exp1'];
                                  $exyear   =$cust_sess['exp2'];
                                   if(strlen($expmonth)==1){
                                      $expmonth = '0'.$expmonth;
                                    }
                                  
                                  $cvv    = $cust_sess['ccv'];
                                 }
                                  
                                    $amount =  (int)$amount*100;
                                
                                
                                   
                                     \Stripe\Stripe::setApiKey($paypassword);
                                        $token  =  $cust_sess['token'];
                                        $charge = \Stripe\Charge::create([
                                                        "amount" => $amount,
                                                        "currency" => "usd",
                                                        "source" => $token, // obtained with Stripe.js
                                                        "description" => "Charge for test Account"
                                                      ]);
                                          
                                            
                                       $charge= json_encode($charge);
                                     $result = json_decode($charge);
                                   if($result->paid=='1' && $result->failure_code=="")
                                   {
                                      $tr_type     =  'stripe_sale';
                                      $sts     =  'SUCCESS';
                                
                                  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
                             
                        }
                        else
                        {
                             $data['records'] = $this->session->userdata('records');
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $result->status .'</div>'); 
                            
                               $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>'. $result->status .'</div>';
                        
                        }

                      }else {
                        $data['records']   = $this->session->userdata('records');
                        $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>';
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>');
                      }

								   
								    
								}
								
					                 
						    if($chk_gateway->gatewayType ==6)
						    {
								    if ($creditStatus) { 
                        require_once APPPATH."third_party/usaepay/usaepay.php"; 


                        $payusername   = $chk_gateway->gatewayUsername;
                        $paypassword   =  $chk_gateway->gatewayPassword;

                        $card_no =$this->czsecurity->xssCleanPostInput('cardNumber');
                                          $expmonth =$this->czsecurity->xssCleanPostInput('cardExpiry');
                                          $exyear =$this->czsecurity->xssCleanPostInput('cardExpiry2');
                                          $cvv =$this->czsecurity->xssCleanPostInput('cardCVC');
                                          $fname   = $cust_sess['fname'];
                                          $lname   = $cust_sess['lname'];
                                          $address1=  $cust_sess['add1'];
                                          $address2=  $cust_sess['add2'];
                        $country =  $cust_sess['country'];
                        $city    =  $cust_sess['city'];
                        $state   =     $cust_sess['state'];
                        $zipcode = $cust_sess['zip'];

                        $saddress1=  $cust_sess['sadd1'];
                                          $saddress2=  $cust_sess['sadd2'];
                        $scountry =  $cust_sess['scountry'];
                        $scity    =  $cust_sess['scity'];
                        $sstate   =     $cust_sess['sstate'];
                        $szipcode = $cust_sess['szip'];
                        $email    = $cust_sess['email'];
                        $company  = $cust_sess['company']; 
                        $invNo  =mt_rand(1000000,2000000); 
                        $transaction = new umTransaction;
                        $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                        $transaction->key=$payusername;
                        $transaction->pin=$paypassword;
                        $transaction->usesandbox=$this->config->item('Sandbox');
                        $transaction->invoice=$invNo;       // invoice number.  must be unique.
                        $transaction->description="Chargezoom Public Invoice Payment";  // description of charge
                        
                        $transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
                        $transaction->command="sale"; 
                                  $transaction->card = $card_no;
                        $expyear   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.$expyear;  
                        $transaction->exp = $expry;
                                      if($cvv!="")
                                      $transaction->cvv2 = $cvv;
                                      
                        $transaction->billcompany = $company;
                        $transaction->billfname = $fname;
                        $transaction->billlname = $lname;
                        $transaction->billstreet = $address1;
                        $transaction->billstreet2 = $address2;
                        $transaction->billcountry = $country;
                        $transaction->billcity    = $city;
                        $transaction->billstate = $state;
                        $transaction->billzip = $zipcode;

                        $transaction->shipcompany = $company;
                        $transaction->shipfname = $fname;
                        $transaction->shiplname = $lanme;
                        $transaction->shipstreet = $saddress1;
                        $transaction->shipstreet2 = $saddress2;
                        $transaction->shipcountry = $scountry;
                        $transaction->shipcity    = $scity;
                        $transaction->shipstate = $sstate;
                        $transaction->shipzip = $zipcode;

                        $transaction->email = $email;

                        $transaction->amount = $amount;

                        $transaction->Process();
                                      
                        $error='';  
                                            
                                            
                        if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                        {  
                            $msg = $transaction->result;
                            $trID = $transaction->refnum;
                                             
                            $tr_type  = 'sale';
                            $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                            
                            $sts ="SUCCESS";
                            $tr_type  = 'sale';
                            $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
                            
                        }else{
                            $msg = $transaction->result;
                            $trID = $transaction->refnum;
                            $sts ="ERROR";
                            $tr_type  = 'sale';
                            $result  = array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );

                            $data['records'] = $this->session->userdata('records');
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $transaction->error .'</div>'); 
                            $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $transaction->error .'</div>';
                        }

                    }else {
                      $data['records']   = $this->session->userdata('records');
                      $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>';
                      $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>');
                    }
								} 
							if($chk_gateway->gatewayType ==7)
						    {
								  if ($creditStatus) { 
                    require_once dirname(__FILE__) . '/../../../../vendor/autoload.php';
                    $payusername   = $chk_gateway->gatewayUsername;
                    $secretApiKey   =  $chk_gateway->gatewayPassword;
                  
                    $config = new PorticoConfig();
               
                    $config->secretApiKey = $secretApiKey;
                    $config->serviceUrl =  $this->config->item('GLOBAL_URL');
   
                    $card_no =$this->czsecurity->xssCleanPostInput('cardNumber');
                    $expmonth =$this->czsecurity->xssCleanPostInput('cardExpiry');
                    $exyear =$this->czsecurity->xssCleanPostInput('cardExpiry2');
                    $cvv =$this->czsecurity->xssCleanPostInput('cardCVC');
                    $address1=$cust_sess['add1'];
                    $country =  $cust_sess['country'];
                    $city    =  $cust_sess['city'];
                    $state   =     $cust_sess['state'];
                    $zipcode = $cust_sess['zip'];

                    ServicesContainer::configureService($config);
                    $card = new CreditCardData();
                    $card->number = $card_no;
                    $card->expMonth = $expmonth;
                    $card->expYear = $exyear;
                    if($cvv!="")
                    $card->cvn = $cvv;
                    

                    $address = new Address();
                    $address->streetAddress1 = $address1;
                    $address->city = $city;
                    $address->state = $state;
                    $address->postalCode = $zipcode;
                    $address->country = $country;


                    $invNo  =mt_rand(5000000,20000000);
                    try
                    {
                        if ($creditStatus) {
                            $response = $card->charge($amount)
                            ->withCurrency("USD")
                            ->withAddress($address)
                            ->withInvoiceNumber($invNo)
                            ->withAllowDuplicates(true)
                            ->execute();
                        }else{
                            $check = new ECheck();
                            $check->accountNumber = $cust_sess['accountNumber'];
                            $check->routingNumber = $cust_sess['routingNumber'];
                            if(strtolower($cust_sess['accountType']) == 'checking'){
                                $check->accountType = 0;
                            }else{
                                $check->accountType = 1;
                            }

                            if(strtoupper($cust_sess['accountHolderType']) == 'PERSONAL'){
                                $check->checkType = 0;
                            }else{
                                $check->checkType = 1;
                            }
                            $check->checkHolderName = $cust_sess['accountName'];
                            $check->secCode = "WEB";

                            $response = $check->charge($amount)
                            ->withCurrency(CURRENCY)
                            ->withAddress($address)
                            ->withInvoiceNumber($invNo)
                            ->withAllowDuplicates(true)
                            ->execute();
                        }

                        if($response->responseCode != 0 && $response->responseCode != '00')
                        {
                            $error='Gateway Error. Invalid Account Details';
                            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                            redirect(current_url());
                        }
                        
                        if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                        {
                            // add level three data
                            $transaction = new Transaction();
                            $transaction->transactionReference = new TransactionReference();
                            $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                            $level_three_request = [
                                'card_no' => $card_no,
                                'amount' => $amount,
                                'invoice_id' => $invNo,
                                'merchID' => $merchid,
                                'transaction_id' => $response->transactionId,
                                'transaction' => $transaction,
                                'levelCommercialData' => $levelCommercialData,
                                'gateway' => 7
                            ];
                            addlevelThreeDataInTransaction($level_three_request);

                                 $msg = $response->responseMessage;
                                 $trID = $response->transactionId;
                            $sts ="SUCCESS";
                            $tr_type  = 'sale';
                            $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                            $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
                              
                        }
                        else
                        {
                          $data['records'] = $this->session->userdata('records');
                          $tr_type  = 'sale';
                          $msg = $response->responseMessage;
                          $trID = $response->transactionId;
                          $sts ="ERROR";
                          $result =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );
                          $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
                          $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>';
                        }
                        
                      }catch (BuilderException $e)
                        {
                            $error= 'Build Exception Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                              $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                        }
                        catch (ConfigurationException $e)
                        {
                            $error='ConfigurationException Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                              $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                        }
                        catch (GatewayException $e)
                        {
                            $error= 'GatewayException Failure: ' . $e->getMessage();
                            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                              $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                        }
                        catch (UnsupportedTransactionException $e)
                        {
                            $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                           $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                            $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                        }
                        catch (ApiException $e)
                        {
                            $error=' ApiException Failure: ' . $e->getMessage();
                         $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                          $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                        }           
                              
                  }else {
                    $data['records']   = $this->session->userdata('records');
                    $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>';
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>');
                  }

								}
                if ($chk_gateway->gatewayType == 14) {
					include APPPATH . 'third_party/Cardpointe.class.php';
					$cardpointeuser = $chk_gateway->gatewayUsername;
                    $cardpointepass = $chk_gateway->gatewayPassword;
					$cardpointeMerchID  = $chk_gateway->gatewayMerchantID;
                    $cardpointeSiteName  = $chk_gateway->gatewaySignature;
                    $client = new CardPointe();
                    $fullName = $cust_sess['fname'].' '.$cust_sess['lname'];
			        if ($creditStatus) { 
                        $exyear   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                          $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.$exyear;
                        $card_no =$this->input->post('cardNumber');
                    
                        $result = $client->authorize_capture($cardpointeSiteName,$cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $amount ,'', $fullName, $cust_sess['add1'], $cust_sess['city'], $cust_sess['state'], $cust_sess['zip']);
                    }else{
                        
                         $result = $client->ach_capture($cardpointeSiteName,$cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountNumber, $routeNumber, $amount, $acct_type, $cust_sess['accountName'], $cust_sess['add1'], $cust_sess['city'], $cust_sess['state'], $cust_sess['zip']);
                        
                    } 
					        if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
										$trID = $responseId = $result['retref'];
										$code_data = "SUCCESS";
                                        $sts="SUCCESS";
										$tr_type   = 'sale';
										$this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
									} else {
										$err_msg      = $result['resptext'];
										$data['records'] = $this->session->userdata('records');
			                            $trID  =  (isset($result['retref']))?$result['retref']:'TXNFailed-'.time();
										$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
										$data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
									}
								}
					    if($chk_gateway->gatewayType ==8)
						  {
							
							    if ($creditStatus) {
                    $this->load->config('cyber_pay');
                    $flag="true";
                    $option =array();
                  
                    $option['merchantID']     = trim($chk_gateway->gatewayUsername);
                    $option['apiKey']         = trim($chk_gateway->gatewayPassword);
                    $option['secretKey']      = trim($chk_gateway->gatewaySignature);
                    
                    if($this->config->item('Sandbox'))
                      $env   = $this->config->item('SandboxENV');
                    else
                      $env   = $this->config->item('ProductionENV');
                    $option['runENV']      = $env;
                    
                    $commonElement = new CyberSource\ExternalConfiguration($option);

                    $config = $commonElement->ConnectionHost();

                    $merchantConfig = $commonElement->merchantConfigObject();
                    $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                    $api_instance = new CyberSource\Api\PaymentsApi($apiclient);

                    $cliRefInfoArr = [
                        'testpayment'
                      
                        ];
                    $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);

                    if ($flag == "true")
                    {
                      $processingInformationArr = [
                        "capture" => true, "commerceIndicator" => "internet"
                      ];
                    }
                    else
                    {
                      $processingInformationArr = [
                        "commerceIndicator" => "internet"
                      ];
                    }
                    $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                       
                    $amountDetailsArr = [
                       "totalAmount" => $amount,
                        "currency" => CURRENCY
                    ];
                    $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

                    $card_no =$this->czsecurity->xssCleanPostInput('cardNumber');
                    $expmonth =$this->czsecurity->xssCleanPostInput('cardExpiry');
                    $exyear =$this->czsecurity->xssCleanPostInput('cardExpiry2');
                    $cvv =$this->czsecurity->xssCleanPostInput('cardCVC');
                    $address1=$cust_sess['add1'];
                    $country =  $cust_sess['country'];
                    $city    =  $cust_sess['city'];
                    $state   =  $cust_sess['state'];
                    $zipcode = $cust_sess['zip'];

                    $invNo  =mt_rand(5000000,20000000);

                    $billtoArr = [
                      "firstName" => $cust_sess['first_name'], 
                      "lastName" => $cust_sess['last_name'],
                      "address1" => $address1,
                      "postalCode" => $zipcode,
                      "locality" => $city,
                      "administrativeArea" => $state,
                      "country" => $country,
                      "phoneNumber" => 122345321,
                      "company" => $cust_sess['company'],
                      "email" => $cust_sess['email'] 

                    ];
                    $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
                    
                    $orderInfoArr = [
                      "amountDetails" => $amountDetInfo, 
                      "billTo" => $billto
                    ]; 
                    $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                   
                    $paymentCardInfo = [
                      "expirationYear" => $exyear,
                      "number" => $card_no,
                      "securityCode" => $cvv,
                      "expirationMonth" => $expmonth
                    ];
                    $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                   
                    $paymentInfoArr = [
                      "card" => $card
                    ];
                    $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

                    $paymentRequestArr = [
                      "clientReferenceInformation" => $client_reference_information, 
                      "orderInformation" => $order_information, 
                      "paymentInformation" => $payment_information, 
                      "processingInformation" => $processingInformation
                    ];

                    $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                    
                    $api_response = list($response, $statusCode, $httpHeader) = null;
                    try
                    {
                      //Calling the Api
                      $api_response = $api_instance->createPayment($paymentRequest);

                      if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
                      {
                        $trID =   $api_response[0]['id'];
                        $msg  =   $api_response[0]['status'];

                        $code =   '200';

                        $sts="SUCCESS";
                        $tr_type  = 'sale';
                        $result =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  

                      }
                      else
                      {
                        $data['records'] = $this->session->userdata('records');
                        $tr_type  = 'sale';
                        $trID  =   $api_response[0]['id'];
                        $msg  =   $api_response[0]['status'];
                        $code =   $api_response[1];

                        $error = $api_response[0]['status'];
                        $this->session->set_flashdata('message','<div class="alert alert-success">  <strong>Payment '.$error.'</strong></div>');
                        $sts ="ERROR";
                        $result =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID );
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>'); 
                        $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $msg .'</div>';
                     }

                    }       
                    catch(Cybersource\ApiException $e)
                    {
                    
                      $error = $e->getMessage();

                      $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
                      $data['error_msg'] ='<div class="alert alert-danger">  <strong>Error:</strong>'. $error .'</div>';
                    }

                  }else {

                    $data['records']   = $this->session->userdata('records');
                    $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>';
                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Authentication not valid.</strong></div>');
                  }
 
                      
                
                                          			   		
										
								} 

                /** Maverick Payment */
                if ($chk_gateway->gatewayType == 17) {
									  // Load Maverick Config and Library
                    $this->load->config('maverick');
                    $this->load->library('MaverickGateway');

                    // Maverick Payment Gateway
                    $this->maverickgateway->setApiMode($this->config->item('maverick_payment_env'));
                    $this->maverickgateway->setTerminalId($chk_gateway->gatewayPassword);
                    $this->maverickgateway->setAccessToken($chk_gateway->gatewayUsername);

                    if ($creditStatus) { 
                        $exyear   = substr($exyear,-2);
                        if(strlen($expmonth)==1){
                          $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.'/'.$exyear;

                        $request_payload = [
                            'level' => 1,
                            'threeds' => [
                                'id' => null,
                            ],
                            'amount' => $amount,
                            'card' => [
                                'number' => $cust_sess['card'],
                                'cvv'    => $cust_sess['ccv'],
                                'exp'    => $expry,
                                'save'   => 'No',
                                'address' => [
                                    'street' => $cust_sess['add1'],
                                    'city' => $cust_sess['city'],
                                    'state' => $cust_sess['state'],
                                    'country' => $cust_sess['country'],
                                    'zip' => $cust_sess['zip'],
                                ]
                            ],
                            'contact' => [
                                'name'   => $cust_sess['fname'].' '.$cust_sess['lname'],
                            ]
                        ];
    
                        // Process Sale
                        $r = $this->maverickgateway->processSale($request_payload);

                    }else{  
                        // get DBA id
                        $dbaId = $this->maverickgateway->getDba(true);

                        // electronic Sale
                        $request_payload = [
                                'amount'          => $amount,
                                'routingNumber' => $routeNumber,
                                'accountName'     => $accountName,
                                'accountNumber'   => $accountNumber,
                                'accountType'     => ucwords($acct_type), // Checking or Savings
                                'transactionType' => 'Debit', // Debit or Credit
                                'customer' => [
                                    'firstName' => $cust_sess['fname'],
                                    'lastName'  => $cust_sess['lname'],
                                    "address1"  => $cust_sess['add1'],
                                    "address_2" => $cust_sess['add2'],
                                    "city"      => $cust_sess['city'],
                                    "state"     => $cust_sess['state'],
                                    "zipCode"   => $cust_sess['zip'],
                                    "country"   => $cust_sess['country'],
                                    'email'     => $cust_sess['email'],
                                ],
                                'dba' => [
                                    'id' => $dbaId,
                                ],
                            ];
                        // Process ACH
                        $r = $this->maverickgateway->processAch($request_payload);
                    }
                    
                    $result = [];

                    $rbody = json_decode($r['response_body'], true);

                    $result['response_code'] = $r['response_code'];

                    $result['data'] = $rbody;
                    
                    // Response                
                    if($r['response_code'] >= 200 && $r['response_code'] < 300) {
                        if(isset($rbody['id']) && $rbody['id']){
                            
                            $responseId = $rbody['id'];
                            $code_data = "SUCCESS";
                            $tr_type   = 'sale';
                            $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
                        } else {
                            $err_msg      = 'Payment failed';
                            $data['records'] = $this->session->userdata('records');

                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
                            $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
                        }

                    } else {
                        $err_msg = $result['message'] = $rbody['message'];
                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> ' . $err_msg . '</div>');
                        $data['error_msg'] = '<div class="alert alert-danger">  <strong>Error:</strong>' . $err_msg . '</div>';
                    }
								}
        }
        else
        {
      		  $sts='SUCCESS';
        
        }				
								
								
								if($sts==='SUCCESS')
								{
								    
								  
							

                        			  $this->session->set_flashdata('message','<div class="alert alert-success">Customer Created Successfully</div>'); 
                        			$input_data['firstName']   = $sessiondata['fname'];
                        			$input_data['lastName']	   = $sessiondata['lname'];
                        			$input_data['fullName']	   = $sessiondata['fname'] .' '. $sessiondata['lname'];
                        			$input_data['userEmail']   = $sessiondata['email'];
                        			$input_data['country']	   = $sessiondata['country'];
                        			$input_data['state']	   = $sessiondata['state'];
                        			$input_data['city']	       = $sessiondata['city'];
                                
                                	$input_data['ship_country']	   = $sessiondata['scountry'];
                        			$input_data['ship_state']	   = $sessiondata['sstate'];
                        			$input_data['ship_city']	       = $sessiondata['scity'];
                        			$input_data['ship_address1']	   = $sessiondata['sadd1'];
                        			$input_data['ship_address2']    = $sessiondata['sadd2'];
                        		   	$input_data['ship_zipcode']	  =$sessiondata['szip'];
                                
                        			$input_data['companyName']	  = $sessiondata['company'];
                        				$input_data['isActive']   = 'true';
                        		    	$input_data['zipcode']	  =$sessiondata['zip']; 
                        			$comp_data = $this->general_model->get_row_data('tbl_company',array('merchantID'=>	$merchid));
                        			$user = 	$comp_data['qbwc_username'];
                                	$input_data['companyID']	       = 	$comp_data['id'];
                        		   
                        		    
                        		    $custID=	$list_id  =$this->general_model->get_random_string('CUS');
                        		       	     $input_data['customListID'] = $list_id;
                        			$input_data['address1']	   =$sessiondata['add1'];
                        			$input_data['address2']    = $sessiondata['add2'];
                        	        $input_data['merchantID']   = $sessiondata['merchantid'];
                        		    $input_data['createdat']    = date('Y-m-d H:i:s');
                                    $cusID ='';	
                        		        $input_data['qb_action'] ="Add Customer";
                        		         $chk_cust = $this->general_model->get_row_data('tbl_customer_login',array('customerEmail'=>$sessiondata['email'],'merchantID'=>$sessiondata['merchantid']));
                        		        if(!empty($chk_cust))
                        		        {
                        		          $custID =   $chk_cust['customerID'];
                        		          
                        		        }
                        		        else
                        		        {
                        			  
                                        $custID	= $input_data['customListID'];
                                        $cusID = $list_id;
			    	                    if(empty($chk_cust) && empty($chk_cust['customerID']))
										 {
										 	$customer_login = array(
												'customerEmail'=>$cust_sess['email'],
												'customerPassword'=>md5($string),
												'createdAt'=>date('Y-m-d H:i:s'),
												'is_logged_in'=>'0',
												'loginCode'=>'xyz',
												'isEnable'=>'0',
												'customerUsername'=>$cust_sess['email'],
												'merchantID'=>$cust_sess['merchantid'],
												'customerID'=>$custID
											 );
									
										   	$this->general_model->insert_row('tbl_customer_login', $customer_login);
										 } 
                            				$cusom_data=array( 'ListID'=>$list_id,'FirstName'=>$input_data['firstName'],'LastName'=>$input_data['lastName'],'FullName'=>$input_data['fullName'],
                            				'Contact'=>$input_data['userEmail'],'companyName'=>$input_data['companyName'], 'companyID'=>$input_data['companyID'],
                            				'qbmerchantID'=>$merchid,'qb_status'=>0,'customerStatus'=>1,'IsActive'=>'true','qb_status'=>0,'qbAction'=>$input_data['qb_action'],
                            				
                            				
                            				 'ShipAddress_Addr1'=>$input_data['ship_address1'],'ShipAddress_Addr2'=>$input_data['ship_address2'],'ShipAddress_City'=>$input_data['ship_city'],'ShipAddress_State'=>$input_data['ship_state'],
                            				'ShipAddress_PostalCode'=>$input_data['ship_zipcode'],'ShipAddress_Country'=>$input_data['ship_country'],
                            			     'BillingAddress_Addr1'=>$input_data['address1'],'BillingAddress_Addr2'=>$input_data['address2'],'BillingAddress_State'=>$input_data['state'],
                            				'BillingAddress_City'=>$input_data['city'],'BillingAddress_Country'=>$input_data['country'], 'BillingAddress_PostalCode'=>$input_data['zipcode'],
                            			
                            				
                            				'TimeCreated'=>date('Y-m-d H:i:s'), 'TimeModified'=>date('Y-m-d H:i:s'));
                            				
                            				if($input_data['ship_address1']=='')
                            				{
                            				 $cusom_data['ShipAddress_Addr1']   =$input_data['address1'];
                            				 $cusom_data['ShipAddress_Addr2']   =$input_data['address2'];	
                            				 $cusom_data['ShipAddress_State']   =$input_data['state'];
                            				 $cusom_data['ShipAddress_City']   =$input_data['city'];
                            				 $cusom_data['ShipAddress_Country']   =$input_data['country'];
                            				 $cusom_data['ShipAddress_PostalCode']   =$input_data['zipcode'];
                            				    
                            				}
                            				
                            			
                            				$this->general_model->insert_row('chargezoom_test_customer',$cusom_data);
                            				
                            			
                            				
                            				
                            		        }    
								  //customer card
										   	
										  
                       $card_data = array(
                            'customerListID'           => $custID,
                            'Billing_Addr1'            => $cust_sess['add1'],
                            'Billing_Addr2'            => $cust_sess['add2'],
                            'Billing_City'             => $cust_sess['city'],
                            'Billing_Country'          => $cust_sess['country'],
                            'Billing_Contact'          => $cust_sess['mobile'],
                            'Billing_State'            => $cust_sess['state'],
                            'Billing_Zipcode'          => $cust_sess['zip'],
                            'merchantID'               => $cust_sess['merchantid'],
                            'createdAt'                => date("Y-m-d H:i:s"),
                        );
                        if($cust_sess['scheduleID'] == 1){
                            $card_type = $this->general_model->getType($cust_sess['card']);
                            $card_data['cardMonth'] = $cust_sess['exp1'];
                            $card_data['cardYear'] = $cust_sess['exp2'];
                            $card_data['CardType'] = $card_type;
                            $card_data['CustomerCard'] = $this->card_model->encrypt($cust_sess['card']);
                            $card_data['CardCVV'] = $this->card_model->encrypt($cust_sess['ccv']);
                            $card_data['customerCardfriendlyName'] = $cust_sess['cardfriendlyname'];
                        }else{
                            $card_data['accountNumber'] = $cust_sess['accountNumber'];
                            $card_data['routeNumber'] = $cust_sess['routeNumber'];
                            $card_data['accountName'] = $cust_sess['accountName'];
                            $card_data['customerCardfriendlyName'] = $cust_sess['accountfriendlyname'];
                            $card_data['secCodeEntryMethod'] = $cust_sess['secCodeEntryMethod'];
                            $card_data['accountType'] = $cust_sess['accountType'];
                            $card_data['accountHolderType'] = $cust_sess['accountHolderType'];
                            $card_data['CardType'] = 'Echeck';
                        }
                        $custom_data_fields['payment_type'] = $card_data['customerCardfriendlyName'];
										   
											
											   
											 $tbl_subscriptions = array(
											   'customerID'=>  $custID,
											   'merchantDataID'=>  $merchid,
											  
											   
											   'generatingDate'=>date("d"),
											   'firstDate'=>date("Y-m-d"),
											   'nextGeneratingDate'=>$next_billing_date,
											   'startDate'=>date("Y-m-d"),
											   'endDate'=>date("Y-m-d"),
											   
											   
											   'contactNumber'=> $cust_sess['mobile'],
											   'address1'=>  $cust_sess['add1'],
											   'address2'=> $cust_sess['add2'],
											   'country'=>  $cust_sess['country'],
											   'state'=>$cust_sess['state'],
											   'city'=>  $cust_sess['city'],
											   'zipcode'=>$cust_sess['zip'],
											   
											   'createdAt'=>   date("Y-m-d H:i:s"),
											   'updatedAt'=>   date("Y-m-d H:i:s"),
											   'taxID'=>  '0',
											   'planID'=>  $splan->planID,
											   
											   'paymentGateway'=> $chk_gateway->gatewayID,
											   'automaticPayment'=>$splan->automaticPayment,
											   'emailRecurring'=>$splan->emailRecurring,
											   'usingExistingAddress'=>$splan->usingExistingAddress,
											    'subscriptionName'=> $splan->planName,
											   'subscriptionPlan'=>$splan->subscriptionPlan,
											   'freeTrial'=>$splan->freeTrial,
											   'invoiceFrequency'=>$splan->invoiceFrequency,
											   'subscriptionAmount'=>$sub_amount,
											   'totalInvoice'=>$splan->totalInvoice,
											   'generatedInvoice'=>1,
											 );
											 
									
											
												
                        $crdata =   $this->card_model->chk_card_firendly_name($custID,$card_data['customerCardfriendlyName'],$merchid);           
                                                               
                        if($crdata >0)
                        {
                            $card_condition = array(
                                'customerListID' =>$custID, 
                                'customerCardfriendlyName'=>$card_data['customerCardfriendlyName'],
                            );
                            $card_data['updatedAt']    = date("Y-m-d H:i:s");
                            $cardID =         $this->card_model->update_card_data($card_condition, $card_data);                 
                        }
                        else
                        {
                            $cardID = $this->card_model->insert_card_data($card_data);
                        }

												
											   
										  
											if ($this->db->trans_status() === FALSE)
											{
												 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong></div>'); 
													$this->db->trans_rollback();
													
											}
											else
											{
											    
												$tbl_subscriptions['cardID'] =$cardID ;
										  
												$subsId =  $this->general_model->insert_row('tbl_chargezoom_subscriptions', $tbl_subscriptions);
                        $customerName = $sessiondata['fname'].' '.$sessiondata['lname'];
                        $nf = $this->addNotificationForMerchant(0,$customerName,$custID,$merchid,$subsId,$splan->planName);

												
										
										if(!empty($subsId))
										{
												$total1=0;
												$item_vals=array();
												$lineArray = array();
												foreach($item as $item_data)
												{
                            $one_prorata_data = ($item_data['oneTimeCharge']) ? 1 : $prorata_data;
    												$qbd_item = array(
    												   'itemListID'=>  $item_data['itemListID'],
    												   'itemFullName'=> $item_data['itemFullName'],
    												   'itemDescription'=>$item_data['itemDescription'],
    												   'itemQuantity'=>$item_data['itemQuantity'],
    												   'itemRate'=> $item_data['itemRate'],
    												   'itemTax'=>  $item_data['itemTax'],
    												   'oneTimeCharge'=> $item_data['oneTimeCharge'],
    												   'subscriptionID'=> $subsId
    												 );
    												$insertId =  $this->general_model->insert_row('tbl_chargezoom_subscription_invoice_item', $qbd_item);
    											
    												

    												   $insert_row['itemListID']    =$item_data['itemListID'];
                                        			   $insert_row['itemQuantity'] =$item_data['itemQuantity'];
                                        			   $insert_row['itemRate']      =$item_data['itemRate']  * $one_prorata_data;
                                        			   $insert_row['itemFullName'] = $item_data['itemFullName'];
                                        			   $insert_row['itemDescription'] =$item_data['itemDescription'];
                                        			   
                                        			   $total1=$total1 + ($item_data['itemQuantity']*$item_data['itemRate'] * $one_prorata_data);	
                                        			   $item_vals[] =$insert_row;
												}
												
												
												     $i    = 0;
                                                    $total = 0;	
                    				$in_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID'=>$merchid));
                    		
                    			   if(!empty($in_data))
                    			   {
                        			$inv_pre   = $in_data['prefix'];
                        			$inv_po    = $in_data['postfix']+1;
                        			$new_inv_no = $inv_pre.$inv_po;
                                      $randomNum =  $this->general_model->get_random_string('INV');
                    			   }else{
                    			      	$inv_pre  = "MDC"; 
                    			     	$inv_po    = '10001';
                        			    $new_inv_no = $inv_pre.$inv_po; 
                        			   $randomNum =   $this->general_model->get_random_string('INV');
                    			   }               
                                     $fullname = $sessiondata['company']; 
                				      	if($splan->automaticPayment)
                				      	{
                            				
                            					$auto='1';
                            				}else{
                            					$auto ='0';	
                            				}
                            				 
                                    			$inv_data =array(
                                    			'Customer_FullName'=>$sessiondata['company'],
                                    			'Customer_ListID'=>$custID,
                                    			'TxnID' => $randomNum,
                                    			'RefNumber'=>$new_inv_no,
                                    			'TimeCreated'=> date('Y-m-d H:i:s'),
                                    			'TimeModified'=> date('Y-m-d H:i:s'),
                                    			'DueDate'     => date('Y-m-d'),
                                    			'ShipAddress_Addr1'=>$cust_sess['add1'],
                                    			'ShipAddress_Addr2'=>$cust_sess['add2'],
                                    			'ShipAddress_City'=>$cust_sess['city'],
                                    			'ShipAddress_Country'=>$cust_sess['country'],
                                    			'ShipAddress_State'=>$cust_sess['state'],
                                    			'ShipAddress_PostalCode'=>$cust_sess['zip'],
                                    			'IsPaid'				=>'false',	
                                    			'insertInvID'=>$randomNum,
                                    			'invoiceRefNumber'=>$inv_po,
                                                'BalanceRemaining'=>$amount,
                                    			'freeTrial'=>0,
                                    			'gatewayID'=>$chk_gateway->gatewayID,
                                    			'autoPayment'=>$auto,
                                    			'cardID'=>$cardID,
                                    			'merchantID'=>  $merchid,
                                    			);
                                    
                            			   $inv_data['qb_action'] = 'Add Invoice';
                            			    $this->company_model->create_new_invoice($inv_data,$item_vals);	
                            			   
                            			  
                            					foreach($item_vals as $k=>$item)
                            					{
                            						$item['subscriptionID'] =0;
                            							$item['invoiceDataID'] =$randomNum;
                            						$ins = $this->general_model->insert_row('tbl_chargezoom_subscription_invoice_item',$item);	
                            				    } 		
                            			
											  
			                                    if (!empty($ins))
			                                    {
			                                      
                                                $cond= array('subscriptionID'=>$subsId);
                                                $subscription_auto_invoices_data = [
                                                  'subscriptionID' => $subsId,
                                                  'invoiceID' => $randomNum,
                                                  'app_type' => 5 //CZ
                                                ];
          
                                                $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);
			                                        	
			                                        	
			                                      
											          	$this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID'=>$merchid), array('postfix'=>$inv_po) );
			                                    }
												
											$crtxnID='';

                    /* gET Txn id*/
                    $gt_type = $chk_gateway->gatewayType;
                    

                    if($gt_type==1 || $gt_type==3 || $gt_type==9)
                    {      
                        $transactionID     = $result['transactionid'];
                        $result['paymentType'] = $cust_sess['scheduleID'];
                    } 
                    if($gt_type==2)
                    {   
                        $result->paymentType = $cust_sess['scheduleID'];
                        $transactionID     = $result->transaction_id;
                    } 
                        

                    if($gt_type==4)
                    {     
                        $transactionID = 'TXNFAILED-'.time();
                        if(isset($result['TRANSACTIONID'])) {  
                            $transactionID = $result['TRANSACTIONID'];  
                        }
                        $result['paymentType'] = $cust_sess['scheduleID'];
                          
                            
                    } 
                    if($gt_type==5)
                    {      
                        $result->paymentType = $cust_sess['scheduleID'];
                        if($result->paid=='1' && $result->failure_code=="")
                        {
                            
                            $transactionID   = $result->id;
                        }
                        else
                        {
                            
                            $transactionID   = 'TXNFAILED-'.time();  
                        }       
                    } 

                    if($gt_type==6 || $gt_type==7 || $gt_type==8)
                    {      
                        $transactionID   = $result['transactionId'];
                        $result['paymentType'] = $cust_sess['scheduleID'];
                    } 

                    if($gt_type== 10){
                        $transactionID     = $result['id'];
                        $result['paymentType'] = $cust_sess['scheduleID'];
                    }

                    if($gt_type== 11 || $gt_type== 13){
                        $result['paymentType'] = $cust_sess['scheduleID'];
                        $transactionID     = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : 'TXNFAILED-'.time();
                    }   
                    if($gt_type== 12){
                        $result['responseType'] = 'SaleResponse';
                        $result['paymentType'] = $cust_sess['scheduleID'];
                        $transactionID     = (isset($result['SaleResponse']['transactionID']) && !empty($result['SaleResponse']['transactionID'])) ? $result['SaleResponse']['transactionID'] : 'TXNFAILED-'.time();
                    }
                    if($gt_type== 14){
                      $result['responseType'] = 'SaleResponse';
                      $result['paymentType'] = $cust_sess['scheduleID'];
                      $transactionID     = (isset($result['retref']) && !empty($result['retref'])) ? $result['retref'] : 'TXNFAILED-'.time();
                    }
                    if($gt_type== 15 || $gt_type== 17){
                      $result['responseType'] = 'SaleResponse';
                      $result['paymentType'] = $cust_sess['scheduleID'];
                      $transactionID     = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : 'TXNFAILED-'.time();
                    }
                    if($gt_type== 16){
                      $result['paymentType'] = $cust_sess['scheduleID'];
                      $transactionID     = (isset($result['AUTH_GUID']) && !empty($result['AUTH_GUID'])) ? $result['AUTH_GUID'] : 'TXNFAILED-'.time();
                    }
                    $transactionByUser = [];
                    $transactionByUser['type'] = 3;
                    $transactionByUser['id'] = $custID;

                    if($creditStatus){
                        $payType = false;
                    }else{
                        $payType = true;
                    }
								    $id = $this->general_model->insert_gateway_transaction_data($result,$tr_type,$chk_gateway->gatewayID,$chk_gateway->gatewayType,$custID,$amount,$cust_sess['merchantid'],$crtxnID, $resellerID,$randomNum,$payType,$transactionByUser, $custom_data_fields);  
						        $this->session->set_flashdata('success','<div class="alert alert-success"><strong> Successfully Paid</strong></div>');  
                    $in_data = [];
                    $in_data['BalanceRemaining'] = $amount;
                    $in_data['RefNumber'] = '';
                    $in_data['FirstName'] = $cust_sess['first_name'];
                    $in_data['LastName'] = $cust_sess['last_name'];
                    $in_data['FullName']  = $cust_sess['company'];
                    $in_data['BillingAddress_Addr1'] = $cust_sess['add1'];
                    $in_data['ShipAddress_Addr1'] = $cust_sess['sadd1'];
                    $in_data['BillingAddress_Addr2'] = $cust_sess['add2'];
                    $in_data['ShipAddress_Addr2'] = $cust_sess['sadd2'];
                    $in_data['BillingAddress_City'] = $cust_sess['city'];
                    $in_data['ShipAddress_City'] = $cust_sess['scity'];
                    $in_data['BillingAddress_State'] = $cust_sess['state'];
                    $in_data['ShipAddress_State'] = $cust_sess['sstate'];
                    $in_data['BillingAddress_PostalCode'] = $cust_sess['zip'];
                    $in_data['ShipAddress_PostalCode'] = $cust_sess['szip'];
                    $in_data['BillingAddress_Country'] = $cust_sess['country'];
                    $in_data['ShipAddress_Country'] = $cust_sess['scountry'];
                    $in_data['merchantID'] = $merchID;
                    $this->session->set_userdata("tranID",$transactionID );
                    $this->session->set_userdata("sess_invoice_id",$in_data);
				
               if($chh_mail =='1')
			 {
                
                    $email = $comp_data['Contact']; $company=$comp_data['companyName']; $customer = $comp_data['FullName'];
			        
			     	$subject = 'ChargeZoom, Verification mail';
			        $message = "<p>Dear $customer,<br><br>.</p><br>We have setup Recurring Payment process for your Automatic Invoices Payment.</p>
               <p> Here all invoices have $".$amount. " or more than $ ".$amount." will be scheduled for authomatic payment. <p> 
               </p><br><br> Thanks,<br>Support, ChargeZoom <br>www.chargezoom.com";	
               
	           
	             
                    $tr_date   =date('Y-m-d H:i:s');
		        	
					$replyTo=$addBCC=$addCC='';
					$email_data          = array(
			                            'customerID'=>$customerID,
										'merchantID'=>$merchid, 
										'emailSubject'=>$subject,
										'emailfrom'=>$config_email,
										'emailto'=>$email,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
										'emailsendAt'=>$tr_date,
										
										);
										
					$this->load->library('email');					
					$this->email->from(ADMIN_EMAIL);
					$this->email->to($email);
					$this->email->subject($subject);
					$this->email->set_mailtype("html");
					$this->email->message($message);
				
			      	if($this->email->send())
								{
                                   
							     $this->general_model->insert_row('tbl_template_data', $email_data);
								}
								else
								{
								
								    $email_data['mailStatus']=0;
								      $this->general_model->insert_row('tbl_template_data', $email_data); 
								}
								
			   
			}
			
						     
						
										    	}
									} 
										   	    $this->session->unset_userdata('records');
										   	   
										    	$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
										    	redirect($thank_url);  
								}
							
					
		


		 }
		 else
		 {
		  
		     $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:</strong> Validation errors.</div>');
		       redirect(current_url(),'refresh');
		     
		 }

		 
	  }
		
    
	 
	    
        // switch statement end
	   
	  
		$oneTimeCharge = 0;
		if(isset($data['onetime'])){
			$oneTimeCharge = $data['onetime'];
			if(isset($data['selected_plan'])){
				$data['selected_plan']->subscriptionAmount += $oneTimeCharge;
			}
		}

		$data['gateway'] = array();
		$surchargePercentage = 0;
		if($splan->automaticPayment == 1) {
            $con1        = array("gatewayID" => $splan->paymentGateway);
        } else {
            $con1        = array("set_as_default" => 1, 'merchantID' => $merchid);
        }
		$gat   = $this->general_model->get_row_data('tbl_merchant_gateway',$con1);
		if($gat){
			if($gat['isSurcharge'] == '1'){
				$surchargePercentage = $gat['surchargePercentage'];
			}

			if($gat['gatewayType']=='5')
				$data['gateway'] =$gat;
		}

    $data['defaultGateway']              = $gat;
    
    $data['selectedGateway']              = $gat;
    

		$data['surchargePercentage'] =$surchargePercentage;
	    $data['customer_portal_data'] = $this->general_model->get_row_data('tbl_config_setting',array('merchantID'=>$merchid)); 
	    $resellerID = resellerID($merchid);
	    $data['reseller'] = $this->general_model->get_row_data('tbl_reseller',array('resellerID'=>$resellerID));
	    
	    
	     $data['template']['title'] = marchentname($merchid) .' - '. $plan.' - '.'Checkout Page';
	    
	    
	    
	    
	    
    	 $data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
	
		$this->load->view('template/template_start', $data);
        $this->load->view('customer/company/checkout', $data);
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	
	    
	}
	public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null,$plan=null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        
        $title = 'New Subscription';
        $nf_desc = ''.$customerName.' subscribed to <b>'.$plan.'</b> on '.$payDateTime.'';
        $type = 3;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
	
}