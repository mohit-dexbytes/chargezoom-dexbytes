<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;

class PaymentProcess extends CI_Controller {
	private $transactionByUser;
	function __construct()
	{
		parent::__construct();
		include APPPATH.'third_party/authorizenet_lib/AuthorizeNetAIM.php';
		include APPPATH . 'third_party/PayTraceAPINEW.php';
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('company/customer_model','customer_model');
	$this->load->model('company/company_model','company_model');
     	$this->load->config('paypal');
		  $this->db1= $this->load->database('otherdb', TRUE);
		  
		 
		if(!$this->session->userdata('customer_logged_in'))
		{
			redirect('login', 'refresh');
		}
		$logged_in_data = $this->session->userdata('customer_logged_in');
		$this->transactionByUser = ['id' => $logged_in_data['customerID'], 'type' => 3];
	}
	
	
	
	function NMIPayment()
	{
	    $customerID     = $this->session->userdata('customer_logged_in');
	    $merchantID =  $customerID['merchantID'];
	    $con = array('merchantID'=>$merchantID);
	    $con1 = array('customerID'=>$customerID['customerID']);
	    
	   
	    $get_cust = $this->general_model->get_row_data('chargezoom_test_customer',$con1);
	    $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
	    // QBO INtregration 
	    $data = $this->general_model->get_row_data('QBO_token',$con);
        $accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID']; 
	    $dataService = DataService::Configure(array(
		 'auth_mode' => 'oauth2',
		 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
		 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		 'accessTokenKey' =>  $accessToken,
		 'refreshTokenKey' => $refreshToken,
		 'QBORealmID' => $realmID,
		 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));
		
		$amount = $this->czsecurity->xssCleanPostInput('inv_amount');	
		$gid = $this->czsecurity->xssCleanPostInput('gateway');
		$con = "merchantID = '".$merchantID."'  AND  gatewayID = $gid";
		$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
	    
	    if(!empty($customerID['Customer_ListID'])){
	       
	       	include APPPATH . 'third_party/nmiDirectPost.class.php';
			$gatewayuser = $chk_gateway->gatewayUsername;
			$gatewaypass = $chk_gateway->gatewayPassword;
			$marchantID =  $chk_gateway->gatewayMerchantID;
			$nmi_data  = array('nmi_user'=>$gatewayuser, 'nmi_password'=>$gatewaypass);
			$transaction = new nmiDirectPost($nmi_data);
			
			if($this->czsecurity->xssCleanPostInput('CardID') =="new1"){	
			    $card_no = $this->czsecurity->xssCleanPostInput('card_number');
				$transaction->setCcNumber($this->czsecurity->xssCleanPostInput('card_number'));
				$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				$exyear   = substr($exyear,2);
				$expry    = $expmonth.$exyear; 
				$transaction->setCcExp($expry);
				$transaction->setCvv($this->czsecurity->xssCleanPostInput('cvv'));
				
				$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				$address1 = $this->czsecurity->xssCleanPostInput('address1').' '.$this->czsecurity->xssCleanPostInput('address2');
				$city = $this->czsecurity->xssCleanPostInput('city');
				$state = $this->czsecurity->xssCleanPostInput('state');
				$zip = $this->czsecurity->xssCleanPostInput('zipcode');
				$country= $this->czsecurity->xssCleanPostInput('country');
				$phone = $this->czsecurity->xssCleanPostInput('contact');
				
				$card_data = array(
				  'cardMonth'   =>$this->czsecurity->xssCleanPostInput('expiry'),
				  'cardYear' => $this->czsecurity->xssCleanPostInput('expiry_year'),
				  'CardType'=>$card_type,
				  'CustomerCard' =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('card_number')),
				  'CardCVV'      =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('cvv')), 
				  'customerListID' =>$customerID['Customer_ListID'],                           
				  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
				  'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
				  'Billing_City'=> $city,
				  'Billing_Country'=>$country,
				  'Billing_Contact'=>$phone,
				  'Billing_State'=>  $state,
				  'Billing_Zipcode'=> $zip,
				  'merchantID'   => $merchantID,
				  'customerCardfriendlyName'=>$friendlyname,
				  'createdAt' 	=> date("Y-m-d H:i:s") 
			   );
		
		  }else{
		      $card_data = array();
		      $cardID = $this->czsecurity->xssCleanPostInput('CardID');
		      $card_data    =   $this->get_carddata($cardID);
		        $card_no = $card_data['CardNo'];
		        $transaction->setCcNumber($card_data['CardNo']);
				$expmonth =  $card_data['cardMonth'];
				$exyear   = $card_data['cardYear'];
				$exyear   = substr($exyear,2);
				$expry    = $expmonth.$exyear; 
				$transaction->setCcExp($expry);
				$transaction->setCvv($card_data['CardCVV']);
		     
		      $address1 = $card_data['address'].' '.$card_data['address1'];
			  $city = $card_data['city'];
		      $state = $card_data['state'];
              $zip =$card_data['zip'];
			  $country= $card_data['country'];
			  $phone = $card_data['contact'];
		}	
			$transaction->setCompany($get_cust['companyName']);
			$transaction->setFirstName($get_cust['firstName']);
			$transaction->setLastName($get_cust['lastName']);
			$transaction->setAddress1($address1);
			$transaction->setCountry($country);
			$transaction->setCity($city);
			$transaction->setState($state);
			$transaction->setZip($zip);
			$transaction->setPhone($phone);
		
			$transaction->setEmail($get_cust['userEmail']);
			$transaction->setAmount($amount);
			$transaction->setTax('tax');
			$transaction->sale();
			// add level III data
            $level_request_data = [
                'transaction' => $transaction,
                'card_no' => $card_no,
                'merchID' => $merchantID,
                'amount' => $amount,
                'invoice_id' => $invoiceID,
                'gateway' => 1
            ];
            $transaction = addlevelThreeDataInTransaction($level_request_data);
			$result = $transaction->execute();
			if($result['response_code'] == '100') 
			{ 
			    
			    $nf = $this->addNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);

			    $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                                    				
                if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
					$theInvoice = current($targetInvoiceArray);
				}
			    $updatedInvoice = Invoice::update($theInvoice, [
                                    				   
				    "sparse " => 'true'
				]);
                                    				
                $updatedResult = $dataService->Update($updatedInvoice);
			    $newPaymentObj = Payment::create([
				    "TotalAmt" => $amount,
				    "SyncToken" => $updatedResult->SyncToken,
				    "CustomerRef" => $updatedResult->CustomerRef,
				     
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $updatedResult->Id,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				 ]);
				    
				$savedPayment = $dataService->Add($newPaymentObj);
				
				$transtion = array(
				    'transactionID'=> $result['transactionid'],
				    'transactionDate'=>  date("Y-m-d H:i:s"), 
				    'transactionStatus'=>  $result['responsetext'],
				    'transactionAmount'=> $amount,
				    'customerListID'=> $customerID['Customer_ListID'],
				    'transactionModified'=> date("Y-m-d H:i:s"),  
				    'transactionType'=>  $result['type'],
				    'transactionGateway'=>$chk_gateway->gatewayType, 
				    'gateway'=>'NMI',
				    'gatewayID'=> $chk_gateway->gatewayType, 
				    'transactionCode'=>$result['response_code'],
				    'merchantID'=>$merchantID,
				    'resellerID'=> resellerID($merchantID), 
				    'qbListTxnID'   => $savedPayment->Id,
			        'invoiceID'  =>$invoiceID
				);
				
				$this->db->trans_begin();
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transtion['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transtion['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transtion['transaction_by_user_id'] = $this->transactionByUser['id'];
					}	
				    $this->general_model->insert_row('customer_transaction',$transtion); 

				    /*Saved default card if not any exits (Only for new card)*/

				    $is_default = 0;
			     	$checkCustomerCard = checkCustomerCard($customerID['Customer_ListID'],$merchantID);
		        	if($checkCustomerCard == 0){
		        		$is_default = 1;
		        	}
		        	$card_data['is_default'] = $is_default;

					$this->db1->insert('customer_card_data', $card_data);
				if ($this->db->trans_status() === FALSE)
				{
					 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>'); 
					 $this->db->trans_rollback();
				
				}else{
				    $condition = array('invoiceID'=>$invoiceID,'merchantID'=>$merchantID,'CustomerListID'=>$customerID['customerID']);
				    $set = array('IsPaid'=>'1','BalanceRemaining'=>'0.00');
				    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $set);
				    $this->db->trans_commit();
				    redirect('QBO_controllers/home/index');
				}
				
			}else{
				$nf = $this->failedNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>');  
			    redirect('company/home/index');
			}
	    }
	}
	
	
	
	
	
	
	// Auth payment gateway
	function AuthPayment()
	{
	   $customerID     = $this->session->userdata('customer_logged_in');
	    $merchantID =  $customerID['merchantID'];
	    $con = array('merchantID'=>$merchantID);
	    $con1 = array('customerID'=>$customerID['customerID']);
	    $get_cust = $this->general_model->get_row_data('chargezoom_test_customer',$con1);
	    $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
	    // QBO INtregration 
	    $data = $this->general_model->get_row_data('QBO_token',$con);
        $accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID']; 
	    $dataService = DataService::Configure(array(
		 'auth_mode' => 'oauth2',
		 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
		 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		 'accessTokenKey' =>  $accessToken,
		 'refreshTokenKey' => $refreshToken,
		 'QBORealmID' => $realmID,
		 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));
		
		$amount = $this->czsecurity->xssCleanPostInput('inv_amount');	
		$gid = $this->czsecurity->xssCleanPostInput('gateway');
		$con = "merchantID = '".$merchantID."'  AND  gatewayID = $gid";
		$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
	    
	    if(!empty($customerID['Customer_ListID'])){ 
        	$apiloginID  =$chk_gateway->gatewayUsername;
        	$transactionKey  = $chk_gateway->gatewayPassword;
			$transaction = new AuthorizeNetAIM($apiloginID,$transactionKey); 
			
			
			if($this->czsecurity->xssCleanPostInput('CardID') =="new1"){	
			    
				$card_no = $this->czsecurity->xssCleanPostInput('card_number');
				$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				
				$expry    = $expmonth.$exyear;
				
				$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				$address1 = $this->czsecurity->xssCleanPostInput('address1').' '.$this->czsecurity->xssCleanPostInput('address2');
				$city = $this->czsecurity->xssCleanPostInput('city');
				$state = $this->czsecurity->xssCleanPostInput('state');
				$zip = $this->czsecurity->xssCleanPostInput('zipcode');
				$country= $this->czsecurity->xssCleanPostInput('country');
				$phone = $this->czsecurity->xssCleanPostInput('contact');
				
				$card_data = array(
				  'cardMonth'   =>$this->czsecurity->xssCleanPostInput('expiry'),
				  'cardYear' => $this->czsecurity->xssCleanPostInput('expiry_year'),
				  'CardType'=>$card_type,
				  'CustomerCard' =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('card_number')),
				  'CardCVV'      =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('cvv')), 
				  'customerListID' =>$customerID['Customer_ListID'],                           
				  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
				  'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
				  'Billing_City'=> $city,
				  'Billing_Country'=>$country,
				  'Billing_Contact'=>$phone,
				  'Billing_State'=>  $state,
				  'Billing_Zipcode'=> $zip,
				  'merchantID'   => $merchantID,
				  'customerCardfriendlyName'=>$friendlyname,
				  'createdAt' 	=> date("Y-m-d H:i:s") 
			   );
		
    		  }else{
    		      
    		      $card_data = array();
    		      $cardID = $this->czsecurity->xssCleanPostInput('CardID');
    		      $card_data    =   $this->get_carddata($cardID);
    		        
    		        $card_no = $card_data['CardNo'];
    				$expmonth =  $card_data['cardMonth'];
    				$exyear   = $card_data['cardYear'];
    				$expry    = $expmonth.$exyear; 
    				
    		     
    		      $address1 = $card_data['address'].' '.$card_data['address1'];
    			  $city = $card_data['city'];
    		      $state = $card_data['state'];
                  $zip =$card_data['zip'];
    			  $country= $card_data['country'];
    			  $phone = $card_data['contact'];
    		}	
    		
    		
			
			$transaction->__set('company',$get_cust['companyName']);
			$transaction->__set('first_name',$get_cust['firstName']);
			$transaction->__set('last_name',  $get_cust['lastName']);
			$transaction->__set('address',  $address1);
			$transaction->__set('country', $country);
			$transaction->__set('city', $city);
			$transaction->__set('state', $state);
			$transaction->__set('phone', $phone);
			$transaction->__set('email', $get_cust['userEmail']);
			$amount = $amount;
			$result = $transaction->authorizeAndCapture($amount,$card_no,$expry);
	
			if($result->response_code == '1'){
			    $nf = $this->addNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
			    $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                                    				
                if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
					$theInvoice = current($targetInvoiceArray);
				}
			    $updatedInvoice = Invoice::update($theInvoice, [
                                    				   
				    "sparse " => 'true'
				]);
                                    				
                $updatedResult = $dataService->Update($updatedInvoice);
			    $newPaymentObj = Payment::create([
				    "TotalAmt" => $amount,
				    "SyncToken" => $updatedResult->SyncToken,
				    "CustomerRef" => $updatedResult->CustomerRef,
				     
				    "Line" => [
				     "LinkedTxn" =>[
				            "TxnId" => $updatedResult->Id,
				            "TxnType" => "Invoice",
				        ],    
				       "Amount" => $amount
			        ]
				 ]);
				    
				$savedPayment = $dataService->Add($newPaymentObj);
				
				$transtion = array(
				    'transactionID'=>$result->transaction_id,
				    'transactionDate'=>  date("Y-m-d H:i:s"), 
				    'transactionStatus'=>$result->response_reason_text,
				    'transactionAmount'=> $amount,
				    'customerListID'=> $customerID['Customer_ListID'],
				    'transactionModified'=> date("Y-m-d H:i:s"),  
				    'transactionType'=>   $result->transaction_type,
				    'transactionGateway'=>$chk_gateway->gatewayType, 
				    'gateway'=>'NMI',
				    'gatewayID'=> $chk_gateway->gatewayType, 
				    'transactionCode'=>$result->response_code,
				    'merchantID'=>$merchantID,
				    'resellerID'=> resellerID($merchantID), 
				    'qbListTxnID'   => $savedPayment->Id,
			        'invoiceID'  =>$invoiceID
				);
				
				$this->db->trans_begin();
					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transtion['transactionCode']);
					if(!empty($this->transactionByUser)){
					    $transtion['transaction_by_user_type'] = $this->transactionByUser['type'];
					    $transtion['transaction_by_user_id'] = $this->transactionByUser['id'];
					}
						
				    $this->general_model->insert_row('customer_transaction',$transtion); 

				    /*Saved default card if not any exits (Only for new card)*/

				    $is_default = 0;
			     	$checkCustomerCard = checkCustomerCard($customerID['Customer_ListID'],$merchantID);
		        	if($checkCustomerCard == 0){
		        		$is_default = 1;
		        	}
		        	$card_data['is_default'] = $is_default;

					$this->db1->insert('customer_card_data', $card_data);
				if ($this->db->trans_status() === FALSE)
				{
					 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>'); 
					 $this->db->trans_rollback();
				
				}else{
				    $condition = array('invoiceID'=>$invoiceID,'merchantID'=>$merchantID,'CustomerListID'=>$customerID['customerID']);
				    $set = array('IsPaid'=>'1','BalanceRemaining'=>'0.00');
				    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $set);
				    $this->db->trans_commit();
				    redirect('QBO_controllers/home/index');
				}
			}else{
				$nf = $this->failedNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>');  
			    redirect('company/home/index');
			}
	    }
	}
	
	
	
	
	
	
	
	
	function paytracePayment()
	{
	    
	     $customerID     = $this->session->userdata('customer_logged_in');
	    $merchantID =  $customerID['merchantID'];
	    $con = array('merchantID'=>$merchantID);
	    $con1 = array('customerID'=>$customerID['customerID']);
	    $get_cust = $this->general_model->get_row_data('chargezoom_test_customer',$con1);
	    $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
	    // QBO INtregration 
	    $data = $this->general_model->get_row_data('QBO_token',$con);
        $accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID']; 
	    $dataService = DataService::Configure(array(
		 'auth_mode' => 'oauth2',
		 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
		 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		 'accessTokenKey' =>  $accessToken,
		 'refreshTokenKey' => $refreshToken,
		 'QBORealmID' => $realmID,
		 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));
		
		$amount = $this->czsecurity->xssCleanPostInput('inv_amount');	
		$gid = $this->czsecurity->xssCleanPostInput('gateway');
		$con = "merchantID = '".$merchantID."'  AND  gatewayID = $gid";
		$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
	    
	    if(!empty($customerID['Customer_ListID'])){ 
	        
	        $card_no='';
		    $expmonth='';
		    $exyear='';
		   
			$payusername   = $chk_gateway->gatewayUsername;
			$paypassword   = $chk_gateway->gatewayPassword;
			$integratorId  = $chk_gateway->gatewaySignature;
			$grant_type    = "password";
			$payAPI = new PayTraceAPINEW();
		    //set the properties for this request in the class
			$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
		 
			$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
			if(!$oauth_moveforward){
				//Decode the Raw Json response.
				$json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
				
				if($this->czsecurity->xssCleanPostInput('CardID') =="new1"){	
			    
				$card_no = $this->czsecurity->xssCleanPostInput('card_number');
				$expmonth =  $this->czsecurity->xssCleanPostInput('expiry');
				$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
				
				$expry    = $expmonth.$exyear;
				$cvv = $this->czsecurity->xssCleanPostInput('cvv');
				$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				$address1 = $this->czsecurity->xssCleanPostInput('address1').' '.$this->czsecurity->xssCleanPostInput('address2');
				$city = $this->czsecurity->xssCleanPostInput('city');
				$state = $this->czsecurity->xssCleanPostInput('state');
				$zip = $this->czsecurity->xssCleanPostInput('zipcode');
				$country= $this->czsecurity->xssCleanPostInput('country');
				$phone = $this->czsecurity->xssCleanPostInput('contact');
				
				$card_data = array(
				  'cardMonth'   =>$this->czsecurity->xssCleanPostInput('expiry'),
				  'cardYear' => $this->czsecurity->xssCleanPostInput('expiry_year'),
				  'CardType'=>$card_type,
				  'CustomerCard' =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('card_number')),
				  'CardCVV'      =>$this->encrypt->encode($this->czsecurity->xssCleanPostInput('cvv')), 
				  'customerListID' =>$customerID['Customer_ListID'],                           
				  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
				  'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
				  'Billing_City'=> $city,
				  'Billing_Country'=>$country,
				  'Billing_Contact'=>$phone,
				  'Billing_State'=>  $state,
				  'Billing_Zipcode'=> $zip,
				  'merchantID'   => $merchantID,
				  'customerCardfriendlyName'=>$friendlyname,
				  'createdAt' 	=> date("Y-m-d H:i:s") 
			   );
		
    		  }else{
    		      
    		      $card_data = array();
    		      $cardID = $this->czsecurity->xssCleanPostInput('CardID');
    		      $card_data    =   $this->get_carddata($cardID);
    		        
    		        $card_no = $card_data['CardNo'];
    				$expmonth =  $card_data['cardMonth'];
    				$exyear   = $card_data['cardYear'];
    				$expry    = $expmonth.$exyear; 
    				
    		      $ccv  = $card_data['ccv'];
    		      $address1 = $card_data['address'].' '.$card_data['address1'];
    			  $city = $card_data['city'];
    		      $state = $card_data['state'];
                  $zip =$card_data['zip'];
    			  $country= $card_data['country'];
    			  $phone = $card_data['contact'];
    		}	
    		
			
				
				$request_data = array(
                "amount" => $amount,
                "credit_card"=> array (
                     "number"=> $card_no,
                     "expiration_month"=> $expmonth,
                     "expiration_year"=> $exyear ),
                "csc"=>$cvv,
               "billing_address"=> array(
                    "name"=>$get_cust['firstName'].'' . $get_cust['lastName'],
                    "street_address"=> $address1,
                    "city"=> $city,
					"state"=> $state,
					"zip"=> $zip
                    )
				);
				
				$request_data = json_encode($request_data);
			 
				
				 $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );		
				$response = $payAPI->jsonDecode($result['temp_json_response']); 


    			if ( $result['http_status_code']=='200' ){
    			    // add level three data in transaction
                    if($response['success']){
                        $level_three_data = [
                            'card_no' => $card_no,
                            'merchID' => $merchantID,
                            'amount' => $amount,
                            'token' => $oauth_token,
                            'integrator_id' => $integratorId,
                            'transaction_id' => $response['transaction_id'],
                            'invoice_id' => $invoiceID,
                            'gateway' => 3,
                        ];
                        addlevelThreeDataInTransaction($level_three_data);
                    }
                    
    			     $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                                    				
                    if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
    					$theInvoice = current($targetInvoiceArray);
    				}
    			    $updatedInvoice = Invoice::update($theInvoice, [
                                        				   
    				    "sparse " => 'true'
    				]);
                                        				
                    $updatedResult = $dataService->Update($updatedInvoice);
    			    $newPaymentObj = Payment::create([
    				    "TotalAmt" => $amount,
    				    "SyncToken" => $updatedResult->SyncToken,
    				    "CustomerRef" => $updatedResult->CustomerRef,
    				     
    				    "Line" => [
    				     "LinkedTxn" =>[
    				            "TxnId" => $updatedResult->Id,
    				            "TxnType" => "Invoice",
    				        ],    
    				       "Amount" => $amount
    			        ]
    				 ]);
    				    
    				$savedPayment = $dataService->Add($newPaymentObj);
    				
    				$transtion = array(
    				    'transactionID'=>$result->transaction_id,
    				    'transactionDate'=>  date("Y-m-d H:i:s"), 
    				    'transactionStatus'=>$result->response_reason_text,
    				    'transactionAmount'=> $amount,
    				    'customerListID'=> $customerID['Customer_ListID'],
    				    'transactionModified'=> date("Y-m-d H:i:s"),  
    				    'transactionType'=>   $result->transaction_type,
    				    'transactionGateway'=>$chk_gateway->gatewayType, 
    				    'gateway'=>'NMI',
    				    'gatewayID'=> $chk_gateway->gatewayType, 
    				    'transactionCode'=>$result->response_code,
    				    'merchantID'=>$merchantID,
    				    'resellerID'=> resellerID($merchantID), 
    				    'qbListTxnID'   => $savedPayment->Id,
    			        'invoiceID'  =>$invoiceID
    				);
    				
    				$this->db->trans_begin();
    					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transtion['transactionCode']);					
    					if(!empty($this->transactionByUser)){
						    $transtion['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transtion['transaction_by_user_id'] = $this->transactionByUser['id'];
						}
    				    $this->general_model->insert_row('customer_transaction',$transtion); 

    				    /*Saved default card if not any exits (Only for new card)*/

					    $is_default = 0;
				     	$checkCustomerCard = checkCustomerCard($customerID['Customer_ListID'],$merchantID);
			        	if($checkCustomerCard == 0){
			        		$is_default = 1;
			        	}
			        	$card_data['is_default'] = $is_default;

						$this->db1->insert('customer_card_data', $card_data);



    					$this->db1->insert('customer_card_data', $card_data);
    				if ($this->db->trans_status() === FALSE)
    				{
    					$nf = $this->failedNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
    					 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>'); 
    					 $this->db->trans_rollback();
    				
    				}else{
    					$nf = $this->addNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
    				    $condition = array('invoiceID'=>$invoiceID,'merchantID'=>$merchantID,'CustomerListID'=>$customerID['customerID']);
    				    $set = array('IsPaid'=>'1','BalanceRemaining'=>'0.00');
    				    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $set);
    				    $this->db->trans_commit();
    				    redirect('company/home/index');
    				}
    			}
    			else{
    			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>');  
			        redirect('company/home/index'); 
    			}
			}
	    }
	}
	
	
	
	
	function stripePayment()
	{
	      require_once APPPATH."third_party/stripe/init.php";	
		require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
	     $customerID     = $this->session->userdata('customer_logged_in');
	    $merchantID =  $customerID['merchantID'];
	    $con = array('merchantID'=>$merchantID);
	    $con1 = array('Customer_ListID'=>$customerID['customerID'],'merchantID'=>$merchantID);
	    $get_cust = $this->general_model->get_row_data('chargezoom_test_customer',$con1);
	    $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
	    // QBO INtregration 
	    $data = $this->general_model->get_row_data('QBO_token',$con);
        $accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID']; 
	    $dataService = DataService::Configure(array(
		 'auth_mode' => 'oauth2',
		 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
		 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		 'accessTokenKey' =>  $accessToken,
		 'refreshTokenKey' => $refreshToken,
		 'QBORealmID' => $realmID,
		 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));
	if(!empty($this->czsecurity->xssCleanPostInput('stripeToken')))
	{
		$amount = $this->czsecurity->xssCleanPostInput('inv_amount');	
		$gid = $this->czsecurity->xssCleanPostInput('gateway');
		$con = "merchantID = '".$merchantID."'  AND  gatewayID = $gid";
		$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
	    
	    if(!empty($customerID['Customer_ListID']))
	    { 
	        
	        $card_no='';
		    $expmonth='';
		    $exyear='';
		   
			$payusername   = $chk_gateway->gatewayUsername;
			$paypassword   = $chk_gateway->gatewayPassword;
			
		
			
    		      
    		      $card_data = array();
    		      $cardID = $this->czsecurity->xssCleanPostInput('CardID');
    		      $card_data    =   $this->get_carddata($cardID);
    		        
    		        $card_no = $card_data['CardNo'];
    				$expmonth =  $card_data['cardMonth'];
    				$exyear   = $card_data['cardYear'];
    				$expry    = $expmonth.$exyear; 
    				
    		      $ccv  = $card_data['CardCVV'];
    		      $address1 = $card_data['address'].' '.$card_data['address1'];
    			  $city = $card_data['city'];
    		      $state = $card_data['state'];
                  $zip =$card_data['zip'];
    			  $country= $card_data['country'];
    			  $phone = $card_data['contact'];
    		
    				$amount =  (int)($amount*100);
				
				
							\Stripe\Stripe::setApiKey($paypassword);
							$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
							$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge Using Stripe Gateway",
								 
								));	
					   
						   $charge= json_encode($charge);
						   $result = json_decode($charge);
						   
						    $trID='';
						    	$qblsID='';
				if($result->paid=='1' && $result->failure_code=="")
				{
					$nf = $this->addNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
				  $code		 =  '200';
				  $trID 	 = $result->id;
				  $inv_amount   = $amount/100;
				   $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                                    				
                    if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1){
    					$theInvoice = current($targetInvoiceArray);
    				}
    			    $updatedInvoice = Invoice::update($theInvoice, [
                                        				   
    				    "sparse " => 'true'
    				]);
                                        				
                    $updatedResult = $dataService->Update($updatedInvoice);
    			    $newPaymentObj = Payment::create([
    				    "TotalAmt" => $inv_amount,
    				    "SyncToken" => $updatedResult->SyncToken,
    				    "CustomerRef" => $updatedResult->CustomerRef,
    				     
    				    "Line" => [
    				     "LinkedTxn" =>[
    				            "TxnId" => $updatedResult->Id,
    				            "TxnType" => "Invoice",
    				        ],    
    				       "Amount" => $inv_amount
    			        ]
    				 ]);
    				    $condition = array('invoiceID'=>$invoiceID,'merchantID'=>$merchantID,'CustomerListID'=>$customerID['customerID']);
    				    $set = array('IsPaid'=>'1','BalanceRemaining'=>'0.00');
    				    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $set);  
    				$savedPayment = $dataService->Add($newPaymentObj);
    				$qblsID = $savedPayment->Id;
    				
    				
				}
				  else{   
				  			$nf = $this->failedNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
				  			$amount= $amount/100;
					       $code =  $result->failure_code;
					       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: '.$result->status.'</strong>.</div>'); 
					   	
					   }  
			
			
    				
    				$transtion = array(
    				    'transactionID'=>$trID,
    				    'transactionDate'=>  date("Y-m-d H:i:s"), 
    				    'transactionStatus'=>$result->status,
    				    'transactionAmount'=> $amount,
    				    'customerListID'=> $customerID['Customer_ListID'],
    				    'transactionModified'=> date("Y-m-d H:i:s"),  
    				    'transactionType'=>   'stripe_sale',
    				    'transactionGateway'=>$chk_gateway->gatewayType, 
    				    'gateway'=>'Stripe',
    				    'gatewayID'=> $chk_gateway->gatewayID, 
    				    'transactionCode'=>$code,
    				    'merchantID'=>$merchantID,
    				    'resellerID'=> resellerID($merchantID), 
    				    'qbListTxnID'   => $qblsID,
    			        'invoiceID'  =>$invoiceID
    				);
    				
    				$this->db->trans_begin();
    					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transtion['transactionCode']);					
    					if(!empty($this->transactionByUser)){
						    $transtion['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transtion['transaction_by_user_id'] = $this->transactionByUser['id'];
						}		
    				    $this->general_model->insert_row('customer_transaction',$transtion); 
    				    	if($this->czsecurity->xssCleanPostInput('CardID') =="new1")
    				    	{
    				    		/*Saved default card if not any exits (Only for new card)*/

							    $is_default = 0;
						     	$checkCustomerCard = checkCustomerCard($customerID['Customer_ListID'],$merchantID);
					        	if($checkCustomerCard == 0){
					        		$is_default = 1;
					        	}
					        	$card_data['is_default'] = $is_default;

    				    		
    				        	$this->db1->insert('customer_card_data', $card_data);
    				    	}
    				if ($this->db->trans_status() === FALSE)
    				{
    					 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>'); 
    					 $this->db->trans_rollback();
    				
    				}else{
    				  
    				    $this->db->trans_commit();
    				    redirect('company/home/index');
    				}
    			
			}
	    }
	    else{
	       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Payment token not valid</div>');  
	     redirect('company/home/index'); 
	 }   
	}
	
	
	function paypalPayment()
	{
	    
	     $customerID     = $this->session->userdata('customer_logged_in');
	      include APPPATH . 'third_party/PayPalAPINEW.php';
	    $merchantID =  $customerID['merchantID'];
	    $con = array('merchantID'=>$merchantID);
	    $con1 = array('Customer_ListID'=>$customerID['customerID'],'merchantID'=>$merchantID);
	    $get_cust = $this->general_model->get_row_data('chargezoom_test_customer',$con1);
	  
	    $invoiceID = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
	    // QBO INtregration 
	    $data = $this->general_model->get_row_data('QBO_token',$con);
        $accessToken = $data['accessToken'];
		$refreshToken = $data['refreshToken'];
	    $realmID      = $data['realmID']; 
	    $dataService = DataService::Configure(array(
		 'auth_mode' => 'oauth2',
		 'ClientID' => "Q0kk26YxI40k2YGblYqRRH4T1Lk3aqxoAcxKlIwjrQeDMsQBh5",
		 'ClientSecret' => "3YFUBmFR56jIXIYzfJAXs66BUTXwSBRh5RvxZtIA",
		 'accessTokenKey' =>  $accessToken,
		 'refreshTokenKey' => $refreshToken,
		 'QBORealmID' => $realmID,
		 'baseUrl' => "https://sandbox-quickbooks.api.intuit.com/"
		));

		$amount = $this->czsecurity->xssCleanPostInput('inv_amount');	
		$gid = $this->czsecurity->xssCleanPostInput('gateway');
		$con = "merchantID = '".$merchantID."'  AND  gatewayID = $gid";
		$chk_gateway = $this->general_model->get_rows('tbl_merchant_gateway',$con);
	    
	    if(!empty($customerID['Customer_ListID']))
	    { 
	        
	        $card_no='';
		    $expmonth='';
		    $exyear='';
		    
		    
		    
    			 	$payusername   = $chk_gateway->gatewayUsername;
		        	$paypassword   = $chk_gateway->gatewayPassword;
					$signature     = $chk_gateway->gatewaySignature;
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $payusername, 	// PayPal API username of the API caller
						'APIPassword' => $paypassword,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
		   
		
	
			if($payusername){
				         
						  
			
    		      $card_data = array();
    		      $cardID = $this->czsecurity->xssCleanPostInput('CardID');
    		      $card_data    =   $this->get_carddata($cardID);
    		      
    		      $address1 = $card_data['address']; 
    		      $address2   = $card_data['address1'];
    			  $city = $card_data['city'];
    		      $state = $card_data['state'];
                  $zip =$card_data['zip'];
    			  $country= $card_data['country'];
    			  $phone = $card_data['contact'];
    			  
    			                  $creditCardType      = $card_data['CardType'];
					  		 
							$creditCardNumber = $card_data['CardNo'];
						    $expDateMonth     =  $card_data['cardMonth'];
							$expDateYear      = $card_data['cardYear'];
								
								// Month must be padded with leading zero
					     	$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								$cvv2Number =   $card_data['CardCVV'];
							$currencyID     = "USD";
						  
                      
								$email         = $get_cust['userEmail'];
								  $companyName =  $get_cust['companyName']; 
								     $firstName = $get_cust['firstName'];
                            $lastName =  $get_cust['lastName']; 
							
								
								
								
								$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
					
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
    			    
                       	 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) 
                       	 {  
						$nf = $this->addNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
					     $code = '111';
					     $trID = $PayPalResult['TRANSACTIONID'];  
					     $amt  = $PayPalResult["AMT"]; 
					       $inv_amount   = $amt;
				   $targetInvoiceArray = $dataService->Query("select * from Invoice where Id='$invoiceID'");
                                    				
                    if(!empty($targetInvoiceArray) && sizeof($targetInvoiceArray) == 1)
                    {
    					$theInvoice = current($targetInvoiceArray);
    				}
    			    $updatedInvoice = Invoice::update($theInvoice, [
                                        				   
    				    "sparse " => 'true'
    				]);
                                        				
                    $updatedResult = $dataService->Update($updatedInvoice);
    			    $newPaymentObj = Payment::create([
    				    "TotalAmt" => $inv_amount,
    				    "SyncToken" => $updatedResult->SyncToken,
    				    "CustomerRef" => $updatedResult->CustomerRef,
    				     
    				    "Line" => [
    				     "LinkedTxn" =>[
    				            "TxnId" => $updatedResult->Id,
    				            "TxnType" => "Invoice",
    				        ],    
    				       "Amount" => $inv_amount
    			        ]
    				 ]);
    				    
    				$savedPayment = $dataService->Add($newPaymentObj);
    				$qblsID = $savedPayment->Id;
    				
    				  $condition = array('invoiceID'=>$invoiceID,'merchantID'=>$merchantID,'CustomerListID'=>$customerID['customerID']);
    				    $set = array('IsPaid'=>'1','BalanceRemaining'=>'0.00');
    				    $this->general_model->update_row_data('chargezoom_test_invoice',$condition, $set);
                       	 }
                       	 else{
                       	 				$nf = $this->failedNotificationForMerchant($amount,$get_cust['FullName'],$customerID['Customer_ListID'],$customerID['merchantID'],$invoiceID);
                       	                 $code = '401';
                       	              	 $responsetext= $PayPalResult['L_LONGMESSAGE0'];
				                       	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: "'.$responsetext.'"</strong>.</div>'); 
						
                       	 }
			          	 $trID='';	$qblsID='';
		
			
    				
    				$transtion = array(
    				    'transactionID'=>$trID,
    				    'transactionDate'=> date('Y-m-d H:i:s',strtotime($PayPalResult["TIMESTAMP"])), 
    				    'transactionStatus'=>$PayPalResult["ACK"],
    				    'transactionAmount'=> $amount,
    				    'customerListID'=> $customerID['Customer_ListID'],
    				    'transactionModified'=> date("Y-m-d H:i:s"),  
    				    'transactionType'=>   'Paypal_sale',
    				    'transactionGateway'=>$chk_gateway->gatewayType, 
    				    'gateway'=>'Paypal',
    				    'gatewayID'=> $chk_gateway->gatewayID, 
    				    'transactionCode'=>$code,
    				    'merchantID'=>$merchantID,
    				    'resellerID'=> resellerID($merchantID), 
    				    'qbListTxnID'   => $qblsID,
    			        'invoiceID'  =>$invoiceID
    				);
    				
    			    	$this->db->trans_begin();
    					$CallCampaign = $this->general_model->triggerCampaign($merchantID,$transtion['transactionCode']);						
    					if(!empty($this->transactionByUser)){
						    $transtion['transaction_by_user_type'] = $this->transactionByUser['type'];
						    $transtion['transaction_by_user_id'] = $this->transactionByUser['id'];
						}		
    				    $this->general_model->insert_row('customer_transaction',$transtion); 
    				    
    				if ($this->db->trans_status() === FALSE)
    				{
    					 $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>'); 
    					 $this->db->trans_rollback();
    				
    				}else{
    				  
    				    $this->db->trans_commit();
    				    redirect('company/home/index');
    				}
    			}
    			else{
    			    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error </strong> Transition Rollback</div>');  
			        redirect('company/home/index'); 
    			}
			}


	}
	
	
	
    public function get_carddata($cardID){  
  
        $card = array();
       	$this->load->library('encrypt');
        $this->db1->select('*');
		$this->db1->from('customer_card_data');
		$this->db1->where('CardID',$cardID);
		$query = $this->db1->get();
		$card_data = $query->row_array();
		
		if(!empty($card_data )){	
				
			  $card['CardNo']     = $this->encrypt->decode($card_data['CustomerCard']) ;
			  $card['cardMonth']  = $card_data['cardMonth'];
			  $card['cardYear']  = $card_data['cardYear'];
			  $card['CardID']    = $card_data['CardID'];
			  $card['CardCVV']   = $this->encrypt->decode($card_data['CardCVV']);
			  $card['CardType']   = $card_data['CardType'];
			  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
			  $card['address']  = $card_data['Billing_Addr1'] ;
			  $card['address1']  = $card_data['Billing_Addr2'] ;
			  $card['city']  = $card_data['Billing_City'] ;
			  $card['state']  = $card_data['Billing_State'] ; 
			  $card['country']  = $card_data['Billing_Country'] ;
			  $card['zip']  = $card_data['Billing_Zipcode'] ;
			  $card['contact']  = $card_data['Billing_Contact'] ;
		}
		
		return  $card;
    }
    
    public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
        	$title = 'Sale Payments';
        	$nf_desc = 'A payment for '.$customerName.' was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
        	$type = 1;
        }else{
        	$title = 'Invoice Checkout Payments';
        	$in_data =    $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID'=>$invoiceNumber));
            if(isset($in_data['RefNumber'])){
                $invoiceRefNumber = $in_data['RefNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }

        	$nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
        	$type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
    
	public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('chargezoom_test_invoice', array('TxnID'=>$invoiceNumber));
        if(isset($in_data['RefNumber'])){
            $invoiceRefNumber = $in_data['RefNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 1,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
}