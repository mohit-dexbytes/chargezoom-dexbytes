<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
 
 
 
 
error_reporting(0);
error_reporting(E_ERROR | E_PARSE);


 use QuickBooksOnline\API\Core\ServiceContext;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\PlatformService\PlatformService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use QuickBooksOnline\API\Facades\Line;
use QuickBooksOnline\API\Facades\Payment;
use QuickBooksOnline\API\Facades\Purchase;
use QuickBooksOnline\API\Facades\RefundReceipt;




use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServiceConfigs\Gateways\PorticoConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
class NMIPayment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
    
	
		   $this->load->config('paypal');
		$this->load->config('quickbooks');
		$this->load->config('paytrace');
		$this->load->model('quickbooks');
		 $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);
		$this->load->model('customer_model');
		$this->load->model('general_model');
		$this->load->model('company_model');
		$this->load->model('card_model');
		
			$this->db1 = $this->load->database('otherdb', true);   

		if(!$this->session->userdata('customer_logged_in'))
		{
			redirect('login', 'refresh');
		}
		    
		
	}
	
	
	public function index(){

	}
	
	 
	 	   
	   
	 public function payment_transaction(){

	
		        $data['primary_nav'] 	= primary_customer_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('customer_logged_in');
				
			    $user_id 				= $data['login_info']['ListID'];
				
		        $transtion   = $this->customer_model->get_transaction_data($user_id);
		        
		        $data['transactions'] = $transtion;
				$this->load->view('template/template_start', $data);
				$this->load->view('template/customer_head', $data);
				$this->load->view('customer/payment_transaction', $data);
				$this->load->view('template/customer_footer',$data);
				$this->load->view('template/template_end', $data);


    }	



	public function customer_card(){
		
	
	       $report=array();
		   $data['primary_nav'] 	= primary_customer_nav();
			$data['template'] 		= template_variable();
			$data['login_info'] 	= $this->session->userdata('customer_logged_in');
			$user_id 				= $data['login_info']['ListID'];
				
		      
		   $card_data =  $this->card_model->get_card_expiry_data($user_id);
	     
		 $data['report6']	    = 	$card_data;
			
         
		    
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/customer_head', $data);
				$this->load->view('customer/payment_card', $data);
				$this->load->view('template/customer_footer',$data);
				$this->load->view('template/template_end', $data);			
			
			
			
	}

	


	  
	  public function insert_new_data(){
	      
	       
                      
			            $customer = $this->czsecurity->xssCleanPostInput('customerID11');	 
			        	
			        	$c_data    = $this->general_model->get_row_data('qb_test_customer', array('ListID'=>$customer));
			            $companyID = $c_data['companyID'];
						$comp_data = $this->general_model->get_row_data('tbl_company', array('id'=>$companyID));
					 	$mechantID = $comp_data['merchantID']; 
				
			        	if(!empty($this->input->post(null, true)))
                		{
							$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
						$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
						$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
						$b_city = $this->czsecurity->xssCleanPostInput('city');
						$b_state = $this->czsecurity->xssCleanPostInput('state');
						$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
						$b_country = $this->czsecurity->xssCleanPostInput('country');
						$b_contact = $this->czsecurity->xssCleanPostInput('contact');
				      
						$card_type = $this->general_model->getType($card_no);		
		
		           	$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->card_model->encrypt($card_no),
										  'CardCVV'      =>$this->card_model->encrypt($cvv), 
										  'CardType'      =>$card_type, 
										 'customerListID' =>$customer, 
										 'merchantID'     =>$mechantID,
										 'companyID'      => $companyID, 
										
										    'Billing_Addr1'     =>$b_addr1,
										    'Billing_Addr2'     =>$b_addr2,
										    'Billing_City'      =>$b_city,
										    'Billing_State'     =>$b_state,
										    'Billing_Zipcode'   =>$b_zipcode,
										    'Billing_Country'   =>$b_country,
										    'Billing_Contact'   =>$b_contact,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
		
			                  $id = $this->card_model->insert_card_data($insert_array);	
					

		 if( !empty($id) ){
		     
		     
		     
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
		 }
		}
		 
	    	redirect('home/index','refresh');
	  }
	  
	 
	 
	 
	public function delete_card_data(){
		
		    
			    $cardID =  $this->uri->segment('3'); 
			   
		       $sts =  $this->db1->query("Delete from customer_card_data where CardID = $cardID ");
				 if($sts){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
		 }
		 
	    	redirect('home/index','refresh');
	}		
	  
	  public function update_card_data()
	  {
	      
	         	$da	= $this->session->userdata('customer_logged_in');
        			
        			    $merchID 				= $da['merchantID'];

			        	$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');
			        	
						$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('edit_cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
						$b_addr1 = $this->czsecurity->xssCleanPostInput('baddress1');
				        $b_addr2 = $this->czsecurity->xssCleanPostInput('baddress2');
				        $b_city = $this->czsecurity->xssCleanPostInput('bcity');
				        $b_state = $this->czsecurity->xssCleanPostInput('bstate');
				        $b_country = $this->czsecurity->xssCleanPostInput('bcountry');
				        $b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
				        $b_zip = $this->czsecurity->xssCleanPostInput('bzipcode');
				        $merchantID = $merchID;
								
			         $condition = array('CardID'=>$cardID);
		           	$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CardCVV'      =>$this->card_model->encrypt($cvv), 
										 'customerCardfriendlyName'=>$friendlyname,
										    'Billing_Addr1'     =>$b_addr1,
										    'Billing_Addr2'     =>$b_addr2,
										    'Billing_City'      =>$b_city,
										    'Billing_State'     =>$b_state,
										    'Billing_Zipcode'   =>$b_zip,
										    'Billing_Country'   =>$b_country,
										    'Billing_Contact'   =>$b_contact,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
				      if($this->czsecurity->xssCleanPostInput('edit_card_number')!='')
				      {
						$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number'); 						 
				     	$insert_array['CustomerCard'] =$this->card_model->encrypt($card_no);
				     	$card_type = $this->general_model->getType($card_no);
				     	$insert_array['CardType'] =$card_type;
				     	
				      }					 
             
			 $id = $this->card_model->update_card_data($condition,  $insert_array);
		 if( $id ){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
		 }
		 
	    	redirect('home/index','refresh');
}
	  

	 
	 public function get_card_data(){
		$customerdata =array();
		if($this->czsecurity->xssCleanPostInput('cardID')!=""){
			 $crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			
			if(!empty($card_data)){
			         $customerdata['status'] =  'success';	
					$customerdata['card']     = $card_data;
					echo json_encode($customerdata)	;
					die;
			}
		}
		
		
	}

   public function pay_invoice()
	  {

	    $da = $this->session->userdata('customer_logged_in');
		$merchID 				= $da['qbmerchantID'];

	    $customerID      	= $da['ListID'];
       	 $amount                = $this->czsecurity->xssCleanPostInput('inv_amount');
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
        $error='';
         
	       $in_data =    $this->company_model->get_company_invoice_data_pay($invoiceID);
	        $user_id = $in_data['qbmerchantID'];
	       
		  $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
		
        
           if(!empty($in_data))
            {
                 $con_cust  = array('ListID'=>$customerID,'qbmerchantID'=>$user_id) ;
                  
           $cust_data1 = $this->general_model->get_select_data('qb_test_customer',array('firstName','lastName','companyName','FullName'), $con_cust);
              
                if( $in_data['BalanceRemaining'] !='0.00')
                    {  
                      
                    if($in_data['DueDate']!='' )
                    {
                    if($cardID=='new1')
                    {
                         $card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						 $expmonth = $this->czsecurity->xssCleanPostInput('expiry');
					 	 $exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
					 	 $cvv      = $this->czsecurity->xssCleanPostInput('cvv');
                                $card_data = array('cardMonth'   =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$card_no,
										  'CardCVV'      =>$cvv,
										  'Billing_Addr1'=> $this->czsecurity->xssCleanPostInput('address1'),
	                                      'Billing_Addr2'=> $this->czsecurity->xssCleanPostInput('address2'),
	                                      'Billing_City'=> $this->czsecurity->xssCleanPostInput('city'),
	                                      'Billing_Country'=> $this->czsecurity->xssCleanPostInput('country'),
	                                      'Billing_Contact'=> $this->czsecurity->xssCleanPostInput('phone'),
	                                      'Billing_State'=> $this->czsecurity->xssCleanPostInput('state'),
	                                      'Billing_Zipcode'=> $this->czsecurity->xssCleanPostInput('zipcode'),
	                                      'CardNo'=>$card_no
                               );           
                       
                    }
                    else
                    {
                        $card_data    =   $this->card_model->get_single_card_data($cardID); 
                  
                        $card_no  = $card_data['CardNo'];
                        $cvv      =  $card_data['CardCVV'];
                        $expmonth =  $card_data['cardMonth'];
                        $exyear   = $card_data['cardYear'];
                        $cardType = $card_data['CardType'];
                        $address1 = $card_data['Billing_Addr1'];
                        $address2=$card_data['Billing_Addr2'];
                        $city     =  $card_data['Billing_City'];
                        $zipcode  = $card_data['Billing_Zipcode'];
                        $state    = $card_data['Billing_State'];
                        $country  = $card_data['Billing_Country'];
                        
                        
                        
                         
                    }
                    $customerID = $in_data['Customer_ListID'];
                    $user_id = $in_data['merchantID'];
                 $m_data = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID'), array('merchID'=>$user_id));
              
	              $resellerID = 	$m_data['resellerID']; 
                    $c_data = $this->general_model->get_select_data('qb_test_customer',array('companyID','companyName', 'Contact','FullName'), array('ListID'=>$customerID, 'qbmerchantID'=>$user_id));
                    
                 
                    $pay_sts = "";
                    $res =array();
                    if($gt_result['gatewayType']=='1')
                    {
                        include APPPATH . 'third_party/nmiDirectPost.class.php';	
                        $nmiuser  = $gt_result['gatewayUsername'];
                        $nmipass  = $gt_result['gatewayPassword'];
                        $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
                        $Customer_ListID = $in_data['Customer_ListID'];
                       
                        
                        $transaction1 = new nmiDirectPost($nmi_data); 
                        $transaction1->setCcNumber($card_data['CardNo']);
                        $expmonth =  $card_data['cardMonth'];
                        $exyear   = $card_data['cardYear'];
                        $exyear   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.$exyear;  
                        $transaction1->setCcExp($expry);
                        $transaction1->setCvv($card_data['CardCVV']);
                        
                        $transaction1->setAmount($amount);
                        // add level III data
                        $level_request_data = [
                            'transaction' => $transaction1,
                            'card_no' => $card_data['CardNo'],
                            'merchID' => $user_id,
                            'amount' => $amount,
                            'invoice_id' => $invoiceID,
                            'gateway' => 1
                        ];
                        $transaction1 = addlevelThreeDataInTransaction($level_request_data);
                        $transaction1->sale();
                        $result = $transaction1->execute();
                        $type='sale' ; 
                        if( $result['response_code']=="100")
                        {
                        $pay_sts = "SUCCESS";
                        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>');  
                        
                        }
                        else
                        {
                         $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>'); 
                         $res = $result; 
                        
                        }   
                       $res = $result;  
                        
                    }
                    
                    if($gt_result['gatewayType']=='2')
                    {
                        include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
                        $apiloginID       = $gt_result['gatewayUsername'];
                        $transactionKey   = $gt_result['gatewayPassword'];
                        
                        $transaction1 = new AuthorizeNetAIM($apiloginID,$transactionKey); 
                        
                        
                        $transaction1->setSandbox($this->config->item('Sandbox'));
                        $transaction1->__set('company',$c_data['companyName']);
                     
                        $transaction1->__set('first_name',$c_data['FullName']);
                       
                        $transaction1->__set('address', $card_data['Billing_Addr1']);
                        $transaction1->__set('country',$card_data['Billing_Country']);
                        $transaction1->__set('city',$card_data['Billing_City']);
                        $transaction1->__set('state', $card_data['Billing_State']);
                        $transaction1->__set('zip', $card_data['Billing_Zipcode']);
                        $expmonth =  $card_data['cardMonth'];
                        $exyear   = $card_data['cardYear'];
                        
                        
                        $exyear   = substr($exyear,2);
                        if(strlen($expmonth)==1)
                        {
                        $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.$exyear;  
                        
                        $crtxnID='';
                        $type='authorisation' ;
                        $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
                    
                        if( $result->response_code=="1")
                        {
                      
                        $pay_sts = "SUCCESS";
                         $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Succesfully invoice paid</strong></div>');
                        
                        }else{
                        
                        
                        
                        }
                       
                        $res =  $result;
                         
                    }
                    
                    if($gt_result['gatewayType']=='3')
                    {
                            
                        include APPPATH . 'third_party/PayTraceAPINEW.php';
                        
                        $payusername   = $gt_result['gatewayUsername'];
                        $paypassword   = $gt_result['gatewayPassword'];
						$integratorId = $gt_result['gatewaySignature'];

                        $grant_type    = "password";
                        
                        
                        $payAPI     = new PayTraceAPINEW();	
                        
                        $oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
                        
                        //call a function of Utilities.php to verify if there is any error with OAuth token. 
                        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);	
                        if(!$oauth_moveforward)
                        {	
                        $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
                        
                        //set Authentication value based on the successful oAuth response.
                        //Add a space between 'Bearer' and access _token 
                        $oauth_token = sprintf("Bearer %s",$json['access_token']);
                       
                        $name = $c_data['FullName'];
                        
                        $address =$card_data['Billing_Addr1'];
                        $city =  $card_data['Billing_City'];
                        $state = $card_data['Billing_State'];
                        $zipcode = $card_data['Billing_Zipcode'];
                        
                        $card_no  = $card_data['CardNo'];
                        
                        $expmonth =  $card_data['cardMonth'];
                        
                        $exyear   = $card_data['cardYear'];
                       
                        $cvv      = $card_data['CardCVV'];
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                        
                        $invoice_number = rand('500000','200000');
                        $request_data = array(
                        "amount" => $amount,
                        "credit_card"=> array (
                        "number"=> $card_no,
                        "expiration_month"=>$expmonth,
                        "expiration_year"=>$exyear ),
                        "csc"=> $cvv,
                        "invoice_id"=>$invoice_number,
                        "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
                        )
                        );  
                        $type='sale' ;
                        $request_data = json_encode($request_data); 
                     
                        $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
                        $response  = $payAPI->jsonDecode($result['temp_json_response']); 
                         
                      
                        if ($result['http_status_code']=='200' )
                        {
                        	// add level three data in transaction
		                    if($response['success']){
		                        $level_three_data = [
		                            'card_no' => $card_no,
		                            'merchID' => $user_id,
		                            'amount' => $amount,
		                            'token' => $oauth_token,
		                            'integrator_id' => $integratorId,
		                            'transaction_id' => $response['transaction_id'],
		                            'invoice_id' => $invoice_number,
		                            'gateway' => 3,
		                        ];
		                        addlevelThreeDataInTransaction($level_three_data);
		                    }
		                    
                        $pay_sts = "SUCCESS";
                        $txnID   = $in_data['TxnID'];  
                        $ispaid 	 = 'true';
                        $res = $response;
                        $res['http_status_code'] =$result['http_status_code'];
                        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                        
                       
                        } 
                        else
                        {
                         $res = $response;
                        $res['http_status_code'] =$result['http_status_code'];
                      
                           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>'); 
                     
                        
                     
                        }
                      
                        }  
                    
                    
                    } 
                    
                    if($gt_result['gatewayType']=='4')
                    {
                    
                    
                    
                        $username  = $gt_result['gatewayUsername'];
                        $password  = $gt_result['gatewayPassword'];
                        $signature = $gt_result['gatewaySignature'];
                        
                        
                        $config = array(
                        'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
                        'APIUsername' => $username, 	// PayPal API username of the API caller
                        'APIPassword' => $password,	// PayPal API password of the API caller
                        'APISignature' => $signature, 	// PayPal API signature of the API caller
                        'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
                        'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
                        );
                        
                        // Show Errors
                        if($config['Sandbox'])
                        {
                        error_reporting(E_ALL);
                        ini_set('display_errors', '1');
                        }
                        
                        $this->load->library('paypal/Paypal_pro', $config);	
                        $res=array();
                        
                        $creditCardType   = 'Visa';
                       
                        $creditCardNumber = $card_data['CardNo'];
                        $expDateMonth     =  $card_data['cardMonth'];
                        $expDateYear      = $card_data['cardYear'];
                        $creditCardNumber 	= $card_no;
                        
                        // Month must be padded with leading zero
                        $padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
                        $cvv2Number =   $card_data['CardCVV'];
                        $currencyID     = "USD";
                        $cust_data       = $this->general_model->get_row_data('qb_test_customer', array('ListID'=>$in_data['Customer_ListID']));
                         
                        $firstName = $cust_data1['firstName'];
                        $lastName =  $cust_data1['lastName']; 
                        $address1 = $in_data['ShipAddress_Addr1']; 
                        $address2 = $in_data['ShipAddress_Addr2']; 
                        $country  = $in_data['ShipAddress_Country']; 
                        $city     = $in_data['ShipAddress_City'];
                        $state    = $in_data['ShipAddress_State'];		
                        $zip  = $in_data['ShipAddress_PostalCode']; 
                        $phone = $in_data['Phone']; 
                        $email = $in_data['Contact']; 
                        $companyName	   = $cust_data1['companyName']; 
                        
                        $DPFields = array(
                        'paymentaction' => 'Sale', 	// How you want to obtain payment.  
                        //Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
                        'ipaddress' => '', 							// Required.  IP address of the payer's browser.
                        'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
                        );
                        
                        $CCDetails = array(
                        'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
                        'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
                        'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
                        'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
                        'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
                        'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
                        );
                        
                        $PayerInfo = array(
                        'email' => $email, 								// Email address of payer.
                        'payerid' => '', 							// Unique PayPal customer ID for payer.
                        'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
                        'business' => '' 							// Payer's business name.
                        );  
                        
                        $PayerName = array(
                        'salutation' => $companyName, 						// Payer's salutation.  20 char max.
                        'firstname' => $firstName, 							// Payer's first name.  25 char max.
                        'middlename' => '', 						// Payer's middle name.  25 char max.
                        'lastname' => $lastName, 							// Payer's last name.  25 char max.
                        'suffix' => ''								// Payer's suffix.  12 char max.
                        );
                        
                        $BillingAddress = array(
                        'street' => $address1, 						// Required.  First street address.
                        'street2' => $address2, 						// Second street address.
                        'city' => $city, 							// Required.  Name of City.
                        'state' => $state, 							// Required. Name of State or Province.
                        'countrycode' => $country, 					// Required.  Country code.
                        'zip' => $zip, 							// Required.  Postal code of payer.
                        'phonenum' => $phone 						// Phone Number of payer.  20 char max.
                        );
                        
                        
                        $PaymentDetails = array(
                        'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
                        'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
                        'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
                        'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
                        'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
                        'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
                        'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
                        'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
                        'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
                        'custom' => '', 						// Free-form field for your own use.  256 char max.
                        'invnum' => '', 						// Your own invoice or tracking number
                        'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
                        'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
                        'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
                        );					
                        
                        $PayPalRequestData = array(
                        'DPFields' => $DPFields, 
                        'CCDetails' => $CCDetails, 
                        'PayerInfo' => $PayerInfo, 
                        'PayerName' => $PayerName, 
                        'BillingAddress' => $BillingAddress, 
                        
                        'PaymentDetails' => $PaymentDetails, 
                        
                        );
                        $type='sale' ;			
                        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
                        $result  =$PayPalResult ;
                        
                        
                        if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))) 
                        {  
                        $pay_sts = "SUCCESS";
                         $res =  $result;
                        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                        
                        
                        } 
                        else
                        {
                         $res =  $result;
                         $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>');
                    
                      
                        }  
                     
                       
                    }
                    
                    if($gt_result['gatewayType']=='5')
                    {
                    
                        require_once APPPATH."third_party/stripe/init.php";	
                        require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
                        $stripeKey      = $gt_result['gatewayUsername'];
                        $strPass        = $gt_result['gatewayPassword'];
                      
                       
                        $real_amt  =  (int)($amount*100); 
                        
                        \Stripe\Stripe::setApiKey($stripeKey);
                        
                        $res =  \Stripe\Token::create([
                        'card' => [
                        'number' =>$card_no,
                        'exp_month' => $expmonth,
                        'exp_year' =>  $exyear,
                        'cvc' => $cvv,
                        'name' =>$c_data['FullName']
                        ]
                        ]);
                        
                        $tcharge= json_encode($res);
                        
                        $rest = json_decode($tcharge);
                        if($rest->id)
                        {	
                        \Stripe\Stripe::setApiKey($strPass);
                        
                        $charge =	\Stripe\Charge::create(array(
                        "amount" => $real_amt,
                        "currency" => "usd",
                        "source" => $rest->id, 
                        "description" => "Charge Using Stripe Gateway",
                        
                        ));	
                        
                        $charge= json_encode($charge);
                        
                        $result = json_decode($charge);
                        
                        
                        
                        $type='sale';
                        $crtxnID='';
                        if($result->paid=='1' && $result->failure_code=="")
                        {
                        $pay_sts = "SUCCESS";
                        $res=  $result; 
                         $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                        }
                        else
                        {
                     
                         $res=  $result; 
                         $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>');
                        }
                        
                       
                        }
                    
                    }  
                    if($gt_result['gatewayType']=='6')
                    {
                    
                        require_once APPPATH."third_party/usaepay/usaepay.php";	
                        $this->load->config('usaePay');
                        $username  = $gt_result['gatewayUsername'];
                        $password = $gt_result['gatewayPassword'];
                        $crtxnID='';  
                        $invNo  =mt_rand(1000000,2000000); 
                        $transaction = new umTransaction;
                        $transaction->key=$username;
						            $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;
                        
                        $transaction->pin=$password;
                        $transaction->usesandbox=$this->config->item('Sandbox');
                        $transaction->invoice=$invNo;   		// invoice number.  must be unique.
                        $transaction->description="Chargezoom Invoice Payment";	// description of charge
                        
                        $transaction->testmode=$this->config->item('TESTMODE');;    // Change this to 0 for the transaction to process
                        $transaction->command="sale";	
                        
                        
                        
                        $transaction->card = $card_no;
                        $expyear   = substr($exyear,2);
                        if(strlen($expmonth)==1){
                        $expmonth = '0'.$expmonth;
                        }
                        $expry    = $expmonth.$expyear;  
                        $transaction->exp = $expry;
                        if($cvv!="")
                        $transaction->cvv2 = $cvv;
                       
                        
                        $transaction->billstreet = $card_data['Billing_Addr1'];
                        $transaction->billstreet2 = $card_data['Billing_Addr2'];
                        $transaction->billcountry = $card_data['Billing_Country'];
                        $transaction->billcity = $card_data['Billing_City'];
                        $transaction->billstate = $card_data['Billing_State'];
                        $transaction->billzip = $card_data['Billing_Zipcode'];
                        
                        $transaction->amount = $amount;
                        
                        $transaction->Process();
                        $type = 'sale';
                        
                     
                        if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                        {
                        
                        $msg = $transaction->result;
                        $trID = $transaction->refnum;
                        
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        $pay_sts = "SUCCESS";
                         $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                        
                        }
                        else
                        {
                        $msg  = $transaction->result;
                        $trID = $transaction->refnum;
                        $res =array('transactionCode'=>300, 'status'=>$msg, 'transactionId'=> $trID );
                        $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>');
                     
                    }   
                    
                    
                    
                    }
                    if($gt_result['gatewayType']=='7')
                    {     
                        require_once dirname(__FILE__) . '/../../../vendor/autoload.php';
                        $this->load->config('globalpayments');
                        
                        $crtxnID='';	     
                        
                        $config = new PorticoConfig();
                        $secretApiKey = $gt_result['gatewayPassword'];
                        $config->secretApiKey = $secretApiKey;
                        $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                        
                        
                        
                        ServicesContainer::configureService($config);
                        $card = new CreditCardData();
                        $card->number = $card_no;
                        $card->expMonth = $expmonth;
                        $card->expYear = $exyear;
                        if($cvv!="")
                        $card->cvn = $cvv;
                        
                        $address = new Address();
                     
                        $address->streetAddress1 = $card_data['Billing_Addr1'];
                        $address->city = $card_data['Billing_City'];
                        $address->state = $card_data['Billing_State'];
                        $address->postalCode = $card_data['Billing_Zipcode'];
                        $address->country = $card_data['Billing_Country'];
                          $type='sale';
                        
                        $invNo  =mt_rand(1000000,2000000);
                        try
                        {
                        $response = $card->charge($amount)
                        ->withCurrency('USD')
                        ->withAddress($address)
                        ->withInvoiceNumber($invNo)
                        ->withAllowDuplicates(true)
                        ->execute();
                        
                        
                        if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                        {
                        	
							// add level three data
	                        $transaction = new Transaction();
	                        $transaction->transactionReference = new TransactionReference();
	                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
	                        $level_three_request = [
	                            'card_no' => $card_no,
	                            'amount' => $amount,
	                            'invoice_id' => $invNo,
	                            'merchID' => $user_id,
	                            'transaction_id' => $response->transactionId,
	                            'transaction' => $transaction,
	                            'levelCommercialData' => $levelCommercialData,
	                            'gateway' => 7
	                        ];
	                        addlevelThreeDataInTransaction($level_three_request);

                        $msg = $response->responseMessage;
                        $trID = $response->transactionId;
                        $st='0'; $action='Pay Invoice'; $msg ="Payment Success "; 
                        $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                        $pay_sts = "SUCCESS";
                        $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                        
                        }
                        else
                        {
                        $res =array('transactionCode'=>$response->responseCode, 'status'=>$msg, 'transactionId'=> $trID );   
                         $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>');
                      
                        }
                        
                        
                        }
                        catch (BuilderException $e)
                        {
                        $error= 'Build Exception Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (ConfigurationException $e)
                        {
                        $error='ConfigurationException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (GatewayException $e)
                        {
                        $error= 'GatewayException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (UnsupportedTransactionException $e)
                        {
                        $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                        catch (ApiException $e)
                        {
                        $error=' ApiException Failure: ' . $e->getMessage();
                        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                        }
                    
                    
                    } 
                    if($gt_result['gatewayType']=='8')
                    {
                        $this->load->config('cyber_pay');
                        $gmerchantID    = $gt_result['gatewayUsername'];
    			        $secretApiKey   = $gt_result['gatewayPassword'];
						$secretKey      = $gt_result['gatewaySignature'];
						
    					 $qbID     = $in_data['TxnID'];
    					 $con_cust  = array('ListID'=>$customerID,'qbmerchantID'=>$user_id) ;
                            
					    $cust_data = $this->general_model->get_select_data('qb_test_customer',array('firstName','lastName','companyName','Phone','Contact'), $con_cust);
                        $res=array(); 	   
						$crtxnID='';  
                        $flag="true";
                        $error=$user='';
                        $type='sale';
                     
        				$option =array();
        				        $option['merchantID']     = trim($gt_result['gatewayUsername']);
            			        $option['apiKey']         = trim($gt_result['gatewayPassword']);
        						$option['secretKey']      = trim($gt_result['gatewaySignature']);
        						
        						if($this->config->item('Sandbox'))
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        			
        			         	$commonElement = new CyberSource\ExternalConfiguration($option);
                         
                        $config = $commonElement->ConnectionHost();
                        
                        $merchantConfig = $commonElement->merchantConfigObject();
                        
                    	$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
                    
                        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
                    	
                        $cliRefInfoArr = [
                    		"code" => "Customer_payment"
                    	];
                        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
                    	
                        if ($flag == "true")
                        {
                            $processingInformationArr = [
                    			"capture" => true, "commerceIndicator" => "internet"
                    		];
                        }
                        else
                        {
                            $processingInformationArr = [
                    			"commerceIndicator" => "internet"
                    		];
                        }
                        
                        
                         $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
                        $amountDetailsArr = [
                    		"totalAmount" => $amount,
                    		"currency" => CURRENCY
                    	];	
                    
                        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
                      
                        $billtoArr = [
                    		"firstName" =>  $cust_data['firstName'],
                    		"lastName" => $cust_data['lastName'],
                    		"address1" => $address1,
                    		"postalCode" => $zipcode,
                    		"locality" => $city,
                    		"administrativeArea" => $state,
                    		"country" => $country,
                    		"phoneNumber" => $cust_data['Phone'],
                    		"company" => $c_data['companyName'],
                    		"email" => $cust_data['Contact']
                    	];
                    
                    	
                    
                        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
             	
                        $orderInfoArr = [
                    		"amountDetails" => $amountDetInfo, 
                    		"billTo" => $billto
                    	];
                        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
                    	
                        $paymentCardInfo = [
                        		"expirationYear" => $exyear,
                        		"number" => $card_no,
                        		"securityCode" => $cvv,
                        		"expirationMonth" => $expmonth
                        	];  
                        	
       
                        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
                       
                        $paymentInfoArr = [
                    		"card" => $card
                        ];
                        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
                    
                        $paymentRequestArr = [
                    		"clientReferenceInformation" => $client_reference_information, 
                    		"orderInformation" => $order_information, 
                    		"paymentInformation" => $payment_information, 
                    		"processingInformation" => $processingInformation
                    	];
                        
                        
                       
                      
                        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
                        
                       
                        
                        $api_response = list($response, $statusCode, $httpHeader) = null;
                     
                       try
                        {
                            //Calling the Api
                                  
                            $api_response = $api_instance->createPayment($paymentRequest);
                       
                            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201')
        					{
        					    
        					  $trID =   $api_response[0]['id'];
        					  $msg  =   $api_response[0]['status'];
        					 
        					  $code =   '200';
        					  $pay_sts ="SUCCESS";
        					  
        					  $res =array('transactionCode'=>'200','status'=>$msg, 'transactionId'=> $trID );
        					  
        	
        				$st='0'; $action='Pay Invoice'; 
                         $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>'); 
                         
        			    	}   
        					else
        					{
        					     $trID  =   $api_response[0]['id'];
        					      $msg  =   $api_response[0]['status'];
        					      $code =   $api_response[1];
        					      $res =array('transactionCode'=>$code, 'status'=>$msg, 'transactionId'=> $trID ); 
        					       
        					      $error = $api_response[0]['status'];
        					        $st='0'; $action='Pay Invoice'; $msg ="Payment Failed".$error; $qbID=$in_data['TxnID'];
        					      $this->session->set_flashdata('message','<div class="alert alert-success">  <strong>Payment '.$error.'</strong></div>');
        					}
                            
                        }
                         
                        	catch(Cybersource\ApiException $e)
        				{
        				
        					  $error = $e->getMessage();
        					
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
        				}
        			

                       
                 }


                if($gt_result['gatewayType']=='10')
                {
                  $apiUsername   = $get_gateway['gatewayUsername'];
                  $apiKey   = $get_gateway['gatewayPassword'];

                  $expmonth =  $card_data['cardMonth'];
                  $exyear   = $card_data['cardYear'];
                  
                  if (strlen($expmonth) > 1 && $expmonth <= 9) {
                    $expmonth = substr($expmonth, 1);
                  }

                  $Customer_ListID = $in_data['Customer_ListID'];

                  $request_data = array(
                    "amount"          => ($amount * 100),
                    "card"            => array(
                      "name"      =>  $cust_data['firstName']. " " .  $cust_data['lasstName'],
                      "number"    => $card_data['CardNo'],
                      "exp_month" => $expmonth,
                      "exp_year"  => $exyear,
                      "cvv"       => $card_data['CardCVV'],
                    ),
                    "address" => array(
                      "line1"       => $card_data['Billing_Addr1'],
                      "line2"       => $card_data['Billing_Addr2'],
                      "city"        => $card_data['Billing_City'],
                      "state"       => $card_data['Billing_State'],
                      "postal_code" => $card_data['Billing_Zipcode'],
                    ),
                  );
                
                  
                  $result    = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);
							    if ($result['status_code'] == '200' || $result['status_code'] == '201') {
								    $result['status_code'] = 200;
                    $pay_sts = "SUCCESS";
                    $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Processed Invoice</strong></div>');  
                  }
                  else
                  {
                    $err_msg      = $result['status']      = $result['error']['message'];
                    $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : '';
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$err_msg.'</div>'); 
                  
                  }   
                  $res = $result;  
                    
                }
                    
                    $crtxnID  ='';	
                    if( $pay_sts == "SUCCESS")
                    {
                    
                    $nf = $this->addNotificationForMerchant($amount,$cust_data1['FullName'],$customerID,$merchID,$invoiceID);
                    $txnID   = $in_data['TxnID'];  
                    $ispaid 	 = 'true';
                    
                    $bamount    = $in_data['BalanceRemaining']-$amount;
                    if($bamount > 0)
                    $ispaid 	 = 'false';
                    $app_amount = $in_data['AppliedAmount']+(-$amount);
                    $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>($app_amount) , 'BalanceRemaining'=>$bamount );
                    $condition  = array('TxnID'=>$in_data['TxnID'] );
                    
                    $this->general_model->update_row_data('qb_test_invoice',$condition,$data);
                    
                    }else{
                    	$nf = $this->failedNotificationForMerchant($amount,$cust_data1['FullName'],$customerID,$merchID,$invoiceID);
                    }
                     if(!empty($res))
                     $id = $this->general_model->insert_gateway_transaction_data($res,$type, $gt_result['gatewayID'],$gt_result['gatewayType'],$customerID,$amount,$user_id,$crtxnID, $resellerID,$in_data['TxnID'], false, ['id' => $customerID, 'type' => 3]);  
                    
                    
                    
                }
                    }
                
             	
            }
            else
            {
               	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error Invalid Invoice</strong></div>');  
            }
    	redirect('home/index','refresh');
	  
	      
	  }   


	
	
	public function update_invoice_date()
	{
		 
		 $invoiceID = $this->czsecurity->xssCleanPostInput('scheduleID');
        $due_date  = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date'))); 
		 
		 $condition =  array('TxnID'=>$invoiceID); 
		 $indata    = $this->customer_model->get_invoice_data_byID($invoiceID);
		 
		 
		 
		if(!empty($indata))
		{		
			 if(date('Y-m-d', strtotime($indata['DueDate'])) < $due_date){
				$update_data = array('DueDate'=>$due_date, 'ScheduledDate'=>$due_date);
				$this->general_model->update_row_data('qb_test_invoice',$condition, $update_data);
				
				 $user = $indata['company_qb_username'];
			
				  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
				  redirect('home/index','refresh');  
			 }else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select greater date...</div>');  
			 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid Invoice.</strong></div>');  
		 }	
          redirect('home/index','refresh');		 
	 } 

	


	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 




	 public function check_vault(){
		 
		 
		  $card=''; $card_name=''; $customerdata=array();
		 if($this->czsecurity->xssCleanPostInput('customerID')!=""){
			 
				
				$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');  
			

			 	$condition     =  array('ListID'=>$customerID); 
			    $customerdata = $this->general_model->get_row_data('qb_test_customer',$condition);
               
				if(!empty($customerdata)){
	                 				
   				   
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_card_expiry_data($customerID);
					$customerdata['card']  = $card_data;
				 
					echo json_encode($customerdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

	  
	
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
				
	$data['login_info'] 	= $this->session->userdata('customer_logged_in');
			$user_id 				= $data['login_info']['ListID'];
		        $transactions           = $this->customer_model->get_invoice_customer_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-right"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No record available</td></tr>';
				}
              die;				

      }	



  public function get_card_edit_data(){
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_mask_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
 
	public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
        	$title = 'Sale Payments';
        	$nf_desc = 'A payment for '.$customerName.' was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
        	$type = 1;
        }else{
        	$title = 'Invoice Checkout Payments';
        	$in_data =    $this->general_model->get_row_data('qb_test_invoice', array('TxnID'=>$invoiceNumber));
	        if(isset($in_data['RefNumber'])){
	            $invoiceRefNumber = $in_data['RefNumber'];
	        }else{
	            $invoiceRefNumber = $invoiceNumber;
	        }
        	$nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
        	$type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 3,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    } 
	public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('qb_test_invoice', array('TxnID'=>$invoiceNumber));
        if(isset($in_data['RefNumber'])){
            $invoiceRefNumber = $in_data['RefNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 3,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
	
	
}