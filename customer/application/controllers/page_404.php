<?php

class Page_404 extends CI_Controller
{
    	public function __construct()
	{
		parent::__construct();
		
		
	}
	
	
	public function index()
	{	 
	     $this->output->set_status_header('404'); 
	    $data['template'] 		= template_variable();
	    $this->load->view('template/template_start', $data);
	    $this->load->view('customer/page_ready_404');
	    	$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
    	public function msg_paid()
	{	 
	     $this->output->set_status_header('404'); 
	    $data['template'] 		= template_variable();
	    $this->load->view('template/template_start', $data);
	    $this->load->view('customer/paidmessage');
	 
	}
}

?>