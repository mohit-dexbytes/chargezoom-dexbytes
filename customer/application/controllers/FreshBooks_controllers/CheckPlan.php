<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

ob_start();
include_once APPPATH . 'libraries/Manage_payments.php';
include_once APPPATH . 'libraries/Freshbooks_data.php';

class CheckPlan extends CI_Controller
{
    private $gatewayEnvironment;
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('fb_company_model');
        $this->load->model('general_model');
        $this->load->helper('general');

        $this->load->model('card_model');
        $this->db1                = $this->load->database('otherdb', true);
        $this->gatewayEnvironment = $this->config->item('environment');
    }

    public function index()
    {
        redirect('wrong_url');
    }

    public function check_out()
    {
        $data['records'] = '';
        $port_url        = current_url();

        $this->session->unset_userdata("tranID");
        $this->session->unset_userdata("sess_invoice_id");
        $marchent_id = $this->uri->segment(2);
        $plan        = $this->uri->segment(3);
        $merchantID  = base64_decode($marchent_id);
        $merchid     = $merchantID;
        $con         = array('merchantID' => $merchantID);
        $Number      = '';

        $port_url = current_url();

        $new_url   = explode('://', $port_url);
        $logo_data = explode('.', $new_url[1]);

        $l_con = array('portalprefix' => $logo_data[0], 'merchantID' => $merchantID);
        $rdata = $this->general_model->get_row_data('tbl_config_setting', $l_con);

        $env = getenv('ENV');
        if (empty($rdata) && $env != 'local') {
          // redirect('wrong_url');
        }

        $m_data         = $this->general_model->get_select_data('tbl_merchant_data', array('resellerID', 'companyName', 'weburl'), array('merchID' => $merchantID));
        $data['m_data'] = $m_data;
        $resellerID     = $m_data['resellerID'];

        $condition_r = array('resellerID' => $resellerID);

        $r_data = $this->general_model->get_select_data('tbl_reseller', array('resellerCompanyName'), $condition_r);

        $data['r_data']       = $r_data;
        $data['prorata_data'] = 1;
        $result               = $this->general_model->get_row_data('app_integration_setting', $con);
        $con2                 = array('merchantDataID' => $merchantID, 'postPlanURL' => $plan);
        $splan                = $this->general_model->get_row_json_data('tbl_subscriptions_plan_fb', $con2);
        $prorata_heading      = '';
        $currentProDate       = date('m/d/Y');

        $item = $products = $this->fb_company_model->fb_subscription_items($splan->planID, $merchantID);
        $data['items']   = $item;
        $proRateday      = 0;
        $nextInvoiceDate = 0;
        $proRate         = 0;

        if (trim($splan->invoiceFrequency) == 'mon') {

            $onetime = $recurring = 0;

            foreach ($item as $itm) {
                if ($itm['oneTimeCharge'] == 0) {
                  $recurring += $itm['itemQuantity'] * $itm['itemRate'];
                }
                if ($itm['oneTimeCharge'] == 1) {
                    $onetime += $itm['itemQuantity'] * $itm['itemRate'];
                }

            }

            $data['onetime']   = $onetime;
            $data['recurring'] = $recurring;

            if (trim($splan->proRate) == 1) {
                $proRate         = 1;
                $proRateday      = $splan->proRateBillingDay;
                $nextInvoiceDate = $splan->nextMonthInvoiceDate;
                $lstm            = date('m', strtotime('first day of last month'));
                $nm              = date('m', strtotime('first day of next month'));
                $orderdate       = date('Y-m-d');

                if ($proRateday < $nextInvoiceDate) {
                    $cutoffdate = date("Y-$lstm-$nextInvoiceDate");
                } else {
                    $cutoffdate = date("Y-m-$nextInvoiceDate");
                }

                $billing_data = date("Y-m-$proRateday");

                $lastbillingdata = date("Y-$lstm-$proRateday");

                if ((strtotime($orderdate) <= strtotime($billing_data)) && strtotime($orderdate) > strtotime($cutoffdate)) {
                    $ndate1        = new DateTime($orderdate);
                    $ndate2        = new DateTime($billing_data);
                    $diff          = $ndate2->diff($ndate1);
                    $remainingdays = $diff->format("%a") + 1;

                    $ndatelast = new DateTime($lastbillingdata);
                    // $ndate2     = new DateTime($billing_data);
                    $diff      = $ndate2->diff($ndatelast);
                    $monthdays = $diff->format("%a");

                    $subAmount                 = ($recurring / $monthdays) * $remainingdays + $recurring;
                    $data['prorata_data']      = (1 / $monthdays) * $remainingdays;
                    $splan->subscriptionAmount = $subAmount + $onetime;
                    $proHeaderDate             = date('m/d/Y', strtotime($lastbillingdata . ' -1 day'));
                    $prorata_heading           = "($currentProDate - $proHeaderDate)";
                } else {
                    $Y = date('Y', strtotime('first day of next month'));

                    $next      = date("$Y-$nm-$proRateday");
                    $next1     = new DateTime($next);
                    $ndate2    = new DateTime($billing_data);
                    $diff      = $ndate2->diff($next1);
                    $monthdays = $diff->format("%a");

                    $date1 = new DateTime($orderdate);
                    $date2 = new DateTime($next);
                    $diff  = $date1->diff($date2);

                    $subAmount                 = ($diff->d) * ($recurring / $monthdays);
                    $data['prorata_data']      = ($diff->d) * (1 / $monthdays);
                    $splan->subscriptionAmount = $subAmount + $onetime;
                    $proHeaderDate             = date('m/d/Y', strtotime($next . ' -1 day'));
                    $prorata_heading           = "($currentProDate - $proHeaderDate)";
                }

            } else {
                $splan->subscriptionAmount = $recurring + $onetime;
                $proRateday                = $splan->proRateBillingDay;
                $nextInvoiceDate           = $splan->nextMonthInvoiceDate;
            }
            $data['recurring'] = $recurring;
            $data['onetime']   = $onetime;
            if ($splan->freeTrial != 0) {
                $data['recurring']         = $recurring;
                $data['recurring_onetime'] = 0.00;
                $splan->subscriptionAmount = $onetime;
            }

        } else {

            $onetime = $recurring = 0;
            foreach ($item as $itm) {
                if ($itm['oneTimeCharge'] == 0) {
                    $recurring += $itm['itemQuantity'] * $itm['itemRate'];

                }
                if ($itm['oneTimeCharge'] == 1) {
                    $onetime += $itm['itemQuantity'] * $itm['itemRate'];
                }

            }

            $data['onetime'] = $onetime;
            if ($splan->freeTrial == 0) {
                $data['recurring']         = $recurring;
                $splan->subscriptionAmount = $onetime + $recurring;
            } else {
                $data['recurring'] = $recurring;
                $data['recurring'] = 0.00;

            }

        }

        $data['prorata_heading'] = $prorata_heading;
        $data['selected_plan']   = $splan;
        $con_gt                  = array('gatewayID' => $splan->paymentGateway);
        $con1                    = array('gatewayID' => $splan->paymentGateway);
        $chk_gateway             = $this->general_model->get_row_json_data('tbl_merchant_gateway', $con_gt);

        if (!empty($data['selected_plan']->confirm_page_url)) {
            $thank_url = $data['selected_plan']->confirm_page_url;
        } else {
            $thank_url = 'Thankyou';
        }

        if (!empty($this->input->post())) {
            switch ($result['appIntegration']) {
                case "3":
                    $val = array(
                      'merchantID' => $merchantID,
                    );
                    $freshbooksToken     = $this->general_model->get_row_data('tbl_freshbooks', $val);
                    if (empty($freshbooksToken)) {
                      $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>FreshBooks Account Integration Error</strong></div>');
                      redirect($thank_url, 'refresh');
                    }

                    $this->subdomain     = $freshbooksToken['sub_domain'];
                    $key                = $freshbooksToken['secretKey'];
                    $this->accessToken   = $freshbooksToken['accessToken'];
                    $this->access_token_secret = $freshbooksToken['oauth_token_secret'];
                    $this->fb_account    = $freshbooksToken['accountType'];
                    $condition1 = array('resellerID' => $this->resellerID);
                    $sub1 = $this->general_model->get_row_data('Config_merchant_portal', $condition1);
                    $domain1 = $sub1['merchantPortalURL'];
                    $URL1 = $domain1 . '/FreshBooks_controllers/Freshbooks_integration/auth';
                
                    define('OAUTH_CONSUMER_KEY', $this->subdomain);
                    define('OAUTH_CONSUMER_SECRET', $key);
                    define('OAUTH_CALLBACK', $URL1);

                    if (!empty($this->czsecurity->xssCleanPostInput('first_name'))) {

                        $this->load->library('form_validation');

                        $this->form_validation->set_rules('email', 'Email', 'required');
                        $this->form_validation->set_rules('first_name', 'First Name', 'required');
                        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
                        $string = base64_encode(mt_rand(1111111, 9999999));
                        if ($this->form_validation->run() == true) {
                            $scheduleID = $this->czsecurity->xssCleanPostInput('scheduleID');
                            session_start();

                            $sessiondata = array(
                                'customer'            => $this->czsecurity->xssCleanPostInput('first_name') . ' ' . $this->czsecurity->xssCleanPostInput('last_name'),
                                'email'               => $this->czsecurity->xssCleanPostInput('email'),
                                'username'            => $this->czsecurity->xssCleanPostInput('email'),
                                'password'            => $string,
                                'fname'               => $this->czsecurity->xssCleanPostInput('first_name'),
                                'lname'               => $this->czsecurity->xssCleanPostInput('last_name'),
                                'company'             => $this->czsecurity->xssCleanPostInput('company'),

                                'country'             => $this->czsecurity->xssCleanPostInput('country'),
                                'zip'                 => $this->czsecurity->xssCleanPostInput('zip'),
                                'add1'                => $this->czsecurity->xssCleanPostInput('address'),
                                'add2'                => $this->czsecurity->xssCleanPostInput('address2'),
                                'city'                => $this->czsecurity->xssCleanPostInput('city'),
                                'state'               => $this->czsecurity->xssCleanPostInput('state'),

                                'scountry'            => $this->czsecurity->xssCleanPostInput('scountry'),
                                'szip'                => $this->czsecurity->xssCleanPostInput('szip'),
                                'sadd1'               => $this->czsecurity->xssCleanPostInput('saddress'),
                                'sadd2'               => $this->czsecurity->xssCleanPostInput('saddress2'),
                                'scity'               => $this->czsecurity->xssCleanPostInput('scity'),
                                'sstate'              => $this->czsecurity->xssCleanPostInput('sstate'),
                                'mobile'              => ($this->czsecurity->xssCleanPostInput('mobile')) ? $this->czsecurity->xssCleanPostInput('mobile') : '',
                                'key'                 => $this->czsecurity->xssCleanPostInput('key'),
                                'token'               => $this->czsecurity->xssCleanPostInput('stripeToken'),
                                'card'                => $this->czsecurity->xssCleanPostInput('cardNumber'),
                                'cardfriendlyname'    => $this->czsecurity->xssCleanPostInput('cardType') . '-' . substr($this->czsecurity->xssCleanPostInput('cardNumber'), -4),
                                'ccv'                 => $this->czsecurity->xssCleanPostInput('cardCVC'),
                                'exp1'                => $this->czsecurity->xssCleanPostInput('cardExpiry'),
                                'exp2'                => $this->czsecurity->xssCleanPostInput('cardExpiry2'),

                                'accountNumber'       => $this->czsecurity->xssCleanPostInput('accountNumber'),
                                'routeNumber'         => $this->czsecurity->xssCleanPostInput('routeNumber'),
                                'accountName'         => 'Checking - ' . substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'accountfriendlyname' => 'Checking - ' . substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'secCodeEntryMethod'  => 'WEB',
                                'accountType'         => 'checking',
                                'accountHolderType'   => 'business',

                                'accept'              => $this->czsecurity->xssCleanPostInput('accept'),
                                'marchantid'          => $merchantID,
                                'scheduleID'          => $scheduleID,

                            );

                            $freshbookSync = new Freshbooks_data($merchantID, $resellerID);
                            $syncObject= new FreshbooksNew($merchantID, OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $this->subdomain, $this->accessToken, $this->access_token_secret);

                            $custID    = false;
                            $check     = array('fullName' => $sessiondata['customer'], 'merchantID' => $merchantID);
                            $checkuser = $this->general_model->get_row_data('Freshbooks_custom_customer', $check);
                            if (empty($checkuser)) {
                                $customerData = [
                                    "organization"      => $sessiondata['company'],
                                    "username"      => $sessiondata['customer'],
                                    "fname"     => $sessiondata['fname'],
                                    "lname"      => $sessiondata['lname'],
                                    "email"     => $sessiondata['email'],

                                    "p_street"      => $sessiondata['add1'],
                                    "p_street2"      => $sessiondata['add2'],
                                    "p_code"       => $sessiondata['zip'],
                                    "p_country"       => $sessiondata['country'],
                                    "p_province"         => $sessiondata['state'],
                                    "p_city"          => $sessiondata['city'],

                                    "s_street" => $sessiondata['sadd1'],
                                    "s_street2" => $sessiondata['sadd2'],
                                    "s_code"  => $sessiondata['szip'],
                                    "s_country"  => $sessiondata['scountry'],
                                    "s_province"    => $sessiondata['sstate'],
                                    "s_city"     => $sessiondata['scity'],
                                ];

                                $client_id        = '';
                                $customerData['userid'] = $client_id;
                                $custID = $syncObject->add_customer($customerData);
                                if (!is_numeric($clients)) {
                                  $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Unable to create customer </strong></div>');
                                  redirect(current_url(), 'refresh');
                                }

                                $customer_login = array(
                                    'customerEmail'    => $sessiondata['email'],
                                    'customerPassword' => md5($string),
                                    'createdAt'        => date('Y-m-d H:i:s'),
                                    'is_logged_in'     => '0',
                                    'loginCode'        => 'xyz',
                                    'isEnable'         => '0',
                                    'customerUsername' => $sessiondata['username'],
                                    'merchantID'       => $sessiondata['marchantid'],
                                    'customerID'       => $custID,
                                );
                                //inser here customer login table
                                $this->general_model->insert_row('tbl_customer_login', $customer_login);
                            } else {
                                $custID = $checkuser['Customer_ListID'];
                            }

                            $prorata_data = $this->czsecurity->xssCleanPostInput('prorata_data');
                            if (!$prorata_data) {
                                $prorata_data = 1;
                            }

                            if ($scheduleID == 2) {
                                $sessiondata['CardType'] = 'Echeck';
                                $creditStatus            = 0;
                                $achType                 = 2;
                                $accountNumber           = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountNumber'));
                                $accountfriendlyname     = 'Checking - ' . substr($accountNumber, -4);
                                $accountName             = $accountfriendlyname;

                                $routeNumber      = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('routeNumber'));
                                $acct_holder_type = 'business';
                                $acct_type        = 'checking';
                                $sec_code         = 'WEB';

                            } else {
                                $sessiondata['CardType'] = $this->czsecurity->xssCleanPostInput('cardType');
                                $creditStatus            = 1;
                                $achType                 = 1;
                            }
                            $this->session->set_userdata('records', $sessiondata);
                            $cust_sess = $this->session->userdata('records');
                            $amount    = $this->czsecurity->xssCleanPostInput('amount');
                            $amount    = round($amount, 2);
                            $code_data = '';

                            $tr_type  = '';
                            $card_no  = $this->czsecurity->xssCleanPostInput('cardNumber');
                            $exyear   = $this->czsecurity->xssCleanPostInput('cardExpiry2');
                            $expmonth = $this->czsecurity->xssCleanPostInput('cardExpiry');
                            $cvv      = $this->czsecurity->xssCleanPostInput('cardCVC');
                            $cardType = $this->general_model->getType($card_no);

                            $accountDetails = [
                                'cardMonth'           => $expmonth,
                                'cardYear'            => $exyear,
                                'CardType'            => $cardType,
                                'CustomerCard'        => $card_no,
                                'CardCVV'             => $cvv,
                                'CardNo'              => $card_no,

                                'accountNumber'       => $this->czsecurity->xssCleanPostInput('accountNumber'),
                                'routeNumber'         => $this->czsecurity->xssCleanPostInput('routeNumber'),
                                'accountName'         => 'Checking - ' . substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'accountfriendlyname' => 'Checking - ' . substr($this->czsecurity->xssCleanPostInput('accountNumber'), -4),
                                'secCodeEntryMethod'  => 'WEB',
                                'accountType'         => 'checking',
                                'accountHolderType'   => 'business',

                                'Billing_Addr1'       => $this->czsecurity->xssCleanPostInput('address'),
                                'Billing_Addr2'       => $this->czsecurity->xssCleanPostInput('address2'),
                                'Billing_City'        => $this->czsecurity->xssCleanPostInput('city'),
                                'Billing_Country'     => $this->czsecurity->xssCleanPostInput('country'),
                                'Billing_Contact'     => '',
                                'Billing_State'       => $this->czsecurity->xssCleanPostInput('state'),
                                'Billing_Zipcode'     => $this->czsecurity->xssCleanPostInput('zip'),
                                'customerListID'      => $custID,
                                'merchantID'          => $merchantID,
                                'createdAt'           => date("Y-m-d H:i:s"),
                            ];

                            $paymentObj = new Manage_payments($merchantID);
                            $saleData   = [
                                'paymentDetails'    => $accountDetails,
                                'transactionByUser' => [],
                                'ach_type'          => $achType,
                                'invoiceID'         => '',
                                'crtxnID'           => '',
                                'companyName'       => $sessiondata['company'],
                                'fullName'          => $sessiondata['customer'],
                                'firstName'         => $sessiondata['fname'],
                                'lastName'          => $sessiondata['lname'],
                                'contact'           => '',
                                'email'             => $sessiondata['email'],
                                'amount'            => $amount,
                                'gatewayID'         => $splan->paymentGateway,
                                'storeResult'       => false,
                                'returnResult'      => true,
                                'customerListID'    => $custID,
                            ];

                            $paidResult = $paymentObj->_processSaleTransaction($saleData);
                            $crtxnID    = '';
                            if (isset($paidResult['response']) && $paidResult['response']) {
                                $result = $paidResult['result'];
                                
                                if ($custID && !empty($custID)) {

                                    $card_data = array(
                                        'customerListID'  => $custID,
                                        'Billing_Addr1'   => $cust_sess['add1'],
                                        'Billing_Addr2'   => $cust_sess['add2'],
                                        'Billing_City'    => $cust_sess['city'],
                                        'Billing_Country' => $cust_sess['country'],
                                        'Billing_Contact' => $cust_sess['mobile'],
                                        'Billing_State'   => $cust_sess['state'],
                                        'Billing_Zipcode' => $cust_sess['zip'],
                                        'merchantID'      => $merchantID,
                                        //'companyID'                 => $companyID,
                                        'createdAt'       => date("Y-m-d H:i:s"),
                                    );
                                    if ($cust_sess['scheduleID'] == 1) {
                                        $card_type                             = $this->general_model->getType($cust_sess['card']);
                                        $card_data['cardMonth']                = $cust_sess['exp1'];
                                        $card_data['cardYear']                 = $cust_sess['exp2'];
                                        $card_data['CardType']                 = $card_type;
                                        $card_data['CustomerCard']             = $this->card_model->encrypt($cust_sess['card']);
                                        $card_data['CardCVV']                  = $this->card_model->encrypt($cust_sess['ccv']);
                                        $card_data['customerCardfriendlyName'] = $cust_sess['cardfriendlyname'];
                                    } else {
                                        $card_data['accountNumber']            = $cust_sess['accountNumber'];
                                        $card_data['routeNumber']              = $cust_sess['routeNumber'];
                                        $card_data['accountName']              = $cust_sess['accountName'];
                                        $card_data['customerCardfriendlyName'] = $cust_sess['accountfriendlyname'];
                                        $card_data['secCodeEntryMethod']       = $cust_sess['secCodeEntryMethod'];
                                        $card_data['accountType']              = $cust_sess['accountType'];
                                        $card_data['accountHolderType']        = $cust_sess['accountHolderType'];
                                        $card_data['CardType']                 = 'Echeck';
                                    }

                                    $data_tbl_subscriptions = array(
                                        'customerID'           => $custID,
                                        'merchantDataID'       => $merchantID,
                                        'subscriptionName'     => $splan->planName,
                                        'subscriptionPlan'     => $splan->subscriptionPlan,
                                        'freeTrial'            => $splan->freeTrial,
                                        'invoiceFrequency'     => $splan->invoiceFrequency,
                                        'subscriptionAmount'   => $splan->subscriptionAmount,
                                        'totalInvoice'         => $splan->totalInvoice,
                                        'generatedInvoice'     => 1,

                                        'totalAmount'          => $splan->totalAmount,
                                        'paidAmount'           => $splan->paidAmount,
                                        'generatingDate'       => date("d"),
                                        'firstDate'            => date("Y-m-d"),
                                        'nextGeneratingDate'   => date("Y-m-d"),
                                        'startDate'            => date("Y-m-d"),
                                        'endDate'              => date("Y-m-d"),

                                        'paymentGateway'       => $chk_gateway->gatewayID,
                                        'automaticPayment'     => $splan->automaticPayment,
                                        'emailRecurring'       => $splan->emailRecurring,
                                        'usingExistingAddress' => $splan->usingExistingAddress,

                                        'contactNumber'        => $cust_sess['mobile'],
                                        'address1'             => $cust_sess['add1'],
                                        'address2'             => $cust_sess['add2'],
                                        'country'              => $cust_sess['country'],
                                        'state'                => $cust_sess['state'],
                                        'city'                 => $cust_sess['city'],
                                        'zipcode'              => $cust_sess['zip'],

                                        'createdAt'            => date("Y-m-d H:i:s"),
                                        'updatedAt'            => date("Y-m-d H:i:s"),
                                        'taxID'                => '0',
                                        'planID'               => $splan->planID,
                                    );

                                    $this->db->trans_begin();

                                    $crdata = $this->card_model->chk_card_firendly_name($custID, $card_data['customerCardfriendlyName'], $merchantID);

                                    if ($crdata > 0) {
                                        $card_condition = array(
                                            'customerListID'           => $custID,
                                            'customerCardfriendlyName' => $card_data['customerCardfriendlyName'],
                                            'merchantID'               => $merchantID,
                                        );
                                        $card_data['updatedAt'] = date("Y-m-d H:i:s");
                                        $cardID                 = $this->card_model->update_card_data($card_condition, $card_data);
                                    } else {
                                        $cardID = $this->card_model->insert_card_data($card_data);
                                    }

                                    if ($this->db->trans_status() === false) {
                                        $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong></div>');
                                        $this->db->trans_rollback();

                                    } else {
                                        $data_tbl_subscriptions['cardID'] = $cardID;

                                        $subsId       = $this->general_model->insert_row('tbl_subscriptions_fb', $data_tbl_subscriptions);
                                        $customerName = $sessiondata['fname'] . ' ' . $sessiondata['lname'];
                                        $nf           = $this->addNotificationForMerchant(0, $customerName, $custID, $merchantID, $subsId, $splan->planName);
                                        foreach ($products as $key => $singleProduct) {
                                    			$item = [];
                                          $item['type'] = 0;

                                          $insert_row['itemListID']      = $singleProduct['productID'];
                                          $insert_row['itemQuantity'] = $item['qty'] = $singleProduct['itemQuantity'];
                                          $insert_row['itemRate']        = $singleProduct['itemRate'];
                                          $insert_row['itemFullName'] = $item['name'] = $singleProduct['itemFullName'];
                                          $insert_row['itemDescription'] = $item['description'] = $singleProduct['itemDescription'];
                                          $insert_row['oneTimeCharge']   = $singleProduct['oneTimeCharge'];
                                          $insert_row['subscriptionID']  = $subsId;

                                          $item['unit_cost'] = array('amount' => $insert_row['itemRate'], 'code' => 'USD');
                                          $fb_items[] = $item;

                                          $ins                           = $this->general_model->insert_row('tbl_subscription_invoice_item_fb', $insert_row);
                                        }

                                        $inv_data = $this->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $merchantID));
                                        if (!empty($inv_data)) {
                                            $inv_pre = $inv_data['prefix'];
                                            $inv_po  = $inv_data['postfix'] + 1;
                                            $this->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $merchantID), array('postfix' => $inv_po));

                                        } else {
                                            $inv_pre = '';
                                            $inv_po  = 10001;
                                            $this->general_model->insert_row('tbl_merchant_invoices', array('postfix' => $inv_po, 'prefix' => $inv_pre, 'merchantID' => $merchantID));
                                        }
                                        $new_inv_no = $inv_pre . $inv_po;

                                        $date      = $data_tbl_subscriptions['firstDate'];
                                        $paycycle  = $data_tbl_subscriptions['invoiceFrequency'];
                                        $in_num    = 1;
                                        $next_date = $this->general_model->set_next_date($paycycle, $date, $in_num, $proRate, $proRateday);

                                        $inputData['refNumber']      = $new_inv_no;
                                        $inputData['customerID']     = $custID;
                                        $inputData['invoiceDate']    = date("Y-m-d");
                                        $inputData['invoiceDueDate'] = $next_date;

                                        $invoice = array(
                                          'customerid' => $custID, 'create_date' => date('Y-m-d'), 'due_offset_days' => 0, 'invoice_number' => $new_inv_no, 'country' => $cust_sess['country'], 'province' => $cust_sess['state'], 'street' => $cust_sess['add1'], 'address' => $cust_sess['add2'], 'city' => $cust_sess['city'],
                                          'status' => 1, "lines" => $fb_items
                                        );
                                    
                                        $newInvoice = $syncObject->add_invoices($invoice, false);
                                        
                                        $invID      = '';
                                        if ($newInvoice) {
                                            $syncObject->storeInvoiceDB([$newInvoice]);
                                            $invID = $newInvoice->invoiceid;

                                            $cond  = array('subscriptionID' => $subsId);

                                            $subdata                       = array();
                                            $subdata['nextGeneratingDate'] = $next_date;

                                            $this->general_model->update_row_data('tbl_subscriptions_xero', $cond, $subdata);

                                            $subscription_auto_invoices_data = [
                                                'subscriptionID' => $subsId,
                                                'invoiceID'      => $invID,
                                                'app_type'       => 3, // Freshbook
                                            ];

                                            $this->db->insert('tbl_subscription_auto_invoices', $subscription_auto_invoices_data);

                                            $invoicePayment = $freshbookSync->create_invoice_payment($invID, $amount);
                                            if ($invoicePayment['status'] != 'ok') {
                                              $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Payment couldnot synced.</strong></div>');
                                              $crtxnID = '';
                                            } else {
                                              $crtxnID = $invoicePayment['payment_id'];
                                            }
                                        }
                                        $this->db->trans_commit();

                                        /* gET Txn id*/
                                        $gt_type = $chk_gateway->gatewayType;

                                        if ($gt_type == 1 || $gt_type == 3 || $gt_type == 9) {
                                            $transactionID         = $result['transactionid'];
                                            $result['paymentType'] = $cust_sess['scheduleID'];
                                        }
                                        if ($gt_type == 2) {
                                            $result->paymentType = $cust_sess['scheduleID'];
                                            $transactionID       = $result->transaction_id;
                                        }

                                        if ($gt_type == 4) {
                                            $transactionID = '';
                                            if (isset($result['TRANSACTIONID'])) {
                                                $transactionID = $result['TRANSACTIONID'];
                                            }
                                            $result['paymentType'] = $cust_sess['scheduleID'];

                                        }
                                        if ($gt_type == 5) {
                                            $result->paymentType = $cust_sess['scheduleID'];
                                            if ($result->paid == '1' && $result->failure_code == "") {

                                                $transactionID = $result->id;
                                            } else {

                                                $transactionID = '';
                                            }
                                        }

                                        if ($gt_type == 6 || $gt_type == 7 || $gt_type == 8) {
                                            $transactionID         = $result['transactionId'];
                                            $result['paymentType'] = $cust_sess['scheduleID'];
                                        }

                                        if ($gt_type == 10) {
                                            $transactionID         = $result['id'];
                                            $result['paymentType'] = $cust_sess['scheduleID'];
                                        }

                                        if ($gt_type == 11 || $gt_type == 13) {
                                            $result['paymentType'] = $cust_sess['scheduleID'];
                                            $transactionID         = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : '';
                                        }
                                        if ($gt_type == 12) {
                                            $result['responseType'] = 'SaleResponse';
                                            $result['paymentType']  = $cust_sess['scheduleID'];
                                            $transactionID          = (isset($result['SaleResponse']['transactionID']) && !empty($result['SaleResponse']['transactionID'])) ? $result['SaleResponse']['transactionID'] : time() . 'TXNFAILED';
                                        }
                                        if ($gt_type == 15) {
                                            $result['responseType'] = 'SaleResponse';
                                            $result['paymentType']  = $cust_sess['scheduleID'];
                                            $transactionID          = (isset($result['data']) && !empty($result['data'])) ? $result['data']['id'] : '';
                                        }
                                        if ($creditStatus) {
                                            $payType = false;
                                        } else {
                                            $payType = true;
                                        }
                                        $transactionByUser         = [];
                                        $transactionByUser['type'] = 3;
                                        $transactionByUser['id']   = $custID;

                                        $trid = $this->general_model->insert_gateway_transaction_data($result, $tr_type, $chk_gateway->gatewayID, $chk_gateway->gatewayType, $custID, $amount, $cust_sess['marchantid'], $crtxnID, $resellerID, $invID, $payType, $transactionByUser);
                                        
                                        $this->session->set_flashdata('success', '<div class="alert alert-success"><strong> Successfully Paid</strong></div>');
                                        $in_data                              = [];
                                        $in_data['BalanceRemaining']          = $amount;
                                        $in_data['RefNumber']                 = $new_inv_no;
                                        $in_data['FirstName']                 = $cust_sess['first_name'];
                                        $in_data['LastName']                  = $cust_sess['last_name'];
                                        $in_data['FullName']                  = $cust_sess['company'];
                                        $in_data['BillingAddress_Addr1']      = $cust_sess['add1'];
                                        $in_data['ShipAddress_Addr1']         = $cust_sess['sadd1'];
                                        $in_data['BillingAddress_Addr2']      = $cust_sess['add2'];
                                        $in_data['ShipAddress_Addr2']         = $cust_sess['sadd2'];
                                        $in_data['BillingAddress_City']       = $cust_sess['city'];
                                        $in_data['ShipAddress_City']          = $cust_sess['scity'];
                                        $in_data['BillingAddress_State']      = $cust_sess['state'];
                                        $in_data['ShipAddress_State']         = $cust_sess['sstate'];
                                        $in_data['BillingAddress_PostalCode'] = $cust_sess['zip'];
                                        $in_data['ShipAddress_PostalCode']    = $cust_sess['szip'];
                                        $in_data['BillingAddress_Country']    = $cust_sess['country'];
                                        $in_data['ShipAddress_Country']       = $cust_sess['scountry'];
                                        $in_data['merchantID']                = $merchantID;
                                        $this->session->set_userdata("tranID", $transactionID);
                                        $this->session->set_userdata("sess_invoice_id", $in_data);

                                    }

                                    $this->session->set_flashdata('message', '<div class="alert alert-success"><strong> Success</strong></div>');
                                } else {
                                    $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error Customer Not Saved After Payment</strong></div>');
                                    redirect(current_url(), 'refresh');
                                }
                                redirect($thank_url);

                            } else {
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Transaction Declined. Please Try Again.</strong></div>');
                            }
                        } else {
                            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: Validation Error All fields are required.</strong></div>');
                            redirect(current_url(), 'refresh');
                        }
                    }

                    break;

                default:

            }

        }

        $data['gateway']     = array();
        $surchargePercentage = 0;
        if ($splan->automaticPayment == 1) {
            $con1 = array("gatewayID" => $splan->paymentGateway);
        } else {
            $con1 = array("set_as_default" => 1, 'merchantID' => $merchantID);
        }

        $gat = $this->general_model->get_row_data('tbl_merchant_gateway', $con1);
        if ($gat) {
            if ($gat['isSurcharge'] == '1') {
                $surchargePercentage = $gat['surchargePercentage'];
            }

            if ($gat['gatewayType'] == '5') {
                $data['gateway'] = $gat;
            }

        }

        $data['defaultGateway']       = $gat;
        $data['selectedGateway']      = $gat;
        $data['surchargePercentage']  = $surchargePercentage;
        $data['customer_portal_data'] = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchantID));
        $resellerID                   = resellerID($merchantID);
        $data['reseller']             = $this->general_model->get_row_data('tbl_reseller', array('resellerID' => $resellerID));
        $data['template']['title']    = marchentname($merchantID) . ' - ' . $plan . ' - ' . 'Checkout Page';

        $data['primary_nav'] = primary_customer_nav();
        $data['template']    = template_variable();

        $this->load->view('template/template_start', $data);
        $this->load->view('customer/freshbook/checkout', $data);
        $this->load->view('template/customer_footer', $data);
        $this->load->view('template/template_end', $data);

    }

    public function getError($eee)
    {
        $eeee = array();
        foreach ($eee as $error => $no_of_errors) {
            $eeee[] = $error;

            foreach ($no_of_errors as $key => $item) {
                //Optional - error message with each individual error key.
                $eeee[$key] = $item;
            }
        }

        return implode(', ', $eeee);

    }

    public function addNotificationForMerchant($payAmount, $customerName, $customerID, $merchantID, $invoiceNumber = null, $plan = null)
    {
        /*Notification Saved*/

        $payDateTime = date('M d, Y h:i A');
        if ($merchantID) {
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if (isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])) {
                $timezone    = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if ($payDateTime) {
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title   = 'New Subscription';
        $nf_desc = '' . $customerName . ' subscribed to <b>' . $plan . '</b> on ' . $payDateTime . '';
        $type    = 3;

        $notifyObj = array(
            'sender_id'    => $customerID,
            'receiver_id'  => $merchantID,
            'title'        => $title,
            'description'  => $nf_desc,
            'is_read'      => 1,
            'recieverType' => 4,
            'type'         => $type,
            'typeID'       => $invoiceNumber,
            'status'       => 1,
            'created_at'   => date('Y-m-d H:i:s'),
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification', $notifyObj);
        /* Update merchant new notification comes*/
        $con        = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0);
        $update     = $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
}
