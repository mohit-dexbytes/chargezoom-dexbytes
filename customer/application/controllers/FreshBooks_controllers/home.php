<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('fb_customer_model');
		$this->load->model('fb_company_model');
		$this->load->model('fb_invoice_model');
		  $this->db1= $this->load->database('otherdb', TRUE);
		  
		if(!$this->session->userdata('customer_logged_in'))
		{
			redirect('login', 'refresh');
		}
	}
	


	
	public function index()
	{
	   $mID='';
	 	$data['primary_nav']  = primary_customer_nav();
        $data['template'] 		= template_variable();   
      
		$data['login_info']     = $this->session->userdata('customer_logged_in');
	    $cusID                  = $data['login_info']['Customer_ListID']; 
        	$user_id            = $data['login_info']['merchantID'];
	   
       
	
         $data['page_num']      = 'customer_freshbook'; 
        $data['check_details'] 		  = $this->fb_customer_model->customer_by_id($cusID);  
        if($data['check_details'] == ""){
              $data['customer'] 		  = $this->fb_customer_model->customer_details($cusID);  
        }
        else{
            $data['customer']  = $this->fb_customer_model->customer_by_id($cusID);  
        }
        
         $data_invoice	          = $this->fb_customer_model->get_customer_invoice_data_sum($cusID, $user_id);
		$data['invoices_count']    = ($data_invoice->incount)?$data_invoice->incount:'0';
	
	     $data['invoices'] 		  = $this->fb_customer_model->get_invoice_upcomming_data($cusID, $user_id);
         $data['latest_invoice']	  = $this->fb_customer_model->get_invoice_latest_data($cusID, $user_id);
         
         $paydata			=	$this->fb_customer_model->get_customer_invoice_data_payment($cusID, $user_id);	
  
	   $data['pay_invoice']       = ($paydata->applied_amount)?:'0.00';
       $data['pay_upcoming']      = ($paydata->upcoming_balance)?$paydata->upcoming_balance:'0.00';	 
       $data['pay_remaining']     = ($paydata->remaining_amount)?$paydata->remaining_amount :'0.00';	
       
       $data['pay_due_amount']  = ($paydata->applied_due)?$paydata->applied_due :'0.00';
       $mail_con  = array('merchantID'=>$user_id,'customerID'=>$cusID);
     	$data['sum_invoice']  =   ($paydata->applied_amount + $paydata->upcoming_balance+$paydata->remaining_amount);   
	 
	   $data['card_data_array']     = $this->get_card_expiry_data($cusID, $user_id);
	   $condition				  = array('merchantID'=>$user_id);
       $data['gateway_datas']		  = $this->general_model->get_table_data('tbl_merchant_gateway', $condition); 
         $sub  = array('sbs.customerID'=>$cusID,'cust.merchantID'=>$user_id);
	  $data['getsubscriptions']   = $this->fb_customer_model->get_subscriptions_data($sub);
	  
	
			$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
			
	
	    $this->load->view('FB_customer/page_customer_details', $data);
	
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	
	public function get_gateway_data(){
	
		
			$gatewayID  = $this->czsecurity->xssCleanPostInput('gatewayID');
		    $condition     = array('gatewayID'=>$gatewayID);
			 
			 $res  = $this->general_model->get_row_data('tbl_merchant_gateway',$condition);
		
           if(!empty($res)){
          
			$res['status'] ='true';
			  echo json_encode($res);
		   }
			   
	    die;
	
	
	}

	
	public function invoices()
	{
	  
		$data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
		
		
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['Customer_ListID'];
		$mID               = $data['login_info']['merchantID']; 
		$today 				    = date('Y-m-d');

		$condition				  = array('merchantID'=>$mID);
			$condition1 =  array('qb.merchantID'=>$mID, 'cs.merchantID'=>$mID, 'cs.Customer_ListID'=>$user_id, 'qb.CustomerListID'=>$user_id);
	
        
        	$data['invoices'] = $this->fb_invoice_model->get_fb_invoice_data('Freshbooks_test_invoice',$condition);
			$data['gateway_datas']		  = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
            $data['inv_datas']   = $this->fb_invoice_model->get_invoice_details($user_id,$mID);
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
		$this->load->view('FB_customer/page_invoices', $data);
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	public function invoice_details()
	{
	    $invoiceID               =$this->uri->segment(3);  
		$condition1 			= array('item_ListID'=>$invoiceID);
		$data['primary_nav'] 	= primary_customer_nav();
		$data['template'] 		= template_variable();
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['ListID'];
		
		$condition2 			= array('TxnID'=>$invoiceID);
		$invoice_data           = $this->general_model->get_row_data('qb_test_invoice',$condition2);
		
	   
	 
	 
		$condition3 			= array('ListID'=>$invoice_data['Customer_ListID'] );
		$customer_data			= $this->general_model->get_row_data('qb_test_customer',$condition3);	
    
		$data['notes']   		= $this->customer_model->get_customer_note_data($customer_data['ListID']);
		$data['customer_data']  = $customer_data ;
		$data['invoice_data']   = $invoice_data ;
	 
	     $data['invoice_items']   = $this->company_model->get_invoice_item_data( $invoiceID);
	     
	     $data['gateway_datas'] = $this->customer_model->get_merchant_gateway($user_id);
	   
		$this->load->view('template/template_start', $data);
		$this->load->view('template/customer_head', $data);
			
	
	    $this->load->view('customer/page_invoice_details', $data);
	
		$this->load->view('template/customer_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	
	public function invoice_details_print()
	{
		 	 $invoiceID             =  $this->uri->segment(3);  
	  
		$data['template'] 		= template_variable();	
		$data['login_info']	    = $this->session->userdata('customer_logged_in');
		$user_id			    = $data['login_info']['ListID'];
		$condition2 			= array('TxnID'=>$invoiceID);
		$invoice_data           = $this->general_model->get_row_data('qb_test_invoice',$condition2);
		
	
		$condition3 			= array('ListID'=>$invoice_data['Customer_ListID'] );
		
		
		$customer_data			= $this->general_model->get_row_data('qb_test_customer',$condition3);	
		$condition4 			= array('id'=>$customer_data['companyID'] );
		$company_data			= $this->general_model->get_row_data('tbl_company',$condition4);	
		
		 $data['notes']   		  = $this->customer_model->get_customer_note_data($invoice_data['ListID']);
		$data['customer_data']  = $customer_data ;
		$data['company_data']    = $company_data;
		$data['invoice_data']   = $invoice_data ;
	
	    $data['invoice_items']   = $this->company_model->get_invoice_item_data( $invoiceID);
	
	
		
	     	$no = $invoiceID;
			$pdfFilePath = "$no.pdf"; 
			
		
			 ini_set('memory_limit','32M'); 
			
			$html = $this->load->view('customer/page_invoice_details_print', $data, true);
			
			 $this->load->library('pdf');
			 $pdf = $this->pdf->load();
			 $pdf->WriteHTML($html); // write the HTML into the PDF
			$pdf->Output($pdfFilePath, 'D'); // save to file because we can
	
		

	}
	
	
	
	
		
	public function changePass()
    {
		
		
		$session_data = $this->session->userdata('customer_logged_in');
		$id = $session_data['loginID'];
	   $this->czsecurity->xssCleanPostInput('user-settings-password') ; 
		$query = $this->customer_login_model->savenewpass($id, $this->czsecurity->xssCleanPostInput('user-settings-password') );			
		$message = '<div class="alert alert-success"><i class="fa fa-check"></i><strong>Success</strong> Password Successfully Updated</div>' ;

		$this->session->set_flashdata('message', $message);
		redirect('customer/home', 'refresh');
	}
	 
	
	
	
		
	
	  
	  
  public function get_card_expiry_data($customerID, $merchantID){  
  
                       $card = array();
               		   $this->load->library('encrypt');
           
	   	        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'   "; 
				    $query1 = $this->db1->query($sql);
				 
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->encrypt->decode($card_data['CustomerCard']),12) ;
						 $card_data['CardID'] = $card_data['CardID'] ;
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

     }
 
}
