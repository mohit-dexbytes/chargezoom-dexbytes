<?php

/**
 * Example CodeIgniter QuickBooks Web Connector integration
 * 
 * This is a tiny pretend application which throws something into the queue so 
 * that the Web Connector can process it. 
 * 
 * @author Keith Palmer <keith@consolibyte.com>
 * 
 * @package QuickBooks
 * @subpackage Documentation
 */

/**
 * Example CodeIgniter controller for QuickBooks Web Connector integrations
 */
class NMIPayment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		
    	
		$this->load->config('paypal');
		$this->load->config('quickbooks');
		$this->load->config('paytrace');
		$this->load->model('quickbooks');
		 $this->quickbooks->dsn('mysqli://' . $this->db->username . ':' . $this->db->password . '@' . $this->db->hostname . '/' . $this->db->database);

		$this->load->model('general_model');
		$this->load->model('fb_customer_model');
		$this->load->model('fb_company_model');
	    $this->load->model('card_model');
		
		 $this->db1= $this->load->database('otherdb', TRUE);

		if(!$this->session->userdata('customer_logged_in'))
		{
			redirect('login', 'refresh');
		}
	}
	
	
	public function index(){

	}
	
	 
	 	   
	   
	 public function payment_transaction(){

	
		        $data['primary_nav'] 	= primary_customer_nav();
				$data['template'] 		= template_variable();
				$data['login_info'] 	= $this->session->userdata('customer_logged_in');
				
			    $user_id 				= $data['login_info']['Customer_ListID'];
				 $m_id 			    	= $data['login_info']['merchantID'];
		       
		         $transtion             =  $this->fb_customer_model->get_customer_transaction_data($user_id, $m_id);
		        
		        $data['transactions'] = $transtion;
				$this->load->view('template/template_start', $data);
				$this->load->view('template/customer_head', $data);
				$this->load->view('FB_customer/payment_transaction', $data);
				$this->load->view('template/customer_footer',$data);
				$this->load->view('template/template_end', $data);


    }	



	public function customer_card(){
		
	
           $this->load->library('encrypt');
	       $report=array();
		   $data['primary_nav'] 	= primary_customer_nav();
			$data['template'] 		= template_variable();
			$data['login_info'] 	= $this->session->userdata('customer_logged_in');
			$user_id 				= $data['login_info']['Customer_ListID'];
				
		      
		   $card_data =  $this->card_model->get_card_expiry_data($user_id);

		   foreach($card_data as $key=> $card){
		       
		         $condition      =  array('id'=>$card['companyID']);
			 
			     $customer_card['ListID']                    = $card['customerListID'] ;
                 $customer_card['CardNo']                    = substr($this->encrypt->decode($card['CustomerCard']),12) ;
				 $customer_card['expired_date']              = $card['expired_date'] ;
                 $customer_card['customerCardfriendlyName']  = $card['customerCardfriendlyName'] ;
				 $customer_card['createdAt']                 = date('F d, Y', strtotime($card['createdAt'] ));
				 
			     $report[$key] = $customer_card;
		   }
			     $data['report6']	    = 	$report;
			
         
		    
				$this->load->view('template/template_start', $data);
				
				
				$this->load->view('template/customer_head', $data);
				$this->load->view('customer/payment_card', $data);
				$this->load->view('template/customer_footer',$data);
				$this->load->view('template/template_end', $data);			
			
			
			
	}

	


	  
	
	  public function insert_new_data()
      {
	      
	         	$da	= $this->session->userdata('customer_logged_in');
		
			    $merchID 				= $da['merchantID'];
	      
	        
	            $this->load->library('encrypt');
			  
	   	    
			        	$customer = $this->czsecurity->xssCleanPostInput('customerID11');	
			        	
			        	$c_data   = $this->general_model->get_row_data('Freshbooks_custom_customer',array('Customer_ListID'=>$customer));
			        	
			        
			        	$companyID = $c_data['companyID'];
			       
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
						$b_addr1 = $this->czsecurity->xssCleanPostInput('address1');
						$b_addr2 = $this->czsecurity->xssCleanPostInput('address2');
						$b_city = $this->czsecurity->xssCleanPostInput('city');
						$b_state = $this->czsecurity->xssCleanPostInput('state');
						$b_zipcode = $this->czsecurity->xssCleanPostInput('zipcode');
						$b_country = $this->czsecurity->xssCleanPostInput('country');
						$b_contact = $this->czsecurity->xssCleanPostInput('contact');
				      
						$card_type = $this->general_model->getType($card_no);		
					
		           	$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CustomerCard' =>$this->encrypt->encode($card_no),
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										  'CardType'      =>$card_type, 
										 'customerListID' =>$customer, 
										 'merchantID'     =>$merchID,
										 'companyID'      => $companyID, 
										
										    'Billing_Addr1'     =>$b_addr1,
										    'Billing_Addr2'     =>$b_addr2,
										    'Billing_City'      =>$b_city,
										    'Billing_State'     =>$b_state,
										    'Billing_Zipcode'   =>$b_zipcode,
										    'Billing_Country'   =>$b_country,
										    'Billing_Contact'   =>$b_contact,
										 'customerCardfriendlyName'=>$friendlyname,
										 'createdAt' 	=> date("Y-m-d H:i:s") );
										 
			$is_default = 0;
	     	$checkCustomerCard = checkCustomerCard($customer,$merchID);
        	if($checkCustomerCard == 0){
        		$is_default = 1;
        	}
        	$insert_array['is_default'] = $is_default;						
		    	
			$id = $this->db1->insert('customer_card_data', $insert_array);

		 if( $id ){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Successfully Inserted</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
		 }
		 
	    	redirect('FreshBooks_controllers/home/index','refresh');
	  }
	  
	 
	
	 
	 
	 
	public function delete_card_data(){
		
		    
    
			    $cardID =  $this->uri->segment('3'); 
			   
		       $sts =  $this->db1->query("Delete from customer_card_data where CardID = $cardID ");
				 if($sts){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
		 }
		 
	    	redirect('FreshBooks_controllers/home/index','refresh');
	}		
	  
	  public function update_card_data()
      {
        	       	$da	= $this->session->userdata('customer_logged_in');

        			    $merchID 				= $da['merchantID'];
        	      
        	        
	                    $this->load->library('encrypt');
	                    
	                   
			        	$cardID = $this->czsecurity->xssCleanPostInput('edit_cardID');
			        	
						$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('edit_cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
						$b_addr1 = $this->czsecurity->xssCleanPostInput('baddress1');
				        $b_addr2 = $this->czsecurity->xssCleanPostInput('baddress2');
				        $b_city = $this->czsecurity->xssCleanPostInput('bcity');
				        $b_state = $this->czsecurity->xssCleanPostInput('bstate');
				        $b_country = $this->czsecurity->xssCleanPostInput('bcountry');
				        $b_contact = $this->czsecurity->xssCleanPostInput('bcontact');
				        $b_zip = $this->czsecurity->xssCleanPostInput('bzipcode');
				        $merchantID = $merchID;
								
			         $condition = array('CardID'=>$cardID);
		           	$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										 'customerCardfriendlyName'=>$friendlyname,
										    'Billing_Addr1'     =>$b_addr1,
										    'Billing_Addr2'     =>$b_addr2,
										    'Billing_City'      =>$b_city,
										    'Billing_State'     =>$b_state,
										    'Billing_Zipcode'   =>$b_zip,
										    'Billing_Country'   =>$b_country,
										    'Billing_Contact'   =>$b_contact,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
				      if($this->czsecurity->xssCleanPostInput('edit_card_number')!='')
				      {
						$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number'); 						 
				     	$insert_array['CustomerCard'] =$this->encrypt->encode($card_no);
				     	$card_type = $this->general_model->getType($card_no);
				     	$insert_array['CardType'] =$card_type;
				     	
				      }					 
                $this->db1->where($condition);
	         
			 $id = $this->db1->update('customer_card_data',  $insert_array);

		 if( $id ){
		    $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Successfully Updated</strong></div>'); 		 
		 }else{
		 
		   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>'); 
		 }
		 
	    redirect('FreshBooks_controllers/home/index','refresh');
	  }
	  

	 
	 public function get_card_data(){
		$customerdata =array();
		if($this->czsecurity->xssCleanPostInput('cardID')!=""){
			 $crID = $this->czsecurity->xssCleanPostInput('cardID');
			$card_data = $this->card_model->get_single_card_data($crID);
			
			if(!empty($card_data)){
			         $customerdata['status'] =  'success';	
					$customerdata['card']     = $card_data;
					echo json_encode($customerdata)	;
					die;
			}
		}
		
		
	}
	 
	 
public function pay_invoice(){
    
    include APPPATH . 'third_party/nmiDirectPost.class.php';
      include APPPATH . 'third_party/nmiCustomerVault.class.php';
      
	   
     
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
         $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
           $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
		
			$nmiuser   = $gt_result['gatewayUsername'];
			$nmipass   = $gt_result['gatewayPassword'];
			$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);   
		
         
       if($cardID!="" && $gateway!=""){  
         
        
		if(!empty($in_data)){ 
		
            $Customer_ListID = $in_data['Customer_ListID'];
		       $card_data    =   $this->card_model->get_single_card_data($cardID);
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					
					  $amount 			  =	 $in_data['BalanceRemaining']; 
				
					  		$transaction1 = new nmiDirectPost($nmi_data); 
							$transaction1->setCcNumber($card_data['CardNo']);
						    $expmonth =  $card_data['cardMonth'];
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $expry    = $expmonth.$exyear;  
							$transaction1->setCcExp($expry);
							$transaction1->setCvv($card_data['CardCVV']);
							$transaction1->setAmount($amount);
				        
						    $transaction1->sale();
						    // add level III data
		                    $level_request_data = [
		                        'transaction' => $transaction1,
		                        'card_no' => $card_data['CardNo'],
		                        'merchID' => $in_data['merchantID'],
		                        'amount' => $amount,
		                        'invoice_id' => $invoiceID,
		                        'gateway' => 1
		                    ];
		                    $transaction1 = addlevelThreeDataInTransaction($level_request_data);
						     $result = $transaction1->execute();

					
					   if( $result['response_code']=="100"){
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
							 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(- $amount) );
						 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['company_qb_username'];
						 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['responsetext'].'.</div>'); 
					   }  
					   
					   $transaction['transactionID']      = $result['transactionid'];
					   $transaction['transactionStatus']  = $result['responsetext'];
					   $transaction['transactionCode']    =  $result['response_code'];
					    $transaction['transactionType']   =  ($result['type'])?$result['type']:'auto-nmi';
					   $transaction['transactionDate']    = date('Y-m-d h:i:s');  
					    $transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']  = $gt_result['gatewayType'] ;
					   $transaction['invoiceTxnID']       = $in_data['TxnID'];
					   $transaction['customerListID']     = $in_data['Customer_ListID'];
					   $transaction['transactionAmount']  = $in_data['BalanceRemaining'];
				       $id = $this->general_model->insert_row('tbl_customer_tansaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: This is not valid invoice</strong>.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Select Gateway and Card</strong>.</div>'); 
		  }
			
		    redirect('home/index','refresh');

    }   
    
    
    
    public function pay_paypal_invoice(){
        
        
         include APPPATH . 'third_party/PayPalAPINEW.php';
	    
	   
        
    
	      $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
         $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	
    			    $username  = $gt_result['gatewayUsername'];
					$password  = $gt_result['gatewayPassword'];
					$signature = $gt_result['gatewaySignature'];
					
					
				    $config = array(
						'Sandbox' => $this->config->item('Sandbox'), 			// Sandbox / testing mode option.
						'APIUsername' => $username, 	// PayPal API username of the API caller
						'APIPassword' => $password,	// PayPal API password of the API caller
						'APISignature' => $signature, 	// PayPal API signature of the API caller
						'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
						'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
					  );
		
					// Show Errors
					if($config['Sandbox'])
					{
						error_reporting(E_ALL);
						ini_set('display_errors', '1');
					}
					
					$this->load->library('paypal/Paypal_pro', $config);	
         
				   if($cardID!="" || $gateway!=""){  
					 
				   
					if(!empty($in_data)){ 
					  
						$Customer_ListID = $in_data['Customer_ListID'];
						   $card_data    =   $this->card_model->get_single_card_data($cardID);
					 if(!empty($card_data)){
							
				if( $in_data['BalanceRemaining'] > 0){
					        $amount 	    =	 $in_data['BalanceRemaining'];
							
					        $cr_amount      =  $this->quickbooks->get_credit_by_customer($in_data['Customer_ListID']);
							if($cr_amount > $amount){
								
								$new_cr_amount = $cr_amount-$amount;
								$this->general_model->update_row_data('tbl_credits',array('creditName'=>$Customer_ListID), array('creditStatus'=>'1') );
								$inser_data =array('creditName'=>$Customer_ListID,'creditStatus'=>'0', 'merchantID'=>$gt_result['merchantID'], 'creditDescription'=>'Credit Amount', 'creditAmount'=>$new_cr_amount, 'creditDate'=>date('Y-m-d H:i:s') );
								$this->general_model->insert_row('tbl_credits',$inser_data);
								
								 $transaction= array();
							   $transaction['transactionID']       = 'N/A';
							   $transaction['transactionStatus']    = 'CREDIT';
							   $transaction['transactionDate']     = date('Y-m-d h:i:s');  
							   $transaction['transactionCode']     = '';  
								$transaction['transactionType']    = "CREDITPAY";
							   $transaction['customerListID']       = $in_data['Customer_ListID'];
							    $transaction['transactionAmount']   = $amount;
						        $txnID      = $in_data['TxnID'];  
								 $ispaid 	 = 'true';
								 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$amount));
								 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
								 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
								 
								 $user = $in_data['qbwc_username'];
								 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
								$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Successfully Credited.</strong></div>');  
								redirect('home/index','refresh');
								
							}else{
								
							$amount           = $amount-$cr_amount;
						   $creditCardType    = 'Visa';
							$creditCardNumber = $card_data['CardNo'];
						    $expDateMonth     =  $card_data['cardMonth'];
							$expDateYear      = $card_data['cardYear'];
								
								// Month must be padded with leading zero
						$padDateMonth 		= str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
								$cvv2Number =   $card_data['CardCVV'];
							$currencyID     = "USD";
						  

                           $firstName = $in_data['FirstName'];
                            $lastName =  $in_data['LastName']; 
                            $companyName =  $in_data['companyName']; 
							$address1 = $in_data['ShipAddress_Addr1']; 
                            $address2 = $in_data['ShipAddress_Addr2']; 
							$country  = $in_data['ShipAddress_Country']; 
							$city     = $in_data['ShipAddress_City'];
							$state    = $in_data['ShipAddress_State'];		
								$zip  = $in_data['ShipAddress_PostalCode']; 
								$phone = $in_data['Phone']; 
								$email = $in_data['Contact']; 
										
		$DPFields = array(
							'paymentaction' => 'Sale', 	// How you want to obtain payment.  
																	//Authorization indidicates the payment is a basic auth subject to settlement with Auth & Capture.  Sale indicates that this is a final sale for which you are requesting payment.  Default is Sale.
							'ipaddress' => '', 							// Required.  IP address of the payer's browser.
							'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
						);
						
		$CCDetails = array(
							'creditcardtype' => $creditCardType, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
							'acct' => $creditCardNumber, 								// Required.  Credit card number.  No spaces or punctuation.  
							'expdate' => $padDateMonth.$expDateYear, 							// Required.  Credit card expiration date.  Format is MMYYYY
							'cvv2' => $cvv2Number, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
							 'startdate' => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
							'issuenumber' => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
						);
						
		$PayerInfo = array(
							'email' => $email, 								// Email address of payer.
							'payerid' => '', 							// Unique PayPal customer ID for payer.
							'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
							'business' => '' 							// Payer's business name.
						);  
						
		$PayerName = array(
							'salutation' => $companyName, 						// Payer's salutation.  20 char max.
							'firstname' => $firstName, 							// Payer's first name.  25 char max.
							'middlename' => '', 						// Payer's middle name.  25 char max.
							'lastname' => $lastName, 							// Payer's last name.  25 char max.
							'suffix' => ''								// Payer's suffix.  12 char max.
						);
					
		$BillingAddress = array(
								'street' => $address1, 						// Required.  First street address.
								'street2' => $address2, 						// Second street address.
								'city' => $city, 							// Required.  Name of City.
								'state' => $state, 							// Required. Name of State or Province.
								'countrycode' => $country, 					// Required.  Country code.
								'zip' => $zip, 							// Required.  Postal code of payer.
								'phonenum' => $phone 						// Phone Number of payer.  20 char max.
							);
	
							
		                       $PaymentDetails = array(
								'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
								'currencycode' => $currencyID , 					// Required.  Three-letter currency code.  Default is USD.
								'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom' => '', 						// Free-form field for your own use.  256 char max.
								'invnum' => '', 						// Your own invoice or tracking number
								'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

						$PayPalRequestData = array(
										'DPFields' => $DPFields, 
										'CCDetails' => $CCDetails, 
										'PayerInfo' => $PayerInfo, 
										'PayerName' => $PayerName, 
										'BillingAddress' => $BillingAddress, 
										
										'PaymentDetails' => $PaymentDetails, 
										
									);
						
							
				        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
						 
						
					 if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {  
					
					           $code = '111';
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$amount) );
						 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['qbwc_username'];
						 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
						 if($cr_amount > 0){
							  $credit_data = array('creditStatus'=>'1');
							  $this->general_model->update_row_data('tbl_credits',array('creditName'=>$in_data['Customer_ListID']), $credit_data );
						 } 
						
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					    $code = '401';
				     	$this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error</strong>.</div>'); 
				
					   	$this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error</strong>.</div>'); 
					   }  
					   
					 }
						
                       $transaction= array();
                         $tranID ='' ;$amt='0.00';
					    if(isset($PayPalResult['TRANSACTIONID'])) { $tranID = $PayPalResult['TRANSACTIONID'];   $amt=$PayPalResult["AMT"];  }
                       
				       $transaction['transactionID']       = $tranID;
					   $transaction['transactionStatus']    = $PayPalResult["ACK"];
					   $transaction['transactionDate']     = date('Y-m-d h:i:s',strtotime($PayPalResult["TIMESTAMP"]));  
					   $transaction['transactionCode']     = $code;  
					  
						$transaction['transactionType']    = "Paypal_sale";	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					   $transaction['customerListID']      = $in_data['Customer_ListID'];
					   $transaction['transactionAmount']   =$amt ;
					
				       $id = $this->general_model->insert_row('tbl_customer_tansaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: This is not valid invoice</strong>.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Select Gateway and Card</strong>.</div>'); 
		  }
			
			 
		    redirect('home/index','refresh');

    }     



	
	
	public function update_invoice_date(){
		 
		 $invoiceID = $this->czsecurity->xssCleanPostInput('scheduleID');
        $due_date  = date('Y-m-d', strtotime($this->czsecurity->xssCleanPostInput('schedule_date'))); 
		 
		 $condition =  array('TxnID'=>$invoiceID); 
		 $indata    = $this->customer_model->get_invoice_data_byID($invoiceID);
		 
		 
		 
		if(!empty($indata))
		{		
			 if(date('Y-m-d', strtotime($indata['DueDate'])) < $due_date){
				$update_data = array('DueDate'=>$due_date, 'ScheduledDate'=>$due_date);
				$this->general_model->update_row_data('qb_test_invoice',$condition, $update_data);
				
				 $user = $indata['company_qb_username'];
		
				  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
				  redirect('home/index','refresh');  
			 }else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Please select greater date...</div>');  
			 }
		}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid Invoice.</strong></div>');  
		 }	
          redirect('home/index','refresh');		 
	 } 

	
	
		 
public function pay_auth_invoice(){
    
     include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
	   
	
	     
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
         $in_data =    $this->quickbooks->get_invoice_data_pay($invoiceID);
          $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
		 
			$apiLogin         = $gt_result['gatewayUsername'];
			$transactionkey   = $gt_result['gatewayPassword'];
			
         
	 if($cardID!="" &&  $gateway!=""){  
		if(!empty($in_data)){ 
		
			$Customer_ListID = $in_data['Customer_ListID'];
		     $card_data      =   $this->card_model->get_single_card_data($cardID);
			 

		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					
							$amount  =	 $in_data['BalanceRemaining'];
					  		$transaction1 = new AuthorizeNetAIM($apiLogin,$transactionkey); 
							$card_no  = $card_data['CardNo'];
						    $expmonth =  $card_data['cardMonth'];
							$exyear   = $card_data['cardYear'];
							$exyear   = substr($exyear,2);
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $expry    = $expmonth.$exyear;  
							

						     $result = $transaction1->authorizeAndCapture($amount,$card_no,$expry);
							 

					
					   if( $result->response_code=="1"){
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$result->amount));
						 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['company_qb_username'];
						 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					   
					   	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: '.$result->response_reason_text.'</strong>.</div>'); 
					   }  
					   
					   $transactiondata= array();
				       $transactiondata['transactionID']       = $result->transaction_id;
					   $transactiondata['transactionStatus']    = $result->response_reason_text;
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result->response_code;  
					   $transactiondata['transactionCard']     = substr($result->account_number,4);  
					    $transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = $result->transaction_type;	   
					   $transactiondata['customerListID']      = $in_data['Customer_ListID'];
					   $transactiondata['transactionAmount']   = $result->amount;
					   $transactiondata['invoiceTxnID']       = $in_data['TxnID'];
					   $id = $this->general_model->insert_row('tbl_customer_tansaction',   $transactiondata);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Not valid</strong>.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: This is not valid invoice</strong>.</div>'); 
			 }
	             }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Select Gateway and Card</strong>.</div>'); 
		  }		 
			 
		    redirect('home/index','refresh');

    }     



      

  
	public function pay_trace_invoice(){
	    
	    
	    	include APPPATH . 'third_party/PayTraceAPINEW.php';
		   
	
    
	 
	 	 $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
         $in_data              =    $this->quickbooks->get_invoice_data_pay($invoiceID);
           $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			   
		 
			$payusername   = $gt_result['gatewayUsername'];
			$paypassword   = $gt_result['gatewayPassword'];
			$integratorId = $gt_result['gatewaySignature'];

			$grant_type    = "password";
	 if($cardID!="" &&  $gateway!=""){  
		if(!empty($in_data)){ 
		
			$Customer_ListID = $in_data['Customer_ListID'];
		     $card_data      =   $this->card_model->get_single_card_data($cardID);
			 

		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					
				$amount  =	 $in_data['BalanceRemaining'];	
				
					$card_no  =  trim($card_data['CardNo']);
						    $expmonth =  trim($card_data['cardMonth']);
							$exyear   = trim($card_data['cardYear']);
							$cvv	  = trim($card_data['CardCVV']);  
							if($cvv!='999'){
								$cvv ='999';
							}	
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							} 
						$payAPI  = new PayTraceAPINEW();	
					 
					$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);

						//call a function of Utilities.php to verify if there is any error with OAuth token. 
						$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

						//If IsFoundOAuthTokenError results True, means no error 
						//next is to move forward for the actual request 

		if(!$oauth_moveforward){ 
		
		
		         $json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
			
				//set Authentication value based on the successful oAuth response.
				//Add a space between 'Bearer' and access _token 
				$oauth_token = sprintf("Bearer %s",$json['access_token']);
		
				$name = $in_data['FullName'];
				$address = $in_data['ShipAddress_Addr2'];
				$city = $in_data['ShipAddress_City'];
				$state = $in_data['ShipAddress_State'];
				$zipcode = ($in_data['ShipAddress_PostalCode'])?$in_data['ShipAddress_PostalCode']:'85284';
				
			
				
				$request_data = array(
                    "amount" => $amount,
                    "credit_card"=> array (
                         "number"=> $card_no,
                         "expiration_month"=>$expmonth,
                         "expiration_year"=>$exyear ),
                    "csc"=> $cvv,
                    "billing_address"=> array(
                        "name"=>$name,
                        "street_address"=> $address,
                        "city"=> $city,
                        "state"=> $state,
                        "zip"=> $zipcode
						)
					);   
					

				     $request_data = json_encode($request_data);
			           $result    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
				       $response  = $payAPI->jsonDecode($result['temp_json_response']); 
			
	             
				   if ( $result['http_status_code']=='200' ){

				   		// add level three data in transaction
	                    if($response['success']){
	                        $level_three_data = [
	                            'card_no' => $card_no,
	                            'merchID' => $in_data['merchantID'],
	                            'amount' => $amount,
	                            'token' => $oauth_token,
	                            'integrator_id' => $integratorId,
	                            'transaction_id' => $response['transaction_id'],
	                            'invoice_id' => '',
	                            'gateway' => 3,
	                        ];
	                        addlevelThreeDataInTransaction($level_three_data);
	                    }
	                    
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$amount));
						 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						 
						 $user = $in_data['qbwc_username'];
						 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					   
							if(!empty($response['errors'])){ $err_msg = $this->getError($response['errors']);}else{ $err_msg = $response['approval_message'];}
				   
							$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong>'.$err_msg.'</div>'); 
					   }  
					   
					   $transactiondata= array();
					   if(isset($response['transaction_id'])){
				       $transactiondata['transactionID']       = $response['transaction_id'];
					   }else{
						    $transactiondata['transactionID']  = '';
					   }
					   $transactiondata['transactionStatus']    = $response['status_message'];
					   $transactiondata['transactionDate']     = date('Y-m-d h:i:s');  
					   $transactiondata['transactionCode']     = $result['http_status_code'];
					   $transactiondata['transactionCard']     = substr($response['masked_card_number'],12);  
					    $transactiondata['gatewayID']          = $gateway;
                       $transactiondata['transactionGateway']  = $gt_result['gatewayType'] ;
						$transactiondata['transactionType']    = 'pay_sale';	   
					   $transactiondata['customerListID']      = $Customer_ListID;
					   $transactiondata['transactionAmount']   = $amount;
				       $id = $this->general_model->insert_row('tbl_customer_tansaction',   $transactiondata);
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Authentication not valid </strong>.</div>'); 
				 }			
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Not valid</strong>.</div>'); 
				}			
                  }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Customer has no card</strong>.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: This is not valid invoice</strong>.</div>'); 
			 }
	             }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: Select Gateway and Card</strong>.</div>'); 
		  }		 
			 
		    redirect('home/index','refresh');

    }     
    
    
	
 public function pay_stripe_invoice(){
     
     	require_once APPPATH."third_party/stripe/init.php";	
		require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
    
     
	   $invoiceID            = $this->czsecurity->xssCleanPostInput('invoiceProcessID');
		 $cardID               = $this->czsecurity->xssCleanPostInput('CardID');
		 $gateway			   = $this->czsecurity->xssCleanPostInput('gateway');		
		 
         $in_data   =    $this->quickbooks->get_invoice_data_pay($invoiceID);
         $gt_result = $this->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID'=>$gateway));
			  
		
             	$nmiuser   = $gt_result['gatewayUsername'];
    		    $nmipass   = $gt_result['gatewayPassword'];
    		  
       if($cardID!="" || $gateway!=""){  
         
       
		if(!empty($in_data)){ 
		  
            $Customer_ListID = $in_data['Customer_ListID'];
		    $card_data    =   $this->card_model->get_single_card_data($cardID);
		 if(!empty($card_data)){
				
				if( $in_data['BalanceRemaining'] > 0){
					     $cr_amount = 0;
					     $amount    =	 $in_data['BalanceRemaining']; 
					    $cr_amount      =  $this->quickbooks->get_credit_by_customer($in_data['Customer_ListID']);
					    
			
					  	
					  if($cr_amount > $amount){
								
								$new_cr_amount = $cr_amount-$amount;
								$this->general_model->update_row_data('tbl_credits',array('creditName'=>$Customer_ListID), array('creditStatus'=>'1') );
								$inser_data =array('creditName'=>$Customer_ListID,'creditStatus'=>'0',
								'merchantID'=>$gt_result['merchantID'],
								'creditDescription'=>'Credit Amount',
								'creditAmount'=>$new_cr_amount, 
								'creditDate'=>date('Y-m-d H:i:s') );
								
								$this->general_model->insert_row('tbl_credits', $inser_data);
								
								$transaction= array();
							   $transaction['transactionID']       = 'N/A';
							   $transaction['transactionStatus']    = 'CREDIT';
							   $transaction['transactionDate']     = date('Y-m-d h:i:s');  
							   $transaction['transactionCode']     = '';  
								$transaction['transactionType']    = "CREDITPAY";
							   $transaction['customerListID']       = $in_data['Customer_ListID'];
							    $transaction['transactionAmount']   = $amount;
						        $txnID      = $in_data['TxnID'];  
								 $ispaid 	 = 'true';
								 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$amount/100) );
								 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
								 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
								 
								 $user = $in_data['qbwc_username'];
								 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);
								$this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Successfully Credited.</strong></div>');  
								redirect('home/customer','refresh'); 
								
							}else{
					       $amount    = $amount-$cr_amount;
					        
							$amount =  (int)($amount*100);
				
					
							\Stripe\Stripe::setApiKey($gt_result['gatewayPassword']);
							$token  =   $this->czsecurity->xssCleanPostInput('stripeToken');
							$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge Using Stripe Gateway",
								 
								));	
					   
						   $charge= json_encode($charge);
						   $result = json_decode($charge);
						   
						    $trID='';
							
						  
				 if($result->paid=='1' && $result->failure_code==""){
						  $code		 =  '200';
						  $trID 	 = $result->id;
						 $txnID      = $in_data['TxnID'];  
						 $ispaid 	 = 'true';
						 $data   	 = array('IsPaid'=>$ispaid, 'AppliedAmount'=>(-$amount/100) );
						 $condition  = array('TxnID'=>$in_data['TxnID'], 'EditSequence'=>$in_data['EditSequence'] );	
						 $this->general_model->update_row_data('qb_test_invoice',$condition, $data);
						  
						  $user = $in_data['qbwc_username'];
						 $this->quickbooks->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $txnID, QB_PRIORITY_INVOICE,'', $user);  
						 if($cr_amount > 0){
							  $credit_data = array('creditStatus'=>'1');
							  $this->general_model->update_row_data('tbl_credits',array('creditName'=>$in_data['Customer_ListID']), $credit_data );
						 } 
						 
						  $this->session->set_flashdata('message','<div class="alert alert-success"><i class="fa fa-check"></i><strong> Success</strong></div>');  
					   } else{
					       $code =  $result->failure_code;
					       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: '.$result->status.'</strong>.</div>'); 
					   	
					   }  
					 }   
					   $transaction['transactionID']      = $trID;
					   $transaction['transactionStatus']    = $result->status;
					   $transaction['transactionDate']     = date('Y-m-d h:i:s');  
					   $transaction['transactionCode']         = $code;  
					  
						$transaction['transactionType']    = 'stripe_sale';	
						$transaction['gatewayID']          = $gateway;
                       $transaction['transactionGateway']    = $gt_result['gatewayType'] ;					
					    $transaction['customerListID']     = $in_data['Customer_ListID'];
					   $transaction['transactionAmount']   = ($result->amount/100);
					  
				       $id = $this->general_model->insert_row('tbl_customer_tansaction',   $transaction);
					   
				}else{
					  $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Not valid.</div>'); 
				}
          
		     }else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Customer has no card.</div>'); 
			 }
		
		 
	    	}else{
	            $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> This is not valid invoice.</div>'); 
			 }
			 
          }else{
			   $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Select Gateway and Card.</div>'); 
		  }
			
			 
		    redirect('home/index','refresh');
           
		 
	   } 	
	
	
	 public function getError($eee){ 
	      $eeee=array();
	   foreach($eee as $error =>$no_of_errors )
        {
         $eeee[]=$error;
        
        foreach($no_of_errors as $key=> $item)
        {
           //Optional - error message with each individual error key.
            $eeee[$key]= $item ; 
        } 
       } 
	   
	   return implode(', ',$eeee);
	  
	 }
	 




	 public function check_vault(){
		 
		 
		  $card=''; $card_name=''; $customerdata=array();
		 if($this->czsecurity->xssCleanPostInput('customerID')!=""){
			 
				
				$customerID 	= $this->czsecurity->xssCleanPostInput('customerID');  
			

			 	$condition     =  array('ListID'=>$customerID); 
			    $customerdata = $this->general_model->get_row_data('qb_test_customer',$condition);
               
				if(!empty($customerdata)){
	                 				
   				    
				   	 $customerdata['status'] =  'success';	     
					
					 $card_data =   $this->card_model->get_card_expiry_data($customerID);
					$customerdata['card']  = $card_data;
				 
					echo json_encode($customerdata)	;
					die;
			    } 	 
			 
	      }		 
		 
	 }		 

	  
	
	 public function view_transaction(){

				
				$invoiceID = $this->czsecurity->xssCleanPostInput('invoiceID');
				
	$data['login_info'] 	= $this->session->userdata('customer_logged_in');
			$user_id 				= $data['login_info']['ListID'];
		        $transactions           = $this->customer_model->get_invoice_customer_transaction_data($invoiceID, $user_id);
				
				if(!empty($transactions) )
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-right"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					<td class="hidden-xs text-right"><?php echo number_format($transaction['transactionAmount'],2); ?></td>
					<td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
<td class="text-right visible-lg"><?php if($transaction['transactionCode']=='300'){ ?> <span class="btn btn-alt1 btn-danger">Failed</span> <?php }else if($transaction['transactionCode']=='100'){ ?> <span class="btn btn-alt1 btn-success">Success</span><?php } ?></td>
					
				</tr>
				
		<?php     }

				}else{
					echo '<tr><td colspan="5" class="text-center">No record available</td></tr>';
				}
              die;				

      }	



  public function get_card_edit_data(){
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
  }
  
	
}