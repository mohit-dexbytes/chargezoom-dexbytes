<?php

ob_start();
use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\PaymentMethods\ECheck;
use GlobalPayments\Api\ServicesConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;

class Update_payment extends CI_Controller
{
    private $resellerID;
    private $gatewayEnvironment;  
	public function __construct()
	{
		parent::__construct();
		$this->load->config('globalpayments');
		$this->load->config('usaePay');
		$this->load->model('general_model');
		$this->load->model('card_model');
		$this->load->model('fb_company_model');
        $this->load->config('TSYS');
        $this->load->config('EPX');
        $this->gatewayEnvironment = $this->config->item('environment');
		include APPPATH . 'third_party/Freshbooks.php';
        include APPPATH . 'third_party/FreshbooksNew.php';
		
	}
	

	
	public function index(){
	    
	   
	 if(!empty($this->czsecurity->xssCleanPostInput('mid'))) 
	 {
	    $this->session->unset_userdata("tranID");
        $this->session->unset_userdata("sess_invoice_id");
	    $trID = '';   
	    $marchant_id = $this->czsecurity->xssCleanPostInput('mid');
        $invoice_no = $this->czsecurity->xssCleanPostInput('invid');
        $token = $this->czsecurity->xssCleanPostInput('token');
        $rs_daata = $this->general_model->get_select_data('tbl_merchant_data',array('resellerID', 'merchant_default_timezone'), array('merchID'=>$marchant_id,'isDelete'=>0));
		 $resellerID =	$rs_daata['resellerID'];
		 $condition1 = array('resellerID'=>$resellerID);
    	
     	 $sub1    = $this->general_model->get_row_data('Config_merchant_portal',$condition1);
		 $domain1 = $sub1['merchantPortalURL'];
		 $URL1 = $domain1.'/FreshBooks_controllers/Freshbooks_integration/auth';
		 
		 $val = array(
			'merchantID' => $marchant_id,
		);
        $Freshbooks_data      = $this->general_model->get_row_data('tbl_freshbooks', $val);

        $subdomain     = $Freshbooks_data['sub_domain'];
         $fb_account   = $Freshbooks_data['accountType'];
        $key           = $Freshbooks_data['secretKey'];
        $accessToken   = $Freshbooks_data['accessToken'];
        $access_token_secret = $Freshbooks_data['oauth_token_secret'];
        
        define('OAUTH_CONSUMER_KEY', $subdomain);
        define('OAUTH_CONSUMER_SECRET', $key);
        define('OAUTH_CALLBACK', $URL1);
        
        
        $con = array('emailCode'=>$token,'merchantID'=>$marchant_id);
         $savepaymentinfo='';
        $checkCode = $this->general_model->get_num_rows('tbl_template_data', $con);

	    if(true)
	    {
	        $con = array('invoiceID'=>$invoice_no,'merchantID'=>$marchant_id);
    	    $result = $this->general_model->get_row_data('Freshbooks_test_invoice', $con);
    	    
    	    $data['get_invoice'] = $result;
    	    
    	    
    	    $payamount = $this->czsecurity->xssCleanPostInput('payamount');

            $payamount = round($payamount,2);
    	    
    	    $in_data   =    $this->fb_company_model->get_fb_invoice_data_pay($invoice_no, $marchant_id);
    	    
            
    	    //Invoice detail
    	     $thankyou ='FreshBooks_controllers/Thankyou';
    	      $con = array('merchantID'=>$marchant_id,'set_as_default'=>1);
    	      $get_gateway = $this->general_model->get_row_data('tbl_merchant_gateway', $con);
    	      $gateway_id = $get_gateway['gatewayID'];
    	      
    	      $eCheckStatus = $get_gateway['echeckStatus'];
    	      
    	      if($get_gateway['gatewayType']==5 )
    	      $data['userName']   =$get_gateway['gatewayUsername'];
    	      else
    	        $data['userName']   ='';
    	      
    	    $bill_email='';  
    	    
    	    $scheduleID = $this->czsecurity->xssCleanPostInput('scheduleID');
    	   
    	    if($this->czsecurity->xssCleanPostInput('pay'))
    	    {
             
				 $qblistID = '';
		
                 $paymethod = $scheduleID;
             
               
		        //Billing Data
		        $fname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('first_name'));
		        $lname = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('last_name'));
		      
		        
		        $country = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('country'));
		        $address = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address'));
		        $address2 = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('address2'));
		        $city = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('city'));
		        $state = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('state'));
		        $zip = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('zip'));
		        $customerID = $in_data['CustomerListID'];
				$companyID = $in_data['companyID'];
				$phone = $in_data['phoneNumber'];
				
				if($scheduleID == 2){
                    $eCheckStatus = '1';
                    $accountName = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountName'));
                    $accountNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('accountNumber'));
                    $routeNumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('routeNumber'));
                    $acct_holder_type = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_holder_type'));
                    $acct_type = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('acct_type'));
                    $sec_code =     'WEB';  

                    $accountName = ($accountName != '')?$accountName:$fname.' '.$lname;
                    $acct_holder_type = ($acct_holder_type != '')?$acct_holder_type:'business';
                    $acct_type = ($acct_type != '')?$acct_type:'checking';
                    
                    //card data array
                    //save card process
                    $card_data = array(
                        'accountName'      =>$accountName,
                        'accountNumber'    =>$accountNumber, 
                        'routeNumber'       =>$routeNumber,
                        'accountHolderType' =>$acct_holder_type,
                        'accountType'      =>$acct_type, 
                        'secCodeEntryMethod' => $sec_code,
                        'customerListID' =>$customerID, 
                        'companyID'      =>$companyID,
                        'merchantID'     => $marchant_id,
                        'createdAt'      => date("Y-m-d H:i:s"),
                        'Billing_Addr1'  =>$address,
                        'Billing_Addr2'  =>$address2,    
                        'Billing_City'   =>$city,
                        'Billing_State'  =>$state,
                        'Billing_Country'    =>$country,
                        'Billing_Contact'    =>$phone,
                        'Billing_Zipcode'    =>$zip,
                        'CardType'  => 'Echeck',
                    );
                
                
                        
                }else{
                    $eCheckStatus = '0';
                    $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
                    $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
                    $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
                    $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));
                    $card_no = $cnumber;
                   
                    $exyear = $expyear;
                    $cardtype      = $this->general_model->getType($cnumber);
                    
                    $card_data = array(
                        'cardMonth'    =>$expmonth,
                        'cardYear'     =>$expyear, 
                        'CardType'     =>$cardtype,
                        'CustomerCard' =>$cnumber,
                        'CardCVV'      =>$cvv, 
                        'customerListID' =>$customerID, 
                        'companyID'      =>$companyID,
                        'merchantID'     => $marchant_id,
                        'createdAt'      => date("Y-m-d H:i:s"),
                        'Billing_Addr1'  =>$address,
                        'Billing_Addr2'  =>$address2,    
                        'Billing_City'   =>$city,
                        'Billing_State'  =>$state,
                        'Billing_Country'    =>$country,
                        'Billing_Contact'    =>$phone,
                        'Billing_Zipcode'    =>$zip,
                        
                    );  
                }
            	
            	
		        $savepaymentinfo = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('savepaymentinfo'));
    		    $sendrecipt = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('sendrecipt'));
			
				
					$condition_mail         = array('templateType'=>'5', 'merchantID'=>$marchant_id); 
					$ref_number =  $result['refNumber']; 

                    $tr_date   =date('Y-m-d h:i A');

                    // Convert added date in timezone 
                    if(isset($rs_daata['merchant_default_timezone']) && !empty($rs_daata['merchant_default_timezone'])){
                        $timezone = ['time' => $tr_date, 'current_format' => 'UTC', 'new_format' => $rs_daata['merchant_default_timezone']];
                        $tr_date = getTimeBySelectedTimezone($timezone);
                        $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
                    }

					$toEmail = $in_data['userEmail']; 
					$company=$in_data['companyName']; 
					$customer = $in_data['fullName'];
					
					
				if(!empty($in_data) )
				{
				
					$gateway = $get_gateway['gatewayType']; 
					
					if($payamount > 0)
				    {
					
    					if($gateway==1 || $gateway==9)
    					{
					        include APPPATH . 'third_party/nmiDirectPost.class.php';
    	                
                            //start NMI
                            $nmiuser   = $get_gateway['gatewayUsername'];
            		        $nmipass   = $get_gateway['gatewayPassword'];
            		        $nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
                		        
                            $transaction = new nmiDirectPost($nmi_data); 
                                	
                            if($paymethod =='2'){ // this payment option is for eCheck 
    	          
    	                        if($eCheckStatus == '1'){
    	                                 
    	                             
                                        $transaction->setAccountName($accountName);
                                        $transaction->setAccount($accountNumber);
                                        $transaction->setRouting($routeNumber);
                                        $sec_code =     'WEB';
                                        $transaction->setAccountType($acct_type);

                                        $transaction->setAccountHolderType($acct_holder_type);
                                        $transaction->setSecCode($sec_code);
                                        $transaction->setPayment('check');

                                        $transaction->setCompany($company);
                                        $transaction->setFirstName($fname);
                                        $transaction->setLastName($lname);

                                        $transaction->setCountry($country);
                                        $transaction->setCity($city);
                                        $transaction->setState($state);
                                        $transaction->setZip($zip);
                                        $transaction->setPhone($phone);
                                        $transaction->setEmail($toEmail);
                    					
    	                                 
    	                       }
    	                                
                            }else{
                                	
                						$transaction->setCcNumber($cnumber);
                					    $expmonth =  $expmonth;
                						$exyear   = $expyear;
                						$exyear   = substr($exyear,2);
                						if(strlen($expmonth)==1){
                							$expmonth = '0'.$expmonth;
                						}
                					    $expry    = $expmonth.$exyear;  
                						$transaction->setCcExp($expry);
                						$transaction->setCvv($cvv);
            						    // add level III data
                                        $level_request_data = [
                                            'transaction' => $transaction,
                                            'card_no' => $cnumber,
                                            'merchID' => $marchant_id,
                                            'amount' => $amount,
                                            'invoice_id' => $invoice_no,
                                            'gateway' => 1
                                        ];
                                        $transaction = addlevelThreeDataInTransaction($level_request_data);
                                	}
    					    
    					    $transaction->setCompany($in_data['companyName']);
    						$transaction->setFirstName($fname);
    						$transaction->setLastName($lname);
    						$transaction->setAddress1($address);
    						$transaction->setCountry($country);
    						$transaction->setCity($city);
    						$transaction->setState($state);
    						$transaction->setZip($zip);
    					
    						$transaction->setAmount($payamount);
            			    $transaction->sale();
            			    
            				$getwayResponse = $transaction->execute(); 
            				
            				$scode='';$tr_type='';
    					    if( $getwayResponse['response_code']=="100")
    					    {
				                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
							    $scode ="SUCCESS";
							    $check_result =$getwayResponse;
							    $tr_type='Sale';
    					    }
    					    else
    					    {
                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
    					        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:'.$getwayResponse['responsetext'].'</strong> </div>');  
            					redirect($_SERVER['HTTP_REFERER']);
    					    }
    					}
    					if($gateway==2)
    					{
    					 	
				
						    include APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
				        	$this->load->config('auth_pay');
						    //Login in Auth
					       	$apiloginID       = $get_gateway['gatewayUsername'];
					     	$transactionKey   = $get_gateway['gatewayPassword'];

					        $name = $fname." ".$lname;
        					if($this->config->item('auth_test_mode')){
        						$sandbox = TRUE;
        					}else{
        						$sandbox = FALSE;
        					}
							$transaction = new AuthorizeNetAIM($apiloginID,$transactionKey);
							$transaction->setSandbox($sandbox);
							
							if($paymethod =='2'){ // this payment option is for eCheck 
    	          
    	                        if($eCheckStatus == '1'){
    	                            $sec_code =     'WEB';   
                                    $transaction->setECheck($routeNumber, $accountNumber, $acct_type, $bank_name='Wells Fargo Bank NA', $accountName, $sec_code);
                	                    
                                	$transaction->__set('company',$company);
            						$transaction->__set('first_name',$fname);
            						$transaction->__set('last_name', $lname);
            						$transaction->__set('address',$address);
            						$transaction->__set('country',$country);
            						$transaction->__set('city',$city);
            						$transaction->__set('state',$state);
            						$transaction->__set('zip',$zip);
            						
            						$transaction->__set('ship_to_address', $address);
            						$transaction->__set('ship_to_country',$country);
            						$transaction->__set('ship_to_city',$city);
            						$transaction->__set('ship_to_state',$state);
            						$transaction->__set('ship_to_zip',$zip);
            						
            						
            						$transaction->__set('phone',$phone);
            					
            						$transaction->__set('email',$toEmail);
                    				$getwayResponse = $transaction->authorizeAndCapture($payamount);        
    	                        }
    	                                
                            }
                            else
                            {
                                	    
    							$card_no  = $cnumber;
    							$expmonth = $expmonth;
    							$exyear   = $expyear;
    							$exyear   = substr($exyear,2);
    							if(strlen($expmonth)==1){
    								$expmonth = '0'.$expmonth;
    							}
    							$amount =$payamount;
    							$expry = $expmonth.$exyear; 
                                            
								$transaction->__set('company',$in_data['companyName']);
								$transaction->__set('first_name', $fname);
								$transaction->__set('last_name',  $lname);
								$transaction->__set('address',  $address);
								$transaction->__set('country', $country);
								$transaction->__set('city', $city);
								$transaction->__set('state', $state);
            								
            					$getwayResponse = $transaction->authorizeAndCapture($payamount,$card_no,$expry);
            							       
                            }
                                	
    						if( $getwayResponse->response_code=="1" && $getwayResponse->transaction_id != 0 && $getwayResponse->transaction_id != '')
    						{
					            $scode ="SUCCESS";
							    $check_result =$getwayResponse;
							    $tr_type=$getwayResponse->type;
					            $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
    							
    						}
    						else{
                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$getwayResponse->response_reason_text .'</div>');  
								redirect($_SERVER['HTTP_REFERER']);
    						}
					
						}
						if($gateway==3)
    					{
							include APPPATH . 'third_party/PayTraceAPINEW.php';
							$this->load->config('paytrace');
							
							$payusername   = $get_gateway['gatewayUsername'];
							$paypassword   = $get_gateway['gatewayPassword'];
							$integratorId  = $get_gateway['gatewaySignature'];

							$grant_type    = "password";
							
							$name = $fname." ".$lname;
					
						
						    $name = $fname." ".$lname;
					        $expmonth = $expmonth;
							if(strlen($expmonth)==1){
								$expmonth = '0'.$expmonth;
							}
						    $amount = $payamount;
							$payAPI  = new PayTraceAPINEW();	
							$oauth_result = $payAPI->oAuthTokenGenerator($grant_type, $payusername, $paypassword);
							
							
							//call a function of Utilities.php to verify if there is any error with OAuth token. 
							$oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
							
						
							
							if(!$oauth_moveforward)
							{ 
								$json 	= $payAPI->jsonDecode($oauth_result['temp_json_response']); 
								//set Authentication value based on the successful oAuth response.
								//Add a space between 'Bearer' and access _token 
								$oauth_token = sprintf("Bearer %s",$json['access_token']);
                                $invoice_number = mt_rand(10000000, 77777777);
								$request_data = array(
									"amount"            => $payamount,
									"credit_card"       => array (
										"number"            => $cnumber,
										"expiration_month"  =>$expmonth,
										"expiration_year"   =>$expyear
									),
									
									"csc"               => $cvv,
									'invoice_id'=> $invoice_number,
									
									"billing_address"=> array(
										"name"          =>$name,
										"street_address"=> $address,
										"city"          => $city,
										"state"         => $state,
										"zip"           => $zip
									)
								);  
							
							   $request_data = json_encode($request_data); 
							   $gatewayres    =  $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_SALE );	
						
							   $response  = $payAPI->jsonDecode($gatewayres['temp_json_response']); 
							  
							   if ( $gatewayres['http_status_code']=='200' )
							   {
                                    // add level three data in transaction
                                    if($response['success']){
                                        $level_three_data = [
                                            'card_no' => $cnumber,
                                            'merchID' => $marchant_id,
                                            'amount' => $payamount,
                                            'token' => $oauth_token,
                                            'integrator_id' => $integratorId,
                                            'transaction_id' => $response['transaction_id'],
                                            'invoice_id' => $invoice_number,
                                            'gateway' => 3,
                                        ];
                                        addlevelThreeDataInTransaction($level_three_data);
                                    }
							                 $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
							                 $scode ="SUCCESS";
        								     $tr_type='pay_sale';
    									    $check_result = $response;
							   }
							   else
							   {
                                        $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
										$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'.$response['status_message'].'</div>'); 
										redirect($_SERVER['HTTP_REFERER']);
								}
									
								
							   
							} else{
								$nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
								$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error:</strong> Authentication not valid.</div>'); 
							}
							redirect($_SERVER['HTTP_REFERER']);
    					}
    					if($gateway==4)
    					{
    					    include APPPATH . 'third_party/PayPalAPINEW.php';
    						$this->load->config('paypal');
							if($this->config->item('mode') == 0){
								$sandbox = TRUE;
							}else{
								$sandbox = FALSE;
							}
							$config = array(
								'Sandbox' => $sandbox, 			// Sandbox / testing mode option.
    							'APIUsername' => $get_gateway['gatewayUsername'], 	// PayPal API username of the API caller
    							'APIPassword' => $get_gateway['gatewayPassword'],	// PayPal API password of the API caller
    							'APISignature' => $get_gateway['gatewaySignature'], 	// PayPal API signature of the API caller
    							'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
    							'APIVersion' => $this->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
    						  );
    						  $this->load->library('paypal/Paypal_pro', $config);	  
    						    if($config['Sandbox'])
    							{
							    	error_reporting(E_ALL);
							    	ini_set('display_errors', '1');
						        	}
						        	
						       $amount       = $payamount;
							$creditCardType   = 'Visa';
							$creditCardNumber = $cnumber;
							$expDateMonth     = $expmonth;
							$expDateYear      = $expyear;
							$creditCardType   = ($cardtype)?$cardtype:$creditCardType;
							$padDateMonth 	  = str_pad($expDateMonth, 2, '0', STR_PAD_LEFT);
							$cvv2Number       =   $cvv;
							$currencyID       = "USD";
							
							$firstName = $fname;
							$lastName =  $lname; 
							$address1 = $address; 
							$address2 = $address2; 
							$country  = $country; 
							$city     = $city;
							$state    = $state;		
							$zip  = $zip;  
							$email = $bill_email; 
											
							$DPFields = array(
								'paymentaction' => 'Sale', 	// How you want to obtain payment.  
								'ipaddress' => '', 							// Required.  IP address of the payer's browser.
								'returnfmfdetails' => '0' 					// Flag to determine whether you want the results returned by FMF.  1 or 0.  Default is 0.
							);
											
							$CCDetails = array(
								'creditcardtype' => $cardtype, 					// Required. Type of credit card.  Visa, MasterCard, Discover, Amex, Maestro, Solo.  If Maestro or Solo, the currency code must be GBP.  In addition, either start date or issue number must be specified.
								'acct'           => $cnumber, 								// Required.  Credit card number.  No spaces or punctuation.  
								'expdate'        => $expmonth.$expyear, 							// Required.  Credit card expiration date.  Format is MMYYYY
								'cvv2'           => $cvv, 								// Requirements determined by your PayPal account settings.  Security digits for credit card.
								'startdate'      => '', 							// Month and year that Maestro or Solo card was issued.  MMYYYY
								'issuenumber'    => ''		 				      // Issue number of Maestro or Solo card.  Two numeric digits max.
							);
											
							$PayerInfo = array(
								'email'          => $bill_email, 								// Email address of payer.
								'payerid'        => '', 							// Unique PayPal customer ID for payer.
								'payerstatus'    => 'verified', 						// Status of payer.  Values are verified or unverified
								'business'       => '' 							// Payer's business name.
							);  
											
							$PayerName = array(
								'salutation'     => '', 						// Payer's salutation.  20 char max.
								'firstname'      => $fname, 							// Payer's first name.  25 char max.
								'middlename'     => '', 						// Payer's middle name.  25 char max.
								'lastname'       => $lname, 							// Payer's last name.  25 char max.
								'suffix'         => ''								// Payer's suffix.  12 char max.
							);
										
							$BillingAddress = array(
								'street'         => $address1, 						// Required.  First street address.
								'street2'        => $address2, 						// Second street address.
								'city'           => $city, 							// Required.  Name of City.
								'state'          => $state, 							// Required. Name of State or Province.
								'countrycode'    => $country, 					// Required.  Country code.
								'zip'            => $zip 						// Phone Number of payer.  20 char max.
							);
						
												
						   $PaymentDetails = array(
								'amt'            => $payamount,					// Required.  Three-letter currency code.  Default is USD.
								'itemamt'        => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
								'shippingamt'    => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
								'insuranceamt'   => '', 					// Total shipping insurance costs for this order.  
								'shipdiscamt'    => '', 					// Shipping discount for the order, specified as a negative number.
								'handlingamt'    => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
								'taxamt'         => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
								'desc'           => '', 							// Description of the order the customer is purchasing.  127 char max.
								'custom'         => '', 						// Free-form field for your own use.  256 char max.
								'invnum'         => '', 						// Your own invoice or tracking number
								'buttonsource'   => '', 					// An ID code for use by 3rd party apps to identify transactions.
								'notifyurl'      => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
								'recurring'      => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
							);					

							$PayPalRequestData = array(
								'DPFields'       => $DPFields, 
								'CCDetails'      => $CCDetails, 
								'PayerInfo'      => $PayerInfo, 
								'PayerName'      => $PayerName, 
								'BillingAddress' => $BillingAddress, 
								'PaymentDetails' => $PaymentDetails, 
								
							);
								
							$PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
							if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))
							{ 	
							                $scode        = "SUCCESS";
        								    $check_result = $PayPalResult;
        								    $tr_type="Paypal_sale";
                                            $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
						
							}
							else{
                                    $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
    								$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>'. $PayPalResult["ACK"].'</div>'); 
									redirect($_SERVER['HTTP_REFERER']);
    						   }
    					}
    					if($gateway==5)
    					{
    					    require_once APPPATH."third_party/stripe/init.php";	
					    	require_once(APPPATH.'third_party/stripe/lib/Stripe.php');
			
    						if($payamount > 0)
    						{
							
    							$paidamount =  (int)($payamount*100);
    							$amount     = $payamount;
    							
    							\Stripe\Stripe::setApiKey($get_gateway['gatewayPassword']);
    							
                                $cnumber = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardNumber'));
                                $expmonth = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry'));
                                $expyear = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardExpiry2'));
                                $cvv = $this->security->xss_clean($this->czsecurity->xssCleanPostInput('cardCVC'));

                                $res = \Stripe\Token::create([
                                    'card' => [
                                        'number'    => $cnumber,
                                        'exp_month' => $expmonth,
                                        'exp_year'  => $expyear,
                                        'cvc'       => $cvv,
										'name'      => $customer,
                                    ],
                                ]);

                                $tcharge = json_encode($res);
                                $rest = json_decode($tcharge);
                                $trID='failed'.time();
                                if ($rest->id) {
                                    $charge =   \Stripe\Charge::create(array(
                                      "amount" => $paidamount,
                                      "currency" => "usd",
                                      "source" => $rest->id, // obtained with Stripe.js
                                      "description" => "Charge Using Stripe Gateway",
                                     
                                    )); 
                                    $charge= json_encode($charge);
                                
                                    $resultstripe = json_decode($charge);
                                
                               
                                   
                                   if($resultstripe->paid=='1' && $resultstripe->failure_code=="")
                                   {
                                    
                                                $scode = "SUCCESS";
                                                $check_result = $resultstripe;
                                                $tr_type="Stripe_sale";
                                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                       
                                   }else{
                                       $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                       $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in Freshbook</div>');  
                                   }

                                }else{
                                    $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$rest->status.'</div>');  
                                }
                                
    							
							   
    						}  
						}	 
    					if($gateway==12)
                        {
                            include APPPATH . 'third_party/TSYS.class.php';

                            $deviceID = $get_gateway['gatewayMerchantID'].'01';            
                            $gatewayTransaction              = new TSYS();
                            $gatewayTransaction->environment = $this->gatewayEnvironment;
                            $gatewayTransaction->deviceID = $deviceID;
                            $result = $gatewayTransaction->generateToken($get_gateway['gatewayUsername'],$get_gateway['gatewayPassword'],$get_gateway['gatewayMerchantID']);
                            $generateToken = '';
                            $responseErrorMsg = '';
                            if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
                                $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
                                $responseErrorMsg = $result['GenerateKeyResponse']['responseMessage'];
                            }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
                                $generateToken = $result['GenerateKeyResponse']['transactionKey'];
                                
                            }
                            $gatewayTransaction->transactionKey = $generateToken;
                            if($eCheckStatus == '1'){
                                $transaction['Ach'] = array(
                                    "deviceID"              => $deviceID,
                                    "transactionKey"        => $generateToken,
                                    "transactionAmount"     => (int)($payamount * 100),
                                    "accountDetails"        => array(
                                            "routingNumber" => $routeNumber,
                                            "accountNumber" => $accountNumber,
                                            "accountType"   => strtoupper($accountType),
                                            "accountNotes"  => "count",
                                            "addressLine1"  => $address1,
                                            "zip"           => ($zip != '')?$zip:'None',
                                            "city"          => ($city != '')?$city:'None'
                                    ),
                                    "achSecCode"                => "WEB",
                                    "originateDate"             => date('Y-m-d'),
                                    "addenda"                   => "addenda",
                                    "firstName"                 => (($fname != ''))?$fname:'None',
                                    "lastName"                  => (($lname != ''))?lfname:'None',
                                    "addressLine1"              => ($address != '')?$address:'None',
                                    "zip"                      => ($zip != '')?$zip:'None',
                                    "city"                      => ($city != '')?$city:'None'  
                                );
                            }else{
                                $exyear1   = substr($expyear,2);
                                if(empty($exyear1)){
                                    $exyear1  = $exyear;
                                }
                                if(strlen($expmonth)==1){
                                  $expmonth = '0'.$expmonth;
                                }
                                $expry    = $expmonth.'/'.$exyear1;

                            
                                
                                $transaction['Sale'] = array(
                                        "deviceID"                          => $deviceID,
                                        "transactionKey"                    => $generateToken,
                                        "cardDataSource"                    => "MANUAL",  
                                        "transactionAmount"                 => (int)($payamount * 100),
                                        "currencyCode"                      => "USD",
                                        "cardNumber"                        => $cnumber,
                                        "expirationDate"                    => $expry,
                                        "cvv2"                              => $cvv,
                                        "addressLine1"                      => ($address != '')?$address:'None',
                                        "zip"                               => ($zip != '')?$zip:'None',
                                        "orderNumber"                       => $invoice_no,
                                        "firstName"                         => (($fname != ''))?$fname:'None',
                                        "lastName"                          => (($lname != ''))?$lname:'None',
                                        "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                                        "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                                        "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                                        "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                                        "terminalOutputCapability"          => "DISPLAY_ONLY",
                                        "maxPinLength"                      => "UNKNOWN",
                                        "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                                        "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                                        "cardPresentDetail"                 => "CARD_PRESENT",
                                        "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                                        "cardholderAuthenticationEntity"    => "OTHER",
                                        "cardDataOutputCapability"          => "NONE",

                                        "customerDetails"   => array( 
                                            "contactDetails" => array(
                                                "addressLine1"=> ($address != '')?$address:'None',
                                                 "addressLine2"  => ($address2 != '')?$address2:'None',
                                                "city"=>($city != '')?$city:'None',
                                                "zip"=>($zip != '')?$zip:'None',
                                            ),
                                            "shippingDetails" => array( 
                                                "firstName"=>(($fname != ''))?$fname:'None',
                                                "lastName"=>(($lname != ''))?$lname:'None',
                                                "addressLine1"=>($address != '')?$address:'None',
                                                 "addressLine2" => ($address2 != '')?$address2:'None',
                                                "city"=>($city != '')?$city:'None',
                                                "zip"=>($zip != '')?$zip:'None' 
                                               
                                             )
                                        )
                                );
                                if($cvv == ''){
                                    unset($transaction['Sale']['cvv2']);
                                }
                            }
                            $responseType = 'SaleResponse';
                            if($generateToken != ''){
                                $result = $gatewayTransaction->processTransaction($transaction);
                            }else{
                                $responseType = 'GenerateKeyResponse';
                            }
                            
                            
                            $trID='';
                            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') 
                            {
                                
                                $scode = "SUCCESS";
                                $check_result = $result[$responseType];
                                $tr_type="sale";
                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                   
                            }else{
                               $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);    
                               $err_msg = $result[$responseType]['responseMessage'];
                                if($responseErrorMsg != ''){
                                    $err_msg = $responseErrorMsg;
                                }

                                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Transaction Failed -  ' .$err_msg. '</strong></div>');  
                                       }
                               
                             
                        }        
    					
    					if($gateway==6)
    					{
                            require_once APPPATH."third_party/usaepay/usaepay.php";
                            $payusername   = $get_gateway['gatewayUsername'];
                            $password      = $get_gateway['gatewayPassword'];
                            $customerID    =  $in_data['CustomerListID'];
                            $cvv='';	
                            $invNo  =mt_rand(1000000,2000000); 

                            if($this->config->item('mode') == 0){
                                $sandbox = TRUE;
                            }else{
                                $sandbox = FALSE;
                            }
                            $transaction = new umTransaction;
                            $transaction->ignoresslcerterrors= ($this->config->item('ignoresslcerterrors') !== null ) ? $this->config->item('ignoresslcerterrors') : true;

                            $transaction->key=$payusername;
                            $transaction->pin=$password;
                            $transaction->usesandbox=$sandbox;
                            $transaction->invoice=$invNo;   		// invoice number.  must be unique.
						    $transaction->description="Chargezoom Public Invoice Payment";	// description of charge
                          
                            $transaction->testmode=0;   // Change this to 0 for the transaction to process
                            $transaction->command="sale";	
                            $transaction->card = $cnumber;
                            $expyear   = substr($expyear,2);



                            if(strlen($expmonth)==1){
                                $expmonth = '0'.$expmonth;
                            }
                            $expry    = $expmonth.$expyear;  
                            $transaction->exp = $expry;
                            if($cvv!="")
                                $transaction->cvv2 = $cvv;
                           
                            $transaction->billfname = $fname;
                            $transaction->billlname = $lname;
                            $transaction->billstreet = $address;
                            $transaction->billstreet2 = $address2;
                            $transaction->billcountry = $country;
                            $transaction->billcity    = $city;
                            $transaction->billstate = $state;
                            $transaction->billzip = $zip;


                            $transaction->shipfname = $fname;
                            $transaction->shiplname = $lname;
                            $transaction->shipstreet = $address;
                            $transaction->shipstreet2 = $address2;
                            $transaction->shipcountry = $country;
                            $transaction->shipcity    = $city;
                            $transaction->shipstate = $state;
                            $transaction->shipzip = $zip;

                            $amount =$payamount;

                         
                            $transaction->amount = $amount;
                          
                            $transaction->Process();


                          
                            $error=''; 
                             
                            if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS')
                            {			
                                $scode = "SUCCESS";
                                $msg = $transaction->result;
                                $trID = $transaction->refnum;

                                $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
                                $check_result = $res;
                                $tr_type="Sale";
                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
        						   
						   }else{
						        $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
						        $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Error in Freshbook</div>');  
						   }
    					    
    					}
    					if($gateway==7)
    					{
    					    
    					            require_once dirname(__FILE__) . '/../../../../vendor/autoload.php';
    		           
                		           
        		                    $amount =  $payamount;
        		                     $payusername   = $get_gateway['gatewayUsername'];
									$secretApiKey   =  $get_gateway['gatewayPassword'];
									
								
									
								    $config = new ServicesConfig();
               
                                    $config->secretApiKey = $secretApiKey;
                                    $config->serviceUrl =  $this->config->item('GLOBAL_URL');
                                    
                                    	
        		                    $customerID = $in_data['CustomerListID'] ;
        		                        ServicesContainer::configure($config);
                                        $card = new CreditCardData();
                                        $card->number = $cnumber;
                                        $card->expMonth = $expmonth;
                                        $card->expYear = $expyear;
                                        if($cvv!="")
                                        $card->cvn = $cvv;
                                        $address1 = $address;
                                        $address = new Address();
                                        $address->streetAddress1 = $address1;
                                        $address->city = $city;
                                        $address->state = $state;
                                        $address->postalCode = $zip;
                                        $address->country = $country;
                                        
        		         	          
                                    
                                    
                                        $invNo  =mt_rand(5000000,20000000);
                                        
                                    
                                        
                                     	try
                                        {
                                            $eCheck_payment = false;
                                            if($scheduleID =='2' && $eCheckStatus == '1'){ // this payment option is for eCheck
                                                $eCheck_payment = true;
                                                $check = new ECheck();
                                                $check->accountNumber = $accountNumber;
                                                $check->routingNumber = $routeNumber;
                                                if(strtolower($acct_type) == 'checking'){
                                                    $check->accountType = 0;
                                                }else{
                                                    $check->accountType = 1;
                                                }

                                                if(strtoupper($acct_holder_type) == 'PERSONAL'){
                                                    $check->checkType = 0;
                                                }else{
                                                    $check->checkType = 1;
                                                }
                                                $check->checkHolderName = $accountName;
                                                $check->secCode = "WEB";

                                                $response = $check->charge($amount)
                                                ->withCurrency(CURRENCY)
                                                ->withAddress($address)
                                                ->withInvoiceNumber($invNo)
                                                ->withAllowDuplicates(true)
                                                ->execute();
                                            }else{

                                                $response = $card->charge($amount)
                                                ->withCurrency("USD")
                                                ->withAddress($address)
                                                ->withInvoiceNumber($invNo)
                                                ->withAllowDuplicates(true)
                                                ->execute();
                                            }
                            
                            			        $error=''; 	  
                            			    if($response->responseCode != 0 && $response->responseCode != '00')
                                            {
                                                $error='Gateway Error. Invalid Account Details';
                                                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>'.$error.'</strong></div>'); 
                                                redirect($_SERVER['HTTP_REFERER']);  

                                            }    
                            	  
                            			          
                            			        if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
                                                {
                                                    
                                                    if(!$eCheck_payment){
                                                        // add level three data
                                                        $transaction = new Transaction();
                                                        $transaction->transactionReference = new TransactionReference();
                                                        $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                                                        $level_three_request = [
                                                            'card_no' => $cnumber,
                                                            'amount' => $amount,
                                                            'invoice_id' => $invNo,
                                                            'merchID' => $marchant_id,
                                                            'transaction_id' => $response->transactionId,
                                                            'transaction' => $transaction,
                                                            'levelCommercialData' => $levelCommercialData,
                                                            'gateway' => 7
                                                        ];
                                                        addlevelThreeDataInTransaction($level_three_request);
                                                    }

                                                   $scode = "SUCCESS";
                                                   $msg = $response->responseMessage;
                                                     $trID = $response->transactionId;
                                                     
                                                     $res =array('transactionCode'=>'200', 'status'=>$msg, 'transactionId'=> $trID );
        								           $check_result = $res;
        								           $tr_type="Sale";
                                                   $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
        								    
                                                }else{
                                                    $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                }
                                        }catch (BuilderException $e)
                                            {
                                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                $error= 'Build Exception Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ConfigurationException $e)
                                            {
                                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                $error='ConfigurationException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (GatewayException $e)
                                            {
                                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                $error= 'GatewayException Failure: ' . $e->getMessage();
                                                $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (UnsupportedTransactionException $e)
                                            {
                                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
                                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            catch (ApiException $e)
                                            {
                                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                                $error=' ApiException Failure: ' . $e->getMessage();
                                             $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
                                            }
                                            
                                            
                                            
                                            
    					}
    					if($gateway==8)
    					{
    					   $this->load->config('cyber_pay');
    					   
        		       
            		        $amount =$payamount;
                            $flag = 'true';
                            
            		        $phone="4158880000"; $email="test@gmail.com"; $companyName='Dummy Company';
            		        
                            $option =array();
        				        $option['merchantID']     = $get_gateway['gatewayUsername'];
            			        $option['apiKey']         = $get_gateway['gatewayPassword'];
        						$option['secretKey']      = $get_gateway['gatewaySignature'];
        						
        						if($this->config->item('mode') == 0){
									$sandbox = TRUE;
								}else{
									$sandbox = FALSE;
								}
        						if($sandbox == 1)
        						$env   = $this->config->item('SandboxENV');
        						else
        						$env   = $this->config->item('ProductionENV');
        						$option['runENV']      = $env;
        						
        				
        				$commonElement = new CyberSource\ExternalConfiguration($option);
        				$config = $commonElement->ConnectionHost();
        				$merchantConfig = $commonElement->merchantConfigObject();
        				$apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        				$api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        				
        				$cliRefInfoArr = [
        					"code" => "test_payment"
        				];
        			
        				
        			
        				$client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        				
        				if ($flag == "true")
        				{
        					$processingInformationArr = [
        						"capture" => true, "commerceIndicator" => "internet"
        					];
        				}
        				else
        				{
        					$processingInformationArr = [
        						"commerceIndicator" => "internet"
        					];
        				}
        				$processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
        
        				$amountDetailsArr = [
        					"totalAmount" => $amount,
        					"currency" => CURRENCY,
        				];
        				$amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);
        				
        				$billtoArr = [
        					"firstName" => $fname,
        					"lastName"  =>$lname,
        					"address1"  => $address,
        					"postalCode"=> $zip,
        					"locality"  => $city,
        					"administrativeArea" => $state,
        					"country"  => $country,
        					"phoneNumber" => $phone,
        					"company"  => $companyName,
        					"email"    => $email
        				];
        				
        			
        				
        				$billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);
        				
        				$orderInfoArr = [
        					"amountDetails" => $amountDetInfo, 
        					"billTo" => $billto
        				];
        				
        				
        					
        				$order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        				
        				$paymentCardInfo = [
        					"expirationYear" => $expyear,
        					"number" => $cnumber,
        					"securityCode" => $cvv,
        					"expirationMonth" => $expmonth
        				];
        				$card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        				
        				$paymentInfoArr = [
        					"card" => $card
        				];
        				$payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);
        
        				$paymentRequestArr = [
        					"clientReferenceInformation" =>$client_reference_information, 
        					"orderInformation" =>$order_information, 
        					"paymentInformation" =>$payment_information, 
        					"processingInformation" =>$processingInformation
        				];
        				$paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        				
        				$api_response = list($response, $statusCode, $httpHeader) = null;
        				
        	         	
        	         	
        				$tr_type  = 'sale'; 
                         try
        				{
        					//Calling the Api
        					$api_response = $api_instance->createPayment($paymentRequest);
        					
        				
        		          	
        					
        					if($api_response[0]['status']!="Declined" && $api_response[1]== '201')
        					{
        					                $scode = "SUCCESS";
        								    $check_result = $api_response;
        								    $tr_type="Cyber_sale";
                                            $trID =   $api_response[0]['id']; 
                                            $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
        					}else{
                                $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                            }
        				
        				    
        				  }catch(Cybersource\ApiException $e)
        				{
        				
                              $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
        					  $error = $e->getMessage();
        					  $this->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
        				}
        				
    					}
                        if($gateway==16)
                        {
                            include APPPATH . 'third_party/EPX.class.php';
                            $CUST_NBR = $get_gateway['gatewayUsername'];
                            $MERCH_NBR = $get_gateway['gatewayPassword'];
                            $DBA_NBR = $get_gateway['gatewaySignature'];
                            $TERMINAL_NBR = $get_gateway['extra_field_1'];

                            $amount = number_format($payamount,2,'.','');
                            $transaction = array(
                                    'CUST_NBR' => $CUST_NBR,
                                    'MERCH_NBR' => $MERCH_NBR,
                                    'DBA_NBR' => $DBA_NBR,
                                    'TERMINAL_NBR' => $TERMINAL_NBR,
                                    'AMOUNT' => $amount,
                                    'TRAN_NBR' => rand(1,10),
                                    'BATCH_ID' => time(),
                                    'VERBOSE_RESPONSE' => 'Y',
                            );
                            if($fname != ''){
                                $transaction['firstName'] = $fname;
                            }
                            if($lname != ''){
                                $transaction['lastName'] = $lname;
                            }
                            
                            if($eCheckStatus == '1'){
                                $transaction['RECV_NAME'] = $accountName;
                                $transaction['ACCOUNT_NBR'] = $accountNumber;
                                $transaction['ROUTING_NBR'] = $routeNumber;

                                if($accountType == 'savings'){
                                    $transaction['TRAN_TYPE'] = 'CKS2';
                                }else{
                                    $transaction['TRAN_TYPE'] = 'CKC2';
                                }
                                if($address != ''){
                                    $transaction['ADDRESS'] = $address;
                                }
                                if($city != ''){
                                    $transaction['CITY'] = $city;
                                }
                                if( $zip != ''){
                                    $transaction['ZIP_CODE'] = $zip;
                                }
                            }else{
                                if (strlen($expmonth) == 1) {
                                    $expmonth = '0' . $expmonth;
                                }
                                $exyear1  = substr($expyear, 2);
                                $transaction['EXP_DATE'] = $exyear1.$expmonth;
                                $transaction['ACCOUNT_NBR'] = $cnumber;
                                $transaction['TRAN_TYPE'] = 'CCE1';

                                if($address != ''){
                                    $transaction['ADDRESS'] = $address;
                                }
                                if($city != ''){
                                    $transaction['CITY'] = $city;
                                }
                                if($zip != ''){
                                    $transaction['ZIP_CODE'] = $zip;
                                }
                                if($cvv && !empty($cvv)){
                                    $transaction['CVV2'] = $cvv;
                                }
                            }
                            
                            $gatewayTransaction              = new EPX();
                            $result = $gatewayTransaction->processTransaction($transaction);
                            if( ($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' )
                            {
                                    $trID       = $responseId = $transactionID = $result['AUTH_GUID'];
                                
                                $scode = "SUCCESS";
                                $result['transactionid'] = $trID;
                                $check_result = $result;
                                $tr_type="sale";
                                $nf = $this->addNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);
                                   
                            }else{
                               $nf = $this->failedNotificationForMerchant($payamount,$customer,$customerID,$marchant_id,$invoice_no);   
                               $trID       = $responseId = $transactionID = 'TXNFAILED-'.time(); 
                               $code       =  '401';
                               $msg = $result['AUTH_RESP_TEXT'];
                               $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> '.$result['AUTH_RESP_TEXT'].'</div>');  
                           }
                             
                        }        
					
        				if($scode=='SUCCESS')
        				{
        				                  $invoiceID      = $result['invoiceID'];  
                						  $payID='';
                						  $pay      = ($payamount);
                						 
            					      	 $remainbal  = $result['BalanceRemaining']- $payamount;
            					      	 
            					      	 if($remainbal == '0')
            					      	  $ispaid 	 = '1';
            					      	 else
            					      	   $ispaid 	 = '0';
            					      	   
            					      	  $applbal  = $result['AppliedAmount']+ $payamount;
                						 $data   	 = array('IsPaid'=>$ispaid,'BalanceRemaining'=>$remainbal,'AppliedAmount'=>$applbal );
                						 $condition  = array('invoiceID'=>$result['invoiceID'],'merchantID'=>$marchant_id );
                						 
                						 
            						     $this->general_model->update_row_data('Freshbooks_test_invoice',$condition, $data);
            						    
            						     
            						      if(isset($accessToken) && isset($access_token_secret))
            						     { 
            						             if($fb_account==1)
            						             {
            						       
                                                   $c = new FreshbooksNew($marchant_id, OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                                                   $payment = array('invoiceid'=>$invoiceID,'amount'=>array('amount'=>$pay),'type'=>'Check','date'=>date('Y-m-d'));
                                                
                                                 }
            						            else
            						             {
            						                  $c = new Freshbooks(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_CALLBACK, $subdomain, $accessToken, $access_token_secret);
                                   
                                                        $payment = '<?xml version="1.0" encoding="utf-8"?>  
                            						    <request method="payment.create">  
                            						     <payment>         
                            						        <invoice_id>'.$invoice_no.'</invoice_id>               
                            						        <amount>'.$payamount.'</amount>             
                            						        <currency_code>USD</currency_code> 
                            						        <type>Check</type>                   
                            						      </payment>  
                            						   </request>'; 
            						            }
            						         
            						       $invoices = $c->add_payment($payment);
            						       
            						      
                                           if ($invoices['status'] != 'ok') 
                                           {
                            
                            			    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error</strong></div>');
                            			
                            			   }else
                            			   $payID = $invoices['id'];
        				    
										}
										
									if($eCheckStatus == '1'){
										$payType = true;
									}else{
										$payType = false;
									}
									$trid = $this->general_model->insert_gateway_transaction_data($check_result, $tr_type,  $get_gateway['gatewayID'], $get_gateway['gatewayType'],$customerID,$payamount,$marchant_id,$payID,$resellerID, $invoiceID,$payType);
									
            		                if(!empty($savepaymentinfo))
            		                {
                	                               $cardId = $this->card_model->process_card($card_data);
                            		}
                    					//save card process end
        					if(!empty($sendrecipt))
        					{
        					    $this->general_model->send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$payamount, $tr_date);	
        					}
                    				$this->session->set_userdata("tranID",$trID );
                                    $this->session->set_userdata("sess_invoice_id",$in_data); 	
    								 $this->session->set_flashdata('message','<div class="alert alert-success"><strong> Successfully Paid </strong></div>');  
    								   redirect($thankyou);
    					
        				}
        				else
        				{
        				  	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Payment Failed</div>'); 
    					   redirect($_SERVER['HTTP_REFERER']);  
        				    
        				}
        				
        				
    				
				}	
				}else{
					$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid invoice</div>'); 
					redirect($_SERVER['HTTP_REFERER']);
				}
			}
         	else
         	{
	         	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
				redirect($_SERVER['HTTP_REFERER']);
	        }
		}
		else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>This Link is not valid.</div>'); 
	    	redirect($_SERVER['HTTP_REFERER']);
	    }
	    
	 }
	 else{
	    	$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong>Invalid Request</div>'); 
	    	redirect($_SERVER['HTTP_REFERER']);
	    }   
	}
	  

	public function addNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        if($invoiceNumber == null){
            $title = 'Sale Payments';
            $nf_desc = 'A payment for '.$customerName.' was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'.';
            $type = 1;
        }else{
            $title = 'Invoice Checkout Payments';
            $in_data =    $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID'=>$merchantID));
            if(isset($in_data['refNumber'])){
                $invoiceRefNumber = $in_data['refNumber'];
            }else{
                $invoiceRefNumber = $invoiceNumber;
            }
            $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was made for <b>$'.$payAmount.'</b> on '.$payDateTime.'';
            $type = 2;
        }
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 4,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

		$condition_mail = array('templateType' => '15', 'merchantID' => $merchantID);
		$ref_number     = $in_data['refNumber'];
		$tr_date        = date('Y-m-d H:i:s');
		$toEmail        = false;
		$company        = $customerName;
		$customer       = $customerName;
		$this->general_model->send_mail_data($condition_mail, $company, $customer, $toEmail, $customerID, $ref_number, $payAmount, $tr_date);
		
        return true;
    }   
    public function failedNotificationForMerchant($payAmount,$customerName,$customerID,$merchantID,$invoiceNumber = null){
        /*Notification Saved*/
        
        $payDateTime = date('M d, Y h:i A');
        if($merchantID){
            $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $merchantID));
            if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                $timezone = ['time' => $payDateTime, 'current_format' => date_default_timezone_get(), 'new_format' => $m_data['merchant_default_timezone']];
                $payDateTime = getTimeBySelectedTimezone($timezone);
                if($payDateTime){
                    $payDateTime = date("M d, Y h:i A", strtotime($payDateTime));
                }
            }
        }
        $title = 'Failed Invoice Checkout payments';
        $in_data =    $this->general_model->get_row_data('Freshbooks_test_invoice', array('invoiceID'=>$invoiceNumber,'merchantID'=>$merchantID));
        if(isset($in_data['refNumber'])){
            $invoiceRefNumber = $in_data['refNumber'];
        }else{
            $invoiceRefNumber = $invoiceNumber;
        }
        $nf_desc = 'A payment for Invoice <b>'.$invoiceRefNumber.'</b> was attempted on '.$payDateTime.' but failed';
        $type = 2;
        
        $notifyObj = array(
            'sender_id' => $customerID,
            'receiver_id' => $merchantID,
            'title' => $title,
            'description' => $nf_desc,
            'is_read' => 1,
            'recieverType' => 4,
            'type' => $type,
            'typeID' => $invoiceNumber,
            'status' => 1,
            'created_at' => date('Y-m-d H:i:s')
        );
        $NotificationSaved = $this->general_model->insert_row('tbl_merchant_notification',$notifyObj);
        /* Update merchant new notification comes*/
        $con  = array('merchID' => $merchantID);
        $input_data = array('notification_read' => 0 );
        $update =   $this->general_model->update_row_data('tbl_merchant_data', $con, $input_data);
        /*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
        return true;
    }
}