<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
class Thankyou extends CI_Controller {
    
	function __construct()
	{
		parent::__construct();
		$this->load->model('general_model');
	}
	
	public function index(){
	  
        $data['primary_nav'] 	= primary_customer_nav();
        $data['template'] 		= template_variable();
        $data['transaction_id'] = $this->session->userdata('tranID');
        $data['invoice_data'] = $this->session->userdata('sess_invoice_id');


        $transaction_id = $data['transaction_id'];
		$payAmount = $data['invoice_data']['BalanceRemaining'];
		$surCharge = 0;
		$isSurcharge = 0;
		$totalAmount = 0;
		$transactionType = 0;
        $merchant_default_timezone = '';
        $transactionCode = 0;
		if($transaction_id != null){
			$con = array('transactionID' => $transaction_id);
            $pay_amount = $this->general_model->get_row_data('customer_transaction', $con);
            if(isset($pay_amount['transactionAmount'])){
                $transactionCode = $pay_amount['transactionCode'];
            	$payAmount = $pay_amount['transactionAmount'];
            	$transactionType = $pay_amount['transactionGateway'];
                $user_id = $data['invoice_data']['merchantID'];
                if($user_id){
                    // get merchant selected timezone
                    $m_data = $this->general_model->get_select_data('tbl_merchant_data', array('merchant_default_timezone'), array('merchID' => $user_id));
                    if(isset($m_data['merchant_default_timezone']) && !empty($m_data['merchant_default_timezone'])){
                        $merchant_default_timezone = $m_data['merchant_default_timezone'];
                    }
                }
            	if($pay_amount['transactionGateway'] == 10){
            		$resultAmount = getiTransactTransactionDetails($user_id,$transaction_id);
            		$totalAmount = $resultAmount['totalAmount'];
            		$surCharge = $resultAmount['surCharge'];
            		$isSurcharge = $resultAmount['isSurcharge'];
            		if($resultAmount['payAmount'] != 0){
            			$payAmount = $resultAmount['payAmount'];
            		}   
            	}
            	
            }
		}
		
        $data['transactionCode'] = $transactionCode;
        $data['merchant_default_timezone'] = $merchant_default_timezone;
        $data['transactionAmount'] = $payAmount;
        $data['surchargeAmount'] = $surCharge;
        $data['totalAmount'] = $totalAmount;
        $data['transactionType'] = $transactionType;
        $data['isSurcharge'] = $isSurcharge;

		$data['ip'] = $this->input->ip_address();
    	$this->load->view('template/template_start', $data);
		$this->load->view('FB_customer/thankyou', $data);
	   	$this->session->unset_userdata('records');
	}
	
}