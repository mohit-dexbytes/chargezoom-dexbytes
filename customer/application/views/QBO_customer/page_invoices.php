<style>
   .cust_view a {
    color: #167bc4 !important;
}
</style>
<!-- Page content -->
<div id="page-content">
    <!-- eCommerce Orders Header -->
        <?php echo $this->session->flashdata('message');   ?>
    <!-- END eCommerce Orders Header -->
	<div class="row">
	<div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-money"></i>
              </div>
               <h4 class="widget-content-light"><strong>Paid</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo $in_details->Paid; ?></strong></span>
             
            </div>
          </div>
        </div>
        
		
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-credit-card"></i>
              </div>
               <h4 class="widget-content-light"><strong>Scheduled</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo  $in_details->schedule; ?></strong></span>
             
            </div>
          </div>
        </div>
        
        
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-clock-o"></i>
              </div>
               <h4 class="widget-content-light"><strong>Total</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo  $in_details->incount; ?></strong></span>
             
            </div> 
          </div>
        </div>
        
        
       <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-thumbs-o-down"></i>
              </div>
               <h4 class="widget-content-light"><strong>Failed</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo  $in_details->Failed; ?></strong></span>
              
            </div>
          </div>
        </div>
        
	</div>
    <!-- END Quick Stats -->




    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Invoices</strong> </h2>
             <?php if($this->session->userdata('logged_in')){ ?>
              <div class="block-options pull-right">
                   
                       <a href="<?php echo base_url(); ?>QBO_controllers/Create_invoice/add_invoice" class="btn btn-sm  btn-success" title="Create New"  >Add New</a>
                     
                </div>
                <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-right">Invoice</th>
                    <th class="hidden-xs text-right">Due Date</th>
                    <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
				
					
				   <td class="text-right cust_view"> <a href="<?php echo base_url();?>QBO_controllers/Create_invoice/invoice_details_page/<?php  echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
				
					<td class="text-right hidden-xs"> <?php echo $invoice['DueDate']; ?></td>
					
					
					 <?php if($invoice['Total_payment']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_qbo_payment_data('<?php  echo $invoice['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format(($invoice['Total_payment']),2); ?></a></td>
						   <?php }
						   else{ ?>
							<td class="hidden-xs text-right cust_view"><a href="#"><?php  echo  '$'.number_format(($invoice['Total_payment']),2); ?></a></td>   
						   <?php } ?>
					
					
					
					<td class="text-right hidden-xs">$<?php echo ($invoice['BalanceRemaining'])?$invoice['BalanceRemaining']:'0.00'; ?></td>
					
					<td class="text-right hidden-xs"> 
                        <?php if($invoice['DueDate'] < date('Y-m-d') && $invoice['BalanceRemaining'] != 0){ ?>
                            <span>Overdue</span>
                        <?php }else if($invoice['BalanceRemaining'] == 0){ ?>
                            <span>Paid</span>
                        <?php }else if($invoice['BalanceRemaining'] == $invoice['Total_payment']){ ?>
                             <span>Open</span>
                        <?php }else  if($invoice['DueDate'] > date('Y-m-d') && $invoice['BalanceRemaining'] != 0){ ?>
                         <span>Open</span>
                        <?php } ?>
                        </td>
					
					<td class="text-center">
					
                            <?php if($invoice['BalanceRemaining'] >0){ ?>
						     <a href="#qbo_invoice_process"  data-backdrop="static" data-keyboard="false" class="btn btn-sm btn-success"
                             data-toggle="modal" onclick="set_qbo_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>','<?php echo $invoice['BalanceRemaining']; ?>');">Process</a>
                           
                             <?php }else{  ?>
                             <a href="javascript:void(0);" class="btn btn-sm btn-default"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" >Processed</a> 
							   <?php }  ?>

					</td>

				</tr>
				
				<?php 
				  }
			   }
			   else { echo'<tr><td colspan="7"> No Records Found </td></tr>'; }  
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->
<div id="qbo_invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>QBO_controllers/Payments/pay_invoice' class="form-horizontal card_form" >
                     
                 
                    
                                                <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                                                foreach($gateway_datas as $gateway_data){
                                                                ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                                        <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
                                                
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
                                                        
                                                </div>
                                            </div>
                                             <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div"></div>
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
                 
                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">  
                         
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [5] }
                ],
                order: [[1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">



window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }

function get_invoice_id(inv_id){
    $('#invID').val(inv_id);
}

function set_invoice_schedule_date_id(id, ind_date){
    $('#scheduleID').val(id);
    $('#schedule_date').val('');

    var nowDate = new Date(ind_date);
   
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
     
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}     

function set_sub_status_id(sID,amount){
	
	  $('#subscID').val(sID);
	  $('#inv_amount1').val(amount);

    
}
   $("#payment_date").datepicker({ 
              format: 'dd-mm-yyyy',
              autoclose: true
            });  
            
            
            
    function create_card_data()
    {
       
      
       if($('#CardID').val()=='new1')
       {
        $('.card_div').html('');
        $('.card_div').css('display','block');
        $('.card_div').html(card_daata);
       }else{
          $('.card_div').html('');
       }
    }
    
    
    
	function set_qbo_invoice_process_id(id, cid, in_val)
{
       
       
	     $('#invoiceProcessID').val(id);
	    
         $('#thest_pay #qbo_check').remove();

	     $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbd_pay'
			}).appendTo('#thest_pay');

      $('#thest_pay #inv_amount').val(in_val);
		 
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>QBO_controllers/Payments/check_qbo_vault",
				
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
 
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
	
	            

		 }
		 
		
var card_daata='<fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="" autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="" autocomplete="off" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder=""></div></div></fieldset>';



	
	
</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
