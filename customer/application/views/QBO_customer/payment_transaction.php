<!-- Page content -->
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Payments</strong> </h2>
        </div>
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="pay_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Customer Name</th>
                    <th class="text-right">Invoice</th>
                     <th class="text-right hidden-xs">Amount</th>
					 
                    
                    <th class="text-right ">Date </th>
                     <th class="text-right hidden-xs">Type</th>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th> 
                   
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
				
					<td class="test-left"><?php echo $transaction['fullName']; ?></a></td>
				
					<td class="text-right "><?php echo ($transaction['invoiceID'])?$transaction['invoiceID']:'--'; ?></td>
					<td class="hidden-xs text-right">$<?php echo ($transaction['transactionAmount'])?$transaction['transactionAmount']:'0.00'; ?></td>
					
				
					<td class=" text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right">
						   <?php if (strpos($transaction['transactionType'], 'sale') !== false) { 
                            echo "Sale";
                        }else if(strpos($transaction['transactionType'], 'auth') !== false){
                             echo "Authorization";
                         }else if(strpos($transaction['transactionType'], 'capture') !== false){
                             echo "Authorization";
                         }
                     else if(strpos($transaction['transactionType'], 'refund') !== false){
                             echo "Refund";
                         }
                         else if(strpos($transaction['transactionType'], 'void') !== false){
                             echo "Void";
                         }
                         else if(strpos($transaction['transactionType'], 'credit') !== false){
                             echo "Credit";
                         }
                         else if(strpos($transaction['transactionType'], 'Offline Payment') !== false){
                             echo "Offline Payment";
                         }
                         ?>
						
						</td>
						<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
				
					
					<td class="text-center hidden-xs">
					    <?php if($transaction['transactionCode']=='100'|| $transaction['transactionCode']=='1' || $transaction['transactionCode']=='200' || $transaction['transactionCode']=='111'){ ?> <span class="btn btn-sm  btn-success">Success</span>
					 <?php }else{  ?> <span style="width:67px;"class="btn btn-sm  btn-danger">Failed</span> <?php } ?></td>
					
					   </td> 
                    
				</tr>
				
				<?php } }
					else { echo'<tr><td colspan="7"> No Records Found </td></tr>'; }  
				 ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

</div>



 <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>QBO_controllers/PaypalPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund Now" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							    <input type="hidden" id="txnstrID" name="txnstrID" class="form-control"  value="" />
                                <input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control"  value="" />
								 <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Refund Now"  />
                            <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
    
 <div id="payment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Delete Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_formpay" method="post" action='<?php echo base_url(); ?>QBO_controllers/AuthPayment/delete_qbo_transaction' class="form-horizontal" >
                        <p id="message_data">Do you really want to delete this payment?</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                              
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit"  name="btn_cancel" class="btn btn-sm btn-success" value="Delete Now"  />
                            <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#pay_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [6] }
                ],
                order: [[ 4, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

	function set_refund_pay(txnid, txntype){
    	
			
    		if(txnid !=""){
			 
				if(txntype=='1'){
    		    $('#txnID').val(txnid);	 
				 var url   = "<?php echo base_url()?>QBO_controllers/Payments/create_customer_refund";
				}	
			else if(txntype=='2'){
    		    $('#txnIDrefund').val(txnid);
				var url   = "<?php echo base_url()?>QBO_controllers/AuthPayment/create_customer_refund";
			}	
			 else if(txntype=='3'){
				$('#paytxnID').val(txnid);
				var url   = "<?php echo base_url()?>QBO_controllers/PaytracePayment/create_customer_refund";
			 }
			 else if(txntype=='4'){
				$('#txnpaypalID').val(txnid);
				var url   = "<?php echo base_url()?>QBO_controllers/PaypalPayment/create_customer_refund";
			 }
			 else if(txntype=='5'){
			    $('#txnstrID').val(txnid);
				var url   = "<?php echo base_url()?>QBO_controllers/StripePayment/create_customer_refund";
			 }
			 
			 $("#data_form").attr("action",url);	
    		}
    	}  



   function set_transaction_pay(t_id)
   {
       	 var form = $("#data_formpay");
                                     $('<input>', {
											'type': 'hidden',
											'id'  : 'paytxnID',
											'name': 'paytxnID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'paytxnID',
											'name': 'paytxnID',
											'value': t_id,
											}).appendTo(form);
							
       
  
   }


</script>


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	


<!-- END Page Content -->