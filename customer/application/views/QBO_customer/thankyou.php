<!DOCTYPE html>
<html lang="en" >

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  
  
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css'>
    
    <style>
         .jumbotron.thank_page {  margin: 0 auto;
    position: relative;
    top: 178px;
          }
  </style>
  
</head>

<body>
<style>
label{
  font-size: 13px;
    color: #38485b;
    font-weight: 800;
    margin-bottom: 7px;
}
.row{
    padding-bottom: 10px !important;
}
.first-row{
    padding-bottom: 0px !important;
}
.success{
    color: #3FBF3F;
}
.faild{
    color: #cf4436;
}
a{
    color: #167bc4;
}
hr{
    margin: 10px;
}
p {
    margin-bottom: 0px !important;
    color: #000;
}
.transaction-print-btn{
    font-size: 13px !important;
}
@media print {
   #DivIdToPrint {
        font-size: 11pt;     
       font-family: Consolas;
       padding: 0px;
       margin: 0px;
    }
}
.display-3{
  text-align: center;
    font-weight: 200;
}
.container{
    background: #ffffff;
    width: 100%;
}
</style>
<?php 

    $transactionAmount = isset($transactionAmount) && ($transactionAmount != null)?number_format($transactionAmount, 2):number_format(0,2);
    $surchargeAmount = isset($surchargeAmount) && ($surchargeAmount != null)?number_format($surchargeAmount, 2):number_format(0, 2);

    $totalAmount = isset($totalAmount) && ($totalAmount != null)?number_format($totalAmount, 2):number_format(0, 2);

    $isSurcharge = isset($isSurcharge) && ($isSurcharge != null)?$isSurcharge:0;
?>
<div class="container">
    <header>
          <div class="col-md-12">
            <div class="navbar-brand-centered" style="padding: 20px 0px;">
               <?php if(!empty($customer_portal_data['ProfileImage'])){?>
                    <img src="<?php echo LOGOURL.$customer_portal_data['ProfileImage'];  ?>" class="img-responsive center-block" />
               <?php }else{ ?>
                    <img src="<?php echo  CZLOGO; ?>" class="img-responsive center-block"  />
               <?php } ?>
            </div>
        </div>
        <span class = "customer_base_url" id="<?php echo base_url(); ?>"></span>
        
     </header>
</div>
<div id="page-content">

    <!-- Products Block -->
        <div class="block thank-block">
        <!-- Products Title -->
            
            <div class="row first-row">
                <div class="col-md-6">
                    <h5><strong style="font-weight: 600;">Transaction Receipt</strong></h5>
                </div>
               
            </div>
            <hr>
            <div class="row">
                <div class="col-md-6">
                    <?php if($this->session->flashdata('success')){
                        ?><h5><strong class="success">Transaction Successful</strong></h5>
                    <?php }else{ 
                         $message = strip_tags($this->session->flashdata('message'));
                         ?>
 
                         <h5><strong class="faild"></strong></h5>
                 <?php   } ?>
                    
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-6">
                    <label class="amount_label">Amount: </label><span class="amount_label"> $<?php echo $transactionAmount; ?></span>
                </div>
                <div class="col-md-6">
                </div>
            </div> 
            <div class="row"> 
                <div class="col-md-6">
                    <?php
                        $datetime = (isset($transactionDetail['transactionDate']) && !empty($transactionDetail['transactionDate'])) ? $transactionDetail['transactionDate'] : date("Y-m-d H:i:s");
                        if(isset($merchant_default_timezone) && !empty($merchant_default_timezone)){
                            $timezone = ['time' => $datetime, 'current_format' => 'UTC', 'new_format' => $merchant_default_timezone];
                            $datetime = getTimeBySelectedTimezone($timezone);
                        }
                    ?>
                    <label>Date: </label> <span><?php echo date("m/d/Y h:i A", strtotime($datetime));?></span>
                </div>
                <div class="col-md-6">
                    <label>Transaction ID:  </label> <span><?php echo $transaction_id;?> </span>
                </div>
            </div>
            <div class="row"> 
                <div class="col-md-6">
                    <label>IP Address: </label><span> <?php echo $ip; ?> </span>
                </div>
                <div class="col-md-6">
                    <label>Invoice(s): </label> <span><?php echo $invoice_data['refNumber'];?></span>
                </div>
            </div>
            <div class="row"> 
                <?php
                    $payment_type = '';
                    $custom_data = (isset($transactionDetail) && !empty($transactionDetail)) ? $transactionDetail['custom_data_fields'] : false;
                    if($custom_data){
                        $json_data = json_decode($custom_data, 1);
                        if(isset($json_data['payment_type'])){
                            $payment_type = $json_data['payment_type'];
                        }
                    }
                ?>  
                <div class="col-md-6">
                    <label>Payment Type: </label><span> <?php echo $payment_type; ?></span>
                </div>
                <div class="col-md-6">
                </div>
            </div>
            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6">
                    <label>Surcharge Amount: </label><span> $<?php echo $surchargeAmount; ?></span>
                </div>
            </div>
            <?php }
            if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
            <div class="row"> 
                <div class="col-md-6">
                    
                </div>
                <div class="col-md-6">
                    <label>Total Amount: </label><span> $<?php echo $totalAmount; ?></span>
                </div>
            </div>    
            <?php } ?>
            
        <?php  
            $BillingAdd = 0;
            $ShippingAdd = 0;

            $isAdd = 0;
            if($invoice_data['BillAddress_Addr1']  || $invoice_data['BillAddress_Addr2'] || $invoice_data['BillAddress_City'] || $invoice_data['BillAddress_State'] || $invoice_data['BillAddress_PostalCode'] || $invoice_data['BillAddress_Country']){
                $BillingAdd = 1;
                $isAdd = 1;
            }
            if($invoice_data['ship_address1']  || $invoice_data['ship_address2'] || $invoice_data['ship_city'] || $invoice_data['ship_state'] || $invoice_data['ship_zipcode'] || $invoice_data['ship_country'] ){
                $ShippingAdd = 1;
                $isAdd = 1;
            }

        ?>      
        <!-- Addresses -->
        <div class="row">
        <?php if($isAdd){ ?>
            <?php if($BillingAdd){ ?>
            <div class="col-sm-6">
                <!-- Billing Address Block -->
                <div class="block">
                    <!-- Billing Address Title -->
                    <div class="block-title">
                        <h2>Billing Address</h2>
                    </div>
                    <h5><span>
                            <?php if ($invoice_data['firstName'] != '') {
                                echo $invoice_data['firstName'] . ' ' . $invoice_data['lastName'];
                            } else {
                                echo $invoice_data['fullName'];
                            }
                            ?>
                        </span></h5>
                    <address>
                        

                        <?php if ($invoice_data['BillAddress_Addr1'] != '') {
                            echo $invoice_data['BillAddress_Addr1'].'<br>'; } ?> 
                            
                            <?php if ($invoice_data['BillAddress_Addr2'] != '') {
                              echo $invoice_data['BillAddress_Addr2'].'<br>'; } else{
                                  echo '';
                              } ?>
                        
                        <?php echo ($invoice_data['BillAddress_City']) ? $invoice_data['BillAddress_City'] . ',' : ''; ?>
                        <?php echo ($invoice_data['BillAddress_State']) ? $invoice_data['BillAddress_State'] : ''; ?>
                        <?php echo ($invoice_data['BillAddress_PostalCode']) ? $invoice_data['BillAddress_PostalCode'].'<br>' : ''; ?>
                        <?php echo ($invoice_data['BillAddress_Country']) ? $invoice_data['BillAddress_Country'].'<br>' : ''; ?>
                        <br>

                    </address>
                    <!-- END Billing Address Content -->
                </div>
                <!-- END Billing Address Block -->
            </div>
            <?php }
            if($ShippingAdd){ ?>
            <div class="col-sm-6">
                <!-- Shipping Address Block -->
                <div class="block">
                    <!-- Shipping Address Title -->
                    <div class="block-title">
                        <h2>Shipping Address</h2>
                    </div>
                    <!-- END Shipping Address Title -->

                    <h5><span> <?php if ($invoice_data['firstName'] != '') {
                                        echo $invoice_data['firstName'] . ' ' . $invoice_data['lastName'];
                                    } else {
                                        echo $invoice_data['fullName'];
                                    }
                                    ?></span></h5>
                    <address>
                        <?php if ($invoice_data['ship_address1'] != '') {
                            echo $invoice_data['ship_address1'].'<br>'; } ?> 
                            
                            <?php if ($invoice_data['ship_address2'] != '') {
                              echo $invoice_data['ship_address2'].'<br>'; } else{
                                  echo '';
                              } ?>
                      


                        <?php echo ($invoice_data['ship_city']) ? $invoice_data['ship_city'] . ',' : ''; ?>
                        <?php echo ($invoice_data['ship_state']) ? $invoice_data['ship_state'] : ''; ?>
                        <?php echo ($invoice_data['ship_zipcode']) ? $invoice_data['ship_zipcode'].'<br>' : ''; ?> <br>
                        <?php echo ($invoice_data['ship_country']) ? $invoice_data['ship_country'].'<br>' : ''; ?> 
                        <br>

                        
                    </address>
                    <!-- END Shipping Address Content -->
                </div>
                <!-- END Shipping Address Block -->
            </div>
            <?php } ?>
        <?php } ?>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="<?php echo base_url('General_controller/printInvoiceTransactionReceiptPDF/1') ?>" class="btn btn-primary transaction-print-btn" target="_blank">Print Receipt</a>
            </div>
        </div>
        <div id='DivIdToPrint' style="display:none;">
            <p>Transaction Receipt</p>
            <br>
            <p>Transaction Status: <?php if($transactionCode == 200 || $transactionCode == 100 || $transactionCode == 1 || $transactionCode == 111 || $transactionCode == 120){
                        ?>Successful
                <?php }else{ ?>
                    Decline
             <?php   } ?></p>
            <p>Amount: $<?php echo $transactionAmount;?></p>

            <?php if(isset($transactionType) && ($isSurcharge) && $transactionType == 10){ ?>
                <p>Surcharge Amount: $<?php echo  $surchargeAmount;?></p>
                <p>Total Amount: $<?php echo  $totalAmount;?></p>
            <?php } ?>
            <p>Date: <?php echo date("m/d/Y h:i A", strtotime($datetime));?></p>
            <p>Transaction ID: <?php echo $transaction_id;?></p>
            <p>IP Address:  <?php echo $ip; ?> </p>
            
            <p>Invoice(s): <?php echo $invoice_data['refNumber'];?></p>
            <br>
            <?php if($BillingAdd){ ?>
                <p>Billing Address</p>
                <p><?php if ($invoice_data['firstName'] != '') {
                    echo $invoice_data['firstName'] . ' ' . $invoice_data['lastName'];
                } else {
                    echo $invoice_data['fullName'];
                }
                ?></p>
                <p> <?php if ($invoice_data['BillAddress_Addr1'] != '') {
                    echo $invoice_data['BillAddress_Addr1']; } ?> 
                </p>
                <p><?php echo ($invoice_data['BillAddress_City']) ? $invoice_data['BillAddress_City'] . ',' : ''; ?>
                <?php echo ($invoice_data['BillAddress_State']) ? $invoice_data['BillAddress_State'] : ''; ?>
                <?php echo ($invoice_data['BillAddress_PostalCode']) ? $invoice_data['BillAddress_PostalCode'] : ''; ?> </p>
                <p><?php echo ($invoice_data['BillAddress_Country']) ? $invoice_data['BillAddress_Country'] : ''; ?> </p>
                <p>
                
                <br>
            <?php } 
            if($ShippingAdd){ ?>
                <p>Shipping Address</p>
                <p><?php if ($invoice_data['firstName'] != '') {
                    echo $invoice_data['firstName'] . ' ' . $invoice_data['lastName'];
                } else {
                    echo $invoice_data['fullName'];
                }
                ?></p>
                <p> <?php if ($invoice_data['ship_address1'] != '') {
                    echo $invoice_data['ship_address1']; } ?> 
                </p>
                <p><?php echo ($invoice_data['ship_city']) ? $invoice_data['ship_city'] . ',' : ''; ?>
                <?php echo ($invoice_data['ship_state']) ? $invoice_data['ship_state'] : ''; ?>
                <?php echo ($invoice_data['ship_zipcode']) ? $invoice_data['ship_zipcode'] : ''; ?> </p>
                <p><?php echo ($invoice_data['ship_country']) ? $invoice_data['ship_country'] : ''; ?> </p>
                <p>
            <?php } ?>
        </div>
        <!-- END Addresses -->
        <div class="myblock3 col-md-12" style="font-size: 12px;padding: 0;">            
            <p style="margin-top: 23px;" class="text-right">Powered by <a target="_blank" href="<?php if(!empty($mr_config['webURL'])){ echo $mr_config['webURL']; } else{ echo'#';}  ?>"><?php if(!empty($mr_config['resellerCompanyName'])){ echo $mr_config['resellerCompanyName']; }  ?></a> | <a target="_blank" href="https://<?php echo getenv('RSDOMAIN').'/terms'; ?>">Terms of Service</a> | <a target="_blank" href="https://<?php echo  getenv('RSDOMAIN').'/privacy-policy'; ?>">Privacy Policy</a> </p>
        </div>

    <script src="<?php echo base_url(JS); ?>/pages/customer_details_company.js"></script>
<script>
    
        function printDiv() 
            {

                document.getElementById("DivIdToPrint").style.fontFamily = 'Consolas';
                
                var printContents = document.getElementById('DivIdToPrint').innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;

            }
    
</script>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>

  

</body>
<script type="text/javascript">
$(document).ready(function() {
   window.setTimeout("fadeMyDiv();", 2000); //call fade in 10 seconds
 })

function fadeMyDiv() {
   $(".flashdata").fadeOut('slow');
}
</script>

</html>