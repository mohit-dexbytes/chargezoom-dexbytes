
<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong> Customer</strong> Transactions</h2>
        </div>
		
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right hidden-xs hidden-sm">Transaction ID</th>

                    <th class="text-right">Invoice</th>
                     <th class="text-right ">Amounts</th>
					 
                    
                    <th class="text-right hidden-xs">Date</th>
                     <th class="text-right hidden-xs">Type</th>
                    
                    <th class=" text-center">Status</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				if(isset($transactions) && $transactions)
				{
					foreach($transactions as $transaction)
					{
				?>
				<tr>
					<td class="text-right hidden-xs hidden-sm"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
					
				
					<td class="text-right "><?php echo $transaction['RefNumber']; ?></td>
					<td class=" text-right"><strong></strong>$<?php echo number_format($transaction['transactionAmount'],2); ?></strong></td>
					
				
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
						<td class="hidden-xs text-right"><?php echo $transaction['transactionType']; ?></td>
					<td class="text-center"><?php 
					 if($transaction['transactionCode']=='100'|| $transaction['transactionCode']=='1' || $transaction['transactionCode']=='200'|| $transaction['transactionCode']=='111' ){ ?> <span class="btn btn-sm  btn-success">Success</span>
					 <?php }else{  ?> <span style="width:67px;"class="btn btn-sm  btn-danger">Failed</span> <?php } ?></td>
					
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>

</div>
<!-- END Page Content -->