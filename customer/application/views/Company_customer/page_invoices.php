<style>
   .cust_view a {
    color: #167bc4 !important;
}
</style>

<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- eCommerce Orders Header -->
    
    <!-- END eCommerce Orders Header -->

    <!-- Quick Stats -->
  
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Invoices</h2>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right" >Invoice</th>
                    
                    <th class="hidden-xs text-right">Due Date</th>
                      
                     <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php      if(!empty($invoices)){

						foreach($invoices as $invoice){
						     $lable='';
						          $statusnew='';
							          
					
						   if($invoice['status']=='Open' && $invoice['userStatus']!='cancel' ){
						   $invoice['status']='Open';
							    $lable ="info";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							     $invoice['status']='Paid';
						   }else  if($invoice['status']=='Failed' && $invoice['userStatus']==''){
							    $lable ="danger";
						   } else  if(strtoupper($invoice['status'])==strtoupper('Cancel')  ){
						 			 
								    $lable ="primary";
						   }else  if($invoice['userStatus']!=''){
						           $invoice['userStatus']='Canceled';
								    $lable ="primary";
						   }
						    
						    
			   ?>
			
				<tr>
					  <td class="text-right cust_view"><a href="<?php echo base_url();?>company/home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                             <?php if((-$invoice['AppliedAmount'])!="0.00"){ ?>
                            <td class="hidden-xs text-right">$<a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo number_format((-$invoice['AppliedAmount']),2); ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right">$<?php echo number_format( (-$invoice['AppliedAmount']),2); ?></td>   
						   <?php } ?>
            	
					<td class="text-right hidden-xs"><strong>$<?php echo $invoice['BalanceRemaining']; ?></strong></td>
					<td class="text-right">
					<?php  echo $invoice['status']; ?></td>
					
					<td class="text-center">
						<div class="btn-group">
						    <?php if($invoice['DueDate'] > date('Y-m-d') && $invoice['userStatus']!='cancel' && $invoice['IsPaid']=='false' || $invoice['status']=='Past Due'){ ?>
						        <a href="#invoice_process" style="width: 82px;" class="btn btn-sm btn-success" onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>', '<?php  echo $invoice['Customer_ListID']; ?>', '<?php  echo $invoice['BalanceRemaining']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
							   <?php } 
							   else  if($invoice['DueDate'] > date('Y-m-d') && strtoupper($invoice['status'])==strtoupper('Cancel') ) 
							   { ?>
							  	<a href="javascript:void(0);" class="btn btn-sm btn-primary">Canceled</a>
							    <?php 
							    }
							    else if( $invoice['BalanceRemaining']=='0.00' and $invoice['userStatus']!='cancel' && $invoice['IsPaid']=='true'  ){ 
							    ?>
								<a href="javascript:void(0);" class="btn btn-sm btn-primary"   data-backdrop="static" data-keyboard="false" data-toggle="modal">Processed</a>
						    	<?php 	
						    	}else{  
						    	echo ""; 
						    	} ?>
							    	
							    	
						</div>
					</td>
				</tr>
				
				<?php 
				  }
			   }	
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->




</div>


<div id="invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
               
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="thest_pay" method="post" action="<?php echo base_url().'company/Payments/pay_invoice'; ?>" class="form-horizontal card_form" >
                     
                 
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway"  class="form-control">
                                                        <option value="" >Select Gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas['gateway'] as $gateway_data){
																?>
                                                            <option value="<?php echo $gateway_data['gatewayID'];  ?>" <?php if($gateway_data['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway_data['gatewayFriendlyName']; ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
                                                
                                     <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div" style="display:none;">
                           
                               
                       </div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                             <input type="hidden" id="customerProcessID" name="customerProcessID" class="form-control"  value="<?php echo $this->uri->segment('4'); ?>" />
                        </div>
                    </div>
                    
					
			       
                    <div class="pull-right">
        			 <input type="submit" id="qbd_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script>

        $(function(){     
			Pagination_view1.init();
        });
</script>
<script>
        
$(function(){     
		
					
	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	


 function set_url(){  
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
			
					url : "<?php echo base_url(); ?>company/home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				
				              data = $.parseJSON(response);
							 gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = base_url+'company/Payments/pay_invoice';
							  } if(gtype=='2'){
									var url   = base_url+'company/Payments/pay_invoice';
							 }if(gtype=='1'){
							   var url   = base_url+'company/Payments/pay_invoice';
							 } if(gtype=='4'){
							   var url   = base_url+'company/Payments/pay_invoice';
							 } if(gtype=='5'){
							   var url   = base_url+'company/Payments/pay_invoice';
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  
							 
							 if(gtype=='6'){
							   var url   = base_url+'company/Payments/pay_invoice';
							 } if(gtype=='7'){
							   var url   = base_url+'company/Payments/pay_invoice';
							 }
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }



	function set_invoice_process_id(id, cid, in_val)
{
       
       
	     $('#invoiceProcessID').val(id);
	    
         $('#thest_pay #qbo_check').remove();

	     $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbd_pay'
			}).appendTo('#thest_pay');

      $('#thest_pay #inv_amount').val(in_val);
		 
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>company/Payments/check_vault",
				
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
 
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
	
	            

		 }
		 
		
var card_daata='<fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="" autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="" autocomplete="off" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder=""></div></div></fieldset>';



	
function create_card_data()
{
   
  
   if($('#CardID').val()=='new1')
   {
    $('.card_div').html('');
    $('.card_div').css('display','block');
    $('.card_div').html(card_daata);
   }else{
      $('.card_div').html('');
   }
}
	

 function set_card_user_data(id, name){
      
	   $('#customerID11').val(id);
 }
     

 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();


	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		  $('#editccform').css('display','none');
		 $('#editcheckingform').css('display','none');
		if(cardID!=""){
			
			$('#edit_cardID').val(cardID);
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>company/Payments/get_card_edit_data",
				data : {'cardID':cardID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					         if(data['card']['CardNo']!="" || data['card']['CardCVV']!="")
                                {
                                    $('#editccform').css('display','block');
    							    $('#edit_card_number').val(data['card']['CardNo']);
    							  	
    								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
    							  	document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
    								 $('#m_card_id').show();  
                                     $('#edit_card_number').attr('disabled',false);
                                    $('#edit_card_number').attr('type','hidden');
                                     $('#m_card').html(data['card']['CardNo']);
    							
    								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
    								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
                                }
                                else
                                {
                                       $('#editcheckingform').css('display','block');
        							    $('#edit_acc_number').val(data['card']['accountNumber']);
        							  	
        								document.getElementById("edit_acc_name").value =data['card']['accountName'];
        								
        								document.getElementById("edit_route_number").value =data['card']['routeNumber'];
        						
        								$('select[name="edit_secCode"]').find('option[value="'+data['card']['secCodeEntryMethod']+'"]').attr("selected",true);
        								$('select[name="edit_acct_type"]').find('option[value="'+data['card']['accountType']+'"]').attr("selected",true);
        								$('select[name="edit_acct_holder_type"]').find('option[value="'+data['card']['accountHolderType']+'"]').attr("selected",true);
                                    
                                }
						        $('#baddress1').val(data['card']['Billing_Addr1']);
							    $('#baddress2').val(data['card']['Billing_Addr2']);
							    $('#bcity').val(data['card']['Billing_City']);
							    $('#bstate').val(data['card']['Billing_State']);
							    $('#bcountry').val(data['card']['Billing_Country']);
							    $('#bcontact').val(data['card']['Billing_Contact']);
							    $('#bzipcode').val(data['card']['Billing_Zipcode']);
					
				}
				
				
			});
			
		}	  
		   
	}
	 


</script>
