
<!-- Page content -->
<div id="page-content">
    
<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Card</strong> Information</h2>
        </div>
	
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
      <table id="ecom-orders" class="table table-bordered table-striped table-vcenter ">
            <thead>
                <tr>
                   
                    <th class="text-left">Friendly Name</th>
                     <th class="text-right">Card Number</th>
                    <th class="text-right hidden-xs">Expiring</th>
                    <th class="hidden-xs text-right">Added On</th>
                   
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report6))
				{
					foreach($report6 as $invoice)
					{
				?>
				<tr>
					
					<td class=" text-left"><?php echo $invoice['customerCardfriendlyName']; ?></td>
                    <td class="text-right"><?php echo ($invoice['CardNo'])?$invoice['CardNo']:''; ?></td>
					<td class="text-right hidden-xs"><?php echo date('F, Y', strtotime($invoice['expired_date'])); ?></td>
					<td class="hidden-xs text-right"><?php echo $invoice['createdAt']; ?></td>
					
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ EcomOrders.init(); });</script>

</div>
<!-- END Page Content -->