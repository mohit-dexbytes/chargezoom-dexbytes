
<!-- Page content -->
<div id="page-content">
    <!-- eCommerce Orders Header -->
        <?php echo $this->session->flashdata('message');   ?>
    <!-- END eCommerce Orders Header -->
	<div class="row">
	<div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-money"></i>
              </div>
               <h4 class="widget-content-light"><strong>Paid</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php  echo $inv_datas->Paid; ?></strong></span>
             
            </div>
          </div>
        </div>
        
		
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-credit-card"></i>
              </div>
               <h4 class="widget-content-light"><strong>Scheduled</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php  echo $inv_datas->schedule; ?></strong></span>
               
            </div>
          </div>
        </div>
        
        
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-clock-o"></i>
              </div>
               <h4 class="widget-content-light"><strong>Total</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1">
              <span class="float-left"><strong><?php  echo $inv_datas->incount; ?></strong></span>
               
            </div>
          </div>
        </div>
        
        
       <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-thumbs-o-down"></i>
              </div>
               <h4 class="widget-content-light"><strong>Failed</strong> Invoices</h4>
            </div>
            <div class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php  echo $inv_datas->Failed; ?></strong></span>
             
            </div>
          </div>
        </div>
        
	</div>
    <!-- END Quick Stats -->



    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Invoices</strong> </h2>
             <?php if($this->session->userdata('logged_in')){ ?>
              <div class="block-options pull-right">
                   
                       <a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_invoice/add_invoice" class="btn btn-sm  btn-success" title="Create New"  >Add New</a>
                     
                </div>
                <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left visible-lg">Customer Name</th>
                    <th class="text-right">Invoice</th>
                    <th class="hidden-xs text-right">Due Date</th>
                    <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
					<td class="text-left visible-lg cust_view"> <?php echo ($invoice['CustomerFullName'])?($invoice['CustomerFullName']):'-----'; ?></td>
					<td class="text-right cust_view"> <a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
					<td class="text-right hidden-xs"> <?php echo $invoice['DueDate']; ?></td>
				
					
					<?php if($invoice['Total_payment']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_fb_payment_data('<?php  echo $invoice['invoiceID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format(($invoice['AppliedAmount']),2); ?></a></td>
						   <?php }
						   else{ ?>
							<td class="hidden-xs text-right cust_view"><a href="#"><?php  echo  '$'.number_format(($invoice['AppliedAmount']),2); ?></a></td>   
						   <?php } ?>
					
					<td class="text-right hidden-xs">$<?php echo ($invoice['BalanceRemaining'])?$invoice['BalanceRemaining']:'0.00'; ?></td>
					<td class="text-right hidden-xs"> 
                       <?php echo $invoice['UserStatus']; ?></td>
                    </td>
						<td class="text-center">
						<div class="">
                            <?php if($invoice['BalanceRemaining'] == 0){ ?>
						          <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">   
						          <li>    <a href="javascript:void(0);" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Refund</a> </li>
						          <li>  <a href="#qbo_invoice_delete" onclick="get_invoice_id('<?php  echo $invoice['invoiceID']; ?>')" data-keyboard="false" data-toggle="modal" class="">Delete</a></li>
						          </ul>
						          </div>
							 <?php }else{ ?>
							 
							   <div class="btn-group dropbtn">
                                 <a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
                              <li> <a href="#fb_invoice_process" class=""  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_fb_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>', '<?php echo $invoice['BalanceRemaining']; ?>'); ">Process</a></li>
                             <li><a href="#qbo_invoice_schedule" onclick="set_invoice_schedule_date_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" class=""  data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a></li>
                                    
                            <li><a href="#set_subs" class=""  onclick="set_sub_status_id('<?php  echo $invoice['invoiceID']; ?>','<?php echo $invoice['BalanceRemaining']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add Payment</a> </li>
                             
                           
                           </ul>
                           </div>
                             <?php } ?>
                             
                             
                                 
 	
							    	
						</div>
					</td>

			

				</tr>
				
				<?php 
				  }
			   }
			   
			   else { echo'<tr><td colspan="7"> No Records Found </td></tr>'; }  
			   
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->
<div id="fb_invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Payments/pay_invoice' class="form-horizontal card_form" >
                     
                 
                    
                                    <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_fb_url();"  class="form-control">
                                                        <option value="" >Select gateway</option>
                                                        <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                                                foreach($gateway_datas as $gateway_data){
                                                                ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                                        <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
                                                
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
                                                        
                                                </div>
                                            </div>
                                             <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div"></div>
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
                    
                    <div class="pull-right">
                     <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
   
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="qbo_invoice_schedule" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Schedule Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc-form_schedule" method="post" action="<?php echo base_url();?>FreshBooks_controllers/Freshbooks_invoice/invoice_schedule" class="form-horizontal">
                     
                 
                    <p>Do you really want to schedule this invoice? Please schedule date to automatically process payment.</p> 
                    
                    <div class="form-group">
                     <label class="col-md-4 control-label" for="card_list">Schedule Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date">
                            <input type="text" id="schedule_date" name="schedule_date" class="form-control input-date " data-date-format="mm-dd-yyyy" placeholder="" value="">
                        </div>
                      </div>    
                    </div>
                    
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="scheduleID" name="scheduleID" class="form-control" value="1-1501156111">
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Schedule">
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br>
                    <br>
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<div id="qbo_invoice_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="Qbwc-form" method="post" action='<?php echo base_url()?>FreshBooks_controllers/Freshbooks_invoice/delete_invoice' class="form-horizontal" >
                     
                 
                    <p>Do you really want to delete this Invoice?</p> 
                    
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invID" name="invID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
             
                    <div class="pull-right">
                     <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete Now"  />
                    <button type="button" class="btn btn-sm btn-success close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="set_subs" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Mark Invoice as Paid</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ccjkck" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/SettingSubscription/status' class="form-horizontal" >
                     
                 
					 <div class="form-group">
                     <label class="col-md-4 control-label">Date</label>
                        <div class="col-md-8">
                      <div class="input-group input-date">
                            <input type="text" id="payment_date" name="payment_date" class="form-control input-date " data-date-format="yyyy-mm-dd" placeholder="" value="">
                        </div>
                      </div>    
                    </div>
                    
                     <div class="form-group">
                     <label class="col-md-4 control-label">Check Number</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="check_number" placeholder="" size="40" name="check_number" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                    <div class="form-group">
                     <label class="col-md-4 control-label" >Amount</label>
                        <div class="col-md-8">
                      <div class="input-group">
                            <input type="text" id="inv_amount" placeholder="" size="40" name="inv_amount" class="form-control">
                        </div>
                      </div>    
                    </div>
                    
                  
                    
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="subscID" name="subscID" class="form-control"  value="" />
							<input type="hidden" id="status" name="status" class="form-control"  value="Paid" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  class="btn btn-sm btn-success" value="Add Payment"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>PaypalPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Refund Now" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
							    <input type="hidden" id="txnstrID" name="txnstrID" class="form-control"  value="" />
                                <input type="hidden" id="txnpaypalID" name="txnpaypalID" class="form-control"  value="" />
								 <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
								 <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
								 <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Refund Now"  />
                            <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
    
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [6] }
                ],
                order: [[1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<script type="text/javascript">

function set_sub_status_id(sID,amount){
	
	  $('#subscID').val(sID);
	 $('#inv_amount').val(amount);
}


window.setTimeout("fadeMyDiv();", 2000);
    function fadeMyDiv() {
        $(".msg_data").fadeOut('slow');
     }

function get_invoice_id(inv_id){
    $('#invID').val(inv_id);
}

function set_invoice_schedule_date_id(id, ind_date){
    $('#scheduleID').val(id);
    $('#schedule_date').val('');

    var nowDate = new Date(ind_date);
   
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
     
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}      
</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
