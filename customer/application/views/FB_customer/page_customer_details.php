<!-- Page content -->
<div id="page-content">
    <!-- Customer Content -->
    <div class="row">
        <div class="col-lg-4">
            <!-- Customer Info Block -->
            <div class="block">
                <!-- Customer Info Title -->
                <div class="block-title">
                    <h2><i class="fa fa-file-o"></i> <strong>Customer Details</strong> </h2>
                </div>
                <!-- END Customer Info Title --> 
                <!-- Customer Info -->
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                        <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" alt="avatar" class="img-circle">
                    </a>
                    <h3>
                        <strong><?php echo $customer->fullName; ?></strong>
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                        
                        <tr>
                            <td class="text-right"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->firstName.' '.$customer->lastName; ?></td>
                        </tr>

                        <tr>
                            <td class="text-right"><strong>Added On</strong></td>
                            <td><?php echo date('M d, Y - h:m', strtotime($customer->updatedAt)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Last Visit</strong></td>
                            <td><?php echo date('M d, Y - h:m', strtotime($customer->updatedAt)); ?></td>
                        </tr>
                       <?php if($this->session->userdata('logged_in')|| in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])  ){ ?> 
                        <tr>
                            <td class="text-right"><strong>Email Address</strong></td>
                           
                            <td class="cust_view"><a href="#set_tempemail_freshbook" onclick="set_template_data('<?php echo $customer->Customer_ListID; ?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->userEmail; ?>')" title="Sending mail" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $customer->userEmail; ?></a></td>
                        </tr>
                             <?php } ?>
                        <tr>
                            <td class="text-right"><strong>Phone Number</strong></td>
                            <td><?php echo $customer->phoneNumber; ?></td>
                        </tr>
                        <tr>
                             <td class="text-right"><strong>Credit Card Info</strong></td>
                            
                            <td> <a href="#card_data_process" class="btn btn-sm btn-success"  onclick="set_card_user_data('<?php  echo $customer->Customer_ListID; ?>', '<?php  echo $customer->fullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
                            
                             <a href="#card_edit_data_process" class="btn btn-sm btn-info"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
                            
                            </td>
                        </tr>

                        <tr>
                            <td class="text-right"><strong>Status</strong></td>
                            <td>Active</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Customer Info -->
            </div>
            
            <!-- Quick Stats Block -->
            <div class="block">
                <!-- Quick Stats Title -->
                <div class="block-title">
                    <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
                </div>
                <!-- END Quick Stats Title -->

                <!-- Quick Stats Content -->
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background">
                            <i class="gi gi-list"></i>
                        </div>
                        <h4 class="text-left">
                            <strong><?php echo $invoices_count; ?></strong><br><small>Invoices in Total</small>
                        </h4>
                    </div>
                </a>
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background-success">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h4 class="text-left text-success">
                            <strong>$ <?php echo number_format($sum_invoice,2); ?></strong><br><small>Invoice Value</small>
                        </h4>
                    </div>
                </a>
                <!-- END Quick Stats Content -->
            </div>
            <!-- END Quick Stats Block -->
            
            
            
        </div>
        
        
        
        <div class="col-lg-8">
            <!-- Orders Block -->
            <div class="block">
                <!-- Orders Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                    
                        <span class="text" style="color:#787878;"><strong>$<?php echo number_format($pay_remaining,2); ?></strong></span>
                    </div>
                      
                    <h2><strong>Outstanding</strong> Invoices</h2>
                </div>
            
                <!-- END Orders Title -->

                <!-- Orders Content -->
                <table id="compamount" class="table  compamount table-bordered table-striped table-vcenter">
                    
                        <thead>
                            <tr>
                            <th class="text-center" >Invoice</th>
                            <th class="hidden-xs text-right">Due Date</th>
                          
                              <th class="text-right">Amount</th>
                               <th class="text-right hidden-xs">Status</th>
                            <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php   if(!empty($invoices)){    
                            foreach($invoices as $invoice){
                        
                          
                         ?>
                        <tr>
                          
                           
                           	<td class="text-right cust_view"> <a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
                            <td class="hidden-xs text-right"><?php  echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                          
                             <td class="text-right"><strong>$<?php echo ($invoice['BalanceRemaining'])?$invoice['BalanceRemaining']:'0.00'; ?></td>
                        
                           <td class="text-right"><?php if($invoice['BalanceRemaining'] == 0){ ?>
                           <span>Paid</span>
                        <?php } 
                        
                        else{ ?>
                        <span>open</span>
                        <?php } ?>
                        </td>
                             
                             
                             
                            <td class="text-center">                  
                            <?php if($invoice['BalanceRemaining'] == 0){ ?>
                                   <a class="btn btn-sm btn-success" disabled>Process</a>
                             <?php }
                             else{ ?>
                                     <a href="#fb_invoice_process" class="btn btn-sm btn-success"  data-backdrop="static" data-keyboard="false"
                             data-toggle="modal" onclick="set_fb_invoice_process_id('<?php  echo $invoice['invoiceID']; ?>','<?php  echo
                             $invoice['CustomerListID']; ?>','<?php echo $invoice['BalanceRemaining']; ?>');">Process</a>
                             <?php } ?>
                                 
                            </td>
                        </tr>
                           <?php  } }	?>    
                        

                        
                    </tbody>
                </table>
                <br>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                        
                        <span class="text" style="color:#787878;"><strong>$<?php echo number_format($pay_invoice,2); ?></strong></span>
                    
                    
                    </div>
                    <h2> <strong>Paid</strong> Invoices</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table  class="table compamount table-bordered table-striped ecom-orders table-vcenter">
                 
                  <thead>
                 
                        <th class="text-center"><strong>Invoice</strong></th>
                       
                        <th class="text-right hidden-xs">Invoice Amount</th>
                            <th class="text-right hidden-xs">Payments</th>
                              <th class="text-right">Balance</th>
                            <th class="text-right">Status</th>
                      
                </thead>
                    <tbody>
                      
                        
                        <?php   if(!empty($latest_invoice)){   

                       
                        foreach($latest_invoice as $invoice){
                          
                         ?>
                        <tr>
                           	<td class="text-right cust_view"> <a href="<?php echo base_url(); ?>FreshBooks_controllers/Freshbooks_invoice/invoice_details_page/<?php echo $invoice['invoiceID']; ?>"><?php echo $invoice['refNumber']; ?></a></td>
                           
                           <td class="hidden-xs text-right">$<?php echo number_format($invoice['Total_payment'],2); ?></td>
                            <td class="hidden-xs text-right"><strong>$<?php  echo  number_format($invoice['AppliedAmount'],2); ?></strong></td>   
                           
                             <td class="text-right"><strong>$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></strong></td>
                            <td class="text-right hidden-xs"> <?php echo $invoice['UserStatus']; ?></td></td>
                            
                        </tr>
                           <?php } }
												
						?>  
                        
                    
                        
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->


            
            

            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                 
                    <h2> <strong>Subscriptions</strong> </h2>
                </div>
                
                
        <!-- All Orders Content -->
        <table id="sub_page" class="table  table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    
                    
                    <th class="text-left hidden-xs">Gateway</th>
                    <th class="text-right ">Amount</th>
                    <th class="text-right hidden-xs">Next Charge</th>
                   
                </tr>
            </thead>
             <tbody>
             <?php   if(!empty($getsubscriptions)){   

                        
                        foreach($getsubscriptions as $getsubscription){ 
                          
                         ?>
            
                   <tr>
                    
                  
                    
                   
                    
                    <td class="text-left hidden-xs"><?php echo $getsubscription['gatewayFriendlyName']; ?>  </td> 
                    
                    
                    
                    <td class="text-right ">$<?php echo ($getsubscription['subscriptionAmount'])?$getsubscription['subscriptionAmount']:'0.00'; ?></td>
                   
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>
                    
                
                </tr>
                
                <?php  } } 
			
				?>
                
            </tbody>
            
                </table>
                <br>
                
            </div>
<!-----------------  END --------------------->
            
            <!-- Customer Addresses Block -->
            <div class="block">
                <!-- Customer Addresses Title -->
                <div class="block-title">
                    <h2><strong>Addresses</strong></h2>
                </div>
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    
                    <div class="col-lg-6">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title">
                                <h2>Billing Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->
                           <?php if(isset($customer->ShipAddress_Addr1) && $customer->ShipAddress_Addr1 != ""){ ?>    
                            <!-- Shipping Address Content -->
                            <h4><strong><?php echo $customer->fullName; ?></strong></h4>
                          <address>
                                <strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                 <?php echo ($customer->ShipAddress_Addr2)?$customer->ShipAddress_Addr2.', ':''; ?> <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo ($customer->ShipAddress_State)?$customer->ShipAddress_State.', ':''; ?> <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->phoneNumber; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><?php echo $customer->userEmail; ?></a>
                            </address>
                            <?php } ?>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                    
                    <div class="col-lg-6">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title">
                                <h2>Shipping Address</h2>
                            </div>
                            <!-- END Billing Address Title -->
                            <?php if(isset($customer->ShipAddress_Addr1) && $customer->ShipAddress_Addr1 != ""){ ?> 
                            <!-- Billing Address Content -->
                            <h4><strong><?php echo $customer->fullName; ?></strong></h4>
                            <address>
                                <strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo ($customer->ShipAddress_Addr2)?$customer->ShipAddress_Addr2.', ':''; ?> <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo ($customer->ShipAddress_State)?$customer->ShipAddress_State.', ':''; ?> <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->phoneNumber; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)" ><?php echo $customer->userEmail; ?> </a>
                            </address>
                             <?php } ?>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <!-- END Customer Addresses Block -->
          
        </div>
    </div>
    <!-- END Customer Content -->
</div>
<!-- END Page Content -->
<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit/Delete  Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
        
                
                     
                 
     <table  class="table table-bordered table-striped table-vcenter">
     
         <?php
            if(!empty($card_data_array)){
             foreach($card_data_array as $cardarray){ ?>
           
                    <tr>
                    <td class="text-left hidden-xs"><?php echo $cardarray['customerCardfriendlyName'];  ?></td>
                    <td class="text-right visible-lg"><?php echo $cardarray['CardNo'];  ?></td>
                    <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
                            <a href="#" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('<?php echo $cardarray['CardID'] ; ?>');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>
                            <a href="<?php echo base_url(); ?>FreshBooks_controllers/Transactions/delete_card_data/<?php echo $cardarray['CardID'] ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>
                          
                        </div> </td>
                </tr>  
            
        <?php } 
        
            }else{ ?>   
         <tr>
                    <td colspan="3">No Card Available</td>
               
                </tr> 
        
        <?php } ?>
       </table> 

        <form id="thest_form" method="post" style="display:none;" action='<?php echo base_url(); ?>FreshBooks_controllers/Transactions/update_card_data' class="form-horizontal" >
                 <fieldset>
                    
                       <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                                 <div id="m_card_id"><span id="m_card"></span> <input type='button' id="btn_mask" class="btn btn-default btn-sm" value="Edit Card" /></div>
                                <input type="hidden" disabled  id="edit_card_number" name="edit_card_number" class="form-control" placeholder="">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                    <select id="edit_expiry" name="edit_expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
                                                        <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
                                                        <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
                                                
                                                    <label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                    <select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
                                                    <?php 
                                                        $cruy = date('y');
                                                        $dyear = $cruy+15;
                                                    for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                                    <?php } ?>
                                                       </select>
                                                </div>
                                                
                                                
                                            </div>     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" placeholder="" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="edit_friendlyname">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_friendlyname" name="edit_friendlyname" class="form-control" placeholder="" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress1" name="baddress1" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress2" name="baddress2" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcity" name="bcity" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bstate" name="bstate" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bzipcode" name="bzipcode" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcountry" name="bcountry" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Contact Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcontact" name="bcontact" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                    </fieldset>
                                          <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-success" value="Edit"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                </form>     
     <!-- panel-group -->
                
                
                    
                <div id="can_div">
                 <div class="pull-right">
                    
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                 </div>
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------Show Payment Data------------------->


<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
            
        
                
                 <form id="thest" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Transactions/insert_new_data' class="form-horizontal" >
                 <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                        <div class="col-md-8">
                               <input type="text" id="customername" name="customername" class="form-control" value="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="card_number" name="card_number" class="form-control" placeholder="">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                    <select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
                                                        <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
                                                        <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
                                                
                                                    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                    <select id="expiry_year" name="expiry_year" class="form-control">
                                                    <?php 
                                                        $cruy = date('y');
                                                        $dyear = $cruy+15;
                                                    for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                                    <?php } ?>
                                                       </select>
                                                </div>
                                                
                                                
                                            </div>     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" placeholder="" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                    
                     <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address1" name="address1" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address2" name="address2" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="city" name="city" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="state" name="state" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="country" name="country" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Contact Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="contact" name="contact" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                    </fieldset>
                    
                     <input type="hidden" id="customerID11" name="customerID11"  value="" />
                  <div class="pull-right">
                     <input type="submit" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                </form>     
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>


<!------------------  View email history Popup popup ------------------>

<div id="view_history" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> View Details </h2>
                
                <div class="modal-body">
        
            <div id="data_history" style="height:740px; min-height:740px;  overflow: auto; ">
            </div>  
                 
            </div>
            
    </div>

               <div class="modal-footer">
                 <button type="button"  align="right" class="btn btn-sm pull-right btn-danger close1" data-dismiss="modal"> Cancel </button>
                 
            </div>
            
                    
            </div>
            
            
    </div>
            
            <!-- END Modal Body -->
        </div>
  

<div id="fb_invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>FreshBooks_controllers/Transactions/pay_invoice' class="form-horizontal card_form" >
                     
                 
                    
                                    <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_fb_url();"  class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($gateway_datas) && !empty($gateway_datas) ){
                                                                foreach($gateway_datas as $gateway_data){
                                                                ?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                                         <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
                                                
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
                                                        
                                                </div>
                                            </div>
                                             <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div"></div>
                    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
                    
                    
                    <div class="pull-right">
                     <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">
   
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 
   <script>


 function get_invoice_id(inv_id){
    $('#invID').val(inv_id);
}       
        
 function set_view_history(eID){
    if(eID!=""){
        
        
     $.ajax({
    url: '<?php echo base_url("FreshBooks_controllers/Settingmail/get_history_id")?>',
    type: 'POST',
    data:{customertempID:eID},
    success: function(data){
        
        $('#data_history').html(data);
              
             
             
    }   
}); 
    
}

}



        
        
$(function()
{    
  $('#btn_mask').click(function(){
    
  $('#m_card_id').hide();  
  $('#edit_card_number').removeAttr('disabled');
  $('#edit_card_number').attr('type','text');
});
       
        nmiValidation.init(); nmiValidation1.init();
        
        Pagination_view1.init();
      
        Pagination_view2.init();
       
                                 
         Pagination_email.init();    
       
        
          /*********************Template Methods**************/ 

    
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
    



var Pagination_email = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#email_hist').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
                    
                     expiry_year: {
                              CCExp: {
                                    month: '#expiry',
                                    year: '#expiry_year'
                              }
                        },
                    
                        
                     cvv: {
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    
                    friendlyname:{
						required:true,
						 minlength: 3,
					},
                    address1:{
						required:true,
					},
					
                  	country:{
						required:true,
					},
					state:{
						required:true,
					},
					city:{
						required:true,
					},
					zipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},
					contact:{
						required:true,
						minlength:10,
						maxlength:12,
						digits:true,
					},
                  
                  
                },
              
            });
            
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
            
            

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
                        minlength: 13,
                        maxlength: 16,
                        number: true
                    },
                    
                     edit_expiry_year: {
                              CCExp: {
                                    month: '#edit_expiry',
                                    year: '#edit_expiry_year'
                              }
                        },
                    
                        
                     edit_cvv: {
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 4,
                    },
                    
                    baddress1:{
						required:true,
					},
					
					edit_friendlyname:{
						required:true,
						 minlength: 3,
					},
				
                  	bcountry:{
						required:true,
					},
					bstate:{
						required:true,
					},
					bcity:{
						required:true,
					},
					bzipcode:{
						required:true,
						minlength:5,
						maxlength:6,
						digits:true,
					},
					bcontact:{
						required:true,
						minlength:10,
						maxlength:12,
						digits:true,
					},
                  
                  
                },
              
            });
            
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
            
            

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

    
    
 function set_card_user_data(id, name){
      
       $('#customerID11').val(id);
       $('#customername').val(name);
 }
         
    function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();       
         
        if(cardID!=""){
            
            $.ajax({
                type:"POST",
                url : "<?php echo base_url(); ?>Payments/get_card_edit_data",
                data : {'cardID':cardID},
                success : function(response){
                    
                         data=$.parseJSON(response);
                        
                           if(data['status']=='success'){
                             
                                $('#edit_cardID').val(data['card']['CardID']);
                                $('#edit_card_number').val(data['card']['CardNo']);
                                 $('#m_card').html(data['card']['CardNo']);
                                 $('#m_card_id').show();  
                                  $('#edit_card_number').attr('disabled','disabled');
                                  $('#edit_card_number').attr('type','hidden');	
                                
                                document.getElementById("edit_cvv").value =data['card']['CardCVV'];
                                
                                document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
                            
                                $('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
                                $('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
                            
                             
                                $('#baddress1').val(data['card']['Billing_Addr1']);
							    $('#baddress2').val(data['card']['Billing_Addr2']);
							    $('#bcity').val(data['card']['Billing_City']);
							    $('#bstate').val(data['card']['Billing_State']);
							    $('#bcountry').val(data['card']['Billing_Country']);
							    $('#bcontact').val(data['card']['Billing_Contact']);
							    $('#bzipcode').val(data['card']['Billing_Zipcode']);
                            
                       }       
                    
                }
                
                
            });
            
        }     
           
    }   
    
    
var Pagination_view1 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0, 1] },
                    { orderable: false, targets: [3] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
    
    
var Pagination_view2 = function() { 
    
    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#sub_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [2] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

    
</script>
<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>




