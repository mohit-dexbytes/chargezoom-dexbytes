<?php
   /**
    * template_start.php
    *
    * Author: pixelcave
    *
    * The first block of code used in every page of the template
    *
    */
   ?>
<!DOCTYPE html>
<!--[if IE 9]>         
<html class="no-js lt-ie10" lang="en">
   <![endif]-->
   <!--[if gt IE 9]><!--> 
   <html class="no-js" lang="en">
      <!--<![endif]-->
      <head>
         <meta charset="utf-8">
         <title><?php echo $templates['title'] ?></title>
         <meta name="description" content="<?php echo $templates['description'] ?>">
         <meta name="author" content="<?php echo $templates['author'] ?>">
         <meta name="robots" content="<?php echo $templates['robots'] ?>">
         <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
         <!-- Icons -->
         <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
         <link rel="shortcut icon" href="<?php echo base_url(IMAGES); ?>/favicon.png">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon57.png" sizes="57x57">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon72.png" sizes="72x72">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon76.png" sizes="76x76">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon114.png" sizes="114x114">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon120.png" sizes="120x120">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon144.png" sizes="144x144">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon152.png" sizes="152x152">
         <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon180.png" sizes="180x180">
         <!-- END Icons -->
         <!-- Stylesheets -->
         <!-- Bootstrap is included in its original form, unaltered -->
         <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
         <!-- Related styles of various icon packs and plugins -->
         <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/plugins.css">
         <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
         <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">
         <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
         <?php if ($templates['theme']) { ?>
         <link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $templates['theme']; ?>.css">
         <?php } ?>
         <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
         <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
         <!-- END Stylesheets -->
         <!-- Modernizr (browser feature detection library) -->
        
         <script src="<?php echo base_url(JS); ?>/vendor/jquery-1.11.3.min.js"></script>
         <script src="<?php echo base_url(JS); ?>/vendor/bootstrap.min.js"></script>
         <script src="<?php echo base_url(JS); ?>/plugins.js"></script>
         <script src="<?php echo base_url(JS); ?>/app.js"></script>
         <script src="<?php echo base_url(JS); ?>/custom.js"></script>
         <script src="<?php echo base_url(JS); ?>/pages/formsWizard.js"></script>
         <script src="<?php echo base_url(JS); ?>/vendor/modernizr-respond.min.js"></script>
      </head>
      <body>
         <?php
            /**
             * page_head.php
             *
             * Author: Ecrubit
             *
             * Header and Sidebar of each page
             *
             */
            ?>
         <!-- Page Wrapper -->
         <!-- In the PHP version you can set the following options from inc/config file -->
         <!--
            Available classes:
            
            'page-loading'      enables page preloader
            -->
         <div id="page-wrapper"<?php if ($templates['page_preloader']) { echo ' class="page-loading"'; } ?>>
            <!-- Preloader -->
            <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
            <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
            <div class="preloader themed-background">
               <h1 class="push-top-bottom text-light text-center"><strong>ChargeZoom</strong></h1>
               <div class="inner">
                  <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
                  <div class="preloader-spinner hidden-lt-ie10"></div>
               </div>
            </div>
            <!-- END Preloader -->
            <!-- Page Container -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
               Available #page-container classes:
               
               '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)
               
               'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
               'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
               'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
               'sidebar-mini sidebar-visible-lg-mini'          for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
               'sidebar-mini sidebar-visible-lg'               for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
               
               'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
               'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
               'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
               
               'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)
               
               'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
               
               'style-alt'                                     for an alternative main style (without it: the default style)
               'footer-fixed'                                  for a fixed footer (without it: a static footer)
               
               'disable-menu-autoscroll'                       add this to disable the main menu auto scrolling when opening a submenu
               
               'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
               'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar
               
               'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links
               -->
            <?php
               $page_classes = '';
               
               if ($templates['header'] == 'navbar-fixed-top') {
                   $page_classes = 'header-fixed-top';
               } else if ($templates['header'] == 'navbar-fixed-bottom') {
                   $page_classes = 'header-fixed-bottom';
               }
               
               if ($templates['sidebar']) {
                   $page_classes .= (($page_classes == '') ? '' : ' ') . $templates['sidebar'];
               }
               
               if ($templates['main_style'] == 'style-alt')  {
                   $page_classes .= (($page_classes == '') ? '' : ' ') . 'style-alt';
               }
               
               if ($templates['footer'] == 'footer-fixed')  {
                   $page_classes .= (($page_classes == '') ? '' : ' ') . 'footer-fixed';
               }
               
               if (!$templates['menu_scroll'])  {
                   $page_classes .= (($page_classes == '') ? '' : ' ') . 'disable-menu-autoscroll';
               }
               
               if ($templates['cookies'] === 'enable-cookies') {
                   $page_classes .= (($page_classes == '') ? '' : ' ') . 'enable-cookies';
               }
               ?>
            <div id="page-container"<?php if ($page_classes) { echo ' class="' . $page_classes . '"'; } ?>>
               <!-- Main Sidebar -->
               <div id="sidebar">
                  <!-- Wrapper for scrolling functionality -->
                  <div id="sidebar-scroll">
                     <!-- Sidebar Content -->
                     <div class="sidebar-content">
                        <!-- Brand -->
                        <a href="index.php" class="sidebar-brand">
                        <img src="<?php echo base_url(IMAGES); ?>/logo.png" alt="avatar">
                        </a>
                        <!-- END Brand -->
                        <!-- User Info -->
                        <div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide">
                           <div class="sidebar-user-avatar">
                              <a href="#">
                              <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar1.jpg" alt="avatar">
                              </a>
                           </div>
                           <div class="sidebar-user-name">Matt Dubois</div>
                           <div class="sidebar-user-links">
                              <a href="#" data-toggle="tooltip" data-placement="bottom" title="My Profile"><i class="gi gi-user"></i></a>
                              <a href="#" data-toggle="tooltip" data-placement="bottom" title="Notifications"><i class="gi gi-envelope"></i></a>
                              <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.php in PHP version) -->
                              <a href="javascript:void(0)" class="enable-tooltip" data-placement="bottom" title="Settings" onclick="$('#modal-user-settings').modal('show');"><i class="gi gi-cogwheel"></i></a>
                              <a href="<?php echo base_url('logout'); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit"></i></a>
                           </div>
                        </div>
                        <!-- END User Info -->
                        <?php if ($primary_nav) { ?>
                        <!-- Sidebar Navigation -->
                        <ul class="sidebar-nav">
                           <li>
                              <a href="#"><i class="gi gi-home sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
                                                         </li>
                                                          <li>
                              <a href="#" class="sidebar-nav-menu"><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-users sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Reseller</span></a>
                                                            <ul>
                                                                  <li>
                                    <a href="<?php echo base_url();?>admin">Create Reseller</a>
                                                                     </li>
                                                                  <li>
                                    <a href="<?php echo base_url();?>admin/reseller_list">Reseller List</a>
                                                                     </li>
                                                                 
                                                                  
                                                               </ul>
                                                         </li>
                        </ul>
                        <!-- END Sidebar Navigation -->
                        <?php } ?>
                     </div>
                     <!-- END Sidebar Content -->
                  </div>
                  <!-- END Wrapper for scrolling functionality -->
               </div>
               <!-- END Main Sidebar -->
               <!-- Main Container -->
               <div id="main-container">
                  <!-- Header -->
                  <!-- In the PHP version you can set the following options from inc/config file -->
                  <!--
                     Available header.navbar classes:
                     
                     'navbar-default'            for the default light header
                     'navbar-inverse'            for an alternative dark header
                     
                     'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                         'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added
                     
                     'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                         'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
                     -->
                  <header class="navbar<?php if ($templates['header_navbar']) { echo ' ' . $templates['header_navbar']; } ?><?php if ($templates['header']) { echo ' '. $templates['header']; } ?>">
                     <?php if ( $templates['header_content'] == 'horizontal-menu' ) { // Horizontal Menu Header Content ?>
                     <!-- END Horizontal Menu + Search -->
                     <?php } else { // Default Header Content  ?>
                     <!-- Left Header Navigation -->
                     <ul class="nav navbar-nav-custom">
                        <!-- Main Sidebar Toggle Button -->
                        <li>
                           <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                           <i class="fa fa-bars fa-fw"></i>
                           </a>
                        </li>
                        <!-- END Main Sidebar Toggle Button -->
                        <!-- Template Options -->
                        <!-- Change Options functionality can be found in js/app.js - templateOptions() -->
                        <!-- END Template Options -->
                     </ul>
                     <!-- END Left Header Navigation -->
                     <!-- Right Header Navigation -->
                     <ul class="nav navbar-nav-custom pull-right">
                        <!-- Alternative Sidebar Toggle Button -->
                        <!-- END Alternative Sidebar Toggle Button -->
                        <!-- User Dropdown -->
                        <li class="dropdown">
                           <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                           <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar1.jpg" alt="avatar"> <i class="fa fa-angle-down"></i>
                           </a>
                           <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                              <li class="dropdown-header text-center">Account</li>
                              <li>
                                 <a href="#">
                                 <i class="fa fa-envelope-o fa-fw pull-right"></i>
                                 <span class="badge pull-right">5</span>
                                 Notifications
                                 </a>
                                 <a href="#"><i class="fa fa-magnet fa-fw pull-right"></i>
                                 <span class="badge pull-right">3</span>
                                 My Subscription
                                 </a>
                                 <a href="#"><i class="fa fa-question fa-fw pull-right"></i>
                                 <span class="badge pull-right">11</span>
                                 FAQ
                                 </a>
                              </li>
                              <li class="divider"></li>
                              <li>
                                 <a href="#">
                                 <i class="fa fa-user fa-fw pull-right"></i>
                                 My Profile
                                 </a>
                                 <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.php in PHP version) -->
                                 <a href="#modal-user-settings" data-toggle="modal">
                                 <i class="fa fa-cog fa-fw pull-right"></i>
                                 Settings
                                 </a>
                              </li>
                           </ul>
                        </li>
                        <!-- END User Dropdown -->
                     </ul>
                     <!-- END Right Header Navigation -->
                     <?php } ?>
                  </header>
                  <!-- END Header -->
                  <?php echo $template['body'] ?>
                  <?php
                     /**
                      * page_footer.php
                      *
                      * Author: pixelcave
                      *
                      * The footer of each page
                      *
                      */
                     ?>
                  <!-- Footer -->
                  <footer class="clearfix">
                     <div class="pull-left">
                        2017 &copy; <a href="http://chargezoom.com" target="_blank"><?php echo $templates['name'] . ' ' . $templates['version']; ?></a>
                     </div>
                  </footer>
                  <!-- END Footer -->
               </div>
               <!-- END Main Container -->
            </div>
            <!-- END Page Container -->
         </div>
         <!-- END Page Wrapper -->
         <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
         <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
         <?php $session = $this->session->userdata('logged_in'); ?>
         <!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->
         <div id="invoice_cancel" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <!-- Modal Header -->
                  <div class="modal-header text-center">
                     <h2 class="modal-title">Cancel Invoice</h2>
                  </div>
                  <!-- END Modal Header -->
                  <!-- Modal Body -->
                  <div class="modal-body">
                     <form id="Qbwc-form" method="post" action='<?php echo base_url(); ?>home/update_customer_invoice_cancel' class="form-horizontal" >
                        <p>Do you really want to cancel this invoice?</p>
                        <div class="form-group">
                           <div class="col-md-8">
                              <input type="hidden" id="invoiceID" name="invoiceID" class="form-control"  value="" />
                           </div>
                        </div>
                        <div class="pull-right">
                           <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                           <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">No</button>
                        </div>
                        <br />
                        <br />
                     </form>
                  </div>
                  <!-- END Modal Body -->
               </div>
            </div>
         </div>
         <div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <!-- Modal Header -->
                  <div class="modal-header text-center">
                     <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
                  </div>
                  <!-- END Modal Header -->
                  <!-- Modal Body -->
                  <div class="modal-body">
                     <form action="#" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" onsubmit="return checkForm(this);">
                        <fieldset>
                           <div class="form-group">
                              <label class="col-md-4 control-label">Name</label>
                              <div class="col-md-8">
                                 <p class="form-control-static"><?php echo $session['firstName'].' '.$session['lastName']; ?></p>
                                 <input type="hidden" value="<?php echo $session['id'];  ?>"  name="user_id" id="user_id" />
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-4 control-label">Email</label>
                              <div class="col-md-8">
                                 <p class="form-control-static"><?php echo $session['companyEmail'];  ?></p>
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-4 control-label" for="user-settings-currentpassword">Current Password</label>
                              <div class="col-md-8">
                                 <input type="password" id="user-settings-currentpassword" name="user-settings-currentpassword" class="form-control" placeholder="">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                              <div class="col-md-8">
                                 <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="">
                              </div>
                           </div>
                           <div class="form-group">
                              <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                              <div class="col-md-8">
                                 <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="">
                              </div>
                           </div>
                        </fieldset>
                        <div class="form-group form-actions">
                           <div class="col-xs-12 text-right">
                              <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                              <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
                           </div>
                        </div>
                     </form>
                  </div>
                  <!-- END Modal Body -->
               </div>
            </div>
         </div>
         <div id="qb-connecter-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <!-- Modal Header -->
                  <div class="modal-header text-center">
                     <h2 class="modal-title"><i class="fa fa-download"></i>Quickbooks Webconnector File</h2>
                     <p>You can create a webconnector file to get Quickbooks data.</p>
                  </div>
                  <!-- END Modal Header -->
                  <!-- Modal Body -->
                  <div class="modal-body">
                     <form id="Qbwc-form" method="post" action='<?php echo base_url(); ?>QuickBooks/config' class="form-horizontal" >
                        <div class="form-group">
                           <label class="col-md-4 control-label">Company Name</label>
                           <div class="col-md-8">
                              <p class="form-control-static"><?php echo $session['companyName'];  ?></p>
                           </div>
                        </div>
                       
                        <div class="form-group">
                           <label class="col-md-4 control-label" for="user-settings-password">Quickbook Password</label>
                           <div class="col-md-8">
                              <input type="password" id="qbpassword" name="qbpassword" class="form-control" placeholder="">
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-4 control-label" for="user-settings-repassword">Quickbook Confirm Password</label>
                           <div class="col-md-8">
                              <input type="password" id="qbrepassword" name="qbrepassword" class="form-control" placeholder="">
                           </div>
                        </div>
                        <div class="form-group form-actions">
                           <div class="col-xs-12 text-center">
                              <input type="submit" id="qb-generate" name="qb-generate" class="btn btn-sm btn-warning" value="Generate Connector file"  />
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="modal-footer text-right">
                     <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Close</button>
                  </div>
                  <!-- END Modal Body -->
               </div>
            </div>
         </div>
         <div id="qb-connecter-info" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
               <div class="modal-content">
                  <!-- Modal Header -->
                  <div class="modal-header text-center">
                     <h2 class="modal-title"><i class="fa fa-download"></i>Quickbooks Webconnector File</h2>
                     <p>You can import your Quickbooks data using  webconnector file .</p>
                  </div>
                  <!-- END Modal Header -->
                  <!-- Modal Body -->
                  <div class="modal-body">
                     <form id="" method="post"  class="form-horizontal" >
                        <div class="form-group">
                           <label class="col-md-4 control-label">1</label>
                           <div class="col-md-8">
                              <p class="form-control-static">Install Quickbook web connector on you machine where Quickbook installed. </p>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-4 control-label">2</label>
                           <div class="col-md-8">
                              <p class="form-control-static">Configure your web connector going to : Start->Quickbooks->Quickbook Webconnector. </p>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-4 control-label">2</label>
                           <div class="col-md-8">
                              <p class="form-control-static">Add you .qwc downloaded file using Add Application button in Quickbook Webconnector UI. </p>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-4 control-label">2</label>
                           <div class="col-md-8">
                              <p class="form-control-static">set the password that you have given in Quickbook Webconnector file to generate. </p>
                           </div>
                        </div>
                        <div class="form-group">
                           <label class="col-md-4 control-label">2</label>
                           <div class="col-md-8">
                              <p class="form-control-static">Click on update button to import data from Quickbooks </p>
                           </div>
                        </div>
                     </form>
                  </div>
                  <div class="modal-footer text-right">
                     <button type="button" class="btn btn-sm btn-default " data-dismiss="modal">Close</button>
                  </div>
                  <!-- END Modal Body -->
               </div>
            </div>
         </div>
         <!-- END User Settings -->
         <script type="text/javascript">
            function checkForm(form)
            {
            var curr_pass = $('#user-settings-currentpassword').val();
            var new_pass = $('#user-settings-password').val();
            var confirm_pass = $('#user-settings-repassword').val();
            
            if(curr_pass == '') {
            alert("Please enter current password!");
                  $('#user-settings-currentpassword').focus();
                  return false;
            }
            else if(new_pass == '') {
            alert("Please enter new password!");
                  $('#user-settings-password').focus();
                  return false;
            }
              else if( new_pass != "" && new_pass == confirm_pass ) {
                if(new_pass.length < 5){
                  alert("Password must contain at least Five characters!");
                  $('#user-settings-password').focus();
                  return false;
                }
              } 
            else {
                alert("Confirm Password dismatched.!");
                $('#user-settings-repassword').focus();
                return false;
              } 
              return true;
            }
            
         </script>
         <?php
            /**
             * template_end.php
             *
             * Author: pixelcave
             *
             * The last block of code used in every page of the template
             *
             * We put it in a separate file for consistency. The reason we
             * separated template_scripts.php and template_end.php is for enabling us
             * put between them extra javascript code needed only in specific pages
             *
             */
            ?><script>$(function(){ FormsWizard.init(); });</script>
      </body>
   </html>