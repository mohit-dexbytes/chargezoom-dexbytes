<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>


<style>
.btn-success.btn-alt1 {
    background-color: #ffffff;
    color: #7db831;
}

.btn-danger.btn-alt1{
	  background-color: #ffffff;
    color: #e74c3c;
	
}

.btn-info.btn-alt1{
	  background-color: #ffffff;
    color: #3498db;
	
}

.btn-warning.btn-alt1{
	  background-color: #ffffff;
    color: #f39c12;
	
}


</style>
  <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>  
  
    $(function(){
    FormsWizard.init(); });
	$(function(){
        setTimeout(function() {
          jQuery('.alert').fadeOut('fast');
        }, 2000); // <-- time in milliseconds
    });
  
  
  $(document).ready(function(){
$('#CardID').change(function(){
			
		var cardlID =  $(this).val();
		
		
		  if(cardlID!='' && cardlID !='new1' ){
			  
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_data",
				data : {'cardID':cardlID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
						 $('#thest_pay1 #number').remove();	
                         $('#thest_pay1 #exp_year').remove();	
                         $('#thest_pay1 #exp_month').remove();	
                         $('#thest_pay1 #cvc').remove();
						
					    if(data['status']=='success'){
						 var form = $("#thest_pay");	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);

									// Prevent the form from submitting with the default action
									
					        }	   
					
				}
				
				
			});
		  }
		
	});	
	
	
	
     
});
  


</script>  
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<?php $session = $this->session->userdata('customer_logged_in'); ?>
<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->


<?php if(isset($page_num) && $page_num=='customer_qbd'){ ?>  
<script>
    
    
  

  
function set_invoice_id(id){
	
	     $('#invoiceID').val(id);
}	      



function set_invoice_schedule_id(id, ind_date){
	$('#scheduleID').val(id);

	var nowDate = new Date(ind_date);
	
	
      var inv_day = new Date(nowDate.getFullYear(), nowDate.getMonth(), nowDate.getDate()+1, 0, 0, 0, 0); 
	 
            $("#schedule_date").datepicker({ 
              format: 'dd-mm-yyyy',
              
              startDate: inv_day,
              endDate:0,
              autoclose: true
            });  
    
}	



function set_invoice_customer_process_id(id){
	
	     $('#invoiceProcessID').val(id);
		 
		  var cid ='<?php echo $this->session->userdata('customer_logged_in')['ListID'];  ?>';
	
		 	if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_vault",
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);

					     if(data['status']=='success'){
						
                              var s=$('#CardID');
							
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		} 
	}	
	
	
 
function set_url(){  
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				 
				              data = $.parseJSON(response);
							var gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = "<?php echo base_url()?>Payments/pay_trace_invoice";
							  }else if(gtype=='2'){
									var url   = "<?php echo base_url()?>Payments/pay_auth_invoice";
							 }else if(gtype=='1'){
							   var url   = "<?php echo base_url()?>Payments/pay_invoice";
							 }else if(gtype=='4'){
							   var url   = "<?php echo base_url()?>Payments/pay_paypal_invoice";
							 }else if(gtype=='5'){
							   var url   = "<?php echo base_url()?>Payments/pay_stripe_invoice";
							   
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  			
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }	 
	
	

	


function set_payment_data(id){
	
    if(id !=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>Payments/view_transaction",
			data:{ 'invoiceID':id},
			success : function(response){
			
				$('#pay-content-data').html(response);
				
			}
			
		});
		
	}  
  
}	
 

</script>

<div id="pay_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Invoice Payment Details</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			 <table id="ecom-orders" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right" >Txn ID</th>
                     <th class="text-right hidden-xs">Amount</th>
                    <th class="text-right visible-lg">Date</th>
                     <th class="text-right hidden-xs">Type</th>
                    <th class="hidden-xs text-right">Status</th>
                   
                </tr>
            </thead>
			  <tbody id="pay-content-data">
                
			
				
			</tbody>
        </table>
                  <div class="pull-right">
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Close</button>
        			</div>
                     <br />
                    <br />
			   
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>





<div id="invoice_customer_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>Payments/pay_invoice' class="form-horizontal" >
                     
                 
					<p>Do you really want to process this invoice before due date?</p> 
					
					<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" onchange="set_url();"  class="form-control">
                                                        <option value="" >Select gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas as $gateway_data){
																?>
                                                           <option value="<?php echo $gateway_data['gatewayID'] ?>" ><?php echo $gateway_data['gatewayFriendlyName'] ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
					
			<div class="form-group ">
					  
						<label class="col-md-4 control-label" for="card_list">Select Card</label>
						<div class="col-md-6">
						
							  <select id="CardID" name="CardID"  class="form-control">
                                     <option value="" >Select Card</option>

                                                        
                                                    </select>
								
						</div>
					</div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Pay Now"  />
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">No</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<?php } ?>

<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"><i class="fa fa-pencil"></i> Settings</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="thest_form_pass" action="<?php echo base_url(); ?>home/changePass" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Name</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['FirstName'].' '.$session['LastName']; ?></p>
								<input type="hidden" value="<?php echo $session['loginID'];  ?>"  name="user_id" id="user_id" />
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $session['Contact'];  ?></p>
                            </div>
                        </div>
                     
						<div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" placeholder="">
                            </div>
                        </div>
                    </fieldset>
                    <div class="form-group form-actions">
                        <div class="col-xs-12 text-right">
                            <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-sm btn-success">Save Changes</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>








<script>


       
        $(function(){  

		
 
    $('#thest_form_pass').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'user-settings-password': {
                        required: true,
                         minlength: 6
                    },
                    'user-settings-repassword': {
                        required: true,
                        equalTo: '#user-settings-password',
						},

			
			},
    });


});
	

 
</script>

<!-- END User Settings -->


<script type="text/javascript">
  function checkForm(form)
  {
	var curr_pass = $('#user-settings-currentpassword').val();
	var new_pass = $('#user-settings-password').val();
	var confirm_pass = $('#user-settings-repassword').val();
	
	if(curr_pass == '') {
		alert("Please enter current password!");
        $('#user-settings-currentpassword').focus();
        return false;
	}
	else if(new_pass == '') {
		alert("Please enter new password!");
        $('#user-settings-password').focus();
        return false;
	}
    else if( new_pass != "" && new_pass == confirm_pass ) {
      if(new_pass.length < 5){
        alert("Password must contain at least Five characters!");
        $('#user-settings-password').focus();
        return false;
      }
    } 
	else {
      alert("Confirm Password dismatched.!");
      $('#user-settings-repassword').focus();
      return false;
    } 
	
	form.submit();
	
    return true;
  }

</script>
<script src="<?php echo base_url(JS); ?>/custom_footer.js"></script>





















<!-- my code-->

  
    <script>

   var card_daata=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="" autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+     <?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        '<option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>'+
													<?php } ?>
                                                       '</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="" autocomplete="off" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder=""></div></div></fieldset>';

  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
}  

function format2(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return "$" + p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

}
    function set_template_data(c_id,cmp_name,cmp_id,cust_Email ){
		
	
	   
           document.getElementById('customertempID').value=c_id;
		   document.getElementById('tempCompanyID').value=cmp_id;
		   document.getElementById('tempCompanyName').value=cmp_name;
		   document.getElementById('toEmail').value=cust_Email;
    }		    
        
	 var gtype ='';
	  function stripeResponseHandler_res(status, response)
	  {
	  
          
                        $("#thest_pay").find('input[name="stripeToken"]').remove();
           
                if (response.error) {
                    // Re-enable the submit button
                    
                    // Show the errors on the form
                    $('#payment_error').text(response.error.message);

                    $('#payment_error-custom').show();
                    $('#payment_error-custom').text(response.error.message);
                    setTimeout(function() {
                        $('#payment_error-custom').hide();
                    }, 2000);
                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);
                       
                    // Doing AJAX form submit to your server.

                 $('#payment_error').show();
                 $("#btn_process").attr("disabled", false);	
                }
            }



 function create_Token_stripe()
 {
   
	
        
        $('#payment_error-custom').hide();
        $('#payment_error-custom').text('');
		var pub_key = $('#stripeApiKey').val();
	
        if(pub_key!=''){
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number11').val(),
                        cvc: $('#ccv11').val(),
                        exp_month: $('#expiry11').val(),
                        exp_year: $('#expiry_year11').val()
                    }, stripeResponseHandler_res);
   
    }
        // Prevent the form from submitting with the default action
        return false;
}
$(document).on('click','.cvvMask',function(){
  
    $(this).attr('type','password');
    $(this).attr('maxlength','4');
}); 
$(document).on('click','.CCMask',function(){
    $(this).attr('type','password');
});
</script>
