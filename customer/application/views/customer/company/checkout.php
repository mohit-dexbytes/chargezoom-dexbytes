<style>
    body{background:#ffffff!important;}
    .backg{background:#f2f2f3;min-height:400px;}
    .back{background:#ffffff;width: 100%;float: left;}
    .nopadding{padding:0px;}
    .heading {padding: 8px 0 0 15px;}
    .padding{padding:15px 0px!important;}
    .myblock{width: 100%;float: left;}
    .myblock2{background:#ffffff;width: 100%;float: left;;border:1px solid #DADADA;margin-top:40px;}
    .policy{margin:3px 5px;}
    .myblock3 {float: left;width: 100%;margin: 30px 0px 50px 0px;}
   .btn-success{background:#3FBF3F!important; #3FBF3F !important;  color: #ffffff !important;}
    .error{color:red;font-size:11px;}
    .servererror{color:red;font-size:11px;width:100%;}
    .terms_block {padding-top: 15px; }
    @media (min-width:992px)
    {
        .container{width:943px}
    }
    @media (min-width:1200px) and (max-width:1400px)
    {
        .container{width:943px}
    }
	.invoice_h3{
		padding-right: 0px !important;
		padding-left: 0px !important;
	}
    .invoice_h3>h3{
      	color: #38485b !important;
	  	margin-bottom: 20px !important;
		margin-top: 20px !important;
		font-weight: 600 !important;
    }
	.form-control{
		border-color: #e8e8e8 !important;
		height: 44px !important;
		font-size: 14px !important;
	}
	label {
		font-size: 13px;
		color: #38485b;
		font-weight: 600;
		margin-bottom: 7px;
	}
	.grey
	{
		background: #F8F8F8;
		clear: both;
		display: inline-block;
		padding: 15px 0 15px 0px;
	}
	.cards
	{
		padding-top: 3.4rem;
	}
	legend {
		padding-bottom: 7px;
		position: relative;
		font-size: 16px;
		font-weight: 600;
		color: #38485b;
		margin-top: 20px;
	}
	.leg:after {
		background: #0c7bff;
	}
	legend:after {
		content: '';
		position: absolute;
		width: 150px;
		height: 3px;
		left: 0;
		top: 28px;
	}
	
  #accept{
    vertical-align: bottom !important;
    margin-top: 8px !important;
  }
   .showDiv{
    display: block;
  }
  .hideDiv{
    display: none ;
  }
</style>
<?php $customer = $records;

  if(isset($selectedGateway) && isset($selectedGateway['gatewayType']) && $selectedGateway['gatewayType'] == 10){
    $state_placeholder = 'CA';
  } else {
    $state_placeholder = 'State';
  }

?>
<div class="container">
    <header><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <div class="col-md-12">
            <div class="navbar-brand-centered" style="padding: 20px 0px;">
               <?php if(!empty($customer_portal_data['ProfileImage'])){?>
                    <img src="<?php echo LOGOURL.$customer_portal_data['ProfileImage'];  ?>" height="250" width="200" class="img-responsive center-block" />
               <?php }else{ ?>
                    <img src="<?php echo CZLOGO; ?>"  height="250" width="200" class="img-responsive center-block"  />
               <?php } ?>
            </div>
        </div>
        
     </header>
</div>
<?php 
  $totalAmount = $selected_plan->subscriptionAmount;

  if(!isset($prorata_data)){
    $prorata_data = 1;
  }

  if(!isset($prorata_heading)){
    $prorata_heading = '';
  }
?> 
<div class="container-fluid backg p-0">
    <div class="container">
      <div class="row">
        <?php  if(isset($error_msg) && !empty($error_msg)){ ?>
      <div class="msg_data">
          <?php echo $error_msg;?>
      </div>
        <?php } 
          echo		$message = $this->session->flashdata('message');
      ?>
      </div>
        <div class="row">
          
            <div class="col-md-5 invoice_h3 col-sm-5">
                <h3><?php echo $selected_plan->planName;    ?></h3>
            </div>
            <div class="col-md-2 col-sm-2"></div>
            <div class="col-md-5 invoice_h3 col-sm-5">
                <h3 class="text-right"><?php if(isset($recurring)){ echo '$'. number_format($recurring_onetime1,2);}else{ echo '$'.'0.00' ; }  ?> / 
                <?php echo  get_frequency($selected_plan->invoiceFrequency);  ?>
                </h3>
            </div>
        </div>
        
        <div class="row">
            <form method="post" id="thest_pay">
                <input type="hidden" id="isShipping"  value="<?php echo $selected_plan->isShipping; ?>">
                <input type="hidden" id="isBilling"  value="<?php echo $selected_plan->isBilling; ?>">
                <div class="col-md-12 nopadding white-bg">
                      <!--personal detail-->
                      <div class="container">
                           <div class="row padding_top_20px">
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">FIRST NAME</label>
            						<input type="text" name="first_name" id="first_name" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['fname']; } ?>" class="form-control" placeholder="">
            						<?php echo form_error('first_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">LAST NAME</label>
            						<input type="text" name="last_name" id="last_name" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['lname']; } ?>">
            						<?php echo form_error('last_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                               </div>
                              <div class="row">
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">COMPANY NAME</label>
            						<input type="text" name="company" id="company" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['company']; } ?>">
            						<?php echo form_error('company', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">EMAIL</label>
            						<input type="text" name="email" id="email" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['email']; } ?>">
            						<?php echo form_error('email', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                          </div>
                      </div>
                       
                      <!--card detail-->
                      <div class="container">
                        <div class="row nopadding">



                          <?php 
                            if($selectedGateway['creditCard'] == 1 && $selectedGateway['echeckStatus'] == 1 ){
                              $value = 1;

                            }else if($selectedGateway['creditCard'] == 1){
                              $value = 1;

                            }else if($selectedGateway['echeckStatus'] == 1){
                              $value = 2;

                            }else{
                              $value = 1;
                            }
                           ?>
                          <input type="hidden" id="scheduleID" name="scheduleID" class="form-control"  value="<?php echo $value; ?>" /> 

                          <?php 
                          $classManageCC = $classManageEC = 'hideDiv';


                          if($selectedGateway['creditCard'] == 1 && $selectedGateway['echeckStatus'] == 1 ){
                            $classManageCC = 'showDiv';

                           ?>
                          <div class="toggleIcon">
                            <div class="col-md-12">
                              <div class="col-md-6 no-padding" id="radioSection">
                                <label class="col-md-6 row text-left">
                                  <input value="1" onclick="get_process_details(this);" checked="" type="radio" name="sch_method" class="radio_pay">  Credit Card 
                                </label>
                                <label class="col-md-6 row">
                                  <input value="2" onclick="get_process_details(this);" type="radio" name="sch_method" class="radio_pay"> eCheck 
                                </label>
                              </div>
                            </div>
                          </div> 
                          <?php } ?> 
                          
                          <?php
                           


                            if($selectedGateway['creditCard'] == 1 && $selectedGateway['echeckStatus'] == 0){

                              $classManageCC = 'showDiv';
                            }
                            
                            if($selectedGateway['creditCard'] == 0 && $selectedGateway['echeckStatus'] == 1){
                              $classManageEC = 'showDiv';
                            }

                           ?>           
                          <div id="eCheckView" class="<?php echo $classManageEC; ?>">
                            <div class="col-md-12 grey col-xs-12">
                              <div class="col-md-12 no-padding">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">ROUTING NUMBER</label>
                                    <input type="text" name="routeNumber" id="routeNumber" class="form-control" >
                                    <?php echo form_error('routeNumber', '<div class="servererror">', '</div>'); ?>
                                  </div>
                                </div> 
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label">ACCOUNT NUMBER</label>
                                    <input type="text" name="accountNumber" id="accountNumber" class="form-control" >
                                    <?php echo form_error('accountNumber', '<div class="servererror">', '</div>'); ?>
                                  </div>

                                  <div class="form-group">
                                    <label class="control-label">CONFIRM ACCOUNT NUMBER</label>
                                    <input type="text" name="confirmAccountNumber" id="confirmAccountNumber" class="form-control" >
                                    <?php echo form_error('confirmAccountNumber', '<div class="servererror">', '</div>'); ?>
                                  </div>
                                
                                </div>   
                              </div> 
                              
                            </div>
                            <div class="col-md-12 pull-right">
                              <div class="form-group">
                                  <div class="pull-right">
                                       <label for="i_authorize_checkbox" style="padding-right: 10px;">I authorize <?php echo (isset($m_data['companyName']) && !empty($m_data['companyName'])) ? $m_data['companyName'] : $reseller['resellerCompanyName']; ?> to debit my account.</label>
                                       <input type="checkbox" required id="i_authorize_checkbox" name="i_authorize_checkbox">
                                      <br>
                                      <label id="i_authorize_checkbox-error" class="error" for="i_authorize_checkbox" style="display: none;float: right;"></label>
                                  </div>
                              </div>
                              <br>
                              <br>
                            </div>
                          </div>             
                          <div id="creditCardView" class="padding_right_2px <?php echo $classManageCC; ?>">
                            <div class="grey col-md-12 col-xs-12">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">CARD TYPE</label>
                                  <select name="cardType" class="form-control input-sm">
                                      <option value="">SELECT CARD TYPE</option>
                                      <option value="Visa">VISA</option>
                                      <option value="MasterCard">MASTERCARD</option>
                                      <option value="American Express">AMERICAN EXPRESS</option>
                                      <option value="Discover">DISCOVER</option>
                                  </select>

                                </div>
                              </div>
                                                          
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label">CARD NUMBER</label>
                                  <input type="text" autocomplete="off" name="cardNumber" id="card_number11" class="form-control" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['card']; } ?>" onChange="get_card_surcharge_details({cardNumber: this.value})">
                                  <?php echo form_error('cardNumber', '<div class="servererror">', '</div>'); ?>
                                 </div>
                              </div>
                                                            
                              <div class="col-md-3">
                                  <div class="form-group">
                                    <label class="control-label">EXPIRATION MONTH</label>
                                    <input type="text" name="cardExpiry" id="expiry11" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['exp1']; } ?>">
                                    <?php echo form_error('cardExpiry', '<div class="servererror">', '</div>'); ?>  
                                   </div>
                              </div>
                                                            
                              <div class="col-md-3">
                                  <div class="form-group">
                                    <label class="control-label">EXPIRATION YEAR</label>
                                    <input type="text" name="cardExpiry2" id="expiry_year11" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['exp2']; } ?>">
                                    <?php echo form_error('cardExpiry2', '<div class="servererror">', '</div>'); ?> 
                                   </div>
                              </div>
                                                            
                              <div class="col-md-2">
                                  <div class="form-group">
                                      <label class="control-label">CVV</label>
                                      <input type="text" onblur="create_Token_stripe();" id="ccv11" name="cardCVC"  class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['ccv']; } ?>" autocomplete="off">
                                      
                                       <?php echo form_error('cardCVC', '<div class="servererror">', '</div>'); ?>
                                  </div>
                              </div>
                              <div class="col-md-4 cards text-right">
                                  <img src="<?php echo base_url();?>resources/img/amex.png">
                                  <img src="<?php echo base_url();?>resources/img/visa.png" />
                                  <img src="<?php echo base_url();?>resources/img/master.png" />
                                  <img src="<?php echo base_url();?>resources/img/discover.png" />
                              </div>
                              <?php
                                if(isset($surchargePercentage) && $surchargePercentage > 0) {
                                  $totalSurchargeAmount = ($totalAmount * $surchargePercentage)/ 100;
                                  $totalSurchargeAmount = number_format($totalSurchargeAmount, 2);
                              ?>
                              <div class='col-md-12'>
                                <div class="panel panel-info notice_box" id='surchargeNotice'  style="display:none">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Surcharge Notice</h3> 
                                    </div> 
                                    <div class="panel-body" id='surchargeNoticeText'>We impose a surcharge on credit cards that are not greater than our cost of acceptance. This surcharge rate is <?php echo $surchargePercentage.'%'; ?> which would be <?php echo '$'.$totalSurchargeAmount; ?> for this transaction. This surcharge is not applied to debit card transactions and is not assessed on debit card transactions.</div>
                                </div>
                              </div>
                              <?php
                                }
                              ?>
                            </div> 
                            <div class="col-md-6">
                              <div class="form-group">
                                <label><input type="hidden" value="savepaymentinfo" id="savepaymentinfo" name="savepaymentinfo" /> </label>
                              
                              </div>
                            </div>
                          </div>
                        </div>
                  
                                  
                         
                    </div>
                            
                  
                    
                    <div class="container">
                           <div class="row nopadding">          
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">STREET ADDRESS</label>
            						<input type="text" name="address" id="address" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['add1']; } ?>">
            						<?php echo form_error('address', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label class="control-label">ADDRESS 2</label>
            						<input type="text" name="address2" id="address2" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['add2']; } ?>">
            					 </div>
                              </div>
                              </div>
                              <div class="row">
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">CITY</label>
            						<input type="text" name="city" id="city" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['city']; } ?>">
            						<?php echo form_error('city', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">STATE</label>
            						<input type="text" name="state" id="state" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['state']; } ?>">
            						<?php echo form_error('state', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                             
                              
                              
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="control-label">ZIP/POSTAL</label>
            						<input type="text" name="zip" id="zip" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['zip']; } ?>">
            						<?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                                 <div class="col-md-3">
                                          <div class="form-group">
                    						<label class="control-label">COUNTRY</label>
                    						<input type="text" name="country" id="country" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['country']; } ?>" >
                    						<?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
                    					 </div>
                                </div>
                          </div>
                      </div>
                      
                      
                      <!--shipping address-->
                      <?php if(!empty($selected_plan->isShipping) && $selected_plan->isShipping ==1){    ?>

                      <div class="heading" style="padding-bottom:20px;">
                          <h5> <strong>Shipping Information</strong></h5>
                      </div>
                        <div class="col-md-12">
												 <input type="checkbox" id="chk_add_copy"> Copy from Billing Address<br><br>
						</div>
                      <div class="shipping">
                         
                           <div class="container">  
                              <div class="row nopadding">
                                  <div class="col-md-6">
                                      <div class="form-group">
                            						<label class="control-label">STREET ADDRESS</label>
                            						<input type="text" name="saddress" id="saddress" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['sadd1']; } ?>">
                            					 </div>
                                  </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                            						<label class="control-label">ADDRESS 2</label>
                            						<input type="text" name="saddress2" id="saddress2" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['sadd2']; } ?>">
                            					</div>
                                  </div>
                              </div>
                              <div class="row">

                                  <div class="col-md-3">
                                      <div class="form-group">
                            						<label class="control-label">CITY</label>
                            						<input type="text" name="scity" id="scity" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['scity']; } ?>">
                            					 </div>
                                  </div>
                                
                                  <div class="col-md-3">
                                      <div class="form-group">
                            						<label class="control-label">STATE</label>
                            						<input type="text" name="sstate" id="sstate" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['sstate']; } ?>">
                            					 </div>
                                  </div>
                                  <div class="col-md-3">
                                      <div class="form-group">
                            						<label class="control-label">ZIP/POSTAL</label>
                            						<input type="text" name="szip" id="szip" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['szip']; } ?>">
                            					 </div>
                                  </div>
                                  <div class="col-md-3">
                                    <div class="form-group">
                          						<label class="control-label">COUNTRY</label>
                          						<input type="text" name="scountry" id="scountry" class="form-control" placeholder="" value="<?php if(isset($customer) && !empty($customer)){ echo $customer['scountry']; } ?>">
                          					 </div>
                                  </div>
                                </div>
                              
                          </div>
                          
                       
                         
                      </div>
                      
                      <?php   }  ?>
                      <hr/>
                </div>  
                
               
                
                
                      <div class="col-md-12 nopadding">
                          
                      
                          
                     <?php   $table1=$table=''; $one=$recr=0;
                     $table.='<div class="col-md-12 white-bg pb-2">
                     <table class="table table-border m-0">
                       <tr>
                        <thead>
                             <td style="width:250px">	<label>NAME</label> </td> 
                             <td class="text-center"><label>QUANTITY</label> </td>
                             <td class="text-right"><label>RATE</label></td>
                             <td class="text-right"><label>AMOUNT</label></td>
                         </thead>
						</tr>';
						$rec_total=0.00;
                        foreach($items as $item)
                        {
                            
                            if($item['oneTimeCharge']==0){  $recr=1;
                          $table.='<tr>
							   <td class="text-left" style="width:250px">'.$item['itemFullName'].'</td><td class="text-center">'.$item['itemQuantity'].'</td><td class="text-right">'.$item['itemRate'].'</td><td class="text-right">'.number_format($item['itemQuantity']*$item['itemRate'],2).'</td></tr>';  
							   $rec_total += $item['itemQuantity']*$item['itemRate'];
							}
							
                        } 
                        $table.='</table> </div>';
                        if($recr==1)
                        {
                             echo '<legend class="leg">Recurring Charges</legend>';
					  		
							echo $table;
						
						} ?>
						</div>
                    	<div class="col-md-12 nopadding white-bg col-xs-12">    
                    		<div class="col-md-9 col-xs-9">
                       			<p class="p_one text-font-weight-600 text-right">Recurring Subtotal</p>
                   			</div>
                     		<div class="col-md-3 col-xs-3"> 
                        		<p class="text-right text-font-weight-600"><?php echo '$'.number_format($rec_total,2);  ?></p>
                    		</div>
						</div> 
						<div class="col-md-12 nopadding one-time-charge-div">
						<?php
                        $table1.='<div class="col-md-12 white-bg pb-2">
                               <table class="table table-border m-0">
                               <tr>
                               <thead>
                                 <td style="width:250px"><label>NAME</label></td>
                                 <td class="text-center"><label>QUANTITY</label></td>
                                 <td class="text-right"><label>RATE</label></td>
                                 <td class="text-right"><label>AMOUNT</label></td>
                                </thead>
                              </tr>';
                              $total = 0.00;
                        foreach($items as $item)
                        {
                           
                            if($item['oneTimeCharge']==1)
                            { 
                                 $one=1;
                                $table1.='<tr>
                                    <td style="width:250px" >'.$item['itemFullName'].'</td>
                                    <td class="text-center">'.$item['itemQuantity'].'</td>
                                    <td class="text-right">'.$item['itemRate'].'</td>
                                    <td class="text-right">'.number_format($item['itemQuantity']*$item['itemRate'],2).'</td>
                                  </tr>';   
                                  $total += $item['itemQuantity']*$item['itemRate'];
                            }
                        } 
                      
                        $table1.='</table></div>';
                        if($one==1)
                        {
                              echo  '<legend class="leg leg-new">One-time Charges</legend>';
                            echo $table1;
                        
                        }
                     ?>
                      
                    <div class="back">
                    	<div class="col-md-12 nopadding padding_top_10px col-xs-12 white-bg">    
                    		<div class="col-md-9 col-xs-9">
                       			<p class="p_one text-font-weight-600 text-right">One-time Subtotal</p>
                   			</div>
                     		<div class="col-md-3 col-xs-3"> 
                        		<p class="text-right text-font-weight-600"><?php echo '$'.number_format($total,2);  ?></p>
                    		</div>
						</div> 
					</div>
                     <div class="col-md-12 nopadding">     
                     <legend class="leg col-xs-12 pl-0">Order Total</legend>
					            <div class="col-md-12 white-bg col-xs-12">
                      
                    	<div class="col-md-9 col-xs-9">
                       
                         <?php if(isset($recurring) && strtoupper(get_frequency($selected_plan->invoiceFrequency))=='MONTHLY' ) { ?>
                          <p style="padding-top:10px">Recurring Charges <?php echo $prorata_heading; ?></p>
                        <p>One-time Charges</p>  <?php } ?>
                        <p class="pull-right text-font-weight-600"><strong>Total Due Now</strong></p>
                   
                    </div>
                     <div class="col-md-3 col-xs-3 padding_top_10px"> 
                           <?php if(isset($recurring) && strtoupper(get_frequency($selected_plan->invoiceFrequency))=='MONTHLY') {  if( $selected_plan->freeTrial > 0){ ?>
                          <p class="text-right padding_top" ><?php echo '$'. number_format($recurring_onetime,2);  ?></p> 
                          <?php }else { ?> <p class="padding_top text-right" ><?php echo '$'. number_format($recurring  * $prorata_data,2);  ?></p>  <?php } ?>
                        <p class="text-right"><?php echo '$'. number_format($onetime,2);  ?></p> 
                        <?php } ?>
                        <p class="text-right"><strong><?php echo '$'. number_format(($selected_plan->subscriptionAmount),2);  ?></strong></p>
                    </div>
				   </div> 
				   </div>
                    <div class="form-group">
                    <input type="hidden" class="form-control" name="prorata_data" value="<?php echo $prorata_data; ?>" required />
							<input type="hidden" class="form-control" name="amount" value="<?php echo $selected_plan->subscriptionAmount; ?>" required />
								<input type="hidden" class="form-control" name="key" id="stripeApiKey" value="<?php if(isset($gateway['gatewayUsername']) && !empty($gateway['gatewayUsername'])){ echo $gateway['gatewayUsername']; } ?>" required />
					</div>
					<div class="p-0 col-md-12 white-bg col-xs-12">
					 <div class="col-md-12 text-right">
											     
                        <div class="form-group pull-right">
				          <?php if(!empty($selected_plan->isService) && $selected_plan->isService ==1){    ?>
                                    <span class="policy">I accept the <a target="_blank"  href="<?php if(!empty($customer_portal_data['serviceurl'])){ echo $customer_portal_data['serviceurl']; } else { echo '#'; } ?>">Terms of Service</a>.</span>
                                    <input type="checkbox" name="accept" id="accept" value="accept" />
                                    <br>
                                    <label id="accept-error" class="error pull-right" for="accept"></label>
                         <?php } ?>    
				        </div>
					   </div>
					   
						<div class="col-md-12 text-right">
						
							<div class="">
						
                <button style="margin-bottom: 20px;" id="btn_process" class="pay_btn_padding_12_52 btn btn-success text-font-weight-600" type="submit" value="1" name="pay">Confirm & Pay</button>
							
							</div>
						
							</div>
						</div>
					 
					
					</div>
                </div>
             
             
           
                 <div class="myblock3">
                    <div class="col-md-6">
                       
                    </div>
                    <div class="col-md-6">
                      <p style="margin-top: 23px;" class="text-right">Powered by <a href="<?php if(!empty($reseller['webURL'])){ echo $reseller['webURL']; } else{ echo'#';}  ?>"><?php if(!empty($reseller['resellerCompanyName'])){ echo $reseller['resellerCompanyName']; }  ?></a></p> 
                      
                       
                    </div>
                    
                </div>
              </form>
         </div>
     </div>
     </div>
</div>

<style>
     input[type="checkbox"]:required:invalid + label { color: red; }
</style>


<script>
$(document).ready(function () {
      $('#tc').click(function(){
        
        if($(this).is(':checked'))
        {
          
       
        $('#frdname').hide();
        }else
         $('#frdname').show();
        
    });
	$('#chk_add_copy').click(function(){
 
     
     if($('#chk_add_copy').is(':checked')){

                      	$('#saddress').val($('#address').val());
								$('#saddress2').val($('#address2').val());
								$('#scity').val($('#city').val());
								$('#sstate').val($('#state').val());
								$('#szip').val($('#zip').val());
								$('#scountry').val($('#country').val());
     }
     else{
     	var val_sp='';
     			$('#address').val(val_sp);
								$('#address2').val(val_sp);
								$('#city').val(val_sp);
								$('#state').val(val_sp);
								$('#zipcode').val(val_sp);
								$('#country').val(val_sp);
     }
    
 });
    
    $("#card_number11").attr("maxlength", 16);
    $("#expiry11").attr("maxlength", 2);
    $("#expiry_year11").attr("maxlength", 4);
    $("#ccv11").attr("maxlength", 4);
    
  $("#thest_pay").validate({
    ignore:":not(:visible)",
    rules: {
       email: {
        required: true,
        email:true,
      },
    
      company:{
          required: true,
            minlength: 3,
            maxlength: 41,
           validate_char: true,
      },
      
       first_name: {
        required: true,
        minlength: 1,
       maxlength: 100,
       validate_char: true,
      },
       last_name: {
        required: true,
         minlength: 1,
         maxlength: 100,
        validate_char: true,
      },
       country: {
         maxlength: 31,
		 validate_addre: true,
      },
      zip: {
         required: function(){
                      if($("#isBilling").val() == 1 ){

                        return true;
                      }else{
                         return false;
                      }
                  },
         minlength: 4,
         maxlength: 6,
        validate_addre: true,
      },
      address: {
        required: function(){
            if($("#isBilling").val() == 1 ){
              return true;
            }else{
               return false;
            }
        },
        maxlength: 25,
		    validate_addre: true,
      },
       address2: {
        maxlength: 25,
		    validate_addre: true,
      },
      city: {
        required: function(){
            if($("#isBilling").val() == 1 ){
              return true;
            }else{
               return false;
            }
        },
        maxlength: 31,
		    validate_addre: true,
      },
      state: {
        required: function(){
            if($("#isBilling").val() == 1 ){
              return true;
            }else{
               return false;
            }
        },
        maxlength: 31,
		    validate_addre: true,
      },
      saddress: {
        required: function(){
            if($("#isShipping").val() == 1 ){
              return true;
            }else{
               return false;
            }
        }
      },
      scity: {
        required: function(){
            if($("#isShipping").val() == 1 ){
              return true;
            }else{
               return false;
            }
        }
      },
      sstate: {
        required: function(){
            if($("#isShipping").val() == 1 ){
              return true;
            }else{
               return false;
            }
        }
      },
      szip: {
        required: function(){
            if($("#isShipping").val() == 1 ){
              return true;
            }else{
               return false;
            }
        }
      },
       cardNumber: {
        required: true,
        digits:true,
        minlength: 13,
        maxlength: 16
      },
       cardfriendlyname: {
        required: true,
         minlength: 14,
         maxlength: 25,
         validate_char: true
      },
       cardExpiry: {
        required: true,
         number:true,
        minlength: 2,
         min: 1, max: 12,
      },
       cardExpiry2: {
        required: true,
        number:true,
        minlength: 4,
         maxlength: 4,
         CCExp: {
				month: '#expiry11',
				year: '#expiry_year11'
		  }
      },
       cardCVC: {
        digits:true,
        minlength: 3,
        maxlength: 4
      },
      accountNumber:{
          required: true,
          number: true,
          minlength: 3,
          maxlength: 20,
      },
      confirmAccountNumber:{
          required: true,
          number: true,
          minlength: 3,
          maxlength: 20,
          equalTo: "#accountNumber"
      },
      routeNumber:{
          required: true,
          number: true,
          minlength: 3,
          maxlength: 12,
      },
      accept: {
        required: true
      },
      i_authorize_checkbox:{
        required: true
      }
    },
    messages: {
       company: {
        required: "Enter Company Name"
      }, 
      cardCVC: {
        minlength: "Must be min 3 number"
      },
      cardExpiry2: {
        required: "Enter Expiry Year",
        minlength: "Expiry year must be min 4 number. exp:2000"
      },
      accountNumber:{
          required: "Enter Account Number",
         
      },
      confirmAccountNumber:{
          required: "Enter Confirm Account Number",
          equalTo: "Account number and confirm account number not match",
      },
      routeNumber:{
          required: "Enter Routing Number",
      },
      cardExpiry: {
        required: "Enter Expiry Month",
        minlength: "Expiry month must be min 2 number"
      },
      cardfriendlyname: {
        required: "Enter Card Friendly Name"
      },
      cardNumber: {
        required: "Enter Card Number",
        minlength: "Must be min 13 digits"
      },
      email: {
        required: "Enter your Valid Email"
      },
     
      zip: {
        required: "Enter Zip Code",
         minlength: "zip Code must be min 4 characters"
      },
       first_name: {
        required: "Enter your First Name"
      },
      last_name: {
        required: "Enter your Last Name",
      },
       country: {
        required: "Enter Country Name"
      },
      address: {
        required: "Enter your Address"
      },
       city: {
        required: "Enter City Name"
      },
      state: {
        required: "Enter State Name"
      },
      saddress: {
        required: "Enter your Address"
      },
      scity: {
        required: "Enter City Name"
      },
      sstate: {
        required: "Enter State Name"
      },
      szip: {
        required: "Enter Zip Code",
      },
      
    },
    submitHandler: function (form) {
        $("#btn_process").attr("disabled", true);
        return true;
    }
  });
  
        
   $.validator.addMethod("phoneUS", function(phone_number, element) {
         
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
        }, "Please specify a valid phone number like as (XXX) XXX-XXXX");
         
          
       $.validator.addMethod("validate_char", function(value, element) {
    
        return this.optional(element) || /^[a-zA-Z][a-zA-Z0-9-_&.,/' ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, and dot hyphen or underscore.");
    
    
     $.validator.addMethod("validate_addre", function(value, element) {
    
          return this.optional(element) || /^[a-zA-Z0-9#][a-zA-Z0-9-_/,. ]+$/i.test(value);
    
      }, "Please enter only letters, numbers, space, hashtag, hyphen or underscore.");
    
         
  
   $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
					
});
/* Get change process invoice tab */
function get_process_details(eid) {


    var type = eid.value;

    $('#scheduleID').val(type);

    if(type == 2){
      $('#creditCardView').hide();
      $('#eCheckView').show();
    }else{
      $('#eCheckView').hide();
      $('#creditCardView').show();
    }
    

}
</script>