<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Page Not Found</title>

        <meta name="description" content="ChargeZoom automates your quickbook payments and help you focus on your business.">
        <meta name="author" content="Ecrubit">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="http://dev.chargezoom.net/resources/img/favicon.png">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="http://dev.chargezoom.net/resources/img/icon180.png" sizes="180x180">
        <!-- END Icons -->
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="http://dev.chargezoom.net/resources/css/bootstrap.min.css">
		

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="http://dev.chargezoom.net/resources/css/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="http://dev.chargezoom.net/resources/css/main.css">

        <!-- Include a specific file here from http://dev.chargezoom.net/resources/css/themes/ folder to alter the default theme of the template -->
        <link id="theme-link" rel="stylesheet" href="http://dev.chargezoom.net/resources/css/themes/night.css">
        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="http://dev.chargezoom.net/resources/css/themes.css">
        <!-- END Stylesheets -->
		

     
    </head>
    <body>
<!-- Error Container -->
<div id="error-container">
   
    <div class="row">
        <div class="col-sm-8 col-sm-offset-2 text-center">
            <h1><i class="fa fa-exclamation-triangle text-info animation-pulse"></i> 400</h1>
            <h2 class="h3">Oops, we are sorry but your request not found..</h2>
        </div>
        
    </div>
</div>
<!-- END Error Container -->

   
<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<script src="js/pages/formsWizard.js"></script>

    </body>
</html>