<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
       <title><?php echo $template['title'] ?></title>
    <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">
    <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
    <style>
    
    /************************** Vinay Css ******************************/
    
    .pay_button
    {
      padding: 7px 25px 6px 25px;
      font-size: 14px;
    }
    
    .padding-top-15{
        padding-top:15px;
    }
.padding-15{padding:15px;}
.cment{
    font-size: 24px;
    margin-top: 67px;
    padding: 10px;
}

.card_label{
    
    font-weight:500;
}
.mar-top-30
{
    margin-top:30px;
}
.pd-top-10
{
 padding-top:10px;   
}
.td_width_250
{
     width:250px;
}

.pd-bottom-20{
   padding-bottom:20px;
}
.mar-top-23
{
  margin-top: 23px;";
}

.mar-top-15
{
  margin-top: 15px;";
}


.navbar_heading{
    
    padding: 20px 0px;
}

.padding-6{
    padding:6px;
}
.bck{
    background:#f2f2f2 !important;
}
.head_invoice{
    
    color:#418eff;
    font-size: 19px;
    padding: 18px 0 10px 0px;
}
   
}
.under_line{
    
    border-bottom:2px solid #418eff !important;
}
.hr_line{
    
    margin:0px;
    border-top: 1px solid #cdcbcb !important;
}
.btn-font-16
{
 font-size:16px;    
}
    .plan{
        font-size: 18px;
       margin: 10px 14px 10px 14px;
    }
    
    /*radio css*/
    .radio-inline {
      position: relative;
      margin-bottom: 12px;
      cursor: pointer;
      -webkit-user-select: none;
      -moz-user-select: none;
      -ms-user-select: none;
      user-select: none;
    }
    
    /* Hide the browser's default radio button */
    .radio-inline input {
      position: absolute;
      opacity: 0;
      cursor: pointer;
    }
    
    /* Create a custom radio button */
    .checkmark {
        position: absolute;
        top: 10px;
        left: 0;
        height: 13px;
        width: 13px;
        background-color: #bcb8b8;
        border-radius: 50%;
    }
    
    /* On mouse-over, add a grey background color */
    .radio-inline:hover input ~ .checkmark {
      background-color: #ccc;
    }
    
    /* When the radio button is checked, add a blue background */
    .radio-inline input:checked ~ .checkmark {
      background-color: #2196F3;
    }
    
    /* Create the indicator (the dot/circle - hidden when not checked) */
    .checkmark:after {
      content: "";
      position: absolute;
      display: none;
    }
    
    /* Show the indicator (dot/circle) when checked */
    .radio-inline input:checked ~ .checkmark:after {
      display: block;
    }
    
    /* Style the indicator (dot/circle) */
    .radio-inline .checkmark:after {
     	top: 4px;
        left: 4px;
        width: 5px;
        height: 5px;
        border-radius: 50%;
        background: white;
    }
    .btn-size{
        
       padding:8px 35px 8px 35px;
    }
/********************************************************************/
    
    body{background:#ffffff!important;}
    .backg{background:#f2f2f3;min-height:400px;border-top:1px solid #DADADA;}
    .back{background:#ffffff;width: 100%;float: left;border:1px solid #DADADA;}
    .nopadding{padding:0px;}
    .heading {padding: 8px 0 0 15px;}
    .padding{padding:15px 0px!important;}
    .myblock{background:#f2f2f3;width: 100%;float: left;}
    .myblock2{background:#ffffff;width: 100%;float: left;;border:1px solid #DADADA;margin-top:40px;}
    .policy{margin:3px 5px;}
    .myblock3 {float: left;width: 100%;margin: 30px 0px 50px 0px;}
    .btn-primary{background:#2676A4;border:1px solid #2676A4;}
    .error{color:red;font-size:11px;}
    .servererror{color:red;font-size:11px;width:100%;}
    .form-group {margin-bottom: 15px; min-height:50px;}
    .pd_div{padding:10px;}
    @media (min-width:992px)
    {
        .container{width:786px}
    }
@media (min-width:1200px) and (max-width:1400px)
    {
        .container{width:786}
    }
</style>

</head>
<body>
<div class="container">
    <header>
        <div class="col-md-12">
            <div class="navbar-brand-centered" style="padding: 20px 0px;">
               <?php if(!empty($customer_portal_data['ProfileImage'])){?>
                    <img src="<?php echo LOGOURL.$customer_portal_data['ProfileImage'];  ?>" class="img-responsive center-block" />
               <?php }else{ ?>
                    <img src="<?php echo  CZLOGO; ?>" class="img-responsive center-block"  />
               <?php } ?>
            </div>
        </div>
        
     </header>
</div>

<div class="container-fluid backg">
    <div class="container">
        <div class="row">
            <div class="msg_data">
                <?php echo $this->session->flashdata('message');   ?>
            </div>
        </div>
         
        <?php
        
        if(isset($_SERVER['HTTPS'])) {
       
          $protocol =($_SERVER['HTTPS']=='on')?'https' : 'http';
          }else
          {
             $protocol =  'http';
          }
      
     
        $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    
        $val = round($get_invoice['BalanceRemaining']);
        if($val > 0 || $get_invoice['IsPaid'] == false){
        ?>
        
        <form method="post" id="thest_pay" action='<?php echo $action; ?>'>
        <div class="row">
          
            <div class="col-md-5">
                <h3 class="head_invoice">Invoice No: <?php if(!empty($get_invoice['RefNumber'])){ echo $get_invoice['RefNumber']; }else{ echo $get_invoice['invoiceID'];} ?></h3>
            </div>
             <div class="col-md-7">
                <h3 class="pull-right head_invoice">Amount: $<input type="text"  name="payamount" id="payamount" onblur="setPayment()" value="<?php if(!empty($get_invoice['BalanceRemaining'])){ echo $get_invoice['BalanceRemaining']; } ?>" minlength="1"/></h3>
            </div>
        </div>
        
        <div class="row">
            
                <input type="hidden" value="<?php echo $mid; ?>" name="mid" />
                <input type="hidden" value="<?php echo $invoice_no ?>" name="invid" />
                <input type="hidden" value="<?php echo $token ?>" name="token" />
                <input type="hidden" value="<?php echo $protocol ?>" name="requrl" />
            <div class="back">
                <div class="col-md-12 nopadding">
                    
                  <div class="heading">
                       <h5> <strong>Cardholder Information</strong></h5>
                  </div>
                  <hr/>
                  
                      <!--personal detail-->
                      <div class="container">
                           <div class="row nopadding">
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>FIRST NAME</label>
            						<input type="text" name="first_name" id="first_name" class="form-control input-md">
            						<?php echo form_error('first_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>LAST NAME</label>
            						<input type="text" name="last_name" id="last_name" class="form-control input-md">
            						<?php echo form_error('last_name', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                                
                          </div>
                      </div>
                       
                      
                      
                      <hr/>
                      
                      <!--card detail-->
                      <div class="container">
                           <div class="row padding-15">
                                <div class="col-md-12 bck">
                                <div class="col-md-4 padding-top-15">
                                  <div class="form-group">
            					
            						<input type="radio" name="paymethod" id="creditCard"  checked="true" class="" value="1">
            							<label>Credit Card</label>
            						
            					 </div>
                              </div>
                              <div class="col-md-4 padding-top-15">
                                  <div class="form-group">
            					
            					    	<input type="radio" name="paymethod" id="eCheck"  class="" value="2">
            					    		<label>eCheck</label>
            					   </div>
                              </div>
                           
                            <!------------------------------ Credit Card Details Start Here ---------------------------------->
                                 <div class="row padding-15">
                                     <div id="credit_card_details" class="desc">
                               
                             <div class="col-md-12 bck">
						      <div class="col-md-6">
                                  <div class="form-group">
                                      
            						<label class="card_label">CARD TYPE</label>
            						<select name="cardType" class="form-control">
            						    <option value="">SELECT CARD TYPE</option>
            						    <option value="Visa">VISA</option>
            						    <option value="MasterCard">MASTERCARD</option>
            						    <option value="American Express" >AMERICAN EXPRESS</option>
            						    <option value="Discover" >DISCOVER</option>
            						</select>
            			        	 <i class="fa fa-angle-down"></i>
            					 </div>
                              </div>
                            
                              
                                <div class="col-md-6">
				                    <div class="form-group">
						            <label class="card_label" for="Card Number">Card Number</label>
						            <div class="input-group">
                                       <input type="text" autocomplete="off" name="cardNumber" id="card_number11" class="form-control input-md">
                                      <span class="input-group-addon"><img src="<?php echo base_url().'resources/img/credit_card.png';?>" /></span>
						             </div>
					               </div>
                               </div>
                             
                 
                              
                             </div>
                             
                             <div class="col-md-12 bck"> 
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label class="card_label"> EXPIRATION MONTH</label>
            				         <input type="text" name="cardExpiry" id="expiry11" class="form-control input-md" placeholder="">
            						<?php echo form_error('cardExpiry', '<div class="servererror">', '</div>'); ?>  
            					 </div>
                              </div>
                               <div class="col-md-1">
                                  <div class="form-group">
            						<label>&nbsp;&nbsp;&nbsp;</label>
            						 <span class="cment">/</span> 
            						
            					 </div>
                              </div>
                              <div class="col-md-3">
                                 
                                  <div class="form-group">
            						<label class="card_label">EXPIRATION YEAR</label>
            					     <input type="text" name="cardExpiry2" id="expiry_year11" class="form-control input-md" placeholder="">
            						<?php echo form_error('cardExpiry2', '<div class="servererror">', '</div>'); ?> 
            					 </div>
                              </div>
                              
                              	<div class="form-group col-md-3">
					                <label class="control-label card_label ">CVV</label>
				                      <div class="input-group">
                                        <input type="text" onblur="create_Token_stripe();" id="ccv11" name="cardCVC"  class="form-control input-sm " placeholder="" autocomplete="off">
            						     <?php echo form_error('cardCVC', '<div class="servererror">', '</div>'); ?>
            						      <span class="input-group-addon bck"><img src="<?php echo base_url().'resources/img/info.png';?>" /></span>
                                    </div>
                                </div>  
                   
                               <div class="col-md-2">
                                  <div class="form-group">
            						<label>&nbsp;&nbsp;&nbsp;</label>
            						 <img src="<?php echo base_url().'resources/css/card.png';  ?>" class="img-responsive"  />
            						
            					 </div>
                              </div>
                         </div>
                         
                              
                           
                            </div>  <!-- End of credit card -->
                            
                             
                              </div>
                            
                            <!------------------ Start of echeck Details ------------------------->
                            
                            <div id="echeck_details" class="desc"> 
                            
                              <div class="col-md-12">
                                    <img src="<?php echo CARD; ?>" class="img-responsive" style="width:30%;padding-bottom:20px;" />
                               </div>
                               
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>ACCOUNT NAME</label>
            						
            						<input type="text" id="accountName" data-stripe="accountName" name="accountName" value="" class="form-control" placeholder="">
            							<?php echo form_error('accountName', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                           
                              
                               <div class="col-md-6">
                                  <div class="form-group">
            						<label>ACCOUNT NUMBER</label>
            						<input type="text" id="accountNumber" name="accountNumber" class="form-control" value="" placeholder="">
            						<?php echo form_error('accountNumber', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>ROUTING NUMBER</label>
            						<input type="text" id="routeNumber" name="routeNumber" class="form-control" value="" placeholder="">
            						<?php echo form_error('routeNumber', '<div class="servererror">', '</div>'); ?>  
            					 </div>
                              </div>
                              
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label>ACCOUNT TYPE</label>
            					   <select id="acct_type" name="acct_type" class="form-control valid" aria-invalid="false" aria-required="true">
                                     <option value="checking" seleced="">Checking</option>
                                      <option value="saving">Saving</option>
                                </select>
            						<?php echo form_error('acct_type', '<div class="servererror">', '</div>'); ?> 
            					 </div>
                              </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                      <label>ACCOUNT HOLDER TYPE</label>
            					  <select id="acct_holder_type" name="acct_holder_type" class="form-control valid" aria-invalid="false" aria-required="true">
                                        <option value="business">Business</option>
                                        <option value="personal">Personal</option>
                                   </select>	
                                   	<?php echo form_error('acct_holder_type', '<div class="servererror">', '</div>'); ?> 
            					 </div>
                            </div>
                           
                           </div>  <!-- End of eCheck Details -->
                         
                            <div class="col-md-6">
                                 
                                  <div class="form-group">
            						<label><input type="checkbox" value="savepaymentinfo" id="savepaymentinfo" name="savepaymentinfo" /> &nbsp; Save Payment Info</label>
            						
            					 </div>
                            </div> 
                              
                            
                         </div>
                         </div>
                    </div>
                            
                  
                    <div class="container">
                           <div class="row">          
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>STREET ADDRESS</label>
            						<input type="text" name="address" id="address" class="form-control input-md">
            						<?php echo form_error('address', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
            						<label>ADDRESS 2</label>
            						<input type="text" name="address2" id="address2" class="form-control input-md">
            					 </div>
                              </div>
                           </div> 
						   <div class="row">   						   
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>CITY</label>
            						<input type="text" name="city" id="city" class="form-control input-md">
            						<?php echo form_error('city', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>STATE</label>
            						<input type="text" name="state" id="state" class="form-control input-md">
            						<?php echo form_error('state', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>ZIP/POSTAL</label>
            						<input type="text" name="zip" id="zip" class="form-control input-md">
            						<?php echo form_error('zip', '<div class="servererror">', '</div>'); ?>
            					 </div>
                              </div>
                             
                             
                              <div class="col-md-3">
                                  <div class="form-group">
            						<label>COUNTRY</label>
            						<input type="text" name="country" id="country" class="form-control input-md" >
            					 </div>
                              </div>
                             
                               <div class="col-md-12 text-right">
                                  
                                <label class="label-control"><b>Amount: $<span id="p_inv_amont"><?php if(!empty($get_invoice['BalanceRemaining'])){ echo number_format($get_invoice['BalanceRemaining'],2); } ?></span></b></label>
                              </div>
                              
                               
                                
                              <div class="col-md-12 text-left">
            						<label><input type="checkbox" value="sendrecipt" id="sendrecipt" name="sendrecipt" /> &nbsp; Send Receipt</label>
            				 </div>
                              
                          </div>
                      </div>
                  
                      
                </div>  
              
             </div>
             
             <input type="hidden" id="stripeApiKey" name="stripe" value="<?php if(!empty($userName)){ echo $userName;} ?>" />
                 <div class="myblock3">
                    <div class="col-md-6 pull-left">
                                 <input type="submit" name="pay" value="Pay Now" class="btn btn-sm btn-success pay_button" />
                    </div>
                    <div class="col-md-6">
                       <p class="text-right">Powered by <a href="<?php echo $mr_config['webURL']; ?>"><?php echo $mr_config['resellerCompanyName']; ?></a></p>
                       
                    </div>
                    
                </div>
              </form>
         </div>
        <?php  }
        
        else{
         
             header("location:".base_url()."page_404/msg_paid");
         
            
            
            
        }  ?>
         
     </div>
</div>
<script src="https://payportal.net/resources/js/vendor/jquery-1.11.3.min.js"></script>
 <script src="https://payportal.net/resources/js/vendor/bootstrap.min.js"></script>
       
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>

$(document).ready(function () {
    $("#card_number11").attr("maxlength", 17);
    $("#expiry11").attr("maxlength", 2);
    $("#expiry_year11").attr("maxlength", 4);
    $("#ccv11").attr("maxlength", 4);
    
  $("#thest_pay").validate({
    rules: {
     
       first_name: {
        required: true,
        minlength: 1,
              maxlength: 100,
      },
       last_name: {
        required: true,
         minlength: 1,
         maxlength: 100,
      },
    
      
       cardNumber: {
        required: true,
        number:true,
        minlength: 13,
        maxlength: 16,
      },
       cardExpiry: {
        required: true,
         number:true,
        minlength: 2,
         min: 1, max: 12,
      },
       cardExpiry2: {
        required: true,
        number:true,
        minlength: 4,
         maxlength: 4,
       
       
     CCExp: {
				month: '#expiry11',
				year: '#expiry_year11'
		  }
      },
       cardCVC: {
        required: true,
        number:true,
        minlength: 3,
        maxlength: 4
      },
       accountName: {
        required: true,
        minlength: 3,
        maxlength: 20
      },
       accountNumber: {
        required: true,
        number:true,
        minlength: 9,
        maxlength: 17
      },
        routeNumber: {
        required: true,
        number:true,
        minlength: 9,
        maxlength: 12
      },
       acct_type: {
        required: true,
      },
      acct_holder_type: {
        required: true,
      },
    },
    messages: {
        
      cardCVC: {
        required: "Enter CVV",
        minlength: "Must be min 3 number"
      },
      cardExpiry2: {
        required: "Enter Expiry Year",
        minlength: "Expiry year must be min 4 number. exp:2000"
      },
      cardExpiry: {
        required: "Enter Expiry Month",
        minlength: "Expiry month must be min 2 number"
      },
      cardfriendlyname: {
        required: "Enter Card Friendly Name"
      },
      cardNumber: {
        required: "Enter Card Number",
        minlength: "Must be min 13 digits",
         maxlength: "Must be max 16 digits"
      },
    
    },
    
  });
   $.validator.addMethod('CCExp', function(value, element, params) {  
		  var minMonth = new Date().getMonth() + 1;
		  var minYear = new Date().getFullYear();
		  var month = parseInt($(params.month).val(), 10);
		  var year = parseInt($(params.year).val(), 10);
		  
		  

		  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
		}, 'Your Credit Card Expiration date is invalid.');
					
});


function format2(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

 
}
  
  function stripeResponseHandler_res(status, response)
	  {
	  
          
                        $("#thest_pay").find('input[name="stripeToken"]').remove();
           
                if (response.error) {
                  
                    $('#submit_btn').removeAttr("disabled");
               
                    $('#payment_error').text(response.error.message);

                } else {
                    var form = $("#thest_pay");
                    // Getting token from the response json.

                    $('<input>', {
                            'type': 'hidden',
                            'name': 'stripeToken',
                            'value': response.id
                        }).appendTo(form);

               
                 $("#btn_process").attr("disabled", false);	
                }
            }
            
            
            
	
 function create_Token_stripe()
 {
   
    
		var pub_key = $('#stripeApiKey').val();
        if(pub_key!=''){
		 Stripe.setPublishableKey(pub_key);
         Stripe.createToken({
                        number: $('#card_number11').val(),
                        cvc: $('#ccv11').val(),
                        exp_month: $('#expiry11').val(),
                        exp_year: $('#expiry_year11').val(),
                        name: $('#first_name').val()+''+$('#last_name').val()
                    }, stripeResponseHandler_res);
   
    }
        // Prevent the form from submitting with the default action
        return false;
		
     
 }
 
 
 $(document).ready(function() {
    $('#credit_card_details').show();
     $('#echeck_details').hide(); 
    $("input[name$='paymethod']").click(function() {
        var cardValue = $(this).val();
         if(cardValue == 2){
             
            $('#echeck_details').show(); 
            $('#credit_card_details').hide(); 
         }else{
             
            $('#echeck_details').hide(); 
            $('#credit_card_details').show(); 
         }
        
    });
});

function setPayment()
{
    var payment = $('#payamount').val();
    
    $('#p_inv_amont').html(format2(payment));
    
    
}
</script>
<script type="text/javascript">
$(document).on('click','.cvvMask',function(){
  
    $(this).attr('type','password');
    $(this).attr('maxlength','4');
}); 
$(document).on('click','.CCMask',function(){
    $(this).attr('type','password');
});
</script>
</body>
</html>