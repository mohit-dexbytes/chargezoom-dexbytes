<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  
  
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css'>

  
  
</head>

<body>
   
  <div class="jumbotron thank_page text-xs-center">
   <div class="msg_data ">
        <?php echo $this->session->flashdata('message');   ?>
  </div> 
  <h1 class="display-3">Thank You!</h1>
 
  
  <p>
    Having trouble Login? <a href="">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-primary btn-sm" href="#" role="button">Access Portal</a>
  </p>
</div>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>

  

</body>

</html>