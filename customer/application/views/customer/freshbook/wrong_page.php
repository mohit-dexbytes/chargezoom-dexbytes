<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
<!--  <script src="<?php echo base_url();  ?>resources/js/footer.js"></script> -->
  
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css'>

  
  
</head>

<body>
   
  <div class="jumbotron wrong_page text-xs-center">
   <div class="msg_data ">
        <?php echo $this->session->flashdata('message');   ?>
  </div> 
  <h1 class="display-3">Wrong URL!</h1>
  <p class="lead"><strong>Please put accurate URL.</strong> </p>
  <p class="lead">Please check your plan URL again or contact your merchant.</p>
  <hr>

</div>
  <script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js'></script>

  

</body>

</html>