<style>


.btn_size {
    width: 67px;
}
   .cust_view a {
    color: #167bc4 !important;
}
.profile-pic {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
    border-radius: 50%;
    border: 4px solid white;
    height: 180px;
    overflow: hidden;
    transform: translateX(-50%) translateY(-50%);
    width: 180px;
    top: 0;
    top: 100px;
}
.user-info {
    *zoom: 1;
    padding: 8px;
    position: relative;
}
.hidden-input {
    left: -999px;
    position: absolute;
}
.profile-pic:hover .image-wrapper {
    bottom: 0;
}
.image-wrapper {
    background: rgba(0, 0, 0, 0.5);
    border-radius: 50%;
    bottom: -50px;
    height: 50px;
    left: 0;
    position: absolute;
    transition: bottom 0.15s linear;
    width: 100%;
}
.image-wrapper .edit {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
    color: white;
    cursor: pointer;
    font-size: 22px;
    top: 12px;
    left: 12px;
}
.profile-pic img {
    box-sizing: border-box;
    height: 100%;
    left: 50%;
    max-height: 100%;
    position: absolute;
    transform: translateX(-50%);
    transition: all 0.15s ease-out;
    width: auto;
}
</style>
<!-- Page content -->
<div id="page-content">
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
	<!-- Customer Content -->
    <div class="row">
        
        <div class="col-lg-4">
            <!-- Customer Info Block -->
			
			
            <div class="block">
                <!-- Customer Info Title -->
                <div class="block-title">
                    <h2><i class="fa fa-file-o"></i> <strong>My</strong> Details</h2>
                </div>
                <!-- END Customer Info Title --> 

                <!-- Customer Info -->
                <div class="block-section text-center">
                        <div class="user-info">
						<div class="profile-pic">
								<?php if($customer->profile_picture!=""){ ?> 
          
								<img src="<?php echo base_url(); ?>../uploads/customer_pic/<?php echo $customer->profile_picture; ?>" />
                                <?php }else{ ?>
                                <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" />
                                <?php } ?>
							 <div class="layer">
								<div class="loader"></div>
							 </div>
							<a class="image-wrapper" href="#">
								<input class="hidden-input" id="changePicture" name="profile_picture" type="file">
								<label class="edit fa fa-pencil" for="changePicture" type="file" title="Change picture"></label>
							
							</a>
						</div>
						
						<input type="hidden" id="cr_url" value="<?php echo current_url(); ?>" />
						</div> 
                    <h3>
                        <strong><?php echo $customer->FirstName.' '.$customer->LastName; ?></strong>
                     
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                       <tr>
                            <td class="text-right"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->FullName; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Added On</strong></td>
                            <td><?php echo date('F d, Y - h:m', strtotime($customer->TimeCreated)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Last Visit</strong></td>
                            <td><?php echo date('F d, Y - h:m', strtotime($customer->TimeModified)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Main Email</strong></td>
                            <td><?php echo $customer->Contact; ?></td>
                        </tr>
						<tr>
                            <td class="text-right"><strong>Main Phone</strong></td>
                            <td><?php echo $customer->Phone; ?></td>
                        </tr>
						<tr>
                              <td class="text-right"><strong>Credit Card Info</strong></td>
                            <td> <a href="#card_data_process" class="btn btn-sm btn-success"  onclick="set_card_user_data('<?php  echo $customer->ListID; ?>', '<?php  echo $customer->FullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
							 <a href="#card_edit_data_process" class="btn btn-sm btn-info"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
							</td>
                        </tr>
						<tr>
                            <td class="text-right"><strong>Status</strong></td>
                            <td><i class="fa fa-check"></i> Active</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Customer Info -->
                
                
                
                
            </div>
            <!-- END Customer Info Block -->

            <!-- Quick Stats Block -->
            <div class="block">
                <!-- Quick Stats Title -->
                <div class="block-title">
                    <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
                </div>
                <!-- END Quick Stats Title -->

                <!-- Quick Stats Content -->
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background">
                            <i class="gi gi-list"></i>
                        </div>
                        <h4 class="text-left">
                            <strong><?php echo $invoices_count; ?></strong><br><small>Invoices in Total</small>
                        </h4>
                    </div>
                </a>
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background-success">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h4 class="text-left text-success">
                            <strong>$ <?php echo $sum_invoice; ?></strong><br><small>Invoice Value</small>
                        </h4>
                    </div>
                </a>
                <!-- END Quick Stats Content -->
            </div>
            <!-- END Quick Stats Block -->
        </div>
        <div class="col-lg-8">
            <!-- Orders Block -->
            <div class="block">
                <!-- Orders Title -->
                <div class="block-title">
				
				
				
				
				   <div class="block-options pull-right">
                
                        <span class="text" style="color:#797981;"><strong>$<?php echo number_format($pay_remaining,2); ?></strong></span>
					</div>
                    <h2><strong>Outstanding</strong> Invoices</h2>
                </div>
                <!-- END Orders Title -->

                <!-- Orders Content -->
                <table id="compamount" class="table page_details table-bordered table-striped table-vcenter">
                    
                          <thead>
                            <th class="text-left" >Invoice</th>
                            <th class="hidden-xs text-right">Due Date</th>
                          
							  <th class="text-right">Amount</th>
								<th class="text-right">Balance</th>
							   <th class="text-right hidden-xs">Status</th>
                           <th class="text-center">Action</th>
                        </thead>
						<tbody>
						<?php   if(!empty($invoices)){     
                        
                        foreach($invoices as $invoice){
						
						  
						 ?>
                        <tr>
                            <td class="text-left cust_view"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                            <td class="hidden-xs text-right"><?php  echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                          
							 <td class="text-right"><strong><?php echo "$".number_format($invoice['AppliedAmount'],2);  ?></strong></td>
						 <td class="text-right"><strong>$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></strong></td>
							<td class="hidden-xs text-right"><?php  echo $invoice['status']; ?></td>	
                           
							 
							 	
							 
                            <td class="text-center">						
								
								 <?php 	 if($invoice['status']=='Open'|| $invoice['status']=='Past Due'){ 
								     ?>
                                
									   <a href="#invoice_process" class="btn btn-sm btn-success"   onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>', '<?php  echo $customer->ListID; ?>', '<?php  echo $invoice['BalanceRemaining']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
								    
									  <?php
                                 
                                 }else 
                                 
                                 if($invoice['status']=='Paid') 
                                 {  ?>
									 
									 <a href="javascript:void(0)"  class="btn btn-sm btn-alt1 btn-success  remove-hover">Paid</a>
								<?php	 }else{ ?><a href="javascript:void(0)"  class="btn btn-sm btn_size btn-danger remove-hover">Canceled</a>
									  
									 <?php } ?>
							</td>
                        </tr>
                                
						   <?php  } }
							
							?>	
						

                        	
						

                        
                    </tbody>
                </table>
               <br> <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
							
                     <span class="text remove-hover" style="color:#797981;"><strong>$<?php echo number_format($pay_invoice,2); ?></strong></span>
                 
					
                    </div>
                    <h2><strong>Paid</strong> Invoices</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table id="ecom-orders" class="table page_details table-bordered table-striped ecom-orders table-vcenter">
				  <thead>
                 
                        <th class="text-left"><strong>Invoice</strong></th>
                        <th class="hidden-xs text-right">Paid Date</th>
                        <th class="text-right hidden-xs">Invoice Amount</th>
					 
                        <th class="text-right hidden-xs">Payments</th>
							  <th class="text-right">Balance</th> 
                           
                </thead>
                    <tbody>
                      
						
						<?php   if(!empty($latest_invoice)){   
 
						foreach($latest_invoice as $invoice){
						  $full='';
						  
						  
						    if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
						   
						   $full =$invoice['AppliedAmount']+$invoice['BalanceRemaining'];
						   
						   if($invoice['AppliedAmount']!="0.00"){
						       $status = "Partial";
						   }
						   	
						    if($invoice['BalanceRemaining'] =="0.00"){
						       $status = "Paid";
						   }
						   
						   
						 ?>
                        <tr>
                            <td class="text-left cust_view"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                           <td class="hidden-xs text-right"><?php  echo  $invoice['TimeModifiedinv']; ?></td>
                           <td class="hidden-xs text-right">$<?php echo ($full)?number_format($full,2):'0.00'; ?></td>
						   
                       
                           
						   <?php if($invoice['AppliedAmount']!="0.00"){ ?>
                            <td class="hidden-xs text-right cust_view"><a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$<?php  echo number_format($invoice['AppliedAmount'],2); ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right"><strong>$<?php  echo  number_format($invoice['AppliedAmount'],2); ?></strong></td>   
						   <?php } ?>
							 <td class="text-right"><strong>$<?php  echo number_format($invoice['BalanceRemaining'],2); ?></strong></td>
							
                        </tr>
						   <?php } } ?>
						
					
                       
                        
                    </tbody>
                </table>
               <br> <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->
	<div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    
                    <h2> <strong>Subscriptions</strong> </h2>
                </div>
				
				
        <!-- All Orders Content -->
        <table id="sub_page" class="table page_details compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Name</th>
					<th class="text-left hidden-xs">Merchant ID</th>
                    <th class="text-left">Gateway</th>
                    <th class="text-right">Amount</th>
					<th class="text-right hidden-xs">Next Charge Date</th>
				
                </tr>
            </thead>
			 <tbody>
			 <?php   if(!empty($getsubscriptions)){   

                        
						foreach($getsubscriptions as $getsubscription){ 
						  
						 ?>
			
                   <tr>
					
                    <td class="text-left "><?php echo $getsubscription['subscriptionName']; ?></td>
					
				    <td class="text-left hidden-xs"> <?php echo $getsubscription['gatewayMerchantID']; ?> </td>
					
                    <td class="text-left "><?php echo $getsubscription['gatewayFriendlyName']; ?>  </td> 
					
					
				    
					<td class="text-right">$<?php echo ($getsubscription['subscriptionAmount'])?$getsubscription['subscriptionAmount']:'0.00'; ?></td>
				   
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>
					
				
				
				</tr>
				
				<?php  } } ?>
				
			</tbody>
			
				</table>
				<br>
				
			</div>
            <!-- Customer Addresses Block -->
            <div class="block">
                <!-- Customer Addresses Title -->
                <div class="block-title">
                    <h2><strong>Addresses</strong></h2>
                </div>
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    <div class="col-lg-6">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title">
                                <h2>My Address</h2>
                            </div>
                            <!-- END Billing Address Title -->

                            <!-- Billing Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                            <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo $customer->ShipAddress_Addr2; ?>, <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo $customer->ShipAddress_State; ?>, <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->Phone; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)" ><?php echo $customer->Contact; ?> </a>
                            </address>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    <div class="col-lg-6">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title">
                                <h2>Invoice Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->

                            <!-- Shipping Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                          <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo $customer->ShipAddress_Addr2; ?>, <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo $customer->ShipAddress_State; ?>, <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->Phone; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><?php echo $customer->Contact; ?></a>
                            </address>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <!-- END Customer Addresses Block -->
         
            
            
            <!-- END Private Notes Block -->
        </div>
    </div>
    <!-- END Customer Content -->

	
<!-- END Page Content -->
<!-- END Page Content -->
<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit/Delete  Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
        
                
                     
                 
     <table  class="table cmptable table-bordered table-striped table-vcenter">
     
         <?php
            if(!empty($card_data_array)){
             foreach($card_data_array as $cardarray){ ?>
           
                    <tr>
                    <td class="text-left hidden-xs"><?php echo $cardarray['customerCardfriendlyName'];  ?></td>
                    <td class="text-right visible-lg"><?php echo $cardarray['CardNo'];  ?></td>
                    <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
                            <a href="#" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('<?php echo $cardarray['CardID'] ; ?>');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>
                            <a href="<?php echo base_url(); ?>Payments/delete_card_data/<?php echo $cardarray['CardID'] ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>
                          
                        </div> </td>
                </tr>  
            
        <?php } 
        
            }else{ ?>   
         <tr>
                    <td colspan="3">No Card Available!</td>
               
                </tr> 
        
        <?php } ?>
       </table> 

        <form id="thest_form" method="post" style="display:none;" action='<?php echo base_url(); ?>Payments/update_card_data' class="form-horizontal" >
                	<div style="display:none;" id="editccform"> 
                	<fieldset>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                                 <div id="m_card_id"><span id="m_card"></span> <input type='button' id="btn_mask" class="btn btn-default btn-sm" value="Edit Card" /></div>
                             
                                 <input type="hidden" disabled  id="edit_card_number" name="edit_card_number" class="form-control" placeholder=""/>
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                    <select id="edit_expiry" name="edit_expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
                                                        <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
                                                        <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
                                                
                                                    <label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                    <select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
                                                    <?php 
                                                        $cruy = date('y');
                                                        $dyear = $cruy+15;
                                                    for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                                    <?php } ?>
                                                       </select>
                                                </div>
                                                
                                                
                                            </div>     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" placeholder=""  autocomplete="off"/>
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="edit_friendlyname">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_friendlyname" name="edit_friendlyname" class="form-control" placeholder="" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                    </div>
                    <div style="display:none;" id="editcheckingform"> 
                     <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress1" name="baddress1" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="baddress2" name="baddress2" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcity" name="bcity" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bstate" name="bstate" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bzipcode" name="bzipcode" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcountry" name="bcountry" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Phone Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="bcontact" name="bcontact" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                    </fieldset>
                    </div>
                     <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
                     <input type="submit" name="btn_process" class="btn btn-sm btn-success" value="Edit"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                </form>     
     <!-- panel-group -->
                
                
                    
                <div id="can_div">
                 <div class="pull-right">
                    
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                 </div>
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------Show Payment Data------------------->

<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
            
        
                
                 <form id="thest" method="post" action='<?php echo base_url(); ?>Payments/insert_new_data' class="form-horizontal" >
                 <fieldset>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                        <div class="col-md-8">
                               <input type="text" id="customername" name="customername" class="form-control" value="" placeholder="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="card_number" name="card_number" class="form-control" placeholder="" autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                    <select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
                                                        <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
                                                        <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
                                                
                                                    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                    <select id="expiry_year" name="expiry_year" class="form-control">
                                                    <?php 
                                                        $cruy = date('y');
                                                        $dyear = $cruy+15;
                                                    for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
                                                    <?php } ?>
                                                       </select>
                                                </div>
                                                
                                                
                                            </div>     
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" placeholder="" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                    
                    <fieldset>
                        <legend>Billing Address</legend>
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 1</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address1" name="address1" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Address Line 2</label>
                        <div class="col-md-8">
                       		   <input type="text" id="address2" name="address2" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">City</label>
                        <div class="col-md-8">
                       		   <input type="text" id="city" name="city" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">State/Province</label>
                        <div class="col-md-8">
                       		   <input type="text" id="state" name="state" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                        <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">ZIP Code</label>
                        <div class="col-md-8">
                       		   <input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                         <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Country</label>
                        <div class="col-md-8">
                       		   <input type="text" id="country" name="country" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                          <div class="form-group">
                        <label class="col-md-4 control-label" for="val_username">Contact Number</label>
                        <div class="col-md-8">
                       		   <input type="text" id="contact" name="contact" class="form-control" value="" placeholder="">
                        </div>
                        </div>
                        
                    </fieldset>
                    
                     <input type="hidden" id="customerID11" name="customerID11"  value="" /> 
                  <div class="pull-right">
                     <input type="submit"  name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
                </form>     
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>





<div id="invoice_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Process Invoice</h2>
               
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action="<?php echo base_url().'Payments/pay_invoice'; ?>" class="form-horizontal card_form" >
                     
                 
					 
					
									<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gateway" name="gateway" class="form-control">
                                                        <option value="" >Select Gateway</option>
														<?php if(isset($gateway_datas) && !empty($gateway_datas) ){
																foreach($gateway_datas['gateway'] as $gateway_data){
																?>
                                                            <option value="<?php echo $gateway_data['gatewayID'];  ?>" <?php if($gateway_data['set_as_default']=='1')echo "selected ='selected' ";  ?> ><?php echo $gateway_data['gatewayFriendlyName']; ?></option>
																<?php } } ?>
                                                    </select>
													
                                                </div>
                                            </div>		
											
										<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Select Card</label>
                                                <div class="col-md-6">
												
                                                    <select id="CardID" name="CardID"  onchange="create_card_data();"  class="form-control">
                                                        <option value="" >Select Card</option>

                                                        
                                                    </select>
														
                                                </div>
                                            </div>
                                                
                                     <div class="form-group ">
                                              
												<label class="col-md-4 control-label" >Amount</label>
                                                <div class="col-md-6">
												
                                                    <input type="text" id="inv_amount" name="inv_amount"  class="form-control" value="" />
                                                      
														
                                                </div>
                                            </div>    
                       <div class="card_div" style="display:none;">
                           
                               
                       </div>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="invoiceProcessID" name="invoiceProcessID" class="form-control"  value="" />
                             <input type="hidden" id="customerProcessID" name="customerProcessID" class="form-control"  value="<?php echo $this->uri->segment('4'); ?>" />
                        </div>
                    </div>
                    
					
			       
                    <div class="pull-right">
        			 <input type="submit" id="qbd_process" name="btn_process" class="btn btn-sm btn-success" value="Process"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------------------  View email history Popup popup ------------------>









<!-- Load and execute javascript code used only in this page -->

<script>$(function(){ Pagination_view.init(); });</script>
<script>


var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('.page_details').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

$(function(){
    FormsWizard.init(); });
	$(function(){
        setTimeout(function() {
          jQuery('.alert').fadeOut('fast');
        }, 2000); 
    });
 $(function(){ 
$('.radio_pay').click(function(){
	  var method = $(this).val(); 
	   if(method==1)
	   {
		   $('#checkingform').hide();
		   $('#ccform').show();
	   }else if (method==2){
			 $('#checkingform').show();
		   $('#ccform').hide();
	   }else {
		   
		 }		   
   });
          
  $('#btn_mask').click(function(){
  $('#m_card_id').hide();  
  $('#edit_card_number').removeAttr('disabled');
  $('#edit_card_number').attr('type','text');
});


});





</script>
 
<script>
        
$(function(){     
		
					
	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	


 function set_url(){  
 
          
		    var gateway_value =$("#gateway").val();
		
          if(gateway_value > 0){
			  $.ajax({
				type:"POST",
			
					url : "<?php echo base_url(); ?>home/get_gateway_data",
				data : {'gatewayID':gateway_value },
				success : function(response){ 
				
				              data = $.parseJSON(response);
							 gtype  = 	data['gatewayType'];
							  if(gtype=='3')
							  {			
								var url   = base_url+'Payments/pay_invoice';
							  } if(gtype=='2'){
									var url   = base_url+'Payments/pay_invoice';
							 }if(gtype=='1'){
							   var url   = base_url+'Payments/pay_invoice';
							 } if(gtype=='4'){
							   var url   = base_url+'Payments/pay_invoice';
							 } if(gtype=='5'){
							   var url   = base_url+'Payments/pay_invoice';
							    $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											
											}).remove();
							   	 var form = $("#thest_pay");
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'stripeApiKey',
											'name': 'stripeApiKey',
											'value': data['gatewayUsername']
											}).appendTo(form);
							   
							 }  
							 
							 if(gtype=='6'){
							   var url   = base_url+'Payments/pay_invoice';
							 } if(gtype=='7'){
							   var url   = base_url+'Payments/pay_invoice';
							 }
				
				            $("#thest_pay").attr("action",url);
					}   
				   
			   });
			}			
			
			
	  }



	function set_invoice_process_id(id, cid, in_val)
{
       
       
	     $('#invoiceProcessID').val(id);
	    
         $('#thest_pay #qbo_check').remove();

	     $('<input>').attr({
		    type: 'hidden',
		    id: 'qbo_check',
		    name: 'qbo_check',
		     value: 'qbd_pay'
			}).appendTo('#thest_pay');

      $('#thest_pay #inv_amount').val(in_val);
		 
		if(cid!=""){
			$('#CardID').find('option').not(':first').remove();
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/check_vault",
				
				data : {'customerID':cid},
				success : function(response){
					
					     data=$.parseJSON(response);
 
					     if(data['status']=='success'){
						
                              var s=$('#CardID');
                               $(s).append('<option value="new1">New Card</option>');
							  var card1 = data['card'];
							    
							    for(var val in  card1) {
									
								  $("<option />", {value: card1[val]['CardID'], text: card1[val]['customerCardfriendlyName'] }).appendTo(s);
							    }
						
							 
					   }	   
					
				}
				
				
			});
			
		}	
		 
		 
	
	            

		 }
		 
		

 var card_daata=' <fieldset><div class="form-group"><label class="col-md-4 control-label" for="card_number">Credit Card Number</label> <div class="col-md-8"><input type="text" id="card_number11" name="card_number" class="form-control" placeholder="" autocomplete="off"></div></div><div class="form-group"><label class="col-md-4 control-label" for="expry">Expiry Month</label><div class="col-md-2"><select id="expiry11" name="expiry" class="form-control"><option value="01">JAN</option><option value="02">FEB</option><option value="03">MAR</option><option value="04">APR</option><option value="05">MAY</option><option value="06">JUN</option>  <option value="07">JUL</option><option value="08">AUG</option><option value="09">SEP</option><option value="10">OCT</option><option value="11">NOV</option><option value="12">DEC</option> </select></div><label class="col-md-3 control-label" for="expiry_year">Expiry Year</label><div class="col-md-3"><select id="expiry_year11" name="expiry_year" class="form-control">'+opt+'</select></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="ccv11" onblur="create_Token_stripe();" name="cvv" class="form-control" placeholder="" autocomplete="off" /></div></div><div class="form-group"><label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label><div class="col-md-8"><input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="" /></div></div></fieldset>'+
                    '<fieldset><legend>Billing Address</legend><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 1</label><div class="col-md-8"><input type="text" id="address1" name="address1" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Address Line 2</label><div class="col-md-8"><input type="text" id="address2" name="address2" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">City</label><div class="col-md-8"><input type="text" id="city" name="city" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">State/Province</label><div class="col-md-8"><input type="text" id="state" name="state" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">ZIP Code</label><div class="col-md-8"><input type="text" id="zipcode" name="zipcode" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Country</label><div class="col-md-8"><input type="text" id="country" name="country" class="form-control" value="" placeholder=""></div></div><div class="form-group"><label class="col-md-4 control-label" for="val_username">Phone Number</label><div class="col-md-8"><input type="text" id="contact" name="contact" class="form-control" value="" placeholder=""></div></div></fieldset>';

	
function create_card_data()
{

  
   if($('#CardID').val()=='new1')
   {
    $('.card_div').html('');
     $('.card_div').css('display','block');
    $('.card_div').html(card_daata);
   }else{
      $('.card_div').html('');
   }
}
	

 function set_card_user_data(id, name){
      
	   $('#customerID11').val(id);
	
 }
     


var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									required: true,
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                    
                    baddress1:{
						required:true,
					},
					
					edit_friendlyname:{
						required:true,
						 minlength: 3,
					},
				
                  
                },
              
            });
            
	$.validator.addMethod("phoneUS", function(phone_number, element) {
            return phone_number.match(/^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4,6}$/);
    }, "Please specify a valid phone number like as (XXX) XX-XXXX");
         		
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		  $('#editccform').css('display','none');
		 $('#editcheckingform').css('display','none');
		if(cardID!=""){
		    $('#edit_cardID').val(cardID);
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_edit_data",
				data : {'cardID':cardID},
				success : function(response){
					
					     data=$.parseJSON(response);
						if(data['status']=='success'){
							    
                                $('#edit_cardID').val(data['card']['CardID']);
					         if(data['card']['CardNo']!="" || data['card']['CardCVV']!="")
                                {
                                    $('#editccform').css('display','block');
    							    $('#edit_card_number').val(data['card']['CardNo']);
    							  
    								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
    							  	document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
    								 $('#m_card_id').show();  
                                   $('#edit_card_number').attr('disabled','disabled');
                                     $('#edit_card_number').attr('type','hidden');
                                     $('#m_card').html(data['card']['CardNo']);
    								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
    								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
                                }
                                else
                                {
                                       $('#editcheckingform').css('display','block');
        							    $('#edit_acc_number').val(data['card']['accountNumber']);
        								document.getElementById("edit_acc_name").value =data['card']['accountName'];
        								document.getElementById("edit_route_number").value =data['card']['routeNumber'];
        						
        								$('select[name="edit_secCode"]').find('option[value="'+data['card']['secCodeEntryMethod']+'"]').attr("selected",true);
        								$('select[name="edit_acct_type"]').find('option[value="'+data['card']['accountType']+'"]').attr("selected",true);
        								$('select[name="edit_acct_holder_type"]').find('option[value="'+data['card']['accountHolderType']+'"]').attr("selected",true);
                                    
                                }
						        $('#baddress1').val(data['card']['Billing_Addr1']);
							    $('#baddress2').val(data['card']['Billing_Addr2']);
							    $('#bcity').val(data['card']['Billing_City']);
							    $('#bstate').val(data['card']['Billing_State']);
							    $('#bcountry').val(data['card']['Billing_Country']);
							    $('#bcontact').val(data['card']['Billing_Contact']);
							    $('#bzipcode').val(data['card']['Billing_Zipcode']);
					
				}
				}
				
				
			});
			
		}	  
		   
	}
	$('#CardID').change(function(){
		var cardlID =  $(this).val();
	
		  if(cardlID!='' && cardlID !='new1' ){
			   $("#btn_process").attr("disabled", true);
			   $("#card_loader").show();
			$.ajax({
				type:"POST",
				url : base_url+'Payments/get_card_data',
				data : {'cardID':cardlID},
				success : function(response){
					
					 $("#card_loader").hide();
					     data=$.parseJSON(response);
						
					    if(data['status']=='success'){
					        $("#btn_process").attr("disabled", false);	
					        
					       
					        if(gtype=='5')
					        {
					            
						 var form = $("#thest_pay");	
                        
                         $('#thest_pay #number').remove();	
                         $('#thest_pay #exp_year').remove();	
                         $('#thest_pay #exp_month').remove();	
                         $('#thest_pay #cvc').remove();	
                        
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'number',
										'name': 'number',
										'value': data['card']['CardNo']
										}).appendTo(form);	
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_year',
										'name': 'exp_year',
										'value': data['card']['cardYear']
										}).appendTo(form);
										
						 $('<input>', {
										'type': 'hidden',
										'id'  : 'exp_month',
										'name': 'exp_month',
										'value': data['card']['cardMonth']
										}).appendTo(form);
										
                           $('<input>', {
										'type': 'hidden',
										'id'  : 'cvc',
										'name': 'cvc',
										'value': data['card']['CardCVV']
										}).appendTo(form);	
						
						
								var pub_key = $('#stripeApiKey').val();
                             if(pub_key){
									 Stripe.setPublishableKey(pub_key);
									 Stripe.createToken({
													number: $('#number').val(),
													cvc: $('#cvc').val(),
													exp_month: $('#exp_month').val(),
													exp_year: $('#exp_year').val()
												}, stripeResponseHandler_res);
                        }
									// Prevent the form from submitting with the default action
							 	
					        }	   
					
				}
				
				}	
			});
		  }
	});
	

</script>



<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}

</style>
 
</div>
