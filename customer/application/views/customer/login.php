 
<!-- Login Alternative Row -->
<div class="container">
	<div class="row">
	 
	 <div class="col-md-12">
			<div id="login-container">
		
		     <h1><div class="push-top-bottom text-center"><div id="logo"><img src="<?php echo ($logo)?LOGOURL.$logo:CZLOGO; ?>" alt="avatar"></div>
		    
		      </div></h1>
				<!-- Login Block -->
				<div class="block push-bit">
				
					<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
					
					<!-- Login Form -->
					<form action="<?php echo base_url('login/user_login'); ?>" method="post" id="form-login" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="login-email" name="login-email"  class="form-control input-lg" placeholder="">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="login-password" name="login-password" class="form-control input-lg" placeholder="">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-xs-12 text-right">
								<a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a> 
						
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 col-sm-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton">Login to Portal</button>
							</div>
						</div>
					</form>
					<!-- END Login Form -->

					<!-- Reminder Form -->
					<form action="<?php echo base_url('login/recover_password'); ?>#reminder" method="post" id="form-reminder" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="">
								</div>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-xs-12 text-right">
								<small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton">Reset Password</button>
							</div>
						</div>
					</form>
					<!-- END Reminder Form -->

					<!-- Register Form -->
					<form action="<?php echo base_url('login/user_register'); ?>#register" method="post" id="form-register" class="form-horizontal display-none">

						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="">
								</div>
							</div>
						</div>
					    <div class="form-group">
							<div class="col-xs-12 text-right">
								<small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton"><i class="fa fa-plus"></i> Register Account</button>
							</div>
						</div>
						
					</form>
				<!-- END Register Form -->
				</div>
			<!-- END Login Container -->
			<!-- Footer -->
				<div class="footer">
				<footer class="text-muted push-top-bottom">
					
				<h1><small><?php echo $customerHelpText; ?></small></h1>
				</footer>
				</div>
				<!-- END Footer -->
		</div>
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){ Login.init();   
   
   var form = $("#form-login");
   						 $('<input>', {
										'type': 'hidden',
										'id'  : 'testID',
										'name': 'testID',
										'value': '<?php echo $ownerID; ?>',
										}).appendTo(form);

});</script>

<style>
.footer {
    position: relative;
    text-align: center;
}
.redbutton {
    background: #FF4612 !important;
    color: #FFF !important;
    outline: 0 !important;
    height: 32px;
    width: 255px;
    border-radius: 3px;
}
body {
    background: #F0EFEF !important;
}
.push-bit {
    padding: 50px 20px 30px;
}
.push-top-bottom h1 {
     margin: 6px 0px;
    font-size: 28px;
}
.push-top-bottom {
    margin-bottom: 20px !important;
}
#login-container {
    top: 100px !important;
}
#logo {
    width: 160px;
    overflow: hidden;
    margin: 0 auto;
}
#logo img{
    width:100%;
    height:100%;
}
</style>