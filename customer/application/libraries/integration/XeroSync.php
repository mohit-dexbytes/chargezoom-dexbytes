<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require_once dirname(__FILE__) . '/../../../../vendor/autoload.php';

class XeroSync
{
    private $xero_config = array();
    private $merchantID;
    private $tokenData = array();
    protected $CI;

    public function __construct($merchID)
    {
        $this->CI         = &get_instance();
        $this->merchantID = $merchID;
        $this->CI->load->model('General_model', 'general_model');

        $this->CI->load->config('xero');
        $this->xero_config = array(
            'urlAuthorize'            => $this->CI->config->item('urlAuthorize'),
            'urlAccessToken'          => $this->CI->config->item('urlAccessToken'),
            'urlResourceOwnerDetails' => $this->CI->config->item('urlResourceOwnerDetails'),
            'clientId'                => $this->CI->config->item('clientId'),
            'clientSecret'            => $this->CI->config->item('urlResourceOwnerDetails'),
            'redirectUri'             => $this->CI->config->item('redirectUri'),
        );

        $this->tokenData = $this->CI->general_model->get_row_data('tbl_xero_token', array('merchantID' => $this->merchantID));
    }

    private function set_last_updated_run($dateTime, $rawDateTime, $sync_action)
    {
        $syncWhere = array('merchantID' => $this->merchantID, 'sync_action' => $sync_action);
        $lastData  = $this->CI->general_model->get_row_data('last_sync_history', $syncWhere);

        $lastRunData = [
            'merchantID'     => $this->merchantID,
            'sync_action'    => $sync_action,
            'lastUpdated'    => $dateTime,
            'lastUpdatedRaw' => $rawDateTime,
            'updatedAt'      => date("Y-m-d H:i:s"),
            'app_type'       => 4,
        ];

        if ($lastData) {
            $this->CI->general_model->update_row_data('last_sync_history', $syncWhere, $lastRunData);
        } else {
            $lastRunData['createdAt'] = date("Y-m-d H:i:s");
            $this->CI->general_model->insert_row('last_sync_history', $lastRunData);
        }
    }

    private function get_last_updated_run($sync_action)
    {
        $syncWhere = array('merchantID' => $this->merchantID, 'sync_action' => $sync_action);
        $lastData  = $this->CI->general_model->get_row_data('last_sync_history', $syncWhere);
        return $lastData;
    }

    private function convertFromXeroDate($rawDateTime)
    {
        $date    = explode('+', $rawDateTime);
        $newTime = substr($date[0], 6);
        $newTime = rtrim($newTime, "/");
        $newTime = rtrim($newTime, ")");
        return ($newTime) / 1000;
    }

    public function sycnCustomer($args = [])
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $lastRun           = $this->get_last_updated_run(LAST_ALL_CUSTOMER_SYNC);
        if ($lastRun) {
            //2021-06-01T11:18:43.202-08:00
            $if_modified_since = date("c", $lastRun['lastUpdated']);
        }

        $where            = ''; // string | Filter by an any element
        $order            = 'Name ASC'; // string | Order by an any element
        $i_ds             = ''; // string[] | Filter by a comma separated list of ContactIDs. Allows you to retrieve a specific set of contacts in a single call.
        $page             = 1; // int | e.g. page=1 - Up to 100 contacts will be returned in a single API call.
        $include_archived = true; // bool | e.g. includeArchived=true - Contacts with a status of ARCHIVED will be included in the response
        $summary_only     = false; // bool | Use summaryOnly=true in GET Contacts endpoint to retrieve a smaller version of the response object. This returns only lightweight fields, excluding computation-heavy fields from the response, making the API calls quick and efficient.

        $keepRunning   = true;
        $entities_data = [];

        $isSynced = 0;
        $error = "Synced Successfull";
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            while ($keepRunning === true) {
                $apiResponse = $accountingApi->getContacts($this->tokenData['tenant_id'], $if_modified_since, $where, $order, $i_ds, $page, $include_archived, $summary_only);
                $contactList = $apiResponse->getContacts();
                if (!empty($contactList)) {
                    $entities_data = array_merge($contactList, $entities_data);
                    $page          = $page + 1;
                } else {
                    $keepRunning = false;
                    break;
                }

            }
            $isSynced = 1;

        } catch (Exception $e) {
            
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($entities_data)) {
            $this->storeContact($entities_data, false, true);
        }

        $logData = [
            'action' => SYNC_CUSTOMER,
            'status' => $isSynced,
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);
    }

    public function sycnCustomerById($contact_id)
    {
        $accessToken   = $this->tokenData['access_token'];
        $entities_data = [];
        $config        = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $error = "Customer Detail Sync";
        $isSynced = 0;
        try {
            $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            $entities_data = $apiInstance->getContact($this->tokenData['tenant_id'], $contact_id);

            $isSynced = 1;
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($entities_data)) {
            $this->storeContact($entities_data, $contact_id, false);
        }

        $logData = [
            'action' => SYNC_CUSTOMER,
            'status' => $isSynced,
            'xeroActionID' => $contact_id,
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);

    }

    public function createUpdateContact($contactData = [], $xeroContactId = '')
    {

        $data = [
            "ContactID"     => $xeroContactId,
            "ContactStatus" => "ACTIVE",
            "Name"          => $contactData['fullName'],
            "FirstName"     => $contactData['firstName'],
            "LastName"      => $contactData['lastName'],
            "EmailAddress"  => $contactData['userEmail'],
            "Addresses"     => [
                [
                    "AddressType"  => "STREET",
                    "AddressLine1" => $contactData['address1'],
                    "AddressLine2" => $contactData['address2'],
                    "City"         => $contactData['City'],
                    "Region"       => $contactData['State'],
                    "PostalCode"   => $contactData['zipcode'],
                    "Country"      => $contactData['Country'],
                ],
                [
                    "AddressType"  => "POBOX",
                    "AddressLine1" => $contactData['ship_address1'],
                    "AddressLine2" => $contactData['ship_address2'],
                    "City"         => $contactData['ship_city'],
                    "Region"       => $contactData['ship_state'],
                    "PostalCode"   => $contactData['ship_zipcode'],
                    "Country"      => $contactData['ship_country'],
                ],
            ],
            "Phones"        => [
                [
                    "PhoneType"   => "DEFAULT",
                    "PhoneNumber" => $contactData['phoneNumber'],
                    // "PhoneAreaCode"    => "",
                    // "PhoneCountryCode" => "",
                ],
            ],
        ];

        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $isSynced = 0;
        $error = "Customer";
        $action = (empty($xeroContactId)) ? CREATE_CUSTOMER : UPDATE_CUSTOMER;
        $return = false;
        try {
            $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            if (!empty($xeroContactId)) {
                $error = "$error Updated";
                $result = $apiInstance->updateContact($this->tokenData['tenant_id'], $xeroContactId, $data);
            } else {
                $error = "$error Created";
                unset($data['ContactID']);
                $result = $apiInstance->createContacts($this->tokenData['tenant_id'], $data, true);
            }
            if ($result && !empty($result)) {
                $ids = $this->storeContact($result, $xeroContactId, false);
                $xeroContactId = $ids[0];
                $return = $xeroContactId;
            }

            $isSynced = 1;
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        $logData = [
            'action' => $action,
            'status' => $isSynced,
            'xeroActionID' => $xeroContactId,
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);

        return $return;
    }

    private function storeContact($syncedContactList, $xeroContactId = false, $updateLastRun = true)
    {
        $whereContact = array('merchantID' => $this->merchantID);
        if ($xeroContactId) {
            $whereContact['Customer_ListID'] = $xeroContactId;
        }

        $customerList = $this->CI->general_model->get_table_data('Xero_custom_customer', $whereContact);
        $newMax       = $rawDateTime       = 0;

        $syncID = [];
        foreach ($syncedContactList as $oneCustomer) {
            $customerID = false;

            $syncID[] = $newCustomerListId = $oneCustomer->getContactID();
            if (!empty($customerList)) {
                foreach ($customerList as $key => $existedCustomer) {
                    if ($existedCustomer['Customer_ListID'] == $newCustomerListId) {
                        $customerID = $existedCustomer['customerID'];
                        unset($customerList[$key]);
                    }
                }
            }

            $XERO_customer_details = [
                'Customer_ListID' => $newCustomerListId,
                'firstName'       => $oneCustomer->getFirstName(),
                'lastName'        => $oneCustomer->getLastName(),
                'fullName'        => $oneCustomer->getName(),
                'userEmail'       => $oneCustomer->getEmailAddress(),
                'merchantID'      => $this->merchantID,
            ];

            $rawDateTime = $oneCustomer->getUpdatedDateUtc();
            $newTime     = $this->convertFromXeroDate($rawDateTime);

            if ($newMax < $newTime) {
                $newMax         = $newTime;
                $maxrawDateTime = $rawDateTime;
            }

            $addresses = $oneCustomer->getAddresses();
            if (!empty($addresses)) {
                foreach ($addresses as $address) {
                    $addressArray = [];
                    if ($address->getAddressType() == 'STREET') {
                        $addressArray = [
                            'address1' => $address->getAddressLine1(),
                            'address2' => $address->getAddressLine2(),
                            'City'     => $address->getCity(),
                            'State'    => $address->getRegion(),
                            'Country'  => $address->getCountry(),
                            'zipCode'  => $address->getPostalCode(),
                        ];
                    } else if ($address->getAddressType() == 'POBOX') {
                        $addressArray = [
                            'ship_address1' => $address->getAddressLine1(),
                            'ship_address2' => $address->getAddressLine2(),
                            'ship_city'     => $address->getCity(),
                            'ship_state'    => $address->getRegion(),
                            'ship_country'  => $address->getCountry(),
                            'ship_zipcode'  => $address->getPostalCode(),
                        ];
                    }

                    $XERO_customer_details = array_merge($XERO_customer_details, $addressArray);
                }
            }

            $phones = $oneCustomer->getPhones();
            if (!empty($phones)) {
                foreach ($phones as $phone) {
                    if ($phone->getPhoneType() == 'DEFAULT') {
                        $XERO_customer_details['phoneNumber'] = $phone->getPhoneCountryCode() . $phone->getPhoneAreaCode() . $phone->getPhoneNumber();
                    }
                }
            }

            if ($customerID) {
                $XERO_customer_details['updatedAt'] = date("Y-m-d H:i:s");
                $this->CI->general_model->update_row_data('Xero_custom_customer', array('customerID' => $customerID), $XERO_customer_details);
            } else {
                $XERO_customer_details['updatedAt'] = date("Y-m-d H:i:s");
                $XERO_customer_details['createdAt'] = date("Y-m-d H:i:s");
                $this->CI->general_model->insert_row('Xero_custom_customer', $XERO_customer_details);
            }

        }

        if ($newMax > 0 && $updateLastRun) {
            $setLastRun = $this->set_last_updated_run($newMax, $maxrawDateTime, LAST_ALL_CUSTOMER_SYNC);
        }

        return $syncID;
    }

    public function syncItems()
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where             = ''; // string | Filter by an any element
        $order             = 'Code ASC'; // string | Order by an any element
        $unitdp            = 4; // int | e.g. unitdp=4 – (Unit Decimal Places) You can opt in to use four decimal places for unit amounts

        $keepRunning   = true;
        $entities_data = [];

        $isSynced = 0;
        $error = "Sync Items";
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            $apiResponse   = $accountingApi->getItems($this->tokenData['tenant_id'], $if_modified_since, $where, $order, $unitdp);
            $entities_data = $apiResponse->getItems();
            $isSynced = 1;

        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        // echo '<pre>';
        // print_r($entities_data);die;
        if (!empty($entities_data)) {
            $this->storeItems($entities_data);
        }

        $logData = [
            'action' => SYNC_ITEM,
            'status' => $isSynced,
            'xeroActionID' => '',
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);
    }

    public function createUpdateItem($itemData = [], $itemID = '')
    {

        $data = [
            "Code"         => $itemData['productCode'],
            "Name"         => $itemData['productName'],
            "Description"  => $itemData['proDescription'],
            'SalesDetails' => [
                'UnitPrice'   => $itemData['averageCost'],
                'AccountCode' => $itemData['AccountCode'],
            ],
        ];

        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $isSynced = 0;

        $action = (empty($itemID)) ? CREATE_ITEM : UPDATE_ITEM;
        $error = (empty($itemID)) ? "Item Create" : "Item Update";

        $return = false;
        try {
            $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            $result = $apiInstance->updateOrCreateItems($this->tokenData['tenant_id'], $data, 4, true);
            if ($result && !empty($result)) {
                $syncID = $this->storeItems($result);
                $itemID = $syncID[0];
                $return = true;
            }

            $isSynced = 1;
        } catch (Exception $e) {
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        $logData = [
            'action' => $action,
            'status' => $isSynced,
            'xeroActionID' => $itemID,
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);
        
        return $return;
    }

    private function storeItems($syncedItemList, $xeroItemId = false, $updateLastRun = true)
    {
        $syncIDs = [];
        $whereItem = array('merchantID' => $this->merchantID);
        if ($xeroItemId) {
            $whereItem['productID'] = $xeroItemId;
        }

        $itemList = $this->CI->general_model->get_table_data('Xero_test_item', $whereItem);
        $newMax   = $rawDateTime   = 0;

        foreach ($syncedItemList as $oneItem) {
            $ListID = false;

            $syncIDs[] = $newItemId = $oneItem->getItemID();
            if (!empty($itemList)) {
                foreach ($itemList as $key => $existedItem) {
                    if ($existedItem['productID'] == $newItemId) {
                        $ListID = $existedItem['ListID'];
                        unset($itemList[$key]);
                    }
                }
            }

            $rawDateTime = $oneItem->getUpdatedDateUtc();
            $newTime     = $this->convertFromXeroDate($rawDateTime);

            if ($newMax < $newTime) {
                $newMax         = $newTime;
                $maxrawDateTime = $rawDateTime;
            }

            $XERO_item_details = [
                'productID'        => $newItemId,
                'Code'             => $oneItem->getCode(),
                'Name'             => $oneItem->getName(),
                'SalesDescription' => $oneItem->getDescription(),
                'QuantityOnHand'   => $oneItem->getQuantityOnHand(),
                'saleCost'         => $oneItem->getSalesDetails()->getUnitPrice(),
                'AccountCode'      => $oneItem->getSalesDetails()->getAccountCode(),
                'merchantID'       => $this->merchantID,
                'TimeModified'     => date("Y-m-d H:i:s"),
                'IsActive'         => 'true',
            ];

            if ($ListID) {
                $this->CI->general_model->update_row_data('Xero_test_item', array('ListID' => $ListID), $XERO_item_details);
            } else {
                $this->CI->general_model->insert_row('Xero_test_item', $XERO_item_details);
            }

        }

        if ($newMax > 0 && $updateLastRun) {
            $setLastRun = $this->set_last_updated_run($newMax, $maxrawDateTime, LAST_ALL_ITEM_SYNC);
        }

        return $syncIDs;
    }

    public function syncAccounts($args = [])
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where             = 'Status=="ACTIVE"'; // string | Filter by an any element
        $order             = 'Name ASC'; // string | Order by an any element

        $lastRun = $this->get_last_updated_run(LAST_ALL_ACCOUNT_SYNC);
        if ($lastRun) {
            //2021-06-01T11:18:43.202-08:00
            $if_modified_since = date("c", $lastRun['lastUpdated']);
        }

        $updateLastRun = true;
        if (!empty($args)) {
            $if_modified_since = '';
            $updateLastRun     = false;

            if (isset($args['EnablePaymentsToAccount'])) {
                $isEnable = ($args['EnablePaymentsToAccount']) ? 'true' : 'false';
                $where .= ' AND EnablePaymentsToAccount==' . $isEnable;
            }
        }

        $keepRunning   = true;
        $entities_data = [];

        $isSynced = 0;
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            $apiResponse   = $accountingApi->getAccounts($this->tokenData['tenant_id'], $if_modified_since, $where, $order);
            $entities_data = $apiResponse->getAccounts();
            $isSynced = 1;
            $error = "Account Sync";
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($entities_data)) {
            $this->storeAccounts($entities_data, false, $updateLastRun);
        }

        $logData = [
            'action' => SYNC_ACCOUNT,
            'status' => $isSynced,
            'xeroActionID' => '',
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);
    }

    private function storeAccounts($syncedAccountList, $xeroAccountId = false, $updateLastRun = true)
    {
        $whereAccount = array('merchantID' => $this->merchantID);
        if ($xeroAccountId) {
            $whereAccount['accountID'] = $xeroAccountId;
        }

        $accountList = $this->CI->general_model->get_table_data('tbl_xero_accounts', $whereAccount);
        $newMax      = $rawDateTime      = 0;

        foreach ($syncedAccountList as $oneAccount) {
            $acID = false;

            $newAccountId = $oneAccount->getAccountID();
            if (!empty($accountList)) {
                foreach ($accountList as $key => $existedId) {
                    if ($existedId['accountID'] == $newAccountId) {
                        $acID = $existedId['acID'];
                        unset($accountList[$key]);
                    }
                }
            }

            $XERO_account_details = [
                'accountID'          => $newAccountId,
                'accountCode'        => $oneAccount->getCode(),
                'accountName'        => $oneAccount->getName(),
                'status'             => $oneAccount->getStatus(),
                'class'              => $oneAccount->getClass(),
                'accountDescription' => $oneAccount->getDescription(),
                'merchantID'         => $this->merchantID,
                'updatedAt'          => date("Y-m-d H:i:s"),
                'isPaymentAccepted'  => ($oneAccount->getEnablePaymentsToAccount()) ? 1 : 0,
            ];

            $rawDateTime = $oneAccount->getUpdatedDateUtc();
            $newTime     = $this->convertFromXeroDate($rawDateTime);

            if ($newMax < $newTime) {
                $newMax         = $newTime;
                $maxrawDateTime = $rawDateTime;
            }

            if ($acID) {
                $this->CI->general_model->update_row_data('tbl_xero_accounts', array('acID' => $acID), $XERO_account_details);
            } else {
                $XERO_account_details['createdAt'] = date("Y-m-d H:i:s");
                $this->CI->general_model->insert_row('tbl_xero_accounts', $XERO_account_details);
            }

        }

        if ($newMax > 0 && $updateLastRun) {
            $setLastRun = $this->set_last_updated_run($newMax, $maxrawDateTime, LAST_ALL_ACCOUNT_SYNC);
        }
    }

    public function syncTaxRates()
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $where    = 'Status=="ACTIVE"'; // string | Filter by an any element
        $order    = 'Name ASC'; // string | Order by an any element
        $tax_type = ''; // string | Filter by tax type

        $entities_data = [];
        $isSynced = 0;
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            $apiResponse   = $accountingApi->getTaxRates($this->tokenData['tenant_id'], $where, $order, $tax_type);
            $entities_data = $apiResponse->getTaxRates();
            $isSynced = 1;
            $error = "Tax Data Sync";
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($entities_data)) {
            $this->storeTaxRates($entities_data, false, false);
        }

        $logData = [
            'action' => SYNC_TAX,
            'status' => $isSynced,
            'xeroActionID' => '',
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);
    }

    private function storeTaxRates($syncedTaxList, $xeroTaxID = false, $updateLastRun = true)
    {
        $whereTax = array('merchantID' => $this->merchantID);
        if ($xeroTaxID) {
            $whereTax['taxID'] = $xeroTaxID;
        }

        $taxList = $this->CI->general_model->get_table_data('tbl_taxes_xero', $whereTax);
        $newMax  = $rawDateTime  = 0;

        foreach ($syncedTaxList as $oneTax) {
            $xerotaxID = false;

            $newTaxId = $oneTax->getTaxType();
            if (!empty($taxList)) {
                foreach ($taxList as $key => $existedId) {
                    if ($existedId['taxID'] == $newTaxId) {
                        $xerotaxID = $existedId['xerotaxID'];
                        unset($taxList[$key]);
                    }
                }
            }

            $componentName = '';
            $rate          = 0;

            $XERO_tax_data = [
                'taxID'          => $newTaxId,
                'friendlyName'   => $oneTax->getName(),
                'taxRate'        => $oneTax->getEffectiveRate(),
                'displayTaxRate' => $oneTax->getDisplayTaxRate(),
                'componentName'  => $oneTax->getName(),
                'Status'         => $oneTax->getStatus(),
                'merchantID'     => $this->merchantID,
            ];

            if ($xerotaxID) {
                $this->CI->general_model->update_row_data('tbl_taxes_xero', array('xerotaxID' => $xerotaxID), $XERO_tax_data);
            } else {
                $this->CI->general_model->insert_row('tbl_taxes_xero', $XERO_tax_data);
            }

        }
    }

    public function syncInvoices($args = [], $initSync = false)
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where             = ''; // string | Filter by an any element
        $order             = 'InvoiceNumber ASC'; // string | Order by an any element
        $i_ds              = ''; // string[] | Filter by a comma-separated list of InvoicesIDs.
        $invoice_numbers   = ''; // string[] | Filter by a comma-separated list of InvoiceNumbers.
        $contact_i_ds      = ''; // string[] | Filter by a comma-separated list of ContactIDs.
        $statuses          = ''; // string[] | Filter by a comma-separated list Statuses. For faster response times we recommend using these explicit parameters instead of passing OR conditions into the Where filter.
        $page              = 1; // int | e.g. page=1 – Up to 100 invoices will be returned in a single API call with line items shown for each invoice
        $include_archived  = true; // bool | e.g. includeArchived=true - Invoices with a status of ARCHIVED will be included in the response
        $created_by_my_app = false; // bool | When set to true you'll only retrieve Invoices created by your app
        $unitdp            = 4; // int | e.g. unitdp=4 – (Unit Decimal Places) You can opt in to use four decimal places for unit amounts
        $summary_only      = true; // bool | Use summaryOnly=true in GET Contacts and Invoices endpoint to retrieve a smaller version of the response object. This returns only lightweight fields, excluding computation-heavy fields from the response, making the API calls quick and efficient.

        $updateLastRun = true;
        $whereArray    = [];
        if (!$initSync) {
            $lastRun = $this->get_last_updated_run(LAST_ALL_INVOICE_SYNC);
            if ($lastRun) {
                //2021-06-01T11:18:43.202-08:00
                $if_modified_since = date("c", $lastRun['lastUpdated']);
            }
        } else {
            $where = 'AmountDue > 0';
        }

        if (isset($args['ContactID'])) {
            $ContactID    = $args['ContactID'];
            $whereArray[] = 'ContactID=="' . $ContactID . '"';
        }

        $InvoiceID = false;
        if (isset($args['InvoiceID'])) {
            $InvoiceID    = $args['InvoiceID'];
            $whereArray[] = 'InvoiceID="' . $InvoiceID . '"';
        }

        if (!empty($whereArray)) {
            $where         = implode(" AND ", $whereArray);
            $updateLastRun = false;
        }

        $keepRunning   = true;
        $entities_data = [];
        $isSynced = 0;

        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            while ($keepRunning === true) {
                $apiResponse = $accountingApi->getInvoices($this->tokenData['tenant_id'], $if_modified_since, $where, $order, $i_ds, $invoice_numbers, $contact_i_ds, $statuses, $page, $include_archived, $created_by_my_app, $unitdp, $summary_only);
                $contactList = $apiResponse->getInvoices();
                if (!empty($contactList)) {
                    $entities_data = array_merge($contactList, $entities_data);
                    $page          = $page + 1;
                } else {
                    $keepRunning = false;
                    break;
                }
            }
            $isSynced = 1;
            $error = "Invoice Sync";
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($entities_data)) {
            $this->storeInvoices($entities_data, $InvoiceID, $updateLastRun);
        }

        $logData = [
            'action' => SYNC_INVOICE,
            'status' => $isSynced,
            'xeroActionID' => '',
            'message' => $error,
            'syncFromCZ' => false,
        ];
        xero_sync_log($logData, $this->merchantID);

    }

    private function storeInvoices($syncedInvoices, $xeroInvoiceId = false, $updateLastRun = true)
    {
        $whereInvoice = array('merchantID' => $this->merchantID);
        if ($xeroInvoiceId) {
            $whereInvoice['invoiceID'] = $xeroInvoiceId;
        }

        $invoiceList = $this->CI->general_model->get_table_data('Xero_test_invoice', $whereInvoice);
        $newMax      = $rawDateTime      = 0;

        $invoiceIDS = [];

        foreach ($syncedInvoices as $oneInvoice) {
            $xeroinvoiceID = false;

            $newInvoiceId = $oneInvoice->getInvoiceID();
            if (!empty($invoiceList)) {
                foreach ($invoiceList as $key => $existedId) {
                    if ($existedId['invoiceID'] == $newInvoiceId) {
                        $xeroinvoiceID = $existedId['xeroinvoiceID'];
                        unset($invoiceList[$key]);
                    }
                }
            }

            $invoiceIDS[] = $newInvoiceId;

            $time_created = $this->convertFromXeroDate($oneInvoice->getDate());
            $due_date     = $this->convertFromXeroDate($oneInvoice->getDueDate());

            $invContact    = $oneInvoice->getContact();
            $xeroContactId = $invContact->getContactID();
            $this->storeContact($invContact, $xeroContactId, false);

            $XERO_invoice_details = [
                'invoiceID'        => $newInvoiceId,
                'CustomerListID'   => $xeroContactId,
                'CustomerFullName' => $invContact->getName(),
                'TimeCreated'      => date("Y-m-d H:i:s", $time_created),
                'DueDate'          => date("Y-m-d H:i:s", $due_date),
                'refNumber'        => $oneInvoice->getInvoiceNumber(),
                'AppliedAmount'    => $oneInvoice->getAmountPaid(),
                'BalanceRemaining' => $oneInvoice->getAmountDue(),
                'Total_payment'    => $oneInvoice->getTotal(),
                'merchantID'       => $this->merchantID,
                'totalTax'         => $oneInvoice->getTotalTax(),
                'lineAmountType'   => $oneInvoice->getLineAmountTypes(),
            ];

            $XERO_invoice_details['IsPaid'] = 'false';
            if ($XERO_invoice_details['BalanceRemaining'] <= 0) {
                $XERO_invoice_details['IsPaid'] = 'true';
            }

            $invStatus = $oneInvoice->getStatus();
            if ($invStatus == 'VOIDED') {
                $XERO_invoice_details['IsPaid']     = 'false';
                $XERO_invoice_details['userStatus'] = 'Cancel';
            }

            $rawDateTime = $oneInvoice->getUpdatedDateUtc();
            $newTime     = $this->convertFromXeroDate($rawDateTime);

            if ($newMax < $newTime) {
                $newMax         = $newTime;
                $maxrawDateTime = $rawDateTime;
            }

            if ($xeroinvoiceID) {
                $this->CI->general_model->update_row_data('Xero_test_invoice', array('xeroinvoiceID' => $xeroinvoiceID), $XERO_invoice_details);
            } else {
                $this->CI->general_model->insert_row('Xero_test_invoice', $XERO_invoice_details);
            }

            $this->storeInvoiceItem($oneInvoice->getLineItems(), $newInvoiceId);

        }

        if ($newMax > 0 && $updateLastRun) {
            $setLastRun = $this->set_last_updated_run($newMax, $maxrawDateTime, LAST_ALL_INVOICE_SYNC);
        }

        return $invoiceIDS;
    }

    private function storeInvoiceItem($newInvoiceItems, $xeroInvoiceId)
    {
        $whereInvoiceItem = array(
            'merchantID' => $this->merchantID,
            'invoiceID'  => $xeroInvoiceId,
        );

        $invoiceItemList = $this->CI->general_model->delete_row_data('tbl_xero_invoice_item', $whereInvoiceItem);
        foreach ($newInvoiceItems as $singleItem) {
            $itemLineID = false;

            $newitemID = $singleItem->getLineItemID();

            $new_invoice_item = [
                'invoiceID'       => $xeroInvoiceId,
                'itemID'          => $newitemID,
                'merchantID'      => $this->merchantID,
                'itemQty'         => $singleItem->getQuantity(),
                'itemPrice'       => $singleItem->getUnitAmount(),
                'item_code'       => $singleItem->getItemCode(),
                'itemDescription' => $singleItem->getDescription(),
                'taxType'         => $singleItem->getTaxType(),
                'taxAmount'       => $singleItem->getTaxAmount(),
            ];

            if ($itemLineID) {
                $this->CI->general_model->update_row_data('tbl_xero_invoice_item', array('itemLineID' => $itemLineID), $new_invoice_item);
            } else {
                $this->CI->general_model->insert_row('tbl_xero_invoice_item', $new_invoice_item);
            }
        }
    }

    public function createUpdateInvoice($requestData = [], $xeroInvoiceId = false)
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $isSynced = 0;

        $action = (empty($xeroInvoiceId)) ? CREATE_ITEM : UPDATE_ITEM;
        $error = (empty($xeroInvoiceId)) ? "Invoice Create" : "Invoice Update";

        $invoiceData = [
            // "InvoiceId" => $xeroInvoiceId,
            "Type"    => "ACCREC",
            "Status"  => "AUTHORISED",
            "Contact" => [
                "ContactID" => $requestData['customerID'],
            ],
            "Date"    => $requestData['invoiceDate'],
            "DueDate" => $requestData['invoiceDueDate'],
        ];

        if (isset($requestData['refNumber']) && !empty($requestData['refNumber'])) {
            $invoiceData['InvoiceNumber'] = $requestData['refNumber'];
        }

        $taxes   = false;
        $taxType = 'NoTax';
        if (isset($requestData['taxes']) && !empty($requestData['taxes'])) {
            $taxes   = true;
            $taxType = ($requestData['taxes'] == 1) ? 'Exclusive' : 'Inclusive';
        }
        $invoiceData['LineAmountTypes'] = $taxType;

        $item_val = [];
        foreach ($requestData['productID'] as $key => $prod) {
            if (isset($requestData['xeroitemID'][$key]) && !empty($requestData['xeroitemID'][$key])) {
                $insert_row['LineItemID'] = $requestData['xeroitemID'][$key];
            }

            $insert_row['ItemCode']    = $requestData['productID'][$key];
            $insert_row['Quantity']    = $requestData['quantity'][$key];
            $insert_row['AccountCode'] = ($requestData['AccountCode'][$key]) ? $requestData['AccountCode'][$key] : '4000';
            $insert_row['UnitAmount']  = $requestData['unit_rate'][$key];
            $insert_row['Description'] = $requestData['description'][$key];
            $insert_row['TaxType']     = ($taxes && isset($requestData['ptaxID'][$key]) && $requestData['ptaxID'][$key]) ? $requestData['ptaxID'][$key] : "NONE";

            $LineAmount               = $requestData['quantity'][$key] * $requestData['unit_rate'][$key];
            $insert_row['LineAmount'] = $LineAmount;

            $item_val[] = $insert_row;
        }

        $invoiceData['LineItems'] = $item_val;

        $return = false;
        try {
            $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            if ($xeroInvoiceId) {
                $result = $apiInstance->updateInvoice($this->tokenData['tenant_id'], $xeroInvoiceId, $invoiceData, 4);
            } else {
                $result = $apiInstance->createInvoices($this->tokenData['tenant_id'], $invoiceData, true, 4);
            }
            if ($result && !empty($result)) {
                $return = $this->storeInvoices($result, $xeroInvoiceId, false);
                $xeroInvoiceId = $return[0];
            }
            $isSynced = 1;
        } catch (Exception $e) {
            echo '<pre>';
            print_r([$invoiceData, $e]);die;
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        $logData = [
            'action' => $action,
            'status' => $isSynced,
            'xeroActionID' => $xeroInvoiceId,
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);

        return $return;
    }

    public function voidInvoice($xeroInvoiceId)
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $invoiceData = [
            "Status" => "VOIDED",
        ];

        $isSynced = 0;
        $return = false;
        try {
            $apiInstance = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            if ($xeroInvoiceId != '') {
                $result = $apiInstance->updateInvoice($this->tokenData['tenant_id'], $xeroInvoiceId, $invoiceData, 4);
            }

            if ($result && !empty($result)) {
                $return = $this->storeInvoices($result, $xeroInvoiceId, false);
            }
            $isSynced = 1;
            $error = "Invoice voided";
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        $logData = [
            'action' => VOID_INVOICE,
            'status' => $isSynced,
            'xeroActionID' => $xeroInvoiceId,
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);

        return $return;
    }

    public function syncAllData($initSync)
    {
        $this->sycnCustomer();
        $this->syncAccounts();
        $this->syncTaxRates();
        $this->syncItems();
        $this->syncInvoices([], $initSync);
    }

    public function isConnected()
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $if_modified_since = ''; // \DateTime | Only records created or modified since this timestamp will be returned
        $where             = ''; // string | Filter by an any element
        $order             = 'InvoiceNumber ASC'; // string | Order by an any element
        $i_ds              = ''; // string[] | Filter by a comma-separated list of InvoicesIDs.
        $invoice_numbers   = ''; // string[] | Filter by a comma-separated list of InvoiceNumbers.
        $contact_i_ds      = ''; // string[] | Filter by a comma-separated list of ContactIDs.
        $statuses          = ''; // string[] | Filter by a comma-separated list Statuses. For faster response times we recommend using these explicit parameters instead of passing OR conditions into the Where filter.
        $page              = 1; // int | e.g. page=1 – Up to 100 invoices will be returned in a single API call with line items shown for each invoice
        $include_archived  = true; // bool | e.g. includeArchived=true - Invoices with a status of ARCHIVED will be included in the response
        $created_by_my_app = false; // bool | When set to true you'll only retrieve Invoices created by your app
        $unitdp            = 4; // int | e.g. unitdp=4 – (Unit Decimal Places) You can opt in to use four decimal places for unit amounts
        $summary_only      = true; // bool | Use summaryOnly=true in GET Contacts and Invoices endpoint to retrieve a smaller version of the response object. This returns only lightweight fields, excluding computation-heavy fields from the response, making the API calls quick and efficient.

        $if_modified_since = date("c");

        $isConnected = false;
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );
            $apiResponse = $accountingApi->getInvoices($this->tokenData['tenant_id'], $if_modified_since, $where, $order, $i_ds, $invoice_numbers, $contact_i_ds, $statuses, $page, $include_archived, $created_by_my_app, $unitdp, $summary_only);
            $isConnected = true;

        } catch (Exception $e) {
            $isConnected = ($e->getCode() == 401) ? false : true;
            $error       = 'Exception when calling AccountingApi->getContact: ' . $e->getMessage() . PHP_EOL;
            // $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        return $isConnected;
    }

    public function createPayment($args)
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $paymentID = '';
        $result    = [];

        $xeroInvoiceId = $args['invoiceID'];
        $isSynced = 0;
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            $paymentData = [
                "Invoice" => [
                    "InvoiceID" => $args['invoiceID'],
                ],
                "Account" => [
                    "Code" => $args['accountCode'],
                ],
                "Date"    => date('Y-m-d', strtotime($args['trnasactionDate'])),
                "Amount"  => $args['amount'],
            ];

            $result = $accountingApi->createPayment($this->tokenData['tenant_id'], $paymentData);
            $isSynced = 1;
            $error = "Payment Successfull";
        } catch (Exception $e) {
            $isSynced = 0;
            $error = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($result)) {
            $paymentID = $result[0]->getPaymentID();
            $invoices  = [$result[0]->getInvoice()];
            $this->storeInvoices($invoices, $args['invoiceID'], false);
        }

        $logData = [
            'action' => PAY_INVOICE,
            'status' => $isSynced,
            'xeroActionID' => $xeroInvoiceId,
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);

        return $paymentID;
    }

    public function deletePayment($args)
    {
        $accessToken = $this->tokenData['access_token'];
        $config      = XeroAPI\XeroPHP\Configuration::getDefaultConfiguration()->setAccessToken("$accessToken");

        $result           = [];
        $ispaymentDeleted = $storeTXN = true;
        if (isset($args['storeTXN'])) {
            $storeTXN = $args['storeTXN'];
        }

        $isSynced = 0;
        try {
            $accountingApi = new XeroAPI\XeroPHP\Api\AccountingApi(
                new GuzzleHttp\Client(),
                $config
            );

            $paymentData = [
                "Status" => "DELETED",
            ];

            $result = $accountingApi->deletePayment($this->tokenData['tenant_id'], $args['paymentId'], $paymentData);
            $isSynced = 1;
            $error = "Payment Deleted";
        } catch (Exception $e) {
            $isSynced = 0;
            $ispaymentDeleted = false;
            $error            = $this->getError($e);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $error . '</div>');
        }

        if (!empty($result) && $storeTXN) {
            $this->storeInvoices($result, false, false);
        }

        $logData = [
            'action' => DELETE_INVOICE_PAYMENT,
            'status' => $isSynced,
            'xeroActionID' =>  $args['paymentId'],
            'message' => $error,
            'syncFromCZ' => true,
        ];
        xero_sync_log($logData, $this->merchantID);

        return $ispaymentDeleted;
    }

    private function getError($error)
    {
        $erroCode  = $error->getCode();
        $errorBody = $error->getResponseBody();
        $errorBody = json_decode($errorBody, true);

        $finalError = "Something went wrong.";
        if ($erroCode == 401) {
            $finalError = "Token authentication error";
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: </strong>' . $finalError . '</div>');
            redirect('Integration/home/index', 'refresh');
        } else if ($erroCode == 400) {
            $finalError = $errorBody['Message'];
        } else {
            $all_error = [];
            foreach ($errorBody['Elements'] as $errorData) {
                if ($errorData['HasValidationErrors']) {
                    $errorTypeKey = "ValidationErrors";
                } else {
                    $all_error = ["Something went wrong. Please check for more errors codes."];
                    continue;
                }
                $erroList = $errorData[$errorTypeKey];
                foreach ($erroList as $erroMsg) {
                    $all_error[] = $erroMsg['Message'];
                }
            }

            if (!empty($all_error)) {
                $finalError = implode(" ", $all_error);
            }
        }

        return $finalError;
    }
}
