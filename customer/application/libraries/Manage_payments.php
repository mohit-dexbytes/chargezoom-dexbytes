<?php

include_once dirname(__FILE__) . '/../../../vendor/autoload.php';

use GlobalPayments\Api\PaymentMethods\CreditCardData;
use GlobalPayments\Api\ServicesConfig;
use GlobalPayments\Api\ServicesContainer;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\Exceptions\ApiException;
use GlobalPayments\Api\Entities\Exceptions\BuilderException;
use GlobalPayments\Api\Entities\Exceptions\ConfigurationException;
use GlobalPayments\Api\Entities\Exceptions\GatewayException;
use GlobalPayments\Api\Entities\Exceptions\UnsupportedTransactionException;
use GlobalPayments\Api\Entities\Transaction;
use GlobalPayments\Api\Entities\CommercialData;
use GlobalPayments\Api\Entities\Enums\TaxType;
use GlobalPayments\Api\PaymentMethods\TransactionReference;
use GlobalPayments\Api\PaymentMethods\ECheck;

include_once APPPATH . 'third_party/itransact-php-master/src/iTransactJSON/iTransactSDK.php';
use iTransact\iTransactSDK\iTTransaction;

include_once APPPATH . 'third_party/Fluidpay.class.php';
include_once APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

include_once APPPATH . 'third_party/nmiDirectPost.class.php';
include_once APPPATH . 'third_party/nmiCustomerVault.class.php';
include_once APPPATH . 'third_party/PayPalAPINEW.php';
include_once APPPATH."third_party/usaepay/usaepay.php";	
include_once APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';
include_once APPPATH . 'third_party/TSYS.class.php';
include_once APPPATH . 'third_party/PayTraceAPINEW.php';
include_once APPPATH . 'third_party/Cardpointe.class.php';

class Manage_payments
{
    public function __construct($merchantID)
    {
        $this->CI = &get_instance();
        $this->merchantID = $merchantID; 
		$this->CI->load->model('general_model');
        $merchantData    = $this->CI->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID));
        $this->resellerID = $merchantData['resellerID']; 
        $this->merchantData = $merchantData; 
    }

    /**
     * Void Feature: common feature to void a transaction
     */
    public function voidTransaction($args){
        $trID = $args['trID'];
        $gatlistval = $args['gatewayID'];
        $gt_result  = $this->CI->general_model->get_row_data('tbl_merchant_gateway', array('gatewayID' => $gatlistval));
        
        $params = $gt_result;
        $params['requestArgs'] = $args;
        $params['trID'] = $args['trID'];
        
        $params['paymentType'] = isset($args['paymentType']) ? $args['paymentType'] : 1;
        switch ($gt_result['gatewayType']) {
			case 1:
				return $this->nmiVoid($params);
				break;
			case 2:
				return $this->authPayVoid($params);
				break;
			case 3:
				return $this->paytraceVoid($params);
				break;
			case 4:
				return $this->paypalVoid($params);
				break;
            // case 5:
            //  return functionName($params); // Void not available for stripe
            //  break;
			case 6:
				return $this->usaepayVoid($params);
				break;
			case 7:
				return $this->globalpayVoid($params);
				break;
			case 8:
				return $this->cybersourceVoid($params);
				break;
			case 9:
				return $this->nmiVoid($params);
				break;
			case 10:
				return $this->iTransactVoid($params);
				break;
            case 11:
            case 13:
				return $this->fluidPayVoid($params);
				break;
            case 12:
                return $this->tsysVoid($params);
                break;
            case 14:
                return $this->cardPointeVoid($params);
                break;
            case 15:
                return $this->payarcVoid($params);
                break;
			default:
				return false;
				break;
		}
    }

    private function nmiVoid($args){
        
        $nmi_data = array('nmi_user' => $args['gatewayUsername'], 'nmi_password' => $args['gatewayPassword']);
        $transaction = new nmiDirectPost($nmi_data);
        
        if($args['paymentType'] == 2){
			$transaction->setPayment('check');
        }
        $transaction->setTransactionId($args['trID']);
        $transaction->void($args['trID']);
        $result     = $transaction->execute();

        if ($result['response_code'] == '100') {
            return true;
        }

        $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $result['responsetext'] . '</div>');
        return false;

    }

    private function authPayVoid($args){
        $this->CI->load->config('auth_pay');
        $transaction =  new AuthorizeNetAIM($args['gatewayUsername'],  $args['gatewayPassword']); 
        $transaction->setSandbox($this->CI->config->item('auth_test_mode'));

        if($args['paymentType'] == 2){
			$transaction->__set('method','echeck');
        }

        $result     = $transaction->void($args['trID']);
        if($result->response_code == '1'){
            return true;
        }

        $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $result->response_reason_text . '</div>');
        return false;
    }

    private function paytraceVoid($args){
		$this->CI->load->config('paytrace');
        
        $payAPI = new PayTraceAPINEW();
        $oauth_result = $payAPI->oAuthTokenGenerator("password", $args['gatewayUsername'], $args['gatewayPassword']);
        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
        
        if($oauth_moveforward){
            return false;
        }

        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        $oauth_token = sprintf("Bearer %s",$json['access_token']);

        if($args['paymentType'] == 2){
            $request_data = array(
                "check_transaction_id" => $args['trID'],
                "integrator_id" => $args['gatewaySignature'],
            );

            $url = URL_ACH_VOID_TRANSACTION;
        } else {
            $request_data = array( "transaction_id" => $args['trID'] );
            $url = URL_VOID_TRANSACTION;
        }

        $request_data = json_encode($request_data);
        $result = $payAPI->processTransaction($oauth_token, $request_data, $url);
        
        $response = $payAPI->jsonDecode($result['temp_json_response']); 
        
        if ( $result['http_status_code']=='200' ){
            return true;
        }

        if(!empty( $result['errors'])){ $err_msg = $this->getError( $result['errors']);}else{ $err_msg =  $result['approval_message'];}
        $this->CI->session->set_flashdata('message','<div class="alert alert-danger">Transaction Failed - '.$err_msg.'</div>');
        return false;

    }

    private function paypalVoid($args){
        $this->CI->load->config('paypal');
        
        $username  = $args['gatewayUsername'];
        $password  = $args['gatewayPassword'];
        $signature = $args['gatewaySignature'];

        $config = array(
            'Sandbox' => $this->CI->config->item('Sandbox'), 			// Sandbox / testing mode option.
            'APIUsername' => $username, 	// PayPal API username of the API caller
            'APIPassword' => $password,	// PayPal API password of the API caller
            'APISignature' => $signature, 	// PayPal API signature of the API caller
            'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->CI->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );
    
        $this->CI->load->library('paypal/Paypal_pro', $config);	

        $DVFields = array(
            'authorizationid' => $args['trID'], 				// Required.  The value of the original authorization ID returned by PayPal.  NOTE:  If voiding a transaction that has been reauthorized, use the ID from the original authorization, not the reauth.
            'note' => 'This test void',  							// An information note about this void that is displayed to the payer in an email and in his transaction history.  255 char max.
            'msgsubid' => ''						// A message ID used for idempotence to uniquely identify a message.
        );	
                    
        $PayPalRequestData = array('DVFields' => $DVFields);
        $PayPalResult = $this->CI->paypal_pro->DoVoid($PayPalRequestData);
        if("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
            return true;
        }
        return false;
    }

    private function usaepayVoid($args){
		$this->CI->load->config('usaePay');

        $transaction= new umTransaction;
        $transaction->ignoresslcerterrors= ($this->CI->config->item('ignoresslcerterrors') !== null ) ? $this->CI->config->item('ignoresslcerterrors') : true;

        $transaction->key= $args['gatewayUsername'];
        $transaction->pin= $args['gatewayPassword'];
        $transaction->usesandbox=$this->CI->config->item('Sandbox');
        $transaction->testmode=$this->CI->config->item('TESTMODE');
        
        $transaction->command="void"; 
        
        $transaction->refnum=  $args['trID'];
        $transaction->Process();
        if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS'){
            return true;
        }

        $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $transaction->result . '</div>');
        return false;
    }

    private function globalpayVoid($args){
        $this->CI->load->config('globalpayments');
        		
        $config = new ServicesConfig();
        $config->secretApiKey = $args['gatewayPassword'];
        $config->serviceUrl =  $this->CI->config->item('GLOBAL_URL');

        $isSuccess = false;
        ServicesContainer::configure($config);
        try{
            if(isset($args['paymentType']) && $args['paymentType'] == 2 ){
                $response = Transaction::fromId($args['trID'], null, 5)->void()->execute();
            }else{
                $response = Transaction::fromId($args['trID'])->void()->execute();
            }
            if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
            {
                $isSuccess = true;
            }
        }
        catch (BuilderException $e)
        {
            $error= 'Transaction Failed - ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (ConfigurationException $e)
        {
            $error='Transaction Failed - ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (GatewayException $e)
        {
            $error= 'Transaction Failed - ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (UnsupportedTransactionException $e)
        {
            $error='Transaction Failed - ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (ApiException $e)
        {
            $error='Transaction Failed - ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        return $isSuccess;
    }

    private function cybersourceVoid($args){
        $this->CI->load->config('cyber_pay');

        $option =array();
        $option['merchantID']     = trim($args['gatewayUsername']);
        $option['apiKey']         = trim($args['gatewayPassword']);
        $option['secretKey']      = trim($args['gatewaySignature']);
        
        if($this->CI->config->item('Sandbox'))
        $env   = $this->CI->config->item('SandboxENV');
        else
        $env   = $this->CI->config->item('ProductionENV');
        $option['runENV']      = $env;

        $commonElement = new CyberSource\ExternalConfiguration($option);

        $config = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance = new CyberSource\Api\VoidApi($apiclient);

        $cliRefInfoArr = [
            'code' => 'test_void'
        ];
        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
    
        $paymentRequestArr = [
            "clientReferenceInformation" => $client_reference_information
        ];
    
        $paymentRequest = new CyberSource\Model\VoidPaymentRequest($paymentRequestArr);
        $api_response = list($response,$statusCode,$httpHeader)=null;

        $isSuccess = false;
        try {
            $api_response = $api_instance->voidPayment($paymentRequest, $tID);
            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                $isSuccess = true;
            }
        }
        catch(Cybersource\ApiException $e)
        {
            $error = $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Transaction Failed - '.$error.' </strong></div>');
        }

        return $isSuccess;
    }

    private function iTransactVoid($args){
        $apiUsername = $args['gatewayUsername'];
        $apiKey      = $args['gatewayPassword'];

        $payload = [];
        $sdk     = new iTTransaction();

        $result = $sdk->voidCardTransaction($apiUsername, $apiKey, $payload, $args['trID']);

        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
            $voidedTransaction = true;
        } else {
            $voidedTransaction = false;
            $err_msg      = $result['status']      = $result['error']['message'];
            $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : '';

            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
        }

        return $voidedTransaction;
    }

    private function fluidPayVoid($args){
        $this->CI->load->config('fluidpay');
        $gatewayTransaction              = new Fluidpay();
        $gatewayTransaction->environment = $this->CI->config->item('environment');
        $gatewayTransaction->apiKey      = $args['gatewayUsername'];

        $result = $gatewayTransaction->voidTransaction($args['trID']);
            
        if ($result['status'] == 'success') {
            $voidedTransaction = true;
        } else {
            $voidedTransaction = false;
            $err_msg      = $result['msg'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
        }
        return $voidedTransaction;
    }

    private function tsysVoid($args){
        $this->CI->load->config('TSYS');
        $deviceID = $gt_result['gatewayMerchantID'].'01';            
        $gatewayTransaction              = new TSYS();
        $gatewayTransaction->environment = $this->CI->config->item('environment');
        $gatewayTransaction->deviceID = $deviceID;
        $result = $gatewayTransaction->generateToken($args['gatewayUsername'],$args['gatewayPassword'],$args['gatewayMerchantID']);
        $generateToken = '';

        if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Transaction Failed - '.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
            return false;
        }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
            $generateToken = $result['GenerateKeyResponse']['transactionKey'];
        }

        $gatewayTransaction->transactionKey = $generateToken;
        $result = $gatewayTransaction->voidTransaction( $args['trID']);
        $responseType = 'VoidResponse';		
        if ($result[$responseType]['status'] == 'PASS') {
            $voidedTransaction = true;
        } else {
            $voidedTransaction = false;
            $err_msg      = $result[$responseType]['responseMessage'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
        }

        return $voidedTransaction;
    }

    
    private function payarcVoid($args)
    {
        $this->CI->load->config('payarc');
        $this->CI->load->library('PayarcGateway');

        // PayArc Payment Gateway, set enviornment and secret key
        $this->CI->payarcgateway->setApiMode($this->CI->config->item('environment'));
        $this->CI->payarcgateway->setSecretKey($args['gatewayUsername']);

        $charge_response = $this->CI->payarcgateway->voidCharge($args['trID'], 'requested_by_customer');

        $result = json_decode($charge_response['response_body'], 1);
            
        if (isset($result['data']) && $result['data']['status'] == 'void') {
            $voidedTransaction = true;
        } else {
            $voidedTransaction = false;
            $err_msg      = $result['message'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
        }
        return $voidedTransaction;
    }

    
    private function cardPointeVoid($args){
        $cardpointeuser = $args['gatewayUsername'];
        $cardpointepass = $args['gatewayPassword'];
        $cardpoineMerchantID = $args['gatewayMerchantID'];
        $cardpointeSiteName  = $args['gatewaySignature'];
        $client = new CardPointe();
        $result = $client->void($cardpointeSiteName, $cardpoineMerchantID, $cardpointeuser, $cardpointepass, $args['trID']);
       
        if ($result['resptext'] == 'Approval' || $result['resptext'] == 'Approved') {
            $voidedTransaction = true;
        } else {
            $voidedTransaction = false;
            $err_msg      = $result['resptext'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
        }
        return $voidedTransaction;
    }
    /**
     * Void Feature: Ends Here
     */

    /**
     * Authorisation Feature: common feature to authorise a transaction
     */

    public function authoriseTransaction($args){
        if(isset($args['gatewayID'])){
            $where = array('gatewayID' => $args['gatewayID']);
        } else {
            $where = array('set_as_default' => 1, 'merchantID' => $this->merchantID);
        }

        $gt_result  = $this->CI->general_model->get_row_data('tbl_merchant_gateway', $where);
        
        if(!$gt_result || empty($gt_result)){
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Invalid gateway details </strong></div>');
            return false;
        }

        $storeResult = (isset($args['storeResult'])) ? $args['storeResult'] : false;
        $returnResult = (isset($args['returnResult'])) ? $args['returnResult'] : $storeResult;

        $params = array_merge($gt_result, $args);
        $params['returnResult'] = $returnResult;

        switch ($gt_result['gatewayType']) {
			case 1:
				$authResult = $this->nmiAuthorise($params);
				break;
			case 2:
				$authResult = $this->authPayAuthorise($params);
				break;
			case 3:
				$authResult = $this->paytraceAuthorise($params);
				break;
			case 4:
				$authResult = $this->paypalAuthorise($params);
				break;
			case 5:
			 	$authResult = $this->stripeAuthorise($params);
			 	break;
			case 6:
				$authResult = $this->usaepayAuthorise($params);
				break;
			case 7:
				$authResult = $this->globalpayAuthorise($params);
				break;
			case 8:
				$authResult = $this->cybersourceAuthorise($params);
				break;
			case 9:
				$authResult = $this->nmiAuthorise($params);
				break;
			case 10:
				$authResult = $this->itransactAuthorise($params);
				break;
            case 11:
            case 13:
				$authResult = $this->fluidPayAuthorise($params);
				break;
            case 12:
                $authResult = $this->TSYSAuthorise($params);
                break;
            case 15:
                $authResult = $this->payarcAuthorise($params);
                break;
            case 16:
                $authResult =  $this->EPXAuthorise($params);
                break;
            case 14:
                $authResult =  $this->cardPointeAuthorise($params);
                break;
            default:
				return false;
                break;
		}

        if($storeResult && isset($authResult['result'])){
            $crtxnID = (isset($args['crtxnID'])) ? $args['crtxnID'] : ''; 
            $invoiceID = (isset($args['invoiceID'])) ? $args['invoiceID'] : ''; 

            $achType = (isset($args['ach_type']) && $args['ach_type'] == 2) ? true : false; 
            $id = $this->CI->general_model->insert_gateway_transaction_data($authResult['result'], 'auth', $gt_result['gatewayID'], $gt_result['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, $crtxnID, $this->resellerID, $invoiceID, false, $args['transactionByUser']);
            $authResult['id'] = $id;
        }
        return $authResult;
    }

    private function nmiAuthorise($args){
        
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = NMIAUTHRIZEMINAMOUNT;
        }
        
        $nmi_data = array('nmi_user' => $args['gatewayUsername'], 'nmi_password' => $args['gatewayPassword']);
        $transaction = new nmiDirectPost($nmi_data);
        
        // $card_data = $args['cardData'];
        $transaction->setCcNumber($args['CustomerCard']);
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $exyear1   = substr($exyear, 2);
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $expry    = $expmonth . $exyear1;
        $transaction->setCcExp($expry);
        if ($args['CardCVV'] != "")
            $transaction->setCvv($args['CardCVV']);

        $transaction->setAmount($args['amount']);
        $transaction->setTax('tax');
        $transaction->auth();

        $result     = $transaction->execute();
        if ($result['response_code'] == '100') {
            $response = true;
        } else {
            $response = false;
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['responsetext'] . '</strong></div>');
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function authPayAuthorise($args){
        include_once APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $this->CI->load->config('auth_pay');
        $transaction =  new AuthorizeNetAIM($args['gatewayUsername'],  $args['gatewayPassword']); 
        $transaction->setSandbox($this->CI->config->item('auth_test_mode'));

        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = AUTHAUTHRIZEMINAMOUNT;
        }

        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $exyear1   = substr($exyear,2);
        if(strlen($expmonth)==1){
            $expmonth = '0'.$expmonth;
        }
        $expry    = $expmonth.$exyear1; 

        $result = $transaction->authorizeOnly($args['amount'],$card_no,$expry);

        if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {
            $response = true;
        } else {
            $response = false;
            $err_msg = isset($result->response_reason_text)?$result->response_reason_text:'Something went wrong';
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function paytraceAuthorise($args){
		$this->CI->load->config('paytrace');
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = PAYTRACEAUTHRIZEMINAMOUNT;
        }
        $payAPI = new PayTraceAPINEW();
        $oauth_result = $payAPI->oAuthTokenGenerator("password", $args['gatewayUsername'], $args['gatewayPassword']);

        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
        
        if($oauth_moveforward){
            return false;
        }

        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        $oauth_token = sprintf("Bearer %s",$json['access_token']);

        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        
        $exyear   = $args['cardYear'];
        $exyear   = substr($exyear,2);
        if(strlen($expmonth)==1){
            $expmonth = '0'.$expmonth;
        }

        $request_data = array(
            "amount" => $args['amount'],
            "credit_card"=> array (
                "number"=> $card_no,
                "expiration_month"=>$expmonth ,
                "expiration_year"=>$exyear ),
            "invoice_id"=> time(),
            "billing_address"=> array(
                "name"=>(isset($args['name'])) ? $args['name'] : '',
                "street_address"=> $args['Billing_Addr1'],
                "city"=> $args['Billing_City'],
                "state"=> $args['Billing_State'],
                "zip"=> $args['Billing_Zipcode'],
            ),
        );
        
        if ($args['CardCVV'] != "")
            $request_data['csc'] = $args['CardCVV'];

        $request_data = json_encode($request_data);
        $result     =   $payAPI->processTransaction($oauth_token,$request_data, URL_KEYED_AUTHORIZATION );
        
        $response = $payAPI->jsonDecode($result['temp_json_response']); 
        // $id = $this->CI->general_model->insert_gateway_transaction_data($response, 'auth', $args['gatewayID'], $args['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, '', $this->resellerID, '');
        
        // echo '<pre>';
        // print_r([$response, $result]);die;
        
        if ( $result['http_status_code']=='200' ){
            $resp = true;
        } else {
            $resp = false;
        }

        if(!empty( $result['errors'])){
            $err_msg = $this->getError( $result['errors']);
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
        }
        
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $resp; 
            $r_data['result'] = $response; 
            return $r_data;
        }
        return $resp;
    }

    private function paypalAuthorise($args){
        $this->CI->load->config('paypal');

        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = PAYPALAUTHRIZEMINAMOUNT;
        }
        
        $username  = $args['gatewayUsername'];
        $password  = $args['gatewayPassword'];
        $signature = $args['gatewaySignature'];

        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth']; 
        $exyear   = $args['cardYear'];
        if(strlen($expmonth)==1){
            $expmonth = '0'.$expmonth;
        }
        $cvv     = $args['CardCVV'];

        $config = array(
            'Sandbox' => $this->CI->config->item('Sandbox'), 			// Sandbox / testing mode option.
            'APIUsername' => $username, 	// PayPal API username of the API caller
            'APIPassword' => $password,	// PayPal API password of the API caller
            'APISignature' => $signature, 	// PayPal API signature of the API caller
            'APISubject' => '', 									// PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion' => $this->CI->config->item('APIVersion')		// API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );
        $option = array('API_UserName' => $username,
                        'API_Password'       => $password,
                        'API_Signature'      => $signature,
                        'API_Endpoint'       => "https://api-3t.paypal.com/nvp",
                        'envoironment'       => 'sandbox',
                        'version'            => '123');


        $this->CI->load->library('paypal/Paypal_pro', $config);	

        $paymentType 		=	'Authorization';
        $creditCardType 	= urlencode('Visa');
        $creditCardNumber 	= urlencode($card_no);
        $expDateMonth 		= $expmonth;
        // Month must be padded with leading zero
        $padDateMonth 		= urlencode(str_pad($expDateMonth, 2, '0', STR_PAD_LEFT));

        $expDateYear= urlencode($exyear);
        $cvv2Number = urlencode($cvv);
        $country 	= 'US';	
        $amount 	= $args['amount'];	//actual amount should be substituted here
        $currencyID = 'USD';// or other currency ('GBP', 'EUR', 'JPY', 'CAD', 'AUD')

        $nvpStr =	"&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber".
								"&EXPDATE=$padDateMonth$expDateYear&CVV2=$cvv2Number&COUNTRYCODE=$country&CURRENCYCODE=$currencyID";

        $paypal = new PayPalAPINEW($option);
        $httpParsedResponseAr = $paypal ->PPHttpPost('DoDirectPayment', $nvpStr, $option);
        
        // $id = $this->CI->general_model->insert_gateway_transaction_data($httpParsedResponseAr3333, 'auth', $args['gatewayID'], $args['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, '', $this->resellerID, '');

        
        if("SUCCESS" == strtoupper($httpParsedResponseAr["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($httpParsedResponseAr["ACK"])) {
            $response = true;
        } else {
            $response = false;
            $code='401';
            $responsetext= urldecode($httpParsedResponseAr['L_LONGMESSAGE0']);
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: "'.$responsetext.'"</strong>.</div>'); 
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $PayPalResult; 
            return $r_data;
        }
        return $response;
    }

    private function getError($eee){ 
		$eeee=array();
		foreach($eee as $error =>$no_of_errors )
		{
            foreach($no_of_errors as $key=> $item)
            {
                $eeee[]= rtrim($item,'.') ; 
            } 
		} 

        // echo '<pre>';
        // print_r([$eeee]);die;
		
		return implode(', ',$eeee);
	
    }
    
    private function usaepayAuthorise($args){
        $this->CI->load->config('usaePay');
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = USAEPAYAUTHRIZEMINAMOUNT;
        }
        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $cvv      = $args['CardCVV'];

        $transaction= new umTransaction;
        $transaction->ignoresslcerterrors= ($this->CI->config->item('ignoresslcerterrors') !== null ) ? $this->CI->config->item('ignoresslcerterrors') : true;

        $transaction->key= $args['gatewayUsername'];
        $transaction->pin= $args['gatewayPassword'];
        $transaction->usesandbox=$this->CI->config->item('Sandbox');
        $transaction->testmode=$this->CI->config->item('TESTMODE');

        $transaction->card = $card_no;
        $expyear   = substr($exyear,2);
        if(strlen($expmonth)==1){
            $expmonth = '0'.$expmonth;
        }
        $expry    = $expmonth.$expyear;  
        $transaction->exp = $expry;
        if($cvv!="")
            $transaction->cvv2 = $cvv;
        
        $transaction->invoice= time();   		// invoice number.  must be unique.
        $transaction->amount = $args['amount'];
        $transaction->command="authonly";	
        $transaction->Process();
        
        $msg = $transaction->result;
        $trID = $transaction->refnum;

        $isProcessed = false;
        if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS'){
            $code = 200;
            $response = true;
        } else {
            $code = 300;
            $response = false;
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error: '.$msg.'</strong></div>'); 
        }

        $res =array('transactionCode'=> "$code", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response; 
    }

    private function globalpayAuthorise($args){
        $this->CI->load->config('globalpayments');
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = HEARTLANDAUTHRIZEMINAMOUNT;
        }
        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $cvv      = $args['CardCVV'];
        		
        $config = new ServicesConfig();
        $config->secretApiKey = $args['gatewayPassword'];
        $config->serviceUrl =  $this->CI->config->item('GLOBAL_URL');

        $card = new CreditCardData();
        $card->number = $card_no;
        $card->expMonth = $expmonth;
        $card->expYear = $exyear;
        if($cvv!="")
            $card->cvn = $cvv;

        $invNo  =mt_rand(1000000,2000000);
        
        $msg = $trID = '';
        $response = false;
        $responseCode = 300;
        ServicesContainer::configure($config);

        try{
            $response = $card->authorize($args['amount'])->withCurrency('USD')->execute();
            if($response->responseMessage=='APPROVAL' || strtoupper($response->responseMessage)=='SUCCESS')
            {
                $msg = $response->responseMessage;
                $trID = $response->transactionId;
                $responseCode = 200;  
                $response = true;
            } else {
                $msg = $response->responseMessage;
                $trID = $response->transactionId;
                $responseCode = $response->responseCode;  
                $this->CI->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed '.$msg.'</strong></div>'); 
            }

            // $id = $this->CI->general_model->insert_gateway_transaction_data($res, 'auth', $args['gatewayID'], $args['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, '', $this->resellerID, '');
        }
        catch (BuilderException $e)
        {
            $error= 'Build Exception Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (ConfigurationException $e)
        {
            $error='ConfigurationException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (GatewayException $e)
        {
            $error= 'GatewayException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (UnsupportedTransactionException $e)
        {
            $error='UnsupportedTransactionException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }
        catch (ApiException $e)
        {
            $error=' ApiException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$error.'</strong>.</div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function cybersourceAuthorise($args){
        $this->CI->load->config('cyber_pay');
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = CYBERSOURCEAUTHRIZEMINAMOUNT;
        }
        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $cvv      = $args['CardCVV'];

        $option =array();
        $option['merchantID']     = trim($args['gatewayUsername']);
        $option['apiKey']         = trim($args['gatewayPassword']);
        $option['secretKey']      = trim($args['gatewaySignature']);
        
        if($this->CI->config->item('Sandbox'))
        $env   = $this->CI->config->item('SandboxENV');
        else
        $env   = $this->CI->config->item('ProductionENV');
        $option['runENV']      = $env;

        $commonElement = new CyberSource\ExternalConfiguration($option);

        $config = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);

        $cliRefInfoArr = [
            'code' => $this->merchantData['companyName']
        ];
        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
    
        $processingInformationArr = [
            "commerceIndicator" => "internet"
        ];
    
        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);

        $amountDetailsArr = [
            "totalAmount" => $args['amount'],
            "currency" => CURRENCY,
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $orderInfoArr = [
            "amountDetails" => $amountDetInfo, 
            // "billTo" => $billto
        ];
        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);
        
        $paymentCardInfo = [
            "expirationYear" => $exyear,
            "number" => $card_no,
            // "securityCode" => $cvv,
            "expirationMonth" => $expmonth
        ];
        if(!empty($cvv)){
            $paymentCardInfo['securityCode'] = $cvv;
        }
        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        
        $paymentInfoArr = [
            "card" => $card
        ];
        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

        $paymentRequestArr = [
            "clientReferenceInformation" =>$client_reference_information, 
            "orderInformation" =>$order_information, 
            "paymentInformation" =>$payment_information, 
            "processingInformation" =>$processingInformation
        ];
        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);
        $api_response = list($response, $statusCode, $httpHeader) = null;
        $type =   'Auth'; 

        $trID = $msg = '';
        $responseCode = 300;
        $response = false;
        try {
            $api_response = $api_instance->createPayment($paymentRequest);
            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                $trID =   $api_response[0]['id'];
                $msg  =   $api_response[0]['status'];
                
                $responseCode =   '200';
                $response = true;
            } else {
                $trID =   $api_response[0]['id'];
                $msg  =   $api_response[0]['status'];
                $responseCode =   $api_response[1];
                $response = false;
                
                $this->CI->session->set_flashdata('message','<div class="alert alert-danger"><strong>Payment Failed '.$msg.'</strong></div>'); 
            }
            // $id = $this->CI->general_model->insert_gateway_transaction_data($res, 'auth', $args['gatewayID'], $args['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, '', $this->resellerID, '');
        }
        catch(Cybersource\ApiException $e)
        {
            $error = $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$error.' </strong></div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function itransactAuthorise($args){
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = ITRANSACTAUTHRIZEMINAMOUNT;
        }
        $apiUsername = $args['gatewayUsername'];
        $apiKey = $args['gatewayPassword'];

        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];

        if (strlen($expmonth) > 1 && $expmonth <= 9) {
            $expmonth = substr($expmonth, 1);
        }

        $request_data = array(
            "amount"          => ($args['amount'] * 100),
            "card"     => array(
                "name" => $args['name'],
                "number" => $card_no,
                "exp_month" => $expmonth,
                "exp_year"  => $exyear,
                // "cvv"       => $cvv,
            ),
            "address" => array(
                "line1"           => $args['Billing_Addr1'],
                "line2" => $args['Billing_Addr2'],
                "city"           => $args['Billing_City'],
                "state"          => $args['Billing_State'],
                "postal_code"            => $args['Billing_Zipcode'],
            ),
            'capture' => false
        );
        
        if ($args['CardCVV'] != ""){
            $request_data['card']['cvv'] = $args['CardCVV']; 
        }

        $isProcessed = false;
        $crtxnID = '';
        $invID = $inID = $responseId  = '';

        $sdk = new iTTransaction();
        $result = $sdk->postCardTransaction($apiUsername, $apiKey, $request_data);

        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
            $result['status_code'] = '200';
            $isProcessed = true;
        } else {
            $err_msg = $result['status'] = $result['error']['message'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err_msg . '</strong></div>');
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $isProcessed; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $isProcessed;
    }

    private function fluidPayAuthorise($args){
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = FLUIDPAYAUTHRIZEMINAMOUNT;
        }

        $this->CI->load->config('fluidpay');
        $gatewayEnvironment = $this->CI->config->item('environment');
        
        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $exyear1  = substr($exyear, 2);
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $expry = $expmonth . $exyear1;

        $transaction = array(
            "type"                => "authorize",
            "amount"              => ($args['amount'] * 100),
            "currency"            => "USD",
            "ip_address"          => $_SERVER['REMOTE_ADDR'],
            "payment_method"      => array(
                "card" => array(
                    "entry_type"      => "keyed",
                    "number"          => $card_no,
                    "expiration_date" => $expry,
                    // "cvc"             => $cvv,
                ),
            ),
            "billing_address"     => array(
                "first_name"     => $args['name'],
                "last_name"      => '',
                "company"        => (isset($args['companyName'])) ? $args['companyName'] : $args['name'],
                "address_line_1" => $args['Billing_Addr1'],
                "address_line_2" => $args['Billing_Addr2'],
                "city"           => $args['Billing_City'],
                "state"          => $args['Billing_State'],
                "postal_code"    => $args['Billing_Zipcode'],
                // "country"        => $args['Billing_Addr1'],
                // "phone"          => $args['Billing_Addr1'],
                // "fax"            => $args['Billing_Addr1'],
                // "email"          => $args['Billing_Addr1'],
            ),
        );
        if ($args['CardCVV'] != ""){
            $transaction['payment_method']['card']['cvc'] = $args['CardCVV'];
        }

        $crtxnID = '';
        $invID = $inID = $responseId  = '';

        $gatewayTransaction              = new Fluidpay();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->apiKey      = $args['gatewayUsername'];

        $result = $gatewayTransaction->processTransaction($transaction);
        
        // $id = $this->CI->general_model->insert_gateway_transaction_data($result, 'auth', $args['gatewayID'], $args['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, '', $this->resellerID, '');
        $response = false;
        if ($result['status'] == 'success') {
            $response = true;
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['msg'] . '</strong></div>');
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }


    private function stripeAuthorise($args){
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = STRIPEAUTHRIZEMINAMOUNT;
        }
        $expmonth = $args['cardMonth'];
        if(strlen($expmonth)==1){
            $expmonth = '0'. $expmonth;
        }
        $plugin = new ChargezoomStripe();
        $plugin->setApiKey($args['gatewayPassword']);
        
        try{
           $res = \Stripe\Token::create([
                                'card' => [
                                'number' =>$args['CustomerCard'],
                                'exp_month' => $expmonth,
                                'exp_year' =>  $args['cardYear'],
                                'cvc' => $args['CardCVV']
                               ]
                    ]); 

        }catch(\Stripe\Error\Card $e) {

        // Since it's a decline, \Stripe\Error\Card will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err['message'] . '</strong></div>');
                return false;
            

        }
        $tcharge= json_encode($res);  
        $rest = json_decode($tcharge);
        
        if($rest->id){

            $token = $rest->id; 
            

            try{
               $charge =   \Stripe\Charge::create(array(
                  "amount" => ($args['amount'] * 100),
                  "currency" => "usd",
                  "source" => $token, // obtained with Stripe.js
                  "description" => "Charge for authrize",
                 'capture'     => 'false' 
                ));  

            }catch(\Stripe\Error\Card $e) {

            // Since it's a decline, \Stripe\Error\Card will be caught
                $body = $e->getJsonBody();
                $err  = $body['error'];
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err['message'] . '</strong></div>');
                    return false;
                

            }
       
           $charge= json_encode($charge);
           $result = json_decode($charge);
           if($result->paid=='1' && $result->failure_code==""){
                $response = true;
            } else {
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result->status . '</strong></div>');
                $response = false;
            }

            if(isset($args['returnResult']) && $args['returnResult']){
                $r_data['response'] = $response; 
                $r_data['result'] = $result; 
                return $r_data;
            }
            return $response;

        }else{
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $rest->status . '</strong></div>');
            return false;
        }

    }

    private function TSYSAuthorise($args){
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = TSYSAUTHRIZEMINAMOUNT;
        }
        $this->CI->load->config('TSYS');
        $expmonth = $args['cardMonth'];
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $expry = $expmonth.'/'.$args['cardYear'];
        $gatewayEnvironment = $this->CI->config->item('environment');
        $deviceID = $args['gatewayMerchantID'].'01';
        $gatewayTransaction              = new TSYS();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->deviceID = $deviceID;
        $result = $gatewayTransaction->generateToken($args['gatewayUsername'],$args['gatewayPassword'],$args['gatewayMerchantID']);
        $generateToken = '';
        $status = 1;

        if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
           
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['GenerateKeyResponse']['responseMessage']. '</strong></div>');

            $status = 0;

        }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
            $generateToken = $result['GenerateKeyResponse']['transactionKey'];
            $status = 1;
            
        }
        if($status == 1){
            $gatewayTransaction->transactionKey = $generateToken;

            $transaction['Auth'] = array(
                "deviceID"                          => $deviceID,
                "transactionKey"                    => $generateToken,
                "cardDataSource"                    => "MANUAL",  
                "transactionAmount"                 => ($args['amount'] * 100),
                "currencyCode"                      => "USD",
                "cardNumber"                        => $args['CustomerCard'],
                "expirationDate"                    => $expry,
                "cvv2"                              => $args['CardCVV'],
               // "customerPhone"                     =>(isset($inputData['contact']))?$inputData['contact']:'None',
                "terminalCapability"                => "ICC_CHIP_READ_ONLY",
                "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
                "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
                "terminalAuthenticationCapability"  => "NO_CAPABILITY",
                "terminalOutputCapability"          => "DISPLAY_ONLY",
                "maxPinLength"                      => "UNKNOWN",
                "terminalCardCaptureCapability"     => "NO_CAPABILITY",
                "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
                "cardPresentDetail"                 => "CARD_PRESENT",
                "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
                "cardholderAuthenticationEntity"    => "OTHER",
                "cardDataOutputCapability"          => "NONE",
            );
            if($args['CardCVV'] == ''){
                unset($transaction['Auth']['cvv2']);
            }
            $responseType = 'AuthResponse';
            $result = $gatewayTransaction->processTransaction($transaction);
            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                $response = true;
            } else{
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result[$responseType]['responseMessage'] . '</strong></div>');
                $response = false;
            }

            if(isset($args['returnResult']) && $args['returnResult']){
                $r_data['response'] = $response; 
                $r_data['result'] = $result; 
                return $r_data;
            }
            return $response;
        }else{
            return false;
        }
        
        
    }

    private function payarcAuthorise($args) {
        

        $this->CI->load->config('payarc');
        $this->CI->load->library('PayarcGateway');

        // PayArc Payment Gateway, set enviornment and secret key
        $this->CI->payarcgateway->setApiMode($this->CI->config->item('environment'));
        $this->CI->payarcgateway->setSecretKey($args['gatewayUsername']);

        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $cvv = $args['CardCVV'];

        // Create Credit Card Token
        $address_info = ['address_line1' => '', 'address_line2' => '', 'state' => '', 'country' => ''];

        $token_response = $this->CI->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);

        $token_data = json_decode($token_response['response_body'], 1);

        if(isset($token_data['status']) && $token_data['status'] == 'error'){
            $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->CI->config->item('environment'),'accessKey'=>$args['gatewayUsername'], 'card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
            // Error while creating the credit card token
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $token_data['message'] . '</strong></div>');
            return false;

        } else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {

            // If token created
            $token_id = $token_data['data']['id'];

            $charge_payload = [];

            $charge_payload['token_id'] = $token_id;
            
            if(isset($args['email']) && !empty($args['email']) && filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
                $charge_payload['email'] = $args['email']; // Customer's email address.
            }

            if(isset($args['contact']) && !empty($args['contact'])) {
                $charge_payload['phone_number'] = $args['contact']; // Customer's contact phone number..
            }

            if(isset($args['zipcode']) && $args['zipcode']) {
                $charge_payload['ship_to_zip'] = $args['zipcode']; 
            }

            $charge_payload['amount'] = $args['amount'] * 100; // must be in cents and min amount is 50c USD

            $charge_payload['currency'] = 'usd'; 

            $charge_payload['capture'] = 0; // 0 for authorize and 1 for capture instantly

            $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.

            $charge_payload['statement_description'] = '';

            $charge_response = $this->CI->payarcgateway->createCharge($charge_payload);

            $result = json_decode($charge_response['response_body'], 1);

            // Handle Card Decline Error
            if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
            {
                $result['message'] = $result['data']['failure_message'];
            }

            $isProcessed = false;
            if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'authorized') {

                $responseId = $result['data']['id'];

                // $this->CI->session->set_flashdata('success', 'Successfully Authorized Credit Card Payment');
                $isProcessed = true;

            } else {
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['message'] . '</strong></div>');
            }

            if(isset($args['returnResult']) && $args['returnResult']){
                $r_data['response'] = $isProcessed; 
                $r_data['result'] = $result; 
                return $r_data;
            }
            return $isProcessed;
        }

    }

    private function cardPointeAuthorise($args){
        if($args['authType'] == 1 || $args['amount'] == ''){
            $args['amount'] = FLUIDPAYAUTHRIZEMINAMOUNT;
        }

        $cardpointeuser   = $args['gatewayUsername'];
        $cardpointepass   = $args['gatewayPassword'];
        $cardpointeMerchID = $args['gatewayMerchantID'];
        $cardpointeSiteName = $args['gatewaySignature'];
        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        $exyear1  = substr($exyear, 2);
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $expry = $expmonth . $exyear1;
        $cvv = $args['CardCVV'];
        $transaction = array(
            "type"                => "authorize",
            "amount"              => ($args['amount'] * 100),
            "currency"            => "USD",
            "ip_address"          => $_SERVER['REMOTE_ADDR'],
            "payment_method"      => array(
                "card" => array(
                    "entry_type"      => "keyed",
                    "number"          => $card_no,
                    "expiration_date" => $expry,
                    
                    // "cvc"             => $cvv,
                ),
            ),
            "billing_address"     => array(
                "first_name"     => $args['name'],
                "last_name"      => '',
                "company"        => (isset($args['companyName'])) ? $args['companyName'] : $args['name'],
                "address_line_1" => $args['Billing_Addr1'],
                "address_line_2" => $args['Billing_Addr2'],
                "city"           => $args['Billing_City'],
                "state"          => $args['Billing_State'],
                "postal_code"    => $args['Billing_Zipcode'],
                // "country"        => $args['Billing_Addr1'],
                // "phone"          => $args['Billing_Addr1'],
                // "fax"            => $args['Billing_Addr1'],
                // "email"          => $args['Billing_Addr1'],
            ),
        );
        $full_name = $transaction['billing_address']['first_name'] . ' ' . $transaction['billing_address']['last_name'];
        if ($args['CardCVV'] != ""){
            $transaction['payment_method']['card']['cvc'] = $args['CardCVV'];
        }

        $crtxnID = '';
        $invID = $inID = $responseId  = '';

        $client = new CardPointe();
        $result = $client->authorize($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $card_no, $expry, $transaction['amount'], $cvv, $full_name, $transaction['billing_address']['address_line_1'], $transaction['billing_address']['city'], $transaction['billing_address']['state'], $transaction['billing_address']['postal_code']);

        $response = false;
        if ($result['respcode'] == '00') {
            $response = true;
        } else {
        $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['resptext'] . '</strong></div>');
        }
        
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    /**
     * Authorisation Feature: Ends Here
     */

    
    /**
     * Sale Feature: common feature to process/sale a transaction
     */

    public function _processSaleTransaction($args){
        if(isset($args['gatewayID'])){
            $where = array('gatewayID' => $args['gatewayID']);
        } else {
            $where = array('set_as_default' => 1, 'merchantID' => $this->merchantID);
        }

        $storeResult = (isset($args['storeResult'])) ? $args['storeResult'] : false;
        $returnResult = (isset($args['returnResult'])) ? $args['returnResult'] : $storeResult;

        $gt_result  = $this->CI->general_model->get_row_data('tbl_merchant_gateway', $where);
        
        if(!$gt_result || empty($gt_result)){
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Invalid gateway details </strong></div>');
            return false;
        }

        $params = array_merge($gt_result, $args);
        $params['returnResult'] = $returnResult;

        switch ($gt_result['gatewayType']) {
			case 1:
				$saleResult = $this->nmiSale($params);
				break;
			case 2:
				$saleResult = $this->authPaySale($params);
				break;
			case 3:
				$saleResult = $this->paytraceSale($params);
				break;
			case 4:
				$saleResult = $this->paypalSale($params);
				break;
			case 5:
			 	$saleResult = $this->stripeSale($params);
			 	break;
			case 6:
				$saleResult = $this->usaepaySale($params);
				break;
			case 7:
				$saleResult = $this->globalpaySale($params);
				break;
			case 8:
				$saleResult = $this->cybersourceSale($params);
				break;
			case 9:
				$saleResult = $this->nmiSale($params);
				break;
			case 10:
				$saleResult = $this->itransactSale($params);
				break;
            case 11:
            case 13:    
				$saleResult = $this->fluidPaySale($params);
				break;
            case 12:
                $saleResult = $this->TSYSSale($params);
                break;
            case 15:
                $saleResult = $this->payarcSale($params);
                break;
            case 14:
                $saleResult = $this->cardPointeSale($params);
                break;
            default:
				$saleResult = false;
				break;
		}

        if($storeResult){
            $achType = (isset($args['ach_type']) && $args['ach_type'] == 2) ? true : false; 
            $id = $this->CI->general_model->insert_gateway_transaction_data($saleResult['result'], 'sale', $gt_result['gatewayID'], $gt_result['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, $args['crtxnID'], $this->resellerID, $args['invoiceID'], $achType, $args['transactionByUser']);
            $saleResult['id'] = $id;
        }
        return $saleResult;
    }

    private function nmiSale($args){
        $nmi_data = array('nmi_user' => $args['gatewayUsername'], 'nmi_password' => $args['gatewayPassword']);
        $transaction = new nmiDirectPost($nmi_data);
        $amount = $args['amount'];
        
        if($args['ach_type'] == 1){
            $transaction->setCcNumber($args['paymentDetails']['CardNo']);
            $args['paymentDetails']['cardYear']   = substr($args['paymentDetails']['cardYear'], 2);
            if (strlen($args['paymentDetails']['cardMonth']) == 1) {
                $args['paymentDetails']['cardMonth'] = '0' . $args['paymentDetails']['cardMonth'];
            }
            $expry    = $args['paymentDetails']['cardMonth'] . $args['paymentDetails']['cardYear'];
            $transaction->setCcExp($expry);

            if ($args['paymentDetails']['CardCVV'] != "")
                $transaction->setCvv($args['paymentDetails']['CardCVV']);

            // add level III data
            $level_request_data = [
                'transaction' => $transaction,
                'card_no' => $args['paymentDetails']['CardNo'],
                'merchID' => $this->merchantID,
                'amount' => $amount,
                'invoice_id' => (isset($args['invoiceID'])) ? $args['invoiceID'] : time(),
                'gateway' => 1
            ];
            $transaction = addlevelThreeDataInTransaction($level_request_data);
        } else if($args['ach_type'] == 2){
            $transaction->setAccountName($args['fullName']);
            $transaction->setAccount($args['paymentDetails']['accountNumber']);
            $transaction->setRouting($args['paymentDetails']['routeNumber']);
            $transaction->setAccountType($args['paymentDetails']['accountType']);
            $transaction->setAccountHolderType($args['paymentDetails']['accountHolderType']);
            $transaction->setSecCode($args['paymentDetails']['secCodeEntryMethod']);
            $transaction->setPayment('check');
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Payment Type</strong></div>');
            return false;
        }

        #Billing Details
        if(isset($args['companyName']) && !empty($args['companyName'])){
            $transaction->setCompany($args['companyName']);
            $transaction->setShippingCompany($args['companyName']);
        }

        if(isset($args['firstName']) && !empty($args['firstName'])){
            $transaction->setFirstName($args['firstName']);
            $transaction->setShippingFirstName($args['firstName']);
        }

        if(isset($args['lastName']) && !empty($args['lastName'])){
            $transaction->setLastName($args['lastName']);
            $transaction->setShippingLastName($args['lastName']);
        }

        if(isset($args['address1']) && !empty($args['address1'])){
            $transaction->setAddress1($args['address1']);
            $transaction->setShippingAddress1($args['address1']);
        }

        if(isset($args['address2']) && !empty($args['address2'])){
            $transaction->setAddress2($args['address2']);
            $transaction->setShippingAddress2($args['address2']);
        }

        if(isset($args['country']) && !empty($args['country'])){
            $transaction->setCountry($args['country']);
            $transaction->setShippingCountry($args['country']);
        }

        if(isset($args['city']) && !empty($args['city'])){
            $transaction->setCity($args['city']);
            $transaction->setShippingCity($args['city']);
        }
        
        if(isset($args['state']) && !empty($args['state'])){
            $transaction->setState($args['state']);
            $transaction->setShippingState($args['state']);
        }

        if(isset($args['zipcode']) && !empty($args['zipcode'])){
            $transaction->setZip($args['zipcode']);
            $transaction->setShippingZip($args['zipcode']);
        }

        if(isset($args['contact']) && !empty($args['contact'])){
            $transaction->setPhone($args['contact']);
        }
        
        $transaction->setAmount($amount);
        $transaction->setTax('tax');
        $transaction->sale();

        $result = $transaction->execute();
        if ($result['response_code'] == '100') {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function authPaySale($args){
        include_once APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $this->CI->load->config('auth_pay');
        $transaction =  new AuthorizeNetAIM($args['gatewayUsername'],  $args['gatewayPassword']); 
        $transaction->setSandbox($this->CI->config->item('auth_test_mode'));
        
        $amount = $args['amount'];
        
        if($args['ach_type'] == 1){
            $args['paymentDetails']['cardYear']   = substr($args['paymentDetails']['cardYear'], 2);
            if (strlen($args['paymentDetails']['cardMonth']) == 1) {
                $args['paymentDetails']['cardMonth'] = '0' . $args['paymentDetails']['cardMonth'];
            }
            $expry    = $args['paymentDetails']['cardMonth'] . $args['paymentDetails']['cardYear'];

            $result = $transaction->authorizeAndCapture($amount,$args['paymentDetails']['CardNo'],$expry);
        } else if($args['ach_type'] == 2){
            $transaction->setECheck($args['paymentDetails']['routeNumber'], $args['paymentDetails']['accountNumber'], $args['paymentDetails']['accountType'], $bank_name='Wells Fargo Bank NA', $args['paymentDetails']['accountName'], $args['paymentDetails']['secCodeEntryMethod']);

            $result = $transaction->authorizeAndCapture($amount,'check');
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: Invalid Payment Type</strong></div>');
            return false;
        }

        if ($result->response_code == '1' && $result->transaction_id != 0 && $result->transaction_id != '') {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function paytraceSale($args){
		$this->CI->load->config('paytrace');
        
        $amount = $args['amount'];
        $payAPI = new PayTraceAPINEW();
        $oauth_result = $payAPI->oAuthTokenGenerator("password", $args['gatewayUsername'], $args['gatewayPassword']);

        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
        
        if($oauth_moveforward){
            return false;
        }

        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        $oauth_token = sprintf("Bearer %s",$json['access_token']);

        $invoice_number = rand('500000', '200000');
        if($args['ach_type'] == 1){
            $request_data = array(
                "amount"          => $amount,
                "credit_card"     => array(
                    "number"           => $args['paymentDetails']['CardNo'],
                    "expiration_month" => $args['paymentDetails']['cardMonth'],
                    "expiration_year"  => $args['paymentDetails']['cardYear']
                ),
                "invoice_id"      => rand('500000', '200000'),
                "billing_address" => array(
                    "name" => $args['paymentDetails']['accountName'],
                    "street_address" => $args['paymentDetails']['Billing_Addr1']. ', '.$args['paymentDetails']['Billing_Addr2'],
                    "city" => $args['paymentDetails']['Billing_City'],
                    "state" => $args['paymentDetails']['Billing_State'],
                    "zip" => $args['paymentDetails']['Billing_Zipcode'],
                ),
            );

            if(!empty($cvv)){
                $request_data['csc'] = $cvv;
            }

            $reqURL = URL_KEYED_SALE;
            $add_level_data = true;
        } else {
            $request_data = array(
                "amount"          => $amount,
                "check"     => array(
                    "account_number"=> $args['paymentDetails']['accountNumber'],
                    "routing_number"=> $args['paymentDetails']['routeNumber'],
                ),
                "integrator_id" => $args['gatewaySignature'],
                "billing_address" => array(
                    "name" => $args['paymentDetails']['accountName'],
                    "street_address" => $args['paymentDetails']['Billing_Addr1']. ', '.$args['paymentDetails']['Billing_Addr2'],
                    "city" => $args['paymentDetails']['Billing_City'],
                    "state" => $args['paymentDetails']['Billing_State'],
                    "zip" => $args['paymentDetails']['Billing_Zipcode'],
                ),
            );

            $reqURL = URL_ACH_SALE;
            $add_level_data = false;
        }

        $request_data = json_encode($request_data);
        $result1      = $payAPI->processTransaction($oauth_token, $request_data, $reqURL);
        $result       = $payAPI->jsonDecode($result1['temp_json_response']);
        $result['http_status_code'] = $result1['http_status_code'];
        if ($result1['http_status_code'] == '200') {
            $response = true;
            if($add_level_data){
                if($result['success']){
                    $level_three_data = [
                        'card_no' => $args['paymentDetails']['CardNo'],
                        'merchID' => $this->merchantID,
                        'amount' => $amount,
                        'token' => $oauth_token,
                        'integrator_id' => $args['gatewaySignature'],
                        'transaction_id' => $result['transaction_id'],
                        'invoice_id' => $invoice_number,
                        'gateway' => 3,
                    ];
                    addlevelThreeDataInTransaction($level_three_data);
                }
            }
        } else {
            $response = false;
        }

        if(!empty( $result['errors'])){
            $err_msg = $this->getError( $result['errors']);
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
        }
        
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function paypalSale($args){
		$this->CI->load->config('paypal');

        $username  = $args['gatewayUsername'];
        $password  = $args['gatewayPassword'];
        $signature = $args['gatewaySignature'];

        $card_no  = $args['paymentDetails']['CardNo'];
        $cvv      =  $args['paymentDetails']['CardCVV'];
        $expmonth =  $args['paymentDetails']['cardMonth'];
        $exyear   = $args['paymentDetails']['cardYear'];
        $creditCardType = $args['paymentDetails']['CardType'];
        $creditCardType = ($creditCardType && !empty($creditCardType)) ? $creditCardType : 'Visa';

        $config = array(
            'Sandbox' => $this->CI->config->item('Sandbox'),
            'APIUsername' => $username,
            'APIPassword' => $password,
            'APISignature' => $signature,
            'APISubject' => '',
            'APIVersion' => $this->CI->config->item('APIVersion'),
        );

        $this->CI->load->library('paypal/Paypal_pro', $config);
        
        $DPFields = array(
            'paymentaction' => 'Sale',
            'ipaddress' => '',
            'returnfmfdetails' => '0'
        );
        
        $CCDetails = array(
            'creditcardtype' => $creditCardType,
            'acct' => $creditCardNumber,
            'expdate' => $padDateMonth.$expDateYear,
            'startdate' => '',
            'issuenumber' => ''
        );

        if(!empty($cvv2Number)){
            $CCDetails['cvv2'] = $cvv2Number;
        }
        
        $PayerInfo = array(
            'email' => $email, 								// Email address of payer.
            'payerid' => '', 							// Unique PayPal customer ID for payer.
            'payerstatus' => 'verified', 						// Status of payer.  Values are verified or unverified
            'business' => '' 							// Payer's business name.
        );  
        
        $PayerName = array(
            'salutation' => $companyName, 						// Payer's salutation.  20 char max.
            'firstname' => $firstName, 							// Payer's first name.  25 char max.
            'middlename' => '', 						// Payer's middle name.  25 char max.
            'lastname' => $lastName, 							// Payer's last name.  25 char max.
            'suffix' => ''								// Payer's suffix.  12 char max.
        );
    
        $BillingAddress = array(
            'street' => $address1, 						// Required.  First street address.
            'street2' => $address2, 						// Second street address.
            'city' => $city, 							// Required.  Name of City.
            'state' => $state, 							// Required. Name of State or Province.
            'countrycode' => $country, 					// Required.  Country code.
            'zip' => $zip, 							// Required.  Postal code of payer.
            'phonenum' => $phone 						// Phone Number of payer.  20 char max.
        );

            
        $PaymentDetails = array(
            'amt' => $amount, 							// Required.  Total amount of order, including shipping, handling, and tax.  
            'currencycode' => 'USD', 					// Required.  Three-letter currency code.  Default is USD.
            'itemamt' => '', 						// Required if you include itemized cart details. (L_AMTn, etc.)  Subtotal of items not including S&H, or tax.
            'shippingamt' => '', 					// Total shipping costs for the order.  If you specify shippingamt, you must also specify itemamt.
            'insuranceamt' => '', 					// Total shipping insurance costs for this order.  
            'shipdiscamt' => '', 					// Shipping discount for the order, specified as a negative number.
            'handlingamt' => '', 					// Total handling costs for the order.  If you specify handlingamt, you must also specify itemamt.
            'taxamt' => '', 						// Required if you specify itemized cart tax details. Sum of tax for all items on the order.  Total sales tax. 
            'desc' => '', 							// Description of the order the customer is purchasing.  127 char max.
            'custom' => '', 						// Free-form field for your own use.  256 char max.
            'invnum' => '', 						// Your own invoice or tracking number
            'buttonsource' => '', 					// An ID code for use by 3rd party apps to identify transactions.
            'notifyurl' => '', 						// URL for receiving Instant Payment Notifications.  This overrides what your profile is set to use.
            'recurring' => ''						// Flag to indicate a recurring transaction.  Value should be Y for recurring, or anything other than Y if it's not recurring.  To pass Y here, you must have an established billing agreement with the buyer.
        );					

        $PayPalRequestData = array(
            'DPFields' => $DPFields, 
            'CCDetails' => $CCDetails, 
            'PayerInfo' => $PayerInfo, 
            'PayerName' => $PayerName, 
            'BillingAddress' => $BillingAddress, 
            
            'PaymentDetails' => $PaymentDetails, 
            
        );
            
        $PayPalResult = $this->paypal_pro->DoDirectPayment($PayPalRequestData);
        if(!empty($PayPalResult["RAWRESPONSE"]) && ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"]))){
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $PayPalResult; 
            return $r_data;
        }
        return $response;
    }

    private function stripeSale($args){
        $amount = $args['amount'];
        $plugin = new ChargezoomStripe();
        $plugin->setApiKey($args['gatewayPassword']);
        $res = \Stripe\Token::create([
            'card' => [
                'number' =>$args['paymentDetails']['CardNo'],
                'exp_month' => $args['paymentDetails']['cardMonth'],
                'exp_year' =>  $args['paymentDetails']['cardYear'],
                'cvc' => $args['paymentDetails']['CardCVV']
            ]
        ]);

        $tcharge= json_encode($res);
        $rest = json_decode($tcharge);
        if($rest->id){
            $token = $rest->id;
        }

        $charge =	\Stripe\Charge::create(array(
            "amount" => $amount,
            "currency" => "usd",
            "source" => $token,
            "description" => "Charge Using Stripe Gateway",

        ));
        $charge= json_encode($charge);
        $result = json_decode($charge);
        if($result->paid=='1' && $result->failure_code==""){
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function usaepaySale($args){
		$this->CI->load->config('usaePay');
        
        $username   = $args['gatewayUsername'];
        $password   = $args['gatewayPassword'];
        $amount = $args['amount'];

        $invNo  =mt_rand(1000000,2000000); 
        $transaction = new umTransaction;
        $transaction->ignoresslcerterrors= ($this->CI->config->item('ignoresslcerterrors') !== null ) ? $this->CI->config->item('ignoresslcerterrors') : true;
        $transaction->key=$username;
        
        $transaction->pin=$password;
        $transaction->usesandbox=$this->CI->config->item('Sandbox');
        $transaction->invoice=$invNo;
        $transaction->description="Chargezoom Invoice Payment";
        $transaction->testmode=$this->CI->config->item('TESTMODE');
        $transaction->command="sale";

        $transaction->card = $args['paymentDetails']['CardNo'];
        $expyear   = substr($args['paymentDetails']['cardYear'],2);
        $expmonth = $args['paymentDetails']['cardMonth'];
        $cvv = $args['paymentDetails']['CardCVV'];

        if(strlen($expmonth)==1){
            $expmonth = '0'.$expmonth;
        }
        $expry    = $expmonth.$expyear;  
        $transaction->exp = $expry;
        if($cvv!="")
            $transaction->cvv2 = $cvv;

        $transaction->amount = $amount;
        $result = $transaction->Process();
        $msg = $transaction->result;
        $trID = $transaction->refnum;
					
        if(strtoupper($transaction->result)=='APPROVED' || strtoupper($transaction->result)=='SUCCESS'){
            $code = 200;
            $response = true;
        } else {
            $code = 300;
            $response = false;
        }

        $res =array('transactionCode'=> "$code", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;             
    }

    private function globalpaySale($args){
        $this->CI->load->config('globalpayments');

        $secretApiKey   = $args['gatewayPassword'];
        $amount = $args['amount'];
        $invNo  =mt_rand(1000000,2000000);

        $config = new ServicesConfig();
        $config->secretApiKey = $secretApiKey;
        $config->serviceUrl =  $this->CI->config->item('GLOBAL_URL');

        ServicesContainer::configure($config);

        if($args['ach_type'] == 1){
            $processPayment = new CreditCardData();
            $processPayment->number = $args['paymentDetails']['CardNo'];
            $processPayment->expMonth = $args['paymentDetails']['cardMonth'];
            $processPayment->expYear = $args['paymentDetails']['cardYear'];
            if($args['paymentDetails']['CardCVV']!="")
                $processPayment->cvn = $args['paymentDetails']['CardCVV'];
                
            $creditCardType = $args['paymentDetails']['CardType'];
            $creditCardType = ($creditCardType && !empty($creditCardType)) ? $creditCardType : 'Visa';
            $processPayment->cardType= $creditCardType;
        } else {
            $processPayment = new ECheck();
            $accountType = $args['paymentDetails']['accountType'];
            $accountHolderType = $args['paymentDetails']['accountHolderType'];

            $processPayment->accountNumber = $args['paymentDetails']['accountNumber'];
            $processPayment->routingNumber = $args['paymentDetails']['routeNumber'];

            if(strtolower($accountType) == 'checking'){
                $processPayment->accountType = 0;
            }else{
                $processPayment->accountType = 1;
            }

            if(strtoupper($accountHolderType) == 'PERSONAL'){
                $processPayment->checkType = 0;
            }else{
                $processPayment->checkType = 1;
            }
            $processPayment->checkHolderName = $args['paymentDetails']['accountName'];
            $processPayment->secCode = "WEB";
        }

        $address = new Address();
        $address->streetAddress1 = $args['paymentDetails']['Billing_Addr1'];
        $address->city = $args['paymentDetails']['Billing_City'];
        $address->state = $args['paymentDetails']['Billing_State'];
        $address->postalCode = $args['paymentDetails']['Billing_Zipcode'];
        $address->country = $args['paymentDetails']['Billing_Country'];

        $response = false;

        $trID = $msg = '';
        $responseCode = 300;
        try
        {
            $result = $processPayment->charge($amount)
            ->withCurrency(CURRENCY)
            ->withAddress($address)
            ->withInvoiceNumber($invNo)
            ->withAllowDuplicates(true)
            ->execute();

            $msg = $result->responseMessage;
            $trID = $result->transactionId;
            if($result->responseMessage=='APPROVAL' || strtoupper($result->responseMessage)=='SUCCESS'){
                
                if($args['ach_type'] == 1){
                    $transaction = new Transaction();
                    $transaction->transactionReference = new TransactionReference();
                    $levelCommercialData = new CommercialData(TaxType::SALES_TAX, 'Level_III');
                    $level_three_request = [
                        'card_no' => $args['paymentDetails']['CardNo'],
                        'amount' => $amount,
                        'invoice_id' => $invNo,
                        'merchID' => $this->merchantID,
                        'transaction_id' => $result->transactionId,
                        'transaction' => $transaction,
                        'levelCommercialData' => $levelCommercialData,
                        'gateway' => 7
                    ];
                    addlevelThreeDataInTransaction($level_three_request);
                }
                $responseCode = 200;  
                $response = true;              
            }
        }
        catch (BuilderException $e)
        {
            $msg= 'Build Exception Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (ConfigurationException $e)
        {
            $msg='ConfigurationException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (GatewayException $e)
        {
            $msg= 'GatewayException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (UnsupportedTransactionException $e)
        {
            $msg='UnsupportedTransactionException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (ApiException $e)
        {
            $msg=' ApiException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function cybersourceSale($args){
        $this->CI->load->config('cyber_pay');
        $amount = $args['amount'];

        $option =array();
        $option['merchantID']     = trim($args['gatewayUsername']);
        $option['apiKey']         = trim($args['gatewayPassword']);
        $option['secretKey']      = trim($args['gatewaySignature']);

        if($this->CI->config->item('Sandbox')){
            $env   = $this->CI->config->item('SandboxENV');
        } else{
            $env   = $this->CI->config->item('ProductionENV');
        }
        $option['runENV']      = $env;

        $commonElement = new CyberSource\ExternalConfiguration($option);
        $config = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance = new CyberSource\Api\PaymentsApi($apiclient);
        
        $cliRefInfoArr = [
            "code" => $this->merchantData['companyName']
        ];

        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        $processingInformationArr = [
            "capture" => true, "commerceIndicator" => "internet"
        ];

        $processingInformation = new CyberSource\Model\Ptsv2paymentsProcessingInformation($processingInformationArr);
                    
        $amountDetailsArr = [
            "totalAmount" => $amount,
            "currency" => CURRENCY
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $billtoArr = [
            "firstName" => $args['paymentDetails']['accountName'],
            "lastName" => '',
            "address1" => $args['paymentDetails']['Billing_Addr1'],
            "postalCode" => $args['paymentDetails']['Billing_Zipcode'],
            "locality" => $args['paymentDetails']['Billing_City'],
            "administrativeArea" => $args['paymentDetails']['Billing_State'],
            "country" => $args['paymentDetails']['Billing_Country'],
            "phoneNumber" => $args['paymentDetails']['Billing_Contact'],
            "company" => (isset($args['company'])) ? $args['company'] : '',
            "email" => (isset($args['email'])) ? $args['email'] : '',
        ];
        $billto = new CyberSource\Model\Ptsv2paymentsOrderInformationBillTo($billtoArr);

        $orderInfoArr = [
            "amountDetails" => $amountDetInfo, 
            "billTo" => $billto
        ];
        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArr);

        $paymentCardInfo = [
            "expirationYear" => $args['paymentDetails']['cardYear'],
            "number" => $args['paymentDetails']['CardNo'],
            "expirationMonth" => $args['paymentDetails']['cardMonth']
        ];
        
        if(!empty($args['paymentDetails']['CardCVV'])){
            $paymentCardInfo['securityCode'] = $args['paymentDetails']['CardCVV'];
        }

        $card = new CyberSource\Model\Ptsv2paymentsPaymentInformationCard($paymentCardInfo);
        
        $paymentInfoArr = [
            "card" => $card
        ];
        $payment_information = new CyberSource\Model\Ptsv2paymentsPaymentInformation($paymentInfoArr);

        $paymentRequestArr = [
            "clientReferenceInformation" => $client_reference_information, 
            "orderInformation" => $order_information, 
            "paymentInformation" => $payment_information, 
            "processingInformation" => $processingInformation
        ];
        $paymentRequest = new CyberSource\Model\CreatePaymentRequest($paymentRequestArr);

        $api_response = list($response, $statusCode, $httpHeader) = null;
        $trID = $msg = '';
        $responseCode = 300;
        $response = false;

        try
        {
            $trID =   $api_response[0]['id'];
            $msg  =   $api_response[0]['status'];
            if($api_response[0]['status']!="DECLINED" && $api_response[1]== '201'){
                $responseCode =   '200';
                $response = true;
            }
        }
        catch(Cybersource\ApiException $e)
        {
            $msg = $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$msg.' </strong></div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function itransactSale($args){
        $amount = $args['amount'];
        $accountDetails = $args['paymentDetails'];

        if($args['ach_type'] == 1){
            $expmonth = $accountDetails['cardMonth'];
            if (strlen($expmonth) > 1 && $expmonth <= 9) {
                $expmonth = substr($expmonth, 1);
            }

            $payload = array(
                "amount"          => ($amount * 100),
                "card"     => array(
                    "name" => $args['fullName'],
                    "number" => $accountDetails['CardNo'],
                    "exp_month" => $expmonth,
                    "exp_year"  => $accountDetails['cardYear'],
                ),
                "billing_address" => [
                    "line1" => $accountDetails['Billing_Addr1'],
                    "line2" => $accountDetails['Billing_Addr2'],
                    "city" => $accountDetails['Billing_City'],
                    "state" => $accountDetails['Billing_State'],
                    "postal_code" => $accountDetails['Billing_Zipcode'],
                    "country" => $accountDetails['Billing_Country']
                ],
            );
        } else {
            $payload = [
                "amount" => ($amount * 100),
                "ach" => [
                    "name" => $args['fullName'],
                    "account_number" => $accountDetails['accountNumber'], 
                    "routing_number" => $accountDetails['routeNumber'], 
                    "phone_number" => $accountDetails['Billing_Contact'], 
                    "sec_code" => $accountDetails['secCodeEntryMethod'], 
                    "savings_account" => ($accountDetails['accountType'] == 'savings') ? true : false, 
                ],
                "address" => [
                    "line1" => $accountDetails['Billing_Addr1'],
                    "line2" => $accountDetails['Billing_Addr2'],
                    "city" => $accountDetails['Billing_City'],
                    "state" => $accountDetails['Billing_State'],
                    "postal_code" => $accountDetails['Billing_Zipcode'],
                    "country" => $accountDetails['Billing_Country']
                ],
            ];
        }

        $sdk = new iTTransaction();
        $result = $sdk->postCardTransaction($args['gatewayUsername'], $args['gatewayPassword'], $payload);

        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
            $result['status_code'] = 200;
            $response = true;
        } else {
            $err_msg = $result['status'] = $result['error']['message'];
            $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function fluidPaySale($args){
        $this->CI->load->config('fluidpay');
        $gatewayEnvironment = $this->CI->config->item('environment');
        $amount = $args['amount'];
        $accountDetails = $args['paymentDetails'];

        if($args['ach_type'] == 1){
            $card_no = $accountDetails['CardNo'];
            $expmonth = $accountDetails['cardMonth'];
            $exyear = $accountDetails['cardYear'];
            
            $exyear1  = substr($exyear, 2);
            if (strlen($expmonth) == 1) {
                $expmonth = '0' . $expmonth;
            }
            $expry = $expmonth . $exyear1;

            $transaction = array(
                "type"                => "sale",
                "amount"              => $amount * 100,
                "currency"            => "USD",
                "ip_address"          => $_SERVER['REMOTE_ADDR'],
                "payment_method"      => array(
                    "card" => array(
                        "entry_type"      => "keyed",
                        "number"          => $card_no,
                        "expiration_date" => $expry,
                        // "cvc"             => $cvv,
                    ),
                ),
                "billing_address"     => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $args['contact'],
                    "fax"            => $args['contact'],
                    "email"          => $args['email'],
                ),
                "shipping_address"    => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $args['contact'],
                    "fax"            => $args['contact'],
                    "email"          => $args['email'],
                ),
            );

        } else {
            $transaction = array(
                "type"                => "sale",
                "amount"              => $amount * 100,
                "currency"            => "USD",
                "ip_address"          => $_SERVER['REMOTE_ADDR'],
                "payment_method"      => array(
                    "ach" => array(
                        "routing_number" => $accountDetails['routeNumber'],
                        "account_number" => $accountDetails['accountNumber'],
                        "sec_code"       => $accountDetails['secCodeEntryMethod'],
                        "account_type"   => $accountDetails['accountType'],
                        //"check_number":"1223",
                    ),
                ),
                "billing_address"     => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "address_line_2" => $accountDetails['Billing_Addr2'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $accountDetails['Billing_Contact'],
                    "fax"            => $accountDetails['Billing_Contact'],
                    "email"          => $args['email'],
                ),
                "shipping_address"    => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "address_line_2" => $accountDetails['Billing_Addr2'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $accountDetails['Billing_Contact'],
                    "fax"            => $accountDetails['Billing_Contact'],
                    "email"          => $args['email'],
                ),
            );
        }

        $response = false;
        $gatewayTransaction              = new Fluidpay();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->apiKey      = $args['gatewayUsername'];
        $result = $gatewayTransaction->processTransaction($transaction);
        if ($result['status'] == 'success') {
            $response = true;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function TSYSSale($args){
        $this->CI->load->config('TSYS');
        $gatewayEnvironment = $this->CI->config->item('environment');
        $amount = $args['amount'];
        $accountDetails = $args['paymentDetails'];

        $responseType = 'SaleResponse';

        $deviceID = $args['gatewayMerchantID'].'01';
        $gatewayTransaction              = new TSYS();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->deviceID = $deviceID;
        $result = $gatewayTransaction->generateToken($args['gatewayUsername'],$args['gatewayPassword'],$args['gatewayMerchantID']);
        //print_r($result);die();
        $generateToken = '';
        if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
            return false;
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
        }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
            $generateToken = $result['GenerateKeyResponse']['transactionKey'];
            
        }

        $gatewayTransaction->transactionKey = $generateToken;
        $orderId = time();

        $card_no = $accountDetails['CardNo'];
        $expmonth = $accountDetails['cardMonth'];
        $exyear = $accountDetails['cardYear'];
        $exyear1  = substr($exyear, 2);
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $expry = $expmonth .'/'. $exyear1;
        $amount = round($amount,2);
        $transaction['Sale'] = array(
            "deviceID"                          => $deviceID,
            "transactionKey"                    => $generateToken,
            "cardDataSource"                    => "MANUAL",  
            "transactionAmount"                 => (int)($amount * 100),
            "currencyCode"                      => "USD",
            "cardNumber"                        => $card_no,
            "expirationDate"                    => $expry,
            "addressLine1"                      => ($accountDetails['Billing_Addr1'] != '')?$accountDetails['Billing_Addr1']:'None',
            "zip"                               => ($accountDetails['Billing_Zipcode'] != '')?$accountDetails['Billing_Zipcode']:'None',
            "orderNumber"                       => "$orderId",
            "notifyEmailID"                     => (($args['email'] != ''))?$args['email']:'chargezoom@chargezoom.com',
            "firstName"                         => (($args['firstName'] != ''))?$args['firstName']:'None',
            "lastName"                          => (($args['lastName'] != ''))?$args['lastName']:'None',
            "terminalCapability"                => "ICC_CHIP_READ_ONLY",
            "terminalOperatingEnvironment"      => "ON_MERCHANT_PREMISES_ATTENDED",
            "cardholderAuthenticationMethod"    => "NOT_AUTHENTICATED",
            "terminalAuthenticationCapability"  => "NO_CAPABILITY",
            "terminalOutputCapability"          => "DISPLAY_ONLY",
            "maxPinLength"                      => "UNKNOWN",
            "terminalCardCaptureCapability"     => "NO_CAPABILITY",
            "cardholderPresentDetail"           => "CARDHOLDER_PRESENT",
            "cardPresentDetail"                 => "CARD_PRESENT",
            "cardDataInputMode"                 => "KEY_ENTERED_INPUT",
            "cardholderAuthenticationEntity"    => "OTHER",
            "cardDataOutputCapability"          => "NONE",

            "customerDetails"   => array( 
                "contactDetails" => array(
                    "addressLine1"=> ($accountDetails['Billing_Addr1'] != '')?$accountDetails['Billing_Addr1']:'None',
                    "addressLine2"  => ($accountDetails['Billing_Addr1'] != '')?$accountDetails['Billing_Addr1']:'None',
                    "city"=>($accountDetails['Billing_City'] != '')?$accountDetails['Billing_City']:'None',
                    "zip"=>($accountDetails['Billing_Zipcode'] != '')?$accountDetails['Billing_Zipcode']:'None',
                ),
                "shippingDetails" => array( 
                    "firstName"=>(($args['firstName'] != ''))?$args['firstName']:'None',
                    "lastName"=>(($args['lastName'] != ''))?$args['lastName']:'None',
                    "addressLine1"=>($accountDetails['Billing_Addr1'] != '')?$accountDetails['Billing_Addr1']:'None',
                    "addressLine2" => ($accountDetails['Billing_Addr1'] != '')?$accountDetails['Billing_Addr1']:'None',
                    "city"=>($accountDetails['Billing_City'] != '')?$accountDetails['Billing_City']:'None',
                    "zip"=>($accountDetails['Billing_Zipcode'] != '')?$accountDetails['Billing_Zipcode']:'None',
                    "emailID"=>(($args['email'] != ''))?$args['email']:'chargezoom@chargezoom.com'
                )
            )
        );
        
        $result = $gatewayTransaction->processTransaction($transaction);
        $response = false;
        $result['responseType'] = $responseType;
        if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
            $response = true;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function payarcSale($args){
        $this->CI->load->config('payarc');
        $this->CI->load->library('PayarcGateway');

        // PayArc Payment Gateway, set enviornment and secret key
        $this->CI->payarcgateway->setApiMode($this->CI->config->item('environment'));
        $this->CI->payarcgateway->setSecretKey($args['gatewayUsername']);

        // $card_data = $args['cardData'];
        $card_no  = $args['CustomerCard'];
        $expmonth =  $args['cardMonth'];
        $exyear   = $args['cardYear'];
        if (strlen($expmonth) == 1) {
            $expmonth = '0' . $expmonth;
        }
        $cvv = $args['CardCVV'];

        $amount = $args['amount'];
        $accountDetails = $args['paymentDetails'];

        // Create Credit Card Token
        $address_info = ['address_line1' => $accountDetails['Billing_Addr1'], 'address_line2' => '', 'state' => $accountDetails['Billing_State'], 'country' => ''];

        $token_response = $this->CI->payarcgateway->createCreditCardToken($card_no, $expmonth, $exyear, $cvv, $address_info);

        $token_data = json_decode($token_response['response_body'], 1);

        if(isset($token_data['status']) && $token_data['status'] == 'error'){
            $this->general_model->addPaymentLog(15, $_SERVER['REQUEST_URI'], ['env' => $this->CI->config->item('environment'),'accessKey'=>$args['gatewayUsername'], 'card_no' => $card_no, 'exp_month' => $expmonth, 'exp_year' => $exyear, 'cvv' => $cvv, 'address' => $address_info], $token_data);
            // Error while creating the credit card token
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $token_data['message'] . '</strong></div>');
            return false;

        } else if(isset($token_data['data']) && !empty($token_data['data']['id'])) {

            // If token created
            $token_id = $token_data['data']['id'];

            $charge_payload = [];

            $charge_payload['token_id'] = $token_id;
            
            if(isset($args['email']) && !empty($args['email']) && filter_var($args['email'], FILTER_VALIDATE_EMAIL)) {
                $charge_payload['email'] = $args['email']; // Customer's email address.
            }

            if(isset($args['contact']) && !empty($args['contact'])) {
                $charge_payload['phone_number'] = $args['contact']; // Customer's contact phone number..
            }

            if(isset($accountDetails['Billing_Zipcode']) && $accountDetails['Billing_Zipcode']) {
                $charge_payload['ship_to_zip'] = $accountDetails['Billing_Zipcode']; 
            }

            $charge_payload['amount'] = $args['amount'] * 100; // must be in cents and min amount is 50c USD

            $charge_payload['currency'] = 'usd'; 

            $charge_payload['capture'] = 1; // 0 for authorize and 1 for capture instantly

            $charge_payload['order_date'] = date('Y-m-d'); // Applicable for Level2 Charge for AMEX card only or Level3 Charge. The date the order was processed.

            $charge_payload['statement_description'] = '';

            $charge_response = $this->CI->payarcgateway->createCharge($charge_payload);

            $result = json_decode($charge_response['response_body'], 1);

            // Handle Card Decline Error
            if (isset($result['data']) && $result['data']['object']== 'Charge' && !empty($result['data']['failure_message']))
            {
                $result['message'] = $result['data']['failure_message'];
            }

            $isProcessed = false;
            if (isset($result['data']) && $result['data']['object']== 'Charge' && $result['data']['status'] == 'submitted_for_settlement') {
                $responseId = $result['data']['id'];
                // $this->CI->session->set_flashdata('success', 'Credit Card Payment Success.');
                $isProcessed = true;
            }

            if(isset($args['returnResult']) && $args['returnResult']){
                $r_data['response'] = $isProcessed; 
                $r_data['result'] = $result; 
                return $r_data;
            }
            return $isProcessed;
        }
    }

    private function cardPointeSale($args){

        $cardpointeuser   = $args['gatewayUsername'];
        $cardpointepass   = $args['gatewayPassword'];
        $cardpointeMerchID = $args['gatewayMerchantID'];
        $cardpointeSiteName  = $args['gatewaySignature'];
        $amount = $args['amount'];
        $accountDetails = $args['paymentDetails'];
		$client = new CardPointe();

        if($args['ach_type'] == 1){
            $card_no = $accountDetails['CardNo'];
            $expmonth = $accountDetails['cardMonth'];
            $exyear = $accountDetails['cardYear'];
            
            $exyear1  = substr($exyear, 2);
            if (strlen($expmonth) == 1) {
                $expmonth = '0' . $expmonth;
            }
            $expry = $expmonth . $exyear1;

            $transaction = array(
                "type"                => "sale",
                "amount"              => $amount * 100,
                "currency"            => "USD",
                "ip_address"          => $_SERVER['REMOTE_ADDR'],
                "payment_method"      => array(
                    "card" => array(
                        "entry_type"      => "keyed",
                        "number"          => $card_no,
                        "expiration_date" => $expry,
                        // "cvc"             => $cvv,
                    ),
                ),
                "billing_address"     => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $args['contact'],
                    "fax"            => $args['contact'],
                    "email"          => $args['email'],
                ),
                "shipping_address"    => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $args['contact'],
                    "fax"            => $args['contact'],
                    "email"          => $args['email'],
                ),
            );
            $full_name = $transaction['first_name'] . ' ' . $transaction['last_name'];
            $result = $client->authorize_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $transaction['payment_method']['card']['number'], $transaction['payment_method']['card']['expiration_date'], $transaction['amount'], $cvv, $full_name, $transaction['billing_address']['address_line_1'], $transaction['billing_address']['city'], $transaction['billing_address']['state'], $transaction['billing_address']['postal_code']);

        } else {
            $transaction = array(
                "type"                => "sale",
                "amount"              => $amount * 100,
                "currency"            => "USD",
                "ip_address"          => $_SERVER['REMOTE_ADDR'],
                "payment_method"      => array(
                    "ach" => array(
                        "routing_number" => $accountDetails['routeNumber'],
                        "account_number" => $accountDetails['accountNumber'],
                        "sec_code"       => $accountDetails['secCodeEntryMethod'],
                        "account_type"   => $accountDetails['accountType'],
                        //"check_number":"1223",
                    ),
                ),
                "billing_address"     => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "address_line_2" => $accountDetails['Billing_Addr2'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $accountDetails['Billing_Contact'],
                    "fax"            => $accountDetails['Billing_Contact'],
                    "email"          => $args['email'],
                ),
                "shipping_address"    => array(
                    "first_name"     => $args['firstName'],
                    "last_name"      => $args['lastName'],
                    "company"        => $args['companyName'],
                    "address_line_1" => $accountDetails['Billing_Addr1'],
                    "address_line_2" => $accountDetails['Billing_Addr2'],
                    "city"           => $accountDetails['Billing_City'],
                    "state"          => $accountDetails['Billing_State'],
                    "postal_code"    => $accountDetails['Billing_Zipcode'],
                    "phone"          => $accountDetails['Billing_Contact'],
                    "fax"            => $accountDetails['Billing_Contact'],
                    "email"          => $args['email'],
                ),
            );
            $full_name = $args['first_name'] . ' ' . $args['last_name'];
            $result = $client->ach_capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $accountDetails['accountNumber'], $accountDetails['routeNumber'], $transaction['amount'], $accountDetails['accountType'], $full_name, $accountDetails['Billing_Addr1'], $accountDetails['Billing_City'], $accountDetails['Billing_State'], $accountDetails['Billing_Zipcode']);

        }

        $response = false;
        if ($result['respcode'] == '00') {
            $response = true;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }
    
    /**
     * Sale Feature: Ends Here
     */

    /**
     * Refund Feature: common feature to refund a transaction
     */
    public function refundTransaction($args){
        $transactionId = $args['transactionId'];
        $storeResult = (isset($args['storeResult'])) ? true : false;

        if($args['appType'] == 1){
            $this->CI->load->model('customer_model', 'customer_model');
            $paydata = $this->CI->customer_model->get_transaction_details_by_id($this->merchantID, $transactionId);
        } else if($args['appType'] == 2){

        } else if($args['appType'] == 3){
            return false;
        } else if($args['appType'] == 4){
            return false;
        } else if($args['appType'] == 5){

        } else {
            return false;
        }

        $where = array('gatewayID' => $paydata['gatewayID']);
        $gt_result  = $this->CI->general_model->get_row_data('tbl_merchant_gateway', $where);
        
        if(!$gt_result || empty($gt_result)){
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Invalid gateway details </strong></div>');
            return false;
        }

        if(!isset($paydata['partial']) || !$paydata['partial'] ){
            $paydata['partial'] = 0;
        }

        $refundAmount = $paydata['transactionAmount'] - $paydata['partial'];

        $params = array_merge($gt_result, $args);
        $params['returnResult'] = $storeResult;
        $params['refundAmount'] = $refundAmount;
        $params['isEcheckPaymentType'] = $achType = (strrpos($paydata['gateway'], 'ECheck'))? true : false;
        $params['partialRefund'] = ( $paydata['transactionAmount'] == $paydata['refundAmount'] ) ? false : true;

        switch ($gt_result['gatewayType']) {
            case 1:
                $saleResult = $this->nmiRefund($params);
                break;
            case 2:
                $saleResult = $this->authPayRefund($params);
                break;
            case 3:
                $saleResult = $this->paytraceRefund($params);
                break;
            case 4:
                $saleResult = $this->paypalRefund($params);
                break;
            case 5:
                $saleResult = $this->stripeRefund($params);
                break;
            case 6:
                $saleResult = $this->usaepayRefund($params);
                break;
            case 7:
                $saleResult = $this->globalpayRefund($params);
                break;
            case 8:
                $saleResult = $this->cybersourceRefund($params);
                break;
            case 9:
                $saleResult = $this->nmiRefund($params);
                break;
            case 10:
                $saleResult = $this->itransactRefund($params);
                break;
            case 11:
            case 13:
                $saleResult = $this->fluidPayRefund($params);
                break;
            case 12:
                $saleResult = $this->TSYSRefund($params);
                break;
            case 14:
                $saleResult = $this->cardPointeRefund($params);
                break;
            default:
                $saleResult = false;
                break;
        }

        if($storeResult){
            $id = $this->CI->general_model->insert_gateway_transaction_data($saleResult['result'], 'refund', $gt_result['gatewayID'], $gt_result['gatewayType'], $args['customerListID'], $refundAmount, $this->merchantID, $args['crtxnID'], $this->resellerID, $args['invoiceID'], $achType, $args['transactionByUser']);
            $saleResult['id'] = $id;
        }
        return $saleResult;
    }

    private function nmiRefund($args){
        $nmiuser  = $args['gatewayUsername'];
        $nmipass  = $args['gatewayPassword'];
        $nmi_data = array('nmi_user' => $nmiuser, 'nmi_password' => $nmipass);

        // $tr_type  ='sale';
        $tr_type     = 'refund';
        $transaction = new nmiDirectPost($nmi_data);

        if($args['isEcheckPaymentType']){
            $transaction->setPayment('check');
        }

        $transaction->setTransactionId($args['transactionId']);
        $transaction->setAmount($args['refundAmount']);
        $transaction->refund($args['transactionId'], $args['refundAmount']);

        $result = $transaction->execute();

        $result = $transaction->execute();
        if ($result['response_code'] == '100') {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }
    
    private function authPayRefund($args){
        include_once APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';
        $this->CI->load->config('auth_pay');

        $apiloginID     = $args['gatewayUsername'];
        $transactionKey = $args['gatewayPassword'];
        $transaction    = new AuthorizeNetAIM($apiloginID, $transactionKey);
        $transaction->setSandbox($this->config->item('auth_test_mode'));

        if($args['isEcheckPaymentType']){
            $transaction->__set('method','echeck');
        }

        $result = $transaction->credit($args['transactionId'], $args['refundAmount'], $args['transactionCard']);
        if ($result->response_code == '1') {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function paytraceRefund($args){
        $this->CI->load->config('paytrace');
        
        $amount = $args['amount'];
        $payAPI = new PayTraceAPINEW();
        $oauth_result = $payAPI->oAuthTokenGenerator("password", $args['gatewayUsername'], $args['gatewayPassword']);

        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);
        
        if($oauth_moveforward){
            return false;
        }

        $json = $payAPI->jsonDecode($oauth_result['temp_json_response']); 
        $oauth_token = sprintf("Bearer %s",$json['access_token']);

        $total        = $args['refundAmount'];
        $request_data = array("transaction_id" => $args['transactionId'], 'amount' => $total);
        $url = URL_TRID_REFUND;

        if($args['isEcheckPaymentType']){
            $request_data = array(
                "check_transaction_id" => $args['transactionId'],
                "integrator_id" => $args['gatewaySignature'],
                'amount' => $total,
            );
            $url = URL_ACH_REFUND_TRANSACTION;
        }

        $request_data = json_encode($request_data);
        $result1      = $payAPI->processTransaction($oauth_token, $request_data, $reqURL);
        $result       = $payAPI->jsonDecode($result1['temp_json_response']);
        $result['http_status_code'] = $result1['http_status_code'];
        if ($result1['http_status_code'] == '200') {
            $response = true;
        } else {
            $response = false;
        }

        if(!empty( $result['errors'])){
            $err_msg = $this->getError( $result['errors']);
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">'.$err_msg.'</div>');  
        }
        
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function paypalRefund($args){
        $this->CI->load->config('paypal');

        $username  = $args['gatewayUsername'];
        $password  = $args['gatewayPassword'];
        $signature = $args['gatewaySignature'];

        $config = array(
            'Sandbox' => $this->CI->config->item('Sandbox'),
            'APIUsername' => $username,
            'APIPassword' => $password,
            'APISignature' => $signature,
            'APISubject' => '',
            'APIVersion' => $this->CI->config->item('APIVersion'),
        );

        $this->CI->load->library('paypal/Paypal_pro', $config);

        if ($args['partialRefund']){
            $restype = "Partial";
        } else {
            $restype = "Full";
        }

        $RTFields = array(
            'transactionid'       => $args['transactionId'], // Required.  PayPal transaction ID for the order you're refunding.
            'payerid'             => '', // Encrypted PayPal customer account ID number.  Note:  Either transaction ID or payer ID must be specified.  127 char max
            'invoiceid'           => '', // Your own invoice tracking number.
            'refundtype'          => $restype, // Required.  Type of refund.  Must be Full, Partial, or Other.
            'amt'                 => $args['refundAmount'], // Refund Amt.  Required if refund type is Partial.
            'currencycode'        => '', // Three-letter currency code.  Required for Partial Refunds.  Do not use for full refunds.
            'note'                => '', // Custom memo about the refund.  255 char max.
            'retryuntil'          => '', // Maximum time until you must retry the refund.  Note:  this field does not apply to point-of-sale transactions.
            'refundsource'        => '', // Type of PayPal funding source (balance or eCheck) that can be used for auto refund.  Values are:  any, default, instant, eCheck
            'merchantstoredetail' => '', // Information about the merchant store.
            'refundadvice'        => '', // Flag to indicate that the buyer was already given store credit for a given transaction.  Values are:  1/0
            'refunditemdetails'   => '', // Details about the individual items to be returned.
            'msgsubid'            => '', // A message ID used for idempotence to uniquely identify a message.
            'storeid'             => '', // ID of a merchant store.  This field is required for point-of-sale transactions.  50 char max.
            'terminalid'          => '', // ID of the terminal.  50 char max.
        );

        $PayPalRequestData = array('RTFields' => $RTFields);

        $PayPalResult = $this->paypal_pro->RefundTransaction($PayPalRequestData);
        if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $PayPalResult; 
            return $r_data;
        }
        return $response;
    }

    private function stripeRefund($args){
        $plugin = new ChargezoomStripe();
        $plugin->setApiKey($args['gatewayPassword']);
        $charge = \Stripe\Refund::create(array(
            "charge" => $args['transactionId'],
        ));

        $charge = json_encode($charge);
        $result = json_decode($charge);
        if (strtoupper($result->status) == strtoupper('succeeded')) {
            $response = true;
        } else {
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function usaepayRefund($args){
		$this->CI->load->config('usaePay');
        $transaction                      = new umTransaction;
        $transaction->ignoresslcerterrors = ($this->config->item('ignoresslcerterrors') !== null) ? $this->config->item('ignoresslcerterrors') : true;

        $transaction->key        = $args['gatewayUsername']; // Your Source Key
        $transaction->pin        = $args['gatewayPassword']; // Source Key Pin
        $transaction->usesandbox = $this->config->item('Sandbox');
        $transaction->testmode   = $this->config->item('TESTMODE');
        $transaction->command    = "refund";
        $transaction->refnum     = $args['transactionId'];
        $transaction->amount     = $args['refundAmount'];
        $transaction->Process();
        $msg = $transaction->result;
        $trID = $transaction->refnum;

        if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {
            $code = 200;
            $response = true;
        } else {
            $code = 300;
            $response = false;
        }

        $res =array('transactionCode'=> "$code", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;  
    }

    private function globalpayRefund($args){
        $this->CI->load->config('globalpayments');
        $secretApiKey = $args['gatewayPassword'];
        $merchantID = $this->merchantID;
        $config     = new ServicesConfig();

        $config->secretApiKey = $secretApiKey;
        $config->serviceUrl   = $this->config->item('GLOBAL_URL');

        $response = false;

        $trID = $msg = '';
        $responseCode = 300;
        try
        {
            $result = Transaction::fromId($args['transactionId'])
                        ->refund($args['refundAmount'])
                        ->withCurrency("USD")
                        ->execute();

            $msg = $result->responseMessage;
            $trID = $result->transactionId;
            if($result->responseMessage=='APPROVAL' || strtoupper($result->responseMessage)=='SUCCESS'){
                $responseCode = 200;  
                $response = true;              
            }
        }
        catch (BuilderException $e)
        {
            $msg= 'Build Exception Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (ConfigurationException $e)
        {
            $msg='ConfigurationException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (GatewayException $e)
        {
            $msg= 'GatewayException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (UnsupportedTransactionException $e)
        {
            $msg='UnsupportedTransactionException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }
        catch (ApiException $e)
        {
            $msg=' ApiException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger">  <strong>'.$msg.'</strong>.</div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function cybersourceRefund($args){
        $this->CI->load->config('cyber_pay');
        $amount = $args['refundAmount'];

        $option =array();
        $option['merchantID']     = trim($args['gatewayUsername']);
        $option['apiKey']         = trim($args['gatewayPassword']);
        $option['secretKey']      = trim($args['gatewaySignature']);

        if($this->CI->config->item('Sandbox')){
            $env   = $this->CI->config->item('SandboxENV');
        } else{
            $env   = $this->CI->config->item('ProductionENV');
        }
        $option['runENV']      = $env;

        $commonElement = new CyberSource\ExternalConfiguration($option);

        $config = $commonElement->ConnectionHost();

        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance   = new CyberSource\Api\RefundApi($apiclient);

        $cliRefInfoArr = [
            "code" => "Refund Payment",
        ];
        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        $amountDetailsArr             = [
            "totalAmount" => $amount,
            "currency"    => CURRENCY,
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $orderInfoArry = [
            "amountDetails" => $amountDetInfo,
        ];

        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
        $paymentRequestArr = [
            "clientReferenceInformation" => $client_reference_information,
            "orderInformation"           => $order_information,
        ];

        $paymentRequest = new CyberSource\Model\RefundPaymentRequest($paymentRequestArr);
        $merchantID     = $this->merchantID;
        $inID           = '';
        $crtxnID      = '';
        $ins_id       = '';
        $tr1ID        = '';

        $responseCode =   '300';
        $response = false;
        $tr_type      = 'refund';
        
        $api_response = list($response, $statusCode, $httpHeader) = null;
        try
        {
            $api_response = $api_instance->refundPayment($paymentRequest, $tID);
            if ($api_response[0]['status'] != "DECLINED" && $api_response[1] == '201') {
                $pay_status = 'SUCCESS';
                $trID      = $api_response[0]['id'];
                $msg        = $api_response[0]['status'];
                $responseCode =   '200';
                $response = true;
            }
        }
        catch(Cybersource\ApiException $e)
        {
            $msg = $e->getMessage();
            $this->CI->session->set_flashdata('message','<div class="alert alert-danger"> <strong>Error:'.$msg.' </strong></div>');
        }

        $res =array('transactionCode'=> "$responseCode", 'status'=>$msg, 'transactionId'=> $trID );
        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $res; 
            return $r_data;
        }
        return $response;
    }

    private function itransactRefund($args){
        $amount = $args['refundAmount'];
        $tID = $args['transactionId'];

        $sdk = new iTTransaction();
        $result = $sdk->refundTransaction($args['gatewayUsername'], $args['gatewayPassword'], $payload, "$tID");

        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
            $result['status_code'] = 200;
            $response = true;
        } else {
            $err_msg = $result['status'] = $result['error']['message'];
            $result['id'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function fluidPayRefund($args){
        $this->CI->load->config('fluidpay');
        $gatewayEnvironment = $this->CI->config->item('environment');
        $amount = $args['refundAmount'];

        $response = false;
        $gatewayTransaction              = new Fluidpay();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->apiKey      = $args['gatewayUsername'];

        $refundAmount = $amount * 100;
        $payload = [
            'amount' => round($refundAmount,2)
        ];

        $result = $gatewayTransaction->refundTransaction($args['transactionId'], $payload);
        if ($result['status'] == 'success') {
            $response = true;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function TSYSRefund($args){
        $this->CI->load->config('TSYS');
        $gatewayEnvironment = $this->CI->config->item('environment');
        $amount = $args['refundAmount'];
        $accountDetails = $args['paymentDetails'];

        $responseType = 'SaleResponse';

        $deviceID = $args['gatewayMerchantID'].'01';
        $gatewayTransaction              = new TSYS();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->deviceID = $deviceID;
        $result = $gatewayTransaction->generateToken($args['gatewayUsername'],$args['gatewayPassword'],$args['gatewayMerchantID']);
        //print_r($result);die();
        $generateToken = '';
        if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL'){
            return false;
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:'.$result['GenerateKeyResponse']['responseMessage'].' </strong>.</div>');
        }else if(isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success'){
            $generateToken = $result['GenerateKeyResponse']['transactionKey'];
            
        }

        $gatewayTransaction->transactionKey = $generateToken;

        $amount = round($amount,2);
        
        $payload = [
            'amount' => ($amount * 100)
        ];
        $result = $gatewayTransaction->refundTransaction($args['transactionId'], $payload);
        
        $response = false;
        $result['responseType'] = $responseType;
        if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
            $response = true;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }

    private function payarcRefund($args){
        
        $response = false;

        $this->CI->load->config('payarc');
        $this->CI->load->library('PayarcGateway');

        // PayArc Payment Gateway, set enviornment and secret key
        $this->CI->payarcgateway->setApiMode($this->CI->config->item('environment'));
        $this->CI->payarcgateway->setSecretKey($args['gatewayUsername']);

        $tID = $args['transactionId'];
        $amount = $args['refundAmount'];

        $charge_response = $this->CI->payarcgateway->refundCharge($tID, ($amount * 100));

        $result = json_decode($charge_response['response_body'], 1);
            
        if (isset($result['data']) && $result['data']['status'] == 'refunded') {
            $response = true; 
            
        } else {
            $response = false; 
            $err_msg      = $result['message'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
        }

        return ['response'=>$response, 'result' => $result];

    }

    private function cardPointeRefund($args){
        $cardpointeuser   = $args['gatewayUsername'];
        $cardpointepass   = $args['gatewayPassword'];
        $cardpointeMerchID = $args['gatewayMerchantID'];
        $cardpointeSiteName  = $args['gatewaySignature'];
        $amount = $args['refundAmount'];
        $tID = $args['transactionId'];

        $client = new CardPointe();
        $result = $client->refund($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $tID, $amount * 100);

        if ($result['resptext'] == 'Approval') {
            $result['status_code'] = 200;
            $response = true;
        } else {
            $err_msg = $result['resptext'];
            $result['retred'] = (isset($result['error']['transaction_id'])) ? $result['error']['transaction_id'] : 'TXNFAILED'.time();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
            $response = false;
        }

        if(isset($args['returnResult']) && $args['returnResult']){
            $r_data['response'] = $response; 
            $r_data['result'] = $result; 
            return $r_data;
        }
        return $response;
    }


    /**
     * Capture Feature: common feature to capture an authorised transaction
    */
    public function _captureTransaction($args)
    {
        if (!isset($args['transactionID']) || empty($args['transactionID'])) {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Invalid gateway details </strong></div>');
            return false;
        }

        $transactionID = $args['transactionID'];
        $con           = array('transactionID' => $transactionID);

        $where     = array('gatewayID' => $args['gatewayID']);
        $gt_result = $this->CI->general_model->get_row_data('tbl_merchant_gateway', $where);
        if (!$gt_result || empty($gt_result)) {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Invalid gateway details </strong></div>');
            return false;
        }

        $storeResult  = (isset($args['storeResult'])) ? $args['storeResult'] : false;
        $returnResult = (isset($args['returnResult'])) ? $args['returnResult'] : $storeResult;

        $params                 = array_merge($gt_result, $args);
        $params['returnResult'] = $returnResult;

        switch ($gt_result['gatewayType']) {
            case 1:
                $captureResult = $this->nmiCapture($params);
                break;
            case 2:
                $captureResult = $this->authPayCapture($params);
                break;
            case 3:
                $captureResult = $this->paytraceCapture($params);
                break;
            case 4:
                $captureResult = $this->paypalCapture($params);
                break;
            case 5:
                $captureResult = $this->stripeCapture($params);
                break;
            case 6:
                $captureResult = $this->usaepayCapture($params);
                break;
            case 7:
                $captureResult = $this->globalpayCapture($params);
                break;
            case 8:
                $captureResult = $this->cybersourceCapture($params);
                break;
            case 9:
                $captureResult = $this->nmiCapture($params);
                break;
            case 10:
                $captureResult = $this->itransactCapture($params);
                break;
            case 11:
            case 13:
                $captureResult = $this->fluidPayCapture($params);
                break;
            case 12:
                $captureResult = $this->TSYSCapture($params);
                break;
            case 15:
                $captureResult = $this->payarcCapture($params);
                break;
            case 14:
                $captureResult = $this->cardPonteCapture($params);
                break;
            case 16:
                $captureResult = $this->EPXCapture($params);
                break;
            default:
                $captureResult = false;
                break;
        }

        if ($captureResult) {
            $condition   = array('transactionID' => $transactionID);
            $update_data = array('transaction_user_status' => "4", 'transactionModified' => date('Y-m-d H:i:s'));
            $this->CI->general_model->update_row_data('customer_transaction', $condition, $update_data);
        }

        if ($storeResult) {
            $id                  = $this->CI->general_model->insert_gateway_transaction_data($captureResult['result'], 'capture', $gt_result['gatewayID'], $gt_result['gatewayType'], $args['customerListID'], $args['amount'], $this->merchantID, null, $this->resellerID, null, false, $args['transactionByUser']);
            $captureResult['id'] = $id;
        }
        return $captureResult;
    }

    private function nmiCapture($args)
    {

        $nmi_data    = array('nmi_user' => $args['gatewayUsername'], 'nmi_password' => $args['gatewayPassword']);
        $transaction = new nmiDirectPost($nmi_data);

        $transaction->setTransactionId($args['transactionID']);
        $transaction->setAmount($args['amount']);
        $transaction->capture($args['transactionID'], $args['amount']);
        $result = $transaction->execute();
        if ($result['response_code'] == '100') {
            $response = true;
        } else {
            $response = false;
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['responsetext'] . '</strong></div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $response;
    }

    private function authPayCapture($args)
    {
        include_once APPPATH . 'third_party/authorizenet_lib/AuthorizeNetAIM.php';

        $this->CI->load->config('auth_pay');
        $transaction = new AuthorizeNetAIM($args['gatewayUsername'], $args['gatewayPassword']);
        $transaction->setSandbox($this->CI->config->item('auth_test_mode'));

        $result = $transaction->priorAuthCapture($args['transactionID'], $args['amount']);
        if ($result->response_code == '1') {
            $response = true;
        } else {
            $response = false;
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $response;
    }

    private function paytraceCapture($args)
    {
        $this->CI->load->config('paytrace');
        $payAPI       = new PayTraceAPINEW();
        $oauth_result = $payAPI->oAuthTokenGenerator("password", $args['gatewayUsername'], $args['gatewayPassword']);

        $oauth_moveforward = $payAPI->isFoundOAuthTokenError($oauth_result);

        if ($oauth_moveforward) {
            return false;
        }

        $json        = $payAPI->jsonDecode($oauth_result['temp_json_response']);
        $oauth_token = sprintf("Bearer %s", $json['access_token']);

        $request_data = array("transaction_id" => $tID);
        $request_data = json_encode($request_data);

        $result   = $payAPI->processTransaction($oauth_token, $request_data, URL_CAPTURE);
        $response = $payAPI->jsonDecode($result['temp_json_response']);
        if ($result['http_status_code'] == '200') {
            $resp = true;

            $card_last_number = $args['transactionCard'];
            $card_detail      = $this->CI->db1->select('CardType')->from('customer_card_data')->where('merchantID = ' . $this->merchantID . ' AND customerCardfriendlyName like "%' . $card_last_number . '%"')->get()->row_array();

            $cardType = isset($card_detail['CardType']) ? strtolower($card_detail['CardType']) : '';

            // add level three data in transaction
            if ($response['success']) {
                $level_three_data = [
                    'card_no'        => '',
                    'merchID'        => $this->merchantID,
                    'amount'         => $args['amount'],
                    'token'          => $oauth_token,
                    'integrator_id'  => $args['gatewaySignature'],
                    'transaction_id' => $response['transaction_id'],
                    'invoice_id'     => '',
                    'gateway'        => 3,
                    'card_type'      => $cardType,
                ];
                addlevelThreeDataInTransaction($level_three_data);
            }
        } else {
            $resp = false;
        }

        if (!empty($result['errors'])) {
            $err_msg = $this->getError($result['errors']);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">' . $err_msg . '</div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $resp;
            $r_data['result']   = $response;
            return $r_data;
        }
        return $resp;
    }

    private function paypalCapture($args)
    {
        $this->CI->load->config('paypal');

        $username  = $args['gatewayUsername'];
        $password  = $args['gatewayPassword'];
        $signature = $args['gatewaySignature'];

        $config = array(
            'Sandbox'      => $this->CI->config->item('Sandbox'), // Sandbox / testing mode option.
            'APIUsername'  => $username, // PayPal API username of the API caller
            'APIPassword'  => $password, // PayPal API password of the API caller
            'APISignature' => $signature, // PayPal API signature of the API caller
            'APISubject'   => '', // PayPal API subject (email address of 3rd party user that has granted API permission for your app)
            'APIVersion'   => $this->CI->config->item('APIVersion'), // API version you'd like to use for your call.  You can set a default version in the class and leave this blank if you want.
        );

        $this->CI->load->library('paypal/Paypal_pro', $config);

        $DCFields = array(
            'authorizationid' => $args['transactionID'],
            'amt'             => $args['amount'],
            'completetype'    => 'Complete',
            'currencycode'    => '',
            'invnum'          => '',
            'note'            => '',
            'softdescriptor'  => '',
            'storeid'         => '',
            'terminalid'      => '',
        );

        $PayPalRequestData = array('DCFields' => $DCFields);
        $PayPalResult      = $this->paypal_pro->DoCapture($PayPalRequestData);

        if ("SUCCESS" == strtoupper($PayPalResult["ACK"]) || "SUCCESSWITHWARNING" == strtoupper($PayPalResult["ACK"])) {
            $response = true;
        } else {
            $response     = false;
            $code         = '401';
            $responsetext = urldecode($PayPalResult['L_LONGMESSAGE0']);
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: "' . $responsetext . '"</strong>.</div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $PayPalResult;
            return $r_data;
        }
        return $response;
    }

    private function stripeCapture($args)
    {

        $plugin = new ChargezoomStripe();
        $plugin->setApiKey($args['gatewayPassword']);
        $response = false;

        try {
            $ch     = \Stripe\Charge::retrieve($args['transactionID']);
            $charge = $ch->capture();
        } catch (\Stripe\Error\Card $e) {
            $body = $e->getJsonBody();
            $err  = $body['error'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err['message'] . '</strong></div>');
            return false;
        }
        $tcharge = json_encode($charge);
        $rest    = json_decode($tcharge);
        if (strtoupper($rest->status) == strtoupper('succeeded')) {
            $response = true;
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $rest->status . '</strong></div>');
            $response = false;
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $rest;
            return $r_data;
        }
        return $response;

    }

    private function usaepayCapture($args)
    {
        $this->CI->load->config('usaePay');

        $transaction                      = new umTransaction;
        $transaction->ignoresslcerterrors = ($this->CI->config->item('ignoresslcerterrors') !== null) ? $this->CI->config->item('ignoresslcerterrors') : true;

        $transaction->key        = $args['gatewayUsername'];
        $transaction->pin        = $args['gatewayPassword'];
        $transaction->usesandbox = $this->CI->config->item('Sandbox');
        $transaction->testmode   = $this->CI->config->item('TESTMODE');

        $transaction->command = "capture";
        $transaction->refnum  = $args['transactionID']; // Specify refnum of the transaction that you would like to capture.
        $transaction->Process();

        $msg  = $transaction->result;
        $trID = $transaction->refnum;

        $response = false;
        if (strtoupper($transaction->result) == 'APPROVED' || strtoupper($transaction->result) == 'SUCCESS') {
            $code     = 200;
            $response = true;
        } else {
            $code     = 300;
            $response = false;
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $msg . '</strong></div>');
        }

        $res = array('transactionCode' => "$code", 'status' => $msg, 'transactionId' => $trID);
        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $res;
            return $r_data;
        }
        return $response;
    }

    private function globalpayCapture($args)
    {
        $this->CI->load->config('globalpayments');

        $config               = new ServicesConfig();
        $config->secretApiKey = $args['gatewayPassword'];
        $config->serviceUrl   = $this->CI->config->item('GLOBAL_URL');

        $msg          = $trID          = '';
        $response     = false;
        $responseCode = 300;
        ServicesContainer::configure($config);

        try {
            $response = Transaction::fromId($args['transactionID'])->capture($args['amount'])->execute();
            if ($response->responseMessage == 'APPROVAL' || strtoupper($response->responseMessage) == 'SUCCESS') {
                $msg          = $response->responseMessage;
                $trID         = $response->transactionId;
                $responseCode = 200;
                $response     = true;
            } else {
                $msg          = $response->responseMessage;
                $trID         = $response->transactionId;
                $responseCode = $response->responseCode;
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Payment Failed ' . $msg . '</strong></div>');
            }
        } catch (BuilderException $e) {
            $error = 'Build Exception Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
        } catch (ConfigurationException $e) {
            $error = 'ConfigurationException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
        } catch (GatewayException $e) {
            $error = 'GatewayException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
        } catch (UnsupportedTransactionException $e) {
            $error = 'UnsupportedTransactionException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
        } catch (ApiException $e) {
            $error = ' ApiException Failure: ' . $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>' . $error . '</strong>.</div>');
        }

        $res = array('transactionCode' => "$responseCode", 'status' => $msg, 'transactionId' => $trID);
        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $res;
            return $r_data;
        }
        return $response;
    }

    private function cybersourceCapture($args)
    {
        $this->CI->load->config('cyber_pay');

        $option               = array();
        $option['merchantID'] = trim($args['gatewayUsername']);
        $option['apiKey']     = trim($args['gatewayPassword']);
        $option['secretKey']  = trim($args['gatewaySignature']);

        if ($this->CI->config->item('Sandbox')) {
            $env = $this->CI->config->item('SandboxENV');
        } else {
            $env = $this->CI->config->item('ProductionENV');
        }

        $option['runENV'] = $env;

        $commonElement = new CyberSource\ExternalConfiguration($option);

        $config         = $commonElement->ConnectionHost();
        $merchantConfig = $commonElement->merchantConfigObject();
        $apiclient      = new CyberSource\ApiClient($config, $merchantConfig);
        $api_instance   = new CyberSource\Api\CaptureApi($apiclient);

        $cliRefInfoArr = [
            "code" => $this->merchantData['companyName'],
        ];

        $client_reference_information = new CyberSource\Model\Ptsv2paymentsClientReferenceInformation($cliRefInfoArr);
        $amountDetailsArr             = [
            "totalAmount" => $args['amount'],
            "currency"    => CURRENCY,
        ];
        $amountDetInfo = new CyberSource\Model\Ptsv2paymentsOrderInformationAmountDetails($amountDetailsArr);

        $orderInfoArry = [
            "amountDetails" => $amountDetInfo,
        ];

        $order_information = new CyberSource\Model\Ptsv2paymentsOrderInformation($orderInfoArry);
        $requestArr        = [
            "clientReferenceInformation" => $client_reference_information,
            "orderInformation"           => $order_information,
        ];
        //Creating model
        $request      = new CyberSource\Model\CapturePaymentRequest($requestArr);
        $api_response = list($response, $statusCode, $httpHeader) = null;
        $type         = 'auth_capture';

        $trID         = $msg         = '';
        $responseCode = 300;
        $response     = false;
        try {
            $api_response = $api_instance->capturePayment($request, $args['transactionID']);
            if ($api_response[0]['status'] != "DECLINED" && $api_response[1] == '201') {
                $trID = $api_response[0]['id'];
                $msg  = $api_response[0]['status'];

                $responseCode = '200';
                $response     = true;
            } else {
                $trID         = $api_response[0]['id'];
                $msg          = $api_response[0]['status'];
                $responseCode = $api_response[1];
                $response     = false;

                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Payment Failed ' . $msg . '</strong></div>');
            }
        } catch (Cybersource\ApiException $e) {
            $error = $e->getMessage();
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error:' . $error . ' </strong></div>');
        }

        $res = array('transactionCode' => "$responseCode", 'status' => $msg, 'transactionId' => $trID);
        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $res;
            return $r_data;
        }
        return $response;
    }

    private function itransactCapture($args)
    {
        $apiUsername = $args['gatewayUsername'];
        $apiKey      = $args['gatewayPassword'];

        $payload = [
            'amount' => ($args['amount'] * 100),
        ];

        $isProcessed = false;

        $sdk    = new iTTransaction();
        $result = $sdk->captureAuthTransaction($apiUsername, $apiKey, $payload, $args['transactionID']);

        if ($result['status_code'] == '200' || $result['status_code'] == '201') {
            $result['status_code'] = '200';
            $isProcessed           = true;
        } else {
            $err_msg = $result['status'] = $result['error']['message'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err_msg . '</strong></div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $isProcessed;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $isProcessed;
    }

    private function fluidPayCapture($args)
    {
        $this->CI->load->config('fluidpay');
        $gatewayEnvironment = $this->CI->config->item('environment');

        $gatewayTransaction              = new Fluidpay();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->apiKey      = $args['gatewayUsername'];

        $transactionData = [
            "amount" => ( (float) $args['amount']) * 100,
        ];

        $result = $gatewayTransaction->captureTransaction($args['transactionID'], $transactionData);

        $response = false;
        if ($result['status'] == 'success') {
            $response = true;
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['msg'] . '</strong></div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $response;
    }

    private function TSYSCapture($args)
    {
        $this->CI->load->config('TSYS');

        $gatewayEnvironment              = $this->CI->config->item('environment');
        $deviceID                        = $args['gatewayMerchantID'] . '01';
        $gatewayTransaction              = new TSYS();
        $gatewayTransaction->environment = $gatewayEnvironment;
        $gatewayTransaction->deviceID    = $deviceID;
        $result                          = $gatewayTransaction->generateToken($args['gatewayUsername'], $args['gatewayPassword'], $args['gatewayMerchantID']);
        $generateToken                   = '';
        $status                          = 1;

        if (isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'FAIL') {

            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['GenerateKeyResponse']['responseMessage'] . '</strong></div>');

            $status = 0;

        } else if (isset($result['GenerateKeyResponse']['status']) && $result['GenerateKeyResponse']['status'] == 'PASS' && $result['GenerateKeyResponse']['responseMessage'] == 'Success') {
            $generateToken = $result['GenerateKeyResponse']['transactionKey'];
            $status        = 1;

        }
        if ($status == 1) {
            $gatewayTransaction->transactionKey = $generateToken;

            $result = $gatewayTransaction->captureTransaction($args['transactionID']);
            $responseType = 'CaptureResponse';

            if (isset($result[$responseType]['status']) && $result[$responseType]['status'] == 'PASS') {
                $response = true;
            } else {
                $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result[$responseType]['responseMessage'] . '</strong></div>');
                $response = false;
            }

            if (isset($args['returnResult']) && $args['returnResult']) {
                $r_data['response'] = $response;
                $r_data['result']   = $result;
                return $r_data;
            }
            return $response;
        } else {
            return false;
        }

    }

    private function payarcCapture($args)
    {

        $this->CI->load->config('payarc');
        $this->CI->load->library('PayarcGateway');

        // PayArc Payment Gateway, set enviornment and secret key
        $this->CI->payarcgateway->setApiMode($this->CI->config->item('environment'));
        $this->CI->payarcgateway->setSecretKey($args['gatewayUsername']);

        $isProcessed = false;
        $charge_response = $this->payarcgateway->captureCharge($tID, $amount * 100);
        $result = json_decode($charge_response['response_body'], 1);
        if (isset($result['data']) && $result['data']['object']== 'Charge') {
            $isProcessed = true;
        } else {
            $err_msg      = $result['message'];
            $result['id'] = '';
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger">Transaction Failed - ' . $err_msg . '</div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $isProcessed;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $isProcessed;
    }

    private function cardPonteCapture($args)
    {
        $client = new CardPointe();
        $cardpointeuser  = $args['gatewayUsername'];
        $cardpointepass  =  $args['gatewayPassword'];
        $cardpointeMerchID = $args['gatewayMerchantID'];
        $cardpointeSiteName  = $args['gatewaySignature'];
        $result = $client->capture($cardpointeSiteName, $cardpointeMerchID, $cardpointeuser, $cardpointepass, $args['transactionID'], $args['amount']);

        $response = false;
        if ($result['respcode'] == '00') {
            $response = true;
        } else {
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $result['resptext'] . '</strong></div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $response;
    }

    private function EPXCapture($args)
    {
        $CUST_NBR = $args['gatewayUsername'];
        $MERCH_NBR = $args['gatewayPassword'];
        $DBA_NBR = $args['gatewaySignature'];
        $TERMINAL_NBR = $args['extra_field_1'];
        $orderId = time();
        $amount = number_format($args['amount'],2,'.','');

        $transaction = array(
            'CUST_NBR' => $CUST_NBR,
            'MERCH_NBR' => $MERCH_NBR,
            'DBA_NBR' => $DBA_NBR,
            'TERMINAL_NBR' => $TERMINAL_NBR,
            'ORIG_AUTH_GUID' => $args['transactionID'],
            'TRAN_TYPE' => 'CCR4',
            'CURRENCY_CODE' => 840,
            'CARD_ENT_METH' => 'Z',
            'AMOUNT' => $amount,
            'TRAN_NBR' => rand(1,10),
            'INVOICE_NBR' => $orderId,
            'BATCH_ID' => time(),
            'VERBOSE_RESPONSE' => 'Y',
        );
        $gatewayTransaction              = new EPX();
        $result = $gatewayTransaction->processTransaction($transaction);

        $response = false;
        if(($result['AUTH_RESP'] == '00' || $result['AUTH_RESP'] == '01') && $result['AUTH_GUID'] != '' ){
            $response = true;
        } else {
            $err_msg      = $result['AUTH_RESP_TEXT'];
            $this->CI->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error: ' . $err_msg . '</strong></div>');
        }

        if (isset($args['returnResult']) && $args['returnResult']) {
            $r_data['response'] = $response;
            $r_data['result']   = $result;
            return $r_data;
        }
        return $response;
    }
}