<?php


function template_variable()
{
	$template = array(
		'name'              => 'ChargeZoom',
		'version'           => '0.1',
		'author'            => 'ChargeZoom',
		'robots'            => 'noindex, nofollow',
		'title'             => 'PayPortal - Cloud Accounting Automation Software',
		'description'       => 'Payment Portal automates your payment processing and accounting reconciliation so you can focus on your business.',
		// true                     enable page preloader
		// false                    disable page preloader
		'page_preloader'    => true,
		// true                     enable main menu auto scrolling when opening a submenu
		// false                    disable main menu auto scrolling when opening a submenu
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['REQUEST_URI'])
	);
 
 
 
	return $template;	
}


function primary_nav()
{
    
 
          
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('home/index'),
			'page_name'=> 'index'
		),
	
		array(
			'name'  => 'Gateway',
			'icon'  => 'gi gi-lock',
			'sub'   => array(
				array(
					'name'  => 'Sale',
					'url'   => base_url('Payments/create_customer_sale'),
					'page_name'=> 'create_customer_sale'
				),
				array(
					'name'  => 'Authorize',
					'url'   =>  base_url('Payments/create_customer_auth'),
					'page_name'=> 'create_customer_auth'
				),
				
				array(
					'name'  => 'Capture/Void',
					'url'   =>  base_url('Payments/payment_capture'),
					'page_name'=> 'payment_capture'
				),
			
				
				array(
					'name'  => 'Refund',
					'url'   =>  base_url('Payments/payment_refund'),
						'page_name'=> 'payment_refund'
				)
			)
		),
		array(
			'name'  => 'Customers',
			'icon'  => 'fa fa-users',
			'url'   => base_url('home/customer'),
			'page_name'=> 'customer'
		),
		array(
			'name'  => 'Subscriptions',
			'icon'  => 'gi gi-refresh',
			'url'   => '#',
	     	'page_name'=> '#'
		),
		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
				array(
					'name'  => 'Refunds',
					'url'   => '#',
						'page_name'=> '#'
				),
				array(
					'name'  => 'Credits',
					'url'   => '#',
						'page_name'=> '#'
				)
			)
		),
		array(
			'name'  => 'Plans & Products',
			'icon'  => 'gi gi-calendar',
		 'url'   => base_url('home/plan_product'),
		 	'page_name'=> 'plan_product'
		),
		array(
			'name'  => 'Reports',
			'icon'  => 'gi gi-signal',
			 'url'   => base_url('report/reports'),
			 'page_name'=> 'reports'
		),
		array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			'sub'   => array(
			
			      array(
                'name'  => 'Accounting Software',
                'url'   =>  base_url('home/company'),
                'page_name'=> 'company'
                ),
				array(
					'name'  => 'Admin Users',
					'url'   => '#',
						'page_name'=> '#'
				),
				array(
					'name'  => 'Admin Roles',
					'url'   => '#',
						'page_name'=> '#'
				),
			array(
					'name'  => 'Customer Portal',
					'url'   => base_url('SettingConfig/setting_customer_portal'),
					'page_name'=> 'setting_customer_portal'
				),
				array(
					'name'  => 'Payment Methods',
					'url'   => '#',
						'page_name'=> '#'
				),
				array(
					'name'  => 'Taxes',
					'url'   => '#',
						'page_name'=> '#'
				),
		    	array(
                'name'  => 'Email Personalization',
                'url'   =>  base_url('Settingmail/email_temlate'),
                'page_name'=> 'email_temlate'
                ),
				array(
					'name'  => 'General Settings',
					'url'   => '#',
						'page_name'=> '#'
				),
				array(
                'name'  => 'Merchant Gateway',
                'url'   =>  base_url('home/update_gateway'),
                'page_name'=> 'update_gateway'
                ),
				array(
					'name'  => 'API',
					'url'   => '#',
						'page_name'=> '#'
				),
				array(
					'name'  => 'Application Settings',
					'url'   => '#',
						'page_name'=> '#'
				)
			)
		)
	);
	return $primary_nav;	
}




function primary_customer_nav()
{
    
       
      $CI = & get_instance(); 
 $status = $CI->session->userdata('customer_logged_in');
$primary_nav='';
 if(empty($status['packageType'])){
     
     @$status['active_app'] = 0;
 }

 
    $u_id='';
    $service='';
     if($CI->session->userdata('logged_in')){
         
        
	
		$u_id 				=  $CI->session->userdata('customer_logged_in')['merchanID'];
		$CI->load->model('general_model');
	
		}
	
 
 
    
    
    
      if(!empty($status['packageType']) && $status['packageType']=='2'){
    
    
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('Payments/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	); 
	
      }
      elseif(!empty($status['packageType']) && $status['packageType']=='1'){
	     $primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('QBO_controllers/home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('QBO_controllers/home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('QBO_controllers/Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('QBO_controllers/Payments/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	); 
	
      }
      elseif(!empty($status['packageType']) && $status['packageType']=='3'){
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('FreshBooks_controllers/home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('FreshBooks_controllers/home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('FreshBooks_controllers/Transactions/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('FreshBooks_controllers/Transactions/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	); 
	
      } 
      elseif(!empty($status['packageType']) && $status['packageType']=='4'){
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('Xero_controllers/home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('Xero_controllers/home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('Xero_controllers/Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('Xero_controllers/Payments/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	); 
	
      }else if(!empty($status['packageType']) && $status['packageType']=='5'){
    
    
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('company/home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('company/home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('company/Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('company/Payments/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	); 
	
      }
	return $primary_nav;	
}

  


?>