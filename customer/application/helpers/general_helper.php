<?php
	use iTransact\iTransactSDK\iTTransaction;
	function mymessage($message)
	{
		echo '
		<div class="alert alert-info">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$message.'
		</div>
		';
	}

 
	function get_services()
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_table('*', 'services');
		return $service;
	}


	function get_services_by_id($id)
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_allservices_by_id($id);
		return $service;
	}
	
		function get_uni_data($tbl,$col,$val,$return)
	{
		$CI =& get_instance();
		$con = array($col => $val);
		$CI->load->model('general_model');
		$service = $CI->general_model->get_row_data($tbl,$con);
		return $service["$return"];
	}
	
		function get_frequency($val)
	{
		$CI =& get_instance();
		$con = array('frequencyValue' => $val);
		$CI->load->model('general_model');
		$service = $CI->general_model->get_row_data('tbl_invoice_frequecy',$con);
	  return	$frequncy = $service["frequencyText"];
	}
	
	
		function resellerID($val)
	{
		$CI =& get_instance();
		$con = array('merchID' => $val);
		$CI->load->model('general_model');
		$service = $CI->general_model->get_select_data('tbl_merchant_data',array('resellerID'),$con);
		return $service["resellerID"];
	}
	
	
   function marchentname($val)
	{
		$CI =& get_instance();
		$con = array('merchID' => $val);
		$CI->load->model('general_model');
		$service = $CI->general_model->get_select_data('tbl_merchant_data',array('firstName', 'lastName'),$con);
		$name =  $service["firstName"].' '. $service["lastName"];
		return $name;
	}

	function alterTransactionCode($args){
		
		switch ($args['transactionGateway']) {
			case 1:
				if($args['transactionCode'] == 200) {
					$args['transactionCode'] = 401;
				}
				break;
			case 11:
				if($args['transactionCode'] == 200) {
					$args['transactionCode'] = 401;
				}
				break;
			default:
				
				break;
		}
		return $args;
	}

	// function for convert time 
	function getTimeBySelectedTimezone($timezone)
	{
		$date = new DateTime($timezone['time'], new DateTimeZone($timezone['current_format']));
		$date->setTimezone(new DateTimeZone($timezone['new_format']));
		return $date->format('Y-m-d H:i:s');
	}
	
	function getiTransactTransactionDetails($merchID,$transaction_id){
		
		
		$CI = &get_instance();
		$gt_result = $CI->general_model->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID,'gatewayType' => 10));

        $apiUsername  = $gt_result['gatewayUsername'];
        $apiKey  = $gt_result['gatewayPassword']; 
        $isSurcharge  = $gt_result['isSurcharge']; 
        $surCharge = 0;
		$totalAmount =0;
		$payAmount = 0;
		$data = array('surCharge' => 0,'totalAmount' => 0,'payAmount' => 0,'isSurcharge' => $isSurcharge );
        if($isSurcharge){
        	$sdk = new iTTransaction();
			$payload = [];

	        $result1 = $sdk->getTransactionByID($apiUsername, $apiKey, $payload,$transaction_id);
	        
	        if ($result1['status_code'] == '200' || $result1['status_code'] == '201') {
	        	if(isset($result1['surcharge_amount']) && $result1['surcharge_amount'] != ''){
	        		$data['surCharge'] = $result1['surcharge_amount'] / 100;
	        	}
	        	$data['totalAmount'] = $result1['authorized_amount'] / 100;
	        	$data['payAmount'] = $result1['amount'] / 100;
	        }
        }

		return $data;
	}
	
	function addlevelThreeDataInTransaction($data)
	{
		$CI = &get_instance();
		
		// check level three data enable or not
		$checkLevelEnable = $CI->general_model->get_select_data('tbl_merchant_gateway', array('enable_level_three_data'), array('merchantID' => $data['merchID'] , 'gatewayType' => $data['gateway']));
		$enable_level_three = (isset($checkLevelEnable['enable_level_three_data'])) ? $checkLevelEnable['enable_level_three_data'] : 0;
		
		$transaction = isset($data['transaction']) ? $data['transaction'] : [];

		$request_data = [];
		if($enable_level_three){
			if(isset($data['card_type']) && !empty($data['card_type']) && empty($data['card_no'])){
				$card_type = $data['card_type'];
			}else{
				// check card type
				$card_type  = $CI->general_model->getType($data['card_no']);
			}
			$card_type = strtolower($card_type);
			
			$level_three_data = $CI->general_model->get_row_data('merchant_level_three_data', array('merchant_id'=> $data['merchID'], 'card_type' => ($card_type == 'visa') ? 'visa' : 'master'));
			if(!empty($level_three_data)){
				// if card type visa and master then add level three data
				if($data['gateway'] == 3){
					if($card_type == 'visa'){
						$url = 'https://api.paytrace.com/v1/level_three/visa';
						$request_data = [
							'transaction_id' => $data['transaction_id'],
							'integrator_id' => $data['integrator_id'],
							'invoice_id' => $data['invoice_id'],
							'customer_reference_id' => $level_three_data['customer_reference_id'],
							'tax_amount' => $level_three_data['local_tax'],
							'national_tax_amount' => $level_three_data['national_tax'],
							'merchant_tax_id' => $level_three_data['merchant_tax_id'],
							'customer_tax_id' => $level_three_data['customer_tax_id'],
							'commodity_code' => $level_three_data['commodity_code'],
							'discount_amount' => $level_three_data['discount_rate'],
							'freight_amount' => $level_three_data['freight_amount'],
							'duty_amount' => $level_three_data['duty_amount'],
							'source_address' => [
								'zip' => $level_three_data['source_zip']
				 			],
				 			'shipping_address' => [
								'zip' => $level_three_data['destination_zip'],
								'country' => $level_three_data['destination_country'],
				 			],
				 			'additional_tax_amount' => $level_three_data['addtnl_tax_freight'],
				 			'additional_tax_rate' => $level_three_data['addtnl_tax_rate'],
				 			'line_items' =>  [[
			 					'additional_tax_amount' => $level_three_data['addtnl_tax_amount'],
			 					'additional_tax_rate' => $level_three_data['line_item_addtnl_tax_rate'],
			 					'amount' => $data['amount'],
			 					'description' => $level_three_data['description'],
			 					'commodity_code' => $level_three_data['line_item_commodity_code'],
			 					'discount_amount' => $level_three_data['discount'],
			 					'product_id' => $level_three_data['product_code'],
			 					'quantity' => 1,
			 					'unit_of_measure' => $level_three_data['unit_measure_code'],
			 					'unit_cost' => $data['amount']
			 				]]
						];
					}else if($card_type == 'mastercard'){

						$url = 'https://api.paytrace.com/v1/level_three/mastercard';
						$request_data = [
							'transaction_id' => $data['transaction_id'],
							'integrator_id' => $data['integrator_id'],
							'invoice_id' => $data['invoice_id'],
							'customer_reference_id' => $level_three_data['customer_reference_id'],
							'tax_amount' => $level_three_data['local_tax'],
							'national_tax_amount' => $level_three_data['national_tax'],
							'freight_amount' => $level_three_data['freight_amount'],
							'duty_amount' => $level_three_data['duty_amount'],
							'source_address' => [
								'zip' => $level_three_data['source_zip']
				 			],
				 			'shipping_address' => [
								'zip' => $level_three_data['destination_zip'],
								'country' => $level_three_data['destination_country'],
				 			],
				 			'additional_tax_amount' => $level_three_data['addtnl_tax_amount'],
				 			'additional_tax_included' => ($level_three_data['addtnl_tax_indicator'] == 'Y') ? true : false,
				 			'line_items' => [
				 				[
				 					'additional_tax_amount' => $level_three_data['line_item_addtnl_tax_rate'],
				 					'additional_tax_included' => ($level_three_data['net_gross_indicator'] == 'Y') ? true : false,
				 					'additional_tax_rate' => $level_three_data['addtnl_tax_rate'],
				 					'amount' => $data['amount'],
				 					'debit_or_credit' => ($level_three_data['debit_credit_indicator'] == 'C') ? 'C' : 'D',
				 					'description' => $level_three_data['description'],
				 					'discount_amount' => $level_three_data['discount'],
				 					'discount_rate' => $level_three_data['discount_rate'],
				 					'discount_included' => true,
				 					'merchant_tax_id' => $level_three_data['merchant_tax_id'],
				 					'product_id' => $level_three_data['product_code'],
				 					'quantity' => 1,
				 					'tax_included' => true,
				 					'unit_of_measure' => $level_three_data['unit_measure_code'],
				 					'unit_cost' => $data['amount']
				 				]
				 			]
						];
					}

					if($request_data){
						$ch = curl_init();
				        curl_setopt($ch, CURLOPT_URL, $url);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				        curl_setopt($ch, CURLOPT_HEADER, FALSE);

				        curl_setopt($ch, CURLOPT_POST, TRUE);

				        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request_data));

				        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				            "Content-Type: application/json",
				            "Authorization: ".$data['token']
				        ));

				        $response = json_decode(curl_exec($ch), 1);
				        curl_close($ch);
				        return $response;
					}
		    	}elseif ($data['gateway'] == 1) {
		    		// nmi gateway code
	    			$level_three_data['unit_measure_code'] = (strtolower($level_three_data['unit_measure_code']) == 'each') ? 'EAC' : $level_three_data['unit_measure_code'];
	    			if($card_type == 'visa' || $card_type == 'mastercard'){

		    			if(isset($data['invoice_no']) && !empty($data['invoice_no'])){
		    				$transaction->addQueryParameter('ponumber', $data['invoice_no']);
		    			}
		    		}
		    		// add level three data in visa card
		    		if($card_type == 'visa'){
		    			$transaction->addQueryParameter('shipping', $level_three_data['freight_amount']);
		    			$transaction->addQueryParameter('shipping_country', $level_three_data['destination_country']);
		    			$transaction->addQueryParameter('shipping_postal', $level_three_data['destination_zip']);
						$transaction->addQueryParameter('ship_from_postal', $level_three_data['source_zip']);
						$transaction->addQueryParameter('summary_commodity_code', $level_three_data['commodity_code']);
		    			$transaction->addQueryParameter('duty_amount', $level_three_data['duty_amount']);
		    			$transaction->addQueryParameter('discount_amount', $level_three_data['discount_rate']);
		    			$transaction->addQueryParameter('national_tax_amount', $level_three_data['national_tax']);

		    			$transaction->addQueryParameter('vat_tax_amount', $level_three_data['addtnl_tax_freight']);
		    			$transaction->addQueryParameter('vat_tax_rate', $level_three_data['addtnl_tax_rate']);

		    			$transaction->addQueryParameter('customer_vat_registration', $level_three_data['customer_tax_id']);
		    			$transaction->addQueryParameter('merchant_vat_registration', $level_three_data['merchant_tax_id']);

						$transaction->addQueryParameter('item_product_code_1', $level_three_data['product_code']);
						$transaction->addQueryParameter('item_description_1', $level_three_data['description']);
						$transaction->addQueryParameter('item_commodity_code_1', $level_three_data['commodity_code']);
						$transaction->addQueryParameter('item_unit_of_measure_1', $level_three_data['unit_measure_code']);
						$transaction->addQueryParameter('item_unit_cost_1', $data['amount']);
						$transaction->addQueryParameter('item_quantity_1', 1);
						$transaction->addQueryParameter('item_total_amount_1', $data['amount']);

						$transaction->addQueryParameter('item_tax_amount_1', $level_three_data['addtnl_tax_amount']);
						$transaction->addQueryParameter('item_tax_rate_1', $level_three_data['line_item_addtnl_tax_rate']);
						
						$transaction->addQueryParameter('item_discount_amount_1', $level_three_data['discount']);

		    		}elseif ($card_type == 'mastercard') {
		    			// add level three data in master card
		    			$transaction->addQueryParameter('shipping', $level_three_data['freight_amount']);
		    			$transaction->addQueryParameter('shipping_country', $level_three_data['destination_country']);
		    			$transaction->addQueryParameter('shipping_postal', $level_three_data['destination_zip']);
						$transaction->addQueryParameter('ship_from_postal', $level_three_data['source_zip']);
		    			$transaction->addQueryParameter('duty_amount', $level_three_data['duty_amount']);
		    			$transaction->addQueryParameter('national_tax_amount', $level_three_data['national_tax']);

		    			$transaction->addQueryParameter('alternate_tax_amount', $level_three_data['addtnl_tax_amount']);
		    			$transaction->addQueryParameter('merchant_vat_registration', $level_three_data['merchant_tax_id']);

						$transaction->addQueryParameter('item_product_code_1', $level_three_data['product_code']);
						$transaction->addQueryParameter('item_description_1', $level_three_data['description']);
						$transaction->addQueryParameter('item_unit_of_measure_1', $level_three_data['unit_measure_code']);
						$transaction->addQueryParameter('item_unit_cost_1', $data['amount']);
						$transaction->addQueryParameter('item_quantity_1', 1);
						
						$transaction->addQueryParameter('item_total_amount_1', $data['amount']);
						$transaction->addQueryParameter('item_tax_amount_1', $level_three_data['line_item_addtnl_tax_rate']);
						$transaction->addQueryParameter('item_tax_rate_1', $level_three_data['addtnl_tax_rate']);
						
						$transaction->addQueryParameter('item_discount_amount_1', $level_three_data['discount']);
						$transaction->addQueryParameter('item_discount_rate_1', $level_three_data['discount_rate']);

						$transaction->addQueryParameter('item_tax_type_1', $level_three_data['addtnl_tax_type']);
		    		}
		    	}elseif ($data['gateway'] == 7){
					return true;

			    	// add level three data in master card
		    		if($card_type == 'mastercard'){
			    	
		    			$commercialData = $data['levelCommercialData'];

						$lineItem = (object) [
						    'description' => $level_three_data['description'],
						    'productCode' => $level_three_data['product_code'],
						    'quantity' => 1,
						    'unitOfMeasure' => strtolower($level_three_data['unit_measure_code']),
						    'unitCost' => $data['amount'],
						];
						$commercialData->addLineItems($lineItem); 

						$transaction->transactionReference->transactionId = $data['transaction_id'];
						$transaction->commercialIndicator = 1;
						$transaction->cardType = 'MC';

		                $response = $transaction->edit()
		                ->withCommercialData($commercialData)
		                ->execute();
		                return $response;
		    		}else if($card_type == 'visa'){
			    		// add level three data in visa card
		    			$commercialData = $data['levelCommercialData'];
						$commercialData->freightAmount = $level_three_data['freight_amount'];
						$commercialData->dutyAmount =  $level_three_data['duty_amount'];
						$commercialData->discountAmount =  $level_three_data['discount_rate'];
						$commercialData->vatInvoiceNumber =  $data['invoice_id'];
						$commercialData->taxAmount =  $level_three_data['addtnl_tax_freight'];
						$commercialData->taxRate =  $level_three_data['addtnl_tax_rate'];

						$commercialData->summaryCommodityCode = $level_three_data['commodity_code'];
						$commercialData->destinationPostalCode = $level_three_data['destination_zip'];
						$commercialData->originPostalCode = $level_three_data['source_zip'];
						$commercialData->destinationCountryCode = $level_three_data['destination_country'];

						$lineItem = [
						    'commodityCode' => $level_three_data['line_item_commodity_code'],
						    'description' => $level_three_data['description'],
						    'productCode' => $level_three_data['product_code'],
						    'quantity' => 1,
						    'unitOfMeasure' => strtolower($level_three_data['unit_measure_code']),
						    'unitCost' => $data['amount'],
						    'taxAmount' => $level_three_data['addtnl_tax_amount'],
						    'taxPercentage' => $level_three_data['line_item_addtnl_tax_rate'],
						    // 'DiscountPerLineItem' => $level_three_data['discount'],
						    'totalAmount' => $data['amount'],
						];
						$commercialData->addLineItems($lineItem); 

						$transaction->transactionReference->transactionId = $data['transaction_id'];
						$transaction->commercialIndicator = 1;
						$transaction->cardType = 'Visa';

		                $response = $transaction->edit()
		                ->withCommercialData($commercialData)
		                ->execute();
		    			return $response;
		    		}
		    	}
		    }
		}
		if($data['gateway'] == 1){
			return $transaction;
		}else{
    		return false;
		}
    }

    function checkCustomerCard($customerID,$merchID)
	{
	    $CI = &get_instance();
	    
	    $crdata = $CI->card_model->getCustomerCardCheck($customerID,$merchID);

	    if($crdata > 0){
	    	$card_data = array('is_default'=>0);
        	$card_condition = array('customerListID' =>$customerID,'merchantID'=>$merchID );
        	$CI->card_model->update_card_data($card_condition, $card_data);
	        return 0;

	    }else{
	        return 0;
	    }
	    die;
	    

	    
	}


function getClientIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      	$ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      	$ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

	function xero_sync_log($args, $merchantID){
		$CI = &get_instance();

		$input_data = [
			'xeroAction' => $args['action'],
			'xeroStatus' => $args['status'],
			'xeroText' => $args['message'],
			'xeroActionID' => (isset($args['xeroActionID'])) ? $args['xeroActionID'] : '',
			'merchantID' => $merchantID,
			'createdAt' => date("Y-m-d H:i:s"),
			'updatedAt' => date("Y-m-d H:i:s"),
		];
		
		$logID = 	 $CI->general_model->insert_row('tbl_xero_log', $input_data);
		if($logID){
			$actionID = 1000 + $logID;
			if($args['syncFromCZ']){
				$xeroActionID = "CX#$actionID";
			} else {
				$xeroActionID = "XC#$actionID";
			}
			$CI->general_model->update_row_data('tbl_xero_log', [ 'id' => $logID ], [ 'xeroSyncID' => $xeroActionID ]);
		}
		
		return true;
	}
	
	if (!function_exists('priceFormat')) {
		function priceFormat($price, $with_currency = false, $currency_pre = true)
		{
			if(!is_numeric($price)){
				$price = (float)$price;
			}
			// If price is empty then set zero
			if (empty($price)) $price = 0;

			if ($with_currency) {
				if ($currency_pre) {
					if ($price < 0) {
						$price = abs($price);
						$price = number_format($price, 2, '.', ',');
						$price = '-$' . $price;

					} else {
						$price = number_format($price, 2, '.', ',');
						$price = '$' . $price;
					}
				} else {
					$price = number_format($price, 2, '.', ',');
					$price = $price . '$';
				}
			} else {
				$price = number_format($price, 2, '.', '');
			}

			return $price;

		}
	}
?>