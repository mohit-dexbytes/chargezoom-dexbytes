<?php
/**
 * Copyright (c) Payroc LLC 2017.
 */

/**
 * Created by PhpStorm.
 * User: prestonf
 * Date: 8/2/17
 * Time: 3:40 PM
 */

namespace iTransact\iTransactSDK;

/**
 * Class CardPayload
 * @package iTransact\iTransactSDK
 */
class ACHPayload
{
    public $name;
    public $account_number;
    public $routing_number;
    public $phone_number;
    public $sec_code;

    /**
     * CardPayload constructor.
     *
     * @param $name
     * @param $account_number
     * @param $routing_number
     * @param $phone_number
     * @param $sec_code
     */
    public function __construct($name, $account_number, $routing_number, $phone_number, $sec_code)
    {
        $this->name = $name;
        $this->account_number = $account_number;
        $this->routing_number = $routing_number;
        $this->phone_number = $phone_number;
        $this->sec_code = $sec_code;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAccountNumber()
    {
        return $this->account_number;
    }

    /**
     * @param mixed $account_number
     */
    public function setAccountNumber($account_number)
    {
        $this->account_number = $account_number;
    }

    /**
     * @return mixed
     */
    public function getRotuingNumber()
    {
        return $this->routing_number;
    }

    /**
     * @param mixed $routing_number
     */
    public function setRotuingNumber($routing_number)
    {
        $this->routing_number = $routing_number;
    }

    /**
     * @return mixed
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param mixed $phone_number
     */
    public function setPhoneNumber($phone_number)
    {
        $this->phone_number = $phone_number;
    }

    /**
     * @return mixed
     */
    public function getSecCode()
    {
        return $this->sec_code;
    }

    /**
     * @param mixed $sec_code
     */
    public function setSecCode($sec_code)
    {
        $this->sec_code = $sec_code;
    }


}