<?php
/**
 * Created by PhpStorm.
 * User: prestonf
 * Date: 1/26/18
 * Time: 4:04 PM
 */

namespace iTransact\iTransactSDK;


class ACHTransactionPayload
{
    public $amount;
    public $ach;
    public $address;

    /**
     * ACHTransactionPayload constructor.
     * @param integer $amount Example: $15.00 should be 1500
     * @param ACHPayload $ach
     * @param AddressPayload $address
     * @param array $metadata Associative array of values
     */
    public function __construct($amount, $ach, $address)
    {
        $this->amount = $amount;
        $this->ach = $ach;
        // TODO - make address optional
        $this->address = $address;
    }


    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getACH()
    {
        return $this->ach;
    }

    /**
     * @param mixed $ach
     */
    public function setACH($ach)
    {
        $this->ach = $ach;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }
}