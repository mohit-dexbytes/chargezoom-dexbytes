<?php
class EPX {
    public $CUST_NBR = 7000;
    public $MERCH_NBR = 700010;
    public $DBA_NBR = 1;
    public $TERMINAL_NBR = 1;
    public $url = 'https://secure.epx.com/';
    
   function __construct()
    {
        if(ENVIRONMENT == 'production'){
            $this->url = 'https://secure.epx.com/';
        }else{
            $this->url = 'https://secure.epxuap.com/';
        }
    }
    

    //////////////////
    // Transactions //
    //////////////////
    public function processTransaction($transaction) {
        $obj = [];
        $url = $this->url;
        return $this->request(array(
            'method' => 'POST',
            'url' => $url,
            'type' => 'transaction',
            'fields' => $transaction  
        ));
    }

    

   


    ///////////////
    // Terminals //
    ///////////////
    

    private function request(array $options) {
       
        $url = $this->url;
        $ch = curl_init();
        $xml = http_build_query($options['fields']);
        /*Log Request Payload*/
        $this->logData($xml, 'Request');
        
        $ch = curl_init();
        $header = array('Content-Type: application/json');
        $curlConfig = array(
            CURLOPT_HTTPHEADER     => $header,
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => 0,
        );
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL,$url); curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml); curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response=curl_exec ($ch); if (curl_errno($ch)!=0) echo curl_error($ch); curl_close($ch);
        /*Log Response Payload*/
        $this->logData($response, 'Response');

        $xml = simplexml_load_string($response);
        $obj = [];

        foreach($xml->FIELDS->FIELD as $a => $b) {
            $obj1 = $this->xml2array ( $b, $out = array () );
            if(isset($obj1[0])){
                $obj[$obj1['@attributes']['KEY']] = $obj1[0];
            }
            
           
        }
        return $obj;
    }
    public function xml2array ( $xmlObject, $out = array () )
    {
        foreach ( (array) $xmlObject as $index => $node )
            $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;

        return $out;
    }
    /**
     * Save log data for request and response
     * 
     * @param string $str  xml data
     * @param string $title title of log 
     *
     * @return None
    */
    public function logData($str, $title=null) : void
    {
        // Log for few days only
        $log = (date('Y-m-d') <= '2021-08-20');
        if($log){
            $fp = fopen('./uploads/epx-log.txt', 'a');

            fwrite($fp, ($title ? $title : '').':'."\r\n"."\r\n");
            fwrite($fp, $str);
            fwrite($fp, "\r\n".'===================================='.date('Y-m-d H:i:s').'==================================='."\r\n"."\r\n");
            fclose($fp);
        }
    }
}