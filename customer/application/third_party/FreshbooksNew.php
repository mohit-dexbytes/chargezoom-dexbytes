<?php

class FreshbooksNew
{
    protected $oauth_consumer_key;
    protected $oauth_consumer_secret;
    protected $oauth_callback;
    protected $oauth_token;
    protected $oauth_token_secret;
    protected $api_url = 'https://api.freshbooks.com/';
    protected $merchantID;

    public function __construct($merchantID, $oauth_consumer_key, $oauth_consumer_secret, $oauth_callback = null, $subdomain = null, $oauth_token = null, $oauth_token_secret = null)
    {
        $this->CI = &get_instance();

        $this->oauth_consumer_key    = $oauth_consumer_key;
        $this->oauth_consumer_secret = $oauth_consumer_secret;

        // If a subdomain is not supplied, it will be set to the user who owns the application's subdomain,
        // This would be good if you are only authenticating yourself, and not other users.
        if ($subdomain === null) {
            $this->subdomain = $this->oauth_consumer_key;
        } else {
            $this->subdomain = $subdomain;
        }

        if ($oauth_callback) {
            $this->oauth_callback = $oauth_callback;
        }

        if ($oauth_token) {
            $this->oauth_token = $oauth_token;
        }

        if ($oauth_token_secret) {
            $this->oauth_token_secret = $oauth_token_secret;
        }

        $this->CI->load->model('General_model', 'general_model');
        $this->merchantID = $merchantID;
    }

    /******Update Process for Items,Customer Invoice and all **************/
    public function put($request, $type)
    {
        // echo '<pre>';
        // print_r([$request, $type]); die;
        $headers = array(
            'Authorization: Bearer ' . $this->oauth_token,
            'Content-Type: application/json',
            'Accept: application/json; charset=UTF-8',
            'Api-Version: aplha');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.freshbooks.com/auth/api/v1/users/me",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        $rds        = json_decode($response);
        $aacount_id = $rds->response->roles[0]->accountid;
        $business_id = $rds->response->groups[0]->business_id;

        if ($type == 'clients') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/users/clients/" . $request['client']['userid'];
        }

        if ($type == 'invoice' || $type == 'invoices') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/invoices/invoices/". $request['invoice']['invoiceid'];
        }

        if ($type == 'items') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/items/items/" . $request['item']['itemid'];

        }

        if ($type == 'services') {
            $url = "https://api.freshbooks.com/comments/business/$business_id/service/". $request['service']['service_id'];
            if (isset($request['type']) && $request['type'] == 'rate') {
                $url = "$url/rate";
                $ra = true;
                $request = [
                    'service_rate' => $request['service_rate']
                ];
            }
            unset($request['service']['service_id']);
        }

        if ($type == 'taxes') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/taxes/taxes/" . $request['tax']['taxid'];

        }

        if ($type == 'payments') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/payments/payments/". $request['payment']['payment_id'];
            unset($request['payment']['payment_id']);
        }

        $request = json_encode($request);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,

            CURLOPT_CUSTOMREQUEST  => "PUT",
            CURLOPT_POSTFIELDS     => $request,

            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if (is_null($err) || $err == "") {
            $result = $response;
        } else {
            $result = $err;
        }

        return $result;
    }

    /******Add Process for Items,Customer Invoice and all **************/

    public function post($request, $type)
    {

        $headers = array(
            'Authorization: Bearer ' . $this->oauth_token,
            'Content-Type: application/json',
            'Accept: application/json; charset=UTF-8',
            'Api-Version: aplha');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.freshbooks.com/auth/api/v1/users/me",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",
            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        $rds        = json_decode($response);
        $aacount_id = $rds->response->roles[0]->accountid;
        $business_id = $rds->response->groups[0]->business_id;

        if ($type == 'clients') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/users/clients";
        }

        if ($type == 'invoices') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/invoices/invoices";
        }

        if ($type == 'items') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/items/items";
        }

        if ($type == 'services') {
            $url = "https://api.freshbooks.com/comments/business/$business_id/service";
            if (isset($request['type']) && $request['type'] == 'rate') {
                $url = "$url/" . $request['service_id'] . "/rate";
                $request = [
                    'service_rate' => $request['service_rate']
                ];
            }
        }
        
        if ($type == 'taxes') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/taxes/taxes";
        }
        if ($type == 'payments') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/payments/payments";

        }
        $request = json_encode($request);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,

            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $request,

            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if (is_null($err) || $err == "") {
            $result = $response;
        } else {
            $result = $err;
        }

        return $result;
    }

    /******Retrieve All records for Items,Customer Invoice and all **************/
    public function get($request, $type)
    {
        $lines   = '';
        $headers = array(
            'Authorization: Bearer ' . $this->oauth_token,
            'Content-Type: application/json',
            'Accept: application/json; charset=UTF-8',
            'Api-Version: aplha');
        $params = '';
        
        $curl   = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.freshbooks.com/auth/api/v1/users/me",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "GET",

            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        }
        $rds = json_decode($response);

        $business_id = $rds->response->groups[0]->business_id;

        $aacount_id = $rds->response->roles[0]->accountid;
        if ($type == 'clients') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/users/clients";
        }

        if ($type == 'invoices') {
            $params .= 'include%5B%5D=lines&';
            if (isset($request['customerid'])) {
                $params .= 'search[customerid]='.$request['customerid'].'&';
                unset($request['customerid']);
            }

            if (isset($request['invoiceid'])) {
                $url    = "https://api.freshbooks.com/accounting/account/$aacount_id/invoices/invoices/" . $request['invoiceid'];
            } else {
                $url   = "https://api.freshbooks.com/accounting/account/$aacount_id/invoices/invoices";
            }
        }

        if ($type == 'items') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/items/items";
        }

        if ($type == 'taxes') {
            $url = "https://api.freshbooks.com/accounting/account/$aacount_id/taxes/taxes";
        }

        if ($type == 'services') {
            $url = "https://api.freshbooks.com/comments/business/$business_id/services";
            if (isset($request['type']) && $request['type'] == 'rate') {
                $url = substr($url, 0, -1) . "/" . $request['service_id'] . "/rate";
            }
            unset($headers[1]);
        }

        foreach ($request as $key => $value) {
            $params .= $key . '=' . urlencode($value) . '&';
        }

        $params = trim($params, '&');
        if ($params != "") {
            $url = "$url?$params";
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL            => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,

            CURLOPT_CUSTOMREQUEST  => "GET",

            CURLOPT_HTTPHEADER     => $headers,
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        if (is_null($err) || $err == "") {
            $result = $response;
        } else {
            $result = $err;
        }

        return $result;
    }

    public function refresh_token($request_data)
    {
        $headers = array('Content-Type: application/json',
            'Accept: application/json; charset=UTF-8',
            'Api-Version: aplha');

        $request = json_encode($request_data);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL            => "https://api.freshbooks.com/auth/oauth/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING       => "",
            CURLOPT_MAXREDIRS      => 10,
            CURLOPT_TIMEOUT        => 0,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST  => "POST",
            CURLOPT_POSTFIELDS     => $request,
            CURLOPT_HTTPHEADER     => $headers,

        ));

        $response = curl_exec($curl);
        $err      = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

    public function add_payment($item)
    {

        $items   = '';
        $payment = array();

        $request = array('payment' => $item);

        $c = $this->_post_data($request, 'payments', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->payment->id;
        }

        if (!empty($items)) {
            $payment['status']     = 'ok';
            $payment['payment_id'] = $items;
        } else {
            $payment['status']     = 'error';
            $payment['payment_id'] = '';
        }

        return $payment;
    }

    public function update_payment($item)
    {

        $items   = '';
        $payment = array();

        $request = array('payment' => $item);

        $c = $this->_post_data($request, 'payments', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->payment->id;
        }

        if (!empty($items)) {
            $payment['status']     = 'ok';
            $payment['payment_id'] = $items;
        } else {
            $payment['status']     = 'error';
            $payment['payment_id'] = '';
        }

        return $payment;
    }

    public function delete_payment($paymentID)
    {

        $items   = '';
        $payment = array();

        $request = array(
            'payment' => [
                'payment_id' => $paymentID,
                'vis_state' => 1
            ],
        );

        $c = $this->_post_data($request, 'payments', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->payment->id;
        }

        if (!empty($items)) {
            $payment['status']     = 'ok';
            $payment['payment_id'] = $items;
        } else {
            $payment['status']     = 'error';
            $payment['payment_id'] = '';
        }

        return $payment;
    }

    public function get_taxes_data($from = null, $to = null, $page = 1, $count = 10, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data = array('page' => $page, 'per_page' => $count);

            if ($from) {
                $x_data['updated_from'] = $from;
            }

            if ($to) {
                $x_data['updated_to'] = $to;
            }

            $x_data['include'] = 'lines';

            $c = $this->_get_data($x_data, 'taxes');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->taxes;

                foreach ($clients as $key => $row) {

                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_taxes($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_taxes($page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('page' => $page, 'per_page' => $count);

            $c = $this->_get_data($request, 'taxes');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->taxes;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_taxes($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function add_taxs($item)
    {

        $items = '';

        $request = array('tax' => $item);

        $c = $this->_post_data($request, 'taxes', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->tax->taxid;
        }

        return $items;
    }

    public function update_taxs($item)
    {

        $items = '';

        $request = array('tax' => $item);

        $c = $this->_post_data($request, 'taxes', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->tax->taxid;
        }

        return $items;
    }

    public function get_invoices_data($from = null, $to = null, $page = 1, $count = 10, $loop = true, $customerID = false)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data = array('page' => $page, 'per_page' => $count);

            if ($from) {
                $x_data['updated_from'] = $from;
            }

            if ($to) {
                $x_data['updated_to'] = $to;
            }

            if ($customerID) {
                $x_data['customerid'] = $customerID;
            }

            $x_data['include'] = 'lines';

            $c = $this->_get_data($x_data, 'invoices');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->invoices;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_invoices($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_invoices($page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('page' => $page, 'per_page' => $count);

            $c = $this->_get_data($request, 'invoices');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->invoices;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_invoices($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function add_invoices($item, $idOnly = true)
    {

        $items = '';

        $request = array('invoice' => $item);

        $c = $this->_post_data($request, 'invoices', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            
            $items = $c->response->result->invoice;
            if($idOnly){
                $items = $items->invoiceid;
            }
        }

        return $items;
    }

    public function update_invoices($item)
    {

        $items = '';

        $request = array('invoice' => $item);

        $c = $this->_post_data($request, 'invoices', 'edit');
        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->invoice->invoiceid;
        }

        return $items;
    }

    public function get_customers_data($from = null, $to = null, $page = 1, $count = 10, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data = array('page' => $page, 'per_page' => $count);

            if ($from) {
                $x_data['updated_from'] = $from;
            }

            if ($to) {
                $x_data['updated_to'] = $to;
            }

            $c = $this->_get_data($x_data, 'clients');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->clients;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_customers($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    private function _get_data($request, $type)
    {
        $this->fberror = null;
        try {

            $clients = $this->get($request, $type);

            return $clients;
        } catch (FreshbooksError $e) {
            $this->fberror = $e->getMessage();
            return 0;
        }
    }

    public function get_customers($page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('page' => $page, 'per_page' => $count);

            $c = $this->_get_data($request, 'clients');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->clients;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_customers($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_items_data($from = null, $to = null, $page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data = array('page' => $page, 'per_page' => $count);

            if ($from) {
                $x_data['updated_from'] = $from;
            }

            if ($to) {
                $x_data['updated_to'] = $to;
            }

            $c = $this->_get_data($x_data, 'items');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->items;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_items($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_service_data($from = null, $to = null, $page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data = array('page' => $page, 'per_page' => $count);

            if ($from) {
                $x_data['updated_from'] = $from;
            }

            if ($to) {
                $x_data['updated_to'] = $to;
            }

            $c = $this->_get_data($x_data, 'services');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c = json_decode($c);

                $clients = $c->services;

                foreach ($clients as $key => $row) {
                    $row->priceDetails = $this->get_service_rate($row->id);
                    $data[]            = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->meta->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_services($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_services($page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('page' => $page, 'per_page' => $count);

            $c = $this->_get_data($request, 'services');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->items;

                foreach ($clients as $key => $row) {
                    $row->priceDetails = $this->get_service_rate($row->id);
                    $data[]            = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_services($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function get_service_rate($service_id)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('service_id' => $service_id, 'type' => 'rate');

            $c = $this->_get_data($request, 'services');

            if (!empty(json_decode($c)->response->errors)) {
                $data = '0';
            } else {
                $data = '0';
                $c    = json_decode($c);
                if(isset($c->service_rate)){
                    $data = $c->service_rate->rate;
                }
            }

        }
        return $data;
    }

    public function get_items($page = 1, $count = 100, $loop = true)
    {
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $request = array('page' => $page, 'per_page' => $count);

            $c = $this->_get_data($request, 'items');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $clients = $c->response->result->items;

                foreach ($clients as $key => $row) {
                    $data[] = $row;
                }

                // Loop through the next pages
                if ($loop) {

                    $pages = (int) $c->response->result->pages;
                    if (($pages > 1) && ($pages >= $page)) {
                        $nextpage = $page + 1;
                        $data     = array_merge($data, $this->get_items($nextpage, $count));
                    }
                }
            }

        }
        return $data;
    }

    public function add_items($item)
    {

        $items = '';

        $request = array('item' => $item);

        $c = $this->_post_data($request, 'items', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->item->itemid;
        }

        return $items;
    }

    public function update_items($item)
    {

        $items = '';

        $request = array('item' => $item);

        $c = $this->_post_data($request, 'items', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->item->itemid;
        }

        return $items;
    }

    public function add_services($item)
    {

        $items = '';

        $request = array('service' => $item);

        $c = $this->_post_data($request, 'services', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->service->id;
        }

        return $items;
    }

    public function update_services($item)
    {

        $items = '';

        $request = array('service' => $item);

        $c = $this->_post_data($request, 'services', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
        }

        return $items;
    }

    public function add_service_rate($item)
    {

        $items = '';

        $request = ['type' => 'rate', 'service_id' => $item['service_id']];
        $request['service_rate'] = array('rate' => $item['rate']);

        $c = $this->_post_data($request, 'services', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->item->itemid;
        }

        return $items;
    }

    public function update_service_rate($item)
    {

        $items = '';

        $request = [
            'type' => 'rate',
            'service' => [
                'service_id' => $item['service_id']
            ]
        ];
        $request['service_rate'] = array('rate' => $item['rate']);

        $c = $this->_post_data($request, 'services', 'edit');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->item->itemid;
        }

        return $items;
    }

    public function add_customer($item)
    {

        $items = '';

        $request = array('client' => $item);

        $c = $this->_post_data($request, 'clients', 'add');

        if (!empty(json_decode($c)->response->errors)) {

        } else {
            $c     = json_decode($c);
            $items = $c->response->result->clients->userid;
        }

        return $items;
    }

    public function update_customer($item)
    {

        $items = '';

        $request = array('client' => $item);

        $c = $this->_post_data($request, 'clients', 'edit');

        $c = json_decode($c);
        if (!empty($c->response->result)) {
            $items = $c->response->result->client->userid;
        } else {

        }

        return $items;
    }

    private function _post_data($request, $type, $action)
    {
        $this->fberror = null;
        try {

            if ($action == 'add') {
                $data = $this->post($request, $type);
            }

            if ($action == 'edit') {
                $data = $this->put($request, $type);
            }

            return $data;
        } catch (FreshbooksError $e) {
            $this->fberror = $e->getMessage();
            return 0;
        }
    }

    public function syncAll()
    {
        $this->syncTaxData();
        $this->syncItems();
        $this->syncCustomer();
        $this->syncInvoice();
        return true;
    }

    public function getLastRun($args = [])
    {
        if (empty($args) || empty($args['fb_action'])) {
            return false;
        }

        $isExist            = false;
        $args['merchantID'] = $this->merchantID;
        $fb_data            = $this->CI->general_model->get_select_data('tbl_fb_config', array('lastUpdated'), $args);
        
        $last_date    = date('Y-m-d H:i:s', strtotime('-6 hour', strtotime('2000-01-01')));
        $current_date = date('Y-m-d H:i:s');

        if (!empty($fb_data)) {
            $last_date = $fb_data['lastUpdated'];
            $isExist   = true;
        }

        $my_timezone = date_default_timezone_get();
        $from        = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => $my_timezone);
        $to          = array('localeFormat' => "Y-m-d H:i:s", 'olsonZone' => 'UTC');

        $last_date    = $this->CI->general_model->datetimeconv($last_date, $from, $to);
        $current_date = $this->CI->general_model->datetimeconv($current_date, $from, $to);

        $last_date    = date('Y-m-d H:i:s', strtotime('-6 hour', strtotime($last_date)));
        $current_date = date('Y-m-d H:i:s', strtotime('-3 hour', strtotime($current_date)));

        return [
            'isExist'      => $isExist,
            'last_date'    => $last_date,
            'current_date' => $current_date,
            'fb_action'    => $args['fb_action'],
        ];
    }

    public function setLastRun($args = [])
    {
        if (empty($args) || empty($args['fb_action']) || !isset($args['isExist'])) {
            return false;
        }

        $args['merchantID'] = $this->merchantID;

        if (!empty($args['isExist'])) {
            $updatedata['lastUpdated'] = date('Y-m-d') . 'T' . date('H:i:s');
            $updatedata['updatedAt']   = date('Y-m-d H:i:s');

            $this->CI->general_model->update_row_data('tbl_fb_config', array('merchantID' => $args['merchantID'], 'fb_action' => $args['fb_action']), $updatedata);
        } else {
            $updateda = date('Y-m-d') . 'T' . date('H:i:s');
            $upteda   = array('merchantID' => $args['merchantID'], 'fb_action' => $args['fb_action'], 'lastUpdated' => $updateda, 'createdAt' => date('Y-m-d H:i:s'), 'updatedAt' => date('Y-m-d H:i:s'));
            $this->CI->general_model->insert_row('tbl_fb_config', $upteda);
        }

        return true;
    }

    public function syncLastRun($args = [])
    {
        if (empty($args) || empty($args['fb_action'])) {
            return false;
        }

        $getLastRun = $this->getLastRun($args);
        if (!$getLastRun) {
            return false;
        }
        
        $setLastRun = $this->setLastRun($getLastRun);
        return $getLastRun;
    }

    public function syncCustomer()
    {
        $syncLastRun = $this->syncLastRun(['fb_action' => 'CustomerQuery']);
        $customer    = $this->get_customers_data($syncLastRun['last_date'], $syncLastRun['current_date']);

        if (!empty($customer)) {
            foreach ($customer as $oneCustomer) {

                $companyName = $oneCustomer->organization;
                $username = $oneCustomer->username;
                $fname = $oneCustomer->fname;
                $lname = $oneCustomer->lname;

                $FB_customer_details = array(
                    "Customer_ListID" => $oneCustomer->userid,
                    "firstName" => "$fname",
                    "lastName" => "$lname",
                    "companyName" => "$companyName",
                    "fullName" => "$username",
                    "userEmail"       => $this->CI->db->escape_str(($oneCustomer->email) ? $oneCustomer->email : $oneCustomer->contacts->contact->email),
                    "phoneNumber"     => $this->CI->db->escape_str(($oneCustomer->home_phone) ? $oneCustomer->home_phone : ''),
                    "address1"        => $this->CI->db->escape_str(($oneCustomer->p_street) ? $oneCustomer->p_street : ''),
                    "address2"        => $this->CI->db->escape_str(($oneCustomer->p_street2) ? $oneCustomer->p_street2 : ''),
                    "zipCode"         => $this->CI->db->escape_str(($oneCustomer->p_code) ? $oneCustomer->p_code : ''),
                    "City"            => $this->CI->db->escape_str(($oneCustomer->p_city) ? $oneCustomer->p_city : ''),
                    "State"           => $this->CI->db->escape_str(($oneCustomer->p_province) ? $oneCustomer->p_province : ''),
                    "Country"         => $this->CI->db->escape_str(($oneCustomer->p_country) ? $oneCustomer->p_country : ''),

                    "ship_address1"   => $this->CI->db->escape_str(($oneCustomer->s_street) ? $oneCustomer->s_street : ''),
                    "ship_address2"   => $this->CI->db->escape_str(($oneCustomer->s_street2) ? $oneCustomer->s_street2 : ''),
                    "ship_zipcode"    => $this->CI->db->escape_str(($oneCustomer->s_code) ? $oneCustomer->s_code : ''),
                    "ship_city"       => $this->CI->db->escape_str(($oneCustomer->s_city) ? $oneCustomer->s_city : ''),
                    "ship_state"      => $this->CI->db->escape_str(($oneCustomer->s_province) ? $oneCustomer->s_province : ''),
                    "ship_country"    => $this->CI->db->escape_str(($oneCustomer->s_country) ? $oneCustomer->s_country : ''),

                    "companyID"       => $oneCustomer->accounting_systemid,
                    "updatedAt"       => $this->CI->db->escape_str($oneCustomer->updated),
                    "merchantID"      => $this->merchantID,
                );

                if ($this->CI->general_model->get_num_rows('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->userid, 'merchantID' => $this->merchantID)) > 0) {

                    $this->CI->general_model->update_row_data('Freshbooks_custom_customer', array('Customer_ListID' => $oneCustomer->userid, 'merchantID' => $this->merchantID), $FB_customer_details);

                } else {
                    $this->CI->general_model->insert_row('Freshbooks_custom_customer', $FB_customer_details);
                }
            }
        }
        return true;
    }

    public function syncInvoice($customerID = false)
    {
        $to = $from = null;
        if(!$customerID){
            $syncLastRun = $this->syncLastRun(['fb_action' => 'InvoiceQuery']);
            $from = $syncLastRun['last_date'];
            $to = $syncLastRun['current_date'];
        }

        $invoices    = $this->get_invoices_data($from, $to, $page = 1, $count = 10, $loop = true, $customerID);
        if (!empty($invoices)) {
            $this->storeInvoiceDB($invoices, $customerID);
        }
        
        return true;
    }

    public function storeInvoiceDB($invoices, $updateInvRef = false){
        if (!empty($invoices)) {
            $invNumber = 0;
            if(!$updateInvRef){
                $in_data = $this->CI->general_model->get_row_data('tbl_merchant_invoices', array('merchantID' => $this->merchantID));
                if (!empty($in_data)) {
                    $invNumber    =   $in_data['postfix'];
                }
            }

            foreach ($invoices as $oneInvoice) {

                $invRefNumber = (string) $oneInvoice->invoice_number;
                $invRefNumberEx = explode('-', $invRefNumber);
                if(isset($invRefNumberEx[1]) && $invNumber < trim($invRefNumberEx[1])){
                    $invNumber = trim($invRefNumberEx[1]);
                }

                $invoice_id                                   = $oneInvoice->invoiceid;
                $FB_invoice_details['invoiceID']              = $oneInvoice->invoiceid;
                $FB_invoice_details['refNumber']              = (string) $invRefNumber;
                $FB_invoice_details['ShipAddress_Addr1']      = $this->CI->db->escape_str($oneInvoice->street);
                $FB_invoice_details['ShipAddress_Addr2']      = $this->CI->db->escape_str($oneInvoice->street2);
                $FB_invoice_details['ShipAddress_City']       = $this->CI->db->escape_str($oneInvoice->city);
                $FB_invoice_details['ShipAddress_Country']    = $this->CI->db->escape_str($oneInvoice->country);
                $FB_invoice_details['ShipAddress_PostalCode'] = $this->CI->db->escape_str($oneInvoice->code);
                $FB_invoice_details['CustomerFullName']       = $this->CI->db->escape_str(($oneInvoice->organization) ? $oneInvoice->organization : '');
                $FB_invoice_details['BalanceRemaining']       = $oneInvoice->outstanding->amount;
                $FB_invoice_details['Total_payment']          = $oneInvoice->amount->amount;
                $FB_invoice_details['AppliedAmount']          = $oneInvoice->paid->amount;
                $FB_invoice_details['UserStatus']             = $this->CI->db->escape_str($oneInvoice->payment_status);
                $FB_invoice_details['CustomerListID']         = $oneInvoice->customerid;
                $FB_invoice_details['TimeModified']           = $this->CI->db->escape_str(($oneInvoice->updated) ? $oneInvoice->updated : '');
                $FB_invoice_details['TimeCreated']            = $this->CI->db->escape_str(($oneInvoice->created_at) ? $oneInvoice->created_at : '');
                $FB_invoice_details['DueDate']                = $this->CI->db->escape_str($oneInvoice->due_date);
                $FB_invoice_details['merchantID']             = $this->merchantID;

                if($FB_invoice_details['BalanceRemaining'] == 0 || $FB_invoice_details['Total_payment'] == $FB_invoice_details['AppliedAmount']){
                    $FB_invoice_details['IsPaid'] = 1;
                } else {
                    $FB_invoice_details['IsPaid'] = 0;
                }

                if($FB_invoice_details['UserStatus'] == 'partial'){
                    $FB_invoice_details['UserStatus'] = 'unpaid';
                }

                $to_tax = 0;
                if (isset($oneInvoice->lines) && $oneInvoice->lines) {
                    $lines = $oneInvoice->lines;

                    $this->CI->general_model->delete_row_data('tbl_freshbook_invoice_item', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id));
                    $this->CI->general_model->delete_row_data('tbl_freshbook_invoice_item_tax', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id));
                    foreach ($lines as $line) {

                        $line_data['itemID'] = $line->lineid;
                        $line_data['itemName'] = $line->name;
                        $line_data['itemDescription'] = (string) $line->description;
                        $line_data['itemPrice'] = $line->unit_cost->amount;
                        $line_data['itemQty'] = $line->qty;
                        $line_data['totalAmount'] = $line->amount->amount;
                        
                        $line_data['merchantID'] = $this->merchantID;
                        $line_data['invoiceID']  = $line->invoiceid;
                        $line_data['createdAt']  = date('Y-m-d H:i:s', strtotime($line->updated));

                        $itemInvID = $this->CI->general_model->insert_row('tbl_freshbook_invoice_item', $line_data);
                        if($itemInvID){
                            $taxIndex = 1;
                            $taxIndexFound = true;
                            do {
                                if (isset($line->{"taxAmount$taxIndex"}) && $line->{"taxAmount$taxIndex"} > 0) {
                                    $taxInvItem = [];
                                    $taxInvItem['itemLineID'] = $itemInvID;
                                    $taxInvItem['merchantID'] = $this->merchantID;
                                    $taxInvItem['invoiceID'] = $invoice_id;

                                    $taxInvItem['taxName'] = $line->{"taxName$taxIndex"};
                                    $taxInvItem['taxAmount'] = $line->{"taxAmount$taxIndex"};
                                    $taxInvItem['taxNumber'] = $line->{"taxNumber$taxIndex"};

                                    $this->CI->general_model->insert_row('tbl_freshbook_invoice_item_tax', $taxInvItem);
                                    ++$taxIndex;
                                } else {
                                    $taxIndexFound = false;
                                }
                            } while ($taxIndexFound === true);
                        }
                    }
                }
                $FB_invoice_details['totalTax'] = $to_tax;

                if ($this->CI->general_model->get_num_rows('Freshbooks_test_invoice', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id)) > 0){
                    $this->CI->general_model->update_row_data('Freshbooks_test_invoice', array('merchantID' => $this->merchantID, 'invoiceID' => $invoice_id), $FB_invoice_details);
                } else{
					$this->CI->general_model->insert_row('Freshbooks_test_invoice', $FB_invoice_details);
                }        
            }

            if(!$updateInvRef && $invNumber != 0){
				$this->CI->general_model->update_row_data('tbl_merchant_invoices', array('merchantID' => $this->merchantID), array('postfix' => $invNumber));
			}
        }
        return true;
    }

    public function syncItems()
    {
        $syncLastRun = $this->syncLastRun(['fb_action' => 'ItemQuery']);
        $items    = $this->get_items_data($syncLastRun['last_date'], $syncLastRun['current_date']);
        if(!empty($items)){
            foreach ($items as $oneItem) {

                $FB_item_details = array(
                    "productID"        => $oneItem->itemid,
                    "Name"             => $this->CI->db->escape_str($oneItem->name),
                    "SalesDescription" => $this->CI->db->escape_str($oneItem->description),
                    "saleCost"         => $this->CI->db->escape_str($oneItem->unit_cost->amount),
                    "QuantityOnHand"   => $this->CI->db->escape_str($oneItem->qty),
                    "TimeModified"     => $this->CI->db->escape_str($oneItem->updated),
                    "Inventory"        => $this->CI->db->escape_str(($oneItem->inventory)),
                    "Tax1"             => $this->CI->db->escape_str(($oneItem->tax1)),
                    "Tax2"             => $this->CI->db->escape_str(($oneItem->tax2)),
                    "IsActive"         => $this->CI->db->escape_str(($oneItem->vis_state == 0) ? 1 : '0'),
                    "merchantID"       => $this->merchantID,
                    "fbCompanyID"      => $oneItem->accounting_systemid,
                );

                if ($this->CI->general_model->get_num_rows('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid)) > 0) {
                    $this->CI->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $oneItem->itemid), $FB_item_details);
                } else {
                    $this->CI->general_model->insert_row('Freshbooks_test_item', $FB_item_details);
                }

            }
        }

        $services    = $this->get_service_data($syncLastRun['last_date'], $syncLastRun['current_date']);
        if(!empty($services)){
            foreach ($services as $services) {
                $FB_services_details = array(
                    "productID"   => $services->id,
                    "Name"        => $this->CI->db->escape_str($services->name),
                    "saleCost"    => $this->CI->db->escape_str(($services->priceDetails == 0) ? '0' : $services->priceDetails),
                    "IsActive"    => $this->CI->db->escape_str(($services->vis_state == 0) ? 1 : '0'),
                    "merchantID"  => $this->merchantID,
                    "fbCompanyID" => $services->business_id,
                );

                if ($this->CI->general_model->get_num_rows('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $services->id)) > 0) {
                    $this->CI->general_model->update_row_data('Freshbooks_test_item', array('merchantID' => $this->merchantID, 'productID' => $services->id), $FB_services_details);
                } else {
                    $this->CI->general_model->insert_row('Freshbooks_test_item', $FB_services_details);
                }

            }
        }
        return true;
    }

    public function syncTaxData(){
        $taxes = $this->get_taxes_data();
        if(!empty($taxes)){
            foreach ($taxes as $oneTax) {
                $fb_tax_details['taxID'] = $oneTax->taxid;
                $fb_tax_details['friendlyName'] = $this->CI->db->escape_str($oneTax->name);
                $fb_tax_details['taxRate'] = $this->CI->db->escape_str($oneTax->amount);
                $fb_tax_details['updatedAt'] = $this->CI->db->escape_str($oneTax->updated);
                $fb_tax_details['createdAt'] = date('Y-m-d H:i:s');
                $fb_tax_details['number'] = $this->CI->db->escape_str($oneTax->number);
                $fb_tax_details['merchantID'] = $this->merchantID;
                $fb_tax_details['companyID'] = $this->CI->db->escape_str($oneTax->accounting_systemid);
                
                if($this->CI->general_model->get_num_rows('tbl_taxes_fb', array('taxID'=>$oneTax->taxid,'merchantID'=>$this->merchantID))>0){
                    $this->CI->general_model->update_row_data('tbl_taxes_fb', array('taxID'=>$oneTax->taxid,'merchantID'=>$this->merchantID), $fb_tax_details);
                } else{
                    $this->CI->general_model->insert_row('tbl_taxes_fb', $fb_tax_details);
                }
            }
        }
        return true;
    }

    public function getInvoiceByID($invoiceID){
        $data = array();
        if ($this->oauth_token_secret && $this->oauth_token) {

            $x_data['invoiceid'] = $invoiceID;
            $x_data['include'] = 'lines';
            $c = $this->_get_data($x_data, 'invoices');

            if (!empty(json_decode($c)->response->errors)) {

            } else {
                $c       = json_decode($c);
                $invData = $c->response->result->invoice;
                $data[] = $invData;

                if (!empty($data)) {
                    $this->storeInvoiceDB($data, $invoiceID);
                }
            }

        }
        return $data;
    }
}
