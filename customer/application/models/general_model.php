<?php
use iTransact\iTransactSDK\iTTransaction;
Class General_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
	
	function insert_row($table_name, $array)
	{

		if($this->db->insert($table_name,$array)){
         
		return $this->db->insert_id();
		}else{ 
		return false;
		}
	}
	
	function update_row($table_name,$reg_no,$array)
	{
		$this->db->where('reg_no', $reg_no);
		if($this->db->update($table_name, $array))
			return true;
		else
			return false;
	}
	
	public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
		if($array !='')
        $this->db->where($array);
		
        $query = $this->db->get();
		if($query->num_rows() > 0)
			return true;
        
        else 
          return false; 
	}
	
	
	function update_row_data($table_name,$condition, $array)
	{
	
		$this->db->where($condition);
		$trt = $this->db->update($table_name, $array); 
		if($trt)
			return true;
		else
			return false;
	}
	
	   public function get_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	public function get_row_json_data($table, $con) {
			
			$data='';	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	  public function get_table_data($table, $con) {
	    $data=array();
	 
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
	  
	  public function get_select_data($table, $clm, $con)
			{
		
					 $data=array();	
					
					 $columns = implode(',',$clm);
					
					$query = $this->db->select($columns)->from($table)->where($con)->get();
					
					if($query->num_rows()>0 ) {
						$data = $query->row_array();
					   return $data;
					} else {
						return $data;
					}			
			}
	  
	  ///---------  to get the email history data --------///
	
	   public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	 //---------  to get the customer subscriptions --------///
	
	 
	public function get_subscriptions_data($condition){
	
		$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscriptions_plan_qb pl','pl.planID = sbs.subscriptionPlan','left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);		
		$this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	  
	  
	  
	  
	  public function get_num_rows($table, $con) {
			
			$num = 0;	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			$num = $query->num_rows();
			return $num;
		
	}
	
	
	public function delete_row_data($table, $condition){
	   
     	    $this->db->where($condition);
            if($this->db->delete($table)){
			  return true;
			}else{
			  return false;
			}
	
	}
	
      
//////// get data from tbl_auth//////		
		   
		   public function get_auth_data($auth_con) {
             $auth=array();
 

			 
			   $query  = $this->db->query("Select authName from tbl_auth where authID in($auth_con)");
			   if($query->num_rows()>0 ) {
				$datas = $query->result_array();
				foreach($datas as $key=>$data){
				
						   $auth[] = $data['authName']; 
				}
				
				  return $auth;
			   } else {
				return $auth;
					}
   }
	  
	  

	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*, cust.companyName');
		$this->db->from('qb_customer_credit cr');
		$this->db->join('qb_test_customer cust','cr.CustomerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where($con);
	
		$this->db->order_by('cr.TimeModified','desc');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
	
      
	  
	 
    public function get_merchant_user_data($userID, $muID='')
    {
		if($muID != "") {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantUserID = '".$muID."' and  tmu.merchantID = '".$userID."' ");	
   			$res = $query->row_array();
			
		} else {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantID = '".$userID."' ");
			$res = $query->result_array();
		} 
		return $res;
	}
	

	public function get_process_trans_count($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $user_id);
		$this->db->where('cust.customerStatus', '1');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->num_rows();  
        }
        else
        {
           return $query->num_rows();
        }
     } 


     public function get_process_trans_data($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where("comp.merchantID ", $user_id);
		$this->db->where('cust.customerStatus', '1');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return false;
        }
     } 

     public function get_modal_invoice_data($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('qb_test_invoice inv');
		$this->db->join('qb_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cust.companyID','INNER');
	    $this->db->where($condition);
		$this->db->where('cust.customerStatus','1');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->result();  
        }
        else
        {
            return false;
        }
	
	} 		 
      
      
    
    public function get_qbo_invoice_data_let( $con) {
	    $data=array();

			$this->db->select('qb.*,cs.fullName  AS custname');
			$this->db->from('QBO_test_invoice qb');
		
			   $this->db->join('QBO_custom_customer cs', 'cs.customer_ListID=qb.CustomerListID','inner'); 
		    	$this->db->where($con);
		
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
         
      
      
      
    public function get_qbo_invoice_data($table, $con) {
	    $data=array();

			$this->db->select('*,(SELECT fullName FROM QBO_custom_customer WHERE customer_ListID = QBO_test_invoice.CustomerListID    LIMIT 1) AS custname');
			$this->db->from($table);
			if(!empty($con)){
			    
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
      
      
      
        public function check_existing_portalprefix($portalprefix)
         {
           $num = 0; 
		
				   
				   $query = $this->db->select('*')->from('tbl_config_setting')->where('portalprefix', $portalprefix)->get();
				   $num = $query->num_rows();
				   if( $query->num_rows()>0){
					$num =$query->num_rows();
				   return true;
					 }
					 $query1 = $this->db->select('*')->from('Config_merchant_portal')->where('portalprefix', $portalprefix)->get();
					if( $query->num_rows()>0){
					 $num =$query1->num_rows();
					 return true;
					 }  
						
				  if($num > 0)
				   return true;
						
						else 
						  return false; 
		 } 
		 
 
     public function check_existing_edit_portalprefix($tab, $rID, $lportalprefix){
    

		if($tab=='tbl_config_setting'){
		 $rdata = $this->get_row_data($tab,array('merchantID'=>$rID));
		 if($lportalprefix==$rdata['portalprefix']){
		  return false;
		 }else{ 
		 

	   if(!empty($this->get_row_data($tab,array('portalprefix'=>$lportalprefix))))
	    return true;
		  else 
	     $sta= false;
	 

		  if($sta ==false)
				if( !empty($this->get_row_data('Config_merchant_portal',array('portalprefix'=>$lportalprefix)))) 
				return  true;
		  else
	      $sta= false;
	  
		return $sta; 
	  }
	  
		}
      }  
      
      public function get_invoice_bal_data($table, $con) {
	    $data=array();
	  
			$this->db->select('BalanceRemaining');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
      
       public function get_xero_invoice_data($table, $con) {
	    $data=array();
	  
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
               $this->db->where('UserStatus !=', 'DELETED');
               $this->db->where('UserStatus !=', 'VOIDED');
              	$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }   
	
		public function delete_tax_row_data($table, $condition){
	   
     	 
     	    $this->db->where("(Status='ARCHIVED' OR Status='DELETED') AND merchantID = $condition");
            if($this->db->delete($table)){
			  return true;
			}else{
			  return false;
			}
	}
	
	
	
	
	
	public function chk_echeck_gateway_status($merchantID, $page='' )
	{
	    $query = $this->db->query("Select * from tbl_merchant_gateway where merchantID='".$merchantID."'  and echeckStatus='1' and  gatewayType IN('1','2')");
	  $res=array(); 
	   
	   if($page!='')
	   {
	        if($query->num_rows() >0)
	    {
	        $res= $query->result_array();
	        
	    }
	        return $res;
	    
	       
	   }else{
	    if($query->num_rows() >0)
	    {
	        return true;
	        
	    }else{
	        return false;
	    }
	    
	   }
	    
	}
	
	
	public function get_random_number($mdata, $mode)
	{
	        if($mode==='DEV')
	        {
               $str =substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,10);
               $str =$str.$mdata['companyName'].$mdata['merchID'].microtime();
	        }
	   
	       if($mode==='PRO')
	       {
	           $str =substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,15);
               $str =$str.$mdata['companyName'].$mdata['merchantEmail'].$mdata['merchID'].microtime();
	           
	       }
	   $code =     sha1($str);
	       return  $code;
	    
	}
	
	/**
 * Replaces all but the last for digits with x's in the given credit card number
 * @param int|string $cc The credit card number to mask
 * @return string The masked credit card number
 */
function MaskCreditCard($cc){
	// Get the cc Length
	$cc_length = strlen($cc);
	// Replace all characters of credit card except the last four and dashes
	for($i=0; $i<$cc_length-4; $i++){
		if($cc[$i] == '-'){continue;}
		$cc[$i] = 'X';
	}
	// Return the masked Credit Card #
	return $cc;
}

  public  function getType($CCNumber)
        {
            
            
$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    );     

            $CCNumber= trim($CCNumber);
            $type='Unknown';
            foreach ($creditcardTypes as $card){
                if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                    continue;
                }
                $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
                if(preg_match($prefixes,$CCNumber) == 1 ){
                    $type= $card['Name'];
                    break;
                }
            }
            return $type;
        }


   public function get_reseller_name_data($merchantID)
   {    if(!empty($merchantID)){
        $this->db->select('r.resellerfirstName, r.lastName, r.resellerCompanyName');
        $this->db->from('tbl_merchant_data m');
        $this->db->join('tbl_reseller r','r.resellerID=m.resellerID','inner');
        $this->db->where('m.merchID',$merchantID);
        $query = $this->db->get();
        $res = $query->row_array();
        $name = $res['resellerCompanyName'];
        return  $name;
    }
   }



    public function get_rows($table, $con) {
			$this->db->select('*');	
			$this->db->from($table);
			$this->db->where($con);
			$query = $this->db->get();
			return  $query->row();
	
	}
	
	
	
	public function set_next_date($paycycle,$date, $in_num,$proRate,$proRateday)
	{
						$date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " -1 day"));
						$next_date ='';
						  if($paycycle=='dly'){
							 $in_num   = ($in_num)? $in_num:'1';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num day"));
						 }
						 if($paycycle=='1wk'){
							 $in_num   = ($in_num)? $in_num:'1';
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }		 
						
						if($paycycle=='2wk'){
							 $in_num   = ($in_num)?$in_num + 1 :'1' ;
							$next_date =  date('Y-m-d',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num week"));
						 }
						if($paycycle=='mon')
						{
						    
						    
						     if($proRate==1)
						     {
						         
						         $in_num   = ($in_num)? $in_num:'1';
						         if($in_num==0)
						         $in_num   = $in_num +1;
							    $next_date =  date('Y-m-'.$proRateday, strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));  
						     }
						    else
						    {
						         $in_num   = ($in_num)? $in_num:'1';
						         if($proRateday=='31')
						         $proRateday = date("t");
						         else if($proRateday=='30' || $proRateday=='29' || $proRateday=='28'  )
						         {
						              if(strtoupper(date("M"))=="JAN")
						               $proRateday  = date("t");
						         }else{
						             $proRateday=1;
						         }
						         
						        
							    $next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));  
                             
                              
						    }
                            
                     
						 }
						if($paycycle=='2mn'){
							 $in_num   = ($in_num)?$in_num + 2*1:'1';
							$next_date =  strtotime(date("Y-m-01", strtotime($date)) . " +$in_num month");
						 }		 
						 if($paycycle=='qtr'){
							 $in_num   = ($in_num)?$in_num + 3*1:'1';
							$next_date =  strtotime(date("Y-m-01", strtotime($date)) . " +$in_num month");
						 }
						if($paycycle=='six'){
							 $in_num   = ($in_num)?$in_num + 6*1:'1';
							$next_date = date('Y-m-01', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='yrl'){
							 $in_num   = ($in_num)?$in_num + 12*1:'1';
							$next_date =  date('Y-m-d', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }	

						if($paycycle=='2yr'){
							 $in_num   = ($in_num)?$in_num + 2*12:'1';
							$next_date =  date('Y-m-01',strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }
						if($paycycle=='3yr'){
							 $in_num   = ($in_num)?$in_num + 3*12:'1';
							$next_date =  date('Y-m-01', strtotime(date("Y-m-d", strtotime($date)) . " +$in_num month"));
						 }						 
							

						return $next_date;		
							
	}
	
	
	function get_random_string( $attr) 
	{
		$randstr='';
		

		$code =substr(str_shuffle("ABCDEFGHIJKLMNOPQRSTVWXYZ0123456789"), 0, 6);
		if($attr=='CUS')
		$num = mt_rand(5000000,9000000);
		if($attr=='INV')
		$num = mt_rand(10000000,50000000);
		if($attr=='PRO')
		$num = mt_rand(500000,900000);
		
		$randstr =$attr.'-'.$num.'-'.$code;
		return $randstr;
	}
	
	
	public function insert_gateway_transaction_data($res, $tr_type, $gateway,$gt_type,$customer,$amount,$mID,$crtxnID,$resellerID,$invID, $echeckType = false, $transactionByUser = [], $custom_data_fields = [])
	{
			$transaction=array();
			$type = array('AUTH', 'PAYPAL_AUTH','PAY_AUTH','AUTH_ONLY','STRIPE_AUTH');
			if(in_array(strtoupper($tr_type),$type))
			$transaction['transaction_user_status']   = '5';  
			
			if($gt_type==1)
			{	   
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionID']     = $res['transactionid'];
				$transaction['transactionStatus']  = $res['responsetext'];
				$transaction['transactionCode']    =  $res['response_code'];
				$transaction['transactionType']   =  $res['type'];
				$transaction['gateway']          = "NMI"; 
			} 
		if($gt_type==2)
			{	
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 	
				
				if($res->response_code == '1' && $res->transaction_id != 0 && $res->transaction_id != ''){
			   		$transaction['transactionID']       = $res->transaction_id;
			   		$transaction['transactionCode']    = $res->response_code;
			    }else{
			   		$transaction['transactionID']  = 'TXNFAILED'.time();
			   		$transaction['transactionCode']    = 400;
			    }
				$transaction['transactionStatus']   = $res->response_reason_text;
				$transaction['transactionType']     = $res->transaction_type;	   
				  
				$transaction['transactionCard']     = substr($res->account_number,4); 
				
				$transaction['gateway']   = "Auth"; 
			} 
				if($gt_type==3)
			{	   
		
				if(isset($res['transaction_id'])){
					$transaction['transactionID']       = $res['transaction_id'];
				} else if(isset($res['check_transaction_id'])){
						$transaction['transactionID']       = $res['check_transaction_id'];
				}else{
					$transaction['transactionID']  = 'TXNFailed-'.time();
				}
				$transaction['transactionStatus']   = $res['status_message'];
				$transaction['transactionDate']     = date('Y-m-d H:i:s');
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionCode']     = $res['http_status_code'];

				if(isset($res['masked_card_number'])) 
					$transaction['transactionCard']     = substr($res['masked_card_number'],12);  
				$transaction['transactionType']    = $tr_type;
						$transaction['gateway']       = "Paytrace";
			} 		

			if($gt_type==4)
			{	  
					$tranID ='' ;$amt='0.00';
					if(isset($res['TRANSACTIONID'])) {  $code='111';$tranID = $res['TRANSACTIONID'];   $amt=$res["AMT"];  }else{ $code='401';}
					
					if($tr_type=='Paypal_refund')
					{	
						$tranID ='' ;$amt='0.00';
					if(isset($PayPalResult['REFUNDTRANSACTIONID'])) { $tranID = $PayPalResult['REFUNDTRANSACTIONID'];   $amt =$PayPalResult['GROSSREFUNDAMT'];  }
					}
					
					$transaction['transactionID']       = $tranID;
					$transaction['transactionStatus']    = $res["ACK"];
					$transaction['transactionDate']     = date('Y-m-d H:i:s',strtotime($res["TIMESTAMP"]));  
					$transaction['transactionModified'] =  date('Y-m-d H:i:s',strtotime($res["TIMESTAMP"])); 
					$transaction['transactionCode']     = $code;  
					$transaction['gateway']             = "Paypal";
						$transaction['transactionType']    = $tr_type;					   
					
			} 
		if($gt_type==5)
		{	   
			
				
				if($res->paid=='1' && $res->failure_code=="")
				{
					$code		 =  '200';
					$trID 	 = $res->id;
				}
				else
				{
					$code =  $res->failure_code; 	
					$trID 	 = '';	
				}		
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionID']     = $trID;
				$transaction['transactionStatus']  = $res->status;
				$transaction['transactionCode']    =  $code;
					$transaction['transactionType']    = $tr_type;	
					$transaction['gateway']          = "Stripe"; 
		} 
		
			if($gt_type==6)
		{	   
				if($res['transactionCode']=='200')
				{
					$code		 =  '200';
					$trID 	 = $res['transactionId'];
				}
				else
				{
					$code     =    $res['transactionCode']; 	
					$trID 	 = $res['transactionId'];	
				}		
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionID']     = $trID;
				$transaction['transactionStatus']  = $res['status'];
				$transaction['transactionCode']    =  $res['transactionCode'];
					$transaction['transactionType']    = $tr_type;	
					$transaction['gateway']          = "USAePay"; 
				
		} 
		
		
		
		if($gt_type==7)
		{	   
				if($res['transactionCode']=='200')
				{
					$code		 =  '200';
					$trID 	 = $res['transactionId'];
				}
				else
				{
					$code     =    $res['transactionCode']; 	
					$trID 	 = $res['transactionId'];	
				}		
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionID']     = $trID;
				$transaction['transactionStatus']  = $res['status'];
				$transaction['transactionCode']    =  $res['transactionCode'];
					$transaction['transactionType']    = $tr_type;	
					$transaction['gateway']          = "Heartland"; 
				if($echeckType){
					$transaction['paymentType'] = 2;
				}
		} 
		
		
		if($gt_type==8)
		{	   
				if($res['transactionCode']=='200')
				{
					$code		 =  '200';
					$trID 	 = $res['transactionId'];
				}
				else
				{
					$code     =    $res['transactionCode']; 	
					$trID 	 = $res['transactionId'];	
				}		
				$transaction['transactionDate']   = date('Y-m-d H:i:s');  
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['transactionID']     = $trID;
				$transaction['transactionStatus']  = $res['status'];
				$transaction['transactionCode']    =  $res['transactionCode'];
					$transaction['transactionType']    = $tr_type;	
					$transaction['gateway']          = "Cybersource"; 
				
		} else if($gt_type==9){	   
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['transactionID']     = $res['transactionid'];
			$transaction['transactionStatus']  = $res['responsetext'];
			$transaction['transactionCode']    =  $res['response_code'];
			$transaction['transactionType']   =  $res['type'];
			$transaction['gateway']          = "Chargezoom"; 
		} else if($gt_type== 10){
			$code = $res['status_code'] ;
			if($code =='201'){
				$code = '200';
			}
			
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['transactionID']     = $res['id'];
			$transaction['transactionStatus']  = $res['status'];
			$transaction['transactionCode']    =  $code;
			$transaction['transactionType']   =  $tr_type;
			$transaction['gateway']          = iTransactGatewayName; 
		}  else if($gt_type== 11 || $gt_type== 13){
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['transactionID']     = (isset($res['data']) && !empty($res['data'])) ? $res['data']['id'] : 'TXNFail-'.time();
			$transaction['transactionStatus']  = $res['msg'];

			$code = 300;
			if($res['status'] == 'success' && isset($res['data']) && !empty($res['data'])) {
				if(isset($res['data']['response_code'])){
					$code = $res['data']['response_code'];
				} else {
					$code = 100;
				}
			}

			$transaction['transactionCode']    =  $code;
			$transaction['transactionType']   =  $tr_type;
			$transaction['gateway']          = $gt_type == 11 ? FluidGatewayName : BASYSGatewayName; 
		} else if($gt_type== 12){
			$responseType = $res['responseType'];
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['transactionID']     = (isset($res[$responseType]['transactionID'])) ? $res[$responseType]['transactionID'] : 'TXNFail-'.time();
			$transaction['transactionStatus']  = $res[$responseType]['responseMessage'];

			$code = 300;
			if($res[$responseType]['status'] == 'PASS') {
				$code = 100;
			}
			$transaction['transactionCode']    =  $code;
			$transaction['transactionType']   =  $tr_type;
			$transaction['gateway']          = TSYSdGatewayName; 
		} else if($gt_type== 14){	   
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 
			$transaction['transactionID']     = $res['retref'];
			$transaction['transactionStatus']  = $res['responsetext'];
			$transaction['transactionCode']    =  $res['response_code'];
			$transaction['transactionType']   =  $res['type'];
			$transaction['gateway']          = "CardPointe"; 
			$code = 300;
			if($res['resptext'] == 'Approval' || $res['resptext'] == "Success") {
				$code = 100;
			}
			$transaction['transactionCode']   =  $code;
		} else if($gt_type== 15){
							
							
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 

			$transaction['transactionID']     = (isset($res['data']) && !empty($res['data'])) ? $res['data']['id'] : 'TXNFail-'.time();
			$transaction['transactionStatus']  = (isset($res['data']) && !empty($res['data'])) ? 'SUCCESS' : $res['message'];

			$code = 300;
			if(isset($res['data']) && !empty($res['data'])) {
				if(isset($res['data']['response_code'])){
					$code = $res['data']['response_code'];
				} else {
					$code = 100;
				}
			}

			$transaction['transactionCode']    =  $code;
			$transaction['transactionType']   =  $tr_type;
			$transaction['gateway']          = PayArcGatewayName; 
		}else if($gt_type== 16){
							
			$code = 300;

			if(isset($res['AUTH_RESP']) && $res['AUTH_RESP'] == '00' ){
				$message = 'SUCCESS';
				$code = 100; 
				$transactionId = $res['AUTH_GUID'];
			}else{
				$message = $res['AUTH_RESP_TEXT'];
				$transactionId = isset($res['transactionid'])? $res['transactionid']:'TXNFAILED'.time();
			}
			$transaction['transactionDate']   = date('Y-m-d H:i:s');  
			$transaction['transactionModified']= date('Y-m-d H:i:s'); 

			$transaction['transactionID']     = $transactionId;
			
			$transaction['transactionStatus']  = $message;

			$transaction['transactionCode']    =  $code;
			$transaction['transactionType']   =  $tr_type;
			$transaction['gateway']          = EPXGatewayName;
		}
		
		$transaction['gatewayID']       = $gateway;
		$transaction['transactionGateway'] = $gt_type;	
		$transaction['customerListID']     = $customer;
		$transaction['transactionAmount']  = $amount;
		$transaction['merchantID']        = $mID;
		$transaction['qbListTxnID']       = $crtxnID;
		$transaction['resellerID']       = $resellerID;
		
		if($gt_type==5 || $gt_type==2)
        {      
            $transaction['paymentType']       = isset($res->paymentType)?$res->paymentType:1;
        }else{
            $transaction['paymentType']       = isset($res['paymentType'])?$res['paymentType']:1;
        }
			
			$res= $this->db->query('Select appIntegration from app_integration_setting where merchantID="'.$mID.'" ')->row_array();
				$service = $res['appIntegration'];
				if($service==2 || $service==5)
				$transaction['invoiceTxnID']        = $invID; 
				else 	
				$transaction['invoiceID']        = $invID; 

			$transaction = alterTransactionCode($transaction);

			if($echeckType){
				$transaction['gateway'] = $transaction['gateway']. " ECheck";
			}
			$CallCampaign = $this->triggerCampaign($mID,$transaction['transactionCode']);

			if(!empty($transactionByUser)){
				$transaction['transaction_by_user_type'] = $transactionByUser['type'];
				$transaction['transaction_by_user_id'] = $transactionByUser['id'];
			}
			if($custom_data_fields){
				$transaction['custom_data_fields'] = json_encode($custom_data_fields);
			}
			$trID = $this->insert_row('customer_transaction',$transaction);
			return $trID;
	
}
     
     
     
     
	public function get_gateway_data($mID)
	{
	    $res=$gateway=array();
	    $def_url ='';
	    $gateway = $this->get_table_data('tbl_merchant_gateway',array('merchantID'=>$mID));
	    
	    $res['gateway'] = $gateway;
	    $stpUser ="0";
	    $def_gatway = $this->get_row_data('tbl_merchant_gateway',array('merchantID'=>$mID,'set_as_default'=>'1'));
	    
	    if(!empty($def_gatway))
	    {
	        if($def_gatway['gatewayType']=='1')
	        {
	        $def_url = 'Payments/';
	        
	        
	        }
	        if($def_gatway['gatewayType']=='2')
	        {
	        $def_url = 'AuthPayment/';
	        
	        }
	        if($def_gatway['gatewayType']=='3')
	        {
	        $def_url = 'PaytracePayment/';

	        }
	        if($def_gatway['gatewayType']=='4')
	        {
	          $def_url = 'PayPalPayment/';
	      
	        }
	        if($def_gatway['gatewayType']=='5')
	        {
	          $def_url = 'StripePayment/';
	          $stpUser = $def_gatway['gatewayUsername'];
	        }
	        if($def_gatway['gatewayType']=='6')
	        {
	          $def_url = 'UsaePay/';
	      
	        }
	         if($def_gatway['gatewayType']=='7')
	        {
	          $def_url = 'GlobalPayment/';
	        
	        }
	        
	          if($def_gatway['gatewayType']=='8')
	        {
	          $def_url = 'CyberPayment/';
	        
			}
			if($def_gatway['gatewayType']=='17')
	        {
				$def_url = 'MaverickPayment/';
	        }
				if($def_gatway['gatewayType']=='14')
	        {
	        $def_url = 'CardPointe/';
	        
	        
	        }
	    }
	  
	    $res['url'] =   $def_url;
	 
	    $res['stripeUser'] =   $stpUser;
	    
	    return $res;

	}

public function get_public_page_data($m_id)
	{
	   $res=array();
	   
	   $this->db->select('r.resellerCompanyName, r.resellerProfileURL as ProfileURL,r.webURL, m.companyName, m.weburl');
	   $this->db->from('tbl_merchant_data m');
	   $this->db->join('tbl_reseller r','r.resellerID=m.resellerID','INNER');
	   $this->db->where('m.merchID',$m_id);
	   $query = $this->db->get();
	   if($query->num_rows()>0)
	   {
	       $res =$query->row_array();
	   }
	 return $res;
	}
	
		
		public function template_data($con)
		{
		
	$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','inner');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
		  return $res =  $query->row_array();
		
	} 	
	
	
	
	public  function send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date   )
	{
		if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
		$user_id = $condition_mail['merchantID'];
		$res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact', 'merchant_default_timezone'), array('merchID'=>$user_id));

		$appIn =   $this->get_select_data('app_integration_setting',array('appIntegration'), array('merchantID'=>$user_id));

		$customerDetail = $this->get_customer_details_all($customerID, $appIn['appIntegration'], $user_id);
		$customerName = $customerDetail['full_name'];
		$company = $customerDetail['company_name'];

	    $phoneNumber='';
	        
				
				if(isset($condition_mail['templateType']) == 5){
					
					if (isset($res['merchant_default_timezone'])  && !empty($res['merchant_default_timezone'])) {
			            // Convert added date in timezone 
			            $timezone = ['time' => date('Y-m-d H:i:s'), 'current_format' => 'UTC', 'new_format' => $res['merchant_default_timezone']];
			            $tr_date = getTimeBySelectedTimezone($timezone);
			            $tr_date   = date('Y-m-d h:i A', strtotime($tr_date));
			        }

				}
	          	$view_data      = $this->template_data($condition_mail);	
					  
				$companyMercName = $merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];
				$config_email = $res['merchantEmail'];	  
							  $subject = $view_data['emailSubject'];
							  $fromEmail = $view_data['fromEmail'];
							$phoneNumber=$res['merchantContact'];
							$addCC      = $view_data['addCC'];
							$addBCC		= $view_data['addBCC'];
				
							if($view_data['fromEmail'] == ''){
								$fromEmail = 'donotreply@payportal.com';
							}

							$display_name = $view_data['mailDisplayName'];
							if($view_data['mailDisplayName'] == ''){
								$display_name = $merchant_name;
							}

							$storeMail = true;
							if(!$toEmail){
								$storeMail = false;
								$toEmail = $config_email;
							}
					     		
							  $in_link='';
							   $message = $view_data['message'];
						
							   $message = stripslashes(str_replace('{{merchant_company_name}}',$companyMercName ,$message ));
							   $message = stripslashes(str_replace('{{customer.company}}',$company ,$message));
							   $message = stripslashes(str_replace('{{customer.name}}',$customerName ,$message));
							   $message = stripslashes(str_replace('{{customer_contact_name}}',$customer ,$message ));
							   $message =	stripslashes(str_replace('{{invoice.refnumber}}',$ref_number, $message));
								$message =	stripslashes(str_replace('{{invoice_payment_pagelink}}',$in_link, $message));
							   $message = stripslashes(str_replace('{{transaction.amount}}',($amount)?('$'.number_format($amount,2)):'0.00' ,$message )); 
							   $message = stripslashes(str_replace('{{transaction.transaction_date}}', $tr_date, $message ));
							   $message = stripslashes(str_replace('{{merchant_name}}',$merchant_name ,$message));
							   $message = stripslashes(str_replace('{{merchant_email}}',$config_email ,$message ));
     						   $message = stripslashes(str_replace('{{merchant_phone}}',$phoneNumber ,$message ));
    
                     $logo_url ='';
		   $config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
		   if(!empty($config_data))
		   $logo_url  = $config_data['ProfileImage']; 
           
		  
	      if( !empty( $config_data['ProfileImage'])){   $logo_url = LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }
    
    		$logo_url=	"<img src='".$logo_url."' />";
        $message = stripslashes(str_replace('{{logo}}',$logo_url ,$message ));
    
    
    
		if($view_data['replyTo'] != ''){
			$replyTo = $view_data['replyTo'];
		}else{
			$replyTo = $res['merchantEmail'];
		}
		$tr_date   = date('Y-m-d H:i:s');
		$email_data          = array(
				'customerID'=>$customerID,
				'merchantID'=>$user_id, 
				'emailSubject'=>$subject,
				'emailfrom'=>$config_email,
				'emailto'=>$toEmail,
				'emailcc'=>$addCC,
				'emailbcc'=>$addBCC,
				'emailreplyto'=>$replyTo,
				'emailMessage'=>$message,
				'emailsendAt'=>$tr_date,
				
				);


		$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$display_name,$message,$replyTo, $addCC, $addBCC);
		if($mail_sent && $storeMail)
		{
			$email_data['send_grid_email_id'] = $mail_sent;
			$email_data['send_grid_email_status'] = 'Sent';
			$this->insert_row('tbl_template_data', $email_data); 
		}
		else
		{
			$email_data['send_grid_email_status'] = 'Failed';
			$email_data['mailStatus']=0;
			$this->insert_row('tbl_template_data', $email_data); 
		}
		
	                          
	                            
	
	
	}
	
	public function chkCardSurcharge($args){
		$cardNumber = $args['cardNumber'];
			
		$query  = $this->db->query("SELECT *
		FROM tbl_card_bin_range
		WHERE LEFT($cardNumber, LENGTH(low_bin)) BETWEEN low_bin AND max_bin
		ORDER BY LENGTH(low_bin) DESC LIMIT 1");
		if($query->num_rows()>0 ) {
			$datas = $query->result_array();
			$res = $datas[0];
			if(isset($res['is_surcharge'])){
				return $res['is_surcharge'];
			}else{
				return 0;
			}
		}
		
		return 0;
	}
	public function triggerCampaign($merchantID,$transactionCode){
		$merchant_condition = ['merchID' => $merchantID];
	    $merchantData = $this->get_row_data('tbl_merchant_data',$merchant_condition);
   		if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == 23)
        {
	   		$tcode = array('100', '200', '111', '1','120');
								
			$data=array();	

			$this->db->select('transactionID')->from('customer_transaction');

			$this->db->where("merchantID", $merchantID);

			$this->db->where_in('(transactionCode)', $tcode);

			$query = $this->db->get();
			$countTotal = $query->num_rows();
			if($countTotal == 0){
				
				if (in_array($transactionCode, $tcode)){
					/* Start campaign in hatchbuck CRM*/  
	                $this->load->library('hatchBuckAPI');
	                
	                $merchantData['merchant_type'] = 'Merchant First Payment';        
	                
	                $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_TRANSACTION);
	                if($status['statusCode'] == 400){
	                    $resource = $this->hatchbuckapi->createContact($merchantData);
	                    if($resource['contactID'] != '0'){
	                        $contact_id = $resource['contactID'];
	                        $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_FIRST_TRANSACTION);
	                    }
	                }
	                /* End campaign in hatchbuck CRM*/ 
				}
			}
		}
   	}

	public  function surcharge_send_mail_data($condition_mail,$company,$customer,$toEmail, $customerID,$ref_number,$amount, $tr_date,$transactionID   )
	{
		if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
	    $phoneNumber='';
        $user_id = $condition_mail['merchantID'];
        $res =   $this->get_select_data('tbl_merchant_data',array('merchantEmail','companyName','firstName','lastName','merchantContact'), array('merchID'=>$user_id));

        $view_data      = $this->template_data($condition_mail);
        if(isset($view_data['templateID'])){
        	$pay_data      = $this->getiTransactTransactionDetails($user_id,$transactionID);	
		    $surchargeAmount = $pay_data['surCharge'];
		    $totalAmount = $pay_data['totalAmount'];

			$merchant_name=	($res['companyName'])?$res['companyName']:$res['firstName'].''.$res['lastName'];	  
			$subject = $view_data['emailSubject'];
			$fromEmail = $view_data['fromEmail'];
			$phoneNumber=$res['merchantContact'];

			if($view_data['fromEmail'] != ''){
				$fromEmail=	$config_email =	$view_data['fromEmail'];
			}else{
				$fromEmail = $config_email = 'donotreply@payportal.com';
			}
					     		
			$in_link='';
			$message = $view_data['message'];

			$message = stripslashes(str_replace('{{merchant_company_name}}',$merchant_name ,$message ));
			$message = stripslashes(str_replace('{{customer.company}}',$company ,$message));
			$message = stripslashes(str_replace('{{customer_contact_name}}',$customer ,$message ));
			$message =	stripslashes(str_replace('{{invoice.refnumber}}',$ref_number, $message));
			$message =	stripslashes(str_replace('{{invoice_payment_pagelink}}',$in_link, $message));
			$message = stripslashes(str_replace('{{transaction.amount}}',($amount)?('$'.number_format($amount,2)):'0.00' ,$message )); 
			$message = stripslashes(str_replace('{{surcharge.amount}}',($surchargeAmount)?('$'.number_format($surchargeAmount,2)):'0.00' ,$message ));
			$message = stripslashes(str_replace('{{authorized.amount}}',($totalAmount)?('$'.number_format($totalAmount,2)):'0.00' ,$message ));
			$message = stripslashes(str_replace('{{transaction.transaction_date}}', $tr_date, $message ));
			$message = stripslashes(str_replace('{{merchant_name}}',$merchant_name ,$message));
			$message = stripslashes(str_replace('{{merchant_email}}',$config_email ,$message ));
			$message = stripslashes(str_replace('{{merchant_phone}}',$phoneNumber ,$message ));

	        $logo_url ='';
		    $config_data = $this->get_select_data('tbl_config_setting',array('ProfileImage'), array('merchantID'=>$user_id));
		    if(!empty($config_data))
		   		$logo_url  = $config_data['ProfileImage']; 
	       
	      	if( !empty( $config_data['ProfileImage'])){   $logo_url = LOGOURL.$config_data['ProfileImage'];  }else{  $logo_url = CZLOGO; }

			$logo_url=	"<img src='".$logo_url."' width='200' height='80' />";
	    	$message = stripslashes(str_replace('{{logo}}',$logo_url ,$message ));

			$addBCC=$addCC='';	
			if($view_data['replyTo'] != ''){
				
				$replyTo = $view_data['replyTo'];
				
			}else{
				$replyTo = $res['merchantEmail'];
			}

			if($view_data['addCC'] != ''){
				$addCC = $view_data['addCC'];
			}

			if($view_data['addBCC'] != ''){
				$addBCC = $view_data['addBCC'];
			}
				
		   	
		   	$email_data          = array(
	                'customerID'=>$customerID,
					'merchantID'=>$user_id, 
					'emailSubject'=>$subject,
					'emailfrom'=>$config_email,
					'emailto'=>$toEmail,
					'emailcc'=>$addCC,
					'emailbcc'=>$addBCC,
					'emailreplyto'=>$replyTo,
					'emailMessage'=>$message,
					'emailsendAt'=>date("Y-m-d h:i:s", strtotime($tr_date)),
					
					);

		  
			$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC, $addBCC);
			if($mail_sent)
			{
				$email_data['send_grid_email_id'] = $mail_sent;
				$email_data['send_grid_email_status'] = 'Sent';
			    $this->insert_row('tbl_template_data', $email_data); 
			}
			else
			{
				$email_data['send_grid_email_status'] = 'Failed';
			    $email_data['mailStatus']=0;
			    $this->insert_row('tbl_template_data', $email_data); 
			}
        }						
	}
	function getiTransactTransactionDetails($merchID,$transaction_id){
		
		$gt_result = $this->get_row_data('tbl_merchant_gateway', array('merchantID' => $merchID,'gatewayType' => 10));

        $apiUsername  = $gt_result['gatewayUsername'];
        $apiKey  = $gt_result['gatewayPassword']; 
        $isSurcharge  = $gt_result['isSurcharge']; 
        $surCharge = 0;
		$totalAmount =0;
		$payAmount = 0;
		$data = array('surCharge' => 0,'totalAmount' => 0,'payAmount' => 0,'isSurcharge' => $isSurcharge );
        if($isSurcharge){
        	$sdk = new iTTransaction();
			$payload = [];

	        $result1 = $sdk->getTransactionByID($apiUsername, $apiKey, $payload,$transaction_id);
	        
	        if ($result1['status_code'] == '200' || $result1['status_code'] == '201') {
	        	if(isset($result1['surcharge_amount']) && $result1['surcharge_amount'] != ''){
	        		$data['surCharge'] = $result1['surcharge_amount'] / 100;
	        	}
	        	$data['totalAmount'] = $result1['authorized_amount'] / 100;
	        	$data['payAmount'] = $result1['amount'] / 100;
	        }
        }

		return $data;
	}
	public function sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo, $addCC, $addBCC){
		$request_array = [
			"personalizations" => [
				[
				"to" => [
					[
					"email" => $toEmail
					]
				],
				"subject" => $subject
				]
			],
			"from" => [
				"email" => $fromEmail,
				"name" => $merchant_name
			],
			"reply_to" => [
				"email" => $replyTo
			],
			"content" => [
				[
				"type" => "text/html",
				"value" => $message
				]
			]
		];

		if(isset($addCC) && !empty($addCC)){
			$addCC = explode(';', $addCC);
	
			$toaddCCArrEmail = [];
			foreach ($addCC as $value) {
				$toaddCCArrEmail[]['email'] = trim($value);
			}
			$request_array['personalizations'][0]['cc'] = $toaddCCArrEmail;
		}
	
		if(isset($addBCC) && !empty($addBCC)){
			$addBCC = explode(';', $addBCC);
	
			$toaddBCCArrEmail = [];
			foreach ($addBCC as $value) {
				$toaddBCCArrEmail[]['email'] = trim($value);
			}
			$request_array['personalizations'][0]['bcc'] = $toaddBCCArrEmail;
		}

		// get api key for send grid
		$api_key = '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';
		$url = 'https://api.sendgrid.com/v3/mail/send';

		// set authorization header
		$headerArray = [
			'Authorization: Bearer '.$api_key,
			'Content-Type: application/json',
			'Accept: application/json'
		];

		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		// add authorization header
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

		$response = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		// parse header data from response
		$header_text = substr($response, 0, $header_size);
		// parse response body from curl response
		$body = substr($response, $header_size);

		$headers = array();
		
		foreach (explode("\r\n", $header_text) as $i => $line){
	        if ($i === 0){
	            $headers['http_code'] = $line;
	       
	        }else{
	        	$explode_value = explode(': ', $line);
	        	$key = isset($explode_value[0]) ? $explode_value[0] : ''; 
	        	$value = isset($explode_value[1]) ? $explode_value[1] : '';
	        	if($key){
	            	$headers[$key] = $value;
	        	}
	        }
		}
		// if mail sent success
		if($httpcode == '202' || $httpcode == '200'){
			$success = isset($headers['X-Message-Id']) ? $headers['X-Message-Id'] : true;
		}else{
			$success = false;
		}
		return $success;
	}

	/**
	 *  Login Throttling
	 */
	public function addLoginAttempt($ip, $post_data=null)
	{
		$base64_data = ($post_data) ? base64_encode(json_encode($post_data)) : null;
		$data = ['ip' => $ip, 'base64_data' => $base64_data, 'created_at' => date('Y-m-d H:i:s')];
		$this->db->insert('tbl_login_attempts', $data);
	}

	public function getLastLoginAttemptCount($ip, $seconds=300)
	{
		
		$this->db->select('count(id) as c');
		$this->db->from('tbl_login_attempts');
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-$seconds) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->group_by('ip');

		$data = $this->db->get()->row_array();
		
		return ($data ? $data['c'] : 0);
	}

	public function deleteLoginAttempt($ip)
	{
		// Deleting past an hour 
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-3600) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->delete('tbl_login_attempts');
	}
	/**
	 * 
	 * End Login Throttling
	 */
	// Add Payment Action Log
	public function addPaymentLog($gateway_id, $access_url=null, $request=null, $response=null, $status=null)
	{
		
		$data = [];
		$data['payment_gateway_id'] = $gateway_id;
		$data['access_url'] = $access_url;
		$data['access_block'] = 'customer';
		$data['request_payload'] = (is_array($request) ? json_encode($request): $request) ;
		$data['response_payload'] = (is_array($response) ? json_encode($response): $response) ;
		$this->db->insert('tbl_payment_logs', $data);
	}

	public function get_customer_details_all($customerID, $active_app, $merchantID){

		$customerName = [
			'full_name' => '',
			'fname' => '',
			'lname' => '',
			'email' => '',
			'company_name' => '',
			'contact_number' => '',
			'customerId' => $customerID,
		];

		if($active_app == 1){
			$customer = $this->get_row_data('QBO_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 2){
			$customer = $this->get_row_data('qb_test_customer', array('qbmerchantID' => $merchantID,'ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['FullName'];
				$customerName['fname'] = $customer['FirstName'];
				$customerName['lname'] = $customer['LastName'];
				$customerName['email'] = $customer['Contact'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['Phone'];
			}
		} else if($active_app == 3){
			$customer = $this->get_row_data('Freshbooks_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 4){
			$customer = $this->get_row_data('Xero_custom_customer', array('merchantID' => $merchantID,'Customer_ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['fullName'];
				$customerName['fname'] = $customer['firstName'];
				$customerName['lname'] = $customer['lastName'];
				$customerName['email'] = $customer['userEmail'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['phoneNumber'];
			}
		} else if($active_app == 5){
			$customer = $this->get_row_data('chargezoom_test_customer', array('qbmerchantID' => $merchantID,'ListID' => $customerID));
			if($customer){
				$customerName['full_name'] = $customer['FullName'];
				$customerName['fname'] = $customer['FirstName'];
				$customerName['lname'] = $customer['LastName'];
				$customerName['email'] = $customer['Contact'];
				$customerName['company_name'] = $customer['companyName'];
				$customerName['contact_number'] = $customer['Phone'];
			}
		}

		return $customerName;
	}
	public function qbo_payment_method($paymentMethod, $merchantID, $isACH = true){
		$where = [
			'payment_type' => ($isACH) ? 'NON_CREDIT_CARD' : 'CREDIT_CARD',
			'merchantID' =>  $merchantID
		];

	    $query  = $this->db->select('*')->from('QBO_payment_method')->where($where)->order_by('payment_name')->get();
		if ($query->num_rows() > 0) {
			$data = $query->result_array();
			foreach ($data as $key => $val) {
				if (strtoupper($val['payment_name']) === strtoupper($paymentMethod)) {
					return $val;
				}
			}

			return $data[0];
		}

		return false;
	}

}

 