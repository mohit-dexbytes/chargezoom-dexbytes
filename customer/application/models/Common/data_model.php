<?php
class Data_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_product_list($userID)
    {

        $result = array();

        $this->db->select('* ');
        $this->db->from('Xero_test_item qb_item');
        $this->db->where('merchantID', $userID);
        $i       = 0;
        $con     = '';
        $column1 = array('qb_item.Name', 'qb_item.SalesDescription', 'qb_item.QuantityOnHand', 'qb_item.saleCost');
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result_array();
        }

        return [
            'count'    => $count,
            'products' => $result,
        ];

    }

    public function get_product_data($userID, $where = false)
    {

        $result = false;

        $this->db->select('* ');
        $this->db->from('Xero_test_item qb_item');
        $this->db->where('merchantID', $userID);
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get();
        $count = $query->num_rows();
        if ($count > 0) {
            $result = $query->result_array();
        }

        return $result;
    }

    public function get_invoice_data($userID, $status = '', $customerID = null)
    {

        $res   = array();
        $today = date('Y-m-d');

        if ($status == 'Past Due') {
            $cond = "AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel'  AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = inv.invoiceID AND merchantID = '$userID' limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =inv.invoiceID limit 1) !='300')";
        } else if ($status == 'Failed') {
            $cond = "AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel' and (select transactionCode from customer_transaction where invoiceTxnID =inv.invoiceID AND merchantID = '$userID' limit 1 )='300'";
        } else if ($status == 'Paid') {
            $cond = "AND IsPaid ='true'";
        } else if ($status == 'Cancelled') {
            $cond = "AND userStatus='cancel'";
        } else if ($status == 'Open') {
            $cond = "AND (userStatus!='cancel' and inv.IsPaid = 'false')  ";
        } else {
            $cond = "";
        }

        $sql = "SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , inv.CustomerListID as ListID, inv.CustomerFullName as FullName,
            ( case   when  IsPaid ='true'     then 'Paid'
            when  IsPaid ='true' and userStatus ='Paid'     then 'Paid'
            when (sc.scheduleDate  IS NOT NULL  and IsPaid='false' AND userStatus!='cancel' ) THEN 'Scheduled'
            when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND  `IsPaid` = 'false'  AND userStatus!='cancel'  then 'Open'
                when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  AND userStatus!='cancel'  and (select transactionCode from customer_transaction where invoiceTxnID =inv.invoiceID AND merchantID = '$userID' limit 1 )='300'  then 'Failed'
            when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'   AND userStatus!='cancel' then 'Overdue'

            else 'Voided' end ) as status,
            ( case   when  inv.IsPaid='true' then 'Done'  when inv.IsPaid='false' then 'Done' else 'Done' end) as c_status,
            ifnull(sc.scheduleDate,`inv`.DueDate ) as DueDate
            FROM Xero_test_invoice inv

            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`invoiceID` AND sc.merchantID=  '" . $userID . "'
            WHERE inv.merchantID = '$userID' $cond
           AND (sc.customerID=inv.CustomerListID OR sc.customerID IS NULL)  ";
        $i   = 0;
        $con = '';
        if ($customerID) {
            $sql .= ' AND inv.CustomerListID = "' . $customerID . '" ';
        }
        $column1 = array('inv.CustomerFullName', 'inv.refNumber', 'inv.AppliedAmount');
        foreach ($column1 as $item) {

            if (isset($_POST['search']) && $_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $sql .= $con;
        }

        $column = 0;
        if (isset($_POST['order']) && $_POST['order'] != '') {
            $column = $_POST['order'][0]['column'];
        }

        if ($column == 0) {
            if (isset($_POST['customerID']) && $_POST['customerID'] != '') {
                $orderName = 'inv.refNumber';
            } else {
                $orderName = 'FullName';
            }

        } elseif ($column == 1) {
            if (isset($_POST['customerID']) && $_POST['customerID'] != '') {
                $orderName = 'DueDate';
            } else {
                $orderName = 'FullName';
            }

        } elseif ($column == 2) {
            if (isset($_POST['customerID']) && $_POST['customerID'] != '') {
                $orderName = 'inv.BalanceRemaining';
            } else {
                $orderName = 'inv.refNumber';
            }

        } elseif ($column == 3) {
            if (isset($_POST['customerID']) && $_POST['customerID'] != '') {
                $orderName = 'c_status';
            } else {
                $orderName = 'DueDate';
            }

        } elseif ($column == 4) {
            if (isset($_POST['customerID']) && $_POST['customerID'] != '') {
                $orderName = 'inv.refNumber';
            } else {
                $orderName = 'inv.BalanceRemaining';
            }

        } elseif ($column == 5) {
            $orderName = 'c_status';
        } else {
            $orderName = 'inv.refNumber';
        }

        if (isset($_POST['order'])) {
            $orderBY = $_POST['order'][0]['dir'];
            $sql .= " ORDER BY $orderName  $orderBY ";
        }

        if (isset($_POST['length']) && $_POST['length'] != -1) {
            $sql .= 'LIMIT ' . $_POST['length'] . ' OFFSET ' . $_POST['start'] . '';
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;
    }

    public function get_invoice_list_count($userID, $status = '', $customerID = null)
    {

        $res   = array();
        $today = date('Y-m-d');

        if ($status == 'Past Due') {
            $cond = "AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel'  AND ((select count(*) as sch_count from customer_transaction where invoiceTxnID = inv.invoiceID AND merchantID = '$userID' limit 1) ='0' OR (select transactionCode from customer_transaction where invoiceTxnID =inv.invoiceID limit 1) !='300')";
        } else if ($status == 'Failed') {
            $cond = "AND DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and inv.IsPaid = 'false'   and userStatus!='cancel' and (select transactionCode from customer_transaction where invoiceTxnID =inv.invoiceID AND merchantID = '$userID' limit 1 )='300'";
        } else if ($status == 'Paid') {
            $cond = "AND IsPaid ='true'";
        } else if ($status == 'Cancelled') {
            $cond = "AND userStatus='cancel'";
        } else if ($status == 'Open') {
            $cond = "AND (userStatus!='cancel' and inv.IsPaid = 'false')  ";
        } else {
            $cond = "";
        }

        $sql = "SELECT inv.TimeCreated FROM Xero_test_invoice inv
            LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`inv`.`invoiceID` AND sc.merchantID=  '" . $userID . "'
	        WHERE inv.merchantID = '$userID'
	        AND (sc.customerID=inv.CustomerListID OR sc.customerID IS NULL) $cond
        ";
        $i       = 0;
        $con     = '';
        $column1 = array('inv.CustomerFullName', 'inv.refNumber', 'inv.AppliedAmount');
        foreach ($column1 as $item) {

            if (isset($_POST['search']) && $_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "AND (" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $sql .= $con;
        }
        if ($customerID) {
            $sql .= ' AND inv.CustomerListID = "' . $customerID . '" ';
        }
        $sql .= " ORDER BY inv.TimeCreated  DESC ";
        $query = $this->db->query($sql);
        //echo  $this->db->last_query(); die;
        $res = $query->num_rows();

        return $res;
    }

    public function get_tax_data($con)
    {
        $res = array();
        $this->db->select('*');
        $this->db->from('tbl_taxes_xero');
        $this->db->where($con);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }
        return $res;
    }

    public function get_plan_data_invoice($userID)
    {
        $res = array();
        $this->db->select('*');
        $this->db->from('xero_test_item');
        $this->db->where(['merchantID' => $userID]);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }
        return $res;
    }

    public function get_invoice_details_item_data($invoiceID, $userID)
    {
        $res = array();
        $sql = "SELECT * FROM Xero_test_item XTI
        INNER JOIN tbl_xero_invoice_item TXI ON TXI.item_code = XTI.Code AND XTI.merchantID = TXI.merchantID
        WHERE TXI.invoiceID = '$invoiceID' AND TXI.merchantID = '$userID'";

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }
        return $res;

    }

    public function template_data($con)
    {
        $res = array();
        $this->db->select('tp.*, typ.*');
        $this->db->from('tbl_email_template tp');
        $this->db->join('tbl_teplate_type typ', 'typ.typeID=tp.templateType', 'left');
        $this->db->where($con);

        $query      = $this->db->get();
        return $res = $query->row_array();
    }

    public function template_data_list($condition)
    {

        $res = array();
        $this->db->select('tp.*, typ.* ');
        $this->db->from('tbl_email_template tp');
        $this->db->join('tbl_teplate_type typ', 'typ.typeID = tp.templateType', 'inner');
        $this->db->where($condition);

        $query      = $this->db->get();
        return $res = $query->result_array();
    }

    public function custom_template_data_list($condition)
    {

        $res = array();
        $this->db->select('tp.* ');
        $this->db->from('tbl_email_template tp');
        $this->db->where($condition);

        $query      = $this->db->get();
        return $res = $query->result_array();
    }

    public function get_invoice_transaction_data($invoiceID, $userID, $action, $appType = false)
    {
        if(!$appType){
            if($this->session->userdata('logged_in')){
                $appType 				= $this->session->userdata('logged_in')['active_app'];
            } else if($this->session->userdata('user_logged_in')){
                $appType 				= $this->session->userdata('user_logged_in')['active_app'];
            } else {
                $appType = 4;
            }
        }
        $this->db->select('tr.id, tr.transactionID,sum(tr.transactionAmount) as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID, cust.* ');
        $this->db->from('customer_transaction tr');
        if ($appType == 4) {
            $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        }
        $this->db->where("cust.merchantID ", $userID);
        if ($action == 'invoice') {
            $this->db->where("tr.invoiceID ", $invoiceID);
        }

        if ($action == 'transaction') {
            $this->db->where("tr.transactionID ", $invoiceID);
        }

        //  $this->db->where('cust.customerStatus', '1');
        $this->db->group_by('transactionID');

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            return $query->result_array();
        }

    }

    public function get_transaction_data_by_id($invoiceID, $userID, $action, $appType = false)
    {
        if(!$appType){
            if($this->session->userdata('logged_in')){
                $appType 				= $this->session->userdata('logged_in')['active_app'];
            } else if($this->session->userdata('user_logged_in')){
                $appType 				= $this->session->userdata('user_logged_in')['active_app'];
            } else {
                $appType = 4;
            }
        }

        $this->db->select('tr.transactionID,sum(tr.transactionAmount)as transactionAmount ,tr.gateway,tr.transactionCode,tr.transactionDate, tr.transaction_user_status, tr.transactionType, cust.FullName, tr.transaction_by_user_type, tr.transaction_by_user_id, tr.merchantID ');
        $this->db->from('customer_transaction tr');
        if ($appType == 4) {
            $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        }
        $this->db->where("cust.merchantID", $userID);
        if ($action == 'transaction') {
            $this->db->where("tr.id", $invoiceID);
        } else {
            $this->db->where("tr.transactionID", $invoiceID);
        }
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function get_invoice_data_template($con)
    {

        $res   = array();
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT `inv`.invoiceID, inv.TimeCreated, `inv`.refNumber AS RefNumber, `inv`.DueDate, (`inv`.AppliedAmount) AS AppliedAmount, `inv`.BalanceRemaining, DATE_FORMAT( inv.TimeModified, '%d-%m-%Y %l.%i %p' ) AS TimeModifiedinv, cust.*, IFNULL( ( SELECT transactionType FROM customer_transaction WHERE invoiceID = inv.invoiceID ORDER BY id DESC LIMIT 1 ), '' ) AS paymentType, IFNULL( ( SELECT transactionCode FROM customer_transaction WHERE invoiceID = inv.invoiceID ORDER BY id DESC LIMIT 1 ), '' ) AS tr_status, IFNULL( ( SELECT transactionDate FROM customer_transaction WHERE invoiceID = inv.invoiceID ORDER BY id DESC LIMIT 1 ), '' ) AS tr_date, IFNULL( ( SELECT transactionAmount FROM customer_transaction WHERE invoiceID = inv.invoiceID ORDER BY id DESC LIMIT 1 ), '' ) AS tr_amount, IFNULL( ( SELECT transactionStatus FROM customer_transaction WHERE invoiceID = inv.invoiceID ORDER BY id DESC LIMIT 1 ), '' ) AS tr_data,( CASE WHEN DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' AND userStatus = 'Active' THEN 'Upcoming' WHEN DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' AND userStatus = 'Active' AND( SELECT transactionCode FROM customer_transaction WHERE invoiceID = inv.invoiceID LIMIT 1 ) = '300' THEN 'Failed' WHEN DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' AND userStatus = 'Active' THEN 'Past Due' WHEN DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND IsPaid = 'true' AND userStatus = 'Active' THEN 'Success' ELSE 'Canceled' END ) AS STATUS , cust.userEmail AS email FROM Xero_test_invoice inv INNER JOIN Xero_custom_customer cust ON inv.CustomerListID = cust.Customer_ListID WHERE inv.CustomerListID != '' $con LIMIT 1");
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $res = $query->row_array();
        }
        return $res;
    }

    public function get_invoice_data_pay($invoiceID, $merchantID)
    {
        $res = array();

        $this->db->select('inv.*,cust.*');
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID', 'INNER');
        $this->db->where('inv.invoiceID', $invoiceID);
        $this->db->where('inv.merchantID', $merchantID);
        $this->db->where('cust.merchantID', $merchantID);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $res = $query->row_array();
        }

        return $res;
    }

    public function plan_items($planID, $merchantID){
        $res = array();

        $this->db->select('PI.*,XTI.*');
        $this->db->from('tbl_subscription_plan_item_xero PI');
        $this->db->join('Xero_test_item XTI', 'XTI.productID = PI.itemListID', 'INNER');
        $this->db->where('PI.planID', $planID);
        $this->db->where('XTI.merchantID', $merchantID);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;
    }
}
