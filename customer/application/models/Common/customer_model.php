<?php
class Customer_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function xero_customer_id($id, $user_id)
    {
        $this->db->from('Xero_custom_customer cus');
        $this->db->where('cus.Customer_ListID', $id);
        $this->db->where('cus.merchantID', $user_id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row();
        } else {
            return false;
        }
    }

    public function get_transaction_history_data($userID, $customerID = null)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1', '120');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT', 'REFUND', 'STRIPE_REFUND', 'USAEPAY_REFUND', 'PAY_REFUND', 'PAYPAL_REFUND', 'CREDIT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,  cust.Customer_ListID, cust.fullName, inv.RefNumber, ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');
        $this->db->join('Xero_test_invoice inv', "inv.invoiceID= tr.invoiceID AND inv.merchantID = '$userID' ", 'LEFT');

        $this->db->where("cust.merchantID", $userID);
        $this->db->where("tr.merchantID ", $userID);
        if (!empty($customerID)) {
            $this->db->where("tr.customerListID ", $customerID);
        }
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            $res1 = $query->result_array();

            // print_r($res1); die;
            foreach ($res1 as $result) {

                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Xero_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        // echo $this->db->last_query();
                        // echo "<br>";
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }

                    //   print_r($res_inv);
                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {
                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID  ');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;

                    }

                } else {

                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        //print_r($res); die;
        return $res;

    }

    public function getPartialNewAmount($transactionID)
    {
        $ref_amt = 0;

        $qr   = $this->db->query('SELECT id FROM customer_transaction WHERE transactionID ="' . $transactionID . '" ORDER BY `id` DESC');
        $data = $qr->result_array();

        if (!empty($data)) {
            foreach ($data as $value) {
                $qr1   = $this->db->query('SELECT sum(transactionAmount) as transactionAmount FROM customer_transaction WHERE parent_id ="' . $value['id'] . '" ORDER BY `id` DESC');
                $data1 = $qr1->row_array();

                if (!empty($data1)) {
                    $ref_amt = $ref_amt + $data1['transactionAmount'];
                }

            }

            return $ref_amt;

        } else {
            return $ref_amt;
        }
    }

    public function get_customer_data($con)
    {
        $res = array();
        $this->db->select('cust.*, (select sum(BalanceRemaining)  from Xero_test_invoice where merchantID ="' . $con['merchantID'] . '" and CustomerListID=cust.Customer_ListID  and userStatus!="VOIDED" ) as balance ');
        $this->db->from('Xero_custom_customer cust');
        $this->db->where($con);
        $query = $this->db->get();
        //echo $this->db->last_query();  die;
        if ($query->num_rows() > 0) {
            $res = $query->result_array();
        }

        return $res;

    }

    public function get_customer_note_data($custID, $mID)
    {

        $res = array();

        $query = $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `Xero_custom_customer` cust ON `pr`.`customerID` = `cust`.`Customer_ListID`
	  WHERE `pr`.`customerID` = '" . $custID . "' and   `pr`.`merchantID` = '" . $mID . "' and `cust`.`merchantID` = '" . $mID . "' group by pr.noteID desc");
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }
        return $res;
    }

    public function get_customers_data($merchantID)
    {
        $this->db->select('*, fullName as companyName');
        $this->db->from('Xero_custom_customer');
        $this->db->where('merchantID', trim($merchantID));
        // $this->db->where('customerStatus','true');
        $this->db->order_by('fullName', 'asc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }

    }

    public function get_transaction_data_captue($userID)
    {

        $today = date("Y-m-d H:i");
        $res   = array();

        $type  = array('AUTH', 'PAYPAL_AUTH', 'PAY_AUTH', 'AUTH_ONLY', 'STRIPE_AUTH');
        $tcode = array('100', '200', '100', '111', '1');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,cust.Customer_ListID,  cust.fullName');

        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'cust.Customer_ListID=tr.customerListID', 'inner');
        $this->db->where('cust.merchantID', $userID);

        $this->db->where('tr.merchantID', $userID);
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where_in('tr.transaction_user_status', 5);
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }

    }

    public function get_transaction_datarefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Xero_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');

        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->not_like("tr.gateway", 'ECheck');
        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res1 = $query->result_array();
            foreach ($res1 as $result) {
                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber from Xero_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }
                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');

                    $data = $qr->row_array();
                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt           = $this->getPartialNewAmount($result['transactionID']);
                        $result['partial'] = $ref_amt;
                    }
                } else {
                    $result['partial'] = $ref_amt;
                }
                $res[] = $result;
            }
        }
        return $res;
    }

    public function get_transaction_data_erefund($userID)
    {
        $today = date("Y-m-d H:i");
        $res   = array();
        $tcode = array('100', '200', '111', '1');
        $type  = array('SALE', 'CAPTURE', 'AUTH_CAPTURE', 'PRIOR_AUTH_CAPTURE', 'PAY_SALE', 'PAY_CAPTURE', 'STRIPE_SALE', 'STRIPE_CAPTURE', 'PAYPAL_SALE', 'PAYPAL_CAPTURE', 'OFFLINE PAYMENT');

        $this->db->select('tr.*,sum(tr.transactionAmount) as transactionAmount,tr.invoiceID,tr.transaction_user_status,cust.Customer_ListID, cust.fullName,(select refNumber from Xero_test_invoice where invoiceID= tr.invoiceID limit 1) as refNumber ');
        $this->db->select('ifnull(GROUP_CONCAT(DISTINCT(tr.invoiceID) order by tr.id asc SEPARATOR"," ),"") as invoice_id', false);
        $this->db->select('DATEDIFF("' . $today . '", DATE_FORMAT(tr.transactionDate, "%Y-%m-%d H:i")) as tr_Day', false);
        $this->db->from('customer_transaction tr');
        $this->db->join('Xero_custom_customer cust', 'tr.customerListID = cust.Customer_ListID', 'INNER');

        $this->db->where("cust.merchantID ", $userID);
        $this->db->where("tr.merchantID ", $userID);
        $this->db->like("tr.gateway", 'ECheck');

        $this->db->where_in('UPPER(tr.transactionType)', $type);
        $this->db->where_in('(tr.transactionCode)', $tcode);
        $this->db->where("tr.transaction_user_status NOT IN (3,2) ");
        $this->db->order_by("tr.transactionDate", 'desc');
        $this->db->group_by("tr.transactionID");

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $res1 = $query->result_array();
            foreach ($res1 as $result) {
                if (!empty($result['invoice_id'])) {
                    $inv_array = array();
                    $inv_array = explode(', ', $result['invoice_id']);
                    $res_inv   = array();
                    foreach ($inv_array as $inv) {
                        $qq = $this->db->query(" Select refNumber   from   Xero_test_invoice where invoiceID='" . trim($inv) . "'  limit 1 ");
                        if ($qq->num_rows > 0) {
                            $res_inv[] = $qq->row_array()['refNumber'];
                        }
                    }
                    $result['invoice_no'] = implode(',', $res_inv);
                }

                $ref_amt = 0;

                if ($result['transactionID'] != "" && strtoupper($result['transactionType']) != strtoupper('Offline Payment')) {

                    $qr = $this->db->query('Select sum(refundAmount)as refundAmount from tbl_customer_refund_transaction where creditTransactionID="' . $result['transactionID'] . '"  group by creditTransactionID');

                    $data = $qr->row_array();

                    if (!empty($data)) {
                        $result['partial'] = $data['refundAmount'];
                    } else {
                        $ref_amt = $this->getPartialNewAmount($result['transactionID']);

                        $result['partial'] = $ref_amt;

                    }

                } else {
                    $result['partial'] = $ref_amt;
                }

                $res[] = $result;
            }
        }

        //print_r($res); die;
        return $res;
    }

    public function get_subscriptions_plan_data($userID)
    {
        $cardDb = getenv('DB_DATABASE2');

        $this->db->select('sbs.planID,sbs.subscriptionID,sbs.startDate,sbs.subscriptionAmount, sbs.customerID,sbs.nextGeneratingDate, sbs.merchantDataID,cust.fullName, cust.companyName, tmg.*,  spl.planName as sub_planName, c.customerCardfriendlyName');
        $this->db->from('tbl_subscriptions_xero sbs');
        $this->db->join('tbl_subscriptions_plan_xero spl', 'spl.planID = sbs.planID', 'INNER');
        $this->db->join('Xero_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join("$cardDb.customer_card_data c", 'c.CardID=sbs.cardID', 'left');
        $this->db->where("sbs.merchantDataID ", $userID);
        $this->db->where("cust.merchantID ", $userID);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return [];
    }

    public function get_total_subscription($merchID)
    {
        $res           = array();
        $today         = date("Y-m-d H:i");
        $next_due_date = date('Y-m', strtotime("+30 days"));

        $this->db->select('*');
        $this->db->from('tbl_subscriptions_xero sbs');
        $this->db->join('Xero_custom_customer cust', 'sbs.customerID=cust.Customer_ListID AND cust.merchantID = sbs.merchantDataID', 'inner');
        $this->db->where('cust.merchantID', $merchID);
        $this->db->where('sbs.subscriptionStatus', "1");
        // $this->db->where('cust.customerStatus', "true");
        $this->db->where('DATE_FORMAT(sbs.startDate, "%Y-%m")>=', date("Y-m"));

        $this->db->group_by('sbs.customerID');
        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            $res['total_subcription'] = $query->num_rows();
        } else {
            $res['total_subcription'] = 0;
        }

        $query = $this->db->query(" SELECT count(*) as sub_count  FROM (`tbl_subscriptions_xero` sbs)
        INNER JOIN `Xero_custom_customer` cust ON `sbs`.`customerID` = `cust`.`Customer_ListID` AND cust.merchantID = sbs.merchantDataID

       WHERE `cust`.`merchantID` = '" . $merchID . "'
       and sbs.subscriptionStatus='1' and DATE_FORMAT(startDate, '%Y-%m')>= '" . date('Y-m') . "'   ");
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            $res['active_subcription'] = $query->row_array()['sub_count'];
        } else {
            $res['active_subcription'] = 0;
        }

        $today_date   = date("Y-m-d");
        $next_7_date  = date('Y-m-d', strtotime("+7 days"));
        $next_1_month = date('Y-m-d', strtotime("+1 month"));

        $query1 = $this->db->query("SELECT
                 COUNT(1) AS exp_count
             FROM
             tbl_subscriptions_xero sbs
             INNER JOIN Xero_custom_customer cust ON
                 sbs.customerID = cust.Customer_ListID AND sbs.merchantDataID = cust.merchantID
             WHERE
                 sbs.merchantDataID = '$merchID' AND subscriptionPlan != '0' AND totalInvoice > '0' AND sbs.subscriptionStatus = '1' AND (
                     ( sbs.endDate BETWEEN '$today_date' AND  '$next_1_month' AND invoicefrequency IN ('mon', '2mn', 'qtr', 'six', 'yrl', '2yr', '3yr'))
                     OR
                     ( sbs.endDate BETWEEN '$today_date' AND  '$next_7_date' AND invoicefrequency IN ('dly', '1wk', '2wk'))
         )");

        if ($query1->num_rows() > 0) {

            $res['exp_subcription'] = $query1->row_array()['exp_count'];
        } else {
            $res['exp_subcription'] = 0;
        }

        $query1 = $this->db->query("SELECT
                 COUNT(distinct(sbs.subscriptionID)) AS failed_count
             FROM
                 Xero_test_invoice inv
             INNER JOIN Xero_custom_customer cust ON
                 inv.CustomerListID = cust.Customer_ListID AND cust.merchantID = '$merchID'
             INNER JOIN tbl_subscription_auto_invoices TSAI ON
                 TSAI.invoiceID = inv.invoiceID AND app_type = 1
             INNER JOIN customer_transaction CTR ON
                 CTR.invoiceID = inv.invoiceID AND transactionCode = 300  AND CTR.merchantID = '$merchID'
             INNER JOIN tbl_subscriptions_xero sbs ON TSAI.subscriptionID = sbs.subscriptionID
             WHERE
                 inv.merchantID = '$merchID' AND sbs.merchantDataID = '$merchID' AND inv.IsPaid != 'true' AND inv.userStatus ='Active' GROUP BY sbs.subscriptionID");

        if ($query1->num_rows() > 0) {
            $res['failed_count'] = $query1->row_array()['failed_count'];
        } else {
            $res['failed_count'] = 0;
        }

        return $res;

    }

    public function get_subcription_cron_data()
    {
        $res = array();

        $today = date('Y-m-d');
        $this->db->select('sbs.*, cust.fullName, cust.companyName, spl.planName as sub_planName, tmg.*');
        $this->db->from('tbl_subscriptions_xero sbs');
        $this->db->join('Xero_custom_customer cust', 'sbs.customerID = cust.Customer_ListID', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'sbs.paymentGateway = tmg.gatewayID', 'Left');
        $this->db->join('tbl_subscriptions_plan_xero spl', 'spl.planID = sbs.planID', 'left');
        $this->db->where("sbs.merchantDataID =cust.merchantID ");

        $this->db->where('sbs.nextGeneratingDate', $today);
        $this->db->group_by("sbs.subscriptionID");
        $this->db->order_by("sbs.createdAt", 'desc');

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_cron_subcription_items_data($sID)
    {
        $res = array();

        $today = date('Y-m-d');
        $this->db->select('*');
        $this->db->from('tbl_subscription_invoice_item_xero sbs');
        $this->db->join('xero_test_item xti', 'xti.productID = sbs.itemListID', 'INNER');
        $this->db->where('sbs.subscriptionID', $sID);

        $query = $this->db->get();
        //echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return false;
    }

    public function get_invoice_data_auto_pay($args = [])
    {
        $res   = array();
        $today = date('Y-m-d');

        $this->db->select('inv.*, cust.companyID, cust.firstName, cust.lastName, cust.Customer_ListID,
		tci.cardID, tci.gatewayID, tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,tmg.gatewaySignature,tci.scheduleDate as schedule, tci.scheduleAmount,  mr.merchID as merchantID, mr.resellerID, mr.companyName as mrCompanyName, tci.scheduleID, tci.autoInvoicePay, tci.autoPay, tci.paymentMethod');
        $this->db->from('Xero_test_invoice inv');
        $this->db->join('tbl_scheduled_invoice_payment tci', 'inv.invoiceID =tci.invoiceID AND inv.merchantID = tci.merchantID', 'INNER');
        $this->db->join('Xero_custom_customer cust', 'inv.CustomerListID = cust.Customer_ListID AND inv.merchantID = cust.merchantID', 'INNER');
        $this->db->join('tbl_merchant_gateway tmg', 'tci.gatewayID = tmg.gatewayID', 'INNER');
        $this->db->join('tbl_merchant_data mr', 'mr.merchID= inv.merchantID', 'inner');
        $this->db->where("(DATE_FORMAT(tci.scheduleDate,'%Y-%m-%d')='$today' OR (DATE_FORMAT(inv.DueDate,'%Y-%m-%d') ='$today' or tci.scheduleDate IS NULL))");
        $this->db->where('inv.IsPaid', 'false');
        $this->db->where("inv.BalanceRemaining != ", '0.00');
        $this->db->where('( tci.autoPay = 1 OR tci.autoInvoicePay = 1 )');
        if (isset($args['scheduleID']) && !empty($args['scheduleID'])) {
            $this->db->where('tci.scheduleID', $args['scheduleID']);
        }
        $this->db->where('tci.isProcessed', '0');
        $this->db->where('mr.isSuspend', '0');
        $this->db->where('mr.isDelete', '0');
        $this->db->where('mr.isEnable', '1');
        $this->db->order_by('inv.xeroinvoiceID', 'DESC');

        $query = $this->db->get();
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {

            return $res = $query->result_array();
        }

        return $res;
    }

    public function getAutoRecurringInvoices()
    {
        $res       = array();
        $today     = date('Y-m-d');
        $yesterday = date('Y-m-d', strtotime('-1 day', strtotime($today)));
        $todayDate = date('d');
        $lastDay   = date('t');

        $autoCondition = "(
            ( QTI.DueDate = '$today' AND TRP.optionData = 1)
            OR ( QTI.TimeCreated LIKE '$today%' AND TRP.optionData = 2)
            OR ( QTI.TimeCreated LIKE '$yesterday%' AND TRP.optionData = 3)
            OR ( ( TRP.month_day = $todayDate OR ( TRP.month_day > '$lastDay' AND '$lastDay' = '$todayDate') ) AND TRP.optionData = 4)
        )";
        $sql = "SELECT QTI.*, QCC.companyID, QCC.firstName, QCC.lastName, QCC.userEmail,QCC.companyName,QCC.phoneNumber as Phone, QCC.Customer_ListID, QCC.fullName, TMG.gatewayUsername, TMG.gatewayPassword, TMG.gatewaySignature,TMG.gatewayMerchantID, TMG.gatewayType, TRP.cardID, TRP.recurring_send_mail, TMG.gatewayID, QT.accessToken, QT.refreshToken, QT.realmID, TMD.resellerID,TMD.merchID
        FROM Xero_test_invoice QTI
        INNER JOIN tbl_recurring_payment TRP ON TRP.merchantID = QTI.merchantID AND TRP.customerID = QTI.CustomerListID
        INNER JOIN tbl_merchant_data TMD ON TMD.merchID = QTI.merchantID AND TMD.isSuspend = 0 AND TMD.isDelete = 0 AND TMD.isEnable = 1
        INNER JOIN Xero_custom_customer QCC ON QCC.merchantID = QTI.merchantID AND QCC.Customer_ListID = QTI.CustomerListID
        INNER JOIN tbl_merchant_gateway TMG ON TRP.merchantID = TMG.merchantID AND TMG.set_as_default = 1
        LEFT JOIN tbl_subscription_auto_invoices TSAI ON QTI.invoiceID = TSAI.invoiceID AND TSAI.app_type = 1 AND TSAI.invoiceID = null
        WHERE QTI.BalanceRemaining <= TRP.amount AND QTI.BalanceRemaining > 0 AND ($autoCondition)
         GROUP BY QTI.id";

        $query = $this->db->query($sql);
        // echo $this->db->last_query(); die;
        if ($query->num_rows() > 0) {
            return $res = $query->result_array();
        }
        return $res;

    }

    public function get_all_customer_count($mid)
    {
        $res = array();
        $this->db->select('*');
        $this->db->from('Xero_custom_customer cus ');
        $this->db->where('cus.merchantID', $mid);

        $column1 = array('cus.fullName', 'cus.firstName', 'cus.lastName', 'cus.userEmail', 'cus.phoneNumber');
        $i       = 0;
        $con     = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $query = $this->db->get();
        $cnt   = $query->num_rows();
        return $cnt;

    }

    public function get_all_customer_details($mid)
    {
        $res = array();
        $this->db->select('cus.*, (select sum(BalanceRemaining)  from 	Xero_test_invoice where CustomerListID = cus.Customer_ListID and merchantID="' . $mid . '" and userStatus!="1") as Balance ');
        $this->db->from('Xero_custom_customer cus ');
        $this->db->where('cus.merchantID', $mid);
        $column1 = array('cus.fullName', 'cus.firstName', 'cus.lastName', 'cus.userEmail', 'cus.phoneNumber');
        $i       = 0;
        $con     = '';
        foreach ($column1 as $item) {

            if ($_POST['search']['value']) {

                if ($i === 0) {
                    $con .= "(" . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';
                } else {
                    $con .= ' OR ' . $item . ' Like ' . '"%' . $_POST['search']['value'] . '%"';

                }

            }

            $column[$i] = $item;
            $i++;
        }

        if ($con != '') {
            $con .= ' ) ';
            $this->db->where($con);
        }
        $column = $_POST['order'][0]['column'];
        if ($column == 0) {
            $orderName = 'cus.fullName';
        } elseif ($column == 1) {
            $orderName = 'cus.firstName';
        } elseif ($column == 2) {
            $orderName = 'cus.userEmail';
        } elseif ($column == 3) {
            $orderName = 'cus.phoneNumber';
        } elseif ($column == 4) {
            $orderName = 'Balance';
        } else {
            $orderName = 'cust.firstName';
        }
        $orderBY = $_POST['order'][0]['dir'];

        if ($_POST['length'] != -1) {
            $this->db->limit($_POST['length'], $_POST['start']);
        }

        $query = $this->db->order_by($orderName, $orderBY)->get();
        if ($query->num_rows() > 0) {
            $data = array('num' => '10000', 'result' => $query->result_array());
        } else {
            $data = array('num' => '0', 'result' => $query->result_array());
        }

        return $data;
    }
}
