<?php


Class Fb_company_model extends CI_Model
{
    
    public function get_invoice_data_by_due($condion){
		
		$res =array();
	    
		
		$this->db->select("`inv`.CustomerListID,`inv`.CustomerFullName, cust.firstName, cust.lastName, `cust`.userEmail,`cust`.fullName,`cust`.companyName, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.Customer_ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
		
         $this->db->where($condion);
        $this->db->group_by('inv.CustomerListID');
		$this->db->limit(10);
		$this->db->order_by('balance','desc');
	   
		$query = $this->db->get();
      	
		return $res = $query->result_array();
		return $res;
	
	} 	
	
	
		public function get_transaction_failure_report_data($userID){
	    
	    	$res =array();
	    
	    $sql ="SELECT `tr`.`id`, `tr`.`invoiceID`,  `tr`.`transactionID`, `tr`.`transactionStatus`, `tr`.`customerListID`, `tr`.`transactionCode`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`,(select refNumber from Freshbooks_test_invoice where invoiceID = tr.invoiceID limit 1) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `Freshbooks_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '$userID' AND transactionCode NOT IN ('200','1','100','111') ";
		
		$query = $this->db->query($sql); 
		if($query->num_rows() > 0){
		 return $res= $query->result_array();
		}
	     return  $res;
	}
	
	public function get_invoice_data_by_past_due($userID){
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,  `inv`.invoiceID, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != '0.00' and 
	  
	  `inv`.`merchantID` = '$userID' and  `cust`.`merchantID` = '$userID'   order by  BalanceRemaining   desc limit 10 ");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	}
	
	public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,  `inv`.invoiceID, 
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and userStatus ='' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00'  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and BalanceRemaining != '0.00' and 
	  `inv`.`merchantID` = '$userID'  order by  DueDate   asc limit 10 ");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
		$res =array();
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
     	$this->db->select('tr.*, cust.*, (select RefNumber from Freshbooks_test_invoice where invoiceID= tr.invoiceID and CustomerListID = cust.customer_ListID and  merchantID="'.$userID.'" ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		$this->db->where('cust.merchantID',$userID);	
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		return $res = $query->result_array();
	
		}
			return $res;
	} 
	
	public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0' ";
		
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  inv.invoiceID, `inv`.RefNumber, (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, `inv`.TimeCreated, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'   then 'Success'
	  else 'Canceled' end ) as status 

	  FROM Freshbooks_test_invoice inv 

	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   
	  
	  `inv`.`merchantID` = '$userID'  $con order by  DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  inv.RefNumber, inv.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,  cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != '0.00' then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = '0.00'  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != '0.00' and  
	  inv.merchantID = '$userID'  order by   DueDate  asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 



      public function get_invoice_data_count($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where($condition);
	    $this->db->where("BalanceRemaining != 0.00");
		
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num;
	
	}
	

   
    
    public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
		$this->db->select("cust.fullName as label, inv.DueDate, inv.invoiceID,  inv.refNumber, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
        $this->db->group_by('cust.Customer_ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(10);
	    if($status=='1'){
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '".$today."'  AND `BalanceRemaining` != '' ");
	    }else{
	        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d')  AND `BalanceRemaining` != ''  ");
	    } 
  		 $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}


    public function get_oldest_due($userID)
    {
		     
        $res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT cust.FullName, `inv`.invoiceID,  `inv`.refNumber, (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.FullName,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND BalanceRemaining !='0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and BalanceRemaining ='0' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM Freshbooks_test_invoice inv 
	  INNER JOIN `Freshbooks_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE  BalanceRemaining !='0.00' and 
	  
	  `inv`.`merchantID` = '$userID'  and  cust.merchantID='$userID' limit 10 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
		
	}


       public function get_company_invoice_data_payment($merchantID){
		$res = array();
		
		
		
		 $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID 
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status!="success") or tr.transaction_user_status IS NULL)   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'" ');
		
		
		 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status!="success") or tr.transaction_user_status IS NULL)  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'"  ');
		    
		     if($rquery->num_rows()>0)
             {   
		    
		       $res['total_amount'] = $rquery->row_array()['total'];     
              
               
                $res['refund_amount'] =  $rfquery->row_array()['rm_amount'] ;
             }else{
                  $res['total'] =0; 
                
                 $res['refund'] =0;
                 
             }

	    return $res;
	} 
	
	   

		public function get_process_trans_count($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('*');
		$this->db->from('customer_transaction tr');
	    $this->db->where("tr.merchantID ", $user_id);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->num_rows();  
        }
        else
        {
           return $query->num_rows();
        }
     } 
     
	 
     
    
    
public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		$num =0;
	
	   $sql ="SELECT `inv`.* FROM (`Freshbooks_test_invoice` inv) INNER JOIN `customer_transaction` tr ON `tr`.`invoiceID` = `inv`.`invoiceID` AND tr.transactionCode = '300' WHERE  `inv`.`merchantID` = '$user_id' ";
	
	  $query = $this->db->query($sql);
	  $num   = $query->num_rows();
		return $num;
	
     }	
     
    
	public function get_paid_invoice_recent($condion){
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
		$this->db->join('customer_transaction trans','trans.invoiceID = inv.invoiceID','INNER');        
		$this->db->order_by('trans.transactionDate','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
	    $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	} 
	
	
		public function chart_general_volume($mID)
    {   
 
    
       $res= array();
		     $months=array();
		    for ($i = 11; $i >=0 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
            }  
    
     foreach($months as $key=> $month)
	 { 
    
  
  $sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  
  	inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
  WHERE tr.merchantID = "'.$mID.'"  AND 
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status !="success")  or tr.transaction_user_status IS NULL)   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join Freshbooks_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status !="success")  or tr.transaction_user_status IS NULL)  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$mID.'" and  tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $row['volume'] = $row['volume']- $rfquery->row_array()['rm_amount'];
            
            $res[] = $row; 
            $res[$key]['Month'] =$month;
        }else{
           
            $res[$key]['volume'] =0.00;
            $res[$key]['Month'] =$month;
        }
     }  
            return $res;
       
    }
   
   
   		public function get_fb_invoice_data_pay($invoiceID, $merchantID)
	{
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('Freshbooks_test_invoice inv');
		$this->db->join('Freshbooks_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
         $this->db->where('inv.merchantID' , $merchantID);
	    $this->db->where('cust.merchantID' , $merchantID);
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}
    
	public function fb_subscription_items($planID, $merchantID){
		$res = array();
		$this->db->select('FBSI.*,FBTI.*, FBTI.Name AS itemFullName');
		$this->db->from('Freshbooks_test_item FBTI');
		$this->db->join('tbl_subscription_plan_item_fb FBSI','FBTI.productID = FBSI.itemListID','INNER');
		$this->db->where('FBTI.merchantID' , $merchantID);
	    $this->db->where('FBSI.planID' , $planID);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			return $res = $query->result_array();
		}
		return $res;
	}
}