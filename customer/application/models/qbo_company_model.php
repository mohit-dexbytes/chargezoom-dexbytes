<?php


Class Qbo_company_model extends CI_Model
{
    
    public function get_invoice_data_by_due($condion){
		
		$res =array();
	    
		$this->db->select("`inv`.CustomerListID,`inv`.CustomerFullName, cust.firstName, cust.lastName, `cust`.userEmail,`cust`.fullName,`cust`.companyName, (case when cust.Customer_ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.Customer_ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
        $this->db->group_by('inv.CustomerListID');
		
		$this->db->order_by('balance','desc');
	   
	    $this->db->where($condion);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	} 	
	
		public function get_transaction_failure_report_data($userID){
	    
	    
	    $res =array();
	    $sql ="SELECT `tr`.`id`, `tr`.`transactionID`, `tr`.`customerListID`,`tr`.`transactionStatus`, `tr`.`transactionCode`,  `tr`.`transactionAmount`, `tr`.`transactionDate`, `tr`.`transactionType`,(select RefNumber from QBO_test_invoice where invoiceID = tr.invoiceID ) as RefNumber, `cust`.`fullName` FROM (`customer_transaction` tr) INNER JOIN `QBO_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID` WHERE `cust`.`merchantID` = '$userID' and tr.merchantID = '$userID'  AND transactionCode NOT IN ('200','1','100','111') ";
		
		$query = $this->db->query($sql); 
		if($query->num_rows() > 0){
		  return $res= $query->result_array();
		}
	     return  $res;
		
	}
	
	public function get_invoice_data_by_past_due($userID)
	{
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,     `inv`.invoiceID,             DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID =inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0 then 'Success'
	  else 'Canceled' end ) as status 
	  FROM QBO_test_invoice inv 
	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `BalanceRemaining` != 0 and 
	  
	  `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   order by  BalanceRemaining   desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return $res= $query->result_array();
		}
	     return  $res;
	
	}
	
	public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.RefNumber,`inv`.invoiceID, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.fullName,cust.userEmail,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and userStatus ='' then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0   then 'Success'
	  else 'Canceled' end ) as status 
	  FROM QBO_test_invoice inv 
	  	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and BalanceRemaining != 0 and 
	  `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   order by  DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res= $query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
		$res =array();
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.*, cust.*, (select RefNumber from QBO_test_invoice where invoiceID= tr.invoiceID and CustomerListID = cust.customer_ListID and  merchantID="'.$userID.'"   ) as RefNumber ');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.customer_ListID','INNER');
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		$this->db->where('cust.merchantID',$userID);
				
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		   return  $res= $query->result_array();
		}
	     return  $res;
		
	} 
	
	public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0' ";
		
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.RefNumber, `inv`.invoiceID,  (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0   then 'Success'
	  else 'Canceled' end ) as status 

	  FROM QBO_test_invoice inv 

	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   
	  
	  `inv`.`merchantID` = '$userID' and cust.merchantID = '$userID'   $con order by  DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 
	
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  inv.RefNumber, `inv`.invoiceID, `inv`.Total_payment,`inv`.BalanceRemaining, `inv`.DueDate,   cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 then 'Scheduled' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `BalanceRemaining` != 0 then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and `BalanceRemaining` = 0  then 'Success'
	  else 'Canceled' end ) as status 
	  FROM QBO_test_invoice inv 
	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `BalanceRemaining` != 0 and  
	  inv.merchantID = '$userID' and cust.merchantID = '$userID'   order by   DueDate  asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 
	
	public function get_qbo_tax_data($uID){
	    $res =array();
	   
		$this->db->select('tax.*,tax_code.taxID as taxRefID');
		$this->db->from('tbl_taxes_qbo tax');
		$this->db->join('tbl_taxe_code_qbo tax_code','tax.taxID = tax_code.taxRef','INNER');	   
	    $this->db->where('tax.merchantID', $uID);
        $this->db->where('tax_code.merchantID', $uID);

		$query = $this->db->get();
      
		$res = $query->result_array();
		return $res;
	}
	
	public function get_subplan_data($planID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT * FROM tbl_subscriptions_plan_qbo INNER JOIN tbl_subscription_plan_item_qbo ON tbl_subscription_plan_item_qbo.planID =  '".$planID."' WHERE tbl_subscriptions_plan_qbo.planID = '".$planID."'");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 	
    
    public function get_terms_data($merchID){
    	$data =array();
	    $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
	   if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	}
	
	public function check_payment_term($name,$netterm,$merchID)
	{
			$query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('".$name."') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('".$netterm."') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
		return $query->num_rows();	
	}
	
	public function chart_general_volume($mID)
    {   
 
    
       $res= array();
		     $months=array();
		    for ($i = 11; $i >=0 ; $i--) {
            $months[] = date("M-Y", strtotime( date( 'Y-m' )." -$i months"));
            }  
    
     foreach($months as $key=> $month)
	 { 
    
  
  $sql = 'SELECT   Month(transactionDate) as Month , sum(tr.transactionAmount) as volume FROM  customer_transaction tr  WHERE tr.merchantID = "'.$mID.'"  AND 
  date_format(tr.transactionDate, "%b-%Y") ="'.$month.'"  and    (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="Refund" or tr.transaction_user_status !="Success")  or tr.transaction_user_status IS NULL)   GROUP BY date_format(transactionDate, "%b-%Y") ' ;
		
			 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join QBO_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "Refund" OR tr.transactionType = "Pay_refund" OR tr.transactionType = "Stripe_refund" OR tr.transactionType = "Credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="Refund" or tr.transaction_user_status !="Success")  or tr.transaction_user_status IS NULL)  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$mID.'" and  tr.merchantID="'.$mID.'"  ');


       $query = $this->db->query($sql);
       

        if($query->num_rows()>0)
        {    
            $row = $query->row_array();
             $row['volume'] = $row['volume']- $rfquery->row_array()['rm_amount'];
            
            $res[] = $row; 
            $res[$key]['Month'] =$month;
        }else{
           
            $res[$key]['volume'] =0.00;
            $res[$key]['Month'] =$month;
        }
     }  
            return $res;
       
    }
    
    public function get_company_invoice_data_payment($merchantID){
		$res = array();
		
		
		
		 $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
		inner join QBO_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID 
		where   (tr.transactionType ="Sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="Pay_sale" or  tr.transactionType ="Prior_auth_capture" or tr.transactionType ="Pay_capture" or  tr.transactionType ="Capture" 
		or  tr.transactionType ="Auth_capture" or tr.transactionType ="Stripe_sale" or tr.transactionType ="Stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="Refund" or tr.transaction_user_status!="Success") or tr.transaction_user_status IS NULL)   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'" ');
		
		
		 $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join QBO_custom_customer cust on cust.Customer_ListID=tr.customerListID 
		inner join tbl_merchant_data mr1 on cust.merchantID=mr1.merchID where 
		(tr.transactionType = "Refund" OR tr.transactionType = "Pay_refund" OR tr.transactionType = "Stripe_refund" OR tr.transactionType = "Credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="Refund" or tr.transaction_user_status!="Success") or tr.transaction_user_status IS NULL)  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   cust.merchantID="'.$merchantID.'" and  tr.merchantID="'.$merchantID.'"  ');
		    
		     if($rquery->num_rows()>0)
             {   
		    
		       $res['total_amount'] = $rquery->row_array()['total'];     
              
               
                $res['refund_amount'] =  $rfquery->row_array()['rm_amount'] ;
             }else{
                  $res['total'] =0; 
                
                 $res['refund'] =0;
                 
             }
	    return $res;
	} 
	
		public function get_process_trans_count($user_id){
     
		$today = date("Y-m-d");
			$this->db->select('*');
		$this->db->from('customer_transaction tr');
	    $this->db->where("tr.merchantID ", $user_id);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') =", $today);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows()>0)
        {
            return $query->num_rows();  
        }
        else
        {
           return $query->num_rows();
        }
     } 
     
      public function get_invoice_data_count($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where($condition);
	    $this->db->where("BalanceRemaining != 0.00");
		
		$query = $this->db->get();
		$num = $query->num_rows();
		return $num;
	
	}
	
	public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		$num =0;
	
	   $sql ="SELECT `inv`.* FROM (`QBO_test_invoice` inv) INNER JOIN `customer_transaction` tr ON `tr`.`invoiceID` = `inv`.`invoiceID` AND tr.transactionCode = '300' WHERE  `inv`.`merchantID` = '$user_id' ";
	
	  $query = $this->db->query($sql);
	  $num   = $query->num_rows();
		return $num;
	
     }	
	
	public function get_paid_invoice_recent($condion){
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.CustomerListID, cust.companyName, (inv.Total_payment) as balance, inv.DueDate, trans.transactionDate, inv.invoiceID ");
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
		$this->db->join('customer_transaction trans','trans.invoiceID = inv.invoiceID','INNER');        
		$this->db->order_by('trans.transactionDate','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and inv.BalanceRemaining ='0' ");
	    $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	}
    
    public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
		$this->db->select("cust.fullName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
        $this->db->group_by('cust.Customer_ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(10);
	    if($status=='1'){
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '".$today."'  AND `BalanceRemaining` != '' ");
	    }else{
	        $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d')  AND `BalanceRemaining` != ''  ");
	    } 
  		 $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}
    public function get_oldest_due($userID){
		     
        $res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT cust.FullName, `inv`.invoiceID, (`inv`.Total_payment) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.Customer_ListID,cust.FullName,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and (select transactionCode from customer_transaction where invoiceID = inv.invoiceID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND BalanceRemaining !='0.00' then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and BalanceRemaining ='0' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM QBO_test_invoice inv 
	  INNER JOIN `QBO_custom_customer` cust ON `inv`.`CustomerListID` = `cust`.`Customer_ListID`
	  WHERE  BalanceRemaining !='0.00' and 
	  
	  `inv`.`merchantID` = '$userID'  and  cust.merchantID='$userID' limit 10 ");
	  
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
		
	}
	  public function get_plan_data($userID){
		
	$result =array();
	    
		$this->db->select('qb_item.Name as Name, qb_item.FullName as FullName,qb_item.Parent_FullName as Parent_FullName,qb_item.Type as Type, qb_item.SalesPrice as SalesPrice, qb_item.ListID as ListID,qb_item.EditSequence as EditSequence, Discount, qb_item.IsActive ,  (case when qb_item.IsActive="true" then "Done"  when qb_item.IsActive="false" then "Done" else "Done" end) as c_status   ');
		$this->db->from('qb_test_item qb_item');
	    $this->db->join('tbl_company comp','comp.id = qb_item.companyListID','INNER');
	   
	    $this->db->where('comp.merchantID',$userID);
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
        
        }
       
       return $result;
      }
      
      
 
		public function get_qbo_invoice_data_pay($invoiceID, $merchantID)
	{
		$res = array();
	
		$this->db->select('inv.*,cust.*');
		$this->db->from('QBO_test_invoice inv');
		$this->db->join('QBO_custom_customer cust','inv.CustomerListID = cust.Customer_ListID','INNER');
	    $this->db->where('inv.invoiceID' , $invoiceID);
         $this->db->where('inv.merchantID' , $merchantID);
	    $this->db->where('cust.merchantID' , $merchantID);
		$query = $this->db->get();

		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	}
	
}