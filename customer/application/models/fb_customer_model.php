<?php


Class Fb_customer_model extends CI_Model
{
	
  	
public function get_customer_invoice_data_sum($custID, $uid){
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,   sum(inv.BalanceRemaining) as amount FROM Freshbooks_test_invoice inv  WHERE `inv`.`CustomerListID` = '".$custID."'   and inv.merchantID='".$uid."' ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}



    public function customer_by_id($id) 
	{
         $this->db->select('cus.*,cmp.*');
		
		$this->db->from('Freshbooks_test_invoice cus');
	    $this->db->join('Freshbooks_custom_customer cmp','cmp.Customer_ListID = cus.CustomerListID','inner');
		$this->db->where('cus.CustomerListID', $id);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
	}


public function get_invoice_upcomming_data($userID, $mid){
	$res =array();
		$today  = date('Y-m-d');

		$query  =  $this->db->query("SELECT * FROM Freshbooks_test_invoice WHERE CustomerListID = $userID  and merchantID=$mid  AND BalanceRemaining != 0");

 	if($query->num_rows() > 0){
		  return  $res=$query->result_array();
		}
   
    return  $res;
}

public function get_invoice_latest_data($userID, $mid){

	$res =array();
 	
 	$today  = date('Y-m-d');
   
 		$query  =  $this->db->query("SELECT *,(SELECT transactionDate FROM customer_transaction WHERE Freshbooks_test_invoice.Total_payment = customer_transaction.transactionAmount AND    customer_transaction.CustomerListID = $userID AND customer_transaction.transactionStatus != 'Failure' LIMIT 1) AS transactionDate FROM Freshbooks_test_invoice WHERE (BalanceRemaining = 0 or  BalanceRemaining!= Total_payment) AND CustomerListID = $userID   and Freshbooks_test_invoice.merchantID=$mid  ");  

	 	if($query->num_rows() > 0){
			  return  $res=$query->result_array();
			}
	   
	    return  $res;
	    
 
 	}


 	public function get_customer_invoice_data_payment($custID, $mid){
$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  ifnull(sum(Total_payment),0) as applied_amount,  
		  ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '".$custID."' and (BalanceRemaining!=0  and Total_payment!=BalanceRemaining) and merchantID=$mid ),'0')as applied_due, 
ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from Freshbooks_test_invoice where `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0') as upcoming_balance FROM Freshbooks_test_invoice   WHERE `CustomerListID` = '".$custID."'  and merchantID=$mid  and BalanceRemaining=0   ");

		 $res=$query->row();
		 return  $res;
		
	}
 	
	 public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
    public function customer_details($custID){
        
         $this->db->select('*');
		
		$this->db->from('Freshbooks_custom_customer cus');
		$this->db->where('Customer_ListID', $custID);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
    }
	
	function get_customers_data($merchantID) 
	{
		 $sql = 'SELECT * from Freshbooks_custom_customer WHERE merchantID = "'.$merchantID.'"';
		 

		$query = $this->db->query($sql);

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}
	
	public function get_transaction_data_refund($userID){
		$today = date("Y-m-d H:i");
		   $query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr 
		inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where tr.merchantID='".$userID."' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL)   and tr.transactionID!='' GROUP BY tr.transactionID DESC");
   
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	


	public function get_transaction_data_captue($userID){
		
		$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		 and ( (tr.transaction_user_status !='success' and tr.transaction_user_status !='refund') or tr.transaction_user_status IS NULL ) GROUP BY id ");
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
	
	
		public function get_transaction_data_erefund($userID){
		$today = date("Y-m-d H:i");
		 $res=array();
        $query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr 
		inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and ((tr.transaction_user_status !='success' and tr.transaction_user_status !='refund')  or  tr.transaction_user_status  IS NULL)   and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck') and tr.transactionID!='' GROUP BY tr.transactionID DESC");

		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
	
	
		public function get_transaction_data_ecaptue($userID){
		
		
	$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  Freshbooks_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
	 and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck')  and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL) 
		   GROUP BY id ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
	
	
	public function get_subscription_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
	
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		$query = $this->db->get();
     
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	public function get_transaction_data($userID){
	    	$res = array();
				$today = date("Y-m-d H:i");
		$this->db->select('tr.*, cust.*');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
     
    	if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 foreach( $res1 as $result)
		 {
		     if(!empty($result['invoiceID']))
		     {
		         $inv =$result['invoiceID'];
		      $res_inv=  $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  Freshbooks_test_invoice where invoiceID IN($inv) and  merchantID=$userID  GROUP BY merchantID ")->row_array()['invoce'];
		      $result['invoiceID'] =$res_inv;
		     }
		     $res[] =$result;
		 } 
		}
		return $res;
		
	}
	
		public function get_refund_transaction_data($userID){
		$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `Freshbooks_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' and cust.merchantID= '$userID'   AND (`tr`.`transactionType` = 'refund' or `tr`.`transactionType` = 'stripe_refund'  OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;

		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*,sum(cr.creditAmount) as balance, cust.companyName, cust.fullName');
		$this->db->from('tbl_credits cr');
		$this->db->join('Freshbooks_custom_customer cust','cr.creditName = cust.Customer_ListID','INNER');
	    $this->db->where($con);
		$this->db->group_by('cr.creditName');
		$this->db->order_by('cr.creditDate','desc');
		$query = $this->db->get();

		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
      
    public function get_customer_note_data($custID, $mID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `Freshbooks_custom_customer` cust ON `pr`.`customerID` = `cust`.`Customer_ListID` WHERE `pr`.`customerID` = '".$custID."'  and pr.merchantID='".$mID."' order by pr.noteID desc");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 
	
	public function get_subscriptions_data($condition){
	
			$this->db->select('sbs.*, cust.fullName, cust.companyName, tmg.*');
		$this->db->from('tbl_subscriptions_fb sbs');
		$this->db->join('Freshbooks_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		
	    $this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);

	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
     
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	function get_billing_data($merchID) 
	{
		 $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "'.$merchID.'" ';
		 

		$query = $this->db->query($sql);
		
		if($query->num_rows()>0 ) {
				return $query->result_array();
			   
			} else {
				return false;
			}	

		
	}
	
	public function get_merchant_billing_data($invID){
		   $res = array();
		   $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");
		   
		   
		   if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return $res;
			}
		
		   
	   }
		public function get_terms_data($merchID){
        
        $data=array();
	    $query = $this->db->query("SELECT *, dpt.id as dfid, pt.id as pid FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID UNION SELECT *, dpt.id as dfid, pt.id as pid  FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.merchantID = $merchID ORDER BY dfid IS NULL, dfid asc");
	   if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	}
	
	public function check_payment_term($name,$netterm,$merchID)
	{
	$query = $this->db->query("SELECT * FROM `tbl_default_payment_terms` dpt LEFT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID AND pt.merchantID = $merchID WHERE dpt.name IN ('".$name."') AND pt.pt_netTerm IS NULL OR dpt.netTerm IN ('".$netterm."') AND pt.pt_netTerm IS NULL OR pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 UNION SELECT * FROM `tbl_default_payment_terms` dpt RIGHT JOIN `tbl_payment_terms` pt ON dpt.id = pt.termID WHERE pt.pt_name IN ('".$name."') AND pt.merchantID = $merchID  AND pt.enable != 1 OR pt.pt_netTerm IN ('".$netterm."') AND pt.merchantID = $merchID  AND pt.enable != 1 AND pt.merchantID = $merchID AND pt.enable != 1");
		return $query->num_rows();	
	}   

    
	function update_refund_payment($tr_id, $gateway)
	{
	    
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="refund" where (transactionID="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	  
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}



        
           public function get_invoice_transaction_data($invoiceID, $userID)
	   {
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	
	    $this->db->where("cust.merchantID ", $userID);
		 $this->db->where("tr.invoiceID ", $invoiceID);
		  $this->db->where("tr.merchantID ", $userID);
		 $this->db->group_by('transactionID');
		
		
		$query = $this->db->get();
    
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	
		public function get_customer_transaction_data($custID,$userID){
				$today = date("Y-m-d H:i");
					$res = array();
		$this->db->select('tr.*, cust.*');
		$this->db->from('customer_transaction tr');
		$this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $userID);
	     $this->db->where("cust.merchantID ", $userID);
	      $this->db->where("cust.Customer_ListID ", $custID);
	       $this->db->where("tr.customerListID ", $custID);
	    $this->db->order_by("tr.transactionDate", 'desc');
		
		$query = $this->db->get();
     
		if($query->num_rows() > 0)
		{
		
		 $res1= $query->result_array();
		 foreach( $res1 as $result)
		 {
		     if(!empty($result['invoiceID']))
		     {
		         $inv =$result['invoiceID'];
		      $res_inv=  $this->db->query(" Select GROUP_CONCAT(refNumber SEPARATOR ', ') as invoce  from  Freshbooks_test_invoice where invoiceID IN($inv) and CustomerListID=$custID and  merchantID=$userID  GROUP BY merchantID ")->row_array()['invoce'];
		      $result['invoiceID'] =$res_inv;
		     }
		     $res[] =$result;
		 } 
		}
		return $res;	
	}






}