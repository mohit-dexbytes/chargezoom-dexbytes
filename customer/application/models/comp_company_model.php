<?php
Class Comp_company_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
		
    
	public function get_transaction_data($userID){
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->where("cust.companyID ", $userID);
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	public function get_invoice_data_pay($invoiceID)
	{
		$res = array();
	
		$this->db->select('inv.*, cust.*, cmp.merchantID, cmp.qbwc_username');
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
      
	    $this->db->where('inv.TxnID' , $invoiceID);
	    $this->db->where('cust.customerStatus', '1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		}
		
		return $res;
	} 		
    
    
	
	public function get_invoice_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.*, 
	  ( case   when  IsPaid ='true' and userStatus =''    then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false'  then 'Upcoming' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''   then 'Past Due'
	   
	  else 'Cancel' end ) as status FROM chargezoom_test_invoice inv
	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  
	  inner join tbl_company t_comp on t_comp.id = cust.companyID
	  WHERE  t_comp.merchantID = '$userID'      ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_invoice_upcomming_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.AppliedAmount,`inv`.BalanceRemaining, `inv`.DueDate,   cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  and userStatus ='' then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM chargezoom_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  inner join tbl_company t_comp on t_comp.id = cust.companyID	  
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and  
	  t_comp.merchantID = '$userID'   order by   DueDate  asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	

	
	public function get_invoice_latest_data($userID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false'  and userStatus ='' then 'Past Due'
	
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 

	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   inner join tbl_company t_comp on t_comp.id = cust.companyID	
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  t_comp.merchantID = '$userID'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	
	public function get_invoice_data_by_id($invID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount,`inv`.BalanceRemaining, 
	  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	  


	  FROM chargezoom_test_invoice inv 

	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  and 
	  
	  `cust`.`companyID` = '$userID'   order by   DATE_FORMAT(inv.TimeModified, '%d-%m-%Y %l.%i%p')  desc limit 10 ");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	
	
	public  function  get_invoice_item_data ($invoiceID){
		$res =array();
		
   $sql = "Select itm.*, qb_item.* from  qb_test_invoice_lineitem  itm  inner join qb_test_item qb_item on  itm.Item_ListID = qb_item.ListID where  itm.TxnID = '$invoiceID' ";	
		
	
		$query = $this->db->query($sql);
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
		
		
	} 		
	
	
	
	
	
	
	
	
		public function get_company_invoice_data_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as incount  FROM chargezoom_test_invoice inner join chargezoom_test_customer cust  on cust.ListID = chargezoom_test_invoice.Customer_ListID  inner join tbl_company tcom on tcom.id=cust.companyID  WHERE tcom.merchantID = '".$compID."' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->incount;
		}
		return false;
	     
	}	
	
	
		public function get_company_customer_count($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT count(*) as cust_count  FROM chargezoom_test_customer   inner join tbl_company tcom on tcom.id=chargezoom_test_customer.companyID  WHERE tcom.merchantID = '".$compID."' ");
		if($query->num_rows() > 0){
		 $res=$query->row();
		 return  $res->cust_count;
		}
		return false;
	}	
	
	
	    public function get_company_invoice_data_payment($compID){
		$res = array();
		  $query  =  $this->db->query("SELECT   sum(-AppliedAmount) as net_amount, 
		  (select sum(-AppliedAmount) from chargezoom_test_invoice where userStatus='') as applied_amount,
		  (select count(*) from chargezoom_test_invoice where AppliedAmount!='0.00'   ) as tri_inv_count,
		  (SELECT  sum(-AppliedAmount) from chargezoom_test_invoice where  MONTH(CURDATE()) = MONTH(DueDate) AND YEAR(CURDATE()) = YEAR(DueDate)   )  as recent_pay 
		  FROM chargezoom_test_invoice   inner join chargezoom_test_customer cust  on cust.ListID = chargezoom_test_invoice.Customer_ListID   inner join tbl_company tcom on tcom.id=cust.companyID  WHERE tcom.merchantID = '".$compID."' ");
		if($query->num_rows() > 0){
		
		return  $res=$query->row();
		
		}
	    return false;
	}	
	

	
	
	
	   public function get_invoice_data_count($condition){
		
		$num =0;
	    
		$this->db->select('inv.* ');
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company cmp','cmp.id = cust.companyID','INNER');
	    $this->db->where($condition);
		
		$query = $this->db->get();
  
		$num = $query->num_rows();
		return $num;
	
	} 		
	
	   public function get_invoice_data_count_failed($user_id){
		$today =date('Y-m-d');
		$num =0;
	
	$sql ="SELECT `inv`.* FROM (`chargezoom_test_invoice` inv) INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID` WHERE DATE_FORMAT(inv.DueDate,'%Y-%m-%d') < '$today' AND `IsPaid` = 'false' AND `userStatus` = '' AND `cust`.`companyID` = '$user_id'   and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'   ";
	
	$query = $this->db->query($sql);
	$num = $query->num_rows();
		return $num;
	
}	
	public function get_piechart_due_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM chargezoom_test_invoice inv inner join chargezoom_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate < CURDATE( ) and cust.companyID = '$coID' and IsPaid='false' ";
	

		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;

		return $num;
	
	}	
	
	
   
	public function get_piechart_30_data($coID){
		$num =0;
	$sql = "SELECT count(*) as inv_count FROM chargezoom_test_invoice inv inner join chargezoom_test_customer cust on inv.Customer_ListID = cust.ListID WHERE inv.DueDate >= DATE_SUB(CURDATE(), INTERVAL 29 Day) and cust.companyID = '".$coID."' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;

		return $num;
	
	}	
	
	
	public function get_piechart_60_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM chargezoom_test_invoice inv inner join chargezoom_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 59 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 30 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
	
	public function get_piechart_80_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM chargezoom_test_invoice inv inner join chargezoom_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate BETWEEN DATE_SUB( CURDATE( ) ,INTERVAL 89 Day ) AND DATE_SUB( CURDATE() ,INTERVAL 60 Day ) and cust.companyID = '$coID' and IsPaid='false' ";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
    
	public function get_piechart_90_data($coID){
	$num =0;
 	$sql = "SELECT count(*) as inv_count FROM chargezoom_test_invoice inv inner join chargezoom_test_customer cust on inv.Customer_ListID = cust.ListID where inv.DueDate <= DATE_SUB( CURDATE() ,INTERVAL 90 Day ) and cust.companyID = '$coID'  and IsPaid='false'";
		
		 $query1 = $this->db->query($sql);
	     $res=$query1->row();
		  $num = $res->inv_count;
		return $num;
	
	}	
	
	
		public function get_chart_data($coID){
		
		
    $query = $this->db->query("call get_month_sum_data('".$coID."')	");		
		
	
	if($query->num_rows() > 0){
		
		  $res=$query->result_array();
		  $chart_arr = array();
		  
		 
		  foreach($res as $chart){
			  
			   $chart_arr['earn_amount'][] = $chart['earn'];
			   $chart_arr['month_total_invoice'][]   = $chart['month_total_invoice'];
			   $chart_arr['inv_month'][]   = $chart['tDate'];
			  
		  }
		 return  $chart_arr; 
		}
		
	}	
	
    
	
	   public function get_plan_data(){
		
		$result =array();
	    
		$this->db->select('qb_item.* ');
		$this->db->from('qb_test_item qb_item');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			$result = $query->result_array();
		}
		return $result;
	
	} 		
	
	
	
	   public function get_Failure_paymemt_data($coID){
		
		$percent =0;
	    
		$sql = "Select count(*) as total_pay , (select count(*) from tbl_customer_tansaction where transactionType='auto-nmi' or transactionType='sale' or transactionType='capture' and transactionCode='100' ) as success_pay, (select count(*) from tbl_customer_tansaction where (transactionType='auto-nmi' or transactionType='sale' or transactionType='capture') and transactionCode !='100' ) as failed_pay from tbl_customer_tansaction inner join qb_test_customer cust on cust.ListID= tbl_customer_tansaction.customerListID inner join tbl_company tcom on tcom.id=cust.companyID  WHERE tcom.merchantID = '".$coID."' ";
		$query = $this->db->query($sql);		
		
      
	     $res=$query->row();
		 $total_pay   = $res->total_pay;
		$success_pay = $res->success_pay;
		 $failed_pay  = $res->failed_pay; 
         $percent=number_format((100 * $failed_pay) / $success_pay, 2) ;  
		
		return $percent;
	
	
	} 	
	
	
	
	
	

    
	   public function get_invoice_data_by_due($condion){
		
		$res =array();
	    
		
		$this->db->select("`inv`.Customer_ListID,`inv`.Customer_FullName, cust.FirstName, cust.LastName, `cust`.Contact,`cust`.FullName, (case when cust.ListID !='' then 'Active' else 'InActive' end) as status , `inv`.ShipAddress_Addr1, `cust`.ListID, sum(inv.BalanceRemaining) as balance ");
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('inv.Customer_ListID');
		$this->db->order_by('balance','desc');
	   
	    $this->db->where($condion);
		
		$query = $this->db->get();
      	
		$res = $query->result_array();
		return $res;
	
	} 		
	

     
	   public function get_invoice_data_by_past_due($userID){
		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,                  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM chargezoom_test_invoice inv 
	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') <'$today'  AND `IsPaid` = 'false' and userStatus ='' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'      order by  BalanceRemaining   desc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 		
	
  public function get_invoice_data_by_past_time_due($userID){
		
		$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID,`inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
		    INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today'  AND `IsPaid` = 'false' and userStatus ='' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'   order by  DueDate   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	

	public function get_transaction_failure_report_data($userID){
		
		
		$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where('comp.merchantID ', $userID);
		 $this->db->where('tr.transactionCode !=','100');
		$this->db->where('tr.transactionDate >=', 'DATE_SUB(CURDATE(), INTERVAL 30 Day)');
	
		$query = $this->db->get();
		if($query->num_rows() > 0){
		  return $query->result_array();
		}
		
	} 		
	
	
	
		
  public function get_invoice_data_open_due($userID, $status){
		
		$res =array();
		$con='';
		if($status=='8'){
		 $con.="and `inv`.BalanceRemaining !='0.00' ";
		
		}
		
		
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT  `inv`.RefNumber, `inv`.TxnID,(-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining, DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , cust.*,
	    (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 

	  FROM chargezoom_test_invoice inv 

	  INNER JOIN `chargezoom_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	  INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   
	  
	  `comp`.`merchantID` = '$userID'  $con  order by  DueDate   asc limit 10 ");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	} 	
	
	
	public function get_transaction_report_data($userID, $minvalue, $maxvalue){
		
		$trDate = "DATE_FORMAT(tr.transactionDate, '%Y-%m-%d')"; 
		
		$this->db->select('tr.*, cust.*, (select RefNumber from qb_test_invoice where TxnID= tr.invoiceTxnID ) as RefNumber ');
		$this->db->from('tbl_customer_tansaction tr');
		$this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
	    $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where('comp.merchantID ', $userID);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') >=", $minvalue);
		$this->db->where("DATE_FORMAT(tr.transactionDate, '%Y-%m-%d') <=", $maxvalue);
		
		$query = $this->db->get();
     
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	
	public function get_credit_card_expire($userID){
	
	$res=array();
	
	$sql = "SELECT c.*,`cust`.ListID, `cust`.FullName,`cust`.Contact,`cust`.companyName, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from tbl_customer_card c   INNER JOIN `qb_test_customer` cust ON `c`.`customerListID` = `cust`.`ListID`   where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)  <= DATE_add( CURDATE( ) ,INTERVAL 60 Day ) and `cust`.`companyID` = '$userID' ";  
	
	
	$query = $this->db->query($sql);
	
	if($query->num_rows() > 0){
		
		  $res = $query->result_array();
		}
	return $res;
	
	
	
	
	}
	
	
	
		public function template_data_list($condition){
		
        $res=array();
		$this->db->select('tp.*, typ.* ');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID = tp.templateType','inner');
		$this->db->where($condition);
		
	  	$query = $this->db->get();
		  return $res =  $query->result_array();
		
	} 		
    	
	
		public function template_data($con){
		
	$res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_email_template tp');
		$this->db->join('tbl_teplate_type typ','typ.typeID=tp.templateType','inner');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
		  return $res =  $query->row_array();
		
	} 		
    	
	   public function get_invoice_due_by_company($condion,$status){
		
		$res =array();
	    $today  = date('Y-m-d');
		$this->db->select("cust.companyName as label, inv.DueDate,  sum(inv.BalanceRemaining) as balance ");
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->group_by('cust.ListID');
		$this->db->order_by('balance','desc');
		$this->db->limit(10);
		if($status=='1'){
	      $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') !=''  AND `IsPaid` = 'false' and userStatus ='' ");
		}
		if($status=='0'){ 

			$this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < $today AND `IsPaid` = 'false' and userStatus ='' ");
		}
	      
  		  $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	} 		
	
	
	
	   public function get_paid_invoice_recent($condion){
		
		$res =array();
	    $today  = date('Y-m-d');

		$this->db->select("cust.FullName,inv.RefNumber,inv.TxnID,inv.Customer_ListID, cust.companyName, (-inv.AppliedAmount) as balance, inv.DueDate, inv.TimeModified ");
		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        
		$this->db->order_by('TimeModified','desc');
		$this->db->limit(10);
	    $this->db->where("DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >$today  and userStatus ='' and inv.AppliedAmount !='0.00' ");
	    $this->db->where($condion);
		
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	
	} 		
	
	
	public function get_oldest_due($userID){
		     

		
		$res =array();
 	      $today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.TxnID, inv.Customer_FullName, cust.companyName, `inv`.RefNumber, (-`inv`.AppliedAmount) as AppliedAmount, inv.DueDate, `inv`.BalanceRemaining,                  DATE_FORMAT(inv.TimeModified,'%d-%m-%Y %l.%i %p') as TimeModifiedinv , inv.TimeCreated, cust.ListID,cust.FullName,cust.Contact,
	   (case when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus ='' then 'Upcoming' 
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus =''  then 'Past Due'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' and IsPaid ='true'  and userStatus ='' then 'Success'
	  else 'Canceled' end ) as status 
	  FROM qb_test_invoice inv 
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`
	   INNER JOIN `tbl_company` comp ON `comp`.`id` = `cust`.`companyID`
	  WHERE   DATE_FORMAT(inv.DueDate, '%Y-%m-%d') > '$today'  AND `IsPaid` = 'false' and userStatus ='' and BalanceRemaining !='0.00' and 
	  
	  `comp`.`merchantID` = '$userID'      order by  inv.TimeModified   asc limit 10 ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	
	
		
	}
	
	
		
		public function create_invoice($inv_data, $items)
		{
		    
		       
		       	$ins_Daat =array(
                                    			'Customer_FullName'=>$inv_data['Customer_FullName'],
                                    			'Customer_ListID'=>$inv_data['Customer_ListID'],
                                    			'TxnID' => $inv_data['TxnID'],
                                    			'RefNumber'=>$inv_data['RefNumber'],
                                    			'TimeCreated'=> $inv_data['TimeCreated'],
                                    			'TimeModified'=>$inv_data['TimeModified'],
                                    			'DueDate'   => $inv_data['DueDate'],
                                    			'ShipAddress_Addr1'=>$inv_data['ShipAddress_Addr1'],
                                    			'ShipAddress_Addr2'=>$inv_data['ShipAddress_Addr2'],
                                    			'ShipAddress_City'=>$inv_data['ShipAddress_City'],
                                    			'ShipAddress_Country'=>$inv_data['ShipAddress_Country'],
                                    			'ShipAddress_State'=>$inv_data['ShipAddress_State'],
                                    			'ShipAddress_PostalCode'=>$inv_data['ShipAddress_PostalCode'],
                                    			'IsPaid'				=>'true',	
                                    			'insertInvID'=>$inv_data['insertInvID'],
                                    			'invoiceRefNumber'=>$inv_data['invoiceRefNumber'],
                                                'AppliedAmount'=>(-$inv_data['BalanceRemaining']),
                                                 'userStatus'=>'Active',
                                                 'BalanceRemaining'=>0
                                    		
                                    			);
                                    			
                                                 $this->db->insert('qb_test_invoice',	$ins_Daat) ;                   			
                                                foreach($items as $k=>$item_data)
                            					{

                            							$insert_row['TxnID'] =$inv_data['TxnID'];
                            							$lineID = mt_rand(50000,1000000);
                            							
                            							$insert_row['TxnLineID']        =$lineID;
		                                                $insert_row['Item_ListID']    =$item_data['itemListID'];
                                        			   $insert_row['Quantity']        =$item_data['itemQuantity'];
                                        			   $insert_row['Rate']            =$item_data['itemRate'];
                                        			   $insert_row['Item_FullName']    = $item_data['itemFullName'];
                                        			   $insert_row['Descrip']          =$item_data['itemDescription'];
                                        			  
                                        			  $this->db->insert('qb_test_invoice_lineitem', $insert_row);
	                                        	}
	                                        	
	            	}                         	
	
		
		public function create_new_invoice($inv_data, $items)
		{
		     
		       
		       	$ins_Daat =array(
                                    			'Customer_FullName'=>$inv_data['Customer_FullName'],
                                    			'Customer_ListID'=>$inv_data['Customer_ListID'],
                                    			'TxnID' => $inv_data['TxnID'],
                                    			'RefNumber'=>$inv_data['RefNumber'],
                                    			'TimeCreated'=> $inv_data['TimeCreated'],
                                    			'TimeModified'=>$inv_data['TimeModified'],
                                    			'DueDate'   => $inv_data['DueDate'],
                                    			'ShipAddress_Addr1'=>$inv_data['ShipAddress_Addr1'],
                                    			'ShipAddress_Addr2'=>$inv_data['ShipAddress_Addr2'],
                                    			'ShipAddress_City'=>$inv_data['ShipAddress_City'],
                                    			'ShipAddress_Country'=>$inv_data['ShipAddress_Country'],
                                    			'ShipAddress_State'=>$inv_data['ShipAddress_State'],
                                    			'ShipAddress_PostalCode'=>$inv_data['ShipAddress_PostalCode'],
                                    			'IsPaid'				=>'true',	
                                    			'insertInvID'=>$inv_data['insertInvID'],
                                    			'invoiceRefNumber'=>$inv_data['invoiceRefNumber'],
                                                'AppliedAmount'=>(-$inv_data['BalanceRemaining']),
                                                 'userStatus'=>'Active',
                                                 'BalanceRemaining'=>0
                                    		
                                    			);
                                    			
                                                 $this->db->insert('chargezoom_test_invoice',	$ins_Daat) ;                   			
                                                foreach($items as $k=>$item_data)
                            					{

                            							$insert_row['TxnID'] =$inv_data['TxnID'];
                            							$lineID = mt_rand(50000,1000000);
                            							
                            							$insert_row['TxnLineID']        =$lineID;
		                                                $insert_row['Item_ListID']    =$item_data['itemListID'];
                                        			   $insert_row['Quantity']        =$item_data['itemQuantity'];
                                        			   $insert_row['Rate']            =$item_data['itemRate'];
                                        			   $insert_row['Item_FullName']    = $item_data['itemFullName'];
                                        			   $insert_row['Descrip']          =$item_data['itemDescription'];
                                        			  
                                        			  $this->db->insert('chargezoom_test_invoice_lineitem', $insert_row);
	                                        	}
	                                        	
	            	}   
	            	
	            	
	            	
	            	
	            	
	public function get_invoice_data_auto_pay()
	{
		$res = array();
		$today = date('Y-m-d');
		
	   $this->db->select('inv.BalanceRemaining, inv.RefNumber, inv.TxnID, inv.DueDate,inv.Customer_ListID,inv.IsPaid, 
	   tci.cardID, tci.gatewayID, tmg.gatewayType,tmg.gatewayType,tmg.gatewayUsername,tmg.gatewayPassword,tmg.gatewaySignature,tci.scheduleDate as schedule, tci.scheduleAmount,  mr.merchID as merchantID, mr.resellerID, cmp.qbwc_username' );
	   $this->db->from('chargezoom_test_invoice inv');
	   $this->db->join('tbl_scheduled_invoice_payment tci','inv.TxnID =tci.invoiceID','LEFT');
	   $this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
       $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
       $this->db->join('tbl_merchant_gateway tmg','tci.gatewayID = tmg.gatewayID','INNER');
	   $this->db->join('tbl_merchant_data mr','mr.merchID=qbmerchantID','inner'); 
       $this->db->where("(DATE_FORMAT(tci.scheduleDate,'%Y-%m-%d')='$today' OR (DATE_FORMAT(inv.DueDate,'%Y-%m-%d') ='$today' or tci.scheduleDate IS NULL))");                                          
	   $this->db->where('inv.IsPaid', 'false');
	   $this->db->where("inv.BalanceRemaining != ", '0.00');
	   $this->db->where('cust.customerStatus', '1');
		
		
		$query = $this->db->get();
  
		if($query->num_rows() > 0){
		
		  return $res = $query->result_array();

		}
		
		return $res;
	} 		
	
	
	
		public function get_company_invoice_data_pay($invoiceID)
	{
		$res = array();
	
		
		
		$this->db->select('inv.TxnID,inv.DueDate, inv.Customer_ListID,inv.AppliedAmount,inv.BalanceRemaining, cust.*, cmp.merchantID');

		$this->db->from('chargezoom_test_invoice inv');
		$this->db->join('chargezoom_test_customer cust','inv.Customer_ListID = cust.ListID','INNER');
	
	    $this->db->join('tbl_company cmp' , 'cmp.id = cust.companyID', 'inner');
      
	    $this->db->where('inv.TxnID' , $invoiceID);
	    $this->db->where('cust.customerStatus', '1');
	    $this->db->where('inv.IsPaid','false');
		$query = $this->db->get();
  
		if($query->num_rows() > 0){
		
		  return $res = $query->row_array();
		   
		}
		
	}
	
 
        }



?>