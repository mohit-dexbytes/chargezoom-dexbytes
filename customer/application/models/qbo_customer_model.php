<?php


Class Qbo_customer_model extends CI_Model
{
	public function get_customer_invoice_data_sum($custID, $uid){
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,   sum(inv.BalanceRemaining) as amount FROM QBO_test_invoice inv  WHERE `inv`.`CustomerListID` = '".$custID."'   and inv.merchantID='".$uid."' ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
    public function customer_by_id($id, $uid) 
	{
         $this->db->select('cus.* ');
		
		$this->db->from('QBO_custom_customer cus');
	    $this->db->join('tbl_merchant_data mr','mr.merchID = cus.merchantID','inner');
		$this->db->where('cus.Customer_ListID', $id);
        $this->db->where('cus.merchantID', $uid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
	}

 
	public function get_invoice_upcomming_data($userID, $mid)
	{
	 $today=date('Y-m-d');
	
       $res=array();
      
      $query=   $this->db->query("SELECT `qb`.*, `cs`.`fullName` AS custname, 
      (case  
      when (sc.scheduleDate IS NOT NULL and IsPaid='0')THEN 'Scheduled' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') >= '$today'AND IsPaid='0') 
       THEN 'Open' when (DATE_FORMAT(qb.DueDate, '%Y-%m-%d') <  '$today'   and IsPaid='0') 
       THEN 'Overdue'when (qb.BalanceRemaining='0' and IsPaid='1')
       Then 'Paid'ELSE 'TEST' END ) 
       as status, ifnull(sc.scheduleDate, qb.DueDate) as DueDate
       FROM (`QBO_test_invoice` qb) 
       INNER JOIN `QBO_custom_customer` cs ON `cs`.`customer_ListID`=`qb`.`CustomerListID` 
       LEFT JOIN `tbl_scheduled_invoice_payment` sc ON `sc`.`invoiceID`=`qb`.`invoiceID`
       WHERE `qb`.`merchantID` = '".$mid."' AND `cs`.`merchantID` = '".$mid."'  and  `qb`.`CustomerListID` = '".$userID."' 
       AND `cs`.`Customer_ListID` = '".$userID."'  and IsPaid='0' and qb.UserStatus !='1'
        AND (sc.merchantID=  '".$mid."' OR sc.merchantID IS NULL)
       AND (sc.customerID=qb.CustomerListID OR sc.customerID IS NULL) 
        AND ((sc.customerID=cs.Customer_ListID and sc.merchantID=cs.merchantID)OR sc.customerID IS NULL)
       ");
  
       	if($query->num_rows() > 0){
		
		  $res= $query->result_array();
		}
		
		return $res;
	}
	
	
	public function get_invoice_upcomming_dataoooo($userID, $mid){
		$res =array();
 		$today  = date('Y-m-d');

 		$query  =  $this->db->query("SELECT * FROM QBO_test_invoice WHERE CustomerListID = $userID and merchantID=$mid  AND BalanceRemaining != 0");

	 	if($query->num_rows() > 0){
			  return  $res=$query->result_array();
			}
	   
	    return  $res;
	}


    	public function get_invoice_details($cusID, $uid ){
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,ifnull(  ( Select count(*)  FROM QBO_test_invoice where   CustomerListID = $cusID and merchantID=$uid  and BalanceRemaining=0 ),0) as Paid,
           ifnull( (select  count(*) from QBO_test_invoice where   CustomerListID = $cusID and merchantID=$uid and  DATE_FORMAT(DueDate,'%Y-%m-%d') >= '".$today."' and BalanceRemaining > 0 ),'0') as schedule, 
           ifnull( (select count(*) from QBO_test_invoice inner join customer_transaction tr on tr.invoiceID=QBO_test_invoice.invoiceID  where  QBO_test_invoice.merchantID=$uid  and  tr.customerListID = $cusID and  tr.merchantID=$uid and (tr.transactionCode ='300')  ),'0') as Failed
            FROM QBO_test_invoice inv  WHERE   inv.CustomerListID = $cusID  and  inv.merchantID='".$uid."'  ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
	
	public function get_invoice_latest_data($userID, $mid)
	{

	$res =array();
 	
 	$today  = date('Y-m-d');
    
   
 		$query  =  $this->db->query("SELECT *,(SELECT transactionDate FROM customer_transaction WHERE QBO_test_invoice.Total_payment = customer_transaction.transactionAmount AND    customer_transaction.CustomerListID = $userID AND customer_transaction.transactionStatus != 'Failure' LIMIT 1) AS transactionDate FROM QBO_test_invoice WHERE (BalanceRemaining = 0 or  BalanceRemaining!= Total_payment) AND CustomerListID = $userID   and QBO_test_invoice.merchantID=$mid  ");  

	 	if($query->num_rows() > 0){
			  return  $res=$query->result_array();
			}
	   
	    return  $res;
	    
 
 	}
 	
 	public function get_customer_invoice_data_payment($custID, $mid){
$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  ifnull(sum(Total_payment),0) as applied_amount,  
		  ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where `CustomerListID` = '".$custID."' and (BalanceRemaining!=0  and Total_payment!=BalanceRemaining) and merchantID=$mid ),'0')as applied_due, 
ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0')as remaining_amount , 
		  ifnull( (select sum(BalanceRemaining) from QBO_test_invoice where `CustomerListID` = '".$custID."'  and merchantID=$mid  ),'0') as upcoming_balance FROM QBO_test_invoice   WHERE `CustomerListID` = '".$custID."'  and merchantID=$mid  and BalanceRemaining=0   ");
		 $res=$query->row();
		 
		 return  $res;
		
	}
	
	 public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
    public function get_all_customer_details( $mid){
        
         $this->db->select('cus.*,    (select sum(BalanceRemaining) 
		 from 	QBO_test_invoice where CustomerListID = cus.Customer_ListID) as Balance ');
		
		$this->db->from('QBO_custom_customer cus ');
		
		
        $this->db->where('cus.merchantID', $mid);
		$query = $this->db->get();
		if($query -> num_rows() > 0)

	      $res= $query->result_array(); 
        
	

		return 	$res; 
    }
	
    public function customer_details($custID, $mid){
        
         $this->db->select('*');
		
		$this->db->from('QBO_custom_customer cus');
		$this->db->where('Customer_ListID', $custID);
        $this->db->where('cus.merchantID', $mid);
		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->row(); 
        
		else

		return false; 
    }
	

function get_customers_data($merchantID) 
	{
		
     
        $this->db->select('*');
        $this->db->from('QBO_custom_customer') ;
        $this->db->where('merchantID', trim($merchantID));

      

		$query = $this->db->get();

		if($query -> num_rows() > 0)

		return $query->result_array(); 

		else

		return false; 
	}	


	public function get_transaction_data_refund($custID, $userID){
		$today = date("Y-m-d H:i");
		 $res=array();
        $query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr 
		inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where  tr.customerListID='".$custID."' and  cust.merchantID='".$userID."' and  tr.merchantID='".$userID."' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture' or tr.transactionType ='usaepay_sale') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and ((tr.transaction_user_status !='success' and tr.transaction_user_status !='refund') or  tr.transaction_user_status  IS NULL)  and tr.gateway!='AUTH ECheck'  and tr.gateway!='NMI ECheck' and tr.transactionID!='' GROUP BY tr.transactionID DESC");

		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
	
	public function get_transaction_data_captue($userID){
		
		
	$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='usaepay_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
		 and ( (tr.transaction_user_status !='success' and tr.transaction_user_status !='refund') or tr.transaction_user_status IS NULL ) GROUP BY id ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
		public function get_transaction_data_erefund($userID){
		$today = date("Y-m-d H:i");
		 $res=array();
        $query =$this->db->query("Select tr.*,  DATEDIFF('$today', DATE_FORMAT(tr.transactionDate, '%Y-%m-%d H:i')) as tr_Day, cust.* from customer_transaction tr 
		inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."' and (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture'
		or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
		or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  
		(tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1'or tr.transactionCode='200') 
		and ((tr.transaction_user_status !='success' and tr.transaction_user_status !='refund')  or  tr.transaction_user_status  IS NULL)   and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck') and tr.transactionID!='' GROUP BY tr.transactionID DESC");

		if($query->num_rows() > 0){
		
		 $res= $query->result_array();
		}
		return $res;
	}
	
	
		public function get_transaction_data_ecaptue($userID){
		
		
	$query =$this->db->query("Select tr.*, cust.* from customer_transaction tr inner join  QBO_custom_customer cust on  tr.customerListID = cust.Customer_ListID Where cust.merchantID='".$userID."' and  tr.merchantID='".$userID."'
		and  (tr.transactionType ='auth'  or tr.transactionType ='stripe_auth' or tr.transactionType ='Paypal_auth' or tr.transactionType='auth_only' 
		or tr.transactionType='pay_auth')  and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode ='1' or tr.transactionCode ='200')  
	 and (tr.gateway='NMI ECheck'  or tr.gateway='AUTH ECheck')  and (tr.transaction_user_status !='refund' or  tr.transaction_user_status  IS NULL) 
		   GROUP BY id ");
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	
	
	public function get_subscriptions_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}

     public function get_subscriptions_plan_data($userID)
	{
	    $this->db->select('sbs.*, cust.fullName, cust.companyName, spl.planName as sub_planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');

    	
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
		$this->db->join('tbl_subscriptions_plan_qbo spl','spl.planID = sbs.planID','left');
	    $this->db->where("sbs.merchantDataID ", $userID);
	     $this->db->where("cust.merchantID ", $userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
    
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}

		
	}
		public function get_cust_subscriptions_data($userID){
	
					
		$this->db->select('sbs.*, cust.fullName,pl.planName as sub_planName, tmg.*');
		$this->db->from('tbl_subscriptions_qbo sbs');
		$this->db->join('QBO_custom_customer cust','sbs.customerID = cust.Customer_ListID','INNER');
	
    	$this->db->join('tbl_subscriptions_plan_qbo pl','pl.planID = sbs.planID','left');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	   $this->db->where($userID);
	    $this->db->group_by("sbs.subscriptionID");
	    $this->db->order_by("sbs.createdAt", 'desc');
	  
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
	
	public function get_transaction_data($userID, $m_id)
	{
				$today = date("Y-m-d H:i");
		$this->db->select('tr.id,tr.transactionID ,tr.invoiceID, tr.transactionDate, tr.customerListID,tr.transactionAmount,tr.transactionType,tr.transactionGateway,tr.transactionCode, cust.fullName, ');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	    $this->db->where("tr.merchantID ", $m_id);
        $this->db->where("cust.merchantID ", $m_id);
          $this->db->where("cust.Customer_ListID ", $userID);
	    $this->db->order_by("tr.transactionDate", 'desc');
	   $this->db->group_by("tr.transactionID", 'desc');
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	}
	
		public function get_refund_transaction_data($userID)
		{
	
		$query =	$this->db->query ("SELECT `tr`.*, `cust`.* FROM (`customer_transaction` tr) 
	INNER JOIN `QBO_custom_customer` cust ON `tr`.`customerListID` = `cust`.`Customer_ListID`
	WHERE `tr`.`merchantID` = '$userID' and cust.merchantID= '$userID'   AND (`tr`.`transactionType` = 'refund' or `tr`.`transactionType` = 'stripe_refund' OR `tr`.`transactionType` = 'usaepay_refund' OR `tr`.`transactionType` = 'pay_refund' OR `tr`.`transactionType` = 'credit' ) ORDER BY `tr`.`transactionDate` desc") ;
	
		
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	


	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*, cust.companyName');
		$this->db->from('qbo_customer_credit cr');
		$this->db->join('QBO_custom_customer cust','cr.CustomerListID = cust.Customer_ListID','INNER');
		
	    $this->db->where($con);
	
		$this->db->order_by('cr.TimeModified','desc');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 	

	  public function get_customer_note_data($custID,$mID){

	$res =array();
 	
	  $query  =  $this->db->query("SELECT  pr.*  FROM tbl_private_note pr  INNER JOIN `QBO_custom_customer` cust ON `pr`.`customerID` 
	  = `cust`.`Customer_ListID` WHERE `pr`.`customerID` = '".$custID."' and  pr.merchantID = '".$mID."'  group  by pr.noteID desc");

		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 
	
    	function get_billing_data($merchID) 
	{
		 $sql = ' SELECT * from tbl_merchant_billing_invoice where merchantID = "'.$merchID.'" ';
		 

		$query = $this->db->query($sql);
		
		if($query->num_rows()>0 ) {
				return $query->result_array();
			   
			} else {
				return false;
			}	

		
	}
	
	 public function get_merchant_billing_data($invID){
		   $res = array();
		   $query = $this->db->query("SELECT bl.*, mr.*, ct.city_name, st.state_name, c.country_name FROM (tbl_merchant_billing_invoice bl) INNER JOIN tbl_merchant_data mr ON mr.merchID=bl.merchantID LEFT JOIN state st ON st.state_id=mr.merchantState LEFT JOIN country c ON c.country_id=mr.merchantCountry LEFT JOIN city ct ON ct.city_id=mr.merchantCity WHERE bl.merchant_invoiceID = '$invID' AND bl.status = 'pending'");
		   
		   if($query->num_rows()>0 ) {
				$res = $query->row_array();
			   return $res;
			} else {
				return $res;
			}
		
		   
	   }
   
   
	function update_refund_payment($tr_id, $gateway)
	{
	    
	    if($gateway=='NMI')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where (  transactionID="'.$tr_id.'" and (transactionType="capture" or transactionType="sale") )';
	    }
	     if($gateway=='AUTH')
	    {
	        
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="prior_auth_capture" or transactionType="auth_capture") ) ';
	    }
	    
	      if($gateway=='PAYTRACE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="refund" where (transactionID="'.$tr_id.'"  and (transactionType="pay_sale" or transactionType="pay_capture") )'; 
	        
	    }
	    
	       if($gateway=='PAYPAL')
	    {
	         $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="Paypal_capture" or transactionType="Paypal_sale") )'; 
	        
	    }
	    
	        if($gateway=='STRIPE')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
		if($gateway=='CardPointe')
	    {
	          $sql =' UPDATE customer_transaction set transaction_user_status="refund" where ( transactionID="'.$tr_id.'"  and (transactionType="stripe_capture" or transactionType="stripe_sale") ) ';
	        
	    }
	    
	  
	   if($this->db->query($sql))
	   return true;
	   else
	   return false;
	}



    public function get_invoice_transaction_data($invoiceID, $userID)
	   {
		
		$this->db->select('tr.*, cust.* ');
		$this->db->from('customer_transaction tr');
		$this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
	
	    $this->db->where("cust.merchantID ", $userID);
		 $this->db->where("tr.invoiceID ", $invoiceID);
		  $this->db->where("tr.merchantID ", $userID);
		$this->db->group_by('transactionID');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 		
	
	
	
		public function get_customer_invoice_data($custID, $mID){

	$res =array();
 	$today  = date('Y-m-d');
	  $query  =  $this->db->query("SELECT `inv`.*,  DATEDIFF(DATE_FORMAT(inv.DueDate, '%Y-%m-%d'),'$today') as leftDay , cust.*, 
	 ( case   when  IsPaid ='true' and userStatus !='cancel'    then 'Success'
	  when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') >= '$today' AND `IsPaid` = 'false' and userStatus !='cancel'  then 'Open' 
	    when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus !='cancel'  and (select transactionCode from tbl_customer_tansaction where invoiceTxnID =inv.TxnID limit 1 )='300'  then 'Failed'
	   when DATE_FORMAT(inv.DueDate, '%Y-%m-%d') < '$today' AND `IsPaid` = 'false' and userStatus !='cancel'   then 'Past Due'
	   
	  else 'Cancel' end ) as status FROM qb_test_invoice inv
	  INNER JOIN `qb_test_customer` cust ON `inv`.`Customer_ListID` = `cust`.`ListID`  WHERE `inv`.`Customer_ListID` = '$custID'  and cust.qbmerchantID='$mID' ");
	
		if($query->num_rows() > 0){
		
		  return  $res=$query->result_array();
		}
	     return  $res;
	} 		
	




}