<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{
    var $table = "settings";

    var $table2 = "setting_groups";

    var $table3 = "options";

    public function getSettingGroups(){
        
        $groups = $this->db->select('*')->from($this->table2)->get()->result_array();
        

        return $groups;
    }
    
    public function getSettingsByGroup($group_identifier)
    {
        $data = $this->db->select('key_name, key_value')->from($this->table.' s1')->join($this->table2.' s2' ,'s1.setting_group_id=s2.id')->where(['s2.unique_identifier' => $group_identifier])->get()->result_array();
        
        $pairs = [];
        
        foreach($data as $row){
            $pairs[$row['key_name']] = $row['key_value'];
        }

        return $pairs;
    }

    public function addSettingGroup($group_name, $group_identifier)
    {
        if($group_name && $group_identifier) {
            $group_id = $this->getGroupIdByIdentifier($group_identifier);

            if(!$group_id){
                $this->db->insert($this->table2, ['name' => $group_name, 'unique_identifier' => $group_identifier]);
                $group_id = $this->db->insert_id();
            }

            return $group_id;
        }

        return false;

    }

    public function getGroupIdByIdentifier($group_identifier){
        $gr = $this->db->select('id')->from($this->table2)->where(['unique_identifier' => $group_identifier])->get()->row_array();
        if($gr) {
            return $gr['id'];
        }
        return false;
    }

    public function updateSettingValue($key_name, $key_value, $group_identifier=null)
    {
        $group_id = null;
        if($group_identifier) {
            $group_id = $this->getGroupIdByIdentifier($group_identifier);

            if(!$group_id){
                $this->db->insert($this->table2, ['name' => $group_identifier, 'unique_identifier' => $group_identifier]);
                $group_id = $this->db->insert_id();
            }
        }

        $where_array = ['key_name' => $key_name];
        if($group_id) {
            $where_array['setting_group_id'] = $group_id;
        }
        $row = $this->db->select('*')->from($this->table)->where($where_array)->get()->row_array();
        if($row){
            $this->db->where($where_array);
            $this->db->update($this->table, ['key_value' => $key_value]);
            
            return $row['id'];
        } else {
            $where_array['key_value'] = $key_value;
            $this->db->insert($this->table, $where_array);
            return $this->db->insert_id();
        }
    }

    public function getOptionValue($option_name){
        $row = $this->db->select('id, option_value')->from($this->table3)->where(['option_name' => $option_name])->get()->row_array();

        if(!empty($row)) {
            return $row['option_value'];
        } else {
            return false;
        }
    }

    public function getAllOptionValue(){
        $result = $this->db->select('id, option_name,option_value')->from($this->table3)->get()->result_array();
        $option = [];

        if(!empty($result)) {
            foreach ($result as $row) {
                $option[$row['option_name']] = $row['option_value'];
            }
        } 
        
        return $option;
    }
    
    public function updateOptionValue($key_name, $key_value)
    {
        
        $where_array = ['option_name' => $key_name];
        
        $row = $this->db->select('*')->from($this->table3)->where($where_array)->get()->row_array();
        if($row){
            $this->db->where($where_array);
            $this->db->update($this->table3, ['option_value' => $key_value, 'updated_date' => date('Y-m-d H:i:s')]);
            
            return $row['id'];
        } else {
            $where_array['option_value'] = $key_value;
            $this->db->insert($this->table3, $where_array);
            return $this->db->insert_id();
        }
    }
}