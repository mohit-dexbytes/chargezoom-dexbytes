<?php


Class Fb_invoice_model extends CI_Model
{

	 public function get_fb_invoice_data($table, $con) {
	 		$data=array();
			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	 }


    public function get_invoice_details($csID, $uid){
    
    
		$res = array();
		$today = date('Y-m-d');
		  $query  =  $this->db->query("SELECT  count(*) as incount,ifnull(  ( Select count(*)  FROM Freshbooks_test_invoice where  CustomerListID=$csID and  merchantID=$uid  and BalanceRemaining=0 ),0) as Paid,
           ifnull( (select count(*) from Freshbooks_test_invoice where  CustomerListID=$csID and   merchantID=$uid and  DATE_FORMAT(DueDate,'%Y-%m-%d') >= '".$today."' and BalanceRemaining > 0 ),'0') as schedule, 
           ifnull( (select count(*) from Freshbooks_test_invoice inner join customer_transaction tr on tr.invoiceID=Freshbooks_test_invoice.invoiceID  where  Freshbooks_test_invoice.CustomerListID=$csID and  Freshbooks_test_invoice.merchantID=$uid  and tr.customerListID=$csID and  tr.merchantID=$uid and (tr.transactionCode ='300')  ),'0') as Failed
            FROM Freshbooks_test_invoice inv  WHERE  CustomerListID=$csID and  inv.merchantID='".$uid."' ");
		if($query->num_rows() > 0){
		
		 $res=$query->row();
		 return  $res;
		}
	    return false;
	}	
   

}