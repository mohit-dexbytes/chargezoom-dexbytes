<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting TSYS sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */

$mode = 1;

#Sandbox mode check
if ($mode == 0) {
  $config['environment'] = 'production';
} else {
  $config['environment'] = 'production';
}
