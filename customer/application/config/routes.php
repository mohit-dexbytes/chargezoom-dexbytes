<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "login";
$route['404_override'] = '';

$route['qbo_check_out/(:any)/(:any)'] = 'QBO_controllers/CheckPlan/check_out/$1/$2';
$route['qbo_check_out1/(:any)/(:any)'] = 'QBO_controllers/CheckPlan/check_out1/$1/$2';
$route['thankyou'] = 'Thankyou';
$route['wrong_url'] = 'QBO_controllers/CheckPlan/wrong_url';


$route['fb_check_out/(:any)/(:any)'] = 'FreshBooks_controllers/CheckPlan/check_out/$1/$2';
$route['thankyou'] = 'Thankyou';
$route['wrong_url'] = 'FreshBooks_controllers/CheckPlan/wrong_url';


$route['qbd_check_out/(:any)/(:any)'] = 'CheckPlan/check_out/$1/$2';
$route['company_check_out/(:any)/(:any)'] = 'company/CheckPlan/check_out/$1/$2';

$route['thankyou'] = 'Thankyou';
$route['wrong_url'] = 'CheckPlan/wrong_url';

$route['update_payment/(:any)/(:any)'] = 'Invoice_payment/update_payment/$1/$2';
$route['update_payment1/(:any)/(:any)'] = 'Invoice_payment/update_payment1/$1/$2';

$route['update_payment/payment'] = 'Invoice_payment/payment/';
$route['update_payment/thankyou'] = 'Invoice_payment/update_payment/thankyou/';
$route['paid_url'] = 'Invoice_payment/paid';

$route['common_checkout/(:any)/(:any)'] = 'Integration/CheckPlan/check_out/$1/$2';

/* End of file routes.php */
/* Location: ./application/config/routes.php */





