<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** 
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting Cybersource's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */
require_once dirname(__FILE__) . '../../../../vendor/autoload.php';


 require_once dirname(__FILE__) . '../../../../cybersource-rest-samples-php/Resources/ExternalConfiguration.php';
$config['Sandbox'] 		 = FALSE;


     $mode='1';
        $CI = & get_instance(); 
       if( $CI->session->userdata('customer_logged_in')){
    	$mode =  $CI->session->userdata('customer_logged_in')['gatewayMode'];
		
		}


if(ENVIRONMENT == 'development'){
	$mode = 0;
} 
     
$config['mode'] = $mode;

if($mode==0)
$config['Sandbox'] = TRUE;
else
$config['Sandbox'] = FALSE;


$config['SandboxENV']    = "cyberSource.environment.SANDBOX";
$config['ProductionENV'] = "cyberSource.environment.PRODUCTION";
