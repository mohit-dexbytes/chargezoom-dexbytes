<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sandbox / Test Mode
 * -------------------------
 * TRUE means you'll be hitting FluidPay's sandbox/test servers.  FALSE means you'll be hitting the live servers.
 */

$mode = '';

$CI   = &get_instance();

if( $CI->session->userdata('customer_logged_in')){
    $mode =  $CI->session->userdata('customer_logged_in')['gatewayMode'];
}

if($mode === '') {
  switch(getenv('ENV')){
    case 'development':
    case 'staging':
    case 'local':
        $mode = 0;
    break;
    case 'production':
        $mode = 1;
    break;
    default:
        $mode = 1;
    break;
  }
}

if ($mode == 0) {
  $config['environment'] = 'sandbox';
} else {
  $config['environment'] = 'production';
}
