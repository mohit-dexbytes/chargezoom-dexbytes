<?php
defined('BASEPATH') or exit('No direct script access allowed');
$config['Sandbox'] = false;

$mode = 1;
$CI   = &get_instance();
if ($CI->session->userdata('customer_logged_in')) {
   $mode = $CI->session->userdata('customer_logged_in')['gatewayMode'];
}


if(ENVIRONMENT == 'development'){
	$mode = 0;
}

if ($mode == 0) {
   $config['Sandbox']        = true;
   $config['PRO_GLOBAL_URL'] = 'https://cert.api2.heartlandportico.com';
   $config['GLOBAL_URL']     = 'https://cert.api2.heartlandportico.com';
} else {
   $config['Sandbox']        = false;
   $config['PRO_GLOBAL_URL'] = 'https://api2.heartlandportico.com';
   $config['GLOBAL_URL']     = 'https://api2.heartlandportico.com';
}

$config['DeveloperId']    = '002914';
$config['VersionNumber']  = '3411';
$config['mode'] = $mode;