<?php
    $config['urlAuthorize'] = 'https://login.xero.com/identity/connect/authorize';
    $config['urlAccessToken'] = 'https://identity.xero.com/connect/token';
    $config['urlResourceOwnerDetails'] = 'https://api.xero.com/api.xro/2.0/Organisation';

    $config['clientId'] = getenv('XERO_CLIENT_ID');
    $config['clientSecret'] = getenv('XERO_CLIENT_SECRET');
    $config['redirectUri'] = getenv('XERO_REDIRECT_URI');

    define('CREATE_CUSTOMER', 'CREATE_CUSTOMER');
    define('UPDATE_CUSTOMER', 'UPDATE_CUSTOMER');
    define('SYNC_CUSTOMER', 'SYNC_CUSTOMER');

    define('SYNC_INVOICE', 'SYNC_INVOICE');
    define('CREATE_INVOICE', 'CREATE_INVOICE');
    define('UPDATE_INVOICE', 'UPDATE_INVOICE');
    define('VOID_INVOICE', 'VOID_INVOICE');
    define('PAY_INVOICE', 'PAY_INVOICE');
    define('DELETE_INVOICE_PAYMENT', 'DELETE_INVOICE_PAYMENT');

    define('SYNC_ITEM', 'SYNC_ITEM');
    define('CREATE_ITEM', 'CREATE_ITEM');
    define('UPDATE_ITEM', 'UPDATE_ITEM');

    define('SYNC_ACCOUNT', 'SYNC_ACCOUNT');
    define('CREATE_ACCOUNT', 'CREATE_ACCOUNT');
    define('UPDATE_ACCOUNT', 'UPDATE_ACCOUNT');

    define('SYNC_TAX', 'SYNC_TAX');
    define('CREATE_TAX', 'CREATE_TAX');
    define('UPDATE_TAX', 'UPDATE_TAX');
?>