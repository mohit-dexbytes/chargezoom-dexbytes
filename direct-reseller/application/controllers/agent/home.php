<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		delete_cache();
	$this->load->model('reseller_panel_model');
		$this->load->model('general_model');
		if(!$this->session->userdata('agent_direct_logged_in'))
		{
			redirect('login', 'refresh');
		}
  
	}
	
	public function index()
	{   
	          $data['primary_nav']  = primary_agent_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('agent_direct_logged_in');
			 
			 
			 $agentID = $data['login_info']['ragentID'];
			 $resID = $data['login_info']['resellerIndexID'];
			  $merchant = $this->reseller_panel_model->get_data_admin_merchant($resID,$agentID);
			  $data['merchant_list'] = $merchant;
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/merchant_list', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	}
	

	public function merchant_list()
	{   
		      $data['primary_nav']  = primary_agent_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('agent_direct_logged_in');
			 
			 
			 $agentID = $data['login_info']['ragentID'];
			 $resID = $data['login_info']['resellerIndexID'];
			  $merchant = $this->reseller_panel_model->get_data_admin_merchant($resID,$agentID);
		
			  $data['merchant_list'] = $merchant;
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/merchant_list', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	
	}
		public function disable_merchant_list()
	{   
		 $data['primary_nav']  = primary_agent_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('agent_direct_logged_in');
			 
			 
			 $agentID = $data['login_info']['ragentID'];
			 $resID = $data['login_info']['resellerIndexID'];
			 $merchant = $this->reseller_panel_model->get_disable_data_admin_merchant($resID,$agentID);

			  $data['merchant_list'] = $merchant;
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/disable_merchant_list', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	
	}
	
	
	 public function merchant_login()
    {     
      
     $loginID = $this->uri->segment('3');  

     $userdata = $this->reseller_panel_model->get_row_data('tbl_merchant_data',array('merchID'=>$loginID));
    
     $user = json_decode (json_encode ($userdata), FALSE);
     
     $data_session['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			 
			 $rID = $data_session['login_info']['resellerID'];
			 
		$Merchant_url = $this->reseller_panel_model->get_row_data('Config_merchant_portal',array('resellerID'=>$rID));	
	
    	$url = $Merchant_url['merchantPortalURL'];
        $base_url = $url."home/index/"; 

    
    if ($user && isset($user->merchantEmail))
		{  
	    
	    $this->reseller_login_model->login_as_mecrchant($loginID);
	     	  $sess_array = (array)$user;
		    
		    if( $user->firstLogin==0 ){
				    
					$condition  = array('merchantID'=>$user->merchID);	
					$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
					$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
					if(!empty($appset)){
						$sess_array['active_app']	=$appset['appIntegration'];
					}else{
						$sess_array['active_app']	='2';
					}
					$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
					
					if($gatway_num){
						$sess_array['merchant_gateway']	='1';
					}else{
						$sess_array['merchant_gateway']	='0';
					}		
					$filenum                   = $this->general_model->get_num_rows('tbl_company', $condition);
					$sess_array['fileID']      = $filenum;
					
					if(!empty($config)){
						
					$sess_array['portalprefix']      =$config['portalprefix'];
					}else{
						$sess_array['portalprefix']      = '';
					}
			
			
					 if($user->companyName !="" && $user->merchantAddress1 !="" && $user->merchantFullAddress !="" &&  $sess_array['merchant_gateway'] !="0" && $sess_array['fileID'] >0 )
					 {
				 
						$input_data   = array('firstLogin'=>'1');
						$condition1   = array('merchID'=>$user->merchID);
						$this->general_model->update_row_data('tbl_merchant_data',$condition1, $input_data);
					  }	
			     
			       
			        $sess_array['active_app'] = '0';
					$this->session->set_userdata('logged_in', $sess_array);
					redirect($url.'home/dashboard_first_login');
				  
			}else{
						
				    $condition  = array('merchantID'=>$user->merchID);	
					$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);
							
					if($gatway_num){
						$sess_array['merchant_gateway']	='1';
					}else{
						$sess_array['merchant_gateway']	='0';
					}	
		           $config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
		           
				   if(!empty($config)){
						
					$sess_array['portalprefix']      =$config['portalprefix'];
					}else{
						$sess_array['portalprefix']      = '';
					}
			          $appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
					if(!empty($appset)){
						$sess_array['active_app']	=$appset['appIntegration'];
					}else{
						$sess_array['active_app']	='2';
					}
			  
			    $this->session->set_userdata('logged_in', $sess_array);
			
				 if($this->session->userdata('logged_in')['firstLogin']=='1' && $this->session->userdata('logged_in')['active_app']=='1' ){
									$furl  =	$url.'QBO_controllers/home/index';
							
								}else if($this->session->userdata('logged_in')['firstLogin']=='1' && $this->session->userdata('logged_in')['active_app']=='3' ){
										$furl  =	$url.'FreshBooks_controllers/home/index';
						
							   }else if($this->session->userdata('logged_in')['firstLogin']=='1' && $this->session->userdata('logged_in')['active_app']=='4' ){
										$furl =	$url.'Xero_controllers/home/index';
							
							   }else{
								   	$furl  =	$url.'home/index';
							
							   }					   

				redirect($furl,'refresh');
			}	
			 $this->session->set_userdata('logged_in', $sess_array);
			redirect($base_url);		

		}
		
           
    } 
	
	
		public function recover_Merch_pwd() 
    {      
		return false;
        $id = $this->uri->segment('4');		
	$results = $this->general_model->get_row_data('tbl_merchant_data', array('merchID'=> $id ));
		if(!empty($results))
		{
			$email= $results['merchantEmail'];
			$this->load->helper('string');
			$code = random_string('alnum',10);
            
			$update = $this->reseller_panel_model->temp_reset_password($code,$email);
			if($update)
			{
				$this->load->library('email');
				$subject = 'OmniMerchant - Reset Password';
				$this->email->from('support@chargezoom.com');
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->set_mailtype("html");

				$message = "<p>Hello, <br><br>Your new password has been updated successfully. Please use the new password: ".$code."</p><br><br> Thanks,<br>Support, OmniMerchant <br>www.omnimerchant.com";
				
				$this->email->message($message);
			
				if($this->email->send())
				{	
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> New password has been sent to your email. Please check it.</div>');
				}
				else
				{
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email was not sent, please contact your administrator.</div>'.$code);
				}				
			}			
			redirect('agent/home/merchant_list', 'refresh');
		}
		else
		{
		   $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email not available!'); 
		   redirect('agent/home/merchant_list', 'refresh');
		}	
	}
	
	
	
	public function create_merchant()
	{   
		return false;
	  if(!empty($this->input->post(null, true))){
	 
	   $this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
		$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
		$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			if($this->czsecurity->xssCleanPostInput('merchID')==""){
		$this->form_validation->set_rules('merchantEmail', 'Email', 'required');
	
		$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}
		
		if ($this->form_validation->run() == true)
		{
	 
		$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
		
		$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
        $LastName = $this->czsecurity->xssCleanPostInput('lastName');
		$input_data['merchantEmail']   = $email;
		$input_data['firstName']	   = $FirstName;
		$input_data['lastName']	       = $LastName;
		
		$input_data['plan_id']   = $this->czsecurity->xssCleanPostInput('Plans');
	
		$da            = $this->session->userdata('agent_direct_logged_in');
		$user_id                       = $da['resellerIndexID'];
		$input_data['resellerID'] = $user_id; 
			$ag_id             = $da['ragentID'];
			$input_data['agentID'] = $ag_id; 
		
		
		if($this->czsecurity->xssCleanPostInput('merchID')!="" ){
				      
			 $id = $this->czsecurity->xssCleanPostInput('merchID');
		
			 $chk_condition = array('merchID'=>$id);
			 $this->reseller_panel_model->update_row_data('tbl_merchant_data',$chk_condition, $input_data);
			 $this->session->set_flashdata('success', 'Successfully Updated');
			
		     redirect(base_url('agent/home/merchant_list'), 'refresh');
		}
		else
	   {
			
			$results = $this->reseller_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail'=> $email ) );
			if($results)
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email Already Exitsts.</div>');
			   
			}
			else 
			{  
		        $code =substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
				$input_data['loginCode']	   = $code;
				$input_data['isEnable']	   = '0';
				
		      $input_data['merchantEmail']   = $email;
               $input_data['merchantPasswordNew'] = password_hash($this->czsecurity->xssCleanPostInput('merchantPassword'), PASSWORD_BCRYPT);
               	$input_data['date_added'] = date('Y-m-d H:i:s');
			  $insert = $this->reseller_panel_model->insert_row('tbl_merchant_data', $input_data);
			   $this->session->set_flashdata('success', 'Successfully Created');
					$this->load->library('email');
					
					$subject = 'OmniMerchant, Verification mail';
					$this->email->from('support@chargezoom.com');
					$this->email->to($email);
					$this->email->subject($subject);
					$this->email->set_mailtype("html");
					$base_url = "http://testreseller.payportal.net/login/enable_merchant/".$code;

				$message = "<p>Dear $FirstName $LastName,<br><br>Thanks for registering with OmniMerchant. Please verify your email id by clicking following verification link.</p><br> <p><a href=".$base_url.">Click Here</a> </p><br><br> Thanks,<br>Support, OmniMerchant <br>www.chargezoom.com";	
					$this->email->message($message);
				
					if($this->email->send())
					{	
						$this->session->set_flashdata('success', 'Successfully Created');
						
					}
					else
					{
						
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email was not sent, please contact your administrator...'.$email);
					}
					
						redirect(base_url('agent/home/merchant_list'), 'refresh');
				}
     		  }				
			 }else{
				$this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error:</strong> Validation errors exists.</div>');
			}		
       }
	              if($this->uri->segment('4')){
					  $merchID  			  = $this->uri->segment('4');	
					  $con                = array('merchID'=> $merchID);  
					  $data['merchant'] 	  = $this->reseller_panel_model->get_row_data('tbl_merchant_data', $con);	
					  
				}
	
	
              $data['primary_nav']  = primary_agent_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('agent_direct_logged_in');
			  
			  $pand   = $this->general_model->get_select_data('tbl_reseller',array('plan_id'),array('resellerID'=>$data['login_info']['resellerIndexID']));
			  $plans   = explode(',',$pand['plan_id']);
			  $newplan  = array();
			  foreach($plans as $key=>$plan){
			     
			      $p_data =$this->reseller_panel_model->get_planfdn_data($plan);
			      
			      $newplan[$key]['plan_id'] =$p_data['plan_id'];
			      
			      if($p_data['friendlyname'] !=""){
							    $newplan[$key]['plan_name'] = $p_data['friendlyname'];
							}
							else{
							   $newplan[$key]['plan_name'] = $p_data['plan_name'];
							}
							
			    
			      
			  }
			  
			 
			  $data['plan_list'] = $newplan;
			 
			 $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/add_merchant', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
	
	}
	
	
	public function delete_merchant()
	{
	
	      $id = $this->czsecurity->xssCleanPostInput('merchID');
		  
		  $res = $this->reseller_panel_model->get_row_data('tbl_merchant_data',array('merchID'=>$id, 'isDelete'=>'0' )) ;	
		 
		  if(!empty($res)){
			  
			  $ins_data = array('merchID'=>$id, 'isDelete'=>'1');
			  
			  $this->reseller_panel_model->update_row_data('tbl_merchant_data',array('merchID'=>$id, 'isDelete'=>'0' ), $ins_data);
			  $this->session->set_flashdata('success', 'Successfully Deleted');
			  
			  
		  }else{
		       $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
			   redirect(base_url('agent/home/merchant_list'));
		        
		  }
	
	}
	
	public function merchant_details()
	{   
	
	         $merchantID =   $this->uri->segment('4');  
         	    
			   $data['primary_nav']  = primary_agent_nav();
			 
			     $data['template']    = template_variable();
			    
		   $data['login_info']   = $this->session->userdata('reseller_direct_logged_in');	
		     
		     $merchant = $this->reseller_panel_model->merchant_by_id($merchantID);
			 
			  $data['merchants'] = $merchant;
			 $data['card_data_array']   = $this->get_card_expiry_data($merchantID);
			  $data['customers']     = $this->reseller_panel_model->get_customers($merchantID);
			  $this->load->view('template/template_start', $data);
			  
			  $data['transactions']     = $this->reseller_panel_model->get_transaction_data($merchantID);
			  
		  $data['invoice']     = $this->reseller_panel_model->get_data_total_invoice($merchantID);
	  $data['amount']     = $this->reseller_panel_model->get_data_total_amount($merchantID); 
			   
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/merchant_details', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
              
	
	}
	
	
	
	  
  public function get_card_expiry_data($merchantID){  
  
                       $card = array();
               		   $this->load->library('encrypt');

	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
				
	       
	  		     $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
			     
			    	$this->db1 = $this->load->database($dsn, true);        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantListID='$merchantID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $customer_card['CardNo']  = substr($this->encrypt->decode($card_data['MerchantCard']),12) ;
						 $customer_card['merchantCardID'] = $card_data['merchantCardID'] ;
						 $customer_card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
						 $card[$key] = $customer_card;
					   }
				}		
					
                return  $card;

     }
	 
	   public function get_card_edit_data(){
	  
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->get_single_card_data($cardID);
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
    }
    
    public function reseller_planfname(){
     
        $planID = $this->czsecurity->xssCleanPostInput('planID');
        $data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
	    $rID                = $data['login_info']['resellerID'];   
        $friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
        $today = date('Y-m-d H:i:s');
        
        $data = array(
            
            "plan_id" => $planID,
            "reseller_id" => $rID,
            "friendlyname" => $friendlyname,
            "date" => $today
         
            );
        
        $condition = array("plan_id" => $planID,  "reseller_id" => $rID); 
        
        $check =  $this->reseller_panel_model->get_table_data('plan_friendlyname',$condition);
       
        if(empty($check)){
          $table = $this->reseller_panel_model->insert_row('plan_friendlyname', $data);   
            
        }else{
           $table = $this->reseller_panel_model->update_row_data('plan_friendlyname',$condition, $data);

		}
		$this->session->set_flashdata('success', 'Success');
        
        redirect('Plan/reseller_plans','refresh'); 
    }
    
    public function get_plan_name(){
        $planID = $this->czsecurity->xssCleanPostInput('plan_id');
        $plan = $this->reseller_panel_model->plan_by_id($planID);
        echo json_encode($plan);
    }
  
  public function get_single_card_data($cardID){  
  
                  $card = array();
               	  $this->load->library('encrypt');

	   	    		
	   	 
	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
				
			   
		         
	  		     $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
			    
			    	$this->db1 = $this->load->database($dsn, true);        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantCardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->encrypt->decode($card_data['MerchantCard']) ;
						 $card['cardMonth']  = $card_data['CardMonth'];
						  $card['cardYear']  = $card_data['CardYear'];
						  $card['merchantCardID']    = $card_data['merchantCardID'];
						  $card['CardCVV']   = $this->encrypt->decode($card_data['CardCVV']);
						  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
				}		
					return  $card;
       }
 

	
	
	/******************************* Merchant Card *******************************************/
	
	  
 public function get_vault_data($cardID){
	                $this->load->library('encrypt');
					$card = array();
	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
		        
	  		     $dsn  ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name; 
			     
			    	$this->db1 = $this->load->database($dsn, true);        
			  
		     	 $sql = "SELECT * from merchant_card_data  where  merchantListID='$merchID'  order by merchantCardID desc limit 1  "; 
				
				 $query1 = $this->db1->query($sql);
				 $card_data = $query1->row_array();
			       if(!empty($card_data )){	
						 $card['CardNo']     = $this->encrypt->decode($card_data['MerchantCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['merchantCardID']    = $card_data['merchantCardID'];
						  $card['CardCVV']   = $this->encrypt->decode($card_data['CardCVV']);
						  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
				}		
 }
 
	public function delete_card_data(){
		    
	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
				
			   
		       
	  		    $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name;
		        $this->db1 = $this->load->database($dsn, true);   
			    $cardID =  $this->uri->segment('3'); 
		       $sts =  $this->db1->query("Delete from merchant_card_data where merchantCardID = $cardID ");
				 if($sts){
					$this->session->set_flashdata('success', 'Successfully Deleted');
		   	 
		 }else{
		 
		   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
		 }
		 
	    	redirect('Reseller_panel/merchant_list','refresh');
	}		
	  
	  public function update_card_data(){
	      
		 
	        
	            $this->load->library('encrypt');
			  
	   	 
	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
				
			   
		        
					
	  		        $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name;
		     		$this->db1 = $this->load->database($dsn, true);    
					
			;
					
			        	$merchantcardID = $this->czsecurity->xssCleanPostInput('edit_cardID');	
						$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('edit_cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
				        
								
			        $condition = array('merchantCardID'=>$merchantcardID);
		           	$insert_array =  array( 'CardMonth'  =>$expmonth,
										   'CardYear'	 =>$exyear, 
										  'MerchantCard' =>$this->encrypt->encode($card_no),
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										 'merchantFriendlyName'=>$friendlyname,
										 'updatedAt' 	=> date("Y-m-d H:i:s") );
                $this->db1->where($condition);
		    
			 $id = $this->db1->update('merchant_card_data',  $insert_array);
	   
		 if( $id ){
			$this->session->set_flashdata('success', 'Successfully Updated');
		    		 
		 }else{
		 
		   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
		 }
		 
	    	redirect('Reseller_panel/merchant_list','refresh');
	  }
	  
	 



	  
	  public function insert_new_data(){
	      
	        
	            $this->load->library('encrypt');
			  
	   	 
	   	    	$db_user = "devcz_quicktest";
				$db_pass = "ecrubit@123";
				$db_name = "devcz_chargezoom_card_db";  
				
			   
		        
	       
	  		        $dsn ='mysqli://' .$db_user. ':' .$db_pass. '@' . 'localhost'. '/' .$db_name;
		   
		     		   $this->db1 = $this->load->database($dsn, true);    
			        	$merchant = $this->czsecurity->xssCleanPostInput('merchID');
						
						
			        	
			        	if($this->session->userdata('reseller_direct_logged_in') ){
			              $resellerID = $this->session->userdata('reseller_direct_logged_in')['resellerID'];
						 
			        	}
			        	
						$card_no  = $this->czsecurity->xssCleanPostInput('card_number'); 
						$expmonth = $this->czsecurity->xssCleanPostInput('expiry');
						$exyear   = $this->czsecurity->xssCleanPostInput('expiry_year');
						$cvv      = $this->czsecurity->xssCleanPostInput('cvv');
						$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
				      
								
			
		           	$insert_array =  array( 'cardMonth'  =>$expmonth,
										   'cardYear'	 =>$exyear, 
										  'MerchantCard' =>$this->encrypt->encode($card_no),
										  'CardCVV'      =>$this->encrypt->encode($cvv), 
										 'merchantListID' =>$merchant, 
										 'resellerID'      => $resellerID, 	
									'merchantFriendlyName'=>$friendlyname,'createdAt' 	=> date("Y-m-d H:i:s"));
										 
									
		    	
		 $id = $this->db1->insert('merchant_card_data', $insert_array);
			
	    	
		 if( $id ){
			$this->session->set_flashdata('success', 'Successfully Created');
		     		 
		 }else{
		 
		   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
		 }
		 
	    	redirect('Reseller_panel/merchant_list','refresh');
	  }
	 
 	public function total_plan_amount()
	{   
    	$data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			 $rID                = $data['login_info']['resellerID'];
		 $plan_total = $this->reseller_panel_model->get_totalplan($rID);
		 echo json_encode($plan_total);
	}
	
	public function total_service_amount()
	{   
    	$data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			  $data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			 $rID                = $data['login_info']['resellerID'];
		 $s_total = $this->reseller_panel_model->get_totalservice($rID);
		 
		  echo json_encode($s_total);
	}
	
	public function reseller_merchant_chart(){
      	    $data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			 
			 $rID                = $data['login_info']['resellerID'];
			$get_result = $this->reseller_panel_model->chart_get_merchant($rID);

			  if ($get_result) {
			 
			   $result_set=array();
			   $result_value=array();

			   foreach ($get_result as $count_merch) {
			        $change = $count_merch->Month;
			        $month_val = $change - 1;
			      array_push($result_value, $month_val);
			
			       array_push($result_value, $count_merch->merchCount);
                  
			   }
			     
			   $opt_array =array_chunk($result_value, 2);
          
			   echo json_encode($opt_array);

   			   }
			}
			
	public function reseller_process_volume(){
      	    $data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			 $rID                = $data['login_info']['resellerID'];
			$get_result = $this->reseller_panel_model->chart_processing_volume($rID);
			  if ($get_result) {
			  
			   $result_set=array();
			   $result_value=array();

			   foreach ($get_result as $count_merch) {
			  
			      array_push($result_value, $count_merch->Month);
			      array_push($result_value, $count_merch->volume);

			   }
			   
			  
			   $opt_array =array_chunk($result_value, 2);
           
			   echo json_encode($opt_array);

   			   }
			}		
}