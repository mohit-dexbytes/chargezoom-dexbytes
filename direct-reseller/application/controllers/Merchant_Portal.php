<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Merchant_Portal extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
	
		delete_cache();
		$this->load->model('reseller_panel_model');
		
		
		  if($this->session->userdata('reseller_direct_logged_in')!="")
		  {
		   
		  }else if($this->session->userdata('agent_direct_logged_in')!="")
		  {
		  	$data['login_info']     = $this->session->userdata('agent_direct_logged_in');
        
            $user_id                = $data['login_info']['resellerIndexID'];

            $agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
            if($agentData['merchantPortal'] == 1){
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
                redirect('Reseller_panel/index', 'refresh'); 
            }
		 
		  }else{
		  
			redirect('login','refresh');
		  }
  
	}
	
	

	public function index()
	{   
		
	}
	
	public function setting_merchant_portal()
	{
		$data['isEditMode'] = 1; 
		if($this->session->userdata('reseller_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
			
			$resID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('agent_direct_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			if($agentData['merchantPortal'] == 2){
                $data['isEditMode'] = 0; 
            }
			$resID 				= $data['login_info']['resellerIndexID'];
		}	
		 
		    $codition               = array('resellerID'=>$resID);
		    $rootDomain             = RSDOMAIN;
		    
		    
		    dirname(FCPATH); 
	    	
		    
			if($this->input->post(null, true))
			{
				  $enable 		   =  $this->czsecurity->xssCleanPostInput('enable');
				  $portal_url 	   =  $this->czsecurity->xssCleanPostInput('portal_url'); 
				   $hepltext       =  $this->czsecurity->xssCleanPostInput('merchant_help_text');
				  
				  
				   $image ='';
				  
				   $url = "https://".$portal_url.".".$rootDomain."/";
				   
				   	if(!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)){
		          	 if(strpos($portal_url, ' ') > 0){
		          	     
    		      	 $this->session->set_flashdata('message','<div class="alert alert-danger"> Error:</strong> Invalid portal url space not allowed.</div>'); 
    		      	 	redirect('home/index', 'refresh'); 
    	             }
			      
		     	 $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error:</strong> Invalid portal url special character not allowed.</div>'); 
		     	 	redirect('home/index', 'refresh'); 
		        }
		       $config['upload_path'] = dirname(FCPATH).'/admin/uploads/reseller_logo/';  
		        if(!empty($_FILES['picture']['name']))
           {
	     
	           
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = time().$_FILES['picture']['name'];
                $config['max_size'] = 60;
                $config['quality'] =  '60%';
				$config['width'] = 200;
				$config['height'] = 50;

               
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                
                $this->upload->initialize($config);
                
               
                if($this->upload->do_upload('picture'))
                {
                     $uploadData = $this->upload->data();
                     $picture = ADMINURL.$uploadData['file_name'];
                   
                   
                   
                }else{
                
                    $error = array('error' => $this->upload->display_errors());
                    
                    print_r($error);
                    
                    $picture = ''; 
                    
                    
                }
                
                $input_data['resellerProfileURL']  = $picture;
                
                }
            
         
                
	       if(!empty($_FILES['pictureportal']['name']))
	       {
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['file_name'] = time().$_FILES['pictureportal']['name'];
                $config['max_size'] = 60;
                $config['quality'] =  '60%';
                $config['width'] = 300;
				$config['height'] = 100;
				
                //Load upload library and initialize configuration
                $this->load->library('upload',$config);
                
                $this->upload->initialize($config);
                
                if($this->upload->do_upload('pictureportal')){
                    $uploadData = $this->upload->data();
                  $pictureportal =ADMINURL.$uploadData['file_name'];
               
                }else{
                  
                    $pictureportal = '';
                }
                
                $input_data['ProfileURL']  = $pictureportal;
                
                }
                
			   
			  $insert_data= array(	'merchantPortal'=>$enable,'merchantPortalURL'=>$url,'merchantHelpText'=>$hepltext, 'resellerID'=>$resID, 'portalprefix'=>$portal_url  );
			  
			  $update_data= array(	'merchantPortal'=>$enable,'merchantPortalURL'=>$url,'merchantHelpText'=>$hepltext,  'portalprefix'=>$portal_url );
			  
            	$chk_condition = array('resellerID'=>$resID);
			    	
			    	if(!empty($input_data))
			    	{
			          $this->general_model->update_row_data('tbl_reseller',$chk_condition, $input_data);
			    	}
                         if($this->general_model->get_num_rows('Config_merchant_portal', array('resellerID'=>$resID))>0 ){
                   
                           $Edit = $this->general_model->check_existing_edit_portalprefix('Config_merchant_portal',$resID, $portal_url); 
				
				          if($Edit)
						{
						    
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> This portal URL already exitsts. Please try again.</div>');
						   
						}
						else{
						    	
						    
						 
						    $this->general_model->update_row_data('Config_merchant_portal',$codition ,$update_data);
						    $this->session->set_flashdata('success', 'Successfully Updated');
						   
						}
						
		            redirect(base_url('Merchant_Portal/setting_merchant_portal'), 'refresh');
			      
			   }
			   else
			   {
			       
			          $check = $this->general_model->check_existing_portalprefix($portal_url);
					 
					  if($check)
						{
							$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> This portal URL already exitsts. Please try again.</div>');
						   
						}
						else{
						   
					
			             $this->general_model->insert_row('Config_merchant_portal',$insert_data);
			             $this->session->set_flashdata('success', 'Successfully Inserted');
			          
			             
			             
						}
						 redirect(base_url('Merchant_Portal/setting_merchant_portal'), 'refresh');
			   }
			   
			   
			}
			
	   
	
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		$data['setting']        =  $this->general_model->get_row_data('Config_merchant_portal', $codition);
		
		$data['reseller']        =  $this->general_model->get_row_data('tbl_reseller', array('resellerID'=>$resID));
		
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/page_merchant_portal', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	
	
	 public function check_url(){
		$res =array();
		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
		if($this->session->userdata('reseller_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
		
			$resID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('agent_direct_logged_in');
		
			$resID 				= $data['login_info']['resellerIndexID'];
		}	
		  
    	
		if(!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)){
			 if(strpos($portal_url, ' ') > 0){
    	       
    			 $res =array('portal_url'=>'Space is not allowed in url', 'status'=>'false');
           echo json_encode($res);
    		die;	
    	    }
			
			$res =array('portal_url'=>'Special character not allowed in url', 'status'=>'false');
    		echo json_encode($res);
    		die;	
		}else{
			$codition   = array('portalprefix'=>$portal_url);
			$setting    =  $this->general_model->get_row_data('Config_merchant_portal', $codition);
			if(isset($setting['ConfigID'])){
				if($setting['resellerID'] == $resID ){
					$res =array('status'=>'true');
		    		echo json_encode($res);
				}else{
					$res =array('portal_url'=>'This Portal URL cannot used','status'=>'false');
		    		echo json_encode($res);
				}
		    	
			}else{
				$res =array('status'=>'true');
		    	echo json_encode($res);
			}

			
			
			
		    	
		die;
		    
		}
	 }		
}
