<?php

/**
 * Resellercron is used to create reseller billing for merchant
 * get_reseller_revenue is used for creating invoices at the first of the month 

 */


class ResellerCron extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('reseller_panel_model');		
		$this->load->model('general_model');		
	}
	
	
	public function index(){
	
	}

   
    public function get_reseller_revenue()
    {
        $rs_datas = $this->general_model->get_table_data('tbl_reseller',array('isDelete'=>'0'));
         
        if(!empty($rs_datas))
        {
            foreach($rs_datas as $rs_data)
            {
                $rID = $rs_data['resellerID'];
                $month =date('M-Y', strtotime(date('Y-m')." -1 month"));
            
                $mdata = $this->reseller_panel_model->get_month_revenue($rID, $month);
                
                $insert_data['resellerID'] = $rID;
                $insert_data['totalRevenue'] = $mdata['revenue'];
                $insert_data['totalVolume'] = $mdata['revenue'];
                $insert_data['newMerchant'] = $mdata['merchant'];
                $insert_data['revenueMonth'] =  date('Y-m-01',strtotime($mdata['month']));
                $insert_data['payoutDate'] = date('Y-m-d', strtotime(date('Y-m-d')." +20 days"));
                $insert_data['payStatus'] = 'Pending';
            
                $insert_data['createdAt'] = date('Y-m-d H:i:s');
                
                $id =$this->general_model->insert_row('tbl_reseller_revenue',$insert_data);
            }
             
        }
    }
    public function check_session()
    {
        if ($this->session->userdata('reseller_direct_logged_in') != "") {
            echo json_encode(array('status' => 'success')); die;
        } else if ($this->session->userdata('agent_direct_logged_in') != "") {
            echo json_encode(array('status' => 'success')); die;
        } else {
            echo json_encode(array('status' => 'failed')); die;
        }
       
    }

}