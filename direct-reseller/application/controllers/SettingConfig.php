<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use \Chargezoom\Core\Http\SessionRequest;
class SettingConfig extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('reseller_panel_model');
		delete_cache();
			  if($this->session->userdata('reseller_direct_logged_in')!="")
		  {
		   
		  }else if($this->session->userdata('agent_direct_logged_in')!="")
		  {
		 	
		  }else{
		  
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{   
		
	}
	
	
	
	
	public function create_template(){
	
		$data['isEditMode'] = 1;
       	if($this->session->userdata('reseller_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
		
			$rID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_direct_logged_in')){
			$data['login_info'] 	= $this->session->userdata('agent_direct_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			if($agentData['generalSettings'] == 2){
				$data['isEditMode'] = 0;
			}
			if($agentData['generalSettings'] == 1){
	            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
	            redirect('Reseller_panel/index', 'refresh'); 
	        }
			$rID 				= $data['login_info']['resellerIndexID'];
		}	
		 $user_id			    = $rID;
		 
	    if($this->czsecurity->xssCleanPostInput('templateName') !=""){
		
		  $templateName =  $this->czsecurity->xssCleanPostInput('templateName');
		  $type 	   =  $this->czsecurity->xssCleanPostInput('type'); 
		   $fromEmail  =  $this->czsecurity->xssCleanPostInput('fromEmail');
		    $toEmail   =  $this->czsecurity->xssCleanPostInput('toEmail');
		  $addCC 	   =  $this->czsecurity->xssCleanPostInput('ccEmail'); 
		   $addBCC 	   =  $this->czsecurity->xssCleanPostInput('bccEmail'); 
		    $replyTo   =  $this->czsecurity->xssCleanPostInput('replyEmail'); 
			$message   = $this->czsecurity->xssCleanPostInput('textarea-ckeditor', false);
			$subject   =  $this->czsecurity->xssCleanPostInput('emailSubject');
		  $createdAt   = date('Y-m-d H:i:s');
		  
		  if($this->czsecurity->xssCleanPostInput('add_attachment')){
		    $add_attachment = '1';  
		  }else{
		   $add_attachment = '0';  
		  }
		  
		  $insert_data = array('templateName'  =>$templateName,
		                        'templateType' => $type,
								 'companyID'   =>$user_id, 
								 'fromEmail'   => $fromEmail,
								 'toEmail'		=> $toEmail,
								 'addCC'       => $addCC,
								  'addBCC'		=> $addBCC,
								 'replyTo'       => $replyTo,
								 'message'		=>$message,
								 'emailSubject' => $subject,
								 'attachedTo'	=>$add_attachment
								
		             );
		if($this->czsecurity->xssCleanPostInput('tempID')!=""){
		           $insert_data['updatedAt']= date('Y-m-d H:i:s'); 
		  		   $condition         		= array('templateID'=>$this->czsecurity->xssCleanPostInput('tempID') );  
				   $data['templatedata']	= $this->general_model->update_row_data('tbl_eml_temp_reseller', $condition,$insert_data);
		}else{			 
				$insert_data['createdAt']= date('Y-m-d H:i:s'); 
		$id =  $this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data);	
		}
		redirect('Settingmail/email_temlate', 'refresh'); 
	   }
	 
		
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
	
		
		  if($this->uri->segment('3')){
		      $temID  				  = $this->uri->segment('3');	
    	      $condition              = array('templateID'=>$temID);  
			 $data['templatedata']	  = $this->general_model->get_row_data('tbl_eml_temp_reseller', $condition);	
			 
			 
			 
			 
	      }
		$data['types']  			  =	 $this->general_model->get_table_data('tbl_teplate_type','');	
		 
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/page_email_template', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	}
	


	 
	 function populate_state()
    {
		$this->load->model('reseller_panel_model');
        $id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->reseller_panel_model->get_table_data('state', array('country_id'=>$id));
        echo json_encode($data);
    }

    function populate_city()
    {
		$this->load->model('reseller_panel_model');
        $id = $this->czsecurity->xssCleanPostInput('id');
		$data = $this->reseller_panel_model->get_table_data('city', array('state_id'=>$id));
        echo json_encode($data);
    }
	
	
	 
	 
	 
	 
	  public function general_setting()
	{   

	$data['isEditMode'] = 1; 
	if($this->session->userdata('reseller_direct_logged_in')){
		$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
		
		$rID 				= $data['login_info']['resellerID'];
	}
	if($this->session->userdata('agent_direct_logged_in')){
		$data['login_info'] 	= $this->session->userdata('agent_direct_logged_in');
		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
		if($agentData['generalSettings'] == 2){
			$data['isEditMode'] = 0;
		}
		if($agentData['generalSettings'] == 1){
            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
            redirect('Reseller_panel/index', 'refresh'); 
        }
		$rID 				= $data['login_info']['resellerIndexID'];
	}	
		 $user_id			    = $rID;
	
	   if(!empty($this->input->post(null, true))){

			$email   = $this->czsecurity->xssCleanPostInput('resellerEmail');
			$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
			$LastName = $this->czsecurity->xssCleanPostInput('lastName');
	
			$input_data['resellerCompanyName']	   = $this->czsecurity->xssCleanPostInput('resellerCompanyName');
			$input_data['resellerfirstName']	   = $FirstName;
			$input_data['lastName']	               =  $LastName;
			$input_data['primaryContact']	   = $this->czsecurity->xssCleanPostInput('primaryContact');
			$input_data['billingfirstName']	   = $this->czsecurity->xssCleanPostInput('billingfirstName');
			$input_data['billinglastName']	   = $this->czsecurity->xssCleanPostInput('billinglastName');
			$input_data['billingContact']	   = $this->czsecurity->xssCleanPostInput('billingContact');
			$input_data['billingEmailAddress']	   = $this->czsecurity->xssCleanPostInput('billingEmailAddress');
			$input_data['federalTaxID']	   = $this->czsecurity->xssCleanPostInput('federalTaxID');
			$input_data['resellerAddress']	   = $this->czsecurity->xssCleanPostInput('resellerAddress');
			$input_data['resellerAddress2']	   = $this->czsecurity->xssCleanPostInput('resellerAddress2');
			$input_data['resellerCountry']	   = $this->czsecurity->xssCleanPostInput('resellerCountry');
			$input_data['resellerState']	   = $this->czsecurity->xssCleanPostInput('resellerState');
			$input_data['resellerCity']	   = $this->czsecurity->xssCleanPostInput('resellerCity');
			$input_data['zipCode']	   = $this->czsecurity->xssCleanPostInput('zipCode');
			$input_data['webURL']	   = $this->czsecurity->xssCleanPostInput('weburl');
		
		
			$input_data['date']	   = date('Y-m-d H:i:s');
		
	    
		
				    
				       
						 $chk_condition = array('resellerID'=>$rID);
						 $this->reseller_panel_model->update_row_data('tbl_reseller',$chk_condition, $input_data);
						 $this->session->set_flashdata('success', 'Successfully Updated');
		     	          redirect('SettingConfig/general_setting', 'refresh'); 
		                 
			   
		}				
    		
	
              $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
			 if($this->session->userdata('reseller_direct_logged_in')){
		$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
		
		$rID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_direct_logged_in')){
		$data['login_info'] 	= $this->session->userdata('agent_direct_logged_in');
		
		$rID 				= $data['login_info']['resellerIndexID'];
		}	
		 $user_id			    = $rID;
			
			  $con                = array('resellerID'=> $user_id);  
			 $data['reseller'] 	  = $this->reseller_panel_model->get_row_data('tbl_reseller', $con);
			 
			   $country = $this->reseller_panel_model->get_table_data('country','');
				$data['country_datas'] = $country;
				
				$state = $this->reseller_panel_model->get_table_data('state','');
				$data['state_datas'] = $state;
			  
			  $city = $this->reseller_panel_model->get_table_data('city','');
				$data['city_datas'] = $city;
			
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/page_general_settings', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	
}

	//-------------- To load the API Key page ---------------//
    public function apikey() 
    {
    	 
		$data['primary_nav']     = primary_nav();
		$data['template']        = template_variable();
		$data['isEditMode'] = 1;
    	if($this->session->userdata('reseller_direct_logged_in')){
			$data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
			$user_id = $data['login_info']['resellerID'];
		}else if($this->session->userdata('agent_direct_logged_in')){
			$data['login_info'] = $this->session->userdata('agent_direct_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			if($agentData['api'] == 2){
				$data['isEditMode'] = 0;
			}
			if($agentData['api'] == 1){
	            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
	            redirect('Reseller_panel/index', 'refresh'); 
	        }
			$user_id = $data['login_info']['resellerIndexID'];
		}		
    	
	    $data['api_data'] = $this->general_model->get_table_data('tbl_reseller_api',array('resellerID'=>$user_id));
	    $this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/APIKey', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
    }

    public function create_api_key() 
    {
           
        if(!empty($this->czsecurity->xssCleanPostInput('api_name')) && !empty($this->czsecurity->xssCleanPostInput('domain_name')))
        {
    		if($this->session->userdata('reseller_direct_logged_in')){
				$data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
				$user_id = $data['login_info']['resellerID'];
			}else if($this->session->userdata('agent_direct_logged_in')){
				$data['login_info'] = $this->session->userdata('agent_direct_logged_in');
				$user_id = $data['login_info']['resellerIndexID'];
			}
    	
    		$r_data = $this->general_model->get_select_data('tbl_reseller', array('resellerID,resellerCompanyName, resellerEmail'), array('resellerID'=>$user_id));
    	
    		$name   = $this->czsecurity->xssCleanPostInput('api_name');
    		$domain = $this->czsecurity->xssCleanPostInput('domain_name');
    		 
    		// dev key
    		$str = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,10);
	        $str = $str.$r_data['resellerCompanyName'].$r_data['resellerID'].microtime();
	   		$dev_key = sha1($str);

	   		//pro key
	        $str = substr(str_shuffle(str_repeat('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(1,10))),1,15);
	        $str = $str.$r_data['resellerCompanyName'].$r_data['resellerEmail'].$r_data['resellerID'].microtime();
	        $pro_key = sha1($str);
		    
		    $api_data['resellerID']= $user_id;
    		$api_data['APIKeyDev'] = $dev_key;
    		$api_data['APIKeyPro'] = $pro_key;
    		$api_data['AppName']   = $name;
    		$api_data['Domain']    = $domain;
    		$api_data['Status']    = 1;
    		$api_data['APIPackage']    = 0;
    		$indid = $this->general_model->insert_row('tbl_reseller_api',$api_data);
    		if($indid)
    		{
    		  	$this->session->set_flashdata('success','Successfully Created');
    		}	
        }else{
            $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');       
        }
        redirect('SettingConfig/apikey');
    }

    public function delete_api_key() 
    {
           
        if(!empty($this->czsecurity->xssCleanPostInput('apiID')))
        {
    		if($this->session->userdata('reseller_direct_logged_in')){
				$data['login_info'] = $this->session->userdata('reseller_direct_logged_in');
				$user_id = $data['login_info']['resellerID'];
			}else if($this->session->userdata('agent_direct_logged_in')){
				$data['login_info'] = $this->session->userdata('agent_direct_logged_in');
				$user_id = $data['login_info']['resellerIndexID'];
			}
			
		 	$apiID = $this->czsecurity->xssCleanPostInput('apiID');
		 	$m_data = $this->general_model->get_row_data('tbl_reseller_api',array('resellerID'=>$user_id,'apiID'=>$apiID));
		 
		 	if(!empty($m_data))
		 	{   
		    	if($this->general_model->delete_row_data('tbl_reseller_api',array('resellerID'=>$user_id,'apiID'=>$apiID)))
		    	{
		      		$this->session->set_flashdata('success','Success');    
		    	}else{
		        	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error</strong></div>');    
		    	}
         	}else{
              	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Error, Invalid API Key</strong></div>');    
         	}
        }else{
        	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Validation Error, Please fill the required fields</strong></div>');         
        }
      
        redirect('SettingConfig/apikey');
    }

	/**
	 * Function to change the password of reseller agent
	 * 
	 * @return void
	 */
	public function changePassword() : void
	{

		if ($this->session->userdata('agent_logged_in')) {
			$session	= $this->session->userdata('agent_logged_in');
			$userID = $session['ragentID'];
		} else {
			$this->session->set_flashdata('error', 'Invalid Request');
			redirect(base_url('Reseller_panel/index'));
		}

		if (!empty($this->input->post(null, true))) {
			$oldPassword     = $this->czsecurity->xssCleanPostInput('user-settings-currentpassword');
			$newPassword = password_hash(
				$this->czsecurity->xssCleanPostInput('user-settings-password'),
				PASSWORD_BCRYPT
			);

			$agentData = $this->general_model->get_row_data('tbl_reseller_agent', array('ragentID' => $userID));
			if (!empty($agentData)) {
				$isValid = true;
				if ($agentData['agentPasswordNew'] !== null) {
					if (!password_verify($oldPassword, $agentData['agentPasswordNew'])) {
						$isValid = false;
					}
				} else if (md5($oldPassword) != $agentData['agentPassword']) {
					$isValid = false;
				}

				if($isValid){
					$this->general_model->update_row_data('tbl_reseller_agent', array('ragentID' => $userID), array('agentPassword' => null, 'agentPasswordNew' => $newPassword));
					/* Destroy all login session */
					$sessionRequest     = new SessionRequest($this->db);
					$updateSession = $sessionRequest->destroySessionUser(SessionRequest::USER_TYPE_AGENT,$userID);
					/*End destroy session*/
					$this->session->set_flashdata('success', 'Password has been changed successfully');
				} else {
					$this->session->set_flashdata('error', 'Please enter correct current password');
				}
			} else {
				$this->session->set_flashdata('error', 'Please enter correct current password');
			}
		} else {
			$this->session->set_flashdata('error', 'Invalid Request');
		}
		redirect(base_url('Reseller_panel/index'));
	}
}