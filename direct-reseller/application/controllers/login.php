<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use \Chargezoom\Core\Http\LoginRequest;
use \Chargezoom\Core\Authentication\Throttle;
use \Chargezoom\Core\Http\SessionRequest;

class Login extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->library('form_validation'); 
		$this->load->model('reseller_login_model');
    	$this->load->model('general_model');

		define('INTERFACE_TYPE','Reseller');
		if($this->session->userdata('reseller_direct_logged_in'))
		{
	
		}

	}
	
	public function index()
	{
	  
	   
	    
		
	    
	    
		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['page']           = 'login';
		
     	$this->load->view('template/template_start', $data);
		$this->load->view('reseller_pages/login', $data);
		$this->load->view('template/template_end', $data);
	}
	


function enable_reseller(){
		
	  $code = $this->uri->segment('3');
	 
	 
	  if($code!=""){
	
		  $res = $this->general_model->get_row_data('tbl_reseller',array('loginCode'=> $code, 'isEnable'=>'0' )) ;	
		  if(!empty($res)){
			  
			  $ins_data = array('loginCode'=>'', 'isEnable'=>'1');
			  
			  $this->general_model->update_row_data('tbl_reseller',array('loginCode'=> $code, 'isEnable'=>'0' ), $ins_data);
			  	$this->session->set_flashdata('success', 'Thanks for verifying your email id. You may login now.');
			  	
			 
			  redirect(base_url('login/login'), 'refresh');
		  }else{
			$this->session->set_flashdata('success', 'Thanks for verifying your email id. You may login now.');
		      
		      
		        redirect(base_url('login/login'), 'refresh');
		  }
			
	  }	
	  
	}	



	public function user_login()
	{
		if (!$this->czsecurity->csrf_verify()) {
			$this->session->set_flashdata('error', $this->czsecurity->errorMessages('token_expired'));

			redirect(base_url('login/login', 'refresh'));
		}

		$email    	= $this->czsecurity->xssCleanPostInput('login-email');
		$password 	= $this->czsecurity->xssCleanPostInput('login-password');
		$type     	= $this->czsecurity->xssCleanPostInput('type');
		$user 		= $this->reseller_login_model->get_user_login($email, $password, $type);
		$request 	= new LoginRequest($email);
		$throttle 	= new Throttle($this->db, $request);
		$sessionRequest 	= new SessionRequest($this->db);
		try {
			$seconds = $throttle->checkLock();

			if ($seconds) {
				$this->session->set_flashdata(
					'error',
					'You have exceeded the maximum login attempts. You can retry again after ' . $seconds . ' seconds.'
				);

				redirect(base_url('login/login', 'refresh'));
			}
		} catch (\Exception $ex) {
			// For the moment... silence the exception
			// In the future - pass exception to Sentry
		}

		if (!empty($user)) {
			if ($user['isSuspend'] == '0') {
				try {
					$throttle->addAttempt(true);
				} catch (\Exception $ex) {
					// For the moment... silence the exception
					// In the future - pass exception to Sentry
				}

				if ($user['login_type']=='RESELLER') {
					$this->general_model->track_user( "Reseller", $user['resellerEmail'], $user['resellerID']);

					$this->session->set_userdata('reseller_direct_logged_in', $user);
					$this->session->set_flashdata('message', '');

					$result_tmps = $this->general_model->get_table_data(
						'tbl_template_reseller_data',
						array('userTemplate' => 2)
					);
					/* Update session record */
					$updateSession = $sessionRequest->updateSessionUser(SessionRequest::USER_TYPE_RESELLER,$user['resellerID'],$this->session->userdata('session_id'));
					/*End session update*/

					foreach ($result_tmps as $res) {
						$insert_data = array(
							'templateName'	=> $res['templateName'],
							'templateType' 	=> $res['templateType'],
							'status'		=> '1',
							'emailSubject' 	=> $res['emailSubject'],
							'createdAt'		=> date('Y-m-d H:i:s'),
							'message'		=> $res['message'],
							'resellerID'	=> $user['resellerID'],
						);

						$res_num = $this->general_model->get_num_rows(
							'tbl_eml_temp_reseller',
							array(
								'resellerID' => $user['resellerID'],
								'templateType' => $res['templateType']
							)
						);

						if ($res_num == 0) {
							$this->general_model->insert_row('tbl_eml_temp_reseller', $insert_data);
						}
					}

					redirect(base_url('Reseller_panel/index', 'refresh'));
				} else {
					$this->general_model->track_user("Agent", $user['agentEmail'], $user['ragentID']);

					$this->session->set_userdata('agent_direct_logged_in', $user);
					$this->session->set_flashdata('message', '');
					/* Update session record */
					$updateSession = $sessionRequest->updateSessionUser(SessionRequest::USER_TYPE_AGENT,$user['ragentID'],$this->session->userdata('session_id'));
					/*End session update*/

					redirect(base_url('Reseller_panel/index', 'refresh'));
				}
			} else {
				$this->session->set_flashdata('error', 'Your Account has been suspended.');

				redirect(base_url('login/login','refresh'));
			}
		} else {
			try {
				$throttle->addAttempt(false);
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - throw exception to Sentry
			}

			$this->session->set_flashdata('login-error', 'Login credentials do not match.');

			redirect(base_url('login/login','refresh'));
		}

		redirect(base_url('login/user_login'));
	}
	
	
	 
	public function user_logout()
	{
		 redirect(base_url('login/user_login'));
	}




	public function recover_password() 
    {
		if(!$this->czsecurity->csrf_verify())
		{
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error: </strong>Token expired !!</div>');
			redirect('login#reminder', 'refresh');
		}

		$email  = $this->czsecurity->xssCleanPostInput('reminder-email');
		$resetType  = $this->czsecurity->xssCleanPostInput('type');
		if($resetType == 1){
			$tableReset = "tbl_reseller";
			$conditionEmail = array('resellerEmail'=> $email );
		} else {
			$tableReset = "tbl_reseller_agent";
			$conditionEmail = array('agentEmail'=> $email );
		}
        
		$result = $this->general_model->get_row_data("$tableReset", $conditionEmail );
		if($result)
		{
			$request 	= new LoginRequest($email);
			$throttle 	= new Throttle($this->db, $request, 0, 3600);
			try {
				$attemptCount = $throttle->getForgetAttemptCount(INTERFACE_TYPE);
				if($attemptCount >= 3){
					$this->session->set_flashdata('error', 'Password Reset request limit reached. Request has been locked for an hour.');
					redirect('/', 'refresh');
				}
				
			} catch (\Exception $ex) {
				// For the moment... silence the exception
				// In the future - pass exception to Sentry
			}

			$this->load->helper('string');
			$newPassword = random_string('alnum',10);

			if($resetType == 1){
				$fullName = $result['resellerfirstName'] . " " . $result['lastName'];
			} else {
				$fullName = $result['agentName'];
			}

			#Email Template Data
			$template_data	  = $this->general_model->get_row_data('tbl_template_reseller_data',array('userTemplate'=>1, 'templateType' => 2));
			if(empty($template_data['fromEmail'])){
				$template_data['fromEmail'] = FROM_EMAIL;
			}

			$template_data['fromName'] = '';

			$loginURL = 'https://manage.'.RSDOMAIN;
			$loginURL = "<a href='$loginURL' target='_blank'>$loginURL</a>";

			#Subject Data alteration
			$subject = $template_data['emailSubject'];
			$subject = stripslashes(str_replace('{{reseller_name}}', $fullName, $subject));

			#Message Data alteration
			$message = $template_data['message'];
			$message = stripslashes(str_replace('{{reseller_name}}', $fullName, $message));
			$message = stripslashes(str_replace('{{reseller_email}}', $email, $message));
			$message = stripslashes(str_replace('{{reseller_password}}', $newPassword, $message));
			$message = stripslashes(str_replace('{{login_url}}', $loginURL, $message));
			$message = stripslashes(str_replace('{{admin_name}}', ucfirst(ENCRYPTION_KEY), $message));

			$args = $template_data;
			$args['subject'] = $subject;
			$args['message'] = $message;
			$args['toEmail'] = $email;
		
			$sendMail = sendgridSendMail($args);
			if($sendMail['httpcode'] == '202' || $sendMail['httpcode'] == '200'){
				if($resetType == 1){
					$update = $this->reseller_login_model->temp_reset_password($newPassword,$email);
				} else {
					$update = $this->general_model->update_row_data(
					    $tableReset,
                        $conditionEmail,
                        [
                            'agentPassword'     => null,
                            'agentPasswordNew'  => password_hash($newPassword, PASSWORD_BCRYPT)
                        ]
                    );
				}

				try {
					$throttle->addForgetAttempt(INTERFACE_TYPE);
				} catch (\Exception $ex) {
					// For the moment... silence the exception
					// In the future - pass exception to Sentry
				}

				#Create Log on Password Update
				$log_session_data = [
					'session' => $this->session->userdata,
					'http_data' => $_SERVER
				];
				
				$logData = [
					'request_data' => json_encode($this->input->post(null, true), true),
					'session_data' => json_encode( $log_session_data, true),
					'executed_sql' => $this->db->last_query(),
					'log_date'	   => date('Y-m-d H:i:s'),
					'action_interface' => 'Direct Reseller - Forgot Password',
					'header_data' => json_encode(apache_request_headers(), true),
				];
				$this->general_model->insert_row('merchant_password_log', $logData);

				#Log scripts end here

				$this->session->set_flashdata('forget-success', 'Send');
			}
			else
			{
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email was not sent, please contact your administrator.</div>');
			}			
			redirect('login', 'refresh');
		}
		else
		{
		   $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email not available.</div>'); 
		   redirect('login#reminder', 'refresh');
		}
	
	}
	
	public function changePass()
    {
		$session_data = $this->session->userdata('reseller_direct_logged_in');
		$id = $session_data['resellerID'];
	   
		$query = $this->reseller_login_model->savenewpass($id, $this->czsecurity->xssCleanPostInput('user-settings-password') );			
		$message = 'Password Successfully Updated...' ;
		$this->session->set_flashdata('success', $message);
		redirect('Reseller_panel/index','refresh');
	}
	   
}
 