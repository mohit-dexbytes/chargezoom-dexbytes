<?php

function sendgridSendMail($args){

    $request_array = [
        "personalizations" => [[
            "to" => [
                [
                "email" => $args['toEmail']
                ]
            ],
            "subject" => $args['subject']
        ]],
        "from" => [
            "email" => $args['fromEmail'],
            "name" => $args['fromName']
        ],
        "content" => [[
            "type" => "text/html",
            "value" => $args['message']
        ]],
    ];

    if(isset($args['addCC']) && !empty($args['addCC'])){
        $addCC = $args['addCC'];
        if(!is_array($addCC)){
            $addCC = explode(';', $addCC);
        }

        $toaddCCArrEmail = [];
        foreach ($addCC as $value) {
            $toaddCCArrEmail[]['email'] = trim($value);
        }
        $request_array['personalizations'][0]['cc'] = $toaddCCArrEmail;
    }

    if(isset($args['addBCC']) && !empty($args['addBCC'])){
        $addBCC = $args['addBCC'];
        if(!is_array($addBCC)){
            $addBCC = explode(';', $addBCC);
        }

        $toaddBCCArrEmail = [];
        foreach ($addBCC as $value) {
            $toaddBCCArrEmail[]['email'] = trim($value);
        }
        $request_array['personalizations'][0]['bcc'] = $toaddBCCArrEmail;
    }
    
    if(!empty($args['replyTo'])){
        $request_array['reply_to'] = 	[
            "email" => $args['replyTo']
        ];
    }
    
    $url = 'https://api.sendgrid.com/v3/mail/send';

    // set authorization header
    $headerArray = [
        'Authorization: Bearer '.SENDGRID_APIKEY,
        'Content-Type: application/json',
        'Accept: application/json'
    ];

    $ch = curl_init($url);
    curl_setopt ($ch, CURLOPT_POST, true);
    curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

    // add authorization header
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

    $response = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    // parse header data from response
    $header_text = substr($response, 0, $header_size);
    // parse response body from curl response
    $body = substr($response, $header_size);

    return [
        'response' => $response,
        'header_size' => $header_size,
        'httpcode' => $httpcode,
        'header_text' => $header_text,
        'body' => $httpcode,
    ];
}

?>