<?php
	function mymessage($message)
	{
		echo '
		<div class="alert alert-info">
			<button data-dismiss="alert" class="close" type="button">×</button> '.$message.'
		</div>
		';
	}

 
	function get_services()
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_table('*', 'services');
		return $service;
	}


	function get_services_by_id($id)
	{
		$CI =& get_instance();
		$CI->load->model('general_model');
		$service = $CI->general_model->get_allservices_by_id($id);
		return $service;
	}	 

	function getGatewayNames($gatewayType){
		$gateway = 'Unknown';
		switch ($gatewayType) {
			case 1:
				$gateway = 'NMI';
				break;
			case 2:
				$gateway = 'Authorize.Net';
				break;
			case 3:
				$gateway = 'Pay Trace';
				break;
			case 4:
				$gateway = 'Paypal';
				break;
			case 5:
				$gateway = 'Stripe';
				break;
			case 6:
				$gateway = 'USAePay';
				break;
			case 7:
				$gateway = 'Heartland';
				break;
			case 8:
				$gateway = 'Cybersource';
				break;
			case 9:
				$gateway = 'Chargezoom';
				break;
			case 10:
				$gateway = iTransactGatewayName;
				break;
			case 11:
				$gateway = FluidGatewayName;
				break;
			case 12:
				$gateway = TSYSGatewayName;
				break;
			case 13:
				$gateway = BASYSGatewayName;
				break;
			case 14:
				$gateway = 'CardPointe';
				break;
			case 15:
				$gateway = PayArcGatewayName;
			break;
			case MAVERICKGATEWAYID:
				$gateway = MaverickGatewayName;
				break;
			case 16:
				$gateway = EPXGatewayName;
				break;
			default:
				$gateway;
				break;
		}
		return $gateway;
	}

	function delete_cache($uri_string=null)
	{
		$CI =& get_instance();
		$path = $CI->config->item('cache_path');
		$path = rtrim($path, DIRECTORY_SEPARATOR);

		$cache_path = ($path == '') ? APPPATH.'cache/' : $path;

		$uri =  base_url($uri_string);
		$cache_path .= md5($uri);

		if (file_exists($cache_path)){
			return unlink($cache_path);
		} else {
			header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
			header("Cache-Control: post-check=0, pre-check=0", false);
			header("Pragma: no-cache");
		}

		return true;
	}

	function generateRandomPassword()
	{
		$chars = "abcdefghijklmnpqrstuvwxyzABCDEFGIHJKLMNPQRSTUVWXYZ123456789!@#$%^&*-_";
	    $password = substr(str_shuffle($chars), 0, 8);
	    return $password;
	}
	function getAgentDataByAgentID($agentID)
	{
		$CI =& get_instance();
		$CI->load->model('reseller_panel_model');
		$data = $CI->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $agentID]);
		return $data;
	}
	
	
function getClientIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      	$ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      	$ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}
function getTableRowLengthValue($length)
{
	$returnValue = 10;
	if($length >= 0){
		$returnValue = $length;
	}
	return $returnValue;
}
function getTableRowOrderColumnValue($columnNumber)
{
	$returnValue = 0;
	if($columnNumber >= 0){
		$returnValue = $columnNumber;
	}
	return $returnValue;
}
function getTableRowStartValue($start)
{
	$returnValue = 0;
	if($start >= 0){
		$returnValue = $start;
	}
	return $returnValue;
}
function getTableRowOrderByValue($orderBy)
{
	$returnValue = 'desc';
	if($orderBy == 'desc' || $orderBy == 'asc'){
		$returnValue = $orderBy;
	}
	return $returnValue;
}