<style>
.hideInfo{
    display: none;
}
.showInfo{
    display: block;
}
.margBtn{
    margin-left: 2%;
}

    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none;
    opacity:0.6;        
    
    }
    .quick_status > .quick_1 {
    padding-left: 0 !important;
    margin-bottom: 20px !important;
}
.card-body-icon {
    position: unset !important;
    background: #FFF;
    float: left;
    border-radius: 50px;
    width: 40px;
    height: 40px;
    font-size: 5rem;
    transform: unset !important;
}
@media only screen and (max-width: 1637px) and (min-width: 1200px){
    .card-body-icon {
        width: 40px !important;
        height: 40px !important;
        margin-top: 5px;
    }
}
.card-body-icon {
    display: inline-flex;
    align-items: center;
}

@media only screen and (max-width: 1637px) and (min-width: 1200px){
.card-body-icon .fa {
    font-size: 2rem;
}
}
.card-body-icon .fa-money {
    color: #28a745;
}
.card-body-icon .fa {
    text-align: center;
    margin: 0 auto;
}
.card {
    position: relative;
    display: flex;
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    flex-direction: column;
    min-width: 0px;
    overflow-wrap: break-word;
    background-clip: border-box;
    border-width: 1px;
    border-style: solid;
    border-color: rgba(0, 0, 0, 0.125);
    border-image: initial;
    border-radius: 0.25rem;
}
@media only screen and (max-width: 1637px) and (min-width: 1200px){
    .card-content h4 {
        font-size: 1.25rem;
        margin-top: 0.7rem !important;
    }
}
.card-content h4 {
    font-size: 1.25rem;
    font-weight: 600;
    margin-bottom: 0;
    margin-top: 0.1rem;
}
.card-content {
    margin-left: 1.5rem;
}
.card-footer {
    padding: 0px !important;
    background-color: transparent !important;
    border-top: none !important;
    font-size: 2rem !important;
}
.card-body-icon .fa-credit-card {
    color: #007bff;
}
.quick_status > .quick_2 {
    padding-right: 0 !important;
    margin-bottom: 20px !important;
}
.dataTables_wrapper {
    background-color: #fff !important;
}
.dataTables_wrapper > div {
    background-color: #f2f2f2 !important;
    border: none !important;
}
.dataTables_wrapper>.row >.col-sm-6.col-xs-5 {
    padding-left: 0px !important;
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    padding-right: 0px !important;
}
.form-control {
    border-color: #e8e8e8!important;
    font-size: 13px;
}
.table thead > tr > th {
    font-size: 13px;
    font-weight: 600;
}
.dataTables_wrapper > div:last-child {
    background-color: #fff !important;
    border-top: 2px solid #f2f2f2 !important;
}
.dataTables_filter>label>.input-group>span.input-group-addon {
    background-color: #A5A5A5 !important;
}
.dataTables_filter>label>.input-group>span.input-group-addon>.fa-search {
    color: #fff !important;
}
.btn-edit-merc{
    background-color: #808080 !important;
    border-color: #808080 !important;
    margin-right: 5px;
    color: #ffffff !important;
}
.btn-edit-merc:hover{
    background-color: #808080 !important;
    border-color: #808080 !important;
    color: #ffffff !important;
}
.btn-login-merc{
    margin-left: 5px;
}
.search_head{
 
}
.btn-red{
   color: #000;
}
.btn-green{
  color: #000;
}
.status_filter {
    width: 100px;
    position: absolute;
    top: 8px;
    left: 68px;
    z-index: 1;
}
.card-body-icon .fa {
    font-size: 25px;
}
.linkMerchant{
    text-decoration: none!important;
    color: #000;
}
.linkMerchant:hover{
    cursor: auto;
}
.manageBlockPadding{
    padding: 2px 0px !important;
}
.manageBlockPadding .block-section{
    padding: 40px 10px 25px 10px;
    margin-bottom: 0!important;
}
.btnGroupMerchant{
    background: #f9f9f9;
    padding: 10px 0px 10px 0px;
    text-align: center;
}
.billinBtn{
    margin-left: 5px;
}

.manageh3 {
    font-size: 16px !important;
    margin-top: 30px !important;
    margin-bottom: 30px !important;
}
.alignText{
    text-align: left !important;
    padding-left: 15px;
}
.customCheckLabel{
    position: relative;
    bottom: 2px;
    left: 5px;
}
div#manageCustomCheck {
    width: 100%;
}
.faCopyPassword{
    position: absolute;
    right: -43px;
    top: 1px;
    font-size: 22px;
    border: 1px solid #ccc;
    padding: 4%;
    background: #d6d1d138;
    color: #8a8383;
    cursor: pointer;
    border-radius: 5%;
}


 .tooltiptext {
    visibility: hidden; 
    width: auto;
    background-color: #555;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 113%;
    right: -37%;
    margin-left: -75px;
    opacity: 0;
    transition: opacity 0.3s;
}

.tooltiptext::after {
  content: "";
  position: absolute;
  top: 100%;
  left: 50%;
  margin-left: -5px;
  border-width: 5px;
  border-style: solid;
  border-color: #555 transparent transparent transparent;
}

.ttp:hover .tooltiptext{
  visibility: visible;
  opacity: 1;
}
@media only screen and  (max-width: 1024px){
    #newGeneratePassword{
        width: 89%;
        margin-left: 2%;
    }
    div#manageCustomCheck {
        margin-left: 2%;
    }
    .tooltiptext {

        right: -12%;

    }
    .faCopyPassword {
        right: 12px;
        padding: 9px;
    }
}
.manageBlockPadding .table{
    word-break: break-word;
}
.alignTxnGrp{
    position: relative;
    left: 206px;
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    left: 66px !important;
}
</style>

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
             	
      <div class="row">
      
      <div class="col-lg-4">
            <!-- Customer Info Block -->
			<legend class="leg">Merchant Details</legend>
			
            <div class="block manageBlockPadding">
                <!-- Customer Info Title -->
                
                <!-- END Customer Info Title --> 

                <!-- Customer Info -->
                <div class="block-section text-center">
                    
                   
				
                   
                    <?php   
                     $url = base_url().'Reseller_panel/create_merchant/'; 
                     $login_url = base_url().'Reseller_panel/merchant_login/';
                     $dis_class= '';
                    if($login_info['login_type']=='RESELLER'  || (  
                        $login_info['login_type']=='AGENT' && ( $isEditMode ) )){ $st1='';   $ed_url =$url.$merchants->merchID ;  }else{  $st1='disabled="disabled"'; $dis_class= 'disabled';   $ed_url='javscript:void(0)'; }    
                         ?>
                    <div class="logoSection">
                        
                        <img class="imgLogo" width="200px" src="<?php if(isset($setting) && !empty($setting['ProfileImage'])){ echo  $resellerPortal.''.LOGOURL.$setting['ProfileImage'];   }else{ echo CZLOGO; } ?>" >
                    </div>       
                    <h3 class="manageh3">
                        <label class="linkMerchant" ><strong ><?php echo $merchants->companyName; ?></strong></label>
                    </h3>      
                    <a href="<?php echo $ed_url; ?>" <?php  echo $st1; ?> class="btn pull-lft btn-sm btn-default btn-edit-merc" >Edit Merchant</a>
                    <a id="resetPasswordBtn" href="#reset_password_merchant" <?php  echo $st1; ?> class="btn btn-sm btn-warning btn-reset-password-merc" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Reset Password</a>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                        
                        <tr>
                            <td class="text-right"><strong>Primary Contact</strong></td>
                            <td><?php echo $merchants->firstName.' '.$merchants->lastName; ?></td>
                        </tr>

                        
            
                        <tr>
                            <td class="text-right"><strong>Email</strong></td>
                            
                            <td><?php echo $merchants->merchantEmail; ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Phone</strong></td>
                            <td><?php echo $merchants->merchantContact; ?></td>
                        </tr>

                        <tr>
                            <td class="text-right"><strong>Status</strong></td>
                            <td>
                                <?php
                                if($merchants->isEnable == 0){
                                ?>

                                    Disable
                                    
                                <?php
                                    }else{
                             ?>
                              Active
                                <?php    
                                }
                                ?>
                                </td>
                        </tr>
                        
						
						
                    </tbody>
                </table>
                <!-- END Customer Info -->
                <div class="btnGroupMerchant">
                    <a href="<?php echo $login_url.$merchants->merchID; ?>" <?php  echo $st1; ?> class="btn pull-lft btn-sm btn-success btn-login-merce"  >Login as Merchant</a>

                    <?php
                    if($login_info['login_type']=='AGENT'){
                            $prev=  $login_info['previledge']; 
                    }
                    if($login_info['login_type']=='RESELLER'  || ( $login_info['login_type']=='AGENT' && ( 
                    $prev=='FullAdmin' || $prev=='EditAll' ||  (isset($login_info['merchant_account']) && $login_info['merchant_account']=='SELF')  ) ))
                    {  
                        $st1='';  $cd='#card_data_process';
                    } 
                    else
                    { 
                        $st1='disabled="disabled"'; $cd= '';
                    }  




                    if($login_info['login_type']=='AGENT'){
                        $prev=  $login_info['previledge']; 
                    }
                    if($login_info['login_type']=='RESELLER'  || ( $login_info['login_type']=='AGENT' && ( 
                    $prev=='FullAdmin' || $prev=='ViewAll' ||  (isset($login_info['merchant_account']) && $login_info['merchant_account']=='SELF')  ) ))
                     {  
                         $st=''; $mg='#card_edit_data_process';
                         
                     }
                     else
                     {
                         $st='disabled="disabled"'; $mg=''; 
                         
                     }
                    ?>
                    <a href="<?php echo $mg; ?>" class="btn btn-sm btn-info billinBtn" id="setbtn"  <?php echo $st; ?>  onclick="set_card_user_data('<?php  echo $merchants->merchID; ?>');"    data-backdrop="static" data-keyboard="false" data-toggle="modal">Billing Info</a>
                </div>
   			</div>
   			
			 
           
			
        </div>
        
        
        <div class="col-lg-8">
        <legend class="leg">Month to Date</legend>
        <div class="quick_status">
        <div class="col-sm-6 col-lg-6 quick_1">
        	<div class="card text-white bg-success o-hidden h-100">
            	<div class="card-body card-padding">
					<div class="card-body-icon">
						<i class="fa fa-money"></i>
				  	</div>
               		<div class="card-content pull-left">
						<h4 class="widget-content-light">Volume</h4>
						<div class="card-footer text-white clearfix z-1">
							<span class="float-left">
								<strong> 
                                    $<?php echo ($amount['applied'])?number_format((float)$amount['applied'],2):'0.00'; ?>
								</strong>
							</span>
							
						</div>
					</div>
            	</div>
          	</div>
        </div>
        
            



        <div class="col-sm-6 col-lg-6 quick_2">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body card-padding">
              <div class="card-body-icon">
                <i class="fa fa-credit-card"></i>
              </div>
				<div class="card-content pull-left">
					 <h4 class="widget-content-light">Transactions</h4>
					 <div class="card-footer text-white clearfix z-1">
						<span class="float-left"><strong><?php echo ($amount['TxnCount'])?$amount['TxnCount']:'0'; ?></strong></span>
						
            		 </div>
				</div>
            </div>
          </div>
        </div>
        
       
            
        </div>
        </div>
        <div class="col-lg-8">
           
       
            <!-- Orders Block -->
            <legend class="leg">Customers</legend>
           
                <!-- Orders Title -->
           <div class="block-title"></div>
		  <table id="compamount" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class=" text-left">Customer Name</th>
                    <th class=" text-left">Full Name</th>
                   <th class="text-right hidden-xs">Processed</th>
                   <th class="text-right hidden-xs">Transactions</th>
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($customers) && $customers)
				{
					foreach($customers as $customer)
					{
				?>
				<tr>
					
				
				
				<td class="text-left"><?php echo $customer['customerFullName']; ?></td>
					
					<td class="text-left"><?php echo $customer['customerFirstName'].' '.$customer['customerLastName']; ?></td>
				
				
					<td class="text-right hidden-xs"><strong>$<?php echo  ($customer['Payment'])?number_format((float)$customer['Payment'] ,2):'0.00'; ?></strong></td>
					<td class="text-right hidden-xs"><?php echo $customer['totalTxn']; ?></td>
				</tr>
				
				<?php } } 
				    else { echo'<tr><td colspan="3"> No Records Found </td>
                        <td style="display: none"></td>
                        <td style="display: none"></td>
                        <td style="display: none"></td></tr>'; } 
				    ?>
				
			</tbody>
        </table>
              <br>
            
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <legend class="leg">Transactions</legend>
            
                <!-- Orders Title -->
         
		            <div class="alignTxnGrp" >  
                        <form class="filter_form" action="<?php echo base_url();?>Reseller_panel/merchant_details/<?php echo $merchants->merchID; ?>" method="post">
                            <select class="form-control status_filter" id='status_filter' name="status_filter">
                                    
                                    <option value="all" <?php if($filter == 'all'){ echo 'Selected';}?>>All</option>
                                    <option value="paid" <?php if($filter == 'paid'){ echo 'Selected';}?>>Paid</option>
                                    <option value="refund" <?php if($filter == 'refund'){ echo 'Selected';}?>>Refund</option>
                                    <option value="failed" <?php if($filter == 'failed'){ echo 'Selected';}?>>Failed</option>
                                    
                            </select>
                         </form>
                    </div>
            		<table id="transaction" class="table table-bordered table-striped table-vcenter">
                            <thead>
                            <tr>
                                <th class="text-left hidden-xs">Transaction ID</th>
                                
                                 <th class="text-right">Amount</th>
            					 
                                
                                <th class="text-right">Date </th>
                                 
                                <th class="text-center">Status</th>
                               
                                
                            </tr>
                        </thead>
                        <tbody>
            			
            				<?php 
            				if(isset($transactions) && $transactions)
            				{
            					foreach($transactions as $transaction)
            					{
            				?>
            				<tr>
            					<td class="text-left"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:''; ?></td>
            					

            					<td class="text-right">
                                    <?php
                                    if(isset($transaction['creditTransactionID']) && $transaction['creditTransactionID'] != ''){
                                         echo '-$' . number_format($transaction['transactionAmount'], 2);
                                    }else{
                                        if ( strtolower($transaction['transactionType'])  == 'refund' || strtolower($transaction['transactionType']) == 'pay_refund' || strtolower($transaction['transactionType']) == 'credit' || strtolower($transaction['transactionType']) == 'stripe_refund' || strtolower($transaction['transactionType']) == 'paypal_refund' ) {
                                            echo '-$' . number_format($transaction['transactionAmount'], 2);
                                        }  else {
                                            echo '$';
                                            echo ($transaction['transactionAmount'])? number_format((float)$transaction['transactionAmount'],2):'0.00';
                                        }

                                    }
                                    
                                    ?>


                                    
                                        
                                </td>
            					
            				    
            					<td class=" text-right"><label class="hidden"><?php echo $transaction['transactionDate']; ?></label><?php echo date('M d, Y', strtotime($transaction['transactionDate'])); ?></td>
            						
            					<td class="text-center">
            					 <?php if($transaction['transactionCode']=='100' || $transaction['transactionCode']=='111' || $transaction['transactionCode']=='120' || $transaction['transactionCode']=='200'|| $transaction['transactionCode']=='1'){
                                    if(isset($transaction['creditTransactionID']) && $transaction['creditTransactionID'] != ''){
                                         echo '<span class="btn-green remove-hover">Refund</span>';
                                    }else{
                                        if ( strtolower($transaction['transactionType'])  == 'refund' || strtolower($transaction['transactionType']) == 'pay_refund' || strtolower($transaction['transactionType']) == 'credit' || strtolower($transaction['transactionType']) == 'stripe_refund' || strtolower($transaction['transactionType']) == 'paypal_refund' ){
                                            echo '<span class="btn-green remove-hover">Refund</span>';
                                        } else {
                                            echo '<span class="btn-green remove-hover">Paid</span>';
                                        }
                                    }
                                        
                                   }else{
                                    echo '<span class="btn-red remove-hover">Failed</span>';
                                 } ?></td>
            					
            				</tr>
            				
            				<?php } }
            				else { echo'<tr><td colspan="4"> No Records Found </td>
                                <td style="display: none"></td>
                                <td style="display: none"></td>
                                <td style="display: none"></td></tr>'; }  
            				
            				?>
            				
            			</tbody>
                    </table>
               
              <br>
            
       </div>
    <!-- END Customer Content -->

   </div>
   
   
   <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:999999;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div> 
</div> 
 <!-- END Page Content -->
<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Billing Info</h2>
                <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span> 
                </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
		
				
				 	 
	 <?php 	 
	 $edit_option = 0;    
	 if(!empty($card_data_array)){		 ?>
     <input type="hidden" id="payOption" name="payOption"  value="<?php echo $merchants->payOption; ?>" />
     <table  class="table table-bordered table-striped table-vcenter">
        <thead>
        <?php if($merchants->payOption == 2){ ?>
          
                <th>Type</th>
                <th class="text-right visible-lg">Friendly Name</th>
                <th class="text-right visible-lg">Action</th>
            
        <?php }else{ ?>
            
                <th>Type</th>
                <th class="text-right visible-lg">Friendly Name</th>
                <th class="text-right visible-lg">Action</th>
           
            
        <?php } ?>
    	 </thead>
         <?php
           
     		 foreach($card_data_array as $cardarray){ ?>
                <?php if($merchants->payOption == 2){ ?> 
                    <tr>
                        <td class="text-left hidden-xs">eCheck</td>
                        <td class="text-right visible-lg"><?php echo  $cardarray['merchantFriendlyName']; ?></td>
                        <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
                                <a href="#" data-toggle="tooltip" title="" class="btn btn-default editInfoByIconClick" onclick="set_edit_card('<?php echo $cardarray['merchantCardID'] ; ?>');" data-original-title="Edit Account"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url().'home/delete_card_data/'.$cardarray['merchantCardID'].'/'.$merchants->merchID ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Account"><i class="fa fa-times"></i></a>
                              
                            </div> 
                        </td>
                    </tr>    
                
                <?php }else{ ?>
                    <tr>
                        <td class="text-left hidden-xs">Credit Card</td>
                        <td class="text-right visible-lg"><?php echo  $cardarray['merchantFriendlyName']; ?></td>
                        
                        <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
                                <a href="#" data-toggle="tooltip" title="" class="btn btn-default editInfoByIconClick" onclick="set_edit_card('<?php echo $cardarray['merchantCardID'] ; ?>');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>
                                <a href="<?php echo base_url().'home/delete_card_data/'.$cardarray['merchantCardID'].'/'.$merchants->merchID ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>
                              
                            </div> 
                        </td>
                    </tr>      
                <?php } ?>    
		<?php } ?>
       </table>	
       <hr>
     <?php  $style = 'style="display:none;"'; $btnTxt = 'Update'; $edit_option = 1;  }else{  $style = 'style="display:block;"'; $btnTxt = 'Save'; }  ?>
     
     
     
		<form id="thest_form" method="post"  <?php echo $style; ?>  action='<?php echo base_url(); ?>home/update_card_data' class="form-horizontal" >
				<fieldset>
			        <input type="hidden" id="merchID" name="merchID"  value="" />

                    <?php if($merchants->payOption == 1){
                        $checkCC = 'checked';
                        $checkEC = '';
                        $displayInfoEC = 'hideInfo';
                        $displayInfoCC = 'showInfo';
                        $payOptionType = 1;
                    }else{
                        
                        $checkEC = 'checked';
                        $checkCC = '';
                        $displayInfoEC = 'showInfo';
                        $displayInfoCC = 'hideInfo';
                        $payOptionType = 2;
                    }
                    
                    ?>
                    <input type="hidden" id="payOptionType" name="payOptionType"  value="<?php echo $payOptionType; ?>" />
                    <?php if($edit_option == 0){ ?>
                    <div class="form-group " >
                        
                        <label class="col-md-6  text-right">
                            <input value="2" <?php echo $checkEC; ?> type="radio" name="formselector" class="radio_pay"></input> Checking Account 
                        </label>   
                        <label class="col-md-6">
                            <input value="1" <?php echo $checkCC; ?>  type="radio" name="formselector" class="radio_pay"></input>  Credit Card 
                        </label>      
                        
  
                    </div>
                    <?php } ?>
			        <div id="CreditCardInfo" class="<?php echo $displayInfoCC; ?>"> 
        		        <div class="form-group">
                            <label class="col-md-4 control-label" for="card_number">Credit Card Number <span class="text-danger">*</span></label>
                            <div class="col-md-8">
                             <?php  if(!empty($card_data_array) && ($card_data_array[0]['CardNo'] != '')){ 	 ?>
                                    <div id="m_card_id">
                                        <span id="m_card"><?php echo $card_data_array[0]['CardNo']; ?></span> 
                                        <input type='button' id="btn_mask" class="btn btn-default btn-sm margBtn" data-toggle="tooltip" data-original-title="Edit Card Number" value="Edit Card" />
                                    </div> 
                                    <input type="hidden" disabled  id="edit_card_number" name="edit_card_number" class="form-control" placeholder="Card Number" autocomplete="off"><?php 
                                }else{ ?> 
                                    <input type="text"   id="edit_card_number" name="edit_card_number" class="form-control" placeholder="Card Number" autocomplete="off"> 
                                <?php }  ?>
                                
                            </div>
                        </div>
            		        
            			  
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="expry">Expiry Month<span class="text-danger">*</span></label>
                            <div class="col-md-2">
                               	<select id="edit_expiry" name="edit_expiry" class="form-control">
                                    <option value="01">JAN</option>
                                    <option value="02">FEB</option>
                                    <option value="03">MAR</option>
                                    <option value="04">APR</option>
                                    <option value="05">MAY</option>
        						    <option value="06">JUN</option>
                                    <option value="07">JUL</option>
                                    <option value="08">AUG</option>
                                    <option value="09">SEP</option>
                                    <option value="10">OCT</option>
        						    <option value="11">NOV</option>
                                    <option value="12">DEC</option>
                                   </select>
                            </div>
        											
        					<label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year <span class="text-danger">*</span></label>
                            <div class="col-md-3">
                               	<select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
        						<?php 
        							$cruy = date('y');
        							$dyear = $cruy+25;
        						for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                    <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
        						<?php } ?>
                                   </select>
                            </div>					
                        </div>     
            			    
                     	<div class="form-group">
                            <label class="col-md-4 control-label" for="cvv">Security Code (CVV)</label>
                            <div class="col-md-8">
                               
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" placeholder="1234" autocomplete="off" />
                                    
                                
                            </div>
                        </div>  
                    </div>
                    <div id="eCheckInfo" class="<?php echo $displayInfoEC; ?>"> 
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="edit_acc_number">Account Number <span class="text-danger">*</span></label>
                            

                            <div class="col-md-8">
                             <?php  if(!empty($card_data_array)  && ($card_data_array[0]['accountNumber'] != '')){    ?>
                                    <div id="m_account_id">
                                        <span id="m_account"><?php echo  'XXXXX'.substr($card_data_array[0]['accountNumber'],-4); ?></span> 
                                        <input type='button' id="btn_mask1" class="btn btn-default btn-sm margBtn" data-toggle="tooltip" data-original-title="Edit Account Number" value="Edit Account" />
                                    </div> 
                                    <input type="hidden" disabled  id="edit_acc_number" name="edit_acc_number" class="form-control" placeholder="Account Number"><?php 
                                }else{ ?> 
                                    <input type="text" id="edit_acc_number" name="edit_acc_number" class="form-control" value="" placeholder="Account Number">
                                <?php }  ?>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="edit_route_number">Routing Number <span class="text-danger">*</span></label>
                            <div class="col-md-8">                           
                                    <input type="text" id="edit_route_number" name="edit_route_number" class="form-control" placeholder="Routing Number">
                            </div>
                        </div>
                        
                        <input type="hidden" name="edit_secCode" id="edit_secCode" value="WEB">

                        <div class="form-group">
                            <label class="col-md-4 control-label" for="edit_acct_type">Account Holder Type</label>
                            <div class="col-md-6">
                                
                                <select id="edit_acct_type" name="edit_acct_type" class="form-control valid" aria-invalid="false">
                                    <option value="business" >Business</option>
                                     <option value="personal" >Personal</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="edit_acct_type">Account Type</label>
                            <div class="col-md-6">
                                
                                    <select id="edit_acct_type" name="edit_acct_type" class="form-control valid" aria-invalid="false">
                                   
                                    <option value="checking" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='checking'){ echo "Seleced"; } ?> >Checking</option>
                                    <option value="saving" <?php if(isset($reseller['accountType']) && $reseller['accountType']=='saving'){ echo "Seleced"; } ?> >Saving</option>
                                   
                                </select>
                            </div>
                        </div>
                    </div> 
                </fieldset>
                    
                <fieldset>
                          
					                   	<legend> Billing Address</legend>		
					                   	<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 1 </label>
                                                <div class="col-md-8">
                                                     <input type="text" id="edit_address1" name="edit_address1" placeholder="Address 1" size="40"  class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 2</label>
                                                <div class="col-md-8">
                                                     <input type="text" id="edit_address2" name="edit_address2" placeholder="Address 2"  class="form-control">
                                                </div>
                                            </div>
                                         
                                           <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">City </label>
                                                <div class="col-md-8">                                                  
								<input type="text" id="edit_city" class="form-control" name="edit_city" placeholder="City" value="">			
                                                </div>
                                            </div>
                                          
                                                  
                                        <div class="form-group ">                                              
												<label class="col-md-4 control-label" for="card_list">State </label>
                                                <div class="col-md-8">
									<input type="text" id="edit_state" class="form-control " name="edit_state" placeholder="State" value="">                                                   
                                                </div>
                                            </div>  
                                            
                                           
                                         
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label " for="card_list">ZIP Code</label>
                                                <div class="col-md-3 ">
                                                     <input id="edit_zipcode" name="edit_zipcode" type="text"  placeholder="ZIP Code"  class="form-control">
                                                </div>
                                                
                                               
                                            </div>  
											   
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Country </label>
                                                <div class="col-md-8">
                                                    <select id="edit_country" name="edit_country"  class="form-control " >
							
								   <option value="United States" >United States</option>
								   <option value="Canada" >Canada</option>
								   
							
								</select>
                                                </div>
                                            </div>   
                    </fieldset>
                                          <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
				     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="<?php echo $btnTxt; ?>"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
	 <!-- panel-group -->
			
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------Show Payment Data------------------->


<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Card</h2>
                <button type="button" class="close btn_mdl_close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span> 
                </button>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			
		
				
                 <form id="thest" method="post" action='<?php echo base_url(); ?>Reseller_panel/insert_new_data' class="form-horizontal" >
				 
				 
				 <fieldset>
			       
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                                 <div id="m_card_id"><span id="m_card"></span> <input type='button' id="btn_mask" class="btn btn-default btn-sm" value="Edit Card" /></div>
                                <input type="hidden" disabled  id="edit_card_number" name="edit_card_number" class="form-control" placeholder="Card Number" autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+25;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" placeholder="1234" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Enter Friendly Name" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                    <fieldset>
                          
					                   
                                              
						<legend> Billing Address</legend>					
                                         
                                             
					                   	<div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 1</label>
                                                <div class="col-md-8">
                                                     <input type="text" id="address1" name="address1" placeholder="Address 1" size="40"  class="form-control">
                                                </div>
                                            </div>
                                            
                                            <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Address 2</label>
                                                <div class="col-md-8">
                                                     <input type="text" id="address2" name="address2" placeholder="Address 2" size="40"  class="form-control">
                                                </div>
                                            </div>
                                         
                                           <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">City </label>
                                                <div class="col-md-8">                                                  
								<input type="text" id="city" class="form-control" name="city" placeholder="City" value="">			
                                                </div>
                                            </div>
                                          
                                                  
                                        <div class="form-group ">                                              
												<label class="col-md-4 control-label" for="card_list">State </label>
                                                <div class="col-md-8">
									<input type="text" id="state" class="form-control " name="state" placeholder="State" value="">                                                   
                                                </div>
                                            </div>  
                                            
                                           
                                         
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label " for="card_list">ZIP Code</label>
                                                <div class="col-md-3 ">
                                                     <input id="zipcode" name="zipcode" type="text"  placeholder="ZIP Code" size="40"  class="form-control">
                                                </div>
                                                
                                               
                                            </div>  
											   
                                         <div class="form-group ">
                                              
												<label class="col-md-4 control-label" for="card_list">Country </label>
                                                <div class="col-md-8">
                                                    <select id="country" name="country"  class="form-control " >
							
								   <option value="United States" >United States</option>
								   <option value="Canada" >Canada</option>
								   
							
								</select>
                                                </div>
                                            </div>   
                    </fieldset>
                  <div class="pull-right">
				     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<div id="reset_password_merchant" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reset Password</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="reset_password_merchant_form" method="post" action="<?php echo base_url().'Settingmail/reset_merchant_password'; ?>" class="form-horizontal">
                    
                    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="resetMerchantID" name="resetMerchantID" class="form-control" value="<?php echo $merchants->merchID; ?>">

                            <input type="hidden" id="firstName" name="firstName" value="<?php echo $merchants->firstName; ?>">
                            <input type="hidden" id="companyName" name="companyName"  value="<?php echo $merchants->companyName; ?>">
                            <input type="hidden" id="merchantEmail" name="merchantEmail"  value="<?php echo $merchants->merchantEmail; ?>">

                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 no-padding alignText" >Set Password</label>
                        <div class="col-md-5 no-padding setPassword ">
                            <input type="text" id="newGeneratePassword" name="newGeneratePassword" class="form-control " value="" ></input> 
                            <div class="ttp">
                                <i onmouseout="outFunc()"  onclick="copyText()" class="fa fa-copy faCopyPassword"></i>
                                <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                            </div>
                            
                            
                                                      
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3 no-padding" ></label>
                        <div class="col-md-5 no-padding" >
                            <div id="manageCustomCheck">
                                <input type="checkbox" id="customCheck" name="customCheck" class="customCheck" value="0" > <label class="customCheckLabel">  Send Merchant Email </label>
                            </div>                               
                        </div>
                    </div>
                    <div class="pull-right">
                        <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Reset Password">
                        <button type="button" class="btn btn-sm close1 btn_can" data-dismiss="modal">Cancel</button>
                    </div>
                    <br>
                    <br>
                </form>     
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


   <script>
   var Pagination_view = function() {

return {
    init: function() {
        / Extend with date sort plugin /
        $.extend($.fn.dataTableExt.oSort, {
       
        } );

        / Initialize Bootstrap Datatables Integration /
        App.datatables();

        / Initialize Datatables /
        $('#compamount').dataTable({
            columnDefs: [
                { type: 'date-custom', targets: [1] },
                { orderable: true, targets: [2] }
            ],
           
            order: [[ 2, "desc" ]],
            pageLength: 10,
            lengthMenu: [[10, 25, 50, 100], [10, 25, 50, 100]]
        });

        / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
    }
};
}();
  $(function(){   
   
   Pagination_view.init();
   Pagination_view1.init();
    nmiValidation1.init();
	 $('#btn_mask').click(function(){
      $('#m_card_id').hide();  
      $('#edit_card_number').removeAttr('disabled');
      $('#edit_card_number').attr('type','text');
    });
	$('#btn_mask1').click(function(){
      $('#m_account_id').hide();  
      $('#edit_acc_number').removeAttr('disabled');
      $('#edit_acc_number').attr('type','text');
    });
    
		
});
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                    
                        address1:{
						required: true,
					},
                    
					country:{
						  required: true,
					},
					city:{
						required: true,
					},
                 state:{
                         required: true,
                    },
			
                    zipcode:{
					 required: true,
					  number: true,
						minlength: 5,
                        maxlength: 6,
				   }
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                   edit_acc_number:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 20,
                        
                    },
                    edit_route_number:{
                        required: true,
                        number: true,
                        minlength: 3,
                        maxlength: 12,
                    },
                    
                  
                  
                },
              
            });
						
			  $.validator.addMethod('CCExp', function(value, element, params) {  
			  var minMonth = new Date().getMonth() + 1;
			  var minYear = new Date().getFullYear();
			  var month = parseInt($(params.month).val(), 10);
			  var year = parseInt($(params.year).val(), 10);
			  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

	
	
	
	
 function set_card_user_data(id){
      
	   $('#merchID').val(id);
	   
 }
		 
	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		$('.editInfoByIconClick').not(this).hide();
        var type = $('#payOption').val();
        
		if(cardID!=""){
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>home/get_card_edit_data",
				data : {'cardID':cardID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					       if(data['status']=='success'){
							  
                                $('#edit_cardID').val(data['card']['merchantCardID']);
							    
                                $('#edit_friendlyname').val(data['card']['merchantFriendlyName']);
						        if(type == 2){
                                    $('#m_account_id').show(); 
                                    $('#edit_acc_number').attr('disabled','disabled');
                                    $('#edit_acc_number').attr('type','hidden');
                                    $('select[name="edit_acct_type"]').find('option[value="'+data['card']['accountType']+'"]').attr("selected",true);
                                    $('select[name="edit_acct_holder_type"]').find('option[value="'+data['card']['accountHolderType']+'"]').attr("selected",true);
                                }else{
                                    $('#m_card_id').show(); 
                                    $('#edit_card_number').attr('disabled','disabled');
                                    $('#edit_card_number').attr('type','hidden');
                                   
                                    if((data['card']['CardMonth']).length == 1)
                                        var cmonth ='0'+data['card']['CardMonth'];
                                    else
                                        var cmonth =data['card']['CardMonth'];

                                    $('select[name="edit_expiry"]').find('option[value="'+cmonth+'"]').attr("selected",true);
                                    $('select[name="edit_expiry_year"]').find('option[value="'+data['card']['CardYear']+'"]').attr("selected",true);
                                }  
								
					               
                                  
								$('#edit_address1').val(data['card']['Billing_Addr1']);
							    $('#edit_address2').val(data['card']['Billing_Addr2']);
							    $('#edit_city').val(data['card']['Billing_City']);
							    $('#edit_state').val(data['card']['Billing_State']);
							    $('select[name="edit_country"]').find('option[value="'+data['card']['Billing_Country']+'"]').attr("selected",true);
							    $('#edit_zipcode').val(data['card']['Billing_Zipcode']);
							  
							
					   }	   
					
				}
				
				
			});
			
		}	  
		   
	}	
	
	
var Pagination_view1 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#transaction').dataTable({
                
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

$('#status_filter').change(function(){
    $('.filter_form').submit();
});	
	
	

$('.radio_pay').click(function(){
  var method = $(this).val(); 
   if(method==1)
   {
        $('#payOptionType').val(1);
        $('#eCheckInfo').hide();
        $('#CreditCardInfo').show();
   }else if (method==2){
        $('#payOptionType').val(2);
        $('#eCheckInfo').show();
        $('#CreditCardInfo').hide();
   }else {
        $('#payOptionType').val(2);
    }         
});	


$('#manageCustomCheck').click(function(){
        
    if($("#customCheck").val() == 1){
        $("#customCheck").val(0);
        $("#customCheck").prop('checked',false);
        
        
    }else{
        $("#customCheck").val(1);
        $("#customCheck").prop('checked',true);
       
    }
});

$('#reset_password_merchant_form').validate({ // initialize plugin
    ignore:":not(:visible)",            
    rules: {
        
    'newGeneratePassword':{
        required: true,
        minlength:8,
        minlength:8,
        maxlength:24,
        check_atleast_one_number: true,
        check_atleast_one_special_char: true,
        check_atleast_one_lower: true,
        check_atleast_one_upper:true,
        check_name:true,
        company_name:true,
        check_email:true,
    } 
 },
});	
$.validator.addMethod("check_atleast_one_number", function(value, element) {
    if(value.length == 0){
        return true ;
    }else{
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
     return true ;

}, "This field should contain at least one number.");

$.validator.addMethod("check_atleast_one_special_char", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true ;
    }
    if (regularExpression.test(value)) {
        return false;
    } 
    return true ;
}, "This field should contain at least one special character.");
$.validator.addMethod("check_atleast_one_lower", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true;
    }
    if (value.match(/([a-z])/) && value.match(/([0-9])/)){
        return true;
    }else{
      return false;
    }
    return true ;
}, "This field should contain at least one lower characters.");
$.validator.addMethod("check_atleast_one_upper", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true;
    }
    if (value.match(/([A-Z])/) && value.match(/([0-9])/)){
        return true;
    }else{
      return false;
    }
    return true ;
}, "This field should contain at least one uppercase characters.");

$.validator.addMethod('check_name', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var firstName = $('#firstName').val();
        if(firstName.length == 0){
            return true ;
        }else{

            if(value.indexOf(firstName) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'First name not used in password.');

$.validator.addMethod('company_name', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var companyName = $('#companyName').val();
        if(companyName.length == 0){
            return true ;
        }else{

            if(value.indexOf(companyName) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'Company name not used in password.');

$.validator.addMethod('check_email', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var merchantEmail = $('#merchantEmail').val();
        if(merchantEmail.length == 0){
            return true ;
        }else{
           
            var res = merchantEmail.split("@");
            if(value.indexOf(res[0]) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'Email not used in password.');
$('#resetPasswordBtn').click(function(){
    var randomstring = randomPassword(20);
    $('#newGeneratePassword').val(randomstring);
    $('#autoPassword').val(randomstring);
    
});


function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}	

function copyText() {
  /* Get the text field */
  var copyText = document.getElementById("newGeneratePassword");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied: " + copyText.value;
}
function outFunc() {
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copy to clipboard";
}
</script>


<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

.profile-pic img {
    width: 150px;
    height: 150px;
    object-fit: cover;
    border: 3px solid #eee;
}

</style>



