<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}
</style>
 <!-- Page content -->
 <?php
	$this->load->view('alert');
?>
	<div id="page-content">
	       
    <div class="block full">
        <!-- Working Tabs Title -->
        <div class="block-title">
            
              <ol class="breadcrumb">
        <li class="breadcrumb-item">
           <strong> <a href="<?php echo base_url(); ?>Settingmail/email_template">E-mail Templates</a></strong>
        </li>
        <li class="breadcrumb-item">
            
           Template Details
          </li>
      </ol>
            
            
            
           
        </div>
        <!-- END Working Tabs Title -->

        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                
                    <!-- Block Tabs Title -->
                    
                    
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    
                                <?php if($isEditMode){ ?>
                                  <form id="form-validation" action="<?php echo base_url();?>Settingmail/create_template" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                <?php }else{ ?>
                                  <form id="form-validation" action="#" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                <?php } ?>
                            
                                
	                                 <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Email Type</label>
                                        <div class="col-md-7">
                                            <input type="text" id="type" disabled="disabled" name="type" value="<?php echo $templatedata['typeName']; ?>" class="form-control" />
                                      
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php if(isset($templatedata)) echo ($templatedata['fromEmail'])?$templatedata['fromEmail']:FROM_EMAIL; ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Name</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromName" name="fromName"  value="<?php if(isset($templatedata)) echo ($templatedata['fromName'])?$templatedata['fromName']:$resellerdata['resellerCompanyName']; ?>" class="form-control" placeholder="Enter the Name">
                                        </div>
                                    </div>
                                 
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC</a><span id="cc_sapn"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href="javascript:void(0);" id="open_bcc">Add BCC</a><span id="bcc_sapn"><strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></span><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">CC Email Addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="Enter the cc email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">BCC Email Addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="Enter the bcc Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">Set the "Reply-To"</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Enter the email">
                                        </div>
                                    </div>
                                     
                                    
                                 
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)){ echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:'';}else{ echo $subject; }  ?>"  class="form-control" placeholder="Enter the Subject">
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)){ echo ($templatedata['message'])?$templatedata['message']:''; }else{ echo $email_temp; } ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="tempID" id="tempID" value="<?php if(isset($templatedata)) echo ($templatedata['templateID'])?$templatedata['templateID']:''; ?>" />
                                      <input type="hidden" name="resellerID" id="resellerID" value="<?php if(isset($resellerdata)) echo ($resellerdata['resellerID'])?$resellerdata['resellerID']:''; ?>" />
                                  <?php if($isEditMode){ ?>
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"> Save </button>
                                      
                                      <a href="<?php echo base_url(); ?>Settingmail/email_template" class="btn btn-sm btn_can" >Cancel</a>
                                    </div>
                                </div>
                                <?php } ?>  
                           </form>	
                         
      
	    <div class="col-md-1"></div>
	  <div class="col-md-10">
	        <div class="modal-header ">
			 
               <h2 class="modal-title pull-left">Template Guide </h2>
             
                
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
              
		    	 <div class="form-group ">
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-7"  >
                                          <label> <p>Copy and paste the following Tags to customize your Email Templates</p>
                                           </label>
                                          </div> 
                                                  
                                             <table class="table table-bordered table-striped ecom-orders table-vcenter">
                                               <th>Description</th>
                                              <th>Tag</th>
                                             
                                              <tbody>
                                                   <tr>   <td>Invoice Balance</td>                                            
                                              <td> <?php  echo "{{invoice_balance}}"; ?></td>
                                             
                                              </tr>
                                                <tr>     <td>Invoice Due Date</td>                                          
                                              <td> <?php  echo "{{invoice_date_due}}"; ?></td>
                                             
                                              </tr>
                                                      <tr>        <td>Invoice Number</td>                                       
                                              <td> <?php  echo "{{invoice_num}}"; ?></td>
                                             
                                              </tr>
                                                   <tr>         <td>Transaction ID</td>                                      
                                              <td> <?php  echo "{{payment_transaction_id}}"; ?></td>
                                             
                                              </tr>
                                                     <tr>  <td>Invoice Amount</td>                                            
                                              <td> <?php  echo "{{invoice_last_payment_amount}}"; ?></td>
                                              
                                              </tr>
                                                   <tr>     <td>Total Invoice Amount</td>                                          
                                              <td> <?php  echo "{{invoice_amount_paid}}"; ?></td>
                                             
                                              </tr>
                                                  <tr>     <td>Invoice Amount</td>                                          
                                              <td> <?php  echo "{{invoice_amount_paid}}"; ?></td>
                                             
                                              </tr>
                                                  <tr>       <td>Invoice Date</td>                                        
                                              <td> <?php  echo "{{invoice_date}}"; ?></td>
                                             
                                              </tr>
                                                  <tr>   <td>Login URL</td>                                            
                                              <td> <?php  echo "{{login_url}}"; ?></td>
                                             
                                              </tr>     
                                             <tr>      <td>Merchant Name</td>                                        
                                              <td> <?php  echo "{{merchant_name}}"; ?></td>
                                              
                                              </tr>
											  <tr>       <td>Reseller Company</td>                                        
                                              <td> <?php  echo "{{reseller_company}}"; ?></td>
                                             
                                              </tr>
                                              
                                             
											  <tr>       <td>Merchant Company</td>                                        
                                              <td> <?php  echo "{{merchant_company}}"; ?></td>
                                             
                                              </tr>
											  
											  <tr> <td>Merchant Email </td>                                             
                                              <td> <?php  echo "{{merchant_email}}"; ?></td>
                                              
                                              </tr>
											  <tr>    <td>Merchant Password </td>                                           
                                              <td> <?php  echo "{{merchant_password}}"; ?></td>
                                             
                                              </tr>
											  
											   <tr>     <td> Reseller Name</td>                                           
                                              <td> <?php  echo "{{reseller_name}}"; ?></td>
                                            
                                              </tr>
                                              
                                              <tr>     <td> Reseller Email</td>                                          
                                              <td> <?php  echo "{{reseller_email}}"; ?></td>
                                             
                                              </tr>
                                              
                                              <tr>   <td> Reseller Phone</td>                                            
                                              <td> <?php  echo "{{reseller_phone}}"; ?></td>
                                             
                                              </tr>
											 
											  <tr>   <td>Link URL</td>                                            
                                              <td> <?php  echo "{{link}}"; ?></td>
                                             
                                              </tr>
											 
											  <tr>   <td>Agent Name</td>                                            
                                              <td> <?php  echo "{{agent_name}}"; ?></td>
                                             
                                              </tr>
											 
											  <tr>   <td>Agent Email</td>                                            
                                              <td> <?php  echo "{{agent_email}}"; ?></td>
                                             
                                              </tr>
											  
											  <tr>   <td>Agent Password</td>                                            
                                              <td> <?php  echo "{{agent_password}}"; ?></td>
                                             
                                              </tr>											                         
                                              <tr>                                              
                                                <td>Friendly Name</td>
                                                <td> <?php  echo "{{plan_name}}"; ?></td>
                                              </tr>
                                                  
                                           
                                              </tbody>
                                             </table>       
                                            
							    
                                       
                                          
                                        </div>
			
			           </div>
			   					
                
            </div>
	  </div>
	  
       <div class="col-md-1"></div>    
            </div>
    


    


	
        </div>
        <!-- END Working Tabs Content -->
        
     
		 </div>
	
	
	    <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:9999999"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
    
   <!-- Modal Header -->
			
            
            
       
   </div> 
        
   
        <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"></script>
        <script>$(function(){    nmiValidation.init();
		
					CKEDITOR.replace( 'textarea-ckeditor', {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
			});
			CKEDITOR.config.allowedContent = true;
		    $('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
					  $('#cc_sapn').hide();
			});
			  $('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
					   $('#bcc_sapn').hide();
			});
			 $('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
			});
			
			
			$('#type').change(function(){
	     var t_name=	$('#type option:selected').text();    
			
			$('#templateName').val(t_name);
			    
			});
			
		 });
         
         
         
         
         
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    templateName: {
                        required: true,
						minlength:3,
                    },
					
					 fromEmail: {
						required: true,
						email:true,	
						},
					
                   		
					 emailSubject: {
                        required: true,
                       
                    },
                    message: {
                         required: true,
                       
                    },
                  
                },
             
            });
			

			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
         
         
         
        
         
         </script>

   
    