<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}
</style> 
<?php
	$this->load->view('alert');
?> 
   <!-- Page content -->
	<div id="page-content">
	              
    <!-- END Wizard Header -->
    <legend class="leg"> General Settings</legend>   
    <!-- Progress Bar Wizard Block -->
    <div class="block full">
	              
        <!-- Progress Bar Wizard Content -->
        <div class="row">
			<?php if($isEditMode){ ?>
				<form method="POST" id="reseller_form"  class="form form-horizontal" action="<?php echo base_url(); ?>SettingConfig/general_setting" enctype="multipart/form-data" >
			<?php }else{ ?>
				<form method="POST" id="reseller_form"  class="form form-horizontal" action="#" enctype="multipart/form-data" >
			<?php } ?>
		 	
			
			 <input type="hidden"  id="resellerID" name="resellerID" value="<?php if(isset($reseller)){echo $reseller['resellerID']; } ?>" /> 
			
			 
			<div class= "col-md-6">
			        <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Company Name </label>
					<div class="col-md-6">
					<input type="text" id="resellerCompanyName" name="resellerCompanyName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerCompanyName']; } ?>" old-placeholder="Your Company Name"><?php echo form_error('resellerCompanyName'); ?></div>
					</div>
			   
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerfirstName']; } ?>" old-placeholder="Your First Name"><?php echo form_error('firstName'); ?></div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['lastName']; } ?>" old-placeholder="Your Last Name"><?php echo form_error('lastName'); ?></div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Primary Phone Number </label>
					<div class="col-md-6">
					<input type="text" id="primaryContact" name="primaryContact" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['primaryContact']; } ?>" old-placeholder="Your Primary Contact"><?php echo form_error('primaryContact'); ?></div>
					</div>
					
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email Address </label>
					<div class="col-md-6">
					<input type="text" id="resellerEmail" name="resellerEmail" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerEmail']; } ?>" old-placeholder="Your Email" ><?php echo form_error('resellerEmail'); ?></div>
					</div>
					
					   
					
						<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Address Line 1 </label>
					<div class="col-md-6">
					<input type="text" id="resellerAddress" name="resellerAddress" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerAddress']; } ?>" old-placeholder="Address 1"><?php echo form_error('resellerAddress'); ?></div>
					</div>
					
					
					
					
				
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">City</label>
						   <div class="col-md-6">
						       	<input type="text" id="city" name="resellerCity" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerCity']; } ?>" old-placeholder="City"><?php echo form_error('city_name'); ?>
						       
							</div>
                        </div>	
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Zip Code </label>
					<div class="col-md-6">
					<input type="text" id="zipCode" name="zipCode" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['zipCode']; } ?>" old-placeholder="Your Zip Code"><?php echo form_error('zipCode'); ?></div>
					</div>
					
					</div>
					
					<div class="col-md-6">
					    
					 
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Federal Tax ID </label>
					<div class="col-md-6">
					<input type="text" id="federalTaxID" name="federalTaxID" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['federalTaxID']; } ?>" old-placeholder="Your Tax ID"><?php echo form_error('federalTaxID'); ?></div>
					</div>
					 <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing First Name </label>
					<div class="col-md-6">
					<input type="text" id="billingfirstName" name="billingfirstName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['billingfirstName']; } ?>" old-placeholder="Your Billing First Name"><?php echo form_error('firstName'); ?></div>
					</div> 
					  <div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing Last Name </label>
					<div class="col-md-6">
					<input type="text" id="billinglastName" name="billinglastName" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['billinglastName']; } ?>" old-placeholder="Your Billing last Name"><?php echo form_error('firstName'); ?></div>
					</div>   
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing Phone Number</label>
					<div class="col-md-6">
					<input type="text" id="billingContact" name="billingContact" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['billingContact']; } ?>" old-placeholder="Your Billing Phone Number"><?php echo form_error('billingContact'); ?></div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Billing Email Address </label>
					<div class="col-md-6">
					<input type="text" id="billingEmailAddress" name="billingEmailAddress" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['billingEmailAddress']; } ?>" old-placeholder="Your Billing Email Address"><?php echo form_error('billingEmailAddress'); ?></div>
					</div>
					
				
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Address Line 2  </label>
					<div class="col-md-6">
					<input type="text" id="resellerAddress2" name="resellerAddress2" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerAddress2']; } ?>" old-placeholder="Address 2"><?php echo form_error('resellerAddress2'); ?></div>
					</div>
					
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">State</label>
						   <div class="col-md-6">
						       <input type="text" id="state" name="resellerState" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['resellerState']; } ?>" old-placeholder="State"><?php echo form_error('state_name'); ?>
							
							</div>
                        </div>	
					
				
					
					 	<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Country</label>
						   <div class="col-md-6">
								<select id="country" class="form-control " name="resellerCountry" >
							
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_name']; ?>" <?php if(isset($reseller)){ if($reseller['resellerCountry']==$country['country_name'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
					  
				
                        
					</div>
					<div class= "col-md-12">
					<div class="form-group">
					<label class="col-md-2 control-label" for="example-username">Website URL</label>
					<div class="col-md-9">
					<input type="url" id="weburl" name="weburl" class="form-control"  value="<?php if(isset($reseller)){ echo $reseller['webURL']; } ?>" old-placeholder="Website URL"><?php echo form_error('webURL'); ?></div>
					</div>
					</div>
					<?php if($isEditMode){ ?>
					<div class="form-group">
					    <div class="col-md-12">
						  	<div class="col-md-10">		
						  	</div>
						  	<div class="col-md-2">
					           <button type="submit" class="submit btn btn-sm btn-success" >Update</button>
					         </div> 
				
				    	</div>
					
				    </div>	
					<?php } ?>
	                				
					
		
		   
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
        
        
    
    </div>
     <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:9999999"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>


$(document).ready(function(){


    $('#reseller_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			   'resellerCompanyName': {
                        required: true,
                        minlength: 3,
                         
                    },
		       'firstName': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'lastName': {
                        required: true,
                        minlength:2,
                    },
					'primaryContact': {
                        required: true,
                       
                    },
                    
                    'url': {
                        required: true,
                       
                    },
					
				'resellerEmail': {
                        required: true,
                        email: true,
						  
                         
                    },
                    'resellerPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'confirmPassword': {
                        required: true,
                        equalTo: '#resellerPassword',
                    },
					'billingContact': {
                        required: true,
                       
                    },
					'billingEmailAddress': {
                        required: true,
                        email: true,
                    },
					'federalTaxID': {
                        required: true,
                        
                    },
                    'country': {
                        required: true,
                        
                    },
                    'state': {
                        required: true,
                        
                    },
                    'city': {
                        required: true,
                        
                    },
                    'resellerAddress': {
                        required: true,
                        minlength:5,
                    },
					'resellerAddress2': {
                        required: true,
                        minlength:5,
                    },
                    'zipCode': {
                        required: true,
                        
                    },
					'Plans': {
                        required: true,
                        
                    },
                    'Tiers': {
                        required: true,
                        
                    },
                    
			
			},
    });
	
	
	$('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('SettingConfig/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('SettingConfig/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	
	
	
	
	
});	    

</script>


</div>


