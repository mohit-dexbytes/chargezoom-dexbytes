<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}

.align_text{
    text-align: left !important;
}
</style>
<?php
	$this->load->view('alert');
?>
	<div id="page-content">
          	
    <legend class="leg"> Merchant Portal</legend>    
    <div class="block-main full">
      
      
        <!-- END Working Tabs Title -->

        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                <div class="block full">
                    <!-- Block Tabs Title -->
                    
                    <div class="block-title">
                     
                        <ul class="nav nav-tabs " data-toggle="tabs">
                            <li class="active"><a href="#portal_setting">Login Page</a></li>
                            <li><a href="#portal_preview">Preview</a></li>
                           
                        </ul>
                    </div>
                    <!-- END Block Tabs Title -->

                    <!-- Tabs Content -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="portal_setting">
                            <?php if($isEditMode){ ?> 
                              <form id="form-portal" action="<?php echo base_url(); ?>Merchant_Portal/setting_merchant_portal" method="post" enctype="multipart/form-data"  class="form-horizontal ">
                            <?php }else{ ?> 
                              <form id="form-portal" action="#" method="post" enctype="multipart/form-data"  class="form-horizontal ">
                            <?php } ?> 
                              
                                   
                                    <div class="form-group">
                                        <label class="col-md-2 control-label align_text" for="portal_url">Merchant Portal URL</label>
                                        <div class="col-md-7">
                                            <div class="input-group">
                                            
                                             <input type="text" id="portal_url" name="portal_url"  value="<?php if(isset($setting) && !empty($setting)){ echo $setting['portalprefix']; } ?>"   class="form-control" placeholder="Enter the portal url">
                                             
                                        <span class="input-group-addon"> .<?php echo RSDOMAIN;?> </span>
                                        
                                        </div>
                                     </div>
                                </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label align_text" for="customer_help_text">Merchant Help Text</label>
                                        <div class="col-md-7">
	                                        <textarea id="merchant_help_text" name="merchant_help_text"  class="form-control"     placeholder="Enter the text"> <?php if(isset($setting) && !empty($setting) && $setting['merchantHelpText']!=''){ echo $setting['merchantHelpText']; } ?></textarea>
                                            
                                        </div>
                                    </div>
                                      
                                <div class="form-group">
							<label class="col-md-2 control-label align_text" for="picture">Login Page</label>
							<div class="col-md-7">
								<input type="file" id="upload_logo" name="picture" class="upload" ><img src="<?php if(isset($reseller) && !empty($reseller['resellerProfileURL'])){echo $reseller['resellerProfileURL']; }else { echo CZLOGO; }?>"  width="200px">
							</div>
						</div>
                        
                        <div class="form-group">
							<label class="col-md-2 control-label align_text" for="pictureportal">Portal Logo</label>
							<div class="col-md-7">
								<input type="file" id="pictureportal" name="pictureportal" class="upload" ><img src="<?php if(isset($reseller) && !empty($reseller['ProfileURL'])){echo $reseller['ProfileURL']; }else { echo CZLOGO; } ?>"  width="200px">
							</div>
						</div>
						
                             
                                <?php if($isEditMode){ ?>    
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-8">
                                        <button type="submit" class="btn btn-sm btn-success">Save</button>
                                       
                                        
                                      
                                    </div>
                                </div>  
                                <?php } ?>
                           </form>
                        </div>
                        <div class="tab-pane" id="portal_preview">
                          <iframe width="100%" height="450" src="<?php if(isset($setting) && !empty($setting)){ echo $setting['merchantPortalURL']; }else{ echo "http://defaul.chargezoom.net/";} ?>"> </iframe>
                        </div>
                      
                    </div>
                    <!-- END Tabs Content -->
                </div>
                <!-- END Block Tabs -->
            </div>
           
        </div>
        <!-- END Working Tabs Content -->
        
       
    
    </div>
    <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:999999;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
   
    </div>
<script>


$(function(){       
   
    $('#form-portal').validate({ // initialize plugin
		ignore:":not(:visible)",

       errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
		
		rules: {
		       'merchant_help_text': {
                        required: true,
                         minlength: 3
                    },
					'portal_url':{
						 required : true,
						 minlength: 3,
						 "remote" :function(){return Chk_url('portal_url');} 
					}			
				 
			
			},
    });
  
});  

       var Chk_url=function(element_name){ 
       
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Merchant_Portal/check_url',
                    type: "post",
					data:
                        {
                            portal_url: function(){return $('input[name=portal_url]').val();},
                        },
                        dataFilter:function(response){
						   data=$.parseJSON(response);
						
                             if (data['status'] == 'true') {
						     	$('#portal_url').removeClass('error');	 
								 $('#portal_url-error').hide();	 
									   return data['status'] ;				   
									 
                              } else {
                               
								   if(data['status'] =='false'){
								      
                                       $('#form-portal').validate().showErrors(function(){
										   
                                        return {key:'portal_url'}; 
                                     })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	

  </script>
   
   </div>
