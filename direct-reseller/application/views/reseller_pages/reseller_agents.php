<!-- Page content -->
<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; 
    opacity:0.6;        
}
.red-tooltip + .tooltip > .tooltip-inner {background-color: #e74c3c;}

.greyColor{
    color: #ccc !important;
}
#agent_page_filter input.form-control {
    width: 185px !important;
}
.manageSubBtn {
    right: 84px !important;
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    position: absolute;
    z-index: 9;
    top: 9px;
    left: 60px;
    width: 220px !important;
}
.btnRightCust {
    right: 0 !important;
}
.btnRightCust1{

}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    <legend class="leg"> Agents</legend>    
    <!-- All Orders Block -->
    <div class="block-main full"  style="position: relative;">
	
        <!-- All Orders Title -->
       
            
        <div class="manageSubBtn btnRightCust1">
                <?php
                   
					if($isEditMode)
					{
				
				?>
                     <a class="btn  btn-sm btn-info" title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#Edit_agent" >Reassign Merchants</a>
                       
                <?php
					}else{
				?>
				<span class="btn  btn-sm btn-info" disabled title="Reassign Merchants" >Reassign Merchants</span>

				<?php
					}
				?>
		</div>
        <div class="addNewBtnCustom btnRightCust">
                <?php
                    
                    
                    if($isEditMode)
                    {
                
                ?>
                   
                        <a href="<?php echo base_url(); ?>SettingAgent/create_agent" class="btn btn-sm btn-success" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="" >Add New</a>
                <?php
                    }else{
                ?>
               
                <span  disabled class="btn btn-sm btn-success"  title="Create New" >Add New</span>          
                        
                <?php
                    }
                ?>
        </div>
        
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="agent_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="text-left hidden">Sort Date</th>
                    <th class="text-left">Agent Name</th>
                     <th class="text-right hidden-xs">Merchants</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
		
				<?php 
			     
				if(isset($r_agent) && $r_agent)
				{
					foreach($r_agent as $reseller)
					{
				?>
				<tr>
                    <td class="text-right hidden"><?php echo $reseller['createdAt']; ?> </td>
					<td class="text-left cust_view">
                    <?php if($isEditMode){ ?>
                        <a href="<?php echo base_url().'SettingAgent/create_agent/'.$reseller['ragentID']; ?>" data-toggle="tooltip" title="Edit Agent" class="">
                            
                       
                        <?php }else{
                            echo '<a href="javascript:void(0);" class="greyColor" disabled data-toggle="tooltip" title="Edit Agent" >';
                        }   ?> 
                        <?php echo $reseller['agentName']; ?>
                        </a> 
                    </td>
				
					
				    <td class="text-right hidden-xs"><?php echo $reseller['addMerchant']; ?> </td>
				    <td class="text-center">
						<div class="btn-group btn-group-xs">
						<?php 
						
						if($reseller['isEnable']=='1')
						{
						   
						
						
						if($isEditMode)
						{  
						?>

							
                           <?php 

                           if($reseller['addMerchant'] > 0){
                                echo '<a href="javascript:void(0);" disabled class="btn btn-danger red-tooltip" data-toggle="modal" data-original-title="Agent cannot be disabled, please reassign Merchants."><i class="fa fa-ban"></i></a>';
                           }else{
                           ?>

					        <a href="#suspend_ragent" onclick="disable_agent('<?php echo $reseller['ragentID']; ?>');" class="btn btn-danger" data-toggle="modal" data-original-title="Disable Agent"><i class="fa fa-ban"></i></a>
						<?php } 
                        }
						else{ ?>

						 
					    <a href="javascript:void(0);" disabled  class="btn btn-success" data-toggle="modal"  data-original-title="Activate Agent"><i class="fa fa-plus-circle"></i></a>
						
						<?php } 

						}
						?>
						
						</div>
					</td>
				</tr>

				
				
				
				<?php } } 
				else { echo'<tr>
                <td colspan="4"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>';  }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
     
    </div>
    <!-- END All Orders Block -->
 
  <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:999999;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>

<div id="add_friendly_name" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
               <h2 class="modal-title" id="title">Friendly Plan Name</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="friendlyname_form" class="form form-horizontal" action="<?php echo base_url(); ?>Reseller_panel/reseller_planfname">
			<input type="hidden" id="planID" name="planID" value=""  />
			  
			    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Plan Name </label>
							<div class="col-md-8">
                        <input type="text" id="plan_name" class="form-control"  value="" disabled>
								</div>
						<input type="hidden" value="" id="plan_id" name="plan_id">		
						</div>
						
					
                       </div> 
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Friendly Name</label>
							<div class="col-md-8">
								<input type="text" id="friendlyname"  name="friendlyname" class="form-control"  value="" placeholder="friendly Name.."><?php echo form_error('friendlyname'); ?></div>
						</div>
						
					
                       </div>      
		      
			  <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>
                    <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


     <!------- Modal for Delete Gateway ------>

  <div id="del_ragent" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reassign Merchants</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_ragent" method="post" action='<?php echo base_url(); ?>SettingAgent/delete_ragent' class="form-horizontal" >
                 <?php if($login_info['login_type']=='RESELLER'){   ?>
			             <div class ="form-group">
						   <label class="col-md-6 control-label" for="example-typeahead">Select Agent to Reassign Merchants</label>
						   <div class="col-md-6">
								<select id="reseller_agent" class="form-control " name="reseller_agent" >
					
								</select>
							</div>
                        </div>	
			   <?php } ?>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="reselleragentid" name="reselleragentid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm close1" data-dismiss="modal" style="background:#ffc000; color:#ffffff"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



     <!------- Modal for Supend Agent ------>

  <div id="suspend_ragent" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disable Agent</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="suspend_ragent" method="post" action='<?php echo base_url(); ?>SettingAgent/suspnd_agent' class="form-horizontal" >
                <p>Do you really want to Disable this Agent?</p>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="reselleragent_id" name="reselleragent_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Disable"  />
                    <button type="button"  class="btn btn-sm close1 btn_can" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 
<div id="Edit_agent" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reassign Merchants</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>SettingAgent/update_agent' class="form-horizontal" >
				 
				 
				   
				   
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Agent</label>
						 <div class="col-md-6">
						   <select id="agent_old" name="agent_old"  class="form-control">
								   <option value="" >Select Current Agent</option>
								    <?php foreach($re_agents as $agent){  ?>
								    <option value="<?php echo $agent['ragentID']; ?>" ><?php echo $agent['agentName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Agent</label>
						 <div class="col-md-6">
						   <select id="agent_new" name="agent_new"  class="form-control">
								   <option  value="" >Select New Agent</option>
								   <?php foreach($re_agents as $agent){  ?>
								    <option value="<?php echo $agent['ragentID']; ?>" ><?php echo $agent['agentName']; ?></option>
								   <?php } ?>
								  
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="col-md-10 text-right">
        			 <input type="submit"  name="btn_cancel1" class="btn btn-sm btn-success newSaveButton" value="Save"  />
                    <button type="button"   class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					    <hr/>
				<tbody id="t_data">
						 
						 </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit"   name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"  class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>

</div>

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init();     nmiValidation1.init();    
    
    
  
 
 $('#agent_old').change(function(){
	 
	
    var agID =  $('#agent_old').val();
	$.ajax({
    url: '<?php echo base_url("SettingAgent/get_merchant_id")?>',
    type: 'POST',
	data:{agent_id:agID},
    success: function(data){
		$('#t_data').html(data);
		
		
			
     }	
  });
	
});	
	  
	
$.validator.addMethod('chkname', function(value, element, params) {
       var val1 = $(params.agent1).val();
      var val2 = $(params.agent2).val();
     
      return (val1!=val2 && val1!='' && val2!='' );
}, 'Please select other agent both are same.');    
    
});
</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#agent_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 1, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

	  
var nmiValidation1 = function() {   

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#gatw').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     
					 agent_old: {
							 required:true,
						},
				    	agent_new:{
							 required:true,
							 chkname: {
									agent1: '#agent_old',
									agent2: '#agent_new'
							  }
					},
					
					
                },
               
            });
					
		
		
        }
    };
}();



function del_ragent_id(id){
	
	     $('#reselleragentid').val(id);
	     
	     $.ajax({
	         
	       url:"<?php echo base_url() ?>/SettingAgent/get_del_agent",
	       method:"POST",
	       data:{agetID : id},
	       success:function(response){
	               var data = $.parseJSON(response);
	               var age_data = data['r_agent'];
							     $('#reseller_agent').append('<option value="option1">None</option>');
							    for(var val in age_data) {
								      $("<option />", {value: age_data[val]['ragentID'], text: age_data[val]['agentName'] }).appendTo($('#reseller_agent'));
							    }
							   
	          
	       }
	         
	     });
	 
}

function  disable_agent(id){
	     $('#reselleragent_id').val(id);
	    
}
function  enable_agent(id){
	     $('#activeagent_id').val(id);
	    
}
    
</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<!------    Add popup form    ------->