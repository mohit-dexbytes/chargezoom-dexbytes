<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; 
    opacity:0.6;         
}
</style>

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
   
    <!-- Quick Stats -->
    <div class="row text-center">
        	
        
    </div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All Merchant Invoices</strong> </h2>
            
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Merchant Name</th>
                    <th class="text-right">Invoice</th>
                    <th class="hidden-xs text-right">Due Date</th>
                     <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-center">Status</th>
                    
                    
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
					<td class="text-left"><?php echo $invoice['merchantName']; ?></td>
					<td class="text-right"><strong><?php  echo $invoice['invoice']; ?></strong></td>
					
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					

					  <?php if($invoice['AppliedAmount']!="0.00"){ ?>
                            <td class="hidden-xs text-right">$<?php echo number_format((int)$invoice['AppliedAmount'],2);?></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right">$<?php  echo ($invoice['AppliedAmount'])?$invoice['AppliedAmount']:'0.00'; ?></td>   
						   <?php } ?>
					<td class="text-right hidden-xs">$<?php $vall= number_format((float)$invoice['BalanceRemaining'],2); echo ($vall)?$vall:'0.00'; ?></td>
					<td class="text-center">
					<?php  if($invoice['status']!=''){ echo ucfirst($invoice['status']); }else{ echo ucfirst($invoice['status']); } ?></td>
					
					
					

				</tr>
				
				<?php 
				  }
			   }	
				?>
			
				
			</tbody>
        </table>
        
         <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:999999;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div> 
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->



<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>




<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
