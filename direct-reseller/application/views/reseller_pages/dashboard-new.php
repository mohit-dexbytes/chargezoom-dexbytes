
<!-- Page content -->
<?php
    $this->load->view('alert');
?>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" >
<link rel="stylesheet" type="text/css" href="<?php echo base_url(CSS); ?>/resellerDashboard.css">
<link rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/global.css">
<div id="page-content">
        <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <!-- eCommerce Dashboard Header -->
    
    <!-- END eCommerce Dashboard Header -->

    <?php if (isset($passwordExpDays)) { ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="alert-non-fade alert-warning">
                    <strong>Password Update:</strong> &nbsp;
                    A required password change is due in <?= $passwordExpDays ?> days. &nbsp;Click
                    <a class="alert-link" href="#modal-user-settings" data-toggle="modal" data-original-title="" title="" style="font-weight:700;">here</a>
                    to change your password.
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Quick Stats -->
    <div class="row">
        <?php if ($this->session->flashdata('error-msg')) { ?>
            <div class="alert alert-danger">
                <strong>Error:</strong> <?= $this->session->flashdata('error-msg') ?>
            </div>
        <?php } ?>
        <?php if ($this->session->flashdata('success-msg')) { ?>
            <div class="alert alert-success">
                <?= $this->session->flashdata('success-msg') ?>
            </div>
        <?php } ?>
        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-success rounded-circle mr-2">
                        <i class="fas fa-chart-line ion-logo-usd avatar-title font-26 text-white"></i>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup">$<?php echo number_format($revenue['volume'],2); ?></span></h4>
                            <p class="text-truncate">Total</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-info rounded-circle mr-2">
                        <i class="fas fa-credit-card ion-logo-usd avatar-title font-26 text-white"></i>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup">$<?php echo $revenueCreditCard?number_format($revenueCreditCard,2):'0.00'; ?></span></h4>
                            <p class="text-truncate">Credit Card</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-purple rounded-circle mr-2">
                        <i class="fas fa-money-check ion-logo-usd avatar-title font-26 text-white"></i>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup">$<?php echo $revenueEcheck?number_format($revenueEcheck,2):'0.00'; ?></span></h4>
                            <p class="text-truncate">eCheck</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card-box">
                <div class="media">
                    <div class="avatar-md bg-primary rounded-circle mr-2">
                        <i class="fas fa-receipt ion-logo-usd avatar-title font-26 text-white"></i>
                        
                    </div>
                    <div class="media-body align-self-center">
                        <div class="text-right">
                            <h4 class="font-20 my-0 font-weight-bold"><span data-plugin="counterup"><?php echo $revenue['tier_status']; ?></span></h4>
                            <p class="text-truncate">Transactions</p>
                        </div>
                    </div>
                </div>
                
            </div>
            <!-- end card-box-->
        </div>
        
    </div>
     
    <!-- END Quick Stats -->

    <!-- Annual volumn show -->
    <div class="row">
            <div class="col-sm-12">
                <div class="block full">
                    <div class="card-header py-3 bg-transparent">
                        
                        <h5 class="card-title">Annual Volume: $<span id="res_total">0.00</span></h5>
                    </div>
                    
                    <div class="revenueGraph">
                        <div id="placeholder" style="height:300px;" class="demo-placeholder"></div>
                        <div id="choices" >
                            
                        </div>

                        <div id="legendContainer" class="legend" ></div>

                    </div>
            </div>
            
        </div>
    </div>
    <div class="row">
            
        <div class="col-sm-6">
            <div class="block full ">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Plan Ratio: </h5>
                    
                </div>
                <div id="chart-pie" class="chart_pie_sm"></div>
           </div> 
        </div>
        <div class="col-sm-6">
            <div class="block full">
                <div class="card-header py-3 bg-transparent">
                    <h5 class="card-title">Merchants: <span id="totl_merchant">0</span></span></h5>
                </div>
                <div id="chart-bars-new" class="chart"></div>
           </div> 
        </div>
        
        
    </div>

    <!-- END Classic and Bars Chart -->

    <!-- Live Chart Block -->
    <section style="position:absolute; top:0;">
        <div class="block full" style="padding: 0;
margin: 0;
border: none;">
        <div class="row chart-area" style="">
            <div id="chart-live" class="chart ls" style="visibility:hidden;"></div>
            <div id="chart-classic" class="chart ls" style="visibility:hidden;"></div>
        </div>      
        </div>
    </section>
    
    </div>
    
    
    
    
      <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
      <script src="<?php echo base_url(JS); ?>/pages/compCharts.js"></script>
                
                <link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
                <script type="text/javascript" src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>
      
<script type="text/javascript">
 function format2(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

}
var pchart_color = []; 
var chartColor=[];  
var label1 = 'Free: <?php echo $plan_ratio['free_amount']; ?>';
var label2 = 'Virtual Terminal: <?php echo $plan_ratio['vt_amount']; ?>';
var label3 = 'Automation Suite: <?php echo $plan_ratio['as_amount']; ?>';
var label4 = 'Enterprise Suite: <?php echo $plan_ratio['ss_amount']; ?>';

pchart_color = [[label1, parseFloat(<?php echo $plan_ratio['free_amount']; ?>)],[label2, parseFloat(<?php echo $plan_ratio['vt_amount']; ?>)],[label3, parseFloat(<?php echo $plan_ratio['as_amount']; ?>)],[label4, parseFloat(<?php echo $plan_ratio['ss_amount']; ?>)]];

    chartColor.push('#7f7f7f');
    chartColor.push('#0077f7');
    chartColor.push('#28a745');
    chartColor.push('#46216f');
    showLegend = true;
     
  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
} 

<?php 
    if($plan_ratio['total'] == 0){
?>
pchart_color=[['No Data', 0.0001]];
chartColor = ['#7F7F7F'];
showLegend = false;
<?php
    }

?>
                
$(function(){              
                
  
                  

});

var CompChartsnew = function() {

    return {
        init: function() {
           
                var chartPie = $('#chart-pie');
                
                   $.plot(chartPie,  pchart_color,
                    {
                    colors: chartColor,
                    legend: {show: true},
                    series: {
                        donut: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) { 
                                   
                                   
                                    var dis_label = pieSeries.subLabel;
                                    
                                    return '<div class="chart-pie-label">' + dis_label + ''+ Math.round(pieSeries.percent) + '%<br></div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );
        
        
        }
    }
}();

$(function(){   
    var newcratd = $('#chart-pie');
       CompCharts.init(pchart_color, newcratd, chartColor, showLegend);  
});
var CompCharts = function() {

    return {
        init: function(pchart_data,char_id, color, showLegend = true) {
       
      
       var colorPalette = color;
            
         char_id.shieldChart({
            events: {
                legendSeriesClick: function (e) {
                    // stop the series item click event, so that 
                    // user clicks do not toggle visibility of the series
                    e.preventDefault();
                }
            },
            theme: "bootstrap",
            seriesPalette: colorPalette,
            seriesSettings: {
                donut: {
                    enablePointSelection: false,
                    addToLegend: showLegend,
                    activeSettings: {
                        pointSelectedState: {
                            enabled: false
                        }
                    },
                    enablePointSelection: false,
                    
                    dataPointText: "",
                    borderColor: '#ffffff',
                    borderWidth:2
                }
            },
            
            chartLegend: {
                align: "right",
                renderDirection: 'vertical',
                verticalAlign: "middle",
                legendItemSettings: {
                    bottomSpacing: 10
                }
            },
            exportOptions: {
                image: false,
                print: false
            },
            primaryHeader: {
                text: " "
            },
            tooltipSettings: {
                customHeaderText: function (point, chart) {
                    return point[0].collectionAlias.split(':')[0];
                },
                customPointText: function (point, chart) {
                    let sum = 0;
                    pchart_data.map(data => {
                        sum += data[1];
                    });
                    let percentage = 0;
                    if (point.y !== 0.0001) {
                        percentage = (point.y*100 / sum).toFixed(2);
                    }
                    return shield.format(
                        '<span>{value}</span>',
                        {
                            value: percentage+"%"
                        }
                    );
                }
            },
            axisY: {
                title: {
                    text: ""
                }
            },
            dataSeries: [{
                seriesType: "donut",
                collectionAlias: "Merchants",
                data: pchart_data
            }]
        });
             
            
         

        }
    };
}();
    </script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.time.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.resize.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.pie.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.selection.js"></script>
<script src="<?php echo base_url(JS); ?>/moltran/jquery.flot.stack.js"></script>

<link href="<?php echo base_url(CSS); ?>/moltran/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
<script type="text/javascript">


        

        $.ajax({
            type:"POST",
            url : "<?php echo base_url(); ?>Reseller_panel/dashboard_chart_json",
            data:{'resellerID': <?php echo $resellerID; ?>},
            dataType:'json',
            success: function (data) {
                var general_volume = data;

               
                var ch_date='';  var  earnwww='0.00';
                
                 ch_date = general_volume.custmonth;

                 earnwww = general_volume.volume;
                
                 newDataSalesmerch = general_volume.merchant;
                var disabledDataSalesmerch = general_volume.disbaledMerchant;
                $('#res_total').html(format2(general_volume.total_payment)) ; 
                $('#totl_merchant').html(general_volume.total_merchant);

                

                $("#chart-bars-new").shieldChart({
                    theme: "light",
                    primaryHeader: {
                        text: " "
                    },
                    exportOptions: {
                        image: false,
                        print: false
                    },
                    axisX: {
                        categoricalValues:ch_date
                    },
                    axisY: {
                        title: {
                            text: ""
                        }
                    },
                    
                    seriesSettings: {
                        bar: {
                            dataPointText: {
                                enabled: true
                            }
                        }
                    },
                    chartLegend: {
                        enabled: false
                    },
                    dataSeries: [{
                        axisY: 0,
                        seriesType: "bar",
                        color: '#3498db',
                        collectionAlias: "Merchant",
                        data: newDataSalesmerch
                    },{
                        axisX: 0,
                        seriesType: "bar",
                        color: 'rgba(255, 99, 132, 0.5)',
                        collectionAlias: "Merchant",
                        data: disabledDataSalesmerch
                    }]
                });


                var x1 = general_volume.revenu_volume;
                var x2 = general_volume.online_volume;
                var x3 = general_volume.eCheck_volume;



                var y1 = general_volume.revenu_month;
                var legendContainer = document.getElementById("legendContainer");


                var datasets = {
                    "Total": {
                        data: x1, label: "Total",color:'#33b86c',hoverable:true, shadowSize: 2,highlightColor: '#',clickable: true
                    },
                    "Credit Card": {
                            data: x2, label: "Credit Card",color:'#007bff',hoverable:true,clickable: true
                        },
                    "eCheck": {
                         data: x3, label: "eCheck",color:'#7e57c2',hoverable:true,clickable: true
                    },
                    
                };
                var i = 1;
                

                var choiceContainer = $("#choices");

                $.each(datasets, function(key, val) {
                    var checkM = '';
                    var classOpacity = '';
                    if(key != 'Total'){
                         checkM = "checked='checked'";
                    }else{
                         classOpacity = 'opacityManage';
                    }
                    choiceContainer.append("<div class='displayLegend " + classOpacity + " ' id='color" + i + "'><input class='checkboxNone' type='checkbox' name='" + key +
                        "' " + checkM + " data-id='" + i + "' id='id" + key + "'></input> <label id='color" + i + "' style='padding:1px;width: 12px;height: 12px;background:"+ val.color +" ;' for='id" + key + "' id='colorSet" + i + "'></label>" +
                        "<label class='legendLabel' for='id" + key + "'>"
                        + val.label + "</label> </div>");
                    ++i;
                    
                });

                var data = [];

                choiceContainer.find("input:checked").each(function () {
                    var key = $(this).attr("name");
                    if (key && datasets[key]) {
                        data.push(datasets[key]);
                    }
                });
                choiceContainer.find("input").click(plotAccordingToChoices);
                $('.checkboxNone').change(function() {
                    var id = $(this).attr("data-id");
                    var checkGraph = choiceContainer.find("input:checked").length;
                    if(checkGraph != 0){
                      if ($(this).prop('checked') == true){
                          $("#color"+id).removeClass('opacityManage');
                      } else {
                          $("#color"+id).addClass('opacityManage');
                      }
                    }
                    
                });
                function plotAccordingToChoices() {

                    var data = [];

                    choiceContainer.find("input:checked").each(function () {
                        var key = $(this).attr("name");
                        
                        if(key != 'Total'){
                            if (key && datasets[key]) {
                                data.push(datasets[key]);
                            }
                        }else{
                            if ($('#idTotal').prop('checked') == true){
                                if (key && datasets[key]) {
                                    data.push(datasets[key]);
                                }
                            }
                        
                        }
                    });

                    if (data.length > 0) {
                        $.plot("#placeholder", data, {
                            series: { lines: { show: !0, fill: !0, lineWidth: 1, fillColor: { colors: [{ opacity: 0.2 }, { opacity: 0.9 }] } }, points: { show: !0 }, shadowSize: 0 },
                            legend: {
                                position: "nw",
                                margin: [0, -24],
                                noColumns: 0,
                                backgroundColor: "transparent",
                                labelBoxBorderColor: null,
                                labelFormatter: function (o, t) {
                                    return o + "&nbsp;&nbsp;";
                                },
                                width: 30,
                                height: 2,
                                container: legendContainer,
                                onItemClick: {
                                      toggleDataSeries: true
                                  },
                            },
                            grid: { hoverable: !0, clickable: !0, borderColor: i, borderWidth: 0, labelMargin: 10, backgroundColor: 'transparent' },
                            yaxis: { min: 0, max: 15, tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            xaxis: { ticks: y1,tickColor: "rgba(108, 120, 151, 0.1)", font: { color: "#8a93a9" } },
                            tooltip: !0,
                            tooltipOpts: { content: function(data, x, y, dataObject) {
                                       
                                            var XdataIndex = dataObject.dataIndex;
                                            var XdataLabel = dataObject.series.xaxis.ticks[XdataIndex].label;
                                            return '$'+format2(y);
                                        }, shifts: { x: -60, y: 25 }, defaultTheme: !1 },

                            
                            
                            yaxis: {
                                autoScale:"exact"
                            }
                        }); 
                    }
                }
                plotAccordingToChoices();
                
               
                 
                 
            }
        });

       function getTooltip(label, x, y) {
     
            return "Your sales for " + x + " was $" + y; 
        }



</script>    
<style>
#chart-pie{
    height: 410px !important;
    margin-bottom: -18px;
}
#chart-pie .shield-container {
    margin-top: -50px;
}
#chart-pie text tspan {
    visibility: hidden;
}
#chart-pie g text tspan {
    visibility: visible;
}
#chart-pie g {
    opacity: 1.5;
    borderWidth:2px;
}
    #chart-classic g text tspan {
    visibility: visible;
}
#chart_pie_customer g {
    opacity: 1.5;
    borderWidth:2px;
}

#chart-classic text tspan {
    visibility: hidden;
}

    #chart-bars-new g text tspan {
    visibility: visible;
}
#chart-bars-new g {
    opacity: 1.5;
    borderWidth:2px;
}

#chart-bars-new text tspan {
    visibility: hidden;
}
.canvasjs-chart-credit{
    display:none;
}
.chart_new {
    height: 260px !important;
}
#chart-bars-new tspan{
    font-size: 12px;
    color: #333333;
    font-family: Segoe UI, Tahoma, Verdana, sans-serif;
}
</style>
    
        
   