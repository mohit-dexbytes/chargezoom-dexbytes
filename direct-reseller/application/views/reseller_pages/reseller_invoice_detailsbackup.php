<?php 
  /*  
	  $status ='';
	  $today =date('Y-m-d');
	  $due_date = date('Y-m-d', strtotime($invoice_data['DueDate'])); 
	  if($due_date < $today && $invoice_data['IsPaid'] =='false' ){  $status ='Pastdue'; }
	  $type ='';
	//  $difference = $date2 - $date1Timestamp;
     $date1=strtotime($due_date);  
      $date2=strtotime($today); 
	  
	  $type_text='';
	  
	  if($date1 >= $date2)
	  {
	      $diff= $date2 - $date1;
	    //  $diff=  $diff/(24*60*60);
	      $diff =floor($diff / (60*60*24) );
	      $type = 3;
	      $type_text = "Invoice due soon/upcoming";
	  }
	  else
	  {
	      
	     $diff= $date2 - $date1;
	      //$diff=  $diff/(24*60*60);
	      $diff= floor($diff / (60*60*24) );
	      if($diff > 7){
	       $type = 2;
	       $type_text = "Invoice past due/overdue";
	       
	      }else{
	       $type = 1;
	        $type_text = "Invoice Due";
	      }
	       
	      
	  }
	*/
	  
?>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">


    
		
		 	 
					
   <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- Products Block -->
    <div class="block">
        <!-- Products Title -->
        <div class="block-title">
			<div class="block-options pull-right">
				<a href="<?php echo base_url().'home/invoice_details_print/'.$invoice['invoice']; ?>" class="btn btn-alt btn-sm btn-info" data-toggle="tooltip" title="Print Invoice">Download PDF</a>
        	</div>
           
          
            <h2><strong>Invoice</strong> Details</h2>
		
		
        </div>
      
      
        
             <form id="form-validation" action=""  method="post" enctype="multipart/form-data" class="form-horizontal ">
	
			     
                <h4>Invoice Number: <strong><?php echo $invoice['invoiceNumber']; ?></strong></h4>
                <h5>Customer Name:  <strong><?php echo $invoice['resellerName']; ?></strong></h5>
                <h5>Invoice Date:  <?php echo date('m/d/Y',strtotime($invoice['createdAt'])); ?></h5>
                 <h5>Due Date:  <?php echo date('m/d/Y',strtotime($invoice['DueDate'])); ?></h5>
                 <h5>Invoice Status:  <strong><?php if($invoice['status']==1) echo 'Paid'; else echo 'Pending';  ?></strong></h5>
            
                   <h5>Payment Method: -- <strong > 
                    
                     </strong></h5>
                 
                
						 
					<div class="table-responsive">
				    <table class="table table-bordered table-vcenter">
					<thead>
                    <tr>
                         <th>S.N.</th>
                        <th>Plan Name</th>
                        
                         <th class="text-right">Unit Rate</th>
                         <th class="text-right">Qty</th>
                         <th class="text-right">Amount</th>
					 </tr>
                </thead>
                <tbody>
				       
				  <?php foreach($items as $k=>$item) { ?>
                    <tr>
                          <td><?php echo ($k+1);   ?></td>
                        <td><?php echo $item['itemName'];   ?></td>
                        <td class="text-right"><?php echo number_format($item['itemPrice']/$item['itemQty'],2);  ?></td>
                        <td class="text-right"><?php echo $item['itemQty'];  ?></td>
                        <td class="text-right"><?php echo number_format($item['itemPrice'],2); ?></td>
                       
                        </tr>
			  <?php } ?>		
                 
                    
                    
					<tr class="info">
                        <td colspan="4" class="text-right text-uppercase"><strong>Subtotal</strong></td>
                        <td class="text-right">$<?php echo number_format($invoice['BalanceRemaining'],2); ?></td>
                    </tr>
				
				     <tr class="success">
                        <td colspan="4" class="text-right text-uppercase"><strong>Discount</strong></td>
                        <td class="text-right">$0.00</td>
                    </tr>
                  
					<tr class="danger">
                        <td colspan="4" class="text-right text-uppercase"><strong>Total Due</strong></td>
                        <td class="text-right"><strong>$<?php echo number_format($invoice['BalanceRemaining'],2); ?></strong></td>
                    </tr>
					</tbody>
					
				</table>
			</div>	
			<?php /*		<div class="col-md-12">
					
					<div class="pull-right">
					
					<a href="#invoice_process" class="btn btn-sm btn-success"  disabled="true"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
				
                   <br><br>
					  </div>						 
					
					</div>  */ ?>
					
				
				 <input type="hidden"  id="invNo" name="invNo" value="" />
			    </form>  
			
        <div>
    </div>  
   
    <!-- Addresses -->
   <div class="row">
        <div class="col-sm-6">
            <!-- Billing Address Block -->
            <div class="block">
                <!-- Billing Address Title -->
                <div class="block-title">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
               
			
                <h4><strong></strong></h4>
				<address>
					<strong></strong><br>
                       
				
					<i class="fa fa-phone"></i> <br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> </a>
				</address>
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
        <div class="col-sm-6">
            <!-- Shipping Address Block -->
            <div class="block">
                <!-- Shipping Address Title -->
                <div class="block-title">
                    <h2><strong>Shipping </strong> Address</h2>
                </div>
                <!-- END Shipping Address Title -->

                <!-- Shipping Address Content -->
                  <h4><strong></strong></h4>
				<address>
					<strong></strong><br>
					 <br>
					 <br>
					 <br><br>
					<i class="fa fa-phone"></i> <br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> </a>
				</address>
                <!-- END Shipping Address Content -->
            </div>
            <!-- END Shipping Address Block -->
        </div>
    </div>
    <!-- END Addresses -->

    <!-- Log Block -->
    
    <!-- END Log Block -->   
</div>


<script src="<?php echo base_url(JS); ?>/pages/customer_details_company.js" ></script>
<script>
 $('.testbtn').click(function(){
	 var form_data=$('#form-validation').serialize();
	var index = ''; 
	   if($(this).val()=='Save')
	   {
			index ="self";
	   }
		else{
		index ="other";	
		}
								$('#index').remove(); 
			 	 
                                    
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'index',
											'name': 'index',
										
											'value':index ,
											}).appendTo($('#form-validation'));
	
	$('#form-validation').submit();
	
 });
</script>
<div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title text-center">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"> </label>
				
                  <form id="form-validation1" action="<?php echo base_url(); ?>company/Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
				  
				   <input type="hidden" id="invoicetempID" name="invoicetempID" value=""> 
			    <input type="hidden" id="customertempID" name="customertempID" value=""> 
					<input type="hidden" id="invoiceCode" name="invoiceCode" value=""> 
						<input type="hidden" id="sendmailbyinvdtl" name="sendmailbyinvdtl" value="1">
			<?php /*	<input type="hidden" id="tempCompanyID" name="tempCompanyID" value=""> 
				
				<input type="hidden" id="tempCompanyName" name="tempCompanyName" value=""> 
                   <input type="hidden" name="type" id="type" value="<?php echo $type ?>" />                */ ?>
							 <input type="hidden" name="type" id="type" value="<?php echo $type ?>" /> 			
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                         <input type="text" name="type_text"  class="form-control"   readonly='readonly' id="type_text" value="<?php echo $type_text ?>"   />
                                          
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                               
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="CC Email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="BCC Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="replyEmail">Set the "Reply-To" to this address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Email">
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
									
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
										
										<input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Email Subject">
                                    </div>
										
                                    </div>
                                     
                                      <div class="form-group">
									  
									  
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                        <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                                        
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

