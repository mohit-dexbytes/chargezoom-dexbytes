

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
   
    <!-- Quick Stats -->
    <div class="row text-center">
       
        
    </div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Agent Commissions</strong> </h2>
            
        </div>
      <form method="post" action="" >
       
       <div class="col-md-2">
         <select name="month" id="monthly"  class="form-control">
             
             <?php  foreach($months as $month){ ?>
             <option value="<?php echo $month['id']; ?>"><?php echo $month['val']; ?></option>
             <?php } ?>
          </select>
       
         
       </div> 
        <div class="col-md-2">
         <select name="year" id="yearly"  class="form-control">
             <?php 
	        	 $firstYear = (int)date('Y') - 8;
                $lastYear = $firstYear + 15;
                for($i=$firstYear;$i<=$lastYear;$i++){  ?>
               <option value="<?php echo $i;  ?>"><?php echo $i;  ?> </option>
			<?php } ?>
          </select>
         
       </div> 
    <div class="col-md-4">
        <input type="submit" name="getData" class="btn btn-sm pull-left rp-btn btn-info" id="getData" value="Go" /> 
        </div>
           
           
       
            
     </form>
    
     <br>
     <br>
     <br>
     
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="agent_commision" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Agent Name</th>
                    <th class="text-right">Merchant Revenue</th>
                    <th class="hidden-xs text-right">Commission %</th>
                    <th class="text-center">Commission Total</th>
                    
                    
                </tr>
            </thead>
            <tbody>
			
				<tr>
					<td class="text-left"></td>
					<td class="text-right"><strong></strong></td>
					
                    <td class="hidden-xs text-right"></td>
					<td class="text-center"></td>
					
					
					

				</tr>
				
				
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
        
         <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:-100px;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->



<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#agent_commision').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>




<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
</div>
