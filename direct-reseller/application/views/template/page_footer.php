<?php
/**
 * page_footer.php
 *
 * Author: pixelcave
 *
 * The footer of each page
 *
 */
?>

 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"> </script>
    <script>
	

	
 



</script>  
           
        </div>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
 </div>
<!-- END Page Wrapper -->




<!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
<a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>
<?php 

    if($this->session->userdata('reseller_direct_logged_in'))
    {
        $session	= $this->session->userdata('reseller_direct_logged_in');
        $changePasswordURL = base_url('home/change_password');

        $userName = $session['resellerfirstName'].' '.$session['lastName'];
        $userEmail = $session['resellerEmail'];
        $userID = $session['resellerID'];
    } elseif ($this->session->userdata('agent_direct_logged_in')) {
        $session	= $this->session->userdata('agent_direct_logged_in');
        $changePasswordURL = base_url('SettingConfig/changePassword');
        $userName = $session['agentName'];
        $userEmail = $session['agentEmail'];
        $userID = $session['ragentID'];
    } else {
        $userEmail = $userName = '';
        $changePasswordURL = base_url('home/change_password');
        $userID = 0;
    }
	

?>
<!-- User Settings, modal which opens from Settings link (found in top right user menu) and the Cog link (found in sidebar user info) -->




<div id="modal-user-settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Change Password</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                <form id="change_res_pwd" action="<?php echo $changePasswordURL; ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-bordered" >
                    <fieldset>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Name</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $userName; ?></p>
								<input type="hidden" value="<?php echo $userID;  ?>"  name="user_id" id="user_id" />
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label">Email</label>
                            <div class="col-md-8">
                                <p class="form-control-static"><?php echo $userEmail;  ?></p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-currentpassword">Current Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-currentpassword" name="user-settings-currentpassword" class="form-control" data-placeholder="Current Password">
                            </div>
                        </div>
						<div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-password">New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-password" name="user-settings-password" class="form-control" data-placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="user-settings-repassword">Confirm New Password</label>
                            <div class="col-md-8">
                                <input type="password" id="user-settings-repassword" name="user-settings-repassword" class="form-control" data-placeholder="Confirm New Password!">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 text-right">
                                 <button type="submit" class="btn btn-sm btn-success newSaveButton">Save</button>
                                <button type="button" class="btn btn-sm btn-default newCloseButton" data-dismiss="modal">Cancel</button>
                               
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>



<script>


$(document).ready(function(){
     
     $('#change_res_pwd').validate({
         
               	ignore:":not(:visible)",

       errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                 
	        	rules: {
                   
                    'user-settings-currentpassword':{
                         required: true,
                          minlength:6,
						maxlength:12,
							 remote : {
						     
						        beforeSend:function() { 
					      	 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'  style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					       },
					           complete:function() {
						              $(".overlay1").remove();
					             },
                                url :"<?php echo base_url(); ?>home/chk_new_password",
                                type: "POST",
                                
                                dataType: 'json',
                                dataFilter: function(response) {
                                    
                                    var rsdata = jQuery.parseJSON(response);
                                 
                                 
                                     if(rsdata.status=='success')
                                     return true;
                                     else{
                                        
                                     return false;
                                     }
                                }
                            },
				    
					
                    },
					'user-settings-password': {
                        required: true,
				    	 minlength:6,
						maxlength:12,
                    },
					'user-settings-repassword': {
                        required: true,
                        equalTo:'#user-settings-password',
					
                    }
                
                   
                  
              },
              
            
                
                messages:
               {
                'user-settings-currentpassword':
                {
                    required: "This is required",
                      minlength: "Please enter minimun 5 characters",
                      remote: 'Password is not same, enter correct password',
                },
               
            },
     });       
});
</script>

<!-- END User Settings -->

<script type="text/javascript">
  function checkForm(form)
  {
	var curr_pass = $('#user-settings-currentpassword').val();
	var new_pass = $('#user-settings-password').val();
	var confirm_pass = $('#user-settings-repassword').val();
	
	if(curr_pass == '') {
		alert("Please enter current password!");
        $('#user-settings-currentpassword').focus();
        return false;
	}
	else if(new_pass == '') {
		alert("Please enter new password!");
        $('#user-settings-password').focus();
        return false;
	}
    else if( new_pass != "" && new_pass == confirm_pass ) {
      if(new_pass.length < 5){
        alert("Password must contain at least Five characters!");
        $('#user-settings-password').focus();
        return false;
      }
    } 
	else {
      alert("Confirm Password dismatched.!");
      $('#user-settings-repassword').focus();
      return false;
    } 
    return true;
  }
    setInterval(function(){
        $.ajax({
            type: 'POST',
            url : "<?php echo base_url(); ?>ResellerCron/check_session",
            success: function(response){
                data=$.parseJSON(response);    

                if(data['status']=='failed'){
                    location.reload(true);
                }
                
            }
        });
    },5000);

  $(document).on('click','.cvvMask',function(){
  
    $(this).attr('type','password');
    $(this).attr('maxlength','4');
  });
  $(document).on('click','.CCMask',function(){
    $(this).attr('type','password');
  });
</script>

<input type='hidden' id='rs_base_url' value='<?php echo base_url(); ?>'>
<script src="<?php echo base_url(JS); ?>/sync-dashboard.js"> </script>