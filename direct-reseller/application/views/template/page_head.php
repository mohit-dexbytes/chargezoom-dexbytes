<?php
/**
 * page_head.php
 *
 * Author: Ecrubit
 *
 * Header and Sidebar of each page
 *
 */
?>

<style>

.error{color:red; }
.mt-1 {
        margin-top: 10px;
        color: gray;
        position: fixed;
        left: 15px;
        bottom: 20px;
    }
.sidebar-brand, .sidebar-title {
    margin:  0px;
    font-weight: 300;
    font-size: 18px;
    display: block;
    color: #ffffff;
	background-color: rgba(0, 0, 0, 0.3) !important;
	  height: 70px;
}  

.navbar{
     height: 70px;
     background-color: #fff;
}

.btn-search{
	background-color: #4d73e0 !important;
}
.icon-color{
	color:#ffffff;
}
.top-header{
  top:16px;	
} 

.nav.navbar-nav-custom > li > a {
    min-width: 50px;
    padding: 15px 7px;
    line-height: 40px;
    text-align: center;
    position: relative;
}
#img-spinner{
    animation: spin 2s linear infinite;
}
p{
	word-break: break-all;
}
  ol {
    display: block;
    list-style-type: decimal;
    -webkit-margin-before: 1em;
    -webkit-margin-after: 1em;
    -webkit-margin-start: 0px;
    -webkit-margin-end: 0px;
    -webkit-padding-start: 40px;
}

.breadcrumb {
    display: flex;
    flex-wrap: wrap;
    padding-top: 0.75rem;
    padding-right: 1rem;
    padding-bottom: 0.75rem;
    padding-left: 1rem;
    margin-bottom: 1rem;
    list-style: none;
    border-top-left-radius: 0.25rem;
    border-top-right-radius: 0.25rem;
    border-bottom-right-radius: 0.25rem;
    border-bottom-left-radius: 0.25rem;
    font-size: 16px;
    background-color: #ffffff;
} 
 
 .breadcrumb>li {
    display: inline-block;
}
ul, ol {
    margin-top: 0;
    
}
li {
    
    text-align: -webkit-match-parent;
}
  .breadcrumb-item a {
    color: #418eff;
}

	.breadcrumb-item a:hover{
	text-decoration:none;
	color: #418eff;
	}
	.breadcrumb-item + .breadcrumb-item::before {
    display: inline-block;
    padding-right: .5rem;
    padding-left: .5rem;
    color: #6c757d;
    content: "/";
}
.sidebar-visible-lg .sidebar-nav a > .sidebar-nav-icon, .sidebar-visible-lg .sidebar-nav a > .sidebar-nav-indicator {
    width: 11px;
}
.cust_view a{
    color: #167bc4;
    text-decoration: none;
}

.tooltip-inner {
  white-space:nowrap;
  max-width:none;
}
@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
@media only screen and (min-device-width: 320px){
	.top-header {
    top: 18px;
    position: absolute;
    left: 20%;
    min-width: 20%;
}
.top-header .input-group {
    width: 70%;
}
.sidebar-visible-lg .sidebar-nav a > .sidebar-nav-icon, .sidebar-visible-lg .sidebar-nav a > .sidebar-nav-indicator {
    width: 11px;
}
}
@media only screen and (min-device-width: 420px){
	.top-header {
    left: 30%;
}
.top-header .input-group {
        width: 35% !important;
}
}
@media only screen and (min-device-width: 600px){
	.top-header {
    left: 5%;
}
}
@media only screen and (min-device-width: 720px){
	.top-header {
		top: 18px;
    position: absolute;
    left: 58%;
    min-width: 20%;
}
.top-header .input-group {
        width: 30%;
}
.sidebar-visible-lg .sidebar-nav a > .sidebar-nav-icon, .sidebar-visible-lg .sidebar-nav a > .sidebar-nav-indicator {
    width: 11px;
}
}

@media only screen and (min-device-width: 1025px){
	.top-header {
		top: 18px;
    position: absolute;
    left: 77%;
    min-width: 20%;
}
.top-header .input-group {
        width: 70%;
}
.sidebar-visible-lg .sidebar-nav a > .sidebar-nav-icon, .sidebar-visible-lg .sidebar-nav a > .sidebar-nav-indicator {
    width: 11px;
}
}
@media only screen and (min-device-width: 1281px){
	.top-header {
    top: 18px;
    position: absolute;
    left: 68%;
    min-width: 20%;
}
.top-header .input-group {
    width: 75%;
}
.sidebar-visible-lg .sidebar-nav a > .sidebar-nav-icon, .sidebar-visible-lg .sidebar-nav a > .sidebar-nav-indicator {
    width: 11px;
}
}
@media screen and (min-width: 1300px){
	.top-header {
		top: 18px;
    position: absolute;
    left: 70%;
    min-width: 20%;
}
.top-header .input-group {
        width: 75%;
}
}
@media screen and (min-width: 1365px){
	.top-header {
		top: 18px;
    position: absolute;
    left: 73%;
    min-width: 20%;
}
.top-header .input-group {
        width: 100%;
}
}

/**********Styles for Firefox**********/
@-moz-document url-prefix() {
       .top-header .input-group {
        width: 100% !important;
}
}

/**********Styles for Opera**********/
 @media all and (-webkit-min-device-pixel-ratio:10000), not all and (-webkit-min-device-pixel-ratio:0) {
        .top-header .input-group {
        width: 75% !important;
}
 }
 /**********Styles for Internet Explorer**********/
@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none){
  .top-header .input-group {
        width: 65% !important;
}
} 
 
@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) and (min-device-width: 720px) { 
 	.top-header {
    left: 62%;
}
}
@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) and (min-device-width: 1025px) { 
 	.top-header {
    left: 5%;
}
}
@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) and (min-device-width: 1281px) { 
 	.top-header {
    left: 5%;
}
}
.search_head
{
    height: auto;
    border-color: #e8e8e8!important;
    background-color: #f8f9fd;
}
ul.sidebar-nav li a:after
{
    background: #555;
    position: absolute;
    content: '';
    width: 90%;
    height: 1px;
    left: 4%;
    bottom: 0px;
}
.sidebar-nav a
{
    position: relative;
}
ul.sidebar-nav li li a:after, ul.sidebar-nav li a.open:after, ul.sidebar-nav li a.active:after
{
   left: 0;
    width: 100%;
}
@media only screen and (-webkit-min-device-pixel-ratio: 1) and (max-device-width: 1024px) and (min-device-width: 768px){
    .top-header
    {
        left: 10% !important;
    }
}
.searchBtnHeight{
    height: 32px !important;
}

@media (max-width: 767px){
    .search_head{
        width: 120px !important;
    }
}
#searchBtn{
    background-color: #A5A5A5 !important;
    line-height: initial;
}
</style>
<script>
$(document).ready(function(){
    $('[data-toggle="modal"]').tooltip();   
});
</script>
<script>
    $(function(){
    FormsWizard.init(); });
	$(function(){
        setTimeout(function() {
          
          jQuery('.alert').fadeOut();
        }, 2000); // <-- time in milliseconds
    });
</script>
<?php 
  $CI = & get_instance(); 
  
if($CI->session->userdata('agent_direct_logged_in'))
 {
 $user_info = $CI->session->userdata('agent_direct_logged_in'); 
 }else{
 $user_info = $CI->session->userdata('reseller_direct_logged_in');
 }
 
 
 ?>
<div id="page-wrapper"<?php if ($template['page_preloader']) { echo ' class="page-loading"'; } ?>>
    <!-- Preloader -->
    <!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
    <!-- Used only if page preloader is enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
     <div class="preloader themed-background-pre">
        
        <div class="inner">
            <h3 class="text-light visible-lt-ie10"><strong>Loading..</strong></h3>
            <div class="spinner">
			<img id="img-spinner" src="<?php echo base_url(IMAGES); ?>/loading.gif" alt="loading">
			</div>			
        </div>	
    </div>
    <!-- END Preloader -->

    <!-- Page Container -->
    <!-- In the PHP version you can set the following options from inc/config file -->
    <!--
        Available #page-container classes:

        '' (None)                                       for a full main and alternative sidebar hidden by default (> 991px)

        'sidebar-visible-lg'                            for a full main sidebar visible by default (> 991px)
        'sidebar-partial'                               for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
        'sidebar-partial sidebar-visible-lg'            for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
        'sidebar-mini sidebar-visible-lg-mini'          for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
        'sidebar-mini sidebar-visible-lg'               for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)

        'sidebar-alt-visible-lg'                        for a full alternative sidebar visible by default (> 991px)
        'sidebar-alt-partial'                           for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
        'sidebar-alt-partial sidebar-alt-visible-lg'    for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)

        'sidebar-partial sidebar-alt-partial'           for both sidebars partial which open on mouse hover, hidden by default (> 991px)

        'sidebar-no-animations'                         add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!

        'style-alt'                                     for an alternative main style (without it: the default style)
        'footer-fixed'                                  for a fixed footer (without it: a static footer)

        'disable-menu-autoscroll'                       add this to disable the main menu auto scrolling when opening a submenu

        'header-fixed-top'                              has to be added only if the class 'navbar-fixed-top' was added on header.navbar
        'header-fixed-bottom'                           has to be added only if the class 'navbar-fixed-bottom' was added on header.navbar

        'enable-cookies'                                enables cookies for remembering active color theme when changed from the sidebar links
    -->
    <?php
        $page_classes = '';

        if ($template['header'] == 'navbar-fixed-top') {
            $page_classes = 'header-fixed-top';
        } else if ($template['header'] == 'navbar-fixed-bottom') {
            $page_classes = 'header-fixed-bottom';
        }

        if ($template['sidebar']) {
            $page_classes .= (($page_classes == '') ? '' : ' ') . $template['sidebar'];
        }

        if ($template['main_style'] == 'style-alt')  {
            $page_classes .= (($page_classes == '') ? '' : ' ') . 'style-alt';
        }

        if ($template['footer'] == 'footer-fixed')  {
            $page_classes .= (($page_classes == '') ? '' : ' ') . 'footer-fixed';
        }

        if (!$template['menu_scroll'])  {
            $page_classes .= (($page_classes == '') ? '' : ' ') . 'disable-menu-autoscroll';
        }

        if ($template['cookies'] === 'enable-cookies') {
            $page_classes .= (($page_classes == '') ? '' : ' ') . 'enable-cookies';
        }
    ?>
    <div id="page-container"<?php if ($page_classes) { echo ' class="' . $page_classes . '"'; } ?>>
        
        <!-- Main Sidebar -->
        <div id="sidebar">
            <!-- Wrapper for scrolling functionality -->
            <div id="sidebar-scroll">
                <!-- Sidebar Content -->
                <div class="sidebar-content">
                    <!-- Brand -->
                    <div class="sidebar-brand">
                        <img style="width:200px; height:70px;" src="<?php echo base_url(IMAGES); ?>/chargezoom_new.png" class="icon" alt="avatar">
                    </div>
                    <!-- END Brand -->

                    <!-- User Info -->
               

                    

                    <?php if ($primary_nav) { ?>
                    <!-- Sidebar Navigation -->
                    <ul class="sidebar-nav">
                        <?php foreach( $primary_nav as $key => $link ) {
                            $link_class = '';
                            $li_active  = '';
                            $menu_link  = '';
							$name = "";
                            // Get 1st level link's vital info
						    $name        = (isset($link['name']) && $link['name']) ? $link['name'] : '#';
                           $url          = (isset($link['url']) && $link['url']) ? $link['url'] : '#';
				
                        
					   
					    	$active     = (isset($link['url']) && ($template['active_page'] == $link['page_name'])) ? ' active' : ''; 
                            $icon       = (isset($link['icon']) && $link['icon']) ? '<i class="' . $link['icon'] . ' sidebar-nav-icon"></i>' : '';
							
						
					  if (isset($link['sub']) && $link['sub']) {
                                // Since it has a submenu, we need to check if we have to add the class active
                                // to its parent li element (only if a 2nd or 3rd level link is active)
								
                                foreach ($link['sub'] as $sub_link) {
								
								
							
                                    if (in_array($template['active_page'], $sub_link)) {
                                    $li_active = ' class="active"';
                                        break;
                                    }

                                    // 3rd level links
                                    if (isset($sub_link['sub']) && $sub_link['sub']) {
                                        foreach ($sub_link['sub'] as $sub2_link) {
                                            if (in_array($template['active_page'], $sub2_link)) {
                                                $li_active = ' class="active"';
                                                break;
                                            }
                                        }
                                    }
                                }

                                $menu_link = 'sidebar-nav-menu';
                            }

                            // Create the class attribute for our link
                            if ($menu_link || $active) {
							
                                $link_class = ' class="'. $menu_link . $active .'"';
                            }
                        ?>
                           
                        <?php if ($url == 'header') { // if it is a header and not a link 
                        ?>
						
                        <li class="sidebar-header">
                            <?php if (isset($link['opt']) && $link['opt']) { // If the header has options set
                            ?>
                            <span class="sidebar-header-options clearfix">ede<?php echo $link['opt']; ?></span>
                            <?php } ?>
                            <span class="sidebar-header-title"><?php echo $link['name']; ?></span>
                        </li>
                        <?php } else { // If it is a link 
						?>
						
                        <li<?php echo $li_active; ?>>
						
						<?php
                        $userMerchantView = 1;
                        $permissionCheckbox = 1;
						if($login_info['login_type']=='AGENT'){
						     
                             $agentData = getAgentDataByAgentID($login_info['ragentID']);
                             if( $agentData['generalSettings'] == 1 && $agentData['plans'] == 1 && $agentData['emailTemplates'] == 1 && $agentData['merchantPortal'] == 1 && $agentData['api'] == 1){
                                $permissionCheckbox = 0;
                             }
                             if($agentData['userMerchantView'] == 1){
                                $userMerchantView = 1;
                             }else{
                                $userMerchantView = 0;
                             }
						     $c_url= base_url().'SettingConfig/agent_setting';
						}
                        
                        /*Condition for manage permission check if access is full admin*/
                        if(($login_info['login_type']=='AGENT' && $permissionCheckbox == 0 && $link['name']=='Configuration' ) || ($login_info['login_type']=='AGENT' && $userMerchantView == 0 && $link['name']=='Merchant Management') ){

                        }
                        else if(isset($agentData['agentManagement']) && $agentData['agentManagement'] == 1 && $link['name']=='Agent Management' ){

                        }else{

                            
							if( $login_info['login_type']=='AGENT' && $link['name']=='ConfigurationBK' ) 
							{
							?>
						
							
								
							<?php }else{ ?>
								
						    <a href="<?php echo $url; ?>" <?php echo $link_class; ?>><?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                            ?><i class="fa fa-angle-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><?php } echo $icon; ?><span class="sidebar-nav-mini-hide"><?php echo $link['name']; ?></span></a>
							
							
                            <?php if (isset($link['sub']) && $link['sub']) { // if the link has a submenu 
                            ?>
                            <ul>
                                <?php foreach ($link['sub'] as $sub_link) {
                                    $link_class = '';
                                    $li_active = '';
                                    $submenu_link = '';

                                    // Get 2nd level link's vital info
                                    $url        = (isset($sub_link['url']) && $sub_link['url']) ? $sub_link['url'] : '#';
                                
                                  $active     = (isset($sub_link['url']) && ($template['active_page'] == $sub_link['page_name'])) ? ' active' : '';

                                    // Check if the link has a submenu
                                    if (isset($sub_link['sub']) && $sub_link['sub']) {
                                        // Since it has a submenu, we need to check if we have to add the class active
                                        // to its parent li element (only if a 3rd level link is active)
                                        foreach ($sub_link['sub'] as $sub2_link) {
                                            if (in_array($template['active_page'], $sub2_link)) {
                                                $li_active = ' class="active"';
                                                break;
                                            }
                                        }

                                        $submenu_link = 'sidebar-nav-submenu';
                                    }

                                    if ($submenu_link || $active) {
                                        $link_class = ' class="'. $submenu_link . $active .'"';
                                    }
                                if(isset($agentData['generalSettings']) && $agentData['generalSettings'] == 1 && $sub_link['page_name'] =='general_setting' ){
                                    
                                }else if(isset($agentData['plans']) && $agentData['plans'] == 1 && $sub_link['page_name'] =='reseller_plans' ){

                                }else if(isset($agentData['emailTemplates']) && $agentData['emailTemplates'] == 1 && $sub_link['page_name'] =='email_template' ){
                                    
                                }else if(isset($agentData['merchantPortal']) && $agentData['merchantPortal'] == 1 && $sub_link['page_name'] =='setting_merchant_portal' ){
                                    
                                }else if(isset($agentData['userMerchantDisabled']) && $agentData['userMerchantDisabled'] == 0 && $agentData['allMerchantDisabled'] == 0 && $sub_link['page_name'] =='disable_merchant_list' ){
                                    
                                }else if(isset($agentData['api']) && $agentData['api'] == 1 && $sub_link['page_name'] =='apikey' ){
                                    
                                }else{


                                ?>
                                    <li id="<?php echo $sub_link['page_name']; ?>" <?php echo $li_active; ?>>
                                        <a href="<?php echo $url; ?>"<?php echo $link_class; ?>><?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?><i class="fa fa-angle-left sidebar-nav-indicator"></i><?php } echo $sub_link['name']; ?></a>
                                        <?php if (isset($sub_link['sub']) && $sub_link['sub']) { ?>
                                            <ul>
                                                <?php foreach ($sub_link['sub'] as $sub2_link) {
                                                    // Get 3rd level link's vital info
                                                    $url    = (isset($sub2_link['url']) && $sub2_link['url']) ? $sub2_link['url'] : '#';
                                                    $active = (isset($sub2_link['url']) && ($template['active_page'] == $sub2_link['url'])) ? ' class="active"' : '';
                                                ?>
                                                <li>
                                                    <a href="<?php echo $url; ?>"<?php echo $active ?>><?php echo $sub2_link['name']; ?></a>
                                                </li>
                                                <?php } ?>
                                            </ul>
                                        <?php } ?>
                                    </li>
                                <?php }
                                } ?>
                            </ul>
						<?php }}

                        } ?><!-- End condition menu permission -->
                        </li>
						<?php } ?>
                        <?php } ?>
                        
                        
                    </ul>
                    <!-- END Sidebar Navigation -->
                    <?php } ?>

                </div>
                <!-- END Sidebar Content -->
            </div>
            <!-- END Wrapper for scrolling functionality -->
        </div>
        <!-- END Main Sidebar -->

        <!-- Main Container -->
        <div id="main-container">
            <!-- Header -->
            <!-- In the PHP version you can set the following options from inc/config file -->
            <!--
                Available header.navbar classes:

                'navbar-default'            for the default light header
                'navbar-inverse'            for an alternative dark header

                'navbar-fixed-top'          for a top fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
                    'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

                'navbar-fixed-bottom'       for a bottom fixed header (fixed sidebars with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
                    'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
            -->
            <header class="navbar<?php if ($template['header_navbar']) { echo ' ' . $template['header_navbar']; } ?><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><?php if ($template['header']) { echo ' '. $template['header']; } ?>
                <?php if ( $template['header_content'] == 'horizontal-menu' ) { // Horizontal Menu Header Content ?>
                
                <!-- END Horizontal Menu + Search -->
                <?php } else { // Default Header Content  ?>
                <!-- Left Header Navigation -->
                <ul class="nav navbar-nav-custom">
                    <!-- Main Sidebar Toggle Button -->
                    <li>
                        <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                            <i class="fa fa-bars fa-fw"></i>
                        </a>
                    </li>
                 
                </ul>
                <!-- END Left Header Navigation -->
				
				 <div class="top-header pull-center ">
						
					
             	<?php  	if ($user_info['login_type']!='AGENT'){ 	 ?>
             
				<form class="fom-horizontal" method="post" action="<?php echo base_url(); ?>Reseller_panel/serch_merchant" role="search">
						<div class="input-group">
							<input type="search" class="form-control search_head" placeholder="Search" name="search_data">
							<div class="input-group-btn">
								<button class="btn btn-search searchBtnHeight"  type="submit" id="searchBtn"><i class="gi gi-search icon-color"></i></button>
							</div>
						</div>
						</form>
						
			<?php } ?>	
						</div> 
				
                <!-- Right Header Navigation -->
                <ul class="nav navbar-nav-custom pull-right">
                    
                    <!-- User Dropdown -->
                    <?php
                        if($this->session->userdata('reseller_direct_logged_in'))
                        {
                            $session	= $this->session->userdata('reseller_direct_logged_in');
                    
                            $userName = $session['resellerfirstName'].' '.$session['lastName'];
                            $userEmail = $session['resellerEmail'];
                            $userID = $session['resellerID'];
                        } elseif ($this->session->userdata('agent_direct_logged_in')) {
                            $session	= $this->session->userdata('agent_direct_logged_in');
                            $userName = $session['agentName'];
                            $userEmail = $session['agentEmail'];
                            $userID = $session['ragentID'];
                        } else {
                            $userEmail = $userName = '';
                            $userID = 0;
                        }
                    ?>
                    <li class="user-name"> <strong><?= $userName; ?></strong></li>
                    <li class="dropdown">
					
					     <?php   
						     
								 $logo = base_url(IMAGES).'/placeholders/avatars/avatar1.jpg';   
						?>
                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo $logo; ?>" alt="avatar"> <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                            <li class="dropdown-header text-center">Account</li>
                           
                            <li class="divider"></li>
                             <li>
                                <a href="#modal-user-settings" data-toggle="modal">
                                    <i class="fa fa-cog fa-fw pull-right"></i>
                                    Change Password
                                </a> 
							  </li>
							 <li class="divider"></li> 
                            <li>
                              <?php 	if ($user_info['login_type']=='AGENT'){ ?>
								<a href="<?php echo base_url('logout/agent'); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit pull-right"></i>Logout</a>
								<?php }else{  ?>
							
									<a href="<?php echo base_url('logout'); ?>" data-toggle="tooltip" data-placement="bottom" title="Logout"><i class="gi gi-exit pull-right"></i>Logout</a>
							    <?php } ?>
                            </li>
                            
                            
                        </ul>
                    </li>
                    <!-- END User Dropdown -->
                </ul>
                <!-- END Right Header Navigation -->
                <?php } ?>
            </header>
            <!-- END Header -->
		
<style>
.navbar.navbar-default {
    background-color: #3498db !important;
}

.btn-info:focus, .btn-success.focus, .btn-success:active, .btn-success:active:hover, .btn-success:active:focus, .btn-success.active, .btn-success.active:hover, .btn-success.active:focus, .open .btn-success.dropdown-toggle, .open .btn-success.dropdown-toggle:hover, .open .btn-success.dropdown-toggle:focus, .open .btn-success.dropdown-toggle.focus {
    background-color:  #167bc4;
    border-color: #0991f2;
    color: #ffffff;
}


.btn-info:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #167bc4;
    border-color: #0991f2;
}

.btn-info:hover {
    background-color:#167bc4;
    border-color: #0991f2;
    color: #ffffff;
}


.btn-info {
     background-color: #1592eb; 
     border-color: #1592eb; 
    color: #ffffff;
}

.btn-danger {
    background-color: #E32E1B;
    border-color: #E32E1B; 
    color: #ffffff;
}




.btn-warning {
    background-color: #F2990C;
   border-color: #f39c12; 
    color: #ffffff;
}

.btn-success:focus, .btn-success.focus, .btn-success:active, .btn-success:active:hover, .btn-success:active:focus, .btn-success.active, .btn-success.active:hover, .btn-success.active:focus, .open .btn-success.dropdown-toggle, .open .btn-success.dropdown-toggle:hover, .open .btn-success.dropdown-toggle:focus, .open .btn-success.dropdown-toggle.focus {
    background-color:  #38A838;
    border-color: #38A838;
    color: #ffffff;
}


.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #38A838;
    border-color: #255625;
}

.btn-success:hover {
    background-color:#38A838;
    border-color: #578022;
    color: #ffffff;
}


.btn-success {
     background-color: #3FBF3F; 
     border-color: #3FBF3F; 
    color: #ffffff;
}

.block-options .btn {
    border-radius: 2px; 
    padding-right: 8px;
    padding-left: 8px;
    min-width: 30px;
    text-align: center;
}

.navbar-form-custom .form-control {
    padding: 10px;
    margin: 0;
    height: 70px;
    font-size: 15px;
    background: transparent;
    border: none;
    z-index: 2000;
}

.remove-hover {
         pointer-events: none;
		 
		}

.navbar-form-custom .form-control:hover, .navbar-form-custom .form-control:focus {
  
}
.navbar-form-custom {
    padding: 0;
    width: 200px;
    float: left;
    height: 70px;
	background-color: #ffffff !important; 
}


.nav.navbar-nav-custom > li.open > a, .nav.navbar-nav-custom > li > a:hover, .nav.navbar-nav-custom > li > a:focus, .navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:focus, .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus, .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus, .navbar-inverse .navbar-nav > li > a:hover, .navbar-inverse .navbar-nav > li > a:focus, .navbar-inverse .navbar-nav > .active > a, .navbar-inverse .navbar-nav > .active > a:hover, .navbar-inverse .navbar-nav > .active > a:focus, .navbar-inverse .navbar-nav > .open > a, .navbar-inverse .navbar-nav > .open > a:hover, .navbar-inverse .navbar-nav > .open > a:focus, a.sidebar-brand:hover, a.sidebar-brand:focus, a.sidebar-title:hover, a.sidebar-title:focus, #to-top:hover, .timeline-list .active .timeline-icon, .table-pricing.table-featured th, .table-pricing th.table-featured, .wizard-steps div.done span, .wizard-steps div.active span, .switch-primary input:checked + span, a.list-group-item.active, a.list-group-item.active:hover, a.list-group-item.active:focus, .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus, .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .nav .open > a, .nav .open > a:hover, .nav .open > a:focus, .pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus, .pager > li > a:hover, .pagination > li > a:hover, .label-primary, .chosen-container .chosen-results li.highlighted, .chosen-container-multi .chosen-choices li.search-choice, .datepicker table tr td.active, .datepicker table tr td.active:hover, .datepicker table tr td.active.disabled, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover:hover, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.active, .datepicker table tr td.active:hover.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled, .datepicker table tr td.active:hover.disabled, .datepicker table tr td.active.disabled.disabled, .datepicker table tr td.active.disabled:hover.disabled, .datepicker table tr td.active[disabled], .datepicker table tr td.active:hover[disabled], .datepicker table tr td.active.disabled[disabled], .datepicker table tr td.active.disabled:hover[disabled], .datepicker table tr td span.active:hover, .datepicker table tr td span.active:hover:hover, .datepicker table tr td span.active.disabled:hover, .datepicker table tr td span.active.disabled:hover:hover, .datepicker table tr td span.active:active, .datepicker table tr td span.active:hover:active, .datepicker table tr td span.active.disabled:active, .datepicker table tr td span.active.disabled:hover:active, .datepicker table tr td span.active.active, .datepicker table tr td span.active:hover.active, .datepicker table tr td span.active.disabled.active, .datepicker table tr td span.active.disabled:hover.active, .datepicker table tr td span.active.disabled, .datepicker table tr td span.active:hover.disabled, .datepicker table tr td span.active.disabled.disabled, .datepicker table tr td span.active.disabled:hover.disabled, .datepicker table tr td span.active[disabled], .datepicker table tr td span.active:hover[disabled], .datepicker table tr td span.active.disabled[disabled], .datepicker table tr td span.active.disabled:hover[disabled], .bootstrap-timepicker-widget table td a:hover, div.tagsinput span.tag, .slider-selection, .themed-background, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-results__option--highlighted[aria-selected], .nav-horizontal a:hover, .nav-horizontal li.active a {
    background-color: #167bc4;
}
.themed-background-pre{
	background-color: #f2f2f2;
}
.navbar.navbar-default {
    background-color: #333333  !important;
}

.nav.navbar-nav-custom > li > a {
    min-width: 45px !important;
    padding: 15px 7px;
    line-height: 40px;
    text-align: center;
    position: relative;
}
 .dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus, .dropdown-menu > .active > a, .dropdown-menu > .active > a:hover, .dropdown-menu > .active > a:focus, .nav .open > a, .nav .open > a:hover, .nav .open > a:focus{
	background-color: #888888 !important;
}
.nav.navbar-nav-custom > li > a:hover, .nav.navbar-nav-custom > li > a:focus{
    background-color: #f8f9fd !important;
}
.nav.navbar-nav-custom > li > a:hover{
    color: #333333 !important;
}
.sidebar-nav a.active, .sidebar-nav ul a.active, .sidebar-nav ul a.active:hover {
    border-left-color: #888888 !important;
}
.sidebar-nav a{
    padding: 9px !important;
}
.preloader .inner {
    margin: 0 0 0 -80px;
}
#chart-bars-new .flot-base{
    position: absolute;
    top: 0px;
    left: 25px!important;
}

</style>			
