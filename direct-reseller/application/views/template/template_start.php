<?php
/**
 * template_start.php
 *
 * Author: pixelcave
 *
 * The first block of code used in every page of the template
 *
 */
 
?>


<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    
    <head>
        <meta charset="UTF-8">
        <meta name="google" content="notranslate">
        <meta http-equiv="Content-Language" content="en">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        

        <title><?php echo $template['title'] ?></title>

        <meta name="description" content="<?php echo $template['description'] ?>">
        <meta name="author" content="<?php echo $template['author'] ?>">
        <meta name="robots" content="<?php echo $template['robots'] ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url(IMAGES); ?>/merch_icon.png">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon180.png" sizes="180x180">
        <!-- END Icons -->
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
		

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css?v=1.0.0">

        <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
        <?php if ($template['theme']) { ?><link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $template['theme']; ?>.css"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
        <!-- END Stylesheets -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/footer_style.css">
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/common/common.css">

        <!-- Modernizr (browser feature detection library) -->
		<script src="<?php echo base_url(JS); ?>/vendor/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/plugins.js"></script>
        <script src="<?php echo base_url(JS); ?>/app.js"></script>
        <script src="<?php echo base_url(JS); ?>/custom.js"></script>
        <script src="<?php echo base_url(JS); ?>/pages/formsWizard.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/modernizr-respond.min.js"> </script>
        
   
        <style>
		.remove-hover {
         pointer-events: none;
		 
		}
		</style>
        
       

 
    </head>
    
  
    <script type='text/javascript'>

window.__lo_site_id = 240209;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
</script>  
	
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-106466457-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments)};
  gtag('js', new Date());

  gtag('config', 'UA-106466457-2');
</script>

    <body>