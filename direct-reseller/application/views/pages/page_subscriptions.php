
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
        <div class="msg_data "><?php echo $this->session->flashdata('message'); ?> </div>
    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Subscriptions</h2>
            <div class="block-options pull-right">
							 <a class="btn  btn-sm btn-info" title="Change Gateway"  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#Edit_gateway" >Change Gateway in Bulk</a>
			
							 
                               <a class="btn  btn-sm btn-success" title="Create New"  href="<?php echo base_url(); ?>SettingSubscription/create_subscription">Add New </a>
                               
				
                        </div>
        </div>
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="sub_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   <th class="visible-lg text-left">Customer Name</th>
                    <th class="text-left visible-lg">Subscription</th>
					<th class="hidden-xs text-left">Merchant ID</th>
                    <th class="hidden-xs text-left">Gateway</th>
                    <th class="visible-lg text-right">Amount</th>
					<th class="hidden-xs text-right">Next Charge Date</th>
					 
					 
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				
				
				
				if(isset($subscriptions) && $subscriptions)
				{
					foreach($subscriptions as $subs)
					{ 
					
				?>
				<tr>
					
					<td class="text-left visible-lg"><a href="javascript:void(0);"><?php echo $subs['companyName']; ?> </a> </td>
					
                    <td class="text-left visible-lg"><?php echo $subs['subscriptionName']; ?></td>
					
				    <td class="text-left visible-lg"> <?php echo $subs['gatewayMerchantID']; ?> </td>
					
                    <td class="text-left hidden-xs"><?php echo $subs['gatewayFriendlyName']; ?>  </td> 
					
					
				    
					<td class="text-right hidden-xs">$<?php echo ($subs['subscriptionAmount'])?$subs['subscriptionAmount']:'0.00'; ?></td>
				   
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($subs['nextGeneratingDate'])); ?></td>
					
				
					
				
					
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="<?php echo base_url('SettingSubscription/create_subscription/'.$subs['subscriptionID']); ?>" data-toggle="tooltip" title="Edit Subscriptions" class="btn btn-default"><i class="fa fa-edit"></i></a>
                            <a href="#del_subs" onclick="set_subs_id('<?php  echo $subs['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Subscriptions" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                           
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->

<div id="Edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway in Bulk</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="gatw" method="post" action='<?php echo base_url(); ?>SettingSubscription/update_gateway' class="form-horizontal" >
				 
                     
                 <p> You may update gateways in bulk here.</p> 
				 
				   
				   
					   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Current Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_old" name="gateway_old"  class="form-control">
								   <option value="" >Select Current Gateway</option>
								    <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						
						</div>
						
				
					
						<div class="form-group ">
                        	<label class="col-md-4 control-label" for="card_list">Change to Gateway</label>
						 <div class="col-md-6">
						   <select id="gateway_new" name="gateway_new"  class="form-control">
								   <option  value="" >Select New Gateway</option>
								   <?php foreach($gateways as $gateway){  ?>
								    <option value="<?php echo $gateway['gatewayID']; ?>" ><?php echo $gateway['gatewayFriendlyName']; ?></option>
								   <?php } ?>
								  
							</select>
							</div>
						</div>
						
				<div class="form-group ">
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel1" name="btn_cancel1" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button"  id="btn_cancel" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>
					</div>
					
					
					
					<table>
					    <hr/>
				<tbody id="t_data">
						 
						 </tbody>
				 </table>
					
					
                    <br />
                    <br />
            
			    </form>	
				
						<div class="pull-right" id="btn" style="display:none;">
					
        			 <input type="submit" id="btn" name="btn_cancel" class="btn btn-sm btn-success" value="Change"  />
                    <button type="button" id="btn" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>
                    </div>	

                      				
            </div>
			
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){  Pagination_view.init();
 });</script>
<script>



	
 
 $('#gateway_old').change(function(){
	 
	
	 
    var gID =  $('#gateway_old').val();

	$.ajax({
    url: '<?php echo base_url("SettingSubscription/get_gateway_id")?>',
    type: 'POST',
	data:{gateway_id:gID},
    success: function(data){
		
		$('#t_data').html(data);
		
		
		
		var newarray = JSON.parse("[" + data.subscriptionID + "]");
			
			 for(var val in  newarray) {
			 $("input[type=checkbox]").each(function() {
				
			if($(this).val()==newarray[val]) {

				   var rowid = $(this).attr('id');
				   document.getElementById(rowid).checked = true;
				}
			});
			 
			 }
		
			
     }	
  });
	
});	
	

  

  
 
var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#sub_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>
<script>
	  
		

	  
var nmiValidation1 = function() {   

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#gatw').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                     
					 gateway_old: {
							 required:true,
						},
					gateway_new:{
							 required:true,
					},
					
					
                },
               
            });
					
		
		
        }
    };
}();


</script>


</div>
<!-- END Page Content -->