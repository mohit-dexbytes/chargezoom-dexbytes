 <!-- Page content -->
 <?php
	$this->load->view('alert');
?>
	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		<h1>
		<i class="gi gi-settings"> </i> <?php  echo "General Settings";?> 
		</h1>
	</div>
	</div>
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

	
	
	
	
    <!-- Progress Bar Wizard Block -->
    <div class="block">
	              
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="form_new" class="form form-horizontal"  enctype="multipart/form-data" action="<?php echo base_url(); ?>SettingConfig/profile_setting">
		         <input type="hidden" id ="merchID" name="merchID" value="<?php if(isset($invoice)){echo $invoice['merchID']; } ?>"  />
                 
				 
				
                  
                    <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">First Name</label>
							<div class="col-md-8">
								<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['firstName']; } ?>" placeholder="Your First name..">
							</div>
						</div>
                       </div> 
					   
					   <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Last Name</label>
							<div class="col-md-8">
								<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['lastName']; } ?>" placeholder="Your Last name..">
							</div>
						</div>
                       </div>
					   
					    
					   
					    <div class="col-sm-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Tagline</label>
							<div class="col-md-8">
								<input type="text" id="tagline" name="tagline"  value="<?php if(isset($invoice)){echo $invoice['merchantTagline']; } ?>" class="form-control" placeholder="Enter tagline for your customer portal..">
							</div>
						</div>
						</div>
					   <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Email</label>
							<div class="col-md-8">
								<input type="text" readonly="readonly" id="merchantEmail" name="merchantEmail" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantEmail']; } ?>" placeholder="Your Email ..">
							</div>
						</div>
                       </div>
					  
						<div class="col-md-6"> 
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['companyName']; } ?>" placeholder="Your Company Name ..">
							</div>
						</div>
                       </div>
					   
					    
					   
					   <div class="col-md-6">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress1']; } ?>" placeholder="Your Address ..">
							</div>
						</div>
					   </div>
					   
					    <div class="col-md-6"> 
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Contact</label>
							<div class="col-md-8">
								<input type="text" id="merchantContact" name="merchantContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantContact']; } ?>" placeholder="Your Contact ..">
							</div>
						</div>
						
                       </div>
					   
                        <div class="col-md-6"> 
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAddress2']; } ?>" placeholder="Your Address ..">
							</div>
						</div>
                       </div>
					   
					
							
					   
					   
							
                       
					   
						
						
						<div class="col-md-6"> 
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="country" for="example-typeahead">Select Country</label>
						   <div class="col-md-8">
								<select id="country" class="form-control " name="country" >
								<option value="Select Country">Select Country ..</option>
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_id']; ?>" <?php if(isset($invoice)){ if($invoice['merchantCountry']==$country['country_id'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
					
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">Select State</label>
						   <div class="col-md-8">
								<select id="state" class="form-control input-typeahead" name="state">
								<option value="Select State">Select State ..</option>
								   <?php foreach($state_datas as $state){  ?>
								   <option value="<?php echo $state['state_id']; ?>"<?php if(isset($invoice)){if($invoice['merchantState']==$state['state_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $state['state_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">Select City</label>
						   <div class="col-md-8">
								<select id="city" class="form-control input-typeahead" name="city">
								<option value="Select City">Select City ..</option>
								   <?php foreach($city_datas as $city){  ?>
								   <option value="<?php echo $city['city_id']; ?>" <?php if(isset($invoice)){if($invoice['merchantCity']==$city['city_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $city['city_name']; ?> </option>
								   <?php } ?> 
								</select>
							</div>
                        </div>
                        </div>
                      <div class="col-md-6"> 
    					   <div class="form-group">
    							<label class="col-md-4 control-label" for="example-username">Alternate Contact</label>
    							<div class="col-md-8">
    								<input type="text"  id="merchantAlternateContact" name="merchantAlternateContact" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantAlternateContact']; } ?>" placeholder="Your Alternate Contact ..">
    							</div>
    						</div>
                       </div>
					   
					      
							
					<div class="col-md-6"> 
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Zip Code</label>
							<div class="col-md-8">
								<input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control"  value="<?php if(isset($invoice)){echo $invoice['merchantZipCode']; } ?>" placeholder="Your Zip Code ..">
							</div>
						</div>
                       </div>
							
					   
					  
					   <div class="col-sm-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Full Address</label>
							<div class="col-md-8">
								<input type="text" id="merchantFullAddress" name="merchantFullAddress"  value="<?php if(isset($invoice)){echo $invoice['merchantFullAddress']; } ?>" class="form-control" placeholder="Enter tagline for your customer portal..">
							</div>
						</div>
						</div>					  
						
						<div class="col-md-6"> 
					   <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Prefix </label>
							<div class="col-md-8">
							<input type="text" id="prefix" name="prefix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['prefix']; } ?>" placeholder=""></div></div>
							
							<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Invoice Start Number </label>
							<div class="col-md-8">
							<input type="text" id="postfix" name="postfix" class="form-control" 
							value="<?php if(isset($prefix) && !empty($prefix)){ echo $prefix['postfix']; } ?>" placeholder=""></div>
							</div>
							</div>
							
							
							
							
					<div class="col-sm-6">
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-file-input">Upload Logo</label>
							<div class="col-md-8">
								<input type="file" id="upload_logo" name="picture" class="upload" ><img src="<?php if(isset($invoice)){echo $invoice['merchantProfileURL']; } ?>" height="80px" width="80px">
							</div>
						</div>
					</div>
		
				  	
	              <div class="form-group">
					<div class="col-md-8 col-md-offset-11">
						
						<button type="submit" class="submit btn btn-sm btn-success" >Update</button>	
					</div>
				</div>					
					
		
		
  	</form>
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

   
    $('#form_new').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'merchantAddress1': {
                        required: true,
                        minlength: 5
                    },
					
					'city':{
					      required:true,
						  
						  
					},
					'state':{
					      required:true,
						  
						  
					},
					'country':{
					      required:true,
					},
					'firstName':{
						    required:true,
							minlength: 2
					},
					'lastName':{
						    required:true,
							minlength: 2
					},
					
					  'merchantContact': {
                        required: true,
						 minlength: 10,
						 number : true,
						 maxlength: 12,
                       
                    },
                    'merchantZipCode': {
                        required: true,
                         minlength: 3
                    },
					'prefix':{
					    required: true,
					   minlength: 3,
					   },
					'postfix':{
					    required: true,
					    digits:true,
                    }
			
			},
    });

 
 
   $('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('SettingConfig/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('SettingConfig/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});   


});	
   
    
		
	

</script>

</div>