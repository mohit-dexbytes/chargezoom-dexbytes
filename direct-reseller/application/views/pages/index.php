
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
   

    <!-- Quick Stats -->
    <div class="row text-center">
	
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-remove-hover">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Processed</strong> Today</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-success"><strong><?php echo $today_invoice; ?></strong></span></div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-remove-hover">
                <div class="widget-extra themed-background-info">
                    <h4 class="widget-content-light"><strong>Scheduled</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-info"><strong><?php echo $upcoming_inv; ?></strong></span></div>
            </a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-remove-hover">
                <div class="widget-extra themed-background-danger">
                    <h4 class="widget-content-light"><strong>Failed</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-danger"><strong><?php echo $failed_inv; ?></strong></span></div>
            </a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-remove-hover">
                <div class="widget-extra themed-background">
                    <h4 class="widget-content-light"><strong>MTD</strong> Collections</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-default"><strong>$ <?php echo $recent_pay; ?></strong></span></div>
            </a>
        </div>
    </div>
    <!-- END Quick Stats -->

    <!-- eShop Overview Block -->
    <div class="block full">
        <!-- eShop Overview Title -->
        <div class="block-title">
            
            <h2><strong>General</strong> Overview</h2>
        </div>
        <!-- END eShop Overview Title -->

        <!-- eShop Overview Content -->
        <div class="row">
            <div id="chart-classic" style="height: 350px;" class="chart"></div>
        </div>
		<hr>
	    <div class="row">
            <div class="col-sm-6">
			 <div class="block">
			  <div class="block-title">
                    
                    <h2><strong>Top 10 Due By Company</strong></h2>
                </div>
			  <div id="chart_pie_customer" class="chart"> 
              
                  
                </div>
			   </div>	
            </div>
            <div class="col-sm-6">
			 <div class="block">
			  <div class="block-title">
                    
                    <h2><strong>Top 10 Past Due By Company</strong></h2>
                </div>
			
			  <div id="chart-pie" class="chart">
			
               
                 
				</div> 
            </div>
        </div>
		
	</div>
	
		
	
	
    <!-- END eShop Overview Block -->

    <!-- Orders and Products -->
	<br>
   <hr>
    <div class="row">
        <div class="col-lg-6">
            <!-- Latest Orders Block -->
            <div class="block">
                <!-- Latest Orders Title -->
                <div class="block-title">
                    
                    <h2><strong>Recent</strong> Payments</h2>
                </div>
                <!-- END Latest Orders Title -->
			    <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                           
                            <td class="hidden-xs text-left"><strong>Customer Name</strong></td>
							<td class="text-right"><strong>Invoice</strong></td>
                            <td class="text-right"><strong>Payment Date</strong></td>
                            <td class="text-right"><strong>Amount</strong></td>
							
                           
                        </tr>
						
						<?php   if(!empty($recent_paid)){   
 
						foreach($recent_paid as $invoice){

					 ?>
                        <tr>
						 
						 <td class="hidden-xs text-left"><?php  echo  $invoice['companyName']; ?></td>
                            <td class="text-right"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><?php  echo $invoice['RefNumber']; ?></a></td>
                            <td class="text-right"><?php  echo  date('F,d Y', strtotime($invoice['TimeModified'])); ?></td>
                            <td class="text-right">$<?php  echo  $invoice['balance']; ?></td>
							 
                              
                        </tr>
						   <?php } }else { ?>	
						
						<tr>
                            <td colspan="5"><strong>No Records Found!</strong></td>
                         
                        </tr>
						
						   <?php } ?>
                       
                        
                    </tbody>
                </table>
              
                <!-- END Latest Orders Content -->
            </div>
            <!-- END Latest Orders Block -->
        </div>
        <div class="col-lg-6">
            <!-- Top Products Block -->
            <div class="block">
                <!-- Top Products Title -->
                <div class="block-title">
                  
                    <h2><strong>Oldest</strong> Invoices</h2>
                </div>
                <!-- END Top Products Title -->

                <!-- Top Products Content -->
               <table class="table table-bordered table-striped table-vcenter">
                    <tbody>
                        <tr>
                            <td class="text-left" ><strong>Customer Name</strong></td>
                            <td class="text-right" ><strong>Invoice</strong></td>
							
                            <td class="hidden-xs text-right"><strong>Due Date</strong></td>
                           
							  <td class="text-right"><strong>Amount</strong></td>

                        </tr>
						
						<?php   if(!empty($oldest_invs)){   

                      
						foreach($oldest_invs as $invoice){
						  
						      if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
							
						 ?>
                        <tr>
                            <td class="hidden-xs text-left"><?php  echo $invoice['companyName']; ?></td>
                            <td class="text-right"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><?php  echo $invoice['RefNumber']; ?></a></td>

							
							<td class="hidden-xs text-right"><?php  echo  date('F,d Y', strtotime($invoice['DueDate'])); ?></td>
						   <td class="text-right">$<?php  echo  $invoice['BalanceRemaining']; ?></td>
                            
                        </tr>
						   <?php  } }else { ?>	
						<tr>
                            <td colspan="5"><strong>No Records Found!</strong></td>
                        </tr>
						
						   <?php } ?>
                       
                        
                    </tbody>
                </table>
                <!-- END Top Products Content -->
            </div>
            <!-- END Top Products Block -->
        </div>
    </div>
    <!-- END Orders and Products -->

	
	<!-- Load and execute javascript code used only in this page -->

    
<script>
var dataEarnings  = [];
  var dataSales   = [];
   var chartMonths = [];
   var chartColor  = [];
     
  var due_data ='<?php  echo $due_data; ?>';
   var data_30 ='<?php  echo $data_30; ?>';
   var data_60 ='<?php  echo $data_60; ?>';
   var data_80 ='<?php  echo $data_80; ?>';
   var data_90 ='<?php  echo $data_90; ?>';
   
 
</script>	           

	<?php 
	
	             $earn     			  = $chart_data['earn_amount'];
				 $month_total_invoice =  $chart_data['month_total_invoice'];
				 $inv_month      	  = $chart_data['inv_month'];
	

	     foreach($earn as $k=>$val ){  
	?> 
		<script>
	           
		var arr1  = [];
		var arr1 = ['<?php echo $k+1;  ?>','<?php echo $val  ?>'];
		dataEarnings.push(arr1);
		
		
	
	</script>
	<?php } 
	
	  foreach($month_total_invoice as $j=>$monthdata ){  
	?> 
		<script>
	           
		
		var arr2  = [];
		var arr2 = ['<?php echo $j+1;  ?>','<?php echo $monthdata ; ?>'];
		dataSales.push(arr2);
	
	
	</script>
	<?php }  
		foreach( $inv_month as $l => $incount ){  
	
	?> 
		<script>
		
	    var arr3  = [];
		var arr3 = ['<?php echo $l+1;  ?>','<?php echo $incount  ?>'];
		chartMonths.push(arr3);
	
	
	</script>
	<?php } ?>
	


<script>



var jsdata = '<?php echo json_encode($company_due); ?>';

var jsddd  = $.parseJSON(jsdata);   
var pchart_color = []; 
   for(var val in  jsddd) {
     pchart_color.push({label:jsddd[val]['label'], data:jsddd[val]['balance']});
     if(val==0){
		
		chartColor.push('#3498db');
	 }else if(val==1){
		 chartColor.push('#27ae60');
	 }else if(val==2){
	chartColor.push('#ffff00');	 	
			 
	 }else if(val==3){
		 chartColor.push('#e67e22');
	 }else if(val==4){
		chartColor.push('#e74c3c');	 
	 }else if(val==5){
		 chartColor.push('#34dbcb');
	 }else if(val==6){
		chartColor.push('#db7734');	 
	 }else if(val==7){
		chartColor.push('#3abc'); 
	 }else if(val==8){
		chartColor.push('#ff1a75 ');	 
	 }else if(val==9){
		chartColor.push('#8c1aff');
	 }	 
     
   }


var jsdata1 = '<?php echo json_encode($customer_due); ?>';

var jscustomer  = $.parseJSON(jsdata1);   
var pchart_color_customer = []; 
var pdchartColor=[];
   for(var val in  jscustomer) {
     pchart_color_customer.push({label:jscustomer[val]['label'], data:jscustomer[val]['balance']});
	     if(val==0){
		
		pdchartColor.push('#3498db');
	 }else if(val==1){
		 pdchartColor.push('#27ae60');
	 }else if(val==2){
	pdchartColor.push('#ffff00');	 	
			 
	 }else if(val==3){
		 pdchartColor.push('#e67e22');
	 }else if(val==4){
		pdchartColor.push('#e74c3c');	 
	 }else if(val==5){
		 pdchartColor.push('#34dbcb');
	 }else if(val==6){
		pdchartColor.push('#db7734');	 
	 }else if(val==7){
		pdchartColor.push('#3abc'); 
	 }else if(val==8){
		pdchartColor.push('#ff1a75 ');	 
	 }else if(val==9){
		pdchartColor.push('#8c1aff');
	 }
     
   }
 
    
/*
 *  Document   : compCharts.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Charts page
 */
$(function(){  CompChartsnew.init();    Pagination_view.init(); });

var ij=0;
var CompChartsnew = function() {

    return {
        init: function() {
         
           
            var chartClassic = $('#chart-classic');
                var chartPie = $('#chart-pie');
				var chart_customer = $('#chart_pie_customer');

            // Classic Chart
        $.plot(chartClassic,
                [
                    {
                        label: 'Earnings',
                        data: dataEarnings,
                        lines: {show: true, fill: true, fillColor: {colors: [{opacity: 0.25}, {opacity: 0.25}]}},
                        points: {show: true, radius: 6}
                    },
                  
                ],
                {
                    colors: ['#3498db', '#333333'],
                    legend: {show: true, position: 'nw', margin: [15, 10]},
                    grid: {borderWidth: 0, hoverable: true, clickable: true},
                    yaxis: {ticks: 4, tickColor: '#eeeeee'},
                    xaxis: {ticks: chartMonths, tickColor: '#ffffff'}
                }
            );  

            // Creating and attaching a tooltip to the classic chart
			
          var previousPoint = null, ttlabel = null;
            chartClassic.bind('plothover', function(event, pos, item) {
            
                if (item) {
                    if (previousPoint !== item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $('#chart-tooltip').remove();
                        var x = item.datapoint[0], y = item.datapoint[1];

                        if (item.seriesIndex === 1) {
                            ttlabel = '<strong>' + y + '</strong> invoices';
                        } else {
                            ttlabel = '$ <strong>' + y + '</strong>';
                        }

                        $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                            .css({top: item.pageY - 45, left: item.pageX + 5}).appendTo("body").show();
                    }
                }
                else {
                    $('#chart-tooltip').remove();
                    previousPoint = null;
                }
            });  
			
	 $.plot(chartPie,  pchart_color,
                    {
                    colors: chartColor,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) { 
                                    return '<div class="chart-pie-label">' + label + '<br>$' + Math.round(pieSeries.data[0][1]) + '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );

		 $.plot(chart_customer,  pchart_color_customer,
                    {
                    colors: pdchartColor,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) {
								
                                    return '<div class="chart-pie-label">' + label + '<br>$' + Math.round(pieSeries.data[0][1]) + '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );
		
			
			
			
			
			
        }
    };
}();




var Pagination_view = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
            
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.ecom-orders').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 1, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>
<!-- END Page Content -->
</div>	
