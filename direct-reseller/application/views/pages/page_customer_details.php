


<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
	<!-- Customer Content -->
    <div class="row">
        <div class="col-lg-4">
            <!-- Customer Info Block -->
			
			
            <div class="block">
                <!-- Customer Info Title -->
                <div class="block-title">
                    <h2><i class="fa fa-file-o"></i> <strong>Customer</strong> Details</h2>
                </div>
                <!-- END Customer Info Title --> 

                <!-- Customer Info -->
                <div class="block-section text-center">
                    <a href="javascript:void(0)">
                        <img src="<?php echo base_url(IMAGES); ?>/placeholders/avatars/avatar4@2x.jpg" alt="avatar" class="img-circle">
                    </a>
                    <h3>
                        <strong><?php echo $customer->FullName; ?></strong>
                     
                    </h3>
                </div>
                <table class="table table-borderless table-striped table-vcenter">
                    <tbody>
                        
                        <tr>
                            <td class="text-right"><strong>Primary Contact</strong></td>
                            <td><?php echo $customer->FirstName.' '.$customer->LastName; ?></td>
                        </tr>

						<tr>
                            <td class="text-right"><strong>Added On</strong></td>
                            <td><?php echo date('M d, Y - h:m', strtotime($customer->TimeCreated)); ?></td>
                        </tr>
                        <tr>
                            <td class="text-right"><strong>Last Visit</strong></td>
                            <td><?php echo date('M d, Y - h:m', strtotime($customer->TimeModified)); ?></td>
                        </tr>
                       <?php if($this->session->userdata('logged_in')|| in_array('Send Email',$this->session->userdata('user_logged_in')['authName'])  ){ ?> 
                        <tr>
                            <td class="text-right"><strong>Main Email</strong></td>
                            
                            <td><a href="#set_tempemail" onclick="set_template_data('<?php echo $customer->ListID?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->Contact; ?>')" title="Sending mail" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo $customer->Contact; ?></a></td>
                        </tr>
                        	 <?php } ?>
						<tr>
                            <td class="text-right"><strong>Main Phone</strong></td>
                            <td><?php echo $customer->Phone; ?></td>
                        </tr>
						<tr>
                             <td class="text-right"><strong>Credit Card Info</strong></td>
							 <?php if($this->session->userdata('logged_in')|| in_array('Create Card',$this->session->userdata('user_logged_in')['authName'])  ){ ?>
                            <td> <a href="#card_data_process" class="btn btn-sm btn-success"  onclick="set_card_user_data('<?php  echo $customer->ListID; ?>', '<?php  echo $customer->FullName; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Add New</a>
							 <?php }if($this->session->userdata('logged_in')|| in_array('Modify Card',$this->session->userdata('user_logged_in')['authName']) ){   ?>
							 <a href="#card_edit_data_process" class="btn btn-sm btn-info"   data-backdrop="static" data-keyboard="false" data-toggle="modal">View/Update</a>
							 <?php } ?>
							</td>
                        </tr>

						<tr>
                            <td class="text-right"><strong>Status</strong></td>
                            <td><i class="fa fa-check"></i> Active</td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Customer Info -->
   			</div>
			
			 <?php if($this->session->userdata('logged_in') || in_array('Send Email',$this->session->userdata('user_logged_in')['authName']) ) { ?>
			 
			 
		    
			<!-----------------   to view email history  ------------------------->
			<div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
                   <a href="#set_tempemail" class="btn btn-sm btn-success" onclick="set_template_data('<?php echo $customer->ListID?>','<?php echo $customer->companyName; ?>', '<?php echo $customer->companyID; ?>','<?php echo $customer->Contact; ?>')" title="Sending mail" data-backdrop="static" data-keyboard="false" data-toggle="modal">Send Mail</a>
                   </div>
                    <h2><strong>Email</strong> History</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table id="email_hist" class="table table-bordered table-striped ecom-orders table-vcenter">
				 
				  <thead>
                        <th class="text-right"><strong>Sent On</strong> </th>
                        <th class="hidden-xs text-left"><strong>Email Subject</strong></th>
                            
                </thead>
                    <tbody>
                      
						<?php   if(!empty($editdatas)){   

                        
						foreach($editdatas as $editdata){ 
						  
						 ?>
                        <tr>
                            <td class="text-right"> <?php echo date('M d, Y - h:m', strtotime($editdata['emailsendAt'])); ?> </td>
							
                           <td> <a href="#view_history" title="View Details" onclick="set_view_history('<?php echo $editdata['mailID'];?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <?php echo $editdata['emailSubject'];?> </a> </td>
						   
						   
                        </tr>
						   <?php } } ?>	
						
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->
			
			 <?php } ?>
			 
            <!-- END Customer Info Block -->

			
            <!-- Quick Stats Block -->
            <div class="block">
                <!-- Quick Stats Title -->
                <div class="block-title">
                    <h2><i class="fa fa-line-chart"></i> <strong>Quick</strong> Stats</h2>
                </div>
                <!-- END Quick Stats Title -->

                <!-- Quick Stats Content -->
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background">
                            <i class="gi gi-list"></i>
                        </div>
                        <h4 class="text-left">
                            <strong><?php echo $invoices_count; ?></strong><br><small>Invoices in Total</small>
                        </h4>
                    </div>
                </a>
                <a href="javascript:void(0)" class="widget widget-hover-effect2 themed-background-muted-light">
                    <div class="widget-simple">
                        <div class="widget-icon pull-right themed-background-success">
                            <i class="fa fa-usd"></i>
                        </div>
                        <h4 class="text-left text-success">
                            <strong>$ <?php echo $sum_invoice; ?></strong><br><small>Invoice Value</small>
                        </h4>
                    </div>
                </a>
                <!-- END Quick Stats Content -->
            </div>
            <!-- END Quick Stats Block -->
            
            
			
        </div>
        
        
        
        <div class="col-lg-8">
            <!-- Orders Block -->
            <div class="block">
                <!-- Orders Title -->
                <div class="block-title">
				
				
				
				
				
                    <div class="block-options pull-right">
                        <span class="btn btn-sm btn-alt1 btn-info remove-hover"><strong>$ <?php echo $pay_upcoming; ?></strong></span>
                        <span class="btn btn-alt1 btn-sm btn-danger remove-hover"><strong>$ <?php echo $pay_remaining; ?></strong></span>
					</div>
                    <h2><strong>Outstanding</strong> Invoices</h2>
                </div>
                <!-- END Orders Title -->

                <!-- Orders Content -->
                <table id="compamount" class="table  table-bordered table-striped table-vcenter">
                    
                        <thead>
                            <th class="text-center" >Invoice</th>
                            <th class="hidden-xs text-right">Due Date</th>
                          
							  <th class="text-right">Amount</th>
							   <th class="text-right">Status</th>
                            <th class="text-center">Action</th>
                        </thead>
						<tbody>
						<?php   if(!empty($invoices)){     foreach($invoices as $invoice){
						
						  
						 ?>
                        <tr>
                            <td class="text-center"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                            <td class="hidden-xs text-right"><?php  echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                          
							 <td class="text-right"><strong><?php if($invoice['IsPaid'] =="false"  ){ echo  "$".$invoice['BalanceRemaining']; }else{ echo "$".number_format($invoice['AppliedAmount'],2); } ?></strong></td>
							<td class="hidden-xs text-right"><?php  echo $invoice['status']; ?></td>
                           
							 
							 
							 
                            <td class="text-center">						
								
								 <?php 	 if($invoice['status']=='Upcoming'|| $invoice['status']=='Past Due'){ 
								     if($this->session->userdata('user_logged_in')&& in_array('Process',$this->session->userdata('user_logged_in')['authName']) ){
								 ?>
								 	 <a href="#invoice_process" class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>', '<?php  echo $customer->ListID; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
								     <a href="#invoice_schedule" class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
									 <a href="#invoice_cancel" class="btn btn-sm btn-danger"  onclick="set_invoice_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a>
									  <?php }if($this->session->userdata('logged_in')){ ?>
									   <a href="#invoice_process" class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>', '<?php  echo $customer->ListID; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
								     <a href="#invoice_schedule" class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
									 <a href="#invoice_cancel" class="btn btn-sm btn-danger"  onclick="set_invoice_id('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a>
									
									  <?php } }else  if($invoice['status']=='Paid') {  ?>
									 
									 <a href="javascript:void(0)"  class="btn btn-sm btn-alt1 btn-success  remove-hover">Paid</a>
								<?php	 }else{ ?><a href="javascript:void(0)" disabled="disabled" class="btn btn-sm btn-primary  remove-hover">Canceled</a>
									  
									 <?php } ?>
							</td>
                        </tr>
						   <?php  } } ?>	
						

                        
                    </tbody>
                </table>
                <br>
                <!-- END Orders Content -->
            </div>
            <!-- END Orders Block -->

            <!-- Products in Cart Block -->
            <div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
						
                        <span class="btn btn-alt1 btn-sm btn-success remove-hover"><strong>$ <?php echo $pay_invoice; ?></strong></span>
                        <span class="btn btn-alt1 btn-sm btn-danger remove-hover"><strong>$ <?php   echo $pay_due_amount; ?></strong></span>
					
                    </div>
                    <h2> <strong>Paid</strong> Invoices</h2>
                </div>
                <!-- END Products in Cart Title -->

                <!-- Products in Cart Content -->
                 <table  class="table compamount table-bordered table-striped ecom-orders table-vcenter">
				 
				  <thead>
                 
                        <th class="text-center"><strong>Invoice</strong></th>
                        <th class="hidden-xs text-right">Paid Date</th>
                        <th class="text-right hidden-xs">Invoice Amount</th>
                            <th class="text-right hidden-xs">Payments</th>
							  <th class="text-right">Balance</th>
                            <th class="text-right">Status</th>
                      
                </thead>
                    <tbody>
                      
						
						<?php   if(!empty($latest_invoice)){   

						foreach($latest_invoice as $invoice){
						  $full='';
						  
						  
						    if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }   
						else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   } else
						   
						   if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
						   
						   $full =$invoice['AppliedAmount']+$invoice['BalanceRemaining'];
						   
						   if($invoice['AppliedAmount']!="0.00"){
						       $status = "Partial";
						   }
						   
						    if($invoice['BalanceRemaining'] =="0.00"){
						       $status = "Paid";
						   }
						   
						   
						 ?>
                        <tr>
                            <td class="text-center"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
                           <td class="hidden-xs text-right"><?php  echo  $invoice['TimeModifiedinv']; ?></td>
                           <td class="hidden-xs text-right">$<?php echo ($full)?$full:'0.00'; ?></td>
                           
						   <?php if($invoice['AppliedAmount']!="0.00"){ ?>
                            <td class="hidden-xs text-right"><a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">$<?php  echo $invoice['AppliedAmount']; ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right"><strong>$<?php  echo  $invoice['AppliedAmount']; ?></strong></td>   
						   <?php } ?>
							 <td class="text-right"><strong>$<?php  echo $invoice['BalanceRemaining']; ?></strong></td>
							 
                              <td class="text-right"><?php  echo  $status; ?></td>
                        </tr>
						   <?php } }?>	
						
					
                        
                    </tbody>
                </table>
                <br>
                <!-- END Products in Cart Content -->
            </div>
            <!-- END Products in Cart Block -->


			
			

			<div class="block">
                <!-- Products in Cart Title -->
                <div class="block-title">
                    <div class="block-options pull-right">
						
                        <a href="<?php echo base_url(); ?>SettingSubscription/create_subscription" class="btn pull-lft  btn-sm btn-success"> Add New </a>
					
                    </div>
                    <h2> <strong>Subscriptions</strong> </h2>
                </div>
				
				
        <!-- All Orders Content -->
        <table id="sub_page" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Name</th>
					<th class="text-left">Merchant ID</th>
                    <th class="text-left">Gateway</th>
                    <th class="text-right">Amount</th>
					<th class="text-right">Next Charge Date</th>
					<th class="text-center">Action</th>
                </tr>
            </thead>
			 <tbody>
			 <?php   if(!empty($getsubscriptions)){   

                        
						foreach($getsubscriptions as $getsubscription){ 
						  
						 ?>
			
                   <tr>
					
                    <td class="text-left visible-lg"><?php echo $getsubscription['subscriptionName']; ?></td>
					
				    <td class="text-left visible-lg"> <?php echo $getsubscription['gatewayMerchantID']; ?> </td>
					
                    <td class="text-left hidden-xs"><?php echo $getsubscription['gatewayFriendlyName']; ?>  </td> 
					
					
				    
					<td class="text-right hidden-xs">$<?php echo ($getsubscription['subscriptionAmount'])?$getsubscription['subscriptionAmount']:'0.00'; ?></td>
				   
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($getsubscription['nextGeneratingDate'])); ?></td>
					
				
					<td class="text-center">
						<div class="btn-group btn-group-sm">
							<a href="<?php echo base_url('SettingSubscription/create_subscription/'.$getsubscription['subscriptionID']); ?>" data-toggle="tooltip" title="Edit Subscriptions" class="btn btn-default"> <i class="fa fa-edit"></i></a>
							
                            <a href="#del_subs" onclick="set_subs_id('<?php  echo $getsubscription['subscriptionID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete Subscriptions" class="btn btn-danger"><i class="fa fa-times"></i></a>
                           
						</div>
					</td>
				</tr>
				
				<?php  } } ?>
				
			</tbody>
			
				</table>
				<br>
				
			</div>
<!-----------------  END --------------------->
			
            <!-- Customer Addresses Block -->
            <div class="block">
                <!-- Customer Addresses Title -->
                <div class="block-title">
                    <h2><strong>Addresses</strong></h2>
                </div>
                <!-- END Customer Addresses Title -->

                <!-- Customer Addresses Content -->
                <div class="row">
                    
                    <div class="col-lg-6">
                        <!-- Shipping Address Block -->
                        <div class="block">
                            <!-- Shipping Address Title -->
                            <div class="block-title">
                                <h2>Billing Address</h2>
                            </div>
                            <!-- END Shipping Address Title -->

                            <!-- Shipping Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                          <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                 <?php echo ($customer->ShipAddress_Addr2)?$customer->ShipAddress_Addr2.', ':''; ?> <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo ($customer->ShipAddress_State)?$customer->ShipAddress_State.', ':''; ?> <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->Phone; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><?php echo $customer->Contact; ?></a>
                            </address>
                            <!-- END Shipping Address Content -->
                        </div>
                        <!-- END Shipping Address Block -->
                    </div>
                    
                    <div class="col-lg-6">
                        <!-- Billing Address Block -->
                        <div class="block">
                            <!-- Billing Address Title -->
                            <div class="block-title">
                                <h2>Shipping Address</h2>
                            </div>
                            <!-- END Billing Address Title -->

                            <!-- Billing Address Content -->
                            <h4><strong><?php echo $customer->FullName; ?></strong></h4>
                            <address>
								<strong><?php echo $customer->companyName; ?></strong><br>
                                <?php echo $customer->ShipAddress_Addr1; ?><br>
                                <?php echo ($customer->ShipAddress_Addr2)?$customer->ShipAddress_Addr2.', ':''; ?> <?php echo $customer->ShipAddress_City; ?><br>
                                <?php echo ($customer->ShipAddress_State)?$customer->ShipAddress_State.', ':''; ?> <?php echo $customer->ShipAddress_PostalCode; ?><br><br>
                                <i class="fa fa-phone"></i> <?php echo $customer->Phone; ?><br>
                                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)" ><?php echo $customer->Contact; ?> </a>
                            </address>
                            <!-- END Billing Address Content -->
                        </div>
                        <!-- END Billing Address Block -->
                    </div>
                    
                </div>
                <!-- END Customer Addresses Content -->
            </div>
            <!-- END Customer Addresses Block -->
            <?php if($this->session->userdata('logged_in')|| in_array('Add Note',$this->session->userdata('user_logged_in')['authName'] )  ) { ?>
            
            <!-- Private Notes Block -->
            <div class="block full">
                <!-- Private Notes Title -->
                <div class="block-title">
                    <h2><strong>Private</strong> Notes</h2>
                </div>
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                    <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer->ListID ; ?>" />
					<br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
				<hr>
							
				<?php   if(!empty($notes)){  foreach($notes as $note){ ?>
				
				<div>
                   <?php echo $note['privateNote']; ?>
                </div>
		     	<div class="pull-right">
					<span>Added on <strong><?php   echo date('M d, Y - h:m', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
					<span><a href="javascript:void(0);"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
				</div>
				<br>
				<hr>
				<?php } } ?>
				
				
				<br>
                <!-- END Private Notes Content -->
            </div>
			
			<?php } ?>
            <!-- END Private Notes Block -->
        </div>
    </div>
    <!-- END Customer Content -->

<!-- END Page Content -->
<div id="card_edit_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Edit/Delete  Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
		
				
				 	 
				 
     <table  class="table table-bordered table-striped table-vcenter">
	 
         <?php
            if(!empty($card_data_array)){
     		 foreach($card_data_array as $cardarray){ ?>
           
                    <tr>
                    <td class="text-left hidden-xs"><?php echo $cardarray['customerCardfriendlyName'];  ?></td>
                    <td class="text-right visible-lg"><?php echo $cardarray['CardNo'];  ?></td>
                    <td class="text-right hidden-xs"><div class="btn-group btn-group-xs">
							<a href="#" data-toggle="tooltip" title="" class="btn btn-default" onclick="set_edit_card('<?php echo $cardarray['CardID'] ; ?>');" data-original-title="Edit Card"><i class="fa fa-edit"></i></a>
							<a href="<?php echo base_url(); ?>Payments/delete_card_data/<?php echo $cardarray['CardID'] ; ?>" onClick="if(confirm('do you really want this')) return true; else return false;"  data-toggle="tooltip" title="" class="btn btn-danger"  data-original-title="Delete Card"><i class="fa fa-times"></i></a>
                          
						</div> </td>
                </tr>  
            
		<?php } 
		
			}else{ ?>   
		 <tr>
                    <td colspan="3">No Card Available!</td>
               
                </tr> 
		
		<?php } ?>
       </table>	

		<form id="thest_form" method="post" style="display:none;" action='<?php echo base_url(); ?>Payments/update_card_data' class="form-horizontal" >
				 <fieldset>
			        
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_card_number" name="edit_card_number" class="form-control" placeholder="Enter Card Number .." autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="edit_expiry" name="edit_expiry" class="form-control">
                                                        <option value="1">JAN</option>
                                                        <option value="2">FEB</option>
                                                        <option value="3">MAR</option>
                                                        <option value="4">APR</option>
                                                        <option value="5">MAY</option>
													    <option value="6">JUN</option>
                                                        <option value="7">JUL</option>
                                                        <option value="8">AUG</option>
                                                        <option value="9">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="edit_expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="edit_expiry_year" name="edit_expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_cvv" name="edit_cvv" class="form-control" placeholder="1234" autocomplete="off" />
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="edit_friendlyname">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="edit_friendlyname" name="edit_friendlyname" class="form-control" placeholder="Enter Friendly Name" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                                          <input type="hidden" id="edit_cardID" name="edit_cardID"  value="" />
                  <div class="pull-right">
				     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Edit"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
	 <!-- panel-group -->
				
				
                	
				<div id="can_div">
				 <div class="pull-right">
				    
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
				 </div>
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!------Show Payment Data------------------->


<div id="card_data_process" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Add Card</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
			
		
				
                 <form id="thest" method="post" action='<?php echo base_url(); ?>Payments/insert_new_data' class="form-horizontal" >
				 <fieldset>
			        <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Customer Name</label>
                        <div class="col-md-8">
                       		   <input type="text" id="customername" name="customername" class="form-control" value="" placeholder="Enter Customer Name ..">
                        </div>
                    </div>
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Credit Card Number</label>
                        <div class="col-md-8">
                           
                                <input type="text" id="card_number" name="card_number" class="form-control" placeholder="Enter Card Number .." autocomplete="off">
                               
                            
                        </div>
                    </div>
                     <div class="form-group">
                                                <label class="col-md-4 control-label" for="expry">Expiry Month</label>
                                                <div class="col-md-2">
                                                   	<select id="expiry" name="expiry" class="form-control">
                                                        <option value="01">JAN</option>
                                                        <option value="02">FEB</option>
                                                        <option value="03">MAR</option>
                                                        <option value="04">APR</option>
                                                        <option value="05">MAY</option>
													    <option value="06">JUN</option>
                                                        <option value="07">JUL</option>
                                                        <option value="08">AUG</option>
                                                        <option value="09">SEP</option>
                                                        <option value="10">OCT</option>
													    <option value="11">NOV</option>
                                                        <option value="12">DEC</option>
                                                       </select>
                                                </div>
												
												    <label class="col-md-3 control-label" for="expiry_year">Expiry Year</label>
                                                <div class="col-md-3">
                                                   	<select id="expiry_year" name="expiry_year" class="form-control">
													<?php 
														$cruy = date('y');
														$dyear = $cruy+15;
													for($i =$cruy; $i< $dyear ;$i++ ){  ?>
                                                        <option value="<?php echo '20'.$i;  ?>"><?php echo "20".$i;  ?> </option>
													<?php } ?>
                                                       </select>
                                                </div>
												
												
                                            </div>     
    				<div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Security Code (CVV)<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="cvv" name="cvv" class="form-control" placeholder="1234"  autocomplete="off"/>
                                
                            
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cvv">Friendly Name<span class="text-danger">*</span></label>
                        <div class="col-md-8">
                           
                                <input type="text" id="friendlyname" name="friendlyname" class="form-control" placeholder="Enter Friendly Name" />
                                
                            
                        </div>
                    </div>
                    </fieldset>
                     <input type="hidden" id="customerID11" name="customerID11"  value="" />
                  <div class="pull-right">
				     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-success" value="Save"  />
                     <button type="button" class="btn btn-sm btn-danger close1" data-dismiss="modal">Cancel</button>

                  
                    </div>
                     <br />
                    <br />
			    </form>		
            </div>                
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!------------------  View email history Popup popup ------------------>

<div id="view_history" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> View Details </h2>
                
				<div class="modal-body">
        
			<div id="data_history" style="height:740px; min-height:740px;  overflow: auto; ">
			</div>	
                 
            </div>
            
	</div>

               <div class="modal-footer">
                 <button type="button"  align="right" class="btn btn-sm pull-right btn-danger close1" data-dismiss="modal"> Cancel </button>
				 
			</div>
			
					
            </div>
			
			
	</div>
			
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js" ></script>
   <script>
        
        
        
 function set_view_history(eID){
	if(eID!=""){
    	
		
     $.ajax({
    url: '<?php echo base_url("Settingmail/get_history_id")?>',
    type: 'POST',
	data:{customertempID:eID},
    success: function(data){
		
		$('#data_history').html(data);
			  
			 
			 
	}	
}); 
	
}

}




var Pagination_email = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#email_hist').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [1] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



        
        
        $(function(){ Pagination_email.init();    nmiValidation.init(); nmiValidation1.init(); Pagination_view1.init(); Pagination_view2.init();
		
		    $('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
			});
			  $('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
			});
			 $('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
			});
		
          /*********************Template Methods**************/ 
	 
 $('#type').change(function(){
 
    
	  var typeID = $(this).val();

	  var companyID = '<?php echo $customer->companyID; ?>';
	  var customer  = '<?php echo $customer->companyName; ?>';  
	  var customerID = '<?php echo $customer->ListID; ?>';   
	 if(typeID !=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>Settingmail/set_template',
		  data : {'typeID':typeID,'companyID':companyID,'customerID':customerID,   'customer':customer },
		  success: function(data){
			       
			     data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			   $('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  }
	   });	 
	   
	 } 
 }); 

	
$.validator.addMethod('CCExp', function(value, element, params) {
      var minMonth = new Date().getMonth() + 1;
      var minYear = new Date().getFullYear();
      var month = parseInt($(params.month).val(), 10);
      var year = parseInt($(params.year).val(), 10);
      return (year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');

});
	

 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 expiry_year: {
							  CCExp: {
									month: '#expiry',
									year: '#expiry_year'
							  }
						},
					
                   		
					 cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();




 
var nmiValidation1 = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#thest_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    edit_card_number: {
                        required: true,
						minlength: 13,
                        maxlength: 16,
					    number: true
                    },
					
					 edit_expiry_year: {
							  CCExp: {
									month: '#edit_expiry',
									year: '#edit_expiry_year'
							  }
						},
					
                   		
					 edit_cvv: {
                        required: true,
                        number: true,
						minlength: 3,
                        maxlength: 4,
                    },
                  
                  
                },
              
            });
			
  $.validator.addMethod('CCExp', function(value, element, params) {  
  var minMonth = new Date().getMonth() + 1;
  var minYear = new Date().getFullYear();
  var month = parseInt($(params.month).val(), 10);
  var year = parseInt($(params.year).val(), 10);
  
  

  return (!month || !year || year > minYear || (year === minYear && month >= minMonth));
}, 'Your Credit Card Expiration date is invalid.');
			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();

	
	
	
	
function delele_notes(note_id){
	
	if(note_id !=""){
		
		
		$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/delele_note",
			data:{ 'noteID': note_id},
			success : function(response){
				
				
				   data=$.parseJSON(response);
				  
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
	}
	
}

function add_notes(){
    
    var formdata= $('#pri_form').serialize();
    if($('#private_note').val()!=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/add_note",
			data:formdata,
			success : function(response){
				
				
		     	 data=$.parseJSON(response);
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
    }
    
}

 function set_card_user_data(id, name){
      
	   $('#customerID11').val(id);
	   $('#customername').val(name);
 }
		 
	function set_edit_card(cardID){
      
        $('#thest_form').show();
        $('#can_div').hide();		
		 
		if(cardID!=""){
			
			$.ajax({
				type:"POST",
				url : "<?php echo base_url(); ?>Payments/get_card_edit_data",
				data : {'cardID':cardID},
				success : function(response){
					
					     data=$.parseJSON(response);
						
					       if(data['status']=='success'){
							    
                                $('#edit_cardID').val(data['card']['CardID']);
							    $('#edit_card_number').val(data['card']['CardNo']);
								document.getElementById("edit_cvv").value =data['card']['CardCVV'];
								document.getElementById("edit_friendlyname").value =data['card']['customerCardfriendlyName'];
							
								$('select[name="edit_expiry"]').find('option[value="'+data['card']['cardMonth']+'"]').attr("selected",true);
								$('select[name="edit_expiry_year"]').find('option[value="'+data['card']['cardYear']+'"]').attr("selected",true);
							
								
							
					   }	   
					
				}
				
				
			});
			
		}	  
		   
	}	
	
	
var Pagination_view1 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [5] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

	
	
	
var Pagination_view2 = function() { 

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('#compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [3] }
                ],
                 
                order: [[ 2, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

	
	

</script>



</div>

