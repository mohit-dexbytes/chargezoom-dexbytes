
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Customers</h2>
              <div class="block-options pull-right">
                           
                               <a class="btn btn-sm btn-success" title="Create New"  href="javascript:void(0);">Add New</a>
                             
                        </div>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="ecom-orders111" class="table compamount table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    
                    <th class="visible-lg text-left">Customer Name</th>
                    <th class="visible-lg text-left">Full Name</th>
                    <th class="text-left visible-lg">Main Email</th>
                    <th class="hidden-xs text-right">Main Phone</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($customers) && $customers)
				{
					foreach($customers as $customer)
					{
				?>
				<tr>
					
				<td class="text-left visible-lg"><a href="<?php echo base_url('home/view_customer/'.$customer['ListID']); ?>"><?php echo $customer['customerFullName']; ?></a></td>
					
					<td class="text-left visible-lg"><?php echo $customer['customerFirstName'].' '.$customer['customerLastName']; ?></td>
				
                    
					<td class="text-left visible-lg"> <a href="#set_tempemail" title="Send Email" data-backdrop="static" onclick="set_template_data('<?php echo $customer['ListID'];?>','<?php echo $customer['companyName'];?>','<?php echo $customer['companyID']; ?>', '<?php echo $customer['customerEmail']; ?>');"  data-keyboard="false" data-toggle="modal"> <?php echo $customer['customerEmail']; ?> </a> </td>
					
					
					
					
					<td class="hidden-xs text-right"><?php echo $customer['customerPhone']; ?></td>
					<td class="text-right hidden-xs"><strong>$ <?php echo ($customer['Payment'])?$customer['Payment']:'0.00'; ?></strong></td>
					<td class="text-center">
					<div class="btn-group btn-group-sm">
							<a href="<?php echo base_url('home/view_customer/'.$customer['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-default"><i class="fa fa-edit"></i></a>
						
						</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
Pagination_view111.init(); });






var Pagination_view111 = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>



 

</div>
<!-- END Page Content -->