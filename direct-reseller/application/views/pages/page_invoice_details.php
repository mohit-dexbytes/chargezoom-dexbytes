

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- Order Status -->
    <div class="row text-center">
	
	
          <div class="col-sm-6 col-lg-3">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Customer Name</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-expandOpen"><?php echo  $invoice_data['Customer_FullName']; ?></span></div>
            </div>
        </div>
	
        <div class="col-sm-6 col-lg-3">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Invoice</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-expandOpen"><?php echo  $invoice_data['RefNumber']; ?></span></div>
            </div>
        </div>
		<div class="col-sm-6 col-lg-3">
            <div class="widget">
                <div class="widget-extra themed-background-muted">
                    <h4 class="widget-content-light"><strong>Due Date</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-muted animation-expandOpen"><?php  echo  date('m-d-Y', strtotime($invoice_data['DueDate'])); ?></span></div>
            </div>
        </div>
		
		
<?php  if($invoice_data['BalanceRemaining'] =="0.00" and $invoice_data['IsPaid']=='true'){  ?>
        <div class="col-sm-6 col-lg-3">
            <div class="widget">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Status</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-success animation-expandOpen"><i class="fa fa-check"></i> Paid</span></div>
            </div>
        </div>
        <?php } 
		  if($invoice_data['AppliedAmount'] !="0.00" and $invoice_data['IsPaid']=='false'){  ?>
                  <div class="col-sm-6 col-lg-3">
            <div class="widget">
                <div class="widget-extra themed-background-warning">
                    <h4 class="widget-content-light"><strong>Status</strong></h4>
                </div>
                <div class="widget-extra-full"><span class="h2 text-warning animation-expandOpen"><i class="fa fa-info-circle"></i> Partial</span></div>
            </div>
        </div>

		<?php }	if($invoice_data['AppliedAmount'] =="0.00" and $invoice_data['IsPaid']=='false'){	?>
         <div class="col-sm-6 col-lg-3">
                    <div class="widget">
                        <div class="widget-extra themed-background-danger">
                            <h4 class="widget-content-light"><strong>Status</strong></h4>
                        </div>
                        <div class="widget-extra-full"><span class="h2 text-danger animation-expandOpen"><i class="fa fa-times-circle"></i> Unpaid</span></div>
                    </div>
                </div>
        
        <?php } ?>
        
        
    </div>
    <!-- END Order Status -->

    <!-- Products Block -->
    <div class="block">
        <!-- Products Title -->
        <div class="block-title">
			<div class="block-options pull-right">
				<a href="<?php echo base_url(); ?>home/invoice_details_print/<?php echo  $invoice_data['TxnID']; ?>" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Print Invoice"><i class="fa fa-print"></i></a>
                <?php  if($invoice_data['BalanceRemaining'] =="0.00" and $invoice_data['IsPaid']=='true'){  ?> 
				<a href="#" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Email Invoice to Customer"><i class="fa fa-envelope-o"></i></a>
                <?php } 
		  if($invoice_data['AppliedAmount'] !="0.00" and $invoice_data['IsPaid']=='false'){  ?>
          <a href="#" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Send Reminder Email to Customer"><i class="fa fa-envelope text-warning"></i></a>
				<a href="#" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Email Invoice to Customer"><i class="fa fa-envelope-o"></i></a>
          <?php }	if($invoice_data['AppliedAmount'] =="0.00" and $invoice_data['IsPaid']=='false'){	?>
          	<a href="#" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Send Reminder Email to Customer"><i class="fa fa-envelope text-danger"></i></a>
			<a href="#" class="btn btn-alt btn-sm btn-default" data-toggle="tooltip" title="Email Invoice to Customer"><i class="fa fa-envelope-o"></i></a>
           <?php } ?>
           <a href="#set_email" title="Send Email"  class="btn btn-alt btn-sm btn-default"  data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-paper-plane-o"></i></a>
			</div>
            <h2><strong>Invoice</strong> Details</h2>
			
        </div>
        <!-- END Products Title -->

        <!-- Products Content -->
         <div class="table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th>Product/Service Name</th>
                        <th class="text-right">Qty</th>
                        <th class="text-right">Unit Rate</th>
						<th class="text-right">Amount</th>
						<th class="text-right">Tax</th>
                    </tr>
                </thead>
                <tbody>
				        <?php  
						
					
						$totaltax =0 ; $total=0; $tax=0; 
						foreach($invoice_items as $key=>$item){ 
						 
						?>
				   
                    <tr>
                        <td><?php echo $item['Item_FullName']; ?></td>
                        <td class="text-right"><?php echo $item['Quantity']; ?></td>
                        <td class="text-right"><?php echo number_format($item['Rate'],2); ?></td>
						<td class="text-right"><?php  $total+=  $item['Quantity']*$item['Rate']; echo number_format($item['Quantity']*$item['Rate'],2);  ?></td>
						<th class="text-right"><strong><?php  if($item['TaxRate']){ $tax= ($item['TaxRate']*100)/($item['Quantity']*$item['Rate']);    $totaltax+= $tax ; }  echo ($tax)?$tax:'0.00'; ?> <a href="#" data-toggle="tooltip" title="<?php echo $item['SalesTaxCode_FullName']; ?> @ <?php echo ($tax)? $tax:'0.00'; ?>%"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></th>
					
                    </tr>
						<?php } ?>
                  
					<tr class="active">
                        <td colspan="3" class="text-right text-uppercase"><strong>Total</strong></td>
                        <td class="text-right"><strong><?php  echo number_format( $total,2) ; ?></strong></td>
						<td class="text-right"><strong><?php echo  number_format($totaltax,2) ; ?></strong></td>
                    </tr>
					<tr>
                        <td colspan="4" class="text-right text-uppercase"><strong>Any Other Tax</strong></td>
                        <td class="text-right"><strong>00.00 <a href="#" data-toggle="tooltip" title="No Other Tax"><i class="fa fa-exclamation-circle text-danger"></i></a></strong></td>
                    </tr>
					<tr class="info">
                        <td colspan="4" class="text-right text-uppercase"><strong>Total Payable</strong></td>
                        <td class="text-right"><strong><?php echo  number_format($total+$totaltax,2) ;  ?></strong></td>
                    </tr>
                    <tr class="success">
                        <td colspan="4" class="text-right text-uppercase"><strong>Paid</strong></td>
                        <td class="text-right"><strong><?php $paid = number_format(-$invoice_data['AppliedAmount'],2); echo ($paid)?$paid:'0.00';  ?></strong></td>
                    </tr>
					<tr class="danger">
                        <td colspan="4" class="text-right text-uppercase"><strong>Balance</strong></td>
                        <td class="text-right"><strong><?php echo ($invoice_data['BalanceRemaining'])?$invoice_data['BalanceRemaining']:'0.00';  ?></strong></td>
                    </tr>
                </tbody>
				
            </table>
			<div class="col-md-12 ">
			<div class="pull-right">
			<a href="#invoice_process" class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice_data['TxnID']; ?>', '<?php  echo $invoice_data['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
								     <a href="#invoice_schedule" class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice_data['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice_data['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
									 <a href="#invoice_cancel" class="btn btn-sm btn-danger"  onclick="set_invoice_id('<?php  echo $invoice_data['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a>
									 <br><br>
			  </div>						 
			 
			</div>
			
        </div>
        <!-- END Products Content -->
    </div>  
    <!-- END Products Block -->

    <!-- Addresses -->
    <div class="row">
        <div class="col-sm-6">
            <!-- Billing Address Block -->
            <div class="block">
                <!-- Billing Address Title -->
                <div class="block-title">
                    <h2><strong>Billing</strong> Address</h2>
                </div>
                <!-- END Billing Address Title -->

                <!-- Billing Address Content -->
                <h4><strong><?php echo $invoice_data['Customer_FullName']; ?></strong></h4>
				<address>
					<strong><?php echo $customer_data['companyName']; ?></strong><br>
					<?php echo $customer_data['ShipAddress_Addr1']; ?><br>
					<?php echo  ($customer_data['ShipAddress_City'])?$customer_data['ShipAddress_City'].',':'' . $invoice_data['ShipAddress_State'];  ?><br>
					<?php echo 'United States' .', '. $customer_data['ShipAddress_PostalCode'];  ?><br><br>
					<i class="fa fa-phone"></i> <?php echo $customer_data['Phone']    ?><br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
				</address>
                <!-- END Billing Address Content -->
            </div>
            <!-- END Billing Address Block -->
        </div>
        <div class="col-sm-6">
            <!-- Shipping Address Block -->
            <div class="block">
                <!-- Shipping Address Title -->
                <div class="block-title">
                    <h2><strong>Shipping/Customer</strong> Address</h2>
                </div>
                <!-- END Shipping Address Title -->

                <!-- Shipping Address Content -->
                  <h4><strong><?php echo $invoice_data['Customer_FullName']; ?></strong></h4>
				<address>
					<strong><?php echo $customer_data['companyName']; ?></strong><br>
					<?php echo $invoice_data['ShipAddress_Addr1']; ?><br>
					<?php echo ($customer_data['ShipAddress_City'])?$customer_data['ShipAddress_City'].',':'' . $invoice_data['ShipAddress_State'];  ?><br>
					<?php echo 'United States' .', '. $customer_data['ShipAddress_PostalCode'];  ?><br><br>
					<i class="fa fa-phone"></i> <?php echo $customer_data['Phone']    ?><br>
					<i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"> <?php echo $customer_data['Contact'];    ?></a>
				</address>
                <!-- END Shipping Address Content -->
            </div>
            <!-- END Shipping Address Block -->
        </div>
    </div>
    <!-- END Addresses -->

    <!-- Log Block -->
    <div class="block full">
                <!-- Private Notes Title -->
                <div class="block-title">
                    <h2><strong>Private</strong> Notes</h2>
                </div>
                <!-- END Private Notes Title -->

                <!-- Private Notes Content -->
                <div class="alert alert-info">
                    <i class="fa fa-fw fa-info-circle"></i> These notes will be for your internal purposes. These will not go to the customer.
                </div>
                <form  method="post"  id="pri_form" onsubmit="return false;" >
                    <textarea id="private_note" name="private_note" class="form-control" rows="4" placeholder="Your note.."></textarea>
                    <input type="hidden" name="customerID" id="customerID" value="<?php echo $customer_data['ListID'] ; ?>" />
					<br>
                    <button type="submit" onclick="add_notes();" class="btn btn-sm btn-success">Add Note</button>
                </form>
				<hr>
				
				<?php 

                   

				if(!empty($notes)){  foreach($notes as $note){ ?>
				
				<div>
                   <?php echo $note['privateNote']; ?>
                </div>
		     	<div class="pull-right">
					<span>Added on <strong><?php   echo date('M d, Y - h:m', strtotime($note['privateNoteDate'])); ?>&nbsp;&nbsp;&nbsp;</strong></span>
					<span><a href="javascript:void(0)"  onclick="delele_notes('<?php echo $note['noteID'] ; ?>');" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a></span>
				</div>
				<br>
				<hr>
				<?php } } ?>
				
				
				<br>
                <!-- END Private Notes Content -->
            </div>
    <!-- END Log Block -->



<div id="set_email" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">

    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
			
            <div class="modal-header ">
               <h2 class="modal-title pull-left">Send Email</h2>
             
                    
                  
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
                  <form id="form-validation" action="<?php echo base_url(); ?>Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                   
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Template</label>
                                        <div class="col-md-7">
                                      
                                          <select id="type" name="type" class="form-control">
                                            <option value="">Choose Template</option>
                                                <option value="1">Invoice due</option>
                                                <option value="2">Invoice past due/overdue</option>
                                                <option value="3">Invoice due soon/upcoming</option>
                                                  <option value="5">Payment Receipt</option>
                                            </select>  
                                            
                                        </div>
                                    </div>
                                   
                                   
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">To Email</label>
                                        <div class="col-md-7">
                                             <input type="text" id="toEmail" name="toEmail"  value=""   class="form-control" placeholder="Enter the name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php   echo $login_info['merchantEmail'];  ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="Enter the cc email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="Enter the bcc Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="replyEmail">Set the "Reply-To" to this address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Enter the email">
                                        </div>
                                    </div>
                                   
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Enter the Subject">
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                  <div class="form-group form-actions">
                                    <div class="col-md-8 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Save </button>
                                        <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Close</button>
                                        
                                      
                                    </div>
                                </div>  
                           </form>
		    	
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

 <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js" ></script>
        <script>
        
        $(function(){   
		
			 CKEDITOR.replace( 'textarea-ckeditor', {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
			});
			CKEDITOR.config.allowedContent = true;
		    $('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
			});
			  $('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
			});
			 $('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
			});
		
          /*********************Template Methods**************/ 
	 
 $('#type').change(function(){
 
    
	  var typeID = $(this).val();

	  var companyID = '<?php echo $customer_data['companyID']; ?>';
	  var customer  = '<?php echo $invoice_data['Customer_FullName']; ?>';  
	  var customerID = '<?php echo $invoice_data['Customer_ListID']; ?>';  
	  var invoiceID  = '<?php echo $invoice_data['TxnID']; ?>';
	 if(typeID !=""){
		 
	   $.ajax({
		  type:"POST",
		  url : '<?php echo base_url(); ?>Settingmail/set_template',
		  data : {'typeID':typeID,'companyID':companyID,'customerID':customerID, 'invoiceID':invoiceID,  'customer':customer },
		  success: function(data){
			     data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
			
			   $('#emailSubject').val(data['emailSubject']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
		  }
	   });	 
	   
	 } 
 }); 


});



function delele_notes(note_id){
	
	if(note_id !=""){
		
		
		$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/delele_note",
			data:{ 'noteID': note_id},
			success : function(response){
				
				
				   data=$.parseJSON(response);
				  
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
	}
	
}

function add_notes(){
    
    var formdata= $('#pri_form').serialize();
   
    if($('#private_note').val()!=""){
    
    
    	$.ajax({  
		       
			type:'POST',
			url:"<?php echo  base_url(); ?>home/add_note",
			data:formdata,
			success : function(response){
				
				 data=$.parseJSON(response);
				 
				  if(data['status']=='success'){
					  location.reload(true);
				  } 
			}
			
		});
		
    }
    
    
    
}


</script>

</div>

