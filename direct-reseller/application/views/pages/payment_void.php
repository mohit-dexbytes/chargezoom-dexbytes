
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    
                        <!-- Forms General Header -->
                        <div class="content-header">
                            <div class="header-section">
                                <h1>
                                    <i class="gi gi-notes_2"></i>General NMI Payment Form<br><small>You can capture payment using following !</small>
                                </h1>
                            </div>
                        </div>
                      
                        <!-- END Forms General Header -->

                        <div class="row">  <div class="col-md-12">
                                <!-- Form Validation Example Block -->
								
								
							
                                <div class="block">
                                    <!-- Form Validation Example Title -->
                                  
                                    <!-- END Form Validation Example Title -->
                             
                                    <!-- Form Validation Example Content -->
                                    <form id="form-validation" action="<?php echo base_url(); ?>Payments/create_customer_void" method="post" class="form-horizontal form-bordered">
                                        <fieldset>
                                            <legend><i class="fa fa-angle-right"></i> Billing Info</legend>
											 
											 <div class="form-group">
                                                <label class="col-md-4 control-label" for="customerID">Cutomer Name</label>
                                                <div class="col-md-6">
                                                    <select id="customerID" name="customerID" class="form-control">
                                                      
                                                        <option value>Choose Customer</option>
														<?php   foreach($customers as $customer){       ?>
														
                                                        <option value="<?php echo $customer['ListID']; ?>"><?php echo  $customer['FullName'] ; ?></option>
														<?php } ?>
                                                    </select>
                                                </div>
                                            </div>
											
											
											
                                            <div class="form-group">
                                                <label class="col-md-4 control-label" for="card_number">Transaction ID <span class="text-danger">*</span></label>
                                                <div class="col-md-6">
                                                    <div class="input-group">
                                                        <input type="text" id="transaction_id" name="transaction_id" class="form-control" placeholder="Your transaction  Number ..">
                                                        <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            
											
                                        </fieldset>
                                      

									  
									
											
                                        <div class="form-group form-actions">
                                            <div class="col-md-8 col-md-offset-4">
                                                <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-arrow-right"></i> void</button>
                                              
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END Form Validation Example Content -->

                                
                                    <!-- END Terms Modal -->
                                </div>
                                <!-- END Validation Block -->
                            </div>
					  </div>

    



    <script>$(function(){  nmiValidation.init(); });
	
	
	
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                rules: {
                    transaction_id: {
                        required: true,
                      
					    number: true
                    },
                    
                  
                    customerID: {
                         required: true,
                       
                    },
					
                  
                },
                messages: {
					  customerID: {
                        required: 'Please select a customer',
                      
                    },
                    transaction_id: {
                        required: 'Please enter a username',
                        minlength: 'Your username must consist of at least 3 characters'
                    },
                   
                }
            });

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
	
	
	
	
	
	
	</script>
</div>