<?php

class Card_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	
		 $this->db1 = $this->load->database('otherdb', TRUE);
	}
	
	
	public function get_ach_card_data($customerID, $merchantID)
	{
	        $card = array();
	        $sql = " SELECT cr.accountName,cr.CardID from customer_card_data cr  where  cr.customerListID='$customerID'  and cr.merchantID='$merchantID' and cr.accountNumber!=''   "; 
			
		
				    $query1 = $this->db1->query($sql);
				    if($query1->num_rows()>0)
                    $card =   $query1->result_array();
				 
                return  $card;
	    
	}
	  
    public function get_card_expiry_data($customerID){  
  
                       $card = array();
               		   $this->load->library('encrypt');
             if($this->session->userdata('logged_in')){
    			$merchantID = $this->session->userdata('logged_in')['merchID'];
    		}
    		if($this->session->userdata('user_logged_in')){
    			$merchantID = $this->session->userdata('user_logged_in')['merchantID'];
    			}	
		
	   	        
			  
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  customerListID='$customerID'  and merchantID='$merchantID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_datas =   $query1->result_array();
				  if(!empty($card_datas )){	
						foreach($card_datas as $key=> $card_data){

						 $card_data['CardNo']  = substr($this->decrypt($card_data['CustomerCard']),12) ;
						 $card_data['CardID'] = $card_data['CardID'] ;
						 $card_data['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						 
						 $card_data['accountNumber'] = $card_data['accountNumber'] ;
						 $card_data['accountName']  = $card_data['accountName'] ;
						 $card_data['CardType'] = $card_data['CardType'] ;
				
						 $card[$key] = $card_data;
					   }
				}		
					
                return  $card;

     }
     
     
     	
	public function get_credit_card_info($compID)
	{
		$card_data=array();
	  
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
		 $query1   = $this->db1->query($sql);
		$card_data =   $query1->result_array();
		return $card_data;
	}
	
	
	
	public function get_credit_card_info_data($compID){
	    
		 
		  $sql  = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 60 Day )   and `merchantID` = '$compID' ";
		  
    	 $query1   = $this->db1->query($sql);
	    return $query1->result_array();
	}
	
	
   public function get_single_card_data($cardID)
  {  
  
                  $card = array();
               	
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c 
		     	where  CardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						
						 $card['CardNo']     = $this->decrypt($card_data['CustomerCard']) ;
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						   $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
                          $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
                          $card['Billing_City']	 = $card_data['Billing_City'];
                          $card['Billing_State']	 = $card_data['Billing_State'];
                          $card['Billing_Country']	 = $card_data['Billing_Country'];
                          $card['Billing_Contact']	 = $card_data['Billing_Contact'];
                          $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];
                           $card['CardType']	 = $card_data['CardType'];
				}		
					
					return  $card;

       }
 

	
  public function get_expiry_card_data($customerID, $type){  
  
                       $card = array();
               		   $this->load->library('encrypt');
					   
			  if($type=='1'){ 
			    /**************Expired Card***********/
			    
		     	$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )+INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date 
				from customer_card_data c  where (STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY) <= DATE_add( CURDATE( ) ,INTERVAL 30 Day )  order by expired_date asc  "; 
			  }
            
			  if($type=='0'){
				  
				  /**************Expiring SOON Card***********/
				$sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from customer_card_data c
				where 
				(STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY)   <=    DATE_add( CURDATE( ) ,INTERVAL 60 Day )  and STR_TO_DATE( CONCAT( '01,', c.cardMonth, ',', c.cardYear), '%d,%m,%Y' )  > DATE_add( CURDATE( ) ,INTERVAL 0 Day )  order by CardID desc  ";  
			 			  
			  }	
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->row_array();
					
			        if(!empty($card_data)){  
					      
			             $card['CustomerCard']     = substr($this->decrypt($card_data['CustomerCard']),12); 
						 $card['cardMonth']  = $card_data['cardMonth'];
						  $card['cardYear']  = $card_data['cardYear'];
						  $card['expiry']    = $card_data['expired_date'];
						  $card['CardID']    = $card_data['CardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['customerListID'] = $card_data['customerListID'];
						  $card['customerCardfriendlyName']  = $card_data['customerCardfriendlyName'] ;
						  
						
					}
					
					return  $card;
					
					
				 
    }
    
    
   public function  last_four_digit_card($cid, $mID)
   {
                       $card = '';
               		   $this->load->library('encrypt');
               	       $sql =   "Select CustomerCard from  customer_card_data where  customerListID = '".$cid."' and merchantID ='".$mID."'  order by CardID desc  limit 1   ";
               
               		   $query1 = $this->db1->query($sql);
                 
               		   if($query1->num_rows() > 0)
               		   {
                          $card_data = $query1->row_array();
                           $card= substr($this->decrypt($card_data['CustomerCard']),12);
               		   }
               		   return  $card;
   }
    
    
     public function check_friendly_name($cid, $cfrname)
     {
		         $card = array();
               		  $this->load->library('encrypt');
					
					
				$query1 = $this->db1->query("Select CardID from customer_card_data where customerListID = '".$cid."' and customerCardfriendlyName ='".$cfrname."' ");
				
					$card = $query1->row_array();	
					
					return $card;
		
	}	

	
	   	
  public function get_merchant_card_expiry_data($merchantID){  
						$card_data = array();    
						$card      = array();
						$this->load->library('encrypt');
				    
			  
	     	 $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where   merchantListID='$merchantID'  "; 
				    $query1 = $this->db1->query($sql);
			        $card_data =   $query1->result_array();
			      
					
					return  $card_data;

 }
 
 	
	 public function get_merchant_single_card_data($merchantCardID){  
  
                  $card = array();
              
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  merchantCardID='$merchantCardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
				  if(!empty($card_data )){	
						 $card['CardID'] = $card_data['merchantCardID'];
						 $card['CardNo']     = $this->decrypt($card_data['MerchantCard']) ;
						 $card['CardMonth']  = $card_data['CardMonth'];
						  $card['CardYear']  = $card_data['CardYear'];
						  $card['merchantCardID']    = $card_data['merchantCardID'];
						  $card['CardCVV']   = $this->decrypt($card_data['CardCVV']);
						  $card['merchantFriendlyName']  = $card_data['merchantFriendlyName'] ;
						  $card['Billing_Addr1']	 = $card_data['Billing_Addr1'];	
		                  $card['Billing_Addr2']	 = $card_data['Billing_Addr2'];
		                  $card['Billing_City']	     = $card_data['Billing_City'];
		                  $card['Billing_State']	 = $card_data['Billing_State'];
		                  $card['Billing_Country']	 = $card_data['Billing_Country'];
		                  $card['Billing_Contact']	 = $card_data['billing_phone_number'];
		                  $card['Billing_Zipcode']	 = $card_data['Billing_Zipcode'];

		                  $card['billing_phone_number'] 	= $card_data['billing_phone_number'];
						  $card['billing_email'] 			= $card_data['billing_email'];
						  $card['billing_first_name'] 		= $card_data['billing_first_name'];
						  $card['billing_last_name'] 		= $card_data['billing_last_name'];


		                  $card['CardType']	     = $card_data['CardType'];
		                   
		                  $card['acc_number']	 = $card_data['accountNumber'];	
		                  $card['route_number'] 	 = $card_data['routeNumber'];
		                
		                  $card['acc_name']	 = $card_data['accountName'];
		                  $card['secCodeEntryMethod']	 = $card_data['secCodeEntryMethod'];
		                  $card['acct_type']	         = $card_data['accountType'];
		                  $card['acct_holder_type']	 = $card_data['accountHolderType'];
				}		
					
					return  $card;

       }
 
   public function get_merchant_card_num($con)
   {
        $num =0;
        $this->db1->select('*');
        $this->db1->from('merchant_card_data');
        $this->db->where($con);
        $query = $this->db->query();
        if($query->num_rows() > 0)
        {
            $num = $query->num_rows();
            
        }
        
        return $num;
       
   }
  
  public function get_single_mask_card_data($cardID)
  {  
  
                  $card_data = array();
               	 
		        $sql = "SELECT *, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where  	merchantCardID='$cardID'    "; 
				    $query1 = $this->db1->query($sql);
                   $card_data =   $query1->row_array();
                   
                   
                   
				  if(!empty($card_data )){	
				
				     	$re = '/\d(?=\d{4})/m';
				     	$str =$this->decrypt($card_data['MerchantCard']);
					   	 $card_data['CardNo']     =  preg_replace($re, "X", $str); 
					   	
						  $card_data['CardCVV']   = $this->decrypt($card_data['CardCVV']);
					
				}		
					
					return  $card_data;

       }    
       
       
       
       public function create_dev_api_key($ins_data)
       {
          
             
             $this->db2->insert('tbl_merchant_api',$ins_data);
             return $id = $this->db2->insert_id();
           
       }
       
       
         public function delete_dev_api_key($con)
       {
          
             $this->db2->where($con);
             $this->db2->delete('tbl_merchant_api');
            
            if($this->db2->affected_rows()>0)
             return true;
             else
             return false;
           
       }
 
    
    public function  get_merchant_card_data($merchantID)
    {
        	$newcard=	$card_data = array();    
						$card      = array();
					
	    	 $sql = "SELECT accountNumber,merchantCardID,CardMonth, CardYear,  CardType, merchantListID, MerchantCard,merchantFriendlyName,CardCVV, STR_TO_DATE( CONCAT( '01,', c.CardMonth, ',', c.CardYear), '%d,%m,%Y' ) + INTERVAL 1 MONTH - INTERVAL 1 DAY as expired_date from merchant_card_data c 
		     	where   merchantListID='$merchantID'  "; 
				    $query1 = $this->db1->query($sql);
			        $card_data1 =   $query1->result_array();
			      
			        if(!empty($card_data1))
			        {
    			        foreach($card_data1 as $card)
    			        {
    					 	$re = '/\d(?=\d{4})/m';
    				     	$str =$this->decrypt($card['MerchantCard']);
    					   	$card_data['CardNo']     =  preg_replace($re, "X", $str); 
    						$card_data['CardCVV']   = $this->decrypt($card['CardCVV']);
    						$card_data['CardType']   = $card['CardType'];
    						$card_data['expiryDate']   = $card['expired_date'];
    						$card_data['merchantCardID']   = $card['merchantCardID'];
    						$card_data['accountNumber']   = $card['accountNumber'];
    						$card_data['merchantFriendlyName']   = $card['merchantFriendlyName'];
    						     
    						$newcard[] = $card_data;
    			        }
			        }   
					return  $newcard;
    }

   
     public function update_card_data($condition, $card_data=array())
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->update('merchant_card_data',$card_data);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('*');
            $this->db1->from('merchant_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             
             return   $card['merchantCardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	 }	 
	 
	 
     public function delete_card_data($condition)
	 {  
	     $status=false;
	     $this->db1->where($condition);
		 $updt = $this->db1->delete('merchant_card_data');
		 if($updt)
		 {
            $status =true;
		 }	 
		 return $status;

	 }	 
	 
	  public function insert_card_data($card_data)
	 {  
	     $status=false;
	   
		 $this->db1->insert('merchant_card_data',$card_data);
		 return $status = $this->db1->insert_id();
		 

	 }	 
	 

	
function sign($message, $key) {
    return hash_hmac('sha256', $message, $key) . $message;
}

function verify($bundle, $key) {
    return hash_equals(
      hash_hmac('sha256', mb_substr($bundle, 64, null, '8bit'), $key),
      mb_substr($bundle, 0, 64, '8bit')
    );
}

function getKey($password, $keysize = 40) {
    return hash_pbkdf2('sha256',$password,'some_token',100000,$keysize,true);
}

function encrypt($message) {
	$password= ENCRYPTION_KEY;
    $iv = random_bytes(16);
    $key = $this->getKey($password);
    $result = $this->sign(openssl_encrypt($message,'aes-256-ctr',$key,OPENSSL_RAW_DATA,$iv), $key);
    return bin2hex($iv).bin2hex($result);
}

function decrypt($hash) {
	
	$password= ENCRYPTION_KEY;
    $iv = hex2bin(substr($hash, 0, 32));
    $data = hex2bin(substr($hash, 32));
    $key = $this->getKey($password);
    if (!$this->verify($data, $key)) {
      return null;
    }
    return openssl_decrypt(mb_substr($data, 64, null, '8bit'),'aes-256-ctr',$key,OPENSSL_RAW_DATA,$iv);
}

	 
	public function update_merchant_card_data($condition, $card_data=array())
	{  
	     $status=false;

	     $this->db1->where($condition);
		 $updt = $this->db1->update('merchant_card_data',$card_data);
		 if($updt)
		 {
            $status =true;
            $this->db1->select('*');
            $this->db1->from('merchant_card_data');
             $this->db1->where($condition);
             $query = $this->db1->get();
             if($query->num_rows() >0)
            {  
             $card =  $query->row_array();
             return   $card['merchantCardID'];
            }
            return $status;
            
		 }	 
		 return $status;

	}	 
	 
    public function insert_merchant_card_data($card_data)
	{  

		$this->db1->insert('merchant_card_data',$card_data);
    	return	 $status = $this->db1->insert_id();

	}       
	public function get_merch_card_data($cardID)
	{
	        $res=[];

	        $this->db1->select('mcd.*');
	        $this->db1->from('merchant_card_data mcd');
	        $this->db1->where('mcd.merchantCardID', $cardID);
	        $res = $this->db1->get()->row();
	        
	        if(isset($res->MerchantCard) && $res->MerchantCard != null){
	        	$res->MerchantCard = $this->decrypt($res->MerchantCard);
	        }
	        if(isset($res->CardCVV) && $res->CardCVV != null){
	        	$res->CardCVV = $this->decrypt($res->CardCVV);
	        }
	       
		 	return $res;
	    
	}   
 
}