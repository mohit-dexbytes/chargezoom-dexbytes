<?php
Class General_model extends CI_Model
{
	
	function general()
	{
		parent::Model();
		$this->load->database();
		
	}
	
	function insert_row($table_name, $array, $skipKeys=['message'])
	{

		$dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		}
		if($this->db->insert($table_name,$dataToInsert))

		return $this->db->insert_id();
		else
		return false;

	}
	
	function update_row($table_name,$reg_no,$array)
	{
		$this->db->where('reg_no', $reg_no);
		if($this->db->update($table_name, $array))
			return true;
		else
			return false;
	}
	
	public function check_existing_user($tbl, $array)
    {
        $this->db->select('*');
        $this->db->from($tbl);
		if($array !='')
        $this->db->where($array);
		
        $query = $this->db->get();
		if($query->num_rows() > 0)
			return true;
        
        else 
          return false; 
	}
	
	
	function update_row_data($table_name,$condition, $array, $skipKeys=['message'])
	{
		$dataToUpdate = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

		$this->db->where($condition);
		$trt = $this->db->update($table_name, $dataToUpdate); 
		if($trt)
			return true;
		else
			return false;
	}
	
	   public function get_row_data($table, $con) {
			
			$data=array();	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	  public function get_table_data($table, $con) {
	    $data=array();

			$this->db->select('*');
			$this->db->from($table);
			if(!empty($con)){
			$this->db->where($con);
			}
			
			$query  = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
	       }
	  }
	  
	  public function get_select_data($table, $clm, $con)
	{
		
		     $data=array();	
			
			 $columns = implode(',',$clm);
			
			$query = $this->db->select($columns)->from($table)->where($con)->get();
		  
			if($query->num_rows()>0 ) {
				$data = $query->row_array();
			   return $data;
			} else {
				return $data;
			}			
	    }
	  
	  ///---------  to get the email history data --------///
	
	   public function get_email_history($con) {
			
			$data=array();	
			
			$this->db->select('*');
			$this->db->from('tbl_template_data');
			$this->db->where($con);
			$query = $this->db->get();
			
			if($query->num_rows()>0 ) {
				$data = $query->result_array();
			   return $data;
			} else {
				return $data;
			}				
	
	}
	
	
	 //---------  to get the customer subscriptions --------///
	
	 
	public function get_subscriptions_data($condition){
	
			$this->db->select('sbs.*, cust.FullName, cust.companyName, pl.planName, tmg.*');
		$this->db->from('tbl_subscriptions sbs');
		$this->db->join('qb_test_customer cust','sbs.customerID = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
		$this->db->join('tbl_subscription_plan pl','pl.planID = sbs.subscriptionPlan','INNER');
		$this->db->join('tbl_merchant_gateway tmg','sbs.paymentGateway = tmg.gatewayID','Left');
	    $this->db->where($condition);
		
		$this->db->where('cust.customerStatus', '1');
	    $this->db->order_by("sbs.createdAt", 'desc');
		
		
		$query = $this->db->get();
		if($query->num_rows() > 0){
		
		  return $query->result_array();
		}
		
	} 
	  
	  
	  
	  
	  public function get_num_rows($table, $con) {
			
			$num = 0;	
			
			$query = $this->db->select('*')->from($table)->where($con)->get();
			$num = $query->num_rows();
			return $num;
		
	}
	
	
	public function delete_row_data($table, $condition){
	   
     	    $this->db->where($condition);
     	    $this->db->delete($table);
            if($this->db->affected_rows()>0){
			  return true;
			}else{
			  return false;
			}
	
	}
	
      
//////// get data from tbl_auth//////		
		   
		   public function get_auth_data($auth_con) {
             $auth=array();
 

			 
			   $query  = $this->db->query("Select authName from tbl_auth where authID in($auth_con)");
			   if($query->num_rows()>0 ) {
				$datas = $query->result_array();
				foreach($datas as $key=>$data){
				
						   $auth[] = $data['authName']; 
				}
				
				  return $auth;
			   } else {
				return $auth;
					}
   }
	  
	  
	  

	public function get_credit_user_data($con)
    {
		$res = array();
		$this->db->select('cr.*,sum(cr.creditAmount) as balance, cust.companyName, cust.FullName');
		$this->db->from('tbl_credits cr');
		$this->db->join('qb_test_customer cust','cr.creditName = cust.ListID','INNER');
		$this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
	    $this->db->where($con);
		$this->db->where('cust.customerStatus', '1');
		$this->db->group_by('cr.creditName');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		 return $res;
		
		
		
	} 
	
      
	  
	 
    public function get_merchant_user_data($userID, $muID='')
    {
		if($muID != "") {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantUserID = '".$muID."' and  tmu.merchantID = '".$userID."' ");		
   			$res = $query->row_array();
			
		} else {
			$query = $this->db->query("Select tmu.*,rl.roleName,rl.authID from  tbl_merchant_user tmu inner join tbl_role rl on rl.roleID=tmu.roleId  inner join tbl_auth ath on rl.authID=ath.authID where tmu.merchantID = '".$userID."' ");
			$res = $query->result_array();
		} 
		return $res;
	}
	
      
      
      
       public function get_data_admin_reseller() {
	   $res = array();
		$this->db->select(' rs.*, (select count(*) from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as customer_count,
		(select sum(-AppliedAmount) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as applied,
		(select sum(BalanceRemaining) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as balance,
		(select count(*) from tbl_merchant_data inner join tbl_reseller on tbl_merchant_data.resellerID=tbl_reseller.resellerID where tbl_reseller.resellerID=mr.resellerID ) as merchant,');
		$this->db->from('tbl_reseller rs');
		$this->db->join('tbl_merchant_data mr', 'rs.resellerID = mr.resellerID','INNER');
		$this->db->join('tbl_company cmp', 'mr.merchID = cmp.merchantID','LEFT');
		$this->db->join('qb_test_customer cust','cust.companyID = cmp.id','LEFT');
		$this->db->join('qb_test_invoice inv','cust.ListID = inv.Customer_ListID','LEFT'); 
	    $this->db->where('rs.isDelete = 0');
		$this->db->group_by('rs.resellerID');
		$query = $this->db->get();
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		
		 return $res;
		
	  } 
      
	 
      
     public function get_data_admin_merchant() {
	   $res = array();
		$this->db->select(' mr.*, (select count(*) from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as customer_count,
		(select sum(-AppliedAmount) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as applied,
		(select sum(BalanceRemaining) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as balance,
		rs.resellerfirstName');
		$this->db->from('tbl_merchant_data mr');
		$this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID','INNER');
		$this->db->join('tbl_company cmp', 'mr.merchID = cmp.merchantID','LEFT');
		$this->db->join('qb_test_customer cust','cust.companyID = cmp.id','LEFT');
		$this->db->join('qb_test_invoice inv','cust.ListID = inv.Customer_ListID','LEFT'); 
	    $this->db->where('mr.isDelete = 0');
		$this->db->group_by('mr.merchID');
		$query = $this->db->get();

        
		if($query->num_rows()){
		
		 $res = $query->result_array();
		}
		
		
		 return $res;
		
	  } 
      
      
      
      public function check_existing_portalprefix($portalprefix)
         {
           $num = 0; 
		
				   
				   $query = $this->db->select('*')->from('Config_merchant_portal')->where('portalprefix', $portalprefix)->get();
				   $num = $query->num_rows();
				   if( $query->num_rows()>0){
					$num =$query->num_rows();
				   return true;
					 }
					 $query1 = $this->db->select('*')->from('tbl_config_setting')->where('portalprefix', $portalprefix)->get();
					if( $query->num_rows()>0){
					 $num =$query1->num_rows();
					 return true;
					 }  
						
				  if($num > 0)
				   return true;
						
						else 
						  return false; 
		 } 
		 
 
     public function check_existing_edit_portalprefix($tab, $rID, $lportalprefix){
    
		if($tab=='Config_merchant_portal'){
		 $rdata = $this->get_row_data($tab,array('resellerID'=>$rID));
		 if($lportalprefix==$rdata['portalprefix']){
		  return false;
		 }else{ 
		 
	   if(!empty($this->get_row_data($tab,array('portalprefix'=>$lportalprefix))))
	    return true;
		  else 
	     $sta= false;
	 

		  if($sta ==false)
				if( !empty($this->get_row_data('tbl_config_setting',array('portalprefix'=>$lportalprefix)))) 
				return  true;
		  else
	      $sta= false;
	  
		return $sta; 
	  }
	  
		}
      }  
      
      
      public function get_reselr_agent_data_data($con)
      {
          $this->db->select(' *, (select count(*) from tbl_merchant_data where agentID=tbl_reseller_agent.ragentID and  resellerID=tbl_reseller_agent.resellerIndexID AND isEnable = 1 and isDelete = 0) as addMerchant, (case when previledge="ViewAll" then "View All" when  previledge="EditAll" then "Edit All" when  previledge="FullAdmin" then "Full Admin" else "None" end) as prev ');
          	$this->db->from('tbl_reseller_agent');
	
	    $this->db->where($con);
		
	  	$query = $this->db->get();

	    return $res =  $query->result_array();
      }
      
      
	
	
  public  function getcardType($CCNumber)
        {
            
            
$creditcardTypes = array(
            array('Name'=>'American Express','cardLength'=>array(15),'cardPrefix'=>array('34', '37'))
            ,array('Name'=>'Maestro','cardLength'=>array(12, 13, 14, 15, 16, 17, 18, 19),'cardPrefix'=>array('5018', '5020', '5038', '6304', '6759', '6761', '6763'))
            ,array('Name'=>'Mastercard','cardLength'=>array(16),'cardPrefix'=>array('51', '52', '53', '54', '55'))
            ,array('Name'=>'Visa','cardLength'=>array(13,16),'cardPrefix'=>array('4'))
            ,array('Name'=>'JCB','cardLength'=>array(16),'cardPrefix'=>array('3528', '3529', '353', '354', '355', '356', '357', '358'))
            ,array('Name'=>'Discover','cardLength'=>array(16),'cardPrefix'=>array('6011', '622126', '622127', '622128', '622129', '62213',
                                        '62214', '62215', '62216', '62217', '62218', '62219',
                                        '6222', '6223', '6224', '6225', '6226', '6227', '6228',
                                        '62290', '62291', '622920', '622921', '622922', '622923',
                                        '622924', '622925', '644', '645', '646', '647', '648',
                                        '649', '65'))
            ,array('Name'=>'Solo','cardLength'=>array(16, 18, 19),'cardPrefix'=>array('6334', '6767'))
            ,array('Name'=>'Unionpay','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('622126', '622127', '622128', '622129', '62213', '62214',
                                        '62215', '62216', '62217', '62218', '62219', '6222', '6223',
                                        '6224', '6225', '6226', '6227', '6228', '62290', '62291',
                                        '622920', '622921', '622922', '622923', '622924', '622925'))
            ,array('Name'=>'Diners Club','cardLength'=>array(14),'cardPrefix'=>array('300', '301', '302', '303', '304', '305', '36'))
            ,array('Name'=>'Diners Club US','cardLength'=>array(16),'cardPrefix'=>array('54', '55'))
            ,array('Name'=>'Diners Club Carte Blanche','cardLength'=>array(14),'cardPrefix'=>array('300','305'))
            ,array('Name'=>'Laser','cardLength'=>array(16, 17, 18, 19),'cardPrefix'=>array('6304', '6706', '6771', '6709'))
    );     

            $CCNumber= trim($CCNumber);
            $type='Unknown';
            foreach ($creditcardTypes as $card){
                if (! in_array(strlen($CCNumber),$card['cardLength'])) {
                    continue;
                }
                $prefixes = '/^('.implode('|',$card['cardPrefix']).')/';            
                if(preg_match($prefixes,$CCNumber) == 1 ){
                    $type= $card['Name'];
                    break;
                }
            }
            return $type;
        }

	
	public function check_reseller_agent($email, $rid)
	{
	       $qu1 = $this->db->query("Select * from tbl_reseller_agent where  agentEmail='".$email."' and ragentID='".$rid."'  ");
	       if($qu1->num_rows() >0 && $qu1->num_rows()==1 )
	       {
	           return true;
	       }else{
	           $qu2 = $this->db->query("Select * from tbl_reseller_agent where  ragentID!='".$rid."' and  agentEmail='".$email."' "); 
	            if($qu2->num_rows() >0 && $qu2->num_rows()==1 )
	            {
	               return false;
	           }else{
	               return true;
	           }
	           
	       }
	    
	}
	public function track_user($user_type, $user_email, $loginID){
	               $this->load->library('user_agent');
                    $log['browser'] = $this->agent->browser();
                     $ip =   getClientIpAddr();
                      $log['browser'] = $this->agent->browser();
                     $ip =   getClientIpAddr();
                 
                        $ch = curl_init(); 

                        // set url 
                        curl_setopt($ch, CURLOPT_URL, "http://ip-api.com/json/$ip"); 
                
                        //return the transfer as a string 
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
                
                        // $output contains the output string 
                        $output = curl_exec($ch);
                
                        // close curl resource to free up system resources 
                		curl_close($ch); 
                		
                       $user_data = json_decode($output, TRUE);
                      
                   $log_data['ip_address'] = $ip;
                   $log_data['City']      = $user_data['city'];
                   $log_data['State']     = $user_data['regionName'];
                   $log_data['Country']   = $user_data['country'];
                  
                   $log_data['ISP']   = $user_data['isp'];
                   $log_data['browser']   = $log['browser'];
                    $log_data['UserType'] = $user_type;
                    $log_data['emailAddress']  =$user_email; 
                  $log_data['UserID'] = $loginID;
                       $log_data['Login_at'] = date('Y-m-d H:i:s'); 
                     $result =  $this->general_model->insert_row('tbl_security_log', $log_data);
	}
	
	public function get_gateway($mID, $rID)
	{
	    $res=array();
	    $this->db->select('g.*');
	    $this->db->from('tbl_merchant_gateway g');
	    $this->db->join('tbl_merchant_data m', 'm.merchID=g.merchantID','INNER');
	    $this->db->where('g.merchantID', $mID);
	    $this->db->where('m.resellerID', $rID);
	    $qr = $this->db->get();
	    if($qr->num_rows()>0)
	    {
	     $res =  $qr->result_array();
    	}
	    return $res;
	
	}
	
	 public function insert_payterm($insert)
   {
               $pay_terms = array(
   array(
      'pt_name' => 'Due on Receipt' ,
      'pt_netTerm' => 0 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
  array(
      'pt_name' => 'Net 10' ,
      'pt_netTerm' => 10 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
                array(
      'pt_name' => 'Net 15' ,
      'pt_netTerm' => 15 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
                array(
      'pt_name' => 'Net 30' ,
      'pt_netTerm' => 30 ,
   		'merchantID'=>$insert,
      'date' => date('Y-m-d H:i:s')
   ),
);
   $this->db->insert_batch('tbl_payment_terms', $pay_terms); 
   
   }  
	
       public function insert_inv_number($mID)
   
   {
   
        		$inv_data['merchantID'] = $mID;
               	$inv_data['prefix']     = DEFAULT_INVOICE_PREFIX;
            	$inv_data['postfix']   = '10000';
                $inv_data['invoiceNo'] = DEFAULT_INVOICE_PREFIX.'-'.'10000';
   				$this->insert_row('tbl_merchant_invoices',$inv_data);
   
   }
	
	 	public function get_invoice_overview($invID)
        {
        $res=array();
        
               $this->db->select('count(merchantID) as merchant, pl.plan_name ');
        $this->db->from('tbl_reseller_invoice_item r');
        $this->db->join('plans pl','pl.plan_id=r.planID','inner');
        $this->db->where('r.resellerInvoice',$invID);
        $this->db->group_by('r.planID');
        $query = $this->db->get();
        if($query->num_rows() > 0)
        {
           $res = $query->result_array();
        
        }
        
        	return $res;	
        
        
        
        }
      	
      	
      	
      	public function template_data($con){
	
	   $res=array();
		$this->db->select('tp.*, typ.*');
		$this->db->from('tbl_eml_temp_reseller tp');
		$this->db->join('reseller_template_type typ','typ.resellertypeID=tp.templateType','inner');
	    $this->db->where($con);
		
	  	$query = $this->db->get();
 
		  return $res =  $query->result_array();
		
	} 
	
      	
      	
      	
	public  function send_mail_data($condition_mail,$merchant_name,$company,$pl_name,$pamount,$login_url,$toEmail,$pass,$rsName,$m_id)
	{
	    if(empty($toEmail) || !filter_var($toEmail, FILTER_VALIDATE_EMAIL)){
		      return false;
		}
	        $user_id = $condition_mail['resellerID'];
	       
	           $res =   $this->get_select_data('tbl_reseller',array('resellerEmail','resellerCompanyName','resellerfirstName','lastName','primaryContact'), array('resellerID'=>$user_id));
              
	          $view_data      = $this->get_row_data('tbl_eml_temp_reseller',$condition_mail);	
					  
							   
			 $reseller_name=	($res['resellerCompanyName'])?$res['resellerCompanyName']:$res['resellerfirstName'].''.$res['lastName'];	  
							  $subject = $view_data['emailSubject'];
							  $fromEmail = $view_data['fromEmail'];
						
 							$fromName = '';
					     	 $fromEmail=	$config_email =	$res['resellerEmail'];	
							  $in_link='';
							   $message = $view_data['message'];
								if($view_data['templateType'] == '1'){
									$fromEmail=	$view_data['fromEmail'];
								}
								if ($view_data['fromName'] != "") {
									$fromName = $view_data['fromName'];
								}

								if($view_data['templateType'] == 1){
									$rsName = $reseller_name;
								}
							
							   $subject = stripslashes(str_replace('{{reseller_company}}',$rsName ,$subject));
							   $subject = stripslashes(str_replace('{{reseller_name}}',$rsName ,$subject));
							   $subject = stripslashes(str_replace('{{reseller_email}}',$res['resellerEmail'] ,$subject));
							   $subject = stripslashes(str_replace('{{reseller_phone}}',$res['primaryContact'] ,$subject));

							   $message = stripslashes(str_replace('{{agent_name}}',$merchant_name ,$message));
							   $message = stripslashes(str_replace('{{agent_email}}',$toEmail ,$message));
							   $message = stripslashes(str_replace('{{agent_password}}',$pass ,$message));

							   
							  $message = stripslashes(str_replace('{{reseller_company}}', $rsName, $message ));
							   $message = stripslashes(str_replace('{{merchant_name}}',$merchant_name ,$message ));
							   $message = stripslashes(str_replace('{{company_name}}',$company ,$message));
							  
							   $message =	stripslashes(str_replace('{{plan_name}}',$pl_name, $message));
								$message = stripslashes(str_replace('{{plan_amount}}',($pamount)?('$'.number_format($pamount,2)):'0.00' ,$message )); 
							   
							   $message = stripslashes(str_replace('{{login_url}}',$login_url ,$message));
							   $message = stripslashes(str_replace('{{merchant_email}}',$toEmail ,$message ));
     						   $message = stripslashes(str_replace('{{merchant_password}}',$pass ,$message ));
     						 
							$message = stripslashes(str_replace('{{reseller_name}}',$reseller_name ,$message));
							$message = stripslashes(str_replace('{{reseller_email}}',$res['resellerEmail'] ,$message));
							$message = stripslashes(str_replace('{{reseller_phone}}',$res['primaryContact'] ,$message));
    
                  
    
							   $replyTo=$addBCC=$addCC='';
							    if($view_data['replyTo'] != ''){
									$replyTo = $view_data['replyTo'];
								}else{
									$replyTo = $config_email;
								}
							   $email_data          = array(
			                            'merchantID'=>$user_id,
										'customerID'=>$m_id, 
										'emailsendAt'=>date('Y-m-d H:i:s'),
										'emailSubject'=>$subject,
										'emailfrom'=>$config_email,
										'emailto'=>$toEmail,
										'emailcc'=>$addCC,
										'emailbcc'=>$addBCC,
										'emailreplyto'=>$replyTo,
										'emailMessage'=>$message,
		                                'mailType'=>'2'
										);
    
										$addCC = $view_data['addCC'];
										$addBCC = $view_data['addBCC'];
							
							    
								$mail_sent = $this->sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo,$fromName, $addCC, $addBCC);
								if($mail_sent)
								{
										$this->insert_row('tbl_template_data', $email_data); 
								}
								else
								{
									$email_data['mailStatus']=0;
										$this->insert_row('tbl_template_data', $email_data); 
								}
								
								
	                            
	
	
	}
	
      	
      	public function get_total_merchants($con)
      	{
      	      $res = 0;
      	      $this->db->select('mr.*');
      	      $this->db->from('tbl_merchant_data mr');
      	      $this->db->join('plans p','p.plan_id=mr.plan_id','inner');
      	      $this->db->where($con);
      	      $query = $this->db->get();
      	      if($query->num_rows()>0)
      	      $res = $query->num_rows();
      	      else
      	      $res ;
      	      return $res;
      	    
      	}
      	
    public function sendMailBySendgrid($toEmail,$subject,$fromEmail,$merchant_name,$message,$replyTo,$fromName = '', $addCC='', $addBCC=''){
    	if($fromName == ''){
    		if ($this->session->userdata('reseller_direct_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('reseller_direct_logged_in');
				$fromName = $data['login_info']['resellerCompanyName'];
			}
			if ($this->session->userdata('agent_direct_logged_in')) {
				$data['login_info']	= $this->session->userdata('agent_direct_logged_in');				
				$fromName =               $data['login_info']['agentName'];
				
			}
    	}
		$request_array = [
			"personalizations" => [
				[
				"to" => [
					[
					"email" => $toEmail
					]
				],
				"subject" => $subject
				]
			],
			"from" => [
				"email" => $fromEmail,
				"name" => $fromName
			],
			"reply_to" => [
				"email" => $replyTo
			],
			"content" => [
				[
				"type" => "text/html",
				"value" => $message
				]
			]
		];

		if(isset($addCC) && !empty($addCC)){
			$addCC = explode(';', $addCC);
	
			$toaddCCArrEmail = [];
			foreach ($addCC as $value) {
				$toaddCCArrEmail[]['email'] = trim($value);
			}
			$request_array['personalizations'][0]['cc'] = $toaddCCArrEmail;
		}
	
		if(isset($addBCC) && !empty($addBCC)){
			$addBCC = explode(';', $addBCC);
	
			$toaddBCCArrEmail = [];
			foreach ($addBCC as $value) {
				$toaddBCCArrEmail[]['email'] = trim($value);
			}
			$request_array['personalizations'][0]['bcc'] = $toaddBCCArrEmail;
		}

		// get api key for send grid
		$api_key = SENDGRID_APIKEY;
		$url = 'https://api.sendgrid.com/v3/mail/send';

		// set authorization header
		$headerArray = [
			'Authorization: Bearer '.$api_key,
			'Content-Type: application/json',
			'Accept: application/json'
		];

		$ch = curl_init($url);
		curl_setopt ($ch, CURLOPT_POST, true);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, json_encode($request_array));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, true);

		// add authorization header
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);

		$response = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		
		// parse header data from response
		$header_text = substr($response, 0, $header_size);
		// parse response body from curl response
		$body = substr($response, $header_size);

		$header = array();
		

		// if mail sent success
		if($httpcode == '202' || $httpcode == '200'){
			$success = true;
		}else{
			$success = false;
		}
		return $success;
	}
	
	/**
	 *  Login Throttling
	 */
	public function addLoginAttempt($ip, $post_data=null)
	{
		$base64_data = ($post_data) ? base64_encode(json_encode($post_data)) : null;
		$data = ['ip' => $ip, 'base64_data' => $base64_data, 'created_at' => date('Y-m-d H:i:s')];
		$this->db->insert('tbl_login_attempts', $data);
	}

	public function getLastLoginAttemptCount($ip, $seconds=300)
	{
		
		$this->db->select('count(id) as c');
		$this->db->from('tbl_login_attempts');
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-$seconds) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->group_by('ip');

		$data = $this->db->get()->row_array();
		
		return ($data ? $data['c'] : 0);
	}

	public function deleteLoginAttempt($ip)
	{
		// Deleting past an hour 
		$this->db->where('ip', $ip);
		$this->db->where('created_at > ', date('Y-m-d H:i:s', time()-3600) );
		$this->db->where('created_at < ', date('Y-m-d H:i:s') );
		$this->db->delete('tbl_login_attempts');
	}
	/**
	 * 
	 * End Login Throttling
	 */
  
	
	// Add Payment Action Log
	public function addPaymentLog($gateway_id, $access_url=null, $request=null, $response=null, $status=null)
	{
		
		$data = [];
		$data['payment_gateway_id'] = $gateway_id;
		$data['access_url'] = $access_url;
		$data['access_block'] = 'direct-reseller';
		$data['request_payload'] = (is_array($request) ? json_encode($request): $request) ;
		$data['response_payload'] = (is_array($response) ? json_encode($response): $response) ;
		$this->db->insert('tbl_payment_logs', $data);
	}
      	
}

 