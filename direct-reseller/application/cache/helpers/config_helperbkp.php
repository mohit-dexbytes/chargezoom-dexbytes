<?php


function template_variable()
{
	$template = array(
		'name'              => 'ChargeZoom',
		'version'           => '0.1',
		'author'            => 'Ecrubit',
		'robots'            => 'noindex, nofollow',
		'title'             => 'ChargeZoom - Automate Your QuickBook Payments',
		'description'       => 'ChargeZoom automates your quickbook payments and help you focus on your business.',
		// true                     enable page preloader
		// false                    disable page preloader
		'page_preloader'    => true,
		// true                     enable main menu auto scrolling when opening a submenu
		// false                    disable main menu auto scrolling when opening a submenu
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['PHP_SELF'])
	);

	return $template;	
}


function primary_nav()
{
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('home/index')
		),
		array(
			'name'  => 'Dashboard (First Login)',
			'icon'  => 'gi gi-right_arrow',
			'url'   =>  base_url('home/dashboard_first_login')
		),
		array(
			'name'  => 'Gateway',
			'icon'  => 'gi gi-lock',
			'sub'   => array(
				array(
					'name'  => 'Sale',
					'url'   => '#'
				),
				array(
					'name'  => 'Authorize',
					'url'   => '#'
				),
				array(
					'name'  => 'Refund',
					'url'   => '#'
				)
			)
		),
		array(
			'name'  => 'Customers',
			'icon'  => 'fa fa-users',
			'url'   => base_url('home/customer')
		),
		array(
			'name'  => 'Subscriptions',
			'icon'  => 'gi gi-refresh',
			'url'   => '#'
		),
		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => 'page_invoices.php'
				),
				array(
					'name'  => 'Payments',
					'url'   => '#'
				),
				array(
					'name'  => 'Refunds',
					'url'   => '#'
				),
				array(
					'name'  => 'Credits',
					'url'   => '#'
				)
			)
		),
		array(
			'name'  => 'Plans & Products',
			'icon'  => 'gi gi-calendar',
			'url'   => '#'
		),
		array(
			'name'  => 'Reports',
			'icon'  => 'gi gi-signal',
			'url'   => '#'
		),
		array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			'sub'   => array(
			       array(
                'name'  => 'Accounting Software',
                'url'   =>  base_url('home/accounting_software')
                ),
				array(
					'name'  => 'Admin Users',
					'url'   => '#'
				),
				array(
					'name'  => 'Admin Roles',
					'url'   => '#'
				),
				array(
					'name'  => 'Customer Portal',
					'url'   => '#'
				),
				array(
					'name'  => 'Payment Methods',
					'url'   => '#'
				),
				array(
					'name'  => 'Taxes',
					'url'   => '#'
				),
				array(
					'name'  => 'Email Personalization',
					'url'   => '#'
				),
				array(
					'name'  => 'General Settings',
					'url'   => '#'
				),
				array(
					'name'  => 'Merchant Gateway',
					'url'   => '#'
				),
				array(
					'name'  => 'API',
					'url'   => '#'
				),
				array(
					'name'  => 'Application Settings',
					'url'   => '#'
				)
			)
		)
	);
	return $primary_nav;	
}


?>