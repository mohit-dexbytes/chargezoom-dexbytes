/*
 *  Document   : login.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Login page
 */
 
 $(function(){ Wizards.init(); });
 
var Wizards = function() {

    // Function for switching form views (login, reminder and register forms)
    

    return {
        init: function() {
            /* Switch Login, Reminder and Register form views */
            
             
            /*  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Login form - Initialize Validation */
            $('#progress-wizard').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    e.closest('.form-group').removeClass('has-success has-error');
                    e.closest('.help-block').remove();
                },
                rules: {
                    'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'companyAddress1': {
                        required: true,
                        minlength: 5
                    },
					  'companyCountry': {
                        required: true,
                         minlength: 3
                    },
                    'companyState': {
                        required: true,
                        minlength: 2
                    },
					  'companyCity': {
                        required: true,
                         minlength: 3
                    },
					  'companyContact': {
                        required: true,
                         minlength: 3
                    },
                    'zipCode': {
                        required: true,
                        minlength: 5
                    },
                },
            
            });

          
          
        }
    };
}();