<?php


function template_variable()
{  
    
  
	$template = array(
		'name'              => 'Chargezoom',
		'version'           => 'ver. 2.23',
		'author'            => 'Chargezoom',
		'robots'            => 'noindex, nofollow',
		'title'             => 'Chargezoom - Cloud Accounting Automation Software',
		'description'       => 'Chargezoom automates your payment processing and accounting reconciliation so you can focus on your business.',
		// true                     enable page preloader
		// false                    disable page preloader
		'page_preloader'    => true,
		// true                     enable main menu auto scrolling when opening a submenu
		// false                    disable main menu auto scrolling when opening a submenu
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['REQUEST_URI'])
	);
 
	return $template;	
}


function primary_nav()
{ 
  $CI = & get_instance(); 

if($CI->session->userdata('agent_logged_in'))
 {
 $user_info = $CI->session->userdata('agent_logged_in');

}
 
if($CI->session->userdata('reseller_logged_in'))
 {
 $user_info = $CI->session->userdata('reseller_logged_in');

}    
     
 
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('Reseller_panel/index'),
			'page_name'=> 'index'
		),
	
		array(
			'name'  => 'Merchant Management',
			'icon'  => 'fa fa-users',
			 'sub'  => array(
			      array(
			'name'  => 'Merchants',
			'url'   => base_url('Reseller_panel/merchant_list'),
			'page_name'=> 'merchant_list'
	        	),
		     array(
			'name'  => 'Disabled Merchants',
			'url'   => base_url('Reseller_panel/disable_merchant_list'),
			'page_name'=> 'disable_merchant_list'
	    	)
	    	)
		),
		array(
             'name' => 'Agent Management',
             'icon' => 'fa fa-user-secret',
             'sub' => array(
                array(
                    'name' => 'Agents',
                    'url'  => base_url('SettingAgent/reseller_agents'),
                    'page_name' => 'reseller_agents'
                    ),
                array(
                    'name' => 'Disabled Agents',
                    'url'  => base_url('SettingAgent/disable_agents'),
                    'page_name' => 'disable_agents'
                    )
             )
      ),
	array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			'sub'   => array(
			     array(
                    'name' => 'General Settings',
                    'url'  => base_url('SettingConfig/general_setting'),
                    'page_name' => 'general_setting'
                    ),
			     	array(
            			'name'  => 'Plans',
            			'url'   => base_url('Plan/reseller_plans'),
            			'page_name'=> 'reseller_plans'
            		), 
            		array(
                'name'  => 'Email Templates',
                'url'   =>  base_url('Settingmail/email_template'),
                'page_name'=> 'email_template'
                ),
                array(
					'name'  => 'API',
					'url'   => base_url('SettingConfig/apikey'),
					'page_name'=> 'apikey'
	         	)
			    )
			 ),  
		
	);


 
	return $primary_nav;	
}



function primary_agent_nav()
{  
   
   
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('agent/home/index'),
			'page_name'=> 'index'
		),
	
		array(
			'name'  => 'Merchant Management',
			'icon'  => 'fa fa-users',
			 'sub'  => array(
			      array(
			'name'  => 'Merchants',
			'url'   => base_url('agent/home/merchant_list'),
			'page_name'=> 'merchant_list'
	        	),
		     array(
			'name'  => 'Disabled Merchants',
			'url'   => base_url('agent/home/disable_merchant_list'),
			'page_name'=> 'disable_merchant_list'
	    	)
	    	)
		),
		
	
	);

	return $primary_nav;	
}


 
  function get_chat_script()
  {
         $CI = & get_instance(); 
 	  	if($CI->session->userdata('reseller_logged_in')){
		$rID 				=  $CI->session->userdata('reseller_logged_in')['resellerID'];
		}
		if($CI->session->userdata('agent_logged_in')){
		$rID  	=  $CI->session->userdata('agent_logged_in')['resellerIndexID'];
	
		}	
      
    $rs =array();
      $script=array();
       if($rID)
       {
       $CI->load->model('general_model');
       $rs  =  $CI->general_model->get_select_data('tbl_reseller', array('ProfileURL', 'Chat'),array('resellerID'=> $rID));  
       }
    
    if(!empty($rs)){  
        $script   = $rs;  
    }
    
    return $script;
    
  } 

