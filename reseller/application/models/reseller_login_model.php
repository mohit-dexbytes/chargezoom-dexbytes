<?php
Class Reseller_Login_model extends CI_Model
{
	private $tbl_user = 'qb_test_customer'; // user table name

	/**
     * Constructor of the class
     *
     */
	function __construct() 
	{ 
		parent::__construct(); 
		define('FORGET_PASSWORD_USER_TYPE', 'Reseller'); 
	}

	function Login()
	{
		parent::Model();
	}

    /**
     * Get user login
     *
     * @param $email
     * @param $password
     * @param $type
     * @return array
     */
    public function get_user_login($email, $password, $type)
    {
        $res = array();

        if ($type == 1) {
            $this->db->select('*');
            $this->db->from('tbl_reseller');
            $this->db->where('resellerEmail',$email);
            $this->db->where('isEnable', '1');
            $this->db->where('isDelete', '0');

            $query = $this->db->get();

            if ($query->num_rows() > 0) {
                $res                = $query->row_array();
                $res['login_type']  = 'RESELLER';

                if ($res['resellerPasswordNew'] !== null) {
                    if (!password_verify($password, $res['resellerPasswordNew'])) {
                        return [];
                    }
                } else {
                    if (md5($password) !== $res['resellerPassword']) {
                        return [];
                    }
                }

                $this->db->where('resellerID', $res['resellerID']);
                $this->db->update('tbl_reseller', array('is_logged_in' => '1'));
            }
	   } else {
           $this->db->select('ag.*,r.isSuspend');
           $this->db->from('tbl_reseller_agent ag');
           $this->db->join('tbl_reseller r','ag.resellerIndexID=r.resellerID','inner');
           $this->db->where('ag.agentEmail',$email);
           $this->db->where('ag.isEnable', '1');
		   $this->db->where('r.isEnable', '1');
           $this->db->where('r.isDelete', '0');

           $query = $this->db->get();

           if ($query->num_rows() > 0) {
               $res = $query->row_array();

               if ($res['agentPasswordNew'] !== null) {
                   if (!password_verify($password, $res['agentPasswordNew'])) {
                       return [];
                   }
               } else {
                   if (md5($password) !== $res['agentPassword']) {
                       return [];
                   }
               }

               $this->db->where('ragentID', $res['ragentID']);
               $this->db->update('tbl_reseller_agent', array('is_logged_in' => '1'));

               $res['login_type'] ='AGENT';
           }
	   }  
	   return $res;
   }  
	
	 
	function user_logout()
	{ 
		$this->db->trans_start();
		$session_data = $this->session->userdata('reseller_logged_in'); 
		$this->db->where('resellerID', $session_data['resellerID']); 
		$this->db->update('tbl_reseller',array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('reseller_logged_in'); 
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}
	
	function agent_logout()
	{ 
		$this->db->trans_start();
		$session_data = $this->session->userdata('agent_logged_in'); 
		$this->db->where('ragentID', $session_data['ragentID']); 
		$this->db->update('tbl_reseller_agent',array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('agent_logged_in'); 
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}
	
	//Forgot Password 
	
	function temp_reset_password($code,$email)
	{
        $passBcrypt = password_hash($code, PASSWORD_BCRYPT);

		$this->db->where('resellerEmail', $email);
	    $this->db->set('resellerPasswordNew', $passBcrypt);
	    if($this->db->update('tbl_reseller'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// Admin Change Password
	
	function savenewpass($id , $password)
	{
		if (isset($id) && $id != '')
		{
		 $this->db->where('resellerID', $id);
		 $this->db->set('resellerPasswordNew', password_hash($password, PASSWORD_BCRYPT));
		 $query = $this->db->update('tbl_reseller');
		 
		 	if ($query) {
				return true;
		 	}
		 	else{
				return false;
		 	}
		}
	 } 
	 
	function login_as_mecrchant($merchID)
	{   
	   $this->db->where('merchID', $merchID);
		$this->db->update('tbl_merchant_data', array(
			'is_logged_in' => '1'
		));
		return true;
	}
	 
}