<?php
class Reseller_panel_model extends CI_Model
{

  function __contruct()
  {
    parent::Model();
    $this->load->database();
  }

  function insert_row($table_name, $array, $skipKeys=['message'])
  {

    $dataToInsert = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToInsert[$key] = strip_tags($value);
			else
				$dataToInsert[$key] = $value;
		} 

		if($this->db->insert($table_name,$dataToInsert))
      return $this->db->insert_id();
    else
      return false;
  }

  function update_row($table_name, $reg_no, $array)
  {
    $this->db->where('reg_no', $reg_no);
    if ($this->db->update($table_name, $array))
      return true;
    else
      return false;
  }

  public function check_existing_user($tbl, $array)
  {
    $this->db->select('*');
    $this->db->from($tbl);
    if ($array != '')
      $this->db->where($array);

    $query = $this->db->get();
    if ($query->num_rows() > 0)
      return true;

    else
      return false;
  }
  public function get_select_data($table, $clm, $con)
  {

    $data = array();

    $columns = implode(',', $clm);

    $query = $this->db->select($columns)->from($table)->where($con)->get();

    if ($query->num_rows() > 0) {
      $data = $query->row_array();
      return $data;
    } else {
      return $data;
    }
  }
  function update_row_data($table_name, $condition, $array, $skipKeys=['message'])
  {
    $dataToUpdate = [];
		foreach ($array as $key => $value) {
			if(!in_array($key, $skipKeys))
				$dataToUpdate[$key] = strip_tags($value);
			else
				$dataToUpdate[$key] = $value;
		}

    $this->db->where($condition);
    $trt = $this->db->update($table_name, $dataToUpdate);
    if ($trt)
      return true;
    else
      return false;
  }

  public function get_row_data($table, $con)
  {

    $data = array();

    $query = $this->db->select('*')->from($table)->where($con)->get();

    if ($query->num_rows() > 0) {
      $data = $query->row_array();
      return $data;
    } else {
      return $data;
    }
  }

  public function get_table_data($table, $con)
  {
    $data = array();
    
    $this->db->select('*');
    $this->db->from($table);
    if (!empty($con)) {
      $this->db->where($con);
    }

    $query  = $this->db->get();
    if ($query->num_rows() > 0) {
      $data = $query->result_array();
      return $data;
    } else {
      return $data;
    }
  }



  ///---------  to get the email history data --------///

  public function get_email_history($con)
  {

    $data = array();

    $this->db->select('*');
    $this->db->from('tbl_template_data');
    $this->db->where($con);
    $query = $this->db->get();

    if ($query->num_rows() > 0) {
      $data = $query->result_array();
      return $data;
    } else {
      return $data;
    }
  }





  public function get_num_rows($table, $con)
  {

    $num = 0;

    $query = $this->db->select('*')->from($table)->where($con)->get();
    $num = $query->num_rows();
    return $num;
  }



  public function delete_row_data($table, $condition)
  {

    $this->db->where($condition);
    $this->db->delete($table);
    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }



  public function merchant_customer_count($merchantID)
  {
    $count = 0;
    $data = array();
    $query = $this->db->query("Select * from app_integration_setting where merchantID='" . $merchantID . "' ");
    $row   = $query->row_array();
    if (!empty($row)) {
      if ($row['appIntegration'] == '1') {

        $qur =  $this->db->query("select count(Customer_ListID) as customer_count from QBO_custom_customer  where QBO_custom_customer.merchantID='" . $merchantID . "'  ");

        $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
		
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and    tr.merchantID="' . $merchantID . '" ');


        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
		(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")  and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '"  ');



      }
      if ($row['appIntegration'] == '2') {
        $qur =     $this->db->query("select count(qb_cust_id) as customer_count from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID='" . $merchantID . "' and  qb_test_customer.customerStatus=1 ");
        $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
		where mr1.merchID = tr.merchantID and   (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")     and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '" ');

        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id 
		inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID where 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and     tr.merchantID="' . $merchantID . '"  ');
      }
      if ($row['appIntegration'] == '3') {
        $qur =    $this->db->query("select count(Freshbooks_custom_customer.Customer_ListID) as customer_count from Freshbooks_custom_customer inner join tbl_freshbooks on Freshbooks_custom_customer.merchantID=tbl_freshbooks.merchantID where Freshbooks_custom_customer.merchantID='" . $merchantID . "'  ");

        $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or   tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3")     and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and    tr.merchantID="' . $merchantID . '" ');

        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and  (tr.transaction_user_status !="3")   and  
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and   tr.merchantID="' . $merchantID . '"  ');
      }
      if ($row['appIntegration'] == '4') {
        $qur =     $this->db->query("select count(Xero_custom_customer.Customer_ListID) as customer_count from Xero_custom_customer inner join  tbl_xero_token  on Xero_custom_customer.merchantID=tbl_xero_token.merchantID where Xero_custom_customer.merchantID='" . $merchantID . "'  ");
        $rquery = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and  (tr.transaction_user_status !="3")   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  tr.merchantID="' . $merchantID . '"  ');



        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID where 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( tr.transaction_user_status !="3")   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and  tr.merchantID="' . $merchantID . '"  ');
      }

      if ($row['appIntegration'] == '5') {
        $qur =     $this->db->query("select count(chargezoom_test_customer.ListID) as customer_count from chargezoom_test_customer 
            inner join tbl_company on chargezoom_test_customer.companyID=tbl_company.id where tbl_company.merchantID='" . $merchantID . "' and  chargezoom_test_customer.customerStatus=1 ");
        $rquery = $this->db->query('   select sum(tr.transactionAmount) as total from customer_transaction tr 
	
		inner join tbl_merchant_data mr1 on tr.merchantID=mr1.merchID 
		where mr1.merchID = tr.merchantID and   (tr.transactionType ="sale" or  tr.transactionType ="Offline Payment" or  tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")     and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) and  tr.merchantID="' . $merchantID . '" ');

        $rfquery = $this->db->query('select sum(tr.transactionAmount) as rm_amount from customer_transaction tr 
		inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id 
		inner join tbl_merchant_data mr1 on cmp.merchantID=mr1.merchID where 
			(UPPER(tr.transactionType) = "REFUND" OR UPPER(tr.transactionType) = "PAY_REFUND" OR UPPER(tr.transactionType) = "STRIPE_REFUND" OR UPPER(tr.transactionType) = "CREDIT" OR UPPER(tr.transactionType) = "PAYPAL_REFUND" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and (tr.transaction_user_status !="3")   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)  and     tr.merchantID="' . $merchantID . '"  ');
      }

      if ($qur->num_rows() > 0) {
        $data['total'] = $rquery->row_array()['total'];
        $data['count'] =  $qur->row_array()['customer_count'];

        $data['refund'] =  $rfquery->row_array()['rm_amount'];
      } else {
        $data['total'] = 0;
        $data['count'] = 0;
        $data['refund'] = 0;
      }
    }
    return  $data;
  }






  public function get_data_admin_merchant($resID, $agentID = '')
  {
    $count = 0;
    $res = array();
    $this->db->select(' mr.*,p.plan_name,ag.agentName, ag.previledge, pr.merchantPortalURL,(SELECT fpl.friendlyname FROM plan_friendlyname fpl WHERE fpl.plan_id = mr.plan_id and mr.resellerID = fpl.reseller_id) AS friendlyname');

    $this->db->from('tbl_merchant_data mr');
    $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
    $this->db->join('tbl_reseller_agent ag', 'ag.ragentID = mr.agentID', 'left');
    $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'LEFT');
    $this->db->join('Config_merchant_portal pr', 'rs.resellerID = pr.resellerID', 'LEFT');
    $this->db->where('mr.isDelete = "0" ');
    $this->db->where('mr.isEnable = "1" ');
    $this->db->where('mr.resellerID', $resID);
    if ($agentID != '')
      $this->db->where('mr.agentID', $agentID);
    $this->db->group_by('mr.merchID');
    $query = $this->db->get();
    if ($query->num_rows()) {

      $res1 = $query->result_array();

      foreach ($res1 as $reus) {


        $tdata = $this->merchant_customer_count($reus['merchID']);

        $customer_count = $total_amount = $refund_amount = 0;

        if ($tdata) {
          $customer_count = ($tdata['count'] ? $tdata['count'] : 0);
          $total_amount = ($tdata['total'] ? $tdata['total'] : 0);
          $refund_amount = ($tdata['refund'] ? $tdata['refund'] : 0);
        }

        $reus['customer_count'] = $customer_count;
        $reus['total_amount'] = $total_amount;
        $reus['refund_amount'] = $refund_amount;

        $res[]  = $reus;
      }



    }



    return $res;
  }



  public function get_disable_data_admin_merchant($resID, $agentID = '')
  {
    $res = array();
    $count = 0;
    $ragentIDSet = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);
      if($agentData['userMerchantDisabled'] == 1 && $agentData['allMerchantDisabled'] == 0){
        $ragentIDSet =$ragentID; 
       
      }
      
    }
    
    $this->db->select(' mr.merchID, mr.companyName, mr.firstName,mr.lastName, mr.date_added, mr.updatedAt, p.plan_name, (SELECT fpl.friendlyname FROM plan_friendlyname fpl WHERE fpl.plan_id = mr.plan_id limit 1 ) AS friendlyname');

    $this->db->from('tbl_merchant_data mr');
    $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
    $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'inner');
    $this->db->where('(mr.isEnable = 0 or  mr.isDelete = 1)');
    $this->db->where('mr.resellerID', $resID);
    if($ragentIDSet != ''){
      $this->db->where('mr.agentID', $ragentID);
     
    }
    

    $this->db->group_by('mr.merchID');
    $query = $this->db->get();

    if ($query->num_rows()) {

      $res1 = $query->result_array();

      foreach ($res1 as $reus) {
        $tdata = $this->merchant_customer_count($reus['merchID']);
        $reus['customer_count']  = (isset($tdata['count']) && $tdata['count'])?$tdata['count']:'0';
        $reus['total_amount']  = (isset($tdata['total']) && $tdata['total'])?$tdata['total']:'0';
        $reus['refund_amount']  = (isset($tdata['refund']) && $tdata['refund'])?$tdata['refund']:'0';

        $res[]  = $reus;
      }
    }



    return $res;
  }
  public function get_customers($merchantID)
  {
    #0 as payments to fasten query need rework
    $res = array();
    $row =  $this->get_row_data('app_integration_setting', array('merchantID' => $merchantID));
    
    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {

       $amountCount = 'select sum(tr.transactionAmount)
            from customer_transaction tr
            inner join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
           where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status ="1") or tr.transaction_user_status IS NULL)';

       

       $txnCount = 'select count(tr.id)
            from customer_transaction tr
            inner join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID
           where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
       ';

       $sql = 'SELECT cust.Customer_ListID as ListID, cust.companyID as companyID, cust.firstName as customerFirstName,cust.lastName as customerLastName, cust.companyName as customerCompany, cust.fullName as customerFullName, cust.userEmail as customerEmail, cust.phoneNumber as customerPhone, cust.createdAt as customerTime
       ,( '.$amountCount.' ) as Payment,( '.$txnCount.' ) as totalTxn
     from QBO_custom_customer cust where cust.merchantID = "'.$merchantID.'"  AND cust.customerStatus = "true"';

      

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {
      
       $amountCount = 'select sum(tr.transactionAmount)
            from customer_transaction tr
            inner join qb_test_customer cust1 on cust1.ListID=tr.customerListID  where tr.customerListID = cust.ListID  and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status="1") or tr.transaction_user_status IS NULL)';

        

       $txnCount = 'select count(tr.id)
            from customer_transaction tr
            inner join qb_test_customer cust1 on cust1.ListID=tr.customerListID  where tr.customerListID = cust.ListID and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") ';


       $sql = ' SELECT cust.ListID as ListID, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName,
     cust.companyName as customerCompany, cust.FullName as customerFullName, cust.Contact as customerEmail, cust.Phone as customerPhone,
     cust.IsActive as IsActive, cust.TimeCreated as customerTime, comp.* , 
     ( '.$amountCount.' ) as Payment,( '.$txnCount.' ) as totalTxn
      from qb_test_customer cust
     inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "' . $merchantID . '" and customerStatus="1" ';



    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {
      $amountCount = 'select sum(tr.transactionAmount)
            from customer_transaction tr
            inner join Freshbooks_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID  where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status ="1" ) or tr.transaction_user_status IS NULL)';

        
       $txnCount = 'select count(tr.id)
            from customer_transaction tr
            inner join Freshbooks_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID  where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")';

       $sql = 'SELECT cust.Customer_ListID as ListID, cust.companyID as companyID, cust.firstName as customerFirstName,cust.lastName as customerLastName, cust.companyName as customerCompany, cust.fullName as customerFullName, cust.userEmail as customerEmail, cust.phoneNumber as customerPhone, cust.updatedAt as customerTime,( '.$amountCount.' ) as Payment,
      ( '.$txnCount.' ) as totalTxn from Freshbooks_custom_customer cust where cust.merchantID = "'.$merchantID.'"  AND cust.customerStatus = "1"';



      
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {
       $amountCount = 'select sum(tr.transactionAmount)
            from customer_transaction tr
            inner join Xero_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID  where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status ="1" ) or tr.transaction_user_status IS NULL)';

        

       $txnCount = 'select count(tr.id)
            from customer_transaction tr
            inner join Xero_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID  where tr.customerListID = cust.Customer_ListID and cust1.merchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture"  or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")'; 

       $sql = 'SELECT cust.Customer_ListID as ListID, cust.companyID as companyID, cust.firstName as customerFirstName,cust.lastName as customerLastName, cust.companyName as customerCompany, cust.fullName as customerFullName, cust.userEmail as customerEmail, cust.phoneNumber as customerPhone, cust.createdAt as customerTime, ( '.$amountCount.' ) as Payment,
      ( '.$txnCount.' ) as totalTxn from Xero_custom_customer cust where cust.merchantID = "'.$merchantID.'"  AND cust.customerStatus = "1"';
      
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        $amountCount = 'select sum(tr.transactionAmount) 
          from customer_transaction tr
          inner join chargezoom_test_customer cust1 on cust1.ListID=tr.customerListID
         where tr.customerListID = cust.ListID and cust1.qbmerchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status ="1" ) or tr.transaction_user_status IS NULL)';

       

        $txnCount = 'select count(tr.id) 
          from customer_transaction tr
          inner join chargezoom_test_customer cust1 on cust1.ListID=tr.customerListID
         where tr.customerListID = cust.ListID and cust1.qbmerchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")';

         $sql = 'SELECT cust.ListID as ListID, cust.EditSequence, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
      cust.FullName as customerFullName, cust.Contact as customerEmail,cust.qb_status,cust.qbAction,  
      cust.Phone as customerPhone, cust.IsActive as IsActive,  (case when cust.IsActive="true" then "Done"  when cust.IsActive="Pending" then "In Sync" else "Done" end) as c_status,
      cust.TimeCreated as customerTime, comp.* , 
      ( '.$amountCount.' ) as Payment,
      ( '.$txnCount.' ) as totalTxn
      from chargezoom_test_customer cust 
      inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'" AND cust.qbmerchantID = "'.$merchantID.'"  AND cust.customerStatus = "1"';
    }else{
      $amountCount = 'select sum(tr.transactionAmount) 
          from customer_transaction tr
          inner join chargezoom_test_customer cust1 on cust1.ListID=tr.customerListID
         where tr.customerListID = cust.ListID and cust1.qbmerchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        and ( (tr.transaction_user_status ="1" ) or tr.transaction_user_status IS NULL)';

       $txnCount = 'select count(tr.id) 
          from customer_transaction tr
          inner join chargezoom_test_customer cust1 on cust1.ListID=tr.customerListID
         where tr.customerListID = cust.ListID and cust1.qbmerchantID="'.$merchantID.'" and tr.merchantID="'.$merchantID.'" and  (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" or  tr.transactionType ="auth" or  tr.transactionType ="stripe_auth" 
        or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
        tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200")';


       $sql = 'SELECT cust.ListID as ListID, cust.EditSequence, cust.companyID as companyID, cust.FirstName as customerFirstName,cust.LastName as customerLastName, cust.companyName as customerCompany, 
      cust.FullName as customerFullName, cust.Contact as customerEmail,cust.qb_status,cust.qbAction,  
      cust.Phone as customerPhone, cust.IsActive as IsActive,  (case when cust.IsActive="true" then "Done"  when cust.IsActive="Pending" then "In Sync" else "Done" end) as c_status,
      cust.TimeCreated as customerTime, comp.* , 
      ( '.$amountCount.' ) as Payment,
      ( '.$txnCount.' ) as totalTxn
       from chargezoom_test_customer cust 
      inner join tbl_company comp on comp.id=cust.companyID   where comp.merchantID = "'.$merchantID.'" AND cust.qbmerchantID = "'.$merchantID.'"  AND cust.customerStatus = "1"';
    }
    

    $query = $this->db->query($sql);
    if ($query->num_rows()) {

      $res = $query->result_array();
    }

    return $res;
  }


  public function get_transaction_data($merchantID,$type='')
  {
    $row =  $this->get_row_data('app_integration_setting', array('merchantID' => $merchantID));
    
    $today = date("Y-m-d H:i");
    $this->db->select('tr.*');
    $this->db->from('customer_transaction tr');

    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {
        
        $this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
       
        
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {

        $this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {

        $this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {

        $this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
       
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else{
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->select('crt.creditTransactionID');
        $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','left');
        $this->db->where("comp.merchantID ", $merchantID);
    }  
    
    
    $this->db->where("tr.merchantID ", $merchantID);
    if($type == 'paid'){
      $this->db->where_in("tr.transactionCode",[1,100,111,120,200]);
      $this->db->where_not_in("tr.transactionType",['refund','pay_refund','credit','stripe_refund','paypal_refund']);
    }elseif($type == 'refund'){
      if (isset($row['appIntegration']) && $row['appIntegration'] == '5'){
        $this->db->select('crt.creditTransactionID');
        $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','INNER');
      }else{
         $this->db->where_in("tr.transactionType",['refund','pay_refund','credit','stripe_refund','paypal_refund']);
      }
      $this->db->where_in("tr.transactionCode",[1,100,111,120,200]);
     
      
    }elseif($type == 'all'){
      if (isset($row['appIntegration']) && $row['appIntegration'] == '5'){
        $this->db->select('crt.creditTransactionID');
        $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','left');
      }
     
      
    }elseif($type == 'failed'){
      $this->db->where_not_in("tr.transactionCode",[1,100,111,120,200]);
    }else{

    }
    $this->db->order_by("tr.transactionDate", 'desc');

    $this->db->order_by("tr.id", 'desc');
    $this->db->group_by("tr.transactionID");

    $query = $this->db->get();


    if ($query->num_rows() > 0) {
      $res1= $query->result_array();
      return $res1;
    }
  }

  function merchant_by_id($merchantID)
  {
    $this->db->select('mr.*');

    $this->db->from('tbl_merchant_data mr');
    $this->db->where('mr.merchID', $merchantID);
    $query = $this->db->get();

    if ($query->num_rows() > 0)

      return $query->row();

    else

      return false;
  }

  function plan_by_id($planid)
  {

    $query = $this->db->query("SELECT * FROM `plans` pl  LEFT JOIN `plan_friendlyname` fpl ON pl.plan_id = fpl.plan_id  WHERE pl.plan_id = '" . $planid . "' ");
    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      return $res;
    } else {
      return false;
    }
  }

  public function reseller_plan_by_id($planid, $rsID)
  {

    $query = $this->db->query("SELECT * FROM `plans` pl  LEFT JOIN `plan_friendlyname` fpl ON pl.plan_id = fpl.plan_id AND fpl.reseller_id = '".$rsID."'  WHERE pl.plan_id = '" . $planid . "' ");
    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      return $res;
    } else {
      return false;
    }
  }

  public function get_data_reseller_dashboard($resellerID)
  {
    $res = array();


    $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
		where   (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status !="success") or tr.transaction_user_status IS NULL)   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete=0
              
            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ');


    $this->db->query('select sum(tr.transactionAmount) as refund from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
		where   
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		
		and ( (tr.transaction_user_status !="refund" or tr.transaction_user_status !="success") or tr.transaction_user_status IS NULL)   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete=0
              
            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID) ');


    $query = $this->db->query("SELECT count(*) as reseller_count , (select count(tbl_merchant_data.merchID) from tbl_merchant_data inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where  tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and tbl_merchant_data.resellerID='" . $resellerID . "'  ) as mr_count, 
		 (select count(*) from qb_test_customer INNER join tbl_company on tbl_company.id=qb_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and  qb_test_customer.customerStatus=1 and tbl_merchant_data.resellerID='" . $resellerID . "' ) as c_count, 
          (select count(*)  from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID  INNER join tbl_company on tbl_company.id=qb_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0  and qb_test_customer.customerStatus=1  and tbl_merchant_data.resellerID='" . $resellerID . "'  ) as inv_count, 
          (select sum(-AppliedAmount) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id  inner join tbl_merchant_data on tbl_company.merchantID=tbl_merchant_data.merchID inner join tbl_reseller on tbl_reseller.resellerID=tbl_merchant_data.resellerID where tbl_merchant_data.isDelete=0 and tbl_reseller.isDelete=0 and  qb_test_customer.customerStatus=1 and tbl_merchant_data.resellerID='" . $resellerID . "'   
		   ) as applied , 
		   (select sum(tr.transactionAmount)from tbl_customer_tansaction tr inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id  inner join tbl_merchant_data mr on cmp.merchantID=mr.merchID inner join tbl_reseller rs on mr.resellerID=rs.resellerID where mr.isDelete=0 and tbl_reseller.isDelete=0 and mr.resellerID='" . $resellerID . "' and   (tr.transactionType ='sale' or tr.transactionType ='Paypal_sale' or tr.transactionType ='Paypal_capture' or  tr.transactionType ='pay_sale' or  tr.transactionType ='prior_auth_capture' or tr.transactionType ='pay_capture' or  tr.transactionType ='capture' 
			or  tr.transactionType ='auth_capture' or tr.transactionType ='stripe_sale' or tr.transactionType ='stripe_capture') and  (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1' or tr.transactionCode='200') 
			and tr.transaction_user_status !='refund'  and tr.transactionID!=''  and cust.customerStatus='1'  and 
			MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as total_amount,
		
		(select sum(tr.transactionAmount) from tbl_customer_tansaction tr inner join qb_test_customer cust on cust.ListID=tr.customerListID inner join tbl_company cmp on cust.companyID=cmp.id inner join tbl_merchant_data mr on cmp.merchantID=mr.merchID inner join tbl_reseller rs on mr.resellerID=rs.resellerID where mr.isDelete=0 and tbl_reseller.isDelete=0 and mr.resellerID='" . $resellerID . "' and  cust.customerStatus='1' AND (tr.transactionType = 'refund' OR tr.transactionType = 'pay_refund' OR tr.transactionType = 'credit' ) and (tr.transactionCode ='100' or tr.transactionCode ='111' or tr.transactionCode='1' or tr.transactionCode='200') and tr.transaction_user_status !='refund' and tr.transactionID!=''   and 
		MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate)) as refund_amount
		
		 from `tbl_reseller` WHERE isDelete='0' ");
    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      return $res;
    } else {
      return $res;
    }
  }


  public function get_data_total_invoice($merchantID)
  {
    $res = array();

    $res = array();
    $row =  $this->get_row_data('app_integration_setting', array('merchantID' => $merchantID));
    
    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {
        $query = $this->db->query('select count(QBO_test_invoice.id) as invoice_count from QBO_test_invoice where  QBO_test_invoice.merchantID = "'.$merchantID.'" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {

        $query = $this->db->query('select count(qb_test_invoice.id) as invoice_count from qb_test_invoice where  qb_test_invoice.qb_inv_merchantID ="'.$merchantID.'" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {

      $query = $this->db->query('select count(txnID) as invoice_count from Freshbooks_test_invoice  where Freshbooks_test_invoice.merchantID =  "'.$merchantID.'" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {

      $query = $this->db->query('select count(xeroinvoiceID) as invoice_count from Xero_test_invoice where  Xero_test_invoice.merchantID="'.$merchantID.'" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        $query = $this->db->query('select count(chargezoom_test_invoice.TxnID) as invoice_count from chargezoom_test_invoice inner join chargezoom_test_customer on chargezoom_test_customer.ListID=chargezoom_test_invoice.Customer_ListID AND chargezoom_test_customer.qbmerchantID = "'.$merchantID.'" INNER join tbl_company on tbl_company.id=chargezoom_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID  where tbl_company.merchantID = "' . $merchantID . '" and customerStatus="1"');
    }else{
        $query = $this->db->query('select count(chargezoom_test_invoice.TxnID) as invoice_count from chargezoom_test_invoice inner join chargezoom_test_customer on chargezoom_test_customer.ListID=chargezoom_test_invoice.Customer_ListID AND chargezoom_test_customer.qbmerchantID = "'.$merchantID.'" INNER join tbl_company on tbl_company.id=chargezoom_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID  where tbl_company.merchantID = "' . $merchantID . '" and customerStatus="1"');
    }


    
    

    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      return $res;
    } else {
      return $res;
    }
  }

  public function get_data_total_amount($merchantID)
  {
    $res = array();
    $row =  $this->get_row_data('app_integration_setting', array('merchantID' => $merchantID));
    
    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {
        $query = $this->db->query('select sum(AppliedAmount + BalanceRemaining) as applied from QBO_test_invoice where  QBO_test_invoice.merchantID = "'.$merchantID.'" and userStatus !="1" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {

        $query = $this->db->query('select sum(-AppliedAmount + BalanceRemaining) as applied from qb_test_invoice where  qb_test_invoice.qb_inv_merchantID ="'.$merchantID.'" and userStatus!="cancel" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {

      $query = $this->db->query('select sum(AppliedAmount + BalanceRemaining)  as applied from Freshbooks_test_invoice  where Freshbooks_test_invoice.merchantID =  "'.$merchantID.'" and userStatus!="cancel" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {

      $query = $this->db->query('select sum(AppliedAmount+ BalanceRemaining) as applied from Xero_test_invoice where  Xero_test_invoice.merchantID="'.$merchantID.'"  and userStatus!="VOIDED" ');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        $query = $this->db->query('select sum(-AppliedAmount + BalanceRemaining) as applied from chargezoom_test_invoice inner join chargezoom_test_customer on chargezoom_test_customer.ListID=chargezoom_test_invoice.Customer_ListID AND chargezoom_test_customer.qbmerchantID = "'.$merchantID.'" INNER join tbl_company on tbl_company.id=chargezoom_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID  where tbl_company.merchantID = "' . $merchantID . '" and customerStatus="1" and userStatus != "cancel"');
    }else{
        $query = $this->db->query('select sum(-AppliedAmount + BalanceRemaining) as applied from chargezoom_test_invoice inner join chargezoom_test_customer on chargezoom_test_customer.ListID=chargezoom_test_invoice.Customer_ListID AND chargezoom_test_customer.qbmerchantID = "'.$merchantID.'" INNER join tbl_company on tbl_company.id=chargezoom_test_customer.companyID INNER JOIN tbl_merchant_data on tbl_merchant_data.merchID=tbl_company.merchantID  where tbl_company.merchantID = "' . $merchantID . '" and customerStatus="1" and userStatus != "cancel"');
    }
    

    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      
      return $res;
    } else {
      return $res;
    }
  }



  function get_serch_merchant($key, $resellerID)
  {


    $res = array();


    $this->db->select(' mr.*,p.plan_name, (select count(*) from qb_test_customer inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as customer_count,
		(select sum(-AppliedAmount) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID  and    MONTH(CURDATE()) = MONTH(qb_test_invoice.TimeModified) AND YEAR(CURDATE()) = YEAR(qb_test_invoice.TimeModified)  ) as applied,
		(select sum(BalanceRemaining) from qb_test_invoice inner join qb_test_customer on qb_test_customer.ListID=qb_test_invoice.Customer_ListID inner join tbl_company on qb_test_customer.companyID=tbl_company.id where tbl_company.merchantID=mr.merchID ) as balance');
    $this->db->from('tbl_merchant_data mr');
    $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
    $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'LEFT');
    $this->db->join('tbl_company cmp', 'mr.merchID = cmp.merchantID', 'LEFT');
    $this->db->join('qb_test_customer cust', 'cust.companyID = cmp.id', 'LEFT');
    $this->db->join('qb_test_invoice inv', 'cust.ListID = inv.Customer_ListID', 'LEFT');

    $this->db->where('mr.isDelete = 0');
    $this->db->where('mr.resellerID', $resellerID);
    $this->db->where("mr.firstName Like '%" . $key . "%' or  mr.lastName  Like  '%" . $key . "%' or mr.companyName Like '%" . $key . "%' or p.plan_name Like  '%" . $key . "%' ");
    $this->db->group_by('mr.merchID');
    $query = $this->db->get();

    if ($query->num_rows()) {

      $res = $query->result_array();
    }
    return $res;
  }


  function temp_reset_password($code, $email)
  {
		return false;

    $this->db->where('merchantEmail', $email);
    $this->db->set('merchantPassword', md5($code));
    if ($this->db->update('tbl_merchant_data')) {
      return true;
    } else {
      return false;
    }
  }

  // reseller Change Password

  function savenewpass($id, $password)
  {
		return false;

    if (isset($id) && $id != '') {
      $this->db->where('merchID', $id);
      $this->db->set('merchantPassword', md5($password));
      $query = $this->db->update('tbl_merchant_data');

      if ($query)
        return true;
      else
        return false;
    }
  }

  public function get_planfdn_data($plan)
  {

    $query = $this->db->query("SELECT * FROM `plans` pl  LEFT JOIN `plan_friendlyname` fpl ON pl.plan_id = fpl.plan_id AND pl.plan_id = $plan");
    if ($query->num_rows() > 0) {
      $res = $query->row_array();
      return $res;
    } else {
      return false;
    }
  }




  public function get_totalplan($rid)
  {
    $query = $this->db->query("SELECT merchID,(SELECT FORMAT(sum(BalanceRemaining),2) FROM `tbl_merchant_billing_invoice` WHERE merchantID = merchID AND DATE_FORMAT(DueDate,'%Y') = year(CURDATE()) AND DATE_FORMAT(DueDate,'%m') = month(CURDATE()) AND Type = 'Plan') as total_plan FROM tbl_merchant_data WHERE resellerID = '" . $rid . "' AND isDelete = 0");
    if ($query->num_rows() > 0) {
      $res = $query->result_array();
      return $res;
    } else {
      return false;
    }
  }

  public function get_totalservice($rid)
  {
    $query = $this->db->query("SELECT merchID,(SELECT FORMAT(sum(BalanceRemaining),2)   FROM `tbl_merchant_billing_invoice` WHERE merchantID = merchID AND DATE_FORMAT(DueDate,'%Y') = year(CURDATE()) AND DATE_FORMAT(DueDate,'%m') = month(CURDATE()) AND Type = 'Service Charge') as total_plan FROM tbl_merchant_data WHERE resellerID = '" . $rid . "' AND isDelete = 0");
    if ($query->num_rows() > 0) {
      $res = $query->result_array();
      return $res;
    } else {
      return false;
    }
  }


  public function get_friendly_plan_name($plan, $rsID)
  {
    $res = array();
    $ress = array();
    $today  = date('Y-m-d');
    $pl = explode(',', $plan);

    $query  = $this->db->select('pl.*')->from('plans pl')->where_in('pl.plan_id', $pl)->get();

    if ($query->num_rows() > 0) {
      $ress = $query->result_array();

      foreach ($ress as $r) {
        $q1 = $this->db->select('pf.friendlyname')->from('plan_friendlyname pf ')->where('pf.reseller_id', $rsID)->where('pf.plan_id', $r['plan_id'])->get();
        if ($q1->num_rows() > 0) {
          $fname = $q1->row_array()['friendlyname'];
        } else {
          $fname  = $r['plan_name'];
        }

        $r['friendlyname'] = $fname;

        $res[] = $r;
      }
    }
    return  $res;
  }

  public function chart_processing_volume($rID)
  {

    $sql = "SELECT Month(transactionDate) as Month,sum(transactionAmount) as volume FROM `customer_transaction` WHERE resellerID = '" . $rID . "' AND transactionDate >= CURDATE() - INTERVAL 1 YEAR GROUP BY Month(transactionDate)";

    $query = $this->db->query($sql);


    if ($query->num_rows() > 0) {
      return $query->result();
    } else {
      return null;
    }
  }



  public function get_rss_revenue($rid)
  {
    $m_datas = array();
    $m_datas1 = array();
    $rrrr = array();

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "AND mr.agentID = $ragentID";
    }



    $sql =   $this->db->query("
      Select  mr.resellerID, mr.merchID, mr.plan_id,p.subscriptionRetail,p.serviceFee,p.planType,   
      (Select count(*) from tbl_merchant_data mr1 left join app_integration_setting app on app.merchantID= mr1.merchID where mr1.resellerID='" . $rid . "' and plan_id=mr.plan_id and mr1.isEnable='1'and mr1.isDelete='0'
      and  YEAR(mr1.date_added) = YEAR(CURRENT_DATE()) and MONTH(mr1.date_added) = MONTH(CURRENT_DATE()))  as m_count
      from  tbl_merchant_data mr
      inner join plans p on p.plan_id=mr.plan_id 
      inner join tbl_reseller rs on rs.resellerID=mr.resellerID

      where  mr.isDelete='0' and mr.isEnable='1' and mr.resellerID='" . $rid . "' $queryAgent
      GROUP BY mr.resellerID, mr.plan_id");

    $m_datas = $sql->result_array();

    $final = 0;
    $totalamount = 0;
    $tier = 0;
    $new_merchant = 0;
    $sr_fee1 = 0;
    $cms = 0;
    $sr_fee = 0;
    $sub_fee = 0;
    $total_transaction = 0;

    if (!empty($m_datas)) {
      $new_merchant = 0;
      foreach ($m_datas as $md) {
        $refund = 0;

        $rev  = $this->get_reseller_payment_new($rid, $md['plan_id'], $date = '',$md['merchID']);
        $refund = $rev['refund'];
        $totalamount += ($rev['rev'] - $refund);

        $total_transaction += $this->get_reseller_transaction($rid, $md['plan_id']);
        
        $new_merchant = ($new_merchant + $md['m_count']);


        $final += $sr_fee;
      }
      
      $sr_fee1 = ($sr_fee1 * $cms) / 100;
      $sub_fee =  ($sub_fee * $cms) / 100;



      $m_datas1['ser_fee'] = sprintf('%0.2f', ($sr_fee1));
      $m_datas1['sub_fee'] = sprintf('%0.2f', ($sub_fee));
      $m_datas1['p_value'] = sprintf('%0.2f', ($sub_fee + $sr_fee1));
      $m_datas1['volume'] = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['tier_status'] = $total_transaction;
      $m_datas1['newMerchant'] = $new_merchant;
    } else {
      $m_datas1['ser_fee'] = sprintf('%0.2f', ($sr_fee1));
      $m_datas1['sub_fee'] = sprintf('%0.2f', ($sub_fee));
      $m_datas1['p_value'] = 0;
      $m_datas1['volume'] = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['tier_status'] = $total_transaction;
      $m_datas1['newMerchant'] = $new_merchant;
    }
    $month = date("M-Y", strtotime(date('Y-m')));
    $qn = $this->db->query("Select count(*) as m_count from tbl_merchant_data m1 inner join tbl_reseller rs1 on rs1.resellerID=m1.resellerID where m1.resellerID='" . $rid . "' and rs1.isDelete='0' and m1.isEnable='1'  and m1.isDelete='0' and date_format(m1.date_added,'%b-%Y') = '$month' ");

    if ($qn->num_rows() > 0)
      $new_merchant = intval($qn->row_array()['m_count']);
    else
      $new_merchant = 0;
    $m_datas1['newMerchant'] = $new_merchant;

    return $m_datas1;
  }




  public function get_agent_data($con)
  {
    $result = array();

    $this->db->select('*')->from('tbl_reseller_agent')->where($con);
    $query = $this->db->get();
    if ($query->num_rows() > 0) {
      $query_data = $query->result_array();

      foreach ($query_data as $i => $r_data) {
        $result[$i + 1] = $r_data;
      }
    }

    return $result;
  }




  public function chart_get_merchant($rid)
  {

    $m_datas = array();
    $m_datas1 = array();
    $rrrr = array();
    $customer = 0;
    $res = array();
    $months = array();

    $queryAgent = $queryAgent2 = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2){
        $queryAgent = "AND mr.agentID = $ragentID";
        $queryAgent2 = "AND m1.agentID = $ragentID";
      }
      
    }

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
    }
    $chart_arr = array();
    $res_reval = 0;
    $totalAnnualamount = 0;
    foreach ($months as $key => $month) {

      $sql =   $this->db->query("
      Select  mr.resellerID, mr.merchID, mr.plan_id,p.subscriptionRetail,p.serviceFee,p.planType,   
      (Select count(m1.merchID) from tbl_merchant_data m1 inner join tbl_reseller rs1 on rs1.resellerID=m1.resellerID  where m1.resellerID='" . $rid . "' ".$queryAgent2." and  m1.isEnable='1'   and m1.isDelete='0' and date_format(m1.date_added,'%b-%Y') = '$month')  as m_count
      from  tbl_merchant_data mr
      inner join plans p on p.plan_id=mr.plan_id 
      inner join tbl_reseller rs on rs.resellerID=mr.resellerID
      where  mr.isDelete='0' and mr.isEnable='1' and mr.resellerID='" . $rid . "' $queryAgent
      GROUP BY mr.resellerID, mr.plan_id");
      $m_datas = $sql->result_array();
      if($month == 'Oct-2020'){
      }
      $final = 0;
      $cms = 0;
      $totalamount = 0.00;
      $tier = 0;
      $new_merchant = 0;
      

      if (!empty($m_datas)) {

        $m_customer = 0;

        foreach ($m_datas as $md) {
          $refund = 0;

          
          $rev  = $this->getResellerDashboardCurrentMonthRevenue($rid, $md['plan_id'],$queryAgent,$month);

          $revCC  = $this->get_dashboard_CC_count($rid,$month);
          $revEC  = $this->get_dashboard_EC_count($rid,$month);
          
          $refund = $rev['refund'];
          $totalamount += $rev['rev'] - $refund;
          $new_merchant = (intval($md['m_count']));
        }

        
        $final = $totalamount;
        $m_datas1['volume'][]   = $totalamount;
        $m_datas1['revenue'][] = $final;
        $m_datas1['eCheck'][] = $revEC;
        $m_datas1['creditCard'][] = $revCC;
        $m_datas1['merchant'][]= $new_merchant;
        $m_datas1['month'][]      = $month;
        $m_datas1['customers'][] = $m_customer;
      } else {
        $m_datas1['volume'][] = $totalamount;
        $m_datas1['revenue'][] = $final;
        $m_datas1['eCheck'][] = 0;
        $m_datas1['creditCard'][] = 0;
        $m_datas1['merchant'][] = $new_merchant;
        $m_datas1['month'][] = $month;
        $m_datas1['customers'][] = 0;
      }
      

      $res_reval = $this->get_reseller_total_payment_new($rid,$queryAgent,$month);
      
      $refundAnnual = $res_reval['refund'];
      $totalAnnualamount += $res_reval['rev'] - $refundAnnual;
     
    }
    
    $res_amount = $totalAnnualamount;

    $m_datas1['total_payment'] = $res_amount;
    
    return $m_datas1;
  }


  public function get_month_revenue($rid, $month)
  {
    $sql =   $this->db->query("Select  mr.resellerID, t.*, 
         mr.plan_id,p.subscriptionRetail,p.serviceFee, p.planType, 
         (Select count(tbl_merchant_data.merchID) from tbl_merchant_data where resellerID=mr.resellerID and plan_id=mr.plan_id  and isDelete='0'  and mr.isEnable='1'  and date_format(date_added,'%b-%Y') = '$month')  as m_count 
         from  tbl_merchant_data mr 
        inner join plans p on p.plan_id=mr.plan_id 
        inner join tbl_reseller rs on rs.resellerID=mr.resellerID
        inner join tiers t on rs.tier_id=t.tier_id
        where mr.resellerID='$rid' and  mr.isDelete='0' and mr.isEnable='1' and date_format(date_added,'%b-%Y') = '$month' 
        GROUP BY mr.resellerID, mr.plan_id  ");
    $m_datas = $sql->result_array();
    $final = 0;
    $cms = 0;
    $totalamount = 0.00;
    $tier = 0;
    $new_merchant = 0;
    if (!empty($m_datas)) {
      foreach ($m_datas as $md) {
        $refund = 0;

        $rev  = $this->get_reseller_payment($rid, $md['plan_id'], $month);

        $refund = $rev['refund'];
        $totalamount += $rev['rev'] - $refund;
        if ($md['planType'] == 1) {

          $sr_fee = ($md['serviceFee'] / 100) * ($rev['rev'] - $refund) + $md['subscriptionRetail'];
        }
        if ($md['planType'] == 2) {
          $total_transaction = $this->get_reseller_transaction($rid, $md['plan_id'], $month);
          $sr_fee = ($md['serviceFee']) * $total_transaction  + $md['subscriptionRetail'];
        }

        $new_merchant += $md['m_count'];

        $final += $sr_fee;
      }

      if ($new_merchant <= $m_datas[0]['tier1_min_new_account'] && $new_merchant < $m_datas[0]['tier2_min_new_account']) {
        $cms =  $m_datas[0]['tier1_commission'];
        $tier = 1;
      }
      if ($new_merchant >= $m_datas[0]['tier2_min_new_account'] && $new_merchant < $m_datas[0]['tier3_min_new_account']) {
        $cms =  $m_datas[0]['tier2_commission'];
        $tier = 2;
      }
      if ($new_merchant >= $m_datas[0]['tier3_min_new_account']) {
        $cms =  $m_datas[0]['tier3_commission'];
        $tier = 3;
      }
      $final = ($final * $cms) / 100;

      $m_datas1['volume']           = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['merchant'] = $new_merchant;
      $m_datas1['month'] = $month;
    } else {
      $m_datas1['volume'] = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['merchant'] = $new_merchant;
      $m_datas1['month'] = $month;
    }

    return  $m_datas1;
  }

  public function get_reseller_payment($resellerID, $pln_id, $date = '')
  {
    $res = array();

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "mr.agentID = $ragentID AND ";
    }

    if ($date != '') {
      $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';
    } else {

      $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
    }

    $query1 = $this->db->query('select sum(tr.transactionAmount) as total from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id 
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
        left join chargezoom_test_customer cust5 on cust5.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
		left join tbl_merchant_data mr5 on mr5.merchID =cust5.qbmerchantID 
		where '. $queryAgent .'
		(tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") ) 
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
               (cust5.qbmerchantID=mr5.merchID or cust5.qbmerchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete=0 and mr.plan_id="' . $pln_id . '"
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID or  cust5.qbmerchantID=tr.merchantID) ' .
      $date);


    $query2 =   $this->db->query('select sum(tr.transactionAmount) as refund from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id 
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
         left join chargezoom_test_customer cust5 on cust5.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
			left join tbl_merchant_data mr5 on mr5.merchID =cust5.qbmerchantID 
		where  '. $queryAgent .'  
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		
		and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") )   
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
              (cust5.qbmerchantID=mr5.merchID or cust5.qbmerchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete=0  and mr.plan_id="' . $pln_id . '"
              
            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID or  cust5.qbmerchantID=tr.merchantID) ' . $date);


    if ($query1->num_rows() > 0) {
      $res['rev'] =  ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
      $res['rev'] = '0';
    }

    if ($query2->num_rows() > 0) {

      $res['refund'] =  ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
    } else {
      $res['refund'] = '0';
    }

    return  $res;
  }


  public function get_reseller_transaction($resellerID, $pln_id, $date = '')
  {
    $res = 0;



    if ($date != '') {
      $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';
    } else {

      $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
    }

    $query1 = $this->db->query('select count(tr.id) as total  from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id 
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
         left join chargezoom_test_customer cust5 on cust5.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
			left join tbl_merchant_data mr5 on mr5.merchID =cust5.qbmerchantID 
		where  
         ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
             (cust5.qbmerchantID=mr5.merchID or cust5.qbmerchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete="0" and mr.plan_id="' . $pln_id . '"
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID or  cust5.qbmerchantID=tr.merchantID) ' .
      $date);

    if ($query1->num_rows() > 0) {
      $res = $query1->row_array()['total'];
    } else {
      $res = '0';
    }


    return  $res;
  }



  public function get_reseller_merchant($rID, $agID)
  {
    $res = array();
   
    $query = $this->db->query(" Select mr.companyName,mr.merchID, ifnull(ag.agentName,'None') as agent 
        from tbl_merchant_data mr inner JOIN tbl_reseller r on 
        r.resellerID=mr.resellerID left JOIN tbl_reseller_agent ag on ag.ragentID=mr.agentID WHERE `mr`.`resellerID` = '$rID' AND `mr`.`agentID` = '$agID' AND `mr`.`isEnable` = 1 AND  `mr`.`isDelete` = 0");
    if ($query->num_rows() > 0) {
      $res = $query->result_array();
    }
    return  $res;
  }


  public function update_bulk_agent($resID, $ageID, $merchID)
  {
    if ($this->db->query("update tbl_merchant_data set agentID='" . $ageID . "' where merchID IN ($merchID)  "))
      return true;
    else
      return false;
  }

  public function  get_total_customer_count($resellerID)
  {

    $total_count = '';
    $c1 = $c2 = $c3 = $c4 = $c5 = 0;

    $qur1 =  $this->db->query("select count(qbc.Customer_ListID) as customer_count 
                                    from QBO_custom_customer qbc 
                                    INNER JOIN tbl_merchant_data  md ON md.merchID =   qbc.merchantID  
                                    INNER JOIN tbl_reseller tr
                                    ON  tr.resellerID = md.resellerID
                     where    md.isDelete=0 and  tr.isDelete=0 && ( MONTH(CURDATE()) = MONTH(qbc.createdAt) AND YEAR(CURDATE()) = YEAR(qbc.createdAt)
            ) and  tr.resellerID = '" . $resellerID . "' ");

    $qur2 =  $this->db->query("select count(*) as customer_count from qb_test_customer  qtc INNER JOIN tbl_merchant_data  md 
            
            ON md.merchID =   qtc.qbmerchantID INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
            
          where ( MONTH(CURDATE()) = MONTH(qtc.TimeCreated) AND YEAR(CURDATE()) = YEAR(qtc.TimeCreated)
          
        )  and tr.resellerID = '" . $resellerID . "' ");

    $qur3 =  $this->db->query("select count(*) as customer_count from Freshbooks_custom_customer fcc INNER JOIN tbl_merchant_data  md 
      
         ON md.merchID =   fcc.merchantID  INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
      
          where    md.isDelete=0 && ( MONTH(CURDATE()) = MONTH(fcc.updatedAt) AND YEAR(CURDATE()) = YEAR(fcc.updatedAt)
          
        )  and   tr.resellerID = '" . $resellerID . "' ");

    $qur4 =  $this->db->query("select count(*) as customer_count from Xero_custom_customer  xcc INNER JOIN tbl_merchant_data  md 
     
        ON  md.merchID =   xcc.merchantID  INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
     
          where   md.isDelete=0 && ( MONTH(CURDATE()) = MONTH(xcc.createdAt) AND YEAR(CURDATE()) = YEAR(xcc.createdAt)
          
        )  and   tr.resellerID = '" . $resellerID . "' ");

    $qur5 =  $this->db->query("select count(*) as customer_count from chargezoom_test_customer ctc INNER JOIN tbl_merchant_data  md 
          
         ON  md.merchID =   ctc.qbmerchantID INNER JOIN tbl_reseller tr  ON tr.resellerID = md.resellerID
          
          where     md.isDelete=0 && ( MONTH(CURDATE()) = MONTH(ctc.TimeCreated) AND YEAR(CURDATE()) = YEAR(ctc.TimeCreated)
          
        )  and  tr.resellerID = '" . $resellerID . "' ");

    if ($qur1->num_rows() > 0) {

      $c1 =  $qur1->row_array()['customer_count'];
    } else if ($qur2->num_rows() > 0) {

      $c2 =  $qur2->row_array()['customer_count'];
    } else if ($qur3->num_rows() > 0) {

      $c3 =  $qur3->row_array()['customer_count'];
    } else if ($qur4->num_rows() > 0) {

      $c4 =  $qur4->row_array()['customer_count'];
    } else if ($qur5->num_rows() > 0) {

      $c5 =  $qur5->row_array()['customer_count'];
    }

    $total_count = $c1 + $c2 + $c3 + $c4 + $c5;

    return $total_count;
  }




  public function  get_reseller_customer_count($resellerID, $month)
  {
    $c1 = $c2 = $c3 = $c4 = $c5 = 0;

    $total_count = 0;
    
    $queryAgent = $queryAgent2 = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2){
        $queryAgent = "AND tm.agentID = $ragentID";
        $queryAgent2 = "AND md.agentID = $ragentID";
      }
    }

    $query =  $this->db->query("select Group_concat(DISTINCT merchID SEPARATOR ',') as merch, Group_concat(DISTINCT appIntegration SEPARATOR ',') api  
                                    from tbl_merchant_data tm
                                 
                                    INNER JOIN tbl_reseller tr ON  tr.resellerID = tm.resellerID
                                    INNER JOIN app_integration_setting app ON  tm.merchID = app.merchantID
                     where    tm.isDelete=0 and tm.isEnable=1 and  tr.isDelete=0 $queryAgent
                     and  tr.resellerID = '" . $resellerID . "' Group By tm.resellerID ");

    if ($query->num_rows() > 0) {
      $res = $query->row_array();
    } else {
      $res = '0';
    }

    $c1 = $c2 = $c3 = $c4 = $c5 = 0;


    if ($res) {
      $appIntegration = explode(',', $res['api']);



      if (in_array('1', $appIntegration)) {

        $query =  $this->db->query("select count(qbc.Customer_ListID)  as customer_count 
                                      from QBO_custom_customer qbc 
                                      INNER JOIN tbl_merchant_data  md ON md.merchID =   qbc.merchantID  $queryAgent2
                                      INNER JOIN tbl_reseller tr ON  tr.resellerID = md.resellerID
                      where md.isDelete=0 and md.isEnable=1 and  tr.isDelete=0 and (date_format(qbc.createdAt,'%b-%Y')= '" . $month . "') and  qbc.merchantID  IN (" . $res['merch'] . ")  and  tr.resellerID = '" . $resellerID . "' ");

        $c1 =    $query->row_array()['customer_count'];
      }

      
      if (in_array('2', $appIntegration)) {

        $query =  $this->db->query("select count(*) as customer_count from qb_test_customer  qtc INNER JOIN tbl_merchant_data  md 
              ON md.merchID =   qtc.qbmerchantID  $queryAgent2 INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
              
            where    md.isDelete=0  and md.isEnable=1 && (  date_format(qtc.TimeCreated,'%b-%Y') = '$month'  )  and qtc.qbmerchantID IN (" . $res['merch'] . ")   and tr.resellerID = '" . $resellerID . "' ");

        $c2 =    $query->row_array()['customer_count'];
      }


      if (in_array('3', $appIntegration)) {

        $query =  $this->db->query("select count(*) as customer_count from Freshbooks_custom_customer fcc INNER JOIN tbl_merchant_data  md 
        
          ON md.merchID =   fcc.merchantID  $queryAgent2 INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
        
            where    md.isDelete=0  and md.isEnable=1  && (  date_format(fcc.updatedAt,'%b-%Y') = '$month'  ) and fcc.merchantID IN (" . $res['merch'] . ")  and   tr.resellerID = '" . $resellerID . "' ");


        $c3 =    $query->row_array()['customer_count'];
      }

      if (in_array('4', $appIntegration)) {

        $query =  $this->db->query("select count(*) as customer_count from Xero_custom_customer  xcc INNER JOIN tbl_merchant_data  md 
      
              ON  md.merchID =   xcc.merchantID  $queryAgent2 INNER JOIN tbl_reseller tr ON tr.resellerID = md.resellerID
      
              where   md.isEnable=1 and md.isDelete=0 && (  date_format(xcc.createdAt,'%b-%Y') = '$month'  ) and xcc.merchantID IN (" . $res['merch'] . ")    and   tr.resellerID = '" . $resellerID . "' ");


        $c4 =    $query->row_array()['customer_count'];
      }


      if (in_array('5', $appIntegration)) {

        $query =  $this->db->query("select count(*) as customer_count from chargezoom_test_customer ctc INNER JOIN tbl_merchant_data  md 
            
          ON  md.merchID =   ctc.qbmerchantID  $queryAgent2 INNER JOIN tbl_reseller tr  ON tr.resellerID = md.resellerID
            
          where  md.isEnable=1 and   md.isDelete=0 && (  date_format(ctc.TimeCreated,'%b-%Y') = '$month'  ) and ctc.qbmerchantID IN (" . $res['merch'] . ")  and  tr.resellerID = '" . $resellerID . "' ");

        $c5 =    $query->row_array()['customer_count'];
      }
    }
    $total_count =  $c1 + $c2 +  $c3 + $c4 + $c5;

    return $total_count;
  }


  public function calculate_plan_ratio($resellerID)
  {

    $res = array();
    $resultData = array();
    $plan_ratio = array();
    $plan_ratio1 = 0;
    $plan_ratio2 = 0;
    $plan_ratio3 = 0;
    $plan_ratio4 = 0;
    $this->db->select('plan_id');
    $this->db->from('tbl_reseller');
    $this->db->where('resellerID', $resellerID);

    $query =  $this->db->get();

    $results = $query->row_array();

    $resultData = explode(",", $results['plan_id']);

    $total = 0;

    foreach ($resultData as $plan_id) {

      $payment = 0;

      if ($this->session->userdata('agent_logged_in')) {
        $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
        $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);
      }

      $this->db->select("merchant_plan_type,count('m.*') as m_count", FALSE);
      $this->db->from('plans');
      $this->db->join('tbl_merchant_data m',  'm.plan_id = plans.plan_id', 'inner');
      $this->db->join('tbl_reseller r',  'r.resellerID = m.resellerID', 'inner');

      $this->db->where('plans.plan_id', $plan_id);
      $this->db->where('m.resellerID', $resellerID);
      $this->db->where('m.isDelete', 0);
      $this->db->where('m.isEnable', 1);
      if(isset($agentData) && $agentData['dashboardPermission'] == 2){
        $this->db->where('m.agentID', $ragentID);
      }
      
      $query =  $this->db->get();

      $res1 = $query->row_array();

      if ($res1['merchant_plan_type'] == 'SS') {

        $plan_ratio1  = $plan_ratio1 + $res1['m_count'];
      }
      if ($res1['merchant_plan_type'] == 'VT') {

        $plan_ratio2 = $plan_ratio2 + $res1['m_count'];
      }
      
      if ($res1['merchant_plan_type'] == 'AS') {

        $plan_ratio3 = $plan_ratio3 + $res1['m_count'];
      }
      if ($res1['merchant_plan_type'] == 'Free') {

        $plan_ratio4 = $plan_ratio4 + $res1['m_count'];
      }
    }

    $total = $plan_ratio1 + $plan_ratio2 + $plan_ratio3 + $plan_ratio4;

    $res['total']     = $total;
    $res['ss_amount'] = $plan_ratio1;
    $res['vt_amount'] = $plan_ratio2;
    $res['as_amount'] = $plan_ratio3;
    $res['free_amount'] = $plan_ratio4;

    return $res;
  }


  public function get_reseller_total_payment($resellerID)
  {
    $res = array();
    $date = '';

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID 	= $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2){
        $queryAgent = "mr.agentID = $ragentID AND";
      }
    }

    $query1 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as total from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id 
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
        left join chargezoom_test_customer cust5 on cust5.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
		left join tbl_merchant_data mr5 on mr5.merchID =cust5.qbmerchantID 
		where '. $queryAgent .' 
		(tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
		or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
		tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") ) 
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
               (cust5.qbmerchantID=mr5.merchID or cust5.qbmerchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '" and r.isDelete=0 
                  and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID or  cust5.qbmerchantID=tr.merchantID) ' .
      $date);


    $query2 =   $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
	    inner join tbl_reseller r on r.resellerID=tr.resellerID
	    inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
               inner join plans p on p.plan_id=mr.plan_id 
		left join QBO_custom_customer cust1 on cust1.Customer_ListID=tr.customerListID 
        left join Xero_custom_customer cust2 on cust2.Customer_ListID=tr.customerListID 
        left join Freshbooks_custom_customer cust3 on cust3.Customer_ListID=tr.customerListID 
        left join qb_test_customer cust4 on cust4.ListID=tr.customerListID 
         left join chargezoom_test_customer cust5 on cust5.ListID=tr.customerListID 
		left join tbl_merchant_data mr1 on mr1.merchID =cust1.merchantID
		left join tbl_merchant_data mr2 on mr1.merchID =cust2.merchantID 
        left join tbl_merchant_data mr3 on mr1.merchID =cust3.merchantID 
		left join tbl_merchant_data mr4 on mr4.merchID =cust4.qbmerchantID 
			left join tbl_merchant_data mr5 on mr5.merchID =cust5.qbmerchantID 
		where  '. $queryAgent .'   
		(tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
		and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
		
		and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") )   
        and ((cust1.merchantID=mr1.merchID or cust1.merchantID IS NULL) or 
             (cust2.merchantID=mr2.merchID or cust2.merchantID IS NULL) OR
             (cust3.merchantID=mr3.merchID or cust3.merchantID IS NULL) OR
              (cust5.qbmerchantID=mr5.merchID or cust5.qbmerchantID IS NULL) OR
             (cust4.qbmerchantID=mr4.merchID or cust4.qbmerchantID IS NULL) ) and tr.resellerID="' . $resellerID . '"  and r.isDelete=0 
              
            and (cust1.merchantID=tr.merchantID or cust2.merchantID=tr.merchantID or cust3.merchantID=tr.merchantID or  cust4.qbmerchantID=tr.merchantID or  cust5.qbmerchantID=tr.merchantID) ' . $date);


    if ($query1->num_rows() > 0) {
      $res['rev'] =  ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
      $res['rev'] = '0';
    }

    if ($query2->num_rows() > 0) {

      $res['refund'] =  ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
    } else {
      $res['refund'] = '0';
    }

    return  $res;
  }

  public function get_reseller_payment_new($resellerID, $pln_id, $date = '',$merchantID)
  {
    $res = array();

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "mr.agentID = $ragentID AND ";
    }

    if ($date != '') {
      $date = ' and  date_format(tr.transactionDate,"%b-%Y") = "' . $date . '"  ';
    } else {

      $date = ' and  MONTH(CURDATE()) = MONTH(tr.transactionDate) AND YEAR(CURDATE()) = YEAR(tr.transactionDate) ';
    }
   

    $Tcount1 = $this->getCountTotalByCondtionTable('QBO_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'total');

    $Tcount2 = $this->getCountTotalByCondtionTable('Xero_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'total');

    $Tcount3 = $this->getCountTotalByCondtionTable('Freshbooks_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'total');

    $Tcount4 = $this->getCountTotalByCondtionTable('qb_test_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'total');

    $Tcount5 = $this->getCountTotalByCondtionTable('chargezoom_test_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'total');

    $res['rev'] = $Tcount1 + $Tcount2 +$Tcount3 +$Tcount4 +$Tcount5;
    
    $Rcount1 = $this->getCountTotalByCondtionTable('QBO_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'refund');

    $Rcount2 = $this->getCountTotalByCondtionTable('Xero_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'refund');

    $Rcount3 = $this->getCountTotalByCondtionTable('Freshbooks_custom_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'refund');

    $Rcount4 = $this->getCountTotalByCondtionTable('qb_test_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'refund');

    $Rcount5 = $this->getCountTotalByCondtionTable('chargezoom_test_customer',$queryAgent,$resellerID,$pln_id,$date,$merchantID,'refund');

    $res['refund'] = $Rcount1 + $Rcount2 + $Rcount3 + $Rcount4 + $Rcount5;

    return  $res;
  } 



  public function getCountTotalByCondtionTable($Qcond,$queryAgent,$resellerID,$pln_id,$date,$merchantID,$type){
    $res = array();
    if($Qcond == 'qb_test_customer' || $Qcond == 'chargezoom_test_customer'){
      $Qcond = 'left join '. $Qcond .' cust1 on cust1.ListID=tr.customerListID';
      $merchntCond = 'cust1.qbmerchantID';
    }else{
      $Qcond = 'left join '. $Qcond .' cust1 on cust1.Customer_ListID=tr.customerListID';
      $merchntCond = 'cust1.merchantID';
    }
    /********* COUNT OF TRANSACTION OF RESELLER */
    if($type == 'total'){
      $query1 = $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as total from customer_transaction tr 
        inner join tbl_reseller r on r.resellerID=tr.resellerID
        inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
                 inner join plans p on p.plan_id=mr.plan_id 
       '. $Qcond .'    
      left join tbl_merchant_data mr1 on mr1.merchID ='. $merchntCond.'
      where '. $queryAgent .'
      (tr.transactionType ="sale" or tr.transactionType ="Paypal_sale" or tr.transactionType ="Paypal_capture" or  tr.transactionType ="Offline Payment"  or  tr.transactionType ="pay_sale" or  tr.transactionType ="prior_auth_capture" or tr.transactionType ="pay_capture" or  tr.transactionType ="capture" 
      or  tr.transactionType ="auth_capture" or tr.transactionType ="stripe_sale" or tr.transactionType ="stripe_capture") and  (tr.transactionCode ="100" or 
      tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
      and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") ) 
          and (('. $merchntCond.' = mr1.merchID or '. $merchntCond.' IS NULL)) and tr.resellerID="' . $resellerID . '"  and mr.resellerID="' . $resellerID . '" and r.isDelete=0 and mr.plan_id="' . $pln_id . '"
                    and ('. $merchntCond.'=tr.merchantID) ' .
        $date);
      if ($query1->num_rows() > 0) {
        $rev =  ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
      } else {
        $rev = '0';
      }
      return $rev;
    }
    /*********REFUND COUNT OF TRANSACTION OF RESELLER */
    if($type == 'refund'){
        $query2 =   $this->db->query('select COALESCE(SUM(tr.transactionAmount),0) as refund from customer_transaction tr 
          inner join tbl_reseller r on r.resellerID=tr.resellerID
          inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
                   inner join plans p on p.plan_id=mr.plan_id 
        '. $Qcond .' 

        left join tbl_merchant_data mr1 on mr1.merchID ='. $merchntCond.' 
        where  '. $queryAgent .'  
        (tr.transactionType = "refund" OR tr.transactionType = "pay_refund" OR tr.transactionType = "stripe_refund" OR tr.transactionType = "credit" )
        and (tr.transactionCode ="100" or tr.transactionCode ="111" or tr.transactionCode="1" or tr.transactionCode="200") 
        
        and ( (tr.transaction_user_status !="2" or tr.transaction_user_status !="3") )   
        and ('. $merchntCond.'=mr1.merchID or '. $merchntCond.' IS NULL) and tr.resellerID="' . $resellerID . '" and mr.resellerID="' . $resellerID . '"  and  r.isDelete=0  and mr.plan_id="' . $pln_id . '"
              
            and ('. $merchntCond.'=tr.merchantID) ' . $date);

      if ($query2->num_rows() > 0) {

        $refund =  ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
      } else {
        $refund = '0';
      }
      return $refund;
    }
    /*********TOTAL COUNT OF TRANSACTION OF RESELLER */
    if($type == 'transaction'){
        $query3 = $this->db->query('select count(*) as total  from customer_transaction tr 
          inner join tbl_reseller r on r.resellerID=tr.resellerID
          inner join tbl_merchant_data mr  on mr.merchID=tr.merchantID
                   inner join plans p on p.plan_id=mr.plan_id 
        '. $Qcond .'  
           
        left join tbl_merchant_data mr1 on mr1.merchID ='. $merchntCond.' 
        where  
             ('. $merchntCond.'=mr1.merchID or '. $merchntCond.' IS NULL) and tr.resellerID="' . $resellerID . '" and mr.resellerID="' . $resellerID . '" and r.isDelete="0" and mr.plan_id="' . $pln_id . '"
                      and ('. $merchntCond.'=tr.merchantID) ' .
          $date);

        if ($query3->num_rows() > 0) {
          return $query3->row_array()['total'];
        } else {
          return 0;
        }
    }
    return 0;
  }

  public function get_dashboard_count($rid)
  {
    $m_datas = array();
    $m_datas1 = array();
    $rrrr = array();

    $queryAgent = '';
    $queryAgentCount = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2){
        $queryAgent = "AND mr.agentID = $ragentID";
        $queryAgentCount = "AND agentID = $ragentID";
      }
        
    }
    $sql =   $this->db->query("
      Select  mr.resellerID, mr.merchID, mr.plan_id,p.subscriptionRetail,p.serviceFee,p.planType,   
      (Select count(*) from tbl_merchant_data where resellerID='" . $rid . "' ".$queryAgentCount." and plan_id=mr.plan_id and isDelete='0' and isEnable='1' and  YEAR(date_added) = YEAR(CURRENT_DATE()) and MONTH(date_added) = MONTH(CURRENT_DATE())  )  as m_count
      from  tbl_merchant_data mr
      inner join plans p on p.plan_id=mr.plan_id 
      inner join tbl_reseller rs on rs.resellerID=mr.resellerID

      where  mr.isDelete='0' and mr.isEnable='1' and mr.resellerID='" . $rid . "' $queryAgent
      GROUP BY mr.resellerID, mr.plan_id");

    $m_datas = $sql->result_array();
    $final = 0;
    $totalamount = 0;
    $tier = 0;
    $new_merchant = 0;
    $sr_fee1 = 0;
    $cms = 0;
    $sr_fee = 0;
    $sub_fee = 0;
    $total_transaction = 0;
    $month = date('M-Y');
    if (!empty($m_datas)) {
      $new_merchant = 0;
      foreach ($m_datas as $md) {

          $rev  = $this->getResellerDashboardCurrentMonthRevenue($rid, $md['plan_id'],$queryAgent,$month);

          $total_transaction +=  $this->get_reseller_month_transaction($rid, $md['plan_id']);
          
          $refund = $rev['refund'];
          $totalamount += ($rev['rev'] - $refund);
          
          $new_merchant = ($new_merchant + $md['m_count']);

      }
      
      $sr_fee1 = ($sr_fee1 * $cms) / 100;
      $sub_fee =  ($sub_fee * $cms) / 100;
      $m_datas1['ser_fee'] = sprintf('%0.2f', ($sr_fee1));
      $m_datas1['sub_fee'] = sprintf('%0.2f', ($sub_fee));
      $m_datas1['p_value'] = sprintf('%0.2f', ($sub_fee + $sr_fee1));
      $m_datas1['volume'] = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['tier_status'] = $total_transaction;
      $m_datas1['newMerchant'] = $new_merchant;
    } else {
      $m_datas1['ser_fee'] = sprintf('%0.2f', ($sr_fee1));
      $m_datas1['sub_fee'] = sprintf('%0.2f', ($sub_fee));
      $m_datas1['p_value'] = 0;
      $m_datas1['volume'] = $totalamount;
      $m_datas1['revenue'] = $final;
      $m_datas1['tier_status'] = $total_transaction;
      $m_datas1['newMerchant'] = $new_merchant;
    }

    return $m_datas1;
  }
  public function getResellerDashboardCurrentMonthRevenue($resellerID,$plan_id,$queryAgent,$month){
    if($month != null && $month != ''){
      $condition = "and (date_format(ct.transactionDate,'%b-%Y')= '". $month ."')";
    }else{
      $condition = '';
    }

    $query1 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as total
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType ='sale' or ct.transactionType ='Paypal_sale' or ct.transactionType ='Paypal_capture' or  ct.transactionType ='Offline Payment'  or  ct.transactionType ='pay_sale' or  ct.transactionType ='prior_auth_capture' or ct.transactionType ='pay_capture' or  ct.transactionType ='capture' 
      or  ct.transactionType ='auth_capture' or ct.transactionType ='stripe_sale' or ct.transactionType ='stripe_capture') ". $queryAgent ." and mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID." and mr.resellerID=".$resellerID." and mr.plan_id=" . $plan_id . " and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='3') ".$condition." ");
    if ($query1->num_rows() > 0) {
        $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
        $total['rev'] = '0';
    }

    $query2 = $this->db->query("
                Select COALESCE(SUM(ct.transactionAmount),0) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' )
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') ". $queryAgent ." 
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0'  and mr.plan_id=" . $plan_id . " and   mr.isEnable='1' and rs.resellerID=".$resellerID."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='2' or ct.transaction_user_status !='3'  or ct.transaction_user_status IS NULL) ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total;

  }

  public function get_reseller_month_transaction($resellerID, $pln_id)
  {
    $res = 0;
    $month = date('M-Y');
    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "AND mr.agentID = $ragentID";
        
    }
    $date = ' and  date_format(ct.transactionDate,"%b-%Y") = "' . $month . '"  ';

    $query1 = $this->db->query('select count(distinct(ct.transactionID)) as total  from customer_transaction ct 
    inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
    inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID 
    where  ct.transactionCode IN ("100","200", "120","111","1") AND ct.transactionType IN ("SALE","CAPTURE","AUTH_CAPTURE","PRIOR_AUTH_CAPTURE","PAY_SALE","PAY-SALE","PAY_CAPTURE","STRIPE_SALE","STRIPE_CAPTURE","PAYPAL_SALE","PAYPAL_CAPTURE","OFFLINE PAYMENT") AND 
        ct.resellerID="' . $resellerID . '" '.$queryAgent.' and mr.resellerID="'.$resellerID.'" and mr.isDelete="0" and mr.plan_id="' . $pln_id . '" '.
    $date);

    if ($query1->num_rows() > 0) {
      $res = $query1->row_array()['total'];
    } else {
      $res = '0';
    }


    return  $res;
  }
  public function get_reseller_total_payment_new($resellerID,$queryAgent,$month){
    if($month != null && $month != ''){
      $condition = "and (date_format(ct.transactionDate,'%b-%Y')= '". $month ."')";
    }else{
      $condition = '';
    }

    $query1 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as total
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType ='sale' or ct.transactionType ='Paypal_sale' or ct.transactionType ='Paypal_capture' or  ct.transactionType ='Offline Payment'  or  ct.transactionType ='pay_sale' or  ct.transactionType ='prior_auth_capture' or ct.transactionType ='pay_capture' or  ct.transactionType ='capture' 
      or  ct.transactionType ='auth_capture' or ct.transactionType ='stripe_sale' or ct.transactionType ='stripe_capture') ". $queryAgent ." and  mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID." and mr.resellerID=".$resellerID." and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='3') ".$condition." ");
    if ($query1->num_rows() > 0) {
        $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
        $total['rev'] = '0';
    }

    $query2 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' ) ". $queryAgent ."
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') 
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='2' or ct.transaction_user_status !='3' or ct.transaction_user_status IS NULL) ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total;

  }
  public function get_data_admin_merchant_by_condition($resID, $agentID = '',$postData = array())
  {
    $count = 0;
    $isBoard = 0;
    $countSf = 0;
    $res = array();
    $sortArrCustomer = $sortArrVolumn = [];
    if($postData['planFilter'] != ''){
        $planIds = explode(',', $postData['planFilter']);
    }else{
        $planIds = '';
    }
    if($postData['agentFilter'] != ''){
        $agentIds = explode(',', $postData['agentFilter']);
    }else{
        $agentIds = '';
    }
    if($postData['softwareFilter'] != ''){
        $softwareIds = explode(',', $postData['softwareFilter']);
        $countSf = count($softwareIds);
        if (in_array(15, $softwareIds))
        {
          $isBoard = 1;
        }
    }else{
        $softwareIds = '';
    }
    $column = getTableRowOrderColumnValue($postData['order'][0]['column']);
    if($column == 0){
        $orderName = 'mr.companyName';
    }elseif($column == 1){
        $orderName = 'mr.companyName';
    }elseif($column == 2){
        $orderName = 'mr.companyName';
    }elseif($column == 3){
        $orderName = 'p.plan_name';
    }elseif($column == 4){
        $orderName = 'mr.date_added';
    }else{
        $orderName = 'mr.date_added';    
    }
    $orderBY = getTableRowOrderByValue($postData['order'][0]['dir']);
    

    $this->db->select('mr.*,p.plan_name,ag.agentName, ag.previledge, pr.merchantPortalURL,(SELECT fpl.friendlyname FROM plan_friendlyname fpl WHERE fpl.plan_id = mr.plan_id and mr.resellerID = fpl.reseller_id) AS friendlyname');

    $this->db->from('tbl_merchant_data mr');
    $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
    $this->db->join('tbl_reseller_agent ag', 'ag.ragentID = mr.agentID', 'left');
    $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'LEFT');
    $this->db->join('Config_merchant_portal pr', 'rs.resellerID = pr.resellerID', 'LEFT');
    $this->db->join('app_integration_setting app', 'app.merchantID = mr.merchID', 'LEFT');

    if($postData['search']['value'] != null || !empty($postData['search']['value'])){
          $search_key = $postData['search']['value'];
          $this->db->like('mr.companyName', $search_key);
         
      }
    $this->db->where('mr.isDelete = "0" ');
    $this->db->where('mr.isEnable = "1" ');
    $this->db->where('mr.resellerID', $resID);
    
    if($planIds != ''){
      $this->db->where_in('mr.plan_id', $planIds);
    }
    if($softwareIds != ''){
      if($countSf == 1){
          if($isBoard){
              $this->db->where('mr.firstLogin',0);
              
          }else{
              $this->db->where_in('app.appIntegration', $softwareIds);
              $this->db->where('mr.firstLogin',1);
          }
      }else{
          if($isBoard){
              
              $this->db->where_in('app.appIntegration', $softwareIds);
              
          }else{
              $this->db->where_in('app.appIntegration', $softwareIds);
              $this->db->where('mr.firstLogin',1);
          }
      }
      
      
    }
    
    if ($agentID != ''){
      $this->db->where('mr.agentID', $agentID);
    }else if ($agentIds != ''){
      $this->db->where_in('mr.agentID', $agentIds);
    }else{

    }
    $postLength = getTableRowLengthValue($postData['length']);
    $startRow = getTableRowStartValue($postData['start']);

    if($postLength != -1){
      if($column != 2 && $column != 1 ){
        $this->db->limit($postLength, $startRow);
      }

    } 
    $this->db->group_by('mr.merchID');
    $query = $this->db->order_by($orderName, $orderBY)->get();

    if ($query->num_rows()) {

      $res1 = $query->result_array();

      foreach ($res1 as $key => $reus) {


        $tdata = $this->merchant_customer_count($reus['merchID']);

        $customer_count = $total_amount = $refund_amount = 0;

        if ($tdata) {
          $customer_count = ($tdata['count'] ? $tdata['count'] : 0);
          $total_amount = ($tdata['total'] ? $tdata['total'] : 0);
          $refund_amount = ($tdata['refund'] ? $tdata['refund'] : 0);
        }

        $reus['customer_count'] = $customer_count;
        $reus['total_amount'] = $total_amount;
        $reus['refund_amount'] = $refund_amount;
        $reus['totalVoulumn'] = $total_amount - $refund_amount;

        $res[]  = $reus;
        $sortArrCustomer[$key]  = $reus['customer_count'];

        $sortArrVolumn[$key]  = $reus['totalVoulumn'];
      }

      if($column == 2){
          if($orderBY == 'asc'){
            array_multisort($sortArrCustomer, SORT_ASC, $res);
          }else{
            array_multisort($sortArrCustomer, SORT_DESC, $res);
          }
          $res = array_slice($res, $startRow,$postLength); 
      }elseif($column == 1){
          if($orderBY == 'asc'){
            array_multisort($sortArrVolumn, SORT_ASC, $res);
          }else{
            array_multisort($sortArrVolumn, SORT_DESC, $res);
          }
          $res = array_slice($res, $startRow,$postLength); 
      }
       
      
    }
    $res['data'] = $res;
    $res['count'] = count($res);
    return $res;
  }
  public function get_dataCount_admin_merchant_by_condition($resID, $agentID = '',$postData = array())
  {
    $count = 0;
    $isBoard = 0;
    $countSf = 0;
    $res = array();
    if($postData['planFilter'] != ''){
        $planIds = explode(',', $postData['planFilter']);
    }else{
        $planIds = '';
    }

    if($postData['agentFilter'] != ''){
        $agentIds = explode(',', $postData['agentFilter']);
    }else{
        $agentIds = '';
    }
    if($postData['softwareFilter'] != ''){
        $softwareIds = explode(',', $postData['softwareFilter']);
        $countSf = count($softwareIds);
        if (in_array(15, $softwareIds))
        {
          $isBoard = 1;
        }
        
    }else{
        $softwareIds = '';
    }

    $column = getTableRowOrderColumnValue($postData['order'][0]['column']);
    if($column == 0){
        $orderName = 'mr.companyName';
    }elseif($column == 1){
        $orderName = 'mr.companyName';
    }elseif($column == 2){
        $orderName = 'mr.companyName';
    }elseif($column == 3){
        $orderName = 'p.plan_name';
    }elseif($column == 4){
        $orderName = 'ag.agentName';
    }else{
        $orderName = 'mr.date_added';    
    }
    $orderBY = getTableRowOrderByValue($postData['order'][0]['dir']);
    $this->db->select(' mr.*,p.plan_name,ag.agentName, ag.previledge, pr.merchantPortalURL,(SELECT fpl.friendlyname FROM plan_friendlyname fpl WHERE fpl.plan_id = mr.plan_id and mr.resellerID = fpl.reseller_id) AS friendlyname');

    $this->db->from('tbl_merchant_data mr');
    $this->db->join('tbl_reseller rs', 'rs.resellerID = mr.resellerID', 'INNER');
    $this->db->join('tbl_reseller_agent ag', 'ag.ragentID = mr.agentID', 'left');
    $this->db->join('plans p', 'mr.plan_id = p.plan_id', 'LEFT');
    $this->db->join('Config_merchant_portal pr', 'rs.resellerID = pr.resellerID', 'LEFT');
    $this->db->join('app_integration_setting app', 'app.merchantID = mr.merchID', 'LEFT');
    $this->db->where('mr.isDelete = "0" ');
    if($postData['search']['value'] != null || !empty($postData['search']['value'])){
          $search_key = $postData['search']['value'];
        $this->db->like('mr.companyName', $search_key);
       
    }
    $this->db->where('mr.isEnable = "1" ');
    $this->db->where('mr.resellerID', $resID);
    if($planIds != ''){
      $this->db->where_in('mr.plan_id', $planIds);
    }
    if($softwareIds != ''){
      if($countSf == 1){
          if($isBoard){
              $this->db->where('mr.firstLogin',0);
              
          }else{
              $this->db->where_in('app.appIntegration', $softwareIds);
              $this->db->where('mr.firstLogin',1);
          }
      }else{
          if($isBoard){
              
             $this->db->where_in('app.appIntegration', $softwareIds);
            
          }else{
              $this->db->where_in('app.appIntegration', $softwareIds);
              $this->db->where('mr.firstLogin',1);
          }
      }
      
      
    }


    if ($agentID != ''){
      $this->db->where('mr.agentID', $agentID);
    }else if ($agentIds != ''){
      $this->db->where_in('mr.agentID', $agentIds);
    }else{

    }
    
    $this->db->group_by('mr.merchID');
    $query = $this->db->order_by($orderName, $orderBY)->get();
    
    return $query->num_rows();
  }
  
  public function get_dashboard_CC_count($rid,$month ='')
  {
    if($month == ''){
      $month = date('M-Y');
    }
    
    $condition = "and (date_format(ct.transactionDate,'%b-%Y')= '". $month ."')";

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "AND mr.agentID = $ragentID";
    }

    $query1 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as total
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType ='sale' or ct.transactionType ='Paypal_sale' or ct.transactionType ='Paypal_capture' or  ct.transactionType ='pay_sale' or  ct.transactionType ='prior_auth_capture' or ct.transactionType ='pay_capture' or  ct.transactionType ='capture' 
      or  ct.transactionType ='auth_capture' or ct.transactionType ='stripe_sale' or ct.transactionType ='stripe_capture') and (ct.gateway  NOT LIKE '%ECheck%')  ". $queryAgent ." and mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$rid." and mr.resellerID=".$rid." and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='3') ".$condition." ");
    if ($query1->num_rows() > 0) {
        $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
        $total['rev'] = '0';
    }

    $query2 = $this->db->query("
                Select COALESCE(SUM(ct.transactionAmount),0) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' )
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.gateway  NOT LIKE '%ECheck%') ". $queryAgent ."
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0'  and  mr.isEnable='1' and rs.resellerID=".$rid."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='2' or ct.transaction_user_status !='3'  or ct.transaction_user_status IS NULL) ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total['rev'] - $total['refund'];
  }
  public function get_dashboard_EC_count($rid,$month ='')
  {
    if($month == ''){
      $month = date('M-Y');
    }
    $condition = "and (date_format(ct.transactionDate,'%b-%Y')= '". $month ."')";

    $queryAgent = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2)
        $queryAgent = "AND mr.agentID = $ragentID";
    }

    $query1 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as total
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  ct.gateway LIKE '%ECheck%' ". $queryAgent ." and mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$rid." and mr.resellerID=".$rid." and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='3') ".$condition." ");
    if ($query1->num_rows() > 0) {
        $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
        $total['rev'] = '0';
    }

    $query2 = $this->db->query("
                Select COALESCE(SUM(ct.transactionAmount),0) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  ct.gateway LIKE '%ECheck%' ". $queryAgent ." and (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' )
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') 
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0'  and  mr.isEnable='1' and rs.resellerID=".$rid."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='2' or ct.transaction_user_status !='3'  or ct.transaction_user_status IS NULL) ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total['rev'] - $total['refund'];
  }
  public function getMerchantTotalAmountProcessed($merchantID){
    
    $amountTotal = 0;
    $txnTotal = 0;
    $row =  $this->get_row_data('app_integration_setting', array('merchantID' => $merchantID));

    $this->db->select('count(*) as totalTXN');
    $this->db->select('(tr.transactionAmount) as total');
    $this->db->from('customer_transaction tr');

    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {
        
        $this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
       
        
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {

        $this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {

        $this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {

        $this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
       
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else{
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->select('crt.creditTransactionID');
        $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','left');
        $this->db->where("comp.merchantID ", $merchantID);
    }
    $this->db->where('MONTH(tr.transactionDate)', date('m'));
    $this->db->where('YEAR(tr.transactionDate)', date('Y'));
    
    $this->db->where("tr.merchantID ", $merchantID);
    $this->db->where("tr.transaction_user_status  !=", 3);
    $this->db->where_in("tr.transactionCode",[1,100,111,120,200]);
    $this->db->where_not_in("tr.transactionType",['refund','pay_refund','credit','stripe_refund','paypal_refund','void']);

   
    $this->db->order_by("tr.transactionDate", 'desc');
    $this->db->group_by("tr.transactionID");

    $query2 = $this->db->get();
    $res1 = $query2->result_array();
    if ($query2->num_rows() > 0) {
        $inc = 1;
        foreach ($res1 as $value) {
          $amountTotal = $amountTotal + $value['total'];
          $txnTotal = $inc;
          $inc = $inc + 1;
        }
        
        
    }


    /*Void case manage*/
    $this->db->select('count(*) as totalTXN');
    $this->db->select('(tr.transactionAmount) as total');
    $this->db->from('customer_transaction tr');

    if (isset($row['appIntegration']) && $row['appIntegration'] == '1') {
        
        $this->db->join('QBO_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');
       
        
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '2') {

        $this->db->join('qb_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '3') {

        $this->db->join('Freshbooks_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '4') {

        $this->db->join('Xero_custom_customer cust','tr.customerListID = cust.Customer_ListID','INNER');

    }else if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
       
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->where("comp.merchantID ", $merchantID);
    }else{
        
        
        $this->db->join('chargezoom_test_customer cust','tr.customerListID = cust.ListID','INNER');
        $this->db->join('tbl_company comp','comp.id = cust.companyID','INNER');
        $this->db->select('crt.creditTransactionID');
        $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','left');
        $this->db->where("comp.merchantID ", $merchantID);
    }
    $this->db->where('MONTH(tr.transactionDate)', date('m'));
    $this->db->where('YEAR(tr.transactionDate)', date('Y'));
    
    $this->db->where("tr.merchantID ", $merchantID);
    $this->db->where_in("tr.transactionCode",[1,100,111,120,200]);
    $this->db->where_in("tr.transactionType",['refund','pay_refund','credit','stripe_refund','paypal_refund','void']);

   
    $this->db->order_by("tr.transactionDate", 'desc');
    $this->db->group_by("tr.transactionID");

    $query3 = $this->db->get();
    $res3 = $query3->result_array();
    if ($query3->num_rows() > 0) {
        $inc = 1;
        foreach ($res3 as $value1) {
          $amountTotal = $amountTotal - $value1['total'];
          $inc = $inc + 1;
        }
        
        
    }
    if (isset($row['appIntegration']) && $row['appIntegration'] == '5') {
      $this->db->select('count(*) as totalTXN');
      $this->db->select('(tr.transactionAmount) as total');
      $this->db->from('customer_transaction tr');
      $this->db->join('tbl_customer_refund_transaction crt','tr.transactionID = crt.creditTransactionID','INNER');
      $this->db->where('MONTH(tr.transactionDate)', date('m'));
      $this->db->where('YEAR(tr.transactionDate)', date('Y'));
      $this->db->where("tr.merchantID ", $merchantID);
      $this->db->where("crt.merchantID ", $merchantID);
      $query1 = $this->db->get();
      $res = $query1->result_array();

      if ($query1->num_rows() > 0) {
          foreach ($res as $value1) {
            $amountTotal = $amountTotal - $value1['total'];
            
          }
      }
    }
    
    return array('applied' => $amountTotal,'TxnCount' => $txnTotal );

  }

  public function chart_get_dashboard_merchant($rid)
  {

    $m_datas = array();
    $m_datas1 = array();
    $rrrr = array();
    $customer = 0;
    $res = array();
    $months = array();

    $queryAgent = $queryAgent2 = '';
    if ($this->session->userdata('agent_logged_in')) {
      $ragentID   = $this->session->userdata('agent_logged_in')['ragentID'];
      $agentData = $this->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $ragentID]);

      if($agentData['dashboardPermission'] == 2){
        $queryAgent = "AND mr.agentID = $ragentID";
        $queryAgent2 = "AND m1.agentID = $ragentID";
      }
      
    }

    for ($i = 11; $i >= 0; $i--) {
      $months[] = date("M-Y", strtotime(date('Y-m') . " -$i months"));
    }
    $chart_arr = array();
    $res_reval = 0;
    $totalAnnualamount = 0;
    $disabledMerchantList = [];
    foreach ($months as $key => $month) {

      $disabledMerchant = $this->getDisbaledMerchant($month,$rid,$queryAgent);
      $disabledMerchantList[] = $disabledMerchant;

      $sql =   $this->db->query("
      Select  mr.resellerID, mr.merchID, mr.plan_id
      from  tbl_merchant_data mr
      inner join tbl_reseller rs on rs.resellerID=mr.resellerID
      where  mr.isDelete='0' and mr.isEnable='1' and mr.resellerID='" . $rid . "' $queryAgent AND DATE_FORMAT(mr.date_added, '%b-%Y') = '$month'
      GROUP BY mr.resellerID");
      $m_datas = $sql->result_array();
      
      $final = 0;
      $cms = 0;
      $totalamount = 0.00;
      $tier = 0;
      $new_merchant = 0;
      $custmonth = date("M", strtotime($month));

      $newMersql = "Select count(*) as m_count from tbl_merchant_data m1 inner join tbl_reseller rs1 on rs1.resellerID=m1.resellerID  where m1.resellerID='" . $rid . "' ".$queryAgent2." and  m1.isEnable='1'   and m1.isDelete='0' and date_format(m1.date_added,'%b-%Y') = '$month'";
      $newMer =   $this->db->query($newMersql)->result_array();
      if(!empty($newMer)){
        $new_merchant = intval($newMer[0]['m_count']);
      }

      if (!empty($m_datas)) {

        $m_customer = 0;

        foreach ($m_datas as $md) {
          $refund = 0;

          
          $rev  = $this->getResellerDashboardCurrentMonthRevenue($rid, $md['plan_id'],$queryAgent,$month);

          $revCC  = $this->get_dashboard_CC_count($rid,$month);
          $revEC  = $this->get_dashboard_EC_count($rid,$month);
          
          $refund = $rev['refund'];
          $totalamount += $rev['rev'] - $refund;
        }

        
        $final = $totalamount;
        $m_datas1['volume'][]   = $totalamount;
        $m_datas1['revenue'][] = $final;
        $m_datas1['eCheck'][] = $revEC;
        $m_datas1['creditCard'][] = $revCC;
        $m_datas1['merchant'][]= $new_merchant;
        $m_datas1['month'][]      = $month;
        $m_datas1['customers'][] = $m_customer;
      } else {
        $m_datas1['volume'][] = $totalamount;
        $m_datas1['revenue'][] = $final;
        $m_datas1['eCheck'][] = 0;
        $m_datas1['creditCard'][] = 0;
        $m_datas1['merchant'][] = $new_merchant;
        $m_datas1['month'][] = $month;
        $m_datas1['customers'][] = 0;
      }
      $m_datas1['custmonth'][] = $custmonth;
      
      $res_reval = $this->get_reseller_total_payment_new($rid,$queryAgent,$month);
      
      $refundAnnual = $res_reval['refund'];
      $totalAnnualamount += $res_reval['rev'] - $refundAnnual;
     
    }
    
    $res_amount = $totalAnnualamount;

    $m_datas1['total_payment'] = $res_amount;


    $ob = [];
    $obRevenu = [];
    $obOnline = [];
    $obeCheck = [];
    $in = 0;
    foreach ($m_datas1['month'] as $value) {
        $custmonths = date("M", strtotime($value));
        $ob1 = [];
        $obR = [];
        $obON = [];
        $obEC = [];
        $inc = strtotime($value);
        $ob1[0] = $inc;
        $obR[] = $inc;
        $obR[] = $custmonths;
        $ob1[1] = $m_datas1['volume'][$in];

        $ob[] = $ob1;

        $obON[0] = $inc;
        $obON[1] = $m_datas1['creditCard'][$in];
        $obOnline[] = $obON;


        $obEC[0] = $inc;
        $obEC[1] = $m_datas1['eCheck'][$in];
        $obeCheck[] = $obEC;

        $obRevenu[] = $obR; 
        $in++;
    }
    
    $m_datas1['revenu_month'] =   $obRevenu;
    $m_datas1['revenu_volume'] =   $ob;
    $m_datas1['online_volume'] =   $obOnline;
    $m_datas1['disbaledMerchant'] =   $disabledMerchantList;
    
    $m_datas1['eCheck_volume'] =   $obeCheck;
    
    return $m_datas1;
  }

  public function resellerRevenue($resellerID,$queryAgent,$month){
    if($month != null && $month != ''){
      $condition = "and (date_format(ct.transactionDate,'%b-%Y')= '". $month ."')";
    }else{
      $condition = '';
    }

    $query1 = $this->db->query("
                Select  COALESCE(SUM(ct.transactionAmount),0) as total
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType ='sale' or ct.transactionType ='Paypal_sale' or ct.transactionType ='Paypal_capture' or  ct.transactionType ='Offline Payment'  or  ct.transactionType ='pay_sale' or  ct.transactionType ='prior_auth_capture' or ct.transactionType ='pay_capture' or  ct.transactionType ='capture' 
      or  ct.transactionType ='auth_capture' or ct.transactionType ='stripe_sale' or ct.transactionType ='stripe_capture') ". $queryAgent ." and mr.isDelete='0' and   mr.isEnable='1' and rs.resellerID=".$resellerID." and mr.resellerID=".$resellerID." and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='3') ".$condition." ");
    if ($query1->num_rows() > 0) {
        $total['rev'] = ($query1->row_array()['total']) ? $query1->row_array()['total'] : 0;
    } else {
        $total['rev'] = '0';
    }

    $query2 = $this->db->query("
                Select COALESCE(SUM(ct.transactionAmount),0) as refund
                 from customer_transaction ct
                inner join  tbl_merchant_data mr on mr.merchID = ct.merchantID
                inner join tbl_reseller rs  ON rs.resellerID = ct.resellerID
                where  (ct.transactionType = 'refund' OR ct.transactionType = 'pay_refund' OR ct.transactionType = 'stripe_refund' OR ct.transactionType = 'credit' )
        and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') ". $queryAgent ." 
        and ( (ct.transaction_user_status !='2' or ct.transaction_user_status !='3') )  and  mr.isDelete='0'  and   mr.isEnable='1' and rs.resellerID=".$resellerID."  and (ct.transactionCode ='100' or ct.transactionCode ='111' or ct.transactionCode='1' or ct.transactionCode='200') and (ct.transaction_user_status !='2' or ct.transaction_user_status !='3'  or ct.transaction_user_status IS NULL) ".$condition." ");
        
        if ($query2->num_rows() > 0) {
            $total['refund'] = ($query2->row_array()['refund']) ? $query2->row_array()['refund'] : 0;
        } else {
            $total['refund'] = '0';
        }

      return $total;

  }
  public function getDisbaledMerchant($month,$rid,$queryAgent)
  {
      $sql =   $this->db->query("
      Select  count(*) as merchantCount
      from  tbl_merchant_data mr
      inner join tbl_reseller rs on rs.resellerID=mr.resellerID
      where  mr.isEnable='0' and mr.resellerID='" . $rid . "' $queryAgent AND DATE_FORMAT(mr.updatedAt, '%b-%Y') = '$month' ");

      $m_datas = $sql->row_array();
      return -$m_datas['merchantCount'];
      
  }
}
