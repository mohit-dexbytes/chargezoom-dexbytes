<?php
Class Login_model extends CI_Model
{
	private $tbl_user = 'tbl_merchant_data'; // user table name 
	function Login()
	{
		parent::Model();
	}

   	function check_user_mecrchant($login_id, $password)
	{
		return false;

		$this->db->select('*');
		$this->db->from($this->tbl_user);
		$this->db->where('merchantEmail', $login_id);
		$this->db->where('merchantPassword', md5($password));
		$this->db->where('isEnable', '1');
		$this->db->limit(1);
		$query = $this->db->get();
		
		if ($query->num_rows() == 1)
		{
			$this->db->where('merchantEmail', $login_id);
			$this->db->where('merchantPassword',md5($password));
			$this->db->update($this->tbl_user, array(
				'is_logged_in' => '1'
			));
			return $query->row();
		}
		else
		{
			return false;
		}
	}


	function user_logout()
	{ 
		$this->db->trans_start();
		$session_data = $this->session->userdata('logged_in'); 
		$this->db->where('merchID', $session_data['merchID']); 
		$this->db->update($this->tbl_user,array('is_logged_in'=>'0')); 
		$this->session->unset_userdata('logged_in'); 
		$this->session->sess_destroy(); 
		$this->db->trans_complete(); 
		return $this->db->trans_status();

	}
	
	//Forgot Password 
	
	function temp_reset_password($code,$email)
	{
		return false;
		
		$this->db->where('merchantEmail', $email);
	    $this->db->set('merchantPassword',md5($code));
	    if($this->db->update($this->tbl_user))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	// Admin Change Password
	
	function savenewpass($id , $password)
	{
		return false;
		if (isset($id) && $id != '')
		{
		 $this -> db ->where('merchID', $id);
		 $this -> db ->set('merchantPassword', md5($password));
		 $query = $this -> db -> update($this->tbl_user);
		 
		 if ($query) 
			return true;
		 else
			return false;
		}
	 }
	 
	 
	 
	function merchant_user_logout()
	{ 
		$this->db->trans_start();
		$this->session->userdata('user_logged_in');
		$this->session->unset_userdata('user_logged_in'); 
		$this->session->sess_destroy(); 
		return true;

	} 
}