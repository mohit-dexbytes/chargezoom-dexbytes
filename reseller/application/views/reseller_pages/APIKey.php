<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <div class="msg_data">
		<?php echo $this->session->flashdata('message');   ?>
	</div>
    <legend class="leg"> API</legend>
    <div class="block-main full"  style="position: relative;">
        
        <div class="addNewBtnCustom" >
            <?php if($isEditMode){
                echo '<a href="#add_api" class="btn btn-sm btn-success" onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal"  >Add New</a>';
            }else{
                echo '<a href="javascript:void(0);" class="btn btn-sm btn-success" disabled >Add New</a>';
            } ?>
           
        </div>
    					
    	<table id="api_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                  
                    <th class=" text-left">Name </th>
                    <th class=" text-left">Domain </th>
                    <th class=" text-left">API Key Development </th>
                    <th class=" text-left">API Key Production </th>
                    <th class=" text-center">Action </th>
                </tr>
            </thead>
            <tbody>
    		
    	        <?php  if(!empty($api_data)){ foreach($api_data as $api){ ?>
    			<tr>
    				<td class="text-left"><?php echo $api['AppName']; ?></td>
    				<td class="text-left"><?php echo $api['Domain']; ?></td>
    				<td class="text-left"><?php echo $api['APIKeyDev']; ?></td>
    				<td class="text-left"><?php echo $api['APIKeyPro']; ?></td>
    		    	<td class="text-center">
    				    <div class="btn-group btn-group-xs">
                        <?php if($isEditMode){ ?>
    				      <a href="#del_api" onclick="Set_api_id('<?php echo $api['apiID'];  ?>');"  data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>
                        <?php }else{
                            echo '<a href="javascript:void(0);" disabled  title="Delete" class="btn btn-danger"> <i class="fa fa-times"> </i> </a>';
                        } ?>
    				
    		    	     </div> 
    		    	</td>
    	        </tr>
    	    <?php } }else{
                echo'<tr>
                    <td colspan="5"> No Records Found </td>
                    <td style="display:none;">  </td>
                    <td style="display:none;">  </td>
                    <td style="display:none;">  </td>
                    <td style="display:none;">  </td>
                    </tr>'; 
            } ?>
    			
    		</tbody>
        </table>
        </br>
    
        <!--END All Orders Content-->
    </div>
    <!-- END All Orders Block -->

</div>



     <!------------ Add popup for gateway   ------->

 <div id="add_api" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New API Key </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="api_form" class="form  form-horizontal" action='<?php echo base_url(); ?>SettingConfig/create_api_key'>
		
		       <div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Name</label>
						<div class="col-md-6">
						   <input type="text" id="api_name" name="api_name"  class="form-control " />
								
						</div>
						
					</div>

					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Domain</label>
						 <div class="col-md-6">
						   <input type="text" id="domain_name" name="domain_name"  class="form-control " />
							</div>
					</div>
			
	         <div class="form-group">
					<div class="col-md-10 text-right">
					<button type="submit" class="submit btn btn-sm btn-success">Generate</button>
				  <button type="button" class="btn btn-sm btn-default newCloseButton" data-dismiss="modal">Cancel</button>
                    </div>
             </div> 
			
	   </form>		
                
            </div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>

</div>



<div id="del_api" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete API Key</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_apirr" method="post" action='<?php echo base_url(); ?>SettingConfig/delete_api_key' class="form-horizontal" >
                     
                 
					<p>Do you really want to delete this API Key ?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="apiID" name="apiID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  name="btn_cancel" class="btn btn-sm btn-danger" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<style>
.table.dataTable {
  width:100% !important;
 }
 .block-title h1{
font-size: 16px;
}

.table thead > tr > th {
    font-size: 13px;
}

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 20px !important;
 }
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    position: absolute;
    z-index: 9;
    top: 9px;
    left: 60px;
    width: 220px !important;
}
.addNewBtnCustom {
    right: 0;
}
</style>
 <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
 <script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#api_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [0] },
                    { orderable: false, targets: [0] }
                ],
                order: [[ 0, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

</script>   
<script >
  
function Set_api_id(id){
	
	     $('#apiID').val(id);
}
 
$(document).ready(function(){

 
    $('#api_form').validate({ // initialize plugin
					
		rules: {
		       'api_name': {
                        required: true,
                        minlength: 3,
                         
                    },
                    'domain_name': {
                        required: true,
                        minlength:3,
                    },
				
			},
    });
});	    
		
    
</script>