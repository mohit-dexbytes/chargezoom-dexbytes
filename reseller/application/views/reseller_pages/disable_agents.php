<!-- Page content -->
<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}
.greyColor{
    color: #ccc !important;
}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <legend class="leg"> Agents</legend>     
    <!-- All Orders Block -->
    <div class="block-main full"  style="position: relative;">
	
        <!-- All Orders Title -->
        
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="agent_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Agent Name</th>                    
                    <th class="text-right hidden-sm">Admin Privileges</th>
					 <th class="text-right  hidden-xs">Disabled Date</th>
                    <th class="text-center">Action </th>
                </tr>
            </thead>
            <tbody>
		
				<?php 
			
				if(isset($r_agent) && $r_agent)
				{
					foreach($r_agent as $reseller)
					{
				?>
				<tr>
					<td class="text-left cust_view">
                        <?php if($isEditMode){ ?>
                            <a href="<?php echo base_url().'SettingAgent/create_agent/'.$reseller['ragentID']; ?>" data-toggle="tooltip" title="Edit Agent" class="">
                                <?php echo $reseller['agentName']; ?>
                            </a>    
                        <?php }else{ 
                            echo '<a href="javascript:void(0);" class="greyColor" disabled data-toggle="tooltip" title="Edit Agent" >';  
                        } ?>
                        
                    </td>
					<td class="text-right hidden-sm"><?php echo $reseller['prev'];  ?></td>					
					<td class="text-right hidden-xs"><?php echo date('M d, Y', strtotime($reseller['updatedAt'])); ?></td>
				    <td class="text-center ">
						<div class="btn-group btn-group-xs">
						<?php 
						
						if($isEditMode)
						{ 
						if($reseller['isEnable']=='1'){ 
						?>
							
                           
					        <a href="#suspend_ragent" onclick="disable_agent('<?php echo $reseller['ragentID']; ?>');" class="btn btn-danger" data-toggle="modal" title="" data-original-title="Disable Agent"><i class="fa fa-ban"></i></a>
						<?php }
						else{ ?>
						 
                       
					    <a href="#open_ragent" onclick="enable_agent('<?php echo $reseller['ragentID']; ?>');" class="btn btn-success" data-toggle="modal"  data-original-title="Enable Agent"><i class="fa fa-plus-circle"></i></a>
						
						 <?php } 
						}else{
							?>

                       
					    <a href="javascript:void(0);" disabled class="btn btn-success" ><i class="fa fa-plus-circle"></i></a>
							
						<?php } ?>
						
						</div>
					</td>
				</tr>

				
				
				
				<?php } } 
				else { echo'<tr>
                <td colspan="4"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>';  }  
				?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
      
    
    </div>
    <!-- END All Orders Block -->
  <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:10%;z-index:999999"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>    
    
</div>


    


<!------- Modal for Active Agent ------>

  <div id="open_ragent" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Enable Agent</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="suspend_ragent" method="post" action='<?php echo base_url(); ?>SettingAgent/active_agent' class="form-horizontal" >
                <p>Do you really want to Enable this Agent?</p>
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="activeagent_id" name="activeagent_id" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Enable"  />
                    <button type="button" class="btn btn-sm close1 btn_can" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>




<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init();});
</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#agent_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: false, targets: [3] }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function del_ragent_id(id){
	
	     $('#reselleragentid').val(id);
	     
	     $.ajax({
	         
	       url:"<?php echo base_url() ?>/SettingAgent/get_del_agent",
	       method:"POST",
	       data:{agetID : id},
	       success:function(response){
	               var data = $.parseJSON(response);
	               var age_data = data['r_agent'];
							     $('#reseller_agent').append('<option value="option1">None</option>');
							    for(var val in age_data) {
								      $("<option />", {value: age_data[val]['ragentID'], text: age_data[val]['agentName'] }).appendTo($('#reseller_agent'));
							    }
							   
	           
	       }
	         
	     });
	 
}

function  disable_agent(id){
	     $('#reselleragent_id').val(id);
	    
}
function  enable_agent(id){
	     $('#activeagent_id').val(id);
	    
}
    
</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<!------    Add popup form    ------->