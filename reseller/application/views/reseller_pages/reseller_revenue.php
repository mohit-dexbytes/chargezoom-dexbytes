 <style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}
</style>

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
  
    <!-- Quick Stats -->
    <div class="row text-center">
        	
        
    </div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>Reseller</strong> Invoices</h2>
            <div class="block-options pull-right">
                           
                        </div>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                   
                    <th class="text-left">Invoice</th>
                   
                    <th class="hidden-xs text-right hidden-sm">Due Date</th>
                 
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-center">Status</th>
                    
                    
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices))
			             {

						foreach($invoices as $invoice){
						     
						    
			   ?>
			
				<tr>
				
					<td class="text-left cust_view"><strong><a href="<?php echo base_url().'home/invoice_details/'.$invoice['invoice']; ?>" ><strong><?php  echo $invoice['invoiceNumber']; ?></strong></a></strong></td>
					
                    <td class="hidden-xs text-right hidden-sm"><?php echo date('m/d/Y', strtotime($invoice['DueDate'])); ?></td>
				
					<td class="text-right hidden-xs">$<?php echo  $vall= number_format((float)$invoice['BalanceRemaining'],2);  ?></td>
					<td class="text-center">
					<?php  if($invoice['status']=='0'){ echo ucfirst('Pending'); }else{ echo ucfirst('Paid'); } ?></td>
					
				
					

				</tr>
				
				<?php 
				  }
			         }	
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->
</div>
<!-- END Page Content -->



<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [5] }
                ],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>