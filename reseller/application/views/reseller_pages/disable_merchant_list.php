<style>
    .btn_can{
    background-color: #f5f5f5;
    color:#333;
    border-color:#e8e8e8;
}
</style>
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
   
    <legend class="leg"> Disabled Merchants</legend>     
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
	     
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="merchant_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="hidden-xs text-left"> Merchant </th>
					<th class="text-left"> Name </th>
				
                    <th class="hidden-xs text-right">Plan</th>
                      <th class="hidden-xs text-right"> Termination Date</th>
                    <th class="text-center"> Action </th>
                    
                </tr>
            </thead>
            <tbody>
			<?php 
				if(isset($merchant_list) && $merchant_list)
				{
					foreach($merchant_list as $merchant)
					{
					  $volume = $merchant['total_amount']-$merchant['refund_amount'];

				?>
				<tr>
				    <?php if($isViewMode == 1)
                        {  ?>
                        <td class="text-left hidden-xs cust_view "> <a href="<?php echo base_url('Reseller_panel/merchant_details/'.$merchant['merchID']); ?>" data-toggle="tooltip" title="View Merchant" ><?php echo ($merchant['companyName'])?$merchant['companyName']:'-----'; ?></a> </td>
                    <?php }else{ ?>
                        <td class="text-left hidden-xs "> <a href="javascript:void(0);" data-toggle="tooltip" title="View Merchant" ><?php echo ($merchant['companyName'])?$merchant['companyName']:'-----'; ?></a> </td>
                    <?php } ?>
					
					<td class="text-left "> <?php echo $merchant['firstName'].' '.$merchant['lastName']; ?></td>
				
				
				<td class="text-right hidden-xs">
						<?php if(isset($merchant['friendlyname']) && $merchant['friendlyname'] != ""){ echo $merchant['friendlyname']; } else{  echo ($merchant['plan_name'])?$merchant['plan_name']:'-----';  } ?>
					</td>
					<td class="text-right"><span class="hidden"><?php echo $merchant['updatedAt']; ?></span><?php echo date('F d, Y', strtotime($merchant['updatedAt'])); ?></td>
				<td class="text-center">
						<div class="">
						
						<?php if($login_info['login_type']=='RESELLER' || ( $login_info['login_type']=='AGENT'  ) )
                        {  ?>
		 				
						
                        <a href="#active_merchant" onclick="set_active_merchant('<?php  echo $merchant['merchID']?>');"   data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-original-title="Enable" class="btn btn-sm btn-success"> Enable </a>

                    <?php }else{
                        echo '<a href="#" class="btn btn-sm btn-success disabled"> Enable </a>';
                    } ?>
                     
                   
							
					</div>
					</td>
				</tr>
				
				<?php } }else{
                    echo'<tr>
                <td colspan="5"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; 
                } ?>
				
				
			</tbody>
        </table>
        <!--END All Orders Content-->
         <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:-50px;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
    </div>
    <!-- END All Orders Block -->

	<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#merchant_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


function set_active_merchant(id){
	
	  $('#merchID').val(id);
	
}

</script>	
	

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>	
	
	<!--------------------del admin Merchant------------------------>

<div id="active_merchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Enable Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="active_merchant" method="post" action='<?php echo base_url(); ?>Reseller_panel/active_merchant' class="form-horizontal" >
                     
                 
					<p>Do you really want to Enable this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchID" name="merchID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-success" value="Enable"  />
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 </div>