   <!-- Page content -->
   <?php
		$this->load->view('alert');
	?>
	<div id="page-content">
	
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->
    <legend class="leg"> Edit Disable Agent</legend>  
    <!-- Progress Bar Wizard Block -->
    <div class="block">
	               
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="agent_form" class="form form-horizontal" action="<?php echo base_url(); ?>SettingAgent/edit_disable_agent">
			
			         <input type="hidden"  id="agID" name="agID" value="<?php if(isset($agent['agentName'])){echo $agent['ragentID']; } ?>" /> 
			
			
			       	<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Name</label>
					<div class="col-md-6">
					<input type="text" id="agent_name" name="agent_name" read class="form-control" readonly  value="<?php if(isset($agent['agentName'])){ echo $agent['agentName']; } ?>" placeholder="Name"></div>
					</div>
						<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email</label>
					<div class="col-md-6">
					<input type="text" id="agent_email" name="agent_email" class="form-control" value="<?php  if(isset($agent['agentEmail'])){ echo $agent['agentEmail']; } ?>" placeholder="Email"></div>
					</div>
			
		            
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username"> &nbsp;&nbsp;</label>
				
					<div class="col-md-3">
					  <label class=" ">   <input type="checkbox"  disabled id="addmerchant" name="addmerchant" <?php if(isset($agent) && $agent['addMerchant']=='1' ){ echo "checked='checked'"; } ?> value="1">   Add New Merchants </label></div>
					<div class="col-md-3">
					    	<label  class="">  <input type="checkbox" disabled id="disablemerchant" <?php if(isset($agent) && $agent['disableMerchant']=='1' ){ echo "checked='checked'"; } ?> name="disablemerchant" value="1">  Disable Merchants</label> </div>
                   
		            </div>
					
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Commission %</label>
					<div class="col-md-2">
					<input type="text" id="agent_commission" name="agent_commission" class="form-control"  readonly value="<?php if(isset($agent['commission'])){ echo $agent['commission']; } ?>" placeholder="Commission %"></div>
					</div>
					
					
					
					<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Admin Privileges</label>
						   <div class="col-md-2">
								<select id="privilege" class="form-control " readonly name="privilege" >
								<option value=" ">Select Privilege</option>
								<option value="None"  <?php if(isset($agent) && $agent['previledge']=='None' ){ echo "Selected"; } ?> >None</option>
								<option value="ViewAll" <?php if(isset($agent) && $agent['previledge']=='ViewAll' ){ echo "Selected"; } ?> >View All</option>
								<option value="EditAll" <?php if(isset($agent) && $agent['previledge']=='EditAll' ){ echo "Selected"; } ?> >Edit All</option>
								<option value="FullAdmin" <?php if(isset($agent) && $agent['previledge']=='FullAdmin' ){ echo "Selected"; } ?> >Full Admin</option>
								</select>
							</div>
                        </div>	
                
	                <div class="form-group pull-right">
					<div class="col-md-12">
							
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
				
			    	
					<a href="<?php echo base_url('SettingAgent/reseller_agents'); ?>" class="btn btn-sm btn-danger ">Cancel</a>
					</div>
				    </div>					
					
		
		
  	</form>
  	
  	
		
        </div>
        
         <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="margin-top:-100px;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

 
    $('#agent_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'agent_name': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'agent_email': {
                        required: true,
                        minlength:3,
                        email:true,
                    },
					
			
                    'agent_password': {
                        required: true,
                        minlength:5,
                    },
					'conf_password': {
                        required: true,
                        equalTo: '#agent_password',
                    }, 
                    
                   'agent_commission': {
                        required: true,
                        number:true
                    },
                   
			
			},
    });
});	    
		
	

</script>

</div>


