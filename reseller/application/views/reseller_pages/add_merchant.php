<!-- Page content -->
<?php
	$this->load->view('alert');
?>
	<div id="page-content">

	
<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}
.error{color:red; }

</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
           <div class="block-title">
               
                <ol class="breadcrumb">
        <li class="breadcrumb-item"><?php if(isset($merchant)){?>
          <strong> <a href="<?php echo base_url(); ?>Reseller_panel/merchant_list">Merchants</a></strong>
        </li>
        <li class="breadcrumb-item">
           
        <?php
             echo "Edit Merchant";
             }else{
             echo "Create New Merchant";
             }
         ?>
         </li>
      </ol>
        
             </div>
	               <?php
						
					
							if($login_info['login_type']=='RESELLER'){  
							       $url = base_url().'Reseller_panel/create_merchant'; 
							       $rs_pass = base_url().'Reseller_panel/recover_Merch_pwd/';
							       $can_url = base_url().'Reseller_panel/merchant_list';
							    
							}else{
							       $url = base_url().'Reseller_panel/create_merchant'; 
							       $rs_pass = base_url().'Reseller_panel/recover_Merch_pwd/';
							       $can_url = base_url().'Reseller_panel/merchant_list';
							}
						
					?>
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
  	     	<form method="POST" id="merchant_form" class="form form-horizontal" action="<?php echo $url; ?>">
			
			 <input type="hidden"  id="merchID" name="merchID" value="<?php if(isset($merchant)){echo $merchant['merchID']; } ?>" /> 
		
                 
				   <div class="col-md-6"> 
				   
        				   	<?php if($login_info['login_type']=='RESELLER'){   ?>
			             <div class ="form-group">
						   <label class="col-md-4 control-label" >Select Agent</label>
						   <div class="col-md-6">
								<select id="reseller_agent" class="form-control " name="reseller_agent" >
								<option value="0">Select Agent</option>
								
								<?php foreach($re_agents as $agent){  ?>
								   <option value="<?php echo $agent['ragentID']; ?>"<?php if(isset($merchant)){ if($merchant['agentID']==$agent['ragentID'] ){ ?> selected="selected" <?php }  ?> <?php }?> > <?php echo $agent['agentName']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
			   <?php } ?>
        						
        				 <div class="form-group">
			           <label class="col-md-4 control-label" for="example-username">Merchant Name</label>
			           <div class="col-md-6">
			               <input type="text" id="companyName" name="companyName" class="form-control" value="<?php if(isset($merchant)){ echo $merchant['companyName']; }?>" placeholder="Merchant Name"><?php echo form_error('companyName'); ?>
			           </div>
			       </div>
        						
        						
        						<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="firstName" name="firstName" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['firstName']; } ?>" placeholder="First Name"><?php echo form_error('firstName'); ?></div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="lastName" name="lastName" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['lastName']; } ?>" placeholder="Last Name"><?php echo form_error('lastName'); ?></div>
					</div>
					
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email</label>
					<div class="col-md-6">
					<input type="text" id="merchantEmail" name="merchantEmail" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantEmail']; } ?>" placeholder="Email"><?php echo form_error('merchantEmail'); ?></div>
					</div>
					
					<?php if(!isset($merchant)) { ?>
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Password </label>
					<div class="col-md-6">
					<input type="password" id="merchantPassword" name="merchantPassword" class="form-control" value="<?php if(isset($merchant)){ echo $merchant['merchantPassword']; } ?>" placeholder="Password"> <?php echo form_error('merchantPassword'); ?></div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Confirm Password </label>
					<div class="col-md-6">
					<input type="password" id="confirmPassword" name="confirmPassword" class="form-control" value="" placeholder="Confirm Password"> </div>
					</div>
					<?php } ?>
					
					<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Select Plan</label>
						   <div class="col-md-6">
								<select id="Plans" class="form-control " onchange="check_plan_data(this);"   name="Plans" >
								<option value="">Select Plan</option>
								<?php foreach($plan_list as $plan){  ?>
								   <option value="<?php echo $plan['plan_id']; ?>"<?php if(isset($merchant)){ if($merchant['plan_id']==$plan['plan_id'] ){ ?> selected="selected" <?php }  ?> <?php }?> > <?php echo $plan['friendlyname']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
                               
        						
                            <div class="form-group">
            					<label class="col-md-4 control-label" for="example-username">Phone Number </label>
            					<div class="col-md-6">
            					<input type="text" id="merchantContact" name="merchantContact" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantContact']; } ?>" placeholder="Phone Number"><?php echo form_error('merchantContact'); ?></div>
            					</div>       
                           
                           	<div class="form-group">
            					<label class="col-md-4 control-label" for="example-username">Alternate Number </label>
            					<div class="col-md-6">
            					<input type="text" id="merchantAlternateContact" name="merchantAlternateContact" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantAlternateContact']; } ?>" placeholder="Alternate Number"><?php echo form_error('merchantAlternateContact'); ?></div>
            					</div>
                                           
        					<div class="form-group">
            					<label class="col-md-4 control-label" for="example-username">Website </label>
            					<div class="col-md-6">
            					<input type="text" id="weburl" name="weburl" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['weburl']; } ?>" placeholder="Website"><?php echo form_error('website'); ?></div>
            					</div>
                   
                    </div>    
                    
                  
					    
			<div class="col-md-6">  
				
			
    			  <div class="form-group">
        					<label class="col-md-4 control-label" for="example-username">Address Line 1 </label>
        					<div class="col-md-6">
        					<input type="text" id="merchantAddress1" name="merchantAddress1" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantAddress1']; } ?>" placeholder="Address Line 1 "><?php echo form_error('merchantAddress1'); ?></div>
        					</div>
    	
                  <div class="form-group">
        					<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
        					<div class="col-md-6">
        					<input type="text" id="merchantAddress2" name="merchantAddress2" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantAddress2']; } ?>" placeholder="Address Line 2 "><?php echo form_error('merchantAddress2'); ?></div>
        					</div>
          
                  <div class="form-group">
                					<label class="col-md-4 control-label" for="example-username">City</label>
                					<div class="col-md-6">
                					<input type="text" id="merchantCity" name="merchantCity" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantCity']; } ?>" placeholder="City"><?php echo form_error('merchantCity'); ?></div>
                					</div>
                  
                  <div class="form-group">
                					<label class="col-md-4 control-label" for="example-username">State</label>
                					<div class="col-md-6">
                					<input type="text" id="merchantState" name="merchantState" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantState']; } ?>" placeholder="State"><?php echo form_error('merchantState'); ?></div>
                					</div>
                  
                  <div class="form-group">
                					<label class="col-md-4 control-label" for="example-username">ZIP Code</label>
                					<div class="col-md-6">
                					<input type="text" id="merchantZipCode" name="merchantZipCode" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantZipCode']; } ?>" placeholder="ZIP Code"><?php echo form_error('merchantZipCode'); ?></div>
                					</div>
            				  	
                  <div class="form-group">
                					<label class="col-md-4 control-label" for="example-username">Country</label>
                					<div class="col-md-6">
                					<input type="text" id="merchantCountry" name="merchantCountry" class="form-control"  value="<?php if(isset($merchant)){ echo $merchant['merchantCountry']; } ?>" placeholder="Country"><?php echo form_error('merchantCountry'); ?></div>
                					</div>
                <input type="hidden" name="chk_gateway"  id="chk_gateway" value="0" />
                					
                <?php if(!isset($merchant)){ ?>         					
                <div class="form-group">
                        <label class="col-md-4 control-label" for="reference"></label>
                        <div class="col-md-6">
                        <input type="checkbox" id="setMail" name="setMail" class="set_checkbox" <?php if(isset($check) && $check >0){echo "checked"; } ?>  /> Send Merchant Email
                        </div>
                </div> 
    				
    		
                        <?php     
                              $option='';
         foreach($all_gateway as $gat_data){ 
								   $option.='<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
								   } 
           echo   $gateway=    '
					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt" onchange="set_gateway_data(this);"  class="form-control">
						          <option value="" >Select Gateway</option>'.$option.'
							</select>
							</div>
					</div>'; ?>
			<div id="pay_data" >		
					 <div id="fr_div" class="form-group " style="display:none">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />
								
						</div>
						
					</div>
					

				
				
                <div id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                           
                        </div>
                    </div>
				</div>

				<div id="cz_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
                <div id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password">
                           
                        </div>
                    </div>
					
				</div>	
				
				<div id="paypal_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>	
			
				<div id="stripe_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="Publishable Key">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="Secret API Key">
                           
                        </div>
                    </div>
					
				</div>	
				
				 <div id="usaepay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>
				 <div id="heartland_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
				</div>
				
				
				<div id="cyber_div" style="display:none" >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>	
				
                            
                        </div>
					<?php } ?>	
					    <div class="col-md-4"></div>
					    <div class="col-md-6">
					<?php if(!isset($merchant)) {?>				
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
					<?php } else {?>
						<div><input type="hidden" id="defaultGatewayId" name="defaultGatewayId" class="form-control"  value="0"><?php echo form_error('defaultGatewayId'); ?><input type="hidden" id="default_gateway" name="default_gateway" class="form-control"  value="0"><?php echo form_error('default_gateway'); ?></div>
					 <a href="<?php echo $rs_pass.$merchant['merchID']; ?>" class="btn btn-sm btn-info ">Reset Password</a>
					<button type="submit" id='updateMerchant' class="submit btn btn-sm btn-success">Update</button>					
					<?php }?>
					<a href="<?php echo base_url('Reseller_panel/merchant_list'); ?>" class="btn btn-sm btn_can ">Cancel</a>
					</div>
					
				   	
		   
    					
		    </div>	

		   
		     
		
  	</form>
  	
  	
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->

 <!-- Progress Bar Wizard Block -->
   <?php  if(isset($merchant) && !empty($merchant))
    { 
    ?>
 
   	<div class="block">  
					     <div class="block-title">
					          
					       <div class="block-options">
					            <h2><strong>Merchant Gateway </strong>  </h2>
                            <div class="block-options pull-right" >
                               <a href="#set_def_gateway" class="btn btn-sm btn-info" style="margin-top:6px;" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Set Default Gateway</a>
                               <a href="#add_gateway" class="btn btn-sm btn-success" style="margin-top:6px;" onclick="" data-backdrop="static" data-keyboard="false" data-toggle="modal" >Add New</a>
                            </div> 
                           </div>    
                        </div> 
					
				
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
  	  <table id="merch_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class=" text-left">Gateway </th>
                    <th class="hidden-xs text-left">Merchant ID</th>
					<th class=" text-left">Gateway Name </th>
					<th class="hidden-xs text-left">User Name</th>
					<th class="text-center"> Action </th> 
                </tr>
            </thead>
            <tbody>
			
			
			
			<?php 
		
		
				$defaultGatewayId = 0;
				if(isset($get_data) && $get_data)
				{
					foreach($get_data as $data)
					{
						
						if($data['set_as_default'] == 1){
							$defaultGatewayId = $data['gatewayID'];
						}
				?>
				<tr>
					
					<td class="text-left "><?php if($data['gatewayType']=='8'){echo"Cybersource";}else if($data['gatewayType']=='5'){echo"Stripe";} else if($data['gatewayType']=='4'){echo"Paypal";}else if($data['gatewayType']=='1'){echo"NMI";} else if($data['gatewayType']=='2'){echo"Authorize.Net";}else if($data['gatewayType']=='6'){echo"USAePay";}else if($data['gatewayType']=='7'){echo"Heartland";}else if($data['gatewayType']=='9'){echo"Chargezoom";} else{
					echo"Pay Trace";} ?> </a> </td>
					
					<td class="text-left hidden-xs"><?php echo $data['merchantID'];?> </a> </td>
					
					<td class="text-left "><?php echo $data['gatewayFriendlyName']; ?> </a> </td>
					
                    <td class="text-left hidden-xs"><?php echo  $data['gatewayUsername']; ?> </a> </td>
				
					
			 	<td class="text-center">
					<div class="btn-group btn-group-xs">
						
				<a href="#edit_gateway" class="btn btn-default" onclick="set_edit_gateway('<?php echo $data['gatewayID'];  ?>');" title="Edit" data-backdrop="static" data-keyboard="false" data-toggle="modal"> <i class="fa fa-edit"> </i> </a>  
              	<a href="#del_gateway" onclick="del_gateway_id('<?php echo $data['gatewayID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Delete" class="btn  btn_can"> <i class="fa fa-times"> </i> </a>
					
				</div>
					
		   </td>  
		</tr>
				
				<?php } }
				else { echo'<tr><td colspan="5"> No Records Found </td></tr>'; }  
				 ?>
				
			</tbody>
        </table>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    
     <div class="row">
         <div class="col-md-12">
        <div class="col-md-4">
        </div>    
        <div class="col-md-4">
            <div class="msg_data" style="z-index:9999999;"><?php echo $this->session->flashdata('message');   ?> 
             </div> 
        </div>
        <div class="col-md-4">
        </div>
      </div>  
    </div>  
<!--------------==============================  Add New Gateway Start Here   ================================-------->

<div id="add_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title"> Add New </h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="nmiform" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/create_gateway">
			   <input type="hidden"  id="merchID" name="merchID" value="<?php if(isset($merchant)){echo $merchant['merchID']; } ?>" />
		     <div class="form-group">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />
								
						</div>
						
					</div>

					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt" onchange="set_gateway_data(this);"  class="form-control">
						       
								   <option value="" >Select Gateway</option>
								   <?php foreach($all_gateway as $gat_data){ 
								   echo '<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
								   } 
								   ?>
								  
							</select>
							</div>
					</div>
					
		    	<div id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                           
                        </div>
                    </div>
				</div>

				<div id="cz_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser" name="czUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="czPassword" name="czPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status" name="cz_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status" name="cz_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
                <div id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password">
                           
                        </div>
                    </div>
					
				</div>	
				
				<div id="paypal_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>	
			
				<div id="stripe_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="Publishable Key">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="Secret API Key">
                           
                        </div>
                    </div>
					
				</div>	
				
				 <div id="usaepay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>
				 <div id="heartland_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
				</div>
				
				
				<div id="cyber_div" style="display:none" >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID" name="cyberMerchantID" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber" name="apiSerialNumber" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey" name="secretKey" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>	
				
				
				
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  placeholder="Merchant ID (optional)">
                           
                        </div>
						
                   </div>

                  
	         <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Add</button>
					
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
					
                    </div>
             </div> 
			
	   </form>		
                
            </div>
			
            <!-- END Modal Body -->
        </div>
     </div>
	 
  </div>
  
  
<!----==================================== End of New Gateway Modal =========================================---->

    
    <?php } ?>
    <!-- END Progress Bar Wizard Block -->


<!-- END Page Content -->



<!--======================================== Set Default Gateway =================================---->

<div id="set_def_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Set Default Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="thest_pay" method="post" action='<?php echo base_url(); ?>home/set_gateway_default' class="form-horizontal card_form" >
                      <input type="hidden"  id="defmerchID" name="defmerchID" value="<?php if(isset($merchant)){echo $merchant['merchID']; } ?>" />
                 
                    
                                                <div class="form-group ">
                                              
                                                <label class="col-md-4 control-label">Gateway</label>
                                                <div class="col-md-6">
                                                    <select id="gatewayid" name="gatewayid" class="form-control">
                                                        <option value="" >Select Gateway</option>
                                                        <?php if(isset($get_data) && !empty($get_data) ){
                                                                foreach($get_data as $data){
                                                                ?>
                                                           <option value="<?php echo $data['gatewayID'] ?>"  <?php if($data['set_as_default']=='1'){ echo 'selected'; } ?>   ><?php echo $data['gatewayFriendlyName'] ?></option>
                                                                <?php } } ?>
                                                    </select>
                                                    
                                                </div>
                                            </div>      
                                            
                    
                 
                    <div class="pull-right">
                         <img id="card_loader" src="<?php echo base_url(); ?>resources/img/ajax-loader.gif" style="display: none;">  
                         
                     <input type="submit" id="btn_process" name="btn_process" class="btn btn-sm btn-info" value="Save"  />
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
                </form>     
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!---===================================== End Of Default Gateway ===============================---->


<!--=====================================   Modal for Edit Gateway  =================================================--->

<div id="edit_gateway" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
			 <div class="modal-header text-center">
                <h2 class="modal-title">Edit Gateway</h2> </div>
                
            	<div class="modal-body">    
			<form method="POST" id="nmiform1" class="form nmifrom form-horizontal" action="<?php echo base_url(); ?>home/update_gateway">
			 	
			<input type="hidden" id="gatewayEditID" name="gatewayEditID" value=""  />
			<input type="hidden" id="editmerchID" name="editmerchID" value="<?php echo $this->uri->segment(3); ?>"  />
			
                     <div class="form-group">
						<label class="col-md-4 control-label" for="example-username"> Friendly Name</label>
						<div class="col-md-6">
								<input type="text" id="fname"  name="fname" class="form-control"  value="" placeholder="">
							</div>
			     </div>
  
                    <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <input type="text" name="gateway" id="gateway" readonly class="form-control" />
						</div>
					</div>
					
		  
				<div id="nmi_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser1" name="nmiUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="nmiPassword1" name="nmiPassword1" class="form-control"  placeholder="password..">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status1" name="nmi_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status1" name="nmi_ach_status1"  >
                           
                        </div>
                    </div>
				</div>	

				<div id="cz_div1" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="czUser1" name="czUser1" class="form-control"; placeholder="Username..">
                        </div>
                   </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="text" id="czPassword1" name="czPassword1" class="form-control"  placeholder="password..">
                           
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_cr_status1">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_cr_status1" name="cz_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="cz_ach_status1">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="cz_ach_status1" name="cz_ach_status1"  >
                           
                        </div>
                    </div>
				</div>

                 <div id="auth_div1" style="display:none">					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID1" name="apiloginID1" class="form-control" placeholder="API LoginID..">
                        </div>
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey1" name="transactionKey1" class="form-control"  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
                                        <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status1" name="auth_cr_status1" checked="checked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status1" name="auth_ach_status1"   >
                           
                        </div>
                    </div>
                    
                    
				</div>	
				
		    	  <div id="pay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser1" name="paytraceUser1" class="form-control"; placeholder="PayTrace  Username..">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword1" name="paytracePassword1" class="form-control"  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
				</div>	
				
				
				<div id="paypal_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser1" name="paypalUser1" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword1" name="paypalPassword1" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature1" name="paypalSignature1" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>
				
				<div id="cyber_div1" style="display:none" >	
				     <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Cyber MerchantID</label>
                        <div class="col-md-6">
                            <input type="text" id="cyberMerchantID1" name="cyberMerchantID1" class="form-control"  placeholder="Enter  MerchantID">
                           
                        </div>
                    </div>
				
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">APIKeyID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiSerialNumber1" name="apiSerialNumber1" class="form-control" placeholder="Enter API Key ID">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="secretKey1" name="secretKey1" class="form-control"  placeholder="Enter  Secret Key">
                           
                        </div>
                    </div>
					
					 
					
				</div>
				
				<div id="stripe_div1" style="display:none" >		
					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser1" name="stripeUser1" class="form-control" placeholder="Publishable Key..">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword1" name="stripePassword1" class="form-control"  placeholder="Secret API Key..">
                           
                        </div>
                    </div>
					
				</div>
				 
				<div id="usaepay_div1" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey1" name="transtionKey1" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin1" name="transtionPin1" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>	
				<div id="heartland_div1" style="display:none" >		
				   
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey1" name="heartpublickey1" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey1" name="heartsecretkey1" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
				</div>	
				
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="mid"  name="mid" class="form-control"  value="" placeholder="MerchantID (optional)"> 
						</div>
			      </div>
							
	            <div class="form-group">
					<div class="col-md-4 pull-right">
					
					<button type="submit" class="submit btn btn-sm btn-success"> Save </button>
					
					<button  type="button" align="right" class="btn btn-sm  btn_can close1" data-dismiss="modal"> Cancel </button>
                   
                   </div>
            </div>
	 </form>
	 </div>
	<!--------- END ---------------->
		</div>

	</div>
 
 </div>

 <!--======================================== Modal for Delete Gateway ================================================------>

  <div id="del_gateway" class="modal fade" tabindex="-1" user="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Delete Gateway</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_gateway11" method="post" action='<?php echo base_url(); ?>home/delete_gateway' class="form-horizontal" >
                     
                 
					<p> Do you really want to delete this Gateway? </p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchantgatewayid" name="merchantgatewayid" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
                 <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn_can" value="Delete"  />
                    <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal"> Cancel </button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

</div>
 


<!---======================================================= Delete Gateway End Here ========================================-->

<script>
$(document).ready(function(){
    
    
    
    
       
        $('#merchant_form').validate({ // initialize plugin
			<?php 
		if(isset($merchant) && $merchant) {?>
		ignore: [],		<?php }?>		
		rules: {
		       'firstName': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'lastName': {
                        required: true,
                        minlength:2,
                    },
					'defaultGatewayId': {
                        required: true,
                        min:1,
                    },
					
				'merchantEmail': {
                        required: true,
                        email: true,
                         
                    },
                    'merchantPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'confirmPassword': {
                        required: true,
                        equalTo: '#merchantPassword',
                    },
                    
                  'Plans':{
                    required:true,  
                  },
                      'nmiUser': {
                 required: true,
                 minlength: 3
                },
			'nmiPassword':{
				 required : true,
				 minlength: 5,
				},
		 
		 
		 
		 
			'apiloginID':{
				required: true,
                 minlength: 3
			},
			'transactionKey':{
				required: true,
                 minlength: 3
			},		 
		 
			'frname':{
				 required: true,
				 minlength: 2
			},	
	
		 
			'paytracePassword':{
				required: true,
                minlength: 5
			},
			
			'paytraceUser':{
				required: true,
                 minlength: 3
			},
		 
			
			'paypalPassword':{
				required: true,
                minlength: 5
			},
			
			'paypalUser':{
				required: true,
                 minlength: 3
			},
		 
			'paypalSignature':{
				required: true,
                minlength: 5
			},
		
			
				'cyberMerchantID':{
				required: true,
                minlength: 5
			},
			
			'apiSerialNumber':{
				required: true,
                 minlength: 3
			},
		 
			'secretKey':{
				required: true,
                minlength: 5
			},
		
          'stripeUser': {
                 required: true,
                 minlength: 3
                },
				
					'stripePassword':{
						 required : true,
						 minlength: 3,
						
					  },	
        
        	'heartsecretkey':{
				required: true,
                 minlength: 3
			},
		 
			'heartpublickey':{
				required: true,
                minlength: 5
			},
				'transtionPin':{
				required: true,
                 minlength: 3
			},
		 
			'transtionKey':{
				required: true,
                minlength: 5
			},
		   'gateway_opt':{
             check_gateway:true,
             }
                 
			
		},
		messages:{
			'defaultGatewayId':
					{
					required: "Please add a default gateway.",
					min: "Please add a default gateway.",
				}
		}
    });
    
    
    
	  
  $('#nmiform').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
			
		 'gateway_opt':{
				 required: true,
		 },	
		 
		  'nmiUser': {
                 required: true,
                 minlength: 3
                },
			'nmiPassword':{
				 required : true,
				 minlength: 5,
				},
		 
		 
		 
		 
			'apiloginID':{
				required: true,
                 minlength: 3
			},
			'transactionKey':{
				required: true,
                 minlength: 3
			},		 
		 
			'frname':{
				 required: true,
				 minlength: 2
			},	
		 
			'nmiUser1': {
                 required: true,
                 minlength: 3
             },
			'nmiPassword1':{
				 required : true,
				 minlength: 5,
				},
		 
		 
			'apiloginID1':{
				required: true,
                 minlength: 3
			},
			
			'transactionKey1':{
				required: true,
                 minlength: 3
			},
		 
		 
			'paytracePassword':{
				required: true,
                minlength: 5
			},
			
			'paytraceUser':{
				required: true,
                 minlength: 3
			},
		 
			'paytracePassword1':{
				required: true,
                minlength: 5
			},
			'paytraceUser1':{
				required: true,
                minlength: 3
			},
			
			'paypalPassword':{
				required: true,
                minlength: 5
			},
			
			'paypalUser':{
				required: true,
                 minlength: 3
			},
		 
			'paypalSignature':{
				required: true,
                minlength: 5
			},
			'paypalUser1':{
				required: true,
                minlength: 3
			},
		    'paypalPassword1':{
				required: true,
                minlength: 5
			},
			
			'paypalSignature1':{
				required: true,
                minlength: 5
			},
		    
			
				'cyberMerchantID':{
				required: true,
                minlength: 5
			},
			
			'apiSerialNumber':{
				required: true,
                 minlength: 3
			},
		 
			'secretKey':{
				required: true,
                minlength: 5
			},
		
			
			'fname':{
			 required:true,
			 minlength:2,
			}
		 
			
	 },
  });

  
   
});	 

$.validator.addMethod("check_gateway", function(value, element) {
       
         if(value=='' && $('#chk_gateway').val()==0)
         return true;
        
		 return value >0 ;
   
        }, "This field is requied");
		



	function check_plan_data(el)
	{
	    
	   var plan= $('#'+el.id).val();
	   
	    $.ajax({
    url: '<?php echo base_url("Plan/get_plan_data")?>',
    type: 'POST',
	data:{plan_id:plan},
    success: function(data){
      var plan = jQuery.parseJSON(data);
 
	    if(plan.status=='success')
	    {
	      $('#pay_data').show();
	      $('#pay_data').removeAttr('disabled','');
	    	$('#chk_gateway').val(1);
			var default_gateway = $('#default_gateway').val();
			$('#defaultGatewayId').val(default_gateway);

	     } else
	      {
	    
	       $('#chk_gateway').val(0);
	       $('#defaultGatewayId').val(1);
	       $('#defaultGatewayId-error').hide();
	}	}
});
	  
	}
	
	
	function set_gateway_data(el)
	{
	        var gateway_value= $('#'+el.id).val();
		   
		  if(gateway_value!="")
		  {
		   $("#fr_div").show();
		   
           if(gateway_value=='3'){			
		       $('#pay_div').show();
			   $('#auth_div').hide();
			   $('#nmi_div').hide();
			   $('#cz_div').hide();
			   $('#stripe_div').hide();
				$('#paypal_div').hide();
				$('#usaepay_div').hide();
				$('#heartland_div').hide(); 
				 $('#cyber_div').hide();  
		   }else if(gateway_value=='2'){
				$('#auth_div').show();
				$('#pay_div').hide();
				$('#nmi_div').hide();
				 $('#stripe_div').hide();
				$('#paypal_div').hide();
				$('#usaepay_div').hide();
			    $('#heartland_div').hide(); 
				$('#cyber_div').hide();  
			   $('#cz_div').hide();
		   }else if(gateway_value=='1'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').show();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();	
                $('#usaepay_div').hide();
                $('#heartland_div').hide(); 
                 $('#cyber_div').hide();  
			   $('#cz_div').hide();
            }else if(gateway_value=='4'){
			     $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').show();	
			    $('#usaepay_div').hide();
			    $('#heartland_div').hide(); 
			     $('#cyber_div').hide();  
			   $('#cz_div').hide();
			}else if(gateway_value=='5'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').show();
				$('#paypal_div').hide();	
			    $('#usaepay_div').hide();
			    $('#heartland_div').hide();
			     $('#cyber_div').hide();  
			   $('#cz_div').hide();
			}else if(gateway_value=='6'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();	
			    $('#usaepay_div').show();
			    $('#heartland_div').hide(); 
			     $('#cyber_div').hide();  
			   $('#cz_div').hide();
			}else if(gateway_value=='7'){
			     $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();	
			    $('#usaepay_div').hide();
			    $('#cyber_div').hide();  
			    $('#heartland_div').show();  
				$('#cz_div').hide();
			}else if(gateway_value=='8'){
			     $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();	
			    $('#usaepay_div').hide();
			    $('#heartland_div').hide();  
			    $('#cyber_div').show();  
			   $('#cz_div').hide();
			}else if(gateway_value=='9'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();	
			    $('#usaepay_div').hide();
			    $('#heartland_div').hide();  
			    $('#cyber_div').hide();  
			   	$('#cz_div').show();
			}
		  }
		  else{
		    $("#fr_div").hide();
			$('#pay_div').hide();
			$('#auth_div').hide();
			$('#nmi_div').hide();	
			$('#stripe_div').hide();
			$('#paypal_div').hide();	
			$('#usaepay_div').hide();
			$('#heartland_div').hide();  
			$('#cyber_div').hide();  
			$('#cz_div').hide();
		  }
	  
	}
	
	function del_gateway_id(id){
	
	     $('#merchantgatewayid').val(id);
}


// Function to  get  gateway 

function set_edit_gateway(gatewayid){

	
	if(gatewayid !=""){
		
     $.ajax({
		url: '<?php echo base_url("Reseller_panel/get_gatewayedit_id")?>',
		type: 'POST',
		data:{gatewayid:gatewayid},
		dataType: 'json',
		success: function(data){
		
		 $('#gatewayEditID').val(data.gatewayID);		
			 
	          
			  if(data.gatewayType=='1')
			  {
				  $('#nmi_div1').show();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#cz_div1').hide();
		          $('#nmiUser1').val(data.gatewayUsername);
			      $('#nmiPassword1').val(data.gatewayPassword);
              }
			  
				if(data.gatewayType=='2')
				{
			       $('#nmi_div1').hide();
				  $('#auth_div1').show();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#cz_div1').hide();
				  $('#apiloginID1').val(data.gatewayUsername);
			      $('#transactionKey1').val(data.gatewayPassword);
				}
				
				if(data.gatewayType=='3')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').show();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#cz_div1').hide();
		          $('#paytraceUser1').val(data.gatewayUsername);
			      $('#paytracePassword1').val(data.gatewayPassword);
				}	

          if(data.gatewayType=='4')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').show();
				  $('#stripe_div1').hide();
				  $('#cz_div1').hide();
		          $('#paypalUser1').val(data.gatewayUsername);
			      $('#paypalPassword1').val(data.gatewayPassword);
				  $('#paypalSignature1').val(data.gatewaySignature);
				}	
               if(data.gatewayType=='5')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').show();
				  $('#cz_div1').hide();
		         $('#stripeUser1').val(data.gatewayUsername);
			      $('#stripePassword1').val(data.gatewayPassword);
				}					
			   if(data.gatewayType=='6')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').show();
				  $('#heartland_div1').hide();
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#transtionKey1').val(data.gatewayUsername);
			      $('#transtionPin1').val(data.gatewayPassword);
				}
				if(data.gatewayType=='7')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').hide();
				  $('#heartland_div1').show(); 
				   $('#cyber_div1').hide();
				  $('#cz_div1').hide();
		          $('#heartpublickey1').val(data.gatewayUsername);
			      $('#heartsecretkey1').val(data.gatewayPassword);
				}	
				
				if(data.gatewayType=='8')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').hide();
				  $('#heartland_div1').hide(); 
				   $('#cyber_div1').show();
				  $('#cz_div1').hide();
		          $('#cyberMerchantID1').val(data.gatewayUsername);
			        $('#apiSerialNumber1').val(data.gatewayPassword);
			      $('#secretKey1').val(data.gatewaySignature);
				}

				if(data.gatewayType=='9')
				{
					
		       	  $('#nmi_div1').hide();
				  $('#auth_div1').hide();
				  $('#pay_div1').hide();
				  $('#paypal_div1').hide();
				  $('#stripe_div1').hide();
				  $('#usaepay_div1').hide();
				  $('#heartland_div1').hide(); 
				   $('#cyber_div1').hide();
				  $('#cz_div1').show();
		          $('#czUser1').val(data.gatewayUsername);
			        $('#czPassword1').val(data.gatewayPassword);
				}

				$('#fname').val(data.gatewayFriendlyName);
				$('#gateway').val(data.gateway);
				$('#mid').val(data.gatewayMerchantID);
			 
			 
	}	
  }); 
	
  }

}


$(function(){  
     
	  $('#gateway_opt').change(function(){
		    var gateway_value =$(this).val();
           if(gateway_value=='3'){			
		       $('#pay_div').show();
			   $('#auth_div').hide();
				$('#nmi_div').hide();
				 $('#stripe_div').hide();
				$('#paypal_div').hide();
		   }else if(gateway_value=='2'){
				$('#auth_div').show();
				$('#pay_div').hide();
				$('#nmi_div').hide();
				 $('#stripe_div').hide();
				$('#paypal_div').hide();
		   }else if(gateway_value=='1'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').show();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();			
            }else if(gateway_value=='4'){
			     $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').show();	
			   
			   }else if(gateway_value=='5'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').show();
				$('#paypal_div').hide();	
			   
			   }
               else   if(gateway_value=='6'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();
					$('#usaepay_div').show();
				 $('#heartland_div').hide();
				$('#cyber_div').hide();
			   
			   }
			else   if(gateway_value=='7'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();
					$('#usaepay_div').hide();
				 $('#heartland_div').show();
				$('#cyber_div').hide();
			   
			   }
			 else  if(gateway_value=='8'){
			    $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();
					$('#usaepay_div').hide();
				 $('#heartland_div').hide();
				$('#cyber_div').show();
			   
			   }
              else{
              
                 $('#pay_div').hide();
				$('#auth_div').hide();
				$('#nmi_div').hide();	
                $('#stripe_div').hide();
				$('#paypal_div').hide();
					$('#usaepay_div').hide();
				 $('#heartland_div').hide();
				$('#cyber_div').hide();
              }
	  }); 
});	  

<?php
 if(isset($defaultGatewayId)) {
	?>
	$('#defaultGatewayId').val('<?php echo $defaultGatewayId; ?>');
	$('#default_gateway').val('<?php echo $defaultGatewayId; ?>');
	<?php
 }
?>
</script>



