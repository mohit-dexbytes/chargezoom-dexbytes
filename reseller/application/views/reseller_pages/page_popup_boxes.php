<script>
    function set_template_data_temp(args){
        if(args.title){
            $('#send_mail_title').html(args.title);
        }

        $.ajax({
			type:"POST",
			url : '../../Settingmail/set_template_ajax',
			data : args,
			success: function(data){
					data=$.parseJSON(data);
			   
			
				  CKEDITOR.instances['textarea-ckeditor'].setData(data['message']);
				if(data['replyTo'] != '') {
					$('#replyTo').val(data['replyTo']);
				}
				$('#emailSubject').val(data['emailSubject']);
				$('#toEmail').val(data['toEmail']);
			    $('#ccEmail').val(data['addCC']);
				$('#bccEmail').val(data['addBCC']);
				$('#replyEmail').val(data['replyTo']);
				$('#fromEmail').val(data['fromEmail']);
				$('#mailDisplayName').val(data['fromName']);
		    }
	    });
        
    }
</script>

<div id="set_tempemail_data" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header ">
                <h2 class="modal-title text-center" id="send_mail_title">Send Email</h2>
            </div>
            <!-- Modal Header Ends -->

            <!-- Modal Body -->
            <div class="modal-body">
                <div id="data_form_template">
                    <label class="label-control" id="template_name"> </label>
                    <form id="form-validation1" action="<?php echo base_url(); ?>Settingmail/send_mail" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <input type="hidden" name="emailType" id="emailType" value="" />
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="type">Template</label>
                            <div class="col-md-7">
                                <input type="text" name="type_text" class="form-control" readonly='readonly' id="type_text" value="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="templteName">To Email</label>
                            <div class="col-md-7">
                                <input type="text" id="toEmail" name="toEmail" value="" class="form-control" placeholder="Email">
                            </div>
                        </div>

                        
                        <div class="form-group" id="reply_div">
                            <label class="col-md-3 control-label" for="replyEmail">Reply To Email</label>
                            <div class="col-md-7">
                                <input type="text" id="replyTo" name="replyEmail" class="form-control" value="" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group" style="display:none" id="from_email_div">
                            <label class="col-md-3 control-label" for="templteName">From Email</label>
                            <div class="col-md-7">
                                <input type="text" id="fromEmail" name="fromEmail"  value="" class="form-control" placeholder="From Email">
                            </div>
                        </div>
                        <div class="form-group" id='display_name_div' style='display:none'>
                            <label class="col-md-3 control-label" for="templteName">Display Name</label>
                            <div class="col-md-7">
                                <input type="text" id="mailDisplayName" name="mailDisplayName" class="form-control" value="" placeholder="Display Name">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="templteName"></label>
                            <div class="col-md-7">
                                <a href="javascript:void(0);" id="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                <a href="javascript:void(0);"  id ="open_from_email">From Email Address<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a>
                                        <a href="javascript:void(0);"  id ="open_display_name">Display Name</a>
                                
                            </div>
                        </div>

                        <div class="form-group" id="cc_div" style="display:none">
                            <label class="col-md-3 control-label" for="ccEmail">CC these email addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="ccEmail" name="ccEmail" value="" class="form-control" placeholder="CC Email">
                            </div>
                        </div>
                        <div class="form-group" id="bcc_div" style="display:none">
                            <label class="col-md-3 control-label" for="bccEmail">BCC these e-mail addresses</label>
                            <div class="col-md-7">
                                <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="" placeholder="BCC Email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                            <div class="col-md-7">
                                <input type="text" id="emailSubject" name="emailSubject" value="" class="form-control" placeholder="Email Subject">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Email Body</label>
                            <div class="col-md-7">
                                <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"></textarea>
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" class="btn btn-sm btn-success"> Send </button>
                                <button type="button" class="btn btn-sm btn-primary1 close1" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Modal Body Ends -->

        </div>
    </div>
</div>

<?php if(isset($merchant)){ ?>

<div id="reset_password_merchant" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Reset Password</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="reset_password_merchant_form" method="post" action="<?php echo base_url().'Settingmail/reset_merchant_password'; ?>" class="form-horizontal">
					
				    <div class="form-group">
                        <div class="col-md-8">
                            <input type="hidden" id="resetMerchantID" name="resetMerchantID" class="form-control" value="<?php echo $merchant['merchID']; ?>">

                             <input type="hidden" id="firstName" name="firstName" value="<?php echo $merchant['firstName']; ?>">
                            <input type="hidden" id="companyName" name="companyName"  value="<?php echo $merchant['companyName']; ?>">
                            <input type="hidden" id="merchantEmail" name="merchantEmail"  value="<?php echo $merchant['merchantEmail']; ?>">


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 no-padding alignText" >Set Password</label>
                        <div class="col-md-5 no-padding setPassword">
                            <input type="text" id="newGeneratePassword" name="newGeneratePassword" class="form-control " value="" >    
                            <div class="ttp">
                                <i onmouseout="outFunc()"  onclick="copyText()" class="fa fa-copy faCopyPassword"></i>
                                <span class="tooltiptext" id="myTooltip">Copy to clipboard</span>
                            </div> 
                                                     
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="control-label col-md-3 no-padding" ></label>
                        <div class="col-md-5 no-padding" >
                            <div id="manageCustomCheck">
                                <input type="checkbox" id="customCheck" name="customCheck" class="customCheck" value="0" > <label class="customCheckLabel">  Send Merchant Email </label>
                            </div>                               
                        </div>
                    </div>
			 
                    <div class="pull-right">
        			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Reset Password">
                        <button type="button" class="btn btn-sm close1 btn_can" data-dismiss="modal">Cancel</button>
                    </div>
                    <br>
                    <br>
			    </form>		
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<?php } ?>

<script>
    $('#open_cc').click(function(){
        $('#cc_div').show();
        $(this).hide();
    });
    $('#open_bcc').click(function(){
        $('#bcc_div').show();
        $(this).hide();
    });
    $('#open_reply').click(function(){
        $('#reply_div').show();
        $(this).hide();
    }); 
    $('#open_from_email').click(function(){
        $('#from_email_div').show();
        $(this).hide();
    });
    $('#open_display_name').click(function(){
        $('#display_name_div').show();
        $(this).hide();
    });
    $('#manageCustomCheck').click(function(){
        
        if($("#customCheck").val() == 1){
            $("#customCheck").val(0);
            $("#customCheck").prop('checked',false);
            
        }else{
            $("#customCheck").val(1);
            $("#customCheck").prop('checked',true);
        }
    });

    $('#reset_password_merchant_form').validate({ // initialize plugin
        ignore:":not(:visible)",            
        rules: {
            
        'newGeneratePassword':{
            required: true,
            minlength:8,
            minlength:8,
            maxlength:24,
            check_atleast_one_number: true,
            check_atleast_one_special_char: true,
            check_atleast_one_lower: true,
            check_atleast_one_upper:true,
            check_name:true,
            company_name:true,
            check_email:true,

        } 
     },
  });

$('#resetPasswordBtn').click(function(){
    var randomstring = randomPassword(20);
    $('#newGeneratePassword').val(randomstring);
    
});

$.validator.addMethod("check_atleast_one_number", function(value, element) {
    if(value.length == 0){
        return true ;
    }else{
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
     return true ;

}, "This field should contain at least one number.");

$.validator.addMethod("check_atleast_one_special_char", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true ;
    }
    if (regularExpression.test(value)) {
        return false;
    } 
    return true ;
}, "This field should contain at least one special character.");
$.validator.addMethod("check_atleast_one_lower", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true;
    }
    if (value.match(/([a-z])/) && value.match(/([0-9])/)){
        return true;
    }else{
      return false;
    }
    return true ;
}, "This field should contain at least one lower characters.");
$.validator.addMethod("check_atleast_one_upper", function(value, element) {
    var regularExpression  = /^[a-zA-Z0-9 ]*$/;
    if(value.length == 0){
        return true;
    }
    if (value.match(/([A-Z])/) && value.match(/([0-9])/)){
        return true;
    }else{
      return false;
    }
    return true ;
}, "This field should contain at least one uppercase characters.");

$.validator.addMethod('check_name', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var firstName = $('#firstName').val();
        if(firstName.length == 0){
            return true ;
        }else{

            if(value.indexOf(firstName) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'First name not used in password.');

$.validator.addMethod('company_name', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var companyName = $('#companyName').val();
        if(companyName.length == 0){
            return true ;
        }else{

            if(value.indexOf(companyName) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'Company name not used in password.');

$.validator.addMethod('check_email', function(value, element, params) {
    if(value.length == 0){
        return true ;
    }else{
        var merchantEmail = $('#merchantEmail').val();
        if(merchantEmail.length == 0){
            return true ;
        }else{
           
            var res = merchantEmail.split("@");
            if(value.indexOf(res[0]) != -1){
                return false;
            }
        }
        if (value.search(/[0-9]/) < 0)
        return false;
       
    }
    return true ;
}, 'Email not used in password.');

function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
        var i = Math.floor(Math.random() * chars.length);
        pass += chars.charAt(i);
    }
    return pass;
}   

function copyText() {
  /* Get the text field */
  var copyText = document.getElementById("newGeneratePassword");

  /* Select the text field */
  copyText.select();
  copyText.setSelectionRange(0, 99999); /* For mobile devices */

  /* Copy the text inside the text field */
  document.execCommand("copy");

  /* Alert the copied text */
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copied: " + copyText.value;
}
function outFunc() {
  var tooltip = document.getElementById("myTooltip");
  tooltip.innerHTML = "Copy to clipboard";
}

</script>
<script src="<?php echo base_url(JS."/helpers/ckeditor/ckeditor.js");?>"> </script>
