
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    
    <!-- Quick Stats -->
    <div class="row">
      
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-credit-card"></i>
              </div>
               <h4 class="widget-content-light"><strong>Processing</strong> Volume</h4>
            </div>
            <a class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong>$ <?php echo number_format($revenue['volume'],2); ?>
			   </strong></span>
              
            </a>
          </div>
        </div>
        
        
     
        
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-warning o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-users"></i>
              </div>
               <h4 class="widget-content-light"><strong>Merchants</strong></h4>
            </div>
            <a class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo $revenue['newMerchant']; ?></strong></span>
              
            </a>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-money"></i>
              </div>
               <h4 class="widget-content-light"><strong>Customers</strong></h4>
            </div>
            <a class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong></strong></span>
               <?php echo $total_customer;?>
            </a>
          </div>
        </div>
        
        
       <div class="col-sm-6 col-lg-3">
          <div class="card text-white bg-danger o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-line-chart"></i>
              </div>
               <h4 class="widget-content-light"><strong>Transactions </strong></h4>
            </div>
            <a class="card-footer text-white clearfix z-1" href="#">
              <span class="float-left"><strong><?php echo $revenue['tier_status']; ?></strong></span>
            
            </a>
          </div>
        </div>
       
    </div>
     <br/>
    <!-- END Quick Stats -->

    <!-- eShop Overview Block -->
    <div class="block full">
        <!-- eShop Overview Title -->
        <div class="block-title">
            <h2><strong>Annual Processing Volume: </strong>$<span id="res_total">0.00</span></h2>
        </div>
        <!-- END eShop Overview Title -->

        <!-- eShop Overview Content -->
        <div class="row">
            <div id="chart-classic" style="height: 350px;" class="chart"></div>
        </div>
		<hr>
	    <div class="row">
		
        <div class="col-sm-6">
            <!-- Classic Chart Block -->
            <div class="block full">
                <!-- Classic Chart Title -->
                <div class="block-title">
                    <h2><strong> Plan Ratio: </strong><?php echo 'ES: <span id="ss_per">0</span>'.' %'.' / '.' AS: <span id="as_per">0 </span>'.' %'.' / '.'VT: <span id="vt_per">0</span>'.' %'; ?></span></h2>
                </div>
                <!-- END Classic Chart Title -->
                
                <!-- Classic Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-pie" class="chart"></div>
                <!-- END Classic Chart Content -->
            </div>
            <!-- END Classic Chart Block -->
        </div>
        <div class="col-sm-6">
            <!-- Bars Chart Block -->
            <div class="block full">
                <!-- Bars Chart Title -->
                <div class="block-title">
                    <h2><strong>Merchants: <span id="totl_merchant">0</span></strong></h2>
                </div>
                <!-- END Bars Chart Title -->

                <!-- Bars Chart Content -->
                <!-- Flot Charts (initialized in js/pages/compCharts.js), for more examples you can check out http://www.flotcharts.org/ -->
                <div id="chart-bars-new" class="chart"></div>
                <!-- END Bars Chart Content -->
            </div>
            <!-- END Bars Chart Block -->
        </div>
    </div>
    <!-- END Classic and Bars Chart -->

    <!-- Live Chart Block -->
	<section style="position:absolute; top:0;">
		<div class="block full" style="padding: 0;
margin: 0;
border: none;">
		<div class="row chart-area" style="">
			<div id="chart-live" class="chart ls" style="visibility:hidden;"></div>
			<div id="chart-classic" class="chart ls" style="visibility:hidden;"></div>
		</div>		
		</div>
	</section>
	
	</div>
	
	</div>	
	
	
	  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
	  <script src="<?php echo base_url(JS); ?>/pages/compCharts.js"></script>
                
                <link id="themecss" rel="stylesheet" type="text/css" href="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/css/shieldui/all.min.css" />          
                <script type="text/javascript" src="<?php echo getenv('HTTPS_SCHEME') . '://' . getenv('RSDOMAIN'); ?>/resources/js/shieldui/shieldui-all.min.js"></script>
	  
<script type="text/javascript">
 function format2(num)
{
   
    var p = parseFloat(num).toFixed(2).split(".");
    return  p[0].split("").reverse().reduce(function(acc, num, i, orig) {
        return  num=="-" ? acc : num + (i && !(i % 3) ? "," : "") + acc;
    }, "") + "." + p[1];

}
var pchart_color = []; 
var chartColor=[];  

      pchart_color.push({label:'Subscription Suite', data:parseFloat(<?php echo $plan_ratio['ss_amount']; ?>) });
      pchart_color.push({label:'Virtual Terminal', data:parseFloat(<?php  echo $plan_ratio['vt_amount'];  ?>)  });
      pchart_color.push({label:'Automation Terminal', data:parseFloat(<?php  echo $plan_ratio['as_amount'];  ?>)  });
      chartColor.push('#46216f');
	
    chartColor.push('#0077f7');
    chartColor.push('#28a745');

     
  function roundN(num,n){
  return parseFloat(Math.round(num * Math.pow(10, n)) /Math.pow(10,n)).toFixed(n);
} 


                
$(function(){         CompChartsnew.init();            
                
$.ajax({
			type:"POST",
			url : "<?php echo base_url(); ?>Reseller_panel/reseller_merchant_chart",
            success: function (data) {
              data=$.parseJSON(data);
            var ch_date='';  var  earnwww='0.00';
            
             ch_date =data['month'];
             earnwww =data['volume'];
             newDataSalesmerch =data['merchant'];
     
            $('#res_total').html(format2(data['total_payment'])) ; 
            $('#totl_merchant').html(data['total_merchant']);
        sum = 0;
$.each(newDataSalesmerch,function(){sum+=parseFloat(this) || 0;});
           
   $('#tot_merch').html(sum) ;  
 $("#chart-classic").shieldChart({
            theme: "light",
            primaryHeader: {
                text: ""
            },
            exportOptions: {
                image: false,
                print: false
            },
            axisX: {
                categoricalValues: ch_date
            },
            tooltipSettings: {
      chartBound: true,
      axisMarkers: {
                    enabled: true,
                    mode: 'xy'
                }, 
             customPointText: function (point, chart) {
            return shield.format(
                '<span>${value}</span>',
                {
                    value: format2(point.y)
                }
            );
        }
    },
           
			
            dataSeries: [{
                seriesType: 'splinearea',
                collectionAlias: "USD ",
                data: earnwww
            }]
			
        });
            
      
             
             
              $("#chart-bars-new").shieldChart({
                theme: "light",
                primaryHeader: {
                    text: " "
                },
                exportOptions: {
                    image: false,
                    print: false
                },
                axisX: {
                    categoricalValues:ch_date
                },
                axisY: {
                    title: {
                        text: ""
                    }
                },
                
                 tooltipSettings: {
      chartBound: true,
      axisMarkers: {
                    enabled: true,
                    mode: 'xy'
                }, 
             customPointText: function (point, chart) {
            return shield.format(
                '<span>{value}</span>',
                {
                    value: (point.y)
                }
            );
        }
    },
                dataSeries: [{
                    seriesType: "bar",
                    collectionAlias: "Merchant",
                    data: newDataSalesmerch
                }]
            });
        }
    		});    
                  

});

var CompChartsnew = function() {

    return {
        init: function() {
           
                var chartPie = $('#chart-pie');
			
	               $.plot(chartPie,  pchart_color,
                    {
                    colors: chartColor,
                    legend: {show: false},
                    series: {
                        pie: {
                            show: true,
                            radius: 1,
                            label: {
                                show: true,
                                radius: 2/ 4,
                                formatter: function(label, pieSeries) { 
                                   
                                    if(label=="Subscription Suite")
                                    $('#ss_per').html(Math.round(pieSeries.percent));
                                     if(label=="Virtual Terminal")
                                    $('#vt_per').html(Math.round(pieSeries.percent));
                                    var dis_label = label;
                                    if(label=="Subscription Suite"){
                                        dis_label = 'Enterprise Suite';
                                    }
                                    if(label=="Automation Terminal"){
                                    $('#as_per').html(Math.round(pieSeries.percent));
                                    var dis_label = 'Automation Suite';
                                    }
                                    return '<div class="chart-pie-label">' + dis_label + '<br>'+ pieSeries.data[0][1] + '</div>';
                                },
                                background: {opacity: 0.75, color: '#000000'}
                            }
                        }
                    }
                }
            );
        
        
        }
    }
}();

	</script>
	
	 
<style>
	#chart-classic g text tspan {
    visibility: visible;
}
#chart-classic g {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-classic text tspan {
    visibility: hidden;
}

	#chart-bars-new g text tspan {
    visibility: visible;
}
#chart-bars-new g {
    opacity: 0.7;
	borderWidth:2px;
}

#chart-bars-new text tspan {
    visibility: hidden;
}
.canvasjs-chart-credit{
    display:none;
}
.chart_new {
    height: 260px !important;
}
</style>
    
        
   