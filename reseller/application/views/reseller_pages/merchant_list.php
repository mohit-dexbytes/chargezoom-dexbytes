
<link rel="stylesheet" href="<?php echo base_url('/resources/css'); ?>/pages/chosen.min.css">
<style>
	.default {
		font-size: 13px !important;
	}
	
	a.redText {
		color: #E32E1B;
	}
    .disabled {
    pointer-events:none;
    opacity:0.6;
    color: #888888!important;
}

    .btn_can{
    background-color: #f5f5f5;
    color:#333;
    border-color:#e8e8e8;
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7{
	position: absolute;
    z-index: 1;
    left: 60px;
    width: 220px !important;
}
.addNewBtnCustomLeft{
	position: absolute;
    z-index: 9;
    right: 0px;
    top: 55px;
}
@media screen and (max-width: 767px){
	.addNewBtnCustomLeft {
	     left: 280px !important;
	     top: 9px;
	     text-align: right;
	}
	.dataTables_wrapper > div:first-child {
	     height: 80px!important;
	}
}
.addFilterNewBtnCustom {

    width: 100% !important;
}
@media screen and (min-width: 768px) and (max-width: 1024px){
	div#softwareList_chosen {
	    width: 64% !important;
	}
}
.filterPartsoftware{
	width: 410px !important;
}
.filterPartsoftware .chosen-container-multi .chosen-choices li.search-field {
    padding: 0% 3% 2% 1%!important;
}
.addFilterNewBtnCustom .chosen-choices{
	height: 44px !important;
    font-size: 14px!important;

}
.addFilterNewBtnCustom .chosen-choices input{

    font-family: inherit!important;
}
.addFilterNewBtnCustom {
    top: 2px;
}

a.btn.btn-sm.btn-grey {
    margin-top: 0px;
}
</style><!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
   
    
    <div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
    <legend class="leg"> Merchant Management</legend> 
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative">
	           
        <!-- All Orders Title -->
        <div class="addNewBtnCustomLeft">
						
							
			<?php 
			if($login_info['login_type']=='RESELLER'){  
			       $url = base_url().'Reseller_panel/create_merchant/'; 
			       $v_url  =  base_url().'Reseller_panel/merchant_details/'; 
			       $del_url = base_url().'Reseller_panel/delete_merchant';
			       
			    
			}else{
			     $url = base_url().'Reseller_panel/create_merchant/';
			     $v_url =  base_url().'Reseller_panel/merchant_details/'; 
			     $del_url = base_url().'Reseller_panel/delete_merchant';
			}
			if(isset($agentData['userMerchantAdd'])){
				if($agentData['userMerchantAdd'] == 1){
					$showNewAddBtn = 1;
				}else{
					$showNewAddBtn = 0;
				}
				
			}else{
				$showNewAddBtn = 1;
			}

			if(isset($agentData['userMerchantDisabled'])){
				if($agentData['userMerchantDisabled'] == 1){
					$showDisableBtn = 1;
				}else{
					$showDisableBtn = 0;
				}
				
			}else{
				$showDisableBtn = 1;
			}
			if($showNewAddBtn == 1){
				
				echo '<a class="btn btn-sm  btn-success" title="Create Merchant"  href="'.$url.'">Add New</a>';
			}
			
			?>
	   		
            
        </div>
        <input type="hidden" name="manageDisabledOption" id="manageDisabledOption" value="<?php echo $showDisableBtn; ?>">
        <div class="addFilterNewBtnCustom ">
        	<input type="hidden" name="planID" id="agentIDStr" value="">
        	<input type="hidden" name="planID" id="planIDStr" value="">
        	<input type="hidden" name="planID" id="softwareIDStr" value="">
        	<?php if($agentFilterHide == 0){ ?>
			<span  class="filterPart">
				<label class="filterLabel">Agent</label>
				<select id="agentList" data-placeholder="Select Agent" class="form-control chosen-select chosen-select-agent" multiple>
				<?php 	if(isset($r_agent) && $r_agent){
							foreach($r_agent as $reseller){
								echo '<option value="'.$reseller['ragentID'].'">'.$reseller['agentName'].'</option>';
							}
						} ?>
					
				</select>
			</span>
			<?php } ?>
            <span class="filterPart">
				<label class="filterLabel">Plan</label>
				<select id="planList" data-placeholder="Select Plan" class="form-control chosen-select chosen-select-plan" multiple>
					<?php if(isset($r_agent_plan) && $r_agent_plan){
							foreach($r_agent_plan as $plan){
								echo '<option value="'.$plan['plan_id'].'">'.$plan['friendlyname'].'</option>';
							}
					} ?>
					
				</select>
			</span>
			<span class="filterPart filterPartsoftware">
				<label class="filterLabel">Software</label>
				<select id="softwareList" data-placeholder="Select Software" class="form-control chosen-select chosen-select-software" multiple>
					<option value="1">QuickBooks Online</option>
					<option value="2">QuickBooks Desktop</option>
					<option value="3">Freshbooks</option>
					<option value="4">Xero</option>
					<option value="5">No Accounting Package</option>
					<option value="15">Not Boarded</option>
				</select>
			</span>

			<span class="filterPartBtn">
				
				<a id="btn-reset" class="btn btn-sm btn-grey" title="Reset Filter"  href="javascript:void(0);">Reset</a>
			</span>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="merchant_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <?php  if($login_info['login_type']=='AGENT'){
                    	$classHide = 'hidden';
                    }else{
                    	$classHide = '';
                    }     ?>
					<th class="text-left"> Merchant </th>
					<th class="text-right hidden-xs"> Volume</th>
					<th class="hidden-xs text-right" style="width:105px !important;"> Customers </th>
				    <th class="hidden-xs text-right">Plan</th>
				    <th class="hidden-xs text-right">Activation</th>
                    
                    <th class="text-center"> Action </th>
                    
                </tr>
            </thead>
            
        </table>
        <!--END All Orders Content-->
        
        
    </div>
    <!-- END All Orders Block -->

	<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ 
});</script>
<script>
$(document).ready(function() {
	callData();
	
    / Extend with date sort plugin /
            
} );
var Pagination_view = function() {

    return {
        init: function() {
            
        }
    };
}();


function set_del_merchant(id){
	
	  $('#merchID').val(id);
	
}

</script>	
	

<style>
@media screen and (min-width: 768px) and (max-width:1024px){
    
    
    .chosen-container-multi .chosen-choices li.search-field {
        padding: 6% 3% !important;
      
    }
}
.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}
.chosen-choices{
	padding: 2% 1% 0% 1% !important;
    background: #fff !important;
    border-color: #e8e8e8!important;
    max-height: 44px!important;
    overflow: auto !important;
}
li.search-choice {
    background: #a1a5a5!important;
    color: #fff !important;
    font-size: 12px!important;
    padding: 2% 12% 3% 3% !important;
    width: auto;
}
.chosen-container-multi .chosen-choices li.search-field{
	padding: 3%!important;
	background: #fff !important;
}
a.search-choice-close {
    top: 6px!important;
}
div.chosen-container.chosen-container-multi {
    top: -2px;
}
.dataTables_wrapper > div:first-child{
	
}
.btn-suspend{
	background: #F2990C;
    border-color: #F2990C;
}
.btn-suspend:hover,.btn-suspend:active,.btn-suspend:focus{
	background: #f2990cbd;
    border-color: #f2990cbd;
}
.btn-suspend:active:hover, .btn-suspend:hover, .open>.btn-suspend:hover, .btn-suspend:active:focus, .btn-suspend:focus, .open>.btn-suspend:focus, .btn-suspend:active.focus, .btn-suspend.active.focus, .open>.btn-suspend.focus {
    background-color: #f2990cbd;
    border-color: #f2990cbd;
}
</style>	
	
<!--------------------del admin Merchant------------------------>

<div id="del_merchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Disable Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="del_merchant" method="post" action='<?php echo $del_url; ?>' class="form-horizontal" >
                     
                 
					<p>Do you really want to Disable this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                            <input type="hidden" id="merchID" name="merchID" class="form-control"  value="" />
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-danger" value="Disable"  />
                    <button type="button" class="btn btn-sm close1 btn_can" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>


 </div>
 
 <!----------------------========================================Start of Suspend Modal====================================================------------------------>

<div id="suspend_merchant" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title" id="sus_txt">Suspend Merchant</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="suspend_merchant_form" method="post" action='' class="form-horizontal" >
                     
                 
					<p id="sub_content">Do you really want to suspend this Merchant?</p> 
					
				    <div class="form-group">
                     
                        <div class="col-md-8">
                          
                        </div>
                    </div>
                    
					
			 
                    <div class="pull-right">
        			 <input type="submit"  id="ddeee" name="btn_cancel" class="btn btn-sm btn-info btn-suspend" value="Suspend"  />
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
                    </div>
                    <br />
                    <br />
            
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>

<!--  End of suspended Modal  --->

<?php //****************************** To Suspend  and Resume Reseller ********************************** ?>

<script>
$( "#btn-reset" ).click(function() {
	$('#agentList').val('').trigger('chosen:updated');
	$('#planList').val('').trigger('chosen:updated');
	$('#softwareList').val('').trigger('chosen:updated');
	$('#agentIDStr').val('');
	$('#planIDStr').val('');
	$('#softwareIDStr').val('');

	callData();
  
});
$('.chosen-select-agent').chosen({}).change( function(obj, result) {
    var agentValue = $('#agentIDStr').val();
   	var value = result.selected;
    if (typeof value === "undefined") {
	    var removeItem = parseInt(arguments[1]['deselected']);
	    if(agentValue != ''){
	    	var myarray = agentValue.split(",");
	    	var y = jQuery.grep(myarray, function(value) {
			  return value != removeItem;
			});
			var x = y.toString();
			value = x;	
		}
	}else{
		if(agentValue != ''){
			value = agentValue+','+value;
		}
	}
	
	$('#agentIDStr').val(value);
    callData();
});
$('.chosen-select-plan').chosen({}).change( function(obj, result) {
    var planValue = $('#planIDStr').val();
   	var value = result.selected;
    if (typeof value === "undefined") {
	    var removeItem = parseInt(arguments[1]['deselected']);
	    if(planValue != ''){
	    	var myarray = planValue.split(",");
	    	var y = jQuery.grep(myarray, function(value) {
			  return value != removeItem;
			});
			var x = y.toString();
			value = x;	
		}
	}else{
		if(planValue != ''){
			value = planValue+','+value;
		}
	}
	$('#planIDStr').val(value);
	callData();
});
$('.chosen-select-software').chosen({}).change( function(obj, result) {
    var softwareValue = $('#softwareIDStr').val();
   	var value = result.selected;
    if (typeof value === "undefined") {
	    var removeItem = parseInt(arguments[1]['deselected']);
	    if(softwareValue != ''){
	    	var myarray = softwareValue.split(",");
	    	var y = jQuery.grep(myarray, function(value) {
			  return value != removeItem;
			});
			var x = y.toString();
			value = x;	
		}
	}else{
		if(softwareValue != ''){
			value = softwareValue+','+value;
		}
	}
	$('#softwareIDStr').val(value);
    callData();
});
function set_merchant_id(id, act, name,merchantID){
    
     var form = $("#suspend_merchant_form");
            $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											
											}).remove();
    if(act=='0')
    {
	                    	var url   = "<?php echo base_url()?>Reseller_panel/update_merchant_status";
									 
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID',
											'value': id
											}).appendTo(form);
											
										 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchantID',
											'name': 'merchantID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchantID',
											'name': 'merchantID',
											'value': merchantID
											}).appendTo(form);		
											
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											'value': act
											}).appendTo(form);
										$("#ddeee").addClass('btn-suspend');	
										 $("#ddeee").val('Suspend')	;							
		                	$("#sus_txt").html("Suspend Merchant");	
		                	  $("#sub_content").html("Are you sure you want to suspend "+name+"?");
				             $("#suspend_merchant_form").attr("action",url);
}else{
    
    	var url   = "<?php echo base_url()?>Reseller_panel/update_merchant_status";
									
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'resellerID',
											'name': 'resellerID',
											'value': id
											}).appendTo(form);
											
										 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchantID',
											'name': 'merchantID'
											
											}).remove();
									 $('<input>', {
											'type': 'hidden',
											'id'  : 'merchantID',
											'name': 'merchantID',
											'value': merchantID
											}).appendTo(form);		
											
								    $('<input>', {
											'type': 'hidden',
											'id'  : 'acct',
											'name': 'acct',
											'value': act
											}).appendTo(form);
										$("#sus_txt").html("Resume Merchant");	
									    $("#sub_content").html("Are you sure you want to resume "+name+"?");
									    $("#ddeee").removeClass('btn-suspend');
									  	$("#ddeee").val('Resume');						
				
				                          $("#suspend_merchant_form").attr("action",url);
}             

}

function callData(){
		$.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            var planIDStr = $('#planIDStr').val();
            var agentIDStr = $('#agentIDStr').val();
            var softwareIDStr = $('#softwareIDStr').val();
            var manageDisabledOption = $('#manageDisabledOption').val();
            $('#merchant_page').dataTable({
                "processing": true,
                "serverSide": true,
                "bDestroy": true,
                "ajax": '../Reseller_panel/serverProcessingMerchant?planFilter='+planIDStr+'&agentFilter='+agentIDStr+'&softwareFilter='+softwareIDStr+'&manageDisabledOption='+manageDisabledOption,
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [5] }
                ],
                "columns": [
                            {
                                "class" : "text-left  cust_view",    // or "className" : 
                            },
                 
                            {
                                 "class" : "text-right hidden-xs",
                            },
                            {
                                "class" : "text-right",    // or "className" : 
                            },
                 
                            {
                                 "class" : "text-right hidden-xs",
                            },
							{
                                 "class" : "text-right hidden-xs",
                            },

                            {
                                 "class" : "text-center hidden-xs",
                            }
                ],
                order: [[ 0, "asc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });
            / Add placeholder attribute to the search input /
           	$('.dataTables_filter input').attr('placeholder', 'Search');
	}
</script>
<script src="<?php echo base_url(JS); ?>/pages/chosen.jquery.min.js"></script>
