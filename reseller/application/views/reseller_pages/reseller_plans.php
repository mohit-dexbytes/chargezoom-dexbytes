<!-- Page content -->
<style>
    p, .table, .alert, .carousel {
    margin-bottom: -1px !important;
}

    .disabled {
    pointer-events:none; //This makes it not clickable
    opacity:0.6;         //This grays it out to look disabled
}
.greyColor{
    color: #ccc !important;
}
.dataTables_wrapper >.row >.col-sm-6.col-xs-7 {
    position: absolute;
    z-index: 9;
    top: 9px;
    left: 60px;
    width: 220px !important;
}
.btnRightCust{
    right: 0 !important;
}
</style>
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    <legend class="leg"> All Plans</legend>     
    <!-- All Orders Block -->
    <div class="block-main full" style="position: relative;">
	
        <!-- All Orders Title -->
        <?php if($settingEnableOption){ ?>
        <div class="addNewBtnCustom btnRightCust">
            <a class="btn  btn-sm btn-info" title=""  data-backdrop="static" data-keyboard="false" data-toggle="modal"  href="#settings" >Settings</a>
        </div>
        <?php } ?>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="plan_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-left">Friendly Name</th>
                     <th class="text-left">Plan ID</th>
                    <th class="text-right hidden-sm">Price</th>
                    
                   
                  
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(isset($resellers) && $resellers)
				{
				    
				    
					foreach($resellers as $reseller)
					{
				?>
				<tr>
					<td class="text-left cust_view">
                        <?php if($isEditMode){ ?>
                            <a href="#add_friendly_name"  onclick="set_plan_fname('<?php  echo $reseller['plan_id']?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"  title="Edit Plan" class=""  > <?php echo $reseller['friendlyname']; ?></a>
                                 
                        <?php }else{
                           echo '<a href="javascript:void(0)" disabled="disabled"  title="Edit Plan" class="greyColor"  > '.$reseller['friendlyname'].'</a>';
                            
                        } ?>    
                       
                            
                    </td>
					<td class="text-left"><?php echo $reseller['plan_name']; ?></td>
					<td class="text-right hidden-xs"><?php echo "$".number_format($reseller['subscriptionRetail'],2); ?></td>
				

				    
				</tr>
				
				<?php } }else{
                    echo'<tr>
                <td colspan="3"> No Records Found </td>
                <td style="display:none;">  </td>
                <td style="display:none;">  </td>
                </tr>'; 
                } ?>
				
			</tbody>
        </table>
        <!--END All Orders Content-->
        
        
    
    </div>
    <!-- END All Orders Block -->
 <div class="row">
    <div class="col-md-12">
    <div class="col-md-4">
    </div>    
    <div class="col-md-4">
    <div class="msg_data" style="margin-top:80%;z-index:9999999"><?php echo $this->session->flashdata('message');   ?> 
    </div> 
    </div>
    <div class="col-md-4">
    </div>
    </div>  
    </div>
</div>
<?php if($settingEnableOption){ ?>
<div id="settings" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
                <h2 class="modal-title">Default Downgrade Plan</h2>
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
                 <form id="defaultPlanForm" method="post" action='<?php echo base_url(); ?>Plan/updateDowngradePlan' class="form-horizontal" >
                    <input type="hidden" name="resellerID" id="resellerID" value="<?php echo $resellerID; ?>">
                    <div class="form-group ">
                                              
                        <label class="col-md-4 control-label" for="card_list">Plan</label>
                        <div class="col-md-6">
                           <select id="plan_id" name="plan_id"  class="form-control">
                                   <option value="" >Select Downgrade Plan</option>
                                    <?php if(isset($resellers) && $resellers)
                                        {
                                            foreach($resellers as $reseller)
                                            {  ?>
                                                <option <?php echo ($downgradePlanID == $reseller['plan_id'])?'selected':''; ?>  value="<?php echo $reseller['plan_id']; ?>" ><?php echo $reseller['friendlyname']; ?></option>
                                            <?php }
                                        } ?>
                                  
                            </select>
                        </div>
                        
                    </div>
                            
                    <div class="form-group ">
                        <div class="col-md-10 text-right">
                            <input type="submit"  name="btn_cancel1" class="btn btn-sm btn-success" value="Update"  />
                            <button type="button"   class="btn btn-sm btn-default close1 newCloseButton" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                    
                    
                </form> 
                 
            </div>
            
            
            <!-- END Modal Body -->
        </div>
    </div>
</div>  
<?php } ?> 
<div id="add_friendly_name" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header text-center">
               <h2 class="modal-title" id="title">Edit Plan</h2>
                
                 
            </div>
            <!-- END Modal Header -->

            <!-- Modal Body -->
            <div class="modal-body">
              <form method="POST" id="friendlyname_form" class="form form-horizontal" action="<?php echo base_url(); ?>Reseller_panel/reseller_planfname">
			<input type="hidden" id="planID" name="planID" value=""  />
			  
			    <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Plan ID </label>
							<div class="col-md-8">
                        <input type="text" id="plan_name" class="form-control"  value="" disabled>
								</div>
						<input type="hidden" value="" id="plan_id" name="plan_id">		
						</div>
						
					
                       </div> 
		        <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Friendly Name</label>
							<div class="col-md-8">
								<input type="text" id="friendlyname"  name="friendlyname" class="form-control"  value="" placeholder="Friendly Name"><?php echo form_error('friendlyname'); ?></div>
						</div>
						
					
                       </div>      
                      <div class="col-md-12">   
                     <div class="form-group">
                         	<div class="col-md-8 pull-right">
				 <label class="control-label" > <input type="checkbox" id="allowMerchant"  name="allowMerchant"  checked  /> Allow Merchant Gateway access</label>
					
                    </div>
						 
						
						
					</div>  
                       </div>     
                        
		      
			  <div class="form-group">
					<div class="col-md-4 pull-right">
					<button type="submit" class="submit btn btn-sm btn-success">Update</button>
                    <button type="button" class="btn btn-sm btn_can close1" data-dismiss="modal">Cancel</button>
					
                    </div>
                    
            </div>
			    </form>		
                
            </div>
            <!-- END Modal Body -->
        </div>
    </div>
</div>
<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init();});
</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#plan_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [1] },
                    { orderable: true, targets: [2] }
                ],
                searching: false,
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();


$('#friendlyname_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'friendlyname': {
                        required: true,
                        minlength: 3,
                         
                    }
			},
    }); 
$('#defaultPlanForm').validate({ // initialize plugin
    ignore:":not(:visible)",            
    rules: {
           'plan_id': {
                    required: true,
                     
                }
        },
});     
function set_plan_fname(pid){    
    var planid = pid;
    $('#planID').val(planid);
    
    $.ajax({
    url: '<?php echo base_url("Reseller_panel/get_plan_name")?>',
    type: 'POST',
	data:{plan_id:planid},
    success: function(data){
      var plan = jQuery.parseJSON(data);
	    $('#plan_name').val(plan.plan_name);
        if(plan.friendlyname){
	       $('#friendlyname').val(plan.friendlyname);
        }else{
           $('#friendlyname').val(plan.plan_name);
        }
	    
	    if(plan.gatewayAccess==1)
	      $('#allowMerchant').prop('checked','checked');
	      else
	         $('#allowMerchant').prop('checked','');
	}	
});
}
</script>

<style>

.table.dataTable {
  width:100% !important;
 }

@media screen and (max-width:352px){
 .table.dataTable {
  width:100% !important;
 }
  
 table.table thead .sorting_desc {
  padding-right: 0px !important;
 }
  
 table.dataTable thead > tr > th {
  padding-left: 5px !important;
  padding-right: 5px !important;
 }
}

</style>
<!------    Add popup form    ------->







