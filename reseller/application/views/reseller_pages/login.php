<!-- Login Alternative Row -->
<?php
	$this->load->view('alert');
?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<!-- Login Container -->
			<div id="login-container">
			<h1 class="push-top-bottom text-center">
					<img src="<?php echo base_url(IMAGES); ?>/chargezoom.png" alt="avatar"><br>
				</h1>
				
				<!-- Login Block -->
				<div class="block push-bit">
				         	<div class="msg_data ">
							 	<?php 
									$message = $this->session->flashdata('message');
									echo $message;
									
									if ($error = $this->session->flashdata('error')) { ?>
										<div class="alert alert-danger">
											<button data-dismiss="alert" class="close" type="button">×</button>
											<strong>Error:</strong> <?= $error ?>
										</div>
								<?php }
									if ($forgetSuccess = $this->session->flashdata('forget-success')) { ?>
										<div class="alert alert-success">A Password Reset request has been sent to your email on file.</div>
								<?php }
									$loginError = $this->session->flashdata('login-error');
									if (!empty($loginError)) {
										$strng1 = "Hmm. That didn't work.";
										$strng2 = "Let's get you back into your account.";
										$strng3 = '<a href="javascript:void(0)" id="link-reminder-login">Help me sign in </a>';
								?>
										<div class="alert alert-danger"> <h4 class="error_step1"><?= $strng1 ?></h4><h5 class="error_step2"><?= $strng2 ?></h5><h5 class="error_step3"><?= $strng3 ?></h5> </div>
								<?php } ?>
							</div>
					<!-- Login Form -->
					<form action="<?php echo base_url('login/user_login'); ?>" method="post" id="form-login" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="login-email" name="login-email"  class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="login-password" name="login-password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
							        <div class="col-xs-6 text-right">
									<input type="radio" name="type" value="1" checked="checked" > Reseller
									</div>
									<div class="col-xs-6">
									<input type="radio" name="type" value="0"> Agent
									</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-right">
								<a href="javascript:void(0)" id="link-reminder-login"><small>Forgot Password?</small></a> 
								
							</div>
						</div>
						<div class="form-group">
						<div class="col-xs-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton"> Login to Dashboard</button>
						</div>
						</div>
						<?php echo $this->czsecurity->appendFormCsrfToken(); ?>
					</form>
					<!-- END Login Form -->

					<!-- Reminder Form -->
					<form action="<?php echo base_url('login/recover_password'); ?>#reminder" method="post" id="form-reminder" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="reminder-email" name="reminder-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
							        <div class="col-xs-6 text-right">
									<input type="radio" name="type" value="1"  checked="checked" > Reseller
									</div>
									<div class="col-xs-6">
									<input type="radio" name="type" value="0"> Agent
									</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-right">
								<small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<button type="submit" class="btn login_click_btn redbutton"> Reset Password</button>
							</div>
						</div>
						<?php echo $this->czsecurity->appendFormCsrfToken(); ?>
					</form>
					<!-- END Reminder Form -->

					<!-- Register Form -->
					
				</div>
				<!-- END Login Block -->
			</div>
			<!-- END Login Container -->
			<!-- Footer -->
			<div class="footer">
				<footer class="text-muted push-top-bottom">
					<a href="http://chargezoom.com" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a>
			
				
				</footer>
			</div>
				<!-- END Footer -->
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->
    

<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){  Login.init(); });</script>
<style>
.footer {
    position: relative;
    top: 100px;
    text-align: center;
}
.redbutton {
    background: #FF4612 !important;
    color: #FFF !important;
    outline: 0 !important;
    height: 43px;
    width: 255px;
    border-radius: 3px;
}
body {
    background: #F0EFEF !important;
}
.push-bit {
    padding: 50px 20px 30px;
}

</style>
