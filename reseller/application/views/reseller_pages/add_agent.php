   <!-- Page content -->
   <?php
		$this->load->view('alert');
	?>
   <div id="page-content">
	 
	
<style>
 .error{color:red; }
 td,th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}
.headingRow th, .dataRow td {
    font-weight: 600;
}
.borderManageLabel {
    border: none;
}
th.borderManageHead {
    border: none;
    padding: 2% 6% 2% 6%;
    width: 20%;
}
table#checkboxTable {   
    width: 77%;
    float: right;
}
td.checkboxAlign {
    padding: 2% 6% 2% 8%;
    width: 20%;
}
.DisableCheck {
    pointer-events: none;
}
</style>   
	
	<div class="msg_data ">
      <?php  
		$message = $this->session->flashdata('message');
		if(isset($message) && $message != "")
		echo $message;
		?> 
    </div>
<?php   if(isset($agent['agentName'])){   ?>
 	<form method="POST" id="edit_form" class="form form-horizontal" action="<?php echo base_url(); ?>SettingAgent/create_agent"> 
	<?php }else{ ?>
    <form method="POST" id="agent_form" class="form form-horizontal" action="<?php echo base_url(); ?>SettingAgent/create_agent"> 
    <?php } ?>
    <!-- END Wizard Header -->
    <legend class="leg"> <?php if(isset($merchant)){ echo "Edit Agent";}else{ echo "Create New Agent"; }?></legend>   
    <!-- Progress Bar Wizard Block -->
    <div class="block full">
	               
         
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
            
	        <input type="hidden"  id="agID" name="agID" value="<?php if(isset($agent['agentName'])){echo $agent['ragentID']; } ?>" /> 
	
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-username">Name</label>
						<div class="col-md-8">
							<input type="text" id="agent_name" name="agent_name" class="form-control"  value="<?php if(isset($agent['agentName'])){ echo $agent['agentName']; } ?>" placeholder="Name">
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label" for="example-username">Email</label>
						<div class="col-md-8">
							<input type="text" id="agent_email" name="agent_email" class="form-control" value="<?php  if(isset($agent['agentEmail'])){ echo $agent['agentEmail']; } ?>" placeholder="Email">
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<?php if($login_info['login_type']=='RESELLER' || ( $login_info['login_type']=='AGENT' ) )
						{   ?>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Password</label>
							<div class="col-md-8">
								<input type="password" autocomplete="off" id="agent_password" name="agent_password" class="form-control"  value="" placeholder="**********">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Confirm Password</label>
							<div class="col-md-8">
								<input type="password" autocomplete="off" id="conf_password" name="conf_password" class="form-control"  value="" placeholder="***********">
							</div>
						</div>
				  <?php  } ?>

				  <?php if(!isset($agent)) { ?> 
				  	<div class="form-group ">
				  		<div class="col-md-12">
							<label class="col-md-11 control-label no-padding" for="example-username">Email Agent</label>
					
							<div class="col-md-1 " style="position: relative;right: -11px;">
						  		<label class="control-label">   
						  			<input type="checkbox" id="agentmail" name="agentmail"  value="1"></label>
						  	</div>
						</div>
					</div>
					<?php } ?>
	                <div class="form-group pull-right">
						<div class="col-md-12">
								
							<button type="submit" class="submit btn btn-sm btn-success">Save</button>			
							<a href="<?php echo base_url('SettingAgent/reseller_agents'); ?>" class="btn btn-sm btn_can ">Cancel</a>
						</div>
				    </div>	
				</div>
			</div>
		  	
  	
  	
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->
    <legend class="leg">Permissions</legend>   
    <!-- Progress Bar Wizard Block -->
    	<div class="block full">
        <!-- Progress Bar Wizard Content -->
        	<div class="row">
        		<div class="col-md-12">
					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="Dashboard">Dashboard</label>
							<div class="col-md-8">
								<select id="dashboardPermission" class="form-control col-md-12" name="dashboardPermission" >
									
									<option value="1" <?php if(isset($agent) && $agent['dashboardPermission']== 1 ){ echo "Selected"; } ?> >Show All</option>
									<option value="2" <?php if(isset($agent) && $agent['dashboardPermission']== 2 ){ echo "Selected"; } ?>>Show User</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="agentManagement">Agent Management</label>
							<div class="col-md-8">
								<select id="agentManagement" class="form-control col-md-12" name="agentManagement" >
									
									<option value="1" <?php if(isset($agent) && $agent['agentManagement']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['agentManagement']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['agentManagement']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="generalSettings">General Settings</label>
							<div class="col-md-8">
								<select id="generalSettings" class="form-control col-md-12" name="generalSettings" >
									
									<option value="1" <?php if(isset($agent) && $agent['generalSettings']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['generalSettings']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['generalSettings']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="plans">Plans</label>
							<div class="col-md-8">
								<select id="plans" class="form-control col-md-12" name="plans" >
									
									<option value="1" <?php if(isset($agent) && $agent['plans']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['plans']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['plans']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="emailTemplates">Email Templates</label>
							<div class="col-md-8">
								<select id="emailTemplates" class="form-control col-md-12" name="emailTemplates" >
									
									<option value="1" <?php if(isset($agent) && $agent['emailTemplates']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['emailTemplates']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['emailTemplates']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="merchantPortal">Merchant Portal</label>
							<div class="col-md-8">
								<select id="merchantPortal" class="form-control col-md-12" name="merchantPortal" >
									
									<option value="1" <?php if(isset($agent) && $agent['merchantPortal']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['merchantPortal']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['merchantPortal']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="api">API</label>
							<div class="col-md-8">
								<select id="api" class="form-control col-md-12" name="api" >
									
									<option value="1" <?php if(isset($agent) && $agent['api']== 1 ){ echo "Selected"; } ?> >Hide</option>
									<option value="2" <?php if(isset($agent) && $agent['api']== 2 ){ echo "Selected"; } ?> >View</option>
									<option value="3" <?php if(isset($agent) && $agent['api']== 3 ){ echo "Selected"; } ?> >Edit</option>
								</select>
							</div>
						</div>
					</div>
					

					<div class="col-md-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="merchantManagement">Merchant Management</label>
							<div class="col-md-8">
								
							</div>
						</div>
						<div class="form-group">
							<div class="tableMatrix">
								<table id="checkboxTable">
									<tr class="headingRow">
									    <th class="borderManageLabel"></th>
									    <th class="borderManageHead">View</th>
									    <th class="borderManageHead">Edit</th>
									    <th class="borderManageHead">Add</th>
									    <th class="borderManageHead">Disable</th>
									</tr>
									<tr class="dataRow">
									    
									    <td class="borderManageLabel">User</td>
									    <td class="checkboxAlign">
									    	<?php 
									    		$DisableCheckClass = '';
								    			$DisableCheckClassView = '';
								    			$DisableCheckClassEdit = '';
								    			$DisableCheckClassAdd = '';
									    		if(isset($agent) && $agent['allMerchantView']== 1 ){
									    			$DisableCheckClassView = 'DisableCheck';
									    		}else if(isset($agent) && $agent['allMerchantEdit']== 1  ){
									    			$DisableCheckClassEdit = 'DisableCheck';
									    		}else if(isset($agent) && $agent['allMerchantAdd']== 1 ){
									    			$DisableCheckClassAdd = 'DisableCheck';
									    		}else if(isset($agent) && $agent['allMerchantDisabled'] == 1 ){
									    			$DisableCheckClass = 'DisableCheck';
									    		}else{

									    		}
									    	 ?>
									    	<input class="optionCheck <?php echo $DisableCheckClassView; ?>" type="checkbox" id="userMerchantView" name="userMerchantView" <?php if(isset($agent) && $agent['userMerchantView']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['userMerchantView']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck <?php echo $DisableCheckClassEdit; ?>" type="checkbox" id="userMerchantEdit" name="userMerchantEdit" <?php if(isset($agent) && $agent['userMerchantEdit']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['userMerchantEdit']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck <?php echo $DisableCheckClassAdd; ?>" type="checkbox" id="userMerchantAdd" name="userMerchantAdd" <?php if(isset($agent) && $agent['userMerchantAdd']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['userMerchantAdd']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck <?php echo $DisableCheckClass; ?>" type="checkbox" id="userMerchantDisabled" name="userMerchantDisabled" <?php if(isset($agent) && $agent['userMerchantDisabled']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['userMerchantDisabled']=='1' )?1:0; ?>">
									    </td>
									</tr>
									<tr class="dataRow">
									   
									    <td class="borderManageLabel">All</td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck" type="checkbox" id="allMerchantView" name="allMerchantView" <?php if(isset($agent) && $agent['allMerchantView']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['allMerchantView']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck" type="checkbox" id="allMerchantEdit" name="allMerchantEdit" <?php if(isset($agent) && $agent['allMerchantEdit']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['allMerchantEdit']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck" type="checkbox" id="allMerchantAdd" name="allMerchantAdd" <?php if(isset($agent) && $agent['allMerchantAdd']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['allMerchantAdd']=='1' )?1:0; ?>">
									    </td>
									    <td class="checkboxAlign">
									    	<input class="optionCheck" type="checkbox" id="allMerchantDisabled" name="allMerchantDisabled" <?php if(isset($agent) && $agent['allMerchantDisabled']=='1' ){ echo "checked='checked'"; } ?> value="<?php echo (isset($agent) && $agent['allMerchantDisabled']=='1' )?1:0; ?>">
									    </td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
        	</div>
        </div>
    </form>

<!-- END Page Content -->


<script>
$(document).ready(function(){
	$('.optionCheck').click(function(){
		var selectID = $(this).attr('id');
		
        if($(this).prop("checked") == true){
        	if(selectID == 'allMerchantView'){
        		
        		$('#userMerchantView').prop('checked',true);
        		$('#userMerchantView').addClass('DisableCheck');
        		$('#userMerchantView').val(1);
        	}else if(selectID == 'allMerchantEdit'){
        		$('#userMerchantEdit').prop('checked',true);
        		$('#userMerchantEdit').addClass('DisableCheck');
        		$('#userMerchantEdit').val(1);
        	}else if(selectID == 'allMerchantAdd'){
        		$('#userMerchantAdd').prop('checked',true);
        		$('#userMerchantAdd').addClass('DisableCheck');
        		$('#userMerchantAdd').val(1);
        	}else if(selectID == 'allMerchantDisabled'){
        		$('#userMerchantDisabled').prop('checked',true);
        		$('#userMerchantDisabled').addClass('DisableCheck');
        		$('#userMerchantDisabled').val(1);
        	}
            $(this).val(1);
        }
        else if($(this).prop("checked") == false){
        	if(selectID == 'allMerchantView'){
        		$('#userMerchantView').removeClass('DisableCheck');
        	}else if(selectID == 'allMerchantEdit'){
        		$('#userMerchantEdit').removeClass('DisableCheck');
        	}else if(selectID == 'allMerchantAdd'){
        		$('#userMerchantAdd').removeClass('DisableCheck');
        	}else if(selectID == 'allMerchantDisabled'){
        		$('#userMerchantDisabled').removeClass('DisableCheck');
        	}
        	$(this).val(0);
        }
    });
    $('#agent_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'agent_name': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'agent_email': {
                        required: true,
                        minlength:3,
                        email:true,
                    },
					
			
                    'agent_password': {
                        required: true,
                        minlength:5,
                    },
					'conf_password': {
                        required: true,
                        equalTo: '#agent_password',
                    }, 
                    
                   'agent_commission': {
                        required: true,
                        number:true
                    },
                   
			
			},
    });

        $('#edit_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'agent_name': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'agent_email': {
                        required: true,
                        minlength:3,
                        email:true,
                    },
					
			
                    'agent_password': {
                       
                        minlength:5,
                    },
					'conf_password': {
                       
                        equalTo: '#agent_password',
                    }, 
                    
                   'agent_commission': {
                        required: true,
                        number:true
                    },
                   
			
			},
    });


});	    
		
	

</script>

</div>


