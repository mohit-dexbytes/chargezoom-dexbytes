

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-settings"></i>Setup Merchant Profile<br><small>Break easily your initial setup into steps!</small>
            </h1>
        </div>
    </div>
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		 <div class="progress">
             <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
          </div>
		 	<form method="POST" class="form form-horizontal"  enctype="multipart/form-data"  action="<?php echo base_url(); ?>home/dashboard_first_login">
		       <div class="step">
		
					<h3><small><strong>Step 1-5:</strong> Setup your profile information.</small><br><hr></h3>
					<div class="col-sm-6">
					
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Company Name</label>
							<div class="col-md-8">
								<input type="text" id="companyName" name="companyName" class="form-control" value="<?php echo ($login_info['companyName'])?$login_info['companyName']:''; ?>" placeholder="Your company name.."><?php echo form_error('companyName'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 1</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress1" name="companyAddress1"   value="<?php echo ($login_info['merchantAddress1'])?$login_info['merchantAddress1']:''; ?>" class="form-control" placeholder="Address line 1.."><?php echo form_error('companyAddress1'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-username">Address Line 2</label>
							<div class="col-md-8">
								<input type="text" id="companyAddress2" name="companyAddress2"  class="form-control"  value="<?php echo ($login_info['merchantAddress2'])?$login_info['merchantAddress2']:''; ?>"  placeholder="Address line 2 (Optional)..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Email</label>
							<div class="col-md-8">
								<input type="text" id="companyEmail" readonly name="companyEmail" class="form-control" value="<?php echo ($login_info['merchantEmail'])?$login_info['merchantEmail']:''; ?>"  placeholder="test@example.com">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Phone</label>
							<div class="col-md-8">
								<input type="text" id="companyContact" name="companyContact" class="form-control" value="<?php echo ($login_info['merchantContact'])?$login_info['merchantContact']:''; ?>" placeholder="Your contact number.."><?php echo form_error('companyContact'); ?>
							</div>
						</div>
					</div>
				
					<div class="col-sm-6">
					
					<div class ="form-group">
						   <label class="col-md-4 control-label" for="example-typeahead">Select Country</label>
						   <div class="col-md-8">
								<select id="country" class="form-control " name="companyCountry" >
								<option value="Select Country">... Select Country ....</option>
								<?php foreach($country_datas as $country){  ?>
								   <option value="<?php echo $country['country_id']; ?>" <?php if(isset($login_info)){ if($login_info['merchantCountry']==$country['country_id'] ){ ?> selected="selected" <?php }  ?> <?php }?>> <?php echo $country['country_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
					
						<div class ="form-group">
						   <label class="col-md-4 control-label"  name="state" for="example-typeahead">Select State</label>
						   <div class="col-md-8">
								<select id="state" class="form-control input-typeahead" name="companyState">
								<option value="Select State">... Select State ....</option>
								   <?php foreach($state_datas as $state){  ?>
								   <option value="<?php echo $state['state_id']; ?>"<?php if(isset($login_info)){if($login_info['merchantState']==$state['state_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $state['state_name']; ?> </option>
								   
								<?php } ?>  
								</select>
							</div>
                        </div>	
						<div class ="form-group">
						   <label class="col-md-4 control-label" name="city" for="example-typeahead">Select City</label>
						   <div class="col-md-8">
								<select id="city" class="form-control input-typeahead" name="companyCity">
								<option value="Select City">... Select City ....</option>
								   <?php foreach($city_datas as $city){  ?>
								   <option value="<?php echo $city['city_id']; ?>" <?php if(isset($login_info)){if($login_info['merchantCity']==$city['city_id'] ){ ?> selected="selected" <?php }  ?><?php }  ?>><?php echo $city['city_name']; ?> </option>
								   <?php } ?> 
								</select>
							</div>
                        </div>	
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-email">Zip Code</label>
							<div class="col-md-8">
								<input type="text" id="zipCode" name="zipCode" class="form-control" value="<?php echo ($login_info['merchantZipCode'])?$login_info['merchantZipCode']:''; ?>" placeholder="Zip code.."><?php echo form_error('zipCode'); ?>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Alternate Phone</label>
							<div class="col-md-8">
								<input type="text" id="alternateContact" name="alternateContact" class="form-control"
								value="<?php echo ($login_info['merchantAlternateContact'])?$login_info['merchantAlternateContact']:''; ?>" placeholder="Your alternate contact number (Optional)..">
							</div>
						</div>
					</div>  
						
						</div>
			<div  class="step">
					<h3><small><strong>Step 2-5:</strong> Setup customer portal and logo.</small><br><hr></h3>
					<div class="col-sm-6">
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-firstname">Tagline</label>
							<div class="col-md-8">
								<input type="text" id="tagline" name="tagline"  value="<?php  echo ($login_info['merchantTagline'])?$login_info['merchantTagline']:''; ?>" class="form-control" placeholder="Enter tagline for your customer portal..">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-lastname">Portal URL</label>
							
							<div class="col-md-8">
								<div class="input-group">
								<input type="text" id="portal_url" name="portal_url" value="<?php  echo ($login_info['portalprefix'])?$login_info['portalprefix']:''; ?>" class="form-control" placeholder="">
								<span class="input-group-btn"><a class="btn btn-primary">.chargezoom.net </a> </span>
									
                                   </div>
                              </div>
						
						
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-textarea-input">Address</label>
							<div class="col-md-8">
								<textarea id="companyFullAddress" name="companyFullAddress" rows="3" class="form-control" placeholder="Your company name and address (as it should appear on an invoice).."><?php  echo ($login_info['merchantFullAddress'])?$login_info['merchantFullAddress']:''; ?></textarea>
							</div>
						</div>
					</div>
					<div class="col-sm-6">
						
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-file-input">Upload Logo</label>
							<div class="col-md-8">
							<?php if(isset($login_info)){ $mgurl = $login_info['merchantProfileURL'];  } ?>
							
							 <span id="fileselector">
								<label class="btn btn-default" for="upload-file-selector">
									<input id="upload-file-selector"  name="picture" type="file">
									<i class="fa_icon icon-upload-alt margin-correction"></i>upload file
								</label>
							</span>
								
								<br>
								 <?php if($mgurl!=""){  echo '<img scr="'.base_url().'uploads/merchant_logo/'.$mgurl.'"  />'; }  ?>
								<span class="text-danger"><strong>Recommended Logo Size is 200 x 50</strong></span>
							</div>
						</div>
					</div>
				</div>
		<div class="step">
		
		<h3><small><strong>Step 3-5:</strong> Connect your Merchant Account .</small><br><hr></h3>
					<div class="col-sm-6" id="nmitest">
					
					   	 <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt"  class="form-control">
								
								   <option value="1" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1') echo "Selected"; ?> >Chargezoom</option>
								   <option value="2" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2') echo "Selected"; ?>  >Authorize.Net</option>
								   <option value="3" <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='3') echo "Selected"; ?>  >Pay Trace</option>
							</select>
							</div>
						</div>	
				  <div id="nmi_div2"   >				
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">NMI User</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control";  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="Your  Username..">
                        </div>
                    </div>
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                        <div class="col-md-6">
                            <input type="password" id="nmiPassword" name="nmiPassword" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='1'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Your  password..">
                           
                        </div>
                    </div>		
						
				</div>	
				  <div id="auth_div2" style="display:none">					
					
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?> placeholder="API LoginID..">
                        </div>
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="Transaction Key..">
                           
                        </div>
                    </div>
				</div>	
				
			<div id="pay_div2" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control";  <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayUsername'];  ?>" <?php }else{ echo "value='' "; }  ?>   placeholder="PayTrace  Username..">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="password" id="paytracePassword" name="paytracePassword" class="form-control"   <?php if(isset($gateway_data) && $gateway_data['gatewayType']=='2'){ ?>  value="<?php echo  $gateway_data['gatewayPassword'];  ?>" <?php }else{ echo "value='' "; }  ?>  placeholder="PayTrace  password..">
                           
                        </div>
                    </div>
				</div>	
				
		         <div class="col-md-12">   
                     <div class="form-group">
							<label class="col-md-4 control-label" for="example-username"> Merchant ID</label>
						<div class="col-md-6">
							<input type="text" id="mid"  name="mid" class="form-control"   value="<?php if(isset($gateway_data)) echo $gateway_data['gatewayMerchantID'];  ?>" placeholder="Enter merchant ID"> 
						</div>
			      </div>
             </div> 
					
				
				
				   <div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						 <div class="col-md-6">
						   <input type="text" id="gfrname" name="gfrname"  class="form-control" value="<?php if(isset($gateway_data)) echo $gateway_data['gatewayFriendlyName'];  ?>" placeholder="Enter your friendly name" />
								
							</div>
				  </div>	
			       
				
					</div>
				
						
						
						
						</div>
		              <div  
		              
		              class="step">
					<h3><small><strong>Step 4-5:</strong> Quickbooks Webconnector File</small><br><hr></h3>
					
					<?php if(isset($login_info['fileID']) && !empty($login_info['fileID']) ){  ?>
					
						<div class="col-sm-12">
					
					   <div class="block"  >
                
						<div class="block-title">
							<h2><strong>Quickbooks</strong> Webconnector File</h2>
						</div>
						
						<h4>Click "Download Web Connector File" to begin automatically syncing QuickBooks data."</h4>
						<br>
				<a href="<?php echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file'.$file_user.'.qwc'; ?>"    class="btn btn-sm btn-info donfile" id="dowlloadID1">Download Web Connector File</a>
						<br>
						<br>
						
					  </div>
					
					</div>
					
					<?php }else{  ?>
					
					<div class="col-sm-12">
					
					   <div class="block" id="qbwwcid" style="display:none;" >
                
						<div class="block-title">
							<h2><strong>Quickbooks</strong> Webconnector File</h2>
						</div>
						
						<h4>Click "Download Web Connector File" to begin automatically syncing QuickBooks data."</h4>
						<br>
						<a href="<?php echo base_url() ?>QuickBooks/your_function/my-quickbooks-wc-file<?php echo $this->session->userdata('logged_in')['merchID'].'.qwc'; ?>"    class="btn btn-sm btn-info donfile" id="dowlloadID2" > Download Web Connector File</a>
						<br>
						<br>
						
					  </div>
					
					 <div class="block" id="qbwwcid1"  >
						<h4>You can create a webconnector file to get QuickBooks data.</h4>
					
						  <a href="#qb-connecter-settings" class="btn btn-sm btn-info" id="dddddc"  data-backdrop="static" data-keyboard="false" data-toggle="modal">Setup QuickBooks</a>
						<br>
						<br>
						<br>
					</div>
					</div>
					
					<?php } ?>
				</div>
				<!-- END Fourth Step -->
				
				<!-- Fifth Step -->
			<div  class="step">
					<h3><small><strong>Last Step:</strong> Import data from your QuickBooks (Clients, Products & Invoices).</small><br><hr></h3>
					<div class="col-sm-6">	
						<div class="form-group">
							<label class="col-md-4 control-label" for="example-password">Import QB Data</label>
							<div class="col-md-8">
								<button id="qb-info" type="button" class="btn btn-sm btn-info"   >QuickBooks Setup Instructions</a>
							</div>
						</div>
					</div>
				</div>
						
						
	              <div class="form-group ">
					<div class="col-md-8 col-md-offset-10">
						<button type="button" class="action back btn btn-sm btn-warning ">Back</button>
						<button type="button" class="action next btn btn-sm btn-success ">Next</button>
						<button type="submit" class="action submit btn btn-sm btn-success">Submit</button>	
					
					</div>
				</div>					
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->
<style>
.overlay {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: transparent;
    background-color: transparent;
    overflow-x: hidden;
    transition: 0.5s;
}


</style>
 <?php  $complete =  $this->session->userdata('logged_in');
        
		  $completed =0;
		 
	    
	
          if($complete['companyName']!="" && $complete['merchantAddress1'] !=""){
			  $completed = 1;
		  }	 
	   if($complete['merchantFullAddress']!="" ){
			 
			  $completed = 2;
		  }		
	    if($complete['merchant_gateway'] =="1" ){
			 
	
			  $completed = 3;
		  }
		 if($complete['fileID']!="0" ) {
			  
			  $completed = 4;
		  }
		  
   
?>

<script>


$(document).ready(function(){

	
	 $('#gateway_opt').change(function(){
		    var gateway_value =$(this).val();
           if(gateway_value=='3'){			
		       $('#pay_div2').show();
			   $('#auth_div2').hide();
				$('#nmi_div2').hide();
		   }else if(gateway_value=='2'){
				$('#auth_div2').show();
				$('#pay_div2').hide();
				$('#nmi_div2').hide();
		   }else{
			   $('#pay_div2').hide();
			$('#auth_div2').hide();
			$('#nmi_div2').show();		
           }
	  }); 
	  
	
	
	
	 
	var current = <?php echo $completed ?>;

	
	widget      = $(".step");
	btnnext     = $(".next");
	btnback     = $(".back"); 
	btnsubmit   = $(".submit");
   
	// Init buttons and UI
	widget.not(':eq(0)').hide();
	 if(current == 4)
			       hideButtons(current+1);
	else hideButtons(current);
	setProgress(current);
	
     if(current < widget.length){
			// Check validation
			 
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
			     hideButtons(current);
			     if(current == 4)
				btnnext.prop("disabled", true);
		}
	// Next button click action
	btnnext.click(function(){
		if(current < widget.length){
			// Check validation
			if($(".form").valid()){
				insert_form_data(current);
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
			}
		}
		hideButtons(current);
	})
 
	// Back button click action
	btnback.click(function(){
		if(current > 1){
			current = current - 2;
			if(current < widget.length){
				
				widget.show();
				widget.not(':eq('+(current++)+')').hide();
				setProgress(current);
				btnnext.prop("disabled", false);
			}
		}
		hideButtons(current);
	})
 

 
    $('.form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'companyName': {
                        required: true,
                         minlength: 3
                    },
                    'companyAddress1': {
                        required: true,
                        minlength: 5
                    },
					  'companyCountry': {
                        required: true,
                         
                    },
                    'companyState': {
                        required: true,
                        
                    },
					  'companyCity': {
                        required: true,
                         
                    },
					  'companyContact': {
                        required: true,
                         minlength: 3
                    },
                    'zipCode': {
                        required: true,
                        minlength: 5
                    },
                    
                        'companyFullAddress':{
						 required: true,
                        minlength: 5
					},
					
					  'portal_url':{
						  "remote" :function(){return Chk_url('portal_url');} 
					},
                    
				 'nmiUser': {
                 required: true,
                 minlength: 3
                },
				
					'nmiPassword':{
						 required : true,
						 minlength: 3,
					  },	
					'gfrname':{
						 required : true,
						 minlength: 3,
						 "remote" :function(){return Chk_fname('gfrname');} 
					},			
				  'qbcompany_name':{
				         required : true,
						 minlength: 3,
				  
				  }	  	
		   
			
			},
    });
	
	

	
	
	    var insert_form_data= function(current){
				var dadadad  = $('.form').serialize()+'&action='+current;
				
			$.ajax({
		       beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
					
					 },
			 complete:function() {
				   $(".overlay1").remove();
			 },
			 url: '<?php echo base_url(); ?>home/dashboard_first_login_ajaxdata',
             type: "post",
			 data: dadadad,
			 success: function(response){
				 
				   data=$.parseJSON(response);
                             if (response['status'] === 'success') {
				
					        return true;
				     }
             }			 
			});
		}
	
	
	 var Chk_url=function(element_name){ 
       
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>SettingConfig/check_url',
                    type: "post",
					data:
                        {
                            portal_url: function(){return $('input[name=portal_url]').val();},
                        },
                        dataFilter:function(response){
						
						   data=$.parseJSON(response);
						
                             if (data['status'] == 'true') {
                                  
							
							    
						     	$('#portal_url').removeClass('error');	 
								 $('#portal_url-error').hide();	 
								 
								 
									   return data['status'] ;				   
									 
                              } else {
                               
								   if(data['status'] =='false'){
								      
                                       $('.form').validate().showErrors(function(){
										   
                                                  return {key:'portal_url'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	
	
       var Chk_fname=function(element_name){ 
           
           
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Payments/chk_friendly_name',
                    type: "post",
					data:
                        {
                            frname: function(){return $('input[name=gfrname]').val();},
                        },
                        dataFilter:function(response){
							
						   data=$.parseJSON(response);
						
                             if (data['status'] == 'true') {
                                  
							
							    
						     	$('#gfrname').removeClass('error');	 
								 $('#gfrname-error').hide();	 
								  $('.next').removeAttr('disabled');
								 
									   return data['status'] ;				   
									 
                              } else {
                                $('.next').attr("disabled", true); 
								   if(data['status'] =='false'){
                                       $('.form').validate().showErrors(function(){
										   
                                                       return {key:'gfrname'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
	
	
       var GetMyRemote=function(element_name){
            $('.next').attr("disabled", true); 
           
            remote={
				
				     beforeSend:function() { 
						 $("<div class='overlay1'> <img src='<?php echo base_url(); ?>uploads/loading.gif'   style='position:absolute;top:40%;left:40%;z-index:2000' id='loading-excel' class='' /> </div>").appendTo("body");
						
					 },
					 complete:function() {
						   $(".overlay1").remove();
					 },
                     url: '<?php echo base_url(); ?>Payments/testNMI',
                    type: "post",
					data:
                        {
                            nmiuser: function(){return $('input[name=nmiUser]').val();},
                            nmipassword:  function(){return $('input[name=nmiPassword]').val();}
                        },
                        dataFilter:function(response){
							
						   data=$.parseJSON(response);
                             if (data['status'] == 'true') {
                                  
									
							     $('.next').removeAttr('disabled');
						     	$('#nmiPassword').removeClass('error');	 
								 $('#nmiPassword-error').hide();	 
								  
									   return data['status'] ;				   
									 
                              } else {
                                
								   if(data['status'] =='false'){
                                       $('.form').validate().showErrors(function(){
										   
                                                       return {key:'nmiPassword'}; 
                                                       })
								   }					   
                               }
							   
                               return JSON.stringify(data[element_name]);
                         }
                    }
                                             
            return remote;
        }		
		
		
	
	$('#qb-generate').click( function(){ 
	
	  
	      if($('#qbpassword').val()!="" && $('#qbpassword').val()===$('#qbrepassword').val() ) { 
	         
	      var uname = $('#qbusername').val(); 
			var url   = "<?php echo base_url().'QuickBooks/your_function/my-quickbooks-wc-file';?>"+uname+'.qwc';
			$("#dowlloadID1").attr("href",url);
			$("#dowlloadID2").attr("href",url);
			
		  $('#qb-connecter-settings .close1').click();
		  $('.next').removeAttr('disabled');
		  $('#qbwwcid').show();
		  $('#qbwwcid1').hide();
		  }
		  
	});
	
	
	
    $('#qb-info').click(function(){
	      
	     $('#qb-connecter-info').modal('show');
		  $('.submit').removeAttr('disabled');
		
	});	
	
	$('.donfile').click(function(){
	
		$('.next').removeAttr('disabled');
		
		
	});	
	
	nmiValidation222.init();
	
 
});
 
// Change progress bar action
setProgress = function(currstep){
	var percent = parseFloat(100 / widget.length) * currstep;
	percent = percent.toFixed();
	if(percent==20)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                  
                        .attr('aria-valuenow', '20')
                        .removeClass('progress-bar-warning progress-bar-info progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-danger');
   if(percent==40)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                     .attr('aria-valuenow', '40')
                        .removeClass('progress-bar-danger progress-bar-info progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-warning');		
  if(percent==60)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                     .attr('aria-valuenow', '60')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-primary progress-bar-success')
                        .addClass('progress-bar-info');		
  if(percent==80)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                     .attr('aria-valuenow', '80')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-info progress-bar-success')
                        .addClass('progress-bar-primary');
  if(percent==100)
	$(".progress-bar").css("width",percent+"%").html(percent+"%")	
	                   .attr('aria-valuenow', '100')
                        .removeClass('progress-bar-danger progress-bar-warning progress-bar-info progress-bar-primary')
                        .addClass('progress-bar-success');							
						
						
}
 
// Hide buttons according to the current step
hideButtons = function(current){
	var limit = parseInt(widget.length); 
 
	$(".action").hide();
 
	if(current < limit) btnnext.show();
	if(current > 1) btnback.show();
	if (current == limit) { 
		btnnext.hide(); 
		btnsubmit.show();
		btnsubmit.prop("disabled", true);
	}
}



$('#country').change(function(){
    var country_id = $(this).val();
    $("#state > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('home/populate_state'); ?>",
        data: {id: country_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#state');
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['state_id'], text: data[val]['state_name'] }).appendTo(s);
           }
			
        }
    });
});

$('#state').change(function(){
    var state_id = $(this).val();
    $("#city > option").remove();
    $.ajax({
        type: "POST",
        url: "<?php echo base_url('home/populate_city'); ?>",
        data: {id: state_id},
        dataType: 'json',
        success:function(data){
			
			var s = $('#city');
			
			 
			   for(var val in  data) {
         
          $("<option />", {value: data[val]['city_id'], text: data[val]['city_name'] }).appendTo(s);
           }
		}
    });
});
	

var nmiValidation222 = function() {  
    
    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#Qbwc_form').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    
					qbcompany_name:{
					    	  required: true,
                        minlength: 3
					    
					},
				    qbpassword: {
							  required: true,
                        minlength: 5
						},
				
					qbrepassword:{
					    
					       required: true,
                         equalTo: '#qbpassword'
					},
                    	
					
                },
               
            });
					
		
						

        }
    };
}();



</script>

</div>

