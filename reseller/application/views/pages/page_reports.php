
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i>All Reports<br><small> You can see your all invoices activity reports here!</small>
            </h1>
        </div>
    </div>
  
    <!-- END Forms General Header -->

    <div class="row">  
	
		<div class="msg_data "><?php echo $this->session->flashdata('message');   ?></div>
	
	<div class="block full">
	    <div class="block-title">
            <h2><strong>All</strong> Customers</h2>
            
        </div>
      
      
      <form method="post" action="<?php echo base_url().'report/transaction_reports' ?>" >
       
       <div class="col-md-3">
           
         <select name="report_type" id="report_type"  class="form-control">
            <option value="1"    <?php if(isset($report_type) &&  $report_type=='1') echo "selected"; ?>  >Top 10 Due by Customer</option>
            <option value="2"  <?php if(isset($report_type) && $report_type=='2') echo "selected"; ?>>Top 10 Past Due by Customer</option>
            <option value="3"  <?php if(isset($report_type) && $report_type=='3') echo "selected"; ?>>Top 10 Past Due Invoices by Amount</option>
              <option value="4"  <?php if(isset($report_type) && $report_type=='4') echo "selected"; ?>>Top 10 Past Due Invoices by Days</option>
            <option value="5"  <?php if(isset($report_type) && $report_type=='5') echo "selected"; ?>>Failed Transactions in Last 30 Days</option>
            <option value="6"  <?php if(isset($report_type) && $report_type=='6') echo "selected"; ?>>Accounts with Credit Cards Expiring</option>
              <option value="7" <?php if(isset($report_type) && $report_type=='7') echo "selected"; ?> >Transaction Report - Based on Date Range </option>
            <option value="8"  <?php if(isset($report_type) && $report_type=='8') echo "selected"; ?>>Open Invoices Report</option>
            <option value="9"  <?php if(isset($report_type) && $report_type=='9') echo "selected"; ?>>All Invoices Report</option>
              <option value="10"  <?php if(isset($report_type) && $report_type=='10') echo "selected"; ?>>Scheduled Payments Report</option>
          </select>
         
       </div> 
    <div class="col-md-4">
       <div id="range_data" <?php if($startdate==''){ ?> style="display:none" <?php  } ?> >
       
             <div class="input-group input-daterange" data-date-format="mm/dd/yyyy">
                    <input type="text" id="startDate" name="startDate" class="form-control text-center" value="<?php if(isset($startdate)) echo($startdate)?($startdate):''; ?>" placeholder="From">
                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                    <input type="text" id="endDate" name="endDate" class="form-control text-center"  value="<?php if(isset($enddate)) echo($enddate)?($enddate):''; ?>"  placeholder="To">
                </div>
             </div>
        </div>
            
        
             <input type="submit" name="getData" class="btn btn-sm pull-left rp-btn btn-info" id="getData" value="Go" /> 
       
        <div class="btn-group pull-left"> 
        
      
	        <a  href="<?php echo base_url(); ?>report/export_csv/<?php echo $report_type; if(isset($startdate)&& $startdate!=''){echo '/'.date('Y-m-d',strtotime($startdate)); }if(isset($enddate)&& $enddate!=''){echo '/'.date('Y-m-d',strtotime($enddate)); } ?>" target="_blank" data-toggle="tooltip" title="" data-original-title="Download CSV"><b class="btn btn-sm btn-success">CSV</b></a>
           
			  <a   href="<?php echo base_url(); ?>report/report_details_pdf/<?php echo $report_type; if(isset($startdate)&& $startdate!=''){echo '/'.date('Y-m-d',strtotime($startdate)); }if(isset($enddate)&& $enddate!=''){echo '/'.date('Y-m-d',strtotime($enddate)); } ?>" target="_blank"   data-toggle="tooltip" title="" data-original-title="Download PDF"><b class="btn btn-sm btn-danger">PDF</b></a>
              
           </div>     
      
       
            
     </form>
   
      
     <br>
     <br>
     <br>
     
        
       <?php  if(isset($report1) &&  !empty($report1) ){  ?>
        
         <table  class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
				  
                    <th class="text-left">Customer Name</th>
                    <th class="visible-lg">Full Name</th>
                   
                    <th class="text-right">Amount</th>
					<th class="text-right hidden-xs">Email</th>
                   
                    <th class="hidden-xs text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report1))
				{
					
					
					foreach($report1 as $key=>$invoice)
					{
				?>
				<tr>
				   
					<td class="text-left"><?php echo $invoice['FirstName'].' '.$invoice['LastName']; ?></td>
					<td class="visible-lg"><?php echo $invoice['Customer_FullName']; ?></a></td>
				
					
					<td class="text-right"><?php echo "$".$invoice['balance']; ?></td>
					
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
			     
					
				
					
					<td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
					
				   
					<td class="text-center">
							<div class="btn-group btn-group-sm">
							<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" ><i class="fa fa-edit"></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" disabled><i class="fa fa-edit"></i></a>
							<?php }?>
							</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
       <?php }  
	   
	 
	    if(isset($report2) &&  !empty($report2) ){        ?> 
        
          <table   class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
                   
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-left hidden-xs">Full Name</th>
                    <th class="text-right">Amount</th>
					<th class="text-right hidden-xs">Email</th>
                    
                    <th class="hidden-xs text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report2))
				{
					foreach($report2 as $key=> $invoice)
					{
				?>
				<tr>
				
					<td class="visible-lg"><?php echo $invoice['FirstName'].' '.$invoice['LastName'] ; ?></a></td>
				
					<td class="visible-lg"><?php echo $invoice['Customer_FullName']; ?></a></td>
					<td class="text-right"><?php echo "$".$invoice['balance']; ?></td>
					
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
			     
					
				
					
					<td class="text-right hidden-xs"><?php  echo $invoice['status']; ?></td>
					
				   
					<td class="text-center">
							<div class="btn-group btn-group-sm">
							<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" ><i class="fa fa-edit"></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" disabled><i class="fa fa-edit"></i></a>
							<?php }?>
							</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
       <?php } 
	   
	    if(isset($report3)){  ?> 
         <table   class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
                    <th class="text-right">Txn ID</th>
                    <th class="visible-lg">Customer Name</th>
                      <th class="text-right">Email</th>
                    <th class="text-right hidden-xs">Due Date</th>
                    <th class="text-right">Amount</th>
				
                   
                     	
                    <th class="hidden-xs text-right">Status</th>
                  <th class="hidden-xs text-right">Created On</td>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report3))
				{
					foreach($report3 as $invoice)
					{
					  if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Past Due'){
							    $lable ="danger";
							    $disabled = "";
						   }else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-right"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></a></td>
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					<td class="text-right"><?php echo "$".$invoice['BalanceRemaining']; ?></td>
					
				
					  <td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
						<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['TimeCreated'])); ?></td>
				   
					<td class="text-center">
							<div class="btn-group btn-group-sm">
							<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" ><i class="fa fa-edit"></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" disabled><i class="fa fa-edit"></i></a>
							<?php }?>
							</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        
          <?php } 
	   
	    if(isset($report4)  ){  ?> 
          <table  class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
                    <th class="text-right">Txn ID</th>
                    <th class="visible-lg">Customer Name</th>
                     <th class="text-right">Email</th>
                    <th class="text-right hidden-xs">Due Date</th>
                    <th class="text-right">Amount</th>
				
                    
                    <th class="hidden-xs text-right">Status</th>
                     <th class="hidden-xs text-right">Created On</td>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report4))
				{
					foreach($report4 as $invoice)
					{
					
					   if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   } else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-right"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></a></td>
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
					<td class="hidden-xs text-center"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					<td class="text-right"><?php echo "$".$invoice['BalanceRemaining']; ?></td>
					
				
			     	
					
				
					
					<td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
					<td class="hidden-xs text-center"><?php echo date('F d, Y', strtotime($invoice['TimeCreated'])); ?></td>
				   
					<td class="text-center">
							<div class="btn-group btn-group-sm">
							<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" ><i class="fa fa-edit"></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" disabled><i class="fa fa-edit"></i></a>
							<?php }?>
							</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        
       <?php } 
	   
	    if(isset($report5) &&  !empty($report5) ){  ?> 
          <table id="ecom-orders"  class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
				   
                    <th class="text-right">Txn ID</th>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right hidden-xs">Txn Date</th>
                    <th class="text-right">Amount</th>
				    <th class="hidden-xs text-right">Invoice</th>
                     <th class="text-right">Status</th>
                  
                     <th class="text-center">Txn Type</th>
                      <th class="text-left">Comment</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report5))
				{
				
					foreach($report5 as $key=> $invoice)
					{
				
				
					if($invoice['transactionCode']!='100'){
					 $lable ="danger";
						$labeltext ="Failed";	   
					}else{
					 $lable ="success";
					 $labeltext ="Success";
							  
					}
				?>
				<tr>
				  
					<td class="text-right"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></a></td>
				
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['transactionDate'])); ?></td>
					<td class="text-right"><?php echo "$".$invoice['transactionAmount']; ?></td>
					
					<td class="hidden-xs text-right"><?php echo $invoice['RefNumber']; ?></td>
			     	
					
				
					
					<td class="text-right hidden-xs"><?php echo $labeltext; ?></td>
					
				   
					<td class="text-center">
						<?php echo $invoice['transactionType']; ?>
					</td>
                    <td class="text-left">
						<?php echo $invoice['transactionStatus']; ?>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
         <?php } 
	   
	    if(isset($report6) &&  !empty($report6) ){ 
	    ?> 
          <table id="ecom-orders" class="table table-bordered table-striped table-vcenter ">
            <thead>
                <tr>
                   
                    <th class="visible-lg">Name</th>
                    <th class="text-right hidden-xs">Company</th>
                    <th class="text-right hidden-xs">Email</th>
                     <th class="text-right">Card Number</th>
                    <th class="text-right">Card Name</th>
					
                    <th class="text-right visible-lg">Expiring</th>
                    <th class="hidden-xs text-right">Status</th>
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report6))
				{
					foreach($report6 as $invoice)
					{
				?>
				<tr>
					
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></a></td>
				
					<td class="text-right hidden-xs"><?php echo $invoice['companyName']; ?></td>
					
					
					<td class="hidden-xs text-right"><?php echo $invoice['Contact']; ?></td>
                    <td class="text-right"><?php echo ($invoice['CardNo'])?$invoice['CardNo']:''; ?></td>
			     	<td class="hidden-xs text-right"><?php echo $invoice['customerCardfriendlyName']; ?></td>
					<td class="text-right"><?php echo date('F, Y', strtotime($invoice['expired_date'])); ?></td>
				
					
					<td class="text-right hidden-xs">Expired</td>
					
				   
					<td class="text-center">
							<div class="btn-group btn-group-sm">
							<?php  
							if($this->session->userdata('logged_in')){
								$data['login_info']	    = $this->session->userdata('logged_in');
								$user_id			    = $data['login_info']['merchID'];
							?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" ><i class="fa fa-edit"></i></a>
							
							<?php } else { ?>
							
							<a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>" data-toggle="tooltip" title="View Details" class="btn btn-sm btn-default" disabled><i class="fa fa-edit"></i></a>
							<?php }?>
							</div>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        
         <?php } 
	  
	    if(isset($report7) &&  !empty($report7) ){ 
	    ?> 
             <table   class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
				   
                    <th class="text-right">Txn ID</th>
					 <th class="hidden-xs text-right">Invoice</th>
                     
                   
					<th class="visible-lg">Full Name</th>
					 <th class="text-right">Amount</th>
                    <th class="text-right hidden-xs">Date</th>
				     <th class="text-right">Remarks</th>
                     <th class="text-right">Status</th>
                    
                     <th class="text-center">Type</th>
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report7))
				{
				
					foreach($report7 as $key=> $invoice)
					{
				
				
					if($invoice['transactionCode']!='100'){
					 $lable ="danger remove-hover";
						$labeltext ="Failed";	   
					}else{
					 $lable ="success remove-hover";
					 $labeltext ="Success";
							  
					}
				?>
				<tr>
				   
					<td class="text-right"><?php echo ($invoice['transactionID'])?$invoice['transactionID']:$invoice['id']; ?></td>
					<td class="hidden-xs text-right"> <?php echo $invoice['RefNumber']; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></td>
					<td class="text-right"> <?php echo "$".$invoice['transactionAmount']; ?></td>
					<td class="hidden-xs text-right"> <?php echo date('M d, Y', strtotime($invoice['transactionDate'])); ?></td>
					
				    <td class="hidden-xs text-right" > <?php echo $invoice['transactionStatus']; ?></td>
					
					
					
			     	
					
			    
					
					<td class="text-right hidden-xs"> <?php echo $labeltext; ?></td>
					
				   
					<td class="text-center">
						<?php echo $invoice['transactionType']; ?>
					</td>
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        
           <?php } 
	   
	    if(isset($report89) &&  !empty($report89) ){ 
	    ?> 
        
        <table   class="table table-bordered table-striped table-vcenter compamount">
            <thead>
                <tr>
                    <th class="text-right">Invoice</th>
                    <th class="visible-lg">Full Name</th>
                    <th class="text-right hidden-xs">Paid</th>
                    <th class="text-right">Balance</th>
				    <th class="hidden-xs text-right">Due Date</th>
                     <th class="text-right">Status</th>
                     <th class="text-right">Added On</th>
                     
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report89))
				{
				
					foreach($report89 as $key=> $invoice)
					{
				
				
					  if($invoice['status']=='Upcoming'){
							    $lable ="warning";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger";
							    $disabled = "";
						   } else  if($invoice['status']=='Canceled'){
						       
								    $lable ="primary";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-right"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></td>
				
					<td class="hidden-xs text-right"><?php echo "$".$invoice['AppliedAmount']; ?></td>
					<td class="text-right"><?php echo "$".$invoice['BalanceRemaining']; ?></td>
				    <td class="hidden-xs text-right"><?php echo date('M d, Y', strtotime($invoice['DueDate'])); ?></td>
                    <td class="text-right hidden-xs"><?php echo $invoice['status']; ?></td>
					
					<td class="text-right">
						<?php echo date('M d, Y', strtotime($invoice['TimeCreated'])); ?>
					</td>
				   
				
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
        
        
         <?php } 
	   
	    if(isset($report10)    ){ 
	    ?> 
          
        <table id="ecom-orders"   class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-right">Invoice</th>
                    <th class="visible-lg">Full Name</th>
                    <th class="text-right hidden-xs">Paid</th>
                    <th class="text-right">Balance</th>
				    <th class="hidden-xs text-right">Due Date</th>
                     <th class="text-right">Status</th>
                   
                  
                     <th class="text-right">Created On</th>
                       
                </tr>
            </thead>
            <tbody>
			
				<?php 
				if(!empty($report10))
				{
				
					foreach($report10 as $invoice)
					{
				
				
					  if($invoice['status']=='Upcoming'){
							    $lable ="warning remove-hover";
							    $disabled = "";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success remove-hover";
							   $disabled = "";
						   }else  if($invoice['status']=='Failed'){
							    $lable ="danger remove-hover";
							    $disabled = "";
						   } else  if($invoice['status']=='Cancel'){
						       
								    $lable ="primary remove-hover";
								     $disabled = "disabled";
						   }
				?>
				<tr>
					<td class="text-right"><?php echo ($invoice['RefNumber'])?$invoice['RefNumber']:''; ?></td>
					<td class="visible-lg"><?php echo $invoice['FullName']; ?></td>
				
					<td class="hidden-xs text-right"><?php echo "$".$invoice['AppliedAmount']; ?></td>
					<td class="text-right"><?php echo "$".$invoice['BalanceRemaining']; ?></td>
				    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
                    <td class="text-right hidden-xs"><?php echo ucfirst($invoice['status']); ?></td>
					
				    
					<td class="text-right">
						<?php echo date('F d, Y', strtotime($invoice['TimeCreated'])); ?>
					</td>
                     
				</tr>
				
				<?php } } ?>
				
			</tbody>
        </table>
          <?php } ?>
    </div>
  </div>  
 <style>
    .rp-btn{
        
        margin-right:4px;
    } 
     
 </style>   
    
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){   
	
	
	Pagination_view.init();

	
	
 $('#report_type').change(function(){

    var report_type = $(this).val();
	
	if(report_type=='7'){
	$('#range_data').show();
	}else{  $('#range_data').hide();  }

 });
 

 
 
 
  });
  
  
  
var Pagination_view = function() {

    return {
        init: function() {
            /* Extend with date sort plugin */
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            /* Initialize Bootstrap Datatables Integration */
            App.datatables();

            /* Initialize Datatables */
            $('.compamount').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();

  
</script>
	



</div>
