<?php
	$this->load->view('alert');
?>
<div id="page-content">
      <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i>Email Personalization<br><small>You can setup email templates here!</small>
            </h1>
	        
        </div>
    </div>
    
    
   <div class="block full">
        <!-- Working Tabs Title -->
        <div class="block-title">
            <h2>Template Details</small></h2>
        </div>
        <!-- END Working Tabs Title -->

        <!-- Working Tabs Content -->
        <div class="row">
            <div class="col-md-12">
                <!-- Block Tabs -->
                    <!-- Tabs Content -->
                                <form id="form-validation" action="<?php echo base_url(); ?>Settingmail/create_template" method="post" enctype="multipart/form-data" class="form-horizontal ">
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Template Name</label>
                                        <div class="col-md-7">
                                                <input type="text" id="templateName" name="templateName"  value="<?php if(isset($templatedata)) echo ($templatedata['templateName'])?$templatedata['templateName']:''; ?>"   class="form-control" placeholder="Enter the name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">From Email Address</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="fromEmail" name="fromEmail"  value="<?php  if(isset($templatedata)) echo ($templatedata['fromEmail'])?$templatedata['fromEmail']:''; ?>" class="form-control" placeholder="Enter the email">
                                        </div>
                                    </div>
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName"></label>
                                        <div class="col-md-7">
	                                       <a href="javascript:void(0);"  id ="open_cc">Add CC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);" id="open_bcc">Add BCC<strong>&nbsp;&nbsp;|&nbsp;&nbsp;</strong></a><a href="javascript:void(0);"  id ="open_reply">Set Reply-To</a>
                                        </div>
                                    </div>
                                    
                                        <div class="form-group" id="cc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">CC Email Addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="ccEmail" name="ccEmail" value="<?php if(isset($templatedata)) echo ($templatedata['addCC'])?$templatedata['addCC']:''; ?>"  class="form-control" placeholder="Enter the cc email">
                                        </div>
                                    </div>
                                      <div class="form-group" id="bcc_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">BCC Email Addresses</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="bccEmail" name="bccEmail" class="form-control" value="<?php if(isset($templatedata)) echo ($templatedata['addBCC'])?$templatedata['addBCC']:''; ?>" placeholder="Enter the bcc Email">
                                        </div>
                                    </div>
                                     <div class="form-group" id="reply_div" style="display:none">
                                        <label class="col-md-3 control-label" for="templteName">Set the "Reply-To"</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="replyEmail" name="replyEmail" class="form-control"  value="<?php if(isset($templatedata)) echo ($templatedata['replyTo'])?$templatedata['replyTo']:''; ?>"  placeholder="Enter the email">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-3 control-label" for="type">Email Type</label>
                                        <div class="col-md-7">
                                            <select id="type" name="type" class="form-control">
                                            
                                               <?php 
											          foreach($types as $type){
											   ?>
                                             	<option value="<?php  echo  $type['typeID']; ?>" <?PHP      if(isset($templatedata)&& $templatedata['templateType']==$type['typeID'] ){ ?>   selected="selected" <?php } ?>><?php  echo  $type['typeText']; ?></option>
					                          <?php } ?>
												
                                                 
                                                    
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group ">
                                        <label class="col-md-3 control-label" >Attach PDF of the Invoice?</label>
                                        <div class="col-md-7">
                                           <label class="switch switch-info"><input type="checkbox" name="add_attachment"<?php  if(isset($templatedata) && $templatedata['attachedTo']){ echo "checked"; }  ?> id="add_attachment"><span></span></label>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-3 control-label" for="templteName">Email Subject</label>
                                        <div class="col-md-7">
	                                        <input type="text" id="emailSubject" name="emailSubject" value="<?php if(isset($templatedata)) echo ($templatedata['emailSubject'])?$templatedata['emailSubject']:''; ?>"  class="form-control" placeholder="Enter the Subject">
                                        </div>
                                    </div>
                                     
                                      <div class="form-group">
                                        <label class="col-md-3 control-label" >Email Body</label>
                                        <div class="col-md-7">
                                            <textarea id="textarea-ckeditor" name="textarea-ckeditor" class="ckeditor"> <?php if(isset($templatedata)) echo ($templatedata['message'])?$templatedata['message']:''; ?></textarea>
                                        </div>
                                    </div>
                                    
                                    <input type="hidden" name="tempID" id="tempID" value="<?php if(isset($templatedata)) echo ($templatedata['templateID'])?$templatedata['templateID']:''; ?>" />
                                  <div class="form-group form-actions">
                                    <div class="col-md-7 col-md-offset-3">
                                        <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Save </button>
                                      
                                      <a href="<?php echo base_url(); ?>Settingmail/email_temlate" class="btn btn-sm btn-danger" >Cancel</a>
                                    </div>
                                </div>  
                           </form>
                         
                    <!-- END Tabs Content -->
               
                <!-- END Block Tabs -->
            </div>
	
        </div>
        <!-- END Working Tabs Content -->
		 </div>
		       
<!---------------- Start Template Guide -------------------->
 
            <!-- Modal Header -->
			
            <div class="modal-header ">
			 
               <h2 class="modal-title pull-left">Template Guide </h2>
             
                
            </div>
           
            <div class="modal-body">
             <div id="data_form_template">
			    <label class="label-control" id="template_name"></label>
              
		    	 <div class="form-group ">
                                        <label class="col-md-3 control-label" ></label>
                                        <div class="col-md-7"  >
                                          <label> <p>You may use following tags in your email templates</p>
                                           </label>
                                          </div> 
                                                  
                                             <table class="table table-bordered table-striped ecom-orders table-vcenter">
                                              
                                              <th>Tag</th>
                                              <th>Description</th>
                                              <tbody>
                                                  
                                             <tr>                                              
                                              <td> <?php  echo "{{ merchant_name }}"; ?></td>
                                              <td>Merchant Name</td>
                                              </tr>
											  
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ merchant_email }}"; ?></td>
                                              <td><font color="red">Merchant Email  </font> </td>
                                              </tr>
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ merchant_phone }}"; ?></td>
                                              <td> <font color="red">Merchant Phone </font> </td>
                                              </tr>
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ customer.company }}"; ?></td>
                                              <td>Customer Contact Name</td>
                                              </tr>
											  
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ invoice_payment_pagelink }}"; ?></td>
                                              <td> <font color="red">Invoice Payment Pagelink </font></td>
                                              </tr>
											  
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ current.date }}"; ?></td>
                                              <td> <font color="red">Current Date </font></td>
                                              </tr>
											  
											  
											  
											  <tr>                                              
                                              <td> <?php  echo "{{ logo }}"; ?></td>
                                              <td>Logo URL</td>
                                              </tr>
                                                  
                                                  
                                              <tr>
                                              
                                              <td> <?php  echo "{{ creditcard.type_name }}"; ?></td>
                                              <td>Card Friendly Name</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php   echo "{{ invoice.url_permalink }}" ?></td>
                                              <td>  Link </td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo  "{{ transaction.amount }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php  echo  "{{ transaction.transaction_method }}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo  "{{ transaction.transaction_date}}"; ?></td>
                                              <td> Transaction Date</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo "{{ transaction.transaction_detail }}"; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo "{'%'  config.email_show_poweredby '%'}"; ?> </td>
                                              <td>Company promotion message</td>
                                              </tr>
                                                 
                                              <td><?php   echo "{{ invoice.refnumber }}" ?></td>
                                              <td> Invoice Number </td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo  "{{ invoice.days_overdue }}";   ?></td>
                                              <td> Transaction Amount</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php  echo  "{{invoice.balance}}"; ?></td>
                                              <td> Transaction Methods</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo  "{{ transaction.currency_symbol }}"; ?></td>
                                              <td>Currency</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo '{{ invoice.due_date|date("F j, Y") }}'; ?></td>
                                              <td>Transaction Message</td>
                                              </tr>
                                              <tr>
                                              
                                              <td><?php echo "{{ creditcard.url_updatelink }}"; ?> </td>
                                              <td>Credit Card Update Link</td>
                                              </tr>
                                              </tbody>
                                             </table>       
                                            
							    
                                        <!--<button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">Close</button>-->
                                        
                                          
                                        </div>
			
			           </div>
			   					
                
            </div>
            <!-- END Modal Body -->
      
<!---------------- End Template Guide -------------------->

   
        <script src="<?php echo base_url(JS); ?>/helpers/ckeditor/ckeditor.js"></script>
        <script>$(function(){    nmiValidation.init();
		
					CKEDITOR.replace( 'textarea-ckeditor', {
				toolbarGroups: [
					{ name: 'document',	   groups: [ 'mode', 'document' ] },			// Displays document group with its two subgroups.
					{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },			// Group's name will be used to create voice label.
					'/',																// Line break - next group will be placed in new line.
					{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
					{ name: 'links' },
					{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ] },
					{ name: 'styles' },
					{ name: 'colors' },
				]
			
				// NOTE: Remember to leave 'toolbar' property with the default value (null).
			});
			CKEDITOR.config.allowedContent = true;
		    $('#open_cc').click(function(){
			          $('#cc_div').show();
					  $(this).hide();
			});
			  $('#open_bcc').click(function(){
			          $('#bcc_div').show();
					   $(this).hide();
			});
			 $('#open_reply').click(function(){
			          $('#reply_div').show();
					   $(this).hide();
			});
		 });
         
         
         
         
         
 
var nmiValidation = function() {

    return {
        init: function() {
            /*
             *  Jquery Validation, Check out more examples and documentation at https://github.com/jzaefferer/jquery-validation
             */

            /* Initialize Form Validation */
            $('#form-validation').validate({
                errorClass: 'help-block animation-slideDown', // You can change the animation class for a different entrance animation - check animations page
                errorElement: 'div',
                errorPlacement: function(error, e) {
                    e.parents('.form-group > div').append(error);
                },
                highlight: function(e) {
                    $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                    $(e).closest('.help-block').remove();
                },
                success: function(e) {
                    // You can use the following if you would like to highlight with green color the input after successful validation!
                    e.closest('.form-group').removeClass('has-success has-error'); 
                    e.closest('.help-block').remove();
                },
                   rules: {
                    templateName: {
                        required: true,
						minlength:3,
                    },
					
					 fromEmail: {
						required: true,
						email:true,	
						},
					
                   		
					 emailSubject: {
                        required: true,
                       
                    },
                    message: {
                         required: true,
                       
                    },
                  
                },
             
            });
			

			
			

            // Initialize Masked Inputs
            // a - Represents an alpha character (A-Z,a-z)
            // 9 - Represents a numeric character (0-9)
            // * - Represents an alphanumeric character (A-Z,a-z,0-9)
            $('#masked_date').mask('99/99/9999');
            $('#masked_date2').mask('99-99-9999');
            $('#masked_phone').mask('(999) 999-9999');
            $('#masked_phone_ext').mask('(999) 999-9999? x99999');
            $('#masked_taxid').mask('99-9999999');
            $('#masked_ssn').mask('999-99-9999');
            $('#masked_pkey').mask('a*-999-a999');
        }
    };
}();
	
	
         
         
         
        
         
         </script>

   
   </div> 
    
    