

<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    <!-- Quick Stats -->
    <div class="row text-center">
        	<div class="msg_data "><?php echo $this->session->flashdata('message');   ?> </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2 remove-hover">
                <div class="widget-extra themed-background-success">
                    <h4 class="widget-content-light"><strong>Paid</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-success"><strong><?php  echo ($success_invoice)?($success_invoice):'0'; ?></strong></span></div>
            </a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2 remove-hover">
                <div class="widget-extra themed-background-info">
                    <h4 class="widget-content-light"><strong>Scheduled</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-info"><strong><?php  echo ($upcomming_inv)?($upcomming_inv):'0'; ?></strong></span></div>
            </a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2 remove-hover">
                <div class="widget-extra themed-background-danger">
                    <h4 class="widget-content-light"><strong>Cancelled</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-danger"><strong><?php  echo ($cancel_inv)?($cancel_inv):'0'; ?></strong></span></div>
            </a>
        </div>
        
        <div class="col-sm-6 col-lg-3">
            <a href="javascript:void(0)" class="widget widget-hover-effect2 remove-hover">
                <div class="widget-extra themed-background-warning">
                    <h4 class="widget-content-light"><strong>Failed</strong> Invoices</h4>
                </div>
                <div class="widget-extra-full"><span class="h2 animation-expandOpen text-warning"><strong><?php  echo ($failed_inv)?($failed_inv):'0'; ?></strong></span></div>
            </a>
        </div>
        
        
    </div>
    <!-- END Quick Stats -->

    <!-- All Orders Block -->
    <div class="block full">
        <!-- All Orders Title -->
        <div class="block-title">
            <h2><strong>All</strong> Invoices</h2>
             <?php if($this->session->userdata('logged_in')){ ?>
              <div class="block-options pull-right">
                   
                       <a class="btn btn-sm  btn-success" title="Create New"  href="javascript:void(0);">Add New</a>
                     
                </div>
                <?php } ?>
        </div>
        <!-- END All Orders Title -->

        <!-- All Orders Content -->
        <table id="invoice_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="visible-lg">Customer Name</th>
                    <th class="text-right">Invoice</th>
                   
                    <th class="hidden-xs text-right">Due Date</th>
                   
                     <th class="text-right hidden-xs">Payment</th>
                    <th class="text-right hidden-xs">Balance</th>
                    <th class="text-right">Status</th>
                    
                    <th class="text-center">Action</th>
                </tr>
            </thead>
            <tbody>
			
			   <?php    if(!empty($invoices)){

						foreach($invoices as $invoice){
						     $lable='';
						     
							              $statusnew='';
							           if($invoice['leftDay'] < 0 &&  $invoice['BalanceRemaining']=='0.00' ){
										 
										 	 $statusnew = "Close";			  
										}else if($invoice['leftDay'] < 0  && $invoice['userStatus']!='' ){
										 $statusnew = "Close";	
										}else if($invoice['leftDay'] < 0 &&  $invoice['BalanceRemaining']!='0.00'  && $invoice['userStatus']==''){ 
										 $statusnew = "Past Due";	
										
										}else if($invoice['leftDay'] >=0 &&  $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='cancel' ){
										
										 $statusnew = "Close";	
										}
										
										else if($invoice['leftDay'] >=0 &&  $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='' ){
										
										 $statusnew = "Open";	
										}
					
						   if($invoice['status']=='Upcoming' && $invoice['userStatus']=='' ){
						   $invoice['status']='Scheduled';
							    $lable ="info";
						   }else  if($invoice['status']=='Success'){
							   $lable ="success";
							     $invoice['status']='Paid';
						   }else  if($invoice['status']=='Failed' && $invoice['userStatus']==''){
							    $lable ="danger";
						   } else  if($invoice['status']=='Cancel'  ){
						 			 
								    $lable ="primary";
						   }else  if($invoice['userStatus']!=''){
						           $invoice['userStatus']='Canceled';
								    $lable ="primary";
						   }
						    
			   ?>
			
				<tr>
					<td class="visible-lg"><a href="<?php echo base_url('home/view_customer/'.$invoice['ListID']); ?>"><?php echo $invoice['FullName']; ?></a></td>
					<td class="text-right"><a href="<?php echo base_url();?>home/invoice_details/<?php  echo $invoice['TxnID']; ?>"><strong><?php  echo $invoice['RefNumber']; ?></strong></a></td>
					
                    <td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($invoice['DueDate'])); ?></td>
					

					   <?php if($invoice['AppliedAmount']!="0.00"){ ?>
                            <td class="hidden-xs text-right"><a href="#pay_data_process"   onclick="set_payment_data('<?php  echo $invoice['TxnID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal"><?php echo '$'.number_format((-$invoice['AppliedAmount']),2); ?></a></td>
						   <?php }else{ ?>
							<td class="hidden-xs text-right"><?php  echo  '$'.$invoice['AppliedAmount']; ?></td>   
						   <?php } ?>
					<td class="text-right hidden-xs"><strong><?php echo '$'.$invoice['BalanceRemaining']; ?></strong></td>
					<td class="text-right">
					<?php  if($invoice['userStatus']!=''){ echo ucfirst($invoice['userStatus']); }else{ echo ucfirst($invoice['status']); } ?></td>
					
					
					<td class="text-center">
						<div class="">
						      <?php if($invoice['status']=="Paid" ){ ?>  

                               <a href="#invoice_process"  disabled class="btn btn-sm btn-success"  onclick=
							"set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo
							 $invoice['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false"
							 data-toggle="modal">Process</a>



						      <a href="#invoice_schedule" disabled class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
							  
							  <a href="javascript:void(0);" disabled class="btn btn-sm btn-primary">Delete</a>


							  <?php } 
							  else  if($invoice['DueDate'] > date('Y-m-d') && $invoice['userStatus']=='' ){ ?>
						      <a href="#invoice_process" class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo $invoice['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
						      <a href="#invoice_schedule" class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
						<a href="#invoice_cancel" class="btn btn-sm btn-danger"  onclick="set_invoice_id('<?php  echo $invoice['TxnID']; ?>' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a>
						
						
							   <?php } else  if($invoice['DueDate'] > date('Y-m-d') && $invoice['userStatus']!='' ) { ?>
							   
							   <a href="#invoice_process"  disabled class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo $invoice['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
						      <a href="#invoice_schedule" disabled class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
							   
							  	<a href="javascript:void(0);" class="btn btn-sm btn-primary">Canceled</a>
							    <?php }else if( $invoice['BalanceRemaining']!='0.00' && $invoice['userStatus']=='' ){ ?>
								<a href="#invoice_process" class="btn btn-sm btn-success"  onclick="set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo $invoice['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Process</a>
								 <a href="#invoice_schedule"  disabled class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
						<a href="#invoice_cancel" class="btn btn-sm btn-danger"  disabled onclick="set_invoice_id('<?php  echo $invoice['TxnID']; ?>' );" data-backdrop="static" data-keyboard="false" data-toggle="modal">Delete</a>
							<?php 	}else{ ?>  


							<a href="#invoice_process"  disabled class="btn btn-sm btn-success"  onclick=
							"set_invoice_process_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo
							 $invoice['Customer_ListID']; ?>');" data-backdrop="static" data-keyboard="false"
							 data-toggle="modal">Process</a>



						      <a href="#invoice_schedule" disabled class="btn btn-sm btn-info"  onclick="set_invoice_schedule_id('<?php  echo $invoice['TxnID']; ?>','<?php  echo date('F d Y',strtotime($invoice['DueDate'])); ?>');" data-backdrop="static" data-keyboard="false" data-toggle="modal">Schedule</a>
							  
							  <a href="javascript:void(0);" disabled class="btn btn-sm btn-primary">Canceled</a>
							<?php } ?>
							    	
							    	
						</div>
					</td>

				</tr>
				
				<?php 
				  }
			   }	
				?>
			
				
			</tbody>
        </table>
        <!-- END All Orders Content -->
    </div>
    <!-- END All Orders Block -->

<!-- END Page Content -->



<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
<script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#invoice_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>



</script>
</div>
