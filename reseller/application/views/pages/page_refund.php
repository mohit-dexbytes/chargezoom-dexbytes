
<!-- Page content -->
<?php
	$this->load->view('alert');
?>
<div id="page-content">
    
    
    <!-- Forms General Header -->
    <div class="content-header">
        <div class="header-section">
            <h1>
                <i class="gi gi-notes_2"></i>Refund Transactions<br><small>You can refund already captured payment here!</small>
            </h1>
        </div>
    </div>
  
    <!-- END Forms General Header -->

    <div class="row">  
        
            <!-- Form Validation Example Block -->
			<div class="msg_data ">
			    <?php echo $this->session->flashdata('message');   ?>
			</div>
	
	</div>
			
	<div class="block full">
        <!-- Form Validation Example Title -->
        <table id="refund_page" class="table table-bordered table-striped table-vcenter">
            <thead>
                <tr>
                    <th class="text-center">Trans ID</th>
                    <th class="visible-lg">Full Name</th>
                   
                    <th class="text-right">Amount</th>
					<th class="text-right hidden-xs">Type</th>
                    <th class="text-right visible-lg">Date</th>
                    <th class="hidden-xs text-right">Status</th>

                </tr>
            </thead>
            <tbody>
			
    			<?php 
    			if(isset($transactions) && $transactions)
    			{
				
    				foreach($transactions as $transaction)
    				{
						if($transaction['transactionType']=='credit')
							$type ="Authorize.net";
						if($transaction['transactionType']=='pay_refund')
						 $type ="Pay Trace";
						if($transaction['transactionType']=='refund')
						$type ="NMI";
    			?>
				<tr>
					<td class="text-center"><?php echo ($transaction['transactionID'])?$transaction['transactionID']:'';   ?></td>
					<td class="visible-lg"><?php echo $transaction['FullName']; ?></a></td>
				    
					<td class="text-right"><?php echo ($transaction['transactionAmount'])?"$".$transaction['transactionAmount']:'0.00'; ?></td>
					<td class="hidden-xs text-right"><?php echo $type; ?></td>
					<td class="hidden-xs text-right"><?php echo date('F d, Y', strtotime($transaction['transactionDate'])); ?></td>
					<?php if( $transaction['transactionCode']=="1" || $transaction['transactionCode']=="100" || $transaction['transactionCode']=="200"  ){ ?>
					<td class="text-right hidden-xs"><span class="btn btn-sm btn-alt1 btn-success remove-hover"><?php echo "Success"; ?></span></td>
					<?php }else{ ?>
						<td class="text-right visible-lg"><span class="btn btn-sm  btn-alt1 btn-danger remove-hover">
						<?php echo "Failed"; ?></span></td>
				    <?php } ?>	
				    
				</tr>
				<?php } } ?>
			</tbody>
        </table>
    </div>
    <!-- Load and execute javascript code used only in this page -->
    <script src="<?php echo base_url(JS); ?>/pages/ecomOrders.js"></script>
    <script>$(function(){ Pagination_view.init(); });</script>
<script>

var Pagination_view = function() {

    return {
        init: function() {
            / Extend with date sort plugin /
            $.extend($.fn.dataTableExt.oSort, {
           
            } );

            / Initialize Bootstrap Datatables Integration /
            App.datatables();

            / Initialize Datatables /
            $('#refund_page').dataTable({
                columnDefs: [
                    { type: 'date-custom', targets: [3] },
                    { orderable: false, targets: [4] }
                ],
                order: [[ 3, "desc" ]],
                pageLength: 10,
                lengthMenu: [[10, 25, 50, 100, 500], [10, 25, 50, 100, 500]]
            });

            / Add placeholder attribute to the search input /
           $('.dataTables_filter input').attr('placeholder', 'Search');
        }
    };
}();



</script>

	<script>
    	$(function(){  nmiValidation.init(); });
    	
    	function set_refund_pay(txnid, txntype){
    		if(txnid !=""){
    		    $('#txnID').val(txnid);	  
    		    $('#txnIDrefund').val(txnid);
				$('#paytxnID').val(txnid);
    		}
    	}   
	
	</script>
	
	  <div id="paytrace_payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>PaytracePayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="paytxnID" name="paytxnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">No</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
	
	

    <div id="auth_payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>AuthPayment/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="txnIDrefund" name="txnIDrefund" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">No</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>

    <div id="payment_refunds" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <h2 class="modal-title">Refund Payment</h2>
                </div>
                <!-- END Modal Header -->
    
                <!-- Modal Body -->
                <div class="modal-body">
                    <form id="data_form" method="post" action='<?php echo base_url(); ?>Payments/create_customer_refund' class="form-horizontal" >
                        <p id="message_data">Do you really want to refund this payment? Clicking "Yes" will initiate the refund process.</p> 
    					<div class="form-group">
                            <div class="col-md-8">
                                <input type="hidden" id="txnID" name="txnID" class="form-control"  value="" />
                            </div>
                        </div>
                        <div class="pull-right">
            			    <input type="submit" id="btn_cancel" name="btn_cancel" class="btn btn-sm btn-warning" value="Yes"  />
                            <button type="button" class="btn btn-sm btn-default close1" data-dismiss="modal">No</button>
                        </div>
                        <br />
                        <br />
                    </form>		
                </div>
                <!-- END Modal Body -->
            </div>
        </div>
    </div>
</div>
