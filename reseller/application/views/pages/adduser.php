   <!-- Page content -->
   	<?php
		$this->load->view('alert');
	?>
	<div id="page-content">
    <!-- Wizard Header -->
    <div class="content-header">
	<div class="header-section">
		<h1>
		<i class="gi gi-settings"> </i> <?php if(isset($user)){ echo "Edit User";}else{ echo "Create New User"; }?> 
		</h1>
	</div>
	</div>
	
<style> .error{color:red; }</style>    
    <!-- END Wizard Header -->

    <!-- Progress Bar Wizard Block -->
    <div class="block">
	               <?php
						$message = $this->session->flashdata('message');
						if(isset($message) && $message != "")
						echo mymessage($message);
					?>
       
        <!-- Progress Bar Wizard Content -->
        <div class="row">
		
		 	<form method="POST" id="role_form" class="form form-horizontal" action="<?php echo base_url(); ?>MerchantUser/create_user">
			
			 <input type="hidden"  id="userID" name="userID" value="<?php if(isset($user)){echo $user['merchantUserID']; } ?>" /> 
			
			
			   
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">First Name </label>
					<div class="col-md-6">
					<input type="text" id="userFname" name="userFname" class="form-control"  value="<?php if(isset($user)){ echo $user['userFname']; } ?>" placeholder="Your First Name.."><?php echo form_error('userFname'); ?> </div>
					</div>
			  
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Last Name </label>
					<div class="col-md-6">
					<input type="text" id="userLname" name="userLname" class="form-control"  value="<?php if(isset($user)){ echo $user['userLname']; } ?>" placeholder="Your Last Name.."><?php echo form_error('userLname'); ?> </div>
					</div>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Email ID </label>
					<div class="col-md-6">
					<input type="text" id="userEmail" name="userEmail" class="form-control"  value="<?php if(isset($user)){ echo $user['userEmail']; } ?>" placeholder="Your Email.."></div>
					</div>
					
					<?php if(!isset($user)) { ?>
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Password </label>
					<div class="col-md-6">
					<input type="password" id="userPassword" name="userPassword" class="form-control" value="<?php if(isset($user)){ echo $user['userPassword']; } ?>" placeholder="Your Password.."> </div>
					</div>
					<?php } ?>
					
					<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">
					Role Name  </label>
					<div class="col-md-6">
					
					     
						<select id="roleID" class="form-control" name="roleID">
						
						<option value=""> Select Role Name </option>
								
						<?php foreach($role_name as $role) { ?>
						<option  value="<?php echo $role['roleID'];?>" <?php if(isset($user) && $user['roleId']==$role['roleID']){ echo "selected"; } ?>  > <?php echo $role['roleName'];?> </option> 
						
								<?php } ?>
							</select>
						</div>			
					</div>
				 	<div class="form-group">
					<label class="col-md-4 control-label" for="example-username">Address </label>
					<div class="col-md-6">
					<input type="text" id="userAddress" name="userAddress" class="form-control"  value="<?php if(isset($user)){ echo $user['userAddress']; } ?>" placeholder="Your Address.."> <?php echo form_error('userAddress'); ?></div>
					</div>
					
    
                    <div class="col-md-12">
                    </div>
                  
	                <div class="form-group">
					<div class="col-md-4 col-md-offset-11">
					<?php if(!isset($user)) {?>				
					<button type="submit" class="submit btn btn-sm btn-success">Save</button>					
					<?php } else {?>										
					<button type="submit" class="submit btn btn-sm btn-success">Update</button>					
					<?php }?>
					</div>
				    </div>					
					
		
		
  	</form>
		
        </div>
        <!-- END Progress Bar Wizard Content -->
    </div>
    <!-- END Progress Bar Wizard Block -->



<!-- END Page Content -->


<script>
$(document).ready(function(){

 
    $('#role_form').validate({ // initialize plugin
		ignore:":not(:visible)",			
		rules: {
		       'userFname': {
                        required: true,
                        minlength: 2,
                         
                    },
                    'userLname': {
                        required: true,
                        minlength:2,
                    },
					
				'userEmail': {
                        required: true,
                        email: true,
                         
                    },
                    'userPassword': {
                        required: true,
                        minlength:5,
                    },
					
                    'userAddress': {
                        required: true,
                        minlength:5,
                    },
                    'roleID': {
                        required: true,
                    
                    },
			
			},
    });
});	    
		
	

</script>

</div>


