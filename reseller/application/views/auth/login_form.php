<script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<link href="http://formvalidation.io/vendor/formvalidation/css/formValidation.min.css" rel="stylesheet">
<script>
			$(document).ready(function() {
				$('#form-reminder').formValidation({
					framework: 'bootstrap',
	              	excluded: [':disabled'],
	              	icon: {
		                valid: 'glyphicon glyphicon-ok',
		                invalid: 'glyphicon glyphicon-remove',
		                validating: 'glyphicon glyphicon-refresh'
	            	},
	           		fields: {
	           			login_pword: {
                   			validators: {
                       			notEmpty: {
                           			message: 'Email is required'
                       			}
                   			}
              			}	
	           		}
				})
				.on('success.form.fv', function(e) {
	            	forgot_pass();
	            	return false;	
            	})
            	.end();
			});
			
			function forgot_pass() {
				var csrf_protect = $('input[name=csrf_protect]').val();
				var login    = $('#login_pword').val();
				
				$.post('<?php echo base_url();?>auth/forgot_pword',
				{
					login:login
				},
				function(data){
				   	
				   	location.reload();  	
				});
				return false;
			}
		</script>
<?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'class'	=> 'small input-text',
);
if ($login_by_username AND $login_by_email) {
	$login_label = 'Email or login';
} else if ($login_by_username) {
	$login_label = 'Login';
} else {
	$login_label = 'Email';
}
$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'class'	=> 'small input-text',
);
$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
);
$captcha = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8,
);


$error_message = '';
if( form_error($login['name']) !=''){
$error_message = "<small class=\"error\">".str_replace("Login field", "Email field", form_error($login['name']) )."</small>";
}

if(isset($errors[$login['name']])){
$error_message .= "<small class=\"error\">".str_replace("Login", "Email", $errors[$login['name']] )."</small>";
}

if( form_error($password['name']) !='' ){
$error_message .= "<small class=\"error\">".form_error($password['name'])."</small>";
}

if(isset($errors[$password['name']])){
$error_message .= "<small class=\"error\">".$errors[$password['name']]."</small>";
}

$captcha_content = '';
if ($show_captcha) {
	if ($use_recaptcha) {
		$captcha_content = '
		<div id="account-signup-divider" class="shared-divider">
			<div class="shared-divider-label">
				<span>Confirmation Code</span>
			</div>
		</div>

		<div id="recaptcha_image"></div>
		<a href="javascript:Recaptcha.reload()">Get another CAPTCHA</a>
		<div class="recaptcha_only_if_image"><a href="javascript:Recaptcha.switch_type(\'audio\')">Get an audio CAPTCHA</a></div>
		<div class="recaptcha_only_if_audio"><a href="javascript:Recaptcha.switch_type(\'image\')">Get an image CAPTCHA</a></div>

		<div class="recaptcha_only_if_image">Enter the words above</div>
		<div class="recaptcha_only_if_audio">Enter the numbers you hear</div>
		<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />

		<div id="account-signup-divider" class="shared-divider"></div>
		';

		$captcha_content .= $recaptcha_html;
	} else {
		$captcha_content = '
		<div id="account-signup-divider" class="shared-divider">
			<div class="shared-divider-label">
				<span>Confirmation Code</span>
			</div>
		</div>

		<p>Enter the code exactly as it appears:</p>
		'.$captcha_html.'
		<p>'.form_label('Confirmation Code', $captcha['id']).'</p>
		<p>'.form_input($captcha).'</p>

		<div id="account-signup-divider" class="shared-divider"></div>
		';
	}
}

if( form_error('recaptcha_response_field') !=''){
$error_message = "<small class=\"error\">".form_error('recaptcha_response_field')."</small>";
}

if( form_error($captcha['name']) !=''){
$error_message = "<small class=\"error\">".form_error($captcha['name'])."</small>";
}
?>
<style>
	.error {
		color: red;
	}
</style>
 
<!-- Login Alternative Row -->
<div class="container">
	<div class="row">
		<div class="col-md-5 col-md-offset-1">
			<div id="login-alt-container">
				<!-- Title -->
				<h1 class="push-top-bottom">
					<img src="<?php echo base_url(IMAGES); ?>/login_logo.png" alt="avatar"><br>
					<small>Welcome to Chargezoom!</small>
				</h1>
				<!-- END Title -->

				<!-- Key Features -->
				<ul class="fa-ul text-muted">
					<li><i class="fa fa-check fa-li text-success"></i> Automated Invoice Payment Triggers</li>
					<li><i class="fa fa-check fa-li text-success"></i> Quick Registration</li>
					<li><i class="fa fa-check fa-li text-success"></i> Integrated with Quickbooks</li>
					<li><i class="fa fa-check fa-li text-success"></i> Integrated with NMI for Payment Processing</li>
					<li><i class="fa fa-check fa-li text-success"></i> .. and many more awesome features!</li>
				</ul>
				<!-- END Key Features -->

				<!-- Footer -->
				<footer class="text-muted push-top-bottom">
					2017 &copy; <a href="http://chargezoom.com" target="_blank"><?php echo $templates['name'] . ' ' . $templates['version']; ?></a>
				</footer>
				<!-- END Footer -->
			</div>
		</div>
		<div class="col-md-5">
			<!-- Login Container -->
			<div id="login-container">
				<!-- Login Title -->
				<div class="login-title text-center">
					<h1><strong>Login</strong></h1>
				</div>
				<!-- END Login Title -->

				<!-- Login Block -->
				<div class="block push-bit">
				
					<?php echo $error_message; ?> 
					
					<!-- Login Form -->
					<form action="<?php echo base_url('auth/login'); ?>" method="post" id="form-login" class="form-horizontal">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="login" name="login"  class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password"  autocomplete="off" id="password" name="password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-4">
							</div>
							<div class="col-xs-8 text-right">
								<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Login to Dashboard</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<a href="javascript:void(0)" id="link-reminder-login"><small>Forgot password?</small></a> -
								
							</div>
						</div>
					</form>
					<!-- END Login Form -->

					<!-- Reminder Form -->
					<form action="#" method="post" id="form-reminder" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="email" id="login_pword" name="login_pword" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-12 text-right">
								<button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-angle-right"></i> Reset Password</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<small>Did you remember your password?</small> <a href="javascript:void(0)" id="link-reminder"><small>Login</small></a>
							</div>
						</div>
					</form>
					<!-- END Reminder Form -->

					<!-- Register Form -->
					<form action="<?php echo base_url('login/user_register'); ?>#register" method="post" id="form-register" class="form-horizontal display-none">
						<div class="form-group">
							<div class="col-xs-6">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-user"></i></span>
									<input type="text" id="register-firstname" name="register-firstname" class="form-control input-lg" placeholder="Firstname">
								</div>
							</div>
							<div class="col-xs-6">
								<input type="text" id="register-lastname" name="register-lastname" class="form-control input-lg" placeholder="Lastname">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-envelope"></i></span>
									<input type="text" id="register-email" name="register-email" class="form-control input-lg" placeholder="Email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="register-password" name="register-password" class="form-control input-lg" placeholder="Password">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<div class="input-group">
									<span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
									<input type="password" autocomplete="off" id="register-password-verify" name="register-password-verify" class="form-control input-lg" placeholder="Verify Password">
								</div>
							</div>
						</div>
						<div class="form-group form-actions">
							<div class="col-xs-6">
							</div>
							<div class="col-xs-6 text-right">
								<button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Register Account</button>
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12 text-center">
								<small>Do you have an account?</small> <a href="javascript:void(0)" id="link-register"><small>Login</small></a>
							</div>
						</div>
					</form>
					<!-- END Register Form -->
				</div>
				<!-- END Login Block -->
			</div>
			<!-- END Login Container -->
		</div>
	</div>
</div>
<!-- END Login Alternative Row -->


<!-- Load and execute javascript code used only in this page -->
<script src="<?php echo base_url(JS); ?>/pages/login.js"></script>
<script>$(function(){ Login.init(); });</script>

