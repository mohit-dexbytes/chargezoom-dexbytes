<?php
/**
 * template_start.php
 *
 * Author: pixelcave
 *
 * The first block of code used in every page of the template
 *
 */
?>
<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title><?php echo $templates['title'] ?></title>

        <meta name="description" content="<?php echo $templates['description'] ?>">
        <meta name="author" content="<?php echo $templates['author'] ?>">
        <meta name="robots" content="<?php echo $templates['robots'] ?>">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo base_url(IMAGES); ?>/favicon.png">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="<?php echo base_url(IMAGES); ?>/icon180.png" sizes="180x180">
        <!-- END Icons -->
		
		<!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/bootstrap.min.css">
		

        <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/plugins.css">

        <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/main.css">

        <!-- Include a specific file here from <?php echo base_url(CSS); ?>/themes/ folder to alter the default theme of the template -->
        <?php if ($templates['theme']) { ?><link id="theme-link" rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes/<?php echo $templates['theme']; ?>.css"><?php } ?>

        <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="<?php echo base_url(CSS); ?>/themes.css">
        <!-- END Stylesheets -->
		

        <!-- Modernizr (browser feature detection library) -->
		<script src="<?php echo base_url(JS); ?>/vendor/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/bootstrap.min.js"></script>
        <script src="<?php echo base_url(JS); ?>/plugins.js"></script>
        <script src="<?php echo base_url(JS); ?>/app.js"></script>
        <script src="<?php echo base_url(JS); ?>/custom.js"></script>
        <script src="<?php echo base_url(JS); ?>/pages/formsWizard.js"></script>
        <script src="<?php echo base_url(JS); ?>/vendor/modernizr-respond.min.js"></script>
    </head>
    <body>
<?php echo $template['body'] ?>
<?php
/**
 * template_scripts.php
 *
 * Author: pixelcave
 *
 * All vital JS scripts are included here
 *
 */
?>

<!-- jQuery, Bootstrap.js, jQuery plugins and Custom JS code -->
<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/app.js"></script>
<script src="js/pages/formsWizard.js"></script>
<?php
/**
 * template_end.php
 *
 * Author: pixelcave
 *
 * The last block of code used in every page of the template
 *
 * We put it in a separate file for consistency. The reason we
 * separated template_scripts.php and template_end.php is for enabling us
 * put between them extra javascript code needed only in specific pages
 *
 */
?><script>$(function(){ FormsWizard.init(); });</script>
    </body>
</html>