<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plan extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('reseller_login_model');
		$this->load->model('reseller_panel_model');
		$this->load->model('general_model');
        delete_cache();
		  if($this->session->userdata('reseller_logged_in')!="")
		  {
		   
		  }else if($this->session->userdata('agent_logged_in')!="")
		  {
            $data['login_info']     = $this->session->userdata('agent_logged_in');
        
            $user_id                = $data['login_info']['resellerIndexID'];

            $agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
            if($agentData['plans'] == 1){
                $this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
                redirect('Reseller_panel/index', 'refresh'); 
            }
		 
		  }else{
		  
			redirect('login','refresh');
		  }
  
  
	}
	
	public function index()
	{   
    	
	
	}

        public function reseller_plans()
	{
        $data['isEditMode'] = 1; 
	    if($this->session->userdata('reseller_logged_in')){
    		$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
    		
    		$rID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_logged_in')){
    		$data['login_info'] 	= $this->session->userdata('agent_logged_in');
    		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

            if($agentData['plans'] == 2){
                $data['isEditMode'] = 0; 
            }
            
    		$rID 				= $data['login_info']['resellerIndexID'];
		}	
			  $data['primary_nav']  = primary_nav();
			  $data['template']   = template_variable();
		
      
			 
             $reseller = $this->reseller_panel_model->get_select_data('tbl_reseller', array('plan_id','downgradePlanID'), array('resellerID'=>$rID));
             
             $paln =  $reseller['plan_id'];
             $data['settingEnableOption'] = 0; 
             if($paln != null){
                $palnIDArr =  explode(',', $paln);
                foreach ($palnIDArr as $planID) {
                    $planTypeGet = $this->reseller_panel_model->get_select_data('plans', array('merchant_plan_type'), array('plan_id'=>$planID));
                    if($planTypeGet['merchant_plan_type'] == 'Free'){
                        $data['settingEnableOption'] = 1;
                    }
                }
                
             }

             $data['downgradePlanID'] = $reseller['downgradePlanID']; 
             $data['resellerID'] = $rID; 
             $planname =  $this->reseller_panel_model->get_friendly_plan_name( $paln, $rID);
             $data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway','');
			  $data['resellers'] = $planname;
			  
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/reseller_plans', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
 
	}
	
	
	public function get_merchant_gateway()
	{
	    if(!empty($this->czsecurity->xssCleanPostInput('mID')))
	    {
	        $user_id    = $this->czsecurity->xssCleanPostInput('mID');
	      $condition	= array('merchantID'=>$user_id);
	     $gatew = $this->general_model->get_table_data('tbl_merchant_gateway',$condition);
	     echo json_encode($gatew); die;
	    }
	    return false;
	}
	
	
	
public function get_plan_data()
{
    if($this->session->userdata('reseller_logged_in')){
        $data['login_info'] = $this->session->userdata('reseller_logged_in');
        $rID = $data['login_info']['resellerID'];
    }
    if($this->session->userdata('agent_logged_in')){
        $data['login_info'] = $this->session->userdata('agent_logged_in');
        $rID = $data['login_info']['resellerIndexID'];
    }   
    
    $res=array();
        $planID = $this->czsecurity->xssCleanPostInput('plan_id');
        $plan = $this->reseller_panel_model->reseller_plan_by_id($planID, $rID);
        $gateway='';
        if($plan['is_free_trial']){
        	$is_free_trial = 1;
            $free_trial_day = $plan['free_trial_day'];
        }else{
        	$is_free_trial = 0;
            $free_trial_day = 0;
        }
       
        if(!empty( $plan) && $plan['gatewayAccess']==0)
        {
          $all_gateway  = $this->general_model->get_table_data('tbl_master_gateway','');
          $option='';
         foreach($all_gateway as $gat_data){ 
								   $option.='<option value="'.$gat_data['gateID'].'" >'.$gat_data['gatewayName'].'</option>';
								   } 
             $gateway=    '<div class="form-group ">
                                              
				 <label class="col-md-4 control-label" for="card_list">Friendly Name</label>
						<div class="col-md-6">
						   <input type="text" id="frname" name="frname"  class="form-control " />
								
						</div>
						
					</div>

					<div class="form-group ">
                                              
						<label class="col-md-4 control-label" for="card_list">Gateway Type</label>
						 <div class="col-md-6">
						   <select id="gateway_opt" name="gateway_opt" onchange="set_gateway_data(this);"  class="form-control">
						          <option value="" >Select Gateway</option>'.$option.'
							</select>
							</div>
					</div>
					
			<div id="nmi_div" style="display:none">			
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Username</label>
                        <div class="col-md-6">
                             <input type="text" id="nmiUser" name="nmiUser" class="form-control"  placeholder="Username">
                        </div>
                   </div>
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Password</label>
                       <div class="col-md-6">
                            <input type="text" id="nmiPassword" name="nmiPassword" class="form-control"  placeholder="Password">
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_cr_status" name="nmi_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="nmi_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="nmi_ach_status" name="nmi_ach_status"  >
                           
                        </div>
                    </div>
				</div>
				
               <div id="auth_div" style="display:none">					
					
				   <div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API LoginID</label>
                        <div class="col-md-6">
                             <input type="text" id="apiloginID" name="apiloginID" class="form-control" placeholder="API LoginID">
                        </div>
                    </div>
					
				 <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Transaction Key</label>
                        <div class="col-md-6">
                            <input type="text" id="transactionKey" name="transactionKey" class="form-control"  placeholder="Transaction Key">
                           
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_cr_status">Credit Card</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_cr_status" name="auth_cr_status" checked="checcked" >
                           
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-4 control-label" for="auth_ach_status">Electronic Check</label>
                        <div class="col-md-6">
                            <input type="checkbox" id="auth_ach_status" name="auth_ach_status"  >
                           
                        </div>
                    </div>
			 </div>	
			
			
				<div id="pay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">PayTrace Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paytraceUser" name="paytraceUser" class="form-control" placeholder="PayTrace  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">PayTrace Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paytracePassword" name="paytracePassword" class="form-control"  placeholder="PayTrace  password">
                           
                        </div>
                    </div>
					
				</div>	
				
				<div id="paypal_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">API Username</label>
                        <div class="col-md-6">
                             <input type="text" id="paypalUser" name="paypalUser" class="form-control" placeholder="Enter  Username">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">API Password</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalPassword" name="paypalPassword" class="form-control"  placeholder="Enter  password">
                           
                        </div>
                    </div>
					
					  <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Signature</label>
                        <div class="col-md-6">
                            <input type="text" id="paypalSignature" name="paypalSignature" class="form-control"  placeholder="Enter  Signature">
                           
                        </div>
                    </div>
					
				</div>	
				<div id="stripe_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Publishable Key</label>
                        <div class="col-md-6">
                             <input type="text" id="stripeUser" name="stripeUser" class="form-control" placeholder="Publishable Key">
                        </div>
						
                    </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret API Key</label>
                        <div class="col-md-6">
                            <input type="text" id="stripePassword" name="stripePassword" class="form-control"  placeholder="Secret API Key">
                           
                        </div>
                    </div>
					
				</div>	
				
				 <div id="usaepay_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">USAePay Transaction Key</label>
                        <div class="col-md-6">
                             <input type="text" id="transtionKey" name="transtionKey" class="form-control"; placeholder="USAePay Transaction Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">USAePay PIN</label>
                        <div class="col-md-6">
                            <input type="text" id="transtionPin" name="transtionPin" class="form-control"  placeholder="USAePay PIN">
                           
                        </div>
                    </div>
				</div>	
				
					 <div id="heartland_div" style="display:none" >		
					<div class="form-group">
                        <label class="col-md-4 control-label" for="customerID">Public Key</label>
                        <div class="col-md-6">
                             <input type="text" id="heartpublickey" name="heartpublickey" class="form-control"; placeholder="Public Key">
                        </div>
                  </div>
					
				    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Secret Key</label>
                        <div class="col-md-6">
                            <input type="text" id="heartsecretkey" name="heartsecretkey" class="form-control"  placeholder="Secret Key">
                           
                        </div>
                    </div>
				</div>	
				
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="card_number">Merchant ID</label>
                        <div class="col-md-6">
                            <input type="text" id="gatewayMerchantID" name="gatewayMerchantID" class="form-control"  placeholder="Merchant ID (optional)">
                           
                        </div>
						
                   </div>';
                    $res['gateway'] =''; 
        $res['status']  ='success';
        }
        else
        {
             $res['gateway'] =''; 
             $res['status']  ='error';
        }
        
        $res['is_free_trial']  = $is_free_trial;
        $res['free_trial_day'] = $free_trial_day;
        
        $res['merchant_plan_type'] = $plan['merchant_plan_type'];
        echo json_encode($res); die;
    }
	public function updateDowngradePlan()
    {
        if(!empty($this->czsecurity->xssCleanPostInput('plan_id')))
        {
            $plan_id    = $this->czsecurity->xssCleanPostInput('plan_id');
            $resellerID = $this->czsecurity->xssCleanPostInput('resellerID');
            $condition  = array('resellerID'=>$resellerID);
            $input_data = array('downgradePlanID' => $plan_id);
                        
            $gatew      = $this->general_model->update_row_data('tbl_reseller', $condition, $input_data);

            $this->session->set_flashdata('success', 'Downgrade Plan Updated Successfully');

        }else{
            $this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Please select downgrade plan.</div>');
        }
        redirect(base_url('Plan/reseller_plans'));
    }


}