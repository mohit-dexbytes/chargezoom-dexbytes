<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use \Chargezoom\Core\Http\SessionRequest;
class Home extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->config('quickbooks');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
	   	$this->load->model('reseller_panel_model');
		delete_cache();
		 if($this->session->userdata('reseller_logged_in')!="")
		  {
		   
		  }else if($this->session->userdata('agent_logged_in')!="")
		  {
		 
		  }else{
		  
			redirect('login','refresh');
		  }
  
	}
	
	

	public function index()
	{   
		
	}
	
	
		public function agent_commissions()
	{
	   	$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		  	if($this->session->userdata('reseller_logged_in')){
		$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
		$resellerID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_logged_in')){
		$data['login_info'] 	= $this->session->userdata('agent_logged_in');
		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

		
			$resellerID 				= $data['login_info']['resellerIndexID'];
		}	 

		$data['months']  =array(array('id'=>'1','val'=>'Jan'), array('id'=>'2','val'=>'Feb'), array('id'=>'3','val'=>'Mar'), array('id'=>'4','val'=>'Apr'), array('id'=>'5','val'=>'May'), array('id'=>'6','val'=>'June'), array('id'=>'7','val'=>'July'), array('id'=>'8','val'=>'Aug'), array('id'=>'9','val'=>'Sep'), array('id'=>'10','val'=>'Oct'), array('id'=>'11','val'=>'Nov'), array('id'=>'12','val'=>'Dec') );
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/agent_Commissions', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}
	

	  public function update_card_data()
	  {
	      
		if($this->session->userdata('reseller_logged_in')){
		$da['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
		$resellerID 				= $da['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_logged_in')){
		$da['login_info'] 	= $this->session->userdata('agent_logged_in');
		
		$resellerID 				= $da['login_info']['resellerIndexID'];
		}	
		$payOption = $this->czsecurity->xssCleanPostInput('payOptionType');
		$merchantcardID = $this->czsecurity->xssCleanPostInput('edit_cardID');
		$this->load->model('card_model');

		$friendlyname = $this->czsecurity->xssCleanPostInput('edit_friendlyname');
		if($payOption == 2){
			$accountNumber  = $this->czsecurity->xssCleanPostInput('edit_acc_number'); 
			$routeNumber = $this->czsecurity->xssCleanPostInput('edit_route_number');
			
			$accountType      = $this->czsecurity->xssCleanPostInput('edit_acct_type');
			$accountHolderType      = $this->czsecurity->xssCleanPostInput('edit_acct_holder_type');
		}else{
			$card_no  = $this->czsecurity->xssCleanPostInput('edit_card_number'); 
			$expmonth = $this->czsecurity->xssCleanPostInput('edit_expiry');
			$exyear   = $this->czsecurity->xssCleanPostInput('edit_expiry_year');
			$cvv      = $this->czsecurity->xssCleanPostInput('edit_cvv');
		}  
		
		      
		$data1 = array(
		    "Billing_Addr1" => $this->czsecurity->xssCleanPostInput('edit_address1'),
		    "Billing_Addr2" => $this->czsecurity->xssCleanPostInput('edit_address2'),
		    "Billing_Country" => $this->czsecurity->xssCleanPostInput('edit_country'),
		    "Billing_State" => $this->czsecurity->xssCleanPostInput('edit_state'),
		    "Billing_City" => $this->czsecurity->xssCleanPostInput('edit_city'),
		    "Billing_Zipcode" => $this->czsecurity->xssCleanPostInput('edit_zipcode'),
		);
				    
				
		$merchantID = $this->czsecurity->xssCleanPostInput('merchID');				
		$condition = array('merchantListID'=>$merchantID);

		$insert_array =  array( 
				'resellerID'  =>$resellerID,
				'merchantListID'=>$merchantID,
				"Billing_Addr1" => $this->czsecurity->xssCleanPostInput('edit_address1'),
				"Billing_Addr2" => $this->czsecurity->xssCleanPostInput('edit_address2'),
				"Billing_Country" => $this->czsecurity->xssCleanPostInput('edit_country'),
				"Billing_State" => $this->czsecurity->xssCleanPostInput('edit_state'),
				"Billing_City" => $this->czsecurity->xssCleanPostInput('edit_city'),
				"Billing_Zipcode" => $this->czsecurity->xssCleanPostInput('edit_zipcode'),
		);
		if($payOption == 2){
			if($accountNumber != null ){
				$insert_array['accountNumber']   		= $accountNumber;
				$insert_array['merchantFriendlyName']   = 'Checking - ' . substr($accountNumber, -4);
			
				$insert_array['accountName']   			= 'Checking - ' . substr($accountNumber, -4);
			}
			
			$insert_array['routeNumber']   			= $routeNumber;
			
			$insert_array['accountType']   			= $accountType;
			$insert_array['accountHolderType']   	= $accountHolderType;
			$insert_array['secCodeEntryMethod']     = 'WEB';
			$insert_array['CardType']        		= 'Checking';
		}else{
			if($card_no != null){
				$card_type = $this->general_model->getcardType($card_no);
				$insert_array['MerchantCard']   = $this->card_model->encrypt($card_no);
				$insert_array['merchantFriendlyName']   = $card_type . ' - ' . substr($card_no, -4);
				$insert_array['CardType']        = $card_type;
			}

			$insert_array['CardCVV']        = $this->card_model->encrypt($cvv);
			$insert_array['CardMonth']        =$expmonth;
			$insert_array['CardYear']        =$exyear;
			
		}
		if($merchantcardID!="")
		{
			
			
			$insert_array['createdAt']    = date('Y-m-d H:i:s');
			$id = $this->card_model->update_card_data($condition, $insert_array);  
			$id = $merchantcardID;		
			$this->session->set_flashdata('success', 'Billing Info Updated Successfully');
		     
	    }
	    else
	    {
	    	
			$insert_array['createdAt']    = date('Y-m-d H:i:s');

			$id = $this->card_model->insert_card_data($insert_array);  
			$this->session->set_flashdata('success', 'Billing Info Saved Successfully');
	 
			if($id){

		   		if(ENVIRONMENT == 'production' && $resellerID == HATCHBUCK_RESELLER_ID){
					/* Start campaign in hatchbuck CRM*/  
                    $this->load->library('hatchBuckAPI');
                    $condition = array('merchID' => $merchantID);
    				$merchantData = $this->general_model->get_row_data('tbl_merchant_data',$condition);
                    
                    $merchantData['merchant_type'] = 'Admin Updated';        
                    
                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                    if($status['statusCode'] == 400){
                        $resource = $this->hatchbuckapi->createContact($merchantData);
                        if($resource['contactID'] != '0'){
                            $contact_id = $resource['contactID'];
                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
                        }
                    }
                    /* End campaign in hatchbuck CRM*/ 
				}
			}
	     }
			 
		 if( $id ){
		 	$chk_condition = array('merchID'=>$merchantID);
		 	$insert_merchantdata = array('payOption' => $payOption,'cardID' => $id );
			$updt = $this->general_model->update_row_data('tbl_merchant_data',$chk_condition, $insert_merchantdata);    		 
		 }else{
		 
		   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>'); 
		 }
		 
	    	redirect('Reseller_panel/merchant_details/'.$merchantID,'refresh');
	  }
	  
	 
		 
   public function get_card_edit_data()
   {
	    $this->load->model('card_model');
	  if($this->czsecurity->xssCleanPostInput('cardID')!=""){
		  $cardID = $this->czsecurity->xssCleanPostInput('cardID');
		  $data   = $this->card_model->get_single_mask_card_data($cardID);
		  
		  	$re = '/\d(?=\d{4})/m';
				     	$str =$data['CardNo'];
					   	 $data['CardNo']     =  preg_replace($re, "X", $str); 
		  
		  
		  
		  echo json_encode(array('status'=>'success', 'card'=>$data));
		  die;
	  } 
	  echo json_encode(array('status'=>'success'));
	   die;
    }
    
	
		public function delete_card_data(){
		
		        $this->load->model('card_model');
			    $cardID =  $this->uri->segment('3'); 
			     $merchantID =  $this->uri->segment('4'); 
			    $con = array('merchantCardID'=>$cardID);
		       $sts =  $this->card_model->delete_card_data($con);
				 if($sts){
				 	$chk_condition = array('merchID'=>$merchantID);
		 			$insert_merchantdata = array('payOption' => 0,'cardID' => 0 );
					$updt = $this->general_model->update_row_data('tbl_merchant_data',$chk_condition, $insert_merchantdata); 
					$this->session->set_flashdata('success', 'Successfully Deleted');
		   
		   
		   
		   		 
		 }else{
		 
		   	$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
		 }
		 
	    	redirect('Reseller_panel/merchant_details/'.$merchantID,'refresh');
	}		
	
	
		public function gateway()
	{
	
        $mId=	$this->uri->segment('3');
        $data['isEditMode'] = 1;
        if($mId!="")
        {
    	    $data['primary_nav'] 	= primary_nav();
    		$data['template'] 		= template_variable();
    		
    		if($this->session->userdata('reseller_logged_in')){
    			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
    		
    			$resellerID 				= $data['login_info']['resellerID'];
    		}
    		if($this->session->userdata('agent_logged_in'))
    		{

    			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
    			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

				if($agentData['userMerchantEdit'] == 0){
					$data['isEditMode'] = 0;
				}
				if($agentData['allMerchantEdit'] == 1){
					$data['isEditMode'] = 1;
				}
    			$resellerID 				= $data['login_info']['resellerIndexID'];
    		}	 
    			
    		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway','');
			  $get_data = $this->general_model->get_gateway($mId, $resellerID);
			  $data['get_data']    = $get_data; 
		      
			  $this->load->view('template/template_start', $data);
			  $this->load->view('template/page_head', $data);
			  $this->load->view('reseller_pages/page_merchant', $data);
			  $this->load->view('template/page_footer',$data);
			  $this->load->view('template/template_end', $data);
        }
        else
        {
          redirect('Reseller_panel/index','refresh');  
        }
	
	
	}
	
	
		
	//---------------- To add gateway  ---------------//
	
	public function create_gateway(){
		$signature = '';
		$extra1 = '';
		$cr_status=1;
		$ach_status=0;
		$isSurcharge = $surchargePercentage = 0;
	
		if(!empty($this->czsecurity->xssCleanPostInput('merchID')))
		{
			$user_id =$this->czsecurity->xssCleanPostInput('merchID');
			$enable_level_three_data = isset($_POST['enable_level_three_data']) ? $_POST['enable_level_three_data'] : 0;
			$condition				= array('merchantID'=>$user_id);
			$gmID                  =$this->czsecurity->xssCleanPostInput('gatewayMerchantID');	
			if($this->czsecurity->xssCleanPostInput('gateway_opt')=='1')
			{
				$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
				if($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;
				
				if($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
				$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='2'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');
				
				if($this->czsecurity->xssCleanPostInput('auth_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;
				
				if($this->czsecurity->xssCleanPostInput('auth_ach_status'))
				$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='3'){
				$this->load->config('paytrace');
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
				$signature  = PAYTRACE_INTEGRATOR_ID; 

				if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='4'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
				$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
			
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='5'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
			
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='6'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='7'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
				if($this->czsecurity->xssCleanPostInput('heart_cr_status')){
					$cr_status       =  1;
				}else{
					$cr_status       =  0;
				}
				
				if($this->czsecurity->xssCleanPostInput('heart_ach_status')){
					$ach_status       = 1;
				}
				else{
					$ach_status       =  0;
				}

			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='8'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
				$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
			} else if($this->czsecurity->xssCleanPostInput('gateway_opt')=='9'){
				$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
					
				if($this->czsecurity->xssCleanPostInput('cz_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if($this->czsecurity->xssCleanPostInput('cz_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '10') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
				if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
					$isSurcharge = 1;
				}
				else{
					$isSurcharge = 0;
				}
				$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '11') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}  else if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '12') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
				$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
				$gmID            = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
				if ($this->czsecurity->xssCleanPostInput('TSYS_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('TSYS_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			}else if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '13') {
				$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
				$nmipassword     = '';
				if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			} else if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '14') {
                $nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
                $nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
                $gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
                $signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
                if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
                    $cr_status = 1;
                } else {
                    $cr_status       =  0;
                }

                if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
                    $ach_status = 1;
                } else {
                    $ach_status = 0;
                }

            } else if ($this->input->post('gateway_opt')  == '15') {
				$nmiuser         = $this->input->post('payarcUser');
				$nmipassword     = '';
				$cr_status       =  1;
				$ach_status       =  0;
			}else if ($this->input->post('gateway_opt')  == '17') {
				$nmiuser         = $this->input->post('maverickAccessToken', true);
				$nmipassword     = $this->input->post('maverickTerminalId', true);
				if ($this->input->post('maverick_cr_status'))
					$cr_status       =  1;
				else
					$cr_status       =  0;

				if ($this->input->post('maverick_ach_status'))
					$ach_status       = 1;
				else
					$ach_status       =  0;
			
			}else if ($this->input->post('gateway_opt') == '16') {
                $nmiuser         = $this->input->post('EPXCustNBR');
                $nmipassword     = $this->input->post('EPXMerchNBR');
                $signature     = $this->input->post('EPXDBANBR');
                $extra1   = $this->input->post('EPXterminal');
                if ($this->input->post('EPX_cr_status'))
                    $cr_status       =  1;
                else
                    $cr_status       =  0;

                if ($this->input->post('EPX_ach_status'))
                    $ach_status       = 1;
                else
                    $ach_status       =  0;
                    
            } 

			$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');
			
			$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt') ;
			$frname           =$this->czsecurity->xssCleanPostInput('frname');
			
					
			$insert_data    = array(
				'gatewayUsername'=>$nmiuser,
				'gatewayPassword'=>$nmipassword, 
				'gatewayMerchantID'=>$gmID,
				'gatewaySignature' =>$signature,
				'extra_field_1' =>$extra1,
				'gatewayType'=>$gatetype,
				'merchantID'=>$user_id, 
				'gatewayFriendlyName'=>$frname ,
				'creditCard'       =>$cr_status,
				'echeckStatus'      =>$ach_status,
				'isSurcharge' => $isSurcharge,
				'surchargePercentage' => $surchargePercentage,
				'enable_level_three_data' => $enable_level_three_data
			);
	
			if($gid = $this->general_model->insert_row('tbl_merchant_gateway', $insert_data)){
				$val1 = array(
                    'merchantID' => $user_id,
                );
                
                $update_data1 = array('set_as_default' => '0', 'updatedAt' => date('Y:m:d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_gateway', $val1, $update_data1);
                
                $val = array(
                    'gatewayID' => $gid,
                );
                $update_data = array('set_as_default' => '1', 'updatedAt' => date('Y:m:d H:i:s'));
				$this->general_model->update_row_data('tbl_merchant_gateway', $val, $update_data);

				$merchant_gateway_data = $this->general_model->get_select_data('tbl_merchant_gateway', array('gatewayID'), array('merchantID'=>$user_id, 'enable_level_three_data' => 1));
            	if($merchant_gateway_data){
            		$enable_level_three = 1;
            	}else{
            		$enable_level_three = 0;
            	}
            	$this->general_model->update_row_data('tbl_merchant_data', ['merchID' => $user_id], ['enable_level_three' => $enable_level_three]);
            	// add level data
				if($user_id && $enable_level_three){
					$this->addDefaultLevelThreeData($user_id);
				}

				$this->session->set_flashdata('success', 'Successfully Inserted');
			}else{
				$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
			}
			redirect(base_url('Reseller_panel/create_merchant/'.$user_id));
		} else {
			redirect('Reseller_panel/index','refresh');  
		}			
	
	}

	//----------- TO update the gateway  --------------//
	
	public function update_gateway() 
	{
	     if($this->czsecurity->xssCleanPostInput('gatewayEditID')!="" ){
	         	$ach_status       =  0;
	         $signature='';
	         $extra1 = '';
	         $cr_status=1;
			$isSurcharge = $surchargePercentage = 0;
	        $gmID                  =$this->czsecurity->xssCleanPostInput('mid'); 
	         $merchantID =$this->czsecurity->xssCleanPostInput('editmerchID');
	        $enable_level_three_data = isset($_POST['enable_level_three_data']) ? $_POST['enable_level_three_data'] : 0;
	         
				      
				        $id = $this->czsecurity->xssCleanPostInput('gatewayEditID');
						
						 $chk_condition = array('gatewayID'=>$id);
						 
				   if($this->czsecurity->xssCleanPostInput('gateway')=='NMI'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword1');
   		      	    if($this->czsecurity->xssCleanPostInput('nmi_cr_status1'))
   		      			$cr_status       =  1;
					else
						$cr_status       =  0;

   		      		if($this->czsecurity->xssCleanPostInput('nmi_ach_status1'))
   		      		$ach_status       = 1;
   		      		else
   		      			$ach_status       =  0;
   		      		
   		      		
   		      		
				
				}
				
				 if(strtolower($this->czsecurity->xssCleanPostInput('gateway'))=='authorize.net'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey1');
   		      		
   		      	   	if($this->czsecurity->xssCleanPostInput('auth_cr_status1'))
   		      			$cr_status       =  1;
					else
						$cr_status       =  0;

   		      		if($this->czsecurity->xssCleanPostInput('auth_ach_status1'))
   		      		$ach_status       = 1;
   		      		else
   		      			$ach_status       =  0;
				
				}
				
				if($this->czsecurity->xssCleanPostInput('gateway')=='Paytrace' || $this->czsecurity->xssCleanPostInput('gateway') == 'Pay Trace'){
					$this->load->config('paytrace');
					$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword1');
					$signature  = PAYTRACE_INTEGRATOR_ID;

					if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;

					if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
				
				} if($this->czsecurity->xssCleanPostInput('gateway')=='Paypal'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword1');
					$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature1');
				
				}
				
			 if($this->czsecurity->xssCleanPostInput('gateway')=='Stripe'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword1');
				
				}
				 if($this->czsecurity->xssCleanPostInput('gateway')=='USAePay'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin1');
				
				}
				if($this->czsecurity->xssCleanPostInput('gateway')=='Heartland'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey1');
   		      		if ($this->czsecurity->xssCleanPostInput('heart_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;

					if ($this->czsecurity->xssCleanPostInput('heart_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
   		      		
				
				}
				
					if($this->czsecurity->xssCleanPostInput('gateway')=='Cybersource')
				{
					$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber1');
					$signature       = $this->czsecurity->xssCleanPostInput('secretKey1');
				
				}

				if($this->czsecurity->xssCleanPostInput('gateway')=='Chargezoom'){
					$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser1');
   		      		$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword1');
   		      	    if($this->czsecurity->xssCleanPostInput('cz_cr_status1'))
   		      			$cr_status       =  1;
					else
						$cr_status       =  0;
   		      		
   		      		if($this->czsecurity->xssCleanPostInput('cz_ach_status1'))
   		      		$ach_status       = 1;
   		      		else
   		      			$ach_status       =  0;
				}

				if ($this->czsecurity->xssCleanPostInput('gateway')== iTransactGatewayName) {
					$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername1');
					$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY1');
					if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;
	
					if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
	
					if ($this->czsecurity->xssCleanPostInput('add_surcharge_box1')){
						$isSurcharge = 1;
					}
					else{
						$isSurcharge = 0;
					}
					$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage1');
				} 

				if ($this->czsecurity->xssCleanPostInput('gateway')  == FluidGatewayName) {
					$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser1');
					$nmipassword     = '';
					if ($this->czsecurity->xssCleanPostInput('fluid_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;
	
					if ($this->czsecurity->xssCleanPostInput('fluid_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
				}
				if ($this->czsecurity->xssCleanPostInput('gateway')  == TSYSGatewayName) {
					$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser1');
					$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword1');
					$gmID            = $this->czsecurity->xssCleanPostInput('TSYSMerchantID1');
					if ($this->czsecurity->xssCleanPostInput('TSYS_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;
	
					if ($this->czsecurity->xssCleanPostInput('TSYS_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
				} 
				if ($this->czsecurity->xssCleanPostInput('gateway')  == BASYSGatewayName) {
					$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser1');
					$nmipassword     = '';
					if ($this->czsecurity->xssCleanPostInput('basys_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;
	
					if ($this->czsecurity->xssCleanPostInput('basys_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
				} 
				if ($this->czsecurity->xssCleanPostInput('gateway')  == 'CardPointe') {
					$nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser1');
					$nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword1');
					$gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID1');
					$signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName1');
					if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status1', true)) {
						$cr_status = 1;
					} else {
						$cr_status = 0;
					}
	
					if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status1', true)) {
						$ach_status = 1;
					} else {
						$ach_status = 0;
					}
				}
				if ($this->input->post('gateway')  == PayArcGatewayName) {
					$nmiuser         = $this->input->post('payarcUser1');
					$nmipassword     = '';
					$cr_status       =  1;
					$ach_status       =  0;
				}else if ($this->input->post('gateway')  == MaverickGatewayName) {
					$nmiuser         = $this->input->post('maverickAccessToken1');
					$nmipassword     = $this->input->post('maverickTerminalId1');
					if ($this->input->post('maverick_cr_status1'))
						$cr_status       =  1;
					else
						$cr_status       =  0;
	
					if ($this->input->post('maverick_ach_status1'))
						$ach_status       = 1;
					else
						$ach_status       =  0;
				}
				if ($this->input->post('gateway') == EPXGatewayName) {
	                $nmiuser         = $this->input->post('EPXCustNBR1');
	                $nmipassword     = $this->input->post('EPXMerchNBR1');
	                $signature     = $this->input->post('EPXDBANBR1');
	                $extra1   = $this->input->post('EPXterminal1');
	                if ($this->input->post('EPX_cr_status1'))
	                    $cr_status       =  1;
	                else
	                    $cr_status       =  0;

	                if ($this->input->post('EPX_ach_status1'))
	                    $ach_status       = 1;
	                else
	                    $ach_status       =  0;
	                    
	            } 

				    
					$gatetype         = $this->czsecurity->xssCleanPostInput('gateway') ;
					$frname           =$this->czsecurity->xssCleanPostInput('fname');
					
					
				$insert_data    = array(
					'gatewayUsername'=>$nmiuser,
					'gatewayPassword'=>$nmipassword, 
					'gatewayMerchantID'=>$gmID,
					'gatewayFriendlyName'=>$frname ,
					'gatewaySignature' =>$signature,
					'extra_field_1' =>$extra1,
					'creditCard'       =>$cr_status,
					'echeckStatus'      =>$ach_status,
					'isSurcharge' => $isSurcharge,
					'surchargePercentage' => $surchargePercentage,
					'enable_level_three_data' => $enable_level_three_data
				);

	                    if($this->general_model->update_row_data('tbl_merchant_gateway',$chk_condition, $insert_data) ){

	                    	$merchant_gateway_data = $this->general_model->get_select_data('tbl_merchant_gateway', array('gatewayID'), array('merchantID'=>$merchantID, 'enable_level_three_data' => 1));
	                    	if($merchant_gateway_data){
	                    		$enable_level_three = 1;
	                    	}else{
	                    		$enable_level_three = 0;
	                    	}
	                    	$this->general_model->update_row_data('tbl_merchant_data', ['merchID' => $merchantID], ['enable_level_three' => $enable_level_three]);
	                    	// add level 
							if($merchantID && $enable_level_three){
								$this->addDefaultLevelThreeData($merchantID);
							}
							$this->session->set_flashdata('success', 'Successfully Updated');
							
				   }else{
					 
				   	$this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong></div>'); 
				
				 }
			
	                   redirect(base_url('Reseller_panel/create_merchant/'.$merchantID));
			
	       }		
	     
	     else
        {
          redirect('Reseller_panel/index','refresh');  
        }
	 
	 } 
	

	
	public function get_gatewayedit_id()
     {
		
        $id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
		'gatewayID' => $id,
		);
		
		$data = $this->general_model->get_row_data('tbl_merchant_gateway',$val);
		$data['gateway'] = getGatewayNames($data['gatewayType']);
		$data['enable_level_three_data'] = ($data['enable_level_three_data']) ? true : false;
        echo json_encode($data);
     }
	


     public function set_gateway_default()
	{
	    
	    
	    if(!empty($this->czsecurity->xssCleanPostInput('gatewayid')))
	    {
	        
	     
		    $merchID   =$this->czsecurity->xssCleanPostInput('defmerchID');
		     
		    $id = $this->czsecurity->xssCleanPostInput('gatewayid');
			$val = array(
			'gatewayID' => $id,
			);
			$val1 = array(
			'merchantID' => $merchID,
			);
			$update_data1 = array('set_as_default'=>'0', 'updatedAt'=>date('Y:m:d H:i:s'));
			$this->general_model->update_row_data('tbl_merchant_gateway',$val1, $update_data1);
			$update_data = array('set_as_default'=>'1', 'updatedAt'=>date('Y:m:d H:i:s'));
				 
		    $this->general_model->update_row_data('tbl_merchant_gateway',$val, $update_data);
				 
			$this->session->set_flashdata('success', 'Successfully Default Gateway Updated');	 
			  
	    }else{
	        
	           $this->session->set_flashdata('message','<div class="alert alert-danger"><strong>Error</strong> Invalid Request</div>');   
	    }
	    redirect(base_url('home/gateway/'.$merchID));	  
	}
	
	
	public function delete_gateway()
	{
	
		 $gatewayID = $this->czsecurity->xssCleanPostInput('merchantgatewayid');
		 $merchantdata=$this->general_model->get_row_data('tbl_merchant_gateway',array('gatewayID'=>$gatewayID));
	     $merchantID=$merchantdata['merchantID'];
	
         $condition1 =  array('gatewayID'=>$gatewayID,'set_as_default'=>'0'); 
         
      
         $del1      = $this->general_model->delete_row_data('tbl_merchant_gateway', $condition1);
            if($del1)
            {
				$this->session->set_flashdata('success', 'Successfully Deleted');
                 
                   
                   
            }
            else 
            {   
                     $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error:</strong> This gateway can no be deleted.</div>');
                      
                redirect(base_url('Reseller_panel/create_merchant/'.$merchantID));     
            }
           	
         redirect(base_url('Reseller_panel/create_merchant/'.$merchantID));
    
	  
	}
	
	
	public function invoice_details()
	{
	 	 	$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		
		  	if($this->session->userdata('reseller_logged_in')){
		$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
		$resellerID 				= $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_logged_in')){
		$data['login_info'] 	= $this->session->userdata('agent_logged_in');
		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

		
		$resellerID 				= $data['login_info']['resellerIndexID'];
		}
	
		$invoiceID = $this->uri->segment('3');
	    $data['plans_overview']  =$this->general_model->get_invoice_overview($invoiceID);
        $data['reseller_data']=$this->general_model->get_row_data('tbl_reseller',array('resellerID'=>$resellerID));
	    $data['invoice']    = $this->general_model->get_row_data('tbl_reseller_billing_invoice',array('invoice'=>$invoiceID)); 
	     $data['items']    = $this->general_model->get_table_data('tbl_reseller_invoice_item',array('resellerInvoice'=>$invoiceID)); 
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/reseller_invoice_details', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	}


		public function invoice_details_print()
	{
	    
	
		 $invoiceID             =  $this->uri->segment(3);  
	  if($invoiceID!="")
     { 
		$data['template'] 		= template_variable();
		$condition2 			= array('invoice'=>$invoiceID);
		
		   $plans_overview  =$this->general_model->get_invoice_overview($invoiceID);
		$invoice_data           = $this->general_model->get_row_data('tbl_reseller_billing_invoice',$condition2);
	   $resellerID= $invoice_data['resellerInvoiceID'];
	
		$logo = CZLOGO;
		
	$company_data=	$this->general_model->get_select_data('tbl_admin',array('adminCompanyName',	'adminEmail','billingfirstName','billinglastName','adminCountry','adminCity','adminState',
		'primaryContact','adminAddress','adminAddress2','zipCode'),array('adminID'=>'1'));
		$reseller_data=	$this->general_model->get_select_data('tbl_reseller',array('resellerfirstName','lastName','primaryContact','resellerEmail','resellerCompanyName'),array('resellerID'=>$resellerID));
	
	 $tt = "\n".$company_data['billingfirstName']." ".$company_data['billinglastName']."<br/>".$company_data['adminCompanyName']."<br/>".($company_data['adminAddress'])."<br/> ".($company_data['adminAddress2'])."<br/>".($company_data['adminCity']).", ".($company_data['adminState'])." ".($company_data['zipCode']).'<br/>'.($company_data['adminCountry']); 
	 $email =  $reseller_data['resellerEmail'];
		
	    $invoice_items  = $this->general_model->get_table_data('tbl_reseller_invoice_item',array('resellerInvoice'=>$invoiceID));
	  
	     	$no = $invoice_data['invoiceNumber'];
			$pdfFilePath = "$no.pdf"; 
			
	 ini_set('memory_limit','320M'); 
	  $this->load->library("TPdf");
	
	$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);    
	define(PDF_HEADER_TITLE,'Reseller Invoice');
	define(PDF_HEADER_STRING,'');
    $pdf->setPrintHeader(false);
    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Chargezoom 1.0');
    $pdf->SetTitle($data['template']['title']);
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetFont('dejavusans', '', 10, '', true);   
  // set cell padding
		$pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
		$pdf->setCellMargins(1, 1, 1, 1);
    // Add a page
  
  $pdf->AddPage();
  
   
		$y = 20;
		$logo_div='<div style="text-align:left; float:left ">
		<img src="'.$logo.'"  border="0" />
		</div>';

		$pdf->SetFillColor(255, 255, 255);

		$pdf->writeHTMLCell(80, 50, '', $y, $logo_div, 0, 0, 1, true, 'J', true);
		
	    $y = 50;
		// set color for background
		$pdf->SetFillColor(255, 255, 255);

		$pdf->SetTextColor(51, 51, 51);

		$pdf->writeHTMLCell(80, 50, '', $y, "<b>From:</b><br/>".$tt, 0, 0, 1, true, 'J', true);
		$pdf->SetTextColor(51, 51, 51);

		$pdf->writeHTMLCell(80, 50, '', '', '<b>Invoice: </b>'.$invoice_data['invoiceNumber']."<br/><br/>".'<b>Due Date: </b>'.date("m/d/Y",strtotime($invoice_data['DueDate'])),0, 1, 1, true, 'J', true);
		
		$bill ='';
				$bill.=$reseller_data['resellerCompanyName'].' <br/>';
              
             
					  
					        if($invoice_data['Billing_Addr1']!=''){
                              $bill.=$invoice_data['Billing_Addr1'].'<br>'. $invoice_data['Billing_Addr2'].'<br/>';
                              } 
							  $bill.=($invoice_data['Billing_City'])?$invoice_data['Billing_City'].', ':'';
                               $bill.=($invoice_data['Billing_State'])?$invoice_data['Billing_State'].' ':'';
                               $bill.=($invoice_data['Billing_PostalCode'])?$invoice_data['Billing_PostalCode'].' ':'';
							    $bill.='<br>';
                            $bill.=($invoice_data['Billing_Country'])?$invoice_data['Billing_Country']:'';
    					  $bill.='<br/><br/>';
                             
							  
			$lll_ship='';	
      
      			foreach($plans_overview as $plns){ $lll_ship.=$plns['plan_name'].': '.$plns['merchant'].'<br/>'; }
			 


                            $y = $pdf->getY();

                    		// write the first column
                    		$pdf->writeHTMLCell(80, 0, '', $y, "<b>Billing Address</b>:<br/>".$bill, 0, 0, 1, true, 'J', true);
                    		$pdf->SetTextColor(51, 51, 51);
                    		$pdf->writeHTMLCell(80, 0, '', '', '<b>Overview</b>:<br/>'.$lll_ship."<br/>",0, 1, 1, true, 'J', true);
                            $pdf->setCellPaddings(1, 1, 1, 1);

// set cell margins
	                    	$pdf->setCellMargins(0, 1, 0, 0);
                           $html= "\n\n\n".'<h3>Items:</h3><table border="1"  cellpadding="4" >
                        <tr>
        <th><b>Merchant</b></th>
        <th align="left"><b>Plan Name</b></th>
      
		    <th align="left"><b>Amount</b></th>
        
    </tr>';
     $html1='';$total_val =0;
						$totaltax =0 ; $total=0; $tax=0; 
						foreach($invoice_items as $key=>$item)
						{
							
						$total+=  $item['itemPrice']; 
				   
                    $html.='<tr>
                        <td >'.$item['merchantName'].'</td>
						<td >'.$item['itemName'].'</td>
                       
                       
						<td >'.number_format($item['itemPrice'], 2).'</td>
                    </tr>';
						} 
					
		
			$html.= '<tr><td colspan="2">Subtotal</td><td>$ '.number_format($total,2).'</td></tr>';	


$html.= '<tr><td colspan="2">Total Due</td><td>$'.number_format(($invoice_data['BalanceRemaining']),2).'</td></tr>';				
		
	  $email1 = ($company_data['adminEmail'])?$company_data['adminEmail']:'#';
                              
	
	$html.='</table>';
$html.='<strong>Important:</strong>
		<p>1. Please ignore if already paid.</p>
		<p>2. Please contact '.$company_data['primaryContact'].' or ';
		$html.='<a href="'.$email1.'">'.$email1.'</a> if you have any questions.</p>
		<br>
		';

$pdf->writeHTML($html, true, false, true, false, '');
	
 
   
    // ---------------------------------------------------------    
  
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    $pdf->Output($pdfFilePath, 'D');  
      
      
       }
     else
     {
         redirect('home/invoices');
     
     }
		
	}
   	   

/******************************************** END ****************************************/
		
/*************************** To Change Password **********************************************/

    /**
     * Change password
     */
    public function change_password()
    {
        if (!empty($this->input->post(null, true))) {
            if ($this->czsecurity->xssCleanPostInput('user-settings-currentpassword') === $this->czsecurity->xssCleanPostInput('user-settings-password')) {
                $this->session->set_flashdata('message', 'Error: New password cannot match old password.');

                redirect(base_url('Reseller_panel/index'));
            }

            $oldPassword    = $this->czsecurity->xssCleanPostInput('user-settings-currentpassword');
            $resellerID     = $this->czsecurity->xssCleanPostInput('user_id');
            $reseller_data  = $this->general_model->get_row_data('tbl_reseller',array('resellerID' => $resellerID));

            if (!empty($reseller_data)) {
                $validPassword = true;

                if ($reseller_data['resellerPasswordNew'] !== null) {
                    if (!password_verify($oldPassword, $reseller_data['resellerPasswordNew'])) {
                        $validPassword = false;
                    }
                } else {
                    if (md5($oldPassword) !== $reseller_data['resellerPassword']) {
                        $validPassword = false;
                    }
                }

                if ($validPassword) {
                    $newPassBcrypt = password_hash(
                        $this->czsecurity->xssCleanPostInput('user-settings-password'),
                        PASSWORD_BCRYPT
                    );

                    $this->general_model->update_row_data(
                        'tbl_reseller',
                        array(
                            'resellerID' => $resellerID
                        ),
                        array(
                            'resellerPassword'      => null,
                            'resellerPasswordNew'   => $newPassBcrypt
                        )
                    );

                    // Create log on password update
                    $log_session_data = [
                        'session' => $this->session->userdata,
                        'http_data' => $_SERVER
                    ];

                    $logData = [
                        'request_data' => json_encode($this->input->post(null, true), true),
                        'session_data' => json_encode($log_session_data, true),
                        'executed_sql' => $this->db->last_query(),
                        'log_date' => date('Y-m-d H:i:s'),
                        'action_interface' => 'Reseller - Change Password',
                        'header_data' => json_encode(apache_request_headers(), true),
                    ];

                    $this->general_model->insert_row('merchant_password_log', $logData);
					
					/* Destroy all login session */
					$sessionRequest     = new SessionRequest($this->db);
					$updateSession = $sessionRequest->destroySessionUser(SessionRequest::USER_TYPE_RESELLER,$resellerID);
					/*End destroy session*/

                    $this->session->set_flashdata('success-msg', 'Password has been changed successfully');
                } else {
                    $this->session->set_flashdata('error-msg', 'Please enter correct current password');
                }
            } else {
                $this->session->set_flashdata('error-msg', 'User not found');
            }
        } else {
            $this->session->set_flashdata('error-msg', 'Invalid request');
        }

        redirect(base_url('Reseller_panel/index'));
    }

    /**
	 * Function to check correct old password for reseller/agent
	 * 
	 * @return void
	 */
	public function chk_new_password() : void
	{
		if(!empty($this->input->post(null, true)))
	    {
	        $oldPassword = $this->czsecurity->xssCleanPostInput('user-settings-currentpassword');

			if($this->session->userdata('reseller_logged_in')){
				$resellerData 	= $this->session->userdata('reseller_logged_in');
				$passwordTable = 'tbl_reseller';
				$oldPasswordColumn = 'resellerPassword';
				$newPasswordColumn = 'resellerPasswordNew';

				$passwordWhere = array('resellerID'=>$resellerData['resellerID']);
			} else if($this->session->userdata('agent_logged_in')){
				$agentData 	= $this->session->userdata('agent_logged_in');
				$passwordTable = 'tbl_reseller_agent';
				$oldPasswordColumn = 'agentPassword';
				$newPasswordColumn = 'agentPasswordNew';

				$passwordWhere = array('ragentID'=>$agentData['ragentID']);
			}

			$userData = $this->general_model->get_row_data($passwordTable, $passwordWhere);
			if (!empty($userData)) {
				if ($userData["$newPasswordColumn"] !== null) {
					if (password_verify($oldPassword, $userData["$newPasswordColumn"])) {
						$data['status'] = 'success';
					} else {
						$data['status'] = 'error';
					}
				} else {
					if (md5($oldPassword) === $userData["$oldPasswordColumn"]) {
						$data['status'] = 'success';
					} else {
						$data['status'] = 'error';
					}
				}
			} else {
				$data['status'] = 'error';
			}
			
	       	echo json_encode($data); die;	        
	    }
	    echo "Invalid Request".' <a href="'.base_url('Reseller_panel/index').'">Go Back</a>';
	    die;
	    
	}

/******************************** End Here ***************************************************/
	public function addDefaultLevelThreeData($merchantID)
	{
		if($merchantID){

			$master_level_data = $this->reseller_panel_model->get_select_data('merchant_level_three_data', ['id'], ['merchant_id' => $merchantID, 'card_type' => 'master']);
			$visa_level_data = $this->reseller_panel_model->get_select_data('merchant_level_three_data', ['id'], ['merchant_id' => $merchantID, 'card_type' => 'visa']);

			if(empty($visa_level_data)){
				$insertData = [];
				$insertData['merchant_id'] = $merchantID;
				$insertData['created_date'] = date('Y-m-d H:i:s');
				$insertData['card_type'] = 'visa';
				$insertData['local_tax'] = 5;
				$insertData['national_tax'] = 0;
				$insertData['commodity_code'] = '4900';
				$insertData['discount_rate'] = 0;
				$insertData['freight_amount'] = 0;
				$insertData['duty_amount'] = 0;
				$insertData['destination_zip'] = '99212';
				$insertData['source_zip'] = '99212';
				$insertData['destination_country'] = 'US';
				$insertData['addtnl_tax_freight'] = 0;
				$insertData['addtnl_tax_rate'] = 0;
				$insertData['line_item_commodity_code'] = '4900';
				$insertData['unit_measure_code'] = 'EACH';
				$insertData['addtnl_tax_amount'] = 0;
				$insertData['line_item_addtnl_tax_rate'] = 0;
				$insertData['discount'] = 0;
				$this->general_model->insert_row('merchant_level_three_data', $insertData);
			}

			if(empty($master_level_data)){
				$insertData = [];
				$insertData['merchant_id'] = $merchantID;
				$insertData['created_date'] = date('Y-m-d H:i:s');
				$insertData['card_type'] = 'master';
				$insertData['local_tax'] = 5;
				$insertData['national_tax'] = 0;
				$insertData['freight_amount'] = 0;
				$insertData['duty_amount'] = 0;
				$insertData['destination_zip'] = '99212';
				$insertData['source_zip'] = '99212';
				$insertData['destination_country'] = 'US';
				$insertData['addtnl_tax_amount'] = 0;
				$insertData['addtnl_tax_indicator'] = 'N';
				$insertData['unit_measure_code'] = 'EACH';
				$insertData['addtnl_tax_amount'] = 0;
				$insertData['addtnl_tax_rate'] = 0;
				$insertData['addtnl_tax_type'] = 0;
				$insertData['line_item_addtnl_tax_rate'] = 0;
				$insertData['net_gross_indicator'] = 'N';
				$insertData['debit_credit_indicator'] = 'D';
				$insertData['addtnl_tax_type'] = 0;
				$insertData['discount'] = 0;
				$insertData['discount_rate'] = 0;

				$this->general_model->insert_row('merchant_level_three_data', $insertData);
			}
		}
	}
}