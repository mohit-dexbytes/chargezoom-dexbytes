<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Reseller_panel extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('reseller_login_model');
		$this->load->model('reseller_panel_model');
		$this->load->model('general_model');
		$this->load->model('card_model');
		$this->db1 = $this->load->database('otherdb', TRUE);
		delete_cache();

		if ($this->session->userdata('reseller_logged_in') != "") {
		} else if ($this->session->userdata('agent_logged_in') != "") {
		} else {
			redirect('login', 'refresh');
		}
	}

	public function index()
	{

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$rID 				= $data['login_info']['resellerID'];

            $where = ['resellerID' => $rID, 'resellerPasswordNew' => null];

            $newPasswordNotFound = $this->general_model->get_row_data('tbl_reseller', $where);
		}
		if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			
			$rID 				= $data['login_info']['resellerIndexID'];

            $where = ['ragentID' => $data['login_info']['ragentID'], 'agentPasswordNew' => null];

            $newPasswordNotFound = $this->general_model->get_row_data('tbl_reseller_agent', $where);
		}

        if ($newPasswordNotFound) {
            $dateDiff                = time() - strtotime(getenv('PASSWORD_EXP_DATE'));
            $data['passwordExpDays'] = abs(round($dateDiff / (60 * 60 * 24)));
        }

		$data['revenue'] = $this->reseller_panel_model->get_dashboard_count($rID);

		$date = date('M-Y');
		$data['revenueCreditCard'] = $this->reseller_panel_model->get_dashboard_CC_count($rID,$date);
		$data['revenueEcheck'] = $this->reseller_panel_model->get_dashboard_EC_count($rID,$date);

		if(!file_exists(FCPATH."uploads/reseller_dashboard_chart-$rID.json")){
			$this->dashboardJSON($rID);
		}
		$data['resellerID'] = $rID;

		$data['total_customer']   = $this->reseller_panel_model->get_reseller_customer_count($rID, $date);

		$data['plan_ratio'] =	$this->reseller_panel_model->calculate_plan_ratio($rID);
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/dashboard-new', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function reports()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['isEditMode'] = 1;
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$resID 				= $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			if($agentData['userMerchantEdit'] == 0){
				$data['isEditMode'] = 0;
			}
			if($agentData['allMerchantEdit'] == 1){
				$data['isEditMode'] = 1;
			}
			$resID 				= $data['login_info']['resellerIndexID'];
		}
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/comingsoon', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function serch_merchant()
	{


		$data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['isEditMode'] = 1;
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$resID 				= $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			if($agentData['userMerchantEdit'] == 0){
				$data['isEditMode'] = 0;
			}
			if($agentData['allMerchantEdit'] == 1){
				$data['isEditMode'] = 1;
			}
			$resID 				= $data['login_info']['resellerIndexID'];
		} else {
			redirect(base_url('Reseller_panel/index'), 'refresh');
		}


		$key = $this->czsecurity->xssCleanPostInput('search_data');


		$merchant = $this->reseller_panel_model->get_serch_merchant($key, $resID);


		$data['merchant_list'] = $merchant;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/merchant_list', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	

	}


	public function merchant_list()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$agentFilterHide = 0;

		$agentData = [];
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$resID 				= $data['login_info']['resellerID'];
			$merchant = $this->reseller_panel_model->get_data_admin_merchant($resID);
			
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
			
			$resID 				= $data['login_info']['resellerIndexID'];

			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
			
			$agID = '';

			if($agentData['userMerchantView'] == 0){
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Create merchant not permitted.</div>');
				redirect(base_url('Reseller_panel/index'));
			}else{
				if($agentData['userMerchantView'] == 1 && $agentData['allMerchantView'] == 0 && $agentData['userMerchantEdit'] == 0 ){
					$agentFilterHide = 1;
				}
			}



			$merchant = $this->reseller_panel_model->get_data_admin_merchant($resID, $agID);
		}


		$data['merchant_list'] = $merchant;
		$data['resID'] = $resID;

		$data['r_agent']        = $this->general_model->get_reselr_agent_data_data(array('resellerIndexID'=>$resID, 'isEnable'=>'1'));

		$reseller = $this->reseller_panel_model->get_select_data('tbl_reseller', array('plan_id'), array('resellerID'=>$resID));
        $paln =  $reseller['plan_id']; 
        $planname =  $this->reseller_panel_model->get_friendly_plan_name( $paln, $resID);

        $data['r_agent_plan'] = $planname;
		$data['agentData'] = $agentData;
		$data['agentFilterHide'] = $agentFilterHide;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/merchant_list', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}
	public function disable_merchant_list()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		$data['login_info'] = $this->session->userdata('reseller_logged_in');
		$data['isEditMode'] = 1;
		$data['isViewMode'] = 1;
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$resID 				= $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
			if($agentData['userMerchantEdit'] == 0){
				$data['isEditMode'] = 0;
			}
			if($agentData['allMerchantEdit'] == 1){
				$data['isEditMode'] = 1;
			}

			if($agentData['userMerchantView'] == 0){
				$data['isViewMode'] = 0;
			}
			if($agentData['allMerchantView'] == 1){
				$data['isViewMode'] = 1;
			}
			
			if($agentData['userMerchantDisabled'] == 0 && $agentData['allMerchantDisabled'] == 0){
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Create merchant not permitted.</div>');
				redirect(base_url('Reseller_panel/index'));
			}
		   	
			$resID 				= $data['login_info']['resellerIndexID'];
		}

		$merchant = $this->reseller_panel_model->get_disable_data_admin_merchant($resID);
		
		$data['merchant_list'] = $merchant;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/disable_merchant_list', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}




	public function merchant_login()
	{
		if ($this->session->userdata('reseller_logged_in')) {
			$da 	= $this->session->userdata('reseller_logged_in');
			$rID 	= $da['resellerID'];
		}
		if ($this->session->userdata('agent_logged_in')) {
			$da	 = $this->session->userdata('agent_logged_in');
			$rID = $da['resellerIndexID'];
		}

		$loginID = $this->uri->segment('3');
		
		$userdata =  $this->reseller_panel_model->get_select_data('tbl_merchant_data', array('merchID', 'resellerID', 'agentID', 'plan_id', 'firstLogin', 'firstName', 'lastName', 'allowGateway', 'merchantContact', 'companyName', 'merchantEmail', 'merchant_default_timezone'), array('merchID' => $loginID));

		$user = json_decode(json_encode($userdata), FALSE);
		$Merchant_url = $this->reseller_panel_model->get_row_data('Config_merchant_portal', array('resellerID' => $rID));

		if ($Merchant_url['merchantPortalURL'] != '') {
			$url = $Merchant_url['merchantPortalURL'];
			$url = preg_replace("/^http:/i", "https:", $url);

			$base_url = $url . "firstlogin/index/";

			if ($user && isset($user->merchantEmail)) {
				$usus_data = $this->reseller_login_model->login_as_mecrchant($loginID);


				if ($this->session->all_userdata()); {
					
					$this->session->unset_userdata('logged_in');
				}

				
				$sess_array = (array) $user;
				$rs  =  $this->general_model->get_select_data('tbl_reseller', array('ProfileURL', 'Chat'), array('resellerID' => $rID));


				if (!empty($rs)) {
					$sess_array['logo_img'] = $rs['ProfileURL'];
					$sess_array['script']   =  $rs['Chat'];
				}

				$this->general_model->track_user("Merchant", $sess_array['merchantEmail'], $sess_array['merchID']);


				if ($user->firstLogin == 0) {

					$condition  = array('merchantID' => $user->merchID);
					$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);
					$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);
					if (!empty($appset)) {
						$sess_array['active_app']	= $appset['appIntegration'];
					} else {
						$sess_array['active_app']	= '2';
					}
					$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);

					if ($gatway_num) {
						$sess_array['merchant_gateway']	= '1';
					} else {
						$sess_array['merchant_gateway']	= '0';
					}

					$filenum = $this->general_model->get_num_rows('tbl_company', $condition);
					$qm =  $this->db->query('select  *  from tbl_company where merchantID="' . $user->merchID . '" and  fileID!=""  ');
					if ($qm->num_rows() > 0)
						$sess_array['fileID'] = $qm->num_rows();
					else {
						$sess_array['fileID']  = '';
					}


					if (!empty($config)) {

						$sess_array['portalprefix']      = $config['portalprefix'];
						$sess_array['ProfileImage']      = $config['ProfileImage'];
					} else {
						$sess_array['portalprefix']      = '';
						$sess_array['ProfileImage']      = '';
					}


					if ($user->companyName != "" && $user->merchantAddress1 != "" && $user->merchantFullAddress != "" &&  $sess_array['merchant_gateway'] != "0" && $sess_array['fileID'] > 0) {

						$input_data   = array('firstLogin' => '1');
						$condition1   = array('merchID' => $user->merchID);
						$this->general_model->update_row_data('tbl_merchant_data', $condition1, $input_data);
					}


					$sess_array['active_app'] = '0';
					$this->session->set_userdata('logged_in', $sess_array);
					redirect($url . 'firstlogin/dashboard_first_login');
				} else {

					$condition  = array('merchantID' => $user->merchID);
					$gatway_num =  $this->general_model->get_num_rows('tbl_merchant_gateway', $condition);




					if ($gatway_num) {
						$sess_array['merchant_gateway']	= '1';
					} else {
						$sess_array['merchant_gateway']	= '0';
					}
					$config     = $this->general_model->get_row_data('tbl_config_setting', $condition);


					if (!empty($config)) {

						$sess_array['portalprefix']      = $config['portalprefix'];
						$sess_array['ProfileImage']      = $config['ProfileImage'];
					} else {
						$sess_array['portalprefix']      = '';
						$sess_array['ProfileImage']      = '';
					}
					$appset     = $this->general_model->get_row_data('app_integration_setting', $condition);


					if (!empty($appset)) {
						$sess_array['active_app']	= $appset['appIntegration'];
						$sess_array['gatewayMode']	= $appset['transactionMode'];
					} else {
						$sess_array['active_app']	= '2';
						$sess_array['gatewayMode']	= $appset['transactionMode'];
					}

					//check session

					if ($this->session->userdata('logged_in')) {

						if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 30)) {
							$this->session->unset_userdata('logged_in', $sess_array);
						}
						$_SESSION['LAST_ACTIVITY'] = time();
					} else {
						$this->session->set_userdata('logged_in', $sess_array);
					}

					$merchantID       = $this->session->userdata('logged_in')['merchID'];
					if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

						$fromEmail       = FROM_EMAIL; 

						$templatedatas =  $this->general_model->get_table_data('tbl_email_template_data', '');

						foreach ($templatedatas as $templatedata) {
							$insert_data = array(
								'templateName'  => $templatedata['templateName'],
								'templateType'    => $templatedata['templateType'],
								'merchantID'      => $merchantID,
								'fromEmail'      => DEFAULT_FROM_EMAIL,
								'message'		  => $templatedata['message'],
								'emailSubject'   => $templatedata['emailSubject'],
								'createdAt'      => date('Y-m-d H:i:s')
							);
							$this->general_model->insert_row('tbl_email_template', $insert_data);
						}
					}

					if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

						$this->general_model->insert_inv_number($merchantID);
					}





					if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
						$this->general_model->insert_payterm($merchantID);
					}

					if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '1') {

						$furl  =	$url . 'QBO_controllers/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '3') {
						$furl  =	$url . 'FreshBooks_controllers/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '4') {
						$furl =	$url . 'Integration/home/index';
					} else if ($this->session->userdata('logged_in')['firstLogin'] == '1' && $this->session->userdata('logged_in')['active_app'] == '5') {
						$furl =	$url . 'company/home/index';
					} else {
						$furl  =	$url . 'home/index';
					}

					redirect($furl, 'refresh');
				}




				$this->session->set_userdata('logged_in', $sess_array);
				redirect($base_url);
			}
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>Please create your portal url.</div>');
			redirect('Reseller_panel/merchant_list', 'refresh');
		}
	}

	public function recover_Merch_pwd()
	{
		return false;
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$resID 				= $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('agent_logged_in');

			$resID 				= $data['login_info']['resellerIndexID'];
		}


		$id = $this->uri->segment('3');
		
		$results = $this->general_model->get_select_data('tbl_merchant_data', array('firstName', 'lastName', 'companyName', 'merchantEmail', 'plan_id'), array('merchID' => $id));
		$res_data = $this->general_model->get_select_data('tbl_reseller', array('resellerfirstName', 'lastName', 'resellerCompanyName', 'resellerEmail'), array('resellerID' => $resID));

		$Merchant_url = $this->reseller_panel_model->get_row_data('Config_merchant_portal', array('resellerID' => $resID));


		if (!empty($results)) {



			if ($res_data['resellerCompanyName'] != "") {
				$rs_name = $res_data['resellerCompanyName'];
			} else {
				$rs_name = $res_data['resellerfirstName'] . '' . $res_data['lastName'];
			}

			if ($results['companyName'] != "") {
				$mch_name = $results['companyName'];
			} else {
				$mch_name = $results['firstName'] . '' . $results['lastName'];
			}

			$email = $results['merchantEmail'];
			$this->load->helper('string');
			$code = random_string('alnum', 10);
			$login_url = $Merchant_url['merchantPortalURL'];
			$update = $this->reseller_panel_model->temp_reset_password($code, $email);
			if ($update) {

				$temp_data = $this->general_model->get_row_data('tbl_eml_temp_reseller', array('resellerID' => $resID, 'templateType' => '2', 'status' => 1));
				$message = $temp_data['message'];
				$subject = $temp_data['emailSubject'];
				$cc = $temp_data['addCC'];
				$bcc = $temp_data['addBCC'];
				$replyTo = $temp_data['replyTo'];
				if ($temp_data['fromEmail'] != '') {
					$fromemail = $temp_data['fromEmail'];
				} else {

					$fromemail = $res_data['resellerEmail'];
				}

				$pl_data = $this->general_model->get_row_data('plan_friendlyname', array('plan_id' => $results['plan_id'], 'reseller_id' => $rsID));
				$friendly_plan_name = '';
				if($pl_data){
					$friendly_plan_name = $pl_data['plan_friendlyname'];
				}

				$login_url = '<a href=' . $login_url . '>' . $login_url . '<a/>';
				$subject = stripslashes(str_replace('{{reseller_name}}', $rs_name, $subject));
				$subject = stripslashes(str_replace('{{reseller_company}}', $rs_name, $subject));
				$message = stripslashes(str_replace('{{reseller_name}}', $rs_name, $message));
				$message = stripslashes(str_replace('{{merchant_name}}', $mch_name, $message));
				$message = stripslashes(str_replace('{{merchant_company}}', $mch_name, $message));
				$message = stripslashes(str_replace('{{login_url}}', $login_url, $message));
				$message = stripslashes(str_replace('{{merchant_email}}', $email, $message));
				$message = stripslashes(str_replace('{{merchant_password}}', $code, $message));
				$message = stripslashes(str_replace('{{reseller_company}}', $rs_name, $message));
				$message = stripslashes(str_replace('{{plan_name}}', $friendly_plan_name, $message));




				$this->load->library('email');
				$this->email->from($fromemail, $rs_name);
				$this->email->to($email);
				$this->email->subject($subject);
				$this->email->set_mailtype("html");

				$this->email->message($message);

				if ($this->email->send()) {
					$this->session->set_flashdata('success', 'Successfully sent new password to Merchant');
					
					
				} else {
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  Email was not sent, please contact your administrator.</div>' . $code);
				}
			}
			redirect('Reseller_panel/merchant_list', 'refresh');
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger"> Email not available!</div>');
			redirect('Reseller_panel/merchant_list', 'refresh');
		}
	}





	public function create_merchant()
	{

		$rsName = '';
		$showAgent = true;
		
		
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$user_id 				=  $data['login_info']['resellerID'];
			$rsID = $user_id;
			$rsName = $data['login_info']['resellerCompanyName'];
		}
		if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$user_id 				= $data['login_info']['resellerIndexID'];
			$rsName =               $data['login_info']['agentName'];
			$rsID = $user_id;

			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
			
			$agID = '';
			if ($this->uri->segment('3') || $this->czsecurity->xssCleanPostInput('merchID') != "") {
				if($agentData['userMerchantEdit'] == 0){
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Update merchant not permitted.</div>');
					redirect(base_url('Reseller_panel/merchant_list'));
				}
			}else{
				if($agentData['userMerchantAdd'] == 0){
					$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Create merchant not permitted.</div>');
					redirect(base_url('Reseller_panel/merchant_list'));
				}
			}
			
			
			if($agentData['userMerchantAdd'] == 1 && $agentData['allMerchantAdd'] == 0){
				$showAgent = false;
				
			}
			
		}
		
		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;


		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			$this->form_validation->set_rules('Plans', 'Plan', 'required|xss_clean');
			if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
				$this->form_validation->set_rules('merchantEmail', 'Email', 'required');
				$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}

			$planID = $this->czsecurity->xssCleanPostInput('Plans');
			$checkPayOption = true;
			if(!empty($planID)){
				$planData = $this->general_model->get_row_data('plans', array('plan_id' => $planID));
				if($planData && $planData['merchant_plan_type'] == 'Free'){
					$checkPayOption = false;
				}
			}

			if($checkPayOption){
				$this->form_validation->set_rules('payOption', 'Payment Option', 'required|xss_clean');
			}
			
			if ($this->czsecurity->xssCleanPostInput('gateway_opt') != "") {
				$this->form_validation->set_rules('gateway_opt', 'Gateway', 'required');
			}
			if ($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {

				if ($this->czsecurity->xssCleanPostInput('payOption') == 1 && $this->czsecurity->xssCleanPostInput('merchID') == "") {
					$this->form_validation->set_rules('cardNumber', 'Card Number', 'required|xss_clean|number|min:13|max:16');
					
				}else if($this->czsecurity->xssCleanPostInput('payOption') == 2 && $this->czsecurity->xssCleanPostInput('merchID') == ""){
					$this->form_validation->set_rules('accountNumber', 'Account Number', 'required|xss_clean|number|min:3|max:12');
					$this->form_validation->set_rules('routNumber', 'Routing Number', 'required|xss_clean|number|min:3|max:12');
					$this->form_validation->set_rules('accountName', 'Account Name', 'required|xss_clean');
				}
			}

			if ($this->form_validation->run() == true) {

				$id = null;
				$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$CompanyName = $this->czsecurity->xssCleanPostInput('companyName');
				$MerchantContact = $this->czsecurity->xssCleanPostInput('merchantContact');
				$merchantEmail = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$MerchantAltNum   = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');
				$MerchantAddress1 = $this->czsecurity->xssCleanPostInput('merchantAddress1');
				$MerchantAddress2 = $this->czsecurity->xssCleanPostInput('merchantAddress2');
				$MerchantCity = $this->czsecurity->xssCleanPostInput('merchantCity');
				$MerchantState = $this->czsecurity->xssCleanPostInput('merchantState');
				$MerchantZipCode = $this->czsecurity->xssCleanPostInput('merchantZipCode');
				$MerchantCountry = $this->czsecurity->xssCleanPostInput('merchantCountry');

				$is_free_trial = ($this->czsecurity->xssCleanPostInput('is_free_trial')  !== 0)?$this->czsecurity->xssCleanPostInput('is_free_trial'):0;
				$free_trial_day = ($this->czsecurity->xssCleanPostInput('free_trial_day')  !== 0)?$this->czsecurity->xssCleanPostInput('free_trial_day'):0;
				
				$portal_url = $this->czsecurity->xssCleanPostInput('portal_url');
				$tagline = $this->czsecurity->xssCleanPostInput('tagline');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');

				$merchantPassword = $this->czsecurity->xssCleanPostInput('merchantPassword');
				if(!empty($merchantPassword)){
					$input_data['merchantPasswordNew'] = password_hash(
                        $merchantPassword,
                        PASSWORD_BCRYPT
                    );
				}


				$input_data['companyName']	   = $CompanyName;
				$input_data['firstName']	   = $FirstName;
				$input_data['lastName']	       = $LastName;
				$input_data['merchantContact']     = $MerchantContact;
				$input_data['merchantEmail']     = $merchantEmail;
				$input_data['merchantAlternateContact']	   = $MerchantAltNum;
				$input_data['weburl']	       = 	$weburl;
				$input_data['merchantAddress1']     = $MerchantAddress1;
				$input_data['merchantAddress2']	       = $MerchantAddress2;
				$input_data['merchantCity']     = $MerchantCity;
				$input_data['merchantState']	   = $MerchantState;
				$input_data['merchantZipCode']	       = $MerchantZipCode;
				$input_data['merchantCountry']     = $MerchantCountry;

				$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');
				$input_data['weburl']     = $weburl;
				$input_data['merchantTagline']     = $tagline;

				$picture  = '';
				
				if (!empty($_FILES['picture']['name'])) {
					

					$config['image_library']  = 'uploads';
                	$config['upload_path']    = '../uploads/merchant_logo/';
                	$config['allowed_types']  = 'jpg|jpeg|png|gif';
	                $config['file_name']      = time() . $_FILES['picture']['name'];
	                $config['create_thumb']   = false;
	                $config['maintain_ratio'] = false;
	                $config['quality']        = '60%';
	                $config['widht']          = LOGOWIDTH;
	                $config['height']         = LOGOHEIGHT;
	                $config['new_image']      = '../uploads/merchant_logo/';

					//Load upload library and initialize configuration
					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					if ($this->upload->do_upload('picture')) {
						
						$uploadData = $this->upload->data();
						$picture = $uploadData['file_name'];
					} 
					
					
				}


				if($this->czsecurity->xssCleanPostInput('payOption') != null){
					
					$input_data['payOption'] = ($this->czsecurity->xssCleanPostInput('payOption') != null)?$this->czsecurity->xssCleanPostInput('payOption'):0;
				}
				
				/*Billing address details*/
				$billing_first_name = ($this->czsecurity->xssCleanPostInput('billingfirstName') != null)?$this->czsecurity->xssCleanPostInput('billingfirstName'):null;

	        	$billing_last_name = ($this->czsecurity->xssCleanPostInput('billinglastName')!= null)?$this->czsecurity->xssCleanPostInput('billinglastName'):null;

	        	$billing_phone_number = ($this->czsecurity->xssCleanPostInput('billingContact')!= null)?$this->czsecurity->xssCleanPostInput('billingContact'):null;

	        	$billing_email = ($this->czsecurity->xssCleanPostInput('billingEmailAddress')!= null)?$this->czsecurity->xssCleanPostInput('billingEmailAddress'):null;

	        	$billing_address = ($this->czsecurity->xssCleanPostInput('billingAddress1')!= null)?$this->czsecurity->xssCleanPostInput('billingAddress1'):null;

	        	$billing_state = ($this->czsecurity->xssCleanPostInput('billingState')!= null)?$this->czsecurity->xssCleanPostInput('billingState'):null;

	        	$billing_city = ($this->czsecurity->xssCleanPostInput('billingCity')!= null)?$this->czsecurity->xssCleanPostInput('billingCity'):null;

        		$billing_zipcode = ($this->czsecurity->xssCleanPostInput('billingPostalCode')!= null)?$this->czsecurity->xssCleanPostInput('billingPostalCode'):null;
				$input_merchant_data['billing_first_name']   = $billing_first_name;
				$input_merchant_data['billing_last_name']   = $billing_last_name;
				$input_merchant_data['billing_phone_number']   = $billing_phone_number;
				$input_merchant_data['billing_email']   = $billing_email;
				$input_merchant_data['Billing_Addr1']   = $billing_address;
				$input_merchant_data['Billing_Country']   = null;
				$input_merchant_data['Billing_State']   = $billing_state;
				$input_merchant_data['Billing_City']   = $billing_city;
				$input_merchant_data['Billing_Zipcode']   = $billing_zipcode;
				$input_merchant_data['resellerID'] = $user_id;
				
				$inputBillingUpdate = $input_merchant_data;     


				if ($this->czsecurity->xssCleanPostInput('Plans') != '') {
					$input_data['plan_id']   = $this->czsecurity->xssCleanPostInput('Plans');
				}
				
				if ($this->session->userdata('agent_logged_in')) {
					if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
						$agent	= $this->session->userdata('agent_logged_in');
						$input_data['agentID'] = $agent['ragentID'];
					}
				} else {
					$input_data['agentID']	   = $this->czsecurity->xssCleanPostInput('reseller_agent');
				}

				if ($this->session->userdata('reseller_logged_in')) {
					$da 	= $this->session->userdata('reseller_logged_in');

					$user_id 				= $da['resellerID'];
				} else if ($this->session->userdata('agent_logged_in')) {
					$da	= $this->session->userdata('agent_logged_in');

					$user_id 				= $da['resellerIndexID'];
				}



				if($this->czsecurity->xssCleanPostInput('payOption') == 1) {
					$card_no = $this->czsecurity->xssCleanPostInput('cardNumber');
					$card_type = $this->general_model->getcardType($card_no);
					$friendlyname = $card_type . ' - ' . substr($card_no, -4);
					$cvv = $this->czsecurity->xssCleanPostInput('cvv');

					$input_merchant_data['MerchantCard'] = $this->card_model->encrypt($card_no);
					$input_merchant_data['CardMonth'] = $this->czsecurity->xssCleanPostInput('expiry');
					$input_merchant_data['CardYear'] = $this->czsecurity->xssCleanPostInput('expiry_year');
					$input_merchant_data['CardCVV'] = $this->card_model->encrypt($cvv);
					$input_merchant_data['CardType']  = $card_type;
					$input_merchant_data['merchantFriendlyName']  = $friendlyname;
					$input_merchant_data['createdAt']  = date('Y-m-d H:i:s');

					$input_merchant_data['accountNumber']  	 = null;
					$input_merchant_data['routeNumber']  		 = null;
					$input_merchant_data['accountName']  		 = null;
					$input_merchant_data['secCodeEntryMethod']  = null;
					$input_merchant_data['accountType']  	     = null;
					$input_merchant_data['accountHolderType']   = null;

				}else if($this->czsecurity->xssCleanPostInput('payOption') == 2) {
					$acc_number   = $this->czsecurity->xssCleanPostInput('accountNumber');
					$route_number = $this->czsecurity->xssCleanPostInput('routNumber');
					$acc_name     = $this->czsecurity->xssCleanPostInput('accountName');
					$secCode      = 'WEB';
					$acct_type        = $this->czsecurity->xssCleanPostInput('acct_type');
					$acct_holder_type = $this->czsecurity->xssCleanPostInput('acct_holder_type');
					$card_type = 'Checking';

					$friendlyname = $card_type . ' - ' . substr($acc_number, -4);

					$input_merchant_data['accountNumber']  	 = $acc_number;
					$input_merchant_data['routeNumber']  		 = $route_number;
					$input_merchant_data['accountName']  		 = $acc_name;
					$input_merchant_data['secCodeEntryMethod']  = $secCode;
					$input_merchant_data['accountType']  	     = $acct_type;
					$input_merchant_data['accountHolderType']   = $acct_holder_type;

					$input_merchant_data['merchantFriendlyName']  = $friendlyname;
					
					$input_merchant_data['createdAt']  = date('Y-m-d H:i:s');

					$input_merchant_data['MerchantCard'] = null;
					$input_merchant_data['CardMonth'] = null;
					$input_merchant_data['CardYear'] = null;
					$input_merchant_data['CardCVV'] = null;
					$input_merchant_data['CardType']  = $card_type;

				}else{

				}
				

				$input_data['resellerID'] = $user_id;
				
				$input_data['updatedAt'] = date('Y-m-d H:i:s');
				
				if ($this->czsecurity->xssCleanPostInput('merchID') != "") {
					
					$id = $this->czsecurity->xssCleanPostInput('merchID');
					$input_merchant_data['merchantListID'] = $id;
					$inputBillingUpdate['merchantListID'] = $id;
					
					/*Check email exits condition on change*/
					$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $id ));
					$current_email = $get_data['merchantEmail'];
					if($current_email != $email){
						$results = $this->general_model->get_table_data('tbl_merchant_data', array('merchantEmail' => $email));

						if (count($results) > 0) {
							
							$this->session->set_flashdata('message', '<div class="alert alert-danger"> <strong>Error</strong> Email Already Exitsts </div>');
							redirect(base_url('Reseller_panel/merchant_list'));
						} 
					}
					if($this->czsecurity->xssCleanPostInput('merchantCardID') > 0){
						$input_data['cardID']  = $this->czsecurity->xssCleanPostInput('merchantCardID');
					}
					$chk_condition = array('merchID' => $id);

					$this->reseller_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data);
					$merchantData = $get_data;
					

	                $mcard_condition = array('merchantListID'=>$id);

					if($this->czsecurity->xssCleanPostInput('merchantCardID') > 0){
						if($this->czsecurity->xssCleanPostInput('is_card_edit') == 1 && ($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) ) {
							$cardid = $this->card_model->update_merchant_card_data($mcard_condition, $input_merchant_data);
						}else{
							$cardid = $this->card_model->update_merchant_card_data($mcard_condition, $inputBillingUpdate);
						}
						 
						if(ENVIRONMENT == 'production' && $rsID == HATCHBUCK_RESELLER_ID)
	                	{
							/* Start campaign in hatchbuck CRM*/  
		                    $this->load->library('hatchBuckAPI');
		                    
		                    $merchantData['merchant_type'] = 'Reseller Update';        
		                    
		                    $resource = $this->hatchbuckapi->createContact($merchantData);
		                    if($status['statusCode'] == 400){
		                        
		                        $resource = $this->hatchbuckapi->updateContact($input_data);
		                    }else{
				                $resource = $this->hatchbuckapi->updateContact($input_data);
		                    }
		                    /* End campaign in hatchbuck CRM*/
		                }
					}else if($this->czsecurity->xssCleanPostInput('merchantCardID') == 0){
						if($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {
							$cardid = $this->card_model->insert_merchant_card_data($input_merchant_data); 

							$input_data1['cardID']  = $cardid;

							$this->general_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data1);

							if($cardid > 0){
								if(ENVIRONMENT == 'production' && $rsID == HATCHBUCK_RESELLER_ID){
									/* Start campaign in hatchbuck CRM*/  
				                    $this->load->library('hatchBuckAPI');
				                    
				                    $merchantData['merchant_type'] = 'Reseller Updated';        
				                    
				                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                    if($status['statusCode'] == 400){
				                        $resource = $this->hatchbuckapi->createContact($merchantData);
				                        if($resource['contactID'] != '0'){
				                            $contact_id = $resource['contactID'];
				                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                        }
				                    }else{
						                $resource = $this->hatchbuckapi->updateContact($input_data);
				                    }
				                    /* End campaign in hatchbuck CRM*/ 
								}
							}
						}
						
					}

					$configSettingData = [];
					if ($this->czsecurity->xssCleanPostInput('portal_url') != "") {
						$portalURL = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));
						$configSettingData['customerPortalURL'] = "https://" . $portalURL . '.'. RSDOMAIN . "/";
						$configSettingData['customerPortal'] = 1;
					}

					if ($picture != ""){
						$configSettingData['ProfileImage'] = $picture;
					}

					if ($tagline != ""){
						$configSettingData['customerHelpText'] = $tagline;
					}

					if(!empty($configSettingData)){
						if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $id))) {
							$this->general_model->update_row_data('tbl_config_setting', array('merchantID' => $id), $configSettingData);
						} else {
							$configSettingData['merchantID'] = $id;
							$this->general_model->insert_row('tbl_config_setting', $configSettingData);
						}
					}

					$this->session->set_flashdata('success', 'Successfully Updated');
					
				} else {

					if ($this->session->userdata('agent_logged_in')) {
						$login_type = 'agentID';
					}

					$results = $this->reseller_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));
					if ($results) {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email Already Exitsts.</div>');
						redirect(base_url('Reseller_panel/merchant_list'));
					} else {
						$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
						$input_data['loginCode']	   = $code;
						$input_data['isEnable']	   = '1';

						$input_data['merchantEmail']   = $email;
						$input_data['merchantPasswordNew'] = password_hash($this->czsecurity->xssCleanPostInput('merchantPassword'), PASSWORD_BCRYPT);

						$input_data['password_update_date'] = null;


						$username = $email;
						$pass     = $this->czsecurity->xssCleanPostInput('merchantPassword');

						if($is_free_trial){
							$input_data['freeTrial'] = 1;
							$input_data['free_trial_day'] = $free_trial_day;
							$freeTrialDay = $free_trial_day - 1;

							$input_data['trial_expiry_date'] = date('Y-m-d', strtotime('+'.$freeTrialDay.' days'));
						}else{
							$input_data['freeTrial'] = 0;
							$input_data['free_trial_day'] = 0;
							$input_data['trial_expiry_date'] = null;
						}

						$input_data['date_added'] = date('Y-m-d H:i:s');
						$input_data['merchant_default_timezone'] = 'America/Los_Angeles';
						$m_id = $this->reseller_panel_model->insert_row('tbl_merchant_data', $input_data);

						$merchantID = $m_id;
						$id = $m_id;

						/*Save card and also update card id in merchant table*/
						$chk_condition1 = array('merchID' => $merchantID);
						if($this->czsecurity->xssCleanPostInput('payOption') == 1 || $this->czsecurity->xssCleanPostInput('payOption') == 2) {
							$input_merchant_data['merchantListID']  = $merchantID;
							$cardid = $this->card_model->insert_merchant_card_data($input_merchant_data); 

							if($cardid){
								$cardid = $cardid;
								if(ENVIRONMENT == 'production' && $rsID == HATCHBUCK_RESELLER_ID){
									/* Start campaign in hatchbuck CRM*/  
				                    $this->load->library('hatchBuckAPI');
				                    
				                    $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$chk_condition1);
				                    $merchantData['merchant_type'] = 'Reseller Create';        
				                    
				                    $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                    if($status['statusCode'] == 400){
				                        $resource = $this->hatchbuckapi->createContact($merchantData);
				                        if($resource['contactID'] != '0'){
				                            $contact_id = $resource['contactID'];
				                            $status = $this->hatchbuckapi->addContactCampaign($merchantData['merchantEmail'], HATCHBUCK_PAYMENT_INFO);
				                        }
				                    }

				                    /* End campaign in hatchbuck CRM*/ 
								}
								


							}else{
								$cardid = 0;
							}

							$this->createInvoiceForMerchant($this->input->post(null, true),$cardid,$merchantID);

							$input_data1['cardID']  = $cardid;
							
							$this->general_model->update_row_data('tbl_merchant_data', $chk_condition1, $input_data1);
			    		   
						}else{
							$cardid = 0;
							$this->createInvoiceForMerchant($this->input->post(null, true),$cardid,$merchantID);
						}

						
		                $merchantData = $this->general_model->get_row_data('tbl_merchant_data',$chk_condition1);
						if(ENVIRONMENT == 'production' && $merchantData['resellerID'] == HATCHBUCK_RESELLER_ID)
                		{
							/* Start campaign in hatchbuck CRM*/  
		                    $this->load->library('hatchBuckAPI');
		                    
		                    $merchantData['merchant_type'] = 'Reseller Create';        
		                    
		                    $resource = $this->hatchbuckapi->createContact($merchantData);
		                    

		                    /* End campaign in hatchbuck CRM*/ 
		                }

						if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = FROM_EMAIL; 

							$templatedatas =  $this->general_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->general_model->insert_row('tbl_email_template', $insert_data);
							}
						}

						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->general_model->insert_inv_number($merchantID);
						}





						if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->general_model->insert_payterm($merchantID);
						}

						$this->session->set_flashdata('success', 'Successfully Created');
						
						$pamount = 0.00;
						$pl_data = $this->general_model->get_row_data('plan_friendlyname', array('plan_id' => $input_data['plan_id'], 'reseller_id' => $rsID));
						$pl_data1 = $this->general_model->get_row_data('plans', array('plan_id' => $input_data['plan_id']));
						$pamount   = $pl_data1['subscriptionRetail'];
						$pl_name = isset($pl_data['friendlyname'])?$pl_data['friendlyname']:$pl_data1['plan_name'];
						$company = $CompanyName;
						$email   =   $input_data['merchantEmail'];
						$merchant_name = $FirstName . '' . $LastName;

						$coditionp  = array('resellerID' => $user_id);
						$pdata  =  $this->general_model->get_row_data('Config_merchant_portal', $coditionp);

						$purl       = isset($pdata['merchantPortalURL'])?$pdata['merchantPortalURL']:'';
						$login   = $purl . "login/";
						$login_url    = '<p><a href="' . $login . '">' . $login . '</a> </p>';

						$temp_data = $this->general_model->get_row_data('tbl_template_reseller_data', array('templateType' => 1));

						if ($chh_mail == '1') {
							$condition_mail         = array('templateType' => '1', 'resellerID' => $rsID);
							$toEmail = $email;

							$company = $CompanyName;
							$merchant_name = $FirstName . ' ' . $LastName;
							$pamount = 0.00;
							$pl_data = $this->general_model->get_row_data('plan_friendlyname', array('plan_id' => $input_data['plan_id'], 'reseller_id' => $rsID));
							$pl_data1 = $this->general_model->get_row_data('plans', array('plan_id' => $input_data['plan_id']));

							$pamount   = $pl_data1['subscriptionRetail'];
							$pl_name = isset($pl_data['friendlyname'])?$pl_data['friendlyname']:$pl_data1['plan_name'];

							$this->general_model->send_mail_data($condition_mail, $merchant_name, $company, $pl_name, $pamount, $login_url, $toEmail, $pass, $rsName, $m_id);
						}

						$signature = '';
						if ($this->czsecurity->xssCleanPostInput('gateway_opt') != '') {
							$gmID = '';
							$cr_status       =  1;
							$isSurcharge = $surchargePercentage = 0;
							$ach_status = 0;
							$extra1 = '';
							$gmID                  = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
								if ($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');

								if ($this->czsecurity->xssCleanPostInput('auth_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
								$signature  = PAYTRACE_INTEGRATOR_ID;
								if ($this->czsecurity->xssCleanPostInput('paytrace_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->czsecurity->xssCleanPostInput('paytrace_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
								$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
								if ($this->czsecurity->xssCleanPostInput('heart_cr_status')){
									$cr_status       =  1;
								}else{
									$cr_status       =  0;
								}

								if ($this->czsecurity->xssCleanPostInput('heart_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
								$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '9') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
								if ($this->czsecurity->xssCleanPostInput('cz_cr_status')){
									$cr_status       =  1;
								}
								else{
									$cr_status       =  0;
								}
								if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '10') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('iTransactUsername');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('iTransactAPIKIEY');
								if ($this->czsecurity->xssCleanPostInput('iTransact_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('iTransact_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('add_surcharge_box')){
									$isSurcharge = 1;
								}
								else{
									$isSurcharge = 0;
								}
								$surchargePercentage = $this->czsecurity->xssCleanPostInput('surchargePercentage');
							} 

							if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '11') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('fluidUser');
								$nmipassword     = '';
								if ($this->czsecurity->xssCleanPostInput('fluid_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('fluid_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							} 
							if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '12') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('TSYSUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('TSYSPassword');
								$gmID            = $this->czsecurity->xssCleanPostInput('TSYSMerchantID');
								if ($this->czsecurity->xssCleanPostInput('TSYS_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('TSYS_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							} 
							if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '13') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('basysUser');
								$nmipassword     = '';
								if ($this->czsecurity->xssCleanPostInput('basys_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('basys_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}  
							if ($this->czsecurity->xssCleanPostInput('gateway_opt')  == '14') {
								$nmiuser     = $this->czsecurity->xssCleanPostInput('cardpointeUser');
								$nmipassword = $this->czsecurity->xssCleanPostInput('cardpointePassword');
								$gmID        = $this->czsecurity->xssCleanPostInput('cardpointeMerchID');
								$signature = $this->czsecurity->xssCleanPostInput('cardpointeSiteName');
								if ($this->czsecurity->xssCleanPostInput('cardpointe_cr_status', true)) {
									$cr_status = 1;
								} else {
									$cr_status = 0;
								}
				
								if ($this->czsecurity->xssCleanPostInput('cardpointe_ach_status', true)) {
									$ach_status = 1;
								} else {
									$ach_status = 0;
								}
							} 
							
							if ($this->input->post('gateway_opt')  == '15') {
								$nmiuser         = $this->input->post('payarcUser');
								$nmipassword     = '';
								$cr_status       =  1;
								$ach_status       =  0;
							}

							if ($this->input->post('gateway_opt') == '17') {

								$nmiuser         = $this->czsecurity->xssCleanPostInput('maverickAccessToken');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('maverickTerminalId');
				
								if ($this->czsecurity->xssCleanPostInput('maverick_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;
				
								if ($this->czsecurity->xssCleanPostInput('maverick_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}
							if ($this->input->post('gateway_opt')  == '16') {
								$nmiuser         = $this->input->post('EPXCustNBR',true);
								$nmipassword     = $this->input->post('EPXMerchNBR',true);
								$signature     = $this->input->post('EPXDBANBR',true);
								$extra1   = $this->input->post('EPXterminal',true);
								if ($this->input->post('EPX_cr_status'))
									$cr_status       =  1;
								else
									$cr_status       =  0;

								if ($this->input->post('EPX_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							} 
							
							$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');

							$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
							$frname          = $this->czsecurity->xssCleanPostInput('frname');
							

							$insert_data    = array(
								'gatewayUsername' => $nmiuser,
								'gatewayPassword' => $nmipassword,
								'gatewayMerchantID' => $gmID,
								'gatewaySignature' => $signature,
								'extra_field_1' => $extra1,
								'gatewayType' => $gatetype,
								'merchantID' => $m_id,
								'gatewayFriendlyName' => $frname,
								'creditCard'       => $cr_status,
								'echeckStatus'      => $ach_status,
								'set_as_default'    => '1',
								'isSurcharge' => $isSurcharge,
								'surchargePercentage' => $surchargePercentage,
							);

							$this->general_model->insert_row('tbl_merchant_gateway', $insert_data);
						}

					}
				}

				if ($this->czsecurity->xssCleanPostInput('portal_url') != "" && $id != null) {
					$portal_url = strtolower($this->czsecurity->xssCleanPostInput('portal_url'));
					$url = "https://" . $portal_url . '.'. RSDOMAIN . "/";
	
	
					if ($this->general_model->get_num_rows('tbl_config_setting', array('merchantID' => $id))) {
						$update_data = array('customerPortalURL' => $url, 'customerHelpText' => $tagline, 'portalprefix' => $portal_url);
	
						if ($picture != "")
							$update_data['ProfileImage'] = $picture;
	
						$this->general_model->update_row_data('tbl_config_setting', array('merchantID' => $id), $update_data);
					} else {
						$insert_data = array(
							'customerPortalURL' => $url, 'customerHelpText' => $tagline,
							'merchantID' => $id, 'portalprefix' => $portal_url,
							'customerPortal' => 1
						);
						if ($picture != "")
							$insert_data['ProfileImage'] = $picture;
						$this->general_model->insert_row('tbl_config_setting', $insert_data);
					}
				}
				
				redirect(base_url('Reseller_panel/merchant_list'));

			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Validation errors exists.</div>');
			}
		}

		delete_cache('Reseller_panel/create_merchant/');

		if ($this->uri->segment('3')) {
			$merchID  			  = $this->uri->segment('3');
			$con                = array('merchID' => $merchID, 'resellerID' => $user_id);
			$data['merchant'] 	  = $this->reseller_panel_model->get_row_data('tbl_merchant_data', $con);
			$data['check'] 	  = $this->reseller_panel_model->get_num_rows('tbl_template_data', array('merchantID' => $user_id, 'mailType' => 2, 'customerID' => $merchID));
			$condition	= array('merchantID' => $merchID);
			$data['get_data'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);

			$config     = $this->general_model->get_row_data('tbl_config_setting', array('merchantID' => $merchID));
			
			if (!empty($config)) {

				$data['merchant']['portalprefix']      = $config['portalprefix'];
				$data['merchant']['ProfileImage']      = $config['ProfileImage'];
			} else {
				$data['merchant']['portalprefix']      = '';
				$data['merchant']['ProfileImage']      = '';
			}
			$data['merchant_card_data'] = $this->card_model->get_merch_card_data($data['merchant']['cardID']);
		}else{
			$data['merchant_card_data'] = [];
		}

		$data['showAgent'] = $showAgent;

		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$data['monthData'] 	  = $this->general_model->get_table_data('tbl_month', '');

		

		$reseller = $this->reseller_panel_model->get_select_data('tbl_reseller', array('plan_id'), array('resellerID' => $user_id));
		$plans  =  $reseller['plan_id'];
		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');
		$newplan =  $this->reseller_panel_model->get_friendly_plan_name($plans, $user_id);

		$data['re_agents']  =   $this->reseller_panel_model->get_agent_data(array('resellerIndexID' => $user_id, 'isEnable' => 1));
		$data['plan_list'] = $newplan;

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/add_merchant_new', $data);
		$this->load->view('reseller_pages/page_popup_boxes', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function create_merchant_03july()
	{
		return false;
		
		$rsName = '';
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$user_id 				=  $data['login_info']['resellerID'];
			$rsID = $user_id;
			$rsName = $data['login_info']['resellerCompanyName'];
		}
		if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$user_id 				= $data['login_info']['resellerIndexID'];
			$rsName =               $data['login_info']['agentName'];
			$rsID = $user_id;
		}

		if ($this->czsecurity->xssCleanPostInput('setMail'))
			$chh_mail = 1;
		else
			$chh_mail = 0;

		if (!empty($this->input->post(null, true))) {

			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
			$this->form_validation->set_rules('firstName', 'First Name', 'required|xss_clean');
			$this->form_validation->set_rules('lastName', 'Last Name', 'required|xss_clean');
			$this->form_validation->set_rules('companyName', 'Company Name', 'required|xss_clean');
			if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
				$this->form_validation->set_rules('merchantEmail', 'Email', 'required');

				$this->form_validation->set_rules('merchantPassword', 'Password', 'required');
			}
			if ($this->czsecurity->xssCleanPostInput('gateway_opt') != "") {

				$this->form_validation->set_rules('gateway_opt', 'Gateway', 'required');
			}
			if ($this->form_validation->run() == true) {
				$email   = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$FirstName = $this->czsecurity->xssCleanPostInput('firstName');
				$LastName = $this->czsecurity->xssCleanPostInput('lastName');
				$CompanyName = $this->czsecurity->xssCleanPostInput('companyName');
				$MerchantContact = $this->czsecurity->xssCleanPostInput('merchantContact');
				$merchantEmail = $this->czsecurity->xssCleanPostInput('merchantEmail');
				$MerchantAltNum   = $this->czsecurity->xssCleanPostInput('merchantAlternateContact');
				$weburl = $this->czsecurity->xssCleanPostInput('weburl');
				$MerchantAddress1 = $this->czsecurity->xssCleanPostInput('merchantAddress1');
				$MerchantAddress2 = $this->czsecurity->xssCleanPostInput('merchantAddress2');
				$MerchantCity = $this->czsecurity->xssCleanPostInput('merchantCity');
				$MerchantState = $this->czsecurity->xssCleanPostInput('merchantState');
				$MerchantZipCode = $this->czsecurity->xssCleanPostInput('merchantZipCode');
				$MerchantCountry = $this->czsecurity->xssCleanPostInput('merchantCountry');



				$input_data['companyName']	   = $CompanyName;
				$input_data['firstName']	   = $FirstName;
				$input_data['lastName']	       = $LastName;
				$input_data['merchantContact']     = $MerchantContact;
				$input_data['merchantEmail']     = $merchantEmail;
				$input_data['merchantAlternateContact']	   = $MerchantAltNum;
				$input_data['weburl']	       = 	$weburl;
				$input_data['merchantAddress1']     = $MerchantAddress1;
				$input_data['merchantAddress2']	       = $MerchantAddress2;
				$input_data['merchantCity']     = $MerchantCity;
				$input_data['merchantState']	   = $MerchantState;
				$input_data['merchantZipCode']	       = $MerchantZipCode;
				$input_data['merchantCountry']     = $MerchantCountry;

				$input_data['gatewayID']	   = $this->czsecurity->xssCleanPostInput('gateway_list');

				if ($this->czsecurity->xssCleanPostInput('Plans') != '') {
					$input_data['plan_id']   = $this->czsecurity->xssCleanPostInput('Plans');
				}
				
				if ($this->session->userdata('agent_logged_in')) {
					if ($this->czsecurity->xssCleanPostInput('merchID') == "") {
						$agent	= $this->session->userdata('agent_logged_in');
						$input_data['agentID'] = $agent['ragentID'];
					}
				} else {
					$input_data['agentID']	   = $this->czsecurity->xssCleanPostInput('reseller_agent');
				}

				if ($this->session->userdata('reseller_logged_in')) {
					$da 	= $this->session->userdata('reseller_logged_in');

					$user_id 				= $da['resellerID'];
				} else if ($this->session->userdata('agent_logged_in')) {
					$da	= $this->session->userdata('agent_logged_in');

					$user_id 				= $da['resellerIndexID'];
				}

				$input_data['resellerID'] = $user_id;
				
				$input_data['updatedAt'] = date('Y-m-d H:i:s');

				if ($this->czsecurity->xssCleanPostInput('merchID') != "") {

					$id = $this->czsecurity->xssCleanPostInput('merchID');
					$chk_condition = array('merchID' => $id);

					$this->reseller_panel_model->update_row_data('tbl_merchant_data', $chk_condition, $input_data);
					$this->session->set_flashdata('success', 'Successfully Updated');
					
					redirect(base_url('Reseller_panel/merchant_list'), 'refresh');
				} else {

					if ($this->session->userdata('agent_logged_in')) {
						$login_type = 'agentID';
					}

					$results = $this->reseller_panel_model->check_existing_user('tbl_merchant_data', array('merchantEmail' => $email));
					if ($results) {
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Email Already Exitsts.</div>');
					} else {
						$code = substr(str_shuffle("0123456789abcdefghijkABCDEFGHIJKLMNOPQRSTVWXYZ"), 0, 11);
						$input_data['loginCode']	   = $code;
						$input_data['isEnable']	   = '1';

						$input_data['merchantEmail']   = $email;
						$input_data['merchantPassword'] = md5($this->czsecurity->xssCleanPostInput('merchantPassword'));


						$username = $email;
						$pass     = $this->czsecurity->xssCleanPostInput('merchantPassword');

						$input_data['date_added'] = date('Y-m-d H:i:s');
						$m_id = $this->reseller_panel_model->insert_row('tbl_merchant_data', $input_data);
						$merchantID = $m_id;
						if ($this->general_model->get_num_rows('tbl_email_template', array('merchantID' => $merchantID)) == '0') {

							$fromEmail       = $this->session->userdata('logged_in')['merchantEmail'];

							$templatedatas =  $this->general_model->get_table_data('tbl_email_template_data', '');

							foreach ($templatedatas as $templatedata) {
								$insert_data = array(
									'templateName'  => $templatedata['templateName'],
									'templateType'    => $templatedata['templateType'],
									'merchantID'      => $merchantID,
									'fromEmail'      => DEFAULT_FROM_EMAIL,
									'message'		  => $templatedata['message'],
									'emailSubject'   => $templatedata['emailSubject'],
									'createdAt'      => date('Y-m-d H:i:s')
								);
								$this->general_model->insert_row('tbl_email_template', $insert_data);
							}
						}

						if ($this->general_model->get_num_rows('tbl_merchant_invoices', array('merchantID' => $merchantID)) == '0') {

							$this->general_model->insert_inv_number($merchantID);
						}

						if ($this->general_model->get_num_rows('tbl_payment_terms', array('merchantID' => $merchantID)) == '0') {
							$this->general_model->insert_payterm($merchantID);
						}

						$this->session->set_flashdata('success', 'Successfully Created');

						$pamount = 0.00;
						$pl_data = $this->general_model->get_row_data('plan_friendlyname', array('plan_id' => $input_data['plan_id'], 'reseller_id' => $rsID));
						$pl_data1 = $this->general_model->get_row_data('plans', array('plan_id' => $input_data['plan_id']));
						$pamount   = $pl_data1['subscriptionRetail'];
						$pl_name = $pl_data['friendlyname'];
						$company = $CompanyName;
						$email   =   $input_data['merchantEmail'];
						$merchant_name = $FirstName . '' . $LastName;

						$coditionp  = array('resellerID' => $user_id);
						$pdata  =  $this->general_model->get_row_data('Config_merchant_portal', $coditionp);

						$purl       = $pdata['merchantPortalURL'];
						$login   = $purl . "login/";
						$login_url    = '<p><a href="' . $login . '">' . $login . '</a> </p>';

						$temp_data = $this->general_model->get_row_data('tbl_template_reseller_data', array('templateType' => 1));

						if ($chh_mail == '1') {
							$condition_mail         = array('templateType' => '1', 'resellerID' => $rsID);
							$toEmail = $email;

							$company = $CompanyName;
							$merchant_name = $FirstName . '' . $LastName;
							$pamount = 0.00;
							$pl_data = $this->general_model->get_row_data('plan_friendlyname', array('plan_id' => $input_data['plan_id'], 'reseller_id' => $rsID));
							$pl_data1 = $this->general_model->get_row_data('plans', array('plan_id' => $input_data['plan_id']));

							$pamount   = $pl_data1['subscriptionRetail'];
							$pl_name = $pl_data['friendlyname'];

							$this->general_model->send_mail_data($condition_mail, $merchant_name, $company, $pl_name, $pamount, $login_url, $toEmail, $pass, $rsName, $m_id);
						}

						$signature = '';
						if ($this->czsecurity->xssCleanPostInput('gateway_opt') != '') {
							$gmID = '';
							$cr_status       =  0;

							$ach_status = 0;
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '1') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('nmiUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('nmiPassword');
								if ($this->czsecurity->xssCleanPostInput('nmi_cr_status'))
									$cr_status       =  1;

								if ($this->czsecurity->xssCleanPostInput('nmi_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '2') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('apiloginID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transactionKey');

								if ($this->czsecurity->xssCleanPostInput('auth_cr_status'))
									$cr_status       =  1;

								if ($this->czsecurity->xssCleanPostInput('auth_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '3') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paytraceUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paytracePassword');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '4') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('paypalUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('paypalPassword');
								$signature       = $this->czsecurity->xssCleanPostInput('paypalSignature');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '5') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('stripeUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('stripePassword');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '6') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('transtionKey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('transtionPin');
							}
							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '7') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('heartpublickey');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('heartsecretkey');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '8') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('cyberMerchantID');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('apiSerialNumber');
								$signature       = $this->czsecurity->xssCleanPostInput('secretKey');
							}

							if ($this->czsecurity->xssCleanPostInput('gateway_opt') == '9') {
								$nmiuser         = $this->czsecurity->xssCleanPostInput('czUser');
								$nmipassword     = $this->czsecurity->xssCleanPostInput('czPassword');
								if ($this->czsecurity->xssCleanPostInput('cz_cr_status')){
									$cr_status       =  1;
								}else{
									$cr_status       =  0;
								}

								if ($this->czsecurity->xssCleanPostInput('cz_ach_status'))
									$ach_status       = 1;
								else
									$ach_status       =  0;
							}

							$gatedata        =  $this->czsecurity->xssCleanPostInput('g_list');

							$gatetype        = $this->czsecurity->xssCleanPostInput('gateway_opt');
							if ($gatetype == '1')
								$frname          = 'NMI';
							if ($gatetype == '2')
								$frname          = 'Authorize.net';
							if ($gatetype == '3')
								$frname          = 'Paytrace';
							if ($gatetype == '4')
								$frname          = 'Paypal';
							if ($gatetype == '5')
								$frname          = 'Stripe';
							if ($gatetype == '6')
								$frname          = 'USAePay';
							if ($gatetype == '7')
								$frname          = 'Heartland';
							if ($gatetype == '8')
								$frname          = 'Cybersource';
							if ($gatetype == '9')
								$frname          = 'Chargezoom';
							$gmID                  = $this->czsecurity->xssCleanPostInput('gatewayMerchantID');

							$insert_data    = array(
								'gatewayUsername' => $nmiuser,
								'gatewayPassword' => $nmipassword,
								'gatewayMerchantID' => $gmID,
								'gatewaySignature' => $signature,
								'gatewayType' => $gatetype,
								'merchantID' => $m_id,
								'gatewayFriendlyName' => $frname,
								'creditCard'       => $cr_status,
								'echeckStatus'      => $ach_status,
								'set_as_default'    => '1'
							);



							$this->general_model->insert_row('tbl_merchant_gateway', $insert_data);
						}

						redirect(base_url('Reseller_panel/merchant_list'), 'refresh');
					}
				}
			} else {
				$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Validation errors exists.</div>');
			}
		}
		if ($this->uri->segment('3')) {
			$merchID  			  = $this->uri->segment('3');
			$con                = array('merchID' => $merchID, 'resellerID' => $user_id);
			$data['merchant'] 	  = $this->reseller_panel_model->get_row_data('tbl_merchant_data', $con);
			$data['check'] 	  = $this->reseller_panel_model->get_num_rows('tbl_template_data', array('merchantID' => $user_id, 'mailType' => 2, 'customerID' => $merchID));
			$condition	= array('merchantID' => $merchID);
			$data['get_data'] = $this->general_model->get_table_data('tbl_merchant_gateway', $condition);
		}


		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		$reseller = $this->reseller_panel_model->get_select_data('tbl_reseller', array('plan_id'), array('resellerID' => $user_id));
		$plans  =  $reseller['plan_id'];
		$data['all_gateway']  = $this->general_model->get_table_data('tbl_master_gateway', '');
		$newplan =  $this->reseller_panel_model->get_friendly_plan_name($plans, $user_id);

		$data['re_agents']  =   $this->reseller_panel_model->get_agent_data(array('resellerIndexID' => $user_id, 'isEnable' => 1));


		$data['plan_list'] = $newplan;



		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/add_merchant', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}

	public function delete_merchant()
	{

		if ($this->session->userdata('reseller_logged_in')) {
			$da 	= $this->session->userdata('reseller_logged_in');

			$user_id 				= $da['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$da	= $this->session->userdata('agent_logged_in');

			$user_id 				= $da['resellerIndexID'];
		}

		$id = $this->czsecurity->xssCleanPostInput('merchID');
		$res = $this->reseller_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id));
		if (!empty($res)) {

			$ins_data = array('merchID' => $id, 'isEnable' => '0',);
			$ins_data['updatedAt'] =  date("Y-m-d H:i:s");
			$this->reseller_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id), $ins_data);
			$this->session->set_flashdata('success', 'Successfully Disabled');
			

			redirect(base_url('Reseller_panel/merchant_list'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is not correct.</div>');
			redirect(base_url('Reseller_panel/merchant_list'));
		}
	}

	public function active_merchant()
	{
		if ($this->session->userdata('reseller_logged_in')) {
			$da 	= $this->session->userdata('reseller_logged_in');

			$user_id 				= $da['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$da	= $this->session->userdata('agent_logged_in');

			$user_id 				= $da['resellerIndexID'];
		}

		$id = $this->czsecurity->xssCleanPostInput('merchID');
		$res = $this->reseller_panel_model->get_row_data('tbl_merchant_data', array('merchID' => $id));
		if (!empty($res)) {

			$ins_data = array('merchID' => $id, 'isEnable' => '1', 'isDelete' => '0');
			$ins_data['updatedAt'] =  date("Y-m-d H:i:s");
			$this->reseller_panel_model->update_row_data('tbl_merchant_data', array('merchID' => $id), $ins_data);
			
 			$this->session->set_flashdata('success', 'Successfully Activated');

			redirect(base_url('Reseller_panel/merchant_list'));
		} else {
			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
			redirect(base_url('Reseller_panel/merchant_list'));
		}
	}


	public function merchant_details()
	{
		$this->load->model('card_model');
		$merchantID =   $this->uri->segment('3');

		$data['primary_nav']  = primary_nav();

		$data['template']    = template_variable();

		$data['isEditMode'] = 1;
		
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$user_id 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$user_id 				= $data['login_info']['resellerIndexID'];

			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
			
			if($agentData['userMerchantEdit'] == 0){
				$data['isEditMode'] = 0;
			}
			if($agentData['allMerchantEdit'] == 1){
				$data['isEditMode'] = 1;
			}
		}
		$type = ($this->czsecurity->xssCleanPostInput('status_filter') != null)?$this->czsecurity->xssCleanPostInput('status_filter'):'paid';
		
		$merchant = $this->reseller_panel_model->merchant_by_id($merchantID);

		$data['merchants'] = $merchant;
		
		if($merchant->cardID > 0 && $merchant->payOption > 0){
			$data['card_data_array']   = $this->card_model->get_merchant_card_data($merchantID);
		}else{
			$data['card_data_array']   = [];
		}
		
		
		$data['customers']     = $this->reseller_panel_model->get_customers($merchantID);
		$data['filter'] = $type;
		$data['transactions']     = $this->reseller_panel_model->get_transaction_data($merchantID,$type);

		$data['invoice']     = $this->reseller_panel_model->get_data_total_invoice($merchantID);
		$data['amount']     = $this->reseller_panel_model->getMerchantTotalAmountProcessed($merchantID);
		$Merchant_url = $this->reseller_panel_model->get_row_data('Config_merchant_portal', array('resellerID' => $user_id));	

		$data['resellerPortal'] =  $Merchant_url['merchantPortalURL'];
		$codition   = array('merchantID' => $merchantID);
		$data['setting'] = $this->general_model->get_row_data('tbl_config_setting', $codition);

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);


		$this->load->view('reseller_pages/merchant_details', $data);
		$this->load->view('template/page_footer', $data);
		$this->load->view('template/template_end', $data);
	}



	public function reseller_planfname()
	{

		$planID = $this->czsecurity->xssCleanPostInput('planID');
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$rID 				= $data['login_info']['resellerIndexID'];
		}
		$friendlyname = $this->czsecurity->xssCleanPostInput('friendlyname');
		$today = date('Y-m-d H:i:s');
		if ($this->czsecurity->xssCleanPostInput('allowMerchant'))
			$gateway = 1;
		else
			$gateway = 0;
		$data = array(

			"plan_id" => $planID,
			"reseller_id" => $rID,
			"friendlyname" => $friendlyname,
			"date" => $today,
			'gatewayAccess' => $gateway
		);

		$condition = array("plan_id" => $planID,  "reseller_id" => $rID);

		$check =  $this->reseller_panel_model->get_table_data('plan_friendlyname', $condition);

		if (empty($check)) {

			$table = $this->reseller_panel_model->insert_row('plan_friendlyname', $data);
		} else {
			$table = $this->reseller_panel_model->update_row_data('plan_friendlyname', $condition, $data);
			$m_condition = array('plan_id' => $planID, 'resellerID' => $rID);

			$m_data      = array('allowGateway' => $gateway, 'updatedAt' => date('Y-m-d H:i:s'));
			$this->general_model->update_row_data('tbl_merchant_data', $m_condition, $m_data);
		}

		$this->session->set_flashdata('success', 'Successfully Updated');

		redirect('Plan/reseller_plans');
	}




	public function get_plan_name()
	{
		$planID = $this->czsecurity->xssCleanPostInput('plan_id');
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');
			$rID 				= $data['login_info']['resellerIndexID'];
		}
		$plan = $this->reseller_panel_model->reseller_plan_by_id($planID, $rID);
	    echo json_encode($plan);
	    die;
	}

	public function delete_card_data()
	{


		$cardID =  $this->uri->segment('3');
		$sts =  $this->db1->query("Delete from merchant_card_data where merchantCardID = $cardID ");
		if ($sts) {
			$this->session->set_flashdata('success', 'Successfully Deleted');
			
		} else {

			$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong> Something Is Wrong.</div>');
		}

		redirect('Reseller_panel/merchant_list', 'refresh');
	}

	public function total_plan_amount()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();

		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$rID 				= $data['login_info']['resellerIndexID'];
		}

		$plan_total = $this->reseller_panel_model->get_totalplan($rID);
		echo json_encode($plan_total);
		die;
	}

	public function total_service_amount()
	{
		$data['primary_nav']  = primary_nav();
		$data['template']   = template_variable();
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$rID 				=  $data['login_info']['resellerID'];
		}
		if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$rID 				= $data['login_info']['resellerIndexID'];
		}

		$s_total = $this->reseller_panel_model->get_totalservice($rID);

		echo json_encode($s_total);
		die;
	}

	public function reseller_merchant_chart()
	{
		$agentId = false;
		if ($this->session->userdata('reseller_logged_in')) {

			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$rID 				= $data['login_info']['resellerIndexID'];
			$agentId = $data['login_info']['ragentID'];

			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $agentId]);

			if($agentData['dashboardPermission'] == 1)
       			$agentId = false;
		}

		$merchantCount = array('resellerID' => $rID, 'isDelete' => 0, 'isEnable' => 1);
		if($agentId){
			$merchantCount['agentID'] = $agentId;
		}
		
		$get_result = $this->reseller_panel_model->chart_get_merchant($rID);

		$get_result['total_merchant'] = $this->general_model->get_total_merchants($merchantCount);
		
		
		echo json_encode($get_result);
		die;
	}

	public function reseller_process_volume()
	{
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');

			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');

			$rID 				= $data['login_info']['resellerIndexID'];
		}
		$get_result = $this->reseller_panel_model->chart_processing_volume($rID);
		if ($get_result) {
			
			$result_set = array();
			$result_value = array();

			foreach ($get_result as $count_merch) {

				array_push($result_value, $count_merch->Month);
				array_push($result_value, $count_merch->volume);
			}


			$opt_array = array_chunk($result_value, 2);

			echo json_encode($opt_array);
		}
	}

	/********************************************** Suspend Merchant Start Here **********************************************************/

	public function update_merchant_status()
	{

		$resellerID = $this->czsecurity->xssCleanPostInput('resellerID');
		$merchantID = $this->czsecurity->xssCleanPostInput('merchantID');
		$acct = $this->czsecurity->xssCleanPostInput('acct');

		$login_type = 'resellerID';
		if ($this->session->userdata('agent_logged_in')) {
			$login_type = 'agentID';
		}

		$res = $this->reseller_panel_model->get_row_data('tbl_merchant_data', array("$login_type" => $resellerID, 'merchID' => $merchantID));

		$ins_data = array('isSuspend' => ($acct == 0 ? '1' : '0'));

		if ($acct == 0) {
			
			$this->session->set_flashdata('success', 'Successfully Suspended');
		} else {
			$ins_data['login_failed_attemp'] = 0;
			$ins_data['isLockedTemp'] = 0;
			$ins_data['accountLockedDate'] = null;
			$ins_data['lockedMessage'] = '';
			$this->session->set_flashdata('success', 'Merchant is now active');
		}
		
		if (!empty($resellerID)) {
			if (!empty($res)) {


				$isUpdated = $this->reseller_panel_model->update_row_data('tbl_merchant_data', array("$login_type" => $resellerID, 'merchID' => $merchantID), $ins_data);

				if (!$isUpdated) {
					$message =  '<div class="alert alert-danger"><strong>Error:</strong> Something Is Wrong.</div>';
				}
			} else {
				$message =  '<div class="alert alert-danger"><strong>Error:</strong> Something went Wrong.</div>';
			}
		} else {
			$message = '<div class="alert alert-danger"><strong>Error:</strong> Invalid Request.</div>';
		}
		$this->session->set_flashdata('message', $message);

		redirect(base_url('Reseller_panel/merchant_list'));

	} // end of function

	/********************************************** Suspend Merchant End Here ***********************************************************/


	public function get_gatewayedit_id()
	{

		$id = $this->czsecurity->xssCleanPostInput('gatewayid');
		$val = array(
			'gatewayID' => $id,
		);

		$data = $this->general_model->get_row_data('tbl_merchant_gateway', $val);
		$data['enable_level_three_data'] = ($data['enable_level_three_data']) ? true : false;
		$data['gateway'] = getGatewayNames($data['gatewayType']);
		echo json_encode($data);
		die;
	}

	public function check_url()
	{
		$res = array();

		$portal_url  = $this->czsecurity->xssCleanPostInput('portal_url');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');

		if (!preg_match('/^[a-zA-Z0-9]+[._]?[a-zA-Z0-9]+$/', $portal_url)) {
			if (strpos($portal_url, ' ') > 0) {

				$res = array('portal_url' => 'Space is not allowed in url', 'status' => 'false');
				echo json_encode($res);
				die;
			}

			$res = array('portal_url' => 'Only letters and numbers allowed', 'status' => 'false');
			echo json_encode($res);
			die;
		} else {
			$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url));
			if ($num == 0) {
				$res = array('status' => 'success');
			} else if ($num == 1) {
				$num = $this->general_model->get_num_rows('tbl_config_setting', array('portalprefix' => $portal_url, 'merchantID' => $merchantID));
				if ($num == 1)
					$res = array('status' => 'success');
				else
					$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			} else {
				$res = array('portal_url' => 'This URL is currently being used', 'status' => 'false');
			}

			echo json_encode($res);
			die;
		}
	}
	/************Inc=voice Merchant on create*****************/
	public function createInvoiceForMerchant($postData,$cardID,$merchantID)
	{
		if ($postData['Plans']) {
			$plan_id = $postData['Plans'];
			$m_name =  $postData['companyName'];
			$is_free_trial = isset($postData['is_free_trial'])?$postData['is_free_trial']:0;
			$free_trial_day = isset($postData['free_trial_day'])?$postData['free_trial_day']:0;
			$updataMain = $this->general_model->get_row_data('plans',array('plan_id'=>$plan_id));

			$inv_data = $this->general_model->get_row_data('tbl_merchant_number', array('numberType' => 'Merchant'));

			$inumber   = $inv_data['liveNumber'] + 1;
            $refNumber = $inv_data['preFix'] . ' - ' . ($inv_data['postFix'] + $inumber);
            $inv_id   = 'ADM' .mt_rand(100000, 999999);
            
			if($is_free_trial == 1){
				$free_trial_day = $free_trial_day;
				$freeTrialDay = $free_trial_day - 1;
				$trial_expiry_date = date('Y-m-d', strtotime('+'.$freeTrialDay.' days'));

				$due_date = $trial_expiry_date;
				$status   = 'paid';
				$balance = 0;
				$total1 = 0;
				$subscriptionItemName = 'Free trial for '.$free_trial_day.' days';
				$subscriptionAmount = 0;
				$rental_amount = 0;
				$plan_expiry = $trial_expiry_date;
			}else{
				$free_trial_day = 0;
				$trial_expiry_date = null;

				/* Plan per day calculation */
                $planAmount = $updataMain['subscriptionRetail'];
                $numberOfDaysInMonth = date('t');
                $amountPerDay = round(($planAmount / $numberOfDaysInMonth),2);
                /*Days compare in month by plan*/
                $billing_up_to_date = date("Y-m-t");
                $datePurchase = date("Y-m-d");
                $currentdate = date("d");
                $nextMonth = date('Y-m-d',strtotime('first day of +1 month'));
                
                $datediff = strtotime($billing_up_to_date) - strtotime($datePurchase);
               	$different_days = round($datediff / (60 * 60 * 24)) + 1;

                if($currentdate >= 25){
                	
					$nextMonth = date('Y-m-d',strtotime('last day of +1 month'));

					$nextMonthAmount = $planAmount;

                	$plan_expiry = $nextMonth;
                }else{
                	$nextMonth = date('Y-m-d',strtotime('first day of +1 month'));
                	$nextMonthAmount = 0;

                	$plan_expiry = date('Y-m-d', strtotime('-1 day', strtotime($nextMonth)));
                }

                $totalInvoiceAmount = $different_days * $amountPerDay;
                $rental_amount = $totalInvoiceAmount + $nextMonthAmount;

				$due_date = date('Y-m-d');
				$status   = 'pending';
				$balance = $rental_amount + $updataMain['serviceFee'];
				$total1 = $updataMain['serviceFee'];

				$subscriptionItemName = 'Subscription Fee'; 
				$subscriptionAmount = $rental_amount;
			}
			if($balance == '0' || $balance == '0.00'){
                $status   = 'paid';
            }else{
                $status   = 'pending';
            }
			$data     = array(
                'merchantName'           => $m_name,
                'merchantID'             => $merchantID,
                'invoice'                => $inv_id,
                'invoiceNumber'          => $refNumber,   
                'DueDate'                => $due_date,
                'BalanceRemaining'       => ($balance),
                'status'                 => $status,
                'gatewayID'              => 0,
                'cardID'                 => $cardID,
                'merchantSubscriptionID' => 0,
                'Type'                   => 'Plan',
                'serviceCharge'          => $total1,
                'invoiceStatus'          => 1,
                'createdAt'              => date('Y-m-d H:i:s'),
                'updatedAt'              => date('Y-m-d H:i:s')
            );
            if ($this->general_model->insert_row('tbl_merchant_billing_invoice', $data)) {

            	$updata1 = array('plan_expired' => $plan_expiry);
                $this->general_model->update_row_data('tbl_merchant_data', array('merchID' => $merchantID), $updata1);

                if($cardID > 0 && $balance > 0){
					$statusUpdate = $this->getChargeOfInvoice($postData, $cardID, $balance,$merchantID,$m_name,$data);
				}

                $updata = array('liveNumber' => $inumber, 'updatedAt' => date('Y-m-d H:i:s'));
                $this->general_model->update_row_data('tbl_merchant_number', array('numberType' => 'Merchant'), $updata);

                $pl_data['itemName']          = $subscriptionItemName;
                $pl_data['itemPrice']         = $subscriptionAmount;
                $pl_data['merchantInvoiceID'] = $inv_id;
                $pl_data['planID'] = $plan_id;
                $pl_data['merchantID'] = $merchantID;
                $pl_data['merchantName'] = $m_name;

                $pl_data['subscriptionID']    = $inv_id;
                $pl_data['createdAt']         = date('Y-m-d H:i:s');
                $this->general_model->insert_row('tbl_merchant_invoice_item', $pl_data);

                $item_data['itemName']          = 'Service Charge';
                $item_data['itemPrice']         = $total1;
                $item_data['merchantInvoiceID'] = $inv_id;
                $item_data['subscriptionID']    = $inv_id;
                $item_data['planID'] = $plan_id;
                $item_data['merchantID'] = $merchantID;
                $item_data['merchantName'] = $m_name ;
                $item_data['createdAt']         = date('Y-m-d H:i:s');
                
            }

		}

		return true;
	}
	public function check_new_email()
	{
		$res = array();

		$merchantEmail  = $this->czsecurity->xssCleanPostInput('merchantEmail');
		$merchantID  = $this->czsecurity->xssCleanPostInput('merchantID');
		if($merchantID > 0){
			$get_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchID' => $merchantID ));
			$current_email = $get_data['merchantEmail'];
			if($current_email == $merchantEmail){
				$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
				echo json_encode($res);
				die;
			}else{
				$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
			}
			
		}else{
			$app_data  = $this->general_model->get_row_data('tbl_merchant_data', array('merchantEmail' => $merchantEmail ));
		}
		if(count($app_data) > 0){
			$res = array('message' => 'Email Already Exitsts', 'status' => 'error');
			echo json_encode($res);
			die;
		}else{
			$res = array('message' => 'Email Not Exitsts', 'status' => 'success');
			echo json_encode($res);
			die;
		}
	}
	
	public function serverProcessingMerchant()
    {
        
        if(isset($_GET['draw'])){
        	$dataPost = $_GET;
        	$previledge = '';
        	if ($this->session->userdata('reseller_logged_in')) {
				$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
				$login_info = $data['login_info'];
				$resID 				= $data['login_info']['resellerID'];
				$merchantData = $this->reseller_panel_model->get_data_admin_merchant_by_condition($resID,'',$dataPost);
				$merchantDataCount = $this->reseller_panel_model->get_dataCount_admin_merchant_by_condition($resID,'',$dataPost);
			} else if ($this->session->userdata('agent_logged_in')) {

				$data['login_info'] 	= $this->session->userdata('agent_logged_in');
				$login_info = $data['login_info'];
				$resID 				= $data['login_info']['resellerIndexID'];

				$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

				$agID = '';
				if($agentData['allMerchantView'] == 1){
					$agID = '';
				}else{
					if($agentData['userMerchantView'] == 1){
						$agID = $data['login_info']['ragentID'];
					}
				}
				

				$merchantData = $this->reseller_panel_model->get_data_admin_merchant_by_condition($resID, $agID,$dataPost);
				
				$merchantDataCount = $this->reseller_panel_model->get_dataCount_admin_merchant_by_condition($resID, $agID,$dataPost);
			}

            $data = [];

            $manageDisabledOption = $dataPost['manageDisabledOption'];
            
            $data3 = [];
            $text = '';
            if($merchantData['count'] > 0){
            	$inc = 0;
            	$prev='';
            	$url = base_url().'Reseller_panel/create_merchant/'; 
            	
                foreach ($merchantData['data'] as  $merchant) {
                	$volume = 0;
                	$setValue = '0';
                	$setValueOne = '1';
                	$data2 = [];
                  	$st2  = '';
                  	$st1  = '';
                  	$text = '';
				  	$volume = $merchant['total_amount']-$merchant['refund_amount'];
				  	if($login_info['login_type']=='AGENT'){ 
					 	$resellerId = $login_info['ragentID'];  
					 	$agentData = $this->reseller_panel_model->get_row_data('tbl_reseller_agent', array('ragentID' => $resellerId));
				             
				            if($merchant['agentID']==$login_info['ragentID'])
				            { 
				               $login_info['merchant_account']='SELF'; }else{   $login_info['merchant_account']='OTHER'; };    
				            
					       
					} else {
						 $resellerId = $login_info['resellerID'];  

					}
					

					$editPermission = 1;
					$disabledPermission = 1;
					if($login_info['login_type']=='AGENT'){
						if($agentData['userMerchantEdit'] == 1){
							if($agentData['allMerchantEdit'] == 0){
								if($merchant['agentID']==$login_info['ragentID']){
									$editPermission = 1;
								}else{
									$editPermission = 0;
								}
							}else{
								$editPermission = 1;
							}
							
						}else{
							$editPermission = 0;
						}
					}
					if($login_info['login_type']=='AGENT'){
						if($agentData['userMerchantDisabled'] == 1){
							if($agentData['allMerchantDisabled'] == 0){
								if($merchant['agentID']==$login_info['ragentID']){
									$disabledPermission = 1;
								}else{
									$disabledPermission = 0;
								}
							}else{
								$disabledPermission = 1;
							}
							
						}else{
							$disabledPermission = 0;
						}
					}
					
					if($editPermission == 1)
					 {   
					 	$st2='';  
					 	$v_ur = base_url().'Reseller_panel/merchant_details/'.$merchant['merchID'];   
					 }
					 else
					 {   
					 	$st2='class="disabled"';  $v_ur='javascript:void(0);'; 
					 } 
					 $classSuspend = '';
					 if($merchant['isSuspend']=='1'){
					 	$classSuspend = 'redText';
					 }
					 if(isset($merchant['companyName'])){
					 	$data2[] = '<a class="'.$classSuspend.'" href="'.$v_ur.'" '.$st2.' >'.$merchant['companyName'].'</a>';
					 }else{
					 	$data2[] = '<a class="'.$classSuspend.'" href="'.$v_ur.'" '.$st2.' >-----</a>';
					 	
					 }
					

    				$data2[] = '$'.number_format($volume,2).'';
    				$data2[] = ''.$merchant['customer_count'].'';

    				if(isset($merchant['friendlyname']) && $merchant['friendlyname'] != ""){
    				 	$data2[] = ''.$merchant['friendlyname'].'';
    				}else{
    					$data2[] = isset($merchant['plan_name'])?$merchant['plan_name']:'-----';
    				}

					$data2[] = ($merchant['date_added']) ? date('M d, Y', strtotime($merchant['date_added'])) : '-----';
					
    				if($login_info['login_type']=='RESELLER'  || (  
                        $login_info['login_type']=='AGENT' && ( $editPermission ||   $login_info['merchant_account']=='SELF' ) ))
    				{
                        $st1='';   
                        $ed_url =$url.$merchant['merchID'];  
                    }
                    else
                    {
                        $st1='disabled="disabled"';    
                        $ed_url='javscript:void(0)';
                    }   
					if($login_info['login_type']=='RESELLER' || ( $login_info['login_type']=='AGENT' && ($prev=='FullAdmin' ||   $login_info['merchant_account']=='SELF'   ) ) )
					{
						$st='';   
						$mh='#del_merchant';   
						$mlogin = base_url('Reseller_panel/merchant_login/'.$merchant['merchID']);  
						     
					}
					else
					{   
						$st='class="disabled"';   
						$mlogin='javascript:void(0);';   
						$mh=''; 
					} 
					$classDis = '';
					$classDisNot = '';
					$textnew = '';
					if($editPermission == 1){
						if($merchant['isSuspend']=='0'){
							$textnew = '<a href="#suspend_merchant" onclick="set_merchant_id(\'' . $resellerId . '\',\''.$setValue.'\',\'' . $merchant['companyName'] . '\',\'' . $merchant['merchID'] . '\');"  data-toggle="modal" >Suspend Merchant</a>';
						}else if($merchant['isSuspend']=='1'){
							$textnew = '<a href="#suspend_merchant" onclick="set_merchant_id(\'' . $resellerId . '\',\''.$setValueOne.'\',\''.$merchant['companyName'].'\',\''.$merchant['merchID'].'\');"   data-toggle="modal" >Resume Merchant</a>';
							$classDis = 'disabled';
							$classDisNot = 'disabled_not';
						}else{
							$textnew = '';

						}
					}
					

					if($disabledPermission == 1){
						$manageDisabledOptionTxt = '<li class="'.$classDisNot.'" ><a href="#del_merchant" onclick="set_del_merchant( \'' . $merchant['merchID'] . '\');"   data-backdrop="static" data-keyboard="false" data-toggle="modal"  data-original-title="" >Disable Merchant </a></li>';
					}else{
						$manageDisabledOptionTxt = '';
					}


    				$data2[] = '<div class="btn-group dropbtn"><a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" aria-expanded="true">Select <span class="caret"></span></a>
                                 <ul class="dropdown-menu text-left">
							   <li>  	</li>
							   <li class="'.$classDis.'" ><a href="'.$mlogin.'"  target="_blank"  data-original-title="Login as Merchant"     title="">Login as Merchant</a> </li>
							   <li>'.$textnew.'</li>
					        	'.$manageDisabledOptionTxt.'
                             </ul>
                       		
				    	</div>';
    				  
                    $data3[$inc] = $data2;
                    $inc = $inc + 1;
                }
            }
            
            $data['draw'] = $_GET['draw'];
            $data['recordsTotal'] = $merchantDataCount;
            $data['recordsFiltered'] = $merchantDataCount;
            $data['data'] = $data3;


            echo json_encode($data);
          

        }
    }
    public function reseller_merchant_dashboard_chart()
	{
		$this->load->helper('file');
		$agentId = false;
		if ($this->session->userdata('reseller_logged_in')) {
			$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
			$rID 				=  $data['login_info']['resellerID'];
		} else if ($this->session->userdata('agent_logged_in')) {
			$data['login_info']	= $this->session->userdata('agent_logged_in');
			$rID 				= $data['login_info']['resellerIndexID'];
			$agentId = $data['login_info']['ragentID'];

			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $agentId]);
			if($agentData['dashboardPermission'] == 1)
       			$agentId = false;
		}

		$response = $this->dashboardJSON($rID, $agentId);

		echo $response;
		die;
	}

	public function getChargeOfInvoice($postData, $cardID, $balance,$merchantID,$merchantName,$in_data)
	{

    	$card_data = $this->card_model->get_merchant_single_card_data($cardID);

		//start NMI

		$merchantName = $merchantName;
        $payamount = $balance;
        $txnID = '';
        if(isset($card_data['CardID'])){
        	$friendlyName  	= $card_data['merchantFriendlyName'] ;
			$Billing_Addr1	 = $card_data['Billing_Addr1'];	
			$Billing_Addr2	 = $card_data['Billing_Addr2'];
			$Billing_City	 = $card_data['Billing_City'];
			$Billing_State	 = $card_data['Billing_State'];
			$Billing_Country = $card_data['Billing_Country'];
			$Billing_Contact	 = $card_data['Billing_Contact'];
			$Billing_Zipcode	 = $card_data['Billing_Zipcode'];
			$billing_phone_number = $card_data['billing_phone_number'];
			$billing_email = $card_data['billing_email'];
			$billing_first_name = $card_data['billing_first_name'];
			$billing_last_name = $card_data['billing_last_name'];
			$CardType	 = $card_data['CardType'];

			
			$sch_method = $postData['payOption'];
			
			$gatewayResponseCode = 400;
			$gatewayType = 'NMI';
		
			if($payamount > 0)
			{

				
				
				if($sch_method == 1){
					$coneGetWay = array('gatewayType'=>5);
				
    				$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);

    				$stripepass = $get_gateway['gatewayPassword'];

					$gatewayType = 'Stripe';
					include APPPATH . 'plugins/Chargezoom-Stripe/ChargezoomStripe.php';

					$cnumber     = $card_data['CardNo'] ;
					$expmonth  = $card_data['CardMonth'];
					$exyear  = $card_data['CardYear'];
					$CardID   = $card_data['CardID'];
					$cvv   = $card_data['CardCVV'];
					$CardType	 = $card_data['CardType'];
					
					$plugin = new ChargezoomStripe();
				    $plugin->setApiKey($stripepass);

				    try {
					  	 $res = \Stripe\Token::create([
									'card' => [
									'number' =>$cnumber,
									'exp_month' => $expmonth,
									'exp_year' =>  $exyear,
									'cvc' => $cvv,
									'address_city'    =>   $Billing_City,
				                    'address_country'   => $Billing_Country,
				                    'address_line1'     => $Billing_Addr1,
				                    'address_line1_check'     => null,
				                    'address_line2'     => $Billing_Addr2,
				                    'address_state'     => $Billing_State,
				                    'address_zip' => $Billing_Zipcode,
				                    'address_zip_check' => null,
									'name' => $merchantName
								   ]
						]);
						$tcharge= json_encode($res);  
        				$rest = json_decode($tcharge);


					}catch (Exception $e) {
						$gatewayResponseCode = 0;
						$responsetext = $e->getMessage();
						
					  // Something else happened, completely unrelated to Stripe
					}
					
        			
        			if(isset($rest->id)){
        				$amount =  (int)($payamount*100);
        				$token = $rest->id; 

        				try {
							$customer = \Stripe\Customer::create(array(
							    'name' => $merchantName,
							    'description' => 'CZ Merchant',
							    'source' => $token,
							    "address" => ["city" => $Billing_City, "country" => $Billing_Country, "line1" => $Billing_Addr1, "line2" => $Billing_Addr2, "postal_code" => $Billing_Zipcode, "state" => $Billing_State]
							));
							$customer= json_encode($customer);
			              	  
							$resultstripecustomer = json_decode($customer);
						}catch (Exception $e) {
							
						  // Something else happened, completely unrelated to Stripe
						}

        				try {
						  	if(isset($resultstripecustomer->id))
							{
								$charge =	\Stripe\Charge::create(array(
							  	  'customer' => $resultstripecustomer->id,
								  "amount" => $amount,
								  "currency" => "usd",
								  "description" => "InvoiceID: #".$in_data['invoiceNumber'],
								 
								));	
							}
							else
							{
								$charge =	\Stripe\Charge::create(array(
								  "amount" => $amount,
								  "currency" => "usd",
								  "source" => $token, // obtained with Stripe.js
								  "description" => "Charge for merchant invoice",
								 
								));
							}
        					
	        				$charge= json_encode($charge);
				        
					   		$result = json_decode($charge);

						}catch (Exception $e) {
							$gatewayResponseCode = 0;
							$responsetext = $e->getMessage();
							
						  // Something else happened, completely unrelated to Stripe
						}
        				 

				   		if($result->paid=='1' && $result->failure_code=="")
						{
						    $gatewayResponseCode = 200;
						    $responsetext = $result->status;
						    $txnID = $result->id;
						}else{
							if(isset($result->status)){
								$gatewayResponseCode = $result->failure_code;
								$responsetext = $result->status;
							}
							
						}

        			}else{
        				if(isset($rest->id)){
        					$txnID = 'TXN-'.$in_data['invoice'];
			        		$gatewayResponseCode = 0;
			        		$responsetext = $result->status;
        				}
        				
        			}
        			

				}else if($sch_method == 2){
					include APPPATH . 'third_party/nmiDirectPost.class.php';
					include APPPATH . 'third_party/nmiCustomerVault.class.php';
					$coneGetWay = array('gatewayType'=>1);
				
    				$get_gateway = $this->general_model->get_row_data('tbl_admin_gateway', $coneGetWay);
    				$nmiuser   = $get_gateway['gatewayUsername'];
					$nmipass   = $get_gateway['gatewayPassword'];
					$nmi_data = array('nmi_user'=>$nmiuser, 'nmi_password'=>$nmipass);
					$transaction1 = new nmiDirectPost($nmi_data); 

					$gatewayType = 'NMI';
					
					$accountName   = $card_data['acc_name'] ;
					$accountNumber = $card_data['acc_number'];
					$routeNumber = $card_data['route_number'];
					$accountType = $card_data['acct_type'];
					$accountHolderType = $card_data['acct_holder_type'];
					$transaction1->setAccountName($accountName);
					$transaction1->setAccount($accountNumber);
					$transaction1->setRouting($routeNumber);
					$sec_code = 'WEB';
					$transaction1->setAccountType($accountType);
					$transaction1->setAccountHolderType($accountHolderType);
					$transaction1->setSecCode($sec_code);
					$transaction1->setPayment('check');

					if($Billing_Addr1 != ''){
						$transaction1->setCountry($Billing_Addr1);
					}else{
						if($postData['merchantAddress1'] != ''){
							$transaction1->setAddress1($postData['merchantAddress1']);
						}
					}
					
					if($Billing_Country != ''){
						$transaction1->setCountry($Billing_Country);
					}else{
						if($postData['merchantCountry'] != ''){
							$transaction1->setCountry($postData['merchantCountry']);
						}else{
							$transaction1->setCountry('US');
						}
						
					}	
					
					if($Billing_City != ''){
						$transaction1->setCity($Billing_City);
					}else{
						if($postData['merchantCity'] != ''){
							$transaction1->setCity($postData['merchantCity']);
						}
					}
					
					if($Billing_Zipcode != ''){
						$transaction1->setZip($Billing_Zipcode);
					}else{
						if($postData['merchantZipCode'] != ''){
							$transaction1->setZip($postData['merchantZipCode']);
						}
					}
					if($billing_phone_number != ''){
						$transaction1->setPhone($billing_phone_number);
					}else{
						if($postData['merchantContact'] != ''){
							$transaction1->setPhone($postData['merchantContact']);
						}
					}
					
					if($Billing_State != ''){
						$transaction1->setState($Billing_State);
					}else{
						if($postData['merchantState'] != ''){
							$transaction1->setState($postData['merchantState']);
						}
					}
					

					$transaction1->setEmail($postData['merchantEmail']);
					$transaction1->setCompany($postData['companyName']);
					$transaction1->setFirstName($postData['firstName']);
					$transaction1->setLastName($postData['lastName']);

					$transaction1->setAmount($payamount);
					$transaction1->setTax('tax');
					$transaction1->sale();
					
					if(!empty($in_data['invoiceNumber'])){

						$transaction1->addQueryParameter('merchant_defined_field_1', 'Invoice Number: '. $in_data['invoiceNumber']);
					}

					$getwayResponse = $transaction1->execute();
					$txnID = $getwayResponse['transactionid'];

					if($getwayResponse['response_code']=="100"){
						$gatewayResponseCode = 200;
						$responsetext = $getwayResponse['responsetext'];
					}
				}else{
					$getwayResponse = 0;
				}

				if($gatewayResponseCode == 200 ){
					
					$invoice      = $in_data['invoice'];  
					$status 	 = 'paid';
					$paid_status = '1';
					$pay        = $payamount;
					$remainbal  = $in_data['BalanceRemaining']-$payamount;
					$data   	 = array('status'=>$status, 'AppliedAmount'=>$pay , 'BalanceRemaining'=>$remainbal );

					$condition  = array('invoice'=>$in_data['invoice'] );
					 
					$this->general_model->update_row_data('tbl_merchant_billing_invoice',$condition, $data);
				    				
				}
				 
			    $transaction = array();
			    $transaction['transactionID']      = $txnID;
				$transaction['transactionStatus']  = $responsetext;
				$transaction['transactionCode']    = $gatewayResponseCode;
				$transaction['transactionType']    = 'sale';
				$transaction['transactionDate']    = date('Y-m-d H:i:s'); 
				$transaction['transactionModified']= date('Y-m-d H:i:s'); 
				$transaction['merchant_invoiceID']       = $in_data['invoice'];
				$transaction['gatewayID']          = $get_gateway['gatewayID'];
				$transaction['transactionGateway'] = 1;	
				$transaction['transactionAmount']  = $payamount;
				$transaction['gateway']            = $gatewayType;
				$transaction['merchantID']         = $merchantID;
			  
				 $id = $this->general_model->insert_row('tbl_merchant_tansaction',$transaction);
			}	
		
        }
        
        return true;
	}

	private function dashboardJSON($rID, $agentId = false){
		$merchantCount = array('resellerID' => $rID, 'isDelete' => 0, 'isEnable' => 1);
		if($agentId){
			$merchantCount['agentID'] = $agentId;
		}
		
		$get_result = $this->reseller_panel_model->chart_get_dashboard_merchant($rID);
		$get_result['total_merchant'] = $this->general_model->get_total_merchants($merchantCount);
		$get_result['merchant_chart_base'] =   0;
		$response = json_encode($get_result);
		write_file(FCPATH."uploads/reseller_dashboard_chart-$rID.json", $response,'w+');
		return $response;
	}

	public function dashboard_chart_json()
	{
		$rID = $this->input->post('resellerID', true);
		
		if($rID){
			$cachedFile = FCPATH."uploads/reseller_dashboard_chart-$rID.json";
			if(file_exists($cachedFile))
			{
				echo file_get_contents($cachedFile);
			} else {
				echo $this->dashboardJSON($rID);
			}
		} else {
			echo json_encode([]);
		}
		die;
	}
}
