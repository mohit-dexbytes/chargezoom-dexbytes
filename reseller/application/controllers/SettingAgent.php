<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SettingAgent extends CI_Controller {
	private $createrType;
	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('general');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('general_model');
		$this->load->model('reseller_panel_model');
		delete_cache();
		
		  if($this->session->userdata('reseller_logged_in')!="")
		  {
		   	$this->createrType = 1;

		  }else if($this->session->userdata('agent_logged_in')!="")
		   {
		   		$this->createrType = 2;

		 		$data['login_info'] 	= $this->session->userdata('agent_logged_in');
		
		   		$user_id 				= $data['login_info']['resellerIndexID'];

		   		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
		   		if($agentData['agentManagement'] == 1){
		   			$this->session->set_flashdata('message','<div class="alert alert-danger"><strong> Not permitted to access.</strong></div>');
					redirect('Reseller_panel/index', 'refresh'); 
		   		}
		  	}else{
		  
			redirect('login','refresh');
		  }
	}
	
	
	public function index()
	{   
		
	}
	
	
	public function reseller_agents()
	{
	
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
		$data['isEditMode']  = 1;
		
       	if($this->session->userdata('reseller_logged_in'))
        {
		  	$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		  	$login_user_id = $data['login_info']['resellerID'];
		
		   $user_id 				= $data['login_info']['resellerID'];

		   $data['r_agent'] = $this->general_model->get_reselr_agent_data_data(array('resellerIndexID'=>$user_id, 'isEnable'=>'1'));
		   $data['re_agents']  =   $this->reseller_panel_model->get_agent_data(array('resellerIndexID'=>$user_id, 'isEnable'=>'1'));
		}
		if($this->session->userdata('agent_logged_in'))
        {
	    	$data['login_info'] 	= $this->session->userdata('agent_logged_in');
		
		   $user_id 				= $data['login_info']['resellerIndexID'];

		   $login_user_id = $data['login_info']['ragentID'];

		   $agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $login_user_id]);
		  
		   if($agentData['agentManagement'] == 2){
		   		$data['isEditMode']  = 0;
		   } 
		   
		   $Currentagent        = [];

		   $allAgent       = $this->general_model->get_reselr_agent_data_data(array('resellerIndexID'=>$user_id, 'isEnable'=>'1'));

		   $data['r_agent'] = array_unique(array_merge($allAgent,$Currentagent), SORT_REGULAR);

		   $data['re_agents']  =   $this->reseller_panel_model->get_agent_data(array('resellerIndexID'=>$user_id, 'isEnable'=>'1'));

		}	
		

		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/reseller_agents', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	}
	

    public function create_agent(){

		
    	 if($this->session->userdata('reseller_logged_in')){
		 	$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
			$user_id 				=  $data['login_info']['resellerID'];
			$login_user_id = $user_id;

		}
		if($this->session->userdata('agent_logged_in')){
			$data['login_info']	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

			$user_id 				= $data['login_info']['resellerIndexID'];
			$login_user_id = $data['login_info']['ragentID'];
			$rdata = $this->general_model->get_row_data('tbl_reseller_agent', array('ragentID'=>$login_user_id));

			if($rdata['agentManagement'] != 3){
				redirect(base_url('SettingAgent/reseller_agents'), 'refresh'); 
			}
			
		}	 
    		 
    	
	    
	    if(!empty($this->input->post(null, true)))
	    {

	        $this->load->library('form_validation');
	        	$this->form_validation->set_error_delimiters('<div class="error" style="color:red">', '</div>');
				
			    $this->form_validation->set_rules('agent_name', 'Agent Name', 'required|min_length[3]xss-clean');
			     if($this->czsecurity->xssCleanPostInput('agID')=='')
			     {
			        $this->form_validation->set_rules('agent_email', 'Agent Email', 'trim|required|min_length[3]|is_unique[tbl_reseller_agent.agentEmail]|xss-clean');
			    
			  
			        $this->form_validation->set_rules('agent_password', 'Password', 'trim|required|min_length[5]xss-clean');
	                 }
			   
			    
			    if($this->form_validation->run()==TRUE)
			    {  
			        
			      $agename=  $indata['agentName']        = $this->czsecurity->xssCleanPostInput('agent_name');
			      $agemail=    $indata['agentEmail']       = $this->czsecurity->xssCleanPostInput('agent_email');
			       
			        $indata['commission']       = $this->czsecurity->xssCleanPostInput('agent_commission');
			        
			        $indata['resellerIndexID'] =  $user_id;
			        

			        $indata['dashboardPermission']    = $this->czsecurity->xssCleanPostInput('dashboardPermission');
			        $indata['agentManagement']    = $this->czsecurity->xssCleanPostInput('agentManagement');
			        $indata['generalSettings']    = $this->czsecurity->xssCleanPostInput('generalSettings');
			        $indata['plans']    = $this->czsecurity->xssCleanPostInput('plans');
			        $indata['emailTemplates']    = $this->czsecurity->xssCleanPostInput('emailTemplates');
			        $indata['merchantPortal']    = $this->czsecurity->xssCleanPostInput('merchantPortal');
			        $indata['api']    = $this->czsecurity->xssCleanPostInput('api');
			        $indata['userMerchantView']    = ($this->czsecurity->xssCleanPostInput('userMerchantView'))?1:0;
			        $indata['userMerchantEdit']    = ($this->czsecurity->xssCleanPostInput('userMerchantEdit'))?1:0;
			        $indata['userMerchantAdd']    = ($this->czsecurity->xssCleanPostInput('userMerchantAdd'))?1:0;
			        $indata['userMerchantDisabled']    = ($this->czsecurity->xssCleanPostInput('userMerchantDisabled'))?1:0;


			        $indata['allMerchantView']    = ($this->czsecurity->xssCleanPostInput('allMerchantView'))?1:0;
			        $indata['allMerchantEdit']    = ($this->czsecurity->xssCleanPostInput('allMerchantEdit'))?1:0;
			        $indata['allMerchantAdd']    = ($this->czsecurity->xssCleanPostInput('allMerchantAdd'))?1:0;
			        $indata['allMerchantDisabled']    = ($this->czsecurity->xssCleanPostInput('allMerchantDisabled'))?1:0;

			        $indata['updatedAt'] = date("Y-m-d H:i:s");
			        if($this->czsecurity->xssCleanPostInput('agent_password')!="" && $this->czsecurity->xssCleanPostInput('conf_password')!="")
			        {
			           $pass = $this->czsecurity->xssCleanPostInput('agent_password');
			           $indata['agentPasswordNew'] = password_hash($this->czsecurity->xssCleanPostInput('agent_password'), PASSWORD_BCRYPT);
			        }
			      if($this->czsecurity->xssCleanPostInput('agID')!='')
			      {     
			            $indata['updatedAt'] = date("Y-m-d H:i:s");
			            $rID=   $this->czsecurity->xssCleanPostInput('agID');   
			            $id = $this->general_model->update_row_data('tbl_reseller_agent',array('ragentID'=>$rID), $indata);
			             $message="Successfully Updated "; 
			          
			      }else{
			         $indata['createdBy'] =  $login_user_id;
			         $indata['createrType'] =    $this->createrType;
			          
			         $indata['createdAt'] =  date("Y-m-d H:i:s");
			         $indata['updatedAt'] = date("Y-m-d H:i:s");
			        $id = $this->general_model->insert_row('tbl_reseller_agent', $indata);
			        if($id>0  && $this->czsecurity->xssCleanPostInput('agentmail')=='1')
			        {
			          $this->load->library('email');
				
			    	$rdata = $this->general_model->get_row_data('tbl_reseller', array('resellerID'=>$user_id));
    			    $rcompany = $rdata['resellerCompanyName'];
    			    $rname    = $rdata['resellerfirstName'].' '. $rdata['lastName'];
    			    $phone    =  $rdata['billingContact'];	
    				
			    	
			    	$condition_mail         = array('templateType' => '9', 'resellerID' => $user_id);
			    	$link = base_url();
        			$login_url = '<a href="'.$link.'">'.$link.'</a>';

			    	$this->general_model->send_mail_data($condition_mail,$agename,$rcompany,'', '', $login_url, $agemail, $pass, $rname, '');
			       
			        
			            
			        }
			         $message="Successfully Created "; 
			      }
			        
			        if($id)
			        {
			            $this->session->set_flashdata('success', $message);
			           
		     
			            
			            
			         
		               redirect(base_url('SettingAgent/reseller_agents'), 'refresh');  
			           }else{
			            
			             $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
		              
			        }
			        
			      
		     
			        
			        
			    }else{
			     if(form_error('agent_email')) 
			     $error = form_error('agent_email');
			     else if(form_error('agent_password'))  
			      $error = form_error('agent_password');
			       else  if(form_error('agent_name'))  
			        $error = form_error('agent_name');
			        else  if(form_error('agent_commission'))  
			         $error = form_error('agent_commission');
			        
			            $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error '.$error.' </strong></div>');  
			       
			    }
	        
	    }else if($this->uri->segment('3'))
	    {
	          $rs_id   = $this->uri->segment('3');
	        $data['agent'] = $this->general_model->get_row_data('tbl_reseller_agent', array('ragentID'=>$rs_id));
	        
	    }
	
    	     $data['primary_nav'] 	= primary_nav();
    		 $data['template'] 		= template_variable();
    		$this->load->view('template/template_start', $data);
    		$this->load->view('template/page_head', $data);
    		$this->load->view('reseller_pages/add_agent', $data);
    		$this->load->view('template/page_footer',$data);
    		$this->load->view('template/template_end', $data);
    	
	}
	
	
	   public function edit_disable_agent()
	   {
	
		
		 if($this->session->userdata('reseller_logged_in')){
		 $data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
		$user_id 				=  $data['login_info']['resellerID'];
		}
		if($this->session->userdata('agent_logged_in')){
		$data['login_info']	= $this->session->userdata('agent_logged_in');
		$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);

		
		$user_id 				= $data['login_info']['resellerIndexID'];
		}	 
    		 
	
	    if(!empty($this->input->post(null, true)))
	    {
	        
			        
			     
			        $indata['agentEmail']       = $this->czsecurity->xssCleanPostInput('agent_email');
			      
			        
			        $indata['updatedAt'] = date("Y-m-d H:i:s");
			         
			      if($this->czsecurity->xssCleanPostInput('agID')!='')
			      {     
			            $indata['updatedAt'] = date("Y-m-d H:i:s");
			            $rID=   $this->czsecurity->xssCleanPostInput('agID');   
			            if($this->general_model->check_reseller_agent($indata['agentEmail'], $rID))
			            {
						$id = $this->general_model->update_row_data('tbl_reseller_agent',array('ragentID'=>$rID), $indata);
						$this->session->set_flashdata('success', 'Agent Succesfully Updated');
			         
		     
			            }else{
			                
			                $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Email Already Exist.</strong></div>');    
			                 redirect(base_url('SettingAgent/edit_disable_agent/'. $rID), 'refresh');   
			            }
			             
			          
			      }else{
			       $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error</strong></div>');      
			           redirect(base_url('SettingAgent/edit_disable_agent/'. $rID), 'refresh');   
			      }
			        
			  redirect(base_url('SettingAgent/disable_agents'), 'refresh');   
			  
	        
	    }else if($this->uri->segment('3'))
	    {
	          $rs_id   = $this->uri->segment('3');
	        $data['agent'] = $this->general_model->get_row_data('tbl_reseller_agent', array('ragentID'=>$rs_id));
	        
	    }
	
    	     $data['primary_nav'] 	= primary_nav();
    		 $data['template'] 		= template_variable();
    		$this->load->view('template/template_start', $data);
    		$this->load->view('template/page_head', $data);
    		$this->load->view('reseller_pages/edit_disable_agent', $data);
    		$this->load->view('template/page_footer',$data);
    		$this->load->view('template/template_end', $data);
    	
	}
	function get_del_agent()
	{
	    
	         $data=array();
	         $data1	    = $this->session->userdata('reseller_logged_in');
    		 $user_id = $data1['resellerID'];
	    
	   if(!empty($this->input->post(null, true)))
	   {
	       
	       
	       
	    	$data['r_agent']=     $sql=$this->db->query("Select ragentID, agentName  from  tbl_reseller_agent where resellerIndexID='".$user_id."' and ragentID != '".$this->czsecurity->xssCleanPostInput('agetID')."'  ")->result_array();
	       
	   }
	   
	    echo json_encode($data);
	    
	}

		
	public function disable_agents(){
		$data['isEditMode'] = 1;
		
		if($this->session->userdata('reseller_logged_in')){
		 	$data['login_info'] 	= $this->session->userdata('reseller_logged_in');
		
			$user_id 				=  $data['login_info']['resellerID'];

			$data['r_agent'] = $this->general_model->get_reselr_agent_data_data(array('resellerIndexID'=>$user_id, 'isEnable'=>'0'));
		}
		if($this->session->userdata('agent_logged_in')){
			$data['login_info']	= $this->session->userdata('agent_logged_in');
			$agentData = $this->reseller_panel_model->get_select_data('tbl_reseller_agent', ['*'], ['ragentID' => $data['login_info']['ragentID']]);
	   		if($agentData['agentManagement'] == 2){
	   			$data['isEditMode'] = 0;
	   		}
			$user_id 				= $data['login_info']['resellerIndexID'];

			$login_user_id = $data['login_info']['ragentID'];

			$data['r_agent'] = $this->general_model->get_reselr_agent_data_data(array('createdBy'=>$login_user_id, 'isEnable'=>'0','createrType' => 2));
		}	 
    		 
		
	    $data['primary_nav'] 	= primary_nav();
		$data['template'] 		= template_variable();
	  
	   	
		$this->load->view('template/template_start', $data);
		$this->load->view('template/page_head', $data);
		$this->load->view('reseller_pages/disable_agents', $data);
		$this->load->view('template/page_footer',$data);
		$this->load->view('template/template_end', $data);
	
	}
	
	
	 
  public function delete_ragent(){
      
      
              $data=array();
	      
       	if($this->session->userdata('reseller_logged_in')){
		$data1 	= $this->session->userdata('reseller_logged_in');
		
		$user_id 				= $data1['resellerID'];
		}
		
    	else if($this->session->userdata('agent_logged_in')){
    	    $data1 = $this->session->userdata('agent_logged_in');
    	    $user_id  = $data1['resellerID'];
    	}
		 
		 
		 if(!empty($this->czsecurity->xssCleanPostInput('reselleragentid')))
		 {
		     
		      if(!empty($this->czsecurity->xssCleanPostInput('reseller_agent')))
		     $new_ragentID = $this->czsecurity->xssCleanPostInput('reseller_agent');
		     else
		      $new_ragentID = 0;
		 $old_ragentID = $this->czsecurity->xssCleanPostInput('reselleragentid');
		 
		 $con     =array('resellerID'=>$user_id, 'agentID'=>$old_ragentID);
		 $upd_data=array('agentID'=>$new_ragentID);
		 
		if($this->general_model->update_row_data('tbl_merchant_data',$con, $upd_data))
		{
		 $condition =  array('ragentID'=>$old_ragentID); 
		 $del      = $this->general_model->delete_row_data('tbl_reseller_agent', $condition);
          if($del){
			$this->session->set_flashdata('success', 'Successfully Deleted');
            
		     
    	          
    	  }else{
                    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: </strong>.</div>');        
    	  }	 
	  
		}else{
		    $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: </strong>.</div>');   
		     
		 }		 
	  
	  
	  
	  
		 }else{
		      $this->session->set_flashdata('message','<div class="alert alert-danger">  <strong>Error: No Agent Selected</strong>.</div>');   
		     
		 } 
		 
		
	  	redirect(base_url('SettingAgent/reseller_agents'));
    	 
	}

	function suspnd_agent()
	{
	    
	         $data=array();
	         $data1	    = $this->session->userdata('reseller_logged_in');
    		 $user_id   = $this->czsecurity->xssCleanPostInput('reselleragent_id');
	    
	        
	    
	    
	   if(!empty($user_id))
	        { 
    	          $con     =array('ragentID'=>$user_id, 'isEnable'=>'1');
    	          $updt=array('isEnable'=>'0');
    	     $updt['updatedAt'] =  date("Y-m-d H:i:s");
    		 $data=$this->general_model->update_row_data('tbl_reseller_agent',$con,$updt);
    		 $this->session->set_flashdata('success', 'Successfully Suspended');
    		
    		 redirect(base_url('SettingAgent/disable_agents'));
    		}	else
			{
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong>');
						redirect(base_url('SettingAgent/disable_agents'));
					
			}
       	
}




	function active_agent()
	{
	    
	         $data=array();
	         $data1	    = $this->session->userdata('reseller_logged_in');
    		 $user_id   = $this->czsecurity->xssCleanPostInput('activeagent_id');
	    
	   if(!empty($user_id))
	        { 
    	          $con     =array('ragentID'=>$user_id, 'isEnable'=>'0');
    	          $updt=array('isEnable'=>'1');
    	           $updt['updatedAt'] =  date("Y-m-d H:i:s");
			 $data=$this->general_model->update_row_data('tbl_reseller_agent',$con,$updt);
			 $this->session->set_flashdata('success', 'Successfully Activated');
    		
    		
    		 redirect(base_url('SettingAgent/reseller_agents'));
    		}	else
			{
						$this->session->set_flashdata('message', '<div class="alert alert-danger">  <strong>Error:</strong></div>');
						redirect(base_url('SettingAgent/reseller_agents'));
					
			}  }
			
			
	
	public function get_merchant_id()
     {
		
			if($this->session->userdata('reseller_logged_in')){
		$da 	= $this->session->userdata('reseller_logged_in');
		
		$user_id 				= $da['resellerID'];
		}
		else if($this->session->userdata('agent_logged_in')){
		$da	= $this->session->userdata('agent_logged_in');
		
		$user_id 				= $da['resellerIndexID'];
		}
		
	if($user_id!='')
	{
        $id = $this->czsecurity->xssCleanPostInput('agent_id');
        
        if($id=='None')
        $id =0;
		$rID = $user_id;
	
		
		$datas = $this->reseller_panel_model->get_reseller_merchant($rID, $id);
		
		?>
		    
        
		   <table class="table table-bordered table-striped table-vcenter">
            
            <tbody>
				
				<tr>
					<th class="text-left col-md-6"> <strong>Merchant</strong></th>
				
					<th class="text-center col-md-6"><strong>Select</strong></th>
			  </tr>	
 	<?php	
	 if(!empty($datas)){
				  foreach($datas as $c_data){
			?>	
		
			<tr>  
				<td class="text-left visible-lg  col-md-6"> <?php echo $c_data['companyName']; ?> </td>
			
			    <td class="text-center visible-lg col-md-6"> <input type="checkbox" name="gate[]" id="gate" checked="checked" value="<?php echo $c_data['merchID']; ?>" /> </td>
			</tr>	
			
           
			<?php     }   } else { ?>

			<tr>  
			<td class="text-left visible-lg col-md-6 control-label"> <?php echo "No Record Found..!";?> </td>
			
			<td class="text-right visible-lg col-md-6 control-label">  </td>				
			    
			  <?php } ?>
           
			</tbody>
        </table>
				
					
		<?php  die;
		
		}else{
		    
		    return false;
		}		
	
			
     }	
			
			
			
	function update_agent()
	{
	   		if($this->session->userdata('reseller_logged_in')){
		$da 	= $this->session->userdata('reseller_logged_in');
		
		$user_id 				= $da['resellerID'];
		}
		else if($this->session->userdata('agent_logged_in')){
		$da	= $this->session->userdata('agent_logged_in');
		
		$user_id 				= $da['resellerIndexID'];
		}
		
		
		if(!empty($this->czsecurity->xssCleanPostInput('agent_new')) and !empty($this->czsecurity->xssCleanPostInput('agent_old')))
		{
		 $age_new =  $this->czsecurity->xssCleanPostInput('agent_new');
		 if($age_new=='None')
		 {
		     $age_new=0;
		 }
		 $age_old =   $this->czsecurity->xssCleanPostInput('agent_old');
		 if($age_old=='None')
		 {
		     $age_old=0;
		 }
		 $sub_ID = @implode(',', $this->czsecurity->xssCleanPostInput('gate')); 
		 $update_data=array('agentID'=>$age_new);
		 $condition  =array('agentID'=>$age_old,'resellerID'=>$user_id);
		 
		 if($this->general_model->get_num_rows('tbl_merchant_data',$condition) > 0)
		 {
		       if($this->reseller_panel_model->update_bulk_agent($user_id, $age_new, $sub_ID))
			   $this->session->set_flashdata('success', 'Successfully Updated Agent');
		     
		     
		      else
		         $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
		 }else{
		     $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Error</strong></div>');
		 }
		 
		}else{
		    
		   $this->session->set_flashdata('message', '<div class="alert alert-danger"><strong>Validation Error.</strong></div>');
		}
	    	redirect(base_url('SettingAgent/reseller_agents'));
	}
			
			
			
}







 
               
	
			 
	   
	 
		 
			 
			 
			 
	   
	 
	




