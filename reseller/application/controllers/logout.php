<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Logout extends CI_Controller {

	function __construct() 
	{ 
		parent::__construct(); 
		$this->load->model('reseller_login_model'); 
	}


	public function index()

	{  
		$data['title'] = 'Logout';

		$status = $this->reseller_login_model->user_logout();

		 redirect('login', 'refresh'); 
	}
	
	public function agent()

	{  
		$data['title'] = 'Logout';

		$status = $this->reseller_login_model->agent_logout();

		 redirect('login', 'refresh'); 
	}
	

}



