<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Czsecurity extends CI_Security {
	
	protected $CI;

	protected $throttleLimit = 5;

	protected $throttleTime = 300;

    public function __construct(){

        // Append application specific cookie prefix
        if (config_item('cookie_prefix'))
        {
            $this->_csrf_cookie_name = config_item('cookie_prefix').$this->_csrf_cookie_name;
        }
            
		$this->_csrf_set_hash();
		$this->csrf_set_cookie();

		// Assign the CodeIgniter super-object
		$this->CI =& get_instance();
		$this->CI->load->model('general_model');
	}
	
	/**
	 * Set Cross Site Request Forgery Protection Cookie
	 *
	 * @return	object
	 */
	public function csrf_set_cookie()
	{
		$expire = time() + $this->_csrf_expire;
		$secure_cookie = (config_item('cookie_secure') === TRUE) ? 1 : 0;

		setcookie(
			$this->_csrf_cookie_name,
			$this->_csrf_hash,
			$expire,
			config_item('cookie_path'),
			config_item('cookie_domain'),
			$secure_cookie,
			config_item('cookie_httponly')
		);

		log_message('debug', "CRSF cookie Set");

		return $this;
	}

    /**
	 * Verify Cross Site Request Forgery Protection
	 *
	 * @return	object
	 */
	public function csrf_verify()
	{
		$ip = $this->getClientIpAddr();
		
		// If it's not a POST request we will set the CSRF cookie
		if (strtoupper($_SERVER['REQUEST_METHOD']) !== 'POST')
		{
            $this->csrf_set_cookie();
            
            return true;
		}

		// Do the tokens exist in both the _POST and _COOKIE arrays?
		if ( ! isset($_POST[$this->_csrf_token_name], $_COOKIE[$this->_csrf_cookie_name]))
		{
			return false;
		}

		// Do the tokens match?
		if ($_POST[$this->_csrf_token_name] != $_COOKIE[$this->_csrf_cookie_name])
		{
			return false;
		}

		// We kill this since we're done and we don't want to
		// polute the _POST array
		unset($_POST[$this->_csrf_token_name]);

		// Nothing should last forever
		unset($_COOKIE[$this->_csrf_cookie_name]);
		$this->_csrf_set_hash();
		$this->csrf_set_cookie();

		return true;
    }
	
	/**
	 * Add csrf token hidden input to form
	 */
    public function appendFormCsrfToken(){
        return '<input type="hidden" id="__token" name="'.$this->get_csrf_token_name().'" value="'.$this->get_csrf_hash().'"/>';
	}

	/**
     * Clean post input
     * @param string $var post variable.
	 * @param boolean $xss_clean DEFAULT true 
     *
     */
	public function xssCleanPostInput($var, $xss_clean=true)
	{
		return $this->CI->input->post($var, $xss_clean);
	}
	/**
     * throttles multiple connections attempts to prevent abuse
     * @param string $prefix session prefix.
     *
     */
    public function throttle($prefix='')
    {
		$ip = $this->getClientIpAddr();

		$seconds = $this->throttleTime;
		
		$message = $this->errorMessages('login_attempt_exceeded');
		
		$lock_time = $this->CI->session->userdata($prefix.'_login_locktime');

		$login_attempts = $this->CI->session->userdata($prefix.'_login_attempt');
		
		$login_attempts = $login_attempts ? $login_attempts : 0;

		$login_attempts = $login_attempts+1;

		// Add Login attempt in database based on the IP
		$this->CI->general_model->addLoginAttempt($ip, $this->CI->input->post(null, true));

		if($lock_time)
		{
			$seconds = $lock_time - time();
			if($seconds <= 0) {
				$login_attempts = 1;
				$this->CI->session->set_userdata($prefix.'_login_locktime', 0);
			} else {
				return ['status' => 'locked', 'seconds' => $seconds, 'message' => sprintf($message , $seconds)];
			}
		}

		$this->CI->session->set_userdata($prefix.'_login_attempt', $login_attempts);

        if ($login_attempts >= $this->throttleLimit) 
		{
			$this->CI->session->set_userdata($prefix.'_login_locktime', time()+$this->throttleTime);

            return ['status' => 'locked', 'seconds' => $seconds, 'message' => sprintf($message , $seconds)];
        } else {

			return ['status' => 'open', 'login_attempts' => $login_attempts, 'message' => ''];
		}
	}

	public function checkThrottle($prefix='')
	{
		$ip = $this->getClientIpAddr();

		$seconds = $this->throttleTime;
		
		$message = $this->errorMessages('login_attempt_exceeded');
		
		
		$lock_time = $this->CI->session->userdata($prefix.'_login_locktime');
		$login_attempts = $this->CI->session->userdata($prefix.'_login_attempt');

		if($lock_time)
		{
			$seconds = $lock_time - time();
			if($seconds > 0) {
				return ['status' => 'locked', 'seconds' => $seconds, 'message' => sprintf($message , $seconds)];
			} else {
				$login_attempts = 1;

				$this->CI->session->set_userdata($prefix.'_login_attempt', $login_attempts);
				$this->CI->session->set_userdata($prefix.'_login_locktime', 0);
			}
		}
		
		return ['status' => 'open', 'login_attempts' => $login_attempts, 'message' => ''];
	}

	public function clearThrottle($prefix='')
	{
		$ip = $this->getClientIpAddr();

		$this->CI->session->set_userdata($prefix.'_login_attempt', 0);
		$this->CI->session->set_userdata($prefix.'_login_locktime', 0);

		// Delete the Login Attempts
		$this->CI->general_model->deleteLoginAttempt($ip);
	}

	public function checkPasswordStrength($password){
		// Validate password strength
		$uppercase = preg_match('@[A-Z]@', $password);
		$lowercase = preg_match('@[a-z]@', $password);
		$number    = preg_match('@[0-9]@', $password);
		$specialChars = preg_match('@[^\w]@', $password);

		if(!$uppercase || !$lowercase || !$number || !$specialChars || strlen($password) < 8) {
			return ['status' => 'FAILED', 'message' => $this->errorMessages('strong_password_failed')];
		}else{
			return ['status' => 'OK', 'message' => 'Good'];
		}
	}

	/**
	 * length - the length of the generated password
	 * characters - types of characters to be used in the password
	 * 
	 */
	function randomPassword($length=8, $characters=null) {
 		// define variables used within the function    
		$symbols = array();
		$passwords = array();
		$used_symbols = '';
		$pass = '';
		
		// an array of different character types    
		$symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
		$symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$symbols["numbers"] = '1234567890';
		$symbols["special_symbols"] = '!?~@#-_+[]{}%*';
		
		if(!empty($characters)) {
			$characters = explode(",",$characters); // get characters types to be used for the passsword
			foreach ($characters as $key=>$value) {
				$used_symbols .= $symbols[$value]; // build a string with all characters
			}

		} else {
			$used_symbols = implode('', $symbols);
		}
		
		$symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1
			
		$password = '';
		for ($i = 0; $i < $length; $i++) {
			$n = rand(0, $symbols_length); // get a random character from the string with all characters
			$password .= $used_symbols[$n]; // add the character to the password string
		}
			
		return $password; // return the generated password
	}

	public function getClientIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	public function errorMessages($code=''){
		$messages = [
			'token_expired' => 'Token expired !!',
			'login_attempt_exceeded' => 'You have exceeded the maximum login attempts. you can retry after %d seconds.',
			'strong_password_failed' => 'Password should be at least 8 characters in length and should include at least one upper case letter, one number, and one special character.',
		];

		return isset($messages[$code]) ? $messages[$code] : '';
	}
    
}