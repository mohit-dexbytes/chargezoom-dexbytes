<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

switch(getenv('ENV')){
    case 'development':
    case 'staging':
    case 'local':
        $config['maverick_payment_env'] = 'sandbox';
    break;
    case 'production':
        $config['maverick_payment_env'] = 'production';
    break;
    default:
        $config['maverick_payment_env'] = 'sandbox';
    break;
}