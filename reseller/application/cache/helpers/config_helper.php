<?php


function template_variable()
{
	$template = array(
		'name'              => 'ChargeZoom',
		'version'           => '0.1',
		'author'            => 'Ecrubit',
		'robots'            => 'noindex, nofollow',
		'title'             => 'ChargeZoom - Automate Your QuickBook Payments',
		'description'       => 'ChargeZoom automates your quickbook payments and help you focus on your business.',
		// true                     enable page preloader
		// false                    disable page preloader
		'page_preloader'    => true,
		// true                     enable main menu auto scrolling when opening a submenu
		// false                    disable main menu auto scrolling when opening a submenu
		'menu_scroll'       => true,
		// 'navbar-default'         for a light header
		// 'navbar-inverse'         for a dark header
		'header_navbar'     => 'navbar-default',
		// ''                       empty for a static layout
		// 'navbar-fixed-top'       for a top fixed header / fixed sidebars
		// 'navbar-fixed-bottom'    for a bottom fixed header / fixed sidebars
		'header'            => '',
		// ''                                               for a full main and alternative sidebar hidden by default (> 991px)
		// 'sidebar-visible-lg'                             for a full main sidebar visible by default (> 991px)
		// 'sidebar-partial'                                for a partial main sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-partial sidebar-visible-lg'             for a partial main sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-mini sidebar-visible-lg-mini'           for a mini main sidebar with a flyout menu, enabled by default (> 991px + Best with static layout)
		// 'sidebar-mini sidebar-visible-lg'                for a mini main sidebar with a flyout menu, disabled by default (> 991px + Best with static layout)
		// 'sidebar-alt-visible-lg'                         for a full alternative sidebar visible by default (> 991px)
		// 'sidebar-alt-partial'                            for a partial alternative sidebar which opens on mouse hover, hidden by default (> 991px)
		// 'sidebar-alt-partial sidebar-alt-visible-lg'     for a partial alternative sidebar which opens on mouse hover, visible by default (> 991px)
		// 'sidebar-partial sidebar-alt-partial'            for both sidebars partial which open on mouse hover, hidden by default (> 991px)
		// 'sidebar-no-animations'                          add this as extra for disabling sidebar animations on large screens (> 991px) - Better performance with heavy pages!
		'sidebar'           => 'sidebar-partial sidebar-visible-lg sidebar-no-animations',
		// ''                       empty for a static footer
		// 'footer-fixed'           for a fixed footer
		'footer'            => '',
		// ''                       empty for default style
		// 'style-alt'              for an alternative main style (affects main page background as well as blocks style)
		'main_style'        => '',
		// ''                           Disable cookies (best for setting an active color theme from the next variable)
		// 'enable-cookies'             Enables cookies for remembering active color theme when changed from the sidebar links (the next color theme variable will be ignored)
		'cookies'           => '',
		// 'night', 'amethyst', 'modern', 'autumn', 'flatie', 'spring', 'fancy', 'fire', 'coral', 'lake',
		// 'forest', 'waterlily', 'emerald', 'blackberry' or '' leave empty for the Default Blue theme
		'theme'             => 'night',
		// ''                       for default content in header
		// 'horizontal-menu'        for a horizontal menu in header
		// This option is just used for feature demostration and you can remove it if you like. You can keep or alter header's content in page_head.php
		'header_content'    => '',
		'active_page'       => basename($_SERVER['REQUEST_URI'])
	);
 
 //echo basename($_SERVER[REQUEST_URI]);  die;
 //$_SERVER[HTTP_HOST];
 
	return $template;	
}


function primary_nav()
{ 
 $CI = & get_instance(); 
 
 
 $auth_names = $CI->session->userdata('user_logged_in')['authName']; //print_r($auth_names) ; exit;
 
 //print_r($auth_names); die;
 
 if($auth_names) {
 
 //echo in_array('Generate Report', $auth_names);  die;
 
 
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('home/index'),
			'page_name'=> 'index'
		),
	
		array(
			'name'  => 'Gateway',
			'icon'  => 'gi gi-lock',
			'sub'   => array(

				array(
					'name'  => 'Sale',
					'url'   => (in_array('Sale', $auth_names))? base_url('Payments/create_customer_sale'):'',
					'page_name'=> 'create_customer_sale'
				),
				array(
					'name'  => 'Authorize',
					'url'   =>   (in_array('Auth', $auth_names)) ? base_url('Payments/create_customer_auth') : '',
					'page_name'=> 'create_customer_auth'
				),
				
				array(
					'name'  => 'Capture/Void',
					'url'   =>  (in_array('Capture/Void', $auth_names)) ? base_url('Payments/payment_capture') : '',
					'page_name'=> 'payment_capture'
				),
			
				
				array(
					'name'  => 'Refund',
					'url'   =>  (in_array('Refund', $auth_names)) ? base_url('Payments/payment_refund') : '',
						'page_name'=> 'payment_refund'
				)
			)
		),
		array(
			'name'  => 'Customers',
			'icon'  => 'fa fa-users',
			'url'   => base_url('home/customer'),
			'page_name'=> 'customer'
		),
    /*   array(
			'name'  => 'Subscriptions',
			'icon'  => 'gi gi-refresh',
			'url'   =>  base_url('SettingSubscription/subscriptions'),
	     	'page_name'=> 'subscriptions'
		), */
		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => (in_array('Invoices', $auth_names)) ? base_url('home/invoices') : '',
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => (in_array('Payment', $auth_names)) ? base_url('Payments/payment_transaction') : '',
					'page_name'=> 'payment_transaction'
				),
				array(
					'name'  => 'Refunds',
					'url'   => (in_array('Refund', $auth_names)) ? base_url('Payments/refund_transaction') : '',
					'page_name'=> 'refund_transaction'
				),  
				array(
					'name'  => 'Credits',
					'url'   =>  (in_array('Credit', $auth_names)) ? base_url('Payments/credit') : '',
						'page_name'=> 'credit'
				)
			)
		),
		array(
			'name'  => 'Products & Services',
			'icon'  => 'gi gi-calendar',
		    'url'   => base_url('home/plan_product'),
		 	'page_name'=> 'plan_product'
		),
		array(
			'name'  => 'Reports',
			'icon'  => 'gi gi-signal',
			 'url'   => (in_array('Generate Report', $auth_names)) ?base_url('report/reports') : '', 
			 'page_name'=> 'reports'
		)/*,
		array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			'sub'   => array(
			
			      array(
                'name'  => 'Accounting Software',
                'url'   =>  base_url('home/company'),
                'page_name'=> 'company'
                ),
				array(
					'name'  => 'Admin Users',
					'url'   => base_url('MerchantUser/admin_user'),
						'page_name'=> 'admin_user'
				),
				array(
					'name'  => 'Admin Roles',
					'url'   => base_url('MerchantUser/admin_role'),
						'page_name'=> 'admin_role'
				),
			array(
					'name'  => 'Customer Portal',
					'url'   => base_url('SettingConfig/setting_customer_portal'),
					'page_name'=> 'setting_customer_portal'
				),
				
				array(
					'name'  => 'Taxes',
						'url'   =>base_url('SettingConfig/comingsoon'),
						'page_name'=> 'comingsoon'
				),
		    	array(
                'name'  => 'Email Personalization',
                'url'   =>  base_url('Settingmail/email_temlate'),
                'page_name'=> 'email_temlate'
                ),
				array(
					'name'  => 'General Settings',
					'url'   => base_url('SettingConfig/profile_setting'),
						'page_name'=> 'profile_setting'
				),
				array(
                'name'  => 'Merchant Gateway',
                'url'   =>  base_url('home/update_gateway'),
                'page_name'=> 'update_gateway'
                ),
				array(
					'name'  => 'API',
					'url'   =>base_url('SettingConfig/comingsoon'),
						'page_name'=> 'comingsoon'
				)
			)
		) */
	);
 } else { 
		$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('home/index'),
			'page_name'=> 'index'
		),
	
		array(
			'name'  => 'Gateway',
			'icon'  => 'gi gi-lock',
			'sub'   => array(

				array(
					'name'  => 'Sale',
					'url'   => base_url('Payments/create_customer_sale'),
					'page_name'=> 'create_customer_sale'
				),
				array(
					'name'  => 'Authorize',
					'url'   => base_url('Payments/create_customer_auth'),
					'page_name'=> 'create_customer_auth'
				),
				
				array(
					'name'  => 'Capture/Void',
					'url'   =>  base_url('Payments/payment_capture'),
					'page_name'=> 'payment_capture'
				),
			
				
				array(
					'name'  => 'Refund',
					'url'   => base_url('Payments/payment_refund'),
						'page_name'=> 'payment_refund'
				)
			)
		),
		array(
			'name'  => 'Customers',
			'icon'  => 'fa fa-users',
			'url'   => base_url('home/customer'),
			'page_name'=> 'customer'
		),
       array(
			'name'  => 'Subscriptions',
			'icon'  => 'gi gi-refresh',
			'url'   =>  base_url('SettingSubscription/subscriptions'),
	     	'page_name'=> 'subscriptions'
		),
		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
				array(
					'name'  => 'Refunds',
					'url'   => base_url('Payments/refund_transaction'),
					'page_name'=> 'refund_transaction'
				),
				array(
					'name'  => 'Credits',
					'url'   => base_url('Payments/credit'),
						'page_name'=> 'credit'
				)
			)
		),
		array(
			'name'  => 'Products & Services',
			'icon'  => 'gi gi-calendar',
		 'url'   => base_url('home/plan_product'),
		 	'page_name'=> 'plan_product'
		),
		array(
			'name'  => 'Reports',
			'icon'  => 'gi gi-signal',
			 'url'   => base_url('report/reports'),
			 'page_name'=> 'reports'
		),
		array(
			'name'  => 'Configuration',
			'icon'  => 'gi gi-settings',
			'sub'   => array(
			
			      array(
                'name'  => 'Accounting Software',
                'url'   =>  base_url('home/company'),
                'page_name'=> 'company'
                ),
				array(
					'name'  => 'Admin Users',
					'url'   => base_url('MerchantUser/admin_user'),
						'page_name'=> 'admin_user'
				),
				array(
					'name'  => 'Admin Roles',
					'url'   => base_url('MerchantUser/admin_role'),
						'page_name'=> 'admin_role'
				),
			array(
					'name'  => 'Customer Portal',
					'url'   => base_url('SettingConfig/setting_customer_portal'),
					'page_name'=> 'setting_customer_portal'
				),
				
				array(
					'name'  => 'Taxes',
						'url'   =>base_url('SettingConfig/comingsoon'),
						'page_name'=> 'comingsoon'
				),
		    	array(
                'name'  => 'Email Personalization',
                'url'   =>  base_url('Settingmail/email_temlate'),
                'page_name'=> 'email_temlate'
                ),
				array(
					'name'  => 'General Settings',
					'url'   => base_url('SettingConfig/profile_setting'),
						'page_name'=> 'profile_setting'
				),
				array(
                'name'  => 'Merchant Gateway',
                'url'   =>  base_url('home/gateway'),
                'page_name'=> 'gateway'
                ),
				array(
					'name'  => 'API',
					'url'   =>base_url('SettingConfig/comingsoon'),
						'page_name'=> 'comingsoon'
				)
			)
		)
	);
 }
	return $primary_nav;	
}




function primary_customer_nav()
{
	$primary_nav = array(
		array(
			'name'  => 'Dashboard',
			'icon'  => 'gi gi-home',
			'url'   => base_url('customer/home/index'),
			'page_name'=> 'index'
		),
	

		array(
			'name'  => 'Invoices & Payments',
			'icon'  => 'gi gi-list',
			'sub'   => array(
				array(
					'name'  => 'Invoices',
					'url'   => base_url('customer/home/invoices'),
					'page_name'=> 'invoices'
				),
				array(
					'name'  => 'Payments',
					'url'   => base_url('customer/Payments/payment_transaction'),
					'page_name'=> 'payment_transaction'
				),
			
			)
		),
		
			array(
			'name'  => 'Customer Card',
			'icon'  => 'fa fa-credit-card',
			'url'   => base_url('customer/Payments/customer_card'),
			'page_name'=> 'customer_card'
		),
		
	
	
	);
	return $primary_nav;	
}

  


?>