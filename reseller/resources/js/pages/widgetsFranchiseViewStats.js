/*
 *  Document   : widgetsStats.js
 *  Author     : Ecrubit
 *  Description: Custom javascript code used in Statistic Widgets page
 */

var WidgetsStats = function() {

    return {
        init: function() {
            
            var miniChartLineOptions = {
                type: 'line',
                width: '270px',
                height: '150px',
                tooltipOffsetX: -25,
                tooltipOffsetY: 20,
                lineWidth: 1,
                lineColor: '#3b3f40',
                fillColor: '#399399',
                spotColor: '#ffffff',
                minSpotColor: '#ffffff',
                maxSpotColor: '#ffffff',
                highlightSpotColor: '#ffffff',
                highlightLineColor: '#ffffff',
                spotRadius: 5,
                tooltipPrefix: '$ ',
                tooltipSuffix: '',
                tooltipFormat: '{{prefix}}{{y}}{{suffix}}'
            };
            
            miniChartLineOptions['lineColor'] = '#333333';
            miniChartLineOptions['fillColor'] = '#777777';
            miniChartLineOptions['tooltipPrefix'] = '+ ';
            miniChartLineOptions['tooltipSuffix'] = ' Earnings';
            $('#widget-mini-chart-line2').sparkline('html', miniChartLineOptions);
	
        }
    };
}();