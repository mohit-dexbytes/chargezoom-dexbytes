(function($) {
    function syncDashboardData() {
        $(window).load(function() {
            $.ajax({
                url: $('#rs_base_url').val() + 'Reseller_panel/reseller_merchant_dashboard_chart',
                data: {
                    is_cron: 1
                },
                success: function(data) {
                    if (data) {
                        console.log('---- Dashboard Updated ------');
                    }
                }
            });
        });
    }
    syncDashboardData();
    // for this example we take 3000 milliseconds
    let interval = 3000;
    setInterval(syncDashboardData, interval);
})(jQuery);